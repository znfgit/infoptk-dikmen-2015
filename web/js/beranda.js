$.ajax({
    type: "GET",
    url: "session",
    success: function(isi){

        var obj = jQuery.parseJSON(isi);

        $.ajax({
            type: "GET",
            url: "getPtk",
            data:{ptk_id: obj.ptk_id},
            success: function(isi){

                var obj = jQuery.parseJSON(isi);

                var keys = Object.keys(obj);

                // console.log(keys);

                for (var i = keys.length - 1; i >= 0; i--) {
                    // keys[i]
                    console.log(keys[i]);

                    $('.'+keys[i]+'_value').html(obj[keys[i]]);

                    if(obj[keys[i]] != ''){
                        $('.'+keys[i]+'_keterangan').html('<span class="icon" style="color:green;font-size:18px">&#61528</span>');
                    }else{
                        $('.'+keys[i]+'_keterangan').html('<span class="icon" style="color:#ffd800;font-size:18px">&#61553</span> &nbsp; Belum Diisi');
                    }

                    // if(obj['rt'] == 0){
                    //     $('.rt_keterangan').html('<span class="icon" style="color:#ffd800;font-size:18px">&#61553</span> &nbsp; Data belum lengkap');
                    // }

                    // if(obj['rw'] == 0){
                    //     $('.rw_keterangan').html('<span class="icon" style="color:#ffd800;font-size:18px">&#61553</span> &nbsp; Data belum lengkap');
                    // }

                    // if(obj['nama_dusun'] == null){
                    //     $('.nama_dusun_keterangan').html('<span class="icon" style="color:#ffd800;font-size:18px">&#61553</span> &nbsp; Data belum lengkap');
                    // }

                    // if(obj['niy_nigk'] == null){
                    //     $('.niy_nigk_keterangan').html('<span class="icon" style="color:#ffd800;font-size:18px">&#61553</span> &nbsp; Data belum lengkap');
                    // }
                    // console.log(obj['nik'].length);

                    if(obj['nik'].length < 16){
                        $('.nik_keterangan').html('<span class="icon" style="color:red;font-size:18px">&#61527</span> &nbsp; Data Belum Valid');
                    }

                    // console.log(obj['npwp'].length);
                    if(obj['npwp'].length < 15){
                        $('.npwp_keterangan').html('<span class="icon" style="color:red;font-size:18px">&#61527</span> &nbsp; Data Belum Valid');
                    }

                    if(obj['status_kepegawaian_id'] != "1"){

                        $('.sk_cpns_panel').css("color", "#dddddd");
                        $('.sk_cpns_keterangan').html('');

                        $('.tgl_cpns_panel').css("color", "#dddddd");
                        $('.tgl_cpns_keterangan').html('');

                        $('.sk_pengangkatan_panel').css("color", "#dddddd");
                        $('.sk_pengangkatan_keterangan').html('');

                        $('.tmt_pengangkatan_panel').css("color", "#dddddd");
                        $('.tmt_pengangkatan_keterangan').html('');

                        $('.tmt_pengangkatan_panel').css("color", "#dddddd");
                        $('.tmt_pengangkatan_keterangan').html('');

                        $('.lembaga_pengangkat_id_str_panel').css("color", "#dddddd");
                        $('.lembaga_pengangkat_id_str_keterangan').html('');

                        $('.pangkat_golongan_id_str_panel').css("color", "#dddddd");
                        $('.pangkat_golongan_id_str_keterangan').html('');

                        $('.tmt_pns_panel').css("color", "#dddddd");
                        $('.tmt_pns_keterangan').html('');
                    }

                    if(obj['status_perkawinan'] != "1"){
                        $('.nama_suami_istri_panel').css("color", "#dddddd");
                        $('.nama_suami_istri_keterangan').html('');

                        $('.nip_suami_istri_panel').css("color", "#dddddd");
                        $('.nip_suami_istri_keterangan').html('');

                        $('.pekerjaan_suami_istri_str_panel').css("color", "#dddddd");
                        $('.pekerjaan_suami_istri_str_keterangan').html('');
                    }

                };

                if(obj['jenis_kelamin'] == 'L'){
                    $('.jenis_kelamin_value').html('Laki-laki');
                }else if(obj['jenis_kelamin'] == 'P'){
                    $('.jenis_kelamin_value').html('Perempuan');
                }

                if(obj['status_perkawinan'] == 1){
                    $('.status_perkawinan_value').html('Kawin');
                }else if(obj['status_perkawinan'] == 2){
                    $('.status_perkawinan_value').html('Belum Kawin');
                }else if(obj['status_perkawinan'] == 3){
                    $('.status_perkawinan_value').html('Janda / Duda');
                }

                if(obj['sudah_lisensi_kepala_sekolah'] == 0){
                    $('.sudah_lisensi_kepala_sekolah_value').html('Tidak');
                }else{
                    $('.sudah_lisensi_kepala_sekolah_value').html('Ya');
                }
                
                if(obj['pernah_diklat_kepengawasan'] == 0){
                    $('.pernah_diklat_kepengawasan_value').html('Tidak');
                }else{
                    $('.pernah_diklat_kepengawasan_value').html('Ya');
                }

                if(obj['mampu_handle_kk'] == 0){
                    $('.mampu_handle_kk_value').html('Tidak');
                }else{
                    $('.mampu_handle_kk_value').html('Ya');
                }

                if(obj['keahlian_braille'] == 0){
                    $('.keahlian_braille_value').html('Tidak');
                }else{
                    $('.keahlian_braille_value').html('Ya');
                }

                if(obj['keahlian_bhs_isyarat'] == 0){
                    $('.keahlian_bhs_isyarat_value').html('Tidak');
                }else{
                    $('.keahlian_bhs_isyarat_value').html('Ya');
                }


            }
        });
         
    }
});

