$('input.submit').click(function (e){
	var username = $('.username').val();
	var password = $('.password').val();

    $('.submit').html('Validasi Pengguna...');

	$.ajax({
        data: { username: username, password: password },
        type: "POST",
        url: "loginptk",
        success: function(isi){

            var obj = jQuery.parseJSON(isi);

            if(obj.success == true){
                $('#login').append('<div class="alert alert-dismissable alert-success peringatan">' + 
                    '<button type="button" class="close" data-dismiss="alert">×</button>' +
                    obj.message + 
                '</div>');

                setTimeout("location.href = '/';",2000);
            }else{
                $('#login').append('<div class="alert alert-dismissable alert-danger peringatan">' + 
                    '<button type="button" class="close" data-dismiss="alert">×</button>' +
                    obj.message + 
                '</div>');
            }

            // console.log(isi);
        }
    });

});