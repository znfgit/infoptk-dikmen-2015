<?php
	error_reporting(E_ERROR);

	define ('ROOT', dirname(__FILE__).'/../');
	define ('D', DIRECTORY_SEPARATOR);
	define ('P', PATH_SEPARATOR);
	define ('SYSDIR', ROOT.D.'system'.D);
	define ('APPNAME', 'angulex');
	define ('DBNAME', 'Dapodikmen');
	define ('WORK_OFFLINE', false);
	define ('SEMESTER_BERJALAN', 20142);
	define ('TA_BERJALAN', 2014);

	// Prepare Class Environment
	$loader = require __DIR__.'/../vendor/autoload.php';
	$loader->add('angulex', __DIR__.'/../src/');

	//usable functions
	require_once '../src/functions.php';

	// Initialize the App
	$app = new Silex\Application();
	$app['debug'] = true;

	//  Propel
	$app['propel.config_file'] = __DIR__.'/config'.$dbms.'/conf/'.DBNAME.'-conf.php';
	$app['propel.model_path'] = __DIR__.'/../src/';
	$app->register(new Propel\Silex\PropelServiceProvider());

	// Register Twig
	$app->register(new Silex\Provider\TwigServiceProvider(), array(
		'twig.path' => __DIR__.'/../web',
	));

	// Register Session
	$app->register(new Silex\Provider\SessionServiceProvider());

	// Register for auth too
	$app->register(new Silex\Provider\UrlGeneratorServiceProvider());

	// Set TIMEZONE
	date_default_timezone_set('Asia/Jakarta');

	return $app;
