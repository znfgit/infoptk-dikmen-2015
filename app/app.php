<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\Debug\Debug;

require_once __DIR__.'/bootstrap.php';

/**
 * Enable Silex Error
 */
// $app->error(function (\Exception $e, $code) use ($app){
//     switch ($code) {
//         case 404:
//             $message = 'Halaman yang diminta tidak dapat ditampilkan.';
        	
//             break;
//         default:
//             $message = 'Mohon Maaf. Ada kesalahan dalam sistem.';
//     }

//     $array = array(
//         "message" => $message
//     );

//     return $app['twig']->render('err.twig', $array);
// });

// default path
$app->get('/', function() use ($app){

    if($app['session']->get('username')){

        $array = array(
            'konektor' => "",
            'judul' => "Info PTK Dikmen"
        );

        return $app['twig']->render('index.twig', $array);
    }else{
        $array = array(
            'konektor' => "",
            'judul' => "Info PTK Dikmen"
        );

        return $app['twig']->render('index_login.html', $array);
    }


    
});

$app->get('laman/{p}', function($p) use ($app){
    $array = array(
        'p' => $p,
        'konektor' => "../",
        'laman' => "",
        'tahun' => date('Y')
    );

    return $app['twig']->render('templates/'.$p.'.twig', $array);
});

$app->post('loginptk', 'angulex\Auth::login');
$app->get('session', 'angulex\Auth::session');
$app->get('logout', 'angulex\Auth::logout');

$app->get('getPtk', 'angulex\System::getPtk');
$app->post('getJamMengajar', 'angulex\System::getJamMengajar');
$app->get('getSemester', 'angulex\System::getSemester');
$app->get('getTugasTambahan', 'angulex\System::getTugasTambahan');
$app->post('getTugasTambahan', 'angulex\System::getTugasTambahan');
$app->post('getSertifikasi', 'angulex\System::getSertifikasi');

$app->get('testing', 'angulex\System::teskoneksi');


return $app;

?>