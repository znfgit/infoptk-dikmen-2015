
-----------------------------------------------------------------------
-- pengguna
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'pengguna')
BEGIN
    DECLARE @reftable_1 nvarchar(60), @constraintname_1 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'pengguna'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_1, @constraintname_1
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_1+' drop constraint '+@constraintname_1)
        FETCH NEXT from refcursor into @reftable_1, @constraintname_1
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [pengguna]
END

CREATE TABLE [pengguna]
(
    [pengguna_id] BIGINT NOT NULL IDENTITY,
    [username] VARCHAR(100) NULL,
    [password] VARCHAR(100) NULL,
    [nama] VARCHAR(100) NULL,
    [nip_nim] VARCHAR(100) NULL,
    [kabupaten_kota_id] CHAR(7) NULL,
    [propinsi_id] CHAR(7) NULL,
    [aktif] SMALLINT NULL,
    CONSTRAINT [pengguna_PK] PRIMARY KEY ([pengguna_id])
);

-----------------------------------------------------------------------
-- provinsi
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'provinsi')
BEGIN
    DECLARE @reftable_2 nvarchar(60), @constraintname_2 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'provinsi'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_2, @constraintname_2
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_2+' drop constraint '+@constraintname_2)
        FETCH NEXT from refcursor into @reftable_2, @constraintname_2
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [provinsi]
END

CREATE TABLE [provinsi]
(
    [privinsi_id] INT NOT NULL,
    [nama] VARCHAR(255) NULL,
    CONSTRAINT [provinsi_PK] PRIMARY KEY ([privinsi_id])
);

-----------------------------------------------------------------------
-- kabupaten_kota
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'kabupaten_kota')
BEGIN
    DECLARE @reftable_3 nvarchar(60), @constraintname_3 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'kabupaten_kota'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_3, @constraintname_3
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_3+' drop constraint '+@constraintname_3)
        FETCH NEXT from refcursor into @reftable_3, @constraintname_3
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [kabupaten_kota]
END

CREATE TABLE [kabupaten_kota]
(
    [kabupaten_kota_id] INT NOT NULL,
    [propinsi_id] INT NULL,
    [nama] VARCHAR(255) NULL,
    CONSTRAINT [kabupaten_kota_PK] PRIMARY KEY ([kabupaten_kota_id])
);

-----------------------------------------------------------------------
-- kecamatan
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'kecamatan')
BEGIN
    DECLARE @reftable_4 nvarchar(60), @constraintname_4 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'kecamatan'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_4, @constraintname_4
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_4+' drop constraint '+@constraintname_4)
        FETCH NEXT from refcursor into @reftable_4, @constraintname_4
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [kecamatan]
END

CREATE TABLE [kecamatan]
(
    [Kecamatan_id] INT NOT NULL,
    [kabupaten_kota_id] INT NULL,
    [nama] VARCHAR(255) NULL,
    CONSTRAINT [kecamatan_PK] PRIMARY KEY ([Kecamatan_id])
);
