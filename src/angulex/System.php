<?php
namespace angulex;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use angulex\Model;

class System
{
	function teskoneksi(Request $Request, Application $app){
		$fetchArr = array();

// SELECT Orders.OrderID, Customers.CustomerName, Orders.OrderDate
// FROM Orders
// INNER JOIN Customers
// ON Orders.CustomerID=Customers.CustomerID;
// $sqlahmad = select top 10 * from pengguna INNER join
		$sql = "select top 10 * from pengguna";
		// $sql = "select top 10 * from ptk,pengguna where ptk.ptk_id = pengguna.pengguna_id ";
		// $sql = "select top 10 * from ptk,pengguna where soft_delete = 0 ";
		// $sql = "select top 10 * from ptk where ptk_id='EB55F7EB-3A1F-4CCF-B18E-16FD081540B0'";
		// $sql = "select top 10 * from ptk where soft_delete = 0 and ptk_id='EB55F7EB-3A1F-4CCF-B18E-16FD081540B0'";


		$fetch = getDataBySql($sql);
// print_r($fetch); die();
		foreach ($fetch as $f) {
			foreach ($f as $fc => $v) {
				$arr[$fc] = $v;

			}

			array_push($fetchArr, $arr);
		}

		return json_encode($fetchArr);
	}

	function getPtk(Request $request, Application $app){
		$ptk_id = $request->get('ptk_id');
		$fetchArr = array();
		$arr = array();

		$sql = "SELECT
					ptk.*, ptkd.*, jptk.jenis_ptk AS jenis_ptk_id_str,
					sk.nama AS status_kepegawaian_id_str,
					skp.nama AS status_keaktifan_id_str,
					ptk.nama AS 'nama_ptk',
					w1.nama AS kecamatan,
					w2.nama AS kabupaten,
					w3.nama AS propinsi,
					w1.kode_wilayah AS kode_kecamatan,
					w2.kode_wilayah AS kode_kab,
					w3.kode_wilayah AS kode_prop,
					ag.nama as agama_id_str,
					lp.nama as lembaga_pengangkat_id_str,
					pg.nama as pangkat_golongan_id_str,
					sg.nama as sumber_gaji_id_str,
					kl.nama as keahlian_laboratorium_id_str,
					pkj.nama as pekerjaan_suami_istri_str
				FROM
					ptk ptk
				JOIN ref.mst_wilayah w1 ON ptk.kode_wilayah = w1.kode_wilayah
				JOIN ref.mst_wilayah w2 ON w1.mst_kode_wilayah = w2.kode_wilayah
				JOIN ref.mst_wilayah w3 ON w2.mst_kode_wilayah = w3.kode_wilayah
				JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
				JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
				LEFT JOIN ref.status_kepegawaian sk ON sk.status_kepegawaian_id = ptk.status_kepegawaian_id
				LEFT JOIN ref.jenis_ptk jptk ON jptk.jenis_ptk_id = ptk.jenis_ptk_id
				LEFT JOIN ref.status_keaktifan_pegawai skp ON skp.status_keaktifan_id = ptk.status_keaktifan_id
				LEFT JOIN ref.lembaga_pengangkat lp ON lp.lembaga_pengangkat_id = ptk.lembaga_pengangkat_id
				LEFT JOIN ref.pangkat_golongan pg ON pg.pangkat_golongan_id = ptk.pangkat_golongan_id
				LEFT JOIN ref.agama ag on ag.agama_id = ptk.agama_id
				LEFT JOIN ref.sumber_gaji sg on sg.sumber_gaji_id = ptk.sumber_gaji_id
				LEFT JOIN ref.keahlian_laboratorium kl on kl.keahlian_laboratorium_id = ptk.keahlian_laboratorium_id
				LEFT JOIN ref.pekerjaan pkj on pkj.pekerjaan_id = ptk.pekerjaan_suami_istri
				WHERE
					ptk.Soft_delete = 0
				AND ptkd.Soft_delete = 0
				AND (
						(
							ptkd.jenis_keluar_id is null and ptkd.tgl_ptk_keluar is null
						) OR (
							ptkd.jenis_keluar_id is not null and ptkd.tgl_ptk_keluar > ta.tanggal_selesai
						) OR (
							ptkd.jenis_keluar_id is null
						)
					)
				AND ptkd.tahun_ajaran_id = ".TA_BERJALAN;

		if(!empty($ptk_id)){
			$sql .= " AND ptk.ptk_id = '".$ptk_id."'";
		}

		// return $sql;die;

		$fetch = getDataBySql($sql);

		foreach ($fetch as $f) {
			
			foreach ($f as $fc => $v) {
				$arr[$fc] = space_char_remover($v);

			}
			
			// array_push($fetchArr, $arr);
		}	

		return json_encode($arr);

	}

	function getJamMengajar(Request $request, Application $app){
		$ptk_id = $request->get('ptk_id');
		$semester_id = $request->get('semester_id');
		$current = $request->get('current');
		$rowCount = $request->get('rowCount');
		$sort = $request->get('sort');

		$fetchArr = array();
		$arr = array();

		$sql = "SELECT
					pb.*,
					ptkt.*,
					rb.nama as rombongan_belajar_id_str,
					sm.nama as semester_id_str
				FROM
					pembelajaran pb
				JOIN ptk_terdaftar ptkt ON ptkt.ptk_terdaftar_id = pb.ptk_terdaftar_id
				JOIN rombongan_belajar rb on rb.rombongan_belajar_id = pb.rombongan_belajar_id
				JOIN ref.semester sm on sm.semester_id = pb.semester_id
				where pb.Soft_delete = 0
				and ptkt.Soft_delete = 0
				and ptkt.jenis_keluar_id is null
				and ptkt.ptk_id = '{$ptk_id}'";

		if($semester_id){
			$sql .= " AND pb.semester_id = ".$semester_id;
		}

		$fetch = getDataBySql($sql);

		foreach ($fetch as $f) {
			
			foreach ($f as $fc => $v) {
				$arr[$fc] = space_char_remover($v);

			}

			$count++;
			
			array_push($fetchArr, $arr);
		}	

		return json_encode($fetchArr);
		// $returnArr = array();
		// $returnArr['current'] = $current;
		// $returnArr['rowCount'] = $rowCount;
		// $returnArr['rows'] = $fetchArr;
		// $returnArr['total'] = $count;

		// return json_encode($returnArr);
	}

	function getSemester(Request $request, Application $app){
		$fetchArr = array();
		$arr = array();

		$sql = "select * from ref.semester where expired_date is null";

		$fetch = getDataBySql($sql);

		foreach ($fetch as $f) {
			
			foreach ($f as $fc => $v) {
				$arr[$fc] = space_char_remover($v);

			}

			$count++;
			
			array_push($fetchArr, $arr);
		}

		return json_encode($fetchArr);

	}

        
        function getTugasTambahan(Request $request, Application $app){
//	return "tugas tambahan done";
//            die();
                $fetchArr = array();
		$arr = array();

		$sql = "SELECT
	ptk.nama,
	tugas_tambahan.tmt_tambahan,
	tugas_tambahan.tst_tambahan,
	jabatan_tugas_ptk.nama nama_tugas
            FROM
	tugas_tambahan
           JOIN ptk ON ptk.ptk_id = tugas_tambahan.ptk_id
           JOIN ref.jabatan_tugas_ptk ON ref.jabatan_tugas_ptk.jabatan_ptk_id = tugas_tambahan.jabatan_ptk_id
        WHERE
            ptk.ptk_id = '1C4CC3ED-7CDB-4F74-8F32-AE04432AD9C0'";
		$fetch = getDataBySql($sql);
//                $fetch = Model\TugasTambahanPeer::doSelect(new \Criteria());
//                print_r($fetch);
//                die();
		foreach ($fetch as $f) {
			
			foreach ($f as $fc => $v) {
				$arr[$fc] = space_char_remover($v);

			}

			$count++;
			
			array_push($fetchArr, $arr);
		}

		return json_encode($fetchArr);

	}

	function getSertifikasi(Request $request, Application $app){
		$fetchArr = array();
		$arr = array();
		$ptk_id = $request->get('ptk_id');

		$sql = "SELECT
					bs.bidang_studi AS bidang_studi_id_str,
					js.jenis_sertifikasi AS jenis_sertifikasi_id_str,
					rs.*
				FROM
					rwy_sertifikasi rs
				JOIN ref.bidang_studi bs ON bs.bidang_studi_id = rs.bidang_studi_id
				JOIN ref.jenis_sertifikasi js ON js.id_jenis_sertifikasi = rs.id_jenis_sertifikasi
				WHERE
					ptk_id = '{$ptk_id}'
				AND Soft_delete = 0";

		$fetch = getDataBySql($sql);

		foreach ($fetch as $f) {
			
			foreach ($f as $fc => $v) {
				$arr[$fc] = space_char_remover($v);

			}

			$count++;
			
			array_push($fetchArr, $arr);
		}

		return json_encode($fetchArr);
	}
}
?>