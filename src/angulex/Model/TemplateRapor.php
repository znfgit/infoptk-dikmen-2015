<?php

namespace angulex\Model;

use angulex\Model\om\BaseTemplateRapor;


/**
 * Skeleton subclass for representing a row from the 'ref.template_rapor' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.angulex.Model
 */
class TemplateRapor extends BaseTemplateRapor
{
}
