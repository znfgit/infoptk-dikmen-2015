<?php

namespace angulex\Model;

use angulex\Model\om\BaseBlockgrant;


/**
 * Skeleton subclass for representing a row from the 'blockgrant' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.angulex.Model
 */
class Blockgrant extends BaseBlockgrant
{
}
