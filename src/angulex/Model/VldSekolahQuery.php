<?php

namespace angulex\Model;

use angulex\Model\om\BaseVldSekolahQuery;


/**
 * Skeleton subclass for performing query and update operations on the 'vld_sekolah' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.angulex.Model
 */
class VldSekolahQuery extends BaseVldSekolahQuery
{
}
