<?php

namespace angulex\Model;

use angulex\Model\om\BaseWdstSyncLog;


/**
 * Skeleton subclass for representing a row from the 'wdst_sync_log' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.angulex.Model
 */
class WdstSyncLog extends BaseWdstSyncLog
{
}
