<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.pangkat_golongan' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class PangkatGolonganTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.PangkatGolonganTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.pangkat_golongan');
        $this->setPhpName('PangkatGolongan');
        $this->setClassname('angulex\\Model\\PangkatGolongan');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('pangkat_golongan_id', 'PangkatGolonganId', 'NUMERIC', true, 4, null);
        $this->addColumn('kode', 'Kode', 'VARCHAR', true, 5, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 20, null);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('InpassingRelatedByPangkatGolonganId', 'angulex\\Model\\Inpassing', RelationMap::ONE_TO_MANY, array('pangkat_golongan_id' => 'pangkat_golongan_id', ), null, null, 'InpassingsRelatedByPangkatGolonganId');
        $this->addRelation('InpassingRelatedByPangkatGolonganId', 'angulex\\Model\\Inpassing', RelationMap::ONE_TO_MANY, array('pangkat_golongan_id' => 'pangkat_golongan_id', ), null, null, 'InpassingsRelatedByPangkatGolonganId');
        $this->addRelation('RwyKepangkatanRelatedByPangkatGolonganId', 'angulex\\Model\\RwyKepangkatan', RelationMap::ONE_TO_MANY, array('pangkat_golongan_id' => 'pangkat_golongan_id', ), null, null, 'RwyKepangkatansRelatedByPangkatGolonganId');
        $this->addRelation('RwyKepangkatanRelatedByPangkatGolonganId', 'angulex\\Model\\RwyKepangkatan', RelationMap::ONE_TO_MANY, array('pangkat_golongan_id' => 'pangkat_golongan_id', ), null, null, 'RwyKepangkatansRelatedByPangkatGolonganId');
        $this->addRelation('PtkRelatedByPangkatGolonganId', 'angulex\\Model\\Ptk', RelationMap::ONE_TO_MANY, array('pangkat_golongan_id' => 'pangkat_golongan_id', ), null, null, 'PtksRelatedByPangkatGolonganId');
        $this->addRelation('PtkRelatedByPangkatGolonganId', 'angulex\\Model\\Ptk', RelationMap::ONE_TO_MANY, array('pangkat_golongan_id' => 'pangkat_golongan_id', ), null, null, 'PtksRelatedByPangkatGolonganId');
        $this->addRelation('RiwayatGajiBerkalaRelatedByPangkatGolonganId', 'angulex\\Model\\RiwayatGajiBerkala', RelationMap::ONE_TO_MANY, array('pangkat_golongan_id' => 'pangkat_golongan_id', ), null, null, 'RiwayatGajiBerkalasRelatedByPangkatGolonganId');
        $this->addRelation('RiwayatGajiBerkalaRelatedByPangkatGolonganId', 'angulex\\Model\\RiwayatGajiBerkala', RelationMap::ONE_TO_MANY, array('pangkat_golongan_id' => 'pangkat_golongan_id', ), null, null, 'RiwayatGajiBerkalasRelatedByPangkatGolonganId');
    } // buildRelations()

} // PangkatGolonganTableMap
