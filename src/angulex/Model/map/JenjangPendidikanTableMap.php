<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.jenjang_pendidikan' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class JenjangPendidikanTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.JenjangPendidikanTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.jenjang_pendidikan');
        $this->setPhpName('JenjangPendidikan');
        $this->setClassname('angulex\\Model\\JenjangPendidikan');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('jenjang_pendidikan_id', 'JenjangPendidikanId', 'NUMERIC', true, 4, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 25, null);
        $this->addColumn('jenjang_lembaga', 'JenjangLembaga', 'NUMERIC', true, 3, null);
        $this->addColumn('jenjang_orang', 'JenjangOrang', 'NUMERIC', true, 3, null);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('TingkatPendidikan', 'angulex\\Model\\TingkatPendidikan', RelationMap::ONE_TO_MANY, array('jenjang_pendidikan_id' => 'jenjang_pendidikan_id', ), null, null, 'TingkatPendidikans');
        $this->addRelation('TemplateUn', 'angulex\\Model\\TemplateUn', RelationMap::ONE_TO_MANY, array('jenjang_pendidikan_id' => 'jenjang_pendidikan_id', ), null, null, 'TemplateUns');
        $this->addRelation('PesertaDidikRelatedByJenjangPendidikanIbu', 'angulex\\Model\\PesertaDidik', RelationMap::ONE_TO_MANY, array('jenjang_pendidikan_id' => 'jenjang_pendidikan_ibu', ), null, null, 'PesertaDidiksRelatedByJenjangPendidikanIbu');
        $this->addRelation('PesertaDidikRelatedByJenjangPendidikanAyah', 'angulex\\Model\\PesertaDidik', RelationMap::ONE_TO_MANY, array('jenjang_pendidikan_id' => 'jenjang_pendidikan_ayah', ), null, null, 'PesertaDidiksRelatedByJenjangPendidikanAyah');
        $this->addRelation('PesertaDidikRelatedByJenjangPendidikanWali', 'angulex\\Model\\PesertaDidik', RelationMap::ONE_TO_MANY, array('jenjang_pendidikan_id' => 'jenjang_pendidikan_wali', ), null, null, 'PesertaDidiksRelatedByJenjangPendidikanWali');
        $this->addRelation('PesertaDidikRelatedByJenjangPendidikanIbu', 'angulex\\Model\\PesertaDidik', RelationMap::ONE_TO_MANY, array('jenjang_pendidikan_id' => 'jenjang_pendidikan_ibu', ), null, null, 'PesertaDidiksRelatedByJenjangPendidikanIbu');
        $this->addRelation('PesertaDidikRelatedByJenjangPendidikanAyah', 'angulex\\Model\\PesertaDidik', RelationMap::ONE_TO_MANY, array('jenjang_pendidikan_id' => 'jenjang_pendidikan_ayah', ), null, null, 'PesertaDidiksRelatedByJenjangPendidikanAyah');
        $this->addRelation('PesertaDidikRelatedByJenjangPendidikanWali', 'angulex\\Model\\PesertaDidik', RelationMap::ONE_TO_MANY, array('jenjang_pendidikan_id' => 'jenjang_pendidikan_wali', ), null, null, 'PesertaDidiksRelatedByJenjangPendidikanWali');
        $this->addRelation('Kurikulum', 'angulex\\Model\\Kurikulum', RelationMap::ONE_TO_MANY, array('jenjang_pendidikan_id' => 'jenjang_pendidikan_id', ), null, null, 'Kurikulums');
        $this->addRelation('RwyPendFormalRelatedByJenjangPendidikanId', 'angulex\\Model\\RwyPendFormal', RelationMap::ONE_TO_MANY, array('jenjang_pendidikan_id' => 'jenjang_pendidikan_id', ), null, null, 'RwyPendFormalsRelatedByJenjangPendidikanId');
        $this->addRelation('RwyPendFormalRelatedByJenjangPendidikanId', 'angulex\\Model\\RwyPendFormal', RelationMap::ONE_TO_MANY, array('jenjang_pendidikan_id' => 'jenjang_pendidikan_id', ), null, null, 'RwyPendFormalsRelatedByJenjangPendidikanId');
        $this->addRelation('AnakRelatedByJenjangPendidikanId', 'angulex\\Model\\Anak', RelationMap::ONE_TO_MANY, array('jenjang_pendidikan_id' => 'jenjang_pendidikan_id', ), null, null, 'AnaksRelatedByJenjangPendidikanId');
        $this->addRelation('AnakRelatedByJenjangPendidikanId', 'angulex\\Model\\Anak', RelationMap::ONE_TO_MANY, array('jenjang_pendidikan_id' => 'jenjang_pendidikan_id', ), null, null, 'AnaksRelatedByJenjangPendidikanId');
    } // buildRelations()

} // JenjangPendidikanTableMap
