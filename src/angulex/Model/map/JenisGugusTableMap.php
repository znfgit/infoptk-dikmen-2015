<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.jenis_gugus' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class JenisGugusTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.JenisGugusTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.jenis_gugus');
        $this->setPhpName('JenisGugus');
        $this->setClassname('angulex\\Model\\JenisGugus');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('jenis_gugus_id', 'JenisGugusId', 'NUMERIC', true, 5, null);
        $this->addColumn('jenis_gugus', 'JenisGugus', 'VARCHAR', true, 30, null);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('GugusSekolahRelatedByJenisGugusId', 'angulex\\Model\\GugusSekolah', RelationMap::ONE_TO_MANY, array('jenis_gugus_id' => 'jenis_gugus_id', ), null, null, 'GugusSekolahsRelatedByJenisGugusId');
        $this->addRelation('GugusSekolahRelatedByJenisGugusId', 'angulex\\Model\\GugusSekolah', RelationMap::ONE_TO_MANY, array('jenis_gugus_id' => 'jenis_gugus_id', ), null, null, 'GugusSekolahsRelatedByJenisGugusId');
    } // buildRelations()

} // JenisGugusTableMap
