<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.template_un' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class TemplateUnTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.TemplateUnTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.template_un');
        $this->setPhpName('TemplateUn');
        $this->setClassname('angulex\\Model\\TemplateUn');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('template_id', 'TemplateId', 'CHAR', true, 16, null);
        $this->addForeignKey('jenjang_pendidikan_id', 'JenjangPendidikanId', 'NUMERIC', 'ref.jenjang_pendidikan', 'jenjang_pendidikan_id', true, 4, null);
        $this->addForeignKey('tahun_ajaran_id', 'TahunAjaranId', 'NUMERIC', 'ref.tahun_ajaran', 'tahun_ajaran_id', true, 6, null);
        $this->addForeignKey('jurusan_id', 'JurusanId', 'VARCHAR', 'ref.jurusan', 'jurusan_id', false, 25, null);
        $this->addColumn('template_ket', 'TemplateKet', 'VARCHAR', false, 250, null);
        $this->addForeignKey('mp1_id', 'Mp1Id', 'INTEGER', 'ref.mata_pelajaran', 'mata_pelajaran_id', false, 4, null);
        $this->addForeignKey('mp2_id', 'Mp2Id', 'INTEGER', 'ref.mata_pelajaran', 'mata_pelajaran_id', false, 4, null);
        $this->addForeignKey('mp3_id', 'Mp3Id', 'INTEGER', 'ref.mata_pelajaran', 'mata_pelajaran_id', false, 4, null);
        $this->addForeignKey('mp4_id', 'Mp4Id', 'INTEGER', 'ref.mata_pelajaran', 'mata_pelajaran_id', false, 4, null);
        $this->addForeignKey('mp5_id', 'Mp5Id', 'INTEGER', 'ref.mata_pelajaran', 'mata_pelajaran_id', false, 4, null);
        $this->addForeignKey('mp6_id', 'Mp6Id', 'INTEGER', 'ref.mata_pelajaran', 'mata_pelajaran_id', false, 4, null);
        $this->addForeignKey('mp7_id', 'Mp7Id', 'INTEGER', 'ref.mata_pelajaran', 'mata_pelajaran_id', false, 4, null);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('JenjangPendidikan', 'angulex\\Model\\JenjangPendidikan', RelationMap::MANY_TO_ONE, array('jenjang_pendidikan_id' => 'jenjang_pendidikan_id', ), null, null);
        $this->addRelation('Jurusan', 'angulex\\Model\\Jurusan', RelationMap::MANY_TO_ONE, array('jurusan_id' => 'jurusan_id', ), null, null);
        $this->addRelation('MataPelajaranRelatedByMp3Id', 'angulex\\Model\\MataPelajaran', RelationMap::MANY_TO_ONE, array('mp3_id' => 'mata_pelajaran_id', ), null, null);
        $this->addRelation('MataPelajaranRelatedByMp4Id', 'angulex\\Model\\MataPelajaran', RelationMap::MANY_TO_ONE, array('mp4_id' => 'mata_pelajaran_id', ), null, null);
        $this->addRelation('MataPelajaranRelatedByMp7Id', 'angulex\\Model\\MataPelajaran', RelationMap::MANY_TO_ONE, array('mp7_id' => 'mata_pelajaran_id', ), null, null);
        $this->addRelation('MataPelajaranRelatedByMp5Id', 'angulex\\Model\\MataPelajaran', RelationMap::MANY_TO_ONE, array('mp5_id' => 'mata_pelajaran_id', ), null, null);
        $this->addRelation('MataPelajaranRelatedByMp1Id', 'angulex\\Model\\MataPelajaran', RelationMap::MANY_TO_ONE, array('mp1_id' => 'mata_pelajaran_id', ), null, null);
        $this->addRelation('MataPelajaranRelatedByMp2Id', 'angulex\\Model\\MataPelajaran', RelationMap::MANY_TO_ONE, array('mp2_id' => 'mata_pelajaran_id', ), null, null);
        $this->addRelation('MataPelajaranRelatedByMp6Id', 'angulex\\Model\\MataPelajaran', RelationMap::MANY_TO_ONE, array('mp6_id' => 'mata_pelajaran_id', ), null, null);
        $this->addRelation('TahunAjaran', 'angulex\\Model\\TahunAjaran', RelationMap::MANY_TO_ONE, array('tahun_ajaran_id' => 'tahun_ajaran_id', ), null, null);
        $this->addRelation('TemplateRapor', 'angulex\\Model\\TemplateRapor', RelationMap::ONE_TO_MANY, array('template_id' => 'template_id', ), null, null, 'TemplateRapors');
    } // buildRelations()

} // TemplateUnTableMap
