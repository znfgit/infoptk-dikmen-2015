<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'jurusan_kerjasama' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class JurusanKerjasamaTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.JurusanKerjasamaTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('jurusan_kerjasama');
        $this->setPhpName('JurusanKerjasama');
        $this->setClassname('angulex\\Model\\JurusanKerjasama');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addForeignPrimaryKey('mou_id', 'MouId', 'CHAR' , 'mou', 'mou_id', true, 16, null);
        $this->addForeignPrimaryKey('mou_id', 'MouId', 'CHAR' , 'mou', 'mou_id', true, 16, null);
        $this->addForeignPrimaryKey('jurusan_sp_id', 'JurusanSpId', 'CHAR' , 'jurusan_sp', 'jurusan_sp_id', true, 16, null);
        $this->addForeignPrimaryKey('jurusan_sp_id', 'JurusanSpId', 'CHAR' , 'jurusan_sp', 'jurusan_sp_id', true, 16, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('JurusanSpRelatedByJurusanSpId', 'angulex\\Model\\JurusanSp', RelationMap::MANY_TO_ONE, array('jurusan_sp_id' => 'jurusan_sp_id', ), null, null);
        $this->addRelation('JurusanSpRelatedByJurusanSpId', 'angulex\\Model\\JurusanSp', RelationMap::MANY_TO_ONE, array('jurusan_sp_id' => 'jurusan_sp_id', ), null, null);
        $this->addRelation('MouRelatedByMouId', 'angulex\\Model\\Mou', RelationMap::MANY_TO_ONE, array('mou_id' => 'mou_id', ), null, null);
        $this->addRelation('MouRelatedByMouId', 'angulex\\Model\\Mou', RelationMap::MANY_TO_ONE, array('mou_id' => 'mou_id', ), null, null);
    } // buildRelations()

} // JurusanKerjasamaTableMap
