<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.bentuk_pendidikan' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class BentukPendidikanTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.BentukPendidikanTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.bentuk_pendidikan');
        $this->setPhpName('BentukPendidikan');
        $this->setClassname('angulex\\Model\\BentukPendidikan');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('bentuk_pendidikan_id', 'BentukPendidikanId', 'SMALLINT', true, 2, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 50, null);
        $this->addColumn('jenjang_paud', 'JenjangPaud', 'NUMERIC', true, 3, null);
        $this->addColumn('jenjang_tk', 'JenjangTk', 'NUMERIC', true, 3, null);
        $this->addColumn('jenjang_sd', 'JenjangSd', 'NUMERIC', true, 3, null);
        $this->addColumn('jenjang_smp', 'JenjangSmp', 'NUMERIC', true, 3, null);
        $this->addColumn('jenjang_sma', 'JenjangSma', 'NUMERIC', true, 3, null);
        $this->addColumn('jenjang_tinggi', 'JenjangTinggi', 'NUMERIC', true, 3, null);
        $this->addColumn('direktorat_pembinaan', 'DirektoratPembinaan', 'VARCHAR', false, 40, null);
        $this->addColumn('aktif', 'Aktif', 'NUMERIC', true, 3, null);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SekolahRelatedByBentukPendidikanId', 'angulex\\Model\\Sekolah', RelationMap::ONE_TO_MANY, array('bentuk_pendidikan_id' => 'bentuk_pendidikan_id', ), null, null, 'SekolahsRelatedByBentukPendidikanId');
        $this->addRelation('SekolahRelatedByBentukPendidikanId', 'angulex\\Model\\Sekolah', RelationMap::ONE_TO_MANY, array('bentuk_pendidikan_id' => 'bentuk_pendidikan_id', ), null, null, 'SekolahsRelatedByBentukPendidikanId');
    } // buildRelations()

} // BentukPendidikanTableMap
