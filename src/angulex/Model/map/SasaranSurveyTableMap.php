<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'sasaran_survey' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class SasaranSurveyTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.SasaranSurveyTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('sasaran_survey');
        $this->setPhpName('SasaranSurvey');
        $this->setClassname('angulex\\Model\\SasaranSurvey');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addForeignPrimaryKey('pengguna_id', 'PenggunaId', 'CHAR' , 'pengguna', 'pengguna_id', true, 16, null);
        $this->addForeignPrimaryKey('pengguna_id', 'PenggunaId', 'CHAR' , 'pengguna', 'pengguna_id', true, 16, null);
        $this->addForeignPrimaryKey('sekolah_id', 'SekolahId', 'CHAR' , 'sekolah', 'sekolah_id', true, 16, null);
        $this->addForeignPrimaryKey('sekolah_id', 'SekolahId', 'CHAR' , 'sekolah', 'sekolah_id', true, 16, null);
        $this->addColumn('nomor_urut', 'NomorUrut', 'INTEGER', true, 4, null);
        $this->addColumn('tanggal_rencana', 'TanggalRencana', 'VARCHAR', false, 20, null);
        $this->addColumn('tanggal_pelaksanaan', 'TanggalPelaksanaan', 'VARCHAR', false, 20, null);
        $this->addColumn('waktu_berangkat', 'WaktuBerangkat', 'TIMESTAMP', false, 16, null);
        $this->addColumn('waktu_sampai', 'WaktuSampai', 'TIMESTAMP', false, 16, null);
        $this->addColumn('waktu_mulai_survey', 'WaktuMulaiSurvey', 'TIMESTAMP', false, 16, null);
        $this->addColumn('waktu_selesai', 'WaktuSelesai', 'TIMESTAMP', false, 16, null);
        $this->addColumn('waktu_isi_form_cetak', 'WaktuIsiFormCetak', 'INTEGER', false, 4, null);
        $this->addColumn('waktu_isi_form_elektronik', 'WaktuIsiFormElektronik', 'INTEGER', false, 4, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('PenggunaRelatedByPenggunaId', 'angulex\\Model\\Pengguna', RelationMap::MANY_TO_ONE, array('pengguna_id' => 'pengguna_id', ), null, null);
        $this->addRelation('PenggunaRelatedByPenggunaId', 'angulex\\Model\\Pengguna', RelationMap::MANY_TO_ONE, array('pengguna_id' => 'pengguna_id', ), null, null);
        $this->addRelation('SekolahRelatedBySekolahId', 'angulex\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('SekolahRelatedBySekolahId', 'angulex\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
    } // buildRelations()

} // SasaranSurveyTableMap
