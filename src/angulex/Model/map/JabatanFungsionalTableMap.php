<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.jabatan_fungsional' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class JabatanFungsionalTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.JabatanFungsionalTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.jabatan_fungsional');
        $this->setPhpName('JabatanFungsional');
        $this->setClassname('angulex\\Model\\JabatanFungsional');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('jabatan_fungsional_id', 'JabatanFungsionalId', 'NUMERIC', true, 4, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 30, null);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('RwyFungsionalRelatedByJabatanFungsionalId', 'angulex\\Model\\RwyFungsional', RelationMap::ONE_TO_MANY, array('jabatan_fungsional_id' => 'jabatan_fungsional_id', ), null, null, 'RwyFungsionalsRelatedByJabatanFungsionalId');
        $this->addRelation('RwyFungsionalRelatedByJabatanFungsionalId', 'angulex\\Model\\RwyFungsional', RelationMap::ONE_TO_MANY, array('jabatan_fungsional_id' => 'jabatan_fungsional_id', ), null, null, 'RwyFungsionalsRelatedByJabatanFungsionalId');
    } // buildRelations()

} // JabatanFungsionalTableMap
