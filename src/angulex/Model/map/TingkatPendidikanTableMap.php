<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.tingkat_pendidikan' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class TingkatPendidikanTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.TingkatPendidikanTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.tingkat_pendidikan');
        $this->setPhpName('TingkatPendidikan');
        $this->setClassname('angulex\\Model\\TingkatPendidikan');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('tingkat_pendidikan_id', 'TingkatPendidikanId', 'NUMERIC', true, 4, null);
        $this->addForeignKey('jenjang_pendidikan_id', 'JenjangPendidikanId', 'NUMERIC', 'ref.jenjang_pendidikan', 'jenjang_pendidikan_id', true, 4, null);
        $this->addColumn('kode', 'Kode', 'VARCHAR', true, 5, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 20, null);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('JenjangPendidikan', 'angulex\\Model\\JenjangPendidikan', RelationMap::MANY_TO_ONE, array('jenjang_pendidikan_id' => 'jenjang_pendidikan_id', ), null, null);
        $this->addRelation('BukuAlatRelatedByTingkatPendidikanId', 'angulex\\Model\\BukuAlat', RelationMap::ONE_TO_MANY, array('tingkat_pendidikan_id' => 'tingkat_pendidikan_id', ), null, null, 'BukuAlatsRelatedByTingkatPendidikanId');
        $this->addRelation('BukuAlatRelatedByTingkatPendidikanId', 'angulex\\Model\\BukuAlat', RelationMap::ONE_TO_MANY, array('tingkat_pendidikan_id' => 'tingkat_pendidikan_id', ), null, null, 'BukuAlatsRelatedByTingkatPendidikanId');
        $this->addRelation('MataPelajaranKurikulum', 'angulex\\Model\\MataPelajaranKurikulum', RelationMap::ONE_TO_MANY, array('tingkat_pendidikan_id' => 'tingkat_pendidikan_id', ), null, null, 'MataPelajaranKurikulums');
        $this->addRelation('GroupMatpel', 'angulex\\Model\\GroupMatpel', RelationMap::ONE_TO_MANY, array('tingkat_pendidikan_id' => 'tingkat_pendidikan_id', ), null, null, 'GroupMatpels');
        $this->addRelation('RombonganBelajarRelatedByTingkatPendidikanId', 'angulex\\Model\\RombonganBelajar', RelationMap::ONE_TO_MANY, array('tingkat_pendidikan_id' => 'tingkat_pendidikan_id', ), null, null, 'RombonganBelajarsRelatedByTingkatPendidikanId');
        $this->addRelation('RombonganBelajarRelatedByTingkatPendidikanId', 'angulex\\Model\\RombonganBelajar', RelationMap::ONE_TO_MANY, array('tingkat_pendidikan_id' => 'tingkat_pendidikan_id', ), null, null, 'RombonganBelajarsRelatedByTingkatPendidikanId');
    } // buildRelations()

} // TingkatPendidikanTableMap
