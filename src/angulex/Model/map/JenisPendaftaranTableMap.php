<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.jenis_pendaftaran' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class JenisPendaftaranTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.JenisPendaftaranTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.jenis_pendaftaran');
        $this->setPhpName('JenisPendaftaran');
        $this->setClassname('angulex\\Model\\JenisPendaftaran');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('jenis_pendaftaran_id', 'JenisPendaftaranId', 'NUMERIC', true, 3, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 20, null);
        $this->addColumn('daftar_sekolah', 'DaftarSekolah', 'NUMERIC', true, 3, null);
        $this->addColumn('daftar_rombel', 'DaftarRombel', 'NUMERIC', true, 3, null);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('AnggotaRombelRelatedByJenisPendaftaranId', 'angulex\\Model\\AnggotaRombel', RelationMap::ONE_TO_MANY, array('jenis_pendaftaran_id' => 'jenis_pendaftaran_id', ), null, null, 'AnggotaRombelsRelatedByJenisPendaftaranId');
        $this->addRelation('AnggotaRombelRelatedByJenisPendaftaranId', 'angulex\\Model\\AnggotaRombel', RelationMap::ONE_TO_MANY, array('jenis_pendaftaran_id' => 'jenis_pendaftaran_id', ), null, null, 'AnggotaRombelsRelatedByJenisPendaftaranId');
        $this->addRelation('RegistrasiPesertaDidikRelatedByJenisPendaftaranId', 'angulex\\Model\\RegistrasiPesertaDidik', RelationMap::ONE_TO_MANY, array('jenis_pendaftaran_id' => 'jenis_pendaftaran_id', ), null, null, 'RegistrasiPesertaDidiksRelatedByJenisPendaftaranId');
        $this->addRelation('RegistrasiPesertaDidikRelatedByJenisPendaftaranId', 'angulex\\Model\\RegistrasiPesertaDidik', RelationMap::ONE_TO_MANY, array('jenis_pendaftaran_id' => 'jenis_pendaftaran_id', ), null, null, 'RegistrasiPesertaDidiksRelatedByJenisPendaftaranId');
        $this->addRelation('PesertaDidikBaruRelatedByJenisPendaftaranId', 'angulex\\Model\\PesertaDidikBaru', RelationMap::ONE_TO_MANY, array('jenis_pendaftaran_id' => 'jenis_pendaftaran_id', ), null, null, 'PesertaDidikBarusRelatedByJenisPendaftaranId');
        $this->addRelation('PesertaDidikBaruRelatedByJenisPendaftaranId', 'angulex\\Model\\PesertaDidikBaru', RelationMap::ONE_TO_MANY, array('jenis_pendaftaran_id' => 'jenis_pendaftaran_id', ), null, null, 'PesertaDidikBarusRelatedByJenisPendaftaranId');
    } // buildRelations()

} // JenisPendaftaranTableMap
