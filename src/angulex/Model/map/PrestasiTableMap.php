<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'prestasi' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class PrestasiTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.PrestasiTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('prestasi');
        $this->setPhpName('Prestasi');
        $this->setClassname('angulex\\Model\\Prestasi');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('prestasi_id', 'PrestasiId', 'CHAR', true, 16, null);
        $this->addForeignKey('jenis_prestasi_id', 'JenisPrestasiId', 'INTEGER', 'ref.jenis_prestasi', 'jenis_prestasi_id', true, 4, null);
        $this->addForeignKey('jenis_prestasi_id', 'JenisPrestasiId', 'INTEGER', 'ref.jenis_prestasi', 'jenis_prestasi_id', true, 4, null);
        $this->addForeignKey('tingkat_prestasi_id', 'TingkatPrestasiId', 'INTEGER', 'ref.tingkat_prestasi', 'tingkat_prestasi_id', true, 4, null);
        $this->addForeignKey('tingkat_prestasi_id', 'TingkatPrestasiId', 'INTEGER', 'ref.tingkat_prestasi', 'tingkat_prestasi_id', true, 4, null);
        $this->addForeignKey('peserta_didik_id', 'PesertaDidikId', 'CHAR', 'peserta_didik', 'peserta_didik_id', true, 16, null);
        $this->addForeignKey('peserta_didik_id', 'PesertaDidikId', 'CHAR', 'peserta_didik', 'peserta_didik_id', true, 16, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 30, null);
        $this->addColumn('tahun_prestasi', 'TahunPrestasi', 'NUMERIC', true, 6, null);
        $this->addColumn('penyelenggara', 'Penyelenggara', 'VARCHAR', false, 80, null);
        $this->addColumn('peringkat', 'Peringkat', 'INTEGER', false, 4, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('PesertaDidikRelatedByPesertaDidikId', 'angulex\\Model\\PesertaDidik', RelationMap::MANY_TO_ONE, array('peserta_didik_id' => 'peserta_didik_id', ), null, null);
        $this->addRelation('PesertaDidikRelatedByPesertaDidikId', 'angulex\\Model\\PesertaDidik', RelationMap::MANY_TO_ONE, array('peserta_didik_id' => 'peserta_didik_id', ), null, null);
        $this->addRelation('JenisPrestasiRelatedByJenisPrestasiId', 'angulex\\Model\\JenisPrestasi', RelationMap::MANY_TO_ONE, array('jenis_prestasi_id' => 'jenis_prestasi_id', ), null, null);
        $this->addRelation('JenisPrestasiRelatedByJenisPrestasiId', 'angulex\\Model\\JenisPrestasi', RelationMap::MANY_TO_ONE, array('jenis_prestasi_id' => 'jenis_prestasi_id', ), null, null);
        $this->addRelation('TingkatPrestasiRelatedByTingkatPrestasiId', 'angulex\\Model\\TingkatPrestasi', RelationMap::MANY_TO_ONE, array('tingkat_prestasi_id' => 'tingkat_prestasi_id', ), null, null);
        $this->addRelation('TingkatPrestasiRelatedByTingkatPrestasiId', 'angulex\\Model\\TingkatPrestasi', RelationMap::MANY_TO_ONE, array('tingkat_prestasi_id' => 'tingkat_prestasi_id', ), null, null);
        $this->addRelation('VldPrestasiRelatedByPrestasiId', 'angulex\\Model\\VldPrestasi', RelationMap::ONE_TO_MANY, array('prestasi_id' => 'prestasi_id', ), null, null, 'VldPrestasisRelatedByPrestasiId');
        $this->addRelation('VldPrestasiRelatedByPrestasiId', 'angulex\\Model\\VldPrestasi', RelationMap::ONE_TO_MANY, array('prestasi_id' => 'prestasi_id', ), null, null, 'VldPrestasisRelatedByPrestasiId');
    } // buildRelations()

} // PrestasiTableMap
