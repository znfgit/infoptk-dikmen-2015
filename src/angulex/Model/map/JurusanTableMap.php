<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.jurusan' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class JurusanTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.JurusanTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.jurusan');
        $this->setPhpName('Jurusan');
        $this->setClassname('angulex\\Model\\Jurusan');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('jurusan_id', 'JurusanId', 'VARCHAR', true, 25, null);
        $this->addColumn('nama_jurusan', 'NamaJurusan', 'VARCHAR', true, 60, null);
        $this->addColumn('untuk_sma', 'UntukSma', 'NUMERIC', true, 3, null);
        $this->addColumn('untuk_smk', 'UntukSmk', 'NUMERIC', true, 3, null);
        $this->addColumn('untuk_pt', 'UntukPt', 'NUMERIC', true, 3, null);
        $this->addColumn('untuk_slb', 'UntukSlb', 'NUMERIC', true, 3, 0);
        $this->addColumn('untuk_smklb', 'UntukSmklb', 'NUMERIC', true, 3, 0);
        $this->addForeignKey('jurusan_induk', 'JurusanInduk', 'VARCHAR', 'ref.jurusan', 'jurusan_id', false, 25, null);
        $this->addForeignKey('level_bidang_id', 'LevelBidangId', 'VARCHAR', 'ref.kelompok_bidang', 'level_bidang_id', true, 5, null);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('JurusanRelatedByJurusanInduk', 'angulex\\Model\\Jurusan', RelationMap::MANY_TO_ONE, array('jurusan_induk' => 'jurusan_id', ), null, null);
        $this->addRelation('KelompokBidang', 'angulex\\Model\\KelompokBidang', RelationMap::MANY_TO_ONE, array('level_bidang_id' => 'level_bidang_id', ), null, null);
        $this->addRelation('JurusanSpRelatedByJurusanId', 'angulex\\Model\\JurusanSp', RelationMap::ONE_TO_MANY, array('jurusan_id' => 'jurusan_id', ), null, null, 'JurusanSpsRelatedByJurusanId');
        $this->addRelation('JurusanSpRelatedByJurusanId', 'angulex\\Model\\JurusanSp', RelationMap::ONE_TO_MANY, array('jurusan_id' => 'jurusan_id', ), null, null, 'JurusanSpsRelatedByJurusanId');
        $this->addRelation('TemplateUn', 'angulex\\Model\\TemplateUn', RelationMap::ONE_TO_MANY, array('jurusan_id' => 'jurusan_id', ), null, null, 'TemplateUns');
        $this->addRelation('JurusanRelatedByJurusanId', 'angulex\\Model\\Jurusan', RelationMap::ONE_TO_MANY, array('jurusan_id' => 'jurusan_induk', ), null, null, 'JurusansRelatedByJurusanId');
        $this->addRelation('Kurikulum', 'angulex\\Model\\Kurikulum', RelationMap::ONE_TO_MANY, array('jurusan_id' => 'jurusan_id', ), null, null, 'Kurikulums');
    } // buildRelations()

} // JurusanTableMap
