<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'sysdiagrams' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class SysdiagramsTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.SysdiagramsTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('sysdiagrams');
        $this->setPhpName('Sysdiagrams');
        $this->setClassname('angulex\\Model\\Sysdiagrams');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addColumn('name', 'Name', 'VARCHAR', true, 256, null);
        $this->addColumn('principal_id', 'PrincipalId', 'INTEGER', true, 4, null);
        $this->addPrimaryKey('diagram_id', 'DiagramId', 'INTEGER', true, 4, null);
        $this->addColumn('version', 'Version', 'INTEGER', false, 4, null);
        $this->addColumn('definition', 'Definition', 'LONGVARBINARY', false, 2147483647, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // SysdiagramsTableMap
