<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'beasiswa_peserta_didik' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class BeasiswaPesertaDidikTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.BeasiswaPesertaDidikTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('beasiswa_peserta_didik');
        $this->setPhpName('BeasiswaPesertaDidik');
        $this->setClassname('angulex\\Model\\BeasiswaPesertaDidik');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('beasiswa_peserta_didik_id', 'BeasiswaPesertaDidikId', 'CHAR', true, 16, null);
        $this->addForeignKey('peserta_didik_id', 'PesertaDidikId', 'CHAR', 'peserta_didik', 'peserta_didik_id', true, 16, null);
        $this->addForeignKey('peserta_didik_id', 'PesertaDidikId', 'CHAR', 'peserta_didik', 'peserta_didik_id', true, 16, null);
        $this->addForeignKey('jenis_beasiswa_id', 'JenisBeasiswaId', 'INTEGER', 'ref.jenis_beasiswa', 'jenis_beasiswa_id', true, 4, null);
        $this->addForeignKey('jenis_beasiswa_id', 'JenisBeasiswaId', 'INTEGER', 'ref.jenis_beasiswa', 'jenis_beasiswa_id', true, 4, null);
        $this->addColumn('keterangan', 'Keterangan', 'VARCHAR', true, 80, null);
        $this->addForeignKey('tahun_mulai', 'TahunMulai', 'NUMERIC', 'ref.tahun_ajaran', 'tahun_ajaran_id', true, 6, null);
        $this->addForeignKey('tahun_mulai', 'TahunMulai', 'NUMERIC', 'ref.tahun_ajaran', 'tahun_ajaran_id', true, 6, null);
        $this->addForeignKey('tahun_selesai', 'TahunSelesai', 'NUMERIC', 'ref.tahun_ajaran', 'tahun_ajaran_id', true, 6, null);
        $this->addForeignKey('tahun_selesai', 'TahunSelesai', 'NUMERIC', 'ref.tahun_ajaran', 'tahun_ajaran_id', true, 6, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('PesertaDidikRelatedByPesertaDidikId', 'angulex\\Model\\PesertaDidik', RelationMap::MANY_TO_ONE, array('peserta_didik_id' => 'peserta_didik_id', ), null, null);
        $this->addRelation('PesertaDidikRelatedByPesertaDidikId', 'angulex\\Model\\PesertaDidik', RelationMap::MANY_TO_ONE, array('peserta_didik_id' => 'peserta_didik_id', ), null, null);
        $this->addRelation('JenisBeasiswaRelatedByJenisBeasiswaId', 'angulex\\Model\\JenisBeasiswa', RelationMap::MANY_TO_ONE, array('jenis_beasiswa_id' => 'jenis_beasiswa_id', ), null, null);
        $this->addRelation('JenisBeasiswaRelatedByJenisBeasiswaId', 'angulex\\Model\\JenisBeasiswa', RelationMap::MANY_TO_ONE, array('jenis_beasiswa_id' => 'jenis_beasiswa_id', ), null, null);
        $this->addRelation('TahunAjaranRelatedByTahunSelesai', 'angulex\\Model\\TahunAjaran', RelationMap::MANY_TO_ONE, array('tahun_selesai' => 'tahun_ajaran_id', ), null, null);
        $this->addRelation('TahunAjaranRelatedByTahunMulai', 'angulex\\Model\\TahunAjaran', RelationMap::MANY_TO_ONE, array('tahun_mulai' => 'tahun_ajaran_id', ), null, null);
        $this->addRelation('TahunAjaranRelatedByTahunSelesai', 'angulex\\Model\\TahunAjaran', RelationMap::MANY_TO_ONE, array('tahun_selesai' => 'tahun_ajaran_id', ), null, null);
        $this->addRelation('TahunAjaranRelatedByTahunMulai', 'angulex\\Model\\TahunAjaran', RelationMap::MANY_TO_ONE, array('tahun_mulai' => 'tahun_ajaran_id', ), null, null);
        $this->addRelation('VldBeaPdRelatedByBeasiswaPesertaDidikId', 'angulex\\Model\\VldBeaPd', RelationMap::ONE_TO_MANY, array('beasiswa_peserta_didik_id' => 'beasiswa_peserta_didik_id', ), null, null, 'VldBeaPdsRelatedByBeasiswaPesertaDidikId');
        $this->addRelation('VldBeaPdRelatedByBeasiswaPesertaDidikId', 'angulex\\Model\\VldBeaPd', RelationMap::ONE_TO_MANY, array('beasiswa_peserta_didik_id' => 'beasiswa_peserta_didik_id', ), null, null, 'VldBeaPdsRelatedByBeasiswaPesertaDidikId');
    } // buildRelations()

} // BeasiswaPesertaDidikTableMap
