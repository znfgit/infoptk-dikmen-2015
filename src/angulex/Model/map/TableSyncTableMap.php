<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.table_sync' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class TableSyncTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.TableSyncTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.table_sync');
        $this->setPhpName('TableSync');
        $this->setClassname('angulex\\Model\\TableSync');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('table_name', 'TableName', 'VARCHAR', true, 30, null);
        $this->addColumn('sync_type', 'SyncType', 'CHAR', true, 1, null);
        $this->addColumn('sync_seq', 'SyncSeq', 'NUMERIC', true, 6, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('WtSrcSyncLogRelatedByTableName', 'angulex\\Model\\WtSrcSyncLog', RelationMap::ONE_TO_MANY, array('table_name' => 'table_name', ), null, null, 'WtSrcSyncLogsRelatedByTableName');
        $this->addRelation('WtSrcSyncLogRelatedByTableName', 'angulex\\Model\\WtSrcSyncLog', RelationMap::ONE_TO_MANY, array('table_name' => 'table_name', ), null, null, 'WtSrcSyncLogsRelatedByTableName');
        $this->addRelation('TableSyncLogRelatedByTableName', 'angulex\\Model\\TableSyncLog', RelationMap::ONE_TO_MANY, array('table_name' => 'table_name', ), null, null, 'TableSyncLogsRelatedByTableName');
        $this->addRelation('TableSyncLogRelatedByTableName', 'angulex\\Model\\TableSyncLog', RelationMap::ONE_TO_MANY, array('table_name' => 'table_name', ), null, null, 'TableSyncLogsRelatedByTableName');
        $this->addRelation('WtDstSyncLogRelatedByTableName', 'angulex\\Model\\WtDstSyncLog', RelationMap::ONE_TO_MANY, array('table_name' => 'table_name', ), null, null, 'WtDstSyncLogsRelatedByTableName');
        $this->addRelation('WtDstSyncLogRelatedByTableName', 'angulex\\Model\\WtDstSyncLog', RelationMap::ONE_TO_MANY, array('table_name' => 'table_name', ), null, null, 'WtDstSyncLogsRelatedByTableName');
    } // buildRelations()

} // TableSyncTableMap
