<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'nilai_test' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class NilaiTestTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.NilaiTestTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('nilai_test');
        $this->setPhpName('NilaiTest');
        $this->setClassname('angulex\\Model\\NilaiTest');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('nilai_test_id', 'NilaiTestId', 'CHAR', true, 16, null);
        $this->addForeignKey('jenis_test_id', 'JenisTestId', 'NUMERIC', 'ref.jenis_test', 'jenis_test_id', true, 5, null);
        $this->addForeignKey('jenis_test_id', 'JenisTestId', 'NUMERIC', 'ref.jenis_test', 'jenis_test_id', true, 5, null);
        $this->addForeignKey('ptk_id', 'PtkId', 'CHAR', 'ptk', 'ptk_id', true, 16, null);
        $this->addForeignKey('ptk_id', 'PtkId', 'CHAR', 'ptk', 'ptk_id', true, 16, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 50, null);
        $this->addColumn('penyelenggara', 'Penyelenggara', 'VARCHAR', true, 80, null);
        $this->addColumn('tahun', 'Tahun', 'NUMERIC', true, 6, null);
        $this->addColumn('skor', 'Skor', 'NUMERIC', true, 8, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('PtkRelatedByPtkId', 'angulex\\Model\\Ptk', RelationMap::MANY_TO_ONE, array('ptk_id' => 'ptk_id', ), null, null);
        $this->addRelation('PtkRelatedByPtkId', 'angulex\\Model\\Ptk', RelationMap::MANY_TO_ONE, array('ptk_id' => 'ptk_id', ), null, null);
        $this->addRelation('JenisTestRelatedByJenisTestId', 'angulex\\Model\\JenisTest', RelationMap::MANY_TO_ONE, array('jenis_test_id' => 'jenis_test_id', ), null, null);
        $this->addRelation('JenisTestRelatedByJenisTestId', 'angulex\\Model\\JenisTest', RelationMap::MANY_TO_ONE, array('jenis_test_id' => 'jenis_test_id', ), null, null);
        $this->addRelation('VldNilaiTestRelatedByNilaiTestId', 'angulex\\Model\\VldNilaiTest', RelationMap::ONE_TO_MANY, array('nilai_test_id' => 'nilai_test_id', ), null, null, 'VldNilaiTestsRelatedByNilaiTestId');
        $this->addRelation('VldNilaiTestRelatedByNilaiTestId', 'angulex\\Model\\VldNilaiTest', RelationMap::ONE_TO_MANY, array('nilai_test_id' => 'nilai_test_id', ), null, null, 'VldNilaiTestsRelatedByNilaiTestId');
    } // buildRelations()

} // NilaiTestTableMap
