<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.jenis_sarana' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class JenisSaranaTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.JenisSaranaTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.jenis_sarana');
        $this->setPhpName('JenisSarana');
        $this->setClassname('angulex\\Model\\JenisSarana');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('jenis_sarana_id', 'JenisSaranaId', 'INTEGER', true, 4, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 30, null);
        $this->addColumn('kelompok', 'Kelompok', 'VARCHAR', false, 50, null);
        $this->addColumn('perlu_penempatan', 'PerluPenempatan', 'NUMERIC', true, 3, null);
        $this->addColumn('keterangan', 'Keterangan', 'VARCHAR', false, 128, null);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SaranaRelatedByJenisSaranaId', 'angulex\\Model\\Sarana', RelationMap::ONE_TO_MANY, array('jenis_sarana_id' => 'jenis_sarana_id', ), null, null, 'SaranasRelatedByJenisSaranaId');
        $this->addRelation('SaranaRelatedByJenisSaranaId', 'angulex\\Model\\Sarana', RelationMap::ONE_TO_MANY, array('jenis_sarana_id' => 'jenis_sarana_id', ), null, null, 'SaranasRelatedByJenisSaranaId');
    } // buildRelations()

} // JenisSaranaTableMap
