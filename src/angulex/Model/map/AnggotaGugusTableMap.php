<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'anggota_gugus' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class AnggotaGugusTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.AnggotaGugusTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('anggota_gugus');
        $this->setPhpName('AnggotaGugus');
        $this->setClassname('angulex\\Model\\AnggotaGugus');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addForeignPrimaryKey('gugus_id', 'GugusId', 'CHAR' , 'gugus_sekolah', 'gugus_id', true, 16, null);
        $this->addForeignPrimaryKey('gugus_id', 'GugusId', 'CHAR' , 'gugus_sekolah', 'gugus_id', true, 16, null);
        $this->addForeignPrimaryKey('sekolah_id', 'SekolahId', 'CHAR' , 'sekolah', 'sekolah_id', true, 16, null);
        $this->addForeignPrimaryKey('sekolah_id', 'SekolahId', 'CHAR' , 'sekolah', 'sekolah_id', true, 16, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('GugusSekolahRelatedByGugusId', 'angulex\\Model\\GugusSekolah', RelationMap::MANY_TO_ONE, array('gugus_id' => 'gugus_id', ), null, null);
        $this->addRelation('GugusSekolahRelatedByGugusId', 'angulex\\Model\\GugusSekolah', RelationMap::MANY_TO_ONE, array('gugus_id' => 'gugus_id', ), null, null);
        $this->addRelation('SekolahRelatedBySekolahId', 'angulex\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('SekolahRelatedBySekolahId', 'angulex\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
    } // buildRelations()

} // AnggotaGugusTableMap
