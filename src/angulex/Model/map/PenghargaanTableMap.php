<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'penghargaan' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class PenghargaanTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.PenghargaanTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('penghargaan');
        $this->setPhpName('Penghargaan');
        $this->setClassname('angulex\\Model\\Penghargaan');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('penghargaan_id', 'PenghargaanId', 'CHAR', true, 16, null);
        $this->addForeignKey('tingkat_penghargaan_id', 'TingkatPenghargaanId', 'INTEGER', 'ref.tingkat_penghargaan', 'tingkat_penghargaan_id', true, 4, null);
        $this->addForeignKey('tingkat_penghargaan_id', 'TingkatPenghargaanId', 'INTEGER', 'ref.tingkat_penghargaan', 'tingkat_penghargaan_id', true, 4, null);
        $this->addForeignKey('ptk_id', 'PtkId', 'CHAR', 'ptk', 'ptk_id', true, 16, null);
        $this->addForeignKey('ptk_id', 'PtkId', 'CHAR', 'ptk', 'ptk_id', true, 16, null);
        $this->addForeignKey('jenis_penghargaan_id', 'JenisPenghargaanId', 'INTEGER', 'ref.jenis_penghargaan', 'jenis_penghargaan_id', true, 4, null);
        $this->addForeignKey('jenis_penghargaan_id', 'JenisPenghargaanId', 'INTEGER', 'ref.jenis_penghargaan', 'jenis_penghargaan_id', true, 4, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 50, null);
        $this->addColumn('tahun', 'Tahun', 'NUMERIC', true, 6, null);
        $this->addColumn('instansi', 'Instansi', 'VARCHAR', false, 80, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('PtkRelatedByPtkId', 'angulex\\Model\\Ptk', RelationMap::MANY_TO_ONE, array('ptk_id' => 'ptk_id', ), null, null);
        $this->addRelation('PtkRelatedByPtkId', 'angulex\\Model\\Ptk', RelationMap::MANY_TO_ONE, array('ptk_id' => 'ptk_id', ), null, null);
        $this->addRelation('JenisPenghargaanRelatedByJenisPenghargaanId', 'angulex\\Model\\JenisPenghargaan', RelationMap::MANY_TO_ONE, array('jenis_penghargaan_id' => 'jenis_penghargaan_id', ), null, null);
        $this->addRelation('JenisPenghargaanRelatedByJenisPenghargaanId', 'angulex\\Model\\JenisPenghargaan', RelationMap::MANY_TO_ONE, array('jenis_penghargaan_id' => 'jenis_penghargaan_id', ), null, null);
        $this->addRelation('TingkatPenghargaanRelatedByTingkatPenghargaanId', 'angulex\\Model\\TingkatPenghargaan', RelationMap::MANY_TO_ONE, array('tingkat_penghargaan_id' => 'tingkat_penghargaan_id', ), null, null);
        $this->addRelation('TingkatPenghargaanRelatedByTingkatPenghargaanId', 'angulex\\Model\\TingkatPenghargaan', RelationMap::MANY_TO_ONE, array('tingkat_penghargaan_id' => 'tingkat_penghargaan_id', ), null, null);
        $this->addRelation('VldPenghargaanRelatedByPenghargaanId', 'angulex\\Model\\VldPenghargaan', RelationMap::ONE_TO_MANY, array('penghargaan_id' => 'penghargaan_id', ), null, null, 'VldPenghargaansRelatedByPenghargaanId');
        $this->addRelation('VldPenghargaanRelatedByPenghargaanId', 'angulex\\Model\\VldPenghargaan', RelationMap::ONE_TO_MANY, array('penghargaan_id' => 'penghargaan_id', ), null, null, 'VldPenghargaansRelatedByPenghargaanId');
    } // buildRelations()

} // PenghargaanTableMap
