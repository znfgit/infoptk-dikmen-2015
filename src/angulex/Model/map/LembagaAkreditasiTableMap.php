<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.lembaga_akreditasi' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class LembagaAkreditasiTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.LembagaAkreditasiTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.lembaga_akreditasi');
        $this->setPhpName('LembagaAkreditasi');
        $this->setClassname('angulex\\Model\\LembagaAkreditasi');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('la_id', 'LaId', 'CHAR', true, 5, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 80, null);
        $this->addColumn('la_tgl_mulai', 'LaTglMulai', 'VARCHAR', true, 20, null);
        $this->addColumn('la_ket', 'LaKet', 'VARCHAR', false, 250, null);
        $this->addColumn('alamat_jalan', 'AlamatJalan', 'VARCHAR', true, 80, null);
        $this->addColumn('rt', 'Rt', 'NUMERIC', false, 4, null);
        $this->addColumn('rw', 'Rw', 'NUMERIC', false, 4, null);
        $this->addColumn('nama_dusun', 'NamaDusun', 'VARCHAR', false, 40, null);
        $this->addColumn('desa_kelurahan', 'DesaKelurahan', 'VARCHAR', true, 40, null);
        $this->addColumn('kode_wilayah', 'KodeWilayah', 'CHAR', true, 8, null);
        $this->addColumn('kode_pos', 'KodePos', 'CHAR', false, 5, null);
        $this->addColumn('lintang', 'Lintang', 'NUMERIC', false, 12, null);
        $this->addColumn('bujur', 'Bujur', 'NUMERIC', false, 12, null);
        $this->addColumn('nomor_telepon', 'NomorTelepon', 'VARCHAR', false, 20, null);
        $this->addColumn('nomor_fax', 'NomorFax', 'VARCHAR', false, 20, null);
        $this->addColumn('email', 'Email', 'VARCHAR', false, 50, null);
        $this->addColumn('website', 'Website', 'VARCHAR', false, 100, null);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('AkreditasiSpRelatedByLaId', 'angulex\\Model\\AkreditasiSp', RelationMap::ONE_TO_MANY, array('la_id' => 'la_id', ), null, null, 'AkreditasiSpsRelatedByLaId');
        $this->addRelation('AkreditasiSpRelatedByLaId', 'angulex\\Model\\AkreditasiSp', RelationMap::ONE_TO_MANY, array('la_id' => 'la_id', ), null, null, 'AkreditasiSpsRelatedByLaId');
        $this->addRelation('AkreditasiProdiRelatedByLaId', 'angulex\\Model\\AkreditasiProdi', RelationMap::ONE_TO_MANY, array('la_id' => 'la_id', ), null, null, 'AkreditasiProdisRelatedByLaId');
        $this->addRelation('AkreditasiProdiRelatedByLaId', 'angulex\\Model\\AkreditasiProdi', RelationMap::ONE_TO_MANY, array('la_id' => 'la_id', ), null, null, 'AkreditasiProdisRelatedByLaId');
    } // buildRelations()

} // LembagaAkreditasiTableMap
