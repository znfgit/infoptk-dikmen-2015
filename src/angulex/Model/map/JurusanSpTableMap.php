<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'jurusan_sp' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class JurusanSpTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.JurusanSpTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('jurusan_sp');
        $this->setPhpName('JurusanSp');
        $this->setClassname('angulex\\Model\\JurusanSp');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('jurusan_sp_id', 'JurusanSpId', 'CHAR', true, 16, null);
        $this->addForeignKey('sekolah_id', 'SekolahId', 'CHAR', 'sekolah', 'sekolah_id', true, 16, null);
        $this->addForeignKey('sekolah_id', 'SekolahId', 'CHAR', 'sekolah', 'sekolah_id', true, 16, null);
        $this->addForeignKey('kebutuhan_khusus_id', 'KebutuhanKhususId', 'INTEGER', 'ref.kebutuhan_khusus', 'kebutuhan_khusus_id', true, 4, null);
        $this->addForeignKey('kebutuhan_khusus_id', 'KebutuhanKhususId', 'INTEGER', 'ref.kebutuhan_khusus', 'kebutuhan_khusus_id', true, 4, null);
        $this->addForeignKey('jurusan_id', 'JurusanId', 'VARCHAR', 'ref.jurusan', 'jurusan_id', true, 25, null);
        $this->addForeignKey('jurusan_id', 'JurusanId', 'VARCHAR', 'ref.jurusan', 'jurusan_id', true, 25, null);
        $this->addColumn('nama_jurusan_sp', 'NamaJurusanSp', 'VARCHAR', true, 60, null);
        $this->addColumn('sk_izin', 'SkIzin', 'VARCHAR', false, 40, null);
        $this->addColumn('tanggal_sk_izin', 'TanggalSkIzin', 'VARCHAR', false, 20, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SekolahRelatedBySekolahId', 'angulex\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('SekolahRelatedBySekolahId', 'angulex\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('JurusanRelatedByJurusanId', 'angulex\\Model\\Jurusan', RelationMap::MANY_TO_ONE, array('jurusan_id' => 'jurusan_id', ), null, null);
        $this->addRelation('JurusanRelatedByJurusanId', 'angulex\\Model\\Jurusan', RelationMap::MANY_TO_ONE, array('jurusan_id' => 'jurusan_id', ), null, null);
        $this->addRelation('KebutuhanKhususRelatedByKebutuhanKhususId', 'angulex\\Model\\KebutuhanKhusus', RelationMap::MANY_TO_ONE, array('kebutuhan_khusus_id' => 'kebutuhan_khusus_id', ), null, null);
        $this->addRelation('KebutuhanKhususRelatedByKebutuhanKhususId', 'angulex\\Model\\KebutuhanKhusus', RelationMap::MANY_TO_ONE, array('kebutuhan_khusus_id' => 'kebutuhan_khusus_id', ), null, null);
        $this->addRelation('RegistrasiPesertaDidikRelatedByJurusanSpId', 'angulex\\Model\\RegistrasiPesertaDidik', RelationMap::ONE_TO_MANY, array('jurusan_sp_id' => 'jurusan_sp_id', ), null, null, 'RegistrasiPesertaDidiksRelatedByJurusanSpId');
        $this->addRelation('RegistrasiPesertaDidikRelatedByJurusanSpId', 'angulex\\Model\\RegistrasiPesertaDidik', RelationMap::ONE_TO_MANY, array('jurusan_sp_id' => 'jurusan_sp_id', ), null, null, 'RegistrasiPesertaDidiksRelatedByJurusanSpId');
        $this->addRelation('VldJurusanSpRelatedByJurusanSpId', 'angulex\\Model\\VldJurusanSp', RelationMap::ONE_TO_MANY, array('jurusan_sp_id' => 'jurusan_sp_id', ), null, null, 'VldJurusanSpsRelatedByJurusanSpId');
        $this->addRelation('VldJurusanSpRelatedByJurusanSpId', 'angulex\\Model\\VldJurusanSp', RelationMap::ONE_TO_MANY, array('jurusan_sp_id' => 'jurusan_sp_id', ), null, null, 'VldJurusanSpsRelatedByJurusanSpId');
        $this->addRelation('JurusanKerjasamaRelatedByJurusanSpId', 'angulex\\Model\\JurusanKerjasama', RelationMap::ONE_TO_MANY, array('jurusan_sp_id' => 'jurusan_sp_id', ), null, null, 'JurusanKerjasamasRelatedByJurusanSpId');
        $this->addRelation('JurusanKerjasamaRelatedByJurusanSpId', 'angulex\\Model\\JurusanKerjasama', RelationMap::ONE_TO_MANY, array('jurusan_sp_id' => 'jurusan_sp_id', ), null, null, 'JurusanKerjasamasRelatedByJurusanSpId');
        $this->addRelation('RombonganBelajarRelatedByJurusanSpId', 'angulex\\Model\\RombonganBelajar', RelationMap::ONE_TO_MANY, array('jurusan_sp_id' => 'jurusan_sp_id', ), null, null, 'RombonganBelajarsRelatedByJurusanSpId');
        $this->addRelation('RombonganBelajarRelatedByJurusanSpId', 'angulex\\Model\\RombonganBelajar', RelationMap::ONE_TO_MANY, array('jurusan_sp_id' => 'jurusan_sp_id', ), null, null, 'RombonganBelajarsRelatedByJurusanSpId');
        $this->addRelation('AkreditasiProdiRelatedByJurusanSpId', 'angulex\\Model\\AkreditasiProdi', RelationMap::ONE_TO_MANY, array('jurusan_sp_id' => 'jurusan_sp_id', ), null, null, 'AkreditasiProdisRelatedByJurusanSpId');
        $this->addRelation('AkreditasiProdiRelatedByJurusanSpId', 'angulex\\Model\\AkreditasiProdi', RelationMap::ONE_TO_MANY, array('jurusan_sp_id' => 'jurusan_sp_id', ), null, null, 'AkreditasiProdisRelatedByJurusanSpId');
    } // buildRelations()

} // JurusanSpTableMap
