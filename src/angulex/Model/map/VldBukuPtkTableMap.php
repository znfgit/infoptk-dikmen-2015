<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'vld_buku_ptk' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class VldBukuPtkTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.VldBukuPtkTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('vld_buku_ptk');
        $this->setPhpName('VldBukuPtk');
        $this->setClassname('angulex\\Model\\VldBukuPtk');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addForeignPrimaryKey('buku_id', 'BukuId', 'CHAR' , 'buku_ptk', 'buku_id', true, 16, null);
        $this->addForeignPrimaryKey('buku_id', 'BukuId', 'CHAR' , 'buku_ptk', 'buku_id', true, 16, null);
        $this->addPrimaryKey('logid', 'Logid', 'CHAR', true, 16, null);
        $this->addForeignKey('idtype', 'Idtype', 'INTEGER', 'ref.errortype', 'idtype', true, 4, null);
        $this->addForeignKey('idtype', 'Idtype', 'INTEGER', 'ref.errortype', 'idtype', true, 4, null);
        $this->addColumn('status_validasi', 'StatusValidasi', 'NUMERIC', false, 4, null);
        $this->addColumn('status_verifikasi', 'StatusVerifikasi', 'NUMERIC', false, 4, null);
        $this->addColumn('field_error', 'FieldError', 'VARCHAR', false, 30, null);
        $this->addColumn('error_message', 'ErrorMessage', 'VARCHAR', false, 150, null);
        $this->addColumn('keterangan', 'Keterangan', 'VARCHAR', false, 255, null);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('app_username', 'AppUsername', 'VARCHAR', false, 50, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('BukuPtkRelatedByBukuId', 'angulex\\Model\\BukuPtk', RelationMap::MANY_TO_ONE, array('buku_id' => 'buku_id', ), null, null);
        $this->addRelation('BukuPtkRelatedByBukuId', 'angulex\\Model\\BukuPtk', RelationMap::MANY_TO_ONE, array('buku_id' => 'buku_id', ), null, null);
        $this->addRelation('ErrortypeRelatedByIdtype', 'angulex\\Model\\Errortype', RelationMap::MANY_TO_ONE, array('idtype' => 'idtype', ), null, null);
        $this->addRelation('ErrortypeRelatedByIdtype', 'angulex\\Model\\Errortype', RelationMap::MANY_TO_ONE, array('idtype' => 'idtype', ), null, null);
    } // buildRelations()

} // VldBukuPtkTableMap
