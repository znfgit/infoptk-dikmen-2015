<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'gugus_sekolah' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class GugusSekolahTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.GugusSekolahTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('gugus_sekolah');
        $this->setPhpName('GugusSekolah');
        $this->setClassname('angulex\\Model\\GugusSekolah');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('gugus_id', 'GugusId', 'CHAR', true, 16, null);
        $this->addForeignKey('sekolah_id', 'SekolahId', 'CHAR', 'sekolah', 'sekolah_id', false, 16, null);
        $this->addForeignKey('sekolah_id', 'SekolahId', 'CHAR', 'sekolah', 'sekolah_id', false, 16, null);
        $this->addForeignKey('jenis_gugus_id', 'JenisGugusId', 'NUMERIC', 'ref.jenis_gugus', 'jenis_gugus_id', true, 5, null);
        $this->addForeignKey('jenis_gugus_id', 'JenisGugusId', 'NUMERIC', 'ref.jenis_gugus', 'jenis_gugus_id', true, 5, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 50, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SekolahRelatedBySekolahId', 'angulex\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('SekolahRelatedBySekolahId', 'angulex\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('JenisGugusRelatedByJenisGugusId', 'angulex\\Model\\JenisGugus', RelationMap::MANY_TO_ONE, array('jenis_gugus_id' => 'jenis_gugus_id', ), null, null);
        $this->addRelation('JenisGugusRelatedByJenisGugusId', 'angulex\\Model\\JenisGugus', RelationMap::MANY_TO_ONE, array('jenis_gugus_id' => 'jenis_gugus_id', ), null, null);
        $this->addRelation('AnggotaGugusRelatedByGugusId', 'angulex\\Model\\AnggotaGugus', RelationMap::ONE_TO_MANY, array('gugus_id' => 'gugus_id', ), null, null, 'AnggotaGugussRelatedByGugusId');
        $this->addRelation('AnggotaGugusRelatedByGugusId', 'angulex\\Model\\AnggotaGugus', RelationMap::ONE_TO_MANY, array('gugus_id' => 'gugus_id', ), null, null, 'AnggotaGugussRelatedByGugusId');
    } // buildRelations()

} // GugusSekolahTableMap
