<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'buku_alat_longitudinal' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class BukuAlatLongitudinalTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.BukuAlatLongitudinalTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('buku_alat_longitudinal');
        $this->setPhpName('BukuAlatLongitudinal');
        $this->setClassname('angulex\\Model\\BukuAlatLongitudinal');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addForeignPrimaryKey('buku_alat_id', 'BukuAlatId', 'CHAR' , 'buku_alat', 'buku_alat_id', true, 16, null);
        $this->addForeignPrimaryKey('buku_alat_id', 'BukuAlatId', 'CHAR' , 'buku_alat', 'buku_alat_id', true, 16, null);
        $this->addForeignPrimaryKey('semester_id', 'SemesterId', 'CHAR' , 'ref.semester', 'semester_id', true, 5, null);
        $this->addForeignPrimaryKey('semester_id', 'SemesterId', 'CHAR' , 'ref.semester', 'semester_id', true, 5, null);
        $this->addColumn('jumlah', 'Jumlah', 'INTEGER', true, 4, null);
        $this->addColumn('status_kelaikan', 'StatusKelaikan', 'NUMERIC', true, 3, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('BukuAlatRelatedByBukuAlatId', 'angulex\\Model\\BukuAlat', RelationMap::MANY_TO_ONE, array('buku_alat_id' => 'buku_alat_id', ), null, null);
        $this->addRelation('BukuAlatRelatedByBukuAlatId', 'angulex\\Model\\BukuAlat', RelationMap::MANY_TO_ONE, array('buku_alat_id' => 'buku_alat_id', ), null, null);
        $this->addRelation('SemesterRelatedBySemesterId', 'angulex\\Model\\Semester', RelationMap::MANY_TO_ONE, array('semester_id' => 'semester_id', ), null, null);
        $this->addRelation('SemesterRelatedBySemesterId', 'angulex\\Model\\Semester', RelationMap::MANY_TO_ONE, array('semester_id' => 'semester_id', ), null, null);
    } // buildRelations()

} // BukuAlatLongitudinalTableMap
