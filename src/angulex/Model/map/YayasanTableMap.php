<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'yayasan' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class YayasanTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.YayasanTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('yayasan');
        $this->setPhpName('Yayasan');
        $this->setClassname('angulex\\Model\\Yayasan');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('yayasan_id', 'YayasanId', 'CHAR', true, 16, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 80, null);
        $this->addColumn('alamat_jalan', 'AlamatJalan', 'VARCHAR', true, 80, null);
        $this->addColumn('rt', 'Rt', 'NUMERIC', false, 4, null);
        $this->addColumn('rw', 'Rw', 'NUMERIC', false, 4, null);
        $this->addColumn('nama_dusun', 'NamaDusun', 'VARCHAR', false, 40, null);
        $this->addColumn('desa_kelurahan', 'DesaKelurahan', 'VARCHAR', true, 40, null);
        $this->addForeignKey('kode_wilayah', 'KodeWilayah', 'CHAR', 'ref.mst_wilayah', 'kode_wilayah', true, 8, null);
        $this->addForeignKey('kode_wilayah', 'KodeWilayah', 'CHAR', 'ref.mst_wilayah', 'kode_wilayah', true, 8, null);
        $this->addColumn('kode_pos', 'KodePos', 'CHAR', false, 5, null);
        $this->addColumn('lintang', 'Lintang', 'NUMERIC', false, 12, null);
        $this->addColumn('bujur', 'Bujur', 'NUMERIC', false, 12, null);
        $this->addColumn('nomor_telepon', 'NomorTelepon', 'VARCHAR', false, 20, null);
        $this->addColumn('nomor_fax', 'NomorFax', 'VARCHAR', false, 20, null);
        $this->addColumn('email', 'Email', 'VARCHAR', false, 50, null);
        $this->addColumn('website', 'Website', 'VARCHAR', false, 100, null);
        $this->addColumn('nama_pimpinan_yayasan', 'NamaPimpinanYayasan', 'VARCHAR', true, 50, null);
        $this->addColumn('no_pendirian_yayasan', 'NoPendirianYayasan', 'VARCHAR', false, 40, null);
        $this->addColumn('tanggal_pendirian_yayasan', 'TanggalPendirianYayasan', 'VARCHAR', false, 20, null);
        $this->addColumn('nomor_pengesahan_pn_ln', 'NomorPengesahanPnLn', 'VARCHAR', false, 30, null);
        $this->addColumn('nomor_sk_bn', 'NomorSkBn', 'VARCHAR', false, 255, null);
        $this->addColumn('tanggal_sk_bn', 'TanggalSkBn', 'VARCHAR', false, 20, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('MstWilayahRelatedByKodeWilayah', 'angulex\\Model\\MstWilayah', RelationMap::MANY_TO_ONE, array('kode_wilayah' => 'kode_wilayah', ), null, null);
        $this->addRelation('MstWilayahRelatedByKodeWilayah', 'angulex\\Model\\MstWilayah', RelationMap::MANY_TO_ONE, array('kode_wilayah' => 'kode_wilayah', ), null, null);
        $this->addRelation('VldYayasanRelatedByYayasanId', 'angulex\\Model\\VldYayasan', RelationMap::ONE_TO_MANY, array('yayasan_id' => 'yayasan_id', ), null, null, 'VldYayasansRelatedByYayasanId');
        $this->addRelation('VldYayasanRelatedByYayasanId', 'angulex\\Model\\VldYayasan', RelationMap::ONE_TO_MANY, array('yayasan_id' => 'yayasan_id', ), null, null, 'VldYayasansRelatedByYayasanId');
    } // buildRelations()

} // YayasanTableMap
