<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'anak' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class AnakTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.AnakTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('anak');
        $this->setPhpName('Anak');
        $this->setClassname('angulex\\Model\\Anak');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('anak_id', 'AnakId', 'CHAR', true, 16, null);
        $this->addForeignKey('ptk_id', 'PtkId', 'CHAR', 'ptk', 'ptk_id', true, 16, null);
        $this->addForeignKey('ptk_id', 'PtkId', 'CHAR', 'ptk', 'ptk_id', true, 16, null);
        $this->addForeignKey('status_anak_id', 'StatusAnakId', 'NUMERIC', 'ref.status_anak', 'status_anak_id', true, 3, null);
        $this->addForeignKey('status_anak_id', 'StatusAnakId', 'NUMERIC', 'ref.status_anak', 'status_anak_id', true, 3, null);
        $this->addForeignKey('jenjang_pendidikan_id', 'JenjangPendidikanId', 'NUMERIC', 'ref.jenjang_pendidikan', 'jenjang_pendidikan_id', true, 4, null);
        $this->addForeignKey('jenjang_pendidikan_id', 'JenjangPendidikanId', 'NUMERIC', 'ref.jenjang_pendidikan', 'jenjang_pendidikan_id', true, 4, null);
        $this->addColumn('nisn', 'Nisn', 'CHAR', false, 10, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 50, null);
        $this->addColumn('jenis_kelamin', 'JenisKelamin', 'CHAR', true, 1, null);
        $this->addColumn('tempat_lahir', 'TempatLahir', 'VARCHAR', false, 20, null);
        $this->addColumn('tanggal_lahir', 'TanggalLahir', 'VARCHAR', true, 20, null);
        $this->addColumn('tahun_masuk', 'TahunMasuk', 'INTEGER', false, 4, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('PtkRelatedByPtkId', 'angulex\\Model\\Ptk', RelationMap::MANY_TO_ONE, array('ptk_id' => 'ptk_id', ), null, null);
        $this->addRelation('PtkRelatedByPtkId', 'angulex\\Model\\Ptk', RelationMap::MANY_TO_ONE, array('ptk_id' => 'ptk_id', ), null, null);
        $this->addRelation('JenjangPendidikanRelatedByJenjangPendidikanId', 'angulex\\Model\\JenjangPendidikan', RelationMap::MANY_TO_ONE, array('jenjang_pendidikan_id' => 'jenjang_pendidikan_id', ), null, null);
        $this->addRelation('JenjangPendidikanRelatedByJenjangPendidikanId', 'angulex\\Model\\JenjangPendidikan', RelationMap::MANY_TO_ONE, array('jenjang_pendidikan_id' => 'jenjang_pendidikan_id', ), null, null);
        $this->addRelation('StatusAnakRelatedByStatusAnakId', 'angulex\\Model\\StatusAnak', RelationMap::MANY_TO_ONE, array('status_anak_id' => 'status_anak_id', ), null, null);
        $this->addRelation('StatusAnakRelatedByStatusAnakId', 'angulex\\Model\\StatusAnak', RelationMap::MANY_TO_ONE, array('status_anak_id' => 'status_anak_id', ), null, null);
        $this->addRelation('VldAnakRelatedByAnakId', 'angulex\\Model\\VldAnak', RelationMap::ONE_TO_MANY, array('anak_id' => 'anak_id', ), null, null, 'VldAnaksRelatedByAnakId');
        $this->addRelation('VldAnakRelatedByAnakId', 'angulex\\Model\\VldAnak', RelationMap::ONE_TO_MANY, array('anak_id' => 'anak_id', ), null, null, 'VldAnaksRelatedByAnakId');
    } // buildRelations()

} // AnakTableMap
