<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'peserta_didik' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class PesertaDidikTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.PesertaDidikTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('peserta_didik');
        $this->setPhpName('PesertaDidik');
        $this->setClassname('angulex\\Model\\PesertaDidik');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('peserta_didik_id', 'PesertaDidikId', 'CHAR', true, 16, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', false, 50, null);
        $this->addColumn('jenis_kelamin', 'JenisKelamin', 'CHAR', true, 1, null);
        $this->addColumn('nisn', 'Nisn', 'CHAR', false, 10, null);
        $this->addColumn('nik', 'Nik', 'CHAR', false, 16, null);
        $this->addColumn('tempat_lahir', 'TempatLahir', 'VARCHAR', false, 20, null);
        $this->addColumn('tanggal_lahir', 'TanggalLahir', 'VARCHAR', true, 20, null);
        $this->addForeignKey('agama_id', 'AgamaId', 'SMALLINT', 'ref.agama', 'agama_id', true, 2, null);
        $this->addForeignKey('agama_id', 'AgamaId', 'SMALLINT', 'ref.agama', 'agama_id', true, 2, null);
        $this->addForeignKey('kewarganegaraan', 'Kewarganegaraan', 'CHAR', 'ref.negara', 'negara_id', true, 2, null);
        $this->addForeignKey('kewarganegaraan', 'Kewarganegaraan', 'CHAR', 'ref.negara', 'negara_id', true, 2, null);
        $this->addForeignKey('kebutuhan_khusus_id', 'KebutuhanKhususId', 'INTEGER', 'ref.kebutuhan_khusus', 'kebutuhan_khusus_id', true, 4, null);
        $this->addForeignKey('kebutuhan_khusus_id', 'KebutuhanKhususId', 'INTEGER', 'ref.kebutuhan_khusus', 'kebutuhan_khusus_id', true, 4, null);
        $this->addForeignKey('sekolah_id', 'SekolahId', 'CHAR', 'sekolah', 'sekolah_id', true, 16, null);
        $this->addForeignKey('sekolah_id', 'SekolahId', 'CHAR', 'sekolah', 'sekolah_id', true, 16, null);
        $this->addColumn('alamat_jalan', 'AlamatJalan', 'VARCHAR', true, 80, null);
        $this->addColumn('rt', 'Rt', 'NUMERIC', false, 4, null);
        $this->addColumn('rw', 'Rw', 'NUMERIC', false, 4, null);
        $this->addColumn('nama_dusun', 'NamaDusun', 'VARCHAR', false, 40, null);
        $this->addColumn('desa_kelurahan', 'DesaKelurahan', 'VARCHAR', true, 40, null);
        $this->addForeignKey('kode_wilayah', 'KodeWilayah', 'CHAR', 'ref.mst_wilayah', 'kode_wilayah', true, 8, null);
        $this->addForeignKey('kode_wilayah', 'KodeWilayah', 'CHAR', 'ref.mst_wilayah', 'kode_wilayah', true, 8, null);
        $this->addColumn('kode_pos', 'KodePos', 'CHAR', false, 5, null);
        $this->addForeignKey('jenis_tinggal_id', 'JenisTinggalId', 'NUMERIC', 'ref.jenis_tinggal', 'jenis_tinggal_id', false, 4, null);
        $this->addForeignKey('jenis_tinggal_id', 'JenisTinggalId', 'NUMERIC', 'ref.jenis_tinggal', 'jenis_tinggal_id', false, 4, null);
        $this->addForeignKey('alat_transportasi_id', 'AlatTransportasiId', 'NUMERIC', 'ref.alat_transportasi', 'alat_transportasi_id', false, 4, null);
        $this->addForeignKey('alat_transportasi_id', 'AlatTransportasiId', 'NUMERIC', 'ref.alat_transportasi', 'alat_transportasi_id', false, 4, null);
        $this->addColumn('nomor_telepon_rumah', 'NomorTeleponRumah', 'VARCHAR', false, 20, null);
        $this->addColumn('nomor_telepon_seluler', 'NomorTeleponSeluler', 'VARCHAR', false, 20, null);
        $this->addColumn('email', 'Email', 'VARCHAR', false, 50, null);
        $this->addColumn('penerima_KPS', 'PenerimaKps', 'NUMERIC', true, 3, null);
        $this->addColumn('no_KPS', 'NoKps', 'VARCHAR', false, 40, null);
        $this->addColumn('status_data', 'StatusData', 'INTEGER', false, 4, null);
        $this->addColumn('nama_ayah', 'NamaAyah', 'VARCHAR', false, 50, null);
        $this->addColumn('tahun_lahir_ayah', 'TahunLahirAyah', 'NUMERIC', false, 6, null);
        $this->addForeignKey('jenjang_pendidikan_ayah', 'JenjangPendidikanAyah', 'NUMERIC', 'ref.jenjang_pendidikan', 'jenjang_pendidikan_id', false, 4, null);
        $this->addForeignKey('jenjang_pendidikan_ayah', 'JenjangPendidikanAyah', 'NUMERIC', 'ref.jenjang_pendidikan', 'jenjang_pendidikan_id', false, 4, null);
        $this->addForeignKey('pekerjaan_id_ayah', 'PekerjaanIdAyah', 'INTEGER', 'ref.pekerjaan', 'pekerjaan_id', false, 4, null);
        $this->addForeignKey('pekerjaan_id_ayah', 'PekerjaanIdAyah', 'INTEGER', 'ref.pekerjaan', 'pekerjaan_id', false, 4, null);
        $this->addForeignKey('penghasilan_id_ayah', 'PenghasilanIdAyah', 'INTEGER', 'ref.penghasilan_orangtua_wali', 'penghasilan_orangtua_wali_id', false, 4, null);
        $this->addForeignKey('penghasilan_id_ayah', 'PenghasilanIdAyah', 'INTEGER', 'ref.penghasilan_orangtua_wali', 'penghasilan_orangtua_wali_id', false, 4, null);
        $this->addForeignKey('kebutuhan_khusus_id_ayah', 'KebutuhanKhususIdAyah', 'INTEGER', 'ref.kebutuhan_khusus', 'kebutuhan_khusus_id', true, 4, null);
        $this->addForeignKey('kebutuhan_khusus_id_ayah', 'KebutuhanKhususIdAyah', 'INTEGER', 'ref.kebutuhan_khusus', 'kebutuhan_khusus_id', true, 4, null);
        $this->addColumn('nama_ibu_kandung', 'NamaIbuKandung', 'VARCHAR', true, 50, null);
        $this->addColumn('tahun_lahir_ibu', 'TahunLahirIbu', 'NUMERIC', false, 6, null);
        $this->addForeignKey('jenjang_pendidikan_ibu', 'JenjangPendidikanIbu', 'NUMERIC', 'ref.jenjang_pendidikan', 'jenjang_pendidikan_id', false, 4, null);
        $this->addForeignKey('jenjang_pendidikan_ibu', 'JenjangPendidikanIbu', 'NUMERIC', 'ref.jenjang_pendidikan', 'jenjang_pendidikan_id', false, 4, null);
        $this->addForeignKey('penghasilan_id_ibu', 'PenghasilanIdIbu', 'INTEGER', 'ref.penghasilan_orangtua_wali', 'penghasilan_orangtua_wali_id', false, 4, null);
        $this->addForeignKey('penghasilan_id_ibu', 'PenghasilanIdIbu', 'INTEGER', 'ref.penghasilan_orangtua_wali', 'penghasilan_orangtua_wali_id', false, 4, null);
        $this->addForeignKey('pekerjaan_id_ibu', 'PekerjaanIdIbu', 'INTEGER', 'ref.pekerjaan', 'pekerjaan_id', false, 4, null);
        $this->addForeignKey('pekerjaan_id_ibu', 'PekerjaanIdIbu', 'INTEGER', 'ref.pekerjaan', 'pekerjaan_id', false, 4, null);
        $this->addForeignKey('kebutuhan_khusus_id_ibu', 'KebutuhanKhususIdIbu', 'INTEGER', 'ref.kebutuhan_khusus', 'kebutuhan_khusus_id', true, 4, null);
        $this->addForeignKey('kebutuhan_khusus_id_ibu', 'KebutuhanKhususIdIbu', 'INTEGER', 'ref.kebutuhan_khusus', 'kebutuhan_khusus_id', true, 4, null);
        $this->addColumn('nama_wali', 'NamaWali', 'VARCHAR', false, 30, null);
        $this->addColumn('tahun_lahir_wali', 'TahunLahirWali', 'NUMERIC', false, 6, null);
        $this->addForeignKey('jenjang_pendidikan_wali', 'JenjangPendidikanWali', 'NUMERIC', 'ref.jenjang_pendidikan', 'jenjang_pendidikan_id', false, 4, null);
        $this->addForeignKey('jenjang_pendidikan_wali', 'JenjangPendidikanWali', 'NUMERIC', 'ref.jenjang_pendidikan', 'jenjang_pendidikan_id', false, 4, null);
        $this->addForeignKey('pekerjaan_id_wali', 'PekerjaanIdWali', 'INTEGER', 'ref.pekerjaan', 'pekerjaan_id', false, 4, null);
        $this->addForeignKey('pekerjaan_id_wali', 'PekerjaanIdWali', 'INTEGER', 'ref.pekerjaan', 'pekerjaan_id', false, 4, null);
        $this->addForeignKey('penghasilan_id_wali', 'PenghasilanIdWali', 'INTEGER', 'ref.penghasilan_orangtua_wali', 'penghasilan_orangtua_wali_id', false, 4, null);
        $this->addForeignKey('penghasilan_id_wali', 'PenghasilanIdWali', 'INTEGER', 'ref.penghasilan_orangtua_wali', 'penghasilan_orangtua_wali_id', false, 4, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SekolahRelatedBySekolahId', 'angulex\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('SekolahRelatedBySekolahId', 'angulex\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('AgamaRelatedByAgamaId', 'angulex\\Model\\Agama', RelationMap::MANY_TO_ONE, array('agama_id' => 'agama_id', ), null, null);
        $this->addRelation('AgamaRelatedByAgamaId', 'angulex\\Model\\Agama', RelationMap::MANY_TO_ONE, array('agama_id' => 'agama_id', ), null, null);
        $this->addRelation('AlatTransportasiRelatedByAlatTransportasiId', 'angulex\\Model\\AlatTransportasi', RelationMap::MANY_TO_ONE, array('alat_transportasi_id' => 'alat_transportasi_id', ), null, null);
        $this->addRelation('AlatTransportasiRelatedByAlatTransportasiId', 'angulex\\Model\\AlatTransportasi', RelationMap::MANY_TO_ONE, array('alat_transportasi_id' => 'alat_transportasi_id', ), null, null);
        $this->addRelation('JenisTinggalRelatedByJenisTinggalId', 'angulex\\Model\\JenisTinggal', RelationMap::MANY_TO_ONE, array('jenis_tinggal_id' => 'jenis_tinggal_id', ), null, null);
        $this->addRelation('JenisTinggalRelatedByJenisTinggalId', 'angulex\\Model\\JenisTinggal', RelationMap::MANY_TO_ONE, array('jenis_tinggal_id' => 'jenis_tinggal_id', ), null, null);
        $this->addRelation('JenjangPendidikanRelatedByJenjangPendidikanIbu', 'angulex\\Model\\JenjangPendidikan', RelationMap::MANY_TO_ONE, array('jenjang_pendidikan_ibu' => 'jenjang_pendidikan_id', ), null, null);
        $this->addRelation('JenjangPendidikanRelatedByJenjangPendidikanAyah', 'angulex\\Model\\JenjangPendidikan', RelationMap::MANY_TO_ONE, array('jenjang_pendidikan_ayah' => 'jenjang_pendidikan_id', ), null, null);
        $this->addRelation('JenjangPendidikanRelatedByJenjangPendidikanWali', 'angulex\\Model\\JenjangPendidikan', RelationMap::MANY_TO_ONE, array('jenjang_pendidikan_wali' => 'jenjang_pendidikan_id', ), null, null);
        $this->addRelation('JenjangPendidikanRelatedByJenjangPendidikanIbu', 'angulex\\Model\\JenjangPendidikan', RelationMap::MANY_TO_ONE, array('jenjang_pendidikan_ibu' => 'jenjang_pendidikan_id', ), null, null);
        $this->addRelation('JenjangPendidikanRelatedByJenjangPendidikanAyah', 'angulex\\Model\\JenjangPendidikan', RelationMap::MANY_TO_ONE, array('jenjang_pendidikan_ayah' => 'jenjang_pendidikan_id', ), null, null);
        $this->addRelation('JenjangPendidikanRelatedByJenjangPendidikanWali', 'angulex\\Model\\JenjangPendidikan', RelationMap::MANY_TO_ONE, array('jenjang_pendidikan_wali' => 'jenjang_pendidikan_id', ), null, null);
        $this->addRelation('KebutuhanKhususRelatedByKebutuhanKhususIdAyah', 'angulex\\Model\\KebutuhanKhusus', RelationMap::MANY_TO_ONE, array('kebutuhan_khusus_id_ayah' => 'kebutuhan_khusus_id', ), null, null);
        $this->addRelation('KebutuhanKhususRelatedByKebutuhanKhususIdIbu', 'angulex\\Model\\KebutuhanKhusus', RelationMap::MANY_TO_ONE, array('kebutuhan_khusus_id_ibu' => 'kebutuhan_khusus_id', ), null, null);
        $this->addRelation('KebutuhanKhususRelatedByKebutuhanKhususId', 'angulex\\Model\\KebutuhanKhusus', RelationMap::MANY_TO_ONE, array('kebutuhan_khusus_id' => 'kebutuhan_khusus_id', ), null, null);
        $this->addRelation('KebutuhanKhususRelatedByKebutuhanKhususIdAyah', 'angulex\\Model\\KebutuhanKhusus', RelationMap::MANY_TO_ONE, array('kebutuhan_khusus_id_ayah' => 'kebutuhan_khusus_id', ), null, null);
        $this->addRelation('KebutuhanKhususRelatedByKebutuhanKhususIdIbu', 'angulex\\Model\\KebutuhanKhusus', RelationMap::MANY_TO_ONE, array('kebutuhan_khusus_id_ibu' => 'kebutuhan_khusus_id', ), null, null);
        $this->addRelation('KebutuhanKhususRelatedByKebutuhanKhususId', 'angulex\\Model\\KebutuhanKhusus', RelationMap::MANY_TO_ONE, array('kebutuhan_khusus_id' => 'kebutuhan_khusus_id', ), null, null);
        $this->addRelation('MstWilayahRelatedByKodeWilayah', 'angulex\\Model\\MstWilayah', RelationMap::MANY_TO_ONE, array('kode_wilayah' => 'kode_wilayah', ), null, null);
        $this->addRelation('MstWilayahRelatedByKodeWilayah', 'angulex\\Model\\MstWilayah', RelationMap::MANY_TO_ONE, array('kode_wilayah' => 'kode_wilayah', ), null, null);
        $this->addRelation('NegaraRelatedByKewarganegaraan', 'angulex\\Model\\Negara', RelationMap::MANY_TO_ONE, array('kewarganegaraan' => 'negara_id', ), null, null);
        $this->addRelation('NegaraRelatedByKewarganegaraan', 'angulex\\Model\\Negara', RelationMap::MANY_TO_ONE, array('kewarganegaraan' => 'negara_id', ), null, null);
        $this->addRelation('PekerjaanRelatedByPekerjaanIdAyah', 'angulex\\Model\\Pekerjaan', RelationMap::MANY_TO_ONE, array('pekerjaan_id_ayah' => 'pekerjaan_id', ), null, null);
        $this->addRelation('PekerjaanRelatedByPekerjaanIdIbu', 'angulex\\Model\\Pekerjaan', RelationMap::MANY_TO_ONE, array('pekerjaan_id_ibu' => 'pekerjaan_id', ), null, null);
        $this->addRelation('PekerjaanRelatedByPekerjaanIdWali', 'angulex\\Model\\Pekerjaan', RelationMap::MANY_TO_ONE, array('pekerjaan_id_wali' => 'pekerjaan_id', ), null, null);
        $this->addRelation('PekerjaanRelatedByPekerjaanIdAyah', 'angulex\\Model\\Pekerjaan', RelationMap::MANY_TO_ONE, array('pekerjaan_id_ayah' => 'pekerjaan_id', ), null, null);
        $this->addRelation('PekerjaanRelatedByPekerjaanIdIbu', 'angulex\\Model\\Pekerjaan', RelationMap::MANY_TO_ONE, array('pekerjaan_id_ibu' => 'pekerjaan_id', ), null, null);
        $this->addRelation('PekerjaanRelatedByPekerjaanIdWali', 'angulex\\Model\\Pekerjaan', RelationMap::MANY_TO_ONE, array('pekerjaan_id_wali' => 'pekerjaan_id', ), null, null);
        $this->addRelation('PenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah', 'angulex\\Model\\PenghasilanOrangtuaWali', RelationMap::MANY_TO_ONE, array('penghasilan_id_ayah' => 'penghasilan_orangtua_wali_id', ), null, null);
        $this->addRelation('PenghasilanOrangtuaWaliRelatedByPenghasilanIdWali', 'angulex\\Model\\PenghasilanOrangtuaWali', RelationMap::MANY_TO_ONE, array('penghasilan_id_wali' => 'penghasilan_orangtua_wali_id', ), null, null);
        $this->addRelation('PenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu', 'angulex\\Model\\PenghasilanOrangtuaWali', RelationMap::MANY_TO_ONE, array('penghasilan_id_ibu' => 'penghasilan_orangtua_wali_id', ), null, null);
        $this->addRelation('PenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah', 'angulex\\Model\\PenghasilanOrangtuaWali', RelationMap::MANY_TO_ONE, array('penghasilan_id_ayah' => 'penghasilan_orangtua_wali_id', ), null, null);
        $this->addRelation('PenghasilanOrangtuaWaliRelatedByPenghasilanIdWali', 'angulex\\Model\\PenghasilanOrangtuaWali', RelationMap::MANY_TO_ONE, array('penghasilan_id_wali' => 'penghasilan_orangtua_wali_id', ), null, null);
        $this->addRelation('PenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu', 'angulex\\Model\\PenghasilanOrangtuaWali', RelationMap::MANY_TO_ONE, array('penghasilan_id_ibu' => 'penghasilan_orangtua_wali_id', ), null, null);
        $this->addRelation('PrestasiRelatedByPesertaDidikId', 'angulex\\Model\\Prestasi', RelationMap::ONE_TO_MANY, array('peserta_didik_id' => 'peserta_didik_id', ), null, null, 'PrestasisRelatedByPesertaDidikId');
        $this->addRelation('PrestasiRelatedByPesertaDidikId', 'angulex\\Model\\Prestasi', RelationMap::ONE_TO_MANY, array('peserta_didik_id' => 'peserta_didik_id', ), null, null, 'PrestasisRelatedByPesertaDidikId');
        $this->addRelation('AnggotaRombelRelatedByPesertaDidikId', 'angulex\\Model\\AnggotaRombel', RelationMap::ONE_TO_MANY, array('peserta_didik_id' => 'peserta_didik_id', ), null, null, 'AnggotaRombelsRelatedByPesertaDidikId');
        $this->addRelation('AnggotaRombelRelatedByPesertaDidikId', 'angulex\\Model\\AnggotaRombel', RelationMap::ONE_TO_MANY, array('peserta_didik_id' => 'peserta_didik_id', ), null, null, 'AnggotaRombelsRelatedByPesertaDidikId');
        $this->addRelation('PesertaDidikLongitudinalRelatedByPesertaDidikId', 'angulex\\Model\\PesertaDidikLongitudinal', RelationMap::ONE_TO_MANY, array('peserta_didik_id' => 'peserta_didik_id', ), null, null, 'PesertaDidikLongitudinalsRelatedByPesertaDidikId');
        $this->addRelation('PesertaDidikLongitudinalRelatedByPesertaDidikId', 'angulex\\Model\\PesertaDidikLongitudinal', RelationMap::ONE_TO_MANY, array('peserta_didik_id' => 'peserta_didik_id', ), null, null, 'PesertaDidikLongitudinalsRelatedByPesertaDidikId');
        $this->addRelation('BeasiswaPesertaDidikRelatedByPesertaDidikId', 'angulex\\Model\\BeasiswaPesertaDidik', RelationMap::ONE_TO_MANY, array('peserta_didik_id' => 'peserta_didik_id', ), null, null, 'BeasiswaPesertaDidiksRelatedByPesertaDidikId');
        $this->addRelation('BeasiswaPesertaDidikRelatedByPesertaDidikId', 'angulex\\Model\\BeasiswaPesertaDidik', RelationMap::ONE_TO_MANY, array('peserta_didik_id' => 'peserta_didik_id', ), null, null, 'BeasiswaPesertaDidiksRelatedByPesertaDidikId');
        $this->addRelation('RegistrasiPesertaDidikRelatedByPesertaDidikId', 'angulex\\Model\\RegistrasiPesertaDidik', RelationMap::ONE_TO_MANY, array('peserta_didik_id' => 'peserta_didik_id', ), null, null, 'RegistrasiPesertaDidiksRelatedByPesertaDidikId');
        $this->addRelation('RegistrasiPesertaDidikRelatedByPesertaDidikId', 'angulex\\Model\\RegistrasiPesertaDidik', RelationMap::ONE_TO_MANY, array('peserta_didik_id' => 'peserta_didik_id', ), null, null, 'RegistrasiPesertaDidiksRelatedByPesertaDidikId');
        $this->addRelation('VldPesertaDidikRelatedByPesertaDidikId', 'angulex\\Model\\VldPesertaDidik', RelationMap::ONE_TO_MANY, array('peserta_didik_id' => 'peserta_didik_id', ), null, null, 'VldPesertaDidiksRelatedByPesertaDidikId');
        $this->addRelation('VldPesertaDidikRelatedByPesertaDidikId', 'angulex\\Model\\VldPesertaDidik', RelationMap::ONE_TO_MANY, array('peserta_didik_id' => 'peserta_didik_id', ), null, null, 'VldPesertaDidiksRelatedByPesertaDidikId');
        $this->addRelation('PesertaDidikBaruRelatedByPesertaDidikId', 'angulex\\Model\\PesertaDidikBaru', RelationMap::ONE_TO_MANY, array('peserta_didik_id' => 'peserta_didik_id', ), null, null, 'PesertaDidikBarusRelatedByPesertaDidikId');
        $this->addRelation('PesertaDidikBaruRelatedByPesertaDidikId', 'angulex\\Model\\PesertaDidikBaru', RelationMap::ONE_TO_MANY, array('peserta_didik_id' => 'peserta_didik_id', ), null, null, 'PesertaDidikBarusRelatedByPesertaDidikId');
    } // buildRelations()

} // PesertaDidikTableMap
