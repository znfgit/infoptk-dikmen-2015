<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.batas_waktu_rapor' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class BatasWaktuRaporTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.BatasWaktuRaporTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.batas_waktu_rapor');
        $this->setPhpName('BatasWaktuRapor');
        $this->setClassname('angulex\\Model\\BatasWaktuRapor');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addForeignPrimaryKey('semester_id', 'SemesterId', 'CHAR' , 'ref.semester', 'semester_id', true, 5, null);
        $this->addColumn('tgl_rapor_mulai', 'TglRaporMulai', 'VARCHAR', true, 20, null);
        $this->addColumn('tgl_rapor_selesai', 'TglRaporSelesai', 'VARCHAR', true, 20, null);
        $this->addColumn('tgl_usm_mulai', 'TglUsmMulai', 'VARCHAR', false, 20, null);
        $this->addColumn('tgl_usm_selesai', 'TglUsmSelesai', 'VARCHAR', false, 20, null);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Semester', 'angulex\\Model\\Semester', RelationMap::MANY_TO_ONE, array('semester_id' => 'semester_id', ), null, null);
    } // buildRelations()

} // BatasWaktuRaporTableMap
