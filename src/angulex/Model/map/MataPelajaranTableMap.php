<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.mata_pelajaran' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class MataPelajaranTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.MataPelajaranTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.mata_pelajaran');
        $this->setPhpName('MataPelajaran');
        $this->setClassname('angulex\\Model\\MataPelajaran');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('mata_pelajaran_id', 'MataPelajaranId', 'INTEGER', true, 4, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 50, null);
        $this->addColumn('pilihan_sekolah', 'PilihanSekolah', 'NUMERIC', true, 3, null);
        $this->addColumn('pilihan_buku', 'PilihanBuku', 'NUMERIC', true, 3, null);
        $this->addColumn('pilihan_kepengawasan', 'PilihanKepengawasan', 'NUMERIC', true, 3, null);
        $this->addColumn('pilihan_evaluasi', 'PilihanEvaluasi', 'NUMERIC', true, 3, 0);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('BukuAlatRelatedByMataPelajaranId', 'angulex\\Model\\BukuAlat', RelationMap::ONE_TO_MANY, array('mata_pelajaran_id' => 'mata_pelajaran_id', ), null, null, 'BukuAlatsRelatedByMataPelajaranId');
        $this->addRelation('BukuAlatRelatedByMataPelajaranId', 'angulex\\Model\\BukuAlat', RelationMap::ONE_TO_MANY, array('mata_pelajaran_id' => 'mata_pelajaran_id', ), null, null, 'BukuAlatsRelatedByMataPelajaranId');
        $this->addRelation('TemplateUnRelatedByMp3Id', 'angulex\\Model\\TemplateUn', RelationMap::ONE_TO_MANY, array('mata_pelajaran_id' => 'mp3_id', ), null, null, 'TemplateUnsRelatedByMp3Id');
        $this->addRelation('TemplateUnRelatedByMp4Id', 'angulex\\Model\\TemplateUn', RelationMap::ONE_TO_MANY, array('mata_pelajaran_id' => 'mp4_id', ), null, null, 'TemplateUnsRelatedByMp4Id');
        $this->addRelation('TemplateUnRelatedByMp7Id', 'angulex\\Model\\TemplateUn', RelationMap::ONE_TO_MANY, array('mata_pelajaran_id' => 'mp7_id', ), null, null, 'TemplateUnsRelatedByMp7Id');
        $this->addRelation('TemplateUnRelatedByMp5Id', 'angulex\\Model\\TemplateUn', RelationMap::ONE_TO_MANY, array('mata_pelajaran_id' => 'mp5_id', ), null, null, 'TemplateUnsRelatedByMp5Id');
        $this->addRelation('TemplateUnRelatedByMp1Id', 'angulex\\Model\\TemplateUn', RelationMap::ONE_TO_MANY, array('mata_pelajaran_id' => 'mp1_id', ), null, null, 'TemplateUnsRelatedByMp1Id');
        $this->addRelation('TemplateUnRelatedByMp2Id', 'angulex\\Model\\TemplateUn', RelationMap::ONE_TO_MANY, array('mata_pelajaran_id' => 'mp2_id', ), null, null, 'TemplateUnsRelatedByMp2Id');
        $this->addRelation('TemplateUnRelatedByMp6Id', 'angulex\\Model\\TemplateUn', RelationMap::ONE_TO_MANY, array('mata_pelajaran_id' => 'mp6_id', ), null, null, 'TemplateUnsRelatedByMp6Id');
        $this->addRelation('TemplateRapor', 'angulex\\Model\\TemplateRapor', RelationMap::ONE_TO_MANY, array('mata_pelajaran_id' => 'mata_pelajaran_id', ), null, null, 'TemplateRapors');
        $this->addRelation('MataPelajaranKurikulum', 'angulex\\Model\\MataPelajaranKurikulum', RelationMap::ONE_TO_MANY, array('mata_pelajaran_id' => 'mata_pelajaran_id', ), null, null, 'MataPelajaranKurikulums');
        $this->addRelation('MapBidangMataPelajaran', 'angulex\\Model\\MapBidangMataPelajaran', RelationMap::ONE_TO_MANY, array('mata_pelajaran_id' => 'mata_pelajaran_id', ), null, null, 'MapBidangMataPelajarans');
        $this->addRelation('PembelajaranRelatedByMataPelajaranId', 'angulex\\Model\\Pembelajaran', RelationMap::ONE_TO_MANY, array('mata_pelajaran_id' => 'mata_pelajaran_id', ), null, null, 'PembelajaransRelatedByMataPelajaranId');
        $this->addRelation('PembelajaranRelatedByMataPelajaranId', 'angulex\\Model\\Pembelajaran', RelationMap::ONE_TO_MANY, array('mata_pelajaran_id' => 'mata_pelajaran_id', ), null, null, 'PembelajaransRelatedByMataPelajaranId');
        $this->addRelation('PengawasTerdaftarRelatedByMataPelajaranId', 'angulex\\Model\\PengawasTerdaftar', RelationMap::ONE_TO_MANY, array('mata_pelajaran_id' => 'mata_pelajaran_id', ), null, null, 'PengawasTerdaftarsRelatedByMataPelajaranId');
        $this->addRelation('PengawasTerdaftarRelatedByMataPelajaranId', 'angulex\\Model\\PengawasTerdaftar', RelationMap::ONE_TO_MANY, array('mata_pelajaran_id' => 'mata_pelajaran_id', ), null, null, 'PengawasTerdaftarsRelatedByMataPelajaranId');
    } // buildRelations()

} // MataPelajaranTableMap
