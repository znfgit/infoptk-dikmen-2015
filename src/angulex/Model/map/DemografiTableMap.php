<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'demografi' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class DemografiTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.DemografiTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('demografi');
        $this->setPhpName('Demografi');
        $this->setClassname('angulex\\Model\\Demografi');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('demografi_id', 'DemografiId', 'CHAR', true, 16, null);
        $this->addForeignKey('kode_wilayah', 'KodeWilayah', 'CHAR', 'ref.mst_wilayah', 'kode_wilayah', true, 8, null);
        $this->addForeignKey('kode_wilayah', 'KodeWilayah', 'CHAR', 'ref.mst_wilayah', 'kode_wilayah', true, 8, null);
        $this->addForeignKey('tahun_ajaran_id', 'TahunAjaranId', 'NUMERIC', 'ref.tahun_ajaran', 'tahun_ajaran_id', true, 6, null);
        $this->addForeignKey('tahun_ajaran_id', 'TahunAjaranId', 'NUMERIC', 'ref.tahun_ajaran', 'tahun_ajaran_id', true, 6, null);
        $this->addColumn('usia_5', 'Usia5', 'BIGINT', true, 8, null);
        $this->addColumn('usia_7', 'Usia7', 'BIGINT', true, 8, null);
        $this->addColumn('usia_13', 'Usia13', 'BIGINT', true, 8, null);
        $this->addColumn('usia_16', 'Usia16', 'BIGINT', true, 8, null);
        $this->addColumn('usia_18', 'Usia18', 'BIGINT', true, 8, null);
        $this->addColumn('jumlah_penduduk', 'JumlahPenduduk', 'BIGINT', true, 8, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('MstWilayahRelatedByKodeWilayah', 'angulex\\Model\\MstWilayah', RelationMap::MANY_TO_ONE, array('kode_wilayah' => 'kode_wilayah', ), null, null);
        $this->addRelation('MstWilayahRelatedByKodeWilayah', 'angulex\\Model\\MstWilayah', RelationMap::MANY_TO_ONE, array('kode_wilayah' => 'kode_wilayah', ), null, null);
        $this->addRelation('TahunAjaranRelatedByTahunAjaranId', 'angulex\\Model\\TahunAjaran', RelationMap::MANY_TO_ONE, array('tahun_ajaran_id' => 'tahun_ajaran_id', ), null, null);
        $this->addRelation('TahunAjaranRelatedByTahunAjaranId', 'angulex\\Model\\TahunAjaran', RelationMap::MANY_TO_ONE, array('tahun_ajaran_id' => 'tahun_ajaran_id', ), null, null);
        $this->addRelation('VldDemografiRelatedByDemografiId', 'angulex\\Model\\VldDemografi', RelationMap::ONE_TO_MANY, array('demografi_id' => 'demografi_id', ), null, null, 'VldDemografisRelatedByDemografiId');
        $this->addRelation('VldDemografiRelatedByDemografiId', 'angulex\\Model\\VldDemografi', RelationMap::ONE_TO_MANY, array('demografi_id' => 'demografi_id', ), null, null, 'VldDemografisRelatedByDemografiId');
    } // buildRelations()

} // DemografiTableMap
