<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ptk' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class PtkTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.PtkTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ptk');
        $this->setPhpName('Ptk');
        $this->setClassname('angulex\\Model\\Ptk');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('ptk_id', 'PtkId', 'CHAR', true, 16, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 50, null);
        $this->addColumn('nip', 'Nip', 'VARCHAR', false, 18, null);
        $this->addColumn('jenis_kelamin', 'JenisKelamin', 'CHAR', true, 1, null);
        $this->addColumn('tempat_lahir', 'TempatLahir', 'VARCHAR', true, 20, null);
        $this->addColumn('tanggal_lahir', 'TanggalLahir', 'VARCHAR', true, 20, null);
        $this->addColumn('nik', 'Nik', 'CHAR', true, 16, null);
        $this->addColumn('niy_nigk', 'NiyNigk', 'VARCHAR', false, 30, null);
        $this->addColumn('nuptk', 'Nuptk', 'CHAR', false, 16, null);
        $this->addForeignKey('status_kepegawaian_id', 'StatusKepegawaianId', 'SMALLINT', 'ref.status_kepegawaian', 'status_kepegawaian_id', true, 2, null);
        $this->addForeignKey('status_kepegawaian_id', 'StatusKepegawaianId', 'SMALLINT', 'ref.status_kepegawaian', 'status_kepegawaian_id', true, 2, null);
        $this->addForeignKey('jenis_ptk_id', 'JenisPtkId', 'NUMERIC', 'ref.jenis_ptk', 'jenis_ptk_id', true, 4, null);
        $this->addForeignKey('jenis_ptk_id', 'JenisPtkId', 'NUMERIC', 'ref.jenis_ptk', 'jenis_ptk_id', true, 4, null);
        $this->addForeignKey('pengawas_bidang_studi_id', 'PengawasBidangStudiId', 'INTEGER', 'ref.bidang_studi', 'bidang_studi_id', false, 4, null);
        $this->addForeignKey('pengawas_bidang_studi_id', 'PengawasBidangStudiId', 'INTEGER', 'ref.bidang_studi', 'bidang_studi_id', false, 4, null);
        $this->addForeignKey('agama_id', 'AgamaId', 'SMALLINT', 'ref.agama', 'agama_id', true, 2, null);
        $this->addForeignKey('agama_id', 'AgamaId', 'SMALLINT', 'ref.agama', 'agama_id', true, 2, null);
        $this->addForeignKey('kewarganegaraan', 'Kewarganegaraan', 'CHAR', 'ref.negara', 'negara_id', true, 2, null);
        $this->addForeignKey('kewarganegaraan', 'Kewarganegaraan', 'CHAR', 'ref.negara', 'negara_id', true, 2, null);
        $this->addColumn('alamat_jalan', 'AlamatJalan', 'VARCHAR', true, 80, null);
        $this->addColumn('rt', 'Rt', 'NUMERIC', false, 4, null);
        $this->addColumn('rw', 'Rw', 'NUMERIC', false, 4, null);
        $this->addColumn('nama_dusun', 'NamaDusun', 'VARCHAR', false, 40, null);
        $this->addColumn('desa_kelurahan', 'DesaKelurahan', 'VARCHAR', true, 40, null);
        $this->addForeignKey('kode_wilayah', 'KodeWilayah', 'CHAR', 'ref.mst_wilayah', 'kode_wilayah', true, 8, null);
        $this->addForeignKey('kode_wilayah', 'KodeWilayah', 'CHAR', 'ref.mst_wilayah', 'kode_wilayah', true, 8, null);
        $this->addColumn('kode_pos', 'KodePos', 'CHAR', false, 5, null);
        $this->addColumn('no_telepon_rumah', 'NoTeleponRumah', 'VARCHAR', false, 20, null);
        $this->addColumn('no_hp', 'NoHp', 'VARCHAR', false, 20, null);
        $this->addColumn('email', 'Email', 'VARCHAR', false, 50, null);
        $this->addForeignKey('entry_sekolah_id', 'EntrySekolahId', 'CHAR', 'sekolah', 'sekolah_id', true, 16, null);
        $this->addForeignKey('entry_sekolah_id', 'EntrySekolahId', 'CHAR', 'sekolah', 'sekolah_id', true, 16, null);
        $this->addForeignKey('status_keaktifan_id', 'StatusKeaktifanId', 'NUMERIC', 'ref.status_keaktifan_pegawai', 'status_keaktifan_id', true, 4, null);
        $this->addForeignKey('status_keaktifan_id', 'StatusKeaktifanId', 'NUMERIC', 'ref.status_keaktifan_pegawai', 'status_keaktifan_id', true, 4, null);
        $this->addColumn('sk_cpns', 'SkCpns', 'VARCHAR', false, 40, null);
        $this->addColumn('tgl_cpns', 'TglCpns', 'VARCHAR', false, 20, null);
        $this->addColumn('sk_pengangkatan', 'SkPengangkatan', 'VARCHAR', false, 40, null);
        $this->addColumn('tmt_pengangkatan', 'TmtPengangkatan', 'VARCHAR', false, 20, null);
        $this->addForeignKey('lembaga_pengangkat_id', 'LembagaPengangkatId', 'NUMERIC', 'ref.lembaga_pengangkat', 'lembaga_pengangkat_id', true, 4, null);
        $this->addForeignKey('lembaga_pengangkat_id', 'LembagaPengangkatId', 'NUMERIC', 'ref.lembaga_pengangkat', 'lembaga_pengangkat_id', true, 4, null);
        $this->addForeignKey('pangkat_golongan_id', 'PangkatGolonganId', 'NUMERIC', 'ref.pangkat_golongan', 'pangkat_golongan_id', false, 4, null);
        $this->addForeignKey('pangkat_golongan_id', 'PangkatGolonganId', 'NUMERIC', 'ref.pangkat_golongan', 'pangkat_golongan_id', false, 4, null);
        $this->addForeignKey('keahlian_laboratorium_id', 'KeahlianLaboratoriumId', 'SMALLINT', 'ref.keahlian_laboratorium', 'keahlian_laboratorium_id', false, 2, null);
        $this->addForeignKey('keahlian_laboratorium_id', 'KeahlianLaboratoriumId', 'SMALLINT', 'ref.keahlian_laboratorium', 'keahlian_laboratorium_id', false, 2, null);
        $this->addForeignKey('sumber_gaji_id', 'SumberGajiId', 'NUMERIC', 'ref.sumber_gaji', 'sumber_gaji_id', true, 4, null);
        $this->addForeignKey('sumber_gaji_id', 'SumberGajiId', 'NUMERIC', 'ref.sumber_gaji', 'sumber_gaji_id', true, 4, null);
        $this->addColumn('nama_ibu_kandung', 'NamaIbuKandung', 'VARCHAR', true, 50, null);
        $this->addColumn('status_perkawinan', 'StatusPerkawinan', 'NUMERIC', true, 3, null);
        $this->addColumn('nama_suami_istri', 'NamaSuamiIstri', 'VARCHAR', false, 50, null);
        $this->addColumn('nip_suami_istri', 'NipSuamiIstri', 'CHAR', false, 18, null);
        $this->addForeignKey('pekerjaan_suami_istri', 'PekerjaanSuamiIstri', 'INTEGER', 'ref.pekerjaan', 'pekerjaan_id', true, 4, null);
        $this->addForeignKey('pekerjaan_suami_istri', 'PekerjaanSuamiIstri', 'INTEGER', 'ref.pekerjaan', 'pekerjaan_id', true, 4, null);
        $this->addColumn('tmt_pns', 'TmtPns', 'VARCHAR', false, 20, null);
        $this->addColumn('sudah_lisensi_kepala_sekolah', 'SudahLisensiKepalaSekolah', 'NUMERIC', true, 3, null);
        $this->addColumn('jumlah_sekolah_binaan', 'JumlahSekolahBinaan', 'SMALLINT', false, 2, null);
        $this->addColumn('pernah_diklat_kepengawasan', 'PernahDiklatKepengawasan', 'NUMERIC', true, 3, null);
        $this->addColumn('status_data', 'StatusData', 'INTEGER', false, 4, null);
        $this->addForeignKey('mampu_handle_kk', 'MampuHandleKk', 'INTEGER', 'ref.kebutuhan_khusus', 'kebutuhan_khusus_id', true, 4, null);
        $this->addForeignKey('mampu_handle_kk', 'MampuHandleKk', 'INTEGER', 'ref.kebutuhan_khusus', 'kebutuhan_khusus_id', true, 4, null);
        $this->addColumn('keahlian_braille', 'KeahlianBraille', 'NUMERIC', false, 3, 0);
        $this->addColumn('keahlian_bhs_isyarat', 'KeahlianBhsIsyarat', 'NUMERIC', false, 3, 0);
        $this->addColumn('npwp', 'Npwp', 'CHAR', false, 15, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SekolahRelatedByEntrySekolahId', 'angulex\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('entry_sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('SekolahRelatedByEntrySekolahId', 'angulex\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('entry_sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('AgamaRelatedByAgamaId', 'angulex\\Model\\Agama', RelationMap::MANY_TO_ONE, array('agama_id' => 'agama_id', ), null, null);
        $this->addRelation('AgamaRelatedByAgamaId', 'angulex\\Model\\Agama', RelationMap::MANY_TO_ONE, array('agama_id' => 'agama_id', ), null, null);
        $this->addRelation('BidangStudiRelatedByPengawasBidangStudiId', 'angulex\\Model\\BidangStudi', RelationMap::MANY_TO_ONE, array('pengawas_bidang_studi_id' => 'bidang_studi_id', ), null, null);
        $this->addRelation('BidangStudiRelatedByPengawasBidangStudiId', 'angulex\\Model\\BidangStudi', RelationMap::MANY_TO_ONE, array('pengawas_bidang_studi_id' => 'bidang_studi_id', ), null, null);
        $this->addRelation('JenisPtkRelatedByJenisPtkId', 'angulex\\Model\\JenisPtk', RelationMap::MANY_TO_ONE, array('jenis_ptk_id' => 'jenis_ptk_id', ), null, null);
        $this->addRelation('JenisPtkRelatedByJenisPtkId', 'angulex\\Model\\JenisPtk', RelationMap::MANY_TO_ONE, array('jenis_ptk_id' => 'jenis_ptk_id', ), null, null);
        $this->addRelation('KeahlianLaboratoriumRelatedByKeahlianLaboratoriumId', 'angulex\\Model\\KeahlianLaboratorium', RelationMap::MANY_TO_ONE, array('keahlian_laboratorium_id' => 'keahlian_laboratorium_id', ), null, null);
        $this->addRelation('KeahlianLaboratoriumRelatedByKeahlianLaboratoriumId', 'angulex\\Model\\KeahlianLaboratorium', RelationMap::MANY_TO_ONE, array('keahlian_laboratorium_id' => 'keahlian_laboratorium_id', ), null, null);
        $this->addRelation('KebutuhanKhususRelatedByMampuHandleKk', 'angulex\\Model\\KebutuhanKhusus', RelationMap::MANY_TO_ONE, array('mampu_handle_kk' => 'kebutuhan_khusus_id', ), null, null);
        $this->addRelation('KebutuhanKhususRelatedByMampuHandleKk', 'angulex\\Model\\KebutuhanKhusus', RelationMap::MANY_TO_ONE, array('mampu_handle_kk' => 'kebutuhan_khusus_id', ), null, null);
        $this->addRelation('LembagaPengangkatRelatedByLembagaPengangkatId', 'angulex\\Model\\LembagaPengangkat', RelationMap::MANY_TO_ONE, array('lembaga_pengangkat_id' => 'lembaga_pengangkat_id', ), null, null);
        $this->addRelation('LembagaPengangkatRelatedByLembagaPengangkatId', 'angulex\\Model\\LembagaPengangkat', RelationMap::MANY_TO_ONE, array('lembaga_pengangkat_id' => 'lembaga_pengangkat_id', ), null, null);
        $this->addRelation('MstWilayahRelatedByKodeWilayah', 'angulex\\Model\\MstWilayah', RelationMap::MANY_TO_ONE, array('kode_wilayah' => 'kode_wilayah', ), null, null);
        $this->addRelation('MstWilayahRelatedByKodeWilayah', 'angulex\\Model\\MstWilayah', RelationMap::MANY_TO_ONE, array('kode_wilayah' => 'kode_wilayah', ), null, null);
        $this->addRelation('NegaraRelatedByKewarganegaraan', 'angulex\\Model\\Negara', RelationMap::MANY_TO_ONE, array('kewarganegaraan' => 'negara_id', ), null, null);
        $this->addRelation('NegaraRelatedByKewarganegaraan', 'angulex\\Model\\Negara', RelationMap::MANY_TO_ONE, array('kewarganegaraan' => 'negara_id', ), null, null);
        $this->addRelation('PangkatGolonganRelatedByPangkatGolonganId', 'angulex\\Model\\PangkatGolongan', RelationMap::MANY_TO_ONE, array('pangkat_golongan_id' => 'pangkat_golongan_id', ), null, null);
        $this->addRelation('PangkatGolonganRelatedByPangkatGolonganId', 'angulex\\Model\\PangkatGolongan', RelationMap::MANY_TO_ONE, array('pangkat_golongan_id' => 'pangkat_golongan_id', ), null, null);
        $this->addRelation('PekerjaanRelatedByPekerjaanSuamiIstri', 'angulex\\Model\\Pekerjaan', RelationMap::MANY_TO_ONE, array('pekerjaan_suami_istri' => 'pekerjaan_id', ), null, null);
        $this->addRelation('PekerjaanRelatedByPekerjaanSuamiIstri', 'angulex\\Model\\Pekerjaan', RelationMap::MANY_TO_ONE, array('pekerjaan_suami_istri' => 'pekerjaan_id', ), null, null);
        $this->addRelation('StatusKepegawaianRelatedByStatusKepegawaianId', 'angulex\\Model\\StatusKepegawaian', RelationMap::MANY_TO_ONE, array('status_kepegawaian_id' => 'status_kepegawaian_id', ), null, null);
        $this->addRelation('StatusKepegawaianRelatedByStatusKepegawaianId', 'angulex\\Model\\StatusKepegawaian', RelationMap::MANY_TO_ONE, array('status_kepegawaian_id' => 'status_kepegawaian_id', ), null, null);
        $this->addRelation('StatusKeaktifanPegawaiRelatedByStatusKeaktifanId', 'angulex\\Model\\StatusKeaktifanPegawai', RelationMap::MANY_TO_ONE, array('status_keaktifan_id' => 'status_keaktifan_id', ), null, null);
        $this->addRelation('StatusKeaktifanPegawaiRelatedByStatusKeaktifanId', 'angulex\\Model\\StatusKeaktifanPegawai', RelationMap::MANY_TO_ONE, array('status_keaktifan_id' => 'status_keaktifan_id', ), null, null);
        $this->addRelation('SumberGajiRelatedBySumberGajiId', 'angulex\\Model\\SumberGaji', RelationMap::MANY_TO_ONE, array('sumber_gaji_id' => 'sumber_gaji_id', ), null, null);
        $this->addRelation('SumberGajiRelatedBySumberGajiId', 'angulex\\Model\\SumberGaji', RelationMap::MANY_TO_ONE, array('sumber_gaji_id' => 'sumber_gaji_id', ), null, null);
        $this->addRelation('KesejahteraanRelatedByPtkId', 'angulex\\Model\\Kesejahteraan', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'KesejahteraansRelatedByPtkId');
        $this->addRelation('KesejahteraanRelatedByPtkId', 'angulex\\Model\\Kesejahteraan', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'KesejahteraansRelatedByPtkId');
        $this->addRelation('VldPtkRelatedByPtkId', 'angulex\\Model\\VldPtk', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'VldPtksRelatedByPtkId');
        $this->addRelation('VldPtkRelatedByPtkId', 'angulex\\Model\\VldPtk', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'VldPtksRelatedByPtkId');
        $this->addRelation('PenghargaanRelatedByPtkId', 'angulex\\Model\\Penghargaan', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'PenghargaansRelatedByPtkId');
        $this->addRelation('PenghargaanRelatedByPtkId', 'angulex\\Model\\Penghargaan', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'PenghargaansRelatedByPtkId');
        $this->addRelation('InpassingRelatedByPtkId', 'angulex\\Model\\Inpassing', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'InpassingsRelatedByPtkId');
        $this->addRelation('InpassingRelatedByPtkId', 'angulex\\Model\\Inpassing', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'InpassingsRelatedByPtkId');
        $this->addRelation('PtkTerdaftarRelatedByPtkId', 'angulex\\Model\\PtkTerdaftar', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'PtkTerdaftarsRelatedByPtkId');
        $this->addRelation('PtkTerdaftarRelatedByPtkId', 'angulex\\Model\\PtkTerdaftar', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'PtkTerdaftarsRelatedByPtkId');
        $this->addRelation('KaryaTulisRelatedByPtkId', 'angulex\\Model\\KaryaTulis', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'KaryaTulissRelatedByPtkId');
        $this->addRelation('KaryaTulisRelatedByPtkId', 'angulex\\Model\\KaryaTulis', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'KaryaTulissRelatedByPtkId');
        $this->addRelation('NilaiTestRelatedByPtkId', 'angulex\\Model\\NilaiTest', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'NilaiTestsRelatedByPtkId');
        $this->addRelation('NilaiTestRelatedByPtkId', 'angulex\\Model\\NilaiTest', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'NilaiTestsRelatedByPtkId');
        $this->addRelation('BukuPtkRelatedByPtkId', 'angulex\\Model\\BukuPtk', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'BukuPtksRelatedByPtkId');
        $this->addRelation('BukuPtkRelatedByPtkId', 'angulex\\Model\\BukuPtk', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'BukuPtksRelatedByPtkId');
        $this->addRelation('BeasiswaPtkRelatedByPtkId', 'angulex\\Model\\BeasiswaPtk', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'BeasiswaPtksRelatedByPtkId');
        $this->addRelation('BeasiswaPtkRelatedByPtkId', 'angulex\\Model\\BeasiswaPtk', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'BeasiswaPtksRelatedByPtkId');
        $this->addRelation('RwyKepangkatanRelatedByPtkId', 'angulex\\Model\\RwyKepangkatan', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'RwyKepangkatansRelatedByPtkId');
        $this->addRelation('RwyKepangkatanRelatedByPtkId', 'angulex\\Model\\RwyKepangkatan', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'RwyKepangkatansRelatedByPtkId');
        $this->addRelation('RombonganBelajarRelatedByPtkId', 'angulex\\Model\\RombonganBelajar', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'RombonganBelajarsRelatedByPtkId');
        $this->addRelation('RombonganBelajarRelatedByPtkId', 'angulex\\Model\\RombonganBelajar', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'RombonganBelajarsRelatedByPtkId');
        $this->addRelation('RiwayatGajiBerkalaRelatedByPtkId', 'angulex\\Model\\RiwayatGajiBerkala', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'RiwayatGajiBerkalasRelatedByPtkId');
        $this->addRelation('RiwayatGajiBerkalaRelatedByPtkId', 'angulex\\Model\\RiwayatGajiBerkala', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'RiwayatGajiBerkalasRelatedByPtkId');
        $this->addRelation('PengawasTerdaftarRelatedByPtkId', 'angulex\\Model\\PengawasTerdaftar', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'PengawasTerdaftarsRelatedByPtkId');
        $this->addRelation('PengawasTerdaftarRelatedByPtkId', 'angulex\\Model\\PengawasTerdaftar', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'PengawasTerdaftarsRelatedByPtkId');
        $this->addRelation('RwyStrukturalRelatedByPtkId', 'angulex\\Model\\RwyStruktural', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'RwyStrukturalsRelatedByPtkId');
        $this->addRelation('RwyStrukturalRelatedByPtkId', 'angulex\\Model\\RwyStruktural', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'RwyStrukturalsRelatedByPtkId');
        $this->addRelation('RwyPendFormalRelatedByPtkId', 'angulex\\Model\\RwyPendFormal', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'RwyPendFormalsRelatedByPtkId');
        $this->addRelation('RwyPendFormalRelatedByPtkId', 'angulex\\Model\\RwyPendFormal', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'RwyPendFormalsRelatedByPtkId');
        $this->addRelation('AnakRelatedByPtkId', 'angulex\\Model\\Anak', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'AnaksRelatedByPtkId');
        $this->addRelation('AnakRelatedByPtkId', 'angulex\\Model\\Anak', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'AnaksRelatedByPtkId');
        $this->addRelation('DiklatRelatedByPtkId', 'angulex\\Model\\Diklat', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'DiklatsRelatedByPtkId');
        $this->addRelation('DiklatRelatedByPtkId', 'angulex\\Model\\Diklat', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'DiklatsRelatedByPtkId');
        $this->addRelation('PtkBaruRelatedByPtkId', 'angulex\\Model\\PtkBaru', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'PtkBarusRelatedByPtkId');
        $this->addRelation('PtkBaruRelatedByPtkId', 'angulex\\Model\\PtkBaru', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'PtkBarusRelatedByPtkId');
        $this->addRelation('TunjanganRelatedByPtkId', 'angulex\\Model\\Tunjangan', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'TunjangansRelatedByPtkId');
        $this->addRelation('TunjanganRelatedByPtkId', 'angulex\\Model\\Tunjangan', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'TunjangansRelatedByPtkId');
        $this->addRelation('RwySertifikasiRelatedByPtkId', 'angulex\\Model\\RwySertifikasi', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'RwySertifikasisRelatedByPtkId');
        $this->addRelation('RwySertifikasiRelatedByPtkId', 'angulex\\Model\\RwySertifikasi', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'RwySertifikasisRelatedByPtkId');
        $this->addRelation('TugasTambahanRelatedByPtkId', 'angulex\\Model\\TugasTambahan', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'TugasTambahansRelatedByPtkId');
        $this->addRelation('TugasTambahanRelatedByPtkId', 'angulex\\Model\\TugasTambahan', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'TugasTambahansRelatedByPtkId');
        $this->addRelation('RwyFungsionalRelatedByPtkId', 'angulex\\Model\\RwyFungsional', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'RwyFungsionalsRelatedByPtkId');
        $this->addRelation('RwyFungsionalRelatedByPtkId', 'angulex\\Model\\RwyFungsional', RelationMap::ONE_TO_MANY, array('ptk_id' => 'ptk_id', ), null, null, 'RwyFungsionalsRelatedByPtkId');
    } // buildRelations()

} // PtkTableMap
