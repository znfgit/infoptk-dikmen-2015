<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'registrasi_peserta_didik' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class RegistrasiPesertaDidikTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.RegistrasiPesertaDidikTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('registrasi_peserta_didik');
        $this->setPhpName('RegistrasiPesertaDidik');
        $this->setClassname('angulex\\Model\\RegistrasiPesertaDidik');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('registrasi_id', 'RegistrasiId', 'CHAR', true, 16, null);
        $this->addForeignKey('jurusan_sp_id', 'JurusanSpId', 'CHAR', 'jurusan_sp', 'jurusan_sp_id', false, 16, null);
        $this->addForeignKey('jurusan_sp_id', 'JurusanSpId', 'CHAR', 'jurusan_sp', 'jurusan_sp_id', false, 16, null);
        $this->addForeignKey('peserta_didik_id', 'PesertaDidikId', 'CHAR', 'peserta_didik', 'peserta_didik_id', true, 16, null);
        $this->addForeignKey('peserta_didik_id', 'PesertaDidikId', 'CHAR', 'peserta_didik', 'peserta_didik_id', true, 16, null);
        $this->addForeignKey('sekolah_id', 'SekolahId', 'CHAR', 'sekolah', 'sekolah_id', true, 16, null);
        $this->addForeignKey('sekolah_id', 'SekolahId', 'CHAR', 'sekolah', 'sekolah_id', true, 16, null);
        $this->addForeignKey('jenis_pendaftaran_id', 'JenisPendaftaranId', 'NUMERIC', 'ref.jenis_pendaftaran', 'jenis_pendaftaran_id', true, 3, null);
        $this->addForeignKey('jenis_pendaftaran_id', 'JenisPendaftaranId', 'NUMERIC', 'ref.jenis_pendaftaran', 'jenis_pendaftaran_id', true, 3, null);
        $this->addColumn('nipd', 'Nipd', 'VARCHAR', false, 18, null);
        $this->addColumn('tanggal_masuk_sekolah', 'TanggalMasukSekolah', 'VARCHAR', true, 20, null);
        $this->addForeignKey('jenis_keluar_id', 'JenisKeluarId', 'CHAR', 'ref.jenis_keluar', 'jenis_keluar_id', false, 1, null);
        $this->addForeignKey('jenis_keluar_id', 'JenisKeluarId', 'CHAR', 'ref.jenis_keluar', 'jenis_keluar_id', false, 1, null);
        $this->addColumn('tanggal_keluar', 'TanggalKeluar', 'VARCHAR', false, 20, null);
        $this->addColumn('keterangan', 'Keterangan', 'VARCHAR', false, 128, null);
        $this->addColumn('no_SKHUN', 'NoSkhun', 'CHAR', false, 20, null);
        $this->addColumn('a_pernah_paud', 'APernahPaud', 'NUMERIC', true, 3, 0);
        $this->addColumn('a_pernah_tk', 'APernahTk', 'NUMERIC', true, 3, 0);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('JurusanSpRelatedByJurusanSpId', 'angulex\\Model\\JurusanSp', RelationMap::MANY_TO_ONE, array('jurusan_sp_id' => 'jurusan_sp_id', ), null, null);
        $this->addRelation('JurusanSpRelatedByJurusanSpId', 'angulex\\Model\\JurusanSp', RelationMap::MANY_TO_ONE, array('jurusan_sp_id' => 'jurusan_sp_id', ), null, null);
        $this->addRelation('PesertaDidikRelatedByPesertaDidikId', 'angulex\\Model\\PesertaDidik', RelationMap::MANY_TO_ONE, array('peserta_didik_id' => 'peserta_didik_id', ), null, null);
        $this->addRelation('PesertaDidikRelatedByPesertaDidikId', 'angulex\\Model\\PesertaDidik', RelationMap::MANY_TO_ONE, array('peserta_didik_id' => 'peserta_didik_id', ), null, null);
        $this->addRelation('SekolahRelatedBySekolahId', 'angulex\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('SekolahRelatedBySekolahId', 'angulex\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('JenisKeluarRelatedByJenisKeluarId', 'angulex\\Model\\JenisKeluar', RelationMap::MANY_TO_ONE, array('jenis_keluar_id' => 'jenis_keluar_id', ), null, null);
        $this->addRelation('JenisKeluarRelatedByJenisKeluarId', 'angulex\\Model\\JenisKeluar', RelationMap::MANY_TO_ONE, array('jenis_keluar_id' => 'jenis_keluar_id', ), null, null);
        $this->addRelation('JenisPendaftaranRelatedByJenisPendaftaranId', 'angulex\\Model\\JenisPendaftaran', RelationMap::MANY_TO_ONE, array('jenis_pendaftaran_id' => 'jenis_pendaftaran_id', ), null, null);
        $this->addRelation('JenisPendaftaranRelatedByJenisPendaftaranId', 'angulex\\Model\\JenisPendaftaran', RelationMap::MANY_TO_ONE, array('jenis_pendaftaran_id' => 'jenis_pendaftaran_id', ), null, null);
    } // buildRelations()

} // RegistrasiPesertaDidikTableMap
