<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'blockgrant' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class BlockgrantTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.BlockgrantTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('blockgrant');
        $this->setPhpName('Blockgrant');
        $this->setClassname('angulex\\Model\\Blockgrant');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('blockgrant_id', 'BlockgrantId', 'CHAR', true, 16, null);
        $this->addForeignKey('sekolah_id', 'SekolahId', 'CHAR', 'sekolah', 'sekolah_id', true, 16, null);
        $this->addForeignKey('sekolah_id', 'SekolahId', 'CHAR', 'sekolah', 'sekolah_id', true, 16, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 50, null);
        $this->addColumn('tahun', 'Tahun', 'NUMERIC', true, 6, null);
        $this->addForeignKey('jenis_bantuan_id', 'JenisBantuanId', 'INTEGER', 'ref.jenis_bantuan', 'jenis_bantuan_id', true, 4, null);
        $this->addForeignKey('jenis_bantuan_id', 'JenisBantuanId', 'INTEGER', 'ref.jenis_bantuan', 'jenis_bantuan_id', true, 4, null);
        $this->addForeignKey('sumber_dana_id', 'SumberDanaId', 'NUMERIC', 'ref.sumber_dana', 'sumber_dana_id', true, 5, null);
        $this->addForeignKey('sumber_dana_id', 'SumberDanaId', 'NUMERIC', 'ref.sumber_dana', 'sumber_dana_id', true, 5, null);
        $this->addColumn('besar_bantuan', 'BesarBantuan', 'NUMERIC', true, 17, null);
        $this->addColumn('dana_pendamping', 'DanaPendamping', 'NUMERIC', true, 17, null);
        $this->addColumn('peruntukan_dana', 'PeruntukanDana', 'VARCHAR', false, 50, null);
        $this->addColumn('asal_data', 'AsalData', 'CHAR', true, 1, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SekolahRelatedBySekolahId', 'angulex\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('SekolahRelatedBySekolahId', 'angulex\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('JenisBantuanRelatedByJenisBantuanId', 'angulex\\Model\\JenisBantuan', RelationMap::MANY_TO_ONE, array('jenis_bantuan_id' => 'jenis_bantuan_id', ), null, null);
        $this->addRelation('JenisBantuanRelatedByJenisBantuanId', 'angulex\\Model\\JenisBantuan', RelationMap::MANY_TO_ONE, array('jenis_bantuan_id' => 'jenis_bantuan_id', ), null, null);
        $this->addRelation('SumberDanaRelatedBySumberDanaId', 'angulex\\Model\\SumberDana', RelationMap::MANY_TO_ONE, array('sumber_dana_id' => 'sumber_dana_id', ), null, null);
        $this->addRelation('SumberDanaRelatedBySumberDanaId', 'angulex\\Model\\SumberDana', RelationMap::MANY_TO_ONE, array('sumber_dana_id' => 'sumber_dana_id', ), null, null);
    } // buildRelations()

} // BlockgrantTableMap
