<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'dudi' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class DudiTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.DudiTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('dudi');
        $this->setPhpName('Dudi');
        $this->setClassname('angulex\\Model\\Dudi');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('dudi_id', 'DudiId', 'CHAR', true, 16, null);
        $this->addForeignKey('bidang_usaha_id', 'BidangUsahaId', 'CHAR', 'ref.bidang_usaha', 'bidang_usaha_id', true, 10, null);
        $this->addForeignKey('bidang_usaha_id', 'BidangUsahaId', 'CHAR', 'ref.bidang_usaha', 'bidang_usaha_id', true, 10, null);
        $this->addColumn('nama_dudi', 'NamaDudi', 'VARCHAR', true, 80, null);
        $this->addColumn('npwp', 'Npwp', 'CHAR', false, 15, null);
        $this->addColumn('telp_kantor', 'TelpKantor', 'VARCHAR', false, 20, null);
        $this->addColumn('fax', 'Fax', 'VARCHAR', false, 20, null);
        $this->addColumn('contact_person', 'ContactPerson', 'VARCHAR', false, 50, null);
        $this->addColumn('telp_cp', 'TelpCp', 'VARCHAR', false, 20, null);
        $this->addColumn('jabatan_cp', 'JabatanCp', 'VARCHAR', false, 40, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('BidangUsahaRelatedByBidangUsahaId', 'angulex\\Model\\BidangUsaha', RelationMap::MANY_TO_ONE, array('bidang_usaha_id' => 'bidang_usaha_id', ), null, null);
        $this->addRelation('BidangUsahaRelatedByBidangUsahaId', 'angulex\\Model\\BidangUsaha', RelationMap::MANY_TO_ONE, array('bidang_usaha_id' => 'bidang_usaha_id', ), null, null);
        $this->addRelation('MouRelatedByDudiId', 'angulex\\Model\\Mou', RelationMap::ONE_TO_MANY, array('dudi_id' => 'dudi_id', ), null, null, 'MousRelatedByDudiId');
        $this->addRelation('MouRelatedByDudiId', 'angulex\\Model\\Mou', RelationMap::ONE_TO_MANY, array('dudi_id' => 'dudi_id', ), null, null, 'MousRelatedByDudiId');
    } // buildRelations()

} // DudiTableMap
