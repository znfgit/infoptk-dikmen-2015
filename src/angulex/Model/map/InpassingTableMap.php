<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'inpassing' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class InpassingTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.InpassingTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('inpassing');
        $this->setPhpName('Inpassing');
        $this->setClassname('angulex\\Model\\Inpassing');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('inpassing_id', 'InpassingId', 'CHAR', true, 16, null);
        $this->addForeignKey('pangkat_golongan_id', 'PangkatGolonganId', 'NUMERIC', 'ref.pangkat_golongan', 'pangkat_golongan_id', true, 4, null);
        $this->addForeignKey('pangkat_golongan_id', 'PangkatGolonganId', 'NUMERIC', 'ref.pangkat_golongan', 'pangkat_golongan_id', true, 4, null);
        $this->addForeignKey('ptk_id', 'PtkId', 'CHAR', 'ptk', 'ptk_id', false, 16, null);
        $this->addForeignKey('ptk_id', 'PtkId', 'CHAR', 'ptk', 'ptk_id', false, 16, null);
        $this->addColumn('no_sk_inpassing', 'NoSkInpassing', 'VARCHAR', true, 40, null);
        $this->addColumn('tmt_inpassing', 'TmtInpassing', 'VARCHAR', true, 20, null);
        $this->addColumn('angka_kredit', 'AngkaKredit', 'NUMERIC', true, 5, 0);
        $this->addColumn('masa_kerja_tahun', 'MasaKerjaTahun', 'NUMERIC', true, 4, 0);
        $this->addColumn('masa_kerja_bulan', 'MasaKerjaBulan', 'NUMERIC', true, 4, 0);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('PtkRelatedByPtkId', 'angulex\\Model\\Ptk', RelationMap::MANY_TO_ONE, array('ptk_id' => 'ptk_id', ), null, null);
        $this->addRelation('PtkRelatedByPtkId', 'angulex\\Model\\Ptk', RelationMap::MANY_TO_ONE, array('ptk_id' => 'ptk_id', ), null, null);
        $this->addRelation('PangkatGolonganRelatedByPangkatGolonganId', 'angulex\\Model\\PangkatGolongan', RelationMap::MANY_TO_ONE, array('pangkat_golongan_id' => 'pangkat_golongan_id', ), null, null);
        $this->addRelation('PangkatGolonganRelatedByPangkatGolonganId', 'angulex\\Model\\PangkatGolongan', RelationMap::MANY_TO_ONE, array('pangkat_golongan_id' => 'pangkat_golongan_id', ), null, null);
        $this->addRelation('VldInpassingRelatedByInpassingId', 'angulex\\Model\\VldInpassing', RelationMap::ONE_TO_MANY, array('inpassing_id' => 'inpassing_id', ), null, null, 'VldInpassingsRelatedByInpassingId');
        $this->addRelation('VldInpassingRelatedByInpassingId', 'angulex\\Model\\VldInpassing', RelationMap::ONE_TO_MANY, array('inpassing_id' => 'inpassing_id', ), null, null, 'VldInpassingsRelatedByInpassingId');
    } // buildRelations()

} // InpassingTableMap
