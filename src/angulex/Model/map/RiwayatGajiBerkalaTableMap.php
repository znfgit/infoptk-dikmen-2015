<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'riwayat_gaji_berkala' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class RiwayatGajiBerkalaTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.RiwayatGajiBerkalaTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('riwayat_gaji_berkala');
        $this->setPhpName('RiwayatGajiBerkala');
        $this->setClassname('angulex\\Model\\RiwayatGajiBerkala');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('riwayat_gaji_berkala_id', 'RiwayatGajiBerkalaId', 'CHAR', true, 16, null);
        $this->addForeignKey('ptk_id', 'PtkId', 'CHAR', 'ptk', 'ptk_id', true, 16, null);
        $this->addForeignKey('ptk_id', 'PtkId', 'CHAR', 'ptk', 'ptk_id', true, 16, null);
        $this->addForeignKey('pangkat_golongan_id', 'PangkatGolonganId', 'NUMERIC', 'ref.pangkat_golongan', 'pangkat_golongan_id', true, 4, null);
        $this->addForeignKey('pangkat_golongan_id', 'PangkatGolonganId', 'NUMERIC', 'ref.pangkat_golongan', 'pangkat_golongan_id', true, 4, null);
        $this->addColumn('nomor_sk', 'NomorSk', 'VARCHAR', true, 40, null);
        $this->addColumn('tanggal_sk', 'TanggalSk', 'VARCHAR', true, 20, null);
        $this->addColumn('tmt_kgb', 'TmtKgb', 'VARCHAR', true, 20, null);
        $this->addColumn('masa_kerja_tahun', 'MasaKerjaTahun', 'NUMERIC', true, 4, null);
        $this->addColumn('masa_kerja_bulan', 'MasaKerjaBulan', 'NUMERIC', true, 4, null);
        $this->addColumn('gaji_pokok', 'GajiPokok', 'NUMERIC', false, 11, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('PtkRelatedByPtkId', 'angulex\\Model\\Ptk', RelationMap::MANY_TO_ONE, array('ptk_id' => 'ptk_id', ), null, null);
        $this->addRelation('PtkRelatedByPtkId', 'angulex\\Model\\Ptk', RelationMap::MANY_TO_ONE, array('ptk_id' => 'ptk_id', ), null, null);
        $this->addRelation('PangkatGolonganRelatedByPangkatGolonganId', 'angulex\\Model\\PangkatGolongan', RelationMap::MANY_TO_ONE, array('pangkat_golongan_id' => 'pangkat_golongan_id', ), null, null);
        $this->addRelation('PangkatGolonganRelatedByPangkatGolonganId', 'angulex\\Model\\PangkatGolongan', RelationMap::MANY_TO_ONE, array('pangkat_golongan_id' => 'pangkat_golongan_id', ), null, null);
    } // buildRelations()

} // RiwayatGajiBerkalaTableMap
