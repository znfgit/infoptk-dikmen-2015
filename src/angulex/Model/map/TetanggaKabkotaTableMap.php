<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.tetangga_kabkota' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class TetanggaKabkotaTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.TetanggaKabkotaTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.tetangga_kabkota');
        $this->setPhpName('TetanggaKabkota');
        $this->setClassname('angulex\\Model\\TetanggaKabkota');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addForeignPrimaryKey('kode_wilayah', 'KodeWilayah', 'CHAR' , 'ref.mst_wilayah', 'kode_wilayah', true, 8, null);
        $this->addForeignPrimaryKey('mst_kode_wilayah', 'MstKodeWilayah', 'CHAR' , 'ref.mst_wilayah', 'kode_wilayah', true, 8, null);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('MstWilayahRelatedByKodeWilayah', 'angulex\\Model\\MstWilayah', RelationMap::MANY_TO_ONE, array('kode_wilayah' => 'kode_wilayah', ), null, null);
        $this->addRelation('MstWilayahRelatedByMstKodeWilayah', 'angulex\\Model\\MstWilayah', RelationMap::MANY_TO_ONE, array('mst_kode_wilayah' => 'kode_wilayah', ), null, null);
    } // buildRelations()

} // TetanggaKabkotaTableMap
