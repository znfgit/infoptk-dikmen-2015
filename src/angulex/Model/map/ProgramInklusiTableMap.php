<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'program_inklusi' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class ProgramInklusiTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.ProgramInklusiTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('program_inklusi');
        $this->setPhpName('ProgramInklusi');
        $this->setClassname('angulex\\Model\\ProgramInklusi');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id_pi', 'IdPi', 'CHAR', true, 16, null);
        $this->addForeignKey('sekolah_id', 'SekolahId', 'CHAR', 'sekolah', 'sekolah_id', true, 16, null);
        $this->addForeignKey('sekolah_id', 'SekolahId', 'CHAR', 'sekolah', 'sekolah_id', true, 16, null);
        $this->addForeignKey('kebutuhan_khusus_id', 'KebutuhanKhususId', 'INTEGER', 'ref.kebutuhan_khusus', 'kebutuhan_khusus_id', true, 4, null);
        $this->addForeignKey('kebutuhan_khusus_id', 'KebutuhanKhususId', 'INTEGER', 'ref.kebutuhan_khusus', 'kebutuhan_khusus_id', true, 4, null);
        $this->addColumn('sk_pi', 'SkPi', 'VARCHAR', true, 40, null);
        $this->addColumn('tmt_pi', 'TmtPi', 'VARCHAR', true, 20, null);
        $this->addColumn('tst_pi', 'TstPi', 'VARCHAR', false, 20, null);
        $this->addColumn('ket_pi', 'KetPi', 'VARCHAR', false, 200, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SekolahRelatedBySekolahId', 'angulex\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('SekolahRelatedBySekolahId', 'angulex\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('KebutuhanKhususRelatedByKebutuhanKhususId', 'angulex\\Model\\KebutuhanKhusus', RelationMap::MANY_TO_ONE, array('kebutuhan_khusus_id' => 'kebutuhan_khusus_id', ), null, null);
        $this->addRelation('KebutuhanKhususRelatedByKebutuhanKhususId', 'angulex\\Model\\KebutuhanKhusus', RelationMap::MANY_TO_ONE, array('kebutuhan_khusus_id' => 'kebutuhan_khusus_id', ), null, null);
    } // buildRelations()

} // ProgramInklusiTableMap
