<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.group_matpel' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class GroupMatpelTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.GroupMatpelTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.group_matpel');
        $this->setPhpName('GroupMatpel');
        $this->setClassname('angulex\\Model\\GroupMatpel');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('gmp_id', 'GmpId', 'CHAR', true, 16, null);
        $this->addForeignKey('kurikulum_id', 'KurikulumId', 'SMALLINT', 'ref.kurikulum', 'kurikulum_id', true, 2, null);
        $this->addForeignKey('tingkat_pendidikan_id', 'TingkatPendidikanId', 'NUMERIC', 'ref.tingkat_pendidikan', 'tingkat_pendidikan_id', true, 4, null);
        $this->addColumn('nama_group', 'NamaGroup', 'VARCHAR', true, 40, null);
        $this->addColumn('jumlah_jam_maksimum', 'JumlahJamMaksimum', 'NUMERIC', true, 4, null);
        $this->addColumn('jumlah_sks_maksimum', 'JumlahSksMaksimum', 'NUMERIC', true, 4, 0);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Kurikulum', 'angulex\\Model\\Kurikulum', RelationMap::MANY_TO_ONE, array('kurikulum_id' => 'kurikulum_id', ), null, null);
        $this->addRelation('TingkatPendidikan', 'angulex\\Model\\TingkatPendidikan', RelationMap::MANY_TO_ONE, array('tingkat_pendidikan_id' => 'tingkat_pendidikan_id', ), null, null);
        $this->addRelation('MataPelajaranKurikulum', 'angulex\\Model\\MataPelajaranKurikulum', RelationMap::ONE_TO_MANY, array('gmp_id' => 'gmp_id', ), null, null, 'MataPelajaranKurikulums');
    } // buildRelations()

} // GroupMatpelTableMap
