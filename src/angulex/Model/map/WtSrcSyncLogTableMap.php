<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'wt_src_sync_log' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class WtSrcSyncLogTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.WtSrcSyncLogTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('wt_src_sync_log');
        $this->setPhpName('WtSrcSyncLog');
        $this->setClassname('angulex\\Model\\WtSrcSyncLog');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addForeignPrimaryKey('kode_wilayah', 'KodeWilayah', 'CHAR' , 'ref.mst_wilayah', 'kode_wilayah', true, 8, null);
        $this->addForeignPrimaryKey('kode_wilayah', 'KodeWilayah', 'CHAR' , 'ref.mst_wilayah', 'kode_wilayah', true, 8, null);
        $this->addForeignPrimaryKey('sekolah_id', 'SekolahId', 'CHAR' , 'sekolah', 'sekolah_id', true, 16, null);
        $this->addForeignPrimaryKey('sekolah_id', 'SekolahId', 'CHAR' , 'sekolah', 'sekolah_id', true, 16, null);
        $this->addForeignPrimaryKey('table_name', 'TableName', 'VARCHAR' , 'ref.table_sync', 'table_name', true, 30, null);
        $this->addForeignPrimaryKey('table_name', 'TableName', 'VARCHAR' , 'ref.table_sync', 'table_name', true, 30, null);
        $this->addColumn('begin_sync', 'BeginSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('end_sync', 'EndSync', 'TIMESTAMP', false, 16, null);
        $this->addColumn('sync_media', 'SyncMedia', 'CHAR', true, 1, '(\'1\')');
        $this->addColumn('is_success', 'IsSuccess', 'NUMERIC', true, 3, null);
        $this->addColumn('n_create', 'NCreate', 'INTEGER', true, 4, null);
        $this->addColumn('n_update', 'NUpdate', 'INTEGER', true, 4, null);
        $this->addColumn('n_hapus', 'NHapus', 'INTEGER', true, 4, null);
        $this->addColumn('n_konflik', 'NKonflik', 'INTEGER', true, 4, null);
        $this->addColumn('selisih_waktu_server', 'SelisihWaktuServer', 'BIGINT', true, 8, null);
        $this->addColumn('alamat_ip', 'AlamatIp', 'VARCHAR', true, 50, null);
        $this->addColumn('pengguna_id', 'PenggunaId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SekolahRelatedBySekolahId', 'angulex\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('SekolahRelatedBySekolahId', 'angulex\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('MstWilayahRelatedByKodeWilayah', 'angulex\\Model\\MstWilayah', RelationMap::MANY_TO_ONE, array('kode_wilayah' => 'kode_wilayah', ), null, null);
        $this->addRelation('MstWilayahRelatedByKodeWilayah', 'angulex\\Model\\MstWilayah', RelationMap::MANY_TO_ONE, array('kode_wilayah' => 'kode_wilayah', ), null, null);
        $this->addRelation('TableSyncRelatedByTableName', 'angulex\\Model\\TableSync', RelationMap::MANY_TO_ONE, array('table_name' => 'table_name', ), null, null);
        $this->addRelation('TableSyncRelatedByTableName', 'angulex\\Model\\TableSync', RelationMap::MANY_TO_ONE, array('table_name' => 'table_name', ), null, null);
    } // buildRelations()

} // WtSrcSyncLogTableMap
