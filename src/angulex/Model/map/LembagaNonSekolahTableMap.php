<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'lembaga_non_sekolah' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class LembagaNonSekolahTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.LembagaNonSekolahTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('lembaga_non_sekolah');
        $this->setPhpName('LembagaNonSekolah');
        $this->setClassname('angulex\\Model\\LembagaNonSekolah');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('lembaga_id', 'LembagaId', 'CHAR', true, 16, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 80, null);
        $this->addColumn('singkatan', 'Singkatan', 'VARCHAR', false, 15, null);
        $this->addForeignKey('jenis_lembaga_id', 'JenisLembagaId', 'NUMERIC', 'ref.jenis_lembaga', 'jenis_lembaga_id', true, 7, null);
        $this->addForeignKey('jenis_lembaga_id', 'JenisLembagaId', 'NUMERIC', 'ref.jenis_lembaga', 'jenis_lembaga_id', true, 7, null);
        $this->addColumn('alamat_jalan', 'AlamatJalan', 'VARCHAR', true, 80, null);
        $this->addColumn('rt', 'Rt', 'NUMERIC', false, 4, null);
        $this->addColumn('rw', 'Rw', 'NUMERIC', false, 4, null);
        $this->addColumn('nama_dusun', 'NamaDusun', 'VARCHAR', false, 40, null);
        $this->addColumn('desa_kelurahan', 'DesaKelurahan', 'VARCHAR', true, 40, null);
        $this->addForeignKey('kode_wilayah', 'KodeWilayah', 'CHAR', 'ref.mst_wilayah', 'kode_wilayah', true, 8, null);
        $this->addForeignKey('kode_wilayah', 'KodeWilayah', 'CHAR', 'ref.mst_wilayah', 'kode_wilayah', true, 8, null);
        $this->addColumn('kode_pos', 'KodePos', 'CHAR', false, 5, null);
        $this->addColumn('lintang', 'Lintang', 'NUMERIC', false, 12, null);
        $this->addColumn('bujur', 'Bujur', 'NUMERIC', false, 12, null);
        $this->addColumn('nomor_telepon', 'NomorTelepon', 'VARCHAR', false, 20, null);
        $this->addColumn('nomor_fax', 'NomorFax', 'VARCHAR', false, 20, null);
        $this->addColumn('email', 'Email', 'VARCHAR', false, 50, null);
        $this->addColumn('website', 'Website', 'VARCHAR', false, 100, null);
        $this->addForeignKey('pengguna_id', 'PenggunaId', 'CHAR', 'pengguna', 'pengguna_id', false, 16, null);
        $this->addForeignKey('pengguna_id', 'PenggunaId', 'CHAR', 'pengguna', 'pengguna_id', false, 16, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('PenggunaRelatedByPenggunaId', 'angulex\\Model\\Pengguna', RelationMap::MANY_TO_ONE, array('pengguna_id' => 'pengguna_id', ), null, null);
        $this->addRelation('PenggunaRelatedByPenggunaId', 'angulex\\Model\\Pengguna', RelationMap::MANY_TO_ONE, array('pengguna_id' => 'pengguna_id', ), null, null);
        $this->addRelation('JenisLembagaRelatedByJenisLembagaId', 'angulex\\Model\\JenisLembaga', RelationMap::MANY_TO_ONE, array('jenis_lembaga_id' => 'jenis_lembaga_id', ), null, null);
        $this->addRelation('JenisLembagaRelatedByJenisLembagaId', 'angulex\\Model\\JenisLembaga', RelationMap::MANY_TO_ONE, array('jenis_lembaga_id' => 'jenis_lembaga_id', ), null, null);
        $this->addRelation('MstWilayahRelatedByKodeWilayah', 'angulex\\Model\\MstWilayah', RelationMap::MANY_TO_ONE, array('kode_wilayah' => 'kode_wilayah', ), null, null);
        $this->addRelation('MstWilayahRelatedByKodeWilayah', 'angulex\\Model\\MstWilayah', RelationMap::MANY_TO_ONE, array('kode_wilayah' => 'kode_wilayah', ), null, null);
        $this->addRelation('PenggunaRelatedByLembagaId', 'angulex\\Model\\Pengguna', RelationMap::ONE_TO_MANY, array('lembaga_id' => 'lembaga_id', ), null, null, 'PenggunasRelatedByLembagaId');
        $this->addRelation('PenggunaRelatedByLembagaId', 'angulex\\Model\\Pengguna', RelationMap::ONE_TO_MANY, array('lembaga_id' => 'lembaga_id', ), null, null, 'PenggunasRelatedByLembagaId');
        $this->addRelation('VldNonsekolahRelatedByLembagaId', 'angulex\\Model\\VldNonsekolah', RelationMap::ONE_TO_MANY, array('lembaga_id' => 'lembaga_id', ), null, null, 'VldNonsekolahsRelatedByLembagaId');
        $this->addRelation('VldNonsekolahRelatedByLembagaId', 'angulex\\Model\\VldNonsekolah', RelationMap::ONE_TO_MANY, array('lembaga_id' => 'lembaga_id', ), null, null, 'VldNonsekolahsRelatedByLembagaId');
        $this->addRelation('PengawasTerdaftarRelatedByLembagaId', 'angulex\\Model\\PengawasTerdaftar', RelationMap::ONE_TO_MANY, array('lembaga_id' => 'lembaga_id', ), null, null, 'PengawasTerdaftarsRelatedByLembagaId');
        $this->addRelation('PengawasTerdaftarRelatedByLembagaId', 'angulex\\Model\\PengawasTerdaftar', RelationMap::ONE_TO_MANY, array('lembaga_id' => 'lembaga_id', ), null, null, 'PengawasTerdaftarsRelatedByLembagaId');
    } // buildRelations()

} // LembagaNonSekolahTableMap
