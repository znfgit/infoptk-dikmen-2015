<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.jenis_sertifikasi' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class JenisSertifikasiTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.JenisSertifikasiTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.jenis_sertifikasi');
        $this->setPhpName('JenisSertifikasi');
        $this->setClassname('angulex\\Model\\JenisSertifikasi');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id_jenis_sertifikasi', 'IdJenisSertifikasi', 'NUMERIC', true, 5, null);
        $this->addColumn('jenis_sertifikasi', 'JenisSertifikasi', 'VARCHAR', true, 30, null);
        $this->addColumn('prof_guru', 'ProfGuru', 'NUMERIC', true, 3, null);
        $this->addColumn('kepala_sekolah', 'KepalaSekolah', 'NUMERIC', true, 3, null);
        $this->addColumn('laboran', 'Laboran', 'NUMERIC', true, 3, null);
        $this->addForeignKey('kebutuhan_khusus_id', 'KebutuhanKhususId', 'INTEGER', 'ref.kebutuhan_khusus', 'kebutuhan_khusus_id', true, 4, null);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('KebutuhanKhusus', 'angulex\\Model\\KebutuhanKhusus', RelationMap::MANY_TO_ONE, array('kebutuhan_khusus_id' => 'kebutuhan_khusus_id', ), null, null);
        $this->addRelation('RwySertifikasiRelatedByIdJenisSertifikasi', 'angulex\\Model\\RwySertifikasi', RelationMap::ONE_TO_MANY, array('id_jenis_sertifikasi' => 'id_jenis_sertifikasi', ), null, null, 'RwySertifikasisRelatedByIdJenisSertifikasi');
        $this->addRelation('RwySertifikasiRelatedByIdJenisSertifikasi', 'angulex\\Model\\RwySertifikasi', RelationMap::ONE_TO_MANY, array('id_jenis_sertifikasi' => 'id_jenis_sertifikasi', ), null, null, 'RwySertifikasisRelatedByIdJenisSertifikasi');
    } // buildRelations()

} // JenisSertifikasiTableMap
