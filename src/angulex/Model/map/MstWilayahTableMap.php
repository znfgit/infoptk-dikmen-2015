<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.mst_wilayah' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class MstWilayahTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.MstWilayahTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.mst_wilayah');
        $this->setPhpName('MstWilayah');
        $this->setClassname('angulex\\Model\\MstWilayah');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('kode_wilayah', 'KodeWilayah', 'CHAR', true, 8, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 40, null);
        $this->addForeignKey('id_level_wilayah', 'IdLevelWilayah', 'SMALLINT', 'ref.level_wilayah', 'id_level_wilayah', true, 2, null);
        $this->addForeignKey('mst_kode_wilayah', 'MstKodeWilayah', 'CHAR', 'ref.mst_wilayah', 'kode_wilayah', false, 8, null);
        $this->addForeignKey('negara_id', 'NegaraId', 'CHAR', 'ref.negara', 'negara_id', true, 2, null);
        $this->addColumn('asal_wilayah', 'AsalWilayah', 'CHAR', false, 8, null);
        $this->addColumn('kode_bps', 'KodeBps', 'CHAR', false, 7, null);
        $this->addColumn('kode_dagri', 'KodeDagri', 'CHAR', false, 7, null);
        $this->addColumn('kode_keu', 'KodeKeu', 'VARCHAR', false, 10, null);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('LevelWilayah', 'angulex\\Model\\LevelWilayah', RelationMap::MANY_TO_ONE, array('id_level_wilayah' => 'id_level_wilayah', ), null, null);
        $this->addRelation('MstWilayahRelatedByMstKodeWilayah', 'angulex\\Model\\MstWilayah', RelationMap::MANY_TO_ONE, array('mst_kode_wilayah' => 'kode_wilayah', ), null, null);
        $this->addRelation('Negara', 'angulex\\Model\\Negara', RelationMap::MANY_TO_ONE, array('negara_id' => 'negara_id', ), null, null);
        $this->addRelation('TetanggaKabkotaRelatedByKodeWilayah', 'angulex\\Model\\TetanggaKabkota', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'kode_wilayah', ), null, null, 'TetanggaKabkotasRelatedByKodeWilayah');
        $this->addRelation('TetanggaKabkotaRelatedByMstKodeWilayah', 'angulex\\Model\\TetanggaKabkota', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'mst_kode_wilayah', ), null, null, 'TetanggaKabkotasRelatedByMstKodeWilayah');
        $this->addRelation('PesertaDidikRelatedByKodeWilayah', 'angulex\\Model\\PesertaDidik', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'kode_wilayah', ), null, null, 'PesertaDidiksRelatedByKodeWilayah');
        $this->addRelation('PesertaDidikRelatedByKodeWilayah', 'angulex\\Model\\PesertaDidik', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'kode_wilayah', ), null, null, 'PesertaDidiksRelatedByKodeWilayah');
        $this->addRelation('MstWilayahRelatedByKodeWilayah', 'angulex\\Model\\MstWilayah', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'mst_kode_wilayah', ), null, null, 'MstWilayahsRelatedByKodeWilayah');
        $this->addRelation('PenggunaRelatedByKodeWilayah', 'angulex\\Model\\Pengguna', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'kode_wilayah', ), null, null, 'PenggunasRelatedByKodeWilayah');
        $this->addRelation('PenggunaRelatedByKodeWilayah', 'angulex\\Model\\Pengguna', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'kode_wilayah', ), null, null, 'PenggunasRelatedByKodeWilayah');
        $this->addRelation('SekolahRelatedByKodeWilayah', 'angulex\\Model\\Sekolah', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'kode_wilayah', ), null, null, 'SekolahsRelatedByKodeWilayah');
        $this->addRelation('SekolahRelatedByKodeWilayah', 'angulex\\Model\\Sekolah', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'kode_wilayah', ), null, null, 'SekolahsRelatedByKodeWilayah');
        $this->addRelation('YayasanRelatedByKodeWilayah', 'angulex\\Model\\Yayasan', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'kode_wilayah', ), null, null, 'YayasansRelatedByKodeWilayah');
        $this->addRelation('YayasanRelatedByKodeWilayah', 'angulex\\Model\\Yayasan', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'kode_wilayah', ), null, null, 'YayasansRelatedByKodeWilayah');
        $this->addRelation('PtkRelatedByKodeWilayah', 'angulex\\Model\\Ptk', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'kode_wilayah', ), null, null, 'PtksRelatedByKodeWilayah');
        $this->addRelation('PtkRelatedByKodeWilayah', 'angulex\\Model\\Ptk', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'kode_wilayah', ), null, null, 'PtksRelatedByKodeWilayah');
        $this->addRelation('WtSrcSyncLogRelatedByKodeWilayah', 'angulex\\Model\\WtSrcSyncLog', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'kode_wilayah', ), null, null, 'WtSrcSyncLogsRelatedByKodeWilayah');
        $this->addRelation('WtSrcSyncLogRelatedByKodeWilayah', 'angulex\\Model\\WtSrcSyncLog', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'kode_wilayah', ), null, null, 'WtSrcSyncLogsRelatedByKodeWilayah');
        $this->addRelation('WtDstSyncLogRelatedByKodeWilayah', 'angulex\\Model\\WtDstSyncLog', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'kode_wilayah', ), null, null, 'WtDstSyncLogsRelatedByKodeWilayah');
        $this->addRelation('WtDstSyncLogRelatedByKodeWilayah', 'angulex\\Model\\WtDstSyncLog', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'kode_wilayah', ), null, null, 'WtDstSyncLogsRelatedByKodeWilayah');
        $this->addRelation('WsyncSessionRelatedByKodeWilayah', 'angulex\\Model\\WsyncSession', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'kode_wilayah', ), null, null, 'WsyncSessionsRelatedByKodeWilayah');
        $this->addRelation('WsyncSessionRelatedByKodeWilayah', 'angulex\\Model\\WsyncSession', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'kode_wilayah', ), null, null, 'WsyncSessionsRelatedByKodeWilayah');
        $this->addRelation('WsrcSyncLogRelatedByKodeWilayah', 'angulex\\Model\\WsrcSyncLog', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'kode_wilayah', ), null, null, 'WsrcSyncLogsRelatedByKodeWilayah');
        $this->addRelation('WsrcSyncLogRelatedByKodeWilayah', 'angulex\\Model\\WsrcSyncLog', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'kode_wilayah', ), null, null, 'WsrcSyncLogsRelatedByKodeWilayah');
        $this->addRelation('WdstSyncLogRelatedByKodeWilayah', 'angulex\\Model\\WdstSyncLog', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'kode_wilayah', ), null, null, 'WdstSyncLogsRelatedByKodeWilayah');
        $this->addRelation('WdstSyncLogRelatedByKodeWilayah', 'angulex\\Model\\WdstSyncLog', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'kode_wilayah', ), null, null, 'WdstSyncLogsRelatedByKodeWilayah');
        $this->addRelation('WPendingJobRelatedByKodeWilayah', 'angulex\\Model\\WPendingJob', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'kode_wilayah', ), null, null, 'WPendingJobsRelatedByKodeWilayah');
        $this->addRelation('WPendingJobRelatedByKodeWilayah', 'angulex\\Model\\WPendingJob', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'kode_wilayah', ), null, null, 'WPendingJobsRelatedByKodeWilayah');
        $this->addRelation('LembagaNonSekolahRelatedByKodeWilayah', 'angulex\\Model\\LembagaNonSekolah', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'kode_wilayah', ), null, null, 'LembagaNonSekolahsRelatedByKodeWilayah');
        $this->addRelation('LembagaNonSekolahRelatedByKodeWilayah', 'angulex\\Model\\LembagaNonSekolah', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'kode_wilayah', ), null, null, 'LembagaNonSekolahsRelatedByKodeWilayah');
        $this->addRelation('DemografiRelatedByKodeWilayah', 'angulex\\Model\\Demografi', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'kode_wilayah', ), null, null, 'DemografisRelatedByKodeWilayah');
        $this->addRelation('DemografiRelatedByKodeWilayah', 'angulex\\Model\\Demografi', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'kode_wilayah', ), null, null, 'DemografisRelatedByKodeWilayah');
    } // buildRelations()

} // MstWilayahTableMap
