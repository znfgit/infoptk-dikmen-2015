<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ptk_terdaftar' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class PtkTerdaftarTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.PtkTerdaftarTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ptk_terdaftar');
        $this->setPhpName('PtkTerdaftar');
        $this->setClassname('angulex\\Model\\PtkTerdaftar');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('ptk_terdaftar_id', 'PtkTerdaftarId', 'CHAR', true, 16, null);
        $this->addForeignKey('ptk_id', 'PtkId', 'CHAR', 'ptk', 'ptk_id', true, 16, null);
        $this->addForeignKey('ptk_id', 'PtkId', 'CHAR', 'ptk', 'ptk_id', true, 16, null);
        $this->addForeignKey('sekolah_id', 'SekolahId', 'CHAR', 'sekolah', 'sekolah_id', true, 16, null);
        $this->addForeignKey('sekolah_id', 'SekolahId', 'CHAR', 'sekolah', 'sekolah_id', true, 16, null);
        $this->addForeignKey('tahun_ajaran_id', 'TahunAjaranId', 'NUMERIC', 'ref.tahun_ajaran', 'tahun_ajaran_id', true, 6, null);
        $this->addForeignKey('tahun_ajaran_id', 'TahunAjaranId', 'NUMERIC', 'ref.tahun_ajaran', 'tahun_ajaran_id', true, 6, null);
        $this->addColumn('nomor_surat_tugas', 'NomorSuratTugas', 'VARCHAR', true, 40, null);
        $this->addColumn('tanggal_surat_tugas', 'TanggalSuratTugas', 'VARCHAR', true, 20, null);
        $this->addColumn('tmt_tugas', 'TmtTugas', 'VARCHAR', true, 20, null);
        $this->addColumn('ptk_induk', 'PtkInduk', 'NUMERIC', true, 3, null);
        $this->addColumn('aktif_bulan_01', 'AktifBulan01', 'NUMERIC', true, 3, 0);
        $this->addColumn('aktif_bulan_02', 'AktifBulan02', 'NUMERIC', true, 3, 0);
        $this->addColumn('aktif_bulan_03', 'AktifBulan03', 'NUMERIC', true, 3, 0);
        $this->addColumn('aktif_bulan_04', 'AktifBulan04', 'NUMERIC', true, 3, 0);
        $this->addColumn('aktif_bulan_05', 'AktifBulan05', 'NUMERIC', true, 3, 0);
        $this->addColumn('aktif_bulan_06', 'AktifBulan06', 'NUMERIC', true, 3, 0);
        $this->addColumn('aktif_bulan_07', 'AktifBulan07', 'NUMERIC', true, 3, 0);
        $this->addColumn('aktif_bulan_08', 'AktifBulan08', 'NUMERIC', true, 3, 0);
        $this->addColumn('aktif_bulan_09', 'AktifBulan09', 'NUMERIC', true, 3, 0);
        $this->addColumn('aktif_bulan_10', 'AktifBulan10', 'NUMERIC', true, 3, 0);
        $this->addColumn('aktif_bulan_11', 'AktifBulan11', 'NUMERIC', true, 3, 0);
        $this->addColumn('aktif_bulan_12', 'AktifBulan12', 'NUMERIC', true, 3, 0);
        $this->addForeignKey('jenis_keluar_id', 'JenisKeluarId', 'CHAR', 'ref.jenis_keluar', 'jenis_keluar_id', false, 1, null);
        $this->addForeignKey('jenis_keluar_id', 'JenisKeluarId', 'CHAR', 'ref.jenis_keluar', 'jenis_keluar_id', false, 1, null);
        $this->addColumn('tgl_ptk_keluar', 'TglPtkKeluar', 'VARCHAR', false, 20, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('PtkRelatedByPtkId', 'angulex\\Model\\Ptk', RelationMap::MANY_TO_ONE, array('ptk_id' => 'ptk_id', ), null, null);
        $this->addRelation('PtkRelatedByPtkId', 'angulex\\Model\\Ptk', RelationMap::MANY_TO_ONE, array('ptk_id' => 'ptk_id', ), null, null);
        $this->addRelation('SekolahRelatedBySekolahId', 'angulex\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('SekolahRelatedBySekolahId', 'angulex\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('JenisKeluarRelatedByJenisKeluarId', 'angulex\\Model\\JenisKeluar', RelationMap::MANY_TO_ONE, array('jenis_keluar_id' => 'jenis_keluar_id', ), null, null);
        $this->addRelation('JenisKeluarRelatedByJenisKeluarId', 'angulex\\Model\\JenisKeluar', RelationMap::MANY_TO_ONE, array('jenis_keluar_id' => 'jenis_keluar_id', ), null, null);
        $this->addRelation('TahunAjaranRelatedByTahunAjaranId', 'angulex\\Model\\TahunAjaran', RelationMap::MANY_TO_ONE, array('tahun_ajaran_id' => 'tahun_ajaran_id', ), null, null);
        $this->addRelation('TahunAjaranRelatedByTahunAjaranId', 'angulex\\Model\\TahunAjaran', RelationMap::MANY_TO_ONE, array('tahun_ajaran_id' => 'tahun_ajaran_id', ), null, null);
        $this->addRelation('PembelajaranRelatedByPtkTerdaftarId', 'angulex\\Model\\Pembelajaran', RelationMap::ONE_TO_MANY, array('ptk_terdaftar_id' => 'ptk_terdaftar_id', ), null, null, 'PembelajaransRelatedByPtkTerdaftarId');
        $this->addRelation('PembelajaranRelatedByPtkTerdaftarId', 'angulex\\Model\\Pembelajaran', RelationMap::ONE_TO_MANY, array('ptk_terdaftar_id' => 'ptk_terdaftar_id', ), null, null, 'PembelajaransRelatedByPtkTerdaftarId');
    } // buildRelations()

} // PtkTerdaftarTableMap
