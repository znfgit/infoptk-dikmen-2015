<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'rombongan_belajar' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class RombonganBelajarTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.RombonganBelajarTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('rombongan_belajar');
        $this->setPhpName('RombonganBelajar');
        $this->setClassname('angulex\\Model\\RombonganBelajar');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('rombongan_belajar_id', 'RombonganBelajarId', 'CHAR', true, 16, null);
        $this->addForeignKey('semester_id', 'SemesterId', 'CHAR', 'ref.semester', 'semester_id', true, 5, null);
        $this->addForeignKey('semester_id', 'SemesterId', 'CHAR', 'ref.semester', 'semester_id', true, 5, null);
        $this->addForeignKey('sekolah_id', 'SekolahId', 'CHAR', 'sekolah', 'sekolah_id', true, 16, null);
        $this->addForeignKey('sekolah_id', 'SekolahId', 'CHAR', 'sekolah', 'sekolah_id', true, 16, null);
        $this->addForeignKey('tingkat_pendidikan_id', 'TingkatPendidikanId', 'NUMERIC', 'ref.tingkat_pendidikan', 'tingkat_pendidikan_id', true, 4, null);
        $this->addForeignKey('tingkat_pendidikan_id', 'TingkatPendidikanId', 'NUMERIC', 'ref.tingkat_pendidikan', 'tingkat_pendidikan_id', true, 4, null);
        $this->addForeignKey('jurusan_sp_id', 'JurusanSpId', 'CHAR', 'jurusan_sp', 'jurusan_sp_id', false, 16, null);
        $this->addForeignKey('jurusan_sp_id', 'JurusanSpId', 'CHAR', 'jurusan_sp', 'jurusan_sp_id', false, 16, null);
        $this->addForeignKey('kurikulum_id', 'KurikulumId', 'SMALLINT', 'ref.kurikulum', 'kurikulum_id', true, 2, null);
        $this->addForeignKey('kurikulum_id', 'KurikulumId', 'SMALLINT', 'ref.kurikulum', 'kurikulum_id', true, 2, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 12, null);
        $this->addForeignKey('ptk_id', 'PtkId', 'CHAR', 'ptk', 'ptk_id', true, 16, null);
        $this->addForeignKey('ptk_id', 'PtkId', 'CHAR', 'ptk', 'ptk_id', true, 16, null);
        $this->addForeignKey('prasarana_id', 'PrasaranaId', 'CHAR', 'prasarana', 'prasarana_id', true, 16, null);
        $this->addForeignKey('prasarana_id', 'PrasaranaId', 'CHAR', 'prasarana', 'prasarana_id', true, 16, null);
        $this->addColumn('moving_class', 'MovingClass', 'NUMERIC', true, 3, null);
        $this->addColumn('jenis_rombel', 'JenisRombel', 'NUMERIC', true, 3, 0);
        $this->addColumn('sks', 'Sks', 'NUMERIC', true, 4, 0);
        $this->addForeignKey('kebutuhan_khusus_id', 'KebutuhanKhususId', 'INTEGER', 'ref.kebutuhan_khusus', 'kebutuhan_khusus_id', true, 4, null);
        $this->addForeignKey('kebutuhan_khusus_id', 'KebutuhanKhususId', 'INTEGER', 'ref.kebutuhan_khusus', 'kebutuhan_khusus_id', true, 4, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('JurusanSpRelatedByJurusanSpId', 'angulex\\Model\\JurusanSp', RelationMap::MANY_TO_ONE, array('jurusan_sp_id' => 'jurusan_sp_id', ), null, null);
        $this->addRelation('JurusanSpRelatedByJurusanSpId', 'angulex\\Model\\JurusanSp', RelationMap::MANY_TO_ONE, array('jurusan_sp_id' => 'jurusan_sp_id', ), null, null);
        $this->addRelation('PrasaranaRelatedByPrasaranaId', 'angulex\\Model\\Prasarana', RelationMap::MANY_TO_ONE, array('prasarana_id' => 'prasarana_id', ), null, null);
        $this->addRelation('PrasaranaRelatedByPrasaranaId', 'angulex\\Model\\Prasarana', RelationMap::MANY_TO_ONE, array('prasarana_id' => 'prasarana_id', ), null, null);
        $this->addRelation('PtkRelatedByPtkId', 'angulex\\Model\\Ptk', RelationMap::MANY_TO_ONE, array('ptk_id' => 'ptk_id', ), null, null);
        $this->addRelation('PtkRelatedByPtkId', 'angulex\\Model\\Ptk', RelationMap::MANY_TO_ONE, array('ptk_id' => 'ptk_id', ), null, null);
        $this->addRelation('SekolahRelatedBySekolahId', 'angulex\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('SekolahRelatedBySekolahId', 'angulex\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('KebutuhanKhususRelatedByKebutuhanKhususId', 'angulex\\Model\\KebutuhanKhusus', RelationMap::MANY_TO_ONE, array('kebutuhan_khusus_id' => 'kebutuhan_khusus_id', ), null, null);
        $this->addRelation('KebutuhanKhususRelatedByKebutuhanKhususId', 'angulex\\Model\\KebutuhanKhusus', RelationMap::MANY_TO_ONE, array('kebutuhan_khusus_id' => 'kebutuhan_khusus_id', ), null, null);
        $this->addRelation('KurikulumRelatedByKurikulumId', 'angulex\\Model\\Kurikulum', RelationMap::MANY_TO_ONE, array('kurikulum_id' => 'kurikulum_id', ), null, null);
        $this->addRelation('KurikulumRelatedByKurikulumId', 'angulex\\Model\\Kurikulum', RelationMap::MANY_TO_ONE, array('kurikulum_id' => 'kurikulum_id', ), null, null);
        $this->addRelation('SemesterRelatedBySemesterId', 'angulex\\Model\\Semester', RelationMap::MANY_TO_ONE, array('semester_id' => 'semester_id', ), null, null);
        $this->addRelation('SemesterRelatedBySemesterId', 'angulex\\Model\\Semester', RelationMap::MANY_TO_ONE, array('semester_id' => 'semester_id', ), null, null);
        $this->addRelation('TingkatPendidikanRelatedByTingkatPendidikanId', 'angulex\\Model\\TingkatPendidikan', RelationMap::MANY_TO_ONE, array('tingkat_pendidikan_id' => 'tingkat_pendidikan_id', ), null, null);
        $this->addRelation('TingkatPendidikanRelatedByTingkatPendidikanId', 'angulex\\Model\\TingkatPendidikan', RelationMap::MANY_TO_ONE, array('tingkat_pendidikan_id' => 'tingkat_pendidikan_id', ), null, null);
        $this->addRelation('VldRombelRelatedByRombonganBelajarId', 'angulex\\Model\\VldRombel', RelationMap::ONE_TO_MANY, array('rombongan_belajar_id' => 'rombongan_belajar_id', ), null, null, 'VldRombelsRelatedByRombonganBelajarId');
        $this->addRelation('VldRombelRelatedByRombonganBelajarId', 'angulex\\Model\\VldRombel', RelationMap::ONE_TO_MANY, array('rombongan_belajar_id' => 'rombongan_belajar_id', ), null, null, 'VldRombelsRelatedByRombonganBelajarId');
        $this->addRelation('AnggotaRombelRelatedByRombonganBelajarId', 'angulex\\Model\\AnggotaRombel', RelationMap::ONE_TO_MANY, array('rombongan_belajar_id' => 'rombongan_belajar_id', ), null, null, 'AnggotaRombelsRelatedByRombonganBelajarId');
        $this->addRelation('AnggotaRombelRelatedByRombonganBelajarId', 'angulex\\Model\\AnggotaRombel', RelationMap::ONE_TO_MANY, array('rombongan_belajar_id' => 'rombongan_belajar_id', ), null, null, 'AnggotaRombelsRelatedByRombonganBelajarId');
        $this->addRelation('PembelajaranRelatedByRombonganBelajarId', 'angulex\\Model\\Pembelajaran', RelationMap::ONE_TO_MANY, array('rombongan_belajar_id' => 'rombongan_belajar_id', ), null, null, 'PembelajaransRelatedByRombonganBelajarId');
        $this->addRelation('PembelajaranRelatedByRombonganBelajarId', 'angulex\\Model\\Pembelajaran', RelationMap::ONE_TO_MANY, array('rombongan_belajar_id' => 'rombongan_belajar_id', ), null, null, 'PembelajaransRelatedByRombonganBelajarId');
    } // buildRelations()

} // RombonganBelajarTableMap
