<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'mou' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class MouTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.MouTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('mou');
        $this->setPhpName('Mou');
        $this->setClassname('angulex\\Model\\Mou');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('mou_id', 'MouId', 'CHAR', true, 16, null);
        $this->addForeignKey('dudi_id', 'DudiId', 'CHAR', 'dudi', 'dudi_id', false, 16, null);
        $this->addForeignKey('dudi_id', 'DudiId', 'CHAR', 'dudi', 'dudi_id', false, 16, null);
        $this->addForeignKey('sekolah_id', 'SekolahId', 'CHAR', 'sekolah', 'sekolah_id', true, 16, null);
        $this->addForeignKey('sekolah_id', 'SekolahId', 'CHAR', 'sekolah', 'sekolah_id', true, 16, null);
        $this->addColumn('nomor_mou', 'NomorMou', 'VARCHAR', true, 40, null);
        $this->addColumn('judul_mou', 'JudulMou', 'VARCHAR', true, 80, null);
        $this->addColumn('tanggal_mulai', 'TanggalMulai', 'VARCHAR', true, 20, null);
        $this->addColumn('tanggal_selesai', 'TanggalSelesai', 'VARCHAR', true, 20, null);
        $this->addColumn('nama_dudi', 'NamaDudi', 'VARCHAR', true, 80, null);
        $this->addColumn('npwp_dudi', 'NpwpDudi', 'CHAR', false, 15, null);
        $this->addColumn('nama_bidang_usaha', 'NamaBidangUsaha', 'VARCHAR', true, 40, null);
        $this->addColumn('telp_kantor', 'TelpKantor', 'VARCHAR', false, 20, null);
        $this->addColumn('fax', 'Fax', 'VARCHAR', false, 20, null);
        $this->addColumn('contact_person', 'ContactPerson', 'VARCHAR', false, 50, null);
        $this->addColumn('telp_cp', 'TelpCp', 'VARCHAR', false, 20, null);
        $this->addColumn('jabatan_cp', 'JabatanCp', 'VARCHAR', false, 40, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('DudiRelatedByDudiId', 'angulex\\Model\\Dudi', RelationMap::MANY_TO_ONE, array('dudi_id' => 'dudi_id', ), null, null);
        $this->addRelation('DudiRelatedByDudiId', 'angulex\\Model\\Dudi', RelationMap::MANY_TO_ONE, array('dudi_id' => 'dudi_id', ), null, null);
        $this->addRelation('SekolahRelatedBySekolahId', 'angulex\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('SekolahRelatedBySekolahId', 'angulex\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('VldMouRelatedByMouId', 'angulex\\Model\\VldMou', RelationMap::ONE_TO_MANY, array('mou_id' => 'mou_id', ), null, null, 'VldMousRelatedByMouId');
        $this->addRelation('VldMouRelatedByMouId', 'angulex\\Model\\VldMou', RelationMap::ONE_TO_MANY, array('mou_id' => 'mou_id', ), null, null, 'VldMousRelatedByMouId');
        $this->addRelation('JurusanKerjasamaRelatedByMouId', 'angulex\\Model\\JurusanKerjasama', RelationMap::ONE_TO_MANY, array('mou_id' => 'mou_id', ), null, null, 'JurusanKerjasamasRelatedByMouId');
        $this->addRelation('JurusanKerjasamaRelatedByMouId', 'angulex\\Model\\JurusanKerjasama', RelationMap::ONE_TO_MANY, array('mou_id' => 'mou_id', ), null, null, 'JurusanKerjasamasRelatedByMouId');
        $this->addRelation('UnitUsahaKerjasamaRelatedByMouId', 'angulex\\Model\\UnitUsahaKerjasama', RelationMap::ONE_TO_MANY, array('mou_id' => 'mou_id', ), null, null, 'UnitUsahaKerjasamasRelatedByMouId');
        $this->addRelation('UnitUsahaKerjasamaRelatedByMouId', 'angulex\\Model\\UnitUsahaKerjasama', RelationMap::ONE_TO_MANY, array('mou_id' => 'mou_id', ), null, null, 'UnitUsahaKerjasamasRelatedByMouId');
    } // buildRelations()

} // MouTableMap
