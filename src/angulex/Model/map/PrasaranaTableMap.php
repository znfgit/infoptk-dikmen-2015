<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'prasarana' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class PrasaranaTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.PrasaranaTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('prasarana');
        $this->setPhpName('Prasarana');
        $this->setClassname('angulex\\Model\\Prasarana');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('prasarana_id', 'PrasaranaId', 'CHAR', true, 16, null);
        $this->addForeignKey('jenis_prasarana_id', 'JenisPrasaranaId', 'INTEGER', 'ref.jenis_prasarana', 'jenis_prasarana_id', true, 4, null);
        $this->addForeignKey('jenis_prasarana_id', 'JenisPrasaranaId', 'INTEGER', 'ref.jenis_prasarana', 'jenis_prasarana_id', true, 4, null);
        $this->addForeignKey('sekolah_id', 'SekolahId', 'CHAR', 'sekolah', 'sekolah_id', true, 16, null);
        $this->addForeignKey('sekolah_id', 'SekolahId', 'CHAR', 'sekolah', 'sekolah_id', true, 16, null);
        $this->addForeignKey('kepemilikan_sarpras_id', 'KepemilikanSarprasId', 'NUMERIC', 'ref.status_kepemilikan_sarpras', 'kepemilikan_sarpras_id', true, 3, null);
        $this->addForeignKey('kepemilikan_sarpras_id', 'KepemilikanSarprasId', 'NUMERIC', 'ref.status_kepemilikan_sarpras', 'kepemilikan_sarpras_id', true, 3, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 30, null);
        $this->addColumn('panjang', 'Panjang', 'FLOAT', false, 8, null);
        $this->addColumn('lebar', 'Lebar', 'FLOAT', false, 8, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SekolahRelatedBySekolahId', 'angulex\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('SekolahRelatedBySekolahId', 'angulex\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('JenisPrasaranaRelatedByJenisPrasaranaId', 'angulex\\Model\\JenisPrasarana', RelationMap::MANY_TO_ONE, array('jenis_prasarana_id' => 'jenis_prasarana_id', ), null, null);
        $this->addRelation('JenisPrasaranaRelatedByJenisPrasaranaId', 'angulex\\Model\\JenisPrasarana', RelationMap::MANY_TO_ONE, array('jenis_prasarana_id' => 'jenis_prasarana_id', ), null, null);
        $this->addRelation('StatusKepemilikanSarprasRelatedByKepemilikanSarprasId', 'angulex\\Model\\StatusKepemilikanSarpras', RelationMap::MANY_TO_ONE, array('kepemilikan_sarpras_id' => 'kepemilikan_sarpras_id', ), null, null);
        $this->addRelation('StatusKepemilikanSarprasRelatedByKepemilikanSarprasId', 'angulex\\Model\\StatusKepemilikanSarpras', RelationMap::MANY_TO_ONE, array('kepemilikan_sarpras_id' => 'kepemilikan_sarpras_id', ), null, null);
        $this->addRelation('BukuAlatRelatedByPrasaranaId', 'angulex\\Model\\BukuAlat', RelationMap::ONE_TO_MANY, array('prasarana_id' => 'prasarana_id', ), null, null, 'BukuAlatsRelatedByPrasaranaId');
        $this->addRelation('BukuAlatRelatedByPrasaranaId', 'angulex\\Model\\BukuAlat', RelationMap::ONE_TO_MANY, array('prasarana_id' => 'prasarana_id', ), null, null, 'BukuAlatsRelatedByPrasaranaId');
        $this->addRelation('VldPrasaranaRelatedByPrasaranaId', 'angulex\\Model\\VldPrasarana', RelationMap::ONE_TO_MANY, array('prasarana_id' => 'prasarana_id', ), null, null, 'VldPrasaranasRelatedByPrasaranaId');
        $this->addRelation('VldPrasaranaRelatedByPrasaranaId', 'angulex\\Model\\VldPrasarana', RelationMap::ONE_TO_MANY, array('prasarana_id' => 'prasarana_id', ), null, null, 'VldPrasaranasRelatedByPrasaranaId');
        $this->addRelation('SaranaRelatedByPrasaranaId', 'angulex\\Model\\Sarana', RelationMap::ONE_TO_MANY, array('prasarana_id' => 'prasarana_id', ), null, null, 'SaranasRelatedByPrasaranaId');
        $this->addRelation('SaranaRelatedByPrasaranaId', 'angulex\\Model\\Sarana', RelationMap::ONE_TO_MANY, array('prasarana_id' => 'prasarana_id', ), null, null, 'SaranasRelatedByPrasaranaId');
        $this->addRelation('PrasaranaLongitudinalRelatedByPrasaranaId', 'angulex\\Model\\PrasaranaLongitudinal', RelationMap::ONE_TO_MANY, array('prasarana_id' => 'prasarana_id', ), null, null, 'PrasaranaLongitudinalsRelatedByPrasaranaId');
        $this->addRelation('PrasaranaLongitudinalRelatedByPrasaranaId', 'angulex\\Model\\PrasaranaLongitudinal', RelationMap::ONE_TO_MANY, array('prasarana_id' => 'prasarana_id', ), null, null, 'PrasaranaLongitudinalsRelatedByPrasaranaId');
        $this->addRelation('RombonganBelajarRelatedByPrasaranaId', 'angulex\\Model\\RombonganBelajar', RelationMap::ONE_TO_MANY, array('prasarana_id' => 'prasarana_id', ), null, null, 'RombonganBelajarsRelatedByPrasaranaId');
        $this->addRelation('RombonganBelajarRelatedByPrasaranaId', 'angulex\\Model\\RombonganBelajar', RelationMap::ONE_TO_MANY, array('prasarana_id' => 'prasarana_id', ), null, null, 'RombonganBelajarsRelatedByPrasaranaId');
    } // buildRelations()

} // PrasaranaTableMap
