<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.kurikulum' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class KurikulumTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.KurikulumTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.kurikulum');
        $this->setPhpName('Kurikulum');
        $this->setClassname('angulex\\Model\\Kurikulum');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('kurikulum_id', 'KurikulumId', 'SMALLINT', true, 2, null);
        $this->addForeignKey('jurusan_id', 'JurusanId', 'VARCHAR', 'ref.jurusan', 'jurusan_id', false, 25, null);
        $this->addForeignKey('jenjang_pendidikan_id', 'JenjangPendidikanId', 'NUMERIC', 'ref.jenjang_pendidikan', 'jenjang_pendidikan_id', true, 4, null);
        $this->addColumn('nama_kurikulum', 'NamaKurikulum', 'VARCHAR', true, 30, null);
        $this->addColumn('mulai_berlaku', 'MulaiBerlaku', 'VARCHAR', true, 20, null);
        $this->addColumn('sistem_sks', 'SistemSks', 'NUMERIC', true, 3, 0);
        $this->addColumn('total_sks', 'TotalSks', 'NUMERIC', true, 5, 0);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('JenjangPendidikan', 'angulex\\Model\\JenjangPendidikan', RelationMap::MANY_TO_ONE, array('jenjang_pendidikan_id' => 'jenjang_pendidikan_id', ), null, null);
        $this->addRelation('Jurusan', 'angulex\\Model\\Jurusan', RelationMap::MANY_TO_ONE, array('jurusan_id' => 'jurusan_id', ), null, null);
        $this->addRelation('MataPelajaranKurikulum', 'angulex\\Model\\MataPelajaranKurikulum', RelationMap::ONE_TO_MANY, array('kurikulum_id' => 'kurikulum_id', ), null, null, 'MataPelajaranKurikulums');
        $this->addRelation('GroupMatpel', 'angulex\\Model\\GroupMatpel', RelationMap::ONE_TO_MANY, array('kurikulum_id' => 'kurikulum_id', ), null, null, 'GroupMatpels');
        $this->addRelation('RombonganBelajarRelatedByKurikulumId', 'angulex\\Model\\RombonganBelajar', RelationMap::ONE_TO_MANY, array('kurikulum_id' => 'kurikulum_id', ), null, null, 'RombonganBelajarsRelatedByKurikulumId');
        $this->addRelation('RombonganBelajarRelatedByKurikulumId', 'angulex\\Model\\RombonganBelajar', RelationMap::ONE_TO_MANY, array('kurikulum_id' => 'kurikulum_id', ), null, null, 'RombonganBelajarsRelatedByKurikulumId');
    } // buildRelations()

} // KurikulumTableMap
