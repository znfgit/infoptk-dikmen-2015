<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'akreditasi_prodi' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class AkreditasiProdiTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.AkreditasiProdiTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('akreditasi_prodi');
        $this->setPhpName('AkreditasiProdi');
        $this->setClassname('angulex\\Model\\AkreditasiProdi');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('akred_prodi_id', 'AkredProdiId', 'CHAR', true, 16, null);
        $this->addForeignKey('akreditasi_id', 'AkreditasiId', 'NUMERIC', 'ref.akreditasi', 'akreditasi_id', true, 3, null);
        $this->addForeignKey('akreditasi_id', 'AkreditasiId', 'NUMERIC', 'ref.akreditasi', 'akreditasi_id', true, 3, null);
        $this->addForeignKey('la_id', 'LaId', 'CHAR', 'ref.lembaga_akreditasi', 'la_id', true, 5, null);
        $this->addForeignKey('la_id', 'LaId', 'CHAR', 'ref.lembaga_akreditasi', 'la_id', true, 5, null);
        $this->addForeignKey('jurusan_sp_id', 'JurusanSpId', 'CHAR', 'jurusan_sp', 'jurusan_sp_id', true, 16, null);
        $this->addForeignKey('jurusan_sp_id', 'JurusanSpId', 'CHAR', 'jurusan_sp', 'jurusan_sp_id', true, 16, null);
        $this->addColumn('no_sk_akred', 'NoSkAkred', 'VARCHAR', true, 40, null);
        $this->addColumn('tgl_sk_akred', 'TglSkAkred', 'VARCHAR', true, 20, null);
        $this->addColumn('tgl_akhir_akred', 'TglAkhirAkred', 'VARCHAR', true, 20, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('JurusanSpRelatedByJurusanSpId', 'angulex\\Model\\JurusanSp', RelationMap::MANY_TO_ONE, array('jurusan_sp_id' => 'jurusan_sp_id', ), null, null);
        $this->addRelation('JurusanSpRelatedByJurusanSpId', 'angulex\\Model\\JurusanSp', RelationMap::MANY_TO_ONE, array('jurusan_sp_id' => 'jurusan_sp_id', ), null, null);
        $this->addRelation('AkreditasiRelatedByAkreditasiId', 'angulex\\Model\\Akreditasi', RelationMap::MANY_TO_ONE, array('akreditasi_id' => 'akreditasi_id', ), null, null);
        $this->addRelation('AkreditasiRelatedByAkreditasiId', 'angulex\\Model\\Akreditasi', RelationMap::MANY_TO_ONE, array('akreditasi_id' => 'akreditasi_id', ), null, null);
        $this->addRelation('LembagaAkreditasiRelatedByLaId', 'angulex\\Model\\LembagaAkreditasi', RelationMap::MANY_TO_ONE, array('la_id' => 'la_id', ), null, null);
        $this->addRelation('LembagaAkreditasiRelatedByLaId', 'angulex\\Model\\LembagaAkreditasi', RelationMap::MANY_TO_ONE, array('la_id' => 'la_id', ), null, null);
    } // buildRelations()

} // AkreditasiProdiTableMap
