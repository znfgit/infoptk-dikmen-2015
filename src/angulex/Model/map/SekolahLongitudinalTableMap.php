<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'sekolah_longitudinal' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class SekolahLongitudinalTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.SekolahLongitudinalTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('sekolah_longitudinal');
        $this->setPhpName('SekolahLongitudinal');
        $this->setClassname('angulex\\Model\\SekolahLongitudinal');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addForeignPrimaryKey('sekolah_id', 'SekolahId', 'CHAR' , 'sekolah', 'sekolah_id', true, 16, null);
        $this->addForeignPrimaryKey('sekolah_id', 'SekolahId', 'CHAR' , 'sekolah', 'sekolah_id', true, 16, null);
        $this->addForeignPrimaryKey('semester_id', 'SemesterId', 'CHAR' , 'ref.semester', 'semester_id', true, 5, null);
        $this->addForeignPrimaryKey('semester_id', 'SemesterId', 'CHAR' , 'ref.semester', 'semester_id', true, 5, null);
        $this->addColumn('daya_listrik', 'DayaListrik', 'NUMERIC', true, 8, null);
        $this->addColumn('wilayah_terpencil', 'WilayahTerpencil', 'NUMERIC', true, 3, null);
        $this->addColumn('wilayah_perbatasan', 'WilayahPerbatasan', 'NUMERIC', true, 3, null);
        $this->addColumn('wilayah_transmigrasi', 'WilayahTransmigrasi', 'NUMERIC', true, 3, null);
        $this->addColumn('wilayah_adat_terpencil', 'WilayahAdatTerpencil', 'NUMERIC', true, 3, null);
        $this->addColumn('wilayah_bencana_alam', 'WilayahBencanaAlam', 'NUMERIC', true, 3, null);
        $this->addColumn('wilayah_bencana_sosial', 'WilayahBencanaSosial', 'NUMERIC', true, 3, null);
        $this->addColumn('partisipasi_bos', 'PartisipasiBos', 'NUMERIC', true, 3, 0);
        $this->addForeignKey('waktu_penyelenggaraan_id', 'WaktuPenyelenggaraanId', 'NUMERIC', 'ref.waktu_penyelenggaraan', 'waktu_penyelenggaraan_id', true, 3, null);
        $this->addForeignKey('waktu_penyelenggaraan_id', 'WaktuPenyelenggaraanId', 'NUMERIC', 'ref.waktu_penyelenggaraan', 'waktu_penyelenggaraan_id', true, 3, null);
        $this->addForeignKey('sumber_listrik_id', 'SumberListrikId', 'NUMERIC', 'ref.sumber_listrik', 'sumber_listrik_id', true, 4, null);
        $this->addForeignKey('sumber_listrik_id', 'SumberListrikId', 'NUMERIC', 'ref.sumber_listrik', 'sumber_listrik_id', true, 4, null);
        $this->addForeignKey('sertifikasi_iso_id', 'SertifikasiIsoId', 'SMALLINT', 'ref.sertifikasi_iso', 'sertifikasi_iso_id', true, 2, null);
        $this->addForeignKey('sertifikasi_iso_id', 'SertifikasiIsoId', 'SMALLINT', 'ref.sertifikasi_iso', 'sertifikasi_iso_id', true, 2, null);
        $this->addForeignKey('akses_internet_id', 'AksesInternetId', 'SMALLINT', 'ref.akses_internet', 'akses_internet_id', false, 2, null);
        $this->addForeignKey('akses_internet_id', 'AksesInternetId', 'SMALLINT', 'ref.akses_internet', 'akses_internet_id', false, 2, null);
        $this->addForeignKey('akses_internet_2_id', 'AksesInternet2Id', 'SMALLINT', 'ref.akses_internet', 'akses_internet_id', true, 2, null);
        $this->addForeignKey('akses_internet_2_id', 'AksesInternet2Id', 'SMALLINT', 'ref.akses_internet', 'akses_internet_id', true, 2, null);
        $this->addColumn('blob_id', 'BlobId', 'CHAR', false, 16, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SekolahRelatedBySekolahId', 'angulex\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('SekolahRelatedBySekolahId', 'angulex\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('AksesInternetRelatedByAksesInternetId', 'angulex\\Model\\AksesInternet', RelationMap::MANY_TO_ONE, array('akses_internet_id' => 'akses_internet_id', ), null, null);
        $this->addRelation('AksesInternetRelatedByAksesInternet2Id', 'angulex\\Model\\AksesInternet', RelationMap::MANY_TO_ONE, array('akses_internet_2_id' => 'akses_internet_id', ), null, null);
        $this->addRelation('AksesInternetRelatedByAksesInternetId', 'angulex\\Model\\AksesInternet', RelationMap::MANY_TO_ONE, array('akses_internet_id' => 'akses_internet_id', ), null, null);
        $this->addRelation('AksesInternetRelatedByAksesInternet2Id', 'angulex\\Model\\AksesInternet', RelationMap::MANY_TO_ONE, array('akses_internet_2_id' => 'akses_internet_id', ), null, null);
        $this->addRelation('SemesterRelatedBySemesterId', 'angulex\\Model\\Semester', RelationMap::MANY_TO_ONE, array('semester_id' => 'semester_id', ), null, null);
        $this->addRelation('SemesterRelatedBySemesterId', 'angulex\\Model\\Semester', RelationMap::MANY_TO_ONE, array('semester_id' => 'semester_id', ), null, null);
        $this->addRelation('SertifikasiIsoRelatedBySertifikasiIsoId', 'angulex\\Model\\SertifikasiIso', RelationMap::MANY_TO_ONE, array('sertifikasi_iso_id' => 'sertifikasi_iso_id', ), null, null);
        $this->addRelation('SertifikasiIsoRelatedBySertifikasiIsoId', 'angulex\\Model\\SertifikasiIso', RelationMap::MANY_TO_ONE, array('sertifikasi_iso_id' => 'sertifikasi_iso_id', ), null, null);
        $this->addRelation('SumberListrikRelatedBySumberListrikId', 'angulex\\Model\\SumberListrik', RelationMap::MANY_TO_ONE, array('sumber_listrik_id' => 'sumber_listrik_id', ), null, null);
        $this->addRelation('SumberListrikRelatedBySumberListrikId', 'angulex\\Model\\SumberListrik', RelationMap::MANY_TO_ONE, array('sumber_listrik_id' => 'sumber_listrik_id', ), null, null);
        $this->addRelation('WaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId', 'angulex\\Model\\WaktuPenyelenggaraan', RelationMap::MANY_TO_ONE, array('waktu_penyelenggaraan_id' => 'waktu_penyelenggaraan_id', ), null, null);
        $this->addRelation('WaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId', 'angulex\\Model\\WaktuPenyelenggaraan', RelationMap::MANY_TO_ONE, array('waktu_penyelenggaraan_id' => 'waktu_penyelenggaraan_id', ), null, null);
    } // buildRelations()

} // SekolahLongitudinalTableMap
