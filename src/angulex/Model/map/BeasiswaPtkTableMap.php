<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'beasiswa_ptk' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class BeasiswaPtkTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.BeasiswaPtkTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('beasiswa_ptk');
        $this->setPhpName('BeasiswaPtk');
        $this->setClassname('angulex\\Model\\BeasiswaPtk');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('beasiswa_ptk_id', 'BeasiswaPtkId', 'CHAR', true, 16, null);
        $this->addForeignKey('jenis_beasiswa_id', 'JenisBeasiswaId', 'INTEGER', 'ref.jenis_beasiswa', 'jenis_beasiswa_id', true, 4, null);
        $this->addForeignKey('jenis_beasiswa_id', 'JenisBeasiswaId', 'INTEGER', 'ref.jenis_beasiswa', 'jenis_beasiswa_id', true, 4, null);
        $this->addForeignKey('ptk_id', 'PtkId', 'CHAR', 'ptk', 'ptk_id', true, 16, null);
        $this->addForeignKey('ptk_id', 'PtkId', 'CHAR', 'ptk', 'ptk_id', true, 16, null);
        $this->addColumn('keterangan', 'Keterangan', 'VARCHAR', true, 80, null);
        $this->addColumn('tahun_mulai', 'TahunMulai', 'NUMERIC', true, 6, null);
        $this->addColumn('tahun_akhir', 'TahunAkhir', 'NUMERIC', false, 6, null);
        $this->addColumn('masih_menerima', 'MasihMenerima', 'NUMERIC', true, 3, 0);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('PtkRelatedByPtkId', 'angulex\\Model\\Ptk', RelationMap::MANY_TO_ONE, array('ptk_id' => 'ptk_id', ), null, null);
        $this->addRelation('PtkRelatedByPtkId', 'angulex\\Model\\Ptk', RelationMap::MANY_TO_ONE, array('ptk_id' => 'ptk_id', ), null, null);
        $this->addRelation('JenisBeasiswaRelatedByJenisBeasiswaId', 'angulex\\Model\\JenisBeasiswa', RelationMap::MANY_TO_ONE, array('jenis_beasiswa_id' => 'jenis_beasiswa_id', ), null, null);
        $this->addRelation('JenisBeasiswaRelatedByJenisBeasiswaId', 'angulex\\Model\\JenisBeasiswa', RelationMap::MANY_TO_ONE, array('jenis_beasiswa_id' => 'jenis_beasiswa_id', ), null, null);
        $this->addRelation('VldBeaPtkRelatedByBeasiswaPtkId', 'angulex\\Model\\VldBeaPtk', RelationMap::ONE_TO_MANY, array('beasiswa_ptk_id' => 'beasiswa_ptk_id', ), null, null, 'VldBeaPtksRelatedByBeasiswaPtkId');
        $this->addRelation('VldBeaPtkRelatedByBeasiswaPtkId', 'angulex\\Model\\VldBeaPtk', RelationMap::ONE_TO_MANY, array('beasiswa_ptk_id' => 'beasiswa_ptk_id', ), null, null, 'VldBeaPtksRelatedByBeasiswaPtkId');
    } // buildRelations()

} // BeasiswaPtkTableMap
