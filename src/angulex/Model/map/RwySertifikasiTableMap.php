<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'rwy_sertifikasi' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class RwySertifikasiTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.RwySertifikasiTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('rwy_sertifikasi');
        $this->setPhpName('RwySertifikasi');
        $this->setClassname('angulex\\Model\\RwySertifikasi');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('riwayat_sertifikasi_id', 'RiwayatSertifikasiId', 'CHAR', true, 16, null);
        $this->addForeignKey('ptk_id', 'PtkId', 'CHAR', 'ptk', 'ptk_id', true, 16, null);
        $this->addForeignKey('ptk_id', 'PtkId', 'CHAR', 'ptk', 'ptk_id', true, 16, null);
        $this->addForeignKey('bidang_studi_id', 'BidangStudiId', 'INTEGER', 'ref.bidang_studi', 'bidang_studi_id', true, 4, null);
        $this->addForeignKey('bidang_studi_id', 'BidangStudiId', 'INTEGER', 'ref.bidang_studi', 'bidang_studi_id', true, 4, null);
        $this->addForeignKey('id_jenis_sertifikasi', 'IdJenisSertifikasi', 'NUMERIC', 'ref.jenis_sertifikasi', 'id_jenis_sertifikasi', true, 5, null);
        $this->addForeignKey('id_jenis_sertifikasi', 'IdJenisSertifikasi', 'NUMERIC', 'ref.jenis_sertifikasi', 'id_jenis_sertifikasi', true, 5, null);
        $this->addColumn('tahun_sertifikasi', 'TahunSertifikasi', 'NUMERIC', true, 6, null);
        $this->addColumn('nomor_sertifikat', 'NomorSertifikat', 'VARCHAR', true, 40, null);
        $this->addColumn('nrg', 'Nrg', 'VARCHAR', false, 12, null);
        $this->addColumn('nomor_peserta', 'NomorPeserta', 'VARCHAR', false, 14, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('PtkRelatedByPtkId', 'angulex\\Model\\Ptk', RelationMap::MANY_TO_ONE, array('ptk_id' => 'ptk_id', ), null, null);
        $this->addRelation('PtkRelatedByPtkId', 'angulex\\Model\\Ptk', RelationMap::MANY_TO_ONE, array('ptk_id' => 'ptk_id', ), null, null);
        $this->addRelation('BidangStudiRelatedByBidangStudiId', 'angulex\\Model\\BidangStudi', RelationMap::MANY_TO_ONE, array('bidang_studi_id' => 'bidang_studi_id', ), null, null);
        $this->addRelation('BidangStudiRelatedByBidangStudiId', 'angulex\\Model\\BidangStudi', RelationMap::MANY_TO_ONE, array('bidang_studi_id' => 'bidang_studi_id', ), null, null);
        $this->addRelation('JenisSertifikasiRelatedByIdJenisSertifikasi', 'angulex\\Model\\JenisSertifikasi', RelationMap::MANY_TO_ONE, array('id_jenis_sertifikasi' => 'id_jenis_sertifikasi', ), null, null);
        $this->addRelation('JenisSertifikasiRelatedByIdJenisSertifikasi', 'angulex\\Model\\JenisSertifikasi', RelationMap::MANY_TO_ONE, array('id_jenis_sertifikasi' => 'id_jenis_sertifikasi', ), null, null);
        $this->addRelation('VldRwySertifikasiRelatedByRiwayatSertifikasiId', 'angulex\\Model\\VldRwySertifikasi', RelationMap::ONE_TO_MANY, array('riwayat_sertifikasi_id' => 'riwayat_sertifikasi_id', ), null, null, 'VldRwySertifikasisRelatedByRiwayatSertifikasiId');
        $this->addRelation('VldRwySertifikasiRelatedByRiwayatSertifikasiId', 'angulex\\Model\\VldRwySertifikasi', RelationMap::ONE_TO_MANY, array('riwayat_sertifikasi_id' => 'riwayat_sertifikasi_id', ), null, null, 'VldRwySertifikasisRelatedByRiwayatSertifikasiId');
    } // buildRelations()

} // RwySertifikasiTableMap
