<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'peserta_didik_longitudinal' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class PesertaDidikLongitudinalTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.PesertaDidikLongitudinalTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('peserta_didik_longitudinal');
        $this->setPhpName('PesertaDidikLongitudinal');
        $this->setClassname('angulex\\Model\\PesertaDidikLongitudinal');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addForeignPrimaryKey('peserta_didik_id', 'PesertaDidikId', 'CHAR' , 'peserta_didik', 'peserta_didik_id', true, 16, null);
        $this->addForeignPrimaryKey('peserta_didik_id', 'PesertaDidikId', 'CHAR' , 'peserta_didik', 'peserta_didik_id', true, 16, null);
        $this->addForeignPrimaryKey('semester_id', 'SemesterId', 'CHAR' , 'ref.semester', 'semester_id', true, 5, null);
        $this->addForeignPrimaryKey('semester_id', 'SemesterId', 'CHAR' , 'ref.semester', 'semester_id', true, 5, null);
        $this->addColumn('tinggi_badan', 'TinggiBadan', 'NUMERIC', true, 5, null);
        $this->addColumn('berat_badan', 'BeratBadan', 'NUMERIC', true, 5, null);
        $this->addColumn('jarak_rumah_ke_sekolah', 'JarakRumahKeSekolah', 'NUMERIC', true, 3, null);
        $this->addColumn('jarak_rumah_ke_sekolah_km', 'JarakRumahKeSekolahKm', 'NUMERIC', false, 5, null);
        $this->addColumn('waktu_tempuh_ke_sekolah', 'WaktuTempuhKeSekolah', 'NUMERIC', true, 3, null);
        $this->addColumn('menit_tempuh_ke_sekolah', 'MenitTempuhKeSekolah', 'NUMERIC', false, 5, null);
        $this->addColumn('jumlah_saudara_kandung', 'JumlahSaudaraKandung', 'NUMERIC', true, 4, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('PesertaDidikRelatedByPesertaDidikId', 'angulex\\Model\\PesertaDidik', RelationMap::MANY_TO_ONE, array('peserta_didik_id' => 'peserta_didik_id', ), null, null);
        $this->addRelation('PesertaDidikRelatedByPesertaDidikId', 'angulex\\Model\\PesertaDidik', RelationMap::MANY_TO_ONE, array('peserta_didik_id' => 'peserta_didik_id', ), null, null);
        $this->addRelation('SemesterRelatedBySemesterId', 'angulex\\Model\\Semester', RelationMap::MANY_TO_ONE, array('semester_id' => 'semester_id', ), null, null);
        $this->addRelation('SemesterRelatedBySemesterId', 'angulex\\Model\\Semester', RelationMap::MANY_TO_ONE, array('semester_id' => 'semester_id', ), null, null);
        $this->addRelation('VldPdLongRelatedByPesertaDidikIdSemesterId', 'angulex\\Model\\VldPdLong', RelationMap::ONE_TO_MANY, array('peserta_didik_id' => 'semester_id', 'semester_id' => 'semester_id', ), null, null, 'VldPdLongsRelatedByPesertaDidikIdSemesterId');
        $this->addRelation('VldPdLongRelatedByPesertaDidikIdSemesterId', 'angulex\\Model\\VldPdLong', RelationMap::ONE_TO_MANY, array('peserta_didik_id' => 'semester_id', 'semester_id' => 'semester_id', ), null, null, 'VldPdLongsRelatedByPesertaDidikIdSemesterId');
    } // buildRelations()

} // PesertaDidikLongitudinalTableMap
