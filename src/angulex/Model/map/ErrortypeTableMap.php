<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.errortype' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class ErrortypeTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.ErrortypeTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.errortype');
        $this->setPhpName('Errortype');
        $this->setClassname('angulex\\Model\\Errortype');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('idtype', 'Idtype', 'INTEGER', true, 4, null);
        $this->addColumn('kategori_error', 'KategoriError', 'INTEGER', false, 4, null);
        $this->addColumn('keterangan', 'Keterangan', 'VARCHAR', false, 255, null);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('VldRwyKepangkatanRelatedByIdtype', 'angulex\\Model\\VldRwyKepangkatan', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldRwyKepangkatansRelatedByIdtype');
        $this->addRelation('VldRwyKepangkatanRelatedByIdtype', 'angulex\\Model\\VldRwyKepangkatan', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldRwyKepangkatansRelatedByIdtype');
        $this->addRelation('VldRwyFungsionalRelatedByIdtype', 'angulex\\Model\\VldRwyFungsional', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldRwyFungsionalsRelatedByIdtype');
        $this->addRelation('VldRwyFungsionalRelatedByIdtype', 'angulex\\Model\\VldRwyFungsional', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldRwyFungsionalsRelatedByIdtype');
        $this->addRelation('VldRombelRelatedByIdtype', 'angulex\\Model\\VldRombel', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldRombelsRelatedByIdtype');
        $this->addRelation('VldRombelRelatedByIdtype', 'angulex\\Model\\VldRombel', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldRombelsRelatedByIdtype');
        $this->addRelation('VldPtkRelatedByIdtype', 'angulex\\Model\\VldPtk', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldPtksRelatedByIdtype');
        $this->addRelation('VldPtkRelatedByIdtype', 'angulex\\Model\\VldPtk', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldPtksRelatedByIdtype');
        $this->addRelation('VldPrestasiRelatedByIdtype', 'angulex\\Model\\VldPrestasi', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldPrestasisRelatedByIdtype');
        $this->addRelation('VldPrestasiRelatedByIdtype', 'angulex\\Model\\VldPrestasi', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldPrestasisRelatedByIdtype');
        $this->addRelation('VldPrasaranaRelatedByIdtype', 'angulex\\Model\\VldPrasarana', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldPrasaranasRelatedByIdtype');
        $this->addRelation('VldPrasaranaRelatedByIdtype', 'angulex\\Model\\VldPrasarana', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldPrasaranasRelatedByIdtype');
        $this->addRelation('VldPesertaDidikRelatedByIdtype', 'angulex\\Model\\VldPesertaDidik', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldPesertaDidiksRelatedByIdtype');
        $this->addRelation('VldPesertaDidikRelatedByIdtype', 'angulex\\Model\\VldPesertaDidik', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldPesertaDidiksRelatedByIdtype');
        $this->addRelation('VldPenghargaanRelatedByIdtype', 'angulex\\Model\\VldPenghargaan', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldPenghargaansRelatedByIdtype');
        $this->addRelation('VldPenghargaanRelatedByIdtype', 'angulex\\Model\\VldPenghargaan', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldPenghargaansRelatedByIdtype');
        $this->addRelation('VldPembelajaranRelatedByIdtype', 'angulex\\Model\\VldPembelajaran', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldPembelajaransRelatedByIdtype');
        $this->addRelation('VldPembelajaranRelatedByIdtype', 'angulex\\Model\\VldPembelajaran', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldPembelajaransRelatedByIdtype');
        $this->addRelation('VldPdLongRelatedByIdtype', 'angulex\\Model\\VldPdLong', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldPdLongsRelatedByIdtype');
        $this->addRelation('VldPdLongRelatedByIdtype', 'angulex\\Model\\VldPdLong', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldPdLongsRelatedByIdtype');
        $this->addRelation('VldNonsekolahRelatedByIdtype', 'angulex\\Model\\VldNonsekolah', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldNonsekolahsRelatedByIdtype');
        $this->addRelation('VldNonsekolahRelatedByIdtype', 'angulex\\Model\\VldNonsekolah', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldNonsekolahsRelatedByIdtype');
        $this->addRelation('VldNilaiTestRelatedByIdtype', 'angulex\\Model\\VldNilaiTest', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldNilaiTestsRelatedByIdtype');
        $this->addRelation('VldNilaiTestRelatedByIdtype', 'angulex\\Model\\VldNilaiTest', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldNilaiTestsRelatedByIdtype');
        $this->addRelation('VldNilaiRaporRelatedByIdtype', 'angulex\\Model\\VldNilaiRapor', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldNilaiRaporsRelatedByIdtype');
        $this->addRelation('VldNilaiRaporRelatedByIdtype', 'angulex\\Model\\VldNilaiRapor', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldNilaiRaporsRelatedByIdtype');
        $this->addRelation('VldMouRelatedByIdtype', 'angulex\\Model\\VldMou', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldMousRelatedByIdtype');
        $this->addRelation('VldMouRelatedByIdtype', 'angulex\\Model\\VldMou', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldMousRelatedByIdtype');
        $this->addRelation('VldKesejahteraanRelatedByIdtype', 'angulex\\Model\\VldKesejahteraan', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldKesejahteraansRelatedByIdtype');
        $this->addRelation('VldKesejahteraanRelatedByIdtype', 'angulex\\Model\\VldKesejahteraan', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldKesejahteraansRelatedByIdtype');
        $this->addRelation('VldKaryaTulisRelatedByIdtype', 'angulex\\Model\\VldKaryaTulis', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldKaryaTulissRelatedByIdtype');
        $this->addRelation('VldKaryaTulisRelatedByIdtype', 'angulex\\Model\\VldKaryaTulis', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldKaryaTulissRelatedByIdtype');
        $this->addRelation('VldJurusanSpRelatedByIdtype', 'angulex\\Model\\VldJurusanSp', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldJurusanSpsRelatedByIdtype');
        $this->addRelation('VldJurusanSpRelatedByIdtype', 'angulex\\Model\\VldJurusanSp', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldJurusanSpsRelatedByIdtype');
        $this->addRelation('VldInpassingRelatedByIdtype', 'angulex\\Model\\VldInpassing', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldInpassingsRelatedByIdtype');
        $this->addRelation('VldInpassingRelatedByIdtype', 'angulex\\Model\\VldInpassing', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldInpassingsRelatedByIdtype');
        $this->addRelation('VldDemografiRelatedByIdtype', 'angulex\\Model\\VldDemografi', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldDemografisRelatedByIdtype');
        $this->addRelation('VldDemografiRelatedByIdtype', 'angulex\\Model\\VldDemografi', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldDemografisRelatedByIdtype');
        $this->addRelation('VldBukuPtkRelatedByIdtype', 'angulex\\Model\\VldBukuPtk', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldBukuPtksRelatedByIdtype');
        $this->addRelation('VldBukuPtkRelatedByIdtype', 'angulex\\Model\\VldBukuPtk', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldBukuPtksRelatedByIdtype');
        $this->addRelation('VldBeaPtkRelatedByIdtype', 'angulex\\Model\\VldBeaPtk', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldBeaPtksRelatedByIdtype');
        $this->addRelation('VldBeaPtkRelatedByIdtype', 'angulex\\Model\\VldBeaPtk', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldBeaPtksRelatedByIdtype');
        $this->addRelation('VldBeaPdRelatedByIdtype', 'angulex\\Model\\VldBeaPd', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldBeaPdsRelatedByIdtype');
        $this->addRelation('VldBeaPdRelatedByIdtype', 'angulex\\Model\\VldBeaPd', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldBeaPdsRelatedByIdtype');
        $this->addRelation('VldAnakRelatedByIdtype', 'angulex\\Model\\VldAnak', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldAnaksRelatedByIdtype');
        $this->addRelation('VldAnakRelatedByIdtype', 'angulex\\Model\\VldAnak', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldAnaksRelatedByIdtype');
        $this->addRelation('VldYayasanRelatedByIdtype', 'angulex\\Model\\VldYayasan', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldYayasansRelatedByIdtype');
        $this->addRelation('VldYayasanRelatedByIdtype', 'angulex\\Model\\VldYayasan', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldYayasansRelatedByIdtype');
        $this->addRelation('VldUnRelatedByIdtype', 'angulex\\Model\\VldUn', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldUnsRelatedByIdtype');
        $this->addRelation('VldUnRelatedByIdtype', 'angulex\\Model\\VldUn', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldUnsRelatedByIdtype');
        $this->addRelation('VldTunjanganRelatedByIdtype', 'angulex\\Model\\VldTunjangan', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldTunjangansRelatedByIdtype');
        $this->addRelation('VldTunjanganRelatedByIdtype', 'angulex\\Model\\VldTunjangan', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldTunjangansRelatedByIdtype');
        $this->addRelation('VldTugasTambahanRelatedByIdtype', 'angulex\\Model\\VldTugasTambahan', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldTugasTambahansRelatedByIdtype');
        $this->addRelation('VldTugasTambahanRelatedByIdtype', 'angulex\\Model\\VldTugasTambahan', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldTugasTambahansRelatedByIdtype');
        $this->addRelation('VldSekolahRelatedByIdtype', 'angulex\\Model\\VldSekolah', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldSekolahsRelatedByIdtype');
        $this->addRelation('VldSekolahRelatedByIdtype', 'angulex\\Model\\VldSekolah', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldSekolahsRelatedByIdtype');
        $this->addRelation('VldSaranaRelatedByIdtype', 'angulex\\Model\\VldSarana', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldSaranasRelatedByIdtype');
        $this->addRelation('VldSaranaRelatedByIdtype', 'angulex\\Model\\VldSarana', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldSaranasRelatedByIdtype');
        $this->addRelation('VldRwyStrukturalRelatedByIdtype', 'angulex\\Model\\VldRwyStruktural', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldRwyStrukturalsRelatedByIdtype');
        $this->addRelation('VldRwyStrukturalRelatedByIdtype', 'angulex\\Model\\VldRwyStruktural', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldRwyStrukturalsRelatedByIdtype');
        $this->addRelation('VldRwySertifikasiRelatedByIdtype', 'angulex\\Model\\VldRwySertifikasi', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldRwySertifikasisRelatedByIdtype');
        $this->addRelation('VldRwySertifikasiRelatedByIdtype', 'angulex\\Model\\VldRwySertifikasi', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldRwySertifikasisRelatedByIdtype');
        $this->addRelation('VldRwyPendFormalRelatedByIdtype', 'angulex\\Model\\VldRwyPendFormal', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldRwyPendFormalsRelatedByIdtype');
        $this->addRelation('VldRwyPendFormalRelatedByIdtype', 'angulex\\Model\\VldRwyPendFormal', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldRwyPendFormalsRelatedByIdtype');
    } // buildRelations()

} // ErrortypeTableMap
