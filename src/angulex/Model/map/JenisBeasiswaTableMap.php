<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.jenis_beasiswa' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class JenisBeasiswaTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.JenisBeasiswaTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.jenis_beasiswa');
        $this->setPhpName('JenisBeasiswa');
        $this->setClassname('angulex\\Model\\JenisBeasiswa');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('jenis_beasiswa_id', 'JenisBeasiswaId', 'INTEGER', true, 4, null);
        $this->addForeignKey('sumber_dana_id', 'SumberDanaId', 'NUMERIC', 'ref.sumber_dana', 'sumber_dana_id', true, 5, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 50, null);
        $this->addColumn('untuk_pd', 'UntukPd', 'NUMERIC', true, 3, null);
        $this->addColumn('untuk_ptk', 'UntukPtk', 'NUMERIC', true, 3, null);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SumberDana', 'angulex\\Model\\SumberDana', RelationMap::MANY_TO_ONE, array('sumber_dana_id' => 'sumber_dana_id', ), null, null);
        $this->addRelation('BeasiswaPesertaDidikRelatedByJenisBeasiswaId', 'angulex\\Model\\BeasiswaPesertaDidik', RelationMap::ONE_TO_MANY, array('jenis_beasiswa_id' => 'jenis_beasiswa_id', ), null, null, 'BeasiswaPesertaDidiksRelatedByJenisBeasiswaId');
        $this->addRelation('BeasiswaPesertaDidikRelatedByJenisBeasiswaId', 'angulex\\Model\\BeasiswaPesertaDidik', RelationMap::ONE_TO_MANY, array('jenis_beasiswa_id' => 'jenis_beasiswa_id', ), null, null, 'BeasiswaPesertaDidiksRelatedByJenisBeasiswaId');
        $this->addRelation('BeasiswaPtkRelatedByJenisBeasiswaId', 'angulex\\Model\\BeasiswaPtk', RelationMap::ONE_TO_MANY, array('jenis_beasiswa_id' => 'jenis_beasiswa_id', ), null, null, 'BeasiswaPtksRelatedByJenisBeasiswaId');
        $this->addRelation('BeasiswaPtkRelatedByJenisBeasiswaId', 'angulex\\Model\\BeasiswaPtk', RelationMap::ONE_TO_MANY, array('jenis_beasiswa_id' => 'jenis_beasiswa_id', ), null, null, 'BeasiswaPtksRelatedByJenisBeasiswaId');
    } // buildRelations()

} // JenisBeasiswaTableMap
