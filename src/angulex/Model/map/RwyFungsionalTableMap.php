<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'rwy_fungsional' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class RwyFungsionalTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.RwyFungsionalTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('rwy_fungsional');
        $this->setPhpName('RwyFungsional');
        $this->setClassname('angulex\\Model\\RwyFungsional');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('riwayat_fungsional_id', 'RiwayatFungsionalId', 'CHAR', true, 16, null);
        $this->addForeignKey('ptk_id', 'PtkId', 'CHAR', 'ptk', 'ptk_id', true, 16, null);
        $this->addForeignKey('ptk_id', 'PtkId', 'CHAR', 'ptk', 'ptk_id', true, 16, null);
        $this->addForeignKey('jabatan_fungsional_id', 'JabatanFungsionalId', 'NUMERIC', 'ref.jabatan_fungsional', 'jabatan_fungsional_id', true, 4, null);
        $this->addForeignKey('jabatan_fungsional_id', 'JabatanFungsionalId', 'NUMERIC', 'ref.jabatan_fungsional', 'jabatan_fungsional_id', true, 4, null);
        $this->addColumn('sk_jabfung', 'SkJabfung', 'VARCHAR', true, 40, null);
        $this->addColumn('tmt_jabatan', 'TmtJabatan', 'VARCHAR', true, 20, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('PtkRelatedByPtkId', 'angulex\\Model\\Ptk', RelationMap::MANY_TO_ONE, array('ptk_id' => 'ptk_id', ), null, null);
        $this->addRelation('PtkRelatedByPtkId', 'angulex\\Model\\Ptk', RelationMap::MANY_TO_ONE, array('ptk_id' => 'ptk_id', ), null, null);
        $this->addRelation('JabatanFungsionalRelatedByJabatanFungsionalId', 'angulex\\Model\\JabatanFungsional', RelationMap::MANY_TO_ONE, array('jabatan_fungsional_id' => 'jabatan_fungsional_id', ), null, null);
        $this->addRelation('JabatanFungsionalRelatedByJabatanFungsionalId', 'angulex\\Model\\JabatanFungsional', RelationMap::MANY_TO_ONE, array('jabatan_fungsional_id' => 'jabatan_fungsional_id', ), null, null);
        $this->addRelation('VldRwyFungsionalRelatedByRiwayatFungsionalId', 'angulex\\Model\\VldRwyFungsional', RelationMap::ONE_TO_MANY, array('riwayat_fungsional_id' => 'riwayat_fungsional_id', ), null, null, 'VldRwyFungsionalsRelatedByRiwayatFungsionalId');
        $this->addRelation('VldRwyFungsionalRelatedByRiwayatFungsionalId', 'angulex\\Model\\VldRwyFungsional', RelationMap::ONE_TO_MANY, array('riwayat_fungsional_id' => 'riwayat_fungsional_id', ), null, null, 'VldRwyFungsionalsRelatedByRiwayatFungsionalId');
    } // buildRelations()

} // RwyFungsionalTableMap
