<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.mata_pelajaran_kurikulum' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class MataPelajaranKurikulumTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.MataPelajaranKurikulumTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.mata_pelajaran_kurikulum');
        $this->setPhpName('MataPelajaranKurikulum');
        $this->setClassname('angulex\\Model\\MataPelajaranKurikulum');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addForeignPrimaryKey('kurikulum_id', 'KurikulumId', 'SMALLINT' , 'ref.kurikulum', 'kurikulum_id', true, 2, null);
        $this->addForeignPrimaryKey('mata_pelajaran_id', 'MataPelajaranId', 'INTEGER' , 'ref.mata_pelajaran', 'mata_pelajaran_id', true, 4, null);
        $this->addForeignPrimaryKey('tingkat_pendidikan_id', 'TingkatPendidikanId', 'NUMERIC' , 'ref.tingkat_pendidikan', 'tingkat_pendidikan_id', true, 4, null);
        $this->addForeignKey('gmp_id', 'GmpId', 'CHAR', 'ref.group_matpel', 'gmp_id', false, 16, null);
        $this->addColumn('jumlah_jam', 'JumlahJam', 'NUMERIC', true, 4, null);
        $this->addColumn('jumlah_jam_maksimum', 'JumlahJamMaksimum', 'NUMERIC', true, 4, null);
        $this->addColumn('wajib', 'Wajib', 'NUMERIC', true, 3, null);
        $this->addColumn('sks', 'Sks', 'NUMERIC', true, 4, 0);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('GroupMatpel', 'angulex\\Model\\GroupMatpel', RelationMap::MANY_TO_ONE, array('gmp_id' => 'gmp_id', ), null, null);
        $this->addRelation('Kurikulum', 'angulex\\Model\\Kurikulum', RelationMap::MANY_TO_ONE, array('kurikulum_id' => 'kurikulum_id', ), null, null);
        $this->addRelation('MataPelajaran', 'angulex\\Model\\MataPelajaran', RelationMap::MANY_TO_ONE, array('mata_pelajaran_id' => 'mata_pelajaran_id', ), null, null);
        $this->addRelation('TingkatPendidikan', 'angulex\\Model\\TingkatPendidikan', RelationMap::MANY_TO_ONE, array('tingkat_pendidikan_id' => 'tingkat_pendidikan_id', ), null, null);
    } // buildRelations()

} // MataPelajaranKurikulumTableMap
