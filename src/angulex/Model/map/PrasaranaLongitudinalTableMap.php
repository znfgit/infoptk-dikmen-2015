<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'prasarana_longitudinal' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class PrasaranaLongitudinalTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.PrasaranaLongitudinalTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('prasarana_longitudinal');
        $this->setPhpName('PrasaranaLongitudinal');
        $this->setClassname('angulex\\Model\\PrasaranaLongitudinal');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addForeignPrimaryKey('prasarana_id', 'PrasaranaId', 'CHAR' , 'prasarana', 'prasarana_id', true, 16, null);
        $this->addForeignPrimaryKey('prasarana_id', 'PrasaranaId', 'CHAR' , 'prasarana', 'prasarana_id', true, 16, null);
        $this->addForeignPrimaryKey('semester_id', 'SemesterId', 'CHAR' , 'ref.semester', 'semester_id', true, 5, null);
        $this->addForeignPrimaryKey('semester_id', 'SemesterId', 'CHAR' , 'ref.semester', 'semester_id', true, 5, null);
        $this->addColumn('rusak_penutup_atap', 'RusakPenutupAtap', 'NUMERIC', true, 8, 0);
        $this->addColumn('rusak_rangka_atap', 'RusakRangkaAtap', 'NUMERIC', true, 8, 0);
        $this->addColumn('rusak_lisplang_talang', 'RusakLisplangTalang', 'NUMERIC', true, 8, 0);
        $this->addColumn('rusak_rangka_plafon', 'RusakRangkaPlafon', 'NUMERIC', true, 8, 0);
        $this->addColumn('rusak_penutup_listplafon', 'RusakPenutupListplafon', 'NUMERIC', true, 8, 0);
        $this->addColumn('rusak_cat_plafon', 'RusakCatPlafon', 'NUMERIC', true, 8, 0);
        $this->addColumn('rusak_kolom_ringbalok', 'RusakKolomRingbalok', 'NUMERIC', true, 8, 0);
        $this->addColumn('rusak_bata_dindingpengisi', 'RusakBataDindingpengisi', 'NUMERIC', true, 8, 0);
        $this->addColumn('rusak_cat_dinding', 'RusakCatDinding', 'NUMERIC', true, 8, 0);
        $this->addColumn('rusak_kusen', 'RusakKusen', 'NUMERIC', true, 8, 0);
        $this->addColumn('rusak_daun_pintu', 'RusakDaunPintu', 'NUMERIC', true, 8, 0);
        $this->addColumn('rusak_daun_jendela', 'RusakDaunJendela', 'NUMERIC', true, 8, 0);
        $this->addColumn('rusak_struktur_bawah', 'RusakStrukturBawah', 'NUMERIC', true, 8, 0);
        $this->addColumn('rusak_penutup_lantai', 'RusakPenutupLantai', 'NUMERIC', true, 8, 0);
        $this->addColumn('rusak_pondasi', 'RusakPondasi', 'NUMERIC', true, 8, 0);
        $this->addColumn('rusak_sloof', 'RusakSloof', 'NUMERIC', true, 8, 0);
        $this->addColumn('rusak_listrik', 'RusakListrik', 'NUMERIC', true, 8, 0);
        $this->addColumn('rusak_airhujan_rabatan', 'RusakAirhujanRabatan', 'NUMERIC', true, 8, 0);
        $this->addColumn('blob_id', 'BlobId', 'CHAR', false, 16, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('PrasaranaRelatedByPrasaranaId', 'angulex\\Model\\Prasarana', RelationMap::MANY_TO_ONE, array('prasarana_id' => 'prasarana_id', ), null, null);
        $this->addRelation('PrasaranaRelatedByPrasaranaId', 'angulex\\Model\\Prasarana', RelationMap::MANY_TO_ONE, array('prasarana_id' => 'prasarana_id', ), null, null);
        $this->addRelation('SemesterRelatedBySemesterId', 'angulex\\Model\\Semester', RelationMap::MANY_TO_ONE, array('semester_id' => 'semester_id', ), null, null);
        $this->addRelation('SemesterRelatedBySemesterId', 'angulex\\Model\\Semester', RelationMap::MANY_TO_ONE, array('semester_id' => 'semester_id', ), null, null);
    } // buildRelations()

} // PrasaranaLongitudinalTableMap
