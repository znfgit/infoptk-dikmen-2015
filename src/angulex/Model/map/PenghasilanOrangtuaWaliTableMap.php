<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.penghasilan_orangtua_wali' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class PenghasilanOrangtuaWaliTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.PenghasilanOrangtuaWaliTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.penghasilan_orangtua_wali');
        $this->setPhpName('PenghasilanOrangtuaWali');
        $this->setClassname('angulex\\Model\\PenghasilanOrangtuaWali');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('penghasilan_orangtua_wali_id', 'PenghasilanOrangtuaWaliId', 'INTEGER', true, 4, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 40, null);
        $this->addColumn('batas_bawah', 'BatasBawah', 'INTEGER', true, 4, null);
        $this->addColumn('batas_atas', 'BatasAtas', 'INTEGER', true, 4, null);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('PesertaDidikRelatedByPenghasilanIdAyah', 'angulex\\Model\\PesertaDidik', RelationMap::ONE_TO_MANY, array('penghasilan_orangtua_wali_id' => 'penghasilan_id_ayah', ), null, null, 'PesertaDidiksRelatedByPenghasilanIdAyah');
        $this->addRelation('PesertaDidikRelatedByPenghasilanIdWali', 'angulex\\Model\\PesertaDidik', RelationMap::ONE_TO_MANY, array('penghasilan_orangtua_wali_id' => 'penghasilan_id_wali', ), null, null, 'PesertaDidiksRelatedByPenghasilanIdWali');
        $this->addRelation('PesertaDidikRelatedByPenghasilanIdIbu', 'angulex\\Model\\PesertaDidik', RelationMap::ONE_TO_MANY, array('penghasilan_orangtua_wali_id' => 'penghasilan_id_ibu', ), null, null, 'PesertaDidiksRelatedByPenghasilanIdIbu');
        $this->addRelation('PesertaDidikRelatedByPenghasilanIdAyah', 'angulex\\Model\\PesertaDidik', RelationMap::ONE_TO_MANY, array('penghasilan_orangtua_wali_id' => 'penghasilan_id_ayah', ), null, null, 'PesertaDidiksRelatedByPenghasilanIdAyah');
        $this->addRelation('PesertaDidikRelatedByPenghasilanIdWali', 'angulex\\Model\\PesertaDidik', RelationMap::ONE_TO_MANY, array('penghasilan_orangtua_wali_id' => 'penghasilan_id_wali', ), null, null, 'PesertaDidiksRelatedByPenghasilanIdWali');
        $this->addRelation('PesertaDidikRelatedByPenghasilanIdIbu', 'angulex\\Model\\PesertaDidik', RelationMap::ONE_TO_MANY, array('penghasilan_orangtua_wali_id' => 'penghasilan_id_ibu', ), null, null, 'PesertaDidiksRelatedByPenghasilanIdIbu');
    } // buildRelations()

} // PenghasilanOrangtuaWaliTableMap
