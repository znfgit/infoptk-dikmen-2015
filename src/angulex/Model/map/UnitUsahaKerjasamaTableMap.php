<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'unit_usaha_kerjasama' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class UnitUsahaKerjasamaTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.UnitUsahaKerjasamaTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('unit_usaha_kerjasama');
        $this->setPhpName('UnitUsahaKerjasama');
        $this->setClassname('angulex\\Model\\UnitUsahaKerjasama');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addForeignPrimaryKey('mou_id', 'MouId', 'CHAR' , 'mou', 'mou_id', true, 16, null);
        $this->addForeignPrimaryKey('mou_id', 'MouId', 'CHAR' , 'mou', 'mou_id', true, 16, null);
        $this->addForeignPrimaryKey('unit_usaha_id', 'UnitUsahaId', 'CHAR' , 'unit_usaha', 'unit_usaha_id', true, 16, null);
        $this->addForeignPrimaryKey('unit_usaha_id', 'UnitUsahaId', 'CHAR' , 'unit_usaha', 'unit_usaha_id', true, 16, null);
        $this->addForeignKey('sumber_dana_id', 'SumberDanaId', 'NUMERIC', 'ref.sumber_dana', 'sumber_dana_id', true, 5, null);
        $this->addForeignKey('sumber_dana_id', 'SumberDanaId', 'NUMERIC', 'ref.sumber_dana', 'sumber_dana_id', true, 5, null);
        $this->addColumn('produk_barang_dihasilkan', 'ProdukBarangDihasilkan', 'VARCHAR', false, 200, null);
        $this->addColumn('jasa_produksi_dihasilkan', 'JasaProduksiDihasilkan', 'VARCHAR', false, 200, null);
        $this->addColumn('omzet_barang_perbulan', 'OmzetBarangPerbulan', 'NUMERIC', false, 20, null);
        $this->addColumn('omzet_jasa_perbulan', 'OmzetJasaPerbulan', 'NUMERIC', false, 20, null);
        $this->addColumn('prestasi_penghargaan', 'PrestasiPenghargaan', 'VARCHAR', false, 200, null);
        $this->addColumn('pangsa_pasar_produk', 'PangsaPasarProduk', 'VARCHAR', false, 200, null);
        $this->addColumn('pangsa_pasar_jasa', 'PangsaPasarJasa', 'VARCHAR', false, 200, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('MouRelatedByMouId', 'angulex\\Model\\Mou', RelationMap::MANY_TO_ONE, array('mou_id' => 'mou_id', ), null, null);
        $this->addRelation('MouRelatedByMouId', 'angulex\\Model\\Mou', RelationMap::MANY_TO_ONE, array('mou_id' => 'mou_id', ), null, null);
        $this->addRelation('UnitUsahaRelatedByUnitUsahaId', 'angulex\\Model\\UnitUsaha', RelationMap::MANY_TO_ONE, array('unit_usaha_id' => 'unit_usaha_id', ), null, null);
        $this->addRelation('UnitUsahaRelatedByUnitUsahaId', 'angulex\\Model\\UnitUsaha', RelationMap::MANY_TO_ONE, array('unit_usaha_id' => 'unit_usaha_id', ), null, null);
        $this->addRelation('SumberDanaRelatedBySumberDanaId', 'angulex\\Model\\SumberDana', RelationMap::MANY_TO_ONE, array('sumber_dana_id' => 'sumber_dana_id', ), null, null);
        $this->addRelation('SumberDanaRelatedBySumberDanaId', 'angulex\\Model\\SumberDana', RelationMap::MANY_TO_ONE, array('sumber_dana_id' => 'sumber_dana_id', ), null, null);
    } // buildRelations()

} // UnitUsahaKerjasamaTableMap
