<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'kesejahteraan' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class KesejahteraanTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.KesejahteraanTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('kesejahteraan');
        $this->setPhpName('Kesejahteraan');
        $this->setClassname('angulex\\Model\\Kesejahteraan');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('kesejahteraan_id', 'KesejahteraanId', 'CHAR', true, 16, null);
        $this->addForeignKey('ptk_id', 'PtkId', 'CHAR', 'ptk', 'ptk_id', true, 16, null);
        $this->addForeignKey('ptk_id', 'PtkId', 'CHAR', 'ptk', 'ptk_id', true, 16, null);
        $this->addForeignKey('jenis_kesejahteraan_id', 'JenisKesejahteraanId', 'INTEGER', 'ref.jenis_kesejahteraan', 'jenis_kesejahteraan_id', true, 4, null);
        $this->addForeignKey('jenis_kesejahteraan_id', 'JenisKesejahteraanId', 'INTEGER', 'ref.jenis_kesejahteraan', 'jenis_kesejahteraan_id', true, 4, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 25, null);
        $this->addColumn('penyelenggara', 'Penyelenggara', 'VARCHAR', true, 80, null);
        $this->addColumn('dari_tahun', 'DariTahun', 'NUMERIC', true, 6, null);
        $this->addColumn('sampai_tahun', 'SampaiTahun', 'NUMERIC', false, 6, null);
        $this->addColumn('status', 'Status', 'INTEGER', false, 4, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('PtkRelatedByPtkId', 'angulex\\Model\\Ptk', RelationMap::MANY_TO_ONE, array('ptk_id' => 'ptk_id', ), null, null);
        $this->addRelation('PtkRelatedByPtkId', 'angulex\\Model\\Ptk', RelationMap::MANY_TO_ONE, array('ptk_id' => 'ptk_id', ), null, null);
        $this->addRelation('JenisKesejahteraanRelatedByJenisKesejahteraanId', 'angulex\\Model\\JenisKesejahteraan', RelationMap::MANY_TO_ONE, array('jenis_kesejahteraan_id' => 'jenis_kesejahteraan_id', ), null, null);
        $this->addRelation('JenisKesejahteraanRelatedByJenisKesejahteraanId', 'angulex\\Model\\JenisKesejahteraan', RelationMap::MANY_TO_ONE, array('jenis_kesejahteraan_id' => 'jenis_kesejahteraan_id', ), null, null);
        $this->addRelation('VldKesejahteraanRelatedByKesejahteraanId', 'angulex\\Model\\VldKesejahteraan', RelationMap::ONE_TO_MANY, array('kesejahteraan_id' => 'kesejahteraan_id', ), null, null, 'VldKesejahteraansRelatedByKesejahteraanId');
        $this->addRelation('VldKesejahteraanRelatedByKesejahteraanId', 'angulex\\Model\\VldKesejahteraan', RelationMap::ONE_TO_MANY, array('kesejahteraan_id' => 'kesejahteraan_id', ), null, null, 'VldKesejahteraansRelatedByKesejahteraanId');
    } // buildRelations()

} // KesejahteraanTableMap
