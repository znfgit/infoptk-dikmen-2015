<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'anggota_rombel' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class AnggotaRombelTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.AnggotaRombelTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('anggota_rombel');
        $this->setPhpName('AnggotaRombel');
        $this->setClassname('angulex\\Model\\AnggotaRombel');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('anggota_rombel_id', 'AnggotaRombelId', 'CHAR', true, 16, null);
        $this->addForeignKey('rombongan_belajar_id', 'RombonganBelajarId', 'CHAR', 'rombongan_belajar', 'rombongan_belajar_id', true, 16, null);
        $this->addForeignKey('rombongan_belajar_id', 'RombonganBelajarId', 'CHAR', 'rombongan_belajar', 'rombongan_belajar_id', true, 16, null);
        $this->addForeignKey('peserta_didik_id', 'PesertaDidikId', 'CHAR', 'peserta_didik', 'peserta_didik_id', true, 16, null);
        $this->addForeignKey('peserta_didik_id', 'PesertaDidikId', 'CHAR', 'peserta_didik', 'peserta_didik_id', true, 16, null);
        $this->addForeignKey('jenis_pendaftaran_id', 'JenisPendaftaranId', 'NUMERIC', 'ref.jenis_pendaftaran', 'jenis_pendaftaran_id', true, 3, null);
        $this->addForeignKey('jenis_pendaftaran_id', 'JenisPendaftaranId', 'NUMERIC', 'ref.jenis_pendaftaran', 'jenis_pendaftaran_id', true, 3, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('PesertaDidikRelatedByPesertaDidikId', 'angulex\\Model\\PesertaDidik', RelationMap::MANY_TO_ONE, array('peserta_didik_id' => 'peserta_didik_id', ), null, null);
        $this->addRelation('PesertaDidikRelatedByPesertaDidikId', 'angulex\\Model\\PesertaDidik', RelationMap::MANY_TO_ONE, array('peserta_didik_id' => 'peserta_didik_id', ), null, null);
        $this->addRelation('RombonganBelajarRelatedByRombonganBelajarId', 'angulex\\Model\\RombonganBelajar', RelationMap::MANY_TO_ONE, array('rombongan_belajar_id' => 'rombongan_belajar_id', ), null, null);
        $this->addRelation('RombonganBelajarRelatedByRombonganBelajarId', 'angulex\\Model\\RombonganBelajar', RelationMap::MANY_TO_ONE, array('rombongan_belajar_id' => 'rombongan_belajar_id', ), null, null);
        $this->addRelation('JenisPendaftaranRelatedByJenisPendaftaranId', 'angulex\\Model\\JenisPendaftaran', RelationMap::MANY_TO_ONE, array('jenis_pendaftaran_id' => 'jenis_pendaftaran_id', ), null, null);
        $this->addRelation('JenisPendaftaranRelatedByJenisPendaftaranId', 'angulex\\Model\\JenisPendaftaran', RelationMap::MANY_TO_ONE, array('jenis_pendaftaran_id' => 'jenis_pendaftaran_id', ), null, null);
    } // buildRelations()

} // AnggotaRombelTableMap
