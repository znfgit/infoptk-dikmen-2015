<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'rwy_struktural' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class RwyStrukturalTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.RwyStrukturalTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('rwy_struktural');
        $this->setPhpName('RwyStruktural');
        $this->setClassname('angulex\\Model\\RwyStruktural');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('riwayat_struktural_id', 'RiwayatStrukturalId', 'CHAR', true, 16, null);
        $this->addForeignKey('ptk_id', 'PtkId', 'CHAR', 'ptk', 'ptk_id', true, 16, null);
        $this->addForeignKey('ptk_id', 'PtkId', 'CHAR', 'ptk', 'ptk_id', true, 16, null);
        $this->addForeignKey('jabatan_ptk_id', 'JabatanPtkId', 'NUMERIC', 'ref.jabatan_tugas_ptk', 'jabatan_ptk_id', true, 5, null);
        $this->addForeignKey('jabatan_ptk_id', 'JabatanPtkId', 'NUMERIC', 'ref.jabatan_tugas_ptk', 'jabatan_ptk_id', true, 5, null);
        $this->addColumn('sk_struktural', 'SkStruktural', 'VARCHAR', true, 40, null);
        $this->addColumn('tmt_jabatan', 'TmtJabatan', 'VARCHAR', true, 20, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('PtkRelatedByPtkId', 'angulex\\Model\\Ptk', RelationMap::MANY_TO_ONE, array('ptk_id' => 'ptk_id', ), null, null);
        $this->addRelation('PtkRelatedByPtkId', 'angulex\\Model\\Ptk', RelationMap::MANY_TO_ONE, array('ptk_id' => 'ptk_id', ), null, null);
        $this->addRelation('JabatanTugasPtkRelatedByJabatanPtkId', 'angulex\\Model\\JabatanTugasPtk', RelationMap::MANY_TO_ONE, array('jabatan_ptk_id' => 'jabatan_ptk_id', ), null, null);
        $this->addRelation('JabatanTugasPtkRelatedByJabatanPtkId', 'angulex\\Model\\JabatanTugasPtk', RelationMap::MANY_TO_ONE, array('jabatan_ptk_id' => 'jabatan_ptk_id', ), null, null);
        $this->addRelation('VldRwyStrukturalRelatedByRiwayatStrukturalId', 'angulex\\Model\\VldRwyStruktural', RelationMap::ONE_TO_MANY, array('riwayat_struktural_id' => 'riwayat_struktural_id', ), null, null, 'VldRwyStrukturalsRelatedByRiwayatStrukturalId');
        $this->addRelation('VldRwyStrukturalRelatedByRiwayatStrukturalId', 'angulex\\Model\\VldRwyStruktural', RelationMap::ONE_TO_MANY, array('riwayat_struktural_id' => 'riwayat_struktural_id', ), null, null, 'VldRwyStrukturalsRelatedByRiwayatStrukturalId');
    } // buildRelations()

} // RwyStrukturalTableMap
