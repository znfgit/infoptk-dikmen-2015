<?php

namespace angulex\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.jenis_keluar' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.angulex.Model.map
 */
class JenisKeluarTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'angulex.Model.map.JenisKeluarTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.jenis_keluar');
        $this->setPhpName('JenisKeluar');
        $this->setClassname('angulex\\Model\\JenisKeluar');
        $this->setPackage('angulex.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('jenis_keluar_id', 'JenisKeluarId', 'CHAR', true, 1, null);
        $this->addColumn('ket_keluar', 'KetKeluar', 'VARCHAR', true, 40, null);
        $this->addColumn('keluar_pd', 'KeluarPd', 'NUMERIC', true, 3, null);
        $this->addColumn('keluar_ptk', 'KeluarPtk', 'NUMERIC', true, 3, null);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('RegistrasiPesertaDidikRelatedByJenisKeluarId', 'angulex\\Model\\RegistrasiPesertaDidik', RelationMap::ONE_TO_MANY, array('jenis_keluar_id' => 'jenis_keluar_id', ), null, null, 'RegistrasiPesertaDidiksRelatedByJenisKeluarId');
        $this->addRelation('RegistrasiPesertaDidikRelatedByJenisKeluarId', 'angulex\\Model\\RegistrasiPesertaDidik', RelationMap::ONE_TO_MANY, array('jenis_keluar_id' => 'jenis_keluar_id', ), null, null, 'RegistrasiPesertaDidiksRelatedByJenisKeluarId');
        $this->addRelation('PtkTerdaftarRelatedByJenisKeluarId', 'angulex\\Model\\PtkTerdaftar', RelationMap::ONE_TO_MANY, array('jenis_keluar_id' => 'jenis_keluar_id', ), null, null, 'PtkTerdaftarsRelatedByJenisKeluarId');
        $this->addRelation('PtkTerdaftarRelatedByJenisKeluarId', 'angulex\\Model\\PtkTerdaftar', RelationMap::ONE_TO_MANY, array('jenis_keluar_id' => 'jenis_keluar_id', ), null, null, 'PtkTerdaftarsRelatedByJenisKeluarId');
        $this->addRelation('PengawasTerdaftarRelatedByJenisKeluarId', 'angulex\\Model\\PengawasTerdaftar', RelationMap::ONE_TO_MANY, array('jenis_keluar_id' => 'jenis_keluar_id', ), null, null, 'PengawasTerdaftarsRelatedByJenisKeluarId');
        $this->addRelation('PengawasTerdaftarRelatedByJenisKeluarId', 'angulex\\Model\\PengawasTerdaftar', RelationMap::ONE_TO_MANY, array('jenis_keluar_id' => 'jenis_keluar_id', ), null, null, 'PengawasTerdaftarsRelatedByJenisKeluarId');
    } // buildRelations()

} // JenisKeluarTableMap
