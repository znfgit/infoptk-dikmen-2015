<?php

namespace angulex\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\Blockgrant;
use angulex\Model\BlockgrantPeer;
use angulex\Model\BlockgrantQuery;
use angulex\Model\JenisBantuan;
use angulex\Model\Sekolah;
use angulex\Model\SumberDana;

/**
 * Base class that represents a query for the 'blockgrant' table.
 *
 * 
 *
 * @method BlockgrantQuery orderByBlockgrantId($order = Criteria::ASC) Order by the blockgrant_id column
 * @method BlockgrantQuery orderBySekolahId($order = Criteria::ASC) Order by the sekolah_id column
 * @method BlockgrantQuery orderByNama($order = Criteria::ASC) Order by the nama column
 * @method BlockgrantQuery orderByTahun($order = Criteria::ASC) Order by the tahun column
 * @method BlockgrantQuery orderByJenisBantuanId($order = Criteria::ASC) Order by the jenis_bantuan_id column
 * @method BlockgrantQuery orderBySumberDanaId($order = Criteria::ASC) Order by the sumber_dana_id column
 * @method BlockgrantQuery orderByBesarBantuan($order = Criteria::ASC) Order by the besar_bantuan column
 * @method BlockgrantQuery orderByDanaPendamping($order = Criteria::ASC) Order by the dana_pendamping column
 * @method BlockgrantQuery orderByPeruntukanDana($order = Criteria::ASC) Order by the peruntukan_dana column
 * @method BlockgrantQuery orderByAsalData($order = Criteria::ASC) Order by the asal_data column
 * @method BlockgrantQuery orderByLastUpdate($order = Criteria::ASC) Order by the Last_update column
 * @method BlockgrantQuery orderBySoftDelete($order = Criteria::ASC) Order by the Soft_delete column
 * @method BlockgrantQuery orderByLastSync($order = Criteria::ASC) Order by the last_sync column
 * @method BlockgrantQuery orderByUpdaterId($order = Criteria::ASC) Order by the Updater_ID column
 *
 * @method BlockgrantQuery groupByBlockgrantId() Group by the blockgrant_id column
 * @method BlockgrantQuery groupBySekolahId() Group by the sekolah_id column
 * @method BlockgrantQuery groupByNama() Group by the nama column
 * @method BlockgrantQuery groupByTahun() Group by the tahun column
 * @method BlockgrantQuery groupByJenisBantuanId() Group by the jenis_bantuan_id column
 * @method BlockgrantQuery groupBySumberDanaId() Group by the sumber_dana_id column
 * @method BlockgrantQuery groupByBesarBantuan() Group by the besar_bantuan column
 * @method BlockgrantQuery groupByDanaPendamping() Group by the dana_pendamping column
 * @method BlockgrantQuery groupByPeruntukanDana() Group by the peruntukan_dana column
 * @method BlockgrantQuery groupByAsalData() Group by the asal_data column
 * @method BlockgrantQuery groupByLastUpdate() Group by the Last_update column
 * @method BlockgrantQuery groupBySoftDelete() Group by the Soft_delete column
 * @method BlockgrantQuery groupByLastSync() Group by the last_sync column
 * @method BlockgrantQuery groupByUpdaterId() Group by the Updater_ID column
 *
 * @method BlockgrantQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method BlockgrantQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method BlockgrantQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method BlockgrantQuery leftJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method BlockgrantQuery rightJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method BlockgrantQuery innerJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the SekolahRelatedBySekolahId relation
 *
 * @method BlockgrantQuery leftJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method BlockgrantQuery rightJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method BlockgrantQuery innerJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the SekolahRelatedBySekolahId relation
 *
 * @method BlockgrantQuery leftJoinJenisBantuanRelatedByJenisBantuanId($relationAlias = null) Adds a LEFT JOIN clause to the query using the JenisBantuanRelatedByJenisBantuanId relation
 * @method BlockgrantQuery rightJoinJenisBantuanRelatedByJenisBantuanId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JenisBantuanRelatedByJenisBantuanId relation
 * @method BlockgrantQuery innerJoinJenisBantuanRelatedByJenisBantuanId($relationAlias = null) Adds a INNER JOIN clause to the query using the JenisBantuanRelatedByJenisBantuanId relation
 *
 * @method BlockgrantQuery leftJoinJenisBantuanRelatedByJenisBantuanId($relationAlias = null) Adds a LEFT JOIN clause to the query using the JenisBantuanRelatedByJenisBantuanId relation
 * @method BlockgrantQuery rightJoinJenisBantuanRelatedByJenisBantuanId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JenisBantuanRelatedByJenisBantuanId relation
 * @method BlockgrantQuery innerJoinJenisBantuanRelatedByJenisBantuanId($relationAlias = null) Adds a INNER JOIN clause to the query using the JenisBantuanRelatedByJenisBantuanId relation
 *
 * @method BlockgrantQuery leftJoinSumberDanaRelatedBySumberDanaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SumberDanaRelatedBySumberDanaId relation
 * @method BlockgrantQuery rightJoinSumberDanaRelatedBySumberDanaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SumberDanaRelatedBySumberDanaId relation
 * @method BlockgrantQuery innerJoinSumberDanaRelatedBySumberDanaId($relationAlias = null) Adds a INNER JOIN clause to the query using the SumberDanaRelatedBySumberDanaId relation
 *
 * @method BlockgrantQuery leftJoinSumberDanaRelatedBySumberDanaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SumberDanaRelatedBySumberDanaId relation
 * @method BlockgrantQuery rightJoinSumberDanaRelatedBySumberDanaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SumberDanaRelatedBySumberDanaId relation
 * @method BlockgrantQuery innerJoinSumberDanaRelatedBySumberDanaId($relationAlias = null) Adds a INNER JOIN clause to the query using the SumberDanaRelatedBySumberDanaId relation
 *
 * @method Blockgrant findOne(PropelPDO $con = null) Return the first Blockgrant matching the query
 * @method Blockgrant findOneOrCreate(PropelPDO $con = null) Return the first Blockgrant matching the query, or a new Blockgrant object populated from the query conditions when no match is found
 *
 * @method Blockgrant findOneBySekolahId(string $sekolah_id) Return the first Blockgrant filtered by the sekolah_id column
 * @method Blockgrant findOneByNama(string $nama) Return the first Blockgrant filtered by the nama column
 * @method Blockgrant findOneByTahun(string $tahun) Return the first Blockgrant filtered by the tahun column
 * @method Blockgrant findOneByJenisBantuanId(int $jenis_bantuan_id) Return the first Blockgrant filtered by the jenis_bantuan_id column
 * @method Blockgrant findOneBySumberDanaId(string $sumber_dana_id) Return the first Blockgrant filtered by the sumber_dana_id column
 * @method Blockgrant findOneByBesarBantuan(string $besar_bantuan) Return the first Blockgrant filtered by the besar_bantuan column
 * @method Blockgrant findOneByDanaPendamping(string $dana_pendamping) Return the first Blockgrant filtered by the dana_pendamping column
 * @method Blockgrant findOneByPeruntukanDana(string $peruntukan_dana) Return the first Blockgrant filtered by the peruntukan_dana column
 * @method Blockgrant findOneByAsalData(string $asal_data) Return the first Blockgrant filtered by the asal_data column
 * @method Blockgrant findOneByLastUpdate(string $Last_update) Return the first Blockgrant filtered by the Last_update column
 * @method Blockgrant findOneBySoftDelete(string $Soft_delete) Return the first Blockgrant filtered by the Soft_delete column
 * @method Blockgrant findOneByLastSync(string $last_sync) Return the first Blockgrant filtered by the last_sync column
 * @method Blockgrant findOneByUpdaterId(string $Updater_ID) Return the first Blockgrant filtered by the Updater_ID column
 *
 * @method array findByBlockgrantId(string $blockgrant_id) Return Blockgrant objects filtered by the blockgrant_id column
 * @method array findBySekolahId(string $sekolah_id) Return Blockgrant objects filtered by the sekolah_id column
 * @method array findByNama(string $nama) Return Blockgrant objects filtered by the nama column
 * @method array findByTahun(string $tahun) Return Blockgrant objects filtered by the tahun column
 * @method array findByJenisBantuanId(int $jenis_bantuan_id) Return Blockgrant objects filtered by the jenis_bantuan_id column
 * @method array findBySumberDanaId(string $sumber_dana_id) Return Blockgrant objects filtered by the sumber_dana_id column
 * @method array findByBesarBantuan(string $besar_bantuan) Return Blockgrant objects filtered by the besar_bantuan column
 * @method array findByDanaPendamping(string $dana_pendamping) Return Blockgrant objects filtered by the dana_pendamping column
 * @method array findByPeruntukanDana(string $peruntukan_dana) Return Blockgrant objects filtered by the peruntukan_dana column
 * @method array findByAsalData(string $asal_data) Return Blockgrant objects filtered by the asal_data column
 * @method array findByLastUpdate(string $Last_update) Return Blockgrant objects filtered by the Last_update column
 * @method array findBySoftDelete(string $Soft_delete) Return Blockgrant objects filtered by the Soft_delete column
 * @method array findByLastSync(string $last_sync) Return Blockgrant objects filtered by the last_sync column
 * @method array findByUpdaterId(string $Updater_ID) Return Blockgrant objects filtered by the Updater_ID column
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseBlockgrantQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseBlockgrantQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Dapodikmen', $modelName = 'angulex\\Model\\Blockgrant', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new BlockgrantQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   BlockgrantQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return BlockgrantQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof BlockgrantQuery) {
            return $criteria;
        }
        $query = new BlockgrantQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Blockgrant|Blockgrant[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = BlockgrantPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(BlockgrantPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Blockgrant A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByBlockgrantId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Blockgrant A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [blockgrant_id], [sekolah_id], [nama], [tahun], [jenis_bantuan_id], [sumber_dana_id], [besar_bantuan], [dana_pendamping], [peruntukan_dana], [asal_data], [Last_update], [Soft_delete], [last_sync], [Updater_ID] FROM [blockgrant] WHERE [blockgrant_id] = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Blockgrant();
            $obj->hydrate($row);
            BlockgrantPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Blockgrant|Blockgrant[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Blockgrant[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return BlockgrantQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(BlockgrantPeer::BLOCKGRANT_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return BlockgrantQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(BlockgrantPeer::BLOCKGRANT_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the blockgrant_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBlockgrantId('fooValue');   // WHERE blockgrant_id = 'fooValue'
     * $query->filterByBlockgrantId('%fooValue%'); // WHERE blockgrant_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $blockgrantId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BlockgrantQuery The current query, for fluid interface
     */
    public function filterByBlockgrantId($blockgrantId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($blockgrantId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $blockgrantId)) {
                $blockgrantId = str_replace('*', '%', $blockgrantId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BlockgrantPeer::BLOCKGRANT_ID, $blockgrantId, $comparison);
    }

    /**
     * Filter the query on the sekolah_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySekolahId('fooValue');   // WHERE sekolah_id = 'fooValue'
     * $query->filterBySekolahId('%fooValue%'); // WHERE sekolah_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sekolahId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BlockgrantQuery The current query, for fluid interface
     */
    public function filterBySekolahId($sekolahId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sekolahId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $sekolahId)) {
                $sekolahId = str_replace('*', '%', $sekolahId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BlockgrantPeer::SEKOLAH_ID, $sekolahId, $comparison);
    }

    /**
     * Filter the query on the nama column
     *
     * Example usage:
     * <code>
     * $query->filterByNama('fooValue');   // WHERE nama = 'fooValue'
     * $query->filterByNama('%fooValue%'); // WHERE nama LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nama The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BlockgrantQuery The current query, for fluid interface
     */
    public function filterByNama($nama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nama)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nama)) {
                $nama = str_replace('*', '%', $nama);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BlockgrantPeer::NAMA, $nama, $comparison);
    }

    /**
     * Filter the query on the tahun column
     *
     * Example usage:
     * <code>
     * $query->filterByTahun(1234); // WHERE tahun = 1234
     * $query->filterByTahun(array(12, 34)); // WHERE tahun IN (12, 34)
     * $query->filterByTahun(array('min' => 12)); // WHERE tahun >= 12
     * $query->filterByTahun(array('max' => 12)); // WHERE tahun <= 12
     * </code>
     *
     * @param     mixed $tahun The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BlockgrantQuery The current query, for fluid interface
     */
    public function filterByTahun($tahun = null, $comparison = null)
    {
        if (is_array($tahun)) {
            $useMinMax = false;
            if (isset($tahun['min'])) {
                $this->addUsingAlias(BlockgrantPeer::TAHUN, $tahun['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tahun['max'])) {
                $this->addUsingAlias(BlockgrantPeer::TAHUN, $tahun['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BlockgrantPeer::TAHUN, $tahun, $comparison);
    }

    /**
     * Filter the query on the jenis_bantuan_id column
     *
     * Example usage:
     * <code>
     * $query->filterByJenisBantuanId(1234); // WHERE jenis_bantuan_id = 1234
     * $query->filterByJenisBantuanId(array(12, 34)); // WHERE jenis_bantuan_id IN (12, 34)
     * $query->filterByJenisBantuanId(array('min' => 12)); // WHERE jenis_bantuan_id >= 12
     * $query->filterByJenisBantuanId(array('max' => 12)); // WHERE jenis_bantuan_id <= 12
     * </code>
     *
     * @see       filterByJenisBantuanRelatedByJenisBantuanId()
     *
     * @see       filterByJenisBantuanRelatedByJenisBantuanId()
     *
     * @param     mixed $jenisBantuanId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BlockgrantQuery The current query, for fluid interface
     */
    public function filterByJenisBantuanId($jenisBantuanId = null, $comparison = null)
    {
        if (is_array($jenisBantuanId)) {
            $useMinMax = false;
            if (isset($jenisBantuanId['min'])) {
                $this->addUsingAlias(BlockgrantPeer::JENIS_BANTUAN_ID, $jenisBantuanId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jenisBantuanId['max'])) {
                $this->addUsingAlias(BlockgrantPeer::JENIS_BANTUAN_ID, $jenisBantuanId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BlockgrantPeer::JENIS_BANTUAN_ID, $jenisBantuanId, $comparison);
    }

    /**
     * Filter the query on the sumber_dana_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySumberDanaId(1234); // WHERE sumber_dana_id = 1234
     * $query->filterBySumberDanaId(array(12, 34)); // WHERE sumber_dana_id IN (12, 34)
     * $query->filterBySumberDanaId(array('min' => 12)); // WHERE sumber_dana_id >= 12
     * $query->filterBySumberDanaId(array('max' => 12)); // WHERE sumber_dana_id <= 12
     * </code>
     *
     * @see       filterBySumberDanaRelatedBySumberDanaId()
     *
     * @see       filterBySumberDanaRelatedBySumberDanaId()
     *
     * @param     mixed $sumberDanaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BlockgrantQuery The current query, for fluid interface
     */
    public function filterBySumberDanaId($sumberDanaId = null, $comparison = null)
    {
        if (is_array($sumberDanaId)) {
            $useMinMax = false;
            if (isset($sumberDanaId['min'])) {
                $this->addUsingAlias(BlockgrantPeer::SUMBER_DANA_ID, $sumberDanaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sumberDanaId['max'])) {
                $this->addUsingAlias(BlockgrantPeer::SUMBER_DANA_ID, $sumberDanaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BlockgrantPeer::SUMBER_DANA_ID, $sumberDanaId, $comparison);
    }

    /**
     * Filter the query on the besar_bantuan column
     *
     * Example usage:
     * <code>
     * $query->filterByBesarBantuan(1234); // WHERE besar_bantuan = 1234
     * $query->filterByBesarBantuan(array(12, 34)); // WHERE besar_bantuan IN (12, 34)
     * $query->filterByBesarBantuan(array('min' => 12)); // WHERE besar_bantuan >= 12
     * $query->filterByBesarBantuan(array('max' => 12)); // WHERE besar_bantuan <= 12
     * </code>
     *
     * @param     mixed $besarBantuan The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BlockgrantQuery The current query, for fluid interface
     */
    public function filterByBesarBantuan($besarBantuan = null, $comparison = null)
    {
        if (is_array($besarBantuan)) {
            $useMinMax = false;
            if (isset($besarBantuan['min'])) {
                $this->addUsingAlias(BlockgrantPeer::BESAR_BANTUAN, $besarBantuan['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($besarBantuan['max'])) {
                $this->addUsingAlias(BlockgrantPeer::BESAR_BANTUAN, $besarBantuan['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BlockgrantPeer::BESAR_BANTUAN, $besarBantuan, $comparison);
    }

    /**
     * Filter the query on the dana_pendamping column
     *
     * Example usage:
     * <code>
     * $query->filterByDanaPendamping(1234); // WHERE dana_pendamping = 1234
     * $query->filterByDanaPendamping(array(12, 34)); // WHERE dana_pendamping IN (12, 34)
     * $query->filterByDanaPendamping(array('min' => 12)); // WHERE dana_pendamping >= 12
     * $query->filterByDanaPendamping(array('max' => 12)); // WHERE dana_pendamping <= 12
     * </code>
     *
     * @param     mixed $danaPendamping The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BlockgrantQuery The current query, for fluid interface
     */
    public function filterByDanaPendamping($danaPendamping = null, $comparison = null)
    {
        if (is_array($danaPendamping)) {
            $useMinMax = false;
            if (isset($danaPendamping['min'])) {
                $this->addUsingAlias(BlockgrantPeer::DANA_PENDAMPING, $danaPendamping['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($danaPendamping['max'])) {
                $this->addUsingAlias(BlockgrantPeer::DANA_PENDAMPING, $danaPendamping['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BlockgrantPeer::DANA_PENDAMPING, $danaPendamping, $comparison);
    }

    /**
     * Filter the query on the peruntukan_dana column
     *
     * Example usage:
     * <code>
     * $query->filterByPeruntukanDana('fooValue');   // WHERE peruntukan_dana = 'fooValue'
     * $query->filterByPeruntukanDana('%fooValue%'); // WHERE peruntukan_dana LIKE '%fooValue%'
     * </code>
     *
     * @param     string $peruntukanDana The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BlockgrantQuery The current query, for fluid interface
     */
    public function filterByPeruntukanDana($peruntukanDana = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($peruntukanDana)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $peruntukanDana)) {
                $peruntukanDana = str_replace('*', '%', $peruntukanDana);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BlockgrantPeer::PERUNTUKAN_DANA, $peruntukanDana, $comparison);
    }

    /**
     * Filter the query on the asal_data column
     *
     * Example usage:
     * <code>
     * $query->filterByAsalData('fooValue');   // WHERE asal_data = 'fooValue'
     * $query->filterByAsalData('%fooValue%'); // WHERE asal_data LIKE '%fooValue%'
     * </code>
     *
     * @param     string $asalData The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BlockgrantQuery The current query, for fluid interface
     */
    public function filterByAsalData($asalData = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($asalData)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $asalData)) {
                $asalData = str_replace('*', '%', $asalData);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BlockgrantPeer::ASAL_DATA, $asalData, $comparison);
    }

    /**
     * Filter the query on the Last_update column
     *
     * Example usage:
     * <code>
     * $query->filterByLastUpdate('2011-03-14'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate('now'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate(array('max' => 'yesterday')); // WHERE Last_update > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastUpdate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BlockgrantQuery The current query, for fluid interface
     */
    public function filterByLastUpdate($lastUpdate = null, $comparison = null)
    {
        if (is_array($lastUpdate)) {
            $useMinMax = false;
            if (isset($lastUpdate['min'])) {
                $this->addUsingAlias(BlockgrantPeer::LAST_UPDATE, $lastUpdate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastUpdate['max'])) {
                $this->addUsingAlias(BlockgrantPeer::LAST_UPDATE, $lastUpdate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BlockgrantPeer::LAST_UPDATE, $lastUpdate, $comparison);
    }

    /**
     * Filter the query on the Soft_delete column
     *
     * Example usage:
     * <code>
     * $query->filterBySoftDelete(1234); // WHERE Soft_delete = 1234
     * $query->filterBySoftDelete(array(12, 34)); // WHERE Soft_delete IN (12, 34)
     * $query->filterBySoftDelete(array('min' => 12)); // WHERE Soft_delete >= 12
     * $query->filterBySoftDelete(array('max' => 12)); // WHERE Soft_delete <= 12
     * </code>
     *
     * @param     mixed $softDelete The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BlockgrantQuery The current query, for fluid interface
     */
    public function filterBySoftDelete($softDelete = null, $comparison = null)
    {
        if (is_array($softDelete)) {
            $useMinMax = false;
            if (isset($softDelete['min'])) {
                $this->addUsingAlias(BlockgrantPeer::SOFT_DELETE, $softDelete['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($softDelete['max'])) {
                $this->addUsingAlias(BlockgrantPeer::SOFT_DELETE, $softDelete['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BlockgrantPeer::SOFT_DELETE, $softDelete, $comparison);
    }

    /**
     * Filter the query on the last_sync column
     *
     * Example usage:
     * <code>
     * $query->filterByLastSync('2011-03-14'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync('now'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync(array('max' => 'yesterday')); // WHERE last_sync > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastSync The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BlockgrantQuery The current query, for fluid interface
     */
    public function filterByLastSync($lastSync = null, $comparison = null)
    {
        if (is_array($lastSync)) {
            $useMinMax = false;
            if (isset($lastSync['min'])) {
                $this->addUsingAlias(BlockgrantPeer::LAST_SYNC, $lastSync['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastSync['max'])) {
                $this->addUsingAlias(BlockgrantPeer::LAST_SYNC, $lastSync['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BlockgrantPeer::LAST_SYNC, $lastSync, $comparison);
    }

    /**
     * Filter the query on the Updater_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdaterId('fooValue');   // WHERE Updater_ID = 'fooValue'
     * $query->filterByUpdaterId('%fooValue%'); // WHERE Updater_ID LIKE '%fooValue%'
     * </code>
     *
     * @param     string $updaterId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BlockgrantQuery The current query, for fluid interface
     */
    public function filterByUpdaterId($updaterId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($updaterId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $updaterId)) {
                $updaterId = str_replace('*', '%', $updaterId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BlockgrantPeer::UPDATER_ID, $updaterId, $comparison);
    }

    /**
     * Filter the query by a related Sekolah object
     *
     * @param   Sekolah|PropelObjectCollection $sekolah The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 BlockgrantQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySekolahRelatedBySekolahId($sekolah, $comparison = null)
    {
        if ($sekolah instanceof Sekolah) {
            return $this
                ->addUsingAlias(BlockgrantPeer::SEKOLAH_ID, $sekolah->getSekolahId(), $comparison);
        } elseif ($sekolah instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BlockgrantPeer::SEKOLAH_ID, $sekolah->toKeyValue('PrimaryKey', 'SekolahId'), $comparison);
        } else {
            throw new PropelException('filterBySekolahRelatedBySekolahId() only accepts arguments of type Sekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SekolahRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return BlockgrantQuery The current query, for fluid interface
     */
    public function joinSekolahRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SekolahRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SekolahRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the SekolahRelatedBySekolahId relation Sekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SekolahQuery A secondary query class using the current class as primary query
     */
    public function useSekolahRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSekolahRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SekolahRelatedBySekolahId', '\angulex\Model\SekolahQuery');
    }

    /**
     * Filter the query by a related Sekolah object
     *
     * @param   Sekolah|PropelObjectCollection $sekolah The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 BlockgrantQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySekolahRelatedBySekolahId($sekolah, $comparison = null)
    {
        if ($sekolah instanceof Sekolah) {
            return $this
                ->addUsingAlias(BlockgrantPeer::SEKOLAH_ID, $sekolah->getSekolahId(), $comparison);
        } elseif ($sekolah instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BlockgrantPeer::SEKOLAH_ID, $sekolah->toKeyValue('PrimaryKey', 'SekolahId'), $comparison);
        } else {
            throw new PropelException('filterBySekolahRelatedBySekolahId() only accepts arguments of type Sekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SekolahRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return BlockgrantQuery The current query, for fluid interface
     */
    public function joinSekolahRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SekolahRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SekolahRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the SekolahRelatedBySekolahId relation Sekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SekolahQuery A secondary query class using the current class as primary query
     */
    public function useSekolahRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSekolahRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SekolahRelatedBySekolahId', '\angulex\Model\SekolahQuery');
    }

    /**
     * Filter the query by a related JenisBantuan object
     *
     * @param   JenisBantuan|PropelObjectCollection $jenisBantuan The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 BlockgrantQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJenisBantuanRelatedByJenisBantuanId($jenisBantuan, $comparison = null)
    {
        if ($jenisBantuan instanceof JenisBantuan) {
            return $this
                ->addUsingAlias(BlockgrantPeer::JENIS_BANTUAN_ID, $jenisBantuan->getJenisBantuanId(), $comparison);
        } elseif ($jenisBantuan instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BlockgrantPeer::JENIS_BANTUAN_ID, $jenisBantuan->toKeyValue('PrimaryKey', 'JenisBantuanId'), $comparison);
        } else {
            throw new PropelException('filterByJenisBantuanRelatedByJenisBantuanId() only accepts arguments of type JenisBantuan or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the JenisBantuanRelatedByJenisBantuanId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return BlockgrantQuery The current query, for fluid interface
     */
    public function joinJenisBantuanRelatedByJenisBantuanId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('JenisBantuanRelatedByJenisBantuanId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'JenisBantuanRelatedByJenisBantuanId');
        }

        return $this;
    }

    /**
     * Use the JenisBantuanRelatedByJenisBantuanId relation JenisBantuan object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\JenisBantuanQuery A secondary query class using the current class as primary query
     */
    public function useJenisBantuanRelatedByJenisBantuanIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinJenisBantuanRelatedByJenisBantuanId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'JenisBantuanRelatedByJenisBantuanId', '\angulex\Model\JenisBantuanQuery');
    }

    /**
     * Filter the query by a related JenisBantuan object
     *
     * @param   JenisBantuan|PropelObjectCollection $jenisBantuan The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 BlockgrantQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJenisBantuanRelatedByJenisBantuanId($jenisBantuan, $comparison = null)
    {
        if ($jenisBantuan instanceof JenisBantuan) {
            return $this
                ->addUsingAlias(BlockgrantPeer::JENIS_BANTUAN_ID, $jenisBantuan->getJenisBantuanId(), $comparison);
        } elseif ($jenisBantuan instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BlockgrantPeer::JENIS_BANTUAN_ID, $jenisBantuan->toKeyValue('PrimaryKey', 'JenisBantuanId'), $comparison);
        } else {
            throw new PropelException('filterByJenisBantuanRelatedByJenisBantuanId() only accepts arguments of type JenisBantuan or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the JenisBantuanRelatedByJenisBantuanId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return BlockgrantQuery The current query, for fluid interface
     */
    public function joinJenisBantuanRelatedByJenisBantuanId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('JenisBantuanRelatedByJenisBantuanId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'JenisBantuanRelatedByJenisBantuanId');
        }

        return $this;
    }

    /**
     * Use the JenisBantuanRelatedByJenisBantuanId relation JenisBantuan object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\JenisBantuanQuery A secondary query class using the current class as primary query
     */
    public function useJenisBantuanRelatedByJenisBantuanIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinJenisBantuanRelatedByJenisBantuanId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'JenisBantuanRelatedByJenisBantuanId', '\angulex\Model\JenisBantuanQuery');
    }

    /**
     * Filter the query by a related SumberDana object
     *
     * @param   SumberDana|PropelObjectCollection $sumberDana The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 BlockgrantQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySumberDanaRelatedBySumberDanaId($sumberDana, $comparison = null)
    {
        if ($sumberDana instanceof SumberDana) {
            return $this
                ->addUsingAlias(BlockgrantPeer::SUMBER_DANA_ID, $sumberDana->getSumberDanaId(), $comparison);
        } elseif ($sumberDana instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BlockgrantPeer::SUMBER_DANA_ID, $sumberDana->toKeyValue('PrimaryKey', 'SumberDanaId'), $comparison);
        } else {
            throw new PropelException('filterBySumberDanaRelatedBySumberDanaId() only accepts arguments of type SumberDana or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SumberDanaRelatedBySumberDanaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return BlockgrantQuery The current query, for fluid interface
     */
    public function joinSumberDanaRelatedBySumberDanaId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SumberDanaRelatedBySumberDanaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SumberDanaRelatedBySumberDanaId');
        }

        return $this;
    }

    /**
     * Use the SumberDanaRelatedBySumberDanaId relation SumberDana object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SumberDanaQuery A secondary query class using the current class as primary query
     */
    public function useSumberDanaRelatedBySumberDanaIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSumberDanaRelatedBySumberDanaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SumberDanaRelatedBySumberDanaId', '\angulex\Model\SumberDanaQuery');
    }

    /**
     * Filter the query by a related SumberDana object
     *
     * @param   SumberDana|PropelObjectCollection $sumberDana The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 BlockgrantQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySumberDanaRelatedBySumberDanaId($sumberDana, $comparison = null)
    {
        if ($sumberDana instanceof SumberDana) {
            return $this
                ->addUsingAlias(BlockgrantPeer::SUMBER_DANA_ID, $sumberDana->getSumberDanaId(), $comparison);
        } elseif ($sumberDana instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BlockgrantPeer::SUMBER_DANA_ID, $sumberDana->toKeyValue('PrimaryKey', 'SumberDanaId'), $comparison);
        } else {
            throw new PropelException('filterBySumberDanaRelatedBySumberDanaId() only accepts arguments of type SumberDana or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SumberDanaRelatedBySumberDanaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return BlockgrantQuery The current query, for fluid interface
     */
    public function joinSumberDanaRelatedBySumberDanaId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SumberDanaRelatedBySumberDanaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SumberDanaRelatedBySumberDanaId');
        }

        return $this;
    }

    /**
     * Use the SumberDanaRelatedBySumberDanaId relation SumberDana object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SumberDanaQuery A secondary query class using the current class as primary query
     */
    public function useSumberDanaRelatedBySumberDanaIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSumberDanaRelatedBySumberDanaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SumberDanaRelatedBySumberDanaId', '\angulex\Model\SumberDanaQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Blockgrant $blockgrant Object to remove from the list of results
     *
     * @return BlockgrantQuery The current query, for fluid interface
     */
    public function prune($blockgrant = null)
    {
        if ($blockgrant) {
            $this->addUsingAlias(BlockgrantPeer::BLOCKGRANT_ID, $blockgrant->getBlockgrantId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
