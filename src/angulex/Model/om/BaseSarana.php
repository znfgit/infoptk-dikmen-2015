<?php

namespace angulex\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelDateTime;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\JenisSarana;
use angulex\Model\JenisSaranaQuery;
use angulex\Model\Prasarana;
use angulex\Model\PrasaranaQuery;
use angulex\Model\Sarana;
use angulex\Model\SaranaLongitudinal;
use angulex\Model\SaranaLongitudinalQuery;
use angulex\Model\SaranaPeer;
use angulex\Model\SaranaQuery;
use angulex\Model\Sekolah;
use angulex\Model\SekolahQuery;
use angulex\Model\StatusKepemilikanSarpras;
use angulex\Model\StatusKepemilikanSarprasQuery;
use angulex\Model\VldSarana;
use angulex\Model\VldSaranaQuery;

/**
 * Base class that represents a row from the 'sarana' table.
 *
 * 
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseSarana extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'angulex\\Model\\SaranaPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        SaranaPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the sarana_id field.
     * @var        string
     */
    protected $sarana_id;

    /**
     * The value for the sekolah_id field.
     * @var        string
     */
    protected $sekolah_id;

    /**
     * The value for the jenis_sarana_id field.
     * @var        int
     */
    protected $jenis_sarana_id;

    /**
     * The value for the prasarana_id field.
     * @var        string
     */
    protected $prasarana_id;

    /**
     * The value for the kepemilikan_sarpras_id field.
     * @var        string
     */
    protected $kepemilikan_sarpras_id;

    /**
     * The value for the nama field.
     * @var        string
     */
    protected $nama;

    /**
     * The value for the last_update field.
     * @var        string
     */
    protected $last_update;

    /**
     * The value for the soft_delete field.
     * @var        string
     */
    protected $soft_delete;

    /**
     * The value for the last_sync field.
     * @var        string
     */
    protected $last_sync;

    /**
     * The value for the updater_id field.
     * @var        string
     */
    protected $updater_id;

    /**
     * @var        Prasarana
     */
    protected $aPrasaranaRelatedByPrasaranaId;

    /**
     * @var        Prasarana
     */
    protected $aPrasaranaRelatedByPrasaranaId;

    /**
     * @var        Sekolah
     */
    protected $aSekolahRelatedBySekolahId;

    /**
     * @var        Sekolah
     */
    protected $aSekolahRelatedBySekolahId;

    /**
     * @var        JenisSarana
     */
    protected $aJenisSaranaRelatedByJenisSaranaId;

    /**
     * @var        JenisSarana
     */
    protected $aJenisSaranaRelatedByJenisSaranaId;

    /**
     * @var        StatusKepemilikanSarpras
     */
    protected $aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId;

    /**
     * @var        StatusKepemilikanSarpras
     */
    protected $aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId;

    /**
     * @var        PropelObjectCollection|SaranaLongitudinal[] Collection to store aggregation of SaranaLongitudinal objects.
     */
    protected $collSaranaLongitudinalsRelatedBySaranaId;
    protected $collSaranaLongitudinalsRelatedBySaranaIdPartial;

    /**
     * @var        PropelObjectCollection|SaranaLongitudinal[] Collection to store aggregation of SaranaLongitudinal objects.
     */
    protected $collSaranaLongitudinalsRelatedBySaranaId;
    protected $collSaranaLongitudinalsRelatedBySaranaIdPartial;

    /**
     * @var        PropelObjectCollection|VldSarana[] Collection to store aggregation of VldSarana objects.
     */
    protected $collVldSaranasRelatedBySaranaId;
    protected $collVldSaranasRelatedBySaranaIdPartial;

    /**
     * @var        PropelObjectCollection|VldSarana[] Collection to store aggregation of VldSarana objects.
     */
    protected $collVldSaranasRelatedBySaranaId;
    protected $collVldSaranasRelatedBySaranaIdPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $saranaLongitudinalsRelatedBySaranaIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $saranaLongitudinalsRelatedBySaranaIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldSaranasRelatedBySaranaIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldSaranasRelatedBySaranaIdScheduledForDeletion = null;

    /**
     * Get the [sarana_id] column value.
     * 
     * @return string
     */
    public function getSaranaId()
    {
        return $this->sarana_id;
    }

    /**
     * Get the [sekolah_id] column value.
     * 
     * @return string
     */
    public function getSekolahId()
    {
        return $this->sekolah_id;
    }

    /**
     * Get the [jenis_sarana_id] column value.
     * 
     * @return int
     */
    public function getJenisSaranaId()
    {
        return $this->jenis_sarana_id;
    }

    /**
     * Get the [prasarana_id] column value.
     * 
     * @return string
     */
    public function getPrasaranaId()
    {
        return $this->prasarana_id;
    }

    /**
     * Get the [kepemilikan_sarpras_id] column value.
     * 
     * @return string
     */
    public function getKepemilikanSarprasId()
    {
        return $this->kepemilikan_sarpras_id;
    }

    /**
     * Get the [nama] column value.
     * 
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Get the [optionally formatted] temporal [last_update] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastUpdate($format = 'Y-m-d H:i:s')
    {
        if ($this->last_update === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_update);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_update, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [soft_delete] column value.
     * 
     * @return string
     */
    public function getSoftDelete()
    {
        return $this->soft_delete;
    }

    /**
     * Get the [optionally formatted] temporal [last_sync] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastSync($format = 'Y-m-d H:i:s')
    {
        if ($this->last_sync === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_sync);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_sync, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [updater_id] column value.
     * 
     * @return string
     */
    public function getUpdaterId()
    {
        return $this->updater_id;
    }

    /**
     * Set the value of [sarana_id] column.
     * 
     * @param string $v new value
     * @return Sarana The current object (for fluent API support)
     */
    public function setSaranaId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->sarana_id !== $v) {
            $this->sarana_id = $v;
            $this->modifiedColumns[] = SaranaPeer::SARANA_ID;
        }


        return $this;
    } // setSaranaId()

    /**
     * Set the value of [sekolah_id] column.
     * 
     * @param string $v new value
     * @return Sarana The current object (for fluent API support)
     */
    public function setSekolahId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->sekolah_id !== $v) {
            $this->sekolah_id = $v;
            $this->modifiedColumns[] = SaranaPeer::SEKOLAH_ID;
        }

        if ($this->aSekolahRelatedBySekolahId !== null && $this->aSekolahRelatedBySekolahId->getSekolahId() !== $v) {
            $this->aSekolahRelatedBySekolahId = null;
        }

        if ($this->aSekolahRelatedBySekolahId !== null && $this->aSekolahRelatedBySekolahId->getSekolahId() !== $v) {
            $this->aSekolahRelatedBySekolahId = null;
        }


        return $this;
    } // setSekolahId()

    /**
     * Set the value of [jenis_sarana_id] column.
     * 
     * @param int $v new value
     * @return Sarana The current object (for fluent API support)
     */
    public function setJenisSaranaId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->jenis_sarana_id !== $v) {
            $this->jenis_sarana_id = $v;
            $this->modifiedColumns[] = SaranaPeer::JENIS_SARANA_ID;
        }

        if ($this->aJenisSaranaRelatedByJenisSaranaId !== null && $this->aJenisSaranaRelatedByJenisSaranaId->getJenisSaranaId() !== $v) {
            $this->aJenisSaranaRelatedByJenisSaranaId = null;
        }

        if ($this->aJenisSaranaRelatedByJenisSaranaId !== null && $this->aJenisSaranaRelatedByJenisSaranaId->getJenisSaranaId() !== $v) {
            $this->aJenisSaranaRelatedByJenisSaranaId = null;
        }


        return $this;
    } // setJenisSaranaId()

    /**
     * Set the value of [prasarana_id] column.
     * 
     * @param string $v new value
     * @return Sarana The current object (for fluent API support)
     */
    public function setPrasaranaId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->prasarana_id !== $v) {
            $this->prasarana_id = $v;
            $this->modifiedColumns[] = SaranaPeer::PRASARANA_ID;
        }

        if ($this->aPrasaranaRelatedByPrasaranaId !== null && $this->aPrasaranaRelatedByPrasaranaId->getPrasaranaId() !== $v) {
            $this->aPrasaranaRelatedByPrasaranaId = null;
        }

        if ($this->aPrasaranaRelatedByPrasaranaId !== null && $this->aPrasaranaRelatedByPrasaranaId->getPrasaranaId() !== $v) {
            $this->aPrasaranaRelatedByPrasaranaId = null;
        }


        return $this;
    } // setPrasaranaId()

    /**
     * Set the value of [kepemilikan_sarpras_id] column.
     * 
     * @param string $v new value
     * @return Sarana The current object (for fluent API support)
     */
    public function setKepemilikanSarprasId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->kepemilikan_sarpras_id !== $v) {
            $this->kepemilikan_sarpras_id = $v;
            $this->modifiedColumns[] = SaranaPeer::KEPEMILIKAN_SARPRAS_ID;
        }

        if ($this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId !== null && $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId->getKepemilikanSarprasId() !== $v) {
            $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId = null;
        }

        if ($this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId !== null && $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId->getKepemilikanSarprasId() !== $v) {
            $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId = null;
        }


        return $this;
    } // setKepemilikanSarprasId()

    /**
     * Set the value of [nama] column.
     * 
     * @param string $v new value
     * @return Sarana The current object (for fluent API support)
     */
    public function setNama($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nama !== $v) {
            $this->nama = $v;
            $this->modifiedColumns[] = SaranaPeer::NAMA;
        }


        return $this;
    } // setNama()

    /**
     * Sets the value of [last_update] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Sarana The current object (for fluent API support)
     */
    public function setLastUpdate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_update !== null || $dt !== null) {
            $currentDateAsString = ($this->last_update !== null && $tmpDt = new DateTime($this->last_update)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_update = $newDateAsString;
                $this->modifiedColumns[] = SaranaPeer::LAST_UPDATE;
            }
        } // if either are not null


        return $this;
    } // setLastUpdate()

    /**
     * Set the value of [soft_delete] column.
     * 
     * @param string $v new value
     * @return Sarana The current object (for fluent API support)
     */
    public function setSoftDelete($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->soft_delete !== $v) {
            $this->soft_delete = $v;
            $this->modifiedColumns[] = SaranaPeer::SOFT_DELETE;
        }


        return $this;
    } // setSoftDelete()

    /**
     * Sets the value of [last_sync] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Sarana The current object (for fluent API support)
     */
    public function setLastSync($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_sync !== null || $dt !== null) {
            $currentDateAsString = ($this->last_sync !== null && $tmpDt = new DateTime($this->last_sync)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_sync = $newDateAsString;
                $this->modifiedColumns[] = SaranaPeer::LAST_SYNC;
            }
        } // if either are not null


        return $this;
    } // setLastSync()

    /**
     * Set the value of [updater_id] column.
     * 
     * @param string $v new value
     * @return Sarana The current object (for fluent API support)
     */
    public function setUpdaterId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->updater_id !== $v) {
            $this->updater_id = $v;
            $this->modifiedColumns[] = SaranaPeer::UPDATER_ID;
        }


        return $this;
    } // setUpdaterId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->sarana_id = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
            $this->sekolah_id = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->jenis_sarana_id = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->prasarana_id = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->kepemilikan_sarpras_id = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->nama = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->last_update = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->soft_delete = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->last_sync = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->updater_id = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 10; // 10 = SaranaPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Sarana object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aSekolahRelatedBySekolahId !== null && $this->sekolah_id !== $this->aSekolahRelatedBySekolahId->getSekolahId()) {
            $this->aSekolahRelatedBySekolahId = null;
        }
        if ($this->aSekolahRelatedBySekolahId !== null && $this->sekolah_id !== $this->aSekolahRelatedBySekolahId->getSekolahId()) {
            $this->aSekolahRelatedBySekolahId = null;
        }
        if ($this->aJenisSaranaRelatedByJenisSaranaId !== null && $this->jenis_sarana_id !== $this->aJenisSaranaRelatedByJenisSaranaId->getJenisSaranaId()) {
            $this->aJenisSaranaRelatedByJenisSaranaId = null;
        }
        if ($this->aJenisSaranaRelatedByJenisSaranaId !== null && $this->jenis_sarana_id !== $this->aJenisSaranaRelatedByJenisSaranaId->getJenisSaranaId()) {
            $this->aJenisSaranaRelatedByJenisSaranaId = null;
        }
        if ($this->aPrasaranaRelatedByPrasaranaId !== null && $this->prasarana_id !== $this->aPrasaranaRelatedByPrasaranaId->getPrasaranaId()) {
            $this->aPrasaranaRelatedByPrasaranaId = null;
        }
        if ($this->aPrasaranaRelatedByPrasaranaId !== null && $this->prasarana_id !== $this->aPrasaranaRelatedByPrasaranaId->getPrasaranaId()) {
            $this->aPrasaranaRelatedByPrasaranaId = null;
        }
        if ($this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId !== null && $this->kepemilikan_sarpras_id !== $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId->getKepemilikanSarprasId()) {
            $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId = null;
        }
        if ($this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId !== null && $this->kepemilikan_sarpras_id !== $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId->getKepemilikanSarprasId()) {
            $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SaranaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = SaranaPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aPrasaranaRelatedByPrasaranaId = null;
            $this->aPrasaranaRelatedByPrasaranaId = null;
            $this->aSekolahRelatedBySekolahId = null;
            $this->aSekolahRelatedBySekolahId = null;
            $this->aJenisSaranaRelatedByJenisSaranaId = null;
            $this->aJenisSaranaRelatedByJenisSaranaId = null;
            $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId = null;
            $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId = null;
            $this->collSaranaLongitudinalsRelatedBySaranaId = null;

            $this->collSaranaLongitudinalsRelatedBySaranaId = null;

            $this->collVldSaranasRelatedBySaranaId = null;

            $this->collVldSaranasRelatedBySaranaId = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SaranaPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = SaranaQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SaranaPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                SaranaPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPrasaranaRelatedByPrasaranaId !== null) {
                if ($this->aPrasaranaRelatedByPrasaranaId->isModified() || $this->aPrasaranaRelatedByPrasaranaId->isNew()) {
                    $affectedRows += $this->aPrasaranaRelatedByPrasaranaId->save($con);
                }
                $this->setPrasaranaRelatedByPrasaranaId($this->aPrasaranaRelatedByPrasaranaId);
            }

            if ($this->aPrasaranaRelatedByPrasaranaId !== null) {
                if ($this->aPrasaranaRelatedByPrasaranaId->isModified() || $this->aPrasaranaRelatedByPrasaranaId->isNew()) {
                    $affectedRows += $this->aPrasaranaRelatedByPrasaranaId->save($con);
                }
                $this->setPrasaranaRelatedByPrasaranaId($this->aPrasaranaRelatedByPrasaranaId);
            }

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if ($this->aSekolahRelatedBySekolahId->isModified() || $this->aSekolahRelatedBySekolahId->isNew()) {
                    $affectedRows += $this->aSekolahRelatedBySekolahId->save($con);
                }
                $this->setSekolahRelatedBySekolahId($this->aSekolahRelatedBySekolahId);
            }

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if ($this->aSekolahRelatedBySekolahId->isModified() || $this->aSekolahRelatedBySekolahId->isNew()) {
                    $affectedRows += $this->aSekolahRelatedBySekolahId->save($con);
                }
                $this->setSekolahRelatedBySekolahId($this->aSekolahRelatedBySekolahId);
            }

            if ($this->aJenisSaranaRelatedByJenisSaranaId !== null) {
                if ($this->aJenisSaranaRelatedByJenisSaranaId->isModified() || $this->aJenisSaranaRelatedByJenisSaranaId->isNew()) {
                    $affectedRows += $this->aJenisSaranaRelatedByJenisSaranaId->save($con);
                }
                $this->setJenisSaranaRelatedByJenisSaranaId($this->aJenisSaranaRelatedByJenisSaranaId);
            }

            if ($this->aJenisSaranaRelatedByJenisSaranaId !== null) {
                if ($this->aJenisSaranaRelatedByJenisSaranaId->isModified() || $this->aJenisSaranaRelatedByJenisSaranaId->isNew()) {
                    $affectedRows += $this->aJenisSaranaRelatedByJenisSaranaId->save($con);
                }
                $this->setJenisSaranaRelatedByJenisSaranaId($this->aJenisSaranaRelatedByJenisSaranaId);
            }

            if ($this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId !== null) {
                if ($this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId->isModified() || $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId->isNew()) {
                    $affectedRows += $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId->save($con);
                }
                $this->setStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId);
            }

            if ($this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId !== null) {
                if ($this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId->isModified() || $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId->isNew()) {
                    $affectedRows += $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId->save($con);
                }
                $this->setStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->saranaLongitudinalsRelatedBySaranaIdScheduledForDeletion !== null) {
                if (!$this->saranaLongitudinalsRelatedBySaranaIdScheduledForDeletion->isEmpty()) {
                    SaranaLongitudinalQuery::create()
                        ->filterByPrimaryKeys($this->saranaLongitudinalsRelatedBySaranaIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->saranaLongitudinalsRelatedBySaranaIdScheduledForDeletion = null;
                }
            }

            if ($this->collSaranaLongitudinalsRelatedBySaranaId !== null) {
                foreach ($this->collSaranaLongitudinalsRelatedBySaranaId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->saranaLongitudinalsRelatedBySaranaIdScheduledForDeletion !== null) {
                if (!$this->saranaLongitudinalsRelatedBySaranaIdScheduledForDeletion->isEmpty()) {
                    SaranaLongitudinalQuery::create()
                        ->filterByPrimaryKeys($this->saranaLongitudinalsRelatedBySaranaIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->saranaLongitudinalsRelatedBySaranaIdScheduledForDeletion = null;
                }
            }

            if ($this->collSaranaLongitudinalsRelatedBySaranaId !== null) {
                foreach ($this->collSaranaLongitudinalsRelatedBySaranaId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldSaranasRelatedBySaranaIdScheduledForDeletion !== null) {
                if (!$this->vldSaranasRelatedBySaranaIdScheduledForDeletion->isEmpty()) {
                    VldSaranaQuery::create()
                        ->filterByPrimaryKeys($this->vldSaranasRelatedBySaranaIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldSaranasRelatedBySaranaIdScheduledForDeletion = null;
                }
            }

            if ($this->collVldSaranasRelatedBySaranaId !== null) {
                foreach ($this->collVldSaranasRelatedBySaranaId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldSaranasRelatedBySaranaIdScheduledForDeletion !== null) {
                if (!$this->vldSaranasRelatedBySaranaIdScheduledForDeletion->isEmpty()) {
                    VldSaranaQuery::create()
                        ->filterByPrimaryKeys($this->vldSaranasRelatedBySaranaIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldSaranasRelatedBySaranaIdScheduledForDeletion = null;
                }
            }

            if ($this->collVldSaranasRelatedBySaranaId !== null) {
                foreach ($this->collVldSaranasRelatedBySaranaId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $criteria = $this->buildCriteria();
        $pk = BasePeer::doInsert($criteria, $con);
        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPrasaranaRelatedByPrasaranaId !== null) {
                if (!$this->aPrasaranaRelatedByPrasaranaId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPrasaranaRelatedByPrasaranaId->getValidationFailures());
                }
            }

            if ($this->aPrasaranaRelatedByPrasaranaId !== null) {
                if (!$this->aPrasaranaRelatedByPrasaranaId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPrasaranaRelatedByPrasaranaId->getValidationFailures());
                }
            }

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if (!$this->aSekolahRelatedBySekolahId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSekolahRelatedBySekolahId->getValidationFailures());
                }
            }

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if (!$this->aSekolahRelatedBySekolahId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSekolahRelatedBySekolahId->getValidationFailures());
                }
            }

            if ($this->aJenisSaranaRelatedByJenisSaranaId !== null) {
                if (!$this->aJenisSaranaRelatedByJenisSaranaId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aJenisSaranaRelatedByJenisSaranaId->getValidationFailures());
                }
            }

            if ($this->aJenisSaranaRelatedByJenisSaranaId !== null) {
                if (!$this->aJenisSaranaRelatedByJenisSaranaId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aJenisSaranaRelatedByJenisSaranaId->getValidationFailures());
                }
            }

            if ($this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId !== null) {
                if (!$this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId->getValidationFailures());
                }
            }

            if ($this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId !== null) {
                if (!$this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId->getValidationFailures());
                }
            }


            if (($retval = SaranaPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collSaranaLongitudinalsRelatedBySaranaId !== null) {
                    foreach ($this->collSaranaLongitudinalsRelatedBySaranaId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSaranaLongitudinalsRelatedBySaranaId !== null) {
                    foreach ($this->collSaranaLongitudinalsRelatedBySaranaId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldSaranasRelatedBySaranaId !== null) {
                    foreach ($this->collVldSaranasRelatedBySaranaId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldSaranasRelatedBySaranaId !== null) {
                    foreach ($this->collVldSaranasRelatedBySaranaId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SaranaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getSaranaId();
                break;
            case 1:
                return $this->getSekolahId();
                break;
            case 2:
                return $this->getJenisSaranaId();
                break;
            case 3:
                return $this->getPrasaranaId();
                break;
            case 4:
                return $this->getKepemilikanSarprasId();
                break;
            case 5:
                return $this->getNama();
                break;
            case 6:
                return $this->getLastUpdate();
                break;
            case 7:
                return $this->getSoftDelete();
                break;
            case 8:
                return $this->getLastSync();
                break;
            case 9:
                return $this->getUpdaterId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Sarana'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Sarana'][$this->getPrimaryKey()] = true;
        $keys = SaranaPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getSaranaId(),
            $keys[1] => $this->getSekolahId(),
            $keys[2] => $this->getJenisSaranaId(),
            $keys[3] => $this->getPrasaranaId(),
            $keys[4] => $this->getKepemilikanSarprasId(),
            $keys[5] => $this->getNama(),
            $keys[6] => $this->getLastUpdate(),
            $keys[7] => $this->getSoftDelete(),
            $keys[8] => $this->getLastSync(),
            $keys[9] => $this->getUpdaterId(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aPrasaranaRelatedByPrasaranaId) {
                $result['PrasaranaRelatedByPrasaranaId'] = $this->aPrasaranaRelatedByPrasaranaId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPrasaranaRelatedByPrasaranaId) {
                $result['PrasaranaRelatedByPrasaranaId'] = $this->aPrasaranaRelatedByPrasaranaId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSekolahRelatedBySekolahId) {
                $result['SekolahRelatedBySekolahId'] = $this->aSekolahRelatedBySekolahId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSekolahRelatedBySekolahId) {
                $result['SekolahRelatedBySekolahId'] = $this->aSekolahRelatedBySekolahId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aJenisSaranaRelatedByJenisSaranaId) {
                $result['JenisSaranaRelatedByJenisSaranaId'] = $this->aJenisSaranaRelatedByJenisSaranaId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aJenisSaranaRelatedByJenisSaranaId) {
                $result['JenisSaranaRelatedByJenisSaranaId'] = $this->aJenisSaranaRelatedByJenisSaranaId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId) {
                $result['StatusKepemilikanSarprasRelatedByKepemilikanSarprasId'] = $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId) {
                $result['StatusKepemilikanSarprasRelatedByKepemilikanSarprasId'] = $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collSaranaLongitudinalsRelatedBySaranaId) {
                $result['SaranaLongitudinalsRelatedBySaranaId'] = $this->collSaranaLongitudinalsRelatedBySaranaId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSaranaLongitudinalsRelatedBySaranaId) {
                $result['SaranaLongitudinalsRelatedBySaranaId'] = $this->collSaranaLongitudinalsRelatedBySaranaId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldSaranasRelatedBySaranaId) {
                $result['VldSaranasRelatedBySaranaId'] = $this->collVldSaranasRelatedBySaranaId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldSaranasRelatedBySaranaId) {
                $result['VldSaranasRelatedBySaranaId'] = $this->collVldSaranasRelatedBySaranaId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SaranaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setSaranaId($value);
                break;
            case 1:
                $this->setSekolahId($value);
                break;
            case 2:
                $this->setJenisSaranaId($value);
                break;
            case 3:
                $this->setPrasaranaId($value);
                break;
            case 4:
                $this->setKepemilikanSarprasId($value);
                break;
            case 5:
                $this->setNama($value);
                break;
            case 6:
                $this->setLastUpdate($value);
                break;
            case 7:
                $this->setSoftDelete($value);
                break;
            case 8:
                $this->setLastSync($value);
                break;
            case 9:
                $this->setUpdaterId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = SaranaPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setSaranaId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setSekolahId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setJenisSaranaId($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setPrasaranaId($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setKepemilikanSarprasId($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setNama($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setLastUpdate($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setSoftDelete($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setLastSync($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setUpdaterId($arr[$keys[9]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(SaranaPeer::DATABASE_NAME);

        if ($this->isColumnModified(SaranaPeer::SARANA_ID)) $criteria->add(SaranaPeer::SARANA_ID, $this->sarana_id);
        if ($this->isColumnModified(SaranaPeer::SEKOLAH_ID)) $criteria->add(SaranaPeer::SEKOLAH_ID, $this->sekolah_id);
        if ($this->isColumnModified(SaranaPeer::JENIS_SARANA_ID)) $criteria->add(SaranaPeer::JENIS_SARANA_ID, $this->jenis_sarana_id);
        if ($this->isColumnModified(SaranaPeer::PRASARANA_ID)) $criteria->add(SaranaPeer::PRASARANA_ID, $this->prasarana_id);
        if ($this->isColumnModified(SaranaPeer::KEPEMILIKAN_SARPRAS_ID)) $criteria->add(SaranaPeer::KEPEMILIKAN_SARPRAS_ID, $this->kepemilikan_sarpras_id);
        if ($this->isColumnModified(SaranaPeer::NAMA)) $criteria->add(SaranaPeer::NAMA, $this->nama);
        if ($this->isColumnModified(SaranaPeer::LAST_UPDATE)) $criteria->add(SaranaPeer::LAST_UPDATE, $this->last_update);
        if ($this->isColumnModified(SaranaPeer::SOFT_DELETE)) $criteria->add(SaranaPeer::SOFT_DELETE, $this->soft_delete);
        if ($this->isColumnModified(SaranaPeer::LAST_SYNC)) $criteria->add(SaranaPeer::LAST_SYNC, $this->last_sync);
        if ($this->isColumnModified(SaranaPeer::UPDATER_ID)) $criteria->add(SaranaPeer::UPDATER_ID, $this->updater_id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(SaranaPeer::DATABASE_NAME);
        $criteria->add(SaranaPeer::SARANA_ID, $this->sarana_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getSaranaId();
    }

    /**
     * Generic method to set the primary key (sarana_id column).
     *
     * @param  string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setSaranaId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getSaranaId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Sarana (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setSekolahId($this->getSekolahId());
        $copyObj->setJenisSaranaId($this->getJenisSaranaId());
        $copyObj->setPrasaranaId($this->getPrasaranaId());
        $copyObj->setKepemilikanSarprasId($this->getKepemilikanSarprasId());
        $copyObj->setNama($this->getNama());
        $copyObj->setLastUpdate($this->getLastUpdate());
        $copyObj->setSoftDelete($this->getSoftDelete());
        $copyObj->setLastSync($this->getLastSync());
        $copyObj->setUpdaterId($this->getUpdaterId());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getSaranaLongitudinalsRelatedBySaranaId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSaranaLongitudinalRelatedBySaranaId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSaranaLongitudinalsRelatedBySaranaId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSaranaLongitudinalRelatedBySaranaId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldSaranasRelatedBySaranaId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldSaranaRelatedBySaranaId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldSaranasRelatedBySaranaId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldSaranaRelatedBySaranaId($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setSaranaId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Sarana Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return SaranaPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new SaranaPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a Prasarana object.
     *
     * @param             Prasarana $v
     * @return Sarana The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPrasaranaRelatedByPrasaranaId(Prasarana $v = null)
    {
        if ($v === null) {
            $this->setPrasaranaId(NULL);
        } else {
            $this->setPrasaranaId($v->getPrasaranaId());
        }

        $this->aPrasaranaRelatedByPrasaranaId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Prasarana object, it will not be re-added.
        if ($v !== null) {
            $v->addSaranaRelatedByPrasaranaId($this);
        }


        return $this;
    }


    /**
     * Get the associated Prasarana object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Prasarana The associated Prasarana object.
     * @throws PropelException
     */
    public function getPrasaranaRelatedByPrasaranaId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPrasaranaRelatedByPrasaranaId === null && (($this->prasarana_id !== "" && $this->prasarana_id !== null)) && $doQuery) {
            $this->aPrasaranaRelatedByPrasaranaId = PrasaranaQuery::create()->findPk($this->prasarana_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPrasaranaRelatedByPrasaranaId->addSaranasRelatedByPrasaranaId($this);
             */
        }

        return $this->aPrasaranaRelatedByPrasaranaId;
    }

    /**
     * Declares an association between this object and a Prasarana object.
     *
     * @param             Prasarana $v
     * @return Sarana The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPrasaranaRelatedByPrasaranaId(Prasarana $v = null)
    {
        if ($v === null) {
            $this->setPrasaranaId(NULL);
        } else {
            $this->setPrasaranaId($v->getPrasaranaId());
        }

        $this->aPrasaranaRelatedByPrasaranaId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Prasarana object, it will not be re-added.
        if ($v !== null) {
            $v->addSaranaRelatedByPrasaranaId($this);
        }


        return $this;
    }


    /**
     * Get the associated Prasarana object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Prasarana The associated Prasarana object.
     * @throws PropelException
     */
    public function getPrasaranaRelatedByPrasaranaId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPrasaranaRelatedByPrasaranaId === null && (($this->prasarana_id !== "" && $this->prasarana_id !== null)) && $doQuery) {
            $this->aPrasaranaRelatedByPrasaranaId = PrasaranaQuery::create()->findPk($this->prasarana_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPrasaranaRelatedByPrasaranaId->addSaranasRelatedByPrasaranaId($this);
             */
        }

        return $this->aPrasaranaRelatedByPrasaranaId;
    }

    /**
     * Declares an association between this object and a Sekolah object.
     *
     * @param             Sekolah $v
     * @return Sarana The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSekolahRelatedBySekolahId(Sekolah $v = null)
    {
        if ($v === null) {
            $this->setSekolahId(NULL);
        } else {
            $this->setSekolahId($v->getSekolahId());
        }

        $this->aSekolahRelatedBySekolahId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Sekolah object, it will not be re-added.
        if ($v !== null) {
            $v->addSaranaRelatedBySekolahId($this);
        }


        return $this;
    }


    /**
     * Get the associated Sekolah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Sekolah The associated Sekolah object.
     * @throws PropelException
     */
    public function getSekolahRelatedBySekolahId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSekolahRelatedBySekolahId === null && (($this->sekolah_id !== "" && $this->sekolah_id !== null)) && $doQuery) {
            $this->aSekolahRelatedBySekolahId = SekolahQuery::create()->findPk($this->sekolah_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSekolahRelatedBySekolahId->addSaranasRelatedBySekolahId($this);
             */
        }

        return $this->aSekolahRelatedBySekolahId;
    }

    /**
     * Declares an association between this object and a Sekolah object.
     *
     * @param             Sekolah $v
     * @return Sarana The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSekolahRelatedBySekolahId(Sekolah $v = null)
    {
        if ($v === null) {
            $this->setSekolahId(NULL);
        } else {
            $this->setSekolahId($v->getSekolahId());
        }

        $this->aSekolahRelatedBySekolahId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Sekolah object, it will not be re-added.
        if ($v !== null) {
            $v->addSaranaRelatedBySekolahId($this);
        }


        return $this;
    }


    /**
     * Get the associated Sekolah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Sekolah The associated Sekolah object.
     * @throws PropelException
     */
    public function getSekolahRelatedBySekolahId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSekolahRelatedBySekolahId === null && (($this->sekolah_id !== "" && $this->sekolah_id !== null)) && $doQuery) {
            $this->aSekolahRelatedBySekolahId = SekolahQuery::create()->findPk($this->sekolah_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSekolahRelatedBySekolahId->addSaranasRelatedBySekolahId($this);
             */
        }

        return $this->aSekolahRelatedBySekolahId;
    }

    /**
     * Declares an association between this object and a JenisSarana object.
     *
     * @param             JenisSarana $v
     * @return Sarana The current object (for fluent API support)
     * @throws PropelException
     */
    public function setJenisSaranaRelatedByJenisSaranaId(JenisSarana $v = null)
    {
        if ($v === null) {
            $this->setJenisSaranaId(NULL);
        } else {
            $this->setJenisSaranaId($v->getJenisSaranaId());
        }

        $this->aJenisSaranaRelatedByJenisSaranaId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the JenisSarana object, it will not be re-added.
        if ($v !== null) {
            $v->addSaranaRelatedByJenisSaranaId($this);
        }


        return $this;
    }


    /**
     * Get the associated JenisSarana object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return JenisSarana The associated JenisSarana object.
     * @throws PropelException
     */
    public function getJenisSaranaRelatedByJenisSaranaId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aJenisSaranaRelatedByJenisSaranaId === null && ($this->jenis_sarana_id !== null) && $doQuery) {
            $this->aJenisSaranaRelatedByJenisSaranaId = JenisSaranaQuery::create()->findPk($this->jenis_sarana_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aJenisSaranaRelatedByJenisSaranaId->addSaranasRelatedByJenisSaranaId($this);
             */
        }

        return $this->aJenisSaranaRelatedByJenisSaranaId;
    }

    /**
     * Declares an association between this object and a JenisSarana object.
     *
     * @param             JenisSarana $v
     * @return Sarana The current object (for fluent API support)
     * @throws PropelException
     */
    public function setJenisSaranaRelatedByJenisSaranaId(JenisSarana $v = null)
    {
        if ($v === null) {
            $this->setJenisSaranaId(NULL);
        } else {
            $this->setJenisSaranaId($v->getJenisSaranaId());
        }

        $this->aJenisSaranaRelatedByJenisSaranaId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the JenisSarana object, it will not be re-added.
        if ($v !== null) {
            $v->addSaranaRelatedByJenisSaranaId($this);
        }


        return $this;
    }


    /**
     * Get the associated JenisSarana object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return JenisSarana The associated JenisSarana object.
     * @throws PropelException
     */
    public function getJenisSaranaRelatedByJenisSaranaId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aJenisSaranaRelatedByJenisSaranaId === null && ($this->jenis_sarana_id !== null) && $doQuery) {
            $this->aJenisSaranaRelatedByJenisSaranaId = JenisSaranaQuery::create()->findPk($this->jenis_sarana_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aJenisSaranaRelatedByJenisSaranaId->addSaranasRelatedByJenisSaranaId($this);
             */
        }

        return $this->aJenisSaranaRelatedByJenisSaranaId;
    }

    /**
     * Declares an association between this object and a StatusKepemilikanSarpras object.
     *
     * @param             StatusKepemilikanSarpras $v
     * @return Sarana The current object (for fluent API support)
     * @throws PropelException
     */
    public function setStatusKepemilikanSarprasRelatedByKepemilikanSarprasId(StatusKepemilikanSarpras $v = null)
    {
        if ($v === null) {
            $this->setKepemilikanSarprasId(NULL);
        } else {
            $this->setKepemilikanSarprasId($v->getKepemilikanSarprasId());
        }

        $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the StatusKepemilikanSarpras object, it will not be re-added.
        if ($v !== null) {
            $v->addSaranaRelatedByKepemilikanSarprasId($this);
        }


        return $this;
    }


    /**
     * Get the associated StatusKepemilikanSarpras object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return StatusKepemilikanSarpras The associated StatusKepemilikanSarpras object.
     * @throws PropelException
     */
    public function getStatusKepemilikanSarprasRelatedByKepemilikanSarprasId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId === null && (($this->kepemilikan_sarpras_id !== "" && $this->kepemilikan_sarpras_id !== null)) && $doQuery) {
            $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId = StatusKepemilikanSarprasQuery::create()->findPk($this->kepemilikan_sarpras_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId->addSaranasRelatedByKepemilikanSarprasId($this);
             */
        }

        return $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId;
    }

    /**
     * Declares an association between this object and a StatusKepemilikanSarpras object.
     *
     * @param             StatusKepemilikanSarpras $v
     * @return Sarana The current object (for fluent API support)
     * @throws PropelException
     */
    public function setStatusKepemilikanSarprasRelatedByKepemilikanSarprasId(StatusKepemilikanSarpras $v = null)
    {
        if ($v === null) {
            $this->setKepemilikanSarprasId(NULL);
        } else {
            $this->setKepemilikanSarprasId($v->getKepemilikanSarprasId());
        }

        $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the StatusKepemilikanSarpras object, it will not be re-added.
        if ($v !== null) {
            $v->addSaranaRelatedByKepemilikanSarprasId($this);
        }


        return $this;
    }


    /**
     * Get the associated StatusKepemilikanSarpras object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return StatusKepemilikanSarpras The associated StatusKepemilikanSarpras object.
     * @throws PropelException
     */
    public function getStatusKepemilikanSarprasRelatedByKepemilikanSarprasId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId === null && (($this->kepemilikan_sarpras_id !== "" && $this->kepemilikan_sarpras_id !== null)) && $doQuery) {
            $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId = StatusKepemilikanSarprasQuery::create()->findPk($this->kepemilikan_sarpras_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId->addSaranasRelatedByKepemilikanSarprasId($this);
             */
        }

        return $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('SaranaLongitudinalRelatedBySaranaId' == $relationName) {
            $this->initSaranaLongitudinalsRelatedBySaranaId();
        }
        if ('SaranaLongitudinalRelatedBySaranaId' == $relationName) {
            $this->initSaranaLongitudinalsRelatedBySaranaId();
        }
        if ('VldSaranaRelatedBySaranaId' == $relationName) {
            $this->initVldSaranasRelatedBySaranaId();
        }
        if ('VldSaranaRelatedBySaranaId' == $relationName) {
            $this->initVldSaranasRelatedBySaranaId();
        }
    }

    /**
     * Clears out the collSaranaLongitudinalsRelatedBySaranaId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Sarana The current object (for fluent API support)
     * @see        addSaranaLongitudinalsRelatedBySaranaId()
     */
    public function clearSaranaLongitudinalsRelatedBySaranaId()
    {
        $this->collSaranaLongitudinalsRelatedBySaranaId = null; // important to set this to null since that means it is uninitialized
        $this->collSaranaLongitudinalsRelatedBySaranaIdPartial = null;

        return $this;
    }

    /**
     * reset is the collSaranaLongitudinalsRelatedBySaranaId collection loaded partially
     *
     * @return void
     */
    public function resetPartialSaranaLongitudinalsRelatedBySaranaId($v = true)
    {
        $this->collSaranaLongitudinalsRelatedBySaranaIdPartial = $v;
    }

    /**
     * Initializes the collSaranaLongitudinalsRelatedBySaranaId collection.
     *
     * By default this just sets the collSaranaLongitudinalsRelatedBySaranaId collection to an empty array (like clearcollSaranaLongitudinalsRelatedBySaranaId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSaranaLongitudinalsRelatedBySaranaId($overrideExisting = true)
    {
        if (null !== $this->collSaranaLongitudinalsRelatedBySaranaId && !$overrideExisting) {
            return;
        }
        $this->collSaranaLongitudinalsRelatedBySaranaId = new PropelObjectCollection();
        $this->collSaranaLongitudinalsRelatedBySaranaId->setModel('SaranaLongitudinal');
    }

    /**
     * Gets an array of SaranaLongitudinal objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Sarana is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SaranaLongitudinal[] List of SaranaLongitudinal objects
     * @throws PropelException
     */
    public function getSaranaLongitudinalsRelatedBySaranaId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSaranaLongitudinalsRelatedBySaranaIdPartial && !$this->isNew();
        if (null === $this->collSaranaLongitudinalsRelatedBySaranaId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSaranaLongitudinalsRelatedBySaranaId) {
                // return empty collection
                $this->initSaranaLongitudinalsRelatedBySaranaId();
            } else {
                $collSaranaLongitudinalsRelatedBySaranaId = SaranaLongitudinalQuery::create(null, $criteria)
                    ->filterBySaranaRelatedBySaranaId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSaranaLongitudinalsRelatedBySaranaIdPartial && count($collSaranaLongitudinalsRelatedBySaranaId)) {
                      $this->initSaranaLongitudinalsRelatedBySaranaId(false);

                      foreach($collSaranaLongitudinalsRelatedBySaranaId as $obj) {
                        if (false == $this->collSaranaLongitudinalsRelatedBySaranaId->contains($obj)) {
                          $this->collSaranaLongitudinalsRelatedBySaranaId->append($obj);
                        }
                      }

                      $this->collSaranaLongitudinalsRelatedBySaranaIdPartial = true;
                    }

                    $collSaranaLongitudinalsRelatedBySaranaId->getInternalIterator()->rewind();
                    return $collSaranaLongitudinalsRelatedBySaranaId;
                }

                if($partial && $this->collSaranaLongitudinalsRelatedBySaranaId) {
                    foreach($this->collSaranaLongitudinalsRelatedBySaranaId as $obj) {
                        if($obj->isNew()) {
                            $collSaranaLongitudinalsRelatedBySaranaId[] = $obj;
                        }
                    }
                }

                $this->collSaranaLongitudinalsRelatedBySaranaId = $collSaranaLongitudinalsRelatedBySaranaId;
                $this->collSaranaLongitudinalsRelatedBySaranaIdPartial = false;
            }
        }

        return $this->collSaranaLongitudinalsRelatedBySaranaId;
    }

    /**
     * Sets a collection of SaranaLongitudinalRelatedBySaranaId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $saranaLongitudinalsRelatedBySaranaId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Sarana The current object (for fluent API support)
     */
    public function setSaranaLongitudinalsRelatedBySaranaId(PropelCollection $saranaLongitudinalsRelatedBySaranaId, PropelPDO $con = null)
    {
        $saranaLongitudinalsRelatedBySaranaIdToDelete = $this->getSaranaLongitudinalsRelatedBySaranaId(new Criteria(), $con)->diff($saranaLongitudinalsRelatedBySaranaId);

        $this->saranaLongitudinalsRelatedBySaranaIdScheduledForDeletion = unserialize(serialize($saranaLongitudinalsRelatedBySaranaIdToDelete));

        foreach ($saranaLongitudinalsRelatedBySaranaIdToDelete as $saranaLongitudinalRelatedBySaranaIdRemoved) {
            $saranaLongitudinalRelatedBySaranaIdRemoved->setSaranaRelatedBySaranaId(null);
        }

        $this->collSaranaLongitudinalsRelatedBySaranaId = null;
        foreach ($saranaLongitudinalsRelatedBySaranaId as $saranaLongitudinalRelatedBySaranaId) {
            $this->addSaranaLongitudinalRelatedBySaranaId($saranaLongitudinalRelatedBySaranaId);
        }

        $this->collSaranaLongitudinalsRelatedBySaranaId = $saranaLongitudinalsRelatedBySaranaId;
        $this->collSaranaLongitudinalsRelatedBySaranaIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SaranaLongitudinal objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SaranaLongitudinal objects.
     * @throws PropelException
     */
    public function countSaranaLongitudinalsRelatedBySaranaId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSaranaLongitudinalsRelatedBySaranaIdPartial && !$this->isNew();
        if (null === $this->collSaranaLongitudinalsRelatedBySaranaId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSaranaLongitudinalsRelatedBySaranaId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSaranaLongitudinalsRelatedBySaranaId());
            }
            $query = SaranaLongitudinalQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySaranaRelatedBySaranaId($this)
                ->count($con);
        }

        return count($this->collSaranaLongitudinalsRelatedBySaranaId);
    }

    /**
     * Method called to associate a SaranaLongitudinal object to this object
     * through the SaranaLongitudinal foreign key attribute.
     *
     * @param    SaranaLongitudinal $l SaranaLongitudinal
     * @return Sarana The current object (for fluent API support)
     */
    public function addSaranaLongitudinalRelatedBySaranaId(SaranaLongitudinal $l)
    {
        if ($this->collSaranaLongitudinalsRelatedBySaranaId === null) {
            $this->initSaranaLongitudinalsRelatedBySaranaId();
            $this->collSaranaLongitudinalsRelatedBySaranaIdPartial = true;
        }
        if (!in_array($l, $this->collSaranaLongitudinalsRelatedBySaranaId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSaranaLongitudinalRelatedBySaranaId($l);
        }

        return $this;
    }

    /**
     * @param	SaranaLongitudinalRelatedBySaranaId $saranaLongitudinalRelatedBySaranaId The saranaLongitudinalRelatedBySaranaId object to add.
     */
    protected function doAddSaranaLongitudinalRelatedBySaranaId($saranaLongitudinalRelatedBySaranaId)
    {
        $this->collSaranaLongitudinalsRelatedBySaranaId[]= $saranaLongitudinalRelatedBySaranaId;
        $saranaLongitudinalRelatedBySaranaId->setSaranaRelatedBySaranaId($this);
    }

    /**
     * @param	SaranaLongitudinalRelatedBySaranaId $saranaLongitudinalRelatedBySaranaId The saranaLongitudinalRelatedBySaranaId object to remove.
     * @return Sarana The current object (for fluent API support)
     */
    public function removeSaranaLongitudinalRelatedBySaranaId($saranaLongitudinalRelatedBySaranaId)
    {
        if ($this->getSaranaLongitudinalsRelatedBySaranaId()->contains($saranaLongitudinalRelatedBySaranaId)) {
            $this->collSaranaLongitudinalsRelatedBySaranaId->remove($this->collSaranaLongitudinalsRelatedBySaranaId->search($saranaLongitudinalRelatedBySaranaId));
            if (null === $this->saranaLongitudinalsRelatedBySaranaIdScheduledForDeletion) {
                $this->saranaLongitudinalsRelatedBySaranaIdScheduledForDeletion = clone $this->collSaranaLongitudinalsRelatedBySaranaId;
                $this->saranaLongitudinalsRelatedBySaranaIdScheduledForDeletion->clear();
            }
            $this->saranaLongitudinalsRelatedBySaranaIdScheduledForDeletion[]= clone $saranaLongitudinalRelatedBySaranaId;
            $saranaLongitudinalRelatedBySaranaId->setSaranaRelatedBySaranaId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Sarana is new, it will return
     * an empty collection; or if this Sarana has previously
     * been saved, it will retrieve related SaranaLongitudinalsRelatedBySaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Sarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SaranaLongitudinal[] List of SaranaLongitudinal objects
     */
    public function getSaranaLongitudinalsRelatedBySaranaIdJoinSemesterRelatedBySemesterId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SaranaLongitudinalQuery::create(null, $criteria);
        $query->joinWith('SemesterRelatedBySemesterId', $join_behavior);

        return $this->getSaranaLongitudinalsRelatedBySaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Sarana is new, it will return
     * an empty collection; or if this Sarana has previously
     * been saved, it will retrieve related SaranaLongitudinalsRelatedBySaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Sarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SaranaLongitudinal[] List of SaranaLongitudinal objects
     */
    public function getSaranaLongitudinalsRelatedBySaranaIdJoinSemesterRelatedBySemesterId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SaranaLongitudinalQuery::create(null, $criteria);
        $query->joinWith('SemesterRelatedBySemesterId', $join_behavior);

        return $this->getSaranaLongitudinalsRelatedBySaranaId($query, $con);
    }

    /**
     * Clears out the collSaranaLongitudinalsRelatedBySaranaId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Sarana The current object (for fluent API support)
     * @see        addSaranaLongitudinalsRelatedBySaranaId()
     */
    public function clearSaranaLongitudinalsRelatedBySaranaId()
    {
        $this->collSaranaLongitudinalsRelatedBySaranaId = null; // important to set this to null since that means it is uninitialized
        $this->collSaranaLongitudinalsRelatedBySaranaIdPartial = null;

        return $this;
    }

    /**
     * reset is the collSaranaLongitudinalsRelatedBySaranaId collection loaded partially
     *
     * @return void
     */
    public function resetPartialSaranaLongitudinalsRelatedBySaranaId($v = true)
    {
        $this->collSaranaLongitudinalsRelatedBySaranaIdPartial = $v;
    }

    /**
     * Initializes the collSaranaLongitudinalsRelatedBySaranaId collection.
     *
     * By default this just sets the collSaranaLongitudinalsRelatedBySaranaId collection to an empty array (like clearcollSaranaLongitudinalsRelatedBySaranaId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSaranaLongitudinalsRelatedBySaranaId($overrideExisting = true)
    {
        if (null !== $this->collSaranaLongitudinalsRelatedBySaranaId && !$overrideExisting) {
            return;
        }
        $this->collSaranaLongitudinalsRelatedBySaranaId = new PropelObjectCollection();
        $this->collSaranaLongitudinalsRelatedBySaranaId->setModel('SaranaLongitudinal');
    }

    /**
     * Gets an array of SaranaLongitudinal objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Sarana is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SaranaLongitudinal[] List of SaranaLongitudinal objects
     * @throws PropelException
     */
    public function getSaranaLongitudinalsRelatedBySaranaId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSaranaLongitudinalsRelatedBySaranaIdPartial && !$this->isNew();
        if (null === $this->collSaranaLongitudinalsRelatedBySaranaId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSaranaLongitudinalsRelatedBySaranaId) {
                // return empty collection
                $this->initSaranaLongitudinalsRelatedBySaranaId();
            } else {
                $collSaranaLongitudinalsRelatedBySaranaId = SaranaLongitudinalQuery::create(null, $criteria)
                    ->filterBySaranaRelatedBySaranaId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSaranaLongitudinalsRelatedBySaranaIdPartial && count($collSaranaLongitudinalsRelatedBySaranaId)) {
                      $this->initSaranaLongitudinalsRelatedBySaranaId(false);

                      foreach($collSaranaLongitudinalsRelatedBySaranaId as $obj) {
                        if (false == $this->collSaranaLongitudinalsRelatedBySaranaId->contains($obj)) {
                          $this->collSaranaLongitudinalsRelatedBySaranaId->append($obj);
                        }
                      }

                      $this->collSaranaLongitudinalsRelatedBySaranaIdPartial = true;
                    }

                    $collSaranaLongitudinalsRelatedBySaranaId->getInternalIterator()->rewind();
                    return $collSaranaLongitudinalsRelatedBySaranaId;
                }

                if($partial && $this->collSaranaLongitudinalsRelatedBySaranaId) {
                    foreach($this->collSaranaLongitudinalsRelatedBySaranaId as $obj) {
                        if($obj->isNew()) {
                            $collSaranaLongitudinalsRelatedBySaranaId[] = $obj;
                        }
                    }
                }

                $this->collSaranaLongitudinalsRelatedBySaranaId = $collSaranaLongitudinalsRelatedBySaranaId;
                $this->collSaranaLongitudinalsRelatedBySaranaIdPartial = false;
            }
        }

        return $this->collSaranaLongitudinalsRelatedBySaranaId;
    }

    /**
     * Sets a collection of SaranaLongitudinalRelatedBySaranaId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $saranaLongitudinalsRelatedBySaranaId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Sarana The current object (for fluent API support)
     */
    public function setSaranaLongitudinalsRelatedBySaranaId(PropelCollection $saranaLongitudinalsRelatedBySaranaId, PropelPDO $con = null)
    {
        $saranaLongitudinalsRelatedBySaranaIdToDelete = $this->getSaranaLongitudinalsRelatedBySaranaId(new Criteria(), $con)->diff($saranaLongitudinalsRelatedBySaranaId);

        $this->saranaLongitudinalsRelatedBySaranaIdScheduledForDeletion = unserialize(serialize($saranaLongitudinalsRelatedBySaranaIdToDelete));

        foreach ($saranaLongitudinalsRelatedBySaranaIdToDelete as $saranaLongitudinalRelatedBySaranaIdRemoved) {
            $saranaLongitudinalRelatedBySaranaIdRemoved->setSaranaRelatedBySaranaId(null);
        }

        $this->collSaranaLongitudinalsRelatedBySaranaId = null;
        foreach ($saranaLongitudinalsRelatedBySaranaId as $saranaLongitudinalRelatedBySaranaId) {
            $this->addSaranaLongitudinalRelatedBySaranaId($saranaLongitudinalRelatedBySaranaId);
        }

        $this->collSaranaLongitudinalsRelatedBySaranaId = $saranaLongitudinalsRelatedBySaranaId;
        $this->collSaranaLongitudinalsRelatedBySaranaIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SaranaLongitudinal objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SaranaLongitudinal objects.
     * @throws PropelException
     */
    public function countSaranaLongitudinalsRelatedBySaranaId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSaranaLongitudinalsRelatedBySaranaIdPartial && !$this->isNew();
        if (null === $this->collSaranaLongitudinalsRelatedBySaranaId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSaranaLongitudinalsRelatedBySaranaId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSaranaLongitudinalsRelatedBySaranaId());
            }
            $query = SaranaLongitudinalQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySaranaRelatedBySaranaId($this)
                ->count($con);
        }

        return count($this->collSaranaLongitudinalsRelatedBySaranaId);
    }

    /**
     * Method called to associate a SaranaLongitudinal object to this object
     * through the SaranaLongitudinal foreign key attribute.
     *
     * @param    SaranaLongitudinal $l SaranaLongitudinal
     * @return Sarana The current object (for fluent API support)
     */
    public function addSaranaLongitudinalRelatedBySaranaId(SaranaLongitudinal $l)
    {
        if ($this->collSaranaLongitudinalsRelatedBySaranaId === null) {
            $this->initSaranaLongitudinalsRelatedBySaranaId();
            $this->collSaranaLongitudinalsRelatedBySaranaIdPartial = true;
        }
        if (!in_array($l, $this->collSaranaLongitudinalsRelatedBySaranaId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSaranaLongitudinalRelatedBySaranaId($l);
        }

        return $this;
    }

    /**
     * @param	SaranaLongitudinalRelatedBySaranaId $saranaLongitudinalRelatedBySaranaId The saranaLongitudinalRelatedBySaranaId object to add.
     */
    protected function doAddSaranaLongitudinalRelatedBySaranaId($saranaLongitudinalRelatedBySaranaId)
    {
        $this->collSaranaLongitudinalsRelatedBySaranaId[]= $saranaLongitudinalRelatedBySaranaId;
        $saranaLongitudinalRelatedBySaranaId->setSaranaRelatedBySaranaId($this);
    }

    /**
     * @param	SaranaLongitudinalRelatedBySaranaId $saranaLongitudinalRelatedBySaranaId The saranaLongitudinalRelatedBySaranaId object to remove.
     * @return Sarana The current object (for fluent API support)
     */
    public function removeSaranaLongitudinalRelatedBySaranaId($saranaLongitudinalRelatedBySaranaId)
    {
        if ($this->getSaranaLongitudinalsRelatedBySaranaId()->contains($saranaLongitudinalRelatedBySaranaId)) {
            $this->collSaranaLongitudinalsRelatedBySaranaId->remove($this->collSaranaLongitudinalsRelatedBySaranaId->search($saranaLongitudinalRelatedBySaranaId));
            if (null === $this->saranaLongitudinalsRelatedBySaranaIdScheduledForDeletion) {
                $this->saranaLongitudinalsRelatedBySaranaIdScheduledForDeletion = clone $this->collSaranaLongitudinalsRelatedBySaranaId;
                $this->saranaLongitudinalsRelatedBySaranaIdScheduledForDeletion->clear();
            }
            $this->saranaLongitudinalsRelatedBySaranaIdScheduledForDeletion[]= clone $saranaLongitudinalRelatedBySaranaId;
            $saranaLongitudinalRelatedBySaranaId->setSaranaRelatedBySaranaId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Sarana is new, it will return
     * an empty collection; or if this Sarana has previously
     * been saved, it will retrieve related SaranaLongitudinalsRelatedBySaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Sarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SaranaLongitudinal[] List of SaranaLongitudinal objects
     */
    public function getSaranaLongitudinalsRelatedBySaranaIdJoinSemesterRelatedBySemesterId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SaranaLongitudinalQuery::create(null, $criteria);
        $query->joinWith('SemesterRelatedBySemesterId', $join_behavior);

        return $this->getSaranaLongitudinalsRelatedBySaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Sarana is new, it will return
     * an empty collection; or if this Sarana has previously
     * been saved, it will retrieve related SaranaLongitudinalsRelatedBySaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Sarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SaranaLongitudinal[] List of SaranaLongitudinal objects
     */
    public function getSaranaLongitudinalsRelatedBySaranaIdJoinSemesterRelatedBySemesterId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SaranaLongitudinalQuery::create(null, $criteria);
        $query->joinWith('SemesterRelatedBySemesterId', $join_behavior);

        return $this->getSaranaLongitudinalsRelatedBySaranaId($query, $con);
    }

    /**
     * Clears out the collVldSaranasRelatedBySaranaId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Sarana The current object (for fluent API support)
     * @see        addVldSaranasRelatedBySaranaId()
     */
    public function clearVldSaranasRelatedBySaranaId()
    {
        $this->collVldSaranasRelatedBySaranaId = null; // important to set this to null since that means it is uninitialized
        $this->collVldSaranasRelatedBySaranaIdPartial = null;

        return $this;
    }

    /**
     * reset is the collVldSaranasRelatedBySaranaId collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldSaranasRelatedBySaranaId($v = true)
    {
        $this->collVldSaranasRelatedBySaranaIdPartial = $v;
    }

    /**
     * Initializes the collVldSaranasRelatedBySaranaId collection.
     *
     * By default this just sets the collVldSaranasRelatedBySaranaId collection to an empty array (like clearcollVldSaranasRelatedBySaranaId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldSaranasRelatedBySaranaId($overrideExisting = true)
    {
        if (null !== $this->collVldSaranasRelatedBySaranaId && !$overrideExisting) {
            return;
        }
        $this->collVldSaranasRelatedBySaranaId = new PropelObjectCollection();
        $this->collVldSaranasRelatedBySaranaId->setModel('VldSarana');
    }

    /**
     * Gets an array of VldSarana objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Sarana is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldSarana[] List of VldSarana objects
     * @throws PropelException
     */
    public function getVldSaranasRelatedBySaranaId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldSaranasRelatedBySaranaIdPartial && !$this->isNew();
        if (null === $this->collVldSaranasRelatedBySaranaId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldSaranasRelatedBySaranaId) {
                // return empty collection
                $this->initVldSaranasRelatedBySaranaId();
            } else {
                $collVldSaranasRelatedBySaranaId = VldSaranaQuery::create(null, $criteria)
                    ->filterBySaranaRelatedBySaranaId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldSaranasRelatedBySaranaIdPartial && count($collVldSaranasRelatedBySaranaId)) {
                      $this->initVldSaranasRelatedBySaranaId(false);

                      foreach($collVldSaranasRelatedBySaranaId as $obj) {
                        if (false == $this->collVldSaranasRelatedBySaranaId->contains($obj)) {
                          $this->collVldSaranasRelatedBySaranaId->append($obj);
                        }
                      }

                      $this->collVldSaranasRelatedBySaranaIdPartial = true;
                    }

                    $collVldSaranasRelatedBySaranaId->getInternalIterator()->rewind();
                    return $collVldSaranasRelatedBySaranaId;
                }

                if($partial && $this->collVldSaranasRelatedBySaranaId) {
                    foreach($this->collVldSaranasRelatedBySaranaId as $obj) {
                        if($obj->isNew()) {
                            $collVldSaranasRelatedBySaranaId[] = $obj;
                        }
                    }
                }

                $this->collVldSaranasRelatedBySaranaId = $collVldSaranasRelatedBySaranaId;
                $this->collVldSaranasRelatedBySaranaIdPartial = false;
            }
        }

        return $this->collVldSaranasRelatedBySaranaId;
    }

    /**
     * Sets a collection of VldSaranaRelatedBySaranaId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldSaranasRelatedBySaranaId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Sarana The current object (for fluent API support)
     */
    public function setVldSaranasRelatedBySaranaId(PropelCollection $vldSaranasRelatedBySaranaId, PropelPDO $con = null)
    {
        $vldSaranasRelatedBySaranaIdToDelete = $this->getVldSaranasRelatedBySaranaId(new Criteria(), $con)->diff($vldSaranasRelatedBySaranaId);

        $this->vldSaranasRelatedBySaranaIdScheduledForDeletion = unserialize(serialize($vldSaranasRelatedBySaranaIdToDelete));

        foreach ($vldSaranasRelatedBySaranaIdToDelete as $vldSaranaRelatedBySaranaIdRemoved) {
            $vldSaranaRelatedBySaranaIdRemoved->setSaranaRelatedBySaranaId(null);
        }

        $this->collVldSaranasRelatedBySaranaId = null;
        foreach ($vldSaranasRelatedBySaranaId as $vldSaranaRelatedBySaranaId) {
            $this->addVldSaranaRelatedBySaranaId($vldSaranaRelatedBySaranaId);
        }

        $this->collVldSaranasRelatedBySaranaId = $vldSaranasRelatedBySaranaId;
        $this->collVldSaranasRelatedBySaranaIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldSarana objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldSarana objects.
     * @throws PropelException
     */
    public function countVldSaranasRelatedBySaranaId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldSaranasRelatedBySaranaIdPartial && !$this->isNew();
        if (null === $this->collVldSaranasRelatedBySaranaId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldSaranasRelatedBySaranaId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldSaranasRelatedBySaranaId());
            }
            $query = VldSaranaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySaranaRelatedBySaranaId($this)
                ->count($con);
        }

        return count($this->collVldSaranasRelatedBySaranaId);
    }

    /**
     * Method called to associate a VldSarana object to this object
     * through the VldSarana foreign key attribute.
     *
     * @param    VldSarana $l VldSarana
     * @return Sarana The current object (for fluent API support)
     */
    public function addVldSaranaRelatedBySaranaId(VldSarana $l)
    {
        if ($this->collVldSaranasRelatedBySaranaId === null) {
            $this->initVldSaranasRelatedBySaranaId();
            $this->collVldSaranasRelatedBySaranaIdPartial = true;
        }
        if (!in_array($l, $this->collVldSaranasRelatedBySaranaId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldSaranaRelatedBySaranaId($l);
        }

        return $this;
    }

    /**
     * @param	VldSaranaRelatedBySaranaId $vldSaranaRelatedBySaranaId The vldSaranaRelatedBySaranaId object to add.
     */
    protected function doAddVldSaranaRelatedBySaranaId($vldSaranaRelatedBySaranaId)
    {
        $this->collVldSaranasRelatedBySaranaId[]= $vldSaranaRelatedBySaranaId;
        $vldSaranaRelatedBySaranaId->setSaranaRelatedBySaranaId($this);
    }

    /**
     * @param	VldSaranaRelatedBySaranaId $vldSaranaRelatedBySaranaId The vldSaranaRelatedBySaranaId object to remove.
     * @return Sarana The current object (for fluent API support)
     */
    public function removeVldSaranaRelatedBySaranaId($vldSaranaRelatedBySaranaId)
    {
        if ($this->getVldSaranasRelatedBySaranaId()->contains($vldSaranaRelatedBySaranaId)) {
            $this->collVldSaranasRelatedBySaranaId->remove($this->collVldSaranasRelatedBySaranaId->search($vldSaranaRelatedBySaranaId));
            if (null === $this->vldSaranasRelatedBySaranaIdScheduledForDeletion) {
                $this->vldSaranasRelatedBySaranaIdScheduledForDeletion = clone $this->collVldSaranasRelatedBySaranaId;
                $this->vldSaranasRelatedBySaranaIdScheduledForDeletion->clear();
            }
            $this->vldSaranasRelatedBySaranaIdScheduledForDeletion[]= clone $vldSaranaRelatedBySaranaId;
            $vldSaranaRelatedBySaranaId->setSaranaRelatedBySaranaId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Sarana is new, it will return
     * an empty collection; or if this Sarana has previously
     * been saved, it will retrieve related VldSaranasRelatedBySaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Sarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldSarana[] List of VldSarana objects
     */
    public function getVldSaranasRelatedBySaranaIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldSaranaQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldSaranasRelatedBySaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Sarana is new, it will return
     * an empty collection; or if this Sarana has previously
     * been saved, it will retrieve related VldSaranasRelatedBySaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Sarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldSarana[] List of VldSarana objects
     */
    public function getVldSaranasRelatedBySaranaIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldSaranaQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldSaranasRelatedBySaranaId($query, $con);
    }

    /**
     * Clears out the collVldSaranasRelatedBySaranaId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Sarana The current object (for fluent API support)
     * @see        addVldSaranasRelatedBySaranaId()
     */
    public function clearVldSaranasRelatedBySaranaId()
    {
        $this->collVldSaranasRelatedBySaranaId = null; // important to set this to null since that means it is uninitialized
        $this->collVldSaranasRelatedBySaranaIdPartial = null;

        return $this;
    }

    /**
     * reset is the collVldSaranasRelatedBySaranaId collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldSaranasRelatedBySaranaId($v = true)
    {
        $this->collVldSaranasRelatedBySaranaIdPartial = $v;
    }

    /**
     * Initializes the collVldSaranasRelatedBySaranaId collection.
     *
     * By default this just sets the collVldSaranasRelatedBySaranaId collection to an empty array (like clearcollVldSaranasRelatedBySaranaId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldSaranasRelatedBySaranaId($overrideExisting = true)
    {
        if (null !== $this->collVldSaranasRelatedBySaranaId && !$overrideExisting) {
            return;
        }
        $this->collVldSaranasRelatedBySaranaId = new PropelObjectCollection();
        $this->collVldSaranasRelatedBySaranaId->setModel('VldSarana');
    }

    /**
     * Gets an array of VldSarana objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Sarana is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldSarana[] List of VldSarana objects
     * @throws PropelException
     */
    public function getVldSaranasRelatedBySaranaId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldSaranasRelatedBySaranaIdPartial && !$this->isNew();
        if (null === $this->collVldSaranasRelatedBySaranaId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldSaranasRelatedBySaranaId) {
                // return empty collection
                $this->initVldSaranasRelatedBySaranaId();
            } else {
                $collVldSaranasRelatedBySaranaId = VldSaranaQuery::create(null, $criteria)
                    ->filterBySaranaRelatedBySaranaId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldSaranasRelatedBySaranaIdPartial && count($collVldSaranasRelatedBySaranaId)) {
                      $this->initVldSaranasRelatedBySaranaId(false);

                      foreach($collVldSaranasRelatedBySaranaId as $obj) {
                        if (false == $this->collVldSaranasRelatedBySaranaId->contains($obj)) {
                          $this->collVldSaranasRelatedBySaranaId->append($obj);
                        }
                      }

                      $this->collVldSaranasRelatedBySaranaIdPartial = true;
                    }

                    $collVldSaranasRelatedBySaranaId->getInternalIterator()->rewind();
                    return $collVldSaranasRelatedBySaranaId;
                }

                if($partial && $this->collVldSaranasRelatedBySaranaId) {
                    foreach($this->collVldSaranasRelatedBySaranaId as $obj) {
                        if($obj->isNew()) {
                            $collVldSaranasRelatedBySaranaId[] = $obj;
                        }
                    }
                }

                $this->collVldSaranasRelatedBySaranaId = $collVldSaranasRelatedBySaranaId;
                $this->collVldSaranasRelatedBySaranaIdPartial = false;
            }
        }

        return $this->collVldSaranasRelatedBySaranaId;
    }

    /**
     * Sets a collection of VldSaranaRelatedBySaranaId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldSaranasRelatedBySaranaId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Sarana The current object (for fluent API support)
     */
    public function setVldSaranasRelatedBySaranaId(PropelCollection $vldSaranasRelatedBySaranaId, PropelPDO $con = null)
    {
        $vldSaranasRelatedBySaranaIdToDelete = $this->getVldSaranasRelatedBySaranaId(new Criteria(), $con)->diff($vldSaranasRelatedBySaranaId);

        $this->vldSaranasRelatedBySaranaIdScheduledForDeletion = unserialize(serialize($vldSaranasRelatedBySaranaIdToDelete));

        foreach ($vldSaranasRelatedBySaranaIdToDelete as $vldSaranaRelatedBySaranaIdRemoved) {
            $vldSaranaRelatedBySaranaIdRemoved->setSaranaRelatedBySaranaId(null);
        }

        $this->collVldSaranasRelatedBySaranaId = null;
        foreach ($vldSaranasRelatedBySaranaId as $vldSaranaRelatedBySaranaId) {
            $this->addVldSaranaRelatedBySaranaId($vldSaranaRelatedBySaranaId);
        }

        $this->collVldSaranasRelatedBySaranaId = $vldSaranasRelatedBySaranaId;
        $this->collVldSaranasRelatedBySaranaIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldSarana objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldSarana objects.
     * @throws PropelException
     */
    public function countVldSaranasRelatedBySaranaId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldSaranasRelatedBySaranaIdPartial && !$this->isNew();
        if (null === $this->collVldSaranasRelatedBySaranaId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldSaranasRelatedBySaranaId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldSaranasRelatedBySaranaId());
            }
            $query = VldSaranaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySaranaRelatedBySaranaId($this)
                ->count($con);
        }

        return count($this->collVldSaranasRelatedBySaranaId);
    }

    /**
     * Method called to associate a VldSarana object to this object
     * through the VldSarana foreign key attribute.
     *
     * @param    VldSarana $l VldSarana
     * @return Sarana The current object (for fluent API support)
     */
    public function addVldSaranaRelatedBySaranaId(VldSarana $l)
    {
        if ($this->collVldSaranasRelatedBySaranaId === null) {
            $this->initVldSaranasRelatedBySaranaId();
            $this->collVldSaranasRelatedBySaranaIdPartial = true;
        }
        if (!in_array($l, $this->collVldSaranasRelatedBySaranaId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldSaranaRelatedBySaranaId($l);
        }

        return $this;
    }

    /**
     * @param	VldSaranaRelatedBySaranaId $vldSaranaRelatedBySaranaId The vldSaranaRelatedBySaranaId object to add.
     */
    protected function doAddVldSaranaRelatedBySaranaId($vldSaranaRelatedBySaranaId)
    {
        $this->collVldSaranasRelatedBySaranaId[]= $vldSaranaRelatedBySaranaId;
        $vldSaranaRelatedBySaranaId->setSaranaRelatedBySaranaId($this);
    }

    /**
     * @param	VldSaranaRelatedBySaranaId $vldSaranaRelatedBySaranaId The vldSaranaRelatedBySaranaId object to remove.
     * @return Sarana The current object (for fluent API support)
     */
    public function removeVldSaranaRelatedBySaranaId($vldSaranaRelatedBySaranaId)
    {
        if ($this->getVldSaranasRelatedBySaranaId()->contains($vldSaranaRelatedBySaranaId)) {
            $this->collVldSaranasRelatedBySaranaId->remove($this->collVldSaranasRelatedBySaranaId->search($vldSaranaRelatedBySaranaId));
            if (null === $this->vldSaranasRelatedBySaranaIdScheduledForDeletion) {
                $this->vldSaranasRelatedBySaranaIdScheduledForDeletion = clone $this->collVldSaranasRelatedBySaranaId;
                $this->vldSaranasRelatedBySaranaIdScheduledForDeletion->clear();
            }
            $this->vldSaranasRelatedBySaranaIdScheduledForDeletion[]= clone $vldSaranaRelatedBySaranaId;
            $vldSaranaRelatedBySaranaId->setSaranaRelatedBySaranaId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Sarana is new, it will return
     * an empty collection; or if this Sarana has previously
     * been saved, it will retrieve related VldSaranasRelatedBySaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Sarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldSarana[] List of VldSarana objects
     */
    public function getVldSaranasRelatedBySaranaIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldSaranaQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldSaranasRelatedBySaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Sarana is new, it will return
     * an empty collection; or if this Sarana has previously
     * been saved, it will retrieve related VldSaranasRelatedBySaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Sarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldSarana[] List of VldSarana objects
     */
    public function getVldSaranasRelatedBySaranaIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldSaranaQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldSaranasRelatedBySaranaId($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->sarana_id = null;
        $this->sekolah_id = null;
        $this->jenis_sarana_id = null;
        $this->prasarana_id = null;
        $this->kepemilikan_sarpras_id = null;
        $this->nama = null;
        $this->last_update = null;
        $this->soft_delete = null;
        $this->last_sync = null;
        $this->updater_id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collSaranaLongitudinalsRelatedBySaranaId) {
                foreach ($this->collSaranaLongitudinalsRelatedBySaranaId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSaranaLongitudinalsRelatedBySaranaId) {
                foreach ($this->collSaranaLongitudinalsRelatedBySaranaId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldSaranasRelatedBySaranaId) {
                foreach ($this->collVldSaranasRelatedBySaranaId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldSaranasRelatedBySaranaId) {
                foreach ($this->collVldSaranasRelatedBySaranaId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aPrasaranaRelatedByPrasaranaId instanceof Persistent) {
              $this->aPrasaranaRelatedByPrasaranaId->clearAllReferences($deep);
            }
            if ($this->aPrasaranaRelatedByPrasaranaId instanceof Persistent) {
              $this->aPrasaranaRelatedByPrasaranaId->clearAllReferences($deep);
            }
            if ($this->aSekolahRelatedBySekolahId instanceof Persistent) {
              $this->aSekolahRelatedBySekolahId->clearAllReferences($deep);
            }
            if ($this->aSekolahRelatedBySekolahId instanceof Persistent) {
              $this->aSekolahRelatedBySekolahId->clearAllReferences($deep);
            }
            if ($this->aJenisSaranaRelatedByJenisSaranaId instanceof Persistent) {
              $this->aJenisSaranaRelatedByJenisSaranaId->clearAllReferences($deep);
            }
            if ($this->aJenisSaranaRelatedByJenisSaranaId instanceof Persistent) {
              $this->aJenisSaranaRelatedByJenisSaranaId->clearAllReferences($deep);
            }
            if ($this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId instanceof Persistent) {
              $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId->clearAllReferences($deep);
            }
            if ($this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId instanceof Persistent) {
              $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collSaranaLongitudinalsRelatedBySaranaId instanceof PropelCollection) {
            $this->collSaranaLongitudinalsRelatedBySaranaId->clearIterator();
        }
        $this->collSaranaLongitudinalsRelatedBySaranaId = null;
        if ($this->collSaranaLongitudinalsRelatedBySaranaId instanceof PropelCollection) {
            $this->collSaranaLongitudinalsRelatedBySaranaId->clearIterator();
        }
        $this->collSaranaLongitudinalsRelatedBySaranaId = null;
        if ($this->collVldSaranasRelatedBySaranaId instanceof PropelCollection) {
            $this->collVldSaranasRelatedBySaranaId->clearIterator();
        }
        $this->collVldSaranasRelatedBySaranaId = null;
        if ($this->collVldSaranasRelatedBySaranaId instanceof PropelCollection) {
            $this->collVldSaranasRelatedBySaranaId->clearIterator();
        }
        $this->collVldSaranasRelatedBySaranaId = null;
        $this->aPrasaranaRelatedByPrasaranaId = null;
        $this->aPrasaranaRelatedByPrasaranaId = null;
        $this->aSekolahRelatedBySekolahId = null;
        $this->aSekolahRelatedBySekolahId = null;
        $this->aJenisSaranaRelatedByJenisSaranaId = null;
        $this->aJenisSaranaRelatedByJenisSaranaId = null;
        $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId = null;
        $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(SaranaPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
