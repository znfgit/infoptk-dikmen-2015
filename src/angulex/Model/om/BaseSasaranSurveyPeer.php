<?php

namespace angulex\Model\om;

use \BasePeer;
use \Criteria;
use \PDO;
use \PDOStatement;
use \Propel;
use \PropelException;
use \PropelPDO;
use angulex\Model\PenggunaPeer;
use angulex\Model\SasaranSurvey;
use angulex\Model\SasaranSurveyPeer;
use angulex\Model\SekolahPeer;
use angulex\Model\map\SasaranSurveyTableMap;

/**
 * Base static class for performing query and update operations on the 'sasaran_survey' table.
 *
 * 
 *
 * @package propel.generator.angulex.Model.om
 */
abstract class BaseSasaranSurveyPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'Dapodikmen';

    /** the table name for this class */
    const TABLE_NAME = 'sasaran_survey';

    /** the related Propel class for this table */
    const OM_CLASS = 'angulex\\Model\\SasaranSurvey';

    /** the related TableMap class for this table */
    const TM_CLASS = 'SasaranSurveyTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 11;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 11;

    /** the column name for the pengguna_id field */
    const PENGGUNA_ID = 'sasaran_survey.pengguna_id';

    /** the column name for the sekolah_id field */
    const SEKOLAH_ID = 'sasaran_survey.sekolah_id';

    /** the column name for the nomor_urut field */
    const NOMOR_URUT = 'sasaran_survey.nomor_urut';

    /** the column name for the tanggal_rencana field */
    const TANGGAL_RENCANA = 'sasaran_survey.tanggal_rencana';

    /** the column name for the tanggal_pelaksanaan field */
    const TANGGAL_PELAKSANAAN = 'sasaran_survey.tanggal_pelaksanaan';

    /** the column name for the waktu_berangkat field */
    const WAKTU_BERANGKAT = 'sasaran_survey.waktu_berangkat';

    /** the column name for the waktu_sampai field */
    const WAKTU_SAMPAI = 'sasaran_survey.waktu_sampai';

    /** the column name for the waktu_mulai_survey field */
    const WAKTU_MULAI_SURVEY = 'sasaran_survey.waktu_mulai_survey';

    /** the column name for the waktu_selesai field */
    const WAKTU_SELESAI = 'sasaran_survey.waktu_selesai';

    /** the column name for the waktu_isi_form_cetak field */
    const WAKTU_ISI_FORM_CETAK = 'sasaran_survey.waktu_isi_form_cetak';

    /** the column name for the waktu_isi_form_elektronik field */
    const WAKTU_ISI_FORM_ELEKTRONIK = 'sasaran_survey.waktu_isi_form_elektronik';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identiy map to hold any loaded instances of SasaranSurvey objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array SasaranSurvey[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. SasaranSurveyPeer::$fieldNames[SasaranSurveyPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('PenggunaId', 'SekolahId', 'NomorUrut', 'TanggalRencana', 'TanggalPelaksanaan', 'WaktuBerangkat', 'WaktuSampai', 'WaktuMulaiSurvey', 'WaktuSelesai', 'WaktuIsiFormCetak', 'WaktuIsiFormElektronik', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('penggunaId', 'sekolahId', 'nomorUrut', 'tanggalRencana', 'tanggalPelaksanaan', 'waktuBerangkat', 'waktuSampai', 'waktuMulaiSurvey', 'waktuSelesai', 'waktuIsiFormCetak', 'waktuIsiFormElektronik', ),
        BasePeer::TYPE_COLNAME => array (SasaranSurveyPeer::PENGGUNA_ID, SasaranSurveyPeer::SEKOLAH_ID, SasaranSurveyPeer::NOMOR_URUT, SasaranSurveyPeer::TANGGAL_RENCANA, SasaranSurveyPeer::TANGGAL_PELAKSANAAN, SasaranSurveyPeer::WAKTU_BERANGKAT, SasaranSurveyPeer::WAKTU_SAMPAI, SasaranSurveyPeer::WAKTU_MULAI_SURVEY, SasaranSurveyPeer::WAKTU_SELESAI, SasaranSurveyPeer::WAKTU_ISI_FORM_CETAK, SasaranSurveyPeer::WAKTU_ISI_FORM_ELEKTRONIK, ),
        BasePeer::TYPE_RAW_COLNAME => array ('PENGGUNA_ID', 'SEKOLAH_ID', 'NOMOR_URUT', 'TANGGAL_RENCANA', 'TANGGAL_PELAKSANAAN', 'WAKTU_BERANGKAT', 'WAKTU_SAMPAI', 'WAKTU_MULAI_SURVEY', 'WAKTU_SELESAI', 'WAKTU_ISI_FORM_CETAK', 'WAKTU_ISI_FORM_ELEKTRONIK', ),
        BasePeer::TYPE_FIELDNAME => array ('pengguna_id', 'sekolah_id', 'nomor_urut', 'tanggal_rencana', 'tanggal_pelaksanaan', 'waktu_berangkat', 'waktu_sampai', 'waktu_mulai_survey', 'waktu_selesai', 'waktu_isi_form_cetak', 'waktu_isi_form_elektronik', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. SasaranSurveyPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('PenggunaId' => 0, 'SekolahId' => 1, 'NomorUrut' => 2, 'TanggalRencana' => 3, 'TanggalPelaksanaan' => 4, 'WaktuBerangkat' => 5, 'WaktuSampai' => 6, 'WaktuMulaiSurvey' => 7, 'WaktuSelesai' => 8, 'WaktuIsiFormCetak' => 9, 'WaktuIsiFormElektronik' => 10, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('penggunaId' => 0, 'sekolahId' => 1, 'nomorUrut' => 2, 'tanggalRencana' => 3, 'tanggalPelaksanaan' => 4, 'waktuBerangkat' => 5, 'waktuSampai' => 6, 'waktuMulaiSurvey' => 7, 'waktuSelesai' => 8, 'waktuIsiFormCetak' => 9, 'waktuIsiFormElektronik' => 10, ),
        BasePeer::TYPE_COLNAME => array (SasaranSurveyPeer::PENGGUNA_ID => 0, SasaranSurveyPeer::SEKOLAH_ID => 1, SasaranSurveyPeer::NOMOR_URUT => 2, SasaranSurveyPeer::TANGGAL_RENCANA => 3, SasaranSurveyPeer::TANGGAL_PELAKSANAAN => 4, SasaranSurveyPeer::WAKTU_BERANGKAT => 5, SasaranSurveyPeer::WAKTU_SAMPAI => 6, SasaranSurveyPeer::WAKTU_MULAI_SURVEY => 7, SasaranSurveyPeer::WAKTU_SELESAI => 8, SasaranSurveyPeer::WAKTU_ISI_FORM_CETAK => 9, SasaranSurveyPeer::WAKTU_ISI_FORM_ELEKTRONIK => 10, ),
        BasePeer::TYPE_RAW_COLNAME => array ('PENGGUNA_ID' => 0, 'SEKOLAH_ID' => 1, 'NOMOR_URUT' => 2, 'TANGGAL_RENCANA' => 3, 'TANGGAL_PELAKSANAAN' => 4, 'WAKTU_BERANGKAT' => 5, 'WAKTU_SAMPAI' => 6, 'WAKTU_MULAI_SURVEY' => 7, 'WAKTU_SELESAI' => 8, 'WAKTU_ISI_FORM_CETAK' => 9, 'WAKTU_ISI_FORM_ELEKTRONIK' => 10, ),
        BasePeer::TYPE_FIELDNAME => array ('pengguna_id' => 0, 'sekolah_id' => 1, 'nomor_urut' => 2, 'tanggal_rencana' => 3, 'tanggal_pelaksanaan' => 4, 'waktu_berangkat' => 5, 'waktu_sampai' => 6, 'waktu_mulai_survey' => 7, 'waktu_selesai' => 8, 'waktu_isi_form_cetak' => 9, 'waktu_isi_form_elektronik' => 10, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = SasaranSurveyPeer::getFieldNames($toType);
        $key = isset(SasaranSurveyPeer::$fieldKeys[$fromType][$name]) ? SasaranSurveyPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(SasaranSurveyPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, SasaranSurveyPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return SasaranSurveyPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. SasaranSurveyPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(SasaranSurveyPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(SasaranSurveyPeer::PENGGUNA_ID);
            $criteria->addSelectColumn(SasaranSurveyPeer::SEKOLAH_ID);
            $criteria->addSelectColumn(SasaranSurveyPeer::NOMOR_URUT);
            $criteria->addSelectColumn(SasaranSurveyPeer::TANGGAL_RENCANA);
            $criteria->addSelectColumn(SasaranSurveyPeer::TANGGAL_PELAKSANAAN);
            $criteria->addSelectColumn(SasaranSurveyPeer::WAKTU_BERANGKAT);
            $criteria->addSelectColumn(SasaranSurveyPeer::WAKTU_SAMPAI);
            $criteria->addSelectColumn(SasaranSurveyPeer::WAKTU_MULAI_SURVEY);
            $criteria->addSelectColumn(SasaranSurveyPeer::WAKTU_SELESAI);
            $criteria->addSelectColumn(SasaranSurveyPeer::WAKTU_ISI_FORM_CETAK);
            $criteria->addSelectColumn(SasaranSurveyPeer::WAKTU_ISI_FORM_ELEKTRONIK);
        } else {
            $criteria->addSelectColumn($alias . '.pengguna_id');
            $criteria->addSelectColumn($alias . '.sekolah_id');
            $criteria->addSelectColumn($alias . '.nomor_urut');
            $criteria->addSelectColumn($alias . '.tanggal_rencana');
            $criteria->addSelectColumn($alias . '.tanggal_pelaksanaan');
            $criteria->addSelectColumn($alias . '.waktu_berangkat');
            $criteria->addSelectColumn($alias . '.waktu_sampai');
            $criteria->addSelectColumn($alias . '.waktu_mulai_survey');
            $criteria->addSelectColumn($alias . '.waktu_selesai');
            $criteria->addSelectColumn($alias . '.waktu_isi_form_cetak');
            $criteria->addSelectColumn($alias . '.waktu_isi_form_elektronik');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SasaranSurveyPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SasaranSurveyPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(SasaranSurveyPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(SasaranSurveyPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 SasaranSurvey
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = SasaranSurveyPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return SasaranSurveyPeer::populateObjects(SasaranSurveyPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(SasaranSurveyPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            SasaranSurveyPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(SasaranSurveyPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      SasaranSurvey $obj A SasaranSurvey object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = serialize(array((string) $obj->getPenggunaId(), (string) $obj->getSekolahId()));
            } // if key === null
            SasaranSurveyPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A SasaranSurvey object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof SasaranSurvey) {
                $key = serialize(array((string) $value->getPenggunaId(), (string) $value->getSekolahId()));
            } elseif (is_array($value) && count($value) === 2) {
                // assume we've been passed a primary key
                $key = serialize(array((string) $value[0], (string) $value[1]));
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or SasaranSurvey object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(SasaranSurveyPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   SasaranSurvey Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(SasaranSurveyPeer::$instances[$key])) {
                return SasaranSurveyPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }
    
    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references)
      {
        foreach (SasaranSurveyPeer::$instances as $instance)
        {
          $instance->clearAllReferences(true);
        }
      }
        SasaranSurveyPeer::$instances = array();
    }
    
    /**
     * Method to invalidate the instance pool of all tables related to sasaran_survey
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null && $row[$startcol + 1] === null) {
            return null;
        }

        return serialize(array((string) $row[$startcol], (string) $row[$startcol + 1]));
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return array((string) $row[$startcol], (string) $row[$startcol + 1]);
    }
    
    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();
    
        // set the class once to avoid overhead in the loop
        $cls = SasaranSurveyPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = SasaranSurveyPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = SasaranSurveyPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                SasaranSurveyPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (SasaranSurvey object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = SasaranSurveyPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = SasaranSurveyPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + SasaranSurveyPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = SasaranSurveyPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            SasaranSurveyPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related PenggunaRelatedByPenggunaId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinPenggunaRelatedByPenggunaId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SasaranSurveyPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SasaranSurveyPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(SasaranSurveyPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SasaranSurveyPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SasaranSurveyPeer::PENGGUNA_ID, PenggunaPeer::PENGGUNA_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related PenggunaRelatedByPenggunaId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinPenggunaRelatedByPenggunaId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SasaranSurveyPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SasaranSurveyPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(SasaranSurveyPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SasaranSurveyPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SasaranSurveyPeer::PENGGUNA_ID, PenggunaPeer::PENGGUNA_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SekolahRelatedBySekolahId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinSekolahRelatedBySekolahId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SasaranSurveyPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SasaranSurveyPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(SasaranSurveyPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SasaranSurveyPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SasaranSurveyPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SekolahRelatedBySekolahId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinSekolahRelatedBySekolahId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SasaranSurveyPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SasaranSurveyPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(SasaranSurveyPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SasaranSurveyPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SasaranSurveyPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of SasaranSurvey objects pre-filled with their Pengguna objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of SasaranSurvey objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinPenggunaRelatedByPenggunaId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SasaranSurveyPeer::DATABASE_NAME);
        }

        SasaranSurveyPeer::addSelectColumns($criteria);
        $startcol = SasaranSurveyPeer::NUM_HYDRATE_COLUMNS;
        PenggunaPeer::addSelectColumns($criteria);

        $criteria->addJoin(SasaranSurveyPeer::PENGGUNA_ID, PenggunaPeer::PENGGUNA_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SasaranSurveyPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SasaranSurveyPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = SasaranSurveyPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SasaranSurveyPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = PenggunaPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = PenggunaPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = PenggunaPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    PenggunaPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (SasaranSurvey) to $obj2 (Pengguna)
                $obj2->addSasaranSurveyRelatedByPenggunaId($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of SasaranSurvey objects pre-filled with their Pengguna objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of SasaranSurvey objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinPenggunaRelatedByPenggunaId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SasaranSurveyPeer::DATABASE_NAME);
        }

        SasaranSurveyPeer::addSelectColumns($criteria);
        $startcol = SasaranSurveyPeer::NUM_HYDRATE_COLUMNS;
        PenggunaPeer::addSelectColumns($criteria);

        $criteria->addJoin(SasaranSurveyPeer::PENGGUNA_ID, PenggunaPeer::PENGGUNA_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SasaranSurveyPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SasaranSurveyPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = SasaranSurveyPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SasaranSurveyPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = PenggunaPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = PenggunaPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = PenggunaPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    PenggunaPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (SasaranSurvey) to $obj2 (Pengguna)
                $obj2->addSasaranSurveyRelatedByPenggunaId($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of SasaranSurvey objects pre-filled with their Sekolah objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of SasaranSurvey objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinSekolahRelatedBySekolahId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SasaranSurveyPeer::DATABASE_NAME);
        }

        SasaranSurveyPeer::addSelectColumns($criteria);
        $startcol = SasaranSurveyPeer::NUM_HYDRATE_COLUMNS;
        SekolahPeer::addSelectColumns($criteria);

        $criteria->addJoin(SasaranSurveyPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SasaranSurveyPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SasaranSurveyPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = SasaranSurveyPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SasaranSurveyPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = SekolahPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = SekolahPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    SekolahPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (SasaranSurvey) to $obj2 (Sekolah)
                $obj2->addSasaranSurveyRelatedBySekolahId($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of SasaranSurvey objects pre-filled with their Sekolah objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of SasaranSurvey objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinSekolahRelatedBySekolahId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SasaranSurveyPeer::DATABASE_NAME);
        }

        SasaranSurveyPeer::addSelectColumns($criteria);
        $startcol = SasaranSurveyPeer::NUM_HYDRATE_COLUMNS;
        SekolahPeer::addSelectColumns($criteria);

        $criteria->addJoin(SasaranSurveyPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SasaranSurveyPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SasaranSurveyPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = SasaranSurveyPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SasaranSurveyPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = SekolahPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = SekolahPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    SekolahPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (SasaranSurvey) to $obj2 (Sekolah)
                $obj2->addSasaranSurveyRelatedBySekolahId($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SasaranSurveyPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SasaranSurveyPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(SasaranSurveyPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SasaranSurveyPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SasaranSurveyPeer::PENGGUNA_ID, PenggunaPeer::PENGGUNA_ID, $join_behavior);

        $criteria->addJoin(SasaranSurveyPeer::PENGGUNA_ID, PenggunaPeer::PENGGUNA_ID, $join_behavior);

        $criteria->addJoin(SasaranSurveyPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(SasaranSurveyPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of SasaranSurvey objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of SasaranSurvey objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SasaranSurveyPeer::DATABASE_NAME);
        }

        SasaranSurveyPeer::addSelectColumns($criteria);
        $startcol2 = SasaranSurveyPeer::NUM_HYDRATE_COLUMNS;

        PenggunaPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + PenggunaPeer::NUM_HYDRATE_COLUMNS;

        PenggunaPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + PenggunaPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(SasaranSurveyPeer::PENGGUNA_ID, PenggunaPeer::PENGGUNA_ID, $join_behavior);

        $criteria->addJoin(SasaranSurveyPeer::PENGGUNA_ID, PenggunaPeer::PENGGUNA_ID, $join_behavior);

        $criteria->addJoin(SasaranSurveyPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(SasaranSurveyPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SasaranSurveyPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SasaranSurveyPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = SasaranSurveyPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SasaranSurveyPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined Pengguna rows

            $key2 = PenggunaPeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = PenggunaPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = PenggunaPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    PenggunaPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (SasaranSurvey) to the collection in $obj2 (Pengguna)
                $obj2->addSasaranSurveyRelatedByPenggunaId($obj1);
            } // if joined row not null

            // Add objects for joined Pengguna rows

            $key3 = PenggunaPeer::getPrimaryKeyHashFromRow($row, $startcol3);
            if ($key3 !== null) {
                $obj3 = PenggunaPeer::getInstanceFromPool($key3);
                if (!$obj3) {

                    $cls = PenggunaPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    PenggunaPeer::addInstanceToPool($obj3, $key3);
                } // if obj3 loaded

                // Add the $obj1 (SasaranSurvey) to the collection in $obj3 (Pengguna)
                $obj3->addSasaranSurveyRelatedByPenggunaId($obj1);
            } // if joined row not null

            // Add objects for joined Sekolah rows

            $key4 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol4);
            if ($key4 !== null) {
                $obj4 = SekolahPeer::getInstanceFromPool($key4);
                if (!$obj4) {

                    $cls = SekolahPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    SekolahPeer::addInstanceToPool($obj4, $key4);
                } // if obj4 loaded

                // Add the $obj1 (SasaranSurvey) to the collection in $obj4 (Sekolah)
                $obj4->addSasaranSurveyRelatedBySekolahId($obj1);
            } // if joined row not null

            // Add objects for joined Sekolah rows

            $key5 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol5);
            if ($key5 !== null) {
                $obj5 = SekolahPeer::getInstanceFromPool($key5);
                if (!$obj5) {

                    $cls = SekolahPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    SekolahPeer::addInstanceToPool($obj5, $key5);
                } // if obj5 loaded

                // Add the $obj1 (SasaranSurvey) to the collection in $obj5 (Sekolah)
                $obj5->addSasaranSurveyRelatedBySekolahId($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining the related PenggunaRelatedByPenggunaId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptPenggunaRelatedByPenggunaId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SasaranSurveyPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SasaranSurveyPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(SasaranSurveyPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SasaranSurveyPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addJoin(SasaranSurveyPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(SasaranSurveyPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related PenggunaRelatedByPenggunaId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptPenggunaRelatedByPenggunaId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SasaranSurveyPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SasaranSurveyPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(SasaranSurveyPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SasaranSurveyPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addJoin(SasaranSurveyPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(SasaranSurveyPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SekolahRelatedBySekolahId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptSekolahRelatedBySekolahId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SasaranSurveyPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SasaranSurveyPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(SasaranSurveyPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SasaranSurveyPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addJoin(SasaranSurveyPeer::PENGGUNA_ID, PenggunaPeer::PENGGUNA_ID, $join_behavior);

        $criteria->addJoin(SasaranSurveyPeer::PENGGUNA_ID, PenggunaPeer::PENGGUNA_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SekolahRelatedBySekolahId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptSekolahRelatedBySekolahId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SasaranSurveyPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SasaranSurveyPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(SasaranSurveyPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SasaranSurveyPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addJoin(SasaranSurveyPeer::PENGGUNA_ID, PenggunaPeer::PENGGUNA_ID, $join_behavior);

        $criteria->addJoin(SasaranSurveyPeer::PENGGUNA_ID, PenggunaPeer::PENGGUNA_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of SasaranSurvey objects pre-filled with all related objects except PenggunaRelatedByPenggunaId.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of SasaranSurvey objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptPenggunaRelatedByPenggunaId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SasaranSurveyPeer::DATABASE_NAME);
        }

        SasaranSurveyPeer::addSelectColumns($criteria);
        $startcol2 = SasaranSurveyPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(SasaranSurveyPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(SasaranSurveyPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SasaranSurveyPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SasaranSurveyPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = SasaranSurveyPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SasaranSurveyPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Sekolah rows

                $key2 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = SekolahPeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = SekolahPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SekolahPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (SasaranSurvey) to the collection in $obj2 (Sekolah)
                $obj2->addSasaranSurveyRelatedBySekolahId($obj1);

            } // if joined row is not null

                // Add objects for joined Sekolah rows

                $key3 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = SekolahPeer::getInstanceFromPool($key3);
                    if (!$obj3) {
    
                        $cls = SekolahPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    SekolahPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (SasaranSurvey) to the collection in $obj3 (Sekolah)
                $obj3->addSasaranSurveyRelatedBySekolahId($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of SasaranSurvey objects pre-filled with all related objects except PenggunaRelatedByPenggunaId.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of SasaranSurvey objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptPenggunaRelatedByPenggunaId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SasaranSurveyPeer::DATABASE_NAME);
        }

        SasaranSurveyPeer::addSelectColumns($criteria);
        $startcol2 = SasaranSurveyPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(SasaranSurveyPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(SasaranSurveyPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SasaranSurveyPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SasaranSurveyPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = SasaranSurveyPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SasaranSurveyPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Sekolah rows

                $key2 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = SekolahPeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = SekolahPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SekolahPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (SasaranSurvey) to the collection in $obj2 (Sekolah)
                $obj2->addSasaranSurveyRelatedBySekolahId($obj1);

            } // if joined row is not null

                // Add objects for joined Sekolah rows

                $key3 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = SekolahPeer::getInstanceFromPool($key3);
                    if (!$obj3) {
    
                        $cls = SekolahPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    SekolahPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (SasaranSurvey) to the collection in $obj3 (Sekolah)
                $obj3->addSasaranSurveyRelatedBySekolahId($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of SasaranSurvey objects pre-filled with all related objects except SekolahRelatedBySekolahId.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of SasaranSurvey objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptSekolahRelatedBySekolahId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SasaranSurveyPeer::DATABASE_NAME);
        }

        SasaranSurveyPeer::addSelectColumns($criteria);
        $startcol2 = SasaranSurveyPeer::NUM_HYDRATE_COLUMNS;

        PenggunaPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + PenggunaPeer::NUM_HYDRATE_COLUMNS;

        PenggunaPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + PenggunaPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(SasaranSurveyPeer::PENGGUNA_ID, PenggunaPeer::PENGGUNA_ID, $join_behavior);

        $criteria->addJoin(SasaranSurveyPeer::PENGGUNA_ID, PenggunaPeer::PENGGUNA_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SasaranSurveyPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SasaranSurveyPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = SasaranSurveyPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SasaranSurveyPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Pengguna rows

                $key2 = PenggunaPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = PenggunaPeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = PenggunaPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    PenggunaPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (SasaranSurvey) to the collection in $obj2 (Pengguna)
                $obj2->addSasaranSurveyRelatedByPenggunaId($obj1);

            } // if joined row is not null

                // Add objects for joined Pengguna rows

                $key3 = PenggunaPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = PenggunaPeer::getInstanceFromPool($key3);
                    if (!$obj3) {
    
                        $cls = PenggunaPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    PenggunaPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (SasaranSurvey) to the collection in $obj3 (Pengguna)
                $obj3->addSasaranSurveyRelatedByPenggunaId($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of SasaranSurvey objects pre-filled with all related objects except SekolahRelatedBySekolahId.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of SasaranSurvey objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptSekolahRelatedBySekolahId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SasaranSurveyPeer::DATABASE_NAME);
        }

        SasaranSurveyPeer::addSelectColumns($criteria);
        $startcol2 = SasaranSurveyPeer::NUM_HYDRATE_COLUMNS;

        PenggunaPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + PenggunaPeer::NUM_HYDRATE_COLUMNS;

        PenggunaPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + PenggunaPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(SasaranSurveyPeer::PENGGUNA_ID, PenggunaPeer::PENGGUNA_ID, $join_behavior);

        $criteria->addJoin(SasaranSurveyPeer::PENGGUNA_ID, PenggunaPeer::PENGGUNA_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SasaranSurveyPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SasaranSurveyPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = SasaranSurveyPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SasaranSurveyPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Pengguna rows

                $key2 = PenggunaPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = PenggunaPeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = PenggunaPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    PenggunaPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (SasaranSurvey) to the collection in $obj2 (Pengguna)
                $obj2->addSasaranSurveyRelatedByPenggunaId($obj1);

            } // if joined row is not null

                // Add objects for joined Pengguna rows

                $key3 = PenggunaPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = PenggunaPeer::getInstanceFromPool($key3);
                    if (!$obj3) {
    
                        $cls = PenggunaPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    PenggunaPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (SasaranSurvey) to the collection in $obj3 (Pengguna)
                $obj3->addSasaranSurveyRelatedByPenggunaId($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(SasaranSurveyPeer::DATABASE_NAME)->getTable(SasaranSurveyPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseSasaranSurveyPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseSasaranSurveyPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new SasaranSurveyTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass()
    {
        return SasaranSurveyPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a SasaranSurvey or Criteria object.
     *
     * @param      mixed $values Criteria or SasaranSurvey object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(SasaranSurveyPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from SasaranSurvey object
        }


        // Set the correct dbName
        $criteria->setDbName(SasaranSurveyPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a SasaranSurvey or Criteria object.
     *
     * @param      mixed $values Criteria or SasaranSurvey object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(SasaranSurveyPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(SasaranSurveyPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(SasaranSurveyPeer::PENGGUNA_ID);
            $value = $criteria->remove(SasaranSurveyPeer::PENGGUNA_ID);
            if ($value) {
                $selectCriteria->add(SasaranSurveyPeer::PENGGUNA_ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(SasaranSurveyPeer::TABLE_NAME);
            }

            $comparison = $criteria->getComparison(SasaranSurveyPeer::SEKOLAH_ID);
            $value = $criteria->remove(SasaranSurveyPeer::SEKOLAH_ID);
            if ($value) {
                $selectCriteria->add(SasaranSurveyPeer::SEKOLAH_ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(SasaranSurveyPeer::TABLE_NAME);
            }

        } else { // $values is SasaranSurvey object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(SasaranSurveyPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the sasaran_survey table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(SasaranSurveyPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(SasaranSurveyPeer::TABLE_NAME, $con, SasaranSurveyPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SasaranSurveyPeer::clearInstancePool();
            SasaranSurveyPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a SasaranSurvey or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or SasaranSurvey object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(SasaranSurveyPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            SasaranSurveyPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof SasaranSurvey) { // it's a model object
            // invalidate the cache for this single object
            SasaranSurveyPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(SasaranSurveyPeer::DATABASE_NAME);
            // primary key is composite; we therefore, expect
            // the primary key passed to be an array of pkey values
            if (count($values) == count($values, COUNT_RECURSIVE)) {
                // array is not multi-dimensional
                $values = array($values);
            }
            foreach ($values as $value) {
                $criterion = $criteria->getNewCriterion(SasaranSurveyPeer::PENGGUNA_ID, $value[0]);
                $criterion->addAnd($criteria->getNewCriterion(SasaranSurveyPeer::SEKOLAH_ID, $value[1]));
                $criteria->addOr($criterion);
                // we can invalidate the cache for this single PK
                SasaranSurveyPeer::removeInstanceFromPool($value);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(SasaranSurveyPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            
            $affectedRows += BasePeer::doDelete($criteria, $con);
            SasaranSurveyPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given SasaranSurvey object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      SasaranSurvey $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(SasaranSurveyPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(SasaranSurveyPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(SasaranSurveyPeer::DATABASE_NAME, SasaranSurveyPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve object using using composite pkey values.
     * @param   string $pengguna_id
     * @param   string $sekolah_id
     * @param      PropelPDO $con
     * @return   SasaranSurvey
     */
    public static function retrieveByPK($pengguna_id, $sekolah_id, PropelPDO $con = null) {
        $_instancePoolKey = serialize(array((string) $pengguna_id, (string) $sekolah_id));
         if (null !== ($obj = SasaranSurveyPeer::getInstanceFromPool($_instancePoolKey))) {
             return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(SasaranSurveyPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $criteria = new Criteria(SasaranSurveyPeer::DATABASE_NAME);
        $criteria->add(SasaranSurveyPeer::PENGGUNA_ID, $pengguna_id);
        $criteria->add(SasaranSurveyPeer::SEKOLAH_ID, $sekolah_id);
        $v = SasaranSurveyPeer::doSelect($criteria, $con);

        return !empty($v) ? $v[0] : null;
    }
} // BaseSasaranSurveyPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseSasaranSurveyPeer::buildTableMap();

