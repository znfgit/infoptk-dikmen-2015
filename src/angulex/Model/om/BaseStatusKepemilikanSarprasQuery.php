<?php

namespace angulex\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\Prasarana;
use angulex\Model\Sarana;
use angulex\Model\StatusKepemilikanSarpras;
use angulex\Model\StatusKepemilikanSarprasPeer;
use angulex\Model\StatusKepemilikanSarprasQuery;

/**
 * Base class that represents a query for the 'ref.status_kepemilikan_sarpras' table.
 *
 * 
 *
 * @method StatusKepemilikanSarprasQuery orderByKepemilikanSarprasId($order = Criteria::ASC) Order by the kepemilikan_sarpras_id column
 * @method StatusKepemilikanSarprasQuery orderByNama($order = Criteria::ASC) Order by the nama column
 * @method StatusKepemilikanSarprasQuery orderByCreateDate($order = Criteria::ASC) Order by the create_date column
 * @method StatusKepemilikanSarprasQuery orderByLastUpdate($order = Criteria::ASC) Order by the last_update column
 * @method StatusKepemilikanSarprasQuery orderByExpiredDate($order = Criteria::ASC) Order by the expired_date column
 * @method StatusKepemilikanSarprasQuery orderByLastSync($order = Criteria::ASC) Order by the last_sync column
 *
 * @method StatusKepemilikanSarprasQuery groupByKepemilikanSarprasId() Group by the kepemilikan_sarpras_id column
 * @method StatusKepemilikanSarprasQuery groupByNama() Group by the nama column
 * @method StatusKepemilikanSarprasQuery groupByCreateDate() Group by the create_date column
 * @method StatusKepemilikanSarprasQuery groupByLastUpdate() Group by the last_update column
 * @method StatusKepemilikanSarprasQuery groupByExpiredDate() Group by the expired_date column
 * @method StatusKepemilikanSarprasQuery groupByLastSync() Group by the last_sync column
 *
 * @method StatusKepemilikanSarprasQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method StatusKepemilikanSarprasQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method StatusKepemilikanSarprasQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method StatusKepemilikanSarprasQuery leftJoinSaranaRelatedByKepemilikanSarprasId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SaranaRelatedByKepemilikanSarprasId relation
 * @method StatusKepemilikanSarprasQuery rightJoinSaranaRelatedByKepemilikanSarprasId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SaranaRelatedByKepemilikanSarprasId relation
 * @method StatusKepemilikanSarprasQuery innerJoinSaranaRelatedByKepemilikanSarprasId($relationAlias = null) Adds a INNER JOIN clause to the query using the SaranaRelatedByKepemilikanSarprasId relation
 *
 * @method StatusKepemilikanSarprasQuery leftJoinSaranaRelatedByKepemilikanSarprasId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SaranaRelatedByKepemilikanSarprasId relation
 * @method StatusKepemilikanSarprasQuery rightJoinSaranaRelatedByKepemilikanSarprasId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SaranaRelatedByKepemilikanSarprasId relation
 * @method StatusKepemilikanSarprasQuery innerJoinSaranaRelatedByKepemilikanSarprasId($relationAlias = null) Adds a INNER JOIN clause to the query using the SaranaRelatedByKepemilikanSarprasId relation
 *
 * @method StatusKepemilikanSarprasQuery leftJoinPrasaranaRelatedByKepemilikanSarprasId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PrasaranaRelatedByKepemilikanSarprasId relation
 * @method StatusKepemilikanSarprasQuery rightJoinPrasaranaRelatedByKepemilikanSarprasId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PrasaranaRelatedByKepemilikanSarprasId relation
 * @method StatusKepemilikanSarprasQuery innerJoinPrasaranaRelatedByKepemilikanSarprasId($relationAlias = null) Adds a INNER JOIN clause to the query using the PrasaranaRelatedByKepemilikanSarprasId relation
 *
 * @method StatusKepemilikanSarprasQuery leftJoinPrasaranaRelatedByKepemilikanSarprasId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PrasaranaRelatedByKepemilikanSarprasId relation
 * @method StatusKepemilikanSarprasQuery rightJoinPrasaranaRelatedByKepemilikanSarprasId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PrasaranaRelatedByKepemilikanSarprasId relation
 * @method StatusKepemilikanSarprasQuery innerJoinPrasaranaRelatedByKepemilikanSarprasId($relationAlias = null) Adds a INNER JOIN clause to the query using the PrasaranaRelatedByKepemilikanSarprasId relation
 *
 * @method StatusKepemilikanSarpras findOne(PropelPDO $con = null) Return the first StatusKepemilikanSarpras matching the query
 * @method StatusKepemilikanSarpras findOneOrCreate(PropelPDO $con = null) Return the first StatusKepemilikanSarpras matching the query, or a new StatusKepemilikanSarpras object populated from the query conditions when no match is found
 *
 * @method StatusKepemilikanSarpras findOneByNama(string $nama) Return the first StatusKepemilikanSarpras filtered by the nama column
 * @method StatusKepemilikanSarpras findOneByCreateDate(string $create_date) Return the first StatusKepemilikanSarpras filtered by the create_date column
 * @method StatusKepemilikanSarpras findOneByLastUpdate(string $last_update) Return the first StatusKepemilikanSarpras filtered by the last_update column
 * @method StatusKepemilikanSarpras findOneByExpiredDate(string $expired_date) Return the first StatusKepemilikanSarpras filtered by the expired_date column
 * @method StatusKepemilikanSarpras findOneByLastSync(string $last_sync) Return the first StatusKepemilikanSarpras filtered by the last_sync column
 *
 * @method array findByKepemilikanSarprasId(string $kepemilikan_sarpras_id) Return StatusKepemilikanSarpras objects filtered by the kepemilikan_sarpras_id column
 * @method array findByNama(string $nama) Return StatusKepemilikanSarpras objects filtered by the nama column
 * @method array findByCreateDate(string $create_date) Return StatusKepemilikanSarpras objects filtered by the create_date column
 * @method array findByLastUpdate(string $last_update) Return StatusKepemilikanSarpras objects filtered by the last_update column
 * @method array findByExpiredDate(string $expired_date) Return StatusKepemilikanSarpras objects filtered by the expired_date column
 * @method array findByLastSync(string $last_sync) Return StatusKepemilikanSarpras objects filtered by the last_sync column
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseStatusKepemilikanSarprasQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseStatusKepemilikanSarprasQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Dapodikmen', $modelName = 'angulex\\Model\\StatusKepemilikanSarpras', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new StatusKepemilikanSarprasQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   StatusKepemilikanSarprasQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return StatusKepemilikanSarprasQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof StatusKepemilikanSarprasQuery) {
            return $criteria;
        }
        $query = new StatusKepemilikanSarprasQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   StatusKepemilikanSarpras|StatusKepemilikanSarpras[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = StatusKepemilikanSarprasPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(StatusKepemilikanSarprasPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 StatusKepemilikanSarpras A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByKepemilikanSarprasId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 StatusKepemilikanSarpras A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [kepemilikan_sarpras_id], [nama], [create_date], [last_update], [expired_date], [last_sync] FROM [ref].[status_kepemilikan_sarpras] WHERE [kepemilikan_sarpras_id] = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new StatusKepemilikanSarpras();
            $obj->hydrate($row);
            StatusKepemilikanSarprasPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return StatusKepemilikanSarpras|StatusKepemilikanSarpras[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|StatusKepemilikanSarpras[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return StatusKepemilikanSarprasQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(StatusKepemilikanSarprasPeer::KEPEMILIKAN_SARPRAS_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return StatusKepemilikanSarprasQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(StatusKepemilikanSarprasPeer::KEPEMILIKAN_SARPRAS_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the kepemilikan_sarpras_id column
     *
     * Example usage:
     * <code>
     * $query->filterByKepemilikanSarprasId(1234); // WHERE kepemilikan_sarpras_id = 1234
     * $query->filterByKepemilikanSarprasId(array(12, 34)); // WHERE kepemilikan_sarpras_id IN (12, 34)
     * $query->filterByKepemilikanSarprasId(array('min' => 12)); // WHERE kepemilikan_sarpras_id >= 12
     * $query->filterByKepemilikanSarprasId(array('max' => 12)); // WHERE kepemilikan_sarpras_id <= 12
     * </code>
     *
     * @param     mixed $kepemilikanSarprasId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return StatusKepemilikanSarprasQuery The current query, for fluid interface
     */
    public function filterByKepemilikanSarprasId($kepemilikanSarprasId = null, $comparison = null)
    {
        if (is_array($kepemilikanSarprasId)) {
            $useMinMax = false;
            if (isset($kepemilikanSarprasId['min'])) {
                $this->addUsingAlias(StatusKepemilikanSarprasPeer::KEPEMILIKAN_SARPRAS_ID, $kepemilikanSarprasId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($kepemilikanSarprasId['max'])) {
                $this->addUsingAlias(StatusKepemilikanSarprasPeer::KEPEMILIKAN_SARPRAS_ID, $kepemilikanSarprasId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StatusKepemilikanSarprasPeer::KEPEMILIKAN_SARPRAS_ID, $kepemilikanSarprasId, $comparison);
    }

    /**
     * Filter the query on the nama column
     *
     * Example usage:
     * <code>
     * $query->filterByNama('fooValue');   // WHERE nama = 'fooValue'
     * $query->filterByNama('%fooValue%'); // WHERE nama LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nama The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return StatusKepemilikanSarprasQuery The current query, for fluid interface
     */
    public function filterByNama($nama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nama)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nama)) {
                $nama = str_replace('*', '%', $nama);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(StatusKepemilikanSarprasPeer::NAMA, $nama, $comparison);
    }

    /**
     * Filter the query on the create_date column
     *
     * Example usage:
     * <code>
     * $query->filterByCreateDate('2011-03-14'); // WHERE create_date = '2011-03-14'
     * $query->filterByCreateDate('now'); // WHERE create_date = '2011-03-14'
     * $query->filterByCreateDate(array('max' => 'yesterday')); // WHERE create_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $createDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return StatusKepemilikanSarprasQuery The current query, for fluid interface
     */
    public function filterByCreateDate($createDate = null, $comparison = null)
    {
        if (is_array($createDate)) {
            $useMinMax = false;
            if (isset($createDate['min'])) {
                $this->addUsingAlias(StatusKepemilikanSarprasPeer::CREATE_DATE, $createDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createDate['max'])) {
                $this->addUsingAlias(StatusKepemilikanSarprasPeer::CREATE_DATE, $createDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StatusKepemilikanSarprasPeer::CREATE_DATE, $createDate, $comparison);
    }

    /**
     * Filter the query on the last_update column
     *
     * Example usage:
     * <code>
     * $query->filterByLastUpdate('2011-03-14'); // WHERE last_update = '2011-03-14'
     * $query->filterByLastUpdate('now'); // WHERE last_update = '2011-03-14'
     * $query->filterByLastUpdate(array('max' => 'yesterday')); // WHERE last_update > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastUpdate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return StatusKepemilikanSarprasQuery The current query, for fluid interface
     */
    public function filterByLastUpdate($lastUpdate = null, $comparison = null)
    {
        if (is_array($lastUpdate)) {
            $useMinMax = false;
            if (isset($lastUpdate['min'])) {
                $this->addUsingAlias(StatusKepemilikanSarprasPeer::LAST_UPDATE, $lastUpdate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastUpdate['max'])) {
                $this->addUsingAlias(StatusKepemilikanSarprasPeer::LAST_UPDATE, $lastUpdate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StatusKepemilikanSarprasPeer::LAST_UPDATE, $lastUpdate, $comparison);
    }

    /**
     * Filter the query on the expired_date column
     *
     * Example usage:
     * <code>
     * $query->filterByExpiredDate('2011-03-14'); // WHERE expired_date = '2011-03-14'
     * $query->filterByExpiredDate('now'); // WHERE expired_date = '2011-03-14'
     * $query->filterByExpiredDate(array('max' => 'yesterday')); // WHERE expired_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $expiredDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return StatusKepemilikanSarprasQuery The current query, for fluid interface
     */
    public function filterByExpiredDate($expiredDate = null, $comparison = null)
    {
        if (is_array($expiredDate)) {
            $useMinMax = false;
            if (isset($expiredDate['min'])) {
                $this->addUsingAlias(StatusKepemilikanSarprasPeer::EXPIRED_DATE, $expiredDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($expiredDate['max'])) {
                $this->addUsingAlias(StatusKepemilikanSarprasPeer::EXPIRED_DATE, $expiredDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StatusKepemilikanSarprasPeer::EXPIRED_DATE, $expiredDate, $comparison);
    }

    /**
     * Filter the query on the last_sync column
     *
     * Example usage:
     * <code>
     * $query->filterByLastSync('2011-03-14'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync('now'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync(array('max' => 'yesterday')); // WHERE last_sync > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastSync The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return StatusKepemilikanSarprasQuery The current query, for fluid interface
     */
    public function filterByLastSync($lastSync = null, $comparison = null)
    {
        if (is_array($lastSync)) {
            $useMinMax = false;
            if (isset($lastSync['min'])) {
                $this->addUsingAlias(StatusKepemilikanSarprasPeer::LAST_SYNC, $lastSync['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastSync['max'])) {
                $this->addUsingAlias(StatusKepemilikanSarprasPeer::LAST_SYNC, $lastSync['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StatusKepemilikanSarprasPeer::LAST_SYNC, $lastSync, $comparison);
    }

    /**
     * Filter the query by a related Sarana object
     *
     * @param   Sarana|PropelObjectCollection $sarana  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 StatusKepemilikanSarprasQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySaranaRelatedByKepemilikanSarprasId($sarana, $comparison = null)
    {
        if ($sarana instanceof Sarana) {
            return $this
                ->addUsingAlias(StatusKepemilikanSarprasPeer::KEPEMILIKAN_SARPRAS_ID, $sarana->getKepemilikanSarprasId(), $comparison);
        } elseif ($sarana instanceof PropelObjectCollection) {
            return $this
                ->useSaranaRelatedByKepemilikanSarprasIdQuery()
                ->filterByPrimaryKeys($sarana->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySaranaRelatedByKepemilikanSarprasId() only accepts arguments of type Sarana or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SaranaRelatedByKepemilikanSarprasId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return StatusKepemilikanSarprasQuery The current query, for fluid interface
     */
    public function joinSaranaRelatedByKepemilikanSarprasId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SaranaRelatedByKepemilikanSarprasId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SaranaRelatedByKepemilikanSarprasId');
        }

        return $this;
    }

    /**
     * Use the SaranaRelatedByKepemilikanSarprasId relation Sarana object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SaranaQuery A secondary query class using the current class as primary query
     */
    public function useSaranaRelatedByKepemilikanSarprasIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSaranaRelatedByKepemilikanSarprasId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SaranaRelatedByKepemilikanSarprasId', '\angulex\Model\SaranaQuery');
    }

    /**
     * Filter the query by a related Sarana object
     *
     * @param   Sarana|PropelObjectCollection $sarana  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 StatusKepemilikanSarprasQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySaranaRelatedByKepemilikanSarprasId($sarana, $comparison = null)
    {
        if ($sarana instanceof Sarana) {
            return $this
                ->addUsingAlias(StatusKepemilikanSarprasPeer::KEPEMILIKAN_SARPRAS_ID, $sarana->getKepemilikanSarprasId(), $comparison);
        } elseif ($sarana instanceof PropelObjectCollection) {
            return $this
                ->useSaranaRelatedByKepemilikanSarprasIdQuery()
                ->filterByPrimaryKeys($sarana->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySaranaRelatedByKepemilikanSarprasId() only accepts arguments of type Sarana or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SaranaRelatedByKepemilikanSarprasId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return StatusKepemilikanSarprasQuery The current query, for fluid interface
     */
    public function joinSaranaRelatedByKepemilikanSarprasId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SaranaRelatedByKepemilikanSarprasId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SaranaRelatedByKepemilikanSarprasId');
        }

        return $this;
    }

    /**
     * Use the SaranaRelatedByKepemilikanSarprasId relation Sarana object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SaranaQuery A secondary query class using the current class as primary query
     */
    public function useSaranaRelatedByKepemilikanSarprasIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSaranaRelatedByKepemilikanSarprasId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SaranaRelatedByKepemilikanSarprasId', '\angulex\Model\SaranaQuery');
    }

    /**
     * Filter the query by a related Prasarana object
     *
     * @param   Prasarana|PropelObjectCollection $prasarana  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 StatusKepemilikanSarprasQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPrasaranaRelatedByKepemilikanSarprasId($prasarana, $comparison = null)
    {
        if ($prasarana instanceof Prasarana) {
            return $this
                ->addUsingAlias(StatusKepemilikanSarprasPeer::KEPEMILIKAN_SARPRAS_ID, $prasarana->getKepemilikanSarprasId(), $comparison);
        } elseif ($prasarana instanceof PropelObjectCollection) {
            return $this
                ->usePrasaranaRelatedByKepemilikanSarprasIdQuery()
                ->filterByPrimaryKeys($prasarana->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPrasaranaRelatedByKepemilikanSarprasId() only accepts arguments of type Prasarana or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PrasaranaRelatedByKepemilikanSarprasId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return StatusKepemilikanSarprasQuery The current query, for fluid interface
     */
    public function joinPrasaranaRelatedByKepemilikanSarprasId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PrasaranaRelatedByKepemilikanSarprasId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PrasaranaRelatedByKepemilikanSarprasId');
        }

        return $this;
    }

    /**
     * Use the PrasaranaRelatedByKepemilikanSarprasId relation Prasarana object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PrasaranaQuery A secondary query class using the current class as primary query
     */
    public function usePrasaranaRelatedByKepemilikanSarprasIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPrasaranaRelatedByKepemilikanSarprasId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PrasaranaRelatedByKepemilikanSarprasId', '\angulex\Model\PrasaranaQuery');
    }

    /**
     * Filter the query by a related Prasarana object
     *
     * @param   Prasarana|PropelObjectCollection $prasarana  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 StatusKepemilikanSarprasQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPrasaranaRelatedByKepemilikanSarprasId($prasarana, $comparison = null)
    {
        if ($prasarana instanceof Prasarana) {
            return $this
                ->addUsingAlias(StatusKepemilikanSarprasPeer::KEPEMILIKAN_SARPRAS_ID, $prasarana->getKepemilikanSarprasId(), $comparison);
        } elseif ($prasarana instanceof PropelObjectCollection) {
            return $this
                ->usePrasaranaRelatedByKepemilikanSarprasIdQuery()
                ->filterByPrimaryKeys($prasarana->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPrasaranaRelatedByKepemilikanSarprasId() only accepts arguments of type Prasarana or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PrasaranaRelatedByKepemilikanSarprasId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return StatusKepemilikanSarprasQuery The current query, for fluid interface
     */
    public function joinPrasaranaRelatedByKepemilikanSarprasId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PrasaranaRelatedByKepemilikanSarprasId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PrasaranaRelatedByKepemilikanSarprasId');
        }

        return $this;
    }

    /**
     * Use the PrasaranaRelatedByKepemilikanSarprasId relation Prasarana object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PrasaranaQuery A secondary query class using the current class as primary query
     */
    public function usePrasaranaRelatedByKepemilikanSarprasIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPrasaranaRelatedByKepemilikanSarprasId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PrasaranaRelatedByKepemilikanSarprasId', '\angulex\Model\PrasaranaQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   StatusKepemilikanSarpras $statusKepemilikanSarpras Object to remove from the list of results
     *
     * @return StatusKepemilikanSarprasQuery The current query, for fluid interface
     */
    public function prune($statusKepemilikanSarpras = null)
    {
        if ($statusKepemilikanSarpras) {
            $this->addUsingAlias(StatusKepemilikanSarprasPeer::KEPEMILIKAN_SARPRAS_ID, $statusKepemilikanSarpras->getKepemilikanSarprasId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
