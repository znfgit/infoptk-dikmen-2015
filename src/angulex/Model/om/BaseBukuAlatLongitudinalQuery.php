<?php

namespace angulex\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\BukuAlat;
use angulex\Model\BukuAlatLongitudinal;
use angulex\Model\BukuAlatLongitudinalPeer;
use angulex\Model\BukuAlatLongitudinalQuery;
use angulex\Model\Semester;

/**
 * Base class that represents a query for the 'buku_alat_longitudinal' table.
 *
 * 
 *
 * @method BukuAlatLongitudinalQuery orderByBukuAlatId($order = Criteria::ASC) Order by the buku_alat_id column
 * @method BukuAlatLongitudinalQuery orderBySemesterId($order = Criteria::ASC) Order by the semester_id column
 * @method BukuAlatLongitudinalQuery orderByJumlah($order = Criteria::ASC) Order by the jumlah column
 * @method BukuAlatLongitudinalQuery orderByStatusKelaikan($order = Criteria::ASC) Order by the status_kelaikan column
 * @method BukuAlatLongitudinalQuery orderByLastUpdate($order = Criteria::ASC) Order by the Last_update column
 * @method BukuAlatLongitudinalQuery orderBySoftDelete($order = Criteria::ASC) Order by the Soft_delete column
 * @method BukuAlatLongitudinalQuery orderByLastSync($order = Criteria::ASC) Order by the last_sync column
 * @method BukuAlatLongitudinalQuery orderByUpdaterId($order = Criteria::ASC) Order by the Updater_ID column
 *
 * @method BukuAlatLongitudinalQuery groupByBukuAlatId() Group by the buku_alat_id column
 * @method BukuAlatLongitudinalQuery groupBySemesterId() Group by the semester_id column
 * @method BukuAlatLongitudinalQuery groupByJumlah() Group by the jumlah column
 * @method BukuAlatLongitudinalQuery groupByStatusKelaikan() Group by the status_kelaikan column
 * @method BukuAlatLongitudinalQuery groupByLastUpdate() Group by the Last_update column
 * @method BukuAlatLongitudinalQuery groupBySoftDelete() Group by the Soft_delete column
 * @method BukuAlatLongitudinalQuery groupByLastSync() Group by the last_sync column
 * @method BukuAlatLongitudinalQuery groupByUpdaterId() Group by the Updater_ID column
 *
 * @method BukuAlatLongitudinalQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method BukuAlatLongitudinalQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method BukuAlatLongitudinalQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method BukuAlatLongitudinalQuery leftJoinBukuAlatRelatedByBukuAlatId($relationAlias = null) Adds a LEFT JOIN clause to the query using the BukuAlatRelatedByBukuAlatId relation
 * @method BukuAlatLongitudinalQuery rightJoinBukuAlatRelatedByBukuAlatId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BukuAlatRelatedByBukuAlatId relation
 * @method BukuAlatLongitudinalQuery innerJoinBukuAlatRelatedByBukuAlatId($relationAlias = null) Adds a INNER JOIN clause to the query using the BukuAlatRelatedByBukuAlatId relation
 *
 * @method BukuAlatLongitudinalQuery leftJoinBukuAlatRelatedByBukuAlatId($relationAlias = null) Adds a LEFT JOIN clause to the query using the BukuAlatRelatedByBukuAlatId relation
 * @method BukuAlatLongitudinalQuery rightJoinBukuAlatRelatedByBukuAlatId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BukuAlatRelatedByBukuAlatId relation
 * @method BukuAlatLongitudinalQuery innerJoinBukuAlatRelatedByBukuAlatId($relationAlias = null) Adds a INNER JOIN clause to the query using the BukuAlatRelatedByBukuAlatId relation
 *
 * @method BukuAlatLongitudinalQuery leftJoinSemesterRelatedBySemesterId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SemesterRelatedBySemesterId relation
 * @method BukuAlatLongitudinalQuery rightJoinSemesterRelatedBySemesterId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SemesterRelatedBySemesterId relation
 * @method BukuAlatLongitudinalQuery innerJoinSemesterRelatedBySemesterId($relationAlias = null) Adds a INNER JOIN clause to the query using the SemesterRelatedBySemesterId relation
 *
 * @method BukuAlatLongitudinalQuery leftJoinSemesterRelatedBySemesterId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SemesterRelatedBySemesterId relation
 * @method BukuAlatLongitudinalQuery rightJoinSemesterRelatedBySemesterId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SemesterRelatedBySemesterId relation
 * @method BukuAlatLongitudinalQuery innerJoinSemesterRelatedBySemesterId($relationAlias = null) Adds a INNER JOIN clause to the query using the SemesterRelatedBySemesterId relation
 *
 * @method BukuAlatLongitudinal findOne(PropelPDO $con = null) Return the first BukuAlatLongitudinal matching the query
 * @method BukuAlatLongitudinal findOneOrCreate(PropelPDO $con = null) Return the first BukuAlatLongitudinal matching the query, or a new BukuAlatLongitudinal object populated from the query conditions when no match is found
 *
 * @method BukuAlatLongitudinal findOneByBukuAlatId(string $buku_alat_id) Return the first BukuAlatLongitudinal filtered by the buku_alat_id column
 * @method BukuAlatLongitudinal findOneBySemesterId(string $semester_id) Return the first BukuAlatLongitudinal filtered by the semester_id column
 * @method BukuAlatLongitudinal findOneByJumlah(int $jumlah) Return the first BukuAlatLongitudinal filtered by the jumlah column
 * @method BukuAlatLongitudinal findOneByStatusKelaikan(string $status_kelaikan) Return the first BukuAlatLongitudinal filtered by the status_kelaikan column
 * @method BukuAlatLongitudinal findOneByLastUpdate(string $Last_update) Return the first BukuAlatLongitudinal filtered by the Last_update column
 * @method BukuAlatLongitudinal findOneBySoftDelete(string $Soft_delete) Return the first BukuAlatLongitudinal filtered by the Soft_delete column
 * @method BukuAlatLongitudinal findOneByLastSync(string $last_sync) Return the first BukuAlatLongitudinal filtered by the last_sync column
 * @method BukuAlatLongitudinal findOneByUpdaterId(string $Updater_ID) Return the first BukuAlatLongitudinal filtered by the Updater_ID column
 *
 * @method array findByBukuAlatId(string $buku_alat_id) Return BukuAlatLongitudinal objects filtered by the buku_alat_id column
 * @method array findBySemesterId(string $semester_id) Return BukuAlatLongitudinal objects filtered by the semester_id column
 * @method array findByJumlah(int $jumlah) Return BukuAlatLongitudinal objects filtered by the jumlah column
 * @method array findByStatusKelaikan(string $status_kelaikan) Return BukuAlatLongitudinal objects filtered by the status_kelaikan column
 * @method array findByLastUpdate(string $Last_update) Return BukuAlatLongitudinal objects filtered by the Last_update column
 * @method array findBySoftDelete(string $Soft_delete) Return BukuAlatLongitudinal objects filtered by the Soft_delete column
 * @method array findByLastSync(string $last_sync) Return BukuAlatLongitudinal objects filtered by the last_sync column
 * @method array findByUpdaterId(string $Updater_ID) Return BukuAlatLongitudinal objects filtered by the Updater_ID column
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseBukuAlatLongitudinalQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseBukuAlatLongitudinalQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Dapodikmen', $modelName = 'angulex\\Model\\BukuAlatLongitudinal', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new BukuAlatLongitudinalQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   BukuAlatLongitudinalQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return BukuAlatLongitudinalQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof BukuAlatLongitudinalQuery) {
            return $criteria;
        }
        $query = new BukuAlatLongitudinalQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34), $con);
     * </code>
     *
     * @param array $key Primary key to use for the query 
                         A Primary key composition: [$buku_alat_id, $semester_id]
     * @param     PropelPDO $con an optional connection object
     *
     * @return   BukuAlatLongitudinal|BukuAlatLongitudinal[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = BukuAlatLongitudinalPeer::getInstanceFromPool(serialize(array((string) $key[0], (string) $key[1]))))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(BukuAlatLongitudinalPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 BukuAlatLongitudinal A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [buku_alat_id], [semester_id], [jumlah], [status_kelaikan], [Last_update], [Soft_delete], [last_sync], [Updater_ID] FROM [buku_alat_longitudinal] WHERE [buku_alat_id] = :p0 AND [semester_id] = :p1';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_STR);			
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new BukuAlatLongitudinal();
            $obj->hydrate($row);
            BukuAlatLongitudinalPeer::addInstanceToPool($obj, serialize(array((string) $key[0], (string) $key[1])));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return BukuAlatLongitudinal|BukuAlatLongitudinal[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|BukuAlatLongitudinal[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return BukuAlatLongitudinalQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(BukuAlatLongitudinalPeer::BUKU_ALAT_ID, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(BukuAlatLongitudinalPeer::SEMESTER_ID, $key[1], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return BukuAlatLongitudinalQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(BukuAlatLongitudinalPeer::BUKU_ALAT_ID, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(BukuAlatLongitudinalPeer::SEMESTER_ID, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the buku_alat_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBukuAlatId('fooValue');   // WHERE buku_alat_id = 'fooValue'
     * $query->filterByBukuAlatId('%fooValue%'); // WHERE buku_alat_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $bukuAlatId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BukuAlatLongitudinalQuery The current query, for fluid interface
     */
    public function filterByBukuAlatId($bukuAlatId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($bukuAlatId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $bukuAlatId)) {
                $bukuAlatId = str_replace('*', '%', $bukuAlatId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BukuAlatLongitudinalPeer::BUKU_ALAT_ID, $bukuAlatId, $comparison);
    }

    /**
     * Filter the query on the semester_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySemesterId('fooValue');   // WHERE semester_id = 'fooValue'
     * $query->filterBySemesterId('%fooValue%'); // WHERE semester_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $semesterId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BukuAlatLongitudinalQuery The current query, for fluid interface
     */
    public function filterBySemesterId($semesterId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($semesterId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $semesterId)) {
                $semesterId = str_replace('*', '%', $semesterId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BukuAlatLongitudinalPeer::SEMESTER_ID, $semesterId, $comparison);
    }

    /**
     * Filter the query on the jumlah column
     *
     * Example usage:
     * <code>
     * $query->filterByJumlah(1234); // WHERE jumlah = 1234
     * $query->filterByJumlah(array(12, 34)); // WHERE jumlah IN (12, 34)
     * $query->filterByJumlah(array('min' => 12)); // WHERE jumlah >= 12
     * $query->filterByJumlah(array('max' => 12)); // WHERE jumlah <= 12
     * </code>
     *
     * @param     mixed $jumlah The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BukuAlatLongitudinalQuery The current query, for fluid interface
     */
    public function filterByJumlah($jumlah = null, $comparison = null)
    {
        if (is_array($jumlah)) {
            $useMinMax = false;
            if (isset($jumlah['min'])) {
                $this->addUsingAlias(BukuAlatLongitudinalPeer::JUMLAH, $jumlah['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jumlah['max'])) {
                $this->addUsingAlias(BukuAlatLongitudinalPeer::JUMLAH, $jumlah['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BukuAlatLongitudinalPeer::JUMLAH, $jumlah, $comparison);
    }

    /**
     * Filter the query on the status_kelaikan column
     *
     * Example usage:
     * <code>
     * $query->filterByStatusKelaikan(1234); // WHERE status_kelaikan = 1234
     * $query->filterByStatusKelaikan(array(12, 34)); // WHERE status_kelaikan IN (12, 34)
     * $query->filterByStatusKelaikan(array('min' => 12)); // WHERE status_kelaikan >= 12
     * $query->filterByStatusKelaikan(array('max' => 12)); // WHERE status_kelaikan <= 12
     * </code>
     *
     * @param     mixed $statusKelaikan The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BukuAlatLongitudinalQuery The current query, for fluid interface
     */
    public function filterByStatusKelaikan($statusKelaikan = null, $comparison = null)
    {
        if (is_array($statusKelaikan)) {
            $useMinMax = false;
            if (isset($statusKelaikan['min'])) {
                $this->addUsingAlias(BukuAlatLongitudinalPeer::STATUS_KELAIKAN, $statusKelaikan['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($statusKelaikan['max'])) {
                $this->addUsingAlias(BukuAlatLongitudinalPeer::STATUS_KELAIKAN, $statusKelaikan['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BukuAlatLongitudinalPeer::STATUS_KELAIKAN, $statusKelaikan, $comparison);
    }

    /**
     * Filter the query on the Last_update column
     *
     * Example usage:
     * <code>
     * $query->filterByLastUpdate('2011-03-14'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate('now'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate(array('max' => 'yesterday')); // WHERE Last_update > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastUpdate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BukuAlatLongitudinalQuery The current query, for fluid interface
     */
    public function filterByLastUpdate($lastUpdate = null, $comparison = null)
    {
        if (is_array($lastUpdate)) {
            $useMinMax = false;
            if (isset($lastUpdate['min'])) {
                $this->addUsingAlias(BukuAlatLongitudinalPeer::LAST_UPDATE, $lastUpdate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastUpdate['max'])) {
                $this->addUsingAlias(BukuAlatLongitudinalPeer::LAST_UPDATE, $lastUpdate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BukuAlatLongitudinalPeer::LAST_UPDATE, $lastUpdate, $comparison);
    }

    /**
     * Filter the query on the Soft_delete column
     *
     * Example usage:
     * <code>
     * $query->filterBySoftDelete(1234); // WHERE Soft_delete = 1234
     * $query->filterBySoftDelete(array(12, 34)); // WHERE Soft_delete IN (12, 34)
     * $query->filterBySoftDelete(array('min' => 12)); // WHERE Soft_delete >= 12
     * $query->filterBySoftDelete(array('max' => 12)); // WHERE Soft_delete <= 12
     * </code>
     *
     * @param     mixed $softDelete The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BukuAlatLongitudinalQuery The current query, for fluid interface
     */
    public function filterBySoftDelete($softDelete = null, $comparison = null)
    {
        if (is_array($softDelete)) {
            $useMinMax = false;
            if (isset($softDelete['min'])) {
                $this->addUsingAlias(BukuAlatLongitudinalPeer::SOFT_DELETE, $softDelete['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($softDelete['max'])) {
                $this->addUsingAlias(BukuAlatLongitudinalPeer::SOFT_DELETE, $softDelete['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BukuAlatLongitudinalPeer::SOFT_DELETE, $softDelete, $comparison);
    }

    /**
     * Filter the query on the last_sync column
     *
     * Example usage:
     * <code>
     * $query->filterByLastSync('2011-03-14'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync('now'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync(array('max' => 'yesterday')); // WHERE last_sync > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastSync The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BukuAlatLongitudinalQuery The current query, for fluid interface
     */
    public function filterByLastSync($lastSync = null, $comparison = null)
    {
        if (is_array($lastSync)) {
            $useMinMax = false;
            if (isset($lastSync['min'])) {
                $this->addUsingAlias(BukuAlatLongitudinalPeer::LAST_SYNC, $lastSync['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastSync['max'])) {
                $this->addUsingAlias(BukuAlatLongitudinalPeer::LAST_SYNC, $lastSync['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BukuAlatLongitudinalPeer::LAST_SYNC, $lastSync, $comparison);
    }

    /**
     * Filter the query on the Updater_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdaterId('fooValue');   // WHERE Updater_ID = 'fooValue'
     * $query->filterByUpdaterId('%fooValue%'); // WHERE Updater_ID LIKE '%fooValue%'
     * </code>
     *
     * @param     string $updaterId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BukuAlatLongitudinalQuery The current query, for fluid interface
     */
    public function filterByUpdaterId($updaterId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($updaterId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $updaterId)) {
                $updaterId = str_replace('*', '%', $updaterId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BukuAlatLongitudinalPeer::UPDATER_ID, $updaterId, $comparison);
    }

    /**
     * Filter the query by a related BukuAlat object
     *
     * @param   BukuAlat|PropelObjectCollection $bukuAlat The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 BukuAlatLongitudinalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByBukuAlatRelatedByBukuAlatId($bukuAlat, $comparison = null)
    {
        if ($bukuAlat instanceof BukuAlat) {
            return $this
                ->addUsingAlias(BukuAlatLongitudinalPeer::BUKU_ALAT_ID, $bukuAlat->getBukuAlatId(), $comparison);
        } elseif ($bukuAlat instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BukuAlatLongitudinalPeer::BUKU_ALAT_ID, $bukuAlat->toKeyValue('PrimaryKey', 'BukuAlatId'), $comparison);
        } else {
            throw new PropelException('filterByBukuAlatRelatedByBukuAlatId() only accepts arguments of type BukuAlat or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BukuAlatRelatedByBukuAlatId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return BukuAlatLongitudinalQuery The current query, for fluid interface
     */
    public function joinBukuAlatRelatedByBukuAlatId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BukuAlatRelatedByBukuAlatId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BukuAlatRelatedByBukuAlatId');
        }

        return $this;
    }

    /**
     * Use the BukuAlatRelatedByBukuAlatId relation BukuAlat object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\BukuAlatQuery A secondary query class using the current class as primary query
     */
    public function useBukuAlatRelatedByBukuAlatIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBukuAlatRelatedByBukuAlatId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BukuAlatRelatedByBukuAlatId', '\angulex\Model\BukuAlatQuery');
    }

    /**
     * Filter the query by a related BukuAlat object
     *
     * @param   BukuAlat|PropelObjectCollection $bukuAlat The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 BukuAlatLongitudinalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByBukuAlatRelatedByBukuAlatId($bukuAlat, $comparison = null)
    {
        if ($bukuAlat instanceof BukuAlat) {
            return $this
                ->addUsingAlias(BukuAlatLongitudinalPeer::BUKU_ALAT_ID, $bukuAlat->getBukuAlatId(), $comparison);
        } elseif ($bukuAlat instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BukuAlatLongitudinalPeer::BUKU_ALAT_ID, $bukuAlat->toKeyValue('PrimaryKey', 'BukuAlatId'), $comparison);
        } else {
            throw new PropelException('filterByBukuAlatRelatedByBukuAlatId() only accepts arguments of type BukuAlat or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BukuAlatRelatedByBukuAlatId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return BukuAlatLongitudinalQuery The current query, for fluid interface
     */
    public function joinBukuAlatRelatedByBukuAlatId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BukuAlatRelatedByBukuAlatId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BukuAlatRelatedByBukuAlatId');
        }

        return $this;
    }

    /**
     * Use the BukuAlatRelatedByBukuAlatId relation BukuAlat object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\BukuAlatQuery A secondary query class using the current class as primary query
     */
    public function useBukuAlatRelatedByBukuAlatIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBukuAlatRelatedByBukuAlatId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BukuAlatRelatedByBukuAlatId', '\angulex\Model\BukuAlatQuery');
    }

    /**
     * Filter the query by a related Semester object
     *
     * @param   Semester|PropelObjectCollection $semester The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 BukuAlatLongitudinalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySemesterRelatedBySemesterId($semester, $comparison = null)
    {
        if ($semester instanceof Semester) {
            return $this
                ->addUsingAlias(BukuAlatLongitudinalPeer::SEMESTER_ID, $semester->getSemesterId(), $comparison);
        } elseif ($semester instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BukuAlatLongitudinalPeer::SEMESTER_ID, $semester->toKeyValue('PrimaryKey', 'SemesterId'), $comparison);
        } else {
            throw new PropelException('filterBySemesterRelatedBySemesterId() only accepts arguments of type Semester or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SemesterRelatedBySemesterId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return BukuAlatLongitudinalQuery The current query, for fluid interface
     */
    public function joinSemesterRelatedBySemesterId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SemesterRelatedBySemesterId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SemesterRelatedBySemesterId');
        }

        return $this;
    }

    /**
     * Use the SemesterRelatedBySemesterId relation Semester object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SemesterQuery A secondary query class using the current class as primary query
     */
    public function useSemesterRelatedBySemesterIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSemesterRelatedBySemesterId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SemesterRelatedBySemesterId', '\angulex\Model\SemesterQuery');
    }

    /**
     * Filter the query by a related Semester object
     *
     * @param   Semester|PropelObjectCollection $semester The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 BukuAlatLongitudinalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySemesterRelatedBySemesterId($semester, $comparison = null)
    {
        if ($semester instanceof Semester) {
            return $this
                ->addUsingAlias(BukuAlatLongitudinalPeer::SEMESTER_ID, $semester->getSemesterId(), $comparison);
        } elseif ($semester instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BukuAlatLongitudinalPeer::SEMESTER_ID, $semester->toKeyValue('PrimaryKey', 'SemesterId'), $comparison);
        } else {
            throw new PropelException('filterBySemesterRelatedBySemesterId() only accepts arguments of type Semester or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SemesterRelatedBySemesterId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return BukuAlatLongitudinalQuery The current query, for fluid interface
     */
    public function joinSemesterRelatedBySemesterId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SemesterRelatedBySemesterId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SemesterRelatedBySemesterId');
        }

        return $this;
    }

    /**
     * Use the SemesterRelatedBySemesterId relation Semester object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SemesterQuery A secondary query class using the current class as primary query
     */
    public function useSemesterRelatedBySemesterIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSemesterRelatedBySemesterId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SemesterRelatedBySemesterId', '\angulex\Model\SemesterQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   BukuAlatLongitudinal $bukuAlatLongitudinal Object to remove from the list of results
     *
     * @return BukuAlatLongitudinalQuery The current query, for fluid interface
     */
    public function prune($bukuAlatLongitudinal = null)
    {
        if ($bukuAlatLongitudinal) {
            $this->addCond('pruneCond0', $this->getAliasedColName(BukuAlatLongitudinalPeer::BUKU_ALAT_ID), $bukuAlatLongitudinal->getBukuAlatId(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(BukuAlatLongitudinalPeer::SEMESTER_ID), $bukuAlatLongitudinal->getSemesterId(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

}
