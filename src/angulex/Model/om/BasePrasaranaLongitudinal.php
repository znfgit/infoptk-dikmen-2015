<?php

namespace angulex\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelDateTime;
use \PropelException;
use \PropelPDO;
use angulex\Model\Prasarana;
use angulex\Model\PrasaranaLongitudinal;
use angulex\Model\PrasaranaLongitudinalPeer;
use angulex\Model\PrasaranaLongitudinalQuery;
use angulex\Model\PrasaranaQuery;
use angulex\Model\Semester;
use angulex\Model\SemesterQuery;

/**
 * Base class that represents a row from the 'prasarana_longitudinal' table.
 *
 * 
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BasePrasaranaLongitudinal extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'angulex\\Model\\PrasaranaLongitudinalPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        PrasaranaLongitudinalPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the prasarana_id field.
     * @var        string
     */
    protected $prasarana_id;

    /**
     * The value for the semester_id field.
     * @var        string
     */
    protected $semester_id;

    /**
     * The value for the rusak_penutup_atap field.
     * Note: this column has a database default value of: '((0))'
     * @var        string
     */
    protected $rusak_penutup_atap;

    /**
     * The value for the rusak_rangka_atap field.
     * Note: this column has a database default value of: '((0))'
     * @var        string
     */
    protected $rusak_rangka_atap;

    /**
     * The value for the rusak_lisplang_talang field.
     * Note: this column has a database default value of: '((0))'
     * @var        string
     */
    protected $rusak_lisplang_talang;

    /**
     * The value for the rusak_rangka_plafon field.
     * Note: this column has a database default value of: '((0))'
     * @var        string
     */
    protected $rusak_rangka_plafon;

    /**
     * The value for the rusak_penutup_listplafon field.
     * Note: this column has a database default value of: '((0))'
     * @var        string
     */
    protected $rusak_penutup_listplafon;

    /**
     * The value for the rusak_cat_plafon field.
     * Note: this column has a database default value of: '((0))'
     * @var        string
     */
    protected $rusak_cat_plafon;

    /**
     * The value for the rusak_kolom_ringbalok field.
     * Note: this column has a database default value of: '((0))'
     * @var        string
     */
    protected $rusak_kolom_ringbalok;

    /**
     * The value for the rusak_bata_dindingpengisi field.
     * Note: this column has a database default value of: '((0))'
     * @var        string
     */
    protected $rusak_bata_dindingpengisi;

    /**
     * The value for the rusak_cat_dinding field.
     * Note: this column has a database default value of: '((0))'
     * @var        string
     */
    protected $rusak_cat_dinding;

    /**
     * The value for the rusak_kusen field.
     * Note: this column has a database default value of: '((0))'
     * @var        string
     */
    protected $rusak_kusen;

    /**
     * The value for the rusak_daun_pintu field.
     * Note: this column has a database default value of: '((0))'
     * @var        string
     */
    protected $rusak_daun_pintu;

    /**
     * The value for the rusak_daun_jendela field.
     * Note: this column has a database default value of: '((0))'
     * @var        string
     */
    protected $rusak_daun_jendela;

    /**
     * The value for the rusak_struktur_bawah field.
     * Note: this column has a database default value of: '((0))'
     * @var        string
     */
    protected $rusak_struktur_bawah;

    /**
     * The value for the rusak_penutup_lantai field.
     * Note: this column has a database default value of: '((0))'
     * @var        string
     */
    protected $rusak_penutup_lantai;

    /**
     * The value for the rusak_pondasi field.
     * Note: this column has a database default value of: '((0))'
     * @var        string
     */
    protected $rusak_pondasi;

    /**
     * The value for the rusak_sloof field.
     * Note: this column has a database default value of: '((0))'
     * @var        string
     */
    protected $rusak_sloof;

    /**
     * The value for the rusak_listrik field.
     * Note: this column has a database default value of: '((0))'
     * @var        string
     */
    protected $rusak_listrik;

    /**
     * The value for the rusak_airhujan_rabatan field.
     * Note: this column has a database default value of: '((0))'
     * @var        string
     */
    protected $rusak_airhujan_rabatan;

    /**
     * The value for the blob_id field.
     * @var        string
     */
    protected $blob_id;

    /**
     * The value for the last_update field.
     * @var        string
     */
    protected $last_update;

    /**
     * The value for the soft_delete field.
     * @var        string
     */
    protected $soft_delete;

    /**
     * The value for the last_sync field.
     * @var        string
     */
    protected $last_sync;

    /**
     * The value for the updater_id field.
     * @var        string
     */
    protected $updater_id;

    /**
     * @var        Prasarana
     */
    protected $aPrasaranaRelatedByPrasaranaId;

    /**
     * @var        Prasarana
     */
    protected $aPrasaranaRelatedByPrasaranaId;

    /**
     * @var        Semester
     */
    protected $aSemesterRelatedBySemesterId;

    /**
     * @var        Semester
     */
    protected $aSemesterRelatedBySemesterId;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->rusak_penutup_atap = '((0))';
        $this->rusak_rangka_atap = '((0))';
        $this->rusak_lisplang_talang = '((0))';
        $this->rusak_rangka_plafon = '((0))';
        $this->rusak_penutup_listplafon = '((0))';
        $this->rusak_cat_plafon = '((0))';
        $this->rusak_kolom_ringbalok = '((0))';
        $this->rusak_bata_dindingpengisi = '((0))';
        $this->rusak_cat_dinding = '((0))';
        $this->rusak_kusen = '((0))';
        $this->rusak_daun_pintu = '((0))';
        $this->rusak_daun_jendela = '((0))';
        $this->rusak_struktur_bawah = '((0))';
        $this->rusak_penutup_lantai = '((0))';
        $this->rusak_pondasi = '((0))';
        $this->rusak_sloof = '((0))';
        $this->rusak_listrik = '((0))';
        $this->rusak_airhujan_rabatan = '((0))';
    }

    /**
     * Initializes internal state of BasePrasaranaLongitudinal object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [prasarana_id] column value.
     * 
     * @return string
     */
    public function getPrasaranaId()
    {
        return $this->prasarana_id;
    }

    /**
     * Get the [semester_id] column value.
     * 
     * @return string
     */
    public function getSemesterId()
    {
        return $this->semester_id;
    }

    /**
     * Get the [rusak_penutup_atap] column value.
     * 
     * @return string
     */
    public function getRusakPenutupAtap()
    {
        return $this->rusak_penutup_atap;
    }

    /**
     * Get the [rusak_rangka_atap] column value.
     * 
     * @return string
     */
    public function getRusakRangkaAtap()
    {
        return $this->rusak_rangka_atap;
    }

    /**
     * Get the [rusak_lisplang_talang] column value.
     * 
     * @return string
     */
    public function getRusakLisplangTalang()
    {
        return $this->rusak_lisplang_talang;
    }

    /**
     * Get the [rusak_rangka_plafon] column value.
     * 
     * @return string
     */
    public function getRusakRangkaPlafon()
    {
        return $this->rusak_rangka_plafon;
    }

    /**
     * Get the [rusak_penutup_listplafon] column value.
     * 
     * @return string
     */
    public function getRusakPenutupListplafon()
    {
        return $this->rusak_penutup_listplafon;
    }

    /**
     * Get the [rusak_cat_plafon] column value.
     * 
     * @return string
     */
    public function getRusakCatPlafon()
    {
        return $this->rusak_cat_plafon;
    }

    /**
     * Get the [rusak_kolom_ringbalok] column value.
     * 
     * @return string
     */
    public function getRusakKolomRingbalok()
    {
        return $this->rusak_kolom_ringbalok;
    }

    /**
     * Get the [rusak_bata_dindingpengisi] column value.
     * 
     * @return string
     */
    public function getRusakBataDindingpengisi()
    {
        return $this->rusak_bata_dindingpengisi;
    }

    /**
     * Get the [rusak_cat_dinding] column value.
     * 
     * @return string
     */
    public function getRusakCatDinding()
    {
        return $this->rusak_cat_dinding;
    }

    /**
     * Get the [rusak_kusen] column value.
     * 
     * @return string
     */
    public function getRusakKusen()
    {
        return $this->rusak_kusen;
    }

    /**
     * Get the [rusak_daun_pintu] column value.
     * 
     * @return string
     */
    public function getRusakDaunPintu()
    {
        return $this->rusak_daun_pintu;
    }

    /**
     * Get the [rusak_daun_jendela] column value.
     * 
     * @return string
     */
    public function getRusakDaunJendela()
    {
        return $this->rusak_daun_jendela;
    }

    /**
     * Get the [rusak_struktur_bawah] column value.
     * 
     * @return string
     */
    public function getRusakStrukturBawah()
    {
        return $this->rusak_struktur_bawah;
    }

    /**
     * Get the [rusak_penutup_lantai] column value.
     * 
     * @return string
     */
    public function getRusakPenutupLantai()
    {
        return $this->rusak_penutup_lantai;
    }

    /**
     * Get the [rusak_pondasi] column value.
     * 
     * @return string
     */
    public function getRusakPondasi()
    {
        return $this->rusak_pondasi;
    }

    /**
     * Get the [rusak_sloof] column value.
     * 
     * @return string
     */
    public function getRusakSloof()
    {
        return $this->rusak_sloof;
    }

    /**
     * Get the [rusak_listrik] column value.
     * 
     * @return string
     */
    public function getRusakListrik()
    {
        return $this->rusak_listrik;
    }

    /**
     * Get the [rusak_airhujan_rabatan] column value.
     * 
     * @return string
     */
    public function getRusakAirhujanRabatan()
    {
        return $this->rusak_airhujan_rabatan;
    }

    /**
     * Get the [blob_id] column value.
     * 
     * @return string
     */
    public function getBlobId()
    {
        return $this->blob_id;
    }

    /**
     * Get the [optionally formatted] temporal [last_update] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastUpdate($format = 'Y-m-d H:i:s')
    {
        if ($this->last_update === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_update);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_update, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [soft_delete] column value.
     * 
     * @return string
     */
    public function getSoftDelete()
    {
        return $this->soft_delete;
    }

    /**
     * Get the [optionally formatted] temporal [last_sync] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastSync($format = 'Y-m-d H:i:s')
    {
        if ($this->last_sync === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_sync);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_sync, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [updater_id] column value.
     * 
     * @return string
     */
    public function getUpdaterId()
    {
        return $this->updater_id;
    }

    /**
     * Set the value of [prasarana_id] column.
     * 
     * @param string $v new value
     * @return PrasaranaLongitudinal The current object (for fluent API support)
     */
    public function setPrasaranaId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->prasarana_id !== $v) {
            $this->prasarana_id = $v;
            $this->modifiedColumns[] = PrasaranaLongitudinalPeer::PRASARANA_ID;
        }

        if ($this->aPrasaranaRelatedByPrasaranaId !== null && $this->aPrasaranaRelatedByPrasaranaId->getPrasaranaId() !== $v) {
            $this->aPrasaranaRelatedByPrasaranaId = null;
        }

        if ($this->aPrasaranaRelatedByPrasaranaId !== null && $this->aPrasaranaRelatedByPrasaranaId->getPrasaranaId() !== $v) {
            $this->aPrasaranaRelatedByPrasaranaId = null;
        }


        return $this;
    } // setPrasaranaId()

    /**
     * Set the value of [semester_id] column.
     * 
     * @param string $v new value
     * @return PrasaranaLongitudinal The current object (for fluent API support)
     */
    public function setSemesterId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->semester_id !== $v) {
            $this->semester_id = $v;
            $this->modifiedColumns[] = PrasaranaLongitudinalPeer::SEMESTER_ID;
        }

        if ($this->aSemesterRelatedBySemesterId !== null && $this->aSemesterRelatedBySemesterId->getSemesterId() !== $v) {
            $this->aSemesterRelatedBySemesterId = null;
        }

        if ($this->aSemesterRelatedBySemesterId !== null && $this->aSemesterRelatedBySemesterId->getSemesterId() !== $v) {
            $this->aSemesterRelatedBySemesterId = null;
        }


        return $this;
    } // setSemesterId()

    /**
     * Set the value of [rusak_penutup_atap] column.
     * 
     * @param string $v new value
     * @return PrasaranaLongitudinal The current object (for fluent API support)
     */
    public function setRusakPenutupAtap($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->rusak_penutup_atap !== $v) {
            $this->rusak_penutup_atap = $v;
            $this->modifiedColumns[] = PrasaranaLongitudinalPeer::RUSAK_PENUTUP_ATAP;
        }


        return $this;
    } // setRusakPenutupAtap()

    /**
     * Set the value of [rusak_rangka_atap] column.
     * 
     * @param string $v new value
     * @return PrasaranaLongitudinal The current object (for fluent API support)
     */
    public function setRusakRangkaAtap($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->rusak_rangka_atap !== $v) {
            $this->rusak_rangka_atap = $v;
            $this->modifiedColumns[] = PrasaranaLongitudinalPeer::RUSAK_RANGKA_ATAP;
        }


        return $this;
    } // setRusakRangkaAtap()

    /**
     * Set the value of [rusak_lisplang_talang] column.
     * 
     * @param string $v new value
     * @return PrasaranaLongitudinal The current object (for fluent API support)
     */
    public function setRusakLisplangTalang($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->rusak_lisplang_talang !== $v) {
            $this->rusak_lisplang_talang = $v;
            $this->modifiedColumns[] = PrasaranaLongitudinalPeer::RUSAK_LISPLANG_TALANG;
        }


        return $this;
    } // setRusakLisplangTalang()

    /**
     * Set the value of [rusak_rangka_plafon] column.
     * 
     * @param string $v new value
     * @return PrasaranaLongitudinal The current object (for fluent API support)
     */
    public function setRusakRangkaPlafon($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->rusak_rangka_plafon !== $v) {
            $this->rusak_rangka_plafon = $v;
            $this->modifiedColumns[] = PrasaranaLongitudinalPeer::RUSAK_RANGKA_PLAFON;
        }


        return $this;
    } // setRusakRangkaPlafon()

    /**
     * Set the value of [rusak_penutup_listplafon] column.
     * 
     * @param string $v new value
     * @return PrasaranaLongitudinal The current object (for fluent API support)
     */
    public function setRusakPenutupListplafon($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->rusak_penutup_listplafon !== $v) {
            $this->rusak_penutup_listplafon = $v;
            $this->modifiedColumns[] = PrasaranaLongitudinalPeer::RUSAK_PENUTUP_LISTPLAFON;
        }


        return $this;
    } // setRusakPenutupListplafon()

    /**
     * Set the value of [rusak_cat_plafon] column.
     * 
     * @param string $v new value
     * @return PrasaranaLongitudinal The current object (for fluent API support)
     */
    public function setRusakCatPlafon($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->rusak_cat_plafon !== $v) {
            $this->rusak_cat_plafon = $v;
            $this->modifiedColumns[] = PrasaranaLongitudinalPeer::RUSAK_CAT_PLAFON;
        }


        return $this;
    } // setRusakCatPlafon()

    /**
     * Set the value of [rusak_kolom_ringbalok] column.
     * 
     * @param string $v new value
     * @return PrasaranaLongitudinal The current object (for fluent API support)
     */
    public function setRusakKolomRingbalok($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->rusak_kolom_ringbalok !== $v) {
            $this->rusak_kolom_ringbalok = $v;
            $this->modifiedColumns[] = PrasaranaLongitudinalPeer::RUSAK_KOLOM_RINGBALOK;
        }


        return $this;
    } // setRusakKolomRingbalok()

    /**
     * Set the value of [rusak_bata_dindingpengisi] column.
     * 
     * @param string $v new value
     * @return PrasaranaLongitudinal The current object (for fluent API support)
     */
    public function setRusakBataDindingpengisi($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->rusak_bata_dindingpengisi !== $v) {
            $this->rusak_bata_dindingpengisi = $v;
            $this->modifiedColumns[] = PrasaranaLongitudinalPeer::RUSAK_BATA_DINDINGPENGISI;
        }


        return $this;
    } // setRusakBataDindingpengisi()

    /**
     * Set the value of [rusak_cat_dinding] column.
     * 
     * @param string $v new value
     * @return PrasaranaLongitudinal The current object (for fluent API support)
     */
    public function setRusakCatDinding($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->rusak_cat_dinding !== $v) {
            $this->rusak_cat_dinding = $v;
            $this->modifiedColumns[] = PrasaranaLongitudinalPeer::RUSAK_CAT_DINDING;
        }


        return $this;
    } // setRusakCatDinding()

    /**
     * Set the value of [rusak_kusen] column.
     * 
     * @param string $v new value
     * @return PrasaranaLongitudinal The current object (for fluent API support)
     */
    public function setRusakKusen($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->rusak_kusen !== $v) {
            $this->rusak_kusen = $v;
            $this->modifiedColumns[] = PrasaranaLongitudinalPeer::RUSAK_KUSEN;
        }


        return $this;
    } // setRusakKusen()

    /**
     * Set the value of [rusak_daun_pintu] column.
     * 
     * @param string $v new value
     * @return PrasaranaLongitudinal The current object (for fluent API support)
     */
    public function setRusakDaunPintu($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->rusak_daun_pintu !== $v) {
            $this->rusak_daun_pintu = $v;
            $this->modifiedColumns[] = PrasaranaLongitudinalPeer::RUSAK_DAUN_PINTU;
        }


        return $this;
    } // setRusakDaunPintu()

    /**
     * Set the value of [rusak_daun_jendela] column.
     * 
     * @param string $v new value
     * @return PrasaranaLongitudinal The current object (for fluent API support)
     */
    public function setRusakDaunJendela($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->rusak_daun_jendela !== $v) {
            $this->rusak_daun_jendela = $v;
            $this->modifiedColumns[] = PrasaranaLongitudinalPeer::RUSAK_DAUN_JENDELA;
        }


        return $this;
    } // setRusakDaunJendela()

    /**
     * Set the value of [rusak_struktur_bawah] column.
     * 
     * @param string $v new value
     * @return PrasaranaLongitudinal The current object (for fluent API support)
     */
    public function setRusakStrukturBawah($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->rusak_struktur_bawah !== $v) {
            $this->rusak_struktur_bawah = $v;
            $this->modifiedColumns[] = PrasaranaLongitudinalPeer::RUSAK_STRUKTUR_BAWAH;
        }


        return $this;
    } // setRusakStrukturBawah()

    /**
     * Set the value of [rusak_penutup_lantai] column.
     * 
     * @param string $v new value
     * @return PrasaranaLongitudinal The current object (for fluent API support)
     */
    public function setRusakPenutupLantai($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->rusak_penutup_lantai !== $v) {
            $this->rusak_penutup_lantai = $v;
            $this->modifiedColumns[] = PrasaranaLongitudinalPeer::RUSAK_PENUTUP_LANTAI;
        }


        return $this;
    } // setRusakPenutupLantai()

    /**
     * Set the value of [rusak_pondasi] column.
     * 
     * @param string $v new value
     * @return PrasaranaLongitudinal The current object (for fluent API support)
     */
    public function setRusakPondasi($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->rusak_pondasi !== $v) {
            $this->rusak_pondasi = $v;
            $this->modifiedColumns[] = PrasaranaLongitudinalPeer::RUSAK_PONDASI;
        }


        return $this;
    } // setRusakPondasi()

    /**
     * Set the value of [rusak_sloof] column.
     * 
     * @param string $v new value
     * @return PrasaranaLongitudinal The current object (for fluent API support)
     */
    public function setRusakSloof($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->rusak_sloof !== $v) {
            $this->rusak_sloof = $v;
            $this->modifiedColumns[] = PrasaranaLongitudinalPeer::RUSAK_SLOOF;
        }


        return $this;
    } // setRusakSloof()

    /**
     * Set the value of [rusak_listrik] column.
     * 
     * @param string $v new value
     * @return PrasaranaLongitudinal The current object (for fluent API support)
     */
    public function setRusakListrik($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->rusak_listrik !== $v) {
            $this->rusak_listrik = $v;
            $this->modifiedColumns[] = PrasaranaLongitudinalPeer::RUSAK_LISTRIK;
        }


        return $this;
    } // setRusakListrik()

    /**
     * Set the value of [rusak_airhujan_rabatan] column.
     * 
     * @param string $v new value
     * @return PrasaranaLongitudinal The current object (for fluent API support)
     */
    public function setRusakAirhujanRabatan($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->rusak_airhujan_rabatan !== $v) {
            $this->rusak_airhujan_rabatan = $v;
            $this->modifiedColumns[] = PrasaranaLongitudinalPeer::RUSAK_AIRHUJAN_RABATAN;
        }


        return $this;
    } // setRusakAirhujanRabatan()

    /**
     * Set the value of [blob_id] column.
     * 
     * @param string $v new value
     * @return PrasaranaLongitudinal The current object (for fluent API support)
     */
    public function setBlobId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->blob_id !== $v) {
            $this->blob_id = $v;
            $this->modifiedColumns[] = PrasaranaLongitudinalPeer::BLOB_ID;
        }


        return $this;
    } // setBlobId()

    /**
     * Sets the value of [last_update] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return PrasaranaLongitudinal The current object (for fluent API support)
     */
    public function setLastUpdate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_update !== null || $dt !== null) {
            $currentDateAsString = ($this->last_update !== null && $tmpDt = new DateTime($this->last_update)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_update = $newDateAsString;
                $this->modifiedColumns[] = PrasaranaLongitudinalPeer::LAST_UPDATE;
            }
        } // if either are not null


        return $this;
    } // setLastUpdate()

    /**
     * Set the value of [soft_delete] column.
     * 
     * @param string $v new value
     * @return PrasaranaLongitudinal The current object (for fluent API support)
     */
    public function setSoftDelete($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->soft_delete !== $v) {
            $this->soft_delete = $v;
            $this->modifiedColumns[] = PrasaranaLongitudinalPeer::SOFT_DELETE;
        }


        return $this;
    } // setSoftDelete()

    /**
     * Sets the value of [last_sync] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return PrasaranaLongitudinal The current object (for fluent API support)
     */
    public function setLastSync($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_sync !== null || $dt !== null) {
            $currentDateAsString = ($this->last_sync !== null && $tmpDt = new DateTime($this->last_sync)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_sync = $newDateAsString;
                $this->modifiedColumns[] = PrasaranaLongitudinalPeer::LAST_SYNC;
            }
        } // if either are not null


        return $this;
    } // setLastSync()

    /**
     * Set the value of [updater_id] column.
     * 
     * @param string $v new value
     * @return PrasaranaLongitudinal The current object (for fluent API support)
     */
    public function setUpdaterId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->updater_id !== $v) {
            $this->updater_id = $v;
            $this->modifiedColumns[] = PrasaranaLongitudinalPeer::UPDATER_ID;
        }


        return $this;
    } // setUpdaterId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->rusak_penutup_atap !== '((0))') {
                return false;
            }

            if ($this->rusak_rangka_atap !== '((0))') {
                return false;
            }

            if ($this->rusak_lisplang_talang !== '((0))') {
                return false;
            }

            if ($this->rusak_rangka_plafon !== '((0))') {
                return false;
            }

            if ($this->rusak_penutup_listplafon !== '((0))') {
                return false;
            }

            if ($this->rusak_cat_plafon !== '((0))') {
                return false;
            }

            if ($this->rusak_kolom_ringbalok !== '((0))') {
                return false;
            }

            if ($this->rusak_bata_dindingpengisi !== '((0))') {
                return false;
            }

            if ($this->rusak_cat_dinding !== '((0))') {
                return false;
            }

            if ($this->rusak_kusen !== '((0))') {
                return false;
            }

            if ($this->rusak_daun_pintu !== '((0))') {
                return false;
            }

            if ($this->rusak_daun_jendela !== '((0))') {
                return false;
            }

            if ($this->rusak_struktur_bawah !== '((0))') {
                return false;
            }

            if ($this->rusak_penutup_lantai !== '((0))') {
                return false;
            }

            if ($this->rusak_pondasi !== '((0))') {
                return false;
            }

            if ($this->rusak_sloof !== '((0))') {
                return false;
            }

            if ($this->rusak_listrik !== '((0))') {
                return false;
            }

            if ($this->rusak_airhujan_rabatan !== '((0))') {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->prasarana_id = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
            $this->semester_id = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->rusak_penutup_atap = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->rusak_rangka_atap = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->rusak_lisplang_talang = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->rusak_rangka_plafon = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->rusak_penutup_listplafon = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->rusak_cat_plafon = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->rusak_kolom_ringbalok = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->rusak_bata_dindingpengisi = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->rusak_cat_dinding = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->rusak_kusen = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->rusak_daun_pintu = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->rusak_daun_jendela = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->rusak_struktur_bawah = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
            $this->rusak_penutup_lantai = ($row[$startcol + 15] !== null) ? (string) $row[$startcol + 15] : null;
            $this->rusak_pondasi = ($row[$startcol + 16] !== null) ? (string) $row[$startcol + 16] : null;
            $this->rusak_sloof = ($row[$startcol + 17] !== null) ? (string) $row[$startcol + 17] : null;
            $this->rusak_listrik = ($row[$startcol + 18] !== null) ? (string) $row[$startcol + 18] : null;
            $this->rusak_airhujan_rabatan = ($row[$startcol + 19] !== null) ? (string) $row[$startcol + 19] : null;
            $this->blob_id = ($row[$startcol + 20] !== null) ? (string) $row[$startcol + 20] : null;
            $this->last_update = ($row[$startcol + 21] !== null) ? (string) $row[$startcol + 21] : null;
            $this->soft_delete = ($row[$startcol + 22] !== null) ? (string) $row[$startcol + 22] : null;
            $this->last_sync = ($row[$startcol + 23] !== null) ? (string) $row[$startcol + 23] : null;
            $this->updater_id = ($row[$startcol + 24] !== null) ? (string) $row[$startcol + 24] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 25; // 25 = PrasaranaLongitudinalPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating PrasaranaLongitudinal object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aPrasaranaRelatedByPrasaranaId !== null && $this->prasarana_id !== $this->aPrasaranaRelatedByPrasaranaId->getPrasaranaId()) {
            $this->aPrasaranaRelatedByPrasaranaId = null;
        }
        if ($this->aPrasaranaRelatedByPrasaranaId !== null && $this->prasarana_id !== $this->aPrasaranaRelatedByPrasaranaId->getPrasaranaId()) {
            $this->aPrasaranaRelatedByPrasaranaId = null;
        }
        if ($this->aSemesterRelatedBySemesterId !== null && $this->semester_id !== $this->aSemesterRelatedBySemesterId->getSemesterId()) {
            $this->aSemesterRelatedBySemesterId = null;
        }
        if ($this->aSemesterRelatedBySemesterId !== null && $this->semester_id !== $this->aSemesterRelatedBySemesterId->getSemesterId()) {
            $this->aSemesterRelatedBySemesterId = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PrasaranaLongitudinalPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = PrasaranaLongitudinalPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aPrasaranaRelatedByPrasaranaId = null;
            $this->aPrasaranaRelatedByPrasaranaId = null;
            $this->aSemesterRelatedBySemesterId = null;
            $this->aSemesterRelatedBySemesterId = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PrasaranaLongitudinalPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = PrasaranaLongitudinalQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PrasaranaLongitudinalPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                PrasaranaLongitudinalPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPrasaranaRelatedByPrasaranaId !== null) {
                if ($this->aPrasaranaRelatedByPrasaranaId->isModified() || $this->aPrasaranaRelatedByPrasaranaId->isNew()) {
                    $affectedRows += $this->aPrasaranaRelatedByPrasaranaId->save($con);
                }
                $this->setPrasaranaRelatedByPrasaranaId($this->aPrasaranaRelatedByPrasaranaId);
            }

            if ($this->aPrasaranaRelatedByPrasaranaId !== null) {
                if ($this->aPrasaranaRelatedByPrasaranaId->isModified() || $this->aPrasaranaRelatedByPrasaranaId->isNew()) {
                    $affectedRows += $this->aPrasaranaRelatedByPrasaranaId->save($con);
                }
                $this->setPrasaranaRelatedByPrasaranaId($this->aPrasaranaRelatedByPrasaranaId);
            }

            if ($this->aSemesterRelatedBySemesterId !== null) {
                if ($this->aSemesterRelatedBySemesterId->isModified() || $this->aSemesterRelatedBySemesterId->isNew()) {
                    $affectedRows += $this->aSemesterRelatedBySemesterId->save($con);
                }
                $this->setSemesterRelatedBySemesterId($this->aSemesterRelatedBySemesterId);
            }

            if ($this->aSemesterRelatedBySemesterId !== null) {
                if ($this->aSemesterRelatedBySemesterId->isModified() || $this->aSemesterRelatedBySemesterId->isNew()) {
                    $affectedRows += $this->aSemesterRelatedBySemesterId->save($con);
                }
                $this->setSemesterRelatedBySemesterId($this->aSemesterRelatedBySemesterId);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $criteria = $this->buildCriteria();
        $pk = BasePeer::doInsert($criteria, $con);
        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPrasaranaRelatedByPrasaranaId !== null) {
                if (!$this->aPrasaranaRelatedByPrasaranaId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPrasaranaRelatedByPrasaranaId->getValidationFailures());
                }
            }

            if ($this->aPrasaranaRelatedByPrasaranaId !== null) {
                if (!$this->aPrasaranaRelatedByPrasaranaId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPrasaranaRelatedByPrasaranaId->getValidationFailures());
                }
            }

            if ($this->aSemesterRelatedBySemesterId !== null) {
                if (!$this->aSemesterRelatedBySemesterId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSemesterRelatedBySemesterId->getValidationFailures());
                }
            }

            if ($this->aSemesterRelatedBySemesterId !== null) {
                if (!$this->aSemesterRelatedBySemesterId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSemesterRelatedBySemesterId->getValidationFailures());
                }
            }


            if (($retval = PrasaranaLongitudinalPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = PrasaranaLongitudinalPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getPrasaranaId();
                break;
            case 1:
                return $this->getSemesterId();
                break;
            case 2:
                return $this->getRusakPenutupAtap();
                break;
            case 3:
                return $this->getRusakRangkaAtap();
                break;
            case 4:
                return $this->getRusakLisplangTalang();
                break;
            case 5:
                return $this->getRusakRangkaPlafon();
                break;
            case 6:
                return $this->getRusakPenutupListplafon();
                break;
            case 7:
                return $this->getRusakCatPlafon();
                break;
            case 8:
                return $this->getRusakKolomRingbalok();
                break;
            case 9:
                return $this->getRusakBataDindingpengisi();
                break;
            case 10:
                return $this->getRusakCatDinding();
                break;
            case 11:
                return $this->getRusakKusen();
                break;
            case 12:
                return $this->getRusakDaunPintu();
                break;
            case 13:
                return $this->getRusakDaunJendela();
                break;
            case 14:
                return $this->getRusakStrukturBawah();
                break;
            case 15:
                return $this->getRusakPenutupLantai();
                break;
            case 16:
                return $this->getRusakPondasi();
                break;
            case 17:
                return $this->getRusakSloof();
                break;
            case 18:
                return $this->getRusakListrik();
                break;
            case 19:
                return $this->getRusakAirhujanRabatan();
                break;
            case 20:
                return $this->getBlobId();
                break;
            case 21:
                return $this->getLastUpdate();
                break;
            case 22:
                return $this->getSoftDelete();
                break;
            case 23:
                return $this->getLastSync();
                break;
            case 24:
                return $this->getUpdaterId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['PrasaranaLongitudinal'][serialize($this->getPrimaryKey())])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['PrasaranaLongitudinal'][serialize($this->getPrimaryKey())] = true;
        $keys = PrasaranaLongitudinalPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getPrasaranaId(),
            $keys[1] => $this->getSemesterId(),
            $keys[2] => $this->getRusakPenutupAtap(),
            $keys[3] => $this->getRusakRangkaAtap(),
            $keys[4] => $this->getRusakLisplangTalang(),
            $keys[5] => $this->getRusakRangkaPlafon(),
            $keys[6] => $this->getRusakPenutupListplafon(),
            $keys[7] => $this->getRusakCatPlafon(),
            $keys[8] => $this->getRusakKolomRingbalok(),
            $keys[9] => $this->getRusakBataDindingpengisi(),
            $keys[10] => $this->getRusakCatDinding(),
            $keys[11] => $this->getRusakKusen(),
            $keys[12] => $this->getRusakDaunPintu(),
            $keys[13] => $this->getRusakDaunJendela(),
            $keys[14] => $this->getRusakStrukturBawah(),
            $keys[15] => $this->getRusakPenutupLantai(),
            $keys[16] => $this->getRusakPondasi(),
            $keys[17] => $this->getRusakSloof(),
            $keys[18] => $this->getRusakListrik(),
            $keys[19] => $this->getRusakAirhujanRabatan(),
            $keys[20] => $this->getBlobId(),
            $keys[21] => $this->getLastUpdate(),
            $keys[22] => $this->getSoftDelete(),
            $keys[23] => $this->getLastSync(),
            $keys[24] => $this->getUpdaterId(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aPrasaranaRelatedByPrasaranaId) {
                $result['PrasaranaRelatedByPrasaranaId'] = $this->aPrasaranaRelatedByPrasaranaId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPrasaranaRelatedByPrasaranaId) {
                $result['PrasaranaRelatedByPrasaranaId'] = $this->aPrasaranaRelatedByPrasaranaId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSemesterRelatedBySemesterId) {
                $result['SemesterRelatedBySemesterId'] = $this->aSemesterRelatedBySemesterId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSemesterRelatedBySemesterId) {
                $result['SemesterRelatedBySemesterId'] = $this->aSemesterRelatedBySemesterId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = PrasaranaLongitudinalPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setPrasaranaId($value);
                break;
            case 1:
                $this->setSemesterId($value);
                break;
            case 2:
                $this->setRusakPenutupAtap($value);
                break;
            case 3:
                $this->setRusakRangkaAtap($value);
                break;
            case 4:
                $this->setRusakLisplangTalang($value);
                break;
            case 5:
                $this->setRusakRangkaPlafon($value);
                break;
            case 6:
                $this->setRusakPenutupListplafon($value);
                break;
            case 7:
                $this->setRusakCatPlafon($value);
                break;
            case 8:
                $this->setRusakKolomRingbalok($value);
                break;
            case 9:
                $this->setRusakBataDindingpengisi($value);
                break;
            case 10:
                $this->setRusakCatDinding($value);
                break;
            case 11:
                $this->setRusakKusen($value);
                break;
            case 12:
                $this->setRusakDaunPintu($value);
                break;
            case 13:
                $this->setRusakDaunJendela($value);
                break;
            case 14:
                $this->setRusakStrukturBawah($value);
                break;
            case 15:
                $this->setRusakPenutupLantai($value);
                break;
            case 16:
                $this->setRusakPondasi($value);
                break;
            case 17:
                $this->setRusakSloof($value);
                break;
            case 18:
                $this->setRusakListrik($value);
                break;
            case 19:
                $this->setRusakAirhujanRabatan($value);
                break;
            case 20:
                $this->setBlobId($value);
                break;
            case 21:
                $this->setLastUpdate($value);
                break;
            case 22:
                $this->setSoftDelete($value);
                break;
            case 23:
                $this->setLastSync($value);
                break;
            case 24:
                $this->setUpdaterId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = PrasaranaLongitudinalPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setPrasaranaId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setSemesterId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setRusakPenutupAtap($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setRusakRangkaAtap($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setRusakLisplangTalang($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setRusakRangkaPlafon($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setRusakPenutupListplafon($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setRusakCatPlafon($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setRusakKolomRingbalok($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setRusakBataDindingpengisi($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setRusakCatDinding($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setRusakKusen($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setRusakDaunPintu($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setRusakDaunJendela($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setRusakStrukturBawah($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setRusakPenutupLantai($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setRusakPondasi($arr[$keys[16]]);
        if (array_key_exists($keys[17], $arr)) $this->setRusakSloof($arr[$keys[17]]);
        if (array_key_exists($keys[18], $arr)) $this->setRusakListrik($arr[$keys[18]]);
        if (array_key_exists($keys[19], $arr)) $this->setRusakAirhujanRabatan($arr[$keys[19]]);
        if (array_key_exists($keys[20], $arr)) $this->setBlobId($arr[$keys[20]]);
        if (array_key_exists($keys[21], $arr)) $this->setLastUpdate($arr[$keys[21]]);
        if (array_key_exists($keys[22], $arr)) $this->setSoftDelete($arr[$keys[22]]);
        if (array_key_exists($keys[23], $arr)) $this->setLastSync($arr[$keys[23]]);
        if (array_key_exists($keys[24], $arr)) $this->setUpdaterId($arr[$keys[24]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(PrasaranaLongitudinalPeer::DATABASE_NAME);

        if ($this->isColumnModified(PrasaranaLongitudinalPeer::PRASARANA_ID)) $criteria->add(PrasaranaLongitudinalPeer::PRASARANA_ID, $this->prasarana_id);
        if ($this->isColumnModified(PrasaranaLongitudinalPeer::SEMESTER_ID)) $criteria->add(PrasaranaLongitudinalPeer::SEMESTER_ID, $this->semester_id);
        if ($this->isColumnModified(PrasaranaLongitudinalPeer::RUSAK_PENUTUP_ATAP)) $criteria->add(PrasaranaLongitudinalPeer::RUSAK_PENUTUP_ATAP, $this->rusak_penutup_atap);
        if ($this->isColumnModified(PrasaranaLongitudinalPeer::RUSAK_RANGKA_ATAP)) $criteria->add(PrasaranaLongitudinalPeer::RUSAK_RANGKA_ATAP, $this->rusak_rangka_atap);
        if ($this->isColumnModified(PrasaranaLongitudinalPeer::RUSAK_LISPLANG_TALANG)) $criteria->add(PrasaranaLongitudinalPeer::RUSAK_LISPLANG_TALANG, $this->rusak_lisplang_talang);
        if ($this->isColumnModified(PrasaranaLongitudinalPeer::RUSAK_RANGKA_PLAFON)) $criteria->add(PrasaranaLongitudinalPeer::RUSAK_RANGKA_PLAFON, $this->rusak_rangka_plafon);
        if ($this->isColumnModified(PrasaranaLongitudinalPeer::RUSAK_PENUTUP_LISTPLAFON)) $criteria->add(PrasaranaLongitudinalPeer::RUSAK_PENUTUP_LISTPLAFON, $this->rusak_penutup_listplafon);
        if ($this->isColumnModified(PrasaranaLongitudinalPeer::RUSAK_CAT_PLAFON)) $criteria->add(PrasaranaLongitudinalPeer::RUSAK_CAT_PLAFON, $this->rusak_cat_plafon);
        if ($this->isColumnModified(PrasaranaLongitudinalPeer::RUSAK_KOLOM_RINGBALOK)) $criteria->add(PrasaranaLongitudinalPeer::RUSAK_KOLOM_RINGBALOK, $this->rusak_kolom_ringbalok);
        if ($this->isColumnModified(PrasaranaLongitudinalPeer::RUSAK_BATA_DINDINGPENGISI)) $criteria->add(PrasaranaLongitudinalPeer::RUSAK_BATA_DINDINGPENGISI, $this->rusak_bata_dindingpengisi);
        if ($this->isColumnModified(PrasaranaLongitudinalPeer::RUSAK_CAT_DINDING)) $criteria->add(PrasaranaLongitudinalPeer::RUSAK_CAT_DINDING, $this->rusak_cat_dinding);
        if ($this->isColumnModified(PrasaranaLongitudinalPeer::RUSAK_KUSEN)) $criteria->add(PrasaranaLongitudinalPeer::RUSAK_KUSEN, $this->rusak_kusen);
        if ($this->isColumnModified(PrasaranaLongitudinalPeer::RUSAK_DAUN_PINTU)) $criteria->add(PrasaranaLongitudinalPeer::RUSAK_DAUN_PINTU, $this->rusak_daun_pintu);
        if ($this->isColumnModified(PrasaranaLongitudinalPeer::RUSAK_DAUN_JENDELA)) $criteria->add(PrasaranaLongitudinalPeer::RUSAK_DAUN_JENDELA, $this->rusak_daun_jendela);
        if ($this->isColumnModified(PrasaranaLongitudinalPeer::RUSAK_STRUKTUR_BAWAH)) $criteria->add(PrasaranaLongitudinalPeer::RUSAK_STRUKTUR_BAWAH, $this->rusak_struktur_bawah);
        if ($this->isColumnModified(PrasaranaLongitudinalPeer::RUSAK_PENUTUP_LANTAI)) $criteria->add(PrasaranaLongitudinalPeer::RUSAK_PENUTUP_LANTAI, $this->rusak_penutup_lantai);
        if ($this->isColumnModified(PrasaranaLongitudinalPeer::RUSAK_PONDASI)) $criteria->add(PrasaranaLongitudinalPeer::RUSAK_PONDASI, $this->rusak_pondasi);
        if ($this->isColumnModified(PrasaranaLongitudinalPeer::RUSAK_SLOOF)) $criteria->add(PrasaranaLongitudinalPeer::RUSAK_SLOOF, $this->rusak_sloof);
        if ($this->isColumnModified(PrasaranaLongitudinalPeer::RUSAK_LISTRIK)) $criteria->add(PrasaranaLongitudinalPeer::RUSAK_LISTRIK, $this->rusak_listrik);
        if ($this->isColumnModified(PrasaranaLongitudinalPeer::RUSAK_AIRHUJAN_RABATAN)) $criteria->add(PrasaranaLongitudinalPeer::RUSAK_AIRHUJAN_RABATAN, $this->rusak_airhujan_rabatan);
        if ($this->isColumnModified(PrasaranaLongitudinalPeer::BLOB_ID)) $criteria->add(PrasaranaLongitudinalPeer::BLOB_ID, $this->blob_id);
        if ($this->isColumnModified(PrasaranaLongitudinalPeer::LAST_UPDATE)) $criteria->add(PrasaranaLongitudinalPeer::LAST_UPDATE, $this->last_update);
        if ($this->isColumnModified(PrasaranaLongitudinalPeer::SOFT_DELETE)) $criteria->add(PrasaranaLongitudinalPeer::SOFT_DELETE, $this->soft_delete);
        if ($this->isColumnModified(PrasaranaLongitudinalPeer::LAST_SYNC)) $criteria->add(PrasaranaLongitudinalPeer::LAST_SYNC, $this->last_sync);
        if ($this->isColumnModified(PrasaranaLongitudinalPeer::UPDATER_ID)) $criteria->add(PrasaranaLongitudinalPeer::UPDATER_ID, $this->updater_id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(PrasaranaLongitudinalPeer::DATABASE_NAME);
        $criteria->add(PrasaranaLongitudinalPeer::PRASARANA_ID, $this->prasarana_id);
        $criteria->add(PrasaranaLongitudinalPeer::SEMESTER_ID, $this->semester_id);

        return $criteria;
    }

    /**
     * Returns the composite primary key for this object.
     * The array elements will be in same order as specified in XML.
     * @return array
     */
    public function getPrimaryKey()
    {
        $pks = array();
        $pks[0] = $this->getPrasaranaId();
        $pks[1] = $this->getSemesterId();

        return $pks;
    }

    /**
     * Set the [composite] primary key.
     *
     * @param array $keys The elements of the composite key (order must match the order in XML file).
     * @return void
     */
    public function setPrimaryKey($keys)
    {
        $this->setPrasaranaId($keys[0]);
        $this->setSemesterId($keys[1]);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return (null === $this->getPrasaranaId()) && (null === $this->getSemesterId());
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of PrasaranaLongitudinal (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setPrasaranaId($this->getPrasaranaId());
        $copyObj->setSemesterId($this->getSemesterId());
        $copyObj->setRusakPenutupAtap($this->getRusakPenutupAtap());
        $copyObj->setRusakRangkaAtap($this->getRusakRangkaAtap());
        $copyObj->setRusakLisplangTalang($this->getRusakLisplangTalang());
        $copyObj->setRusakRangkaPlafon($this->getRusakRangkaPlafon());
        $copyObj->setRusakPenutupListplafon($this->getRusakPenutupListplafon());
        $copyObj->setRusakCatPlafon($this->getRusakCatPlafon());
        $copyObj->setRusakKolomRingbalok($this->getRusakKolomRingbalok());
        $copyObj->setRusakBataDindingpengisi($this->getRusakBataDindingpengisi());
        $copyObj->setRusakCatDinding($this->getRusakCatDinding());
        $copyObj->setRusakKusen($this->getRusakKusen());
        $copyObj->setRusakDaunPintu($this->getRusakDaunPintu());
        $copyObj->setRusakDaunJendela($this->getRusakDaunJendela());
        $copyObj->setRusakStrukturBawah($this->getRusakStrukturBawah());
        $copyObj->setRusakPenutupLantai($this->getRusakPenutupLantai());
        $copyObj->setRusakPondasi($this->getRusakPondasi());
        $copyObj->setRusakSloof($this->getRusakSloof());
        $copyObj->setRusakListrik($this->getRusakListrik());
        $copyObj->setRusakAirhujanRabatan($this->getRusakAirhujanRabatan());
        $copyObj->setBlobId($this->getBlobId());
        $copyObj->setLastUpdate($this->getLastUpdate());
        $copyObj->setSoftDelete($this->getSoftDelete());
        $copyObj->setLastSync($this->getLastSync());
        $copyObj->setUpdaterId($this->getUpdaterId());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return PrasaranaLongitudinal Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return PrasaranaLongitudinalPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new PrasaranaLongitudinalPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a Prasarana object.
     *
     * @param             Prasarana $v
     * @return PrasaranaLongitudinal The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPrasaranaRelatedByPrasaranaId(Prasarana $v = null)
    {
        if ($v === null) {
            $this->setPrasaranaId(NULL);
        } else {
            $this->setPrasaranaId($v->getPrasaranaId());
        }

        $this->aPrasaranaRelatedByPrasaranaId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Prasarana object, it will not be re-added.
        if ($v !== null) {
            $v->addPrasaranaLongitudinalRelatedByPrasaranaId($this);
        }


        return $this;
    }


    /**
     * Get the associated Prasarana object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Prasarana The associated Prasarana object.
     * @throws PropelException
     */
    public function getPrasaranaRelatedByPrasaranaId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPrasaranaRelatedByPrasaranaId === null && (($this->prasarana_id !== "" && $this->prasarana_id !== null)) && $doQuery) {
            $this->aPrasaranaRelatedByPrasaranaId = PrasaranaQuery::create()->findPk($this->prasarana_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPrasaranaRelatedByPrasaranaId->addPrasaranaLongitudinalsRelatedByPrasaranaId($this);
             */
        }

        return $this->aPrasaranaRelatedByPrasaranaId;
    }

    /**
     * Declares an association between this object and a Prasarana object.
     *
     * @param             Prasarana $v
     * @return PrasaranaLongitudinal The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPrasaranaRelatedByPrasaranaId(Prasarana $v = null)
    {
        if ($v === null) {
            $this->setPrasaranaId(NULL);
        } else {
            $this->setPrasaranaId($v->getPrasaranaId());
        }

        $this->aPrasaranaRelatedByPrasaranaId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Prasarana object, it will not be re-added.
        if ($v !== null) {
            $v->addPrasaranaLongitudinalRelatedByPrasaranaId($this);
        }


        return $this;
    }


    /**
     * Get the associated Prasarana object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Prasarana The associated Prasarana object.
     * @throws PropelException
     */
    public function getPrasaranaRelatedByPrasaranaId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPrasaranaRelatedByPrasaranaId === null && (($this->prasarana_id !== "" && $this->prasarana_id !== null)) && $doQuery) {
            $this->aPrasaranaRelatedByPrasaranaId = PrasaranaQuery::create()->findPk($this->prasarana_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPrasaranaRelatedByPrasaranaId->addPrasaranaLongitudinalsRelatedByPrasaranaId($this);
             */
        }

        return $this->aPrasaranaRelatedByPrasaranaId;
    }

    /**
     * Declares an association between this object and a Semester object.
     *
     * @param             Semester $v
     * @return PrasaranaLongitudinal The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSemesterRelatedBySemesterId(Semester $v = null)
    {
        if ($v === null) {
            $this->setSemesterId(NULL);
        } else {
            $this->setSemesterId($v->getSemesterId());
        }

        $this->aSemesterRelatedBySemesterId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Semester object, it will not be re-added.
        if ($v !== null) {
            $v->addPrasaranaLongitudinalRelatedBySemesterId($this);
        }


        return $this;
    }


    /**
     * Get the associated Semester object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Semester The associated Semester object.
     * @throws PropelException
     */
    public function getSemesterRelatedBySemesterId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSemesterRelatedBySemesterId === null && (($this->semester_id !== "" && $this->semester_id !== null)) && $doQuery) {
            $this->aSemesterRelatedBySemesterId = SemesterQuery::create()->findPk($this->semester_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSemesterRelatedBySemesterId->addPrasaranaLongitudinalsRelatedBySemesterId($this);
             */
        }

        return $this->aSemesterRelatedBySemesterId;
    }

    /**
     * Declares an association between this object and a Semester object.
     *
     * @param             Semester $v
     * @return PrasaranaLongitudinal The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSemesterRelatedBySemesterId(Semester $v = null)
    {
        if ($v === null) {
            $this->setSemesterId(NULL);
        } else {
            $this->setSemesterId($v->getSemesterId());
        }

        $this->aSemesterRelatedBySemesterId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Semester object, it will not be re-added.
        if ($v !== null) {
            $v->addPrasaranaLongitudinalRelatedBySemesterId($this);
        }


        return $this;
    }


    /**
     * Get the associated Semester object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Semester The associated Semester object.
     * @throws PropelException
     */
    public function getSemesterRelatedBySemesterId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSemesterRelatedBySemesterId === null && (($this->semester_id !== "" && $this->semester_id !== null)) && $doQuery) {
            $this->aSemesterRelatedBySemesterId = SemesterQuery::create()->findPk($this->semester_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSemesterRelatedBySemesterId->addPrasaranaLongitudinalsRelatedBySemesterId($this);
             */
        }

        return $this->aSemesterRelatedBySemesterId;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->prasarana_id = null;
        $this->semester_id = null;
        $this->rusak_penutup_atap = null;
        $this->rusak_rangka_atap = null;
        $this->rusak_lisplang_talang = null;
        $this->rusak_rangka_plafon = null;
        $this->rusak_penutup_listplafon = null;
        $this->rusak_cat_plafon = null;
        $this->rusak_kolom_ringbalok = null;
        $this->rusak_bata_dindingpengisi = null;
        $this->rusak_cat_dinding = null;
        $this->rusak_kusen = null;
        $this->rusak_daun_pintu = null;
        $this->rusak_daun_jendela = null;
        $this->rusak_struktur_bawah = null;
        $this->rusak_penutup_lantai = null;
        $this->rusak_pondasi = null;
        $this->rusak_sloof = null;
        $this->rusak_listrik = null;
        $this->rusak_airhujan_rabatan = null;
        $this->blob_id = null;
        $this->last_update = null;
        $this->soft_delete = null;
        $this->last_sync = null;
        $this->updater_id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->aPrasaranaRelatedByPrasaranaId instanceof Persistent) {
              $this->aPrasaranaRelatedByPrasaranaId->clearAllReferences($deep);
            }
            if ($this->aPrasaranaRelatedByPrasaranaId instanceof Persistent) {
              $this->aPrasaranaRelatedByPrasaranaId->clearAllReferences($deep);
            }
            if ($this->aSemesterRelatedBySemesterId instanceof Persistent) {
              $this->aSemesterRelatedBySemesterId->clearAllReferences($deep);
            }
            if ($this->aSemesterRelatedBySemesterId instanceof Persistent) {
              $this->aSemesterRelatedBySemesterId->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        $this->aPrasaranaRelatedByPrasaranaId = null;
        $this->aPrasaranaRelatedByPrasaranaId = null;
        $this->aSemesterRelatedBySemesterId = null;
        $this->aSemesterRelatedBySemesterId = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(PrasaranaLongitudinalPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
