<?php

namespace angulex\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\PesertaDidik;
use angulex\Model\PesertaDidikLongitudinal;
use angulex\Model\PesertaDidikLongitudinalPeer;
use angulex\Model\PesertaDidikLongitudinalQuery;
use angulex\Model\Semester;
use angulex\Model\VldPdLong;

/**
 * Base class that represents a query for the 'peserta_didik_longitudinal' table.
 *
 * 
 *
 * @method PesertaDidikLongitudinalQuery orderByPesertaDidikId($order = Criteria::ASC) Order by the peserta_didik_id column
 * @method PesertaDidikLongitudinalQuery orderBySemesterId($order = Criteria::ASC) Order by the semester_id column
 * @method PesertaDidikLongitudinalQuery orderByTinggiBadan($order = Criteria::ASC) Order by the tinggi_badan column
 * @method PesertaDidikLongitudinalQuery orderByBeratBadan($order = Criteria::ASC) Order by the berat_badan column
 * @method PesertaDidikLongitudinalQuery orderByJarakRumahKeSekolah($order = Criteria::ASC) Order by the jarak_rumah_ke_sekolah column
 * @method PesertaDidikLongitudinalQuery orderByJarakRumahKeSekolahKm($order = Criteria::ASC) Order by the jarak_rumah_ke_sekolah_km column
 * @method PesertaDidikLongitudinalQuery orderByWaktuTempuhKeSekolah($order = Criteria::ASC) Order by the waktu_tempuh_ke_sekolah column
 * @method PesertaDidikLongitudinalQuery orderByMenitTempuhKeSekolah($order = Criteria::ASC) Order by the menit_tempuh_ke_sekolah column
 * @method PesertaDidikLongitudinalQuery orderByJumlahSaudaraKandung($order = Criteria::ASC) Order by the jumlah_saudara_kandung column
 * @method PesertaDidikLongitudinalQuery orderByLastUpdate($order = Criteria::ASC) Order by the Last_update column
 * @method PesertaDidikLongitudinalQuery orderBySoftDelete($order = Criteria::ASC) Order by the Soft_delete column
 * @method PesertaDidikLongitudinalQuery orderByLastSync($order = Criteria::ASC) Order by the last_sync column
 * @method PesertaDidikLongitudinalQuery orderByUpdaterId($order = Criteria::ASC) Order by the Updater_ID column
 *
 * @method PesertaDidikLongitudinalQuery groupByPesertaDidikId() Group by the peserta_didik_id column
 * @method PesertaDidikLongitudinalQuery groupBySemesterId() Group by the semester_id column
 * @method PesertaDidikLongitudinalQuery groupByTinggiBadan() Group by the tinggi_badan column
 * @method PesertaDidikLongitudinalQuery groupByBeratBadan() Group by the berat_badan column
 * @method PesertaDidikLongitudinalQuery groupByJarakRumahKeSekolah() Group by the jarak_rumah_ke_sekolah column
 * @method PesertaDidikLongitudinalQuery groupByJarakRumahKeSekolahKm() Group by the jarak_rumah_ke_sekolah_km column
 * @method PesertaDidikLongitudinalQuery groupByWaktuTempuhKeSekolah() Group by the waktu_tempuh_ke_sekolah column
 * @method PesertaDidikLongitudinalQuery groupByMenitTempuhKeSekolah() Group by the menit_tempuh_ke_sekolah column
 * @method PesertaDidikLongitudinalQuery groupByJumlahSaudaraKandung() Group by the jumlah_saudara_kandung column
 * @method PesertaDidikLongitudinalQuery groupByLastUpdate() Group by the Last_update column
 * @method PesertaDidikLongitudinalQuery groupBySoftDelete() Group by the Soft_delete column
 * @method PesertaDidikLongitudinalQuery groupByLastSync() Group by the last_sync column
 * @method PesertaDidikLongitudinalQuery groupByUpdaterId() Group by the Updater_ID column
 *
 * @method PesertaDidikLongitudinalQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method PesertaDidikLongitudinalQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method PesertaDidikLongitudinalQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method PesertaDidikLongitudinalQuery leftJoinPesertaDidikRelatedByPesertaDidikId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PesertaDidikRelatedByPesertaDidikId relation
 * @method PesertaDidikLongitudinalQuery rightJoinPesertaDidikRelatedByPesertaDidikId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PesertaDidikRelatedByPesertaDidikId relation
 * @method PesertaDidikLongitudinalQuery innerJoinPesertaDidikRelatedByPesertaDidikId($relationAlias = null) Adds a INNER JOIN clause to the query using the PesertaDidikRelatedByPesertaDidikId relation
 *
 * @method PesertaDidikLongitudinalQuery leftJoinPesertaDidikRelatedByPesertaDidikId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PesertaDidikRelatedByPesertaDidikId relation
 * @method PesertaDidikLongitudinalQuery rightJoinPesertaDidikRelatedByPesertaDidikId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PesertaDidikRelatedByPesertaDidikId relation
 * @method PesertaDidikLongitudinalQuery innerJoinPesertaDidikRelatedByPesertaDidikId($relationAlias = null) Adds a INNER JOIN clause to the query using the PesertaDidikRelatedByPesertaDidikId relation
 *
 * @method PesertaDidikLongitudinalQuery leftJoinSemesterRelatedBySemesterId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SemesterRelatedBySemesterId relation
 * @method PesertaDidikLongitudinalQuery rightJoinSemesterRelatedBySemesterId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SemesterRelatedBySemesterId relation
 * @method PesertaDidikLongitudinalQuery innerJoinSemesterRelatedBySemesterId($relationAlias = null) Adds a INNER JOIN clause to the query using the SemesterRelatedBySemesterId relation
 *
 * @method PesertaDidikLongitudinalQuery leftJoinSemesterRelatedBySemesterId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SemesterRelatedBySemesterId relation
 * @method PesertaDidikLongitudinalQuery rightJoinSemesterRelatedBySemesterId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SemesterRelatedBySemesterId relation
 * @method PesertaDidikLongitudinalQuery innerJoinSemesterRelatedBySemesterId($relationAlias = null) Adds a INNER JOIN clause to the query using the SemesterRelatedBySemesterId relation
 *
 * @method PesertaDidikLongitudinalQuery leftJoinVldPdLongRelatedByPesertaDidikIdSemesterId($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldPdLongRelatedByPesertaDidikIdSemesterId relation
 * @method PesertaDidikLongitudinalQuery rightJoinVldPdLongRelatedByPesertaDidikIdSemesterId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldPdLongRelatedByPesertaDidikIdSemesterId relation
 * @method PesertaDidikLongitudinalQuery innerJoinVldPdLongRelatedByPesertaDidikIdSemesterId($relationAlias = null) Adds a INNER JOIN clause to the query using the VldPdLongRelatedByPesertaDidikIdSemesterId relation
 *
 * @method PesertaDidikLongitudinalQuery leftJoinVldPdLongRelatedByPesertaDidikIdSemesterId($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldPdLongRelatedByPesertaDidikIdSemesterId relation
 * @method PesertaDidikLongitudinalQuery rightJoinVldPdLongRelatedByPesertaDidikIdSemesterId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldPdLongRelatedByPesertaDidikIdSemesterId relation
 * @method PesertaDidikLongitudinalQuery innerJoinVldPdLongRelatedByPesertaDidikIdSemesterId($relationAlias = null) Adds a INNER JOIN clause to the query using the VldPdLongRelatedByPesertaDidikIdSemesterId relation
 *
 * @method PesertaDidikLongitudinal findOne(PropelPDO $con = null) Return the first PesertaDidikLongitudinal matching the query
 * @method PesertaDidikLongitudinal findOneOrCreate(PropelPDO $con = null) Return the first PesertaDidikLongitudinal matching the query, or a new PesertaDidikLongitudinal object populated from the query conditions when no match is found
 *
 * @method PesertaDidikLongitudinal findOneByPesertaDidikId(string $peserta_didik_id) Return the first PesertaDidikLongitudinal filtered by the peserta_didik_id column
 * @method PesertaDidikLongitudinal findOneBySemesterId(string $semester_id) Return the first PesertaDidikLongitudinal filtered by the semester_id column
 * @method PesertaDidikLongitudinal findOneByTinggiBadan(string $tinggi_badan) Return the first PesertaDidikLongitudinal filtered by the tinggi_badan column
 * @method PesertaDidikLongitudinal findOneByBeratBadan(string $berat_badan) Return the first PesertaDidikLongitudinal filtered by the berat_badan column
 * @method PesertaDidikLongitudinal findOneByJarakRumahKeSekolah(string $jarak_rumah_ke_sekolah) Return the first PesertaDidikLongitudinal filtered by the jarak_rumah_ke_sekolah column
 * @method PesertaDidikLongitudinal findOneByJarakRumahKeSekolahKm(string $jarak_rumah_ke_sekolah_km) Return the first PesertaDidikLongitudinal filtered by the jarak_rumah_ke_sekolah_km column
 * @method PesertaDidikLongitudinal findOneByWaktuTempuhKeSekolah(string $waktu_tempuh_ke_sekolah) Return the first PesertaDidikLongitudinal filtered by the waktu_tempuh_ke_sekolah column
 * @method PesertaDidikLongitudinal findOneByMenitTempuhKeSekolah(string $menit_tempuh_ke_sekolah) Return the first PesertaDidikLongitudinal filtered by the menit_tempuh_ke_sekolah column
 * @method PesertaDidikLongitudinal findOneByJumlahSaudaraKandung(string $jumlah_saudara_kandung) Return the first PesertaDidikLongitudinal filtered by the jumlah_saudara_kandung column
 * @method PesertaDidikLongitudinal findOneByLastUpdate(string $Last_update) Return the first PesertaDidikLongitudinal filtered by the Last_update column
 * @method PesertaDidikLongitudinal findOneBySoftDelete(string $Soft_delete) Return the first PesertaDidikLongitudinal filtered by the Soft_delete column
 * @method PesertaDidikLongitudinal findOneByLastSync(string $last_sync) Return the first PesertaDidikLongitudinal filtered by the last_sync column
 * @method PesertaDidikLongitudinal findOneByUpdaterId(string $Updater_ID) Return the first PesertaDidikLongitudinal filtered by the Updater_ID column
 *
 * @method array findByPesertaDidikId(string $peserta_didik_id) Return PesertaDidikLongitudinal objects filtered by the peserta_didik_id column
 * @method array findBySemesterId(string $semester_id) Return PesertaDidikLongitudinal objects filtered by the semester_id column
 * @method array findByTinggiBadan(string $tinggi_badan) Return PesertaDidikLongitudinal objects filtered by the tinggi_badan column
 * @method array findByBeratBadan(string $berat_badan) Return PesertaDidikLongitudinal objects filtered by the berat_badan column
 * @method array findByJarakRumahKeSekolah(string $jarak_rumah_ke_sekolah) Return PesertaDidikLongitudinal objects filtered by the jarak_rumah_ke_sekolah column
 * @method array findByJarakRumahKeSekolahKm(string $jarak_rumah_ke_sekolah_km) Return PesertaDidikLongitudinal objects filtered by the jarak_rumah_ke_sekolah_km column
 * @method array findByWaktuTempuhKeSekolah(string $waktu_tempuh_ke_sekolah) Return PesertaDidikLongitudinal objects filtered by the waktu_tempuh_ke_sekolah column
 * @method array findByMenitTempuhKeSekolah(string $menit_tempuh_ke_sekolah) Return PesertaDidikLongitudinal objects filtered by the menit_tempuh_ke_sekolah column
 * @method array findByJumlahSaudaraKandung(string $jumlah_saudara_kandung) Return PesertaDidikLongitudinal objects filtered by the jumlah_saudara_kandung column
 * @method array findByLastUpdate(string $Last_update) Return PesertaDidikLongitudinal objects filtered by the Last_update column
 * @method array findBySoftDelete(string $Soft_delete) Return PesertaDidikLongitudinal objects filtered by the Soft_delete column
 * @method array findByLastSync(string $last_sync) Return PesertaDidikLongitudinal objects filtered by the last_sync column
 * @method array findByUpdaterId(string $Updater_ID) Return PesertaDidikLongitudinal objects filtered by the Updater_ID column
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BasePesertaDidikLongitudinalQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BasePesertaDidikLongitudinalQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Dapodikmen', $modelName = 'angulex\\Model\\PesertaDidikLongitudinal', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new PesertaDidikLongitudinalQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   PesertaDidikLongitudinalQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return PesertaDidikLongitudinalQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof PesertaDidikLongitudinalQuery) {
            return $criteria;
        }
        $query = new PesertaDidikLongitudinalQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34), $con);
     * </code>
     *
     * @param array $key Primary key to use for the query 
                         A Primary key composition: [$peserta_didik_id, $semester_id]
     * @param     PropelPDO $con an optional connection object
     *
     * @return   PesertaDidikLongitudinal|PesertaDidikLongitudinal[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = PesertaDidikLongitudinalPeer::getInstanceFromPool(serialize(array((string) $key[0], (string) $key[1]))))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(PesertaDidikLongitudinalPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 PesertaDidikLongitudinal A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [peserta_didik_id], [semester_id], [tinggi_badan], [berat_badan], [jarak_rumah_ke_sekolah], [jarak_rumah_ke_sekolah_km], [waktu_tempuh_ke_sekolah], [menit_tempuh_ke_sekolah], [jumlah_saudara_kandung], [Last_update], [Soft_delete], [last_sync], [Updater_ID] FROM [peserta_didik_longitudinal] WHERE [peserta_didik_id] = :p0 AND [semester_id] = :p1';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_STR);			
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new PesertaDidikLongitudinal();
            $obj->hydrate($row);
            PesertaDidikLongitudinalPeer::addInstanceToPool($obj, serialize(array((string) $key[0], (string) $key[1])));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return PesertaDidikLongitudinal|PesertaDidikLongitudinal[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|PesertaDidikLongitudinal[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return PesertaDidikLongitudinalQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(PesertaDidikLongitudinalPeer::PESERTA_DIDIK_ID, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(PesertaDidikLongitudinalPeer::SEMESTER_ID, $key[1], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return PesertaDidikLongitudinalQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(PesertaDidikLongitudinalPeer::PESERTA_DIDIK_ID, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(PesertaDidikLongitudinalPeer::SEMESTER_ID, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the peserta_didik_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPesertaDidikId('fooValue');   // WHERE peserta_didik_id = 'fooValue'
     * $query->filterByPesertaDidikId('%fooValue%'); // WHERE peserta_didik_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $pesertaDidikId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PesertaDidikLongitudinalQuery The current query, for fluid interface
     */
    public function filterByPesertaDidikId($pesertaDidikId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($pesertaDidikId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $pesertaDidikId)) {
                $pesertaDidikId = str_replace('*', '%', $pesertaDidikId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PesertaDidikLongitudinalPeer::PESERTA_DIDIK_ID, $pesertaDidikId, $comparison);
    }

    /**
     * Filter the query on the semester_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySemesterId('fooValue');   // WHERE semester_id = 'fooValue'
     * $query->filterBySemesterId('%fooValue%'); // WHERE semester_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $semesterId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PesertaDidikLongitudinalQuery The current query, for fluid interface
     */
    public function filterBySemesterId($semesterId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($semesterId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $semesterId)) {
                $semesterId = str_replace('*', '%', $semesterId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PesertaDidikLongitudinalPeer::SEMESTER_ID, $semesterId, $comparison);
    }

    /**
     * Filter the query on the tinggi_badan column
     *
     * Example usage:
     * <code>
     * $query->filterByTinggiBadan(1234); // WHERE tinggi_badan = 1234
     * $query->filterByTinggiBadan(array(12, 34)); // WHERE tinggi_badan IN (12, 34)
     * $query->filterByTinggiBadan(array('min' => 12)); // WHERE tinggi_badan >= 12
     * $query->filterByTinggiBadan(array('max' => 12)); // WHERE tinggi_badan <= 12
     * </code>
     *
     * @param     mixed $tinggiBadan The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PesertaDidikLongitudinalQuery The current query, for fluid interface
     */
    public function filterByTinggiBadan($tinggiBadan = null, $comparison = null)
    {
        if (is_array($tinggiBadan)) {
            $useMinMax = false;
            if (isset($tinggiBadan['min'])) {
                $this->addUsingAlias(PesertaDidikLongitudinalPeer::TINGGI_BADAN, $tinggiBadan['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tinggiBadan['max'])) {
                $this->addUsingAlias(PesertaDidikLongitudinalPeer::TINGGI_BADAN, $tinggiBadan['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PesertaDidikLongitudinalPeer::TINGGI_BADAN, $tinggiBadan, $comparison);
    }

    /**
     * Filter the query on the berat_badan column
     *
     * Example usage:
     * <code>
     * $query->filterByBeratBadan(1234); // WHERE berat_badan = 1234
     * $query->filterByBeratBadan(array(12, 34)); // WHERE berat_badan IN (12, 34)
     * $query->filterByBeratBadan(array('min' => 12)); // WHERE berat_badan >= 12
     * $query->filterByBeratBadan(array('max' => 12)); // WHERE berat_badan <= 12
     * </code>
     *
     * @param     mixed $beratBadan The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PesertaDidikLongitudinalQuery The current query, for fluid interface
     */
    public function filterByBeratBadan($beratBadan = null, $comparison = null)
    {
        if (is_array($beratBadan)) {
            $useMinMax = false;
            if (isset($beratBadan['min'])) {
                $this->addUsingAlias(PesertaDidikLongitudinalPeer::BERAT_BADAN, $beratBadan['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($beratBadan['max'])) {
                $this->addUsingAlias(PesertaDidikLongitudinalPeer::BERAT_BADAN, $beratBadan['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PesertaDidikLongitudinalPeer::BERAT_BADAN, $beratBadan, $comparison);
    }

    /**
     * Filter the query on the jarak_rumah_ke_sekolah column
     *
     * Example usage:
     * <code>
     * $query->filterByJarakRumahKeSekolah(1234); // WHERE jarak_rumah_ke_sekolah = 1234
     * $query->filterByJarakRumahKeSekolah(array(12, 34)); // WHERE jarak_rumah_ke_sekolah IN (12, 34)
     * $query->filterByJarakRumahKeSekolah(array('min' => 12)); // WHERE jarak_rumah_ke_sekolah >= 12
     * $query->filterByJarakRumahKeSekolah(array('max' => 12)); // WHERE jarak_rumah_ke_sekolah <= 12
     * </code>
     *
     * @param     mixed $jarakRumahKeSekolah The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PesertaDidikLongitudinalQuery The current query, for fluid interface
     */
    public function filterByJarakRumahKeSekolah($jarakRumahKeSekolah = null, $comparison = null)
    {
        if (is_array($jarakRumahKeSekolah)) {
            $useMinMax = false;
            if (isset($jarakRumahKeSekolah['min'])) {
                $this->addUsingAlias(PesertaDidikLongitudinalPeer::JARAK_RUMAH_KE_SEKOLAH, $jarakRumahKeSekolah['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jarakRumahKeSekolah['max'])) {
                $this->addUsingAlias(PesertaDidikLongitudinalPeer::JARAK_RUMAH_KE_SEKOLAH, $jarakRumahKeSekolah['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PesertaDidikLongitudinalPeer::JARAK_RUMAH_KE_SEKOLAH, $jarakRumahKeSekolah, $comparison);
    }

    /**
     * Filter the query on the jarak_rumah_ke_sekolah_km column
     *
     * Example usage:
     * <code>
     * $query->filterByJarakRumahKeSekolahKm(1234); // WHERE jarak_rumah_ke_sekolah_km = 1234
     * $query->filterByJarakRumahKeSekolahKm(array(12, 34)); // WHERE jarak_rumah_ke_sekolah_km IN (12, 34)
     * $query->filterByJarakRumahKeSekolahKm(array('min' => 12)); // WHERE jarak_rumah_ke_sekolah_km >= 12
     * $query->filterByJarakRumahKeSekolahKm(array('max' => 12)); // WHERE jarak_rumah_ke_sekolah_km <= 12
     * </code>
     *
     * @param     mixed $jarakRumahKeSekolahKm The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PesertaDidikLongitudinalQuery The current query, for fluid interface
     */
    public function filterByJarakRumahKeSekolahKm($jarakRumahKeSekolahKm = null, $comparison = null)
    {
        if (is_array($jarakRumahKeSekolahKm)) {
            $useMinMax = false;
            if (isset($jarakRumahKeSekolahKm['min'])) {
                $this->addUsingAlias(PesertaDidikLongitudinalPeer::JARAK_RUMAH_KE_SEKOLAH_KM, $jarakRumahKeSekolahKm['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jarakRumahKeSekolahKm['max'])) {
                $this->addUsingAlias(PesertaDidikLongitudinalPeer::JARAK_RUMAH_KE_SEKOLAH_KM, $jarakRumahKeSekolahKm['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PesertaDidikLongitudinalPeer::JARAK_RUMAH_KE_SEKOLAH_KM, $jarakRumahKeSekolahKm, $comparison);
    }

    /**
     * Filter the query on the waktu_tempuh_ke_sekolah column
     *
     * Example usage:
     * <code>
     * $query->filterByWaktuTempuhKeSekolah(1234); // WHERE waktu_tempuh_ke_sekolah = 1234
     * $query->filterByWaktuTempuhKeSekolah(array(12, 34)); // WHERE waktu_tempuh_ke_sekolah IN (12, 34)
     * $query->filterByWaktuTempuhKeSekolah(array('min' => 12)); // WHERE waktu_tempuh_ke_sekolah >= 12
     * $query->filterByWaktuTempuhKeSekolah(array('max' => 12)); // WHERE waktu_tempuh_ke_sekolah <= 12
     * </code>
     *
     * @param     mixed $waktuTempuhKeSekolah The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PesertaDidikLongitudinalQuery The current query, for fluid interface
     */
    public function filterByWaktuTempuhKeSekolah($waktuTempuhKeSekolah = null, $comparison = null)
    {
        if (is_array($waktuTempuhKeSekolah)) {
            $useMinMax = false;
            if (isset($waktuTempuhKeSekolah['min'])) {
                $this->addUsingAlias(PesertaDidikLongitudinalPeer::WAKTU_TEMPUH_KE_SEKOLAH, $waktuTempuhKeSekolah['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($waktuTempuhKeSekolah['max'])) {
                $this->addUsingAlias(PesertaDidikLongitudinalPeer::WAKTU_TEMPUH_KE_SEKOLAH, $waktuTempuhKeSekolah['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PesertaDidikLongitudinalPeer::WAKTU_TEMPUH_KE_SEKOLAH, $waktuTempuhKeSekolah, $comparison);
    }

    /**
     * Filter the query on the menit_tempuh_ke_sekolah column
     *
     * Example usage:
     * <code>
     * $query->filterByMenitTempuhKeSekolah(1234); // WHERE menit_tempuh_ke_sekolah = 1234
     * $query->filterByMenitTempuhKeSekolah(array(12, 34)); // WHERE menit_tempuh_ke_sekolah IN (12, 34)
     * $query->filterByMenitTempuhKeSekolah(array('min' => 12)); // WHERE menit_tempuh_ke_sekolah >= 12
     * $query->filterByMenitTempuhKeSekolah(array('max' => 12)); // WHERE menit_tempuh_ke_sekolah <= 12
     * </code>
     *
     * @param     mixed $menitTempuhKeSekolah The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PesertaDidikLongitudinalQuery The current query, for fluid interface
     */
    public function filterByMenitTempuhKeSekolah($menitTempuhKeSekolah = null, $comparison = null)
    {
        if (is_array($menitTempuhKeSekolah)) {
            $useMinMax = false;
            if (isset($menitTempuhKeSekolah['min'])) {
                $this->addUsingAlias(PesertaDidikLongitudinalPeer::MENIT_TEMPUH_KE_SEKOLAH, $menitTempuhKeSekolah['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($menitTempuhKeSekolah['max'])) {
                $this->addUsingAlias(PesertaDidikLongitudinalPeer::MENIT_TEMPUH_KE_SEKOLAH, $menitTempuhKeSekolah['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PesertaDidikLongitudinalPeer::MENIT_TEMPUH_KE_SEKOLAH, $menitTempuhKeSekolah, $comparison);
    }

    /**
     * Filter the query on the jumlah_saudara_kandung column
     *
     * Example usage:
     * <code>
     * $query->filterByJumlahSaudaraKandung(1234); // WHERE jumlah_saudara_kandung = 1234
     * $query->filterByJumlahSaudaraKandung(array(12, 34)); // WHERE jumlah_saudara_kandung IN (12, 34)
     * $query->filterByJumlahSaudaraKandung(array('min' => 12)); // WHERE jumlah_saudara_kandung >= 12
     * $query->filterByJumlahSaudaraKandung(array('max' => 12)); // WHERE jumlah_saudara_kandung <= 12
     * </code>
     *
     * @param     mixed $jumlahSaudaraKandung The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PesertaDidikLongitudinalQuery The current query, for fluid interface
     */
    public function filterByJumlahSaudaraKandung($jumlahSaudaraKandung = null, $comparison = null)
    {
        if (is_array($jumlahSaudaraKandung)) {
            $useMinMax = false;
            if (isset($jumlahSaudaraKandung['min'])) {
                $this->addUsingAlias(PesertaDidikLongitudinalPeer::JUMLAH_SAUDARA_KANDUNG, $jumlahSaudaraKandung['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jumlahSaudaraKandung['max'])) {
                $this->addUsingAlias(PesertaDidikLongitudinalPeer::JUMLAH_SAUDARA_KANDUNG, $jumlahSaudaraKandung['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PesertaDidikLongitudinalPeer::JUMLAH_SAUDARA_KANDUNG, $jumlahSaudaraKandung, $comparison);
    }

    /**
     * Filter the query on the Last_update column
     *
     * Example usage:
     * <code>
     * $query->filterByLastUpdate('2011-03-14'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate('now'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate(array('max' => 'yesterday')); // WHERE Last_update > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastUpdate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PesertaDidikLongitudinalQuery The current query, for fluid interface
     */
    public function filterByLastUpdate($lastUpdate = null, $comparison = null)
    {
        if (is_array($lastUpdate)) {
            $useMinMax = false;
            if (isset($lastUpdate['min'])) {
                $this->addUsingAlias(PesertaDidikLongitudinalPeer::LAST_UPDATE, $lastUpdate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastUpdate['max'])) {
                $this->addUsingAlias(PesertaDidikLongitudinalPeer::LAST_UPDATE, $lastUpdate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PesertaDidikLongitudinalPeer::LAST_UPDATE, $lastUpdate, $comparison);
    }

    /**
     * Filter the query on the Soft_delete column
     *
     * Example usage:
     * <code>
     * $query->filterBySoftDelete(1234); // WHERE Soft_delete = 1234
     * $query->filterBySoftDelete(array(12, 34)); // WHERE Soft_delete IN (12, 34)
     * $query->filterBySoftDelete(array('min' => 12)); // WHERE Soft_delete >= 12
     * $query->filterBySoftDelete(array('max' => 12)); // WHERE Soft_delete <= 12
     * </code>
     *
     * @param     mixed $softDelete The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PesertaDidikLongitudinalQuery The current query, for fluid interface
     */
    public function filterBySoftDelete($softDelete = null, $comparison = null)
    {
        if (is_array($softDelete)) {
            $useMinMax = false;
            if (isset($softDelete['min'])) {
                $this->addUsingAlias(PesertaDidikLongitudinalPeer::SOFT_DELETE, $softDelete['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($softDelete['max'])) {
                $this->addUsingAlias(PesertaDidikLongitudinalPeer::SOFT_DELETE, $softDelete['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PesertaDidikLongitudinalPeer::SOFT_DELETE, $softDelete, $comparison);
    }

    /**
     * Filter the query on the last_sync column
     *
     * Example usage:
     * <code>
     * $query->filterByLastSync('2011-03-14'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync('now'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync(array('max' => 'yesterday')); // WHERE last_sync > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastSync The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PesertaDidikLongitudinalQuery The current query, for fluid interface
     */
    public function filterByLastSync($lastSync = null, $comparison = null)
    {
        if (is_array($lastSync)) {
            $useMinMax = false;
            if (isset($lastSync['min'])) {
                $this->addUsingAlias(PesertaDidikLongitudinalPeer::LAST_SYNC, $lastSync['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastSync['max'])) {
                $this->addUsingAlias(PesertaDidikLongitudinalPeer::LAST_SYNC, $lastSync['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PesertaDidikLongitudinalPeer::LAST_SYNC, $lastSync, $comparison);
    }

    /**
     * Filter the query on the Updater_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdaterId('fooValue');   // WHERE Updater_ID = 'fooValue'
     * $query->filterByUpdaterId('%fooValue%'); // WHERE Updater_ID LIKE '%fooValue%'
     * </code>
     *
     * @param     string $updaterId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PesertaDidikLongitudinalQuery The current query, for fluid interface
     */
    public function filterByUpdaterId($updaterId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($updaterId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $updaterId)) {
                $updaterId = str_replace('*', '%', $updaterId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PesertaDidikLongitudinalPeer::UPDATER_ID, $updaterId, $comparison);
    }

    /**
     * Filter the query by a related PesertaDidik object
     *
     * @param   PesertaDidik|PropelObjectCollection $pesertaDidik The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PesertaDidikLongitudinalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPesertaDidikRelatedByPesertaDidikId($pesertaDidik, $comparison = null)
    {
        if ($pesertaDidik instanceof PesertaDidik) {
            return $this
                ->addUsingAlias(PesertaDidikLongitudinalPeer::PESERTA_DIDIK_ID, $pesertaDidik->getPesertaDidikId(), $comparison);
        } elseif ($pesertaDidik instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PesertaDidikLongitudinalPeer::PESERTA_DIDIK_ID, $pesertaDidik->toKeyValue('PrimaryKey', 'PesertaDidikId'), $comparison);
        } else {
            throw new PropelException('filterByPesertaDidikRelatedByPesertaDidikId() only accepts arguments of type PesertaDidik or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PesertaDidikRelatedByPesertaDidikId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PesertaDidikLongitudinalQuery The current query, for fluid interface
     */
    public function joinPesertaDidikRelatedByPesertaDidikId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PesertaDidikRelatedByPesertaDidikId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PesertaDidikRelatedByPesertaDidikId');
        }

        return $this;
    }

    /**
     * Use the PesertaDidikRelatedByPesertaDidikId relation PesertaDidik object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PesertaDidikQuery A secondary query class using the current class as primary query
     */
    public function usePesertaDidikRelatedByPesertaDidikIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPesertaDidikRelatedByPesertaDidikId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PesertaDidikRelatedByPesertaDidikId', '\angulex\Model\PesertaDidikQuery');
    }

    /**
     * Filter the query by a related PesertaDidik object
     *
     * @param   PesertaDidik|PropelObjectCollection $pesertaDidik The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PesertaDidikLongitudinalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPesertaDidikRelatedByPesertaDidikId($pesertaDidik, $comparison = null)
    {
        if ($pesertaDidik instanceof PesertaDidik) {
            return $this
                ->addUsingAlias(PesertaDidikLongitudinalPeer::PESERTA_DIDIK_ID, $pesertaDidik->getPesertaDidikId(), $comparison);
        } elseif ($pesertaDidik instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PesertaDidikLongitudinalPeer::PESERTA_DIDIK_ID, $pesertaDidik->toKeyValue('PrimaryKey', 'PesertaDidikId'), $comparison);
        } else {
            throw new PropelException('filterByPesertaDidikRelatedByPesertaDidikId() only accepts arguments of type PesertaDidik or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PesertaDidikRelatedByPesertaDidikId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PesertaDidikLongitudinalQuery The current query, for fluid interface
     */
    public function joinPesertaDidikRelatedByPesertaDidikId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PesertaDidikRelatedByPesertaDidikId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PesertaDidikRelatedByPesertaDidikId');
        }

        return $this;
    }

    /**
     * Use the PesertaDidikRelatedByPesertaDidikId relation PesertaDidik object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PesertaDidikQuery A secondary query class using the current class as primary query
     */
    public function usePesertaDidikRelatedByPesertaDidikIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPesertaDidikRelatedByPesertaDidikId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PesertaDidikRelatedByPesertaDidikId', '\angulex\Model\PesertaDidikQuery');
    }

    /**
     * Filter the query by a related Semester object
     *
     * @param   Semester|PropelObjectCollection $semester The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PesertaDidikLongitudinalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySemesterRelatedBySemesterId($semester, $comparison = null)
    {
        if ($semester instanceof Semester) {
            return $this
                ->addUsingAlias(PesertaDidikLongitudinalPeer::SEMESTER_ID, $semester->getSemesterId(), $comparison);
        } elseif ($semester instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PesertaDidikLongitudinalPeer::SEMESTER_ID, $semester->toKeyValue('PrimaryKey', 'SemesterId'), $comparison);
        } else {
            throw new PropelException('filterBySemesterRelatedBySemesterId() only accepts arguments of type Semester or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SemesterRelatedBySemesterId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PesertaDidikLongitudinalQuery The current query, for fluid interface
     */
    public function joinSemesterRelatedBySemesterId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SemesterRelatedBySemesterId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SemesterRelatedBySemesterId');
        }

        return $this;
    }

    /**
     * Use the SemesterRelatedBySemesterId relation Semester object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SemesterQuery A secondary query class using the current class as primary query
     */
    public function useSemesterRelatedBySemesterIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSemesterRelatedBySemesterId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SemesterRelatedBySemesterId', '\angulex\Model\SemesterQuery');
    }

    /**
     * Filter the query by a related Semester object
     *
     * @param   Semester|PropelObjectCollection $semester The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PesertaDidikLongitudinalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySemesterRelatedBySemesterId($semester, $comparison = null)
    {
        if ($semester instanceof Semester) {
            return $this
                ->addUsingAlias(PesertaDidikLongitudinalPeer::SEMESTER_ID, $semester->getSemesterId(), $comparison);
        } elseif ($semester instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PesertaDidikLongitudinalPeer::SEMESTER_ID, $semester->toKeyValue('PrimaryKey', 'SemesterId'), $comparison);
        } else {
            throw new PropelException('filterBySemesterRelatedBySemesterId() only accepts arguments of type Semester or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SemesterRelatedBySemesterId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PesertaDidikLongitudinalQuery The current query, for fluid interface
     */
    public function joinSemesterRelatedBySemesterId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SemesterRelatedBySemesterId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SemesterRelatedBySemesterId');
        }

        return $this;
    }

    /**
     * Use the SemesterRelatedBySemesterId relation Semester object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SemesterQuery A secondary query class using the current class as primary query
     */
    public function useSemesterRelatedBySemesterIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSemesterRelatedBySemesterId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SemesterRelatedBySemesterId', '\angulex\Model\SemesterQuery');
    }

    /**
     * Filter the query by a related VldPdLong object
     *
     * @param   VldPdLong|PropelObjectCollection $vldPdLong  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PesertaDidikLongitudinalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldPdLongRelatedByPesertaDidikIdSemesterId($vldPdLong, $comparison = null)
    {
        if ($vldPdLong instanceof VldPdLong) {
            return $this
                ->addUsingAlias(PesertaDidikLongitudinalPeer::PESERTA_DIDIK_ID, $vldPdLong->getSemesterId(), $comparison)
                ->addUsingAlias(PesertaDidikLongitudinalPeer::SEMESTER_ID, $vldPdLong->getSemesterId(), $comparison);
        } else {
            throw new PropelException('filterByVldPdLongRelatedByPesertaDidikIdSemesterId() only accepts arguments of type VldPdLong');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldPdLongRelatedByPesertaDidikIdSemesterId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PesertaDidikLongitudinalQuery The current query, for fluid interface
     */
    public function joinVldPdLongRelatedByPesertaDidikIdSemesterId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldPdLongRelatedByPesertaDidikIdSemesterId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldPdLongRelatedByPesertaDidikIdSemesterId');
        }

        return $this;
    }

    /**
     * Use the VldPdLongRelatedByPesertaDidikIdSemesterId relation VldPdLong object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldPdLongQuery A secondary query class using the current class as primary query
     */
    public function useVldPdLongRelatedByPesertaDidikIdSemesterIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldPdLongRelatedByPesertaDidikIdSemesterId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldPdLongRelatedByPesertaDidikIdSemesterId', '\angulex\Model\VldPdLongQuery');
    }

    /**
     * Filter the query by a related VldPdLong object
     *
     * @param   VldPdLong|PropelObjectCollection $vldPdLong  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PesertaDidikLongitudinalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldPdLongRelatedByPesertaDidikIdSemesterId($vldPdLong, $comparison = null)
    {
        if ($vldPdLong instanceof VldPdLong) {
            return $this
                ->addUsingAlias(PesertaDidikLongitudinalPeer::PESERTA_DIDIK_ID, $vldPdLong->getSemesterId(), $comparison)
                ->addUsingAlias(PesertaDidikLongitudinalPeer::SEMESTER_ID, $vldPdLong->getSemesterId(), $comparison);
        } else {
            throw new PropelException('filterByVldPdLongRelatedByPesertaDidikIdSemesterId() only accepts arguments of type VldPdLong');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldPdLongRelatedByPesertaDidikIdSemesterId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PesertaDidikLongitudinalQuery The current query, for fluid interface
     */
    public function joinVldPdLongRelatedByPesertaDidikIdSemesterId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldPdLongRelatedByPesertaDidikIdSemesterId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldPdLongRelatedByPesertaDidikIdSemesterId');
        }

        return $this;
    }

    /**
     * Use the VldPdLongRelatedByPesertaDidikIdSemesterId relation VldPdLong object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldPdLongQuery A secondary query class using the current class as primary query
     */
    public function useVldPdLongRelatedByPesertaDidikIdSemesterIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldPdLongRelatedByPesertaDidikIdSemesterId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldPdLongRelatedByPesertaDidikIdSemesterId', '\angulex\Model\VldPdLongQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   PesertaDidikLongitudinal $pesertaDidikLongitudinal Object to remove from the list of results
     *
     * @return PesertaDidikLongitudinalQuery The current query, for fluid interface
     */
    public function prune($pesertaDidikLongitudinal = null)
    {
        if ($pesertaDidikLongitudinal) {
            $this->addCond('pruneCond0', $this->getAliasedColName(PesertaDidikLongitudinalPeer::PESERTA_DIDIK_ID), $pesertaDidikLongitudinal->getPesertaDidikId(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(PesertaDidikLongitudinalPeer::SEMESTER_ID), $pesertaDidikLongitudinal->getSemesterId(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

}
