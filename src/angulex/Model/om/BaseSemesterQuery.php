<?php

namespace angulex\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\BatasWaktuRapor;
use angulex\Model\BukuAlatLongitudinal;
use angulex\Model\Pembelajaran;
use angulex\Model\PesertaDidikLongitudinal;
use angulex\Model\PrasaranaLongitudinal;
use angulex\Model\RombonganBelajar;
use angulex\Model\Sanitasi;
use angulex\Model\SaranaLongitudinal;
use angulex\Model\SekolahLongitudinal;
use angulex\Model\Semester;
use angulex\Model\SemesterPeer;
use angulex\Model\SemesterQuery;
use angulex\Model\TahunAjaran;

/**
 * Base class that represents a query for the 'ref.semester' table.
 *
 * 
 *
 * @method SemesterQuery orderBySemesterId($order = Criteria::ASC) Order by the semester_id column
 * @method SemesterQuery orderByTahunAjaranId($order = Criteria::ASC) Order by the tahun_ajaran_id column
 * @method SemesterQuery orderByNama($order = Criteria::ASC) Order by the nama column
 * @method SemesterQuery orderBySemester($order = Criteria::ASC) Order by the semester column
 * @method SemesterQuery orderByPeriodeAktif($order = Criteria::ASC) Order by the periode_aktif column
 * @method SemesterQuery orderByTanggalMulai($order = Criteria::ASC) Order by the tanggal_mulai column
 * @method SemesterQuery orderByTanggalSelesai($order = Criteria::ASC) Order by the tanggal_selesai column
 * @method SemesterQuery orderByCreateDate($order = Criteria::ASC) Order by the create_date column
 * @method SemesterQuery orderByLastUpdate($order = Criteria::ASC) Order by the last_update column
 * @method SemesterQuery orderByExpiredDate($order = Criteria::ASC) Order by the expired_date column
 * @method SemesterQuery orderByLastSync($order = Criteria::ASC) Order by the last_sync column
 *
 * @method SemesterQuery groupBySemesterId() Group by the semester_id column
 * @method SemesterQuery groupByTahunAjaranId() Group by the tahun_ajaran_id column
 * @method SemesterQuery groupByNama() Group by the nama column
 * @method SemesterQuery groupBySemester() Group by the semester column
 * @method SemesterQuery groupByPeriodeAktif() Group by the periode_aktif column
 * @method SemesterQuery groupByTanggalMulai() Group by the tanggal_mulai column
 * @method SemesterQuery groupByTanggalSelesai() Group by the tanggal_selesai column
 * @method SemesterQuery groupByCreateDate() Group by the create_date column
 * @method SemesterQuery groupByLastUpdate() Group by the last_update column
 * @method SemesterQuery groupByExpiredDate() Group by the expired_date column
 * @method SemesterQuery groupByLastSync() Group by the last_sync column
 *
 * @method SemesterQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SemesterQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SemesterQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method SemesterQuery leftJoinTahunAjaran($relationAlias = null) Adds a LEFT JOIN clause to the query using the TahunAjaran relation
 * @method SemesterQuery rightJoinTahunAjaran($relationAlias = null) Adds a RIGHT JOIN clause to the query using the TahunAjaran relation
 * @method SemesterQuery innerJoinTahunAjaran($relationAlias = null) Adds a INNER JOIN clause to the query using the TahunAjaran relation
 *
 * @method SemesterQuery leftJoinSekolahLongitudinalRelatedBySemesterId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SekolahLongitudinalRelatedBySemesterId relation
 * @method SemesterQuery rightJoinSekolahLongitudinalRelatedBySemesterId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SekolahLongitudinalRelatedBySemesterId relation
 * @method SemesterQuery innerJoinSekolahLongitudinalRelatedBySemesterId($relationAlias = null) Adds a INNER JOIN clause to the query using the SekolahLongitudinalRelatedBySemesterId relation
 *
 * @method SemesterQuery leftJoinSekolahLongitudinalRelatedBySemesterId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SekolahLongitudinalRelatedBySemesterId relation
 * @method SemesterQuery rightJoinSekolahLongitudinalRelatedBySemesterId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SekolahLongitudinalRelatedBySemesterId relation
 * @method SemesterQuery innerJoinSekolahLongitudinalRelatedBySemesterId($relationAlias = null) Adds a INNER JOIN clause to the query using the SekolahLongitudinalRelatedBySemesterId relation
 *
 * @method SemesterQuery leftJoinPesertaDidikLongitudinalRelatedBySemesterId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PesertaDidikLongitudinalRelatedBySemesterId relation
 * @method SemesterQuery rightJoinPesertaDidikLongitudinalRelatedBySemesterId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PesertaDidikLongitudinalRelatedBySemesterId relation
 * @method SemesterQuery innerJoinPesertaDidikLongitudinalRelatedBySemesterId($relationAlias = null) Adds a INNER JOIN clause to the query using the PesertaDidikLongitudinalRelatedBySemesterId relation
 *
 * @method SemesterQuery leftJoinPesertaDidikLongitudinalRelatedBySemesterId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PesertaDidikLongitudinalRelatedBySemesterId relation
 * @method SemesterQuery rightJoinPesertaDidikLongitudinalRelatedBySemesterId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PesertaDidikLongitudinalRelatedBySemesterId relation
 * @method SemesterQuery innerJoinPesertaDidikLongitudinalRelatedBySemesterId($relationAlias = null) Adds a INNER JOIN clause to the query using the PesertaDidikLongitudinalRelatedBySemesterId relation
 *
 * @method SemesterQuery leftJoinSanitasiRelatedBySemesterId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SanitasiRelatedBySemesterId relation
 * @method SemesterQuery rightJoinSanitasiRelatedBySemesterId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SanitasiRelatedBySemesterId relation
 * @method SemesterQuery innerJoinSanitasiRelatedBySemesterId($relationAlias = null) Adds a INNER JOIN clause to the query using the SanitasiRelatedBySemesterId relation
 *
 * @method SemesterQuery leftJoinSanitasiRelatedBySemesterId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SanitasiRelatedBySemesterId relation
 * @method SemesterQuery rightJoinSanitasiRelatedBySemesterId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SanitasiRelatedBySemesterId relation
 * @method SemesterQuery innerJoinSanitasiRelatedBySemesterId($relationAlias = null) Adds a INNER JOIN clause to the query using the SanitasiRelatedBySemesterId relation
 *
 * @method SemesterQuery leftJoinSaranaLongitudinalRelatedBySemesterId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SaranaLongitudinalRelatedBySemesterId relation
 * @method SemesterQuery rightJoinSaranaLongitudinalRelatedBySemesterId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SaranaLongitudinalRelatedBySemesterId relation
 * @method SemesterQuery innerJoinSaranaLongitudinalRelatedBySemesterId($relationAlias = null) Adds a INNER JOIN clause to the query using the SaranaLongitudinalRelatedBySemesterId relation
 *
 * @method SemesterQuery leftJoinSaranaLongitudinalRelatedBySemesterId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SaranaLongitudinalRelatedBySemesterId relation
 * @method SemesterQuery rightJoinSaranaLongitudinalRelatedBySemesterId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SaranaLongitudinalRelatedBySemesterId relation
 * @method SemesterQuery innerJoinSaranaLongitudinalRelatedBySemesterId($relationAlias = null) Adds a INNER JOIN clause to the query using the SaranaLongitudinalRelatedBySemesterId relation
 *
 * @method SemesterQuery leftJoinBatasWaktuRapor($relationAlias = null) Adds a LEFT JOIN clause to the query using the BatasWaktuRapor relation
 * @method SemesterQuery rightJoinBatasWaktuRapor($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BatasWaktuRapor relation
 * @method SemesterQuery innerJoinBatasWaktuRapor($relationAlias = null) Adds a INNER JOIN clause to the query using the BatasWaktuRapor relation
 *
 * @method SemesterQuery leftJoinPembelajaranRelatedBySemesterId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PembelajaranRelatedBySemesterId relation
 * @method SemesterQuery rightJoinPembelajaranRelatedBySemesterId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PembelajaranRelatedBySemesterId relation
 * @method SemesterQuery innerJoinPembelajaranRelatedBySemesterId($relationAlias = null) Adds a INNER JOIN clause to the query using the PembelajaranRelatedBySemesterId relation
 *
 * @method SemesterQuery leftJoinPembelajaranRelatedBySemesterId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PembelajaranRelatedBySemesterId relation
 * @method SemesterQuery rightJoinPembelajaranRelatedBySemesterId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PembelajaranRelatedBySemesterId relation
 * @method SemesterQuery innerJoinPembelajaranRelatedBySemesterId($relationAlias = null) Adds a INNER JOIN clause to the query using the PembelajaranRelatedBySemesterId relation
 *
 * @method SemesterQuery leftJoinPrasaranaLongitudinalRelatedBySemesterId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PrasaranaLongitudinalRelatedBySemesterId relation
 * @method SemesterQuery rightJoinPrasaranaLongitudinalRelatedBySemesterId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PrasaranaLongitudinalRelatedBySemesterId relation
 * @method SemesterQuery innerJoinPrasaranaLongitudinalRelatedBySemesterId($relationAlias = null) Adds a INNER JOIN clause to the query using the PrasaranaLongitudinalRelatedBySemesterId relation
 *
 * @method SemesterQuery leftJoinPrasaranaLongitudinalRelatedBySemesterId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PrasaranaLongitudinalRelatedBySemesterId relation
 * @method SemesterQuery rightJoinPrasaranaLongitudinalRelatedBySemesterId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PrasaranaLongitudinalRelatedBySemesterId relation
 * @method SemesterQuery innerJoinPrasaranaLongitudinalRelatedBySemesterId($relationAlias = null) Adds a INNER JOIN clause to the query using the PrasaranaLongitudinalRelatedBySemesterId relation
 *
 * @method SemesterQuery leftJoinRombonganBelajarRelatedBySemesterId($relationAlias = null) Adds a LEFT JOIN clause to the query using the RombonganBelajarRelatedBySemesterId relation
 * @method SemesterQuery rightJoinRombonganBelajarRelatedBySemesterId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RombonganBelajarRelatedBySemesterId relation
 * @method SemesterQuery innerJoinRombonganBelajarRelatedBySemesterId($relationAlias = null) Adds a INNER JOIN clause to the query using the RombonganBelajarRelatedBySemesterId relation
 *
 * @method SemesterQuery leftJoinRombonganBelajarRelatedBySemesterId($relationAlias = null) Adds a LEFT JOIN clause to the query using the RombonganBelajarRelatedBySemesterId relation
 * @method SemesterQuery rightJoinRombonganBelajarRelatedBySemesterId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RombonganBelajarRelatedBySemesterId relation
 * @method SemesterQuery innerJoinRombonganBelajarRelatedBySemesterId($relationAlias = null) Adds a INNER JOIN clause to the query using the RombonganBelajarRelatedBySemesterId relation
 *
 * @method SemesterQuery leftJoinBukuAlatLongitudinalRelatedBySemesterId($relationAlias = null) Adds a LEFT JOIN clause to the query using the BukuAlatLongitudinalRelatedBySemesterId relation
 * @method SemesterQuery rightJoinBukuAlatLongitudinalRelatedBySemesterId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BukuAlatLongitudinalRelatedBySemesterId relation
 * @method SemesterQuery innerJoinBukuAlatLongitudinalRelatedBySemesterId($relationAlias = null) Adds a INNER JOIN clause to the query using the BukuAlatLongitudinalRelatedBySemesterId relation
 *
 * @method SemesterQuery leftJoinBukuAlatLongitudinalRelatedBySemesterId($relationAlias = null) Adds a LEFT JOIN clause to the query using the BukuAlatLongitudinalRelatedBySemesterId relation
 * @method SemesterQuery rightJoinBukuAlatLongitudinalRelatedBySemesterId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BukuAlatLongitudinalRelatedBySemesterId relation
 * @method SemesterQuery innerJoinBukuAlatLongitudinalRelatedBySemesterId($relationAlias = null) Adds a INNER JOIN clause to the query using the BukuAlatLongitudinalRelatedBySemesterId relation
 *
 * @method Semester findOne(PropelPDO $con = null) Return the first Semester matching the query
 * @method Semester findOneOrCreate(PropelPDO $con = null) Return the first Semester matching the query, or a new Semester object populated from the query conditions when no match is found
 *
 * @method Semester findOneByTahunAjaranId(string $tahun_ajaran_id) Return the first Semester filtered by the tahun_ajaran_id column
 * @method Semester findOneByNama(string $nama) Return the first Semester filtered by the nama column
 * @method Semester findOneBySemester(string $semester) Return the first Semester filtered by the semester column
 * @method Semester findOneByPeriodeAktif(string $periode_aktif) Return the first Semester filtered by the periode_aktif column
 * @method Semester findOneByTanggalMulai(string $tanggal_mulai) Return the first Semester filtered by the tanggal_mulai column
 * @method Semester findOneByTanggalSelesai(string $tanggal_selesai) Return the first Semester filtered by the tanggal_selesai column
 * @method Semester findOneByCreateDate(string $create_date) Return the first Semester filtered by the create_date column
 * @method Semester findOneByLastUpdate(string $last_update) Return the first Semester filtered by the last_update column
 * @method Semester findOneByExpiredDate(string $expired_date) Return the first Semester filtered by the expired_date column
 * @method Semester findOneByLastSync(string $last_sync) Return the first Semester filtered by the last_sync column
 *
 * @method array findBySemesterId(string $semester_id) Return Semester objects filtered by the semester_id column
 * @method array findByTahunAjaranId(string $tahun_ajaran_id) Return Semester objects filtered by the tahun_ajaran_id column
 * @method array findByNama(string $nama) Return Semester objects filtered by the nama column
 * @method array findBySemester(string $semester) Return Semester objects filtered by the semester column
 * @method array findByPeriodeAktif(string $periode_aktif) Return Semester objects filtered by the periode_aktif column
 * @method array findByTanggalMulai(string $tanggal_mulai) Return Semester objects filtered by the tanggal_mulai column
 * @method array findByTanggalSelesai(string $tanggal_selesai) Return Semester objects filtered by the tanggal_selesai column
 * @method array findByCreateDate(string $create_date) Return Semester objects filtered by the create_date column
 * @method array findByLastUpdate(string $last_update) Return Semester objects filtered by the last_update column
 * @method array findByExpiredDate(string $expired_date) Return Semester objects filtered by the expired_date column
 * @method array findByLastSync(string $last_sync) Return Semester objects filtered by the last_sync column
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseSemesterQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSemesterQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Dapodikmen', $modelName = 'angulex\\Model\\Semester', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SemesterQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   SemesterQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SemesterQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SemesterQuery) {
            return $criteria;
        }
        $query = new SemesterQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Semester|Semester[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SemesterPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SemesterPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Semester A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneBySemesterId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Semester A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [semester_id], [tahun_ajaran_id], [nama], [semester], [periode_aktif], [tanggal_mulai], [tanggal_selesai], [create_date], [last_update], [expired_date], [last_sync] FROM [ref].[semester] WHERE [semester_id] = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Semester();
            $obj->hydrate($row);
            SemesterPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Semester|Semester[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Semester[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SemesterQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SemesterPeer::SEMESTER_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SemesterQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SemesterPeer::SEMESTER_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the semester_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySemesterId('fooValue');   // WHERE semester_id = 'fooValue'
     * $query->filterBySemesterId('%fooValue%'); // WHERE semester_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $semesterId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SemesterQuery The current query, for fluid interface
     */
    public function filterBySemesterId($semesterId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($semesterId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $semesterId)) {
                $semesterId = str_replace('*', '%', $semesterId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SemesterPeer::SEMESTER_ID, $semesterId, $comparison);
    }

    /**
     * Filter the query on the tahun_ajaran_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTahunAjaranId(1234); // WHERE tahun_ajaran_id = 1234
     * $query->filterByTahunAjaranId(array(12, 34)); // WHERE tahun_ajaran_id IN (12, 34)
     * $query->filterByTahunAjaranId(array('min' => 12)); // WHERE tahun_ajaran_id >= 12
     * $query->filterByTahunAjaranId(array('max' => 12)); // WHERE tahun_ajaran_id <= 12
     * </code>
     *
     * @see       filterByTahunAjaran()
     *
     * @param     mixed $tahunAjaranId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SemesterQuery The current query, for fluid interface
     */
    public function filterByTahunAjaranId($tahunAjaranId = null, $comparison = null)
    {
        if (is_array($tahunAjaranId)) {
            $useMinMax = false;
            if (isset($tahunAjaranId['min'])) {
                $this->addUsingAlias(SemesterPeer::TAHUN_AJARAN_ID, $tahunAjaranId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tahunAjaranId['max'])) {
                $this->addUsingAlias(SemesterPeer::TAHUN_AJARAN_ID, $tahunAjaranId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SemesterPeer::TAHUN_AJARAN_ID, $tahunAjaranId, $comparison);
    }

    /**
     * Filter the query on the nama column
     *
     * Example usage:
     * <code>
     * $query->filterByNama('fooValue');   // WHERE nama = 'fooValue'
     * $query->filterByNama('%fooValue%'); // WHERE nama LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nama The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SemesterQuery The current query, for fluid interface
     */
    public function filterByNama($nama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nama)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nama)) {
                $nama = str_replace('*', '%', $nama);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SemesterPeer::NAMA, $nama, $comparison);
    }

    /**
     * Filter the query on the semester column
     *
     * Example usage:
     * <code>
     * $query->filterBySemester(1234); // WHERE semester = 1234
     * $query->filterBySemester(array(12, 34)); // WHERE semester IN (12, 34)
     * $query->filterBySemester(array('min' => 12)); // WHERE semester >= 12
     * $query->filterBySemester(array('max' => 12)); // WHERE semester <= 12
     * </code>
     *
     * @param     mixed $semester The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SemesterQuery The current query, for fluid interface
     */
    public function filterBySemester($semester = null, $comparison = null)
    {
        if (is_array($semester)) {
            $useMinMax = false;
            if (isset($semester['min'])) {
                $this->addUsingAlias(SemesterPeer::SEMESTER, $semester['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($semester['max'])) {
                $this->addUsingAlias(SemesterPeer::SEMESTER, $semester['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SemesterPeer::SEMESTER, $semester, $comparison);
    }

    /**
     * Filter the query on the periode_aktif column
     *
     * Example usage:
     * <code>
     * $query->filterByPeriodeAktif(1234); // WHERE periode_aktif = 1234
     * $query->filterByPeriodeAktif(array(12, 34)); // WHERE periode_aktif IN (12, 34)
     * $query->filterByPeriodeAktif(array('min' => 12)); // WHERE periode_aktif >= 12
     * $query->filterByPeriodeAktif(array('max' => 12)); // WHERE periode_aktif <= 12
     * </code>
     *
     * @param     mixed $periodeAktif The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SemesterQuery The current query, for fluid interface
     */
    public function filterByPeriodeAktif($periodeAktif = null, $comparison = null)
    {
        if (is_array($periodeAktif)) {
            $useMinMax = false;
            if (isset($periodeAktif['min'])) {
                $this->addUsingAlias(SemesterPeer::PERIODE_AKTIF, $periodeAktif['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($periodeAktif['max'])) {
                $this->addUsingAlias(SemesterPeer::PERIODE_AKTIF, $periodeAktif['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SemesterPeer::PERIODE_AKTIF, $periodeAktif, $comparison);
    }

    /**
     * Filter the query on the tanggal_mulai column
     *
     * Example usage:
     * <code>
     * $query->filterByTanggalMulai('fooValue');   // WHERE tanggal_mulai = 'fooValue'
     * $query->filterByTanggalMulai('%fooValue%'); // WHERE tanggal_mulai LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tanggalMulai The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SemesterQuery The current query, for fluid interface
     */
    public function filterByTanggalMulai($tanggalMulai = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tanggalMulai)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tanggalMulai)) {
                $tanggalMulai = str_replace('*', '%', $tanggalMulai);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SemesterPeer::TANGGAL_MULAI, $tanggalMulai, $comparison);
    }

    /**
     * Filter the query on the tanggal_selesai column
     *
     * Example usage:
     * <code>
     * $query->filterByTanggalSelesai('fooValue');   // WHERE tanggal_selesai = 'fooValue'
     * $query->filterByTanggalSelesai('%fooValue%'); // WHERE tanggal_selesai LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tanggalSelesai The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SemesterQuery The current query, for fluid interface
     */
    public function filterByTanggalSelesai($tanggalSelesai = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tanggalSelesai)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tanggalSelesai)) {
                $tanggalSelesai = str_replace('*', '%', $tanggalSelesai);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SemesterPeer::TANGGAL_SELESAI, $tanggalSelesai, $comparison);
    }

    /**
     * Filter the query on the create_date column
     *
     * Example usage:
     * <code>
     * $query->filterByCreateDate('2011-03-14'); // WHERE create_date = '2011-03-14'
     * $query->filterByCreateDate('now'); // WHERE create_date = '2011-03-14'
     * $query->filterByCreateDate(array('max' => 'yesterday')); // WHERE create_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $createDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SemesterQuery The current query, for fluid interface
     */
    public function filterByCreateDate($createDate = null, $comparison = null)
    {
        if (is_array($createDate)) {
            $useMinMax = false;
            if (isset($createDate['min'])) {
                $this->addUsingAlias(SemesterPeer::CREATE_DATE, $createDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createDate['max'])) {
                $this->addUsingAlias(SemesterPeer::CREATE_DATE, $createDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SemesterPeer::CREATE_DATE, $createDate, $comparison);
    }

    /**
     * Filter the query on the last_update column
     *
     * Example usage:
     * <code>
     * $query->filterByLastUpdate('2011-03-14'); // WHERE last_update = '2011-03-14'
     * $query->filterByLastUpdate('now'); // WHERE last_update = '2011-03-14'
     * $query->filterByLastUpdate(array('max' => 'yesterday')); // WHERE last_update > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastUpdate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SemesterQuery The current query, for fluid interface
     */
    public function filterByLastUpdate($lastUpdate = null, $comparison = null)
    {
        if (is_array($lastUpdate)) {
            $useMinMax = false;
            if (isset($lastUpdate['min'])) {
                $this->addUsingAlias(SemesterPeer::LAST_UPDATE, $lastUpdate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastUpdate['max'])) {
                $this->addUsingAlias(SemesterPeer::LAST_UPDATE, $lastUpdate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SemesterPeer::LAST_UPDATE, $lastUpdate, $comparison);
    }

    /**
     * Filter the query on the expired_date column
     *
     * Example usage:
     * <code>
     * $query->filterByExpiredDate('2011-03-14'); // WHERE expired_date = '2011-03-14'
     * $query->filterByExpiredDate('now'); // WHERE expired_date = '2011-03-14'
     * $query->filterByExpiredDate(array('max' => 'yesterday')); // WHERE expired_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $expiredDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SemesterQuery The current query, for fluid interface
     */
    public function filterByExpiredDate($expiredDate = null, $comparison = null)
    {
        if (is_array($expiredDate)) {
            $useMinMax = false;
            if (isset($expiredDate['min'])) {
                $this->addUsingAlias(SemesterPeer::EXPIRED_DATE, $expiredDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($expiredDate['max'])) {
                $this->addUsingAlias(SemesterPeer::EXPIRED_DATE, $expiredDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SemesterPeer::EXPIRED_DATE, $expiredDate, $comparison);
    }

    /**
     * Filter the query on the last_sync column
     *
     * Example usage:
     * <code>
     * $query->filterByLastSync('2011-03-14'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync('now'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync(array('max' => 'yesterday')); // WHERE last_sync > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastSync The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SemesterQuery The current query, for fluid interface
     */
    public function filterByLastSync($lastSync = null, $comparison = null)
    {
        if (is_array($lastSync)) {
            $useMinMax = false;
            if (isset($lastSync['min'])) {
                $this->addUsingAlias(SemesterPeer::LAST_SYNC, $lastSync['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastSync['max'])) {
                $this->addUsingAlias(SemesterPeer::LAST_SYNC, $lastSync['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SemesterPeer::LAST_SYNC, $lastSync, $comparison);
    }

    /**
     * Filter the query by a related TahunAjaran object
     *
     * @param   TahunAjaran|PropelObjectCollection $tahunAjaran The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SemesterQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByTahunAjaran($tahunAjaran, $comparison = null)
    {
        if ($tahunAjaran instanceof TahunAjaran) {
            return $this
                ->addUsingAlias(SemesterPeer::TAHUN_AJARAN_ID, $tahunAjaran->getTahunAjaranId(), $comparison);
        } elseif ($tahunAjaran instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SemesterPeer::TAHUN_AJARAN_ID, $tahunAjaran->toKeyValue('PrimaryKey', 'TahunAjaranId'), $comparison);
        } else {
            throw new PropelException('filterByTahunAjaran() only accepts arguments of type TahunAjaran or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the TahunAjaran relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SemesterQuery The current query, for fluid interface
     */
    public function joinTahunAjaran($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('TahunAjaran');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'TahunAjaran');
        }

        return $this;
    }

    /**
     * Use the TahunAjaran relation TahunAjaran object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\TahunAjaranQuery A secondary query class using the current class as primary query
     */
    public function useTahunAjaranQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTahunAjaran($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'TahunAjaran', '\angulex\Model\TahunAjaranQuery');
    }

    /**
     * Filter the query by a related SekolahLongitudinal object
     *
     * @param   SekolahLongitudinal|PropelObjectCollection $sekolahLongitudinal  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SemesterQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySekolahLongitudinalRelatedBySemesterId($sekolahLongitudinal, $comparison = null)
    {
        if ($sekolahLongitudinal instanceof SekolahLongitudinal) {
            return $this
                ->addUsingAlias(SemesterPeer::SEMESTER_ID, $sekolahLongitudinal->getSemesterId(), $comparison);
        } elseif ($sekolahLongitudinal instanceof PropelObjectCollection) {
            return $this
                ->useSekolahLongitudinalRelatedBySemesterIdQuery()
                ->filterByPrimaryKeys($sekolahLongitudinal->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySekolahLongitudinalRelatedBySemesterId() only accepts arguments of type SekolahLongitudinal or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SekolahLongitudinalRelatedBySemesterId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SemesterQuery The current query, for fluid interface
     */
    public function joinSekolahLongitudinalRelatedBySemesterId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SekolahLongitudinalRelatedBySemesterId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SekolahLongitudinalRelatedBySemesterId');
        }

        return $this;
    }

    /**
     * Use the SekolahLongitudinalRelatedBySemesterId relation SekolahLongitudinal object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SekolahLongitudinalQuery A secondary query class using the current class as primary query
     */
    public function useSekolahLongitudinalRelatedBySemesterIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSekolahLongitudinalRelatedBySemesterId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SekolahLongitudinalRelatedBySemesterId', '\angulex\Model\SekolahLongitudinalQuery');
    }

    /**
     * Filter the query by a related SekolahLongitudinal object
     *
     * @param   SekolahLongitudinal|PropelObjectCollection $sekolahLongitudinal  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SemesterQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySekolahLongitudinalRelatedBySemesterId($sekolahLongitudinal, $comparison = null)
    {
        if ($sekolahLongitudinal instanceof SekolahLongitudinal) {
            return $this
                ->addUsingAlias(SemesterPeer::SEMESTER_ID, $sekolahLongitudinal->getSemesterId(), $comparison);
        } elseif ($sekolahLongitudinal instanceof PropelObjectCollection) {
            return $this
                ->useSekolahLongitudinalRelatedBySemesterIdQuery()
                ->filterByPrimaryKeys($sekolahLongitudinal->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySekolahLongitudinalRelatedBySemesterId() only accepts arguments of type SekolahLongitudinal or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SekolahLongitudinalRelatedBySemesterId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SemesterQuery The current query, for fluid interface
     */
    public function joinSekolahLongitudinalRelatedBySemesterId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SekolahLongitudinalRelatedBySemesterId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SekolahLongitudinalRelatedBySemesterId');
        }

        return $this;
    }

    /**
     * Use the SekolahLongitudinalRelatedBySemesterId relation SekolahLongitudinal object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SekolahLongitudinalQuery A secondary query class using the current class as primary query
     */
    public function useSekolahLongitudinalRelatedBySemesterIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSekolahLongitudinalRelatedBySemesterId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SekolahLongitudinalRelatedBySemesterId', '\angulex\Model\SekolahLongitudinalQuery');
    }

    /**
     * Filter the query by a related PesertaDidikLongitudinal object
     *
     * @param   PesertaDidikLongitudinal|PropelObjectCollection $pesertaDidikLongitudinal  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SemesterQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPesertaDidikLongitudinalRelatedBySemesterId($pesertaDidikLongitudinal, $comparison = null)
    {
        if ($pesertaDidikLongitudinal instanceof PesertaDidikLongitudinal) {
            return $this
                ->addUsingAlias(SemesterPeer::SEMESTER_ID, $pesertaDidikLongitudinal->getSemesterId(), $comparison);
        } elseif ($pesertaDidikLongitudinal instanceof PropelObjectCollection) {
            return $this
                ->usePesertaDidikLongitudinalRelatedBySemesterIdQuery()
                ->filterByPrimaryKeys($pesertaDidikLongitudinal->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPesertaDidikLongitudinalRelatedBySemesterId() only accepts arguments of type PesertaDidikLongitudinal or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PesertaDidikLongitudinalRelatedBySemesterId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SemesterQuery The current query, for fluid interface
     */
    public function joinPesertaDidikLongitudinalRelatedBySemesterId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PesertaDidikLongitudinalRelatedBySemesterId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PesertaDidikLongitudinalRelatedBySemesterId');
        }

        return $this;
    }

    /**
     * Use the PesertaDidikLongitudinalRelatedBySemesterId relation PesertaDidikLongitudinal object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PesertaDidikLongitudinalQuery A secondary query class using the current class as primary query
     */
    public function usePesertaDidikLongitudinalRelatedBySemesterIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPesertaDidikLongitudinalRelatedBySemesterId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PesertaDidikLongitudinalRelatedBySemesterId', '\angulex\Model\PesertaDidikLongitudinalQuery');
    }

    /**
     * Filter the query by a related PesertaDidikLongitudinal object
     *
     * @param   PesertaDidikLongitudinal|PropelObjectCollection $pesertaDidikLongitudinal  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SemesterQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPesertaDidikLongitudinalRelatedBySemesterId($pesertaDidikLongitudinal, $comparison = null)
    {
        if ($pesertaDidikLongitudinal instanceof PesertaDidikLongitudinal) {
            return $this
                ->addUsingAlias(SemesterPeer::SEMESTER_ID, $pesertaDidikLongitudinal->getSemesterId(), $comparison);
        } elseif ($pesertaDidikLongitudinal instanceof PropelObjectCollection) {
            return $this
                ->usePesertaDidikLongitudinalRelatedBySemesterIdQuery()
                ->filterByPrimaryKeys($pesertaDidikLongitudinal->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPesertaDidikLongitudinalRelatedBySemesterId() only accepts arguments of type PesertaDidikLongitudinal or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PesertaDidikLongitudinalRelatedBySemesterId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SemesterQuery The current query, for fluid interface
     */
    public function joinPesertaDidikLongitudinalRelatedBySemesterId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PesertaDidikLongitudinalRelatedBySemesterId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PesertaDidikLongitudinalRelatedBySemesterId');
        }

        return $this;
    }

    /**
     * Use the PesertaDidikLongitudinalRelatedBySemesterId relation PesertaDidikLongitudinal object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PesertaDidikLongitudinalQuery A secondary query class using the current class as primary query
     */
    public function usePesertaDidikLongitudinalRelatedBySemesterIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPesertaDidikLongitudinalRelatedBySemesterId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PesertaDidikLongitudinalRelatedBySemesterId', '\angulex\Model\PesertaDidikLongitudinalQuery');
    }

    /**
     * Filter the query by a related Sanitasi object
     *
     * @param   Sanitasi|PropelObjectCollection $sanitasi  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SemesterQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySanitasiRelatedBySemesterId($sanitasi, $comparison = null)
    {
        if ($sanitasi instanceof Sanitasi) {
            return $this
                ->addUsingAlias(SemesterPeer::SEMESTER_ID, $sanitasi->getSemesterId(), $comparison);
        } elseif ($sanitasi instanceof PropelObjectCollection) {
            return $this
                ->useSanitasiRelatedBySemesterIdQuery()
                ->filterByPrimaryKeys($sanitasi->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySanitasiRelatedBySemesterId() only accepts arguments of type Sanitasi or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SanitasiRelatedBySemesterId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SemesterQuery The current query, for fluid interface
     */
    public function joinSanitasiRelatedBySemesterId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SanitasiRelatedBySemesterId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SanitasiRelatedBySemesterId');
        }

        return $this;
    }

    /**
     * Use the SanitasiRelatedBySemesterId relation Sanitasi object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SanitasiQuery A secondary query class using the current class as primary query
     */
    public function useSanitasiRelatedBySemesterIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSanitasiRelatedBySemesterId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SanitasiRelatedBySemesterId', '\angulex\Model\SanitasiQuery');
    }

    /**
     * Filter the query by a related Sanitasi object
     *
     * @param   Sanitasi|PropelObjectCollection $sanitasi  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SemesterQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySanitasiRelatedBySemesterId($sanitasi, $comparison = null)
    {
        if ($sanitasi instanceof Sanitasi) {
            return $this
                ->addUsingAlias(SemesterPeer::SEMESTER_ID, $sanitasi->getSemesterId(), $comparison);
        } elseif ($sanitasi instanceof PropelObjectCollection) {
            return $this
                ->useSanitasiRelatedBySemesterIdQuery()
                ->filterByPrimaryKeys($sanitasi->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySanitasiRelatedBySemesterId() only accepts arguments of type Sanitasi or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SanitasiRelatedBySemesterId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SemesterQuery The current query, for fluid interface
     */
    public function joinSanitasiRelatedBySemesterId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SanitasiRelatedBySemesterId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SanitasiRelatedBySemesterId');
        }

        return $this;
    }

    /**
     * Use the SanitasiRelatedBySemesterId relation Sanitasi object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SanitasiQuery A secondary query class using the current class as primary query
     */
    public function useSanitasiRelatedBySemesterIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSanitasiRelatedBySemesterId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SanitasiRelatedBySemesterId', '\angulex\Model\SanitasiQuery');
    }

    /**
     * Filter the query by a related SaranaLongitudinal object
     *
     * @param   SaranaLongitudinal|PropelObjectCollection $saranaLongitudinal  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SemesterQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySaranaLongitudinalRelatedBySemesterId($saranaLongitudinal, $comparison = null)
    {
        if ($saranaLongitudinal instanceof SaranaLongitudinal) {
            return $this
                ->addUsingAlias(SemesterPeer::SEMESTER_ID, $saranaLongitudinal->getSemesterId(), $comparison);
        } elseif ($saranaLongitudinal instanceof PropelObjectCollection) {
            return $this
                ->useSaranaLongitudinalRelatedBySemesterIdQuery()
                ->filterByPrimaryKeys($saranaLongitudinal->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySaranaLongitudinalRelatedBySemesterId() only accepts arguments of type SaranaLongitudinal or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SaranaLongitudinalRelatedBySemesterId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SemesterQuery The current query, for fluid interface
     */
    public function joinSaranaLongitudinalRelatedBySemesterId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SaranaLongitudinalRelatedBySemesterId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SaranaLongitudinalRelatedBySemesterId');
        }

        return $this;
    }

    /**
     * Use the SaranaLongitudinalRelatedBySemesterId relation SaranaLongitudinal object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SaranaLongitudinalQuery A secondary query class using the current class as primary query
     */
    public function useSaranaLongitudinalRelatedBySemesterIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSaranaLongitudinalRelatedBySemesterId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SaranaLongitudinalRelatedBySemesterId', '\angulex\Model\SaranaLongitudinalQuery');
    }

    /**
     * Filter the query by a related SaranaLongitudinal object
     *
     * @param   SaranaLongitudinal|PropelObjectCollection $saranaLongitudinal  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SemesterQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySaranaLongitudinalRelatedBySemesterId($saranaLongitudinal, $comparison = null)
    {
        if ($saranaLongitudinal instanceof SaranaLongitudinal) {
            return $this
                ->addUsingAlias(SemesterPeer::SEMESTER_ID, $saranaLongitudinal->getSemesterId(), $comparison);
        } elseif ($saranaLongitudinal instanceof PropelObjectCollection) {
            return $this
                ->useSaranaLongitudinalRelatedBySemesterIdQuery()
                ->filterByPrimaryKeys($saranaLongitudinal->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySaranaLongitudinalRelatedBySemesterId() only accepts arguments of type SaranaLongitudinal or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SaranaLongitudinalRelatedBySemesterId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SemesterQuery The current query, for fluid interface
     */
    public function joinSaranaLongitudinalRelatedBySemesterId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SaranaLongitudinalRelatedBySemesterId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SaranaLongitudinalRelatedBySemesterId');
        }

        return $this;
    }

    /**
     * Use the SaranaLongitudinalRelatedBySemesterId relation SaranaLongitudinal object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SaranaLongitudinalQuery A secondary query class using the current class as primary query
     */
    public function useSaranaLongitudinalRelatedBySemesterIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSaranaLongitudinalRelatedBySemesterId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SaranaLongitudinalRelatedBySemesterId', '\angulex\Model\SaranaLongitudinalQuery');
    }

    /**
     * Filter the query by a related BatasWaktuRapor object
     *
     * @param   BatasWaktuRapor|PropelObjectCollection $batasWaktuRapor  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SemesterQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByBatasWaktuRapor($batasWaktuRapor, $comparison = null)
    {
        if ($batasWaktuRapor instanceof BatasWaktuRapor) {
            return $this
                ->addUsingAlias(SemesterPeer::SEMESTER_ID, $batasWaktuRapor->getSemesterId(), $comparison);
        } elseif ($batasWaktuRapor instanceof PropelObjectCollection) {
            return $this
                ->useBatasWaktuRaporQuery()
                ->filterByPrimaryKeys($batasWaktuRapor->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBatasWaktuRapor() only accepts arguments of type BatasWaktuRapor or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BatasWaktuRapor relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SemesterQuery The current query, for fluid interface
     */
    public function joinBatasWaktuRapor($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BatasWaktuRapor');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BatasWaktuRapor');
        }

        return $this;
    }

    /**
     * Use the BatasWaktuRapor relation BatasWaktuRapor object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\BatasWaktuRaporQuery A secondary query class using the current class as primary query
     */
    public function useBatasWaktuRaporQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBatasWaktuRapor($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BatasWaktuRapor', '\angulex\Model\BatasWaktuRaporQuery');
    }

    /**
     * Filter the query by a related Pembelajaran object
     *
     * @param   Pembelajaran|PropelObjectCollection $pembelajaran  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SemesterQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPembelajaranRelatedBySemesterId($pembelajaran, $comparison = null)
    {
        if ($pembelajaran instanceof Pembelajaran) {
            return $this
                ->addUsingAlias(SemesterPeer::SEMESTER_ID, $pembelajaran->getSemesterId(), $comparison);
        } elseif ($pembelajaran instanceof PropelObjectCollection) {
            return $this
                ->usePembelajaranRelatedBySemesterIdQuery()
                ->filterByPrimaryKeys($pembelajaran->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPembelajaranRelatedBySemesterId() only accepts arguments of type Pembelajaran or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PembelajaranRelatedBySemesterId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SemesterQuery The current query, for fluid interface
     */
    public function joinPembelajaranRelatedBySemesterId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PembelajaranRelatedBySemesterId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PembelajaranRelatedBySemesterId');
        }

        return $this;
    }

    /**
     * Use the PembelajaranRelatedBySemesterId relation Pembelajaran object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PembelajaranQuery A secondary query class using the current class as primary query
     */
    public function usePembelajaranRelatedBySemesterIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPembelajaranRelatedBySemesterId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PembelajaranRelatedBySemesterId', '\angulex\Model\PembelajaranQuery');
    }

    /**
     * Filter the query by a related Pembelajaran object
     *
     * @param   Pembelajaran|PropelObjectCollection $pembelajaran  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SemesterQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPembelajaranRelatedBySemesterId($pembelajaran, $comparison = null)
    {
        if ($pembelajaran instanceof Pembelajaran) {
            return $this
                ->addUsingAlias(SemesterPeer::SEMESTER_ID, $pembelajaran->getSemesterId(), $comparison);
        } elseif ($pembelajaran instanceof PropelObjectCollection) {
            return $this
                ->usePembelajaranRelatedBySemesterIdQuery()
                ->filterByPrimaryKeys($pembelajaran->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPembelajaranRelatedBySemesterId() only accepts arguments of type Pembelajaran or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PembelajaranRelatedBySemesterId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SemesterQuery The current query, for fluid interface
     */
    public function joinPembelajaranRelatedBySemesterId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PembelajaranRelatedBySemesterId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PembelajaranRelatedBySemesterId');
        }

        return $this;
    }

    /**
     * Use the PembelajaranRelatedBySemesterId relation Pembelajaran object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PembelajaranQuery A secondary query class using the current class as primary query
     */
    public function usePembelajaranRelatedBySemesterIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPembelajaranRelatedBySemesterId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PembelajaranRelatedBySemesterId', '\angulex\Model\PembelajaranQuery');
    }

    /**
     * Filter the query by a related PrasaranaLongitudinal object
     *
     * @param   PrasaranaLongitudinal|PropelObjectCollection $prasaranaLongitudinal  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SemesterQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPrasaranaLongitudinalRelatedBySemesterId($prasaranaLongitudinal, $comparison = null)
    {
        if ($prasaranaLongitudinal instanceof PrasaranaLongitudinal) {
            return $this
                ->addUsingAlias(SemesterPeer::SEMESTER_ID, $prasaranaLongitudinal->getSemesterId(), $comparison);
        } elseif ($prasaranaLongitudinal instanceof PropelObjectCollection) {
            return $this
                ->usePrasaranaLongitudinalRelatedBySemesterIdQuery()
                ->filterByPrimaryKeys($prasaranaLongitudinal->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPrasaranaLongitudinalRelatedBySemesterId() only accepts arguments of type PrasaranaLongitudinal or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PrasaranaLongitudinalRelatedBySemesterId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SemesterQuery The current query, for fluid interface
     */
    public function joinPrasaranaLongitudinalRelatedBySemesterId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PrasaranaLongitudinalRelatedBySemesterId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PrasaranaLongitudinalRelatedBySemesterId');
        }

        return $this;
    }

    /**
     * Use the PrasaranaLongitudinalRelatedBySemesterId relation PrasaranaLongitudinal object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PrasaranaLongitudinalQuery A secondary query class using the current class as primary query
     */
    public function usePrasaranaLongitudinalRelatedBySemesterIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPrasaranaLongitudinalRelatedBySemesterId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PrasaranaLongitudinalRelatedBySemesterId', '\angulex\Model\PrasaranaLongitudinalQuery');
    }

    /**
     * Filter the query by a related PrasaranaLongitudinal object
     *
     * @param   PrasaranaLongitudinal|PropelObjectCollection $prasaranaLongitudinal  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SemesterQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPrasaranaLongitudinalRelatedBySemesterId($prasaranaLongitudinal, $comparison = null)
    {
        if ($prasaranaLongitudinal instanceof PrasaranaLongitudinal) {
            return $this
                ->addUsingAlias(SemesterPeer::SEMESTER_ID, $prasaranaLongitudinal->getSemesterId(), $comparison);
        } elseif ($prasaranaLongitudinal instanceof PropelObjectCollection) {
            return $this
                ->usePrasaranaLongitudinalRelatedBySemesterIdQuery()
                ->filterByPrimaryKeys($prasaranaLongitudinal->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPrasaranaLongitudinalRelatedBySemesterId() only accepts arguments of type PrasaranaLongitudinal or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PrasaranaLongitudinalRelatedBySemesterId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SemesterQuery The current query, for fluid interface
     */
    public function joinPrasaranaLongitudinalRelatedBySemesterId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PrasaranaLongitudinalRelatedBySemesterId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PrasaranaLongitudinalRelatedBySemesterId');
        }

        return $this;
    }

    /**
     * Use the PrasaranaLongitudinalRelatedBySemesterId relation PrasaranaLongitudinal object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PrasaranaLongitudinalQuery A secondary query class using the current class as primary query
     */
    public function usePrasaranaLongitudinalRelatedBySemesterIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPrasaranaLongitudinalRelatedBySemesterId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PrasaranaLongitudinalRelatedBySemesterId', '\angulex\Model\PrasaranaLongitudinalQuery');
    }

    /**
     * Filter the query by a related RombonganBelajar object
     *
     * @param   RombonganBelajar|PropelObjectCollection $rombonganBelajar  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SemesterQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRombonganBelajarRelatedBySemesterId($rombonganBelajar, $comparison = null)
    {
        if ($rombonganBelajar instanceof RombonganBelajar) {
            return $this
                ->addUsingAlias(SemesterPeer::SEMESTER_ID, $rombonganBelajar->getSemesterId(), $comparison);
        } elseif ($rombonganBelajar instanceof PropelObjectCollection) {
            return $this
                ->useRombonganBelajarRelatedBySemesterIdQuery()
                ->filterByPrimaryKeys($rombonganBelajar->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRombonganBelajarRelatedBySemesterId() only accepts arguments of type RombonganBelajar or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RombonganBelajarRelatedBySemesterId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SemesterQuery The current query, for fluid interface
     */
    public function joinRombonganBelajarRelatedBySemesterId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RombonganBelajarRelatedBySemesterId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RombonganBelajarRelatedBySemesterId');
        }

        return $this;
    }

    /**
     * Use the RombonganBelajarRelatedBySemesterId relation RombonganBelajar object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\RombonganBelajarQuery A secondary query class using the current class as primary query
     */
    public function useRombonganBelajarRelatedBySemesterIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRombonganBelajarRelatedBySemesterId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RombonganBelajarRelatedBySemesterId', '\angulex\Model\RombonganBelajarQuery');
    }

    /**
     * Filter the query by a related RombonganBelajar object
     *
     * @param   RombonganBelajar|PropelObjectCollection $rombonganBelajar  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SemesterQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRombonganBelajarRelatedBySemesterId($rombonganBelajar, $comparison = null)
    {
        if ($rombonganBelajar instanceof RombonganBelajar) {
            return $this
                ->addUsingAlias(SemesterPeer::SEMESTER_ID, $rombonganBelajar->getSemesterId(), $comparison);
        } elseif ($rombonganBelajar instanceof PropelObjectCollection) {
            return $this
                ->useRombonganBelajarRelatedBySemesterIdQuery()
                ->filterByPrimaryKeys($rombonganBelajar->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRombonganBelajarRelatedBySemesterId() only accepts arguments of type RombonganBelajar or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RombonganBelajarRelatedBySemesterId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SemesterQuery The current query, for fluid interface
     */
    public function joinRombonganBelajarRelatedBySemesterId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RombonganBelajarRelatedBySemesterId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RombonganBelajarRelatedBySemesterId');
        }

        return $this;
    }

    /**
     * Use the RombonganBelajarRelatedBySemesterId relation RombonganBelajar object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\RombonganBelajarQuery A secondary query class using the current class as primary query
     */
    public function useRombonganBelajarRelatedBySemesterIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRombonganBelajarRelatedBySemesterId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RombonganBelajarRelatedBySemesterId', '\angulex\Model\RombonganBelajarQuery');
    }

    /**
     * Filter the query by a related BukuAlatLongitudinal object
     *
     * @param   BukuAlatLongitudinal|PropelObjectCollection $bukuAlatLongitudinal  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SemesterQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByBukuAlatLongitudinalRelatedBySemesterId($bukuAlatLongitudinal, $comparison = null)
    {
        if ($bukuAlatLongitudinal instanceof BukuAlatLongitudinal) {
            return $this
                ->addUsingAlias(SemesterPeer::SEMESTER_ID, $bukuAlatLongitudinal->getSemesterId(), $comparison);
        } elseif ($bukuAlatLongitudinal instanceof PropelObjectCollection) {
            return $this
                ->useBukuAlatLongitudinalRelatedBySemesterIdQuery()
                ->filterByPrimaryKeys($bukuAlatLongitudinal->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBukuAlatLongitudinalRelatedBySemesterId() only accepts arguments of type BukuAlatLongitudinal or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BukuAlatLongitudinalRelatedBySemesterId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SemesterQuery The current query, for fluid interface
     */
    public function joinBukuAlatLongitudinalRelatedBySemesterId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BukuAlatLongitudinalRelatedBySemesterId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BukuAlatLongitudinalRelatedBySemesterId');
        }

        return $this;
    }

    /**
     * Use the BukuAlatLongitudinalRelatedBySemesterId relation BukuAlatLongitudinal object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\BukuAlatLongitudinalQuery A secondary query class using the current class as primary query
     */
    public function useBukuAlatLongitudinalRelatedBySemesterIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBukuAlatLongitudinalRelatedBySemesterId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BukuAlatLongitudinalRelatedBySemesterId', '\angulex\Model\BukuAlatLongitudinalQuery');
    }

    /**
     * Filter the query by a related BukuAlatLongitudinal object
     *
     * @param   BukuAlatLongitudinal|PropelObjectCollection $bukuAlatLongitudinal  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SemesterQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByBukuAlatLongitudinalRelatedBySemesterId($bukuAlatLongitudinal, $comparison = null)
    {
        if ($bukuAlatLongitudinal instanceof BukuAlatLongitudinal) {
            return $this
                ->addUsingAlias(SemesterPeer::SEMESTER_ID, $bukuAlatLongitudinal->getSemesterId(), $comparison);
        } elseif ($bukuAlatLongitudinal instanceof PropelObjectCollection) {
            return $this
                ->useBukuAlatLongitudinalRelatedBySemesterIdQuery()
                ->filterByPrimaryKeys($bukuAlatLongitudinal->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBukuAlatLongitudinalRelatedBySemesterId() only accepts arguments of type BukuAlatLongitudinal or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BukuAlatLongitudinalRelatedBySemesterId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SemesterQuery The current query, for fluid interface
     */
    public function joinBukuAlatLongitudinalRelatedBySemesterId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BukuAlatLongitudinalRelatedBySemesterId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BukuAlatLongitudinalRelatedBySemesterId');
        }

        return $this;
    }

    /**
     * Use the BukuAlatLongitudinalRelatedBySemesterId relation BukuAlatLongitudinal object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\BukuAlatLongitudinalQuery A secondary query class using the current class as primary query
     */
    public function useBukuAlatLongitudinalRelatedBySemesterIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBukuAlatLongitudinalRelatedBySemesterId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BukuAlatLongitudinalRelatedBySemesterId', '\angulex\Model\BukuAlatLongitudinalQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Semester $semester Object to remove from the list of results
     *
     * @return SemesterQuery The current query, for fluid interface
     */
    public function prune($semester = null)
    {
        if ($semester) {
            $this->addUsingAlias(SemesterPeer::SEMESTER_ID, $semester->getSemesterId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
