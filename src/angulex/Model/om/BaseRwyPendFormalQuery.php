<?php

namespace angulex\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\BidangStudi;
use angulex\Model\GelarAkademik;
use angulex\Model\JenjangPendidikan;
use angulex\Model\Ptk;
use angulex\Model\RwyPendFormal;
use angulex\Model\RwyPendFormalPeer;
use angulex\Model\RwyPendFormalQuery;
use angulex\Model\VldRwyPendFormal;

/**
 * Base class that represents a query for the 'rwy_pend_formal' table.
 *
 * 
 *
 * @method RwyPendFormalQuery orderByRiwayatPendidikanFormalId($order = Criteria::ASC) Order by the riwayat_pendidikan_formal_id column
 * @method RwyPendFormalQuery orderByPtkId($order = Criteria::ASC) Order by the ptk_id column
 * @method RwyPendFormalQuery orderByBidangStudiId($order = Criteria::ASC) Order by the bidang_studi_id column
 * @method RwyPendFormalQuery orderByJenjangPendidikanId($order = Criteria::ASC) Order by the jenjang_pendidikan_id column
 * @method RwyPendFormalQuery orderByGelarAkademikId($order = Criteria::ASC) Order by the gelar_akademik_id column
 * @method RwyPendFormalQuery orderBySatuanPendidikanFormal($order = Criteria::ASC) Order by the satuan_pendidikan_formal column
 * @method RwyPendFormalQuery orderByFakultas($order = Criteria::ASC) Order by the fakultas column
 * @method RwyPendFormalQuery orderByKependidikan($order = Criteria::ASC) Order by the kependidikan column
 * @method RwyPendFormalQuery orderByTahunMasuk($order = Criteria::ASC) Order by the tahun_masuk column
 * @method RwyPendFormalQuery orderByTahunLulus($order = Criteria::ASC) Order by the tahun_lulus column
 * @method RwyPendFormalQuery orderByNim($order = Criteria::ASC) Order by the nim column
 * @method RwyPendFormalQuery orderByStatusKuliah($order = Criteria::ASC) Order by the status_kuliah column
 * @method RwyPendFormalQuery orderBySemester($order = Criteria::ASC) Order by the semester column
 * @method RwyPendFormalQuery orderByIpk($order = Criteria::ASC) Order by the ipk column
 * @method RwyPendFormalQuery orderByLastUpdate($order = Criteria::ASC) Order by the Last_update column
 * @method RwyPendFormalQuery orderBySoftDelete($order = Criteria::ASC) Order by the Soft_delete column
 * @method RwyPendFormalQuery orderByLastSync($order = Criteria::ASC) Order by the last_sync column
 * @method RwyPendFormalQuery orderByUpdaterId($order = Criteria::ASC) Order by the Updater_ID column
 *
 * @method RwyPendFormalQuery groupByRiwayatPendidikanFormalId() Group by the riwayat_pendidikan_formal_id column
 * @method RwyPendFormalQuery groupByPtkId() Group by the ptk_id column
 * @method RwyPendFormalQuery groupByBidangStudiId() Group by the bidang_studi_id column
 * @method RwyPendFormalQuery groupByJenjangPendidikanId() Group by the jenjang_pendidikan_id column
 * @method RwyPendFormalQuery groupByGelarAkademikId() Group by the gelar_akademik_id column
 * @method RwyPendFormalQuery groupBySatuanPendidikanFormal() Group by the satuan_pendidikan_formal column
 * @method RwyPendFormalQuery groupByFakultas() Group by the fakultas column
 * @method RwyPendFormalQuery groupByKependidikan() Group by the kependidikan column
 * @method RwyPendFormalQuery groupByTahunMasuk() Group by the tahun_masuk column
 * @method RwyPendFormalQuery groupByTahunLulus() Group by the tahun_lulus column
 * @method RwyPendFormalQuery groupByNim() Group by the nim column
 * @method RwyPendFormalQuery groupByStatusKuliah() Group by the status_kuliah column
 * @method RwyPendFormalQuery groupBySemester() Group by the semester column
 * @method RwyPendFormalQuery groupByIpk() Group by the ipk column
 * @method RwyPendFormalQuery groupByLastUpdate() Group by the Last_update column
 * @method RwyPendFormalQuery groupBySoftDelete() Group by the Soft_delete column
 * @method RwyPendFormalQuery groupByLastSync() Group by the last_sync column
 * @method RwyPendFormalQuery groupByUpdaterId() Group by the Updater_ID column
 *
 * @method RwyPendFormalQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method RwyPendFormalQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method RwyPendFormalQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method RwyPendFormalQuery leftJoinPtkRelatedByPtkId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PtkRelatedByPtkId relation
 * @method RwyPendFormalQuery rightJoinPtkRelatedByPtkId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PtkRelatedByPtkId relation
 * @method RwyPendFormalQuery innerJoinPtkRelatedByPtkId($relationAlias = null) Adds a INNER JOIN clause to the query using the PtkRelatedByPtkId relation
 *
 * @method RwyPendFormalQuery leftJoinPtkRelatedByPtkId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PtkRelatedByPtkId relation
 * @method RwyPendFormalQuery rightJoinPtkRelatedByPtkId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PtkRelatedByPtkId relation
 * @method RwyPendFormalQuery innerJoinPtkRelatedByPtkId($relationAlias = null) Adds a INNER JOIN clause to the query using the PtkRelatedByPtkId relation
 *
 * @method RwyPendFormalQuery leftJoinBidangStudiRelatedByBidangStudiId($relationAlias = null) Adds a LEFT JOIN clause to the query using the BidangStudiRelatedByBidangStudiId relation
 * @method RwyPendFormalQuery rightJoinBidangStudiRelatedByBidangStudiId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BidangStudiRelatedByBidangStudiId relation
 * @method RwyPendFormalQuery innerJoinBidangStudiRelatedByBidangStudiId($relationAlias = null) Adds a INNER JOIN clause to the query using the BidangStudiRelatedByBidangStudiId relation
 *
 * @method RwyPendFormalQuery leftJoinBidangStudiRelatedByBidangStudiId($relationAlias = null) Adds a LEFT JOIN clause to the query using the BidangStudiRelatedByBidangStudiId relation
 * @method RwyPendFormalQuery rightJoinBidangStudiRelatedByBidangStudiId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BidangStudiRelatedByBidangStudiId relation
 * @method RwyPendFormalQuery innerJoinBidangStudiRelatedByBidangStudiId($relationAlias = null) Adds a INNER JOIN clause to the query using the BidangStudiRelatedByBidangStudiId relation
 *
 * @method RwyPendFormalQuery leftJoinGelarAkademikRelatedByGelarAkademikId($relationAlias = null) Adds a LEFT JOIN clause to the query using the GelarAkademikRelatedByGelarAkademikId relation
 * @method RwyPendFormalQuery rightJoinGelarAkademikRelatedByGelarAkademikId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the GelarAkademikRelatedByGelarAkademikId relation
 * @method RwyPendFormalQuery innerJoinGelarAkademikRelatedByGelarAkademikId($relationAlias = null) Adds a INNER JOIN clause to the query using the GelarAkademikRelatedByGelarAkademikId relation
 *
 * @method RwyPendFormalQuery leftJoinGelarAkademikRelatedByGelarAkademikId($relationAlias = null) Adds a LEFT JOIN clause to the query using the GelarAkademikRelatedByGelarAkademikId relation
 * @method RwyPendFormalQuery rightJoinGelarAkademikRelatedByGelarAkademikId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the GelarAkademikRelatedByGelarAkademikId relation
 * @method RwyPendFormalQuery innerJoinGelarAkademikRelatedByGelarAkademikId($relationAlias = null) Adds a INNER JOIN clause to the query using the GelarAkademikRelatedByGelarAkademikId relation
 *
 * @method RwyPendFormalQuery leftJoinJenjangPendidikanRelatedByJenjangPendidikanId($relationAlias = null) Adds a LEFT JOIN clause to the query using the JenjangPendidikanRelatedByJenjangPendidikanId relation
 * @method RwyPendFormalQuery rightJoinJenjangPendidikanRelatedByJenjangPendidikanId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JenjangPendidikanRelatedByJenjangPendidikanId relation
 * @method RwyPendFormalQuery innerJoinJenjangPendidikanRelatedByJenjangPendidikanId($relationAlias = null) Adds a INNER JOIN clause to the query using the JenjangPendidikanRelatedByJenjangPendidikanId relation
 *
 * @method RwyPendFormalQuery leftJoinJenjangPendidikanRelatedByJenjangPendidikanId($relationAlias = null) Adds a LEFT JOIN clause to the query using the JenjangPendidikanRelatedByJenjangPendidikanId relation
 * @method RwyPendFormalQuery rightJoinJenjangPendidikanRelatedByJenjangPendidikanId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JenjangPendidikanRelatedByJenjangPendidikanId relation
 * @method RwyPendFormalQuery innerJoinJenjangPendidikanRelatedByJenjangPendidikanId($relationAlias = null) Adds a INNER JOIN clause to the query using the JenjangPendidikanRelatedByJenjangPendidikanId relation
 *
 * @method RwyPendFormalQuery leftJoinVldRwyPendFormalRelatedByRiwayatPendidikanFormalId($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldRwyPendFormalRelatedByRiwayatPendidikanFormalId relation
 * @method RwyPendFormalQuery rightJoinVldRwyPendFormalRelatedByRiwayatPendidikanFormalId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldRwyPendFormalRelatedByRiwayatPendidikanFormalId relation
 * @method RwyPendFormalQuery innerJoinVldRwyPendFormalRelatedByRiwayatPendidikanFormalId($relationAlias = null) Adds a INNER JOIN clause to the query using the VldRwyPendFormalRelatedByRiwayatPendidikanFormalId relation
 *
 * @method RwyPendFormalQuery leftJoinVldRwyPendFormalRelatedByRiwayatPendidikanFormalId($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldRwyPendFormalRelatedByRiwayatPendidikanFormalId relation
 * @method RwyPendFormalQuery rightJoinVldRwyPendFormalRelatedByRiwayatPendidikanFormalId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldRwyPendFormalRelatedByRiwayatPendidikanFormalId relation
 * @method RwyPendFormalQuery innerJoinVldRwyPendFormalRelatedByRiwayatPendidikanFormalId($relationAlias = null) Adds a INNER JOIN clause to the query using the VldRwyPendFormalRelatedByRiwayatPendidikanFormalId relation
 *
 * @method RwyPendFormal findOne(PropelPDO $con = null) Return the first RwyPendFormal matching the query
 * @method RwyPendFormal findOneOrCreate(PropelPDO $con = null) Return the first RwyPendFormal matching the query, or a new RwyPendFormal object populated from the query conditions when no match is found
 *
 * @method RwyPendFormal findOneByPtkId(string $ptk_id) Return the first RwyPendFormal filtered by the ptk_id column
 * @method RwyPendFormal findOneByBidangStudiId(int $bidang_studi_id) Return the first RwyPendFormal filtered by the bidang_studi_id column
 * @method RwyPendFormal findOneByJenjangPendidikanId(string $jenjang_pendidikan_id) Return the first RwyPendFormal filtered by the jenjang_pendidikan_id column
 * @method RwyPendFormal findOneByGelarAkademikId(int $gelar_akademik_id) Return the first RwyPendFormal filtered by the gelar_akademik_id column
 * @method RwyPendFormal findOneBySatuanPendidikanFormal(string $satuan_pendidikan_formal) Return the first RwyPendFormal filtered by the satuan_pendidikan_formal column
 * @method RwyPendFormal findOneByFakultas(string $fakultas) Return the first RwyPendFormal filtered by the fakultas column
 * @method RwyPendFormal findOneByKependidikan(string $kependidikan) Return the first RwyPendFormal filtered by the kependidikan column
 * @method RwyPendFormal findOneByTahunMasuk(string $tahun_masuk) Return the first RwyPendFormal filtered by the tahun_masuk column
 * @method RwyPendFormal findOneByTahunLulus(string $tahun_lulus) Return the first RwyPendFormal filtered by the tahun_lulus column
 * @method RwyPendFormal findOneByNim(string $nim) Return the first RwyPendFormal filtered by the nim column
 * @method RwyPendFormal findOneByStatusKuliah(string $status_kuliah) Return the first RwyPendFormal filtered by the status_kuliah column
 * @method RwyPendFormal findOneBySemester(string $semester) Return the first RwyPendFormal filtered by the semester column
 * @method RwyPendFormal findOneByIpk(string $ipk) Return the first RwyPendFormal filtered by the ipk column
 * @method RwyPendFormal findOneByLastUpdate(string $Last_update) Return the first RwyPendFormal filtered by the Last_update column
 * @method RwyPendFormal findOneBySoftDelete(string $Soft_delete) Return the first RwyPendFormal filtered by the Soft_delete column
 * @method RwyPendFormal findOneByLastSync(string $last_sync) Return the first RwyPendFormal filtered by the last_sync column
 * @method RwyPendFormal findOneByUpdaterId(string $Updater_ID) Return the first RwyPendFormal filtered by the Updater_ID column
 *
 * @method array findByRiwayatPendidikanFormalId(string $riwayat_pendidikan_formal_id) Return RwyPendFormal objects filtered by the riwayat_pendidikan_formal_id column
 * @method array findByPtkId(string $ptk_id) Return RwyPendFormal objects filtered by the ptk_id column
 * @method array findByBidangStudiId(int $bidang_studi_id) Return RwyPendFormal objects filtered by the bidang_studi_id column
 * @method array findByJenjangPendidikanId(string $jenjang_pendidikan_id) Return RwyPendFormal objects filtered by the jenjang_pendidikan_id column
 * @method array findByGelarAkademikId(int $gelar_akademik_id) Return RwyPendFormal objects filtered by the gelar_akademik_id column
 * @method array findBySatuanPendidikanFormal(string $satuan_pendidikan_formal) Return RwyPendFormal objects filtered by the satuan_pendidikan_formal column
 * @method array findByFakultas(string $fakultas) Return RwyPendFormal objects filtered by the fakultas column
 * @method array findByKependidikan(string $kependidikan) Return RwyPendFormal objects filtered by the kependidikan column
 * @method array findByTahunMasuk(string $tahun_masuk) Return RwyPendFormal objects filtered by the tahun_masuk column
 * @method array findByTahunLulus(string $tahun_lulus) Return RwyPendFormal objects filtered by the tahun_lulus column
 * @method array findByNim(string $nim) Return RwyPendFormal objects filtered by the nim column
 * @method array findByStatusKuliah(string $status_kuliah) Return RwyPendFormal objects filtered by the status_kuliah column
 * @method array findBySemester(string $semester) Return RwyPendFormal objects filtered by the semester column
 * @method array findByIpk(string $ipk) Return RwyPendFormal objects filtered by the ipk column
 * @method array findByLastUpdate(string $Last_update) Return RwyPendFormal objects filtered by the Last_update column
 * @method array findBySoftDelete(string $Soft_delete) Return RwyPendFormal objects filtered by the Soft_delete column
 * @method array findByLastSync(string $last_sync) Return RwyPendFormal objects filtered by the last_sync column
 * @method array findByUpdaterId(string $Updater_ID) Return RwyPendFormal objects filtered by the Updater_ID column
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseRwyPendFormalQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseRwyPendFormalQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Dapodikmen', $modelName = 'angulex\\Model\\RwyPendFormal', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new RwyPendFormalQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   RwyPendFormalQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return RwyPendFormalQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof RwyPendFormalQuery) {
            return $criteria;
        }
        $query = new RwyPendFormalQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   RwyPendFormal|RwyPendFormal[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = RwyPendFormalPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(RwyPendFormalPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 RwyPendFormal A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByRiwayatPendidikanFormalId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 RwyPendFormal A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [riwayat_pendidikan_formal_id], [ptk_id], [bidang_studi_id], [jenjang_pendidikan_id], [gelar_akademik_id], [satuan_pendidikan_formal], [fakultas], [kependidikan], [tahun_masuk], [tahun_lulus], [nim], [status_kuliah], [semester], [ipk], [Last_update], [Soft_delete], [last_sync], [Updater_ID] FROM [rwy_pend_formal] WHERE [riwayat_pendidikan_formal_id] = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new RwyPendFormal();
            $obj->hydrate($row);
            RwyPendFormalPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return RwyPendFormal|RwyPendFormal[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|RwyPendFormal[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return RwyPendFormalQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(RwyPendFormalPeer::RIWAYAT_PENDIDIKAN_FORMAL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return RwyPendFormalQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(RwyPendFormalPeer::RIWAYAT_PENDIDIKAN_FORMAL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the riwayat_pendidikan_formal_id column
     *
     * Example usage:
     * <code>
     * $query->filterByRiwayatPendidikanFormalId('fooValue');   // WHERE riwayat_pendidikan_formal_id = 'fooValue'
     * $query->filterByRiwayatPendidikanFormalId('%fooValue%'); // WHERE riwayat_pendidikan_formal_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $riwayatPendidikanFormalId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyPendFormalQuery The current query, for fluid interface
     */
    public function filterByRiwayatPendidikanFormalId($riwayatPendidikanFormalId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($riwayatPendidikanFormalId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $riwayatPendidikanFormalId)) {
                $riwayatPendidikanFormalId = str_replace('*', '%', $riwayatPendidikanFormalId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RwyPendFormalPeer::RIWAYAT_PENDIDIKAN_FORMAL_ID, $riwayatPendidikanFormalId, $comparison);
    }

    /**
     * Filter the query on the ptk_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPtkId('fooValue');   // WHERE ptk_id = 'fooValue'
     * $query->filterByPtkId('%fooValue%'); // WHERE ptk_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ptkId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyPendFormalQuery The current query, for fluid interface
     */
    public function filterByPtkId($ptkId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ptkId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ptkId)) {
                $ptkId = str_replace('*', '%', $ptkId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RwyPendFormalPeer::PTK_ID, $ptkId, $comparison);
    }

    /**
     * Filter the query on the bidang_studi_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBidangStudiId(1234); // WHERE bidang_studi_id = 1234
     * $query->filterByBidangStudiId(array(12, 34)); // WHERE bidang_studi_id IN (12, 34)
     * $query->filterByBidangStudiId(array('min' => 12)); // WHERE bidang_studi_id >= 12
     * $query->filterByBidangStudiId(array('max' => 12)); // WHERE bidang_studi_id <= 12
     * </code>
     *
     * @see       filterByBidangStudiRelatedByBidangStudiId()
     *
     * @see       filterByBidangStudiRelatedByBidangStudiId()
     *
     * @param     mixed $bidangStudiId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyPendFormalQuery The current query, for fluid interface
     */
    public function filterByBidangStudiId($bidangStudiId = null, $comparison = null)
    {
        if (is_array($bidangStudiId)) {
            $useMinMax = false;
            if (isset($bidangStudiId['min'])) {
                $this->addUsingAlias(RwyPendFormalPeer::BIDANG_STUDI_ID, $bidangStudiId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($bidangStudiId['max'])) {
                $this->addUsingAlias(RwyPendFormalPeer::BIDANG_STUDI_ID, $bidangStudiId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RwyPendFormalPeer::BIDANG_STUDI_ID, $bidangStudiId, $comparison);
    }

    /**
     * Filter the query on the jenjang_pendidikan_id column
     *
     * Example usage:
     * <code>
     * $query->filterByJenjangPendidikanId(1234); // WHERE jenjang_pendidikan_id = 1234
     * $query->filterByJenjangPendidikanId(array(12, 34)); // WHERE jenjang_pendidikan_id IN (12, 34)
     * $query->filterByJenjangPendidikanId(array('min' => 12)); // WHERE jenjang_pendidikan_id >= 12
     * $query->filterByJenjangPendidikanId(array('max' => 12)); // WHERE jenjang_pendidikan_id <= 12
     * </code>
     *
     * @see       filterByJenjangPendidikanRelatedByJenjangPendidikanId()
     *
     * @see       filterByJenjangPendidikanRelatedByJenjangPendidikanId()
     *
     * @param     mixed $jenjangPendidikanId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyPendFormalQuery The current query, for fluid interface
     */
    public function filterByJenjangPendidikanId($jenjangPendidikanId = null, $comparison = null)
    {
        if (is_array($jenjangPendidikanId)) {
            $useMinMax = false;
            if (isset($jenjangPendidikanId['min'])) {
                $this->addUsingAlias(RwyPendFormalPeer::JENJANG_PENDIDIKAN_ID, $jenjangPendidikanId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jenjangPendidikanId['max'])) {
                $this->addUsingAlias(RwyPendFormalPeer::JENJANG_PENDIDIKAN_ID, $jenjangPendidikanId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RwyPendFormalPeer::JENJANG_PENDIDIKAN_ID, $jenjangPendidikanId, $comparison);
    }

    /**
     * Filter the query on the gelar_akademik_id column
     *
     * Example usage:
     * <code>
     * $query->filterByGelarAkademikId(1234); // WHERE gelar_akademik_id = 1234
     * $query->filterByGelarAkademikId(array(12, 34)); // WHERE gelar_akademik_id IN (12, 34)
     * $query->filterByGelarAkademikId(array('min' => 12)); // WHERE gelar_akademik_id >= 12
     * $query->filterByGelarAkademikId(array('max' => 12)); // WHERE gelar_akademik_id <= 12
     * </code>
     *
     * @see       filterByGelarAkademikRelatedByGelarAkademikId()
     *
     * @see       filterByGelarAkademikRelatedByGelarAkademikId()
     *
     * @param     mixed $gelarAkademikId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyPendFormalQuery The current query, for fluid interface
     */
    public function filterByGelarAkademikId($gelarAkademikId = null, $comparison = null)
    {
        if (is_array($gelarAkademikId)) {
            $useMinMax = false;
            if (isset($gelarAkademikId['min'])) {
                $this->addUsingAlias(RwyPendFormalPeer::GELAR_AKADEMIK_ID, $gelarAkademikId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($gelarAkademikId['max'])) {
                $this->addUsingAlias(RwyPendFormalPeer::GELAR_AKADEMIK_ID, $gelarAkademikId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RwyPendFormalPeer::GELAR_AKADEMIK_ID, $gelarAkademikId, $comparison);
    }

    /**
     * Filter the query on the satuan_pendidikan_formal column
     *
     * Example usage:
     * <code>
     * $query->filterBySatuanPendidikanFormal('fooValue');   // WHERE satuan_pendidikan_formal = 'fooValue'
     * $query->filterBySatuanPendidikanFormal('%fooValue%'); // WHERE satuan_pendidikan_formal LIKE '%fooValue%'
     * </code>
     *
     * @param     string $satuanPendidikanFormal The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyPendFormalQuery The current query, for fluid interface
     */
    public function filterBySatuanPendidikanFormal($satuanPendidikanFormal = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($satuanPendidikanFormal)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $satuanPendidikanFormal)) {
                $satuanPendidikanFormal = str_replace('*', '%', $satuanPendidikanFormal);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RwyPendFormalPeer::SATUAN_PENDIDIKAN_FORMAL, $satuanPendidikanFormal, $comparison);
    }

    /**
     * Filter the query on the fakultas column
     *
     * Example usage:
     * <code>
     * $query->filterByFakultas('fooValue');   // WHERE fakultas = 'fooValue'
     * $query->filterByFakultas('%fooValue%'); // WHERE fakultas LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fakultas The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyPendFormalQuery The current query, for fluid interface
     */
    public function filterByFakultas($fakultas = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fakultas)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $fakultas)) {
                $fakultas = str_replace('*', '%', $fakultas);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RwyPendFormalPeer::FAKULTAS, $fakultas, $comparison);
    }

    /**
     * Filter the query on the kependidikan column
     *
     * Example usage:
     * <code>
     * $query->filterByKependidikan(1234); // WHERE kependidikan = 1234
     * $query->filterByKependidikan(array(12, 34)); // WHERE kependidikan IN (12, 34)
     * $query->filterByKependidikan(array('min' => 12)); // WHERE kependidikan >= 12
     * $query->filterByKependidikan(array('max' => 12)); // WHERE kependidikan <= 12
     * </code>
     *
     * @param     mixed $kependidikan The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyPendFormalQuery The current query, for fluid interface
     */
    public function filterByKependidikan($kependidikan = null, $comparison = null)
    {
        if (is_array($kependidikan)) {
            $useMinMax = false;
            if (isset($kependidikan['min'])) {
                $this->addUsingAlias(RwyPendFormalPeer::KEPENDIDIKAN, $kependidikan['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($kependidikan['max'])) {
                $this->addUsingAlias(RwyPendFormalPeer::KEPENDIDIKAN, $kependidikan['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RwyPendFormalPeer::KEPENDIDIKAN, $kependidikan, $comparison);
    }

    /**
     * Filter the query on the tahun_masuk column
     *
     * Example usage:
     * <code>
     * $query->filterByTahunMasuk(1234); // WHERE tahun_masuk = 1234
     * $query->filterByTahunMasuk(array(12, 34)); // WHERE tahun_masuk IN (12, 34)
     * $query->filterByTahunMasuk(array('min' => 12)); // WHERE tahun_masuk >= 12
     * $query->filterByTahunMasuk(array('max' => 12)); // WHERE tahun_masuk <= 12
     * </code>
     *
     * @param     mixed $tahunMasuk The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyPendFormalQuery The current query, for fluid interface
     */
    public function filterByTahunMasuk($tahunMasuk = null, $comparison = null)
    {
        if (is_array($tahunMasuk)) {
            $useMinMax = false;
            if (isset($tahunMasuk['min'])) {
                $this->addUsingAlias(RwyPendFormalPeer::TAHUN_MASUK, $tahunMasuk['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tahunMasuk['max'])) {
                $this->addUsingAlias(RwyPendFormalPeer::TAHUN_MASUK, $tahunMasuk['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RwyPendFormalPeer::TAHUN_MASUK, $tahunMasuk, $comparison);
    }

    /**
     * Filter the query on the tahun_lulus column
     *
     * Example usage:
     * <code>
     * $query->filterByTahunLulus(1234); // WHERE tahun_lulus = 1234
     * $query->filterByTahunLulus(array(12, 34)); // WHERE tahun_lulus IN (12, 34)
     * $query->filterByTahunLulus(array('min' => 12)); // WHERE tahun_lulus >= 12
     * $query->filterByTahunLulus(array('max' => 12)); // WHERE tahun_lulus <= 12
     * </code>
     *
     * @param     mixed $tahunLulus The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyPendFormalQuery The current query, for fluid interface
     */
    public function filterByTahunLulus($tahunLulus = null, $comparison = null)
    {
        if (is_array($tahunLulus)) {
            $useMinMax = false;
            if (isset($tahunLulus['min'])) {
                $this->addUsingAlias(RwyPendFormalPeer::TAHUN_LULUS, $tahunLulus['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tahunLulus['max'])) {
                $this->addUsingAlias(RwyPendFormalPeer::TAHUN_LULUS, $tahunLulus['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RwyPendFormalPeer::TAHUN_LULUS, $tahunLulus, $comparison);
    }

    /**
     * Filter the query on the nim column
     *
     * Example usage:
     * <code>
     * $query->filterByNim('fooValue');   // WHERE nim = 'fooValue'
     * $query->filterByNim('%fooValue%'); // WHERE nim LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nim The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyPendFormalQuery The current query, for fluid interface
     */
    public function filterByNim($nim = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nim)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nim)) {
                $nim = str_replace('*', '%', $nim);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RwyPendFormalPeer::NIM, $nim, $comparison);
    }

    /**
     * Filter the query on the status_kuliah column
     *
     * Example usage:
     * <code>
     * $query->filterByStatusKuliah(1234); // WHERE status_kuliah = 1234
     * $query->filterByStatusKuliah(array(12, 34)); // WHERE status_kuliah IN (12, 34)
     * $query->filterByStatusKuliah(array('min' => 12)); // WHERE status_kuliah >= 12
     * $query->filterByStatusKuliah(array('max' => 12)); // WHERE status_kuliah <= 12
     * </code>
     *
     * @param     mixed $statusKuliah The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyPendFormalQuery The current query, for fluid interface
     */
    public function filterByStatusKuliah($statusKuliah = null, $comparison = null)
    {
        if (is_array($statusKuliah)) {
            $useMinMax = false;
            if (isset($statusKuliah['min'])) {
                $this->addUsingAlias(RwyPendFormalPeer::STATUS_KULIAH, $statusKuliah['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($statusKuliah['max'])) {
                $this->addUsingAlias(RwyPendFormalPeer::STATUS_KULIAH, $statusKuliah['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RwyPendFormalPeer::STATUS_KULIAH, $statusKuliah, $comparison);
    }

    /**
     * Filter the query on the semester column
     *
     * Example usage:
     * <code>
     * $query->filterBySemester(1234); // WHERE semester = 1234
     * $query->filterBySemester(array(12, 34)); // WHERE semester IN (12, 34)
     * $query->filterBySemester(array('min' => 12)); // WHERE semester >= 12
     * $query->filterBySemester(array('max' => 12)); // WHERE semester <= 12
     * </code>
     *
     * @param     mixed $semester The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyPendFormalQuery The current query, for fluid interface
     */
    public function filterBySemester($semester = null, $comparison = null)
    {
        if (is_array($semester)) {
            $useMinMax = false;
            if (isset($semester['min'])) {
                $this->addUsingAlias(RwyPendFormalPeer::SEMESTER, $semester['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($semester['max'])) {
                $this->addUsingAlias(RwyPendFormalPeer::SEMESTER, $semester['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RwyPendFormalPeer::SEMESTER, $semester, $comparison);
    }

    /**
     * Filter the query on the ipk column
     *
     * Example usage:
     * <code>
     * $query->filterByIpk(1234); // WHERE ipk = 1234
     * $query->filterByIpk(array(12, 34)); // WHERE ipk IN (12, 34)
     * $query->filterByIpk(array('min' => 12)); // WHERE ipk >= 12
     * $query->filterByIpk(array('max' => 12)); // WHERE ipk <= 12
     * </code>
     *
     * @param     mixed $ipk The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyPendFormalQuery The current query, for fluid interface
     */
    public function filterByIpk($ipk = null, $comparison = null)
    {
        if (is_array($ipk)) {
            $useMinMax = false;
            if (isset($ipk['min'])) {
                $this->addUsingAlias(RwyPendFormalPeer::IPK, $ipk['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($ipk['max'])) {
                $this->addUsingAlias(RwyPendFormalPeer::IPK, $ipk['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RwyPendFormalPeer::IPK, $ipk, $comparison);
    }

    /**
     * Filter the query on the Last_update column
     *
     * Example usage:
     * <code>
     * $query->filterByLastUpdate('2011-03-14'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate('now'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate(array('max' => 'yesterday')); // WHERE Last_update > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastUpdate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyPendFormalQuery The current query, for fluid interface
     */
    public function filterByLastUpdate($lastUpdate = null, $comparison = null)
    {
        if (is_array($lastUpdate)) {
            $useMinMax = false;
            if (isset($lastUpdate['min'])) {
                $this->addUsingAlias(RwyPendFormalPeer::LAST_UPDATE, $lastUpdate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastUpdate['max'])) {
                $this->addUsingAlias(RwyPendFormalPeer::LAST_UPDATE, $lastUpdate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RwyPendFormalPeer::LAST_UPDATE, $lastUpdate, $comparison);
    }

    /**
     * Filter the query on the Soft_delete column
     *
     * Example usage:
     * <code>
     * $query->filterBySoftDelete(1234); // WHERE Soft_delete = 1234
     * $query->filterBySoftDelete(array(12, 34)); // WHERE Soft_delete IN (12, 34)
     * $query->filterBySoftDelete(array('min' => 12)); // WHERE Soft_delete >= 12
     * $query->filterBySoftDelete(array('max' => 12)); // WHERE Soft_delete <= 12
     * </code>
     *
     * @param     mixed $softDelete The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyPendFormalQuery The current query, for fluid interface
     */
    public function filterBySoftDelete($softDelete = null, $comparison = null)
    {
        if (is_array($softDelete)) {
            $useMinMax = false;
            if (isset($softDelete['min'])) {
                $this->addUsingAlias(RwyPendFormalPeer::SOFT_DELETE, $softDelete['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($softDelete['max'])) {
                $this->addUsingAlias(RwyPendFormalPeer::SOFT_DELETE, $softDelete['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RwyPendFormalPeer::SOFT_DELETE, $softDelete, $comparison);
    }

    /**
     * Filter the query on the last_sync column
     *
     * Example usage:
     * <code>
     * $query->filterByLastSync('2011-03-14'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync('now'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync(array('max' => 'yesterday')); // WHERE last_sync > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastSync The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyPendFormalQuery The current query, for fluid interface
     */
    public function filterByLastSync($lastSync = null, $comparison = null)
    {
        if (is_array($lastSync)) {
            $useMinMax = false;
            if (isset($lastSync['min'])) {
                $this->addUsingAlias(RwyPendFormalPeer::LAST_SYNC, $lastSync['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastSync['max'])) {
                $this->addUsingAlias(RwyPendFormalPeer::LAST_SYNC, $lastSync['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RwyPendFormalPeer::LAST_SYNC, $lastSync, $comparison);
    }

    /**
     * Filter the query on the Updater_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdaterId('fooValue');   // WHERE Updater_ID = 'fooValue'
     * $query->filterByUpdaterId('%fooValue%'); // WHERE Updater_ID LIKE '%fooValue%'
     * </code>
     *
     * @param     string $updaterId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyPendFormalQuery The current query, for fluid interface
     */
    public function filterByUpdaterId($updaterId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($updaterId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $updaterId)) {
                $updaterId = str_replace('*', '%', $updaterId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RwyPendFormalPeer::UPDATER_ID, $updaterId, $comparison);
    }

    /**
     * Filter the query by a related Ptk object
     *
     * @param   Ptk|PropelObjectCollection $ptk The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RwyPendFormalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPtkRelatedByPtkId($ptk, $comparison = null)
    {
        if ($ptk instanceof Ptk) {
            return $this
                ->addUsingAlias(RwyPendFormalPeer::PTK_ID, $ptk->getPtkId(), $comparison);
        } elseif ($ptk instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RwyPendFormalPeer::PTK_ID, $ptk->toKeyValue('PrimaryKey', 'PtkId'), $comparison);
        } else {
            throw new PropelException('filterByPtkRelatedByPtkId() only accepts arguments of type Ptk or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PtkRelatedByPtkId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RwyPendFormalQuery The current query, for fluid interface
     */
    public function joinPtkRelatedByPtkId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PtkRelatedByPtkId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PtkRelatedByPtkId');
        }

        return $this;
    }

    /**
     * Use the PtkRelatedByPtkId relation Ptk object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PtkQuery A secondary query class using the current class as primary query
     */
    public function usePtkRelatedByPtkIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPtkRelatedByPtkId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PtkRelatedByPtkId', '\angulex\Model\PtkQuery');
    }

    /**
     * Filter the query by a related Ptk object
     *
     * @param   Ptk|PropelObjectCollection $ptk The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RwyPendFormalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPtkRelatedByPtkId($ptk, $comparison = null)
    {
        if ($ptk instanceof Ptk) {
            return $this
                ->addUsingAlias(RwyPendFormalPeer::PTK_ID, $ptk->getPtkId(), $comparison);
        } elseif ($ptk instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RwyPendFormalPeer::PTK_ID, $ptk->toKeyValue('PrimaryKey', 'PtkId'), $comparison);
        } else {
            throw new PropelException('filterByPtkRelatedByPtkId() only accepts arguments of type Ptk or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PtkRelatedByPtkId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RwyPendFormalQuery The current query, for fluid interface
     */
    public function joinPtkRelatedByPtkId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PtkRelatedByPtkId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PtkRelatedByPtkId');
        }

        return $this;
    }

    /**
     * Use the PtkRelatedByPtkId relation Ptk object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PtkQuery A secondary query class using the current class as primary query
     */
    public function usePtkRelatedByPtkIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPtkRelatedByPtkId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PtkRelatedByPtkId', '\angulex\Model\PtkQuery');
    }

    /**
     * Filter the query by a related BidangStudi object
     *
     * @param   BidangStudi|PropelObjectCollection $bidangStudi The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RwyPendFormalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByBidangStudiRelatedByBidangStudiId($bidangStudi, $comparison = null)
    {
        if ($bidangStudi instanceof BidangStudi) {
            return $this
                ->addUsingAlias(RwyPendFormalPeer::BIDANG_STUDI_ID, $bidangStudi->getBidangStudiId(), $comparison);
        } elseif ($bidangStudi instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RwyPendFormalPeer::BIDANG_STUDI_ID, $bidangStudi->toKeyValue('PrimaryKey', 'BidangStudiId'), $comparison);
        } else {
            throw new PropelException('filterByBidangStudiRelatedByBidangStudiId() only accepts arguments of type BidangStudi or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BidangStudiRelatedByBidangStudiId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RwyPendFormalQuery The current query, for fluid interface
     */
    public function joinBidangStudiRelatedByBidangStudiId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BidangStudiRelatedByBidangStudiId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BidangStudiRelatedByBidangStudiId');
        }

        return $this;
    }

    /**
     * Use the BidangStudiRelatedByBidangStudiId relation BidangStudi object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\BidangStudiQuery A secondary query class using the current class as primary query
     */
    public function useBidangStudiRelatedByBidangStudiIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBidangStudiRelatedByBidangStudiId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BidangStudiRelatedByBidangStudiId', '\angulex\Model\BidangStudiQuery');
    }

    /**
     * Filter the query by a related BidangStudi object
     *
     * @param   BidangStudi|PropelObjectCollection $bidangStudi The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RwyPendFormalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByBidangStudiRelatedByBidangStudiId($bidangStudi, $comparison = null)
    {
        if ($bidangStudi instanceof BidangStudi) {
            return $this
                ->addUsingAlias(RwyPendFormalPeer::BIDANG_STUDI_ID, $bidangStudi->getBidangStudiId(), $comparison);
        } elseif ($bidangStudi instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RwyPendFormalPeer::BIDANG_STUDI_ID, $bidangStudi->toKeyValue('PrimaryKey', 'BidangStudiId'), $comparison);
        } else {
            throw new PropelException('filterByBidangStudiRelatedByBidangStudiId() only accepts arguments of type BidangStudi or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BidangStudiRelatedByBidangStudiId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RwyPendFormalQuery The current query, for fluid interface
     */
    public function joinBidangStudiRelatedByBidangStudiId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BidangStudiRelatedByBidangStudiId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BidangStudiRelatedByBidangStudiId');
        }

        return $this;
    }

    /**
     * Use the BidangStudiRelatedByBidangStudiId relation BidangStudi object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\BidangStudiQuery A secondary query class using the current class as primary query
     */
    public function useBidangStudiRelatedByBidangStudiIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBidangStudiRelatedByBidangStudiId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BidangStudiRelatedByBidangStudiId', '\angulex\Model\BidangStudiQuery');
    }

    /**
     * Filter the query by a related GelarAkademik object
     *
     * @param   GelarAkademik|PropelObjectCollection $gelarAkademik The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RwyPendFormalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByGelarAkademikRelatedByGelarAkademikId($gelarAkademik, $comparison = null)
    {
        if ($gelarAkademik instanceof GelarAkademik) {
            return $this
                ->addUsingAlias(RwyPendFormalPeer::GELAR_AKADEMIK_ID, $gelarAkademik->getGelarAkademikId(), $comparison);
        } elseif ($gelarAkademik instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RwyPendFormalPeer::GELAR_AKADEMIK_ID, $gelarAkademik->toKeyValue('PrimaryKey', 'GelarAkademikId'), $comparison);
        } else {
            throw new PropelException('filterByGelarAkademikRelatedByGelarAkademikId() only accepts arguments of type GelarAkademik or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the GelarAkademikRelatedByGelarAkademikId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RwyPendFormalQuery The current query, for fluid interface
     */
    public function joinGelarAkademikRelatedByGelarAkademikId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('GelarAkademikRelatedByGelarAkademikId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'GelarAkademikRelatedByGelarAkademikId');
        }

        return $this;
    }

    /**
     * Use the GelarAkademikRelatedByGelarAkademikId relation GelarAkademik object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\GelarAkademikQuery A secondary query class using the current class as primary query
     */
    public function useGelarAkademikRelatedByGelarAkademikIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinGelarAkademikRelatedByGelarAkademikId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'GelarAkademikRelatedByGelarAkademikId', '\angulex\Model\GelarAkademikQuery');
    }

    /**
     * Filter the query by a related GelarAkademik object
     *
     * @param   GelarAkademik|PropelObjectCollection $gelarAkademik The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RwyPendFormalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByGelarAkademikRelatedByGelarAkademikId($gelarAkademik, $comparison = null)
    {
        if ($gelarAkademik instanceof GelarAkademik) {
            return $this
                ->addUsingAlias(RwyPendFormalPeer::GELAR_AKADEMIK_ID, $gelarAkademik->getGelarAkademikId(), $comparison);
        } elseif ($gelarAkademik instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RwyPendFormalPeer::GELAR_AKADEMIK_ID, $gelarAkademik->toKeyValue('PrimaryKey', 'GelarAkademikId'), $comparison);
        } else {
            throw new PropelException('filterByGelarAkademikRelatedByGelarAkademikId() only accepts arguments of type GelarAkademik or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the GelarAkademikRelatedByGelarAkademikId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RwyPendFormalQuery The current query, for fluid interface
     */
    public function joinGelarAkademikRelatedByGelarAkademikId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('GelarAkademikRelatedByGelarAkademikId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'GelarAkademikRelatedByGelarAkademikId');
        }

        return $this;
    }

    /**
     * Use the GelarAkademikRelatedByGelarAkademikId relation GelarAkademik object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\GelarAkademikQuery A secondary query class using the current class as primary query
     */
    public function useGelarAkademikRelatedByGelarAkademikIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinGelarAkademikRelatedByGelarAkademikId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'GelarAkademikRelatedByGelarAkademikId', '\angulex\Model\GelarAkademikQuery');
    }

    /**
     * Filter the query by a related JenjangPendidikan object
     *
     * @param   JenjangPendidikan|PropelObjectCollection $jenjangPendidikan The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RwyPendFormalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJenjangPendidikanRelatedByJenjangPendidikanId($jenjangPendidikan, $comparison = null)
    {
        if ($jenjangPendidikan instanceof JenjangPendidikan) {
            return $this
                ->addUsingAlias(RwyPendFormalPeer::JENJANG_PENDIDIKAN_ID, $jenjangPendidikan->getJenjangPendidikanId(), $comparison);
        } elseif ($jenjangPendidikan instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RwyPendFormalPeer::JENJANG_PENDIDIKAN_ID, $jenjangPendidikan->toKeyValue('PrimaryKey', 'JenjangPendidikanId'), $comparison);
        } else {
            throw new PropelException('filterByJenjangPendidikanRelatedByJenjangPendidikanId() only accepts arguments of type JenjangPendidikan or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the JenjangPendidikanRelatedByJenjangPendidikanId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RwyPendFormalQuery The current query, for fluid interface
     */
    public function joinJenjangPendidikanRelatedByJenjangPendidikanId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('JenjangPendidikanRelatedByJenjangPendidikanId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'JenjangPendidikanRelatedByJenjangPendidikanId');
        }

        return $this;
    }

    /**
     * Use the JenjangPendidikanRelatedByJenjangPendidikanId relation JenjangPendidikan object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\JenjangPendidikanQuery A secondary query class using the current class as primary query
     */
    public function useJenjangPendidikanRelatedByJenjangPendidikanIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinJenjangPendidikanRelatedByJenjangPendidikanId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'JenjangPendidikanRelatedByJenjangPendidikanId', '\angulex\Model\JenjangPendidikanQuery');
    }

    /**
     * Filter the query by a related JenjangPendidikan object
     *
     * @param   JenjangPendidikan|PropelObjectCollection $jenjangPendidikan The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RwyPendFormalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJenjangPendidikanRelatedByJenjangPendidikanId($jenjangPendidikan, $comparison = null)
    {
        if ($jenjangPendidikan instanceof JenjangPendidikan) {
            return $this
                ->addUsingAlias(RwyPendFormalPeer::JENJANG_PENDIDIKAN_ID, $jenjangPendidikan->getJenjangPendidikanId(), $comparison);
        } elseif ($jenjangPendidikan instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RwyPendFormalPeer::JENJANG_PENDIDIKAN_ID, $jenjangPendidikan->toKeyValue('PrimaryKey', 'JenjangPendidikanId'), $comparison);
        } else {
            throw new PropelException('filterByJenjangPendidikanRelatedByJenjangPendidikanId() only accepts arguments of type JenjangPendidikan or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the JenjangPendidikanRelatedByJenjangPendidikanId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RwyPendFormalQuery The current query, for fluid interface
     */
    public function joinJenjangPendidikanRelatedByJenjangPendidikanId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('JenjangPendidikanRelatedByJenjangPendidikanId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'JenjangPendidikanRelatedByJenjangPendidikanId');
        }

        return $this;
    }

    /**
     * Use the JenjangPendidikanRelatedByJenjangPendidikanId relation JenjangPendidikan object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\JenjangPendidikanQuery A secondary query class using the current class as primary query
     */
    public function useJenjangPendidikanRelatedByJenjangPendidikanIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinJenjangPendidikanRelatedByJenjangPendidikanId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'JenjangPendidikanRelatedByJenjangPendidikanId', '\angulex\Model\JenjangPendidikanQuery');
    }

    /**
     * Filter the query by a related VldRwyPendFormal object
     *
     * @param   VldRwyPendFormal|PropelObjectCollection $vldRwyPendFormal  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RwyPendFormalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldRwyPendFormalRelatedByRiwayatPendidikanFormalId($vldRwyPendFormal, $comparison = null)
    {
        if ($vldRwyPendFormal instanceof VldRwyPendFormal) {
            return $this
                ->addUsingAlias(RwyPendFormalPeer::RIWAYAT_PENDIDIKAN_FORMAL_ID, $vldRwyPendFormal->getRiwayatPendidikanFormalId(), $comparison);
        } elseif ($vldRwyPendFormal instanceof PropelObjectCollection) {
            return $this
                ->useVldRwyPendFormalRelatedByRiwayatPendidikanFormalIdQuery()
                ->filterByPrimaryKeys($vldRwyPendFormal->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldRwyPendFormalRelatedByRiwayatPendidikanFormalId() only accepts arguments of type VldRwyPendFormal or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldRwyPendFormalRelatedByRiwayatPendidikanFormalId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RwyPendFormalQuery The current query, for fluid interface
     */
    public function joinVldRwyPendFormalRelatedByRiwayatPendidikanFormalId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldRwyPendFormalRelatedByRiwayatPendidikanFormalId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldRwyPendFormalRelatedByRiwayatPendidikanFormalId');
        }

        return $this;
    }

    /**
     * Use the VldRwyPendFormalRelatedByRiwayatPendidikanFormalId relation VldRwyPendFormal object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldRwyPendFormalQuery A secondary query class using the current class as primary query
     */
    public function useVldRwyPendFormalRelatedByRiwayatPendidikanFormalIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldRwyPendFormalRelatedByRiwayatPendidikanFormalId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldRwyPendFormalRelatedByRiwayatPendidikanFormalId', '\angulex\Model\VldRwyPendFormalQuery');
    }

    /**
     * Filter the query by a related VldRwyPendFormal object
     *
     * @param   VldRwyPendFormal|PropelObjectCollection $vldRwyPendFormal  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RwyPendFormalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldRwyPendFormalRelatedByRiwayatPendidikanFormalId($vldRwyPendFormal, $comparison = null)
    {
        if ($vldRwyPendFormal instanceof VldRwyPendFormal) {
            return $this
                ->addUsingAlias(RwyPendFormalPeer::RIWAYAT_PENDIDIKAN_FORMAL_ID, $vldRwyPendFormal->getRiwayatPendidikanFormalId(), $comparison);
        } elseif ($vldRwyPendFormal instanceof PropelObjectCollection) {
            return $this
                ->useVldRwyPendFormalRelatedByRiwayatPendidikanFormalIdQuery()
                ->filterByPrimaryKeys($vldRwyPendFormal->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldRwyPendFormalRelatedByRiwayatPendidikanFormalId() only accepts arguments of type VldRwyPendFormal or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldRwyPendFormalRelatedByRiwayatPendidikanFormalId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RwyPendFormalQuery The current query, for fluid interface
     */
    public function joinVldRwyPendFormalRelatedByRiwayatPendidikanFormalId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldRwyPendFormalRelatedByRiwayatPendidikanFormalId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldRwyPendFormalRelatedByRiwayatPendidikanFormalId');
        }

        return $this;
    }

    /**
     * Use the VldRwyPendFormalRelatedByRiwayatPendidikanFormalId relation VldRwyPendFormal object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldRwyPendFormalQuery A secondary query class using the current class as primary query
     */
    public function useVldRwyPendFormalRelatedByRiwayatPendidikanFormalIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldRwyPendFormalRelatedByRiwayatPendidikanFormalId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldRwyPendFormalRelatedByRiwayatPendidikanFormalId', '\angulex\Model\VldRwyPendFormalQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   RwyPendFormal $rwyPendFormal Object to remove from the list of results
     *
     * @return RwyPendFormalQuery The current query, for fluid interface
     */
    public function prune($rwyPendFormal = null)
    {
        if ($rwyPendFormal) {
            $this->addUsingAlias(RwyPendFormalPeer::RIWAYAT_PENDIDIKAN_FORMAL_ID, $rwyPendFormal->getRiwayatPendidikanFormalId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
