<?php

namespace angulex\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\JenisSarana;
use angulex\Model\JenisSaranaPeer;
use angulex\Model\JenisSaranaQuery;
use angulex\Model\Sarana;

/**
 * Base class that represents a query for the 'ref.jenis_sarana' table.
 *
 * 
 *
 * @method JenisSaranaQuery orderByJenisSaranaId($order = Criteria::ASC) Order by the jenis_sarana_id column
 * @method JenisSaranaQuery orderByNama($order = Criteria::ASC) Order by the nama column
 * @method JenisSaranaQuery orderByKelompok($order = Criteria::ASC) Order by the kelompok column
 * @method JenisSaranaQuery orderByPerluPenempatan($order = Criteria::ASC) Order by the perlu_penempatan column
 * @method JenisSaranaQuery orderByKeterangan($order = Criteria::ASC) Order by the keterangan column
 * @method JenisSaranaQuery orderByCreateDate($order = Criteria::ASC) Order by the create_date column
 * @method JenisSaranaQuery orderByLastUpdate($order = Criteria::ASC) Order by the last_update column
 * @method JenisSaranaQuery orderByExpiredDate($order = Criteria::ASC) Order by the expired_date column
 * @method JenisSaranaQuery orderByLastSync($order = Criteria::ASC) Order by the last_sync column
 *
 * @method JenisSaranaQuery groupByJenisSaranaId() Group by the jenis_sarana_id column
 * @method JenisSaranaQuery groupByNama() Group by the nama column
 * @method JenisSaranaQuery groupByKelompok() Group by the kelompok column
 * @method JenisSaranaQuery groupByPerluPenempatan() Group by the perlu_penempatan column
 * @method JenisSaranaQuery groupByKeterangan() Group by the keterangan column
 * @method JenisSaranaQuery groupByCreateDate() Group by the create_date column
 * @method JenisSaranaQuery groupByLastUpdate() Group by the last_update column
 * @method JenisSaranaQuery groupByExpiredDate() Group by the expired_date column
 * @method JenisSaranaQuery groupByLastSync() Group by the last_sync column
 *
 * @method JenisSaranaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method JenisSaranaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method JenisSaranaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method JenisSaranaQuery leftJoinSaranaRelatedByJenisSaranaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SaranaRelatedByJenisSaranaId relation
 * @method JenisSaranaQuery rightJoinSaranaRelatedByJenisSaranaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SaranaRelatedByJenisSaranaId relation
 * @method JenisSaranaQuery innerJoinSaranaRelatedByJenisSaranaId($relationAlias = null) Adds a INNER JOIN clause to the query using the SaranaRelatedByJenisSaranaId relation
 *
 * @method JenisSaranaQuery leftJoinSaranaRelatedByJenisSaranaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SaranaRelatedByJenisSaranaId relation
 * @method JenisSaranaQuery rightJoinSaranaRelatedByJenisSaranaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SaranaRelatedByJenisSaranaId relation
 * @method JenisSaranaQuery innerJoinSaranaRelatedByJenisSaranaId($relationAlias = null) Adds a INNER JOIN clause to the query using the SaranaRelatedByJenisSaranaId relation
 *
 * @method JenisSarana findOne(PropelPDO $con = null) Return the first JenisSarana matching the query
 * @method JenisSarana findOneOrCreate(PropelPDO $con = null) Return the first JenisSarana matching the query, or a new JenisSarana object populated from the query conditions when no match is found
 *
 * @method JenisSarana findOneByNama(string $nama) Return the first JenisSarana filtered by the nama column
 * @method JenisSarana findOneByKelompok(string $kelompok) Return the first JenisSarana filtered by the kelompok column
 * @method JenisSarana findOneByPerluPenempatan(string $perlu_penempatan) Return the first JenisSarana filtered by the perlu_penempatan column
 * @method JenisSarana findOneByKeterangan(string $keterangan) Return the first JenisSarana filtered by the keterangan column
 * @method JenisSarana findOneByCreateDate(string $create_date) Return the first JenisSarana filtered by the create_date column
 * @method JenisSarana findOneByLastUpdate(string $last_update) Return the first JenisSarana filtered by the last_update column
 * @method JenisSarana findOneByExpiredDate(string $expired_date) Return the first JenisSarana filtered by the expired_date column
 * @method JenisSarana findOneByLastSync(string $last_sync) Return the first JenisSarana filtered by the last_sync column
 *
 * @method array findByJenisSaranaId(int $jenis_sarana_id) Return JenisSarana objects filtered by the jenis_sarana_id column
 * @method array findByNama(string $nama) Return JenisSarana objects filtered by the nama column
 * @method array findByKelompok(string $kelompok) Return JenisSarana objects filtered by the kelompok column
 * @method array findByPerluPenempatan(string $perlu_penempatan) Return JenisSarana objects filtered by the perlu_penempatan column
 * @method array findByKeterangan(string $keterangan) Return JenisSarana objects filtered by the keterangan column
 * @method array findByCreateDate(string $create_date) Return JenisSarana objects filtered by the create_date column
 * @method array findByLastUpdate(string $last_update) Return JenisSarana objects filtered by the last_update column
 * @method array findByExpiredDate(string $expired_date) Return JenisSarana objects filtered by the expired_date column
 * @method array findByLastSync(string $last_sync) Return JenisSarana objects filtered by the last_sync column
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseJenisSaranaQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseJenisSaranaQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Dapodikmen', $modelName = 'angulex\\Model\\JenisSarana', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new JenisSaranaQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   JenisSaranaQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return JenisSaranaQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof JenisSaranaQuery) {
            return $criteria;
        }
        $query = new JenisSaranaQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   JenisSarana|JenisSarana[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = JenisSaranaPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(JenisSaranaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 JenisSarana A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByJenisSaranaId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 JenisSarana A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [jenis_sarana_id], [nama], [kelompok], [perlu_penempatan], [keterangan], [create_date], [last_update], [expired_date], [last_sync] FROM [ref].[jenis_sarana] WHERE [jenis_sarana_id] = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new JenisSarana();
            $obj->hydrate($row);
            JenisSaranaPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return JenisSarana|JenisSarana[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|JenisSarana[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return JenisSaranaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(JenisSaranaPeer::JENIS_SARANA_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return JenisSaranaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(JenisSaranaPeer::JENIS_SARANA_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the jenis_sarana_id column
     *
     * Example usage:
     * <code>
     * $query->filterByJenisSaranaId(1234); // WHERE jenis_sarana_id = 1234
     * $query->filterByJenisSaranaId(array(12, 34)); // WHERE jenis_sarana_id IN (12, 34)
     * $query->filterByJenisSaranaId(array('min' => 12)); // WHERE jenis_sarana_id >= 12
     * $query->filterByJenisSaranaId(array('max' => 12)); // WHERE jenis_sarana_id <= 12
     * </code>
     *
     * @param     mixed $jenisSaranaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JenisSaranaQuery The current query, for fluid interface
     */
    public function filterByJenisSaranaId($jenisSaranaId = null, $comparison = null)
    {
        if (is_array($jenisSaranaId)) {
            $useMinMax = false;
            if (isset($jenisSaranaId['min'])) {
                $this->addUsingAlias(JenisSaranaPeer::JENIS_SARANA_ID, $jenisSaranaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jenisSaranaId['max'])) {
                $this->addUsingAlias(JenisSaranaPeer::JENIS_SARANA_ID, $jenisSaranaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JenisSaranaPeer::JENIS_SARANA_ID, $jenisSaranaId, $comparison);
    }

    /**
     * Filter the query on the nama column
     *
     * Example usage:
     * <code>
     * $query->filterByNama('fooValue');   // WHERE nama = 'fooValue'
     * $query->filterByNama('%fooValue%'); // WHERE nama LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nama The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JenisSaranaQuery The current query, for fluid interface
     */
    public function filterByNama($nama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nama)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nama)) {
                $nama = str_replace('*', '%', $nama);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(JenisSaranaPeer::NAMA, $nama, $comparison);
    }

    /**
     * Filter the query on the kelompok column
     *
     * Example usage:
     * <code>
     * $query->filterByKelompok('fooValue');   // WHERE kelompok = 'fooValue'
     * $query->filterByKelompok('%fooValue%'); // WHERE kelompok LIKE '%fooValue%'
     * </code>
     *
     * @param     string $kelompok The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JenisSaranaQuery The current query, for fluid interface
     */
    public function filterByKelompok($kelompok = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($kelompok)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $kelompok)) {
                $kelompok = str_replace('*', '%', $kelompok);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(JenisSaranaPeer::KELOMPOK, $kelompok, $comparison);
    }

    /**
     * Filter the query on the perlu_penempatan column
     *
     * Example usage:
     * <code>
     * $query->filterByPerluPenempatan(1234); // WHERE perlu_penempatan = 1234
     * $query->filterByPerluPenempatan(array(12, 34)); // WHERE perlu_penempatan IN (12, 34)
     * $query->filterByPerluPenempatan(array('min' => 12)); // WHERE perlu_penempatan >= 12
     * $query->filterByPerluPenempatan(array('max' => 12)); // WHERE perlu_penempatan <= 12
     * </code>
     *
     * @param     mixed $perluPenempatan The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JenisSaranaQuery The current query, for fluid interface
     */
    public function filterByPerluPenempatan($perluPenempatan = null, $comparison = null)
    {
        if (is_array($perluPenempatan)) {
            $useMinMax = false;
            if (isset($perluPenempatan['min'])) {
                $this->addUsingAlias(JenisSaranaPeer::PERLU_PENEMPATAN, $perluPenempatan['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($perluPenempatan['max'])) {
                $this->addUsingAlias(JenisSaranaPeer::PERLU_PENEMPATAN, $perluPenempatan['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JenisSaranaPeer::PERLU_PENEMPATAN, $perluPenempatan, $comparison);
    }

    /**
     * Filter the query on the keterangan column
     *
     * Example usage:
     * <code>
     * $query->filterByKeterangan('fooValue');   // WHERE keterangan = 'fooValue'
     * $query->filterByKeterangan('%fooValue%'); // WHERE keterangan LIKE '%fooValue%'
     * </code>
     *
     * @param     string $keterangan The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JenisSaranaQuery The current query, for fluid interface
     */
    public function filterByKeterangan($keterangan = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($keterangan)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $keterangan)) {
                $keterangan = str_replace('*', '%', $keterangan);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(JenisSaranaPeer::KETERANGAN, $keterangan, $comparison);
    }

    /**
     * Filter the query on the create_date column
     *
     * Example usage:
     * <code>
     * $query->filterByCreateDate('2011-03-14'); // WHERE create_date = '2011-03-14'
     * $query->filterByCreateDate('now'); // WHERE create_date = '2011-03-14'
     * $query->filterByCreateDate(array('max' => 'yesterday')); // WHERE create_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $createDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JenisSaranaQuery The current query, for fluid interface
     */
    public function filterByCreateDate($createDate = null, $comparison = null)
    {
        if (is_array($createDate)) {
            $useMinMax = false;
            if (isset($createDate['min'])) {
                $this->addUsingAlias(JenisSaranaPeer::CREATE_DATE, $createDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createDate['max'])) {
                $this->addUsingAlias(JenisSaranaPeer::CREATE_DATE, $createDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JenisSaranaPeer::CREATE_DATE, $createDate, $comparison);
    }

    /**
     * Filter the query on the last_update column
     *
     * Example usage:
     * <code>
     * $query->filterByLastUpdate('2011-03-14'); // WHERE last_update = '2011-03-14'
     * $query->filterByLastUpdate('now'); // WHERE last_update = '2011-03-14'
     * $query->filterByLastUpdate(array('max' => 'yesterday')); // WHERE last_update > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastUpdate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JenisSaranaQuery The current query, for fluid interface
     */
    public function filterByLastUpdate($lastUpdate = null, $comparison = null)
    {
        if (is_array($lastUpdate)) {
            $useMinMax = false;
            if (isset($lastUpdate['min'])) {
                $this->addUsingAlias(JenisSaranaPeer::LAST_UPDATE, $lastUpdate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastUpdate['max'])) {
                $this->addUsingAlias(JenisSaranaPeer::LAST_UPDATE, $lastUpdate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JenisSaranaPeer::LAST_UPDATE, $lastUpdate, $comparison);
    }

    /**
     * Filter the query on the expired_date column
     *
     * Example usage:
     * <code>
     * $query->filterByExpiredDate('2011-03-14'); // WHERE expired_date = '2011-03-14'
     * $query->filterByExpiredDate('now'); // WHERE expired_date = '2011-03-14'
     * $query->filterByExpiredDate(array('max' => 'yesterday')); // WHERE expired_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $expiredDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JenisSaranaQuery The current query, for fluid interface
     */
    public function filterByExpiredDate($expiredDate = null, $comparison = null)
    {
        if (is_array($expiredDate)) {
            $useMinMax = false;
            if (isset($expiredDate['min'])) {
                $this->addUsingAlias(JenisSaranaPeer::EXPIRED_DATE, $expiredDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($expiredDate['max'])) {
                $this->addUsingAlias(JenisSaranaPeer::EXPIRED_DATE, $expiredDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JenisSaranaPeer::EXPIRED_DATE, $expiredDate, $comparison);
    }

    /**
     * Filter the query on the last_sync column
     *
     * Example usage:
     * <code>
     * $query->filterByLastSync('2011-03-14'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync('now'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync(array('max' => 'yesterday')); // WHERE last_sync > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastSync The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JenisSaranaQuery The current query, for fluid interface
     */
    public function filterByLastSync($lastSync = null, $comparison = null)
    {
        if (is_array($lastSync)) {
            $useMinMax = false;
            if (isset($lastSync['min'])) {
                $this->addUsingAlias(JenisSaranaPeer::LAST_SYNC, $lastSync['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastSync['max'])) {
                $this->addUsingAlias(JenisSaranaPeer::LAST_SYNC, $lastSync['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JenisSaranaPeer::LAST_SYNC, $lastSync, $comparison);
    }

    /**
     * Filter the query by a related Sarana object
     *
     * @param   Sarana|PropelObjectCollection $sarana  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 JenisSaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySaranaRelatedByJenisSaranaId($sarana, $comparison = null)
    {
        if ($sarana instanceof Sarana) {
            return $this
                ->addUsingAlias(JenisSaranaPeer::JENIS_SARANA_ID, $sarana->getJenisSaranaId(), $comparison);
        } elseif ($sarana instanceof PropelObjectCollection) {
            return $this
                ->useSaranaRelatedByJenisSaranaIdQuery()
                ->filterByPrimaryKeys($sarana->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySaranaRelatedByJenisSaranaId() only accepts arguments of type Sarana or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SaranaRelatedByJenisSaranaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return JenisSaranaQuery The current query, for fluid interface
     */
    public function joinSaranaRelatedByJenisSaranaId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SaranaRelatedByJenisSaranaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SaranaRelatedByJenisSaranaId');
        }

        return $this;
    }

    /**
     * Use the SaranaRelatedByJenisSaranaId relation Sarana object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SaranaQuery A secondary query class using the current class as primary query
     */
    public function useSaranaRelatedByJenisSaranaIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSaranaRelatedByJenisSaranaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SaranaRelatedByJenisSaranaId', '\angulex\Model\SaranaQuery');
    }

    /**
     * Filter the query by a related Sarana object
     *
     * @param   Sarana|PropelObjectCollection $sarana  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 JenisSaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySaranaRelatedByJenisSaranaId($sarana, $comparison = null)
    {
        if ($sarana instanceof Sarana) {
            return $this
                ->addUsingAlias(JenisSaranaPeer::JENIS_SARANA_ID, $sarana->getJenisSaranaId(), $comparison);
        } elseif ($sarana instanceof PropelObjectCollection) {
            return $this
                ->useSaranaRelatedByJenisSaranaIdQuery()
                ->filterByPrimaryKeys($sarana->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySaranaRelatedByJenisSaranaId() only accepts arguments of type Sarana or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SaranaRelatedByJenisSaranaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return JenisSaranaQuery The current query, for fluid interface
     */
    public function joinSaranaRelatedByJenisSaranaId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SaranaRelatedByJenisSaranaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SaranaRelatedByJenisSaranaId');
        }

        return $this;
    }

    /**
     * Use the SaranaRelatedByJenisSaranaId relation Sarana object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SaranaQuery A secondary query class using the current class as primary query
     */
    public function useSaranaRelatedByJenisSaranaIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSaranaRelatedByJenisSaranaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SaranaRelatedByJenisSaranaId', '\angulex\Model\SaranaQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   JenisSarana $jenisSarana Object to remove from the list of results
     *
     * @return JenisSaranaQuery The current query, for fluid interface
     */
    public function prune($jenisSarana = null)
    {
        if ($jenisSarana) {
            $this->addUsingAlias(JenisSaranaPeer::JENIS_SARANA_ID, $jenisSarana->getJenisSaranaId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
