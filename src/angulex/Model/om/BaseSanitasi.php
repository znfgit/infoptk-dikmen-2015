<?php

namespace angulex\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelDateTime;
use \PropelException;
use \PropelPDO;
use angulex\Model\Sanitasi;
use angulex\Model\SanitasiPeer;
use angulex\Model\SanitasiQuery;
use angulex\Model\Sekolah;
use angulex\Model\SekolahQuery;
use angulex\Model\Semester;
use angulex\Model\SemesterQuery;
use angulex\Model\SumberAir;
use angulex\Model\SumberAirQuery;

/**
 * Base class that represents a row from the 'sanitasi' table.
 *
 * 
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseSanitasi extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'angulex\\Model\\SanitasiPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        SanitasiPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the sekolah_id field.
     * @var        string
     */
    protected $sekolah_id;

    /**
     * The value for the semester_id field.
     * @var        string
     */
    protected $semester_id;

    /**
     * The value for the sumber_air_id field.
     * @var        string
     */
    protected $sumber_air_id;

    /**
     * The value for the ketersediaan_air field.
     * @var        string
     */
    protected $ketersediaan_air;

    /**
     * The value for the kecukupan_air field.
     * @var        string
     */
    protected $kecukupan_air;

    /**
     * The value for the minum_siswa field.
     * @var        string
     */
    protected $minum_siswa;

    /**
     * The value for the memproses_air field.
     * @var        string
     */
    protected $memproses_air;

    /**
     * The value for the siswa_bawa_air field.
     * @var        string
     */
    protected $siswa_bawa_air;

    /**
     * The value for the toilet_siswa_laki field.
     * @var        string
     */
    protected $toilet_siswa_laki;

    /**
     * The value for the toilet_siswa_perempuan field.
     * @var        string
     */
    protected $toilet_siswa_perempuan;

    /**
     * The value for the toilet_siswa_kk field.
     * @var        string
     */
    protected $toilet_siswa_kk;

    /**
     * The value for the toilet_siswa_kecil field.
     * @var        string
     */
    protected $toilet_siswa_kecil;

    /**
     * The value for the tempat_cuci_tangan field.
     * Note: this column has a database default value of: '((0))'
     * @var        string
     */
    protected $tempat_cuci_tangan;

    /**
     * The value for the last_update field.
     * @var        string
     */
    protected $last_update;

    /**
     * The value for the soft_delete field.
     * @var        string
     */
    protected $soft_delete;

    /**
     * The value for the last_sync field.
     * @var        string
     */
    protected $last_sync;

    /**
     * The value for the updater_id field.
     * @var        string
     */
    protected $updater_id;

    /**
     * @var        Sekolah
     */
    protected $aSekolahRelatedBySekolahId;

    /**
     * @var        Sekolah
     */
    protected $aSekolahRelatedBySekolahId;

    /**
     * @var        Semester
     */
    protected $aSemesterRelatedBySemesterId;

    /**
     * @var        Semester
     */
    protected $aSemesterRelatedBySemesterId;

    /**
     * @var        SumberAir
     */
    protected $aSumberAirRelatedBySumberAirId;

    /**
     * @var        SumberAir
     */
    protected $aSumberAirRelatedBySumberAirId;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->tempat_cuci_tangan = '((0))';
    }

    /**
     * Initializes internal state of BaseSanitasi object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [sekolah_id] column value.
     * 
     * @return string
     */
    public function getSekolahId()
    {
        return $this->sekolah_id;
    }

    /**
     * Get the [semester_id] column value.
     * 
     * @return string
     */
    public function getSemesterId()
    {
        return $this->semester_id;
    }

    /**
     * Get the [sumber_air_id] column value.
     * 
     * @return string
     */
    public function getSumberAirId()
    {
        return $this->sumber_air_id;
    }

    /**
     * Get the [ketersediaan_air] column value.
     * 
     * @return string
     */
    public function getKetersediaanAir()
    {
        return $this->ketersediaan_air;
    }

    /**
     * Get the [kecukupan_air] column value.
     * 
     * @return string
     */
    public function getKecukupanAir()
    {
        return $this->kecukupan_air;
    }

    /**
     * Get the [minum_siswa] column value.
     * 
     * @return string
     */
    public function getMinumSiswa()
    {
        return $this->minum_siswa;
    }

    /**
     * Get the [memproses_air] column value.
     * 
     * @return string
     */
    public function getMemprosesAir()
    {
        return $this->memproses_air;
    }

    /**
     * Get the [siswa_bawa_air] column value.
     * 
     * @return string
     */
    public function getSiswaBawaAir()
    {
        return $this->siswa_bawa_air;
    }

    /**
     * Get the [toilet_siswa_laki] column value.
     * 
     * @return string
     */
    public function getToiletSiswaLaki()
    {
        return $this->toilet_siswa_laki;
    }

    /**
     * Get the [toilet_siswa_perempuan] column value.
     * 
     * @return string
     */
    public function getToiletSiswaPerempuan()
    {
        return $this->toilet_siswa_perempuan;
    }

    /**
     * Get the [toilet_siswa_kk] column value.
     * 
     * @return string
     */
    public function getToiletSiswaKk()
    {
        return $this->toilet_siswa_kk;
    }

    /**
     * Get the [toilet_siswa_kecil] column value.
     * 
     * @return string
     */
    public function getToiletSiswaKecil()
    {
        return $this->toilet_siswa_kecil;
    }

    /**
     * Get the [tempat_cuci_tangan] column value.
     * 
     * @return string
     */
    public function getTempatCuciTangan()
    {
        return $this->tempat_cuci_tangan;
    }

    /**
     * Get the [optionally formatted] temporal [last_update] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastUpdate($format = 'Y-m-d H:i:s')
    {
        if ($this->last_update === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_update);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_update, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [soft_delete] column value.
     * 
     * @return string
     */
    public function getSoftDelete()
    {
        return $this->soft_delete;
    }

    /**
     * Get the [optionally formatted] temporal [last_sync] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastSync($format = 'Y-m-d H:i:s')
    {
        if ($this->last_sync === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_sync);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_sync, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [updater_id] column value.
     * 
     * @return string
     */
    public function getUpdaterId()
    {
        return $this->updater_id;
    }

    /**
     * Set the value of [sekolah_id] column.
     * 
     * @param string $v new value
     * @return Sanitasi The current object (for fluent API support)
     */
    public function setSekolahId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->sekolah_id !== $v) {
            $this->sekolah_id = $v;
            $this->modifiedColumns[] = SanitasiPeer::SEKOLAH_ID;
        }

        if ($this->aSekolahRelatedBySekolahId !== null && $this->aSekolahRelatedBySekolahId->getSekolahId() !== $v) {
            $this->aSekolahRelatedBySekolahId = null;
        }

        if ($this->aSekolahRelatedBySekolahId !== null && $this->aSekolahRelatedBySekolahId->getSekolahId() !== $v) {
            $this->aSekolahRelatedBySekolahId = null;
        }


        return $this;
    } // setSekolahId()

    /**
     * Set the value of [semester_id] column.
     * 
     * @param string $v new value
     * @return Sanitasi The current object (for fluent API support)
     */
    public function setSemesterId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->semester_id !== $v) {
            $this->semester_id = $v;
            $this->modifiedColumns[] = SanitasiPeer::SEMESTER_ID;
        }

        if ($this->aSemesterRelatedBySemesterId !== null && $this->aSemesterRelatedBySemesterId->getSemesterId() !== $v) {
            $this->aSemesterRelatedBySemesterId = null;
        }

        if ($this->aSemesterRelatedBySemesterId !== null && $this->aSemesterRelatedBySemesterId->getSemesterId() !== $v) {
            $this->aSemesterRelatedBySemesterId = null;
        }


        return $this;
    } // setSemesterId()

    /**
     * Set the value of [sumber_air_id] column.
     * 
     * @param string $v new value
     * @return Sanitasi The current object (for fluent API support)
     */
    public function setSumberAirId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->sumber_air_id !== $v) {
            $this->sumber_air_id = $v;
            $this->modifiedColumns[] = SanitasiPeer::SUMBER_AIR_ID;
        }

        if ($this->aSumberAirRelatedBySumberAirId !== null && $this->aSumberAirRelatedBySumberAirId->getSumberAirId() !== $v) {
            $this->aSumberAirRelatedBySumberAirId = null;
        }

        if ($this->aSumberAirRelatedBySumberAirId !== null && $this->aSumberAirRelatedBySumberAirId->getSumberAirId() !== $v) {
            $this->aSumberAirRelatedBySumberAirId = null;
        }


        return $this;
    } // setSumberAirId()

    /**
     * Set the value of [ketersediaan_air] column.
     * 
     * @param string $v new value
     * @return Sanitasi The current object (for fluent API support)
     */
    public function setKetersediaanAir($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->ketersediaan_air !== $v) {
            $this->ketersediaan_air = $v;
            $this->modifiedColumns[] = SanitasiPeer::KETERSEDIAAN_AIR;
        }


        return $this;
    } // setKetersediaanAir()

    /**
     * Set the value of [kecukupan_air] column.
     * 
     * @param string $v new value
     * @return Sanitasi The current object (for fluent API support)
     */
    public function setKecukupanAir($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->kecukupan_air !== $v) {
            $this->kecukupan_air = $v;
            $this->modifiedColumns[] = SanitasiPeer::KECUKUPAN_AIR;
        }


        return $this;
    } // setKecukupanAir()

    /**
     * Set the value of [minum_siswa] column.
     * 
     * @param string $v new value
     * @return Sanitasi The current object (for fluent API support)
     */
    public function setMinumSiswa($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->minum_siswa !== $v) {
            $this->minum_siswa = $v;
            $this->modifiedColumns[] = SanitasiPeer::MINUM_SISWA;
        }


        return $this;
    } // setMinumSiswa()

    /**
     * Set the value of [memproses_air] column.
     * 
     * @param string $v new value
     * @return Sanitasi The current object (for fluent API support)
     */
    public function setMemprosesAir($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->memproses_air !== $v) {
            $this->memproses_air = $v;
            $this->modifiedColumns[] = SanitasiPeer::MEMPROSES_AIR;
        }


        return $this;
    } // setMemprosesAir()

    /**
     * Set the value of [siswa_bawa_air] column.
     * 
     * @param string $v new value
     * @return Sanitasi The current object (for fluent API support)
     */
    public function setSiswaBawaAir($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->siswa_bawa_air !== $v) {
            $this->siswa_bawa_air = $v;
            $this->modifiedColumns[] = SanitasiPeer::SISWA_BAWA_AIR;
        }


        return $this;
    } // setSiswaBawaAir()

    /**
     * Set the value of [toilet_siswa_laki] column.
     * 
     * @param string $v new value
     * @return Sanitasi The current object (for fluent API support)
     */
    public function setToiletSiswaLaki($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->toilet_siswa_laki !== $v) {
            $this->toilet_siswa_laki = $v;
            $this->modifiedColumns[] = SanitasiPeer::TOILET_SISWA_LAKI;
        }


        return $this;
    } // setToiletSiswaLaki()

    /**
     * Set the value of [toilet_siswa_perempuan] column.
     * 
     * @param string $v new value
     * @return Sanitasi The current object (for fluent API support)
     */
    public function setToiletSiswaPerempuan($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->toilet_siswa_perempuan !== $v) {
            $this->toilet_siswa_perempuan = $v;
            $this->modifiedColumns[] = SanitasiPeer::TOILET_SISWA_PEREMPUAN;
        }


        return $this;
    } // setToiletSiswaPerempuan()

    /**
     * Set the value of [toilet_siswa_kk] column.
     * 
     * @param string $v new value
     * @return Sanitasi The current object (for fluent API support)
     */
    public function setToiletSiswaKk($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->toilet_siswa_kk !== $v) {
            $this->toilet_siswa_kk = $v;
            $this->modifiedColumns[] = SanitasiPeer::TOILET_SISWA_KK;
        }


        return $this;
    } // setToiletSiswaKk()

    /**
     * Set the value of [toilet_siswa_kecil] column.
     * 
     * @param string $v new value
     * @return Sanitasi The current object (for fluent API support)
     */
    public function setToiletSiswaKecil($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->toilet_siswa_kecil !== $v) {
            $this->toilet_siswa_kecil = $v;
            $this->modifiedColumns[] = SanitasiPeer::TOILET_SISWA_KECIL;
        }


        return $this;
    } // setToiletSiswaKecil()

    /**
     * Set the value of [tempat_cuci_tangan] column.
     * 
     * @param string $v new value
     * @return Sanitasi The current object (for fluent API support)
     */
    public function setTempatCuciTangan($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tempat_cuci_tangan !== $v) {
            $this->tempat_cuci_tangan = $v;
            $this->modifiedColumns[] = SanitasiPeer::TEMPAT_CUCI_TANGAN;
        }


        return $this;
    } // setTempatCuciTangan()

    /**
     * Sets the value of [last_update] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Sanitasi The current object (for fluent API support)
     */
    public function setLastUpdate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_update !== null || $dt !== null) {
            $currentDateAsString = ($this->last_update !== null && $tmpDt = new DateTime($this->last_update)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_update = $newDateAsString;
                $this->modifiedColumns[] = SanitasiPeer::LAST_UPDATE;
            }
        } // if either are not null


        return $this;
    } // setLastUpdate()

    /**
     * Set the value of [soft_delete] column.
     * 
     * @param string $v new value
     * @return Sanitasi The current object (for fluent API support)
     */
    public function setSoftDelete($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->soft_delete !== $v) {
            $this->soft_delete = $v;
            $this->modifiedColumns[] = SanitasiPeer::SOFT_DELETE;
        }


        return $this;
    } // setSoftDelete()

    /**
     * Sets the value of [last_sync] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Sanitasi The current object (for fluent API support)
     */
    public function setLastSync($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_sync !== null || $dt !== null) {
            $currentDateAsString = ($this->last_sync !== null && $tmpDt = new DateTime($this->last_sync)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_sync = $newDateAsString;
                $this->modifiedColumns[] = SanitasiPeer::LAST_SYNC;
            }
        } // if either are not null


        return $this;
    } // setLastSync()

    /**
     * Set the value of [updater_id] column.
     * 
     * @param string $v new value
     * @return Sanitasi The current object (for fluent API support)
     */
    public function setUpdaterId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->updater_id !== $v) {
            $this->updater_id = $v;
            $this->modifiedColumns[] = SanitasiPeer::UPDATER_ID;
        }


        return $this;
    } // setUpdaterId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->tempat_cuci_tangan !== '((0))') {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->sekolah_id = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
            $this->semester_id = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->sumber_air_id = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->ketersediaan_air = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->kecukupan_air = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->minum_siswa = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->memproses_air = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->siswa_bawa_air = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->toilet_siswa_laki = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->toilet_siswa_perempuan = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->toilet_siswa_kk = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->toilet_siswa_kecil = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->tempat_cuci_tangan = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->last_update = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->soft_delete = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
            $this->last_sync = ($row[$startcol + 15] !== null) ? (string) $row[$startcol + 15] : null;
            $this->updater_id = ($row[$startcol + 16] !== null) ? (string) $row[$startcol + 16] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 17; // 17 = SanitasiPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Sanitasi object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aSekolahRelatedBySekolahId !== null && $this->sekolah_id !== $this->aSekolahRelatedBySekolahId->getSekolahId()) {
            $this->aSekolahRelatedBySekolahId = null;
        }
        if ($this->aSekolahRelatedBySekolahId !== null && $this->sekolah_id !== $this->aSekolahRelatedBySekolahId->getSekolahId()) {
            $this->aSekolahRelatedBySekolahId = null;
        }
        if ($this->aSemesterRelatedBySemesterId !== null && $this->semester_id !== $this->aSemesterRelatedBySemesterId->getSemesterId()) {
            $this->aSemesterRelatedBySemesterId = null;
        }
        if ($this->aSemesterRelatedBySemesterId !== null && $this->semester_id !== $this->aSemesterRelatedBySemesterId->getSemesterId()) {
            $this->aSemesterRelatedBySemesterId = null;
        }
        if ($this->aSumberAirRelatedBySumberAirId !== null && $this->sumber_air_id !== $this->aSumberAirRelatedBySumberAirId->getSumberAirId()) {
            $this->aSumberAirRelatedBySumberAirId = null;
        }
        if ($this->aSumberAirRelatedBySumberAirId !== null && $this->sumber_air_id !== $this->aSumberAirRelatedBySumberAirId->getSumberAirId()) {
            $this->aSumberAirRelatedBySumberAirId = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SanitasiPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = SanitasiPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aSekolahRelatedBySekolahId = null;
            $this->aSekolahRelatedBySekolahId = null;
            $this->aSemesterRelatedBySemesterId = null;
            $this->aSemesterRelatedBySemesterId = null;
            $this->aSumberAirRelatedBySumberAirId = null;
            $this->aSumberAirRelatedBySumberAirId = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SanitasiPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = SanitasiQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SanitasiPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                SanitasiPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if ($this->aSekolahRelatedBySekolahId->isModified() || $this->aSekolahRelatedBySekolahId->isNew()) {
                    $affectedRows += $this->aSekolahRelatedBySekolahId->save($con);
                }
                $this->setSekolahRelatedBySekolahId($this->aSekolahRelatedBySekolahId);
            }

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if ($this->aSekolahRelatedBySekolahId->isModified() || $this->aSekolahRelatedBySekolahId->isNew()) {
                    $affectedRows += $this->aSekolahRelatedBySekolahId->save($con);
                }
                $this->setSekolahRelatedBySekolahId($this->aSekolahRelatedBySekolahId);
            }

            if ($this->aSemesterRelatedBySemesterId !== null) {
                if ($this->aSemesterRelatedBySemesterId->isModified() || $this->aSemesterRelatedBySemesterId->isNew()) {
                    $affectedRows += $this->aSemesterRelatedBySemesterId->save($con);
                }
                $this->setSemesterRelatedBySemesterId($this->aSemesterRelatedBySemesterId);
            }

            if ($this->aSemesterRelatedBySemesterId !== null) {
                if ($this->aSemesterRelatedBySemesterId->isModified() || $this->aSemesterRelatedBySemesterId->isNew()) {
                    $affectedRows += $this->aSemesterRelatedBySemesterId->save($con);
                }
                $this->setSemesterRelatedBySemesterId($this->aSemesterRelatedBySemesterId);
            }

            if ($this->aSumberAirRelatedBySumberAirId !== null) {
                if ($this->aSumberAirRelatedBySumberAirId->isModified() || $this->aSumberAirRelatedBySumberAirId->isNew()) {
                    $affectedRows += $this->aSumberAirRelatedBySumberAirId->save($con);
                }
                $this->setSumberAirRelatedBySumberAirId($this->aSumberAirRelatedBySumberAirId);
            }

            if ($this->aSumberAirRelatedBySumberAirId !== null) {
                if ($this->aSumberAirRelatedBySumberAirId->isModified() || $this->aSumberAirRelatedBySumberAirId->isNew()) {
                    $affectedRows += $this->aSumberAirRelatedBySumberAirId->save($con);
                }
                $this->setSumberAirRelatedBySumberAirId($this->aSumberAirRelatedBySumberAirId);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $criteria = $this->buildCriteria();
        $pk = BasePeer::doInsert($criteria, $con);
        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if (!$this->aSekolahRelatedBySekolahId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSekolahRelatedBySekolahId->getValidationFailures());
                }
            }

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if (!$this->aSekolahRelatedBySekolahId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSekolahRelatedBySekolahId->getValidationFailures());
                }
            }

            if ($this->aSemesterRelatedBySemesterId !== null) {
                if (!$this->aSemesterRelatedBySemesterId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSemesterRelatedBySemesterId->getValidationFailures());
                }
            }

            if ($this->aSemesterRelatedBySemesterId !== null) {
                if (!$this->aSemesterRelatedBySemesterId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSemesterRelatedBySemesterId->getValidationFailures());
                }
            }

            if ($this->aSumberAirRelatedBySumberAirId !== null) {
                if (!$this->aSumberAirRelatedBySumberAirId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSumberAirRelatedBySumberAirId->getValidationFailures());
                }
            }

            if ($this->aSumberAirRelatedBySumberAirId !== null) {
                if (!$this->aSumberAirRelatedBySumberAirId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSumberAirRelatedBySumberAirId->getValidationFailures());
                }
            }


            if (($retval = SanitasiPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SanitasiPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getSekolahId();
                break;
            case 1:
                return $this->getSemesterId();
                break;
            case 2:
                return $this->getSumberAirId();
                break;
            case 3:
                return $this->getKetersediaanAir();
                break;
            case 4:
                return $this->getKecukupanAir();
                break;
            case 5:
                return $this->getMinumSiswa();
                break;
            case 6:
                return $this->getMemprosesAir();
                break;
            case 7:
                return $this->getSiswaBawaAir();
                break;
            case 8:
                return $this->getToiletSiswaLaki();
                break;
            case 9:
                return $this->getToiletSiswaPerempuan();
                break;
            case 10:
                return $this->getToiletSiswaKk();
                break;
            case 11:
                return $this->getToiletSiswaKecil();
                break;
            case 12:
                return $this->getTempatCuciTangan();
                break;
            case 13:
                return $this->getLastUpdate();
                break;
            case 14:
                return $this->getSoftDelete();
                break;
            case 15:
                return $this->getLastSync();
                break;
            case 16:
                return $this->getUpdaterId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Sanitasi'][serialize($this->getPrimaryKey())])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Sanitasi'][serialize($this->getPrimaryKey())] = true;
        $keys = SanitasiPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getSekolahId(),
            $keys[1] => $this->getSemesterId(),
            $keys[2] => $this->getSumberAirId(),
            $keys[3] => $this->getKetersediaanAir(),
            $keys[4] => $this->getKecukupanAir(),
            $keys[5] => $this->getMinumSiswa(),
            $keys[6] => $this->getMemprosesAir(),
            $keys[7] => $this->getSiswaBawaAir(),
            $keys[8] => $this->getToiletSiswaLaki(),
            $keys[9] => $this->getToiletSiswaPerempuan(),
            $keys[10] => $this->getToiletSiswaKk(),
            $keys[11] => $this->getToiletSiswaKecil(),
            $keys[12] => $this->getTempatCuciTangan(),
            $keys[13] => $this->getLastUpdate(),
            $keys[14] => $this->getSoftDelete(),
            $keys[15] => $this->getLastSync(),
            $keys[16] => $this->getUpdaterId(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aSekolahRelatedBySekolahId) {
                $result['SekolahRelatedBySekolahId'] = $this->aSekolahRelatedBySekolahId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSekolahRelatedBySekolahId) {
                $result['SekolahRelatedBySekolahId'] = $this->aSekolahRelatedBySekolahId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSemesterRelatedBySemesterId) {
                $result['SemesterRelatedBySemesterId'] = $this->aSemesterRelatedBySemesterId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSemesterRelatedBySemesterId) {
                $result['SemesterRelatedBySemesterId'] = $this->aSemesterRelatedBySemesterId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSumberAirRelatedBySumberAirId) {
                $result['SumberAirRelatedBySumberAirId'] = $this->aSumberAirRelatedBySumberAirId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSumberAirRelatedBySumberAirId) {
                $result['SumberAirRelatedBySumberAirId'] = $this->aSumberAirRelatedBySumberAirId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SanitasiPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setSekolahId($value);
                break;
            case 1:
                $this->setSemesterId($value);
                break;
            case 2:
                $this->setSumberAirId($value);
                break;
            case 3:
                $this->setKetersediaanAir($value);
                break;
            case 4:
                $this->setKecukupanAir($value);
                break;
            case 5:
                $this->setMinumSiswa($value);
                break;
            case 6:
                $this->setMemprosesAir($value);
                break;
            case 7:
                $this->setSiswaBawaAir($value);
                break;
            case 8:
                $this->setToiletSiswaLaki($value);
                break;
            case 9:
                $this->setToiletSiswaPerempuan($value);
                break;
            case 10:
                $this->setToiletSiswaKk($value);
                break;
            case 11:
                $this->setToiletSiswaKecil($value);
                break;
            case 12:
                $this->setTempatCuciTangan($value);
                break;
            case 13:
                $this->setLastUpdate($value);
                break;
            case 14:
                $this->setSoftDelete($value);
                break;
            case 15:
                $this->setLastSync($value);
                break;
            case 16:
                $this->setUpdaterId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = SanitasiPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setSekolahId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setSemesterId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setSumberAirId($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setKetersediaanAir($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setKecukupanAir($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setMinumSiswa($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setMemprosesAir($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setSiswaBawaAir($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setToiletSiswaLaki($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setToiletSiswaPerempuan($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setToiletSiswaKk($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setToiletSiswaKecil($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setTempatCuciTangan($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setLastUpdate($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setSoftDelete($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setLastSync($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setUpdaterId($arr[$keys[16]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(SanitasiPeer::DATABASE_NAME);

        if ($this->isColumnModified(SanitasiPeer::SEKOLAH_ID)) $criteria->add(SanitasiPeer::SEKOLAH_ID, $this->sekolah_id);
        if ($this->isColumnModified(SanitasiPeer::SEMESTER_ID)) $criteria->add(SanitasiPeer::SEMESTER_ID, $this->semester_id);
        if ($this->isColumnModified(SanitasiPeer::SUMBER_AIR_ID)) $criteria->add(SanitasiPeer::SUMBER_AIR_ID, $this->sumber_air_id);
        if ($this->isColumnModified(SanitasiPeer::KETERSEDIAAN_AIR)) $criteria->add(SanitasiPeer::KETERSEDIAAN_AIR, $this->ketersediaan_air);
        if ($this->isColumnModified(SanitasiPeer::KECUKUPAN_AIR)) $criteria->add(SanitasiPeer::KECUKUPAN_AIR, $this->kecukupan_air);
        if ($this->isColumnModified(SanitasiPeer::MINUM_SISWA)) $criteria->add(SanitasiPeer::MINUM_SISWA, $this->minum_siswa);
        if ($this->isColumnModified(SanitasiPeer::MEMPROSES_AIR)) $criteria->add(SanitasiPeer::MEMPROSES_AIR, $this->memproses_air);
        if ($this->isColumnModified(SanitasiPeer::SISWA_BAWA_AIR)) $criteria->add(SanitasiPeer::SISWA_BAWA_AIR, $this->siswa_bawa_air);
        if ($this->isColumnModified(SanitasiPeer::TOILET_SISWA_LAKI)) $criteria->add(SanitasiPeer::TOILET_SISWA_LAKI, $this->toilet_siswa_laki);
        if ($this->isColumnModified(SanitasiPeer::TOILET_SISWA_PEREMPUAN)) $criteria->add(SanitasiPeer::TOILET_SISWA_PEREMPUAN, $this->toilet_siswa_perempuan);
        if ($this->isColumnModified(SanitasiPeer::TOILET_SISWA_KK)) $criteria->add(SanitasiPeer::TOILET_SISWA_KK, $this->toilet_siswa_kk);
        if ($this->isColumnModified(SanitasiPeer::TOILET_SISWA_KECIL)) $criteria->add(SanitasiPeer::TOILET_SISWA_KECIL, $this->toilet_siswa_kecil);
        if ($this->isColumnModified(SanitasiPeer::TEMPAT_CUCI_TANGAN)) $criteria->add(SanitasiPeer::TEMPAT_CUCI_TANGAN, $this->tempat_cuci_tangan);
        if ($this->isColumnModified(SanitasiPeer::LAST_UPDATE)) $criteria->add(SanitasiPeer::LAST_UPDATE, $this->last_update);
        if ($this->isColumnModified(SanitasiPeer::SOFT_DELETE)) $criteria->add(SanitasiPeer::SOFT_DELETE, $this->soft_delete);
        if ($this->isColumnModified(SanitasiPeer::LAST_SYNC)) $criteria->add(SanitasiPeer::LAST_SYNC, $this->last_sync);
        if ($this->isColumnModified(SanitasiPeer::UPDATER_ID)) $criteria->add(SanitasiPeer::UPDATER_ID, $this->updater_id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(SanitasiPeer::DATABASE_NAME);
        $criteria->add(SanitasiPeer::SEKOLAH_ID, $this->sekolah_id);
        $criteria->add(SanitasiPeer::SEMESTER_ID, $this->semester_id);

        return $criteria;
    }

    /**
     * Returns the composite primary key for this object.
     * The array elements will be in same order as specified in XML.
     * @return array
     */
    public function getPrimaryKey()
    {
        $pks = array();
        $pks[0] = $this->getSekolahId();
        $pks[1] = $this->getSemesterId();

        return $pks;
    }

    /**
     * Set the [composite] primary key.
     *
     * @param array $keys The elements of the composite key (order must match the order in XML file).
     * @return void
     */
    public function setPrimaryKey($keys)
    {
        $this->setSekolahId($keys[0]);
        $this->setSemesterId($keys[1]);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return (null === $this->getSekolahId()) && (null === $this->getSemesterId());
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Sanitasi (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setSekolahId($this->getSekolahId());
        $copyObj->setSemesterId($this->getSemesterId());
        $copyObj->setSumberAirId($this->getSumberAirId());
        $copyObj->setKetersediaanAir($this->getKetersediaanAir());
        $copyObj->setKecukupanAir($this->getKecukupanAir());
        $copyObj->setMinumSiswa($this->getMinumSiswa());
        $copyObj->setMemprosesAir($this->getMemprosesAir());
        $copyObj->setSiswaBawaAir($this->getSiswaBawaAir());
        $copyObj->setToiletSiswaLaki($this->getToiletSiswaLaki());
        $copyObj->setToiletSiswaPerempuan($this->getToiletSiswaPerempuan());
        $copyObj->setToiletSiswaKk($this->getToiletSiswaKk());
        $copyObj->setToiletSiswaKecil($this->getToiletSiswaKecil());
        $copyObj->setTempatCuciTangan($this->getTempatCuciTangan());
        $copyObj->setLastUpdate($this->getLastUpdate());
        $copyObj->setSoftDelete($this->getSoftDelete());
        $copyObj->setLastSync($this->getLastSync());
        $copyObj->setUpdaterId($this->getUpdaterId());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Sanitasi Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return SanitasiPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new SanitasiPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a Sekolah object.
     *
     * @param             Sekolah $v
     * @return Sanitasi The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSekolahRelatedBySekolahId(Sekolah $v = null)
    {
        if ($v === null) {
            $this->setSekolahId(NULL);
        } else {
            $this->setSekolahId($v->getSekolahId());
        }

        $this->aSekolahRelatedBySekolahId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Sekolah object, it will not be re-added.
        if ($v !== null) {
            $v->addSanitasiRelatedBySekolahId($this);
        }


        return $this;
    }


    /**
     * Get the associated Sekolah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Sekolah The associated Sekolah object.
     * @throws PropelException
     */
    public function getSekolahRelatedBySekolahId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSekolahRelatedBySekolahId === null && (($this->sekolah_id !== "" && $this->sekolah_id !== null)) && $doQuery) {
            $this->aSekolahRelatedBySekolahId = SekolahQuery::create()->findPk($this->sekolah_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSekolahRelatedBySekolahId->addSanitasisRelatedBySekolahId($this);
             */
        }

        return $this->aSekolahRelatedBySekolahId;
    }

    /**
     * Declares an association between this object and a Sekolah object.
     *
     * @param             Sekolah $v
     * @return Sanitasi The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSekolahRelatedBySekolahId(Sekolah $v = null)
    {
        if ($v === null) {
            $this->setSekolahId(NULL);
        } else {
            $this->setSekolahId($v->getSekolahId());
        }

        $this->aSekolahRelatedBySekolahId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Sekolah object, it will not be re-added.
        if ($v !== null) {
            $v->addSanitasiRelatedBySekolahId($this);
        }


        return $this;
    }


    /**
     * Get the associated Sekolah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Sekolah The associated Sekolah object.
     * @throws PropelException
     */
    public function getSekolahRelatedBySekolahId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSekolahRelatedBySekolahId === null && (($this->sekolah_id !== "" && $this->sekolah_id !== null)) && $doQuery) {
            $this->aSekolahRelatedBySekolahId = SekolahQuery::create()->findPk($this->sekolah_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSekolahRelatedBySekolahId->addSanitasisRelatedBySekolahId($this);
             */
        }

        return $this->aSekolahRelatedBySekolahId;
    }

    /**
     * Declares an association between this object and a Semester object.
     *
     * @param             Semester $v
     * @return Sanitasi The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSemesterRelatedBySemesterId(Semester $v = null)
    {
        if ($v === null) {
            $this->setSemesterId(NULL);
        } else {
            $this->setSemesterId($v->getSemesterId());
        }

        $this->aSemesterRelatedBySemesterId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Semester object, it will not be re-added.
        if ($v !== null) {
            $v->addSanitasiRelatedBySemesterId($this);
        }


        return $this;
    }


    /**
     * Get the associated Semester object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Semester The associated Semester object.
     * @throws PropelException
     */
    public function getSemesterRelatedBySemesterId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSemesterRelatedBySemesterId === null && (($this->semester_id !== "" && $this->semester_id !== null)) && $doQuery) {
            $this->aSemesterRelatedBySemesterId = SemesterQuery::create()->findPk($this->semester_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSemesterRelatedBySemesterId->addSanitasisRelatedBySemesterId($this);
             */
        }

        return $this->aSemesterRelatedBySemesterId;
    }

    /**
     * Declares an association between this object and a Semester object.
     *
     * @param             Semester $v
     * @return Sanitasi The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSemesterRelatedBySemesterId(Semester $v = null)
    {
        if ($v === null) {
            $this->setSemesterId(NULL);
        } else {
            $this->setSemesterId($v->getSemesterId());
        }

        $this->aSemesterRelatedBySemesterId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Semester object, it will not be re-added.
        if ($v !== null) {
            $v->addSanitasiRelatedBySemesterId($this);
        }


        return $this;
    }


    /**
     * Get the associated Semester object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Semester The associated Semester object.
     * @throws PropelException
     */
    public function getSemesterRelatedBySemesterId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSemesterRelatedBySemesterId === null && (($this->semester_id !== "" && $this->semester_id !== null)) && $doQuery) {
            $this->aSemesterRelatedBySemesterId = SemesterQuery::create()->findPk($this->semester_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSemesterRelatedBySemesterId->addSanitasisRelatedBySemesterId($this);
             */
        }

        return $this->aSemesterRelatedBySemesterId;
    }

    /**
     * Declares an association between this object and a SumberAir object.
     *
     * @param             SumberAir $v
     * @return Sanitasi The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSumberAirRelatedBySumberAirId(SumberAir $v = null)
    {
        if ($v === null) {
            $this->setSumberAirId(NULL);
        } else {
            $this->setSumberAirId($v->getSumberAirId());
        }

        $this->aSumberAirRelatedBySumberAirId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the SumberAir object, it will not be re-added.
        if ($v !== null) {
            $v->addSanitasiRelatedBySumberAirId($this);
        }


        return $this;
    }


    /**
     * Get the associated SumberAir object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return SumberAir The associated SumberAir object.
     * @throws PropelException
     */
    public function getSumberAirRelatedBySumberAirId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSumberAirRelatedBySumberAirId === null && (($this->sumber_air_id !== "" && $this->sumber_air_id !== null)) && $doQuery) {
            $this->aSumberAirRelatedBySumberAirId = SumberAirQuery::create()->findPk($this->sumber_air_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSumberAirRelatedBySumberAirId->addSanitasisRelatedBySumberAirId($this);
             */
        }

        return $this->aSumberAirRelatedBySumberAirId;
    }

    /**
     * Declares an association between this object and a SumberAir object.
     *
     * @param             SumberAir $v
     * @return Sanitasi The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSumberAirRelatedBySumberAirId(SumberAir $v = null)
    {
        if ($v === null) {
            $this->setSumberAirId(NULL);
        } else {
            $this->setSumberAirId($v->getSumberAirId());
        }

        $this->aSumberAirRelatedBySumberAirId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the SumberAir object, it will not be re-added.
        if ($v !== null) {
            $v->addSanitasiRelatedBySumberAirId($this);
        }


        return $this;
    }


    /**
     * Get the associated SumberAir object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return SumberAir The associated SumberAir object.
     * @throws PropelException
     */
    public function getSumberAirRelatedBySumberAirId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSumberAirRelatedBySumberAirId === null && (($this->sumber_air_id !== "" && $this->sumber_air_id !== null)) && $doQuery) {
            $this->aSumberAirRelatedBySumberAirId = SumberAirQuery::create()->findPk($this->sumber_air_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSumberAirRelatedBySumberAirId->addSanitasisRelatedBySumberAirId($this);
             */
        }

        return $this->aSumberAirRelatedBySumberAirId;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->sekolah_id = null;
        $this->semester_id = null;
        $this->sumber_air_id = null;
        $this->ketersediaan_air = null;
        $this->kecukupan_air = null;
        $this->minum_siswa = null;
        $this->memproses_air = null;
        $this->siswa_bawa_air = null;
        $this->toilet_siswa_laki = null;
        $this->toilet_siswa_perempuan = null;
        $this->toilet_siswa_kk = null;
        $this->toilet_siswa_kecil = null;
        $this->tempat_cuci_tangan = null;
        $this->last_update = null;
        $this->soft_delete = null;
        $this->last_sync = null;
        $this->updater_id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->aSekolahRelatedBySekolahId instanceof Persistent) {
              $this->aSekolahRelatedBySekolahId->clearAllReferences($deep);
            }
            if ($this->aSekolahRelatedBySekolahId instanceof Persistent) {
              $this->aSekolahRelatedBySekolahId->clearAllReferences($deep);
            }
            if ($this->aSemesterRelatedBySemesterId instanceof Persistent) {
              $this->aSemesterRelatedBySemesterId->clearAllReferences($deep);
            }
            if ($this->aSemesterRelatedBySemesterId instanceof Persistent) {
              $this->aSemesterRelatedBySemesterId->clearAllReferences($deep);
            }
            if ($this->aSumberAirRelatedBySumberAirId instanceof Persistent) {
              $this->aSumberAirRelatedBySumberAirId->clearAllReferences($deep);
            }
            if ($this->aSumberAirRelatedBySumberAirId instanceof Persistent) {
              $this->aSumberAirRelatedBySumberAirId->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        $this->aSekolahRelatedBySekolahId = null;
        $this->aSekolahRelatedBySekolahId = null;
        $this->aSemesterRelatedBySemesterId = null;
        $this->aSemesterRelatedBySemesterId = null;
        $this->aSumberAirRelatedBySumberAirId = null;
        $this->aSumberAirRelatedBySumberAirId = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(SanitasiPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
