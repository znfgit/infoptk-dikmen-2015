<?php

namespace angulex\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\JenisKeluar;
use angulex\Model\JenisPendaftaran;
use angulex\Model\JurusanSp;
use angulex\Model\PesertaDidik;
use angulex\Model\RegistrasiPesertaDidik;
use angulex\Model\RegistrasiPesertaDidikPeer;
use angulex\Model\RegistrasiPesertaDidikQuery;
use angulex\Model\Sekolah;

/**
 * Base class that represents a query for the 'registrasi_peserta_didik' table.
 *
 * 
 *
 * @method RegistrasiPesertaDidikQuery orderByRegistrasiId($order = Criteria::ASC) Order by the registrasi_id column
 * @method RegistrasiPesertaDidikQuery orderByJurusanSpId($order = Criteria::ASC) Order by the jurusan_sp_id column
 * @method RegistrasiPesertaDidikQuery orderByPesertaDidikId($order = Criteria::ASC) Order by the peserta_didik_id column
 * @method RegistrasiPesertaDidikQuery orderBySekolahId($order = Criteria::ASC) Order by the sekolah_id column
 * @method RegistrasiPesertaDidikQuery orderByJenisPendaftaranId($order = Criteria::ASC) Order by the jenis_pendaftaran_id column
 * @method RegistrasiPesertaDidikQuery orderByNipd($order = Criteria::ASC) Order by the nipd column
 * @method RegistrasiPesertaDidikQuery orderByTanggalMasukSekolah($order = Criteria::ASC) Order by the tanggal_masuk_sekolah column
 * @method RegistrasiPesertaDidikQuery orderByJenisKeluarId($order = Criteria::ASC) Order by the jenis_keluar_id column
 * @method RegistrasiPesertaDidikQuery orderByTanggalKeluar($order = Criteria::ASC) Order by the tanggal_keluar column
 * @method RegistrasiPesertaDidikQuery orderByKeterangan($order = Criteria::ASC) Order by the keterangan column
 * @method RegistrasiPesertaDidikQuery orderByNoSkhun($order = Criteria::ASC) Order by the no_SKHUN column
 * @method RegistrasiPesertaDidikQuery orderByAPernahPaud($order = Criteria::ASC) Order by the a_pernah_paud column
 * @method RegistrasiPesertaDidikQuery orderByAPernahTk($order = Criteria::ASC) Order by the a_pernah_tk column
 * @method RegistrasiPesertaDidikQuery orderByLastUpdate($order = Criteria::ASC) Order by the Last_update column
 * @method RegistrasiPesertaDidikQuery orderBySoftDelete($order = Criteria::ASC) Order by the Soft_delete column
 * @method RegistrasiPesertaDidikQuery orderByLastSync($order = Criteria::ASC) Order by the last_sync column
 * @method RegistrasiPesertaDidikQuery orderByUpdaterId($order = Criteria::ASC) Order by the Updater_ID column
 *
 * @method RegistrasiPesertaDidikQuery groupByRegistrasiId() Group by the registrasi_id column
 * @method RegistrasiPesertaDidikQuery groupByJurusanSpId() Group by the jurusan_sp_id column
 * @method RegistrasiPesertaDidikQuery groupByPesertaDidikId() Group by the peserta_didik_id column
 * @method RegistrasiPesertaDidikQuery groupBySekolahId() Group by the sekolah_id column
 * @method RegistrasiPesertaDidikQuery groupByJenisPendaftaranId() Group by the jenis_pendaftaran_id column
 * @method RegistrasiPesertaDidikQuery groupByNipd() Group by the nipd column
 * @method RegistrasiPesertaDidikQuery groupByTanggalMasukSekolah() Group by the tanggal_masuk_sekolah column
 * @method RegistrasiPesertaDidikQuery groupByJenisKeluarId() Group by the jenis_keluar_id column
 * @method RegistrasiPesertaDidikQuery groupByTanggalKeluar() Group by the tanggal_keluar column
 * @method RegistrasiPesertaDidikQuery groupByKeterangan() Group by the keterangan column
 * @method RegistrasiPesertaDidikQuery groupByNoSkhun() Group by the no_SKHUN column
 * @method RegistrasiPesertaDidikQuery groupByAPernahPaud() Group by the a_pernah_paud column
 * @method RegistrasiPesertaDidikQuery groupByAPernahTk() Group by the a_pernah_tk column
 * @method RegistrasiPesertaDidikQuery groupByLastUpdate() Group by the Last_update column
 * @method RegistrasiPesertaDidikQuery groupBySoftDelete() Group by the Soft_delete column
 * @method RegistrasiPesertaDidikQuery groupByLastSync() Group by the last_sync column
 * @method RegistrasiPesertaDidikQuery groupByUpdaterId() Group by the Updater_ID column
 *
 * @method RegistrasiPesertaDidikQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method RegistrasiPesertaDidikQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method RegistrasiPesertaDidikQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method RegistrasiPesertaDidikQuery leftJoinJurusanSpRelatedByJurusanSpId($relationAlias = null) Adds a LEFT JOIN clause to the query using the JurusanSpRelatedByJurusanSpId relation
 * @method RegistrasiPesertaDidikQuery rightJoinJurusanSpRelatedByJurusanSpId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JurusanSpRelatedByJurusanSpId relation
 * @method RegistrasiPesertaDidikQuery innerJoinJurusanSpRelatedByJurusanSpId($relationAlias = null) Adds a INNER JOIN clause to the query using the JurusanSpRelatedByJurusanSpId relation
 *
 * @method RegistrasiPesertaDidikQuery leftJoinJurusanSpRelatedByJurusanSpId($relationAlias = null) Adds a LEFT JOIN clause to the query using the JurusanSpRelatedByJurusanSpId relation
 * @method RegistrasiPesertaDidikQuery rightJoinJurusanSpRelatedByJurusanSpId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JurusanSpRelatedByJurusanSpId relation
 * @method RegistrasiPesertaDidikQuery innerJoinJurusanSpRelatedByJurusanSpId($relationAlias = null) Adds a INNER JOIN clause to the query using the JurusanSpRelatedByJurusanSpId relation
 *
 * @method RegistrasiPesertaDidikQuery leftJoinPesertaDidikRelatedByPesertaDidikId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PesertaDidikRelatedByPesertaDidikId relation
 * @method RegistrasiPesertaDidikQuery rightJoinPesertaDidikRelatedByPesertaDidikId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PesertaDidikRelatedByPesertaDidikId relation
 * @method RegistrasiPesertaDidikQuery innerJoinPesertaDidikRelatedByPesertaDidikId($relationAlias = null) Adds a INNER JOIN clause to the query using the PesertaDidikRelatedByPesertaDidikId relation
 *
 * @method RegistrasiPesertaDidikQuery leftJoinPesertaDidikRelatedByPesertaDidikId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PesertaDidikRelatedByPesertaDidikId relation
 * @method RegistrasiPesertaDidikQuery rightJoinPesertaDidikRelatedByPesertaDidikId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PesertaDidikRelatedByPesertaDidikId relation
 * @method RegistrasiPesertaDidikQuery innerJoinPesertaDidikRelatedByPesertaDidikId($relationAlias = null) Adds a INNER JOIN clause to the query using the PesertaDidikRelatedByPesertaDidikId relation
 *
 * @method RegistrasiPesertaDidikQuery leftJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method RegistrasiPesertaDidikQuery rightJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method RegistrasiPesertaDidikQuery innerJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the SekolahRelatedBySekolahId relation
 *
 * @method RegistrasiPesertaDidikQuery leftJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method RegistrasiPesertaDidikQuery rightJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method RegistrasiPesertaDidikQuery innerJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the SekolahRelatedBySekolahId relation
 *
 * @method RegistrasiPesertaDidikQuery leftJoinJenisKeluarRelatedByJenisKeluarId($relationAlias = null) Adds a LEFT JOIN clause to the query using the JenisKeluarRelatedByJenisKeluarId relation
 * @method RegistrasiPesertaDidikQuery rightJoinJenisKeluarRelatedByJenisKeluarId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JenisKeluarRelatedByJenisKeluarId relation
 * @method RegistrasiPesertaDidikQuery innerJoinJenisKeluarRelatedByJenisKeluarId($relationAlias = null) Adds a INNER JOIN clause to the query using the JenisKeluarRelatedByJenisKeluarId relation
 *
 * @method RegistrasiPesertaDidikQuery leftJoinJenisKeluarRelatedByJenisKeluarId($relationAlias = null) Adds a LEFT JOIN clause to the query using the JenisKeluarRelatedByJenisKeluarId relation
 * @method RegistrasiPesertaDidikQuery rightJoinJenisKeluarRelatedByJenisKeluarId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JenisKeluarRelatedByJenisKeluarId relation
 * @method RegistrasiPesertaDidikQuery innerJoinJenisKeluarRelatedByJenisKeluarId($relationAlias = null) Adds a INNER JOIN clause to the query using the JenisKeluarRelatedByJenisKeluarId relation
 *
 * @method RegistrasiPesertaDidikQuery leftJoinJenisPendaftaranRelatedByJenisPendaftaranId($relationAlias = null) Adds a LEFT JOIN clause to the query using the JenisPendaftaranRelatedByJenisPendaftaranId relation
 * @method RegistrasiPesertaDidikQuery rightJoinJenisPendaftaranRelatedByJenisPendaftaranId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JenisPendaftaranRelatedByJenisPendaftaranId relation
 * @method RegistrasiPesertaDidikQuery innerJoinJenisPendaftaranRelatedByJenisPendaftaranId($relationAlias = null) Adds a INNER JOIN clause to the query using the JenisPendaftaranRelatedByJenisPendaftaranId relation
 *
 * @method RegistrasiPesertaDidikQuery leftJoinJenisPendaftaranRelatedByJenisPendaftaranId($relationAlias = null) Adds a LEFT JOIN clause to the query using the JenisPendaftaranRelatedByJenisPendaftaranId relation
 * @method RegistrasiPesertaDidikQuery rightJoinJenisPendaftaranRelatedByJenisPendaftaranId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JenisPendaftaranRelatedByJenisPendaftaranId relation
 * @method RegistrasiPesertaDidikQuery innerJoinJenisPendaftaranRelatedByJenisPendaftaranId($relationAlias = null) Adds a INNER JOIN clause to the query using the JenisPendaftaranRelatedByJenisPendaftaranId relation
 *
 * @method RegistrasiPesertaDidik findOne(PropelPDO $con = null) Return the first RegistrasiPesertaDidik matching the query
 * @method RegistrasiPesertaDidik findOneOrCreate(PropelPDO $con = null) Return the first RegistrasiPesertaDidik matching the query, or a new RegistrasiPesertaDidik object populated from the query conditions when no match is found
 *
 * @method RegistrasiPesertaDidik findOneByJurusanSpId(string $jurusan_sp_id) Return the first RegistrasiPesertaDidik filtered by the jurusan_sp_id column
 * @method RegistrasiPesertaDidik findOneByPesertaDidikId(string $peserta_didik_id) Return the first RegistrasiPesertaDidik filtered by the peserta_didik_id column
 * @method RegistrasiPesertaDidik findOneBySekolahId(string $sekolah_id) Return the first RegistrasiPesertaDidik filtered by the sekolah_id column
 * @method RegistrasiPesertaDidik findOneByJenisPendaftaranId(string $jenis_pendaftaran_id) Return the first RegistrasiPesertaDidik filtered by the jenis_pendaftaran_id column
 * @method RegistrasiPesertaDidik findOneByNipd(string $nipd) Return the first RegistrasiPesertaDidik filtered by the nipd column
 * @method RegistrasiPesertaDidik findOneByTanggalMasukSekolah(string $tanggal_masuk_sekolah) Return the first RegistrasiPesertaDidik filtered by the tanggal_masuk_sekolah column
 * @method RegistrasiPesertaDidik findOneByJenisKeluarId(string $jenis_keluar_id) Return the first RegistrasiPesertaDidik filtered by the jenis_keluar_id column
 * @method RegistrasiPesertaDidik findOneByTanggalKeluar(string $tanggal_keluar) Return the first RegistrasiPesertaDidik filtered by the tanggal_keluar column
 * @method RegistrasiPesertaDidik findOneByKeterangan(string $keterangan) Return the first RegistrasiPesertaDidik filtered by the keterangan column
 * @method RegistrasiPesertaDidik findOneByNoSkhun(string $no_SKHUN) Return the first RegistrasiPesertaDidik filtered by the no_SKHUN column
 * @method RegistrasiPesertaDidik findOneByAPernahPaud(string $a_pernah_paud) Return the first RegistrasiPesertaDidik filtered by the a_pernah_paud column
 * @method RegistrasiPesertaDidik findOneByAPernahTk(string $a_pernah_tk) Return the first RegistrasiPesertaDidik filtered by the a_pernah_tk column
 * @method RegistrasiPesertaDidik findOneByLastUpdate(string $Last_update) Return the first RegistrasiPesertaDidik filtered by the Last_update column
 * @method RegistrasiPesertaDidik findOneBySoftDelete(string $Soft_delete) Return the first RegistrasiPesertaDidik filtered by the Soft_delete column
 * @method RegistrasiPesertaDidik findOneByLastSync(string $last_sync) Return the first RegistrasiPesertaDidik filtered by the last_sync column
 * @method RegistrasiPesertaDidik findOneByUpdaterId(string $Updater_ID) Return the first RegistrasiPesertaDidik filtered by the Updater_ID column
 *
 * @method array findByRegistrasiId(string $registrasi_id) Return RegistrasiPesertaDidik objects filtered by the registrasi_id column
 * @method array findByJurusanSpId(string $jurusan_sp_id) Return RegistrasiPesertaDidik objects filtered by the jurusan_sp_id column
 * @method array findByPesertaDidikId(string $peserta_didik_id) Return RegistrasiPesertaDidik objects filtered by the peserta_didik_id column
 * @method array findBySekolahId(string $sekolah_id) Return RegistrasiPesertaDidik objects filtered by the sekolah_id column
 * @method array findByJenisPendaftaranId(string $jenis_pendaftaran_id) Return RegistrasiPesertaDidik objects filtered by the jenis_pendaftaran_id column
 * @method array findByNipd(string $nipd) Return RegistrasiPesertaDidik objects filtered by the nipd column
 * @method array findByTanggalMasukSekolah(string $tanggal_masuk_sekolah) Return RegistrasiPesertaDidik objects filtered by the tanggal_masuk_sekolah column
 * @method array findByJenisKeluarId(string $jenis_keluar_id) Return RegistrasiPesertaDidik objects filtered by the jenis_keluar_id column
 * @method array findByTanggalKeluar(string $tanggal_keluar) Return RegistrasiPesertaDidik objects filtered by the tanggal_keluar column
 * @method array findByKeterangan(string $keterangan) Return RegistrasiPesertaDidik objects filtered by the keterangan column
 * @method array findByNoSkhun(string $no_SKHUN) Return RegistrasiPesertaDidik objects filtered by the no_SKHUN column
 * @method array findByAPernahPaud(string $a_pernah_paud) Return RegistrasiPesertaDidik objects filtered by the a_pernah_paud column
 * @method array findByAPernahTk(string $a_pernah_tk) Return RegistrasiPesertaDidik objects filtered by the a_pernah_tk column
 * @method array findByLastUpdate(string $Last_update) Return RegistrasiPesertaDidik objects filtered by the Last_update column
 * @method array findBySoftDelete(string $Soft_delete) Return RegistrasiPesertaDidik objects filtered by the Soft_delete column
 * @method array findByLastSync(string $last_sync) Return RegistrasiPesertaDidik objects filtered by the last_sync column
 * @method array findByUpdaterId(string $Updater_ID) Return RegistrasiPesertaDidik objects filtered by the Updater_ID column
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseRegistrasiPesertaDidikQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseRegistrasiPesertaDidikQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Dapodikmen', $modelName = 'angulex\\Model\\RegistrasiPesertaDidik', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new RegistrasiPesertaDidikQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   RegistrasiPesertaDidikQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return RegistrasiPesertaDidikQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof RegistrasiPesertaDidikQuery) {
            return $criteria;
        }
        $query = new RegistrasiPesertaDidikQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   RegistrasiPesertaDidik|RegistrasiPesertaDidik[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = RegistrasiPesertaDidikPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(RegistrasiPesertaDidikPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 RegistrasiPesertaDidik A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByRegistrasiId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 RegistrasiPesertaDidik A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [registrasi_id], [jurusan_sp_id], [peserta_didik_id], [sekolah_id], [jenis_pendaftaran_id], [nipd], [tanggal_masuk_sekolah], [jenis_keluar_id], [tanggal_keluar], [keterangan], [no_SKHUN], [a_pernah_paud], [a_pernah_tk], [Last_update], [Soft_delete], [last_sync], [Updater_ID] FROM [registrasi_peserta_didik] WHERE [registrasi_id] = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new RegistrasiPesertaDidik();
            $obj->hydrate($row);
            RegistrasiPesertaDidikPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return RegistrasiPesertaDidik|RegistrasiPesertaDidik[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|RegistrasiPesertaDidik[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return RegistrasiPesertaDidikQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(RegistrasiPesertaDidikPeer::REGISTRASI_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return RegistrasiPesertaDidikQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(RegistrasiPesertaDidikPeer::REGISTRASI_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the registrasi_id column
     *
     * Example usage:
     * <code>
     * $query->filterByRegistrasiId('fooValue');   // WHERE registrasi_id = 'fooValue'
     * $query->filterByRegistrasiId('%fooValue%'); // WHERE registrasi_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $registrasiId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RegistrasiPesertaDidikQuery The current query, for fluid interface
     */
    public function filterByRegistrasiId($registrasiId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($registrasiId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $registrasiId)) {
                $registrasiId = str_replace('*', '%', $registrasiId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RegistrasiPesertaDidikPeer::REGISTRASI_ID, $registrasiId, $comparison);
    }

    /**
     * Filter the query on the jurusan_sp_id column
     *
     * Example usage:
     * <code>
     * $query->filterByJurusanSpId('fooValue');   // WHERE jurusan_sp_id = 'fooValue'
     * $query->filterByJurusanSpId('%fooValue%'); // WHERE jurusan_sp_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $jurusanSpId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RegistrasiPesertaDidikQuery The current query, for fluid interface
     */
    public function filterByJurusanSpId($jurusanSpId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($jurusanSpId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $jurusanSpId)) {
                $jurusanSpId = str_replace('*', '%', $jurusanSpId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RegistrasiPesertaDidikPeer::JURUSAN_SP_ID, $jurusanSpId, $comparison);
    }

    /**
     * Filter the query on the peserta_didik_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPesertaDidikId('fooValue');   // WHERE peserta_didik_id = 'fooValue'
     * $query->filterByPesertaDidikId('%fooValue%'); // WHERE peserta_didik_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $pesertaDidikId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RegistrasiPesertaDidikQuery The current query, for fluid interface
     */
    public function filterByPesertaDidikId($pesertaDidikId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($pesertaDidikId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $pesertaDidikId)) {
                $pesertaDidikId = str_replace('*', '%', $pesertaDidikId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RegistrasiPesertaDidikPeer::PESERTA_DIDIK_ID, $pesertaDidikId, $comparison);
    }

    /**
     * Filter the query on the sekolah_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySekolahId('fooValue');   // WHERE sekolah_id = 'fooValue'
     * $query->filterBySekolahId('%fooValue%'); // WHERE sekolah_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sekolahId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RegistrasiPesertaDidikQuery The current query, for fluid interface
     */
    public function filterBySekolahId($sekolahId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sekolahId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $sekolahId)) {
                $sekolahId = str_replace('*', '%', $sekolahId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RegistrasiPesertaDidikPeer::SEKOLAH_ID, $sekolahId, $comparison);
    }

    /**
     * Filter the query on the jenis_pendaftaran_id column
     *
     * Example usage:
     * <code>
     * $query->filterByJenisPendaftaranId(1234); // WHERE jenis_pendaftaran_id = 1234
     * $query->filterByJenisPendaftaranId(array(12, 34)); // WHERE jenis_pendaftaran_id IN (12, 34)
     * $query->filterByJenisPendaftaranId(array('min' => 12)); // WHERE jenis_pendaftaran_id >= 12
     * $query->filterByJenisPendaftaranId(array('max' => 12)); // WHERE jenis_pendaftaran_id <= 12
     * </code>
     *
     * @see       filterByJenisPendaftaranRelatedByJenisPendaftaranId()
     *
     * @see       filterByJenisPendaftaranRelatedByJenisPendaftaranId()
     *
     * @param     mixed $jenisPendaftaranId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RegistrasiPesertaDidikQuery The current query, for fluid interface
     */
    public function filterByJenisPendaftaranId($jenisPendaftaranId = null, $comparison = null)
    {
        if (is_array($jenisPendaftaranId)) {
            $useMinMax = false;
            if (isset($jenisPendaftaranId['min'])) {
                $this->addUsingAlias(RegistrasiPesertaDidikPeer::JENIS_PENDAFTARAN_ID, $jenisPendaftaranId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jenisPendaftaranId['max'])) {
                $this->addUsingAlias(RegistrasiPesertaDidikPeer::JENIS_PENDAFTARAN_ID, $jenisPendaftaranId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RegistrasiPesertaDidikPeer::JENIS_PENDAFTARAN_ID, $jenisPendaftaranId, $comparison);
    }

    /**
     * Filter the query on the nipd column
     *
     * Example usage:
     * <code>
     * $query->filterByNipd('fooValue');   // WHERE nipd = 'fooValue'
     * $query->filterByNipd('%fooValue%'); // WHERE nipd LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nipd The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RegistrasiPesertaDidikQuery The current query, for fluid interface
     */
    public function filterByNipd($nipd = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nipd)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nipd)) {
                $nipd = str_replace('*', '%', $nipd);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RegistrasiPesertaDidikPeer::NIPD, $nipd, $comparison);
    }

    /**
     * Filter the query on the tanggal_masuk_sekolah column
     *
     * Example usage:
     * <code>
     * $query->filterByTanggalMasukSekolah('fooValue');   // WHERE tanggal_masuk_sekolah = 'fooValue'
     * $query->filterByTanggalMasukSekolah('%fooValue%'); // WHERE tanggal_masuk_sekolah LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tanggalMasukSekolah The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RegistrasiPesertaDidikQuery The current query, for fluid interface
     */
    public function filterByTanggalMasukSekolah($tanggalMasukSekolah = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tanggalMasukSekolah)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tanggalMasukSekolah)) {
                $tanggalMasukSekolah = str_replace('*', '%', $tanggalMasukSekolah);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RegistrasiPesertaDidikPeer::TANGGAL_MASUK_SEKOLAH, $tanggalMasukSekolah, $comparison);
    }

    /**
     * Filter the query on the jenis_keluar_id column
     *
     * Example usage:
     * <code>
     * $query->filterByJenisKeluarId('fooValue');   // WHERE jenis_keluar_id = 'fooValue'
     * $query->filterByJenisKeluarId('%fooValue%'); // WHERE jenis_keluar_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $jenisKeluarId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RegistrasiPesertaDidikQuery The current query, for fluid interface
     */
    public function filterByJenisKeluarId($jenisKeluarId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($jenisKeluarId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $jenisKeluarId)) {
                $jenisKeluarId = str_replace('*', '%', $jenisKeluarId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RegistrasiPesertaDidikPeer::JENIS_KELUAR_ID, $jenisKeluarId, $comparison);
    }

    /**
     * Filter the query on the tanggal_keluar column
     *
     * Example usage:
     * <code>
     * $query->filterByTanggalKeluar('fooValue');   // WHERE tanggal_keluar = 'fooValue'
     * $query->filterByTanggalKeluar('%fooValue%'); // WHERE tanggal_keluar LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tanggalKeluar The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RegistrasiPesertaDidikQuery The current query, for fluid interface
     */
    public function filterByTanggalKeluar($tanggalKeluar = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tanggalKeluar)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tanggalKeluar)) {
                $tanggalKeluar = str_replace('*', '%', $tanggalKeluar);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RegistrasiPesertaDidikPeer::TANGGAL_KELUAR, $tanggalKeluar, $comparison);
    }

    /**
     * Filter the query on the keterangan column
     *
     * Example usage:
     * <code>
     * $query->filterByKeterangan('fooValue');   // WHERE keterangan = 'fooValue'
     * $query->filterByKeterangan('%fooValue%'); // WHERE keterangan LIKE '%fooValue%'
     * </code>
     *
     * @param     string $keterangan The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RegistrasiPesertaDidikQuery The current query, for fluid interface
     */
    public function filterByKeterangan($keterangan = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($keterangan)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $keterangan)) {
                $keterangan = str_replace('*', '%', $keterangan);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RegistrasiPesertaDidikPeer::KETERANGAN, $keterangan, $comparison);
    }

    /**
     * Filter the query on the no_SKHUN column
     *
     * Example usage:
     * <code>
     * $query->filterByNoSkhun('fooValue');   // WHERE no_SKHUN = 'fooValue'
     * $query->filterByNoSkhun('%fooValue%'); // WHERE no_SKHUN LIKE '%fooValue%'
     * </code>
     *
     * @param     string $noSkhun The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RegistrasiPesertaDidikQuery The current query, for fluid interface
     */
    public function filterByNoSkhun($noSkhun = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($noSkhun)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $noSkhun)) {
                $noSkhun = str_replace('*', '%', $noSkhun);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RegistrasiPesertaDidikPeer::NO_SKHUN, $noSkhun, $comparison);
    }

    /**
     * Filter the query on the a_pernah_paud column
     *
     * Example usage:
     * <code>
     * $query->filterByAPernahPaud(1234); // WHERE a_pernah_paud = 1234
     * $query->filterByAPernahPaud(array(12, 34)); // WHERE a_pernah_paud IN (12, 34)
     * $query->filterByAPernahPaud(array('min' => 12)); // WHERE a_pernah_paud >= 12
     * $query->filterByAPernahPaud(array('max' => 12)); // WHERE a_pernah_paud <= 12
     * </code>
     *
     * @param     mixed $aPernahPaud The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RegistrasiPesertaDidikQuery The current query, for fluid interface
     */
    public function filterByAPernahPaud($aPernahPaud = null, $comparison = null)
    {
        if (is_array($aPernahPaud)) {
            $useMinMax = false;
            if (isset($aPernahPaud['min'])) {
                $this->addUsingAlias(RegistrasiPesertaDidikPeer::A_PERNAH_PAUD, $aPernahPaud['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($aPernahPaud['max'])) {
                $this->addUsingAlias(RegistrasiPesertaDidikPeer::A_PERNAH_PAUD, $aPernahPaud['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RegistrasiPesertaDidikPeer::A_PERNAH_PAUD, $aPernahPaud, $comparison);
    }

    /**
     * Filter the query on the a_pernah_tk column
     *
     * Example usage:
     * <code>
     * $query->filterByAPernahTk(1234); // WHERE a_pernah_tk = 1234
     * $query->filterByAPernahTk(array(12, 34)); // WHERE a_pernah_tk IN (12, 34)
     * $query->filterByAPernahTk(array('min' => 12)); // WHERE a_pernah_tk >= 12
     * $query->filterByAPernahTk(array('max' => 12)); // WHERE a_pernah_tk <= 12
     * </code>
     *
     * @param     mixed $aPernahTk The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RegistrasiPesertaDidikQuery The current query, for fluid interface
     */
    public function filterByAPernahTk($aPernahTk = null, $comparison = null)
    {
        if (is_array($aPernahTk)) {
            $useMinMax = false;
            if (isset($aPernahTk['min'])) {
                $this->addUsingAlias(RegistrasiPesertaDidikPeer::A_PERNAH_TK, $aPernahTk['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($aPernahTk['max'])) {
                $this->addUsingAlias(RegistrasiPesertaDidikPeer::A_PERNAH_TK, $aPernahTk['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RegistrasiPesertaDidikPeer::A_PERNAH_TK, $aPernahTk, $comparison);
    }

    /**
     * Filter the query on the Last_update column
     *
     * Example usage:
     * <code>
     * $query->filterByLastUpdate('2011-03-14'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate('now'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate(array('max' => 'yesterday')); // WHERE Last_update > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastUpdate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RegistrasiPesertaDidikQuery The current query, for fluid interface
     */
    public function filterByLastUpdate($lastUpdate = null, $comparison = null)
    {
        if (is_array($lastUpdate)) {
            $useMinMax = false;
            if (isset($lastUpdate['min'])) {
                $this->addUsingAlias(RegistrasiPesertaDidikPeer::LAST_UPDATE, $lastUpdate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastUpdate['max'])) {
                $this->addUsingAlias(RegistrasiPesertaDidikPeer::LAST_UPDATE, $lastUpdate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RegistrasiPesertaDidikPeer::LAST_UPDATE, $lastUpdate, $comparison);
    }

    /**
     * Filter the query on the Soft_delete column
     *
     * Example usage:
     * <code>
     * $query->filterBySoftDelete(1234); // WHERE Soft_delete = 1234
     * $query->filterBySoftDelete(array(12, 34)); // WHERE Soft_delete IN (12, 34)
     * $query->filterBySoftDelete(array('min' => 12)); // WHERE Soft_delete >= 12
     * $query->filterBySoftDelete(array('max' => 12)); // WHERE Soft_delete <= 12
     * </code>
     *
     * @param     mixed $softDelete The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RegistrasiPesertaDidikQuery The current query, for fluid interface
     */
    public function filterBySoftDelete($softDelete = null, $comparison = null)
    {
        if (is_array($softDelete)) {
            $useMinMax = false;
            if (isset($softDelete['min'])) {
                $this->addUsingAlias(RegistrasiPesertaDidikPeer::SOFT_DELETE, $softDelete['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($softDelete['max'])) {
                $this->addUsingAlias(RegistrasiPesertaDidikPeer::SOFT_DELETE, $softDelete['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RegistrasiPesertaDidikPeer::SOFT_DELETE, $softDelete, $comparison);
    }

    /**
     * Filter the query on the last_sync column
     *
     * Example usage:
     * <code>
     * $query->filterByLastSync('2011-03-14'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync('now'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync(array('max' => 'yesterday')); // WHERE last_sync > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastSync The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RegistrasiPesertaDidikQuery The current query, for fluid interface
     */
    public function filterByLastSync($lastSync = null, $comparison = null)
    {
        if (is_array($lastSync)) {
            $useMinMax = false;
            if (isset($lastSync['min'])) {
                $this->addUsingAlias(RegistrasiPesertaDidikPeer::LAST_SYNC, $lastSync['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastSync['max'])) {
                $this->addUsingAlias(RegistrasiPesertaDidikPeer::LAST_SYNC, $lastSync['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RegistrasiPesertaDidikPeer::LAST_SYNC, $lastSync, $comparison);
    }

    /**
     * Filter the query on the Updater_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdaterId('fooValue');   // WHERE Updater_ID = 'fooValue'
     * $query->filterByUpdaterId('%fooValue%'); // WHERE Updater_ID LIKE '%fooValue%'
     * </code>
     *
     * @param     string $updaterId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RegistrasiPesertaDidikQuery The current query, for fluid interface
     */
    public function filterByUpdaterId($updaterId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($updaterId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $updaterId)) {
                $updaterId = str_replace('*', '%', $updaterId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RegistrasiPesertaDidikPeer::UPDATER_ID, $updaterId, $comparison);
    }

    /**
     * Filter the query by a related JurusanSp object
     *
     * @param   JurusanSp|PropelObjectCollection $jurusanSp The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RegistrasiPesertaDidikQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJurusanSpRelatedByJurusanSpId($jurusanSp, $comparison = null)
    {
        if ($jurusanSp instanceof JurusanSp) {
            return $this
                ->addUsingAlias(RegistrasiPesertaDidikPeer::JURUSAN_SP_ID, $jurusanSp->getJurusanSpId(), $comparison);
        } elseif ($jurusanSp instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RegistrasiPesertaDidikPeer::JURUSAN_SP_ID, $jurusanSp->toKeyValue('PrimaryKey', 'JurusanSpId'), $comparison);
        } else {
            throw new PropelException('filterByJurusanSpRelatedByJurusanSpId() only accepts arguments of type JurusanSp or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the JurusanSpRelatedByJurusanSpId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RegistrasiPesertaDidikQuery The current query, for fluid interface
     */
    public function joinJurusanSpRelatedByJurusanSpId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('JurusanSpRelatedByJurusanSpId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'JurusanSpRelatedByJurusanSpId');
        }

        return $this;
    }

    /**
     * Use the JurusanSpRelatedByJurusanSpId relation JurusanSp object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\JurusanSpQuery A secondary query class using the current class as primary query
     */
    public function useJurusanSpRelatedByJurusanSpIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinJurusanSpRelatedByJurusanSpId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'JurusanSpRelatedByJurusanSpId', '\angulex\Model\JurusanSpQuery');
    }

    /**
     * Filter the query by a related JurusanSp object
     *
     * @param   JurusanSp|PropelObjectCollection $jurusanSp The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RegistrasiPesertaDidikQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJurusanSpRelatedByJurusanSpId($jurusanSp, $comparison = null)
    {
        if ($jurusanSp instanceof JurusanSp) {
            return $this
                ->addUsingAlias(RegistrasiPesertaDidikPeer::JURUSAN_SP_ID, $jurusanSp->getJurusanSpId(), $comparison);
        } elseif ($jurusanSp instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RegistrasiPesertaDidikPeer::JURUSAN_SP_ID, $jurusanSp->toKeyValue('PrimaryKey', 'JurusanSpId'), $comparison);
        } else {
            throw new PropelException('filterByJurusanSpRelatedByJurusanSpId() only accepts arguments of type JurusanSp or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the JurusanSpRelatedByJurusanSpId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RegistrasiPesertaDidikQuery The current query, for fluid interface
     */
    public function joinJurusanSpRelatedByJurusanSpId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('JurusanSpRelatedByJurusanSpId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'JurusanSpRelatedByJurusanSpId');
        }

        return $this;
    }

    /**
     * Use the JurusanSpRelatedByJurusanSpId relation JurusanSp object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\JurusanSpQuery A secondary query class using the current class as primary query
     */
    public function useJurusanSpRelatedByJurusanSpIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinJurusanSpRelatedByJurusanSpId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'JurusanSpRelatedByJurusanSpId', '\angulex\Model\JurusanSpQuery');
    }

    /**
     * Filter the query by a related PesertaDidik object
     *
     * @param   PesertaDidik|PropelObjectCollection $pesertaDidik The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RegistrasiPesertaDidikQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPesertaDidikRelatedByPesertaDidikId($pesertaDidik, $comparison = null)
    {
        if ($pesertaDidik instanceof PesertaDidik) {
            return $this
                ->addUsingAlias(RegistrasiPesertaDidikPeer::PESERTA_DIDIK_ID, $pesertaDidik->getPesertaDidikId(), $comparison);
        } elseif ($pesertaDidik instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RegistrasiPesertaDidikPeer::PESERTA_DIDIK_ID, $pesertaDidik->toKeyValue('PrimaryKey', 'PesertaDidikId'), $comparison);
        } else {
            throw new PropelException('filterByPesertaDidikRelatedByPesertaDidikId() only accepts arguments of type PesertaDidik or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PesertaDidikRelatedByPesertaDidikId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RegistrasiPesertaDidikQuery The current query, for fluid interface
     */
    public function joinPesertaDidikRelatedByPesertaDidikId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PesertaDidikRelatedByPesertaDidikId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PesertaDidikRelatedByPesertaDidikId');
        }

        return $this;
    }

    /**
     * Use the PesertaDidikRelatedByPesertaDidikId relation PesertaDidik object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PesertaDidikQuery A secondary query class using the current class as primary query
     */
    public function usePesertaDidikRelatedByPesertaDidikIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPesertaDidikRelatedByPesertaDidikId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PesertaDidikRelatedByPesertaDidikId', '\angulex\Model\PesertaDidikQuery');
    }

    /**
     * Filter the query by a related PesertaDidik object
     *
     * @param   PesertaDidik|PropelObjectCollection $pesertaDidik The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RegistrasiPesertaDidikQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPesertaDidikRelatedByPesertaDidikId($pesertaDidik, $comparison = null)
    {
        if ($pesertaDidik instanceof PesertaDidik) {
            return $this
                ->addUsingAlias(RegistrasiPesertaDidikPeer::PESERTA_DIDIK_ID, $pesertaDidik->getPesertaDidikId(), $comparison);
        } elseif ($pesertaDidik instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RegistrasiPesertaDidikPeer::PESERTA_DIDIK_ID, $pesertaDidik->toKeyValue('PrimaryKey', 'PesertaDidikId'), $comparison);
        } else {
            throw new PropelException('filterByPesertaDidikRelatedByPesertaDidikId() only accepts arguments of type PesertaDidik or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PesertaDidikRelatedByPesertaDidikId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RegistrasiPesertaDidikQuery The current query, for fluid interface
     */
    public function joinPesertaDidikRelatedByPesertaDidikId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PesertaDidikRelatedByPesertaDidikId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PesertaDidikRelatedByPesertaDidikId');
        }

        return $this;
    }

    /**
     * Use the PesertaDidikRelatedByPesertaDidikId relation PesertaDidik object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PesertaDidikQuery A secondary query class using the current class as primary query
     */
    public function usePesertaDidikRelatedByPesertaDidikIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPesertaDidikRelatedByPesertaDidikId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PesertaDidikRelatedByPesertaDidikId', '\angulex\Model\PesertaDidikQuery');
    }

    /**
     * Filter the query by a related Sekolah object
     *
     * @param   Sekolah|PropelObjectCollection $sekolah The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RegistrasiPesertaDidikQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySekolahRelatedBySekolahId($sekolah, $comparison = null)
    {
        if ($sekolah instanceof Sekolah) {
            return $this
                ->addUsingAlias(RegistrasiPesertaDidikPeer::SEKOLAH_ID, $sekolah->getSekolahId(), $comparison);
        } elseif ($sekolah instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RegistrasiPesertaDidikPeer::SEKOLAH_ID, $sekolah->toKeyValue('PrimaryKey', 'SekolahId'), $comparison);
        } else {
            throw new PropelException('filterBySekolahRelatedBySekolahId() only accepts arguments of type Sekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SekolahRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RegistrasiPesertaDidikQuery The current query, for fluid interface
     */
    public function joinSekolahRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SekolahRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SekolahRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the SekolahRelatedBySekolahId relation Sekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SekolahQuery A secondary query class using the current class as primary query
     */
    public function useSekolahRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSekolahRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SekolahRelatedBySekolahId', '\angulex\Model\SekolahQuery');
    }

    /**
     * Filter the query by a related Sekolah object
     *
     * @param   Sekolah|PropelObjectCollection $sekolah The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RegistrasiPesertaDidikQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySekolahRelatedBySekolahId($sekolah, $comparison = null)
    {
        if ($sekolah instanceof Sekolah) {
            return $this
                ->addUsingAlias(RegistrasiPesertaDidikPeer::SEKOLAH_ID, $sekolah->getSekolahId(), $comparison);
        } elseif ($sekolah instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RegistrasiPesertaDidikPeer::SEKOLAH_ID, $sekolah->toKeyValue('PrimaryKey', 'SekolahId'), $comparison);
        } else {
            throw new PropelException('filterBySekolahRelatedBySekolahId() only accepts arguments of type Sekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SekolahRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RegistrasiPesertaDidikQuery The current query, for fluid interface
     */
    public function joinSekolahRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SekolahRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SekolahRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the SekolahRelatedBySekolahId relation Sekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SekolahQuery A secondary query class using the current class as primary query
     */
    public function useSekolahRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSekolahRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SekolahRelatedBySekolahId', '\angulex\Model\SekolahQuery');
    }

    /**
     * Filter the query by a related JenisKeluar object
     *
     * @param   JenisKeluar|PropelObjectCollection $jenisKeluar The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RegistrasiPesertaDidikQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJenisKeluarRelatedByJenisKeluarId($jenisKeluar, $comparison = null)
    {
        if ($jenisKeluar instanceof JenisKeluar) {
            return $this
                ->addUsingAlias(RegistrasiPesertaDidikPeer::JENIS_KELUAR_ID, $jenisKeluar->getJenisKeluarId(), $comparison);
        } elseif ($jenisKeluar instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RegistrasiPesertaDidikPeer::JENIS_KELUAR_ID, $jenisKeluar->toKeyValue('PrimaryKey', 'JenisKeluarId'), $comparison);
        } else {
            throw new PropelException('filterByJenisKeluarRelatedByJenisKeluarId() only accepts arguments of type JenisKeluar or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the JenisKeluarRelatedByJenisKeluarId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RegistrasiPesertaDidikQuery The current query, for fluid interface
     */
    public function joinJenisKeluarRelatedByJenisKeluarId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('JenisKeluarRelatedByJenisKeluarId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'JenisKeluarRelatedByJenisKeluarId');
        }

        return $this;
    }

    /**
     * Use the JenisKeluarRelatedByJenisKeluarId relation JenisKeluar object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\JenisKeluarQuery A secondary query class using the current class as primary query
     */
    public function useJenisKeluarRelatedByJenisKeluarIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinJenisKeluarRelatedByJenisKeluarId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'JenisKeluarRelatedByJenisKeluarId', '\angulex\Model\JenisKeluarQuery');
    }

    /**
     * Filter the query by a related JenisKeluar object
     *
     * @param   JenisKeluar|PropelObjectCollection $jenisKeluar The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RegistrasiPesertaDidikQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJenisKeluarRelatedByJenisKeluarId($jenisKeluar, $comparison = null)
    {
        if ($jenisKeluar instanceof JenisKeluar) {
            return $this
                ->addUsingAlias(RegistrasiPesertaDidikPeer::JENIS_KELUAR_ID, $jenisKeluar->getJenisKeluarId(), $comparison);
        } elseif ($jenisKeluar instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RegistrasiPesertaDidikPeer::JENIS_KELUAR_ID, $jenisKeluar->toKeyValue('PrimaryKey', 'JenisKeluarId'), $comparison);
        } else {
            throw new PropelException('filterByJenisKeluarRelatedByJenisKeluarId() only accepts arguments of type JenisKeluar or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the JenisKeluarRelatedByJenisKeluarId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RegistrasiPesertaDidikQuery The current query, for fluid interface
     */
    public function joinJenisKeluarRelatedByJenisKeluarId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('JenisKeluarRelatedByJenisKeluarId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'JenisKeluarRelatedByJenisKeluarId');
        }

        return $this;
    }

    /**
     * Use the JenisKeluarRelatedByJenisKeluarId relation JenisKeluar object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\JenisKeluarQuery A secondary query class using the current class as primary query
     */
    public function useJenisKeluarRelatedByJenisKeluarIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinJenisKeluarRelatedByJenisKeluarId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'JenisKeluarRelatedByJenisKeluarId', '\angulex\Model\JenisKeluarQuery');
    }

    /**
     * Filter the query by a related JenisPendaftaran object
     *
     * @param   JenisPendaftaran|PropelObjectCollection $jenisPendaftaran The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RegistrasiPesertaDidikQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJenisPendaftaranRelatedByJenisPendaftaranId($jenisPendaftaran, $comparison = null)
    {
        if ($jenisPendaftaran instanceof JenisPendaftaran) {
            return $this
                ->addUsingAlias(RegistrasiPesertaDidikPeer::JENIS_PENDAFTARAN_ID, $jenisPendaftaran->getJenisPendaftaranId(), $comparison);
        } elseif ($jenisPendaftaran instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RegistrasiPesertaDidikPeer::JENIS_PENDAFTARAN_ID, $jenisPendaftaran->toKeyValue('PrimaryKey', 'JenisPendaftaranId'), $comparison);
        } else {
            throw new PropelException('filterByJenisPendaftaranRelatedByJenisPendaftaranId() only accepts arguments of type JenisPendaftaran or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the JenisPendaftaranRelatedByJenisPendaftaranId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RegistrasiPesertaDidikQuery The current query, for fluid interface
     */
    public function joinJenisPendaftaranRelatedByJenisPendaftaranId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('JenisPendaftaranRelatedByJenisPendaftaranId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'JenisPendaftaranRelatedByJenisPendaftaranId');
        }

        return $this;
    }

    /**
     * Use the JenisPendaftaranRelatedByJenisPendaftaranId relation JenisPendaftaran object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\JenisPendaftaranQuery A secondary query class using the current class as primary query
     */
    public function useJenisPendaftaranRelatedByJenisPendaftaranIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinJenisPendaftaranRelatedByJenisPendaftaranId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'JenisPendaftaranRelatedByJenisPendaftaranId', '\angulex\Model\JenisPendaftaranQuery');
    }

    /**
     * Filter the query by a related JenisPendaftaran object
     *
     * @param   JenisPendaftaran|PropelObjectCollection $jenisPendaftaran The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RegistrasiPesertaDidikQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJenisPendaftaranRelatedByJenisPendaftaranId($jenisPendaftaran, $comparison = null)
    {
        if ($jenisPendaftaran instanceof JenisPendaftaran) {
            return $this
                ->addUsingAlias(RegistrasiPesertaDidikPeer::JENIS_PENDAFTARAN_ID, $jenisPendaftaran->getJenisPendaftaranId(), $comparison);
        } elseif ($jenisPendaftaran instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RegistrasiPesertaDidikPeer::JENIS_PENDAFTARAN_ID, $jenisPendaftaran->toKeyValue('PrimaryKey', 'JenisPendaftaranId'), $comparison);
        } else {
            throw new PropelException('filterByJenisPendaftaranRelatedByJenisPendaftaranId() only accepts arguments of type JenisPendaftaran or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the JenisPendaftaranRelatedByJenisPendaftaranId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RegistrasiPesertaDidikQuery The current query, for fluid interface
     */
    public function joinJenisPendaftaranRelatedByJenisPendaftaranId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('JenisPendaftaranRelatedByJenisPendaftaranId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'JenisPendaftaranRelatedByJenisPendaftaranId');
        }

        return $this;
    }

    /**
     * Use the JenisPendaftaranRelatedByJenisPendaftaranId relation JenisPendaftaran object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\JenisPendaftaranQuery A secondary query class using the current class as primary query
     */
    public function useJenisPendaftaranRelatedByJenisPendaftaranIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinJenisPendaftaranRelatedByJenisPendaftaranId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'JenisPendaftaranRelatedByJenisPendaftaranId', '\angulex\Model\JenisPendaftaranQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   RegistrasiPesertaDidik $registrasiPesertaDidik Object to remove from the list of results
     *
     * @return RegistrasiPesertaDidikQuery The current query, for fluid interface
     */
    public function prune($registrasiPesertaDidik = null)
    {
        if ($registrasiPesertaDidik) {
            $this->addUsingAlias(RegistrasiPesertaDidikPeer::REGISTRASI_ID, $registrasiPesertaDidik->getRegistrasiId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
