<?php

namespace angulex\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\MataPelajaran;
use angulex\Model\Pembelajaran;
use angulex\Model\PembelajaranPeer;
use angulex\Model\PembelajaranQuery;
use angulex\Model\PtkTerdaftar;
use angulex\Model\RombonganBelajar;
use angulex\Model\Semester;
use angulex\Model\VldPembelajaran;

/**
 * Base class that represents a query for the 'pembelajaran' table.
 *
 * 
 *
 * @method PembelajaranQuery orderByPembelajaranId($order = Criteria::ASC) Order by the pembelajaran_id column
 * @method PembelajaranQuery orderByRombonganBelajarId($order = Criteria::ASC) Order by the rombongan_belajar_id column
 * @method PembelajaranQuery orderBySemesterId($order = Criteria::ASC) Order by the semester_id column
 * @method PembelajaranQuery orderByMataPelajaranId($order = Criteria::ASC) Order by the mata_pelajaran_id column
 * @method PembelajaranQuery orderByPtkTerdaftarId($order = Criteria::ASC) Order by the ptk_terdaftar_id column
 * @method PembelajaranQuery orderBySkMengajar($order = Criteria::ASC) Order by the sk_mengajar column
 * @method PembelajaranQuery orderByTanggalSkMengajar($order = Criteria::ASC) Order by the tanggal_sk_mengajar column
 * @method PembelajaranQuery orderByJamMengajarPerMinggu($order = Criteria::ASC) Order by the jam_mengajar_per_minggu column
 * @method PembelajaranQuery orderByStatusDiKurikulum($order = Criteria::ASC) Order by the status_di_kurikulum column
 * @method PembelajaranQuery orderByNamaMataPelajaran($order = Criteria::ASC) Order by the nama_mata_pelajaran column
 * @method PembelajaranQuery orderByLastUpdate($order = Criteria::ASC) Order by the Last_update column
 * @method PembelajaranQuery orderBySoftDelete($order = Criteria::ASC) Order by the Soft_delete column
 * @method PembelajaranQuery orderByLastSync($order = Criteria::ASC) Order by the last_sync column
 * @method PembelajaranQuery orderByUpdaterId($order = Criteria::ASC) Order by the Updater_ID column
 *
 * @method PembelajaranQuery groupByPembelajaranId() Group by the pembelajaran_id column
 * @method PembelajaranQuery groupByRombonganBelajarId() Group by the rombongan_belajar_id column
 * @method PembelajaranQuery groupBySemesterId() Group by the semester_id column
 * @method PembelajaranQuery groupByMataPelajaranId() Group by the mata_pelajaran_id column
 * @method PembelajaranQuery groupByPtkTerdaftarId() Group by the ptk_terdaftar_id column
 * @method PembelajaranQuery groupBySkMengajar() Group by the sk_mengajar column
 * @method PembelajaranQuery groupByTanggalSkMengajar() Group by the tanggal_sk_mengajar column
 * @method PembelajaranQuery groupByJamMengajarPerMinggu() Group by the jam_mengajar_per_minggu column
 * @method PembelajaranQuery groupByStatusDiKurikulum() Group by the status_di_kurikulum column
 * @method PembelajaranQuery groupByNamaMataPelajaran() Group by the nama_mata_pelajaran column
 * @method PembelajaranQuery groupByLastUpdate() Group by the Last_update column
 * @method PembelajaranQuery groupBySoftDelete() Group by the Soft_delete column
 * @method PembelajaranQuery groupByLastSync() Group by the last_sync column
 * @method PembelajaranQuery groupByUpdaterId() Group by the Updater_ID column
 *
 * @method PembelajaranQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method PembelajaranQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method PembelajaranQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method PembelajaranQuery leftJoinPtkTerdaftarRelatedByPtkTerdaftarId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PtkTerdaftarRelatedByPtkTerdaftarId relation
 * @method PembelajaranQuery rightJoinPtkTerdaftarRelatedByPtkTerdaftarId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PtkTerdaftarRelatedByPtkTerdaftarId relation
 * @method PembelajaranQuery innerJoinPtkTerdaftarRelatedByPtkTerdaftarId($relationAlias = null) Adds a INNER JOIN clause to the query using the PtkTerdaftarRelatedByPtkTerdaftarId relation
 *
 * @method PembelajaranQuery leftJoinPtkTerdaftarRelatedByPtkTerdaftarId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PtkTerdaftarRelatedByPtkTerdaftarId relation
 * @method PembelajaranQuery rightJoinPtkTerdaftarRelatedByPtkTerdaftarId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PtkTerdaftarRelatedByPtkTerdaftarId relation
 * @method PembelajaranQuery innerJoinPtkTerdaftarRelatedByPtkTerdaftarId($relationAlias = null) Adds a INNER JOIN clause to the query using the PtkTerdaftarRelatedByPtkTerdaftarId relation
 *
 * @method PembelajaranQuery leftJoinRombonganBelajarRelatedByRombonganBelajarId($relationAlias = null) Adds a LEFT JOIN clause to the query using the RombonganBelajarRelatedByRombonganBelajarId relation
 * @method PembelajaranQuery rightJoinRombonganBelajarRelatedByRombonganBelajarId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RombonganBelajarRelatedByRombonganBelajarId relation
 * @method PembelajaranQuery innerJoinRombonganBelajarRelatedByRombonganBelajarId($relationAlias = null) Adds a INNER JOIN clause to the query using the RombonganBelajarRelatedByRombonganBelajarId relation
 *
 * @method PembelajaranQuery leftJoinRombonganBelajarRelatedByRombonganBelajarId($relationAlias = null) Adds a LEFT JOIN clause to the query using the RombonganBelajarRelatedByRombonganBelajarId relation
 * @method PembelajaranQuery rightJoinRombonganBelajarRelatedByRombonganBelajarId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RombonganBelajarRelatedByRombonganBelajarId relation
 * @method PembelajaranQuery innerJoinRombonganBelajarRelatedByRombonganBelajarId($relationAlias = null) Adds a INNER JOIN clause to the query using the RombonganBelajarRelatedByRombonganBelajarId relation
 *
 * @method PembelajaranQuery leftJoinMataPelajaranRelatedByMataPelajaranId($relationAlias = null) Adds a LEFT JOIN clause to the query using the MataPelajaranRelatedByMataPelajaranId relation
 * @method PembelajaranQuery rightJoinMataPelajaranRelatedByMataPelajaranId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MataPelajaranRelatedByMataPelajaranId relation
 * @method PembelajaranQuery innerJoinMataPelajaranRelatedByMataPelajaranId($relationAlias = null) Adds a INNER JOIN clause to the query using the MataPelajaranRelatedByMataPelajaranId relation
 *
 * @method PembelajaranQuery leftJoinMataPelajaranRelatedByMataPelajaranId($relationAlias = null) Adds a LEFT JOIN clause to the query using the MataPelajaranRelatedByMataPelajaranId relation
 * @method PembelajaranQuery rightJoinMataPelajaranRelatedByMataPelajaranId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MataPelajaranRelatedByMataPelajaranId relation
 * @method PembelajaranQuery innerJoinMataPelajaranRelatedByMataPelajaranId($relationAlias = null) Adds a INNER JOIN clause to the query using the MataPelajaranRelatedByMataPelajaranId relation
 *
 * @method PembelajaranQuery leftJoinSemesterRelatedBySemesterId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SemesterRelatedBySemesterId relation
 * @method PembelajaranQuery rightJoinSemesterRelatedBySemesterId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SemesterRelatedBySemesterId relation
 * @method PembelajaranQuery innerJoinSemesterRelatedBySemesterId($relationAlias = null) Adds a INNER JOIN clause to the query using the SemesterRelatedBySemesterId relation
 *
 * @method PembelajaranQuery leftJoinSemesterRelatedBySemesterId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SemesterRelatedBySemesterId relation
 * @method PembelajaranQuery rightJoinSemesterRelatedBySemesterId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SemesterRelatedBySemesterId relation
 * @method PembelajaranQuery innerJoinSemesterRelatedBySemesterId($relationAlias = null) Adds a INNER JOIN clause to the query using the SemesterRelatedBySemesterId relation
 *
 * @method PembelajaranQuery leftJoinVldPembelajaranRelatedByPembelajaranId($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldPembelajaranRelatedByPembelajaranId relation
 * @method PembelajaranQuery rightJoinVldPembelajaranRelatedByPembelajaranId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldPembelajaranRelatedByPembelajaranId relation
 * @method PembelajaranQuery innerJoinVldPembelajaranRelatedByPembelajaranId($relationAlias = null) Adds a INNER JOIN clause to the query using the VldPembelajaranRelatedByPembelajaranId relation
 *
 * @method PembelajaranQuery leftJoinVldPembelajaranRelatedByPembelajaranId($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldPembelajaranRelatedByPembelajaranId relation
 * @method PembelajaranQuery rightJoinVldPembelajaranRelatedByPembelajaranId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldPembelajaranRelatedByPembelajaranId relation
 * @method PembelajaranQuery innerJoinVldPembelajaranRelatedByPembelajaranId($relationAlias = null) Adds a INNER JOIN clause to the query using the VldPembelajaranRelatedByPembelajaranId relation
 *
 * @method Pembelajaran findOne(PropelPDO $con = null) Return the first Pembelajaran matching the query
 * @method Pembelajaran findOneOrCreate(PropelPDO $con = null) Return the first Pembelajaran matching the query, or a new Pembelajaran object populated from the query conditions when no match is found
 *
 * @method Pembelajaran findOneByRombonganBelajarId(string $rombongan_belajar_id) Return the first Pembelajaran filtered by the rombongan_belajar_id column
 * @method Pembelajaran findOneBySemesterId(string $semester_id) Return the first Pembelajaran filtered by the semester_id column
 * @method Pembelajaran findOneByMataPelajaranId(int $mata_pelajaran_id) Return the first Pembelajaran filtered by the mata_pelajaran_id column
 * @method Pembelajaran findOneByPtkTerdaftarId(string $ptk_terdaftar_id) Return the first Pembelajaran filtered by the ptk_terdaftar_id column
 * @method Pembelajaran findOneBySkMengajar(string $sk_mengajar) Return the first Pembelajaran filtered by the sk_mengajar column
 * @method Pembelajaran findOneByTanggalSkMengajar(string $tanggal_sk_mengajar) Return the first Pembelajaran filtered by the tanggal_sk_mengajar column
 * @method Pembelajaran findOneByJamMengajarPerMinggu(string $jam_mengajar_per_minggu) Return the first Pembelajaran filtered by the jam_mengajar_per_minggu column
 * @method Pembelajaran findOneByStatusDiKurikulum(string $status_di_kurikulum) Return the first Pembelajaran filtered by the status_di_kurikulum column
 * @method Pembelajaran findOneByNamaMataPelajaran(string $nama_mata_pelajaran) Return the first Pembelajaran filtered by the nama_mata_pelajaran column
 * @method Pembelajaran findOneByLastUpdate(string $Last_update) Return the first Pembelajaran filtered by the Last_update column
 * @method Pembelajaran findOneBySoftDelete(string $Soft_delete) Return the first Pembelajaran filtered by the Soft_delete column
 * @method Pembelajaran findOneByLastSync(string $last_sync) Return the first Pembelajaran filtered by the last_sync column
 * @method Pembelajaran findOneByUpdaterId(string $Updater_ID) Return the first Pembelajaran filtered by the Updater_ID column
 *
 * @method array findByPembelajaranId(string $pembelajaran_id) Return Pembelajaran objects filtered by the pembelajaran_id column
 * @method array findByRombonganBelajarId(string $rombongan_belajar_id) Return Pembelajaran objects filtered by the rombongan_belajar_id column
 * @method array findBySemesterId(string $semester_id) Return Pembelajaran objects filtered by the semester_id column
 * @method array findByMataPelajaranId(int $mata_pelajaran_id) Return Pembelajaran objects filtered by the mata_pelajaran_id column
 * @method array findByPtkTerdaftarId(string $ptk_terdaftar_id) Return Pembelajaran objects filtered by the ptk_terdaftar_id column
 * @method array findBySkMengajar(string $sk_mengajar) Return Pembelajaran objects filtered by the sk_mengajar column
 * @method array findByTanggalSkMengajar(string $tanggal_sk_mengajar) Return Pembelajaran objects filtered by the tanggal_sk_mengajar column
 * @method array findByJamMengajarPerMinggu(string $jam_mengajar_per_minggu) Return Pembelajaran objects filtered by the jam_mengajar_per_minggu column
 * @method array findByStatusDiKurikulum(string $status_di_kurikulum) Return Pembelajaran objects filtered by the status_di_kurikulum column
 * @method array findByNamaMataPelajaran(string $nama_mata_pelajaran) Return Pembelajaran objects filtered by the nama_mata_pelajaran column
 * @method array findByLastUpdate(string $Last_update) Return Pembelajaran objects filtered by the Last_update column
 * @method array findBySoftDelete(string $Soft_delete) Return Pembelajaran objects filtered by the Soft_delete column
 * @method array findByLastSync(string $last_sync) Return Pembelajaran objects filtered by the last_sync column
 * @method array findByUpdaterId(string $Updater_ID) Return Pembelajaran objects filtered by the Updater_ID column
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BasePembelajaranQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BasePembelajaranQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Dapodikmen', $modelName = 'angulex\\Model\\Pembelajaran', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new PembelajaranQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   PembelajaranQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return PembelajaranQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof PembelajaranQuery) {
            return $criteria;
        }
        $query = new PembelajaranQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Pembelajaran|Pembelajaran[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = PembelajaranPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(PembelajaranPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Pembelajaran A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByPembelajaranId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Pembelajaran A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [pembelajaran_id], [rombongan_belajar_id], [semester_id], [mata_pelajaran_id], [ptk_terdaftar_id], [sk_mengajar], [tanggal_sk_mengajar], [jam_mengajar_per_minggu], [status_di_kurikulum], [nama_mata_pelajaran], [Last_update], [Soft_delete], [last_sync], [Updater_ID] FROM [pembelajaran] WHERE [pembelajaran_id] = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Pembelajaran();
            $obj->hydrate($row);
            PembelajaranPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Pembelajaran|Pembelajaran[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Pembelajaran[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return PembelajaranQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PembelajaranPeer::PEMBELAJARAN_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return PembelajaranQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PembelajaranPeer::PEMBELAJARAN_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the pembelajaran_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPembelajaranId('fooValue');   // WHERE pembelajaran_id = 'fooValue'
     * $query->filterByPembelajaranId('%fooValue%'); // WHERE pembelajaran_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $pembelajaranId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PembelajaranQuery The current query, for fluid interface
     */
    public function filterByPembelajaranId($pembelajaranId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($pembelajaranId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $pembelajaranId)) {
                $pembelajaranId = str_replace('*', '%', $pembelajaranId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PembelajaranPeer::PEMBELAJARAN_ID, $pembelajaranId, $comparison);
    }

    /**
     * Filter the query on the rombongan_belajar_id column
     *
     * Example usage:
     * <code>
     * $query->filterByRombonganBelajarId('fooValue');   // WHERE rombongan_belajar_id = 'fooValue'
     * $query->filterByRombonganBelajarId('%fooValue%'); // WHERE rombongan_belajar_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $rombonganBelajarId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PembelajaranQuery The current query, for fluid interface
     */
    public function filterByRombonganBelajarId($rombonganBelajarId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($rombonganBelajarId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $rombonganBelajarId)) {
                $rombonganBelajarId = str_replace('*', '%', $rombonganBelajarId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PembelajaranPeer::ROMBONGAN_BELAJAR_ID, $rombonganBelajarId, $comparison);
    }

    /**
     * Filter the query on the semester_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySemesterId('fooValue');   // WHERE semester_id = 'fooValue'
     * $query->filterBySemesterId('%fooValue%'); // WHERE semester_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $semesterId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PembelajaranQuery The current query, for fluid interface
     */
    public function filterBySemesterId($semesterId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($semesterId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $semesterId)) {
                $semesterId = str_replace('*', '%', $semesterId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PembelajaranPeer::SEMESTER_ID, $semesterId, $comparison);
    }

    /**
     * Filter the query on the mata_pelajaran_id column
     *
     * Example usage:
     * <code>
     * $query->filterByMataPelajaranId(1234); // WHERE mata_pelajaran_id = 1234
     * $query->filterByMataPelajaranId(array(12, 34)); // WHERE mata_pelajaran_id IN (12, 34)
     * $query->filterByMataPelajaranId(array('min' => 12)); // WHERE mata_pelajaran_id >= 12
     * $query->filterByMataPelajaranId(array('max' => 12)); // WHERE mata_pelajaran_id <= 12
     * </code>
     *
     * @see       filterByMataPelajaranRelatedByMataPelajaranId()
     *
     * @see       filterByMataPelajaranRelatedByMataPelajaranId()
     *
     * @param     mixed $mataPelajaranId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PembelajaranQuery The current query, for fluid interface
     */
    public function filterByMataPelajaranId($mataPelajaranId = null, $comparison = null)
    {
        if (is_array($mataPelajaranId)) {
            $useMinMax = false;
            if (isset($mataPelajaranId['min'])) {
                $this->addUsingAlias(PembelajaranPeer::MATA_PELAJARAN_ID, $mataPelajaranId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($mataPelajaranId['max'])) {
                $this->addUsingAlias(PembelajaranPeer::MATA_PELAJARAN_ID, $mataPelajaranId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PembelajaranPeer::MATA_PELAJARAN_ID, $mataPelajaranId, $comparison);
    }

    /**
     * Filter the query on the ptk_terdaftar_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPtkTerdaftarId('fooValue');   // WHERE ptk_terdaftar_id = 'fooValue'
     * $query->filterByPtkTerdaftarId('%fooValue%'); // WHERE ptk_terdaftar_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ptkTerdaftarId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PembelajaranQuery The current query, for fluid interface
     */
    public function filterByPtkTerdaftarId($ptkTerdaftarId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ptkTerdaftarId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ptkTerdaftarId)) {
                $ptkTerdaftarId = str_replace('*', '%', $ptkTerdaftarId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PembelajaranPeer::PTK_TERDAFTAR_ID, $ptkTerdaftarId, $comparison);
    }

    /**
     * Filter the query on the sk_mengajar column
     *
     * Example usage:
     * <code>
     * $query->filterBySkMengajar('fooValue');   // WHERE sk_mengajar = 'fooValue'
     * $query->filterBySkMengajar('%fooValue%'); // WHERE sk_mengajar LIKE '%fooValue%'
     * </code>
     *
     * @param     string $skMengajar The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PembelajaranQuery The current query, for fluid interface
     */
    public function filterBySkMengajar($skMengajar = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($skMengajar)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $skMengajar)) {
                $skMengajar = str_replace('*', '%', $skMengajar);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PembelajaranPeer::SK_MENGAJAR, $skMengajar, $comparison);
    }

    /**
     * Filter the query on the tanggal_sk_mengajar column
     *
     * Example usage:
     * <code>
     * $query->filterByTanggalSkMengajar('fooValue');   // WHERE tanggal_sk_mengajar = 'fooValue'
     * $query->filterByTanggalSkMengajar('%fooValue%'); // WHERE tanggal_sk_mengajar LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tanggalSkMengajar The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PembelajaranQuery The current query, for fluid interface
     */
    public function filterByTanggalSkMengajar($tanggalSkMengajar = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tanggalSkMengajar)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tanggalSkMengajar)) {
                $tanggalSkMengajar = str_replace('*', '%', $tanggalSkMengajar);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PembelajaranPeer::TANGGAL_SK_MENGAJAR, $tanggalSkMengajar, $comparison);
    }

    /**
     * Filter the query on the jam_mengajar_per_minggu column
     *
     * Example usage:
     * <code>
     * $query->filterByJamMengajarPerMinggu(1234); // WHERE jam_mengajar_per_minggu = 1234
     * $query->filterByJamMengajarPerMinggu(array(12, 34)); // WHERE jam_mengajar_per_minggu IN (12, 34)
     * $query->filterByJamMengajarPerMinggu(array('min' => 12)); // WHERE jam_mengajar_per_minggu >= 12
     * $query->filterByJamMengajarPerMinggu(array('max' => 12)); // WHERE jam_mengajar_per_minggu <= 12
     * </code>
     *
     * @param     mixed $jamMengajarPerMinggu The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PembelajaranQuery The current query, for fluid interface
     */
    public function filterByJamMengajarPerMinggu($jamMengajarPerMinggu = null, $comparison = null)
    {
        if (is_array($jamMengajarPerMinggu)) {
            $useMinMax = false;
            if (isset($jamMengajarPerMinggu['min'])) {
                $this->addUsingAlias(PembelajaranPeer::JAM_MENGAJAR_PER_MINGGU, $jamMengajarPerMinggu['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jamMengajarPerMinggu['max'])) {
                $this->addUsingAlias(PembelajaranPeer::JAM_MENGAJAR_PER_MINGGU, $jamMengajarPerMinggu['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PembelajaranPeer::JAM_MENGAJAR_PER_MINGGU, $jamMengajarPerMinggu, $comparison);
    }

    /**
     * Filter the query on the status_di_kurikulum column
     *
     * Example usage:
     * <code>
     * $query->filterByStatusDiKurikulum(1234); // WHERE status_di_kurikulum = 1234
     * $query->filterByStatusDiKurikulum(array(12, 34)); // WHERE status_di_kurikulum IN (12, 34)
     * $query->filterByStatusDiKurikulum(array('min' => 12)); // WHERE status_di_kurikulum >= 12
     * $query->filterByStatusDiKurikulum(array('max' => 12)); // WHERE status_di_kurikulum <= 12
     * </code>
     *
     * @param     mixed $statusDiKurikulum The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PembelajaranQuery The current query, for fluid interface
     */
    public function filterByStatusDiKurikulum($statusDiKurikulum = null, $comparison = null)
    {
        if (is_array($statusDiKurikulum)) {
            $useMinMax = false;
            if (isset($statusDiKurikulum['min'])) {
                $this->addUsingAlias(PembelajaranPeer::STATUS_DI_KURIKULUM, $statusDiKurikulum['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($statusDiKurikulum['max'])) {
                $this->addUsingAlias(PembelajaranPeer::STATUS_DI_KURIKULUM, $statusDiKurikulum['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PembelajaranPeer::STATUS_DI_KURIKULUM, $statusDiKurikulum, $comparison);
    }

    /**
     * Filter the query on the nama_mata_pelajaran column
     *
     * Example usage:
     * <code>
     * $query->filterByNamaMataPelajaran('fooValue');   // WHERE nama_mata_pelajaran = 'fooValue'
     * $query->filterByNamaMataPelajaran('%fooValue%'); // WHERE nama_mata_pelajaran LIKE '%fooValue%'
     * </code>
     *
     * @param     string $namaMataPelajaran The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PembelajaranQuery The current query, for fluid interface
     */
    public function filterByNamaMataPelajaran($namaMataPelajaran = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($namaMataPelajaran)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $namaMataPelajaran)) {
                $namaMataPelajaran = str_replace('*', '%', $namaMataPelajaran);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PembelajaranPeer::NAMA_MATA_PELAJARAN, $namaMataPelajaran, $comparison);
    }

    /**
     * Filter the query on the Last_update column
     *
     * Example usage:
     * <code>
     * $query->filterByLastUpdate('2011-03-14'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate('now'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate(array('max' => 'yesterday')); // WHERE Last_update > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastUpdate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PembelajaranQuery The current query, for fluid interface
     */
    public function filterByLastUpdate($lastUpdate = null, $comparison = null)
    {
        if (is_array($lastUpdate)) {
            $useMinMax = false;
            if (isset($lastUpdate['min'])) {
                $this->addUsingAlias(PembelajaranPeer::LAST_UPDATE, $lastUpdate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastUpdate['max'])) {
                $this->addUsingAlias(PembelajaranPeer::LAST_UPDATE, $lastUpdate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PembelajaranPeer::LAST_UPDATE, $lastUpdate, $comparison);
    }

    /**
     * Filter the query on the Soft_delete column
     *
     * Example usage:
     * <code>
     * $query->filterBySoftDelete(1234); // WHERE Soft_delete = 1234
     * $query->filterBySoftDelete(array(12, 34)); // WHERE Soft_delete IN (12, 34)
     * $query->filterBySoftDelete(array('min' => 12)); // WHERE Soft_delete >= 12
     * $query->filterBySoftDelete(array('max' => 12)); // WHERE Soft_delete <= 12
     * </code>
     *
     * @param     mixed $softDelete The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PembelajaranQuery The current query, for fluid interface
     */
    public function filterBySoftDelete($softDelete = null, $comparison = null)
    {
        if (is_array($softDelete)) {
            $useMinMax = false;
            if (isset($softDelete['min'])) {
                $this->addUsingAlias(PembelajaranPeer::SOFT_DELETE, $softDelete['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($softDelete['max'])) {
                $this->addUsingAlias(PembelajaranPeer::SOFT_DELETE, $softDelete['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PembelajaranPeer::SOFT_DELETE, $softDelete, $comparison);
    }

    /**
     * Filter the query on the last_sync column
     *
     * Example usage:
     * <code>
     * $query->filterByLastSync('2011-03-14'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync('now'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync(array('max' => 'yesterday')); // WHERE last_sync > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastSync The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PembelajaranQuery The current query, for fluid interface
     */
    public function filterByLastSync($lastSync = null, $comparison = null)
    {
        if (is_array($lastSync)) {
            $useMinMax = false;
            if (isset($lastSync['min'])) {
                $this->addUsingAlias(PembelajaranPeer::LAST_SYNC, $lastSync['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastSync['max'])) {
                $this->addUsingAlias(PembelajaranPeer::LAST_SYNC, $lastSync['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PembelajaranPeer::LAST_SYNC, $lastSync, $comparison);
    }

    /**
     * Filter the query on the Updater_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdaterId('fooValue');   // WHERE Updater_ID = 'fooValue'
     * $query->filterByUpdaterId('%fooValue%'); // WHERE Updater_ID LIKE '%fooValue%'
     * </code>
     *
     * @param     string $updaterId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PembelajaranQuery The current query, for fluid interface
     */
    public function filterByUpdaterId($updaterId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($updaterId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $updaterId)) {
                $updaterId = str_replace('*', '%', $updaterId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PembelajaranPeer::UPDATER_ID, $updaterId, $comparison);
    }

    /**
     * Filter the query by a related PtkTerdaftar object
     *
     * @param   PtkTerdaftar|PropelObjectCollection $ptkTerdaftar The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PembelajaranQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPtkTerdaftarRelatedByPtkTerdaftarId($ptkTerdaftar, $comparison = null)
    {
        if ($ptkTerdaftar instanceof PtkTerdaftar) {
            return $this
                ->addUsingAlias(PembelajaranPeer::PTK_TERDAFTAR_ID, $ptkTerdaftar->getPtkTerdaftarId(), $comparison);
        } elseif ($ptkTerdaftar instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PembelajaranPeer::PTK_TERDAFTAR_ID, $ptkTerdaftar->toKeyValue('PrimaryKey', 'PtkTerdaftarId'), $comparison);
        } else {
            throw new PropelException('filterByPtkTerdaftarRelatedByPtkTerdaftarId() only accepts arguments of type PtkTerdaftar or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PtkTerdaftarRelatedByPtkTerdaftarId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PembelajaranQuery The current query, for fluid interface
     */
    public function joinPtkTerdaftarRelatedByPtkTerdaftarId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PtkTerdaftarRelatedByPtkTerdaftarId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PtkTerdaftarRelatedByPtkTerdaftarId');
        }

        return $this;
    }

    /**
     * Use the PtkTerdaftarRelatedByPtkTerdaftarId relation PtkTerdaftar object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PtkTerdaftarQuery A secondary query class using the current class as primary query
     */
    public function usePtkTerdaftarRelatedByPtkTerdaftarIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPtkTerdaftarRelatedByPtkTerdaftarId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PtkTerdaftarRelatedByPtkTerdaftarId', '\angulex\Model\PtkTerdaftarQuery');
    }

    /**
     * Filter the query by a related PtkTerdaftar object
     *
     * @param   PtkTerdaftar|PropelObjectCollection $ptkTerdaftar The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PembelajaranQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPtkTerdaftarRelatedByPtkTerdaftarId($ptkTerdaftar, $comparison = null)
    {
        if ($ptkTerdaftar instanceof PtkTerdaftar) {
            return $this
                ->addUsingAlias(PembelajaranPeer::PTK_TERDAFTAR_ID, $ptkTerdaftar->getPtkTerdaftarId(), $comparison);
        } elseif ($ptkTerdaftar instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PembelajaranPeer::PTK_TERDAFTAR_ID, $ptkTerdaftar->toKeyValue('PrimaryKey', 'PtkTerdaftarId'), $comparison);
        } else {
            throw new PropelException('filterByPtkTerdaftarRelatedByPtkTerdaftarId() only accepts arguments of type PtkTerdaftar or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PtkTerdaftarRelatedByPtkTerdaftarId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PembelajaranQuery The current query, for fluid interface
     */
    public function joinPtkTerdaftarRelatedByPtkTerdaftarId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PtkTerdaftarRelatedByPtkTerdaftarId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PtkTerdaftarRelatedByPtkTerdaftarId');
        }

        return $this;
    }

    /**
     * Use the PtkTerdaftarRelatedByPtkTerdaftarId relation PtkTerdaftar object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PtkTerdaftarQuery A secondary query class using the current class as primary query
     */
    public function usePtkTerdaftarRelatedByPtkTerdaftarIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPtkTerdaftarRelatedByPtkTerdaftarId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PtkTerdaftarRelatedByPtkTerdaftarId', '\angulex\Model\PtkTerdaftarQuery');
    }

    /**
     * Filter the query by a related RombonganBelajar object
     *
     * @param   RombonganBelajar|PropelObjectCollection $rombonganBelajar The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PembelajaranQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRombonganBelajarRelatedByRombonganBelajarId($rombonganBelajar, $comparison = null)
    {
        if ($rombonganBelajar instanceof RombonganBelajar) {
            return $this
                ->addUsingAlias(PembelajaranPeer::ROMBONGAN_BELAJAR_ID, $rombonganBelajar->getRombonganBelajarId(), $comparison);
        } elseif ($rombonganBelajar instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PembelajaranPeer::ROMBONGAN_BELAJAR_ID, $rombonganBelajar->toKeyValue('PrimaryKey', 'RombonganBelajarId'), $comparison);
        } else {
            throw new PropelException('filterByRombonganBelajarRelatedByRombonganBelajarId() only accepts arguments of type RombonganBelajar or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RombonganBelajarRelatedByRombonganBelajarId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PembelajaranQuery The current query, for fluid interface
     */
    public function joinRombonganBelajarRelatedByRombonganBelajarId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RombonganBelajarRelatedByRombonganBelajarId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RombonganBelajarRelatedByRombonganBelajarId');
        }

        return $this;
    }

    /**
     * Use the RombonganBelajarRelatedByRombonganBelajarId relation RombonganBelajar object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\RombonganBelajarQuery A secondary query class using the current class as primary query
     */
    public function useRombonganBelajarRelatedByRombonganBelajarIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRombonganBelajarRelatedByRombonganBelajarId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RombonganBelajarRelatedByRombonganBelajarId', '\angulex\Model\RombonganBelajarQuery');
    }

    /**
     * Filter the query by a related RombonganBelajar object
     *
     * @param   RombonganBelajar|PropelObjectCollection $rombonganBelajar The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PembelajaranQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRombonganBelajarRelatedByRombonganBelajarId($rombonganBelajar, $comparison = null)
    {
        if ($rombonganBelajar instanceof RombonganBelajar) {
            return $this
                ->addUsingAlias(PembelajaranPeer::ROMBONGAN_BELAJAR_ID, $rombonganBelajar->getRombonganBelajarId(), $comparison);
        } elseif ($rombonganBelajar instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PembelajaranPeer::ROMBONGAN_BELAJAR_ID, $rombonganBelajar->toKeyValue('PrimaryKey', 'RombonganBelajarId'), $comparison);
        } else {
            throw new PropelException('filterByRombonganBelajarRelatedByRombonganBelajarId() only accepts arguments of type RombonganBelajar or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RombonganBelajarRelatedByRombonganBelajarId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PembelajaranQuery The current query, for fluid interface
     */
    public function joinRombonganBelajarRelatedByRombonganBelajarId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RombonganBelajarRelatedByRombonganBelajarId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RombonganBelajarRelatedByRombonganBelajarId');
        }

        return $this;
    }

    /**
     * Use the RombonganBelajarRelatedByRombonganBelajarId relation RombonganBelajar object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\RombonganBelajarQuery A secondary query class using the current class as primary query
     */
    public function useRombonganBelajarRelatedByRombonganBelajarIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRombonganBelajarRelatedByRombonganBelajarId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RombonganBelajarRelatedByRombonganBelajarId', '\angulex\Model\RombonganBelajarQuery');
    }

    /**
     * Filter the query by a related MataPelajaran object
     *
     * @param   MataPelajaran|PropelObjectCollection $mataPelajaran The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PembelajaranQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByMataPelajaranRelatedByMataPelajaranId($mataPelajaran, $comparison = null)
    {
        if ($mataPelajaran instanceof MataPelajaran) {
            return $this
                ->addUsingAlias(PembelajaranPeer::MATA_PELAJARAN_ID, $mataPelajaran->getMataPelajaranId(), $comparison);
        } elseif ($mataPelajaran instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PembelajaranPeer::MATA_PELAJARAN_ID, $mataPelajaran->toKeyValue('PrimaryKey', 'MataPelajaranId'), $comparison);
        } else {
            throw new PropelException('filterByMataPelajaranRelatedByMataPelajaranId() only accepts arguments of type MataPelajaran or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MataPelajaranRelatedByMataPelajaranId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PembelajaranQuery The current query, for fluid interface
     */
    public function joinMataPelajaranRelatedByMataPelajaranId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MataPelajaranRelatedByMataPelajaranId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MataPelajaranRelatedByMataPelajaranId');
        }

        return $this;
    }

    /**
     * Use the MataPelajaranRelatedByMataPelajaranId relation MataPelajaran object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\MataPelajaranQuery A secondary query class using the current class as primary query
     */
    public function useMataPelajaranRelatedByMataPelajaranIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMataPelajaranRelatedByMataPelajaranId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MataPelajaranRelatedByMataPelajaranId', '\angulex\Model\MataPelajaranQuery');
    }

    /**
     * Filter the query by a related MataPelajaran object
     *
     * @param   MataPelajaran|PropelObjectCollection $mataPelajaran The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PembelajaranQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByMataPelajaranRelatedByMataPelajaranId($mataPelajaran, $comparison = null)
    {
        if ($mataPelajaran instanceof MataPelajaran) {
            return $this
                ->addUsingAlias(PembelajaranPeer::MATA_PELAJARAN_ID, $mataPelajaran->getMataPelajaranId(), $comparison);
        } elseif ($mataPelajaran instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PembelajaranPeer::MATA_PELAJARAN_ID, $mataPelajaran->toKeyValue('PrimaryKey', 'MataPelajaranId'), $comparison);
        } else {
            throw new PropelException('filterByMataPelajaranRelatedByMataPelajaranId() only accepts arguments of type MataPelajaran or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MataPelajaranRelatedByMataPelajaranId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PembelajaranQuery The current query, for fluid interface
     */
    public function joinMataPelajaranRelatedByMataPelajaranId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MataPelajaranRelatedByMataPelajaranId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MataPelajaranRelatedByMataPelajaranId');
        }

        return $this;
    }

    /**
     * Use the MataPelajaranRelatedByMataPelajaranId relation MataPelajaran object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\MataPelajaranQuery A secondary query class using the current class as primary query
     */
    public function useMataPelajaranRelatedByMataPelajaranIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMataPelajaranRelatedByMataPelajaranId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MataPelajaranRelatedByMataPelajaranId', '\angulex\Model\MataPelajaranQuery');
    }

    /**
     * Filter the query by a related Semester object
     *
     * @param   Semester|PropelObjectCollection $semester The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PembelajaranQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySemesterRelatedBySemesterId($semester, $comparison = null)
    {
        if ($semester instanceof Semester) {
            return $this
                ->addUsingAlias(PembelajaranPeer::SEMESTER_ID, $semester->getSemesterId(), $comparison);
        } elseif ($semester instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PembelajaranPeer::SEMESTER_ID, $semester->toKeyValue('PrimaryKey', 'SemesterId'), $comparison);
        } else {
            throw new PropelException('filterBySemesterRelatedBySemesterId() only accepts arguments of type Semester or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SemesterRelatedBySemesterId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PembelajaranQuery The current query, for fluid interface
     */
    public function joinSemesterRelatedBySemesterId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SemesterRelatedBySemesterId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SemesterRelatedBySemesterId');
        }

        return $this;
    }

    /**
     * Use the SemesterRelatedBySemesterId relation Semester object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SemesterQuery A secondary query class using the current class as primary query
     */
    public function useSemesterRelatedBySemesterIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSemesterRelatedBySemesterId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SemesterRelatedBySemesterId', '\angulex\Model\SemesterQuery');
    }

    /**
     * Filter the query by a related Semester object
     *
     * @param   Semester|PropelObjectCollection $semester The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PembelajaranQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySemesterRelatedBySemesterId($semester, $comparison = null)
    {
        if ($semester instanceof Semester) {
            return $this
                ->addUsingAlias(PembelajaranPeer::SEMESTER_ID, $semester->getSemesterId(), $comparison);
        } elseif ($semester instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PembelajaranPeer::SEMESTER_ID, $semester->toKeyValue('PrimaryKey', 'SemesterId'), $comparison);
        } else {
            throw new PropelException('filterBySemesterRelatedBySemesterId() only accepts arguments of type Semester or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SemesterRelatedBySemesterId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PembelajaranQuery The current query, for fluid interface
     */
    public function joinSemesterRelatedBySemesterId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SemesterRelatedBySemesterId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SemesterRelatedBySemesterId');
        }

        return $this;
    }

    /**
     * Use the SemesterRelatedBySemesterId relation Semester object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SemesterQuery A secondary query class using the current class as primary query
     */
    public function useSemesterRelatedBySemesterIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSemesterRelatedBySemesterId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SemesterRelatedBySemesterId', '\angulex\Model\SemesterQuery');
    }

    /**
     * Filter the query by a related VldPembelajaran object
     *
     * @param   VldPembelajaran|PropelObjectCollection $vldPembelajaran  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PembelajaranQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldPembelajaranRelatedByPembelajaranId($vldPembelajaran, $comparison = null)
    {
        if ($vldPembelajaran instanceof VldPembelajaran) {
            return $this
                ->addUsingAlias(PembelajaranPeer::PEMBELAJARAN_ID, $vldPembelajaran->getPembelajaranId(), $comparison);
        } elseif ($vldPembelajaran instanceof PropelObjectCollection) {
            return $this
                ->useVldPembelajaranRelatedByPembelajaranIdQuery()
                ->filterByPrimaryKeys($vldPembelajaran->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldPembelajaranRelatedByPembelajaranId() only accepts arguments of type VldPembelajaran or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldPembelajaranRelatedByPembelajaranId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PembelajaranQuery The current query, for fluid interface
     */
    public function joinVldPembelajaranRelatedByPembelajaranId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldPembelajaranRelatedByPembelajaranId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldPembelajaranRelatedByPembelajaranId');
        }

        return $this;
    }

    /**
     * Use the VldPembelajaranRelatedByPembelajaranId relation VldPembelajaran object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldPembelajaranQuery A secondary query class using the current class as primary query
     */
    public function useVldPembelajaranRelatedByPembelajaranIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldPembelajaranRelatedByPembelajaranId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldPembelajaranRelatedByPembelajaranId', '\angulex\Model\VldPembelajaranQuery');
    }

    /**
     * Filter the query by a related VldPembelajaran object
     *
     * @param   VldPembelajaran|PropelObjectCollection $vldPembelajaran  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PembelajaranQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldPembelajaranRelatedByPembelajaranId($vldPembelajaran, $comparison = null)
    {
        if ($vldPembelajaran instanceof VldPembelajaran) {
            return $this
                ->addUsingAlias(PembelajaranPeer::PEMBELAJARAN_ID, $vldPembelajaran->getPembelajaranId(), $comparison);
        } elseif ($vldPembelajaran instanceof PropelObjectCollection) {
            return $this
                ->useVldPembelajaranRelatedByPembelajaranIdQuery()
                ->filterByPrimaryKeys($vldPembelajaran->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldPembelajaranRelatedByPembelajaranId() only accepts arguments of type VldPembelajaran or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldPembelajaranRelatedByPembelajaranId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PembelajaranQuery The current query, for fluid interface
     */
    public function joinVldPembelajaranRelatedByPembelajaranId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldPembelajaranRelatedByPembelajaranId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldPembelajaranRelatedByPembelajaranId');
        }

        return $this;
    }

    /**
     * Use the VldPembelajaranRelatedByPembelajaranId relation VldPembelajaran object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldPembelajaranQuery A secondary query class using the current class as primary query
     */
    public function useVldPembelajaranRelatedByPembelajaranIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldPembelajaranRelatedByPembelajaranId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldPembelajaranRelatedByPembelajaranId', '\angulex\Model\VldPembelajaranQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Pembelajaran $pembelajaran Object to remove from the list of results
     *
     * @return PembelajaranQuery The current query, for fluid interface
     */
    public function prune($pembelajaran = null)
    {
        if ($pembelajaran) {
            $this->addUsingAlias(PembelajaranPeer::PEMBELAJARAN_ID, $pembelajaran->getPembelajaranId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
