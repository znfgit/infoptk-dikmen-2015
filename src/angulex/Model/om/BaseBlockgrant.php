<?php

namespace angulex\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelDateTime;
use \PropelException;
use \PropelPDO;
use angulex\Model\Blockgrant;
use angulex\Model\BlockgrantPeer;
use angulex\Model\BlockgrantQuery;
use angulex\Model\JenisBantuan;
use angulex\Model\JenisBantuanQuery;
use angulex\Model\Sekolah;
use angulex\Model\SekolahQuery;
use angulex\Model\SumberDana;
use angulex\Model\SumberDanaQuery;

/**
 * Base class that represents a row from the 'blockgrant' table.
 *
 * 
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseBlockgrant extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'angulex\\Model\\BlockgrantPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        BlockgrantPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the blockgrant_id field.
     * @var        string
     */
    protected $blockgrant_id;

    /**
     * The value for the sekolah_id field.
     * @var        string
     */
    protected $sekolah_id;

    /**
     * The value for the nama field.
     * @var        string
     */
    protected $nama;

    /**
     * The value for the tahun field.
     * @var        string
     */
    protected $tahun;

    /**
     * The value for the jenis_bantuan_id field.
     * @var        int
     */
    protected $jenis_bantuan_id;

    /**
     * The value for the sumber_dana_id field.
     * @var        string
     */
    protected $sumber_dana_id;

    /**
     * The value for the besar_bantuan field.
     * @var        string
     */
    protected $besar_bantuan;

    /**
     * The value for the dana_pendamping field.
     * @var        string
     */
    protected $dana_pendamping;

    /**
     * The value for the peruntukan_dana field.
     * @var        string
     */
    protected $peruntukan_dana;

    /**
     * The value for the asal_data field.
     * @var        string
     */
    protected $asal_data;

    /**
     * The value for the last_update field.
     * @var        string
     */
    protected $last_update;

    /**
     * The value for the soft_delete field.
     * @var        string
     */
    protected $soft_delete;

    /**
     * The value for the last_sync field.
     * @var        string
     */
    protected $last_sync;

    /**
     * The value for the updater_id field.
     * @var        string
     */
    protected $updater_id;

    /**
     * @var        Sekolah
     */
    protected $aSekolahRelatedBySekolahId;

    /**
     * @var        Sekolah
     */
    protected $aSekolahRelatedBySekolahId;

    /**
     * @var        JenisBantuan
     */
    protected $aJenisBantuanRelatedByJenisBantuanId;

    /**
     * @var        JenisBantuan
     */
    protected $aJenisBantuanRelatedByJenisBantuanId;

    /**
     * @var        SumberDana
     */
    protected $aSumberDanaRelatedBySumberDanaId;

    /**
     * @var        SumberDana
     */
    protected $aSumberDanaRelatedBySumberDanaId;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Get the [blockgrant_id] column value.
     * 
     * @return string
     */
    public function getBlockgrantId()
    {
        return $this->blockgrant_id;
    }

    /**
     * Get the [sekolah_id] column value.
     * 
     * @return string
     */
    public function getSekolahId()
    {
        return $this->sekolah_id;
    }

    /**
     * Get the [nama] column value.
     * 
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Get the [tahun] column value.
     * 
     * @return string
     */
    public function getTahun()
    {
        return $this->tahun;
    }

    /**
     * Get the [jenis_bantuan_id] column value.
     * 
     * @return int
     */
    public function getJenisBantuanId()
    {
        return $this->jenis_bantuan_id;
    }

    /**
     * Get the [sumber_dana_id] column value.
     * 
     * @return string
     */
    public function getSumberDanaId()
    {
        return $this->sumber_dana_id;
    }

    /**
     * Get the [besar_bantuan] column value.
     * 
     * @return string
     */
    public function getBesarBantuan()
    {
        return $this->besar_bantuan;
    }

    /**
     * Get the [dana_pendamping] column value.
     * 
     * @return string
     */
    public function getDanaPendamping()
    {
        return $this->dana_pendamping;
    }

    /**
     * Get the [peruntukan_dana] column value.
     * 
     * @return string
     */
    public function getPeruntukanDana()
    {
        return $this->peruntukan_dana;
    }

    /**
     * Get the [asal_data] column value.
     * 
     * @return string
     */
    public function getAsalData()
    {
        return $this->asal_data;
    }

    /**
     * Get the [optionally formatted] temporal [last_update] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastUpdate($format = 'Y-m-d H:i:s')
    {
        if ($this->last_update === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_update);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_update, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [soft_delete] column value.
     * 
     * @return string
     */
    public function getSoftDelete()
    {
        return $this->soft_delete;
    }

    /**
     * Get the [optionally formatted] temporal [last_sync] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastSync($format = 'Y-m-d H:i:s')
    {
        if ($this->last_sync === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_sync);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_sync, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [updater_id] column value.
     * 
     * @return string
     */
    public function getUpdaterId()
    {
        return $this->updater_id;
    }

    /**
     * Set the value of [blockgrant_id] column.
     * 
     * @param string $v new value
     * @return Blockgrant The current object (for fluent API support)
     */
    public function setBlockgrantId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->blockgrant_id !== $v) {
            $this->blockgrant_id = $v;
            $this->modifiedColumns[] = BlockgrantPeer::BLOCKGRANT_ID;
        }


        return $this;
    } // setBlockgrantId()

    /**
     * Set the value of [sekolah_id] column.
     * 
     * @param string $v new value
     * @return Blockgrant The current object (for fluent API support)
     */
    public function setSekolahId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->sekolah_id !== $v) {
            $this->sekolah_id = $v;
            $this->modifiedColumns[] = BlockgrantPeer::SEKOLAH_ID;
        }

        if ($this->aSekolahRelatedBySekolahId !== null && $this->aSekolahRelatedBySekolahId->getSekolahId() !== $v) {
            $this->aSekolahRelatedBySekolahId = null;
        }

        if ($this->aSekolahRelatedBySekolahId !== null && $this->aSekolahRelatedBySekolahId->getSekolahId() !== $v) {
            $this->aSekolahRelatedBySekolahId = null;
        }


        return $this;
    } // setSekolahId()

    /**
     * Set the value of [nama] column.
     * 
     * @param string $v new value
     * @return Blockgrant The current object (for fluent API support)
     */
    public function setNama($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nama !== $v) {
            $this->nama = $v;
            $this->modifiedColumns[] = BlockgrantPeer::NAMA;
        }


        return $this;
    } // setNama()

    /**
     * Set the value of [tahun] column.
     * 
     * @param string $v new value
     * @return Blockgrant The current object (for fluent API support)
     */
    public function setTahun($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tahun !== $v) {
            $this->tahun = $v;
            $this->modifiedColumns[] = BlockgrantPeer::TAHUN;
        }


        return $this;
    } // setTahun()

    /**
     * Set the value of [jenis_bantuan_id] column.
     * 
     * @param int $v new value
     * @return Blockgrant The current object (for fluent API support)
     */
    public function setJenisBantuanId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->jenis_bantuan_id !== $v) {
            $this->jenis_bantuan_id = $v;
            $this->modifiedColumns[] = BlockgrantPeer::JENIS_BANTUAN_ID;
        }

        if ($this->aJenisBantuanRelatedByJenisBantuanId !== null && $this->aJenisBantuanRelatedByJenisBantuanId->getJenisBantuanId() !== $v) {
            $this->aJenisBantuanRelatedByJenisBantuanId = null;
        }

        if ($this->aJenisBantuanRelatedByJenisBantuanId !== null && $this->aJenisBantuanRelatedByJenisBantuanId->getJenisBantuanId() !== $v) {
            $this->aJenisBantuanRelatedByJenisBantuanId = null;
        }


        return $this;
    } // setJenisBantuanId()

    /**
     * Set the value of [sumber_dana_id] column.
     * 
     * @param string $v new value
     * @return Blockgrant The current object (for fluent API support)
     */
    public function setSumberDanaId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->sumber_dana_id !== $v) {
            $this->sumber_dana_id = $v;
            $this->modifiedColumns[] = BlockgrantPeer::SUMBER_DANA_ID;
        }

        if ($this->aSumberDanaRelatedBySumberDanaId !== null && $this->aSumberDanaRelatedBySumberDanaId->getSumberDanaId() !== $v) {
            $this->aSumberDanaRelatedBySumberDanaId = null;
        }

        if ($this->aSumberDanaRelatedBySumberDanaId !== null && $this->aSumberDanaRelatedBySumberDanaId->getSumberDanaId() !== $v) {
            $this->aSumberDanaRelatedBySumberDanaId = null;
        }


        return $this;
    } // setSumberDanaId()

    /**
     * Set the value of [besar_bantuan] column.
     * 
     * @param string $v new value
     * @return Blockgrant The current object (for fluent API support)
     */
    public function setBesarBantuan($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->besar_bantuan !== $v) {
            $this->besar_bantuan = $v;
            $this->modifiedColumns[] = BlockgrantPeer::BESAR_BANTUAN;
        }


        return $this;
    } // setBesarBantuan()

    /**
     * Set the value of [dana_pendamping] column.
     * 
     * @param string $v new value
     * @return Blockgrant The current object (for fluent API support)
     */
    public function setDanaPendamping($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->dana_pendamping !== $v) {
            $this->dana_pendamping = $v;
            $this->modifiedColumns[] = BlockgrantPeer::DANA_PENDAMPING;
        }


        return $this;
    } // setDanaPendamping()

    /**
     * Set the value of [peruntukan_dana] column.
     * 
     * @param string $v new value
     * @return Blockgrant The current object (for fluent API support)
     */
    public function setPeruntukanDana($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->peruntukan_dana !== $v) {
            $this->peruntukan_dana = $v;
            $this->modifiedColumns[] = BlockgrantPeer::PERUNTUKAN_DANA;
        }


        return $this;
    } // setPeruntukanDana()

    /**
     * Set the value of [asal_data] column.
     * 
     * @param string $v new value
     * @return Blockgrant The current object (for fluent API support)
     */
    public function setAsalData($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->asal_data !== $v) {
            $this->asal_data = $v;
            $this->modifiedColumns[] = BlockgrantPeer::ASAL_DATA;
        }


        return $this;
    } // setAsalData()

    /**
     * Sets the value of [last_update] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Blockgrant The current object (for fluent API support)
     */
    public function setLastUpdate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_update !== null || $dt !== null) {
            $currentDateAsString = ($this->last_update !== null && $tmpDt = new DateTime($this->last_update)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_update = $newDateAsString;
                $this->modifiedColumns[] = BlockgrantPeer::LAST_UPDATE;
            }
        } // if either are not null


        return $this;
    } // setLastUpdate()

    /**
     * Set the value of [soft_delete] column.
     * 
     * @param string $v new value
     * @return Blockgrant The current object (for fluent API support)
     */
    public function setSoftDelete($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->soft_delete !== $v) {
            $this->soft_delete = $v;
            $this->modifiedColumns[] = BlockgrantPeer::SOFT_DELETE;
        }


        return $this;
    } // setSoftDelete()

    /**
     * Sets the value of [last_sync] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Blockgrant The current object (for fluent API support)
     */
    public function setLastSync($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_sync !== null || $dt !== null) {
            $currentDateAsString = ($this->last_sync !== null && $tmpDt = new DateTime($this->last_sync)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_sync = $newDateAsString;
                $this->modifiedColumns[] = BlockgrantPeer::LAST_SYNC;
            }
        } // if either are not null


        return $this;
    } // setLastSync()

    /**
     * Set the value of [updater_id] column.
     * 
     * @param string $v new value
     * @return Blockgrant The current object (for fluent API support)
     */
    public function setUpdaterId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->updater_id !== $v) {
            $this->updater_id = $v;
            $this->modifiedColumns[] = BlockgrantPeer::UPDATER_ID;
        }


        return $this;
    } // setUpdaterId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->blockgrant_id = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
            $this->sekolah_id = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->nama = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->tahun = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->jenis_bantuan_id = ($row[$startcol + 4] !== null) ? (int) $row[$startcol + 4] : null;
            $this->sumber_dana_id = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->besar_bantuan = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->dana_pendamping = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->peruntukan_dana = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->asal_data = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->last_update = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->soft_delete = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->last_sync = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->updater_id = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 14; // 14 = BlockgrantPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Blockgrant object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aSekolahRelatedBySekolahId !== null && $this->sekolah_id !== $this->aSekolahRelatedBySekolahId->getSekolahId()) {
            $this->aSekolahRelatedBySekolahId = null;
        }
        if ($this->aSekolahRelatedBySekolahId !== null && $this->sekolah_id !== $this->aSekolahRelatedBySekolahId->getSekolahId()) {
            $this->aSekolahRelatedBySekolahId = null;
        }
        if ($this->aJenisBantuanRelatedByJenisBantuanId !== null && $this->jenis_bantuan_id !== $this->aJenisBantuanRelatedByJenisBantuanId->getJenisBantuanId()) {
            $this->aJenisBantuanRelatedByJenisBantuanId = null;
        }
        if ($this->aJenisBantuanRelatedByJenisBantuanId !== null && $this->jenis_bantuan_id !== $this->aJenisBantuanRelatedByJenisBantuanId->getJenisBantuanId()) {
            $this->aJenisBantuanRelatedByJenisBantuanId = null;
        }
        if ($this->aSumberDanaRelatedBySumberDanaId !== null && $this->sumber_dana_id !== $this->aSumberDanaRelatedBySumberDanaId->getSumberDanaId()) {
            $this->aSumberDanaRelatedBySumberDanaId = null;
        }
        if ($this->aSumberDanaRelatedBySumberDanaId !== null && $this->sumber_dana_id !== $this->aSumberDanaRelatedBySumberDanaId->getSumberDanaId()) {
            $this->aSumberDanaRelatedBySumberDanaId = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(BlockgrantPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = BlockgrantPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aSekolahRelatedBySekolahId = null;
            $this->aSekolahRelatedBySekolahId = null;
            $this->aJenisBantuanRelatedByJenisBantuanId = null;
            $this->aJenisBantuanRelatedByJenisBantuanId = null;
            $this->aSumberDanaRelatedBySumberDanaId = null;
            $this->aSumberDanaRelatedBySumberDanaId = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(BlockgrantPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = BlockgrantQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(BlockgrantPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                BlockgrantPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if ($this->aSekolahRelatedBySekolahId->isModified() || $this->aSekolahRelatedBySekolahId->isNew()) {
                    $affectedRows += $this->aSekolahRelatedBySekolahId->save($con);
                }
                $this->setSekolahRelatedBySekolahId($this->aSekolahRelatedBySekolahId);
            }

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if ($this->aSekolahRelatedBySekolahId->isModified() || $this->aSekolahRelatedBySekolahId->isNew()) {
                    $affectedRows += $this->aSekolahRelatedBySekolahId->save($con);
                }
                $this->setSekolahRelatedBySekolahId($this->aSekolahRelatedBySekolahId);
            }

            if ($this->aJenisBantuanRelatedByJenisBantuanId !== null) {
                if ($this->aJenisBantuanRelatedByJenisBantuanId->isModified() || $this->aJenisBantuanRelatedByJenisBantuanId->isNew()) {
                    $affectedRows += $this->aJenisBantuanRelatedByJenisBantuanId->save($con);
                }
                $this->setJenisBantuanRelatedByJenisBantuanId($this->aJenisBantuanRelatedByJenisBantuanId);
            }

            if ($this->aJenisBantuanRelatedByJenisBantuanId !== null) {
                if ($this->aJenisBantuanRelatedByJenisBantuanId->isModified() || $this->aJenisBantuanRelatedByJenisBantuanId->isNew()) {
                    $affectedRows += $this->aJenisBantuanRelatedByJenisBantuanId->save($con);
                }
                $this->setJenisBantuanRelatedByJenisBantuanId($this->aJenisBantuanRelatedByJenisBantuanId);
            }

            if ($this->aSumberDanaRelatedBySumberDanaId !== null) {
                if ($this->aSumberDanaRelatedBySumberDanaId->isModified() || $this->aSumberDanaRelatedBySumberDanaId->isNew()) {
                    $affectedRows += $this->aSumberDanaRelatedBySumberDanaId->save($con);
                }
                $this->setSumberDanaRelatedBySumberDanaId($this->aSumberDanaRelatedBySumberDanaId);
            }

            if ($this->aSumberDanaRelatedBySumberDanaId !== null) {
                if ($this->aSumberDanaRelatedBySumberDanaId->isModified() || $this->aSumberDanaRelatedBySumberDanaId->isNew()) {
                    $affectedRows += $this->aSumberDanaRelatedBySumberDanaId->save($con);
                }
                $this->setSumberDanaRelatedBySumberDanaId($this->aSumberDanaRelatedBySumberDanaId);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $criteria = $this->buildCriteria();
        $pk = BasePeer::doInsert($criteria, $con);
        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if (!$this->aSekolahRelatedBySekolahId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSekolahRelatedBySekolahId->getValidationFailures());
                }
            }

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if (!$this->aSekolahRelatedBySekolahId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSekolahRelatedBySekolahId->getValidationFailures());
                }
            }

            if ($this->aJenisBantuanRelatedByJenisBantuanId !== null) {
                if (!$this->aJenisBantuanRelatedByJenisBantuanId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aJenisBantuanRelatedByJenisBantuanId->getValidationFailures());
                }
            }

            if ($this->aJenisBantuanRelatedByJenisBantuanId !== null) {
                if (!$this->aJenisBantuanRelatedByJenisBantuanId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aJenisBantuanRelatedByJenisBantuanId->getValidationFailures());
                }
            }

            if ($this->aSumberDanaRelatedBySumberDanaId !== null) {
                if (!$this->aSumberDanaRelatedBySumberDanaId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSumberDanaRelatedBySumberDanaId->getValidationFailures());
                }
            }

            if ($this->aSumberDanaRelatedBySumberDanaId !== null) {
                if (!$this->aSumberDanaRelatedBySumberDanaId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSumberDanaRelatedBySumberDanaId->getValidationFailures());
                }
            }


            if (($retval = BlockgrantPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = BlockgrantPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getBlockgrantId();
                break;
            case 1:
                return $this->getSekolahId();
                break;
            case 2:
                return $this->getNama();
                break;
            case 3:
                return $this->getTahun();
                break;
            case 4:
                return $this->getJenisBantuanId();
                break;
            case 5:
                return $this->getSumberDanaId();
                break;
            case 6:
                return $this->getBesarBantuan();
                break;
            case 7:
                return $this->getDanaPendamping();
                break;
            case 8:
                return $this->getPeruntukanDana();
                break;
            case 9:
                return $this->getAsalData();
                break;
            case 10:
                return $this->getLastUpdate();
                break;
            case 11:
                return $this->getSoftDelete();
                break;
            case 12:
                return $this->getLastSync();
                break;
            case 13:
                return $this->getUpdaterId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Blockgrant'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Blockgrant'][$this->getPrimaryKey()] = true;
        $keys = BlockgrantPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getBlockgrantId(),
            $keys[1] => $this->getSekolahId(),
            $keys[2] => $this->getNama(),
            $keys[3] => $this->getTahun(),
            $keys[4] => $this->getJenisBantuanId(),
            $keys[5] => $this->getSumberDanaId(),
            $keys[6] => $this->getBesarBantuan(),
            $keys[7] => $this->getDanaPendamping(),
            $keys[8] => $this->getPeruntukanDana(),
            $keys[9] => $this->getAsalData(),
            $keys[10] => $this->getLastUpdate(),
            $keys[11] => $this->getSoftDelete(),
            $keys[12] => $this->getLastSync(),
            $keys[13] => $this->getUpdaterId(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aSekolahRelatedBySekolahId) {
                $result['SekolahRelatedBySekolahId'] = $this->aSekolahRelatedBySekolahId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSekolahRelatedBySekolahId) {
                $result['SekolahRelatedBySekolahId'] = $this->aSekolahRelatedBySekolahId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aJenisBantuanRelatedByJenisBantuanId) {
                $result['JenisBantuanRelatedByJenisBantuanId'] = $this->aJenisBantuanRelatedByJenisBantuanId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aJenisBantuanRelatedByJenisBantuanId) {
                $result['JenisBantuanRelatedByJenisBantuanId'] = $this->aJenisBantuanRelatedByJenisBantuanId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSumberDanaRelatedBySumberDanaId) {
                $result['SumberDanaRelatedBySumberDanaId'] = $this->aSumberDanaRelatedBySumberDanaId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSumberDanaRelatedBySumberDanaId) {
                $result['SumberDanaRelatedBySumberDanaId'] = $this->aSumberDanaRelatedBySumberDanaId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = BlockgrantPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setBlockgrantId($value);
                break;
            case 1:
                $this->setSekolahId($value);
                break;
            case 2:
                $this->setNama($value);
                break;
            case 3:
                $this->setTahun($value);
                break;
            case 4:
                $this->setJenisBantuanId($value);
                break;
            case 5:
                $this->setSumberDanaId($value);
                break;
            case 6:
                $this->setBesarBantuan($value);
                break;
            case 7:
                $this->setDanaPendamping($value);
                break;
            case 8:
                $this->setPeruntukanDana($value);
                break;
            case 9:
                $this->setAsalData($value);
                break;
            case 10:
                $this->setLastUpdate($value);
                break;
            case 11:
                $this->setSoftDelete($value);
                break;
            case 12:
                $this->setLastSync($value);
                break;
            case 13:
                $this->setUpdaterId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = BlockgrantPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setBlockgrantId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setSekolahId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setNama($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setTahun($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setJenisBantuanId($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setSumberDanaId($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setBesarBantuan($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setDanaPendamping($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setPeruntukanDana($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setAsalData($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setLastUpdate($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setSoftDelete($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setLastSync($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setUpdaterId($arr[$keys[13]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(BlockgrantPeer::DATABASE_NAME);

        if ($this->isColumnModified(BlockgrantPeer::BLOCKGRANT_ID)) $criteria->add(BlockgrantPeer::BLOCKGRANT_ID, $this->blockgrant_id);
        if ($this->isColumnModified(BlockgrantPeer::SEKOLAH_ID)) $criteria->add(BlockgrantPeer::SEKOLAH_ID, $this->sekolah_id);
        if ($this->isColumnModified(BlockgrantPeer::NAMA)) $criteria->add(BlockgrantPeer::NAMA, $this->nama);
        if ($this->isColumnModified(BlockgrantPeer::TAHUN)) $criteria->add(BlockgrantPeer::TAHUN, $this->tahun);
        if ($this->isColumnModified(BlockgrantPeer::JENIS_BANTUAN_ID)) $criteria->add(BlockgrantPeer::JENIS_BANTUAN_ID, $this->jenis_bantuan_id);
        if ($this->isColumnModified(BlockgrantPeer::SUMBER_DANA_ID)) $criteria->add(BlockgrantPeer::SUMBER_DANA_ID, $this->sumber_dana_id);
        if ($this->isColumnModified(BlockgrantPeer::BESAR_BANTUAN)) $criteria->add(BlockgrantPeer::BESAR_BANTUAN, $this->besar_bantuan);
        if ($this->isColumnModified(BlockgrantPeer::DANA_PENDAMPING)) $criteria->add(BlockgrantPeer::DANA_PENDAMPING, $this->dana_pendamping);
        if ($this->isColumnModified(BlockgrantPeer::PERUNTUKAN_DANA)) $criteria->add(BlockgrantPeer::PERUNTUKAN_DANA, $this->peruntukan_dana);
        if ($this->isColumnModified(BlockgrantPeer::ASAL_DATA)) $criteria->add(BlockgrantPeer::ASAL_DATA, $this->asal_data);
        if ($this->isColumnModified(BlockgrantPeer::LAST_UPDATE)) $criteria->add(BlockgrantPeer::LAST_UPDATE, $this->last_update);
        if ($this->isColumnModified(BlockgrantPeer::SOFT_DELETE)) $criteria->add(BlockgrantPeer::SOFT_DELETE, $this->soft_delete);
        if ($this->isColumnModified(BlockgrantPeer::LAST_SYNC)) $criteria->add(BlockgrantPeer::LAST_SYNC, $this->last_sync);
        if ($this->isColumnModified(BlockgrantPeer::UPDATER_ID)) $criteria->add(BlockgrantPeer::UPDATER_ID, $this->updater_id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(BlockgrantPeer::DATABASE_NAME);
        $criteria->add(BlockgrantPeer::BLOCKGRANT_ID, $this->blockgrant_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getBlockgrantId();
    }

    /**
     * Generic method to set the primary key (blockgrant_id column).
     *
     * @param  string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setBlockgrantId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getBlockgrantId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Blockgrant (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setSekolahId($this->getSekolahId());
        $copyObj->setNama($this->getNama());
        $copyObj->setTahun($this->getTahun());
        $copyObj->setJenisBantuanId($this->getJenisBantuanId());
        $copyObj->setSumberDanaId($this->getSumberDanaId());
        $copyObj->setBesarBantuan($this->getBesarBantuan());
        $copyObj->setDanaPendamping($this->getDanaPendamping());
        $copyObj->setPeruntukanDana($this->getPeruntukanDana());
        $copyObj->setAsalData($this->getAsalData());
        $copyObj->setLastUpdate($this->getLastUpdate());
        $copyObj->setSoftDelete($this->getSoftDelete());
        $copyObj->setLastSync($this->getLastSync());
        $copyObj->setUpdaterId($this->getUpdaterId());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setBlockgrantId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Blockgrant Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return BlockgrantPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new BlockgrantPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a Sekolah object.
     *
     * @param             Sekolah $v
     * @return Blockgrant The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSekolahRelatedBySekolahId(Sekolah $v = null)
    {
        if ($v === null) {
            $this->setSekolahId(NULL);
        } else {
            $this->setSekolahId($v->getSekolahId());
        }

        $this->aSekolahRelatedBySekolahId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Sekolah object, it will not be re-added.
        if ($v !== null) {
            $v->addBlockgrantRelatedBySekolahId($this);
        }


        return $this;
    }


    /**
     * Get the associated Sekolah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Sekolah The associated Sekolah object.
     * @throws PropelException
     */
    public function getSekolahRelatedBySekolahId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSekolahRelatedBySekolahId === null && (($this->sekolah_id !== "" && $this->sekolah_id !== null)) && $doQuery) {
            $this->aSekolahRelatedBySekolahId = SekolahQuery::create()->findPk($this->sekolah_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSekolahRelatedBySekolahId->addBlockgrantsRelatedBySekolahId($this);
             */
        }

        return $this->aSekolahRelatedBySekolahId;
    }

    /**
     * Declares an association between this object and a Sekolah object.
     *
     * @param             Sekolah $v
     * @return Blockgrant The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSekolahRelatedBySekolahId(Sekolah $v = null)
    {
        if ($v === null) {
            $this->setSekolahId(NULL);
        } else {
            $this->setSekolahId($v->getSekolahId());
        }

        $this->aSekolahRelatedBySekolahId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Sekolah object, it will not be re-added.
        if ($v !== null) {
            $v->addBlockgrantRelatedBySekolahId($this);
        }


        return $this;
    }


    /**
     * Get the associated Sekolah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Sekolah The associated Sekolah object.
     * @throws PropelException
     */
    public function getSekolahRelatedBySekolahId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSekolahRelatedBySekolahId === null && (($this->sekolah_id !== "" && $this->sekolah_id !== null)) && $doQuery) {
            $this->aSekolahRelatedBySekolahId = SekolahQuery::create()->findPk($this->sekolah_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSekolahRelatedBySekolahId->addBlockgrantsRelatedBySekolahId($this);
             */
        }

        return $this->aSekolahRelatedBySekolahId;
    }

    /**
     * Declares an association between this object and a JenisBantuan object.
     *
     * @param             JenisBantuan $v
     * @return Blockgrant The current object (for fluent API support)
     * @throws PropelException
     */
    public function setJenisBantuanRelatedByJenisBantuanId(JenisBantuan $v = null)
    {
        if ($v === null) {
            $this->setJenisBantuanId(NULL);
        } else {
            $this->setJenisBantuanId($v->getJenisBantuanId());
        }

        $this->aJenisBantuanRelatedByJenisBantuanId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the JenisBantuan object, it will not be re-added.
        if ($v !== null) {
            $v->addBlockgrantRelatedByJenisBantuanId($this);
        }


        return $this;
    }


    /**
     * Get the associated JenisBantuan object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return JenisBantuan The associated JenisBantuan object.
     * @throws PropelException
     */
    public function getJenisBantuanRelatedByJenisBantuanId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aJenisBantuanRelatedByJenisBantuanId === null && ($this->jenis_bantuan_id !== null) && $doQuery) {
            $this->aJenisBantuanRelatedByJenisBantuanId = JenisBantuanQuery::create()->findPk($this->jenis_bantuan_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aJenisBantuanRelatedByJenisBantuanId->addBlockgrantsRelatedByJenisBantuanId($this);
             */
        }

        return $this->aJenisBantuanRelatedByJenisBantuanId;
    }

    /**
     * Declares an association between this object and a JenisBantuan object.
     *
     * @param             JenisBantuan $v
     * @return Blockgrant The current object (for fluent API support)
     * @throws PropelException
     */
    public function setJenisBantuanRelatedByJenisBantuanId(JenisBantuan $v = null)
    {
        if ($v === null) {
            $this->setJenisBantuanId(NULL);
        } else {
            $this->setJenisBantuanId($v->getJenisBantuanId());
        }

        $this->aJenisBantuanRelatedByJenisBantuanId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the JenisBantuan object, it will not be re-added.
        if ($v !== null) {
            $v->addBlockgrantRelatedByJenisBantuanId($this);
        }


        return $this;
    }


    /**
     * Get the associated JenisBantuan object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return JenisBantuan The associated JenisBantuan object.
     * @throws PropelException
     */
    public function getJenisBantuanRelatedByJenisBantuanId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aJenisBantuanRelatedByJenisBantuanId === null && ($this->jenis_bantuan_id !== null) && $doQuery) {
            $this->aJenisBantuanRelatedByJenisBantuanId = JenisBantuanQuery::create()->findPk($this->jenis_bantuan_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aJenisBantuanRelatedByJenisBantuanId->addBlockgrantsRelatedByJenisBantuanId($this);
             */
        }

        return $this->aJenisBantuanRelatedByJenisBantuanId;
    }

    /**
     * Declares an association between this object and a SumberDana object.
     *
     * @param             SumberDana $v
     * @return Blockgrant The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSumberDanaRelatedBySumberDanaId(SumberDana $v = null)
    {
        if ($v === null) {
            $this->setSumberDanaId(NULL);
        } else {
            $this->setSumberDanaId($v->getSumberDanaId());
        }

        $this->aSumberDanaRelatedBySumberDanaId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the SumberDana object, it will not be re-added.
        if ($v !== null) {
            $v->addBlockgrantRelatedBySumberDanaId($this);
        }


        return $this;
    }


    /**
     * Get the associated SumberDana object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return SumberDana The associated SumberDana object.
     * @throws PropelException
     */
    public function getSumberDanaRelatedBySumberDanaId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSumberDanaRelatedBySumberDanaId === null && (($this->sumber_dana_id !== "" && $this->sumber_dana_id !== null)) && $doQuery) {
            $this->aSumberDanaRelatedBySumberDanaId = SumberDanaQuery::create()->findPk($this->sumber_dana_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSumberDanaRelatedBySumberDanaId->addBlockgrantsRelatedBySumberDanaId($this);
             */
        }

        return $this->aSumberDanaRelatedBySumberDanaId;
    }

    /**
     * Declares an association between this object and a SumberDana object.
     *
     * @param             SumberDana $v
     * @return Blockgrant The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSumberDanaRelatedBySumberDanaId(SumberDana $v = null)
    {
        if ($v === null) {
            $this->setSumberDanaId(NULL);
        } else {
            $this->setSumberDanaId($v->getSumberDanaId());
        }

        $this->aSumberDanaRelatedBySumberDanaId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the SumberDana object, it will not be re-added.
        if ($v !== null) {
            $v->addBlockgrantRelatedBySumberDanaId($this);
        }


        return $this;
    }


    /**
     * Get the associated SumberDana object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return SumberDana The associated SumberDana object.
     * @throws PropelException
     */
    public function getSumberDanaRelatedBySumberDanaId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSumberDanaRelatedBySumberDanaId === null && (($this->sumber_dana_id !== "" && $this->sumber_dana_id !== null)) && $doQuery) {
            $this->aSumberDanaRelatedBySumberDanaId = SumberDanaQuery::create()->findPk($this->sumber_dana_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSumberDanaRelatedBySumberDanaId->addBlockgrantsRelatedBySumberDanaId($this);
             */
        }

        return $this->aSumberDanaRelatedBySumberDanaId;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->blockgrant_id = null;
        $this->sekolah_id = null;
        $this->nama = null;
        $this->tahun = null;
        $this->jenis_bantuan_id = null;
        $this->sumber_dana_id = null;
        $this->besar_bantuan = null;
        $this->dana_pendamping = null;
        $this->peruntukan_dana = null;
        $this->asal_data = null;
        $this->last_update = null;
        $this->soft_delete = null;
        $this->last_sync = null;
        $this->updater_id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->aSekolahRelatedBySekolahId instanceof Persistent) {
              $this->aSekolahRelatedBySekolahId->clearAllReferences($deep);
            }
            if ($this->aSekolahRelatedBySekolahId instanceof Persistent) {
              $this->aSekolahRelatedBySekolahId->clearAllReferences($deep);
            }
            if ($this->aJenisBantuanRelatedByJenisBantuanId instanceof Persistent) {
              $this->aJenisBantuanRelatedByJenisBantuanId->clearAllReferences($deep);
            }
            if ($this->aJenisBantuanRelatedByJenisBantuanId instanceof Persistent) {
              $this->aJenisBantuanRelatedByJenisBantuanId->clearAllReferences($deep);
            }
            if ($this->aSumberDanaRelatedBySumberDanaId instanceof Persistent) {
              $this->aSumberDanaRelatedBySumberDanaId->clearAllReferences($deep);
            }
            if ($this->aSumberDanaRelatedBySumberDanaId instanceof Persistent) {
              $this->aSumberDanaRelatedBySumberDanaId->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        $this->aSekolahRelatedBySekolahId = null;
        $this->aSekolahRelatedBySekolahId = null;
        $this->aJenisBantuanRelatedByJenisBantuanId = null;
        $this->aJenisBantuanRelatedByJenisBantuanId = null;
        $this->aSumberDanaRelatedBySumberDanaId = null;
        $this->aSumberDanaRelatedBySumberDanaId = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(BlockgrantPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
