<?php

namespace angulex\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelDateTime;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\Demografi;
use angulex\Model\DemografiQuery;
use angulex\Model\LembagaNonSekolah;
use angulex\Model\LembagaNonSekolahQuery;
use angulex\Model\LevelWilayah;
use angulex\Model\LevelWilayahQuery;
use angulex\Model\MstWilayah;
use angulex\Model\MstWilayahPeer;
use angulex\Model\MstWilayahQuery;
use angulex\Model\Negara;
use angulex\Model\NegaraQuery;
use angulex\Model\Pengguna;
use angulex\Model\PenggunaQuery;
use angulex\Model\PesertaDidik;
use angulex\Model\PesertaDidikQuery;
use angulex\Model\Ptk;
use angulex\Model\PtkQuery;
use angulex\Model\Sekolah;
use angulex\Model\SekolahQuery;
use angulex\Model\TetanggaKabkota;
use angulex\Model\TetanggaKabkotaQuery;
use angulex\Model\WPendingJob;
use angulex\Model\WPendingJobQuery;
use angulex\Model\WdstSyncLog;
use angulex\Model\WdstSyncLogQuery;
use angulex\Model\WsrcSyncLog;
use angulex\Model\WsrcSyncLogQuery;
use angulex\Model\WsyncSession;
use angulex\Model\WsyncSessionQuery;
use angulex\Model\WtDstSyncLog;
use angulex\Model\WtDstSyncLogQuery;
use angulex\Model\WtSrcSyncLog;
use angulex\Model\WtSrcSyncLogQuery;
use angulex\Model\Yayasan;
use angulex\Model\YayasanQuery;

/**
 * Base class that represents a row from the 'ref.mst_wilayah' table.
 *
 * 
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseMstWilayah extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'angulex\\Model\\MstWilayahPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        MstWilayahPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the kode_wilayah field.
     * @var        string
     */
    protected $kode_wilayah;

    /**
     * The value for the nama field.
     * @var        string
     */
    protected $nama;

    /**
     * The value for the id_level_wilayah field.
     * @var        int
     */
    protected $id_level_wilayah;

    /**
     * The value for the mst_kode_wilayah field.
     * @var        string
     */
    protected $mst_kode_wilayah;

    /**
     * The value for the negara_id field.
     * @var        string
     */
    protected $negara_id;

    /**
     * The value for the asal_wilayah field.
     * @var        string
     */
    protected $asal_wilayah;

    /**
     * The value for the kode_bps field.
     * @var        string
     */
    protected $kode_bps;

    /**
     * The value for the kode_dagri field.
     * @var        string
     */
    protected $kode_dagri;

    /**
     * The value for the kode_keu field.
     * @var        string
     */
    protected $kode_keu;

    /**
     * The value for the create_date field.
     * @var        string
     */
    protected $create_date;

    /**
     * The value for the last_update field.
     * @var        string
     */
    protected $last_update;

    /**
     * The value for the expired_date field.
     * @var        string
     */
    protected $expired_date;

    /**
     * The value for the last_sync field.
     * @var        string
     */
    protected $last_sync;

    /**
     * @var        LevelWilayah
     */
    protected $aLevelWilayah;

    /**
     * @var        MstWilayah
     */
    protected $aMstWilayahRelatedByMstKodeWilayah;

    /**
     * @var        Negara
     */
    protected $aNegara;

    /**
     * @var        PropelObjectCollection|TetanggaKabkota[] Collection to store aggregation of TetanggaKabkota objects.
     */
    protected $collTetanggaKabkotasRelatedByKodeWilayah;
    protected $collTetanggaKabkotasRelatedByKodeWilayahPartial;

    /**
     * @var        PropelObjectCollection|TetanggaKabkota[] Collection to store aggregation of TetanggaKabkota objects.
     */
    protected $collTetanggaKabkotasRelatedByMstKodeWilayah;
    protected $collTetanggaKabkotasRelatedByMstKodeWilayahPartial;

    /**
     * @var        PropelObjectCollection|PesertaDidik[] Collection to store aggregation of PesertaDidik objects.
     */
    protected $collPesertaDidiksRelatedByKodeWilayah;
    protected $collPesertaDidiksRelatedByKodeWilayahPartial;

    /**
     * @var        PropelObjectCollection|PesertaDidik[] Collection to store aggregation of PesertaDidik objects.
     */
    protected $collPesertaDidiksRelatedByKodeWilayah;
    protected $collPesertaDidiksRelatedByKodeWilayahPartial;

    /**
     * @var        PropelObjectCollection|MstWilayah[] Collection to store aggregation of MstWilayah objects.
     */
    protected $collMstWilayahsRelatedByKodeWilayah;
    protected $collMstWilayahsRelatedByKodeWilayahPartial;

    /**
     * @var        PropelObjectCollection|Pengguna[] Collection to store aggregation of Pengguna objects.
     */
    protected $collPenggunasRelatedByKodeWilayah;
    protected $collPenggunasRelatedByKodeWilayahPartial;

    /**
     * @var        PropelObjectCollection|Pengguna[] Collection to store aggregation of Pengguna objects.
     */
    protected $collPenggunasRelatedByKodeWilayah;
    protected $collPenggunasRelatedByKodeWilayahPartial;

    /**
     * @var        PropelObjectCollection|Sekolah[] Collection to store aggregation of Sekolah objects.
     */
    protected $collSekolahsRelatedByKodeWilayah;
    protected $collSekolahsRelatedByKodeWilayahPartial;

    /**
     * @var        PropelObjectCollection|Sekolah[] Collection to store aggregation of Sekolah objects.
     */
    protected $collSekolahsRelatedByKodeWilayah;
    protected $collSekolahsRelatedByKodeWilayahPartial;

    /**
     * @var        PropelObjectCollection|Yayasan[] Collection to store aggregation of Yayasan objects.
     */
    protected $collYayasansRelatedByKodeWilayah;
    protected $collYayasansRelatedByKodeWilayahPartial;

    /**
     * @var        PropelObjectCollection|Yayasan[] Collection to store aggregation of Yayasan objects.
     */
    protected $collYayasansRelatedByKodeWilayah;
    protected $collYayasansRelatedByKodeWilayahPartial;

    /**
     * @var        PropelObjectCollection|Ptk[] Collection to store aggregation of Ptk objects.
     */
    protected $collPtksRelatedByKodeWilayah;
    protected $collPtksRelatedByKodeWilayahPartial;

    /**
     * @var        PropelObjectCollection|Ptk[] Collection to store aggregation of Ptk objects.
     */
    protected $collPtksRelatedByKodeWilayah;
    protected $collPtksRelatedByKodeWilayahPartial;

    /**
     * @var        PropelObjectCollection|WtSrcSyncLog[] Collection to store aggregation of WtSrcSyncLog objects.
     */
    protected $collWtSrcSyncLogsRelatedByKodeWilayah;
    protected $collWtSrcSyncLogsRelatedByKodeWilayahPartial;

    /**
     * @var        PropelObjectCollection|WtSrcSyncLog[] Collection to store aggregation of WtSrcSyncLog objects.
     */
    protected $collWtSrcSyncLogsRelatedByKodeWilayah;
    protected $collWtSrcSyncLogsRelatedByKodeWilayahPartial;

    /**
     * @var        PropelObjectCollection|WtDstSyncLog[] Collection to store aggregation of WtDstSyncLog objects.
     */
    protected $collWtDstSyncLogsRelatedByKodeWilayah;
    protected $collWtDstSyncLogsRelatedByKodeWilayahPartial;

    /**
     * @var        PropelObjectCollection|WtDstSyncLog[] Collection to store aggregation of WtDstSyncLog objects.
     */
    protected $collWtDstSyncLogsRelatedByKodeWilayah;
    protected $collWtDstSyncLogsRelatedByKodeWilayahPartial;

    /**
     * @var        PropelObjectCollection|WsyncSession[] Collection to store aggregation of WsyncSession objects.
     */
    protected $collWsyncSessionsRelatedByKodeWilayah;
    protected $collWsyncSessionsRelatedByKodeWilayahPartial;

    /**
     * @var        PropelObjectCollection|WsyncSession[] Collection to store aggregation of WsyncSession objects.
     */
    protected $collWsyncSessionsRelatedByKodeWilayah;
    protected $collWsyncSessionsRelatedByKodeWilayahPartial;

    /**
     * @var        PropelObjectCollection|WsrcSyncLog[] Collection to store aggregation of WsrcSyncLog objects.
     */
    protected $collWsrcSyncLogsRelatedByKodeWilayah;
    protected $collWsrcSyncLogsRelatedByKodeWilayahPartial;

    /**
     * @var        PropelObjectCollection|WsrcSyncLog[] Collection to store aggregation of WsrcSyncLog objects.
     */
    protected $collWsrcSyncLogsRelatedByKodeWilayah;
    protected $collWsrcSyncLogsRelatedByKodeWilayahPartial;

    /**
     * @var        PropelObjectCollection|WdstSyncLog[] Collection to store aggregation of WdstSyncLog objects.
     */
    protected $collWdstSyncLogsRelatedByKodeWilayah;
    protected $collWdstSyncLogsRelatedByKodeWilayahPartial;

    /**
     * @var        PropelObjectCollection|WdstSyncLog[] Collection to store aggregation of WdstSyncLog objects.
     */
    protected $collWdstSyncLogsRelatedByKodeWilayah;
    protected $collWdstSyncLogsRelatedByKodeWilayahPartial;

    /**
     * @var        PropelObjectCollection|WPendingJob[] Collection to store aggregation of WPendingJob objects.
     */
    protected $collWPendingJobsRelatedByKodeWilayah;
    protected $collWPendingJobsRelatedByKodeWilayahPartial;

    /**
     * @var        PropelObjectCollection|WPendingJob[] Collection to store aggregation of WPendingJob objects.
     */
    protected $collWPendingJobsRelatedByKodeWilayah;
    protected $collWPendingJobsRelatedByKodeWilayahPartial;

    /**
     * @var        PropelObjectCollection|LembagaNonSekolah[] Collection to store aggregation of LembagaNonSekolah objects.
     */
    protected $collLembagaNonSekolahsRelatedByKodeWilayah;
    protected $collLembagaNonSekolahsRelatedByKodeWilayahPartial;

    /**
     * @var        PropelObjectCollection|LembagaNonSekolah[] Collection to store aggregation of LembagaNonSekolah objects.
     */
    protected $collLembagaNonSekolahsRelatedByKodeWilayah;
    protected $collLembagaNonSekolahsRelatedByKodeWilayahPartial;

    /**
     * @var        PropelObjectCollection|Demografi[] Collection to store aggregation of Demografi objects.
     */
    protected $collDemografisRelatedByKodeWilayah;
    protected $collDemografisRelatedByKodeWilayahPartial;

    /**
     * @var        PropelObjectCollection|Demografi[] Collection to store aggregation of Demografi objects.
     */
    protected $collDemografisRelatedByKodeWilayah;
    protected $collDemografisRelatedByKodeWilayahPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $tetanggaKabkotasRelatedByKodeWilayahScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $tetanggaKabkotasRelatedByMstKodeWilayahScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $pesertaDidiksRelatedByKodeWilayahScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $pesertaDidiksRelatedByKodeWilayahScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $mstWilayahsRelatedByKodeWilayahScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $penggunasRelatedByKodeWilayahScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $penggunasRelatedByKodeWilayahScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $sekolahsRelatedByKodeWilayahScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $sekolahsRelatedByKodeWilayahScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $yayasansRelatedByKodeWilayahScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $yayasansRelatedByKodeWilayahScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $ptksRelatedByKodeWilayahScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $ptksRelatedByKodeWilayahScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $wtSrcSyncLogsRelatedByKodeWilayahScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $wtSrcSyncLogsRelatedByKodeWilayahScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $wtDstSyncLogsRelatedByKodeWilayahScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $wtDstSyncLogsRelatedByKodeWilayahScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $wsyncSessionsRelatedByKodeWilayahScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $wsyncSessionsRelatedByKodeWilayahScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $wsrcSyncLogsRelatedByKodeWilayahScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $wsrcSyncLogsRelatedByKodeWilayahScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $wdstSyncLogsRelatedByKodeWilayahScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $wdstSyncLogsRelatedByKodeWilayahScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $wPendingJobsRelatedByKodeWilayahScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $wPendingJobsRelatedByKodeWilayahScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $lembagaNonSekolahsRelatedByKodeWilayahScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $lembagaNonSekolahsRelatedByKodeWilayahScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $demografisRelatedByKodeWilayahScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $demografisRelatedByKodeWilayahScheduledForDeletion = null;

    /**
     * Get the [kode_wilayah] column value.
     * 
     * @return string
     */
    public function getKodeWilayah()
    {
        return $this->kode_wilayah;
    }

    /**
     * Get the [nama] column value.
     * 
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Get the [id_level_wilayah] column value.
     * 
     * @return int
     */
    public function getIdLevelWilayah()
    {
        return $this->id_level_wilayah;
    }

    /**
     * Get the [mst_kode_wilayah] column value.
     * 
     * @return string
     */
    public function getMstKodeWilayah()
    {
        return $this->mst_kode_wilayah;
    }

    /**
     * Get the [negara_id] column value.
     * 
     * @return string
     */
    public function getNegaraId()
    {
        return $this->negara_id;
    }

    /**
     * Get the [asal_wilayah] column value.
     * 
     * @return string
     */
    public function getAsalWilayah()
    {
        return $this->asal_wilayah;
    }

    /**
     * Get the [kode_bps] column value.
     * 
     * @return string
     */
    public function getKodeBps()
    {
        return $this->kode_bps;
    }

    /**
     * Get the [kode_dagri] column value.
     * 
     * @return string
     */
    public function getKodeDagri()
    {
        return $this->kode_dagri;
    }

    /**
     * Get the [kode_keu] column value.
     * 
     * @return string
     */
    public function getKodeKeu()
    {
        return $this->kode_keu;
    }

    /**
     * Get the [optionally formatted] temporal [create_date] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreateDate($format = 'Y-m-d H:i:s')
    {
        if ($this->create_date === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->create_date);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->create_date, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [last_update] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastUpdate($format = 'Y-m-d H:i:s')
    {
        if ($this->last_update === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_update);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_update, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [expired_date] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getExpiredDate($format = 'Y-m-d H:i:s')
    {
        if ($this->expired_date === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->expired_date);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->expired_date, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [last_sync] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastSync($format = 'Y-m-d H:i:s')
    {
        if ($this->last_sync === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_sync);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_sync, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Set the value of [kode_wilayah] column.
     * 
     * @param string $v new value
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setKodeWilayah($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->kode_wilayah !== $v) {
            $this->kode_wilayah = $v;
            $this->modifiedColumns[] = MstWilayahPeer::KODE_WILAYAH;
        }


        return $this;
    } // setKodeWilayah()

    /**
     * Set the value of [nama] column.
     * 
     * @param string $v new value
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setNama($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nama !== $v) {
            $this->nama = $v;
            $this->modifiedColumns[] = MstWilayahPeer::NAMA;
        }


        return $this;
    } // setNama()

    /**
     * Set the value of [id_level_wilayah] column.
     * 
     * @param int $v new value
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setIdLevelWilayah($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_level_wilayah !== $v) {
            $this->id_level_wilayah = $v;
            $this->modifiedColumns[] = MstWilayahPeer::ID_LEVEL_WILAYAH;
        }

        if ($this->aLevelWilayah !== null && $this->aLevelWilayah->getIdLevelWilayah() !== $v) {
            $this->aLevelWilayah = null;
        }


        return $this;
    } // setIdLevelWilayah()

    /**
     * Set the value of [mst_kode_wilayah] column.
     * 
     * @param string $v new value
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setMstKodeWilayah($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->mst_kode_wilayah !== $v) {
            $this->mst_kode_wilayah = $v;
            $this->modifiedColumns[] = MstWilayahPeer::MST_KODE_WILAYAH;
        }

        if ($this->aMstWilayahRelatedByMstKodeWilayah !== null && $this->aMstWilayahRelatedByMstKodeWilayah->getKodeWilayah() !== $v) {
            $this->aMstWilayahRelatedByMstKodeWilayah = null;
        }


        return $this;
    } // setMstKodeWilayah()

    /**
     * Set the value of [negara_id] column.
     * 
     * @param string $v new value
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setNegaraId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->negara_id !== $v) {
            $this->negara_id = $v;
            $this->modifiedColumns[] = MstWilayahPeer::NEGARA_ID;
        }

        if ($this->aNegara !== null && $this->aNegara->getNegaraId() !== $v) {
            $this->aNegara = null;
        }


        return $this;
    } // setNegaraId()

    /**
     * Set the value of [asal_wilayah] column.
     * 
     * @param string $v new value
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setAsalWilayah($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->asal_wilayah !== $v) {
            $this->asal_wilayah = $v;
            $this->modifiedColumns[] = MstWilayahPeer::ASAL_WILAYAH;
        }


        return $this;
    } // setAsalWilayah()

    /**
     * Set the value of [kode_bps] column.
     * 
     * @param string $v new value
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setKodeBps($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->kode_bps !== $v) {
            $this->kode_bps = $v;
            $this->modifiedColumns[] = MstWilayahPeer::KODE_BPS;
        }


        return $this;
    } // setKodeBps()

    /**
     * Set the value of [kode_dagri] column.
     * 
     * @param string $v new value
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setKodeDagri($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->kode_dagri !== $v) {
            $this->kode_dagri = $v;
            $this->modifiedColumns[] = MstWilayahPeer::KODE_DAGRI;
        }


        return $this;
    } // setKodeDagri()

    /**
     * Set the value of [kode_keu] column.
     * 
     * @param string $v new value
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setKodeKeu($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->kode_keu !== $v) {
            $this->kode_keu = $v;
            $this->modifiedColumns[] = MstWilayahPeer::KODE_KEU;
        }


        return $this;
    } // setKodeKeu()

    /**
     * Sets the value of [create_date] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setCreateDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->create_date !== null || $dt !== null) {
            $currentDateAsString = ($this->create_date !== null && $tmpDt = new DateTime($this->create_date)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->create_date = $newDateAsString;
                $this->modifiedColumns[] = MstWilayahPeer::CREATE_DATE;
            }
        } // if either are not null


        return $this;
    } // setCreateDate()

    /**
     * Sets the value of [last_update] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setLastUpdate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_update !== null || $dt !== null) {
            $currentDateAsString = ($this->last_update !== null && $tmpDt = new DateTime($this->last_update)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_update = $newDateAsString;
                $this->modifiedColumns[] = MstWilayahPeer::LAST_UPDATE;
            }
        } // if either are not null


        return $this;
    } // setLastUpdate()

    /**
     * Sets the value of [expired_date] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setExpiredDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->expired_date !== null || $dt !== null) {
            $currentDateAsString = ($this->expired_date !== null && $tmpDt = new DateTime($this->expired_date)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->expired_date = $newDateAsString;
                $this->modifiedColumns[] = MstWilayahPeer::EXPIRED_DATE;
            }
        } // if either are not null


        return $this;
    } // setExpiredDate()

    /**
     * Sets the value of [last_sync] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setLastSync($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_sync !== null || $dt !== null) {
            $currentDateAsString = ($this->last_sync !== null && $tmpDt = new DateTime($this->last_sync)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_sync = $newDateAsString;
                $this->modifiedColumns[] = MstWilayahPeer::LAST_SYNC;
            }
        } // if either are not null


        return $this;
    } // setLastSync()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->kode_wilayah = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
            $this->nama = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->id_level_wilayah = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->mst_kode_wilayah = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->negara_id = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->asal_wilayah = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->kode_bps = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->kode_dagri = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->kode_keu = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->create_date = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->last_update = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->expired_date = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->last_sync = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 13; // 13 = MstWilayahPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating MstWilayah object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aLevelWilayah !== null && $this->id_level_wilayah !== $this->aLevelWilayah->getIdLevelWilayah()) {
            $this->aLevelWilayah = null;
        }
        if ($this->aMstWilayahRelatedByMstKodeWilayah !== null && $this->mst_kode_wilayah !== $this->aMstWilayahRelatedByMstKodeWilayah->getKodeWilayah()) {
            $this->aMstWilayahRelatedByMstKodeWilayah = null;
        }
        if ($this->aNegara !== null && $this->negara_id !== $this->aNegara->getNegaraId()) {
            $this->aNegara = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(MstWilayahPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = MstWilayahPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aLevelWilayah = null;
            $this->aMstWilayahRelatedByMstKodeWilayah = null;
            $this->aNegara = null;
            $this->collTetanggaKabkotasRelatedByKodeWilayah = null;

            $this->collTetanggaKabkotasRelatedByMstKodeWilayah = null;

            $this->collPesertaDidiksRelatedByKodeWilayah = null;

            $this->collPesertaDidiksRelatedByKodeWilayah = null;

            $this->collMstWilayahsRelatedByKodeWilayah = null;

            $this->collPenggunasRelatedByKodeWilayah = null;

            $this->collPenggunasRelatedByKodeWilayah = null;

            $this->collSekolahsRelatedByKodeWilayah = null;

            $this->collSekolahsRelatedByKodeWilayah = null;

            $this->collYayasansRelatedByKodeWilayah = null;

            $this->collYayasansRelatedByKodeWilayah = null;

            $this->collPtksRelatedByKodeWilayah = null;

            $this->collPtksRelatedByKodeWilayah = null;

            $this->collWtSrcSyncLogsRelatedByKodeWilayah = null;

            $this->collWtSrcSyncLogsRelatedByKodeWilayah = null;

            $this->collWtDstSyncLogsRelatedByKodeWilayah = null;

            $this->collWtDstSyncLogsRelatedByKodeWilayah = null;

            $this->collWsyncSessionsRelatedByKodeWilayah = null;

            $this->collWsyncSessionsRelatedByKodeWilayah = null;

            $this->collWsrcSyncLogsRelatedByKodeWilayah = null;

            $this->collWsrcSyncLogsRelatedByKodeWilayah = null;

            $this->collWdstSyncLogsRelatedByKodeWilayah = null;

            $this->collWdstSyncLogsRelatedByKodeWilayah = null;

            $this->collWPendingJobsRelatedByKodeWilayah = null;

            $this->collWPendingJobsRelatedByKodeWilayah = null;

            $this->collLembagaNonSekolahsRelatedByKodeWilayah = null;

            $this->collLembagaNonSekolahsRelatedByKodeWilayah = null;

            $this->collDemografisRelatedByKodeWilayah = null;

            $this->collDemografisRelatedByKodeWilayah = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(MstWilayahPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = MstWilayahQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(MstWilayahPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                MstWilayahPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aLevelWilayah !== null) {
                if ($this->aLevelWilayah->isModified() || $this->aLevelWilayah->isNew()) {
                    $affectedRows += $this->aLevelWilayah->save($con);
                }
                $this->setLevelWilayah($this->aLevelWilayah);
            }

            if ($this->aMstWilayahRelatedByMstKodeWilayah !== null) {
                if ($this->aMstWilayahRelatedByMstKodeWilayah->isModified() || $this->aMstWilayahRelatedByMstKodeWilayah->isNew()) {
                    $affectedRows += $this->aMstWilayahRelatedByMstKodeWilayah->save($con);
                }
                $this->setMstWilayahRelatedByMstKodeWilayah($this->aMstWilayahRelatedByMstKodeWilayah);
            }

            if ($this->aNegara !== null) {
                if ($this->aNegara->isModified() || $this->aNegara->isNew()) {
                    $affectedRows += $this->aNegara->save($con);
                }
                $this->setNegara($this->aNegara);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->tetanggaKabkotasRelatedByKodeWilayahScheduledForDeletion !== null) {
                if (!$this->tetanggaKabkotasRelatedByKodeWilayahScheduledForDeletion->isEmpty()) {
                    TetanggaKabkotaQuery::create()
                        ->filterByPrimaryKeys($this->tetanggaKabkotasRelatedByKodeWilayahScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->tetanggaKabkotasRelatedByKodeWilayahScheduledForDeletion = null;
                }
            }

            if ($this->collTetanggaKabkotasRelatedByKodeWilayah !== null) {
                foreach ($this->collTetanggaKabkotasRelatedByKodeWilayah as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->tetanggaKabkotasRelatedByMstKodeWilayahScheduledForDeletion !== null) {
                if (!$this->tetanggaKabkotasRelatedByMstKodeWilayahScheduledForDeletion->isEmpty()) {
                    TetanggaKabkotaQuery::create()
                        ->filterByPrimaryKeys($this->tetanggaKabkotasRelatedByMstKodeWilayahScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->tetanggaKabkotasRelatedByMstKodeWilayahScheduledForDeletion = null;
                }
            }

            if ($this->collTetanggaKabkotasRelatedByMstKodeWilayah !== null) {
                foreach ($this->collTetanggaKabkotasRelatedByMstKodeWilayah as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->pesertaDidiksRelatedByKodeWilayahScheduledForDeletion !== null) {
                if (!$this->pesertaDidiksRelatedByKodeWilayahScheduledForDeletion->isEmpty()) {
                    PesertaDidikQuery::create()
                        ->filterByPrimaryKeys($this->pesertaDidiksRelatedByKodeWilayahScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->pesertaDidiksRelatedByKodeWilayahScheduledForDeletion = null;
                }
            }

            if ($this->collPesertaDidiksRelatedByKodeWilayah !== null) {
                foreach ($this->collPesertaDidiksRelatedByKodeWilayah as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->pesertaDidiksRelatedByKodeWilayahScheduledForDeletion !== null) {
                if (!$this->pesertaDidiksRelatedByKodeWilayahScheduledForDeletion->isEmpty()) {
                    PesertaDidikQuery::create()
                        ->filterByPrimaryKeys($this->pesertaDidiksRelatedByKodeWilayahScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->pesertaDidiksRelatedByKodeWilayahScheduledForDeletion = null;
                }
            }

            if ($this->collPesertaDidiksRelatedByKodeWilayah !== null) {
                foreach ($this->collPesertaDidiksRelatedByKodeWilayah as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->mstWilayahsRelatedByKodeWilayahScheduledForDeletion !== null) {
                if (!$this->mstWilayahsRelatedByKodeWilayahScheduledForDeletion->isEmpty()) {
                    foreach ($this->mstWilayahsRelatedByKodeWilayahScheduledForDeletion as $mstWilayahRelatedByKodeWilayah) {
                        // need to save related object because we set the relation to null
                        $mstWilayahRelatedByKodeWilayah->save($con);
                    }
                    $this->mstWilayahsRelatedByKodeWilayahScheduledForDeletion = null;
                }
            }

            if ($this->collMstWilayahsRelatedByKodeWilayah !== null) {
                foreach ($this->collMstWilayahsRelatedByKodeWilayah as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->penggunasRelatedByKodeWilayahScheduledForDeletion !== null) {
                if (!$this->penggunasRelatedByKodeWilayahScheduledForDeletion->isEmpty()) {
                    PenggunaQuery::create()
                        ->filterByPrimaryKeys($this->penggunasRelatedByKodeWilayahScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->penggunasRelatedByKodeWilayahScheduledForDeletion = null;
                }
            }

            if ($this->collPenggunasRelatedByKodeWilayah !== null) {
                foreach ($this->collPenggunasRelatedByKodeWilayah as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->penggunasRelatedByKodeWilayahScheduledForDeletion !== null) {
                if (!$this->penggunasRelatedByKodeWilayahScheduledForDeletion->isEmpty()) {
                    PenggunaQuery::create()
                        ->filterByPrimaryKeys($this->penggunasRelatedByKodeWilayahScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->penggunasRelatedByKodeWilayahScheduledForDeletion = null;
                }
            }

            if ($this->collPenggunasRelatedByKodeWilayah !== null) {
                foreach ($this->collPenggunasRelatedByKodeWilayah as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->sekolahsRelatedByKodeWilayahScheduledForDeletion !== null) {
                if (!$this->sekolahsRelatedByKodeWilayahScheduledForDeletion->isEmpty()) {
                    SekolahQuery::create()
                        ->filterByPrimaryKeys($this->sekolahsRelatedByKodeWilayahScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->sekolahsRelatedByKodeWilayahScheduledForDeletion = null;
                }
            }

            if ($this->collSekolahsRelatedByKodeWilayah !== null) {
                foreach ($this->collSekolahsRelatedByKodeWilayah as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->sekolahsRelatedByKodeWilayahScheduledForDeletion !== null) {
                if (!$this->sekolahsRelatedByKodeWilayahScheduledForDeletion->isEmpty()) {
                    SekolahQuery::create()
                        ->filterByPrimaryKeys($this->sekolahsRelatedByKodeWilayahScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->sekolahsRelatedByKodeWilayahScheduledForDeletion = null;
                }
            }

            if ($this->collSekolahsRelatedByKodeWilayah !== null) {
                foreach ($this->collSekolahsRelatedByKodeWilayah as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->yayasansRelatedByKodeWilayahScheduledForDeletion !== null) {
                if (!$this->yayasansRelatedByKodeWilayahScheduledForDeletion->isEmpty()) {
                    YayasanQuery::create()
                        ->filterByPrimaryKeys($this->yayasansRelatedByKodeWilayahScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->yayasansRelatedByKodeWilayahScheduledForDeletion = null;
                }
            }

            if ($this->collYayasansRelatedByKodeWilayah !== null) {
                foreach ($this->collYayasansRelatedByKodeWilayah as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->yayasansRelatedByKodeWilayahScheduledForDeletion !== null) {
                if (!$this->yayasansRelatedByKodeWilayahScheduledForDeletion->isEmpty()) {
                    YayasanQuery::create()
                        ->filterByPrimaryKeys($this->yayasansRelatedByKodeWilayahScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->yayasansRelatedByKodeWilayahScheduledForDeletion = null;
                }
            }

            if ($this->collYayasansRelatedByKodeWilayah !== null) {
                foreach ($this->collYayasansRelatedByKodeWilayah as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->ptksRelatedByKodeWilayahScheduledForDeletion !== null) {
                if (!$this->ptksRelatedByKodeWilayahScheduledForDeletion->isEmpty()) {
                    PtkQuery::create()
                        ->filterByPrimaryKeys($this->ptksRelatedByKodeWilayahScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->ptksRelatedByKodeWilayahScheduledForDeletion = null;
                }
            }

            if ($this->collPtksRelatedByKodeWilayah !== null) {
                foreach ($this->collPtksRelatedByKodeWilayah as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->ptksRelatedByKodeWilayahScheduledForDeletion !== null) {
                if (!$this->ptksRelatedByKodeWilayahScheduledForDeletion->isEmpty()) {
                    PtkQuery::create()
                        ->filterByPrimaryKeys($this->ptksRelatedByKodeWilayahScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->ptksRelatedByKodeWilayahScheduledForDeletion = null;
                }
            }

            if ($this->collPtksRelatedByKodeWilayah !== null) {
                foreach ($this->collPtksRelatedByKodeWilayah as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->wtSrcSyncLogsRelatedByKodeWilayahScheduledForDeletion !== null) {
                if (!$this->wtSrcSyncLogsRelatedByKodeWilayahScheduledForDeletion->isEmpty()) {
                    WtSrcSyncLogQuery::create()
                        ->filterByPrimaryKeys($this->wtSrcSyncLogsRelatedByKodeWilayahScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->wtSrcSyncLogsRelatedByKodeWilayahScheduledForDeletion = null;
                }
            }

            if ($this->collWtSrcSyncLogsRelatedByKodeWilayah !== null) {
                foreach ($this->collWtSrcSyncLogsRelatedByKodeWilayah as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->wtSrcSyncLogsRelatedByKodeWilayahScheduledForDeletion !== null) {
                if (!$this->wtSrcSyncLogsRelatedByKodeWilayahScheduledForDeletion->isEmpty()) {
                    WtSrcSyncLogQuery::create()
                        ->filterByPrimaryKeys($this->wtSrcSyncLogsRelatedByKodeWilayahScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->wtSrcSyncLogsRelatedByKodeWilayahScheduledForDeletion = null;
                }
            }

            if ($this->collWtSrcSyncLogsRelatedByKodeWilayah !== null) {
                foreach ($this->collWtSrcSyncLogsRelatedByKodeWilayah as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->wtDstSyncLogsRelatedByKodeWilayahScheduledForDeletion !== null) {
                if (!$this->wtDstSyncLogsRelatedByKodeWilayahScheduledForDeletion->isEmpty()) {
                    WtDstSyncLogQuery::create()
                        ->filterByPrimaryKeys($this->wtDstSyncLogsRelatedByKodeWilayahScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->wtDstSyncLogsRelatedByKodeWilayahScheduledForDeletion = null;
                }
            }

            if ($this->collWtDstSyncLogsRelatedByKodeWilayah !== null) {
                foreach ($this->collWtDstSyncLogsRelatedByKodeWilayah as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->wtDstSyncLogsRelatedByKodeWilayahScheduledForDeletion !== null) {
                if (!$this->wtDstSyncLogsRelatedByKodeWilayahScheduledForDeletion->isEmpty()) {
                    WtDstSyncLogQuery::create()
                        ->filterByPrimaryKeys($this->wtDstSyncLogsRelatedByKodeWilayahScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->wtDstSyncLogsRelatedByKodeWilayahScheduledForDeletion = null;
                }
            }

            if ($this->collWtDstSyncLogsRelatedByKodeWilayah !== null) {
                foreach ($this->collWtDstSyncLogsRelatedByKodeWilayah as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->wsyncSessionsRelatedByKodeWilayahScheduledForDeletion !== null) {
                if (!$this->wsyncSessionsRelatedByKodeWilayahScheduledForDeletion->isEmpty()) {
                    WsyncSessionQuery::create()
                        ->filterByPrimaryKeys($this->wsyncSessionsRelatedByKodeWilayahScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->wsyncSessionsRelatedByKodeWilayahScheduledForDeletion = null;
                }
            }

            if ($this->collWsyncSessionsRelatedByKodeWilayah !== null) {
                foreach ($this->collWsyncSessionsRelatedByKodeWilayah as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->wsyncSessionsRelatedByKodeWilayahScheduledForDeletion !== null) {
                if (!$this->wsyncSessionsRelatedByKodeWilayahScheduledForDeletion->isEmpty()) {
                    WsyncSessionQuery::create()
                        ->filterByPrimaryKeys($this->wsyncSessionsRelatedByKodeWilayahScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->wsyncSessionsRelatedByKodeWilayahScheduledForDeletion = null;
                }
            }

            if ($this->collWsyncSessionsRelatedByKodeWilayah !== null) {
                foreach ($this->collWsyncSessionsRelatedByKodeWilayah as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->wsrcSyncLogsRelatedByKodeWilayahScheduledForDeletion !== null) {
                if (!$this->wsrcSyncLogsRelatedByKodeWilayahScheduledForDeletion->isEmpty()) {
                    WsrcSyncLogQuery::create()
                        ->filterByPrimaryKeys($this->wsrcSyncLogsRelatedByKodeWilayahScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->wsrcSyncLogsRelatedByKodeWilayahScheduledForDeletion = null;
                }
            }

            if ($this->collWsrcSyncLogsRelatedByKodeWilayah !== null) {
                foreach ($this->collWsrcSyncLogsRelatedByKodeWilayah as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->wsrcSyncLogsRelatedByKodeWilayahScheduledForDeletion !== null) {
                if (!$this->wsrcSyncLogsRelatedByKodeWilayahScheduledForDeletion->isEmpty()) {
                    WsrcSyncLogQuery::create()
                        ->filterByPrimaryKeys($this->wsrcSyncLogsRelatedByKodeWilayahScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->wsrcSyncLogsRelatedByKodeWilayahScheduledForDeletion = null;
                }
            }

            if ($this->collWsrcSyncLogsRelatedByKodeWilayah !== null) {
                foreach ($this->collWsrcSyncLogsRelatedByKodeWilayah as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->wdstSyncLogsRelatedByKodeWilayahScheduledForDeletion !== null) {
                if (!$this->wdstSyncLogsRelatedByKodeWilayahScheduledForDeletion->isEmpty()) {
                    WdstSyncLogQuery::create()
                        ->filterByPrimaryKeys($this->wdstSyncLogsRelatedByKodeWilayahScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->wdstSyncLogsRelatedByKodeWilayahScheduledForDeletion = null;
                }
            }

            if ($this->collWdstSyncLogsRelatedByKodeWilayah !== null) {
                foreach ($this->collWdstSyncLogsRelatedByKodeWilayah as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->wdstSyncLogsRelatedByKodeWilayahScheduledForDeletion !== null) {
                if (!$this->wdstSyncLogsRelatedByKodeWilayahScheduledForDeletion->isEmpty()) {
                    WdstSyncLogQuery::create()
                        ->filterByPrimaryKeys($this->wdstSyncLogsRelatedByKodeWilayahScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->wdstSyncLogsRelatedByKodeWilayahScheduledForDeletion = null;
                }
            }

            if ($this->collWdstSyncLogsRelatedByKodeWilayah !== null) {
                foreach ($this->collWdstSyncLogsRelatedByKodeWilayah as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->wPendingJobsRelatedByKodeWilayahScheduledForDeletion !== null) {
                if (!$this->wPendingJobsRelatedByKodeWilayahScheduledForDeletion->isEmpty()) {
                    WPendingJobQuery::create()
                        ->filterByPrimaryKeys($this->wPendingJobsRelatedByKodeWilayahScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->wPendingJobsRelatedByKodeWilayahScheduledForDeletion = null;
                }
            }

            if ($this->collWPendingJobsRelatedByKodeWilayah !== null) {
                foreach ($this->collWPendingJobsRelatedByKodeWilayah as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->wPendingJobsRelatedByKodeWilayahScheduledForDeletion !== null) {
                if (!$this->wPendingJobsRelatedByKodeWilayahScheduledForDeletion->isEmpty()) {
                    WPendingJobQuery::create()
                        ->filterByPrimaryKeys($this->wPendingJobsRelatedByKodeWilayahScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->wPendingJobsRelatedByKodeWilayahScheduledForDeletion = null;
                }
            }

            if ($this->collWPendingJobsRelatedByKodeWilayah !== null) {
                foreach ($this->collWPendingJobsRelatedByKodeWilayah as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->lembagaNonSekolahsRelatedByKodeWilayahScheduledForDeletion !== null) {
                if (!$this->lembagaNonSekolahsRelatedByKodeWilayahScheduledForDeletion->isEmpty()) {
                    LembagaNonSekolahQuery::create()
                        ->filterByPrimaryKeys($this->lembagaNonSekolahsRelatedByKodeWilayahScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->lembagaNonSekolahsRelatedByKodeWilayahScheduledForDeletion = null;
                }
            }

            if ($this->collLembagaNonSekolahsRelatedByKodeWilayah !== null) {
                foreach ($this->collLembagaNonSekolahsRelatedByKodeWilayah as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->lembagaNonSekolahsRelatedByKodeWilayahScheduledForDeletion !== null) {
                if (!$this->lembagaNonSekolahsRelatedByKodeWilayahScheduledForDeletion->isEmpty()) {
                    LembagaNonSekolahQuery::create()
                        ->filterByPrimaryKeys($this->lembagaNonSekolahsRelatedByKodeWilayahScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->lembagaNonSekolahsRelatedByKodeWilayahScheduledForDeletion = null;
                }
            }

            if ($this->collLembagaNonSekolahsRelatedByKodeWilayah !== null) {
                foreach ($this->collLembagaNonSekolahsRelatedByKodeWilayah as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->demografisRelatedByKodeWilayahScheduledForDeletion !== null) {
                if (!$this->demografisRelatedByKodeWilayahScheduledForDeletion->isEmpty()) {
                    DemografiQuery::create()
                        ->filterByPrimaryKeys($this->demografisRelatedByKodeWilayahScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->demografisRelatedByKodeWilayahScheduledForDeletion = null;
                }
            }

            if ($this->collDemografisRelatedByKodeWilayah !== null) {
                foreach ($this->collDemografisRelatedByKodeWilayah as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->demografisRelatedByKodeWilayahScheduledForDeletion !== null) {
                if (!$this->demografisRelatedByKodeWilayahScheduledForDeletion->isEmpty()) {
                    DemografiQuery::create()
                        ->filterByPrimaryKeys($this->demografisRelatedByKodeWilayahScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->demografisRelatedByKodeWilayahScheduledForDeletion = null;
                }
            }

            if ($this->collDemografisRelatedByKodeWilayah !== null) {
                foreach ($this->collDemografisRelatedByKodeWilayah as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $criteria = $this->buildCriteria();
        $pk = BasePeer::doInsert($criteria, $con);
        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aLevelWilayah !== null) {
                if (!$this->aLevelWilayah->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aLevelWilayah->getValidationFailures());
                }
            }

            if ($this->aMstWilayahRelatedByMstKodeWilayah !== null) {
                if (!$this->aMstWilayahRelatedByMstKodeWilayah->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aMstWilayahRelatedByMstKodeWilayah->getValidationFailures());
                }
            }

            if ($this->aNegara !== null) {
                if (!$this->aNegara->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aNegara->getValidationFailures());
                }
            }


            if (($retval = MstWilayahPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collTetanggaKabkotasRelatedByKodeWilayah !== null) {
                    foreach ($this->collTetanggaKabkotasRelatedByKodeWilayah as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collTetanggaKabkotasRelatedByMstKodeWilayah !== null) {
                    foreach ($this->collTetanggaKabkotasRelatedByMstKodeWilayah as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPesertaDidiksRelatedByKodeWilayah !== null) {
                    foreach ($this->collPesertaDidiksRelatedByKodeWilayah as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPesertaDidiksRelatedByKodeWilayah !== null) {
                    foreach ($this->collPesertaDidiksRelatedByKodeWilayah as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collMstWilayahsRelatedByKodeWilayah !== null) {
                    foreach ($this->collMstWilayahsRelatedByKodeWilayah as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPenggunasRelatedByKodeWilayah !== null) {
                    foreach ($this->collPenggunasRelatedByKodeWilayah as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPenggunasRelatedByKodeWilayah !== null) {
                    foreach ($this->collPenggunasRelatedByKodeWilayah as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSekolahsRelatedByKodeWilayah !== null) {
                    foreach ($this->collSekolahsRelatedByKodeWilayah as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSekolahsRelatedByKodeWilayah !== null) {
                    foreach ($this->collSekolahsRelatedByKodeWilayah as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collYayasansRelatedByKodeWilayah !== null) {
                    foreach ($this->collYayasansRelatedByKodeWilayah as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collYayasansRelatedByKodeWilayah !== null) {
                    foreach ($this->collYayasansRelatedByKodeWilayah as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPtksRelatedByKodeWilayah !== null) {
                    foreach ($this->collPtksRelatedByKodeWilayah as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPtksRelatedByKodeWilayah !== null) {
                    foreach ($this->collPtksRelatedByKodeWilayah as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collWtSrcSyncLogsRelatedByKodeWilayah !== null) {
                    foreach ($this->collWtSrcSyncLogsRelatedByKodeWilayah as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collWtSrcSyncLogsRelatedByKodeWilayah !== null) {
                    foreach ($this->collWtSrcSyncLogsRelatedByKodeWilayah as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collWtDstSyncLogsRelatedByKodeWilayah !== null) {
                    foreach ($this->collWtDstSyncLogsRelatedByKodeWilayah as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collWtDstSyncLogsRelatedByKodeWilayah !== null) {
                    foreach ($this->collWtDstSyncLogsRelatedByKodeWilayah as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collWsyncSessionsRelatedByKodeWilayah !== null) {
                    foreach ($this->collWsyncSessionsRelatedByKodeWilayah as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collWsyncSessionsRelatedByKodeWilayah !== null) {
                    foreach ($this->collWsyncSessionsRelatedByKodeWilayah as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collWsrcSyncLogsRelatedByKodeWilayah !== null) {
                    foreach ($this->collWsrcSyncLogsRelatedByKodeWilayah as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collWsrcSyncLogsRelatedByKodeWilayah !== null) {
                    foreach ($this->collWsrcSyncLogsRelatedByKodeWilayah as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collWdstSyncLogsRelatedByKodeWilayah !== null) {
                    foreach ($this->collWdstSyncLogsRelatedByKodeWilayah as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collWdstSyncLogsRelatedByKodeWilayah !== null) {
                    foreach ($this->collWdstSyncLogsRelatedByKodeWilayah as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collWPendingJobsRelatedByKodeWilayah !== null) {
                    foreach ($this->collWPendingJobsRelatedByKodeWilayah as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collWPendingJobsRelatedByKodeWilayah !== null) {
                    foreach ($this->collWPendingJobsRelatedByKodeWilayah as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collLembagaNonSekolahsRelatedByKodeWilayah !== null) {
                    foreach ($this->collLembagaNonSekolahsRelatedByKodeWilayah as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collLembagaNonSekolahsRelatedByKodeWilayah !== null) {
                    foreach ($this->collLembagaNonSekolahsRelatedByKodeWilayah as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collDemografisRelatedByKodeWilayah !== null) {
                    foreach ($this->collDemografisRelatedByKodeWilayah as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collDemografisRelatedByKodeWilayah !== null) {
                    foreach ($this->collDemografisRelatedByKodeWilayah as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = MstWilayahPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getKodeWilayah();
                break;
            case 1:
                return $this->getNama();
                break;
            case 2:
                return $this->getIdLevelWilayah();
                break;
            case 3:
                return $this->getMstKodeWilayah();
                break;
            case 4:
                return $this->getNegaraId();
                break;
            case 5:
                return $this->getAsalWilayah();
                break;
            case 6:
                return $this->getKodeBps();
                break;
            case 7:
                return $this->getKodeDagri();
                break;
            case 8:
                return $this->getKodeKeu();
                break;
            case 9:
                return $this->getCreateDate();
                break;
            case 10:
                return $this->getLastUpdate();
                break;
            case 11:
                return $this->getExpiredDate();
                break;
            case 12:
                return $this->getLastSync();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['MstWilayah'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['MstWilayah'][$this->getPrimaryKey()] = true;
        $keys = MstWilayahPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getKodeWilayah(),
            $keys[1] => $this->getNama(),
            $keys[2] => $this->getIdLevelWilayah(),
            $keys[3] => $this->getMstKodeWilayah(),
            $keys[4] => $this->getNegaraId(),
            $keys[5] => $this->getAsalWilayah(),
            $keys[6] => $this->getKodeBps(),
            $keys[7] => $this->getKodeDagri(),
            $keys[8] => $this->getKodeKeu(),
            $keys[9] => $this->getCreateDate(),
            $keys[10] => $this->getLastUpdate(),
            $keys[11] => $this->getExpiredDate(),
            $keys[12] => $this->getLastSync(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aLevelWilayah) {
                $result['LevelWilayah'] = $this->aLevelWilayah->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aMstWilayahRelatedByMstKodeWilayah) {
                $result['MstWilayahRelatedByMstKodeWilayah'] = $this->aMstWilayahRelatedByMstKodeWilayah->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aNegara) {
                $result['Negara'] = $this->aNegara->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collTetanggaKabkotasRelatedByKodeWilayah) {
                $result['TetanggaKabkotasRelatedByKodeWilayah'] = $this->collTetanggaKabkotasRelatedByKodeWilayah->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collTetanggaKabkotasRelatedByMstKodeWilayah) {
                $result['TetanggaKabkotasRelatedByMstKodeWilayah'] = $this->collTetanggaKabkotasRelatedByMstKodeWilayah->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPesertaDidiksRelatedByKodeWilayah) {
                $result['PesertaDidiksRelatedByKodeWilayah'] = $this->collPesertaDidiksRelatedByKodeWilayah->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPesertaDidiksRelatedByKodeWilayah) {
                $result['PesertaDidiksRelatedByKodeWilayah'] = $this->collPesertaDidiksRelatedByKodeWilayah->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collMstWilayahsRelatedByKodeWilayah) {
                $result['MstWilayahsRelatedByKodeWilayah'] = $this->collMstWilayahsRelatedByKodeWilayah->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPenggunasRelatedByKodeWilayah) {
                $result['PenggunasRelatedByKodeWilayah'] = $this->collPenggunasRelatedByKodeWilayah->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPenggunasRelatedByKodeWilayah) {
                $result['PenggunasRelatedByKodeWilayah'] = $this->collPenggunasRelatedByKodeWilayah->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSekolahsRelatedByKodeWilayah) {
                $result['SekolahsRelatedByKodeWilayah'] = $this->collSekolahsRelatedByKodeWilayah->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSekolahsRelatedByKodeWilayah) {
                $result['SekolahsRelatedByKodeWilayah'] = $this->collSekolahsRelatedByKodeWilayah->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collYayasansRelatedByKodeWilayah) {
                $result['YayasansRelatedByKodeWilayah'] = $this->collYayasansRelatedByKodeWilayah->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collYayasansRelatedByKodeWilayah) {
                $result['YayasansRelatedByKodeWilayah'] = $this->collYayasansRelatedByKodeWilayah->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPtksRelatedByKodeWilayah) {
                $result['PtksRelatedByKodeWilayah'] = $this->collPtksRelatedByKodeWilayah->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPtksRelatedByKodeWilayah) {
                $result['PtksRelatedByKodeWilayah'] = $this->collPtksRelatedByKodeWilayah->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collWtSrcSyncLogsRelatedByKodeWilayah) {
                $result['WtSrcSyncLogsRelatedByKodeWilayah'] = $this->collWtSrcSyncLogsRelatedByKodeWilayah->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collWtSrcSyncLogsRelatedByKodeWilayah) {
                $result['WtSrcSyncLogsRelatedByKodeWilayah'] = $this->collWtSrcSyncLogsRelatedByKodeWilayah->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collWtDstSyncLogsRelatedByKodeWilayah) {
                $result['WtDstSyncLogsRelatedByKodeWilayah'] = $this->collWtDstSyncLogsRelatedByKodeWilayah->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collWtDstSyncLogsRelatedByKodeWilayah) {
                $result['WtDstSyncLogsRelatedByKodeWilayah'] = $this->collWtDstSyncLogsRelatedByKodeWilayah->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collWsyncSessionsRelatedByKodeWilayah) {
                $result['WsyncSessionsRelatedByKodeWilayah'] = $this->collWsyncSessionsRelatedByKodeWilayah->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collWsyncSessionsRelatedByKodeWilayah) {
                $result['WsyncSessionsRelatedByKodeWilayah'] = $this->collWsyncSessionsRelatedByKodeWilayah->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collWsrcSyncLogsRelatedByKodeWilayah) {
                $result['WsrcSyncLogsRelatedByKodeWilayah'] = $this->collWsrcSyncLogsRelatedByKodeWilayah->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collWsrcSyncLogsRelatedByKodeWilayah) {
                $result['WsrcSyncLogsRelatedByKodeWilayah'] = $this->collWsrcSyncLogsRelatedByKodeWilayah->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collWdstSyncLogsRelatedByKodeWilayah) {
                $result['WdstSyncLogsRelatedByKodeWilayah'] = $this->collWdstSyncLogsRelatedByKodeWilayah->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collWdstSyncLogsRelatedByKodeWilayah) {
                $result['WdstSyncLogsRelatedByKodeWilayah'] = $this->collWdstSyncLogsRelatedByKodeWilayah->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collWPendingJobsRelatedByKodeWilayah) {
                $result['WPendingJobsRelatedByKodeWilayah'] = $this->collWPendingJobsRelatedByKodeWilayah->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collWPendingJobsRelatedByKodeWilayah) {
                $result['WPendingJobsRelatedByKodeWilayah'] = $this->collWPendingJobsRelatedByKodeWilayah->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collLembagaNonSekolahsRelatedByKodeWilayah) {
                $result['LembagaNonSekolahsRelatedByKodeWilayah'] = $this->collLembagaNonSekolahsRelatedByKodeWilayah->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collLembagaNonSekolahsRelatedByKodeWilayah) {
                $result['LembagaNonSekolahsRelatedByKodeWilayah'] = $this->collLembagaNonSekolahsRelatedByKodeWilayah->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collDemografisRelatedByKodeWilayah) {
                $result['DemografisRelatedByKodeWilayah'] = $this->collDemografisRelatedByKodeWilayah->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collDemografisRelatedByKodeWilayah) {
                $result['DemografisRelatedByKodeWilayah'] = $this->collDemografisRelatedByKodeWilayah->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = MstWilayahPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setKodeWilayah($value);
                break;
            case 1:
                $this->setNama($value);
                break;
            case 2:
                $this->setIdLevelWilayah($value);
                break;
            case 3:
                $this->setMstKodeWilayah($value);
                break;
            case 4:
                $this->setNegaraId($value);
                break;
            case 5:
                $this->setAsalWilayah($value);
                break;
            case 6:
                $this->setKodeBps($value);
                break;
            case 7:
                $this->setKodeDagri($value);
                break;
            case 8:
                $this->setKodeKeu($value);
                break;
            case 9:
                $this->setCreateDate($value);
                break;
            case 10:
                $this->setLastUpdate($value);
                break;
            case 11:
                $this->setExpiredDate($value);
                break;
            case 12:
                $this->setLastSync($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = MstWilayahPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setKodeWilayah($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setNama($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setIdLevelWilayah($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setMstKodeWilayah($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setNegaraId($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setAsalWilayah($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setKodeBps($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setKodeDagri($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setKodeKeu($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setCreateDate($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setLastUpdate($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setExpiredDate($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setLastSync($arr[$keys[12]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(MstWilayahPeer::DATABASE_NAME);

        if ($this->isColumnModified(MstWilayahPeer::KODE_WILAYAH)) $criteria->add(MstWilayahPeer::KODE_WILAYAH, $this->kode_wilayah);
        if ($this->isColumnModified(MstWilayahPeer::NAMA)) $criteria->add(MstWilayahPeer::NAMA, $this->nama);
        if ($this->isColumnModified(MstWilayahPeer::ID_LEVEL_WILAYAH)) $criteria->add(MstWilayahPeer::ID_LEVEL_WILAYAH, $this->id_level_wilayah);
        if ($this->isColumnModified(MstWilayahPeer::MST_KODE_WILAYAH)) $criteria->add(MstWilayahPeer::MST_KODE_WILAYAH, $this->mst_kode_wilayah);
        if ($this->isColumnModified(MstWilayahPeer::NEGARA_ID)) $criteria->add(MstWilayahPeer::NEGARA_ID, $this->negara_id);
        if ($this->isColumnModified(MstWilayahPeer::ASAL_WILAYAH)) $criteria->add(MstWilayahPeer::ASAL_WILAYAH, $this->asal_wilayah);
        if ($this->isColumnModified(MstWilayahPeer::KODE_BPS)) $criteria->add(MstWilayahPeer::KODE_BPS, $this->kode_bps);
        if ($this->isColumnModified(MstWilayahPeer::KODE_DAGRI)) $criteria->add(MstWilayahPeer::KODE_DAGRI, $this->kode_dagri);
        if ($this->isColumnModified(MstWilayahPeer::KODE_KEU)) $criteria->add(MstWilayahPeer::KODE_KEU, $this->kode_keu);
        if ($this->isColumnModified(MstWilayahPeer::CREATE_DATE)) $criteria->add(MstWilayahPeer::CREATE_DATE, $this->create_date);
        if ($this->isColumnModified(MstWilayahPeer::LAST_UPDATE)) $criteria->add(MstWilayahPeer::LAST_UPDATE, $this->last_update);
        if ($this->isColumnModified(MstWilayahPeer::EXPIRED_DATE)) $criteria->add(MstWilayahPeer::EXPIRED_DATE, $this->expired_date);
        if ($this->isColumnModified(MstWilayahPeer::LAST_SYNC)) $criteria->add(MstWilayahPeer::LAST_SYNC, $this->last_sync);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(MstWilayahPeer::DATABASE_NAME);
        $criteria->add(MstWilayahPeer::KODE_WILAYAH, $this->kode_wilayah);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getKodeWilayah();
    }

    /**
     * Generic method to set the primary key (kode_wilayah column).
     *
     * @param  string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setKodeWilayah($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getKodeWilayah();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of MstWilayah (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setNama($this->getNama());
        $copyObj->setIdLevelWilayah($this->getIdLevelWilayah());
        $copyObj->setMstKodeWilayah($this->getMstKodeWilayah());
        $copyObj->setNegaraId($this->getNegaraId());
        $copyObj->setAsalWilayah($this->getAsalWilayah());
        $copyObj->setKodeBps($this->getKodeBps());
        $copyObj->setKodeDagri($this->getKodeDagri());
        $copyObj->setKodeKeu($this->getKodeKeu());
        $copyObj->setCreateDate($this->getCreateDate());
        $copyObj->setLastUpdate($this->getLastUpdate());
        $copyObj->setExpiredDate($this->getExpiredDate());
        $copyObj->setLastSync($this->getLastSync());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getTetanggaKabkotasRelatedByKodeWilayah() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addTetanggaKabkotaRelatedByKodeWilayah($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getTetanggaKabkotasRelatedByMstKodeWilayah() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addTetanggaKabkotaRelatedByMstKodeWilayah($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPesertaDidiksRelatedByKodeWilayah() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPesertaDidikRelatedByKodeWilayah($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPesertaDidiksRelatedByKodeWilayah() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPesertaDidikRelatedByKodeWilayah($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getMstWilayahsRelatedByKodeWilayah() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addMstWilayahRelatedByKodeWilayah($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPenggunasRelatedByKodeWilayah() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPenggunaRelatedByKodeWilayah($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPenggunasRelatedByKodeWilayah() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPenggunaRelatedByKodeWilayah($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSekolahsRelatedByKodeWilayah() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSekolahRelatedByKodeWilayah($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSekolahsRelatedByKodeWilayah() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSekolahRelatedByKodeWilayah($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getYayasansRelatedByKodeWilayah() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addYayasanRelatedByKodeWilayah($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getYayasansRelatedByKodeWilayah() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addYayasanRelatedByKodeWilayah($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPtksRelatedByKodeWilayah() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPtkRelatedByKodeWilayah($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPtksRelatedByKodeWilayah() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPtkRelatedByKodeWilayah($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getWtSrcSyncLogsRelatedByKodeWilayah() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addWtSrcSyncLogRelatedByKodeWilayah($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getWtSrcSyncLogsRelatedByKodeWilayah() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addWtSrcSyncLogRelatedByKodeWilayah($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getWtDstSyncLogsRelatedByKodeWilayah() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addWtDstSyncLogRelatedByKodeWilayah($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getWtDstSyncLogsRelatedByKodeWilayah() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addWtDstSyncLogRelatedByKodeWilayah($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getWsyncSessionsRelatedByKodeWilayah() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addWsyncSessionRelatedByKodeWilayah($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getWsyncSessionsRelatedByKodeWilayah() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addWsyncSessionRelatedByKodeWilayah($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getWsrcSyncLogsRelatedByKodeWilayah() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addWsrcSyncLogRelatedByKodeWilayah($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getWsrcSyncLogsRelatedByKodeWilayah() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addWsrcSyncLogRelatedByKodeWilayah($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getWdstSyncLogsRelatedByKodeWilayah() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addWdstSyncLogRelatedByKodeWilayah($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getWdstSyncLogsRelatedByKodeWilayah() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addWdstSyncLogRelatedByKodeWilayah($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getWPendingJobsRelatedByKodeWilayah() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addWPendingJobRelatedByKodeWilayah($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getWPendingJobsRelatedByKodeWilayah() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addWPendingJobRelatedByKodeWilayah($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getLembagaNonSekolahsRelatedByKodeWilayah() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addLembagaNonSekolahRelatedByKodeWilayah($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getLembagaNonSekolahsRelatedByKodeWilayah() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addLembagaNonSekolahRelatedByKodeWilayah($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getDemografisRelatedByKodeWilayah() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addDemografiRelatedByKodeWilayah($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getDemografisRelatedByKodeWilayah() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addDemografiRelatedByKodeWilayah($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setKodeWilayah(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return MstWilayah Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return MstWilayahPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new MstWilayahPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a LevelWilayah object.
     *
     * @param             LevelWilayah $v
     * @return MstWilayah The current object (for fluent API support)
     * @throws PropelException
     */
    public function setLevelWilayah(LevelWilayah $v = null)
    {
        if ($v === null) {
            $this->setIdLevelWilayah(NULL);
        } else {
            $this->setIdLevelWilayah($v->getIdLevelWilayah());
        }

        $this->aLevelWilayah = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the LevelWilayah object, it will not be re-added.
        if ($v !== null) {
            $v->addMstWilayah($this);
        }


        return $this;
    }


    /**
     * Get the associated LevelWilayah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return LevelWilayah The associated LevelWilayah object.
     * @throws PropelException
     */
    public function getLevelWilayah(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aLevelWilayah === null && ($this->id_level_wilayah !== null) && $doQuery) {
            $this->aLevelWilayah = LevelWilayahQuery::create()->findPk($this->id_level_wilayah, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aLevelWilayah->addMstWilayahs($this);
             */
        }

        return $this->aLevelWilayah;
    }

    /**
     * Declares an association between this object and a MstWilayah object.
     *
     * @param             MstWilayah $v
     * @return MstWilayah The current object (for fluent API support)
     * @throws PropelException
     */
    public function setMstWilayahRelatedByMstKodeWilayah(MstWilayah $v = null)
    {
        if ($v === null) {
            $this->setMstKodeWilayah(NULL);
        } else {
            $this->setMstKodeWilayah($v->getKodeWilayah());
        }

        $this->aMstWilayahRelatedByMstKodeWilayah = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the MstWilayah object, it will not be re-added.
        if ($v !== null) {
            $v->addMstWilayahRelatedByKodeWilayah($this);
        }


        return $this;
    }


    /**
     * Get the associated MstWilayah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return MstWilayah The associated MstWilayah object.
     * @throws PropelException
     */
    public function getMstWilayahRelatedByMstKodeWilayah(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aMstWilayahRelatedByMstKodeWilayah === null && (($this->mst_kode_wilayah !== "" && $this->mst_kode_wilayah !== null)) && $doQuery) {
            $this->aMstWilayahRelatedByMstKodeWilayah = MstWilayahQuery::create()->findPk($this->mst_kode_wilayah, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aMstWilayahRelatedByMstKodeWilayah->addMstWilayahsRelatedByKodeWilayah($this);
             */
        }

        return $this->aMstWilayahRelatedByMstKodeWilayah;
    }

    /**
     * Declares an association between this object and a Negara object.
     *
     * @param             Negara $v
     * @return MstWilayah The current object (for fluent API support)
     * @throws PropelException
     */
    public function setNegara(Negara $v = null)
    {
        if ($v === null) {
            $this->setNegaraId(NULL);
        } else {
            $this->setNegaraId($v->getNegaraId());
        }

        $this->aNegara = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Negara object, it will not be re-added.
        if ($v !== null) {
            $v->addMstWilayah($this);
        }


        return $this;
    }


    /**
     * Get the associated Negara object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Negara The associated Negara object.
     * @throws PropelException
     */
    public function getNegara(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aNegara === null && (($this->negara_id !== "" && $this->negara_id !== null)) && $doQuery) {
            $this->aNegara = NegaraQuery::create()->findPk($this->negara_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aNegara->addMstWilayahs($this);
             */
        }

        return $this->aNegara;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('TetanggaKabkotaRelatedByKodeWilayah' == $relationName) {
            $this->initTetanggaKabkotasRelatedByKodeWilayah();
        }
        if ('TetanggaKabkotaRelatedByMstKodeWilayah' == $relationName) {
            $this->initTetanggaKabkotasRelatedByMstKodeWilayah();
        }
        if ('PesertaDidikRelatedByKodeWilayah' == $relationName) {
            $this->initPesertaDidiksRelatedByKodeWilayah();
        }
        if ('PesertaDidikRelatedByKodeWilayah' == $relationName) {
            $this->initPesertaDidiksRelatedByKodeWilayah();
        }
        if ('MstWilayahRelatedByKodeWilayah' == $relationName) {
            $this->initMstWilayahsRelatedByKodeWilayah();
        }
        if ('PenggunaRelatedByKodeWilayah' == $relationName) {
            $this->initPenggunasRelatedByKodeWilayah();
        }
        if ('PenggunaRelatedByKodeWilayah' == $relationName) {
            $this->initPenggunasRelatedByKodeWilayah();
        }
        if ('SekolahRelatedByKodeWilayah' == $relationName) {
            $this->initSekolahsRelatedByKodeWilayah();
        }
        if ('SekolahRelatedByKodeWilayah' == $relationName) {
            $this->initSekolahsRelatedByKodeWilayah();
        }
        if ('YayasanRelatedByKodeWilayah' == $relationName) {
            $this->initYayasansRelatedByKodeWilayah();
        }
        if ('YayasanRelatedByKodeWilayah' == $relationName) {
            $this->initYayasansRelatedByKodeWilayah();
        }
        if ('PtkRelatedByKodeWilayah' == $relationName) {
            $this->initPtksRelatedByKodeWilayah();
        }
        if ('PtkRelatedByKodeWilayah' == $relationName) {
            $this->initPtksRelatedByKodeWilayah();
        }
        if ('WtSrcSyncLogRelatedByKodeWilayah' == $relationName) {
            $this->initWtSrcSyncLogsRelatedByKodeWilayah();
        }
        if ('WtSrcSyncLogRelatedByKodeWilayah' == $relationName) {
            $this->initWtSrcSyncLogsRelatedByKodeWilayah();
        }
        if ('WtDstSyncLogRelatedByKodeWilayah' == $relationName) {
            $this->initWtDstSyncLogsRelatedByKodeWilayah();
        }
        if ('WtDstSyncLogRelatedByKodeWilayah' == $relationName) {
            $this->initWtDstSyncLogsRelatedByKodeWilayah();
        }
        if ('WsyncSessionRelatedByKodeWilayah' == $relationName) {
            $this->initWsyncSessionsRelatedByKodeWilayah();
        }
        if ('WsyncSessionRelatedByKodeWilayah' == $relationName) {
            $this->initWsyncSessionsRelatedByKodeWilayah();
        }
        if ('WsrcSyncLogRelatedByKodeWilayah' == $relationName) {
            $this->initWsrcSyncLogsRelatedByKodeWilayah();
        }
        if ('WsrcSyncLogRelatedByKodeWilayah' == $relationName) {
            $this->initWsrcSyncLogsRelatedByKodeWilayah();
        }
        if ('WdstSyncLogRelatedByKodeWilayah' == $relationName) {
            $this->initWdstSyncLogsRelatedByKodeWilayah();
        }
        if ('WdstSyncLogRelatedByKodeWilayah' == $relationName) {
            $this->initWdstSyncLogsRelatedByKodeWilayah();
        }
        if ('WPendingJobRelatedByKodeWilayah' == $relationName) {
            $this->initWPendingJobsRelatedByKodeWilayah();
        }
        if ('WPendingJobRelatedByKodeWilayah' == $relationName) {
            $this->initWPendingJobsRelatedByKodeWilayah();
        }
        if ('LembagaNonSekolahRelatedByKodeWilayah' == $relationName) {
            $this->initLembagaNonSekolahsRelatedByKodeWilayah();
        }
        if ('LembagaNonSekolahRelatedByKodeWilayah' == $relationName) {
            $this->initLembagaNonSekolahsRelatedByKodeWilayah();
        }
        if ('DemografiRelatedByKodeWilayah' == $relationName) {
            $this->initDemografisRelatedByKodeWilayah();
        }
        if ('DemografiRelatedByKodeWilayah' == $relationName) {
            $this->initDemografisRelatedByKodeWilayah();
        }
    }

    /**
     * Clears out the collTetanggaKabkotasRelatedByKodeWilayah collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return MstWilayah The current object (for fluent API support)
     * @see        addTetanggaKabkotasRelatedByKodeWilayah()
     */
    public function clearTetanggaKabkotasRelatedByKodeWilayah()
    {
        $this->collTetanggaKabkotasRelatedByKodeWilayah = null; // important to set this to null since that means it is uninitialized
        $this->collTetanggaKabkotasRelatedByKodeWilayahPartial = null;

        return $this;
    }

    /**
     * reset is the collTetanggaKabkotasRelatedByKodeWilayah collection loaded partially
     *
     * @return void
     */
    public function resetPartialTetanggaKabkotasRelatedByKodeWilayah($v = true)
    {
        $this->collTetanggaKabkotasRelatedByKodeWilayahPartial = $v;
    }

    /**
     * Initializes the collTetanggaKabkotasRelatedByKodeWilayah collection.
     *
     * By default this just sets the collTetanggaKabkotasRelatedByKodeWilayah collection to an empty array (like clearcollTetanggaKabkotasRelatedByKodeWilayah());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initTetanggaKabkotasRelatedByKodeWilayah($overrideExisting = true)
    {
        if (null !== $this->collTetanggaKabkotasRelatedByKodeWilayah && !$overrideExisting) {
            return;
        }
        $this->collTetanggaKabkotasRelatedByKodeWilayah = new PropelObjectCollection();
        $this->collTetanggaKabkotasRelatedByKodeWilayah->setModel('TetanggaKabkota');
    }

    /**
     * Gets an array of TetanggaKabkota objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this MstWilayah is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|TetanggaKabkota[] List of TetanggaKabkota objects
     * @throws PropelException
     */
    public function getTetanggaKabkotasRelatedByKodeWilayah($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collTetanggaKabkotasRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collTetanggaKabkotasRelatedByKodeWilayah || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collTetanggaKabkotasRelatedByKodeWilayah) {
                // return empty collection
                $this->initTetanggaKabkotasRelatedByKodeWilayah();
            } else {
                $collTetanggaKabkotasRelatedByKodeWilayah = TetanggaKabkotaQuery::create(null, $criteria)
                    ->filterByMstWilayahRelatedByKodeWilayah($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collTetanggaKabkotasRelatedByKodeWilayahPartial && count($collTetanggaKabkotasRelatedByKodeWilayah)) {
                      $this->initTetanggaKabkotasRelatedByKodeWilayah(false);

                      foreach($collTetanggaKabkotasRelatedByKodeWilayah as $obj) {
                        if (false == $this->collTetanggaKabkotasRelatedByKodeWilayah->contains($obj)) {
                          $this->collTetanggaKabkotasRelatedByKodeWilayah->append($obj);
                        }
                      }

                      $this->collTetanggaKabkotasRelatedByKodeWilayahPartial = true;
                    }

                    $collTetanggaKabkotasRelatedByKodeWilayah->getInternalIterator()->rewind();
                    return $collTetanggaKabkotasRelatedByKodeWilayah;
                }

                if($partial && $this->collTetanggaKabkotasRelatedByKodeWilayah) {
                    foreach($this->collTetanggaKabkotasRelatedByKodeWilayah as $obj) {
                        if($obj->isNew()) {
                            $collTetanggaKabkotasRelatedByKodeWilayah[] = $obj;
                        }
                    }
                }

                $this->collTetanggaKabkotasRelatedByKodeWilayah = $collTetanggaKabkotasRelatedByKodeWilayah;
                $this->collTetanggaKabkotasRelatedByKodeWilayahPartial = false;
            }
        }

        return $this->collTetanggaKabkotasRelatedByKodeWilayah;
    }

    /**
     * Sets a collection of TetanggaKabkotaRelatedByKodeWilayah objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $tetanggaKabkotasRelatedByKodeWilayah A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setTetanggaKabkotasRelatedByKodeWilayah(PropelCollection $tetanggaKabkotasRelatedByKodeWilayah, PropelPDO $con = null)
    {
        $tetanggaKabkotasRelatedByKodeWilayahToDelete = $this->getTetanggaKabkotasRelatedByKodeWilayah(new Criteria(), $con)->diff($tetanggaKabkotasRelatedByKodeWilayah);

        $this->tetanggaKabkotasRelatedByKodeWilayahScheduledForDeletion = unserialize(serialize($tetanggaKabkotasRelatedByKodeWilayahToDelete));

        foreach ($tetanggaKabkotasRelatedByKodeWilayahToDelete as $tetanggaKabkotaRelatedByKodeWilayahRemoved) {
            $tetanggaKabkotaRelatedByKodeWilayahRemoved->setMstWilayahRelatedByKodeWilayah(null);
        }

        $this->collTetanggaKabkotasRelatedByKodeWilayah = null;
        foreach ($tetanggaKabkotasRelatedByKodeWilayah as $tetanggaKabkotaRelatedByKodeWilayah) {
            $this->addTetanggaKabkotaRelatedByKodeWilayah($tetanggaKabkotaRelatedByKodeWilayah);
        }

        $this->collTetanggaKabkotasRelatedByKodeWilayah = $tetanggaKabkotasRelatedByKodeWilayah;
        $this->collTetanggaKabkotasRelatedByKodeWilayahPartial = false;

        return $this;
    }

    /**
     * Returns the number of related TetanggaKabkota objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related TetanggaKabkota objects.
     * @throws PropelException
     */
    public function countTetanggaKabkotasRelatedByKodeWilayah(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collTetanggaKabkotasRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collTetanggaKabkotasRelatedByKodeWilayah || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collTetanggaKabkotasRelatedByKodeWilayah) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getTetanggaKabkotasRelatedByKodeWilayah());
            }
            $query = TetanggaKabkotaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMstWilayahRelatedByKodeWilayah($this)
                ->count($con);
        }

        return count($this->collTetanggaKabkotasRelatedByKodeWilayah);
    }

    /**
     * Method called to associate a TetanggaKabkota object to this object
     * through the TetanggaKabkota foreign key attribute.
     *
     * @param    TetanggaKabkota $l TetanggaKabkota
     * @return MstWilayah The current object (for fluent API support)
     */
    public function addTetanggaKabkotaRelatedByKodeWilayah(TetanggaKabkota $l)
    {
        if ($this->collTetanggaKabkotasRelatedByKodeWilayah === null) {
            $this->initTetanggaKabkotasRelatedByKodeWilayah();
            $this->collTetanggaKabkotasRelatedByKodeWilayahPartial = true;
        }
        if (!in_array($l, $this->collTetanggaKabkotasRelatedByKodeWilayah->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddTetanggaKabkotaRelatedByKodeWilayah($l);
        }

        return $this;
    }

    /**
     * @param	TetanggaKabkotaRelatedByKodeWilayah $tetanggaKabkotaRelatedByKodeWilayah The tetanggaKabkotaRelatedByKodeWilayah object to add.
     */
    protected function doAddTetanggaKabkotaRelatedByKodeWilayah($tetanggaKabkotaRelatedByKodeWilayah)
    {
        $this->collTetanggaKabkotasRelatedByKodeWilayah[]= $tetanggaKabkotaRelatedByKodeWilayah;
        $tetanggaKabkotaRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah($this);
    }

    /**
     * @param	TetanggaKabkotaRelatedByKodeWilayah $tetanggaKabkotaRelatedByKodeWilayah The tetanggaKabkotaRelatedByKodeWilayah object to remove.
     * @return MstWilayah The current object (for fluent API support)
     */
    public function removeTetanggaKabkotaRelatedByKodeWilayah($tetanggaKabkotaRelatedByKodeWilayah)
    {
        if ($this->getTetanggaKabkotasRelatedByKodeWilayah()->contains($tetanggaKabkotaRelatedByKodeWilayah)) {
            $this->collTetanggaKabkotasRelatedByKodeWilayah->remove($this->collTetanggaKabkotasRelatedByKodeWilayah->search($tetanggaKabkotaRelatedByKodeWilayah));
            if (null === $this->tetanggaKabkotasRelatedByKodeWilayahScheduledForDeletion) {
                $this->tetanggaKabkotasRelatedByKodeWilayahScheduledForDeletion = clone $this->collTetanggaKabkotasRelatedByKodeWilayah;
                $this->tetanggaKabkotasRelatedByKodeWilayahScheduledForDeletion->clear();
            }
            $this->tetanggaKabkotasRelatedByKodeWilayahScheduledForDeletion[]= clone $tetanggaKabkotaRelatedByKodeWilayah;
            $tetanggaKabkotaRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah(null);
        }

        return $this;
    }

    /**
     * Clears out the collTetanggaKabkotasRelatedByMstKodeWilayah collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return MstWilayah The current object (for fluent API support)
     * @see        addTetanggaKabkotasRelatedByMstKodeWilayah()
     */
    public function clearTetanggaKabkotasRelatedByMstKodeWilayah()
    {
        $this->collTetanggaKabkotasRelatedByMstKodeWilayah = null; // important to set this to null since that means it is uninitialized
        $this->collTetanggaKabkotasRelatedByMstKodeWilayahPartial = null;

        return $this;
    }

    /**
     * reset is the collTetanggaKabkotasRelatedByMstKodeWilayah collection loaded partially
     *
     * @return void
     */
    public function resetPartialTetanggaKabkotasRelatedByMstKodeWilayah($v = true)
    {
        $this->collTetanggaKabkotasRelatedByMstKodeWilayahPartial = $v;
    }

    /**
     * Initializes the collTetanggaKabkotasRelatedByMstKodeWilayah collection.
     *
     * By default this just sets the collTetanggaKabkotasRelatedByMstKodeWilayah collection to an empty array (like clearcollTetanggaKabkotasRelatedByMstKodeWilayah());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initTetanggaKabkotasRelatedByMstKodeWilayah($overrideExisting = true)
    {
        if (null !== $this->collTetanggaKabkotasRelatedByMstKodeWilayah && !$overrideExisting) {
            return;
        }
        $this->collTetanggaKabkotasRelatedByMstKodeWilayah = new PropelObjectCollection();
        $this->collTetanggaKabkotasRelatedByMstKodeWilayah->setModel('TetanggaKabkota');
    }

    /**
     * Gets an array of TetanggaKabkota objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this MstWilayah is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|TetanggaKabkota[] List of TetanggaKabkota objects
     * @throws PropelException
     */
    public function getTetanggaKabkotasRelatedByMstKodeWilayah($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collTetanggaKabkotasRelatedByMstKodeWilayahPartial && !$this->isNew();
        if (null === $this->collTetanggaKabkotasRelatedByMstKodeWilayah || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collTetanggaKabkotasRelatedByMstKodeWilayah) {
                // return empty collection
                $this->initTetanggaKabkotasRelatedByMstKodeWilayah();
            } else {
                $collTetanggaKabkotasRelatedByMstKodeWilayah = TetanggaKabkotaQuery::create(null, $criteria)
                    ->filterByMstWilayahRelatedByMstKodeWilayah($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collTetanggaKabkotasRelatedByMstKodeWilayahPartial && count($collTetanggaKabkotasRelatedByMstKodeWilayah)) {
                      $this->initTetanggaKabkotasRelatedByMstKodeWilayah(false);

                      foreach($collTetanggaKabkotasRelatedByMstKodeWilayah as $obj) {
                        if (false == $this->collTetanggaKabkotasRelatedByMstKodeWilayah->contains($obj)) {
                          $this->collTetanggaKabkotasRelatedByMstKodeWilayah->append($obj);
                        }
                      }

                      $this->collTetanggaKabkotasRelatedByMstKodeWilayahPartial = true;
                    }

                    $collTetanggaKabkotasRelatedByMstKodeWilayah->getInternalIterator()->rewind();
                    return $collTetanggaKabkotasRelatedByMstKodeWilayah;
                }

                if($partial && $this->collTetanggaKabkotasRelatedByMstKodeWilayah) {
                    foreach($this->collTetanggaKabkotasRelatedByMstKodeWilayah as $obj) {
                        if($obj->isNew()) {
                            $collTetanggaKabkotasRelatedByMstKodeWilayah[] = $obj;
                        }
                    }
                }

                $this->collTetanggaKabkotasRelatedByMstKodeWilayah = $collTetanggaKabkotasRelatedByMstKodeWilayah;
                $this->collTetanggaKabkotasRelatedByMstKodeWilayahPartial = false;
            }
        }

        return $this->collTetanggaKabkotasRelatedByMstKodeWilayah;
    }

    /**
     * Sets a collection of TetanggaKabkotaRelatedByMstKodeWilayah objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $tetanggaKabkotasRelatedByMstKodeWilayah A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setTetanggaKabkotasRelatedByMstKodeWilayah(PropelCollection $tetanggaKabkotasRelatedByMstKodeWilayah, PropelPDO $con = null)
    {
        $tetanggaKabkotasRelatedByMstKodeWilayahToDelete = $this->getTetanggaKabkotasRelatedByMstKodeWilayah(new Criteria(), $con)->diff($tetanggaKabkotasRelatedByMstKodeWilayah);

        $this->tetanggaKabkotasRelatedByMstKodeWilayahScheduledForDeletion = unserialize(serialize($tetanggaKabkotasRelatedByMstKodeWilayahToDelete));

        foreach ($tetanggaKabkotasRelatedByMstKodeWilayahToDelete as $tetanggaKabkotaRelatedByMstKodeWilayahRemoved) {
            $tetanggaKabkotaRelatedByMstKodeWilayahRemoved->setMstWilayahRelatedByMstKodeWilayah(null);
        }

        $this->collTetanggaKabkotasRelatedByMstKodeWilayah = null;
        foreach ($tetanggaKabkotasRelatedByMstKodeWilayah as $tetanggaKabkotaRelatedByMstKodeWilayah) {
            $this->addTetanggaKabkotaRelatedByMstKodeWilayah($tetanggaKabkotaRelatedByMstKodeWilayah);
        }

        $this->collTetanggaKabkotasRelatedByMstKodeWilayah = $tetanggaKabkotasRelatedByMstKodeWilayah;
        $this->collTetanggaKabkotasRelatedByMstKodeWilayahPartial = false;

        return $this;
    }

    /**
     * Returns the number of related TetanggaKabkota objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related TetanggaKabkota objects.
     * @throws PropelException
     */
    public function countTetanggaKabkotasRelatedByMstKodeWilayah(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collTetanggaKabkotasRelatedByMstKodeWilayahPartial && !$this->isNew();
        if (null === $this->collTetanggaKabkotasRelatedByMstKodeWilayah || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collTetanggaKabkotasRelatedByMstKodeWilayah) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getTetanggaKabkotasRelatedByMstKodeWilayah());
            }
            $query = TetanggaKabkotaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMstWilayahRelatedByMstKodeWilayah($this)
                ->count($con);
        }

        return count($this->collTetanggaKabkotasRelatedByMstKodeWilayah);
    }

    /**
     * Method called to associate a TetanggaKabkota object to this object
     * through the TetanggaKabkota foreign key attribute.
     *
     * @param    TetanggaKabkota $l TetanggaKabkota
     * @return MstWilayah The current object (for fluent API support)
     */
    public function addTetanggaKabkotaRelatedByMstKodeWilayah(TetanggaKabkota $l)
    {
        if ($this->collTetanggaKabkotasRelatedByMstKodeWilayah === null) {
            $this->initTetanggaKabkotasRelatedByMstKodeWilayah();
            $this->collTetanggaKabkotasRelatedByMstKodeWilayahPartial = true;
        }
        if (!in_array($l, $this->collTetanggaKabkotasRelatedByMstKodeWilayah->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddTetanggaKabkotaRelatedByMstKodeWilayah($l);
        }

        return $this;
    }

    /**
     * @param	TetanggaKabkotaRelatedByMstKodeWilayah $tetanggaKabkotaRelatedByMstKodeWilayah The tetanggaKabkotaRelatedByMstKodeWilayah object to add.
     */
    protected function doAddTetanggaKabkotaRelatedByMstKodeWilayah($tetanggaKabkotaRelatedByMstKodeWilayah)
    {
        $this->collTetanggaKabkotasRelatedByMstKodeWilayah[]= $tetanggaKabkotaRelatedByMstKodeWilayah;
        $tetanggaKabkotaRelatedByMstKodeWilayah->setMstWilayahRelatedByMstKodeWilayah($this);
    }

    /**
     * @param	TetanggaKabkotaRelatedByMstKodeWilayah $tetanggaKabkotaRelatedByMstKodeWilayah The tetanggaKabkotaRelatedByMstKodeWilayah object to remove.
     * @return MstWilayah The current object (for fluent API support)
     */
    public function removeTetanggaKabkotaRelatedByMstKodeWilayah($tetanggaKabkotaRelatedByMstKodeWilayah)
    {
        if ($this->getTetanggaKabkotasRelatedByMstKodeWilayah()->contains($tetanggaKabkotaRelatedByMstKodeWilayah)) {
            $this->collTetanggaKabkotasRelatedByMstKodeWilayah->remove($this->collTetanggaKabkotasRelatedByMstKodeWilayah->search($tetanggaKabkotaRelatedByMstKodeWilayah));
            if (null === $this->tetanggaKabkotasRelatedByMstKodeWilayahScheduledForDeletion) {
                $this->tetanggaKabkotasRelatedByMstKodeWilayahScheduledForDeletion = clone $this->collTetanggaKabkotasRelatedByMstKodeWilayah;
                $this->tetanggaKabkotasRelatedByMstKodeWilayahScheduledForDeletion->clear();
            }
            $this->tetanggaKabkotasRelatedByMstKodeWilayahScheduledForDeletion[]= clone $tetanggaKabkotaRelatedByMstKodeWilayah;
            $tetanggaKabkotaRelatedByMstKodeWilayah->setMstWilayahRelatedByMstKodeWilayah(null);
        }

        return $this;
    }

    /**
     * Clears out the collPesertaDidiksRelatedByKodeWilayah collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return MstWilayah The current object (for fluent API support)
     * @see        addPesertaDidiksRelatedByKodeWilayah()
     */
    public function clearPesertaDidiksRelatedByKodeWilayah()
    {
        $this->collPesertaDidiksRelatedByKodeWilayah = null; // important to set this to null since that means it is uninitialized
        $this->collPesertaDidiksRelatedByKodeWilayahPartial = null;

        return $this;
    }

    /**
     * reset is the collPesertaDidiksRelatedByKodeWilayah collection loaded partially
     *
     * @return void
     */
    public function resetPartialPesertaDidiksRelatedByKodeWilayah($v = true)
    {
        $this->collPesertaDidiksRelatedByKodeWilayahPartial = $v;
    }

    /**
     * Initializes the collPesertaDidiksRelatedByKodeWilayah collection.
     *
     * By default this just sets the collPesertaDidiksRelatedByKodeWilayah collection to an empty array (like clearcollPesertaDidiksRelatedByKodeWilayah());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPesertaDidiksRelatedByKodeWilayah($overrideExisting = true)
    {
        if (null !== $this->collPesertaDidiksRelatedByKodeWilayah && !$overrideExisting) {
            return;
        }
        $this->collPesertaDidiksRelatedByKodeWilayah = new PropelObjectCollection();
        $this->collPesertaDidiksRelatedByKodeWilayah->setModel('PesertaDidik');
    }

    /**
     * Gets an array of PesertaDidik objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this MstWilayah is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     * @throws PropelException
     */
    public function getPesertaDidiksRelatedByKodeWilayah($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPesertaDidiksRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collPesertaDidiksRelatedByKodeWilayah || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPesertaDidiksRelatedByKodeWilayah) {
                // return empty collection
                $this->initPesertaDidiksRelatedByKodeWilayah();
            } else {
                $collPesertaDidiksRelatedByKodeWilayah = PesertaDidikQuery::create(null, $criteria)
                    ->filterByMstWilayahRelatedByKodeWilayah($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPesertaDidiksRelatedByKodeWilayahPartial && count($collPesertaDidiksRelatedByKodeWilayah)) {
                      $this->initPesertaDidiksRelatedByKodeWilayah(false);

                      foreach($collPesertaDidiksRelatedByKodeWilayah as $obj) {
                        if (false == $this->collPesertaDidiksRelatedByKodeWilayah->contains($obj)) {
                          $this->collPesertaDidiksRelatedByKodeWilayah->append($obj);
                        }
                      }

                      $this->collPesertaDidiksRelatedByKodeWilayahPartial = true;
                    }

                    $collPesertaDidiksRelatedByKodeWilayah->getInternalIterator()->rewind();
                    return $collPesertaDidiksRelatedByKodeWilayah;
                }

                if($partial && $this->collPesertaDidiksRelatedByKodeWilayah) {
                    foreach($this->collPesertaDidiksRelatedByKodeWilayah as $obj) {
                        if($obj->isNew()) {
                            $collPesertaDidiksRelatedByKodeWilayah[] = $obj;
                        }
                    }
                }

                $this->collPesertaDidiksRelatedByKodeWilayah = $collPesertaDidiksRelatedByKodeWilayah;
                $this->collPesertaDidiksRelatedByKodeWilayahPartial = false;
            }
        }

        return $this->collPesertaDidiksRelatedByKodeWilayah;
    }

    /**
     * Sets a collection of PesertaDidikRelatedByKodeWilayah objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $pesertaDidiksRelatedByKodeWilayah A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setPesertaDidiksRelatedByKodeWilayah(PropelCollection $pesertaDidiksRelatedByKodeWilayah, PropelPDO $con = null)
    {
        $pesertaDidiksRelatedByKodeWilayahToDelete = $this->getPesertaDidiksRelatedByKodeWilayah(new Criteria(), $con)->diff($pesertaDidiksRelatedByKodeWilayah);

        $this->pesertaDidiksRelatedByKodeWilayahScheduledForDeletion = unserialize(serialize($pesertaDidiksRelatedByKodeWilayahToDelete));

        foreach ($pesertaDidiksRelatedByKodeWilayahToDelete as $pesertaDidikRelatedByKodeWilayahRemoved) {
            $pesertaDidikRelatedByKodeWilayahRemoved->setMstWilayahRelatedByKodeWilayah(null);
        }

        $this->collPesertaDidiksRelatedByKodeWilayah = null;
        foreach ($pesertaDidiksRelatedByKodeWilayah as $pesertaDidikRelatedByKodeWilayah) {
            $this->addPesertaDidikRelatedByKodeWilayah($pesertaDidikRelatedByKodeWilayah);
        }

        $this->collPesertaDidiksRelatedByKodeWilayah = $pesertaDidiksRelatedByKodeWilayah;
        $this->collPesertaDidiksRelatedByKodeWilayahPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PesertaDidik objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related PesertaDidik objects.
     * @throws PropelException
     */
    public function countPesertaDidiksRelatedByKodeWilayah(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPesertaDidiksRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collPesertaDidiksRelatedByKodeWilayah || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPesertaDidiksRelatedByKodeWilayah) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPesertaDidiksRelatedByKodeWilayah());
            }
            $query = PesertaDidikQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMstWilayahRelatedByKodeWilayah($this)
                ->count($con);
        }

        return count($this->collPesertaDidiksRelatedByKodeWilayah);
    }

    /**
     * Method called to associate a PesertaDidik object to this object
     * through the PesertaDidik foreign key attribute.
     *
     * @param    PesertaDidik $l PesertaDidik
     * @return MstWilayah The current object (for fluent API support)
     */
    public function addPesertaDidikRelatedByKodeWilayah(PesertaDidik $l)
    {
        if ($this->collPesertaDidiksRelatedByKodeWilayah === null) {
            $this->initPesertaDidiksRelatedByKodeWilayah();
            $this->collPesertaDidiksRelatedByKodeWilayahPartial = true;
        }
        if (!in_array($l, $this->collPesertaDidiksRelatedByKodeWilayah->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPesertaDidikRelatedByKodeWilayah($l);
        }

        return $this;
    }

    /**
     * @param	PesertaDidikRelatedByKodeWilayah $pesertaDidikRelatedByKodeWilayah The pesertaDidikRelatedByKodeWilayah object to add.
     */
    protected function doAddPesertaDidikRelatedByKodeWilayah($pesertaDidikRelatedByKodeWilayah)
    {
        $this->collPesertaDidiksRelatedByKodeWilayah[]= $pesertaDidikRelatedByKodeWilayah;
        $pesertaDidikRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah($this);
    }

    /**
     * @param	PesertaDidikRelatedByKodeWilayah $pesertaDidikRelatedByKodeWilayah The pesertaDidikRelatedByKodeWilayah object to remove.
     * @return MstWilayah The current object (for fluent API support)
     */
    public function removePesertaDidikRelatedByKodeWilayah($pesertaDidikRelatedByKodeWilayah)
    {
        if ($this->getPesertaDidiksRelatedByKodeWilayah()->contains($pesertaDidikRelatedByKodeWilayah)) {
            $this->collPesertaDidiksRelatedByKodeWilayah->remove($this->collPesertaDidiksRelatedByKodeWilayah->search($pesertaDidikRelatedByKodeWilayah));
            if (null === $this->pesertaDidiksRelatedByKodeWilayahScheduledForDeletion) {
                $this->pesertaDidiksRelatedByKodeWilayahScheduledForDeletion = clone $this->collPesertaDidiksRelatedByKodeWilayah;
                $this->pesertaDidiksRelatedByKodeWilayahScheduledForDeletion->clear();
            }
            $this->pesertaDidiksRelatedByKodeWilayahScheduledForDeletion[]= clone $pesertaDidikRelatedByKodeWilayah;
            $pesertaDidikRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinAgamaRelatedByAgamaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('AgamaRelatedByAgamaId', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinAgamaRelatedByAgamaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('AgamaRelatedByAgamaId', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinAlatTransportasiRelatedByAlatTransportasiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('AlatTransportasiRelatedByAlatTransportasiId', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinAlatTransportasiRelatedByAlatTransportasiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('AlatTransportasiRelatedByAlatTransportasiId', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinJenisTinggalRelatedByJenisTinggalId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisTinggalRelatedByJenisTinggalId', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinJenisTinggalRelatedByJenisTinggalId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisTinggalRelatedByJenisTinggalId', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinJenjangPendidikanRelatedByJenjangPendidikanIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinJenjangPendidikanRelatedByJenjangPendidikanAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinJenjangPendidikanRelatedByJenjangPendidikanWali($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanWali', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinJenjangPendidikanRelatedByJenjangPendidikanIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinJenjangPendidikanRelatedByJenjangPendidikanAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinJenjangPendidikanRelatedByJenjangPendidikanWali($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanWali', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinKebutuhanKhususRelatedByKebutuhanKhususIdAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususIdAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinKebutuhanKhususRelatedByKebutuhanKhususIdIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususIdIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinKebutuhanKhususRelatedByKebutuhanKhususId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususId', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinKebutuhanKhususRelatedByKebutuhanKhususIdAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususIdAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinKebutuhanKhususRelatedByKebutuhanKhususIdIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususIdIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinKebutuhanKhususRelatedByKebutuhanKhususId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususId', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinNegaraRelatedByKewarganegaraan($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('NegaraRelatedByKewarganegaraan', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinNegaraRelatedByKewarganegaraan($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('NegaraRelatedByKewarganegaraan', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinPekerjaanRelatedByPekerjaanIdAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PekerjaanRelatedByPekerjaanIdAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinPekerjaanRelatedByPekerjaanIdIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PekerjaanRelatedByPekerjaanIdIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinPekerjaanRelatedByPekerjaanIdWali($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PekerjaanRelatedByPekerjaanIdWali', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinPekerjaanRelatedByPekerjaanIdAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PekerjaanRelatedByPekerjaanIdAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinPekerjaanRelatedByPekerjaanIdIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PekerjaanRelatedByPekerjaanIdIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinPekerjaanRelatedByPekerjaanIdWali($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PekerjaanRelatedByPekerjaanIdWali', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdWali', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdWali', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }

    /**
     * Clears out the collPesertaDidiksRelatedByKodeWilayah collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return MstWilayah The current object (for fluent API support)
     * @see        addPesertaDidiksRelatedByKodeWilayah()
     */
    public function clearPesertaDidiksRelatedByKodeWilayah()
    {
        $this->collPesertaDidiksRelatedByKodeWilayah = null; // important to set this to null since that means it is uninitialized
        $this->collPesertaDidiksRelatedByKodeWilayahPartial = null;

        return $this;
    }

    /**
     * reset is the collPesertaDidiksRelatedByKodeWilayah collection loaded partially
     *
     * @return void
     */
    public function resetPartialPesertaDidiksRelatedByKodeWilayah($v = true)
    {
        $this->collPesertaDidiksRelatedByKodeWilayahPartial = $v;
    }

    /**
     * Initializes the collPesertaDidiksRelatedByKodeWilayah collection.
     *
     * By default this just sets the collPesertaDidiksRelatedByKodeWilayah collection to an empty array (like clearcollPesertaDidiksRelatedByKodeWilayah());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPesertaDidiksRelatedByKodeWilayah($overrideExisting = true)
    {
        if (null !== $this->collPesertaDidiksRelatedByKodeWilayah && !$overrideExisting) {
            return;
        }
        $this->collPesertaDidiksRelatedByKodeWilayah = new PropelObjectCollection();
        $this->collPesertaDidiksRelatedByKodeWilayah->setModel('PesertaDidik');
    }

    /**
     * Gets an array of PesertaDidik objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this MstWilayah is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     * @throws PropelException
     */
    public function getPesertaDidiksRelatedByKodeWilayah($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPesertaDidiksRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collPesertaDidiksRelatedByKodeWilayah || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPesertaDidiksRelatedByKodeWilayah) {
                // return empty collection
                $this->initPesertaDidiksRelatedByKodeWilayah();
            } else {
                $collPesertaDidiksRelatedByKodeWilayah = PesertaDidikQuery::create(null, $criteria)
                    ->filterByMstWilayahRelatedByKodeWilayah($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPesertaDidiksRelatedByKodeWilayahPartial && count($collPesertaDidiksRelatedByKodeWilayah)) {
                      $this->initPesertaDidiksRelatedByKodeWilayah(false);

                      foreach($collPesertaDidiksRelatedByKodeWilayah as $obj) {
                        if (false == $this->collPesertaDidiksRelatedByKodeWilayah->contains($obj)) {
                          $this->collPesertaDidiksRelatedByKodeWilayah->append($obj);
                        }
                      }

                      $this->collPesertaDidiksRelatedByKodeWilayahPartial = true;
                    }

                    $collPesertaDidiksRelatedByKodeWilayah->getInternalIterator()->rewind();
                    return $collPesertaDidiksRelatedByKodeWilayah;
                }

                if($partial && $this->collPesertaDidiksRelatedByKodeWilayah) {
                    foreach($this->collPesertaDidiksRelatedByKodeWilayah as $obj) {
                        if($obj->isNew()) {
                            $collPesertaDidiksRelatedByKodeWilayah[] = $obj;
                        }
                    }
                }

                $this->collPesertaDidiksRelatedByKodeWilayah = $collPesertaDidiksRelatedByKodeWilayah;
                $this->collPesertaDidiksRelatedByKodeWilayahPartial = false;
            }
        }

        return $this->collPesertaDidiksRelatedByKodeWilayah;
    }

    /**
     * Sets a collection of PesertaDidikRelatedByKodeWilayah objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $pesertaDidiksRelatedByKodeWilayah A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setPesertaDidiksRelatedByKodeWilayah(PropelCollection $pesertaDidiksRelatedByKodeWilayah, PropelPDO $con = null)
    {
        $pesertaDidiksRelatedByKodeWilayahToDelete = $this->getPesertaDidiksRelatedByKodeWilayah(new Criteria(), $con)->diff($pesertaDidiksRelatedByKodeWilayah);

        $this->pesertaDidiksRelatedByKodeWilayahScheduledForDeletion = unserialize(serialize($pesertaDidiksRelatedByKodeWilayahToDelete));

        foreach ($pesertaDidiksRelatedByKodeWilayahToDelete as $pesertaDidikRelatedByKodeWilayahRemoved) {
            $pesertaDidikRelatedByKodeWilayahRemoved->setMstWilayahRelatedByKodeWilayah(null);
        }

        $this->collPesertaDidiksRelatedByKodeWilayah = null;
        foreach ($pesertaDidiksRelatedByKodeWilayah as $pesertaDidikRelatedByKodeWilayah) {
            $this->addPesertaDidikRelatedByKodeWilayah($pesertaDidikRelatedByKodeWilayah);
        }

        $this->collPesertaDidiksRelatedByKodeWilayah = $pesertaDidiksRelatedByKodeWilayah;
        $this->collPesertaDidiksRelatedByKodeWilayahPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PesertaDidik objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related PesertaDidik objects.
     * @throws PropelException
     */
    public function countPesertaDidiksRelatedByKodeWilayah(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPesertaDidiksRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collPesertaDidiksRelatedByKodeWilayah || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPesertaDidiksRelatedByKodeWilayah) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPesertaDidiksRelatedByKodeWilayah());
            }
            $query = PesertaDidikQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMstWilayahRelatedByKodeWilayah($this)
                ->count($con);
        }

        return count($this->collPesertaDidiksRelatedByKodeWilayah);
    }

    /**
     * Method called to associate a PesertaDidik object to this object
     * through the PesertaDidik foreign key attribute.
     *
     * @param    PesertaDidik $l PesertaDidik
     * @return MstWilayah The current object (for fluent API support)
     */
    public function addPesertaDidikRelatedByKodeWilayah(PesertaDidik $l)
    {
        if ($this->collPesertaDidiksRelatedByKodeWilayah === null) {
            $this->initPesertaDidiksRelatedByKodeWilayah();
            $this->collPesertaDidiksRelatedByKodeWilayahPartial = true;
        }
        if (!in_array($l, $this->collPesertaDidiksRelatedByKodeWilayah->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPesertaDidikRelatedByKodeWilayah($l);
        }

        return $this;
    }

    /**
     * @param	PesertaDidikRelatedByKodeWilayah $pesertaDidikRelatedByKodeWilayah The pesertaDidikRelatedByKodeWilayah object to add.
     */
    protected function doAddPesertaDidikRelatedByKodeWilayah($pesertaDidikRelatedByKodeWilayah)
    {
        $this->collPesertaDidiksRelatedByKodeWilayah[]= $pesertaDidikRelatedByKodeWilayah;
        $pesertaDidikRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah($this);
    }

    /**
     * @param	PesertaDidikRelatedByKodeWilayah $pesertaDidikRelatedByKodeWilayah The pesertaDidikRelatedByKodeWilayah object to remove.
     * @return MstWilayah The current object (for fluent API support)
     */
    public function removePesertaDidikRelatedByKodeWilayah($pesertaDidikRelatedByKodeWilayah)
    {
        if ($this->getPesertaDidiksRelatedByKodeWilayah()->contains($pesertaDidikRelatedByKodeWilayah)) {
            $this->collPesertaDidiksRelatedByKodeWilayah->remove($this->collPesertaDidiksRelatedByKodeWilayah->search($pesertaDidikRelatedByKodeWilayah));
            if (null === $this->pesertaDidiksRelatedByKodeWilayahScheduledForDeletion) {
                $this->pesertaDidiksRelatedByKodeWilayahScheduledForDeletion = clone $this->collPesertaDidiksRelatedByKodeWilayah;
                $this->pesertaDidiksRelatedByKodeWilayahScheduledForDeletion->clear();
            }
            $this->pesertaDidiksRelatedByKodeWilayahScheduledForDeletion[]= clone $pesertaDidikRelatedByKodeWilayah;
            $pesertaDidikRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinAgamaRelatedByAgamaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('AgamaRelatedByAgamaId', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinAgamaRelatedByAgamaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('AgamaRelatedByAgamaId', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinAlatTransportasiRelatedByAlatTransportasiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('AlatTransportasiRelatedByAlatTransportasiId', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinAlatTransportasiRelatedByAlatTransportasiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('AlatTransportasiRelatedByAlatTransportasiId', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinJenisTinggalRelatedByJenisTinggalId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisTinggalRelatedByJenisTinggalId', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinJenisTinggalRelatedByJenisTinggalId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisTinggalRelatedByJenisTinggalId', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinJenjangPendidikanRelatedByJenjangPendidikanIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinJenjangPendidikanRelatedByJenjangPendidikanAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinJenjangPendidikanRelatedByJenjangPendidikanWali($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanWali', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinJenjangPendidikanRelatedByJenjangPendidikanIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinJenjangPendidikanRelatedByJenjangPendidikanAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinJenjangPendidikanRelatedByJenjangPendidikanWali($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanWali', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinKebutuhanKhususRelatedByKebutuhanKhususIdAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususIdAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinKebutuhanKhususRelatedByKebutuhanKhususIdIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususIdIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinKebutuhanKhususRelatedByKebutuhanKhususId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususId', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinKebutuhanKhususRelatedByKebutuhanKhususIdAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususIdAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinKebutuhanKhususRelatedByKebutuhanKhususIdIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususIdIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinKebutuhanKhususRelatedByKebutuhanKhususId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususId', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinNegaraRelatedByKewarganegaraan($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('NegaraRelatedByKewarganegaraan', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinNegaraRelatedByKewarganegaraan($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('NegaraRelatedByKewarganegaraan', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinPekerjaanRelatedByPekerjaanIdAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PekerjaanRelatedByPekerjaanIdAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinPekerjaanRelatedByPekerjaanIdIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PekerjaanRelatedByPekerjaanIdIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinPekerjaanRelatedByPekerjaanIdWali($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PekerjaanRelatedByPekerjaanIdWali', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinPekerjaanRelatedByPekerjaanIdAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PekerjaanRelatedByPekerjaanIdAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinPekerjaanRelatedByPekerjaanIdIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PekerjaanRelatedByPekerjaanIdIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinPekerjaanRelatedByPekerjaanIdWali($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PekerjaanRelatedByPekerjaanIdWali', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdWali', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdWali', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByKodeWilayahJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByKodeWilayah($query, $con);
    }

    /**
     * Clears out the collMstWilayahsRelatedByKodeWilayah collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return MstWilayah The current object (for fluent API support)
     * @see        addMstWilayahsRelatedByKodeWilayah()
     */
    public function clearMstWilayahsRelatedByKodeWilayah()
    {
        $this->collMstWilayahsRelatedByKodeWilayah = null; // important to set this to null since that means it is uninitialized
        $this->collMstWilayahsRelatedByKodeWilayahPartial = null;

        return $this;
    }

    /**
     * reset is the collMstWilayahsRelatedByKodeWilayah collection loaded partially
     *
     * @return void
     */
    public function resetPartialMstWilayahsRelatedByKodeWilayah($v = true)
    {
        $this->collMstWilayahsRelatedByKodeWilayahPartial = $v;
    }

    /**
     * Initializes the collMstWilayahsRelatedByKodeWilayah collection.
     *
     * By default this just sets the collMstWilayahsRelatedByKodeWilayah collection to an empty array (like clearcollMstWilayahsRelatedByKodeWilayah());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initMstWilayahsRelatedByKodeWilayah($overrideExisting = true)
    {
        if (null !== $this->collMstWilayahsRelatedByKodeWilayah && !$overrideExisting) {
            return;
        }
        $this->collMstWilayahsRelatedByKodeWilayah = new PropelObjectCollection();
        $this->collMstWilayahsRelatedByKodeWilayah->setModel('MstWilayah');
    }

    /**
     * Gets an array of MstWilayah objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this MstWilayah is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|MstWilayah[] List of MstWilayah objects
     * @throws PropelException
     */
    public function getMstWilayahsRelatedByKodeWilayah($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collMstWilayahsRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collMstWilayahsRelatedByKodeWilayah || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collMstWilayahsRelatedByKodeWilayah) {
                // return empty collection
                $this->initMstWilayahsRelatedByKodeWilayah();
            } else {
                $collMstWilayahsRelatedByKodeWilayah = MstWilayahQuery::create(null, $criteria)
                    ->filterByMstWilayahRelatedByMstKodeWilayah($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collMstWilayahsRelatedByKodeWilayahPartial && count($collMstWilayahsRelatedByKodeWilayah)) {
                      $this->initMstWilayahsRelatedByKodeWilayah(false);

                      foreach($collMstWilayahsRelatedByKodeWilayah as $obj) {
                        if (false == $this->collMstWilayahsRelatedByKodeWilayah->contains($obj)) {
                          $this->collMstWilayahsRelatedByKodeWilayah->append($obj);
                        }
                      }

                      $this->collMstWilayahsRelatedByKodeWilayahPartial = true;
                    }

                    $collMstWilayahsRelatedByKodeWilayah->getInternalIterator()->rewind();
                    return $collMstWilayahsRelatedByKodeWilayah;
                }

                if($partial && $this->collMstWilayahsRelatedByKodeWilayah) {
                    foreach($this->collMstWilayahsRelatedByKodeWilayah as $obj) {
                        if($obj->isNew()) {
                            $collMstWilayahsRelatedByKodeWilayah[] = $obj;
                        }
                    }
                }

                $this->collMstWilayahsRelatedByKodeWilayah = $collMstWilayahsRelatedByKodeWilayah;
                $this->collMstWilayahsRelatedByKodeWilayahPartial = false;
            }
        }

        return $this->collMstWilayahsRelatedByKodeWilayah;
    }

    /**
     * Sets a collection of MstWilayahRelatedByKodeWilayah objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $mstWilayahsRelatedByKodeWilayah A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setMstWilayahsRelatedByKodeWilayah(PropelCollection $mstWilayahsRelatedByKodeWilayah, PropelPDO $con = null)
    {
        $mstWilayahsRelatedByKodeWilayahToDelete = $this->getMstWilayahsRelatedByKodeWilayah(new Criteria(), $con)->diff($mstWilayahsRelatedByKodeWilayah);

        $this->mstWilayahsRelatedByKodeWilayahScheduledForDeletion = unserialize(serialize($mstWilayahsRelatedByKodeWilayahToDelete));

        foreach ($mstWilayahsRelatedByKodeWilayahToDelete as $mstWilayahRelatedByKodeWilayahRemoved) {
            $mstWilayahRelatedByKodeWilayahRemoved->setMstWilayahRelatedByMstKodeWilayah(null);
        }

        $this->collMstWilayahsRelatedByKodeWilayah = null;
        foreach ($mstWilayahsRelatedByKodeWilayah as $mstWilayahRelatedByKodeWilayah) {
            $this->addMstWilayahRelatedByKodeWilayah($mstWilayahRelatedByKodeWilayah);
        }

        $this->collMstWilayahsRelatedByKodeWilayah = $mstWilayahsRelatedByKodeWilayah;
        $this->collMstWilayahsRelatedByKodeWilayahPartial = false;

        return $this;
    }

    /**
     * Returns the number of related MstWilayah objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related MstWilayah objects.
     * @throws PropelException
     */
    public function countMstWilayahsRelatedByKodeWilayah(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collMstWilayahsRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collMstWilayahsRelatedByKodeWilayah || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collMstWilayahsRelatedByKodeWilayah) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getMstWilayahsRelatedByKodeWilayah());
            }
            $query = MstWilayahQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMstWilayahRelatedByMstKodeWilayah($this)
                ->count($con);
        }

        return count($this->collMstWilayahsRelatedByKodeWilayah);
    }

    /**
     * Method called to associate a MstWilayah object to this object
     * through the MstWilayah foreign key attribute.
     *
     * @param    MstWilayah $l MstWilayah
     * @return MstWilayah The current object (for fluent API support)
     */
    public function addMstWilayahRelatedByKodeWilayah(MstWilayah $l)
    {
        if ($this->collMstWilayahsRelatedByKodeWilayah === null) {
            $this->initMstWilayahsRelatedByKodeWilayah();
            $this->collMstWilayahsRelatedByKodeWilayahPartial = true;
        }
        if (!in_array($l, $this->collMstWilayahsRelatedByKodeWilayah->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddMstWilayahRelatedByKodeWilayah($l);
        }

        return $this;
    }

    /**
     * @param	MstWilayahRelatedByKodeWilayah $mstWilayahRelatedByKodeWilayah The mstWilayahRelatedByKodeWilayah object to add.
     */
    protected function doAddMstWilayahRelatedByKodeWilayah($mstWilayahRelatedByKodeWilayah)
    {
        $this->collMstWilayahsRelatedByKodeWilayah[]= $mstWilayahRelatedByKodeWilayah;
        $mstWilayahRelatedByKodeWilayah->setMstWilayahRelatedByMstKodeWilayah($this);
    }

    /**
     * @param	MstWilayahRelatedByKodeWilayah $mstWilayahRelatedByKodeWilayah The mstWilayahRelatedByKodeWilayah object to remove.
     * @return MstWilayah The current object (for fluent API support)
     */
    public function removeMstWilayahRelatedByKodeWilayah($mstWilayahRelatedByKodeWilayah)
    {
        if ($this->getMstWilayahsRelatedByKodeWilayah()->contains($mstWilayahRelatedByKodeWilayah)) {
            $this->collMstWilayahsRelatedByKodeWilayah->remove($this->collMstWilayahsRelatedByKodeWilayah->search($mstWilayahRelatedByKodeWilayah));
            if (null === $this->mstWilayahsRelatedByKodeWilayahScheduledForDeletion) {
                $this->mstWilayahsRelatedByKodeWilayahScheduledForDeletion = clone $this->collMstWilayahsRelatedByKodeWilayah;
                $this->mstWilayahsRelatedByKodeWilayahScheduledForDeletion->clear();
            }
            $this->mstWilayahsRelatedByKodeWilayahScheduledForDeletion[]= $mstWilayahRelatedByKodeWilayah;
            $mstWilayahRelatedByKodeWilayah->setMstWilayahRelatedByMstKodeWilayah(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related MstWilayahsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|MstWilayah[] List of MstWilayah objects
     */
    public function getMstWilayahsRelatedByKodeWilayahJoinLevelWilayah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = MstWilayahQuery::create(null, $criteria);
        $query->joinWith('LevelWilayah', $join_behavior);

        return $this->getMstWilayahsRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related MstWilayahsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|MstWilayah[] List of MstWilayah objects
     */
    public function getMstWilayahsRelatedByKodeWilayahJoinNegara($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = MstWilayahQuery::create(null, $criteria);
        $query->joinWith('Negara', $join_behavior);

        return $this->getMstWilayahsRelatedByKodeWilayah($query, $con);
    }

    /**
     * Clears out the collPenggunasRelatedByKodeWilayah collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return MstWilayah The current object (for fluent API support)
     * @see        addPenggunasRelatedByKodeWilayah()
     */
    public function clearPenggunasRelatedByKodeWilayah()
    {
        $this->collPenggunasRelatedByKodeWilayah = null; // important to set this to null since that means it is uninitialized
        $this->collPenggunasRelatedByKodeWilayahPartial = null;

        return $this;
    }

    /**
     * reset is the collPenggunasRelatedByKodeWilayah collection loaded partially
     *
     * @return void
     */
    public function resetPartialPenggunasRelatedByKodeWilayah($v = true)
    {
        $this->collPenggunasRelatedByKodeWilayahPartial = $v;
    }

    /**
     * Initializes the collPenggunasRelatedByKodeWilayah collection.
     *
     * By default this just sets the collPenggunasRelatedByKodeWilayah collection to an empty array (like clearcollPenggunasRelatedByKodeWilayah());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPenggunasRelatedByKodeWilayah($overrideExisting = true)
    {
        if (null !== $this->collPenggunasRelatedByKodeWilayah && !$overrideExisting) {
            return;
        }
        $this->collPenggunasRelatedByKodeWilayah = new PropelObjectCollection();
        $this->collPenggunasRelatedByKodeWilayah->setModel('Pengguna');
    }

    /**
     * Gets an array of Pengguna objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this MstWilayah is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Pengguna[] List of Pengguna objects
     * @throws PropelException
     */
    public function getPenggunasRelatedByKodeWilayah($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPenggunasRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collPenggunasRelatedByKodeWilayah || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPenggunasRelatedByKodeWilayah) {
                // return empty collection
                $this->initPenggunasRelatedByKodeWilayah();
            } else {
                $collPenggunasRelatedByKodeWilayah = PenggunaQuery::create(null, $criteria)
                    ->filterByMstWilayahRelatedByKodeWilayah($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPenggunasRelatedByKodeWilayahPartial && count($collPenggunasRelatedByKodeWilayah)) {
                      $this->initPenggunasRelatedByKodeWilayah(false);

                      foreach($collPenggunasRelatedByKodeWilayah as $obj) {
                        if (false == $this->collPenggunasRelatedByKodeWilayah->contains($obj)) {
                          $this->collPenggunasRelatedByKodeWilayah->append($obj);
                        }
                      }

                      $this->collPenggunasRelatedByKodeWilayahPartial = true;
                    }

                    $collPenggunasRelatedByKodeWilayah->getInternalIterator()->rewind();
                    return $collPenggunasRelatedByKodeWilayah;
                }

                if($partial && $this->collPenggunasRelatedByKodeWilayah) {
                    foreach($this->collPenggunasRelatedByKodeWilayah as $obj) {
                        if($obj->isNew()) {
                            $collPenggunasRelatedByKodeWilayah[] = $obj;
                        }
                    }
                }

                $this->collPenggunasRelatedByKodeWilayah = $collPenggunasRelatedByKodeWilayah;
                $this->collPenggunasRelatedByKodeWilayahPartial = false;
            }
        }

        return $this->collPenggunasRelatedByKodeWilayah;
    }

    /**
     * Sets a collection of PenggunaRelatedByKodeWilayah objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $penggunasRelatedByKodeWilayah A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setPenggunasRelatedByKodeWilayah(PropelCollection $penggunasRelatedByKodeWilayah, PropelPDO $con = null)
    {
        $penggunasRelatedByKodeWilayahToDelete = $this->getPenggunasRelatedByKodeWilayah(new Criteria(), $con)->diff($penggunasRelatedByKodeWilayah);

        $this->penggunasRelatedByKodeWilayahScheduledForDeletion = unserialize(serialize($penggunasRelatedByKodeWilayahToDelete));

        foreach ($penggunasRelatedByKodeWilayahToDelete as $penggunaRelatedByKodeWilayahRemoved) {
            $penggunaRelatedByKodeWilayahRemoved->setMstWilayahRelatedByKodeWilayah(null);
        }

        $this->collPenggunasRelatedByKodeWilayah = null;
        foreach ($penggunasRelatedByKodeWilayah as $penggunaRelatedByKodeWilayah) {
            $this->addPenggunaRelatedByKodeWilayah($penggunaRelatedByKodeWilayah);
        }

        $this->collPenggunasRelatedByKodeWilayah = $penggunasRelatedByKodeWilayah;
        $this->collPenggunasRelatedByKodeWilayahPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Pengguna objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Pengguna objects.
     * @throws PropelException
     */
    public function countPenggunasRelatedByKodeWilayah(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPenggunasRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collPenggunasRelatedByKodeWilayah || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPenggunasRelatedByKodeWilayah) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPenggunasRelatedByKodeWilayah());
            }
            $query = PenggunaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMstWilayahRelatedByKodeWilayah($this)
                ->count($con);
        }

        return count($this->collPenggunasRelatedByKodeWilayah);
    }

    /**
     * Method called to associate a Pengguna object to this object
     * through the Pengguna foreign key attribute.
     *
     * @param    Pengguna $l Pengguna
     * @return MstWilayah The current object (for fluent API support)
     */
    public function addPenggunaRelatedByKodeWilayah(Pengguna $l)
    {
        if ($this->collPenggunasRelatedByKodeWilayah === null) {
            $this->initPenggunasRelatedByKodeWilayah();
            $this->collPenggunasRelatedByKodeWilayahPartial = true;
        }
        if (!in_array($l, $this->collPenggunasRelatedByKodeWilayah->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPenggunaRelatedByKodeWilayah($l);
        }

        return $this;
    }

    /**
     * @param	PenggunaRelatedByKodeWilayah $penggunaRelatedByKodeWilayah The penggunaRelatedByKodeWilayah object to add.
     */
    protected function doAddPenggunaRelatedByKodeWilayah($penggunaRelatedByKodeWilayah)
    {
        $this->collPenggunasRelatedByKodeWilayah[]= $penggunaRelatedByKodeWilayah;
        $penggunaRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah($this);
    }

    /**
     * @param	PenggunaRelatedByKodeWilayah $penggunaRelatedByKodeWilayah The penggunaRelatedByKodeWilayah object to remove.
     * @return MstWilayah The current object (for fluent API support)
     */
    public function removePenggunaRelatedByKodeWilayah($penggunaRelatedByKodeWilayah)
    {
        if ($this->getPenggunasRelatedByKodeWilayah()->contains($penggunaRelatedByKodeWilayah)) {
            $this->collPenggunasRelatedByKodeWilayah->remove($this->collPenggunasRelatedByKodeWilayah->search($penggunaRelatedByKodeWilayah));
            if (null === $this->penggunasRelatedByKodeWilayahScheduledForDeletion) {
                $this->penggunasRelatedByKodeWilayahScheduledForDeletion = clone $this->collPenggunasRelatedByKodeWilayah;
                $this->penggunasRelatedByKodeWilayahScheduledForDeletion->clear();
            }
            $this->penggunasRelatedByKodeWilayahScheduledForDeletion[]= clone $penggunaRelatedByKodeWilayah;
            $penggunaRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PenggunasRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pengguna[] List of Pengguna objects
     */
    public function getPenggunasRelatedByKodeWilayahJoinLembagaNonSekolahRelatedByLembagaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PenggunaQuery::create(null, $criteria);
        $query->joinWith('LembagaNonSekolahRelatedByLembagaId', $join_behavior);

        return $this->getPenggunasRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PenggunasRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pengguna[] List of Pengguna objects
     */
    public function getPenggunasRelatedByKodeWilayahJoinLembagaNonSekolahRelatedByLembagaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PenggunaQuery::create(null, $criteria);
        $query->joinWith('LembagaNonSekolahRelatedByLembagaId', $join_behavior);

        return $this->getPenggunasRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PenggunasRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pengguna[] List of Pengguna objects
     */
    public function getPenggunasRelatedByKodeWilayahJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PenggunaQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPenggunasRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PenggunasRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pengguna[] List of Pengguna objects
     */
    public function getPenggunasRelatedByKodeWilayahJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PenggunaQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPenggunasRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PenggunasRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pengguna[] List of Pengguna objects
     */
    public function getPenggunasRelatedByKodeWilayahJoinPeranRelatedByPeranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PenggunaQuery::create(null, $criteria);
        $query->joinWith('PeranRelatedByPeranId', $join_behavior);

        return $this->getPenggunasRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PenggunasRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pengguna[] List of Pengguna objects
     */
    public function getPenggunasRelatedByKodeWilayahJoinPeranRelatedByPeranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PenggunaQuery::create(null, $criteria);
        $query->joinWith('PeranRelatedByPeranId', $join_behavior);

        return $this->getPenggunasRelatedByKodeWilayah($query, $con);
    }

    /**
     * Clears out the collPenggunasRelatedByKodeWilayah collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return MstWilayah The current object (for fluent API support)
     * @see        addPenggunasRelatedByKodeWilayah()
     */
    public function clearPenggunasRelatedByKodeWilayah()
    {
        $this->collPenggunasRelatedByKodeWilayah = null; // important to set this to null since that means it is uninitialized
        $this->collPenggunasRelatedByKodeWilayahPartial = null;

        return $this;
    }

    /**
     * reset is the collPenggunasRelatedByKodeWilayah collection loaded partially
     *
     * @return void
     */
    public function resetPartialPenggunasRelatedByKodeWilayah($v = true)
    {
        $this->collPenggunasRelatedByKodeWilayahPartial = $v;
    }

    /**
     * Initializes the collPenggunasRelatedByKodeWilayah collection.
     *
     * By default this just sets the collPenggunasRelatedByKodeWilayah collection to an empty array (like clearcollPenggunasRelatedByKodeWilayah());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPenggunasRelatedByKodeWilayah($overrideExisting = true)
    {
        if (null !== $this->collPenggunasRelatedByKodeWilayah && !$overrideExisting) {
            return;
        }
        $this->collPenggunasRelatedByKodeWilayah = new PropelObjectCollection();
        $this->collPenggunasRelatedByKodeWilayah->setModel('Pengguna');
    }

    /**
     * Gets an array of Pengguna objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this MstWilayah is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Pengguna[] List of Pengguna objects
     * @throws PropelException
     */
    public function getPenggunasRelatedByKodeWilayah($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPenggunasRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collPenggunasRelatedByKodeWilayah || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPenggunasRelatedByKodeWilayah) {
                // return empty collection
                $this->initPenggunasRelatedByKodeWilayah();
            } else {
                $collPenggunasRelatedByKodeWilayah = PenggunaQuery::create(null, $criteria)
                    ->filterByMstWilayahRelatedByKodeWilayah($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPenggunasRelatedByKodeWilayahPartial && count($collPenggunasRelatedByKodeWilayah)) {
                      $this->initPenggunasRelatedByKodeWilayah(false);

                      foreach($collPenggunasRelatedByKodeWilayah as $obj) {
                        if (false == $this->collPenggunasRelatedByKodeWilayah->contains($obj)) {
                          $this->collPenggunasRelatedByKodeWilayah->append($obj);
                        }
                      }

                      $this->collPenggunasRelatedByKodeWilayahPartial = true;
                    }

                    $collPenggunasRelatedByKodeWilayah->getInternalIterator()->rewind();
                    return $collPenggunasRelatedByKodeWilayah;
                }

                if($partial && $this->collPenggunasRelatedByKodeWilayah) {
                    foreach($this->collPenggunasRelatedByKodeWilayah as $obj) {
                        if($obj->isNew()) {
                            $collPenggunasRelatedByKodeWilayah[] = $obj;
                        }
                    }
                }

                $this->collPenggunasRelatedByKodeWilayah = $collPenggunasRelatedByKodeWilayah;
                $this->collPenggunasRelatedByKodeWilayahPartial = false;
            }
        }

        return $this->collPenggunasRelatedByKodeWilayah;
    }

    /**
     * Sets a collection of PenggunaRelatedByKodeWilayah objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $penggunasRelatedByKodeWilayah A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setPenggunasRelatedByKodeWilayah(PropelCollection $penggunasRelatedByKodeWilayah, PropelPDO $con = null)
    {
        $penggunasRelatedByKodeWilayahToDelete = $this->getPenggunasRelatedByKodeWilayah(new Criteria(), $con)->diff($penggunasRelatedByKodeWilayah);

        $this->penggunasRelatedByKodeWilayahScheduledForDeletion = unserialize(serialize($penggunasRelatedByKodeWilayahToDelete));

        foreach ($penggunasRelatedByKodeWilayahToDelete as $penggunaRelatedByKodeWilayahRemoved) {
            $penggunaRelatedByKodeWilayahRemoved->setMstWilayahRelatedByKodeWilayah(null);
        }

        $this->collPenggunasRelatedByKodeWilayah = null;
        foreach ($penggunasRelatedByKodeWilayah as $penggunaRelatedByKodeWilayah) {
            $this->addPenggunaRelatedByKodeWilayah($penggunaRelatedByKodeWilayah);
        }

        $this->collPenggunasRelatedByKodeWilayah = $penggunasRelatedByKodeWilayah;
        $this->collPenggunasRelatedByKodeWilayahPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Pengguna objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Pengguna objects.
     * @throws PropelException
     */
    public function countPenggunasRelatedByKodeWilayah(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPenggunasRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collPenggunasRelatedByKodeWilayah || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPenggunasRelatedByKodeWilayah) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPenggunasRelatedByKodeWilayah());
            }
            $query = PenggunaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMstWilayahRelatedByKodeWilayah($this)
                ->count($con);
        }

        return count($this->collPenggunasRelatedByKodeWilayah);
    }

    /**
     * Method called to associate a Pengguna object to this object
     * through the Pengguna foreign key attribute.
     *
     * @param    Pengguna $l Pengguna
     * @return MstWilayah The current object (for fluent API support)
     */
    public function addPenggunaRelatedByKodeWilayah(Pengguna $l)
    {
        if ($this->collPenggunasRelatedByKodeWilayah === null) {
            $this->initPenggunasRelatedByKodeWilayah();
            $this->collPenggunasRelatedByKodeWilayahPartial = true;
        }
        if (!in_array($l, $this->collPenggunasRelatedByKodeWilayah->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPenggunaRelatedByKodeWilayah($l);
        }

        return $this;
    }

    /**
     * @param	PenggunaRelatedByKodeWilayah $penggunaRelatedByKodeWilayah The penggunaRelatedByKodeWilayah object to add.
     */
    protected function doAddPenggunaRelatedByKodeWilayah($penggunaRelatedByKodeWilayah)
    {
        $this->collPenggunasRelatedByKodeWilayah[]= $penggunaRelatedByKodeWilayah;
        $penggunaRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah($this);
    }

    /**
     * @param	PenggunaRelatedByKodeWilayah $penggunaRelatedByKodeWilayah The penggunaRelatedByKodeWilayah object to remove.
     * @return MstWilayah The current object (for fluent API support)
     */
    public function removePenggunaRelatedByKodeWilayah($penggunaRelatedByKodeWilayah)
    {
        if ($this->getPenggunasRelatedByKodeWilayah()->contains($penggunaRelatedByKodeWilayah)) {
            $this->collPenggunasRelatedByKodeWilayah->remove($this->collPenggunasRelatedByKodeWilayah->search($penggunaRelatedByKodeWilayah));
            if (null === $this->penggunasRelatedByKodeWilayahScheduledForDeletion) {
                $this->penggunasRelatedByKodeWilayahScheduledForDeletion = clone $this->collPenggunasRelatedByKodeWilayah;
                $this->penggunasRelatedByKodeWilayahScheduledForDeletion->clear();
            }
            $this->penggunasRelatedByKodeWilayahScheduledForDeletion[]= clone $penggunaRelatedByKodeWilayah;
            $penggunaRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PenggunasRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pengguna[] List of Pengguna objects
     */
    public function getPenggunasRelatedByKodeWilayahJoinLembagaNonSekolahRelatedByLembagaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PenggunaQuery::create(null, $criteria);
        $query->joinWith('LembagaNonSekolahRelatedByLembagaId', $join_behavior);

        return $this->getPenggunasRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PenggunasRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pengguna[] List of Pengguna objects
     */
    public function getPenggunasRelatedByKodeWilayahJoinLembagaNonSekolahRelatedByLembagaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PenggunaQuery::create(null, $criteria);
        $query->joinWith('LembagaNonSekolahRelatedByLembagaId', $join_behavior);

        return $this->getPenggunasRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PenggunasRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pengguna[] List of Pengguna objects
     */
    public function getPenggunasRelatedByKodeWilayahJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PenggunaQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPenggunasRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PenggunasRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pengguna[] List of Pengguna objects
     */
    public function getPenggunasRelatedByKodeWilayahJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PenggunaQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPenggunasRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PenggunasRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pengguna[] List of Pengguna objects
     */
    public function getPenggunasRelatedByKodeWilayahJoinPeranRelatedByPeranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PenggunaQuery::create(null, $criteria);
        $query->joinWith('PeranRelatedByPeranId', $join_behavior);

        return $this->getPenggunasRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PenggunasRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pengguna[] List of Pengguna objects
     */
    public function getPenggunasRelatedByKodeWilayahJoinPeranRelatedByPeranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PenggunaQuery::create(null, $criteria);
        $query->joinWith('PeranRelatedByPeranId', $join_behavior);

        return $this->getPenggunasRelatedByKodeWilayah($query, $con);
    }

    /**
     * Clears out the collSekolahsRelatedByKodeWilayah collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return MstWilayah The current object (for fluent API support)
     * @see        addSekolahsRelatedByKodeWilayah()
     */
    public function clearSekolahsRelatedByKodeWilayah()
    {
        $this->collSekolahsRelatedByKodeWilayah = null; // important to set this to null since that means it is uninitialized
        $this->collSekolahsRelatedByKodeWilayahPartial = null;

        return $this;
    }

    /**
     * reset is the collSekolahsRelatedByKodeWilayah collection loaded partially
     *
     * @return void
     */
    public function resetPartialSekolahsRelatedByKodeWilayah($v = true)
    {
        $this->collSekolahsRelatedByKodeWilayahPartial = $v;
    }

    /**
     * Initializes the collSekolahsRelatedByKodeWilayah collection.
     *
     * By default this just sets the collSekolahsRelatedByKodeWilayah collection to an empty array (like clearcollSekolahsRelatedByKodeWilayah());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSekolahsRelatedByKodeWilayah($overrideExisting = true)
    {
        if (null !== $this->collSekolahsRelatedByKodeWilayah && !$overrideExisting) {
            return;
        }
        $this->collSekolahsRelatedByKodeWilayah = new PropelObjectCollection();
        $this->collSekolahsRelatedByKodeWilayah->setModel('Sekolah');
    }

    /**
     * Gets an array of Sekolah objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this MstWilayah is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Sekolah[] List of Sekolah objects
     * @throws PropelException
     */
    public function getSekolahsRelatedByKodeWilayah($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSekolahsRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collSekolahsRelatedByKodeWilayah || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSekolahsRelatedByKodeWilayah) {
                // return empty collection
                $this->initSekolahsRelatedByKodeWilayah();
            } else {
                $collSekolahsRelatedByKodeWilayah = SekolahQuery::create(null, $criteria)
                    ->filterByMstWilayahRelatedByKodeWilayah($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSekolahsRelatedByKodeWilayahPartial && count($collSekolahsRelatedByKodeWilayah)) {
                      $this->initSekolahsRelatedByKodeWilayah(false);

                      foreach($collSekolahsRelatedByKodeWilayah as $obj) {
                        if (false == $this->collSekolahsRelatedByKodeWilayah->contains($obj)) {
                          $this->collSekolahsRelatedByKodeWilayah->append($obj);
                        }
                      }

                      $this->collSekolahsRelatedByKodeWilayahPartial = true;
                    }

                    $collSekolahsRelatedByKodeWilayah->getInternalIterator()->rewind();
                    return $collSekolahsRelatedByKodeWilayah;
                }

                if($partial && $this->collSekolahsRelatedByKodeWilayah) {
                    foreach($this->collSekolahsRelatedByKodeWilayah as $obj) {
                        if($obj->isNew()) {
                            $collSekolahsRelatedByKodeWilayah[] = $obj;
                        }
                    }
                }

                $this->collSekolahsRelatedByKodeWilayah = $collSekolahsRelatedByKodeWilayah;
                $this->collSekolahsRelatedByKodeWilayahPartial = false;
            }
        }

        return $this->collSekolahsRelatedByKodeWilayah;
    }

    /**
     * Sets a collection of SekolahRelatedByKodeWilayah objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $sekolahsRelatedByKodeWilayah A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setSekolahsRelatedByKodeWilayah(PropelCollection $sekolahsRelatedByKodeWilayah, PropelPDO $con = null)
    {
        $sekolahsRelatedByKodeWilayahToDelete = $this->getSekolahsRelatedByKodeWilayah(new Criteria(), $con)->diff($sekolahsRelatedByKodeWilayah);

        $this->sekolahsRelatedByKodeWilayahScheduledForDeletion = unserialize(serialize($sekolahsRelatedByKodeWilayahToDelete));

        foreach ($sekolahsRelatedByKodeWilayahToDelete as $sekolahRelatedByKodeWilayahRemoved) {
            $sekolahRelatedByKodeWilayahRemoved->setMstWilayahRelatedByKodeWilayah(null);
        }

        $this->collSekolahsRelatedByKodeWilayah = null;
        foreach ($sekolahsRelatedByKodeWilayah as $sekolahRelatedByKodeWilayah) {
            $this->addSekolahRelatedByKodeWilayah($sekolahRelatedByKodeWilayah);
        }

        $this->collSekolahsRelatedByKodeWilayah = $sekolahsRelatedByKodeWilayah;
        $this->collSekolahsRelatedByKodeWilayahPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Sekolah objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Sekolah objects.
     * @throws PropelException
     */
    public function countSekolahsRelatedByKodeWilayah(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSekolahsRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collSekolahsRelatedByKodeWilayah || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSekolahsRelatedByKodeWilayah) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSekolahsRelatedByKodeWilayah());
            }
            $query = SekolahQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMstWilayahRelatedByKodeWilayah($this)
                ->count($con);
        }

        return count($this->collSekolahsRelatedByKodeWilayah);
    }

    /**
     * Method called to associate a Sekolah object to this object
     * through the Sekolah foreign key attribute.
     *
     * @param    Sekolah $l Sekolah
     * @return MstWilayah The current object (for fluent API support)
     */
    public function addSekolahRelatedByKodeWilayah(Sekolah $l)
    {
        if ($this->collSekolahsRelatedByKodeWilayah === null) {
            $this->initSekolahsRelatedByKodeWilayah();
            $this->collSekolahsRelatedByKodeWilayahPartial = true;
        }
        if (!in_array($l, $this->collSekolahsRelatedByKodeWilayah->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSekolahRelatedByKodeWilayah($l);
        }

        return $this;
    }

    /**
     * @param	SekolahRelatedByKodeWilayah $sekolahRelatedByKodeWilayah The sekolahRelatedByKodeWilayah object to add.
     */
    protected function doAddSekolahRelatedByKodeWilayah($sekolahRelatedByKodeWilayah)
    {
        $this->collSekolahsRelatedByKodeWilayah[]= $sekolahRelatedByKodeWilayah;
        $sekolahRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah($this);
    }

    /**
     * @param	SekolahRelatedByKodeWilayah $sekolahRelatedByKodeWilayah The sekolahRelatedByKodeWilayah object to remove.
     * @return MstWilayah The current object (for fluent API support)
     */
    public function removeSekolahRelatedByKodeWilayah($sekolahRelatedByKodeWilayah)
    {
        if ($this->getSekolahsRelatedByKodeWilayah()->contains($sekolahRelatedByKodeWilayah)) {
            $this->collSekolahsRelatedByKodeWilayah->remove($this->collSekolahsRelatedByKodeWilayah->search($sekolahRelatedByKodeWilayah));
            if (null === $this->sekolahsRelatedByKodeWilayahScheduledForDeletion) {
                $this->sekolahsRelatedByKodeWilayahScheduledForDeletion = clone $this->collSekolahsRelatedByKodeWilayah;
                $this->sekolahsRelatedByKodeWilayahScheduledForDeletion->clear();
            }
            $this->sekolahsRelatedByKodeWilayahScheduledForDeletion[]= clone $sekolahRelatedByKodeWilayah;
            $sekolahRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related SekolahsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sekolah[] List of Sekolah objects
     */
    public function getSekolahsRelatedByKodeWilayahJoinBentukPendidikanRelatedByBentukPendidikanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SekolahQuery::create(null, $criteria);
        $query->joinWith('BentukPendidikanRelatedByBentukPendidikanId', $join_behavior);

        return $this->getSekolahsRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related SekolahsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sekolah[] List of Sekolah objects
     */
    public function getSekolahsRelatedByKodeWilayahJoinBentukPendidikanRelatedByBentukPendidikanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SekolahQuery::create(null, $criteria);
        $query->joinWith('BentukPendidikanRelatedByBentukPendidikanId', $join_behavior);

        return $this->getSekolahsRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related SekolahsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sekolah[] List of Sekolah objects
     */
    public function getSekolahsRelatedByKodeWilayahJoinKebutuhanKhususRelatedByKebutuhanKhususId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SekolahQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususId', $join_behavior);

        return $this->getSekolahsRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related SekolahsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sekolah[] List of Sekolah objects
     */
    public function getSekolahsRelatedByKodeWilayahJoinKebutuhanKhususRelatedByKebutuhanKhususId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SekolahQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususId', $join_behavior);

        return $this->getSekolahsRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related SekolahsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sekolah[] List of Sekolah objects
     */
    public function getSekolahsRelatedByKodeWilayahJoinStatusKepemilikanRelatedByStatusKepemilikanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SekolahQuery::create(null, $criteria);
        $query->joinWith('StatusKepemilikanRelatedByStatusKepemilikanId', $join_behavior);

        return $this->getSekolahsRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related SekolahsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sekolah[] List of Sekolah objects
     */
    public function getSekolahsRelatedByKodeWilayahJoinStatusKepemilikanRelatedByStatusKepemilikanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SekolahQuery::create(null, $criteria);
        $query->joinWith('StatusKepemilikanRelatedByStatusKepemilikanId', $join_behavior);

        return $this->getSekolahsRelatedByKodeWilayah($query, $con);
    }

    /**
     * Clears out the collSekolahsRelatedByKodeWilayah collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return MstWilayah The current object (for fluent API support)
     * @see        addSekolahsRelatedByKodeWilayah()
     */
    public function clearSekolahsRelatedByKodeWilayah()
    {
        $this->collSekolahsRelatedByKodeWilayah = null; // important to set this to null since that means it is uninitialized
        $this->collSekolahsRelatedByKodeWilayahPartial = null;

        return $this;
    }

    /**
     * reset is the collSekolahsRelatedByKodeWilayah collection loaded partially
     *
     * @return void
     */
    public function resetPartialSekolahsRelatedByKodeWilayah($v = true)
    {
        $this->collSekolahsRelatedByKodeWilayahPartial = $v;
    }

    /**
     * Initializes the collSekolahsRelatedByKodeWilayah collection.
     *
     * By default this just sets the collSekolahsRelatedByKodeWilayah collection to an empty array (like clearcollSekolahsRelatedByKodeWilayah());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSekolahsRelatedByKodeWilayah($overrideExisting = true)
    {
        if (null !== $this->collSekolahsRelatedByKodeWilayah && !$overrideExisting) {
            return;
        }
        $this->collSekolahsRelatedByKodeWilayah = new PropelObjectCollection();
        $this->collSekolahsRelatedByKodeWilayah->setModel('Sekolah');
    }

    /**
     * Gets an array of Sekolah objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this MstWilayah is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Sekolah[] List of Sekolah objects
     * @throws PropelException
     */
    public function getSekolahsRelatedByKodeWilayah($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSekolahsRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collSekolahsRelatedByKodeWilayah || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSekolahsRelatedByKodeWilayah) {
                // return empty collection
                $this->initSekolahsRelatedByKodeWilayah();
            } else {
                $collSekolahsRelatedByKodeWilayah = SekolahQuery::create(null, $criteria)
                    ->filterByMstWilayahRelatedByKodeWilayah($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSekolahsRelatedByKodeWilayahPartial && count($collSekolahsRelatedByKodeWilayah)) {
                      $this->initSekolahsRelatedByKodeWilayah(false);

                      foreach($collSekolahsRelatedByKodeWilayah as $obj) {
                        if (false == $this->collSekolahsRelatedByKodeWilayah->contains($obj)) {
                          $this->collSekolahsRelatedByKodeWilayah->append($obj);
                        }
                      }

                      $this->collSekolahsRelatedByKodeWilayahPartial = true;
                    }

                    $collSekolahsRelatedByKodeWilayah->getInternalIterator()->rewind();
                    return $collSekolahsRelatedByKodeWilayah;
                }

                if($partial && $this->collSekolahsRelatedByKodeWilayah) {
                    foreach($this->collSekolahsRelatedByKodeWilayah as $obj) {
                        if($obj->isNew()) {
                            $collSekolahsRelatedByKodeWilayah[] = $obj;
                        }
                    }
                }

                $this->collSekolahsRelatedByKodeWilayah = $collSekolahsRelatedByKodeWilayah;
                $this->collSekolahsRelatedByKodeWilayahPartial = false;
            }
        }

        return $this->collSekolahsRelatedByKodeWilayah;
    }

    /**
     * Sets a collection of SekolahRelatedByKodeWilayah objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $sekolahsRelatedByKodeWilayah A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setSekolahsRelatedByKodeWilayah(PropelCollection $sekolahsRelatedByKodeWilayah, PropelPDO $con = null)
    {
        $sekolahsRelatedByKodeWilayahToDelete = $this->getSekolahsRelatedByKodeWilayah(new Criteria(), $con)->diff($sekolahsRelatedByKodeWilayah);

        $this->sekolahsRelatedByKodeWilayahScheduledForDeletion = unserialize(serialize($sekolahsRelatedByKodeWilayahToDelete));

        foreach ($sekolahsRelatedByKodeWilayahToDelete as $sekolahRelatedByKodeWilayahRemoved) {
            $sekolahRelatedByKodeWilayahRemoved->setMstWilayahRelatedByKodeWilayah(null);
        }

        $this->collSekolahsRelatedByKodeWilayah = null;
        foreach ($sekolahsRelatedByKodeWilayah as $sekolahRelatedByKodeWilayah) {
            $this->addSekolahRelatedByKodeWilayah($sekolahRelatedByKodeWilayah);
        }

        $this->collSekolahsRelatedByKodeWilayah = $sekolahsRelatedByKodeWilayah;
        $this->collSekolahsRelatedByKodeWilayahPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Sekolah objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Sekolah objects.
     * @throws PropelException
     */
    public function countSekolahsRelatedByKodeWilayah(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSekolahsRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collSekolahsRelatedByKodeWilayah || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSekolahsRelatedByKodeWilayah) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSekolahsRelatedByKodeWilayah());
            }
            $query = SekolahQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMstWilayahRelatedByKodeWilayah($this)
                ->count($con);
        }

        return count($this->collSekolahsRelatedByKodeWilayah);
    }

    /**
     * Method called to associate a Sekolah object to this object
     * through the Sekolah foreign key attribute.
     *
     * @param    Sekolah $l Sekolah
     * @return MstWilayah The current object (for fluent API support)
     */
    public function addSekolahRelatedByKodeWilayah(Sekolah $l)
    {
        if ($this->collSekolahsRelatedByKodeWilayah === null) {
            $this->initSekolahsRelatedByKodeWilayah();
            $this->collSekolahsRelatedByKodeWilayahPartial = true;
        }
        if (!in_array($l, $this->collSekolahsRelatedByKodeWilayah->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSekolahRelatedByKodeWilayah($l);
        }

        return $this;
    }

    /**
     * @param	SekolahRelatedByKodeWilayah $sekolahRelatedByKodeWilayah The sekolahRelatedByKodeWilayah object to add.
     */
    protected function doAddSekolahRelatedByKodeWilayah($sekolahRelatedByKodeWilayah)
    {
        $this->collSekolahsRelatedByKodeWilayah[]= $sekolahRelatedByKodeWilayah;
        $sekolahRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah($this);
    }

    /**
     * @param	SekolahRelatedByKodeWilayah $sekolahRelatedByKodeWilayah The sekolahRelatedByKodeWilayah object to remove.
     * @return MstWilayah The current object (for fluent API support)
     */
    public function removeSekolahRelatedByKodeWilayah($sekolahRelatedByKodeWilayah)
    {
        if ($this->getSekolahsRelatedByKodeWilayah()->contains($sekolahRelatedByKodeWilayah)) {
            $this->collSekolahsRelatedByKodeWilayah->remove($this->collSekolahsRelatedByKodeWilayah->search($sekolahRelatedByKodeWilayah));
            if (null === $this->sekolahsRelatedByKodeWilayahScheduledForDeletion) {
                $this->sekolahsRelatedByKodeWilayahScheduledForDeletion = clone $this->collSekolahsRelatedByKodeWilayah;
                $this->sekolahsRelatedByKodeWilayahScheduledForDeletion->clear();
            }
            $this->sekolahsRelatedByKodeWilayahScheduledForDeletion[]= clone $sekolahRelatedByKodeWilayah;
            $sekolahRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related SekolahsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sekolah[] List of Sekolah objects
     */
    public function getSekolahsRelatedByKodeWilayahJoinBentukPendidikanRelatedByBentukPendidikanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SekolahQuery::create(null, $criteria);
        $query->joinWith('BentukPendidikanRelatedByBentukPendidikanId', $join_behavior);

        return $this->getSekolahsRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related SekolahsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sekolah[] List of Sekolah objects
     */
    public function getSekolahsRelatedByKodeWilayahJoinBentukPendidikanRelatedByBentukPendidikanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SekolahQuery::create(null, $criteria);
        $query->joinWith('BentukPendidikanRelatedByBentukPendidikanId', $join_behavior);

        return $this->getSekolahsRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related SekolahsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sekolah[] List of Sekolah objects
     */
    public function getSekolahsRelatedByKodeWilayahJoinKebutuhanKhususRelatedByKebutuhanKhususId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SekolahQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususId', $join_behavior);

        return $this->getSekolahsRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related SekolahsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sekolah[] List of Sekolah objects
     */
    public function getSekolahsRelatedByKodeWilayahJoinKebutuhanKhususRelatedByKebutuhanKhususId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SekolahQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususId', $join_behavior);

        return $this->getSekolahsRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related SekolahsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sekolah[] List of Sekolah objects
     */
    public function getSekolahsRelatedByKodeWilayahJoinStatusKepemilikanRelatedByStatusKepemilikanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SekolahQuery::create(null, $criteria);
        $query->joinWith('StatusKepemilikanRelatedByStatusKepemilikanId', $join_behavior);

        return $this->getSekolahsRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related SekolahsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sekolah[] List of Sekolah objects
     */
    public function getSekolahsRelatedByKodeWilayahJoinStatusKepemilikanRelatedByStatusKepemilikanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SekolahQuery::create(null, $criteria);
        $query->joinWith('StatusKepemilikanRelatedByStatusKepemilikanId', $join_behavior);

        return $this->getSekolahsRelatedByKodeWilayah($query, $con);
    }

    /**
     * Clears out the collYayasansRelatedByKodeWilayah collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return MstWilayah The current object (for fluent API support)
     * @see        addYayasansRelatedByKodeWilayah()
     */
    public function clearYayasansRelatedByKodeWilayah()
    {
        $this->collYayasansRelatedByKodeWilayah = null; // important to set this to null since that means it is uninitialized
        $this->collYayasansRelatedByKodeWilayahPartial = null;

        return $this;
    }

    /**
     * reset is the collYayasansRelatedByKodeWilayah collection loaded partially
     *
     * @return void
     */
    public function resetPartialYayasansRelatedByKodeWilayah($v = true)
    {
        $this->collYayasansRelatedByKodeWilayahPartial = $v;
    }

    /**
     * Initializes the collYayasansRelatedByKodeWilayah collection.
     *
     * By default this just sets the collYayasansRelatedByKodeWilayah collection to an empty array (like clearcollYayasansRelatedByKodeWilayah());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initYayasansRelatedByKodeWilayah($overrideExisting = true)
    {
        if (null !== $this->collYayasansRelatedByKodeWilayah && !$overrideExisting) {
            return;
        }
        $this->collYayasansRelatedByKodeWilayah = new PropelObjectCollection();
        $this->collYayasansRelatedByKodeWilayah->setModel('Yayasan');
    }

    /**
     * Gets an array of Yayasan objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this MstWilayah is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Yayasan[] List of Yayasan objects
     * @throws PropelException
     */
    public function getYayasansRelatedByKodeWilayah($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collYayasansRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collYayasansRelatedByKodeWilayah || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collYayasansRelatedByKodeWilayah) {
                // return empty collection
                $this->initYayasansRelatedByKodeWilayah();
            } else {
                $collYayasansRelatedByKodeWilayah = YayasanQuery::create(null, $criteria)
                    ->filterByMstWilayahRelatedByKodeWilayah($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collYayasansRelatedByKodeWilayahPartial && count($collYayasansRelatedByKodeWilayah)) {
                      $this->initYayasansRelatedByKodeWilayah(false);

                      foreach($collYayasansRelatedByKodeWilayah as $obj) {
                        if (false == $this->collYayasansRelatedByKodeWilayah->contains($obj)) {
                          $this->collYayasansRelatedByKodeWilayah->append($obj);
                        }
                      }

                      $this->collYayasansRelatedByKodeWilayahPartial = true;
                    }

                    $collYayasansRelatedByKodeWilayah->getInternalIterator()->rewind();
                    return $collYayasansRelatedByKodeWilayah;
                }

                if($partial && $this->collYayasansRelatedByKodeWilayah) {
                    foreach($this->collYayasansRelatedByKodeWilayah as $obj) {
                        if($obj->isNew()) {
                            $collYayasansRelatedByKodeWilayah[] = $obj;
                        }
                    }
                }

                $this->collYayasansRelatedByKodeWilayah = $collYayasansRelatedByKodeWilayah;
                $this->collYayasansRelatedByKodeWilayahPartial = false;
            }
        }

        return $this->collYayasansRelatedByKodeWilayah;
    }

    /**
     * Sets a collection of YayasanRelatedByKodeWilayah objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $yayasansRelatedByKodeWilayah A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setYayasansRelatedByKodeWilayah(PropelCollection $yayasansRelatedByKodeWilayah, PropelPDO $con = null)
    {
        $yayasansRelatedByKodeWilayahToDelete = $this->getYayasansRelatedByKodeWilayah(new Criteria(), $con)->diff($yayasansRelatedByKodeWilayah);

        $this->yayasansRelatedByKodeWilayahScheduledForDeletion = unserialize(serialize($yayasansRelatedByKodeWilayahToDelete));

        foreach ($yayasansRelatedByKodeWilayahToDelete as $yayasanRelatedByKodeWilayahRemoved) {
            $yayasanRelatedByKodeWilayahRemoved->setMstWilayahRelatedByKodeWilayah(null);
        }

        $this->collYayasansRelatedByKodeWilayah = null;
        foreach ($yayasansRelatedByKodeWilayah as $yayasanRelatedByKodeWilayah) {
            $this->addYayasanRelatedByKodeWilayah($yayasanRelatedByKodeWilayah);
        }

        $this->collYayasansRelatedByKodeWilayah = $yayasansRelatedByKodeWilayah;
        $this->collYayasansRelatedByKodeWilayahPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Yayasan objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Yayasan objects.
     * @throws PropelException
     */
    public function countYayasansRelatedByKodeWilayah(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collYayasansRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collYayasansRelatedByKodeWilayah || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collYayasansRelatedByKodeWilayah) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getYayasansRelatedByKodeWilayah());
            }
            $query = YayasanQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMstWilayahRelatedByKodeWilayah($this)
                ->count($con);
        }

        return count($this->collYayasansRelatedByKodeWilayah);
    }

    /**
     * Method called to associate a Yayasan object to this object
     * through the Yayasan foreign key attribute.
     *
     * @param    Yayasan $l Yayasan
     * @return MstWilayah The current object (for fluent API support)
     */
    public function addYayasanRelatedByKodeWilayah(Yayasan $l)
    {
        if ($this->collYayasansRelatedByKodeWilayah === null) {
            $this->initYayasansRelatedByKodeWilayah();
            $this->collYayasansRelatedByKodeWilayahPartial = true;
        }
        if (!in_array($l, $this->collYayasansRelatedByKodeWilayah->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddYayasanRelatedByKodeWilayah($l);
        }

        return $this;
    }

    /**
     * @param	YayasanRelatedByKodeWilayah $yayasanRelatedByKodeWilayah The yayasanRelatedByKodeWilayah object to add.
     */
    protected function doAddYayasanRelatedByKodeWilayah($yayasanRelatedByKodeWilayah)
    {
        $this->collYayasansRelatedByKodeWilayah[]= $yayasanRelatedByKodeWilayah;
        $yayasanRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah($this);
    }

    /**
     * @param	YayasanRelatedByKodeWilayah $yayasanRelatedByKodeWilayah The yayasanRelatedByKodeWilayah object to remove.
     * @return MstWilayah The current object (for fluent API support)
     */
    public function removeYayasanRelatedByKodeWilayah($yayasanRelatedByKodeWilayah)
    {
        if ($this->getYayasansRelatedByKodeWilayah()->contains($yayasanRelatedByKodeWilayah)) {
            $this->collYayasansRelatedByKodeWilayah->remove($this->collYayasansRelatedByKodeWilayah->search($yayasanRelatedByKodeWilayah));
            if (null === $this->yayasansRelatedByKodeWilayahScheduledForDeletion) {
                $this->yayasansRelatedByKodeWilayahScheduledForDeletion = clone $this->collYayasansRelatedByKodeWilayah;
                $this->yayasansRelatedByKodeWilayahScheduledForDeletion->clear();
            }
            $this->yayasansRelatedByKodeWilayahScheduledForDeletion[]= clone $yayasanRelatedByKodeWilayah;
            $yayasanRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah(null);
        }

        return $this;
    }

    /**
     * Clears out the collYayasansRelatedByKodeWilayah collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return MstWilayah The current object (for fluent API support)
     * @see        addYayasansRelatedByKodeWilayah()
     */
    public function clearYayasansRelatedByKodeWilayah()
    {
        $this->collYayasansRelatedByKodeWilayah = null; // important to set this to null since that means it is uninitialized
        $this->collYayasansRelatedByKodeWilayahPartial = null;

        return $this;
    }

    /**
     * reset is the collYayasansRelatedByKodeWilayah collection loaded partially
     *
     * @return void
     */
    public function resetPartialYayasansRelatedByKodeWilayah($v = true)
    {
        $this->collYayasansRelatedByKodeWilayahPartial = $v;
    }

    /**
     * Initializes the collYayasansRelatedByKodeWilayah collection.
     *
     * By default this just sets the collYayasansRelatedByKodeWilayah collection to an empty array (like clearcollYayasansRelatedByKodeWilayah());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initYayasansRelatedByKodeWilayah($overrideExisting = true)
    {
        if (null !== $this->collYayasansRelatedByKodeWilayah && !$overrideExisting) {
            return;
        }
        $this->collYayasansRelatedByKodeWilayah = new PropelObjectCollection();
        $this->collYayasansRelatedByKodeWilayah->setModel('Yayasan');
    }

    /**
     * Gets an array of Yayasan objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this MstWilayah is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Yayasan[] List of Yayasan objects
     * @throws PropelException
     */
    public function getYayasansRelatedByKodeWilayah($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collYayasansRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collYayasansRelatedByKodeWilayah || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collYayasansRelatedByKodeWilayah) {
                // return empty collection
                $this->initYayasansRelatedByKodeWilayah();
            } else {
                $collYayasansRelatedByKodeWilayah = YayasanQuery::create(null, $criteria)
                    ->filterByMstWilayahRelatedByKodeWilayah($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collYayasansRelatedByKodeWilayahPartial && count($collYayasansRelatedByKodeWilayah)) {
                      $this->initYayasansRelatedByKodeWilayah(false);

                      foreach($collYayasansRelatedByKodeWilayah as $obj) {
                        if (false == $this->collYayasansRelatedByKodeWilayah->contains($obj)) {
                          $this->collYayasansRelatedByKodeWilayah->append($obj);
                        }
                      }

                      $this->collYayasansRelatedByKodeWilayahPartial = true;
                    }

                    $collYayasansRelatedByKodeWilayah->getInternalIterator()->rewind();
                    return $collYayasansRelatedByKodeWilayah;
                }

                if($partial && $this->collYayasansRelatedByKodeWilayah) {
                    foreach($this->collYayasansRelatedByKodeWilayah as $obj) {
                        if($obj->isNew()) {
                            $collYayasansRelatedByKodeWilayah[] = $obj;
                        }
                    }
                }

                $this->collYayasansRelatedByKodeWilayah = $collYayasansRelatedByKodeWilayah;
                $this->collYayasansRelatedByKodeWilayahPartial = false;
            }
        }

        return $this->collYayasansRelatedByKodeWilayah;
    }

    /**
     * Sets a collection of YayasanRelatedByKodeWilayah objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $yayasansRelatedByKodeWilayah A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setYayasansRelatedByKodeWilayah(PropelCollection $yayasansRelatedByKodeWilayah, PropelPDO $con = null)
    {
        $yayasansRelatedByKodeWilayahToDelete = $this->getYayasansRelatedByKodeWilayah(new Criteria(), $con)->diff($yayasansRelatedByKodeWilayah);

        $this->yayasansRelatedByKodeWilayahScheduledForDeletion = unserialize(serialize($yayasansRelatedByKodeWilayahToDelete));

        foreach ($yayasansRelatedByKodeWilayahToDelete as $yayasanRelatedByKodeWilayahRemoved) {
            $yayasanRelatedByKodeWilayahRemoved->setMstWilayahRelatedByKodeWilayah(null);
        }

        $this->collYayasansRelatedByKodeWilayah = null;
        foreach ($yayasansRelatedByKodeWilayah as $yayasanRelatedByKodeWilayah) {
            $this->addYayasanRelatedByKodeWilayah($yayasanRelatedByKodeWilayah);
        }

        $this->collYayasansRelatedByKodeWilayah = $yayasansRelatedByKodeWilayah;
        $this->collYayasansRelatedByKodeWilayahPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Yayasan objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Yayasan objects.
     * @throws PropelException
     */
    public function countYayasansRelatedByKodeWilayah(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collYayasansRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collYayasansRelatedByKodeWilayah || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collYayasansRelatedByKodeWilayah) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getYayasansRelatedByKodeWilayah());
            }
            $query = YayasanQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMstWilayahRelatedByKodeWilayah($this)
                ->count($con);
        }

        return count($this->collYayasansRelatedByKodeWilayah);
    }

    /**
     * Method called to associate a Yayasan object to this object
     * through the Yayasan foreign key attribute.
     *
     * @param    Yayasan $l Yayasan
     * @return MstWilayah The current object (for fluent API support)
     */
    public function addYayasanRelatedByKodeWilayah(Yayasan $l)
    {
        if ($this->collYayasansRelatedByKodeWilayah === null) {
            $this->initYayasansRelatedByKodeWilayah();
            $this->collYayasansRelatedByKodeWilayahPartial = true;
        }
        if (!in_array($l, $this->collYayasansRelatedByKodeWilayah->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddYayasanRelatedByKodeWilayah($l);
        }

        return $this;
    }

    /**
     * @param	YayasanRelatedByKodeWilayah $yayasanRelatedByKodeWilayah The yayasanRelatedByKodeWilayah object to add.
     */
    protected function doAddYayasanRelatedByKodeWilayah($yayasanRelatedByKodeWilayah)
    {
        $this->collYayasansRelatedByKodeWilayah[]= $yayasanRelatedByKodeWilayah;
        $yayasanRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah($this);
    }

    /**
     * @param	YayasanRelatedByKodeWilayah $yayasanRelatedByKodeWilayah The yayasanRelatedByKodeWilayah object to remove.
     * @return MstWilayah The current object (for fluent API support)
     */
    public function removeYayasanRelatedByKodeWilayah($yayasanRelatedByKodeWilayah)
    {
        if ($this->getYayasansRelatedByKodeWilayah()->contains($yayasanRelatedByKodeWilayah)) {
            $this->collYayasansRelatedByKodeWilayah->remove($this->collYayasansRelatedByKodeWilayah->search($yayasanRelatedByKodeWilayah));
            if (null === $this->yayasansRelatedByKodeWilayahScheduledForDeletion) {
                $this->yayasansRelatedByKodeWilayahScheduledForDeletion = clone $this->collYayasansRelatedByKodeWilayah;
                $this->yayasansRelatedByKodeWilayahScheduledForDeletion->clear();
            }
            $this->yayasansRelatedByKodeWilayahScheduledForDeletion[]= clone $yayasanRelatedByKodeWilayah;
            $yayasanRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah(null);
        }

        return $this;
    }

    /**
     * Clears out the collPtksRelatedByKodeWilayah collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return MstWilayah The current object (for fluent API support)
     * @see        addPtksRelatedByKodeWilayah()
     */
    public function clearPtksRelatedByKodeWilayah()
    {
        $this->collPtksRelatedByKodeWilayah = null; // important to set this to null since that means it is uninitialized
        $this->collPtksRelatedByKodeWilayahPartial = null;

        return $this;
    }

    /**
     * reset is the collPtksRelatedByKodeWilayah collection loaded partially
     *
     * @return void
     */
    public function resetPartialPtksRelatedByKodeWilayah($v = true)
    {
        $this->collPtksRelatedByKodeWilayahPartial = $v;
    }

    /**
     * Initializes the collPtksRelatedByKodeWilayah collection.
     *
     * By default this just sets the collPtksRelatedByKodeWilayah collection to an empty array (like clearcollPtksRelatedByKodeWilayah());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPtksRelatedByKodeWilayah($overrideExisting = true)
    {
        if (null !== $this->collPtksRelatedByKodeWilayah && !$overrideExisting) {
            return;
        }
        $this->collPtksRelatedByKodeWilayah = new PropelObjectCollection();
        $this->collPtksRelatedByKodeWilayah->setModel('Ptk');
    }

    /**
     * Gets an array of Ptk objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this MstWilayah is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     * @throws PropelException
     */
    public function getPtksRelatedByKodeWilayah($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPtksRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collPtksRelatedByKodeWilayah || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPtksRelatedByKodeWilayah) {
                // return empty collection
                $this->initPtksRelatedByKodeWilayah();
            } else {
                $collPtksRelatedByKodeWilayah = PtkQuery::create(null, $criteria)
                    ->filterByMstWilayahRelatedByKodeWilayah($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPtksRelatedByKodeWilayahPartial && count($collPtksRelatedByKodeWilayah)) {
                      $this->initPtksRelatedByKodeWilayah(false);

                      foreach($collPtksRelatedByKodeWilayah as $obj) {
                        if (false == $this->collPtksRelatedByKodeWilayah->contains($obj)) {
                          $this->collPtksRelatedByKodeWilayah->append($obj);
                        }
                      }

                      $this->collPtksRelatedByKodeWilayahPartial = true;
                    }

                    $collPtksRelatedByKodeWilayah->getInternalIterator()->rewind();
                    return $collPtksRelatedByKodeWilayah;
                }

                if($partial && $this->collPtksRelatedByKodeWilayah) {
                    foreach($this->collPtksRelatedByKodeWilayah as $obj) {
                        if($obj->isNew()) {
                            $collPtksRelatedByKodeWilayah[] = $obj;
                        }
                    }
                }

                $this->collPtksRelatedByKodeWilayah = $collPtksRelatedByKodeWilayah;
                $this->collPtksRelatedByKodeWilayahPartial = false;
            }
        }

        return $this->collPtksRelatedByKodeWilayah;
    }

    /**
     * Sets a collection of PtkRelatedByKodeWilayah objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $ptksRelatedByKodeWilayah A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setPtksRelatedByKodeWilayah(PropelCollection $ptksRelatedByKodeWilayah, PropelPDO $con = null)
    {
        $ptksRelatedByKodeWilayahToDelete = $this->getPtksRelatedByKodeWilayah(new Criteria(), $con)->diff($ptksRelatedByKodeWilayah);

        $this->ptksRelatedByKodeWilayahScheduledForDeletion = unserialize(serialize($ptksRelatedByKodeWilayahToDelete));

        foreach ($ptksRelatedByKodeWilayahToDelete as $ptkRelatedByKodeWilayahRemoved) {
            $ptkRelatedByKodeWilayahRemoved->setMstWilayahRelatedByKodeWilayah(null);
        }

        $this->collPtksRelatedByKodeWilayah = null;
        foreach ($ptksRelatedByKodeWilayah as $ptkRelatedByKodeWilayah) {
            $this->addPtkRelatedByKodeWilayah($ptkRelatedByKodeWilayah);
        }

        $this->collPtksRelatedByKodeWilayah = $ptksRelatedByKodeWilayah;
        $this->collPtksRelatedByKodeWilayahPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Ptk objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Ptk objects.
     * @throws PropelException
     */
    public function countPtksRelatedByKodeWilayah(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPtksRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collPtksRelatedByKodeWilayah || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPtksRelatedByKodeWilayah) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPtksRelatedByKodeWilayah());
            }
            $query = PtkQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMstWilayahRelatedByKodeWilayah($this)
                ->count($con);
        }

        return count($this->collPtksRelatedByKodeWilayah);
    }

    /**
     * Method called to associate a Ptk object to this object
     * through the Ptk foreign key attribute.
     *
     * @param    Ptk $l Ptk
     * @return MstWilayah The current object (for fluent API support)
     */
    public function addPtkRelatedByKodeWilayah(Ptk $l)
    {
        if ($this->collPtksRelatedByKodeWilayah === null) {
            $this->initPtksRelatedByKodeWilayah();
            $this->collPtksRelatedByKodeWilayahPartial = true;
        }
        if (!in_array($l, $this->collPtksRelatedByKodeWilayah->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPtkRelatedByKodeWilayah($l);
        }

        return $this;
    }

    /**
     * @param	PtkRelatedByKodeWilayah $ptkRelatedByKodeWilayah The ptkRelatedByKodeWilayah object to add.
     */
    protected function doAddPtkRelatedByKodeWilayah($ptkRelatedByKodeWilayah)
    {
        $this->collPtksRelatedByKodeWilayah[]= $ptkRelatedByKodeWilayah;
        $ptkRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah($this);
    }

    /**
     * @param	PtkRelatedByKodeWilayah $ptkRelatedByKodeWilayah The ptkRelatedByKodeWilayah object to remove.
     * @return MstWilayah The current object (for fluent API support)
     */
    public function removePtkRelatedByKodeWilayah($ptkRelatedByKodeWilayah)
    {
        if ($this->getPtksRelatedByKodeWilayah()->contains($ptkRelatedByKodeWilayah)) {
            $this->collPtksRelatedByKodeWilayah->remove($this->collPtksRelatedByKodeWilayah->search($ptkRelatedByKodeWilayah));
            if (null === $this->ptksRelatedByKodeWilayahScheduledForDeletion) {
                $this->ptksRelatedByKodeWilayahScheduledForDeletion = clone $this->collPtksRelatedByKodeWilayah;
                $this->ptksRelatedByKodeWilayahScheduledForDeletion->clear();
            }
            $this->ptksRelatedByKodeWilayahScheduledForDeletion[]= clone $ptkRelatedByKodeWilayah;
            $ptkRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinSekolahRelatedByEntrySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedByEntrySekolahId', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinSekolahRelatedByEntrySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedByEntrySekolahId', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinAgamaRelatedByAgamaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('AgamaRelatedByAgamaId', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinAgamaRelatedByAgamaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('AgamaRelatedByAgamaId', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinBidangStudiRelatedByPengawasBidangStudiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('BidangStudiRelatedByPengawasBidangStudiId', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinBidangStudiRelatedByPengawasBidangStudiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('BidangStudiRelatedByPengawasBidangStudiId', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinJenisPtkRelatedByJenisPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('JenisPtkRelatedByJenisPtkId', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinJenisPtkRelatedByJenisPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('JenisPtkRelatedByJenisPtkId', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('KeahlianLaboratoriumRelatedByKeahlianLaboratoriumId', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('KeahlianLaboratoriumRelatedByKeahlianLaboratoriumId', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinKebutuhanKhususRelatedByMampuHandleKk($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByMampuHandleKk', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinKebutuhanKhususRelatedByMampuHandleKk($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByMampuHandleKk', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinLembagaPengangkatRelatedByLembagaPengangkatId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('LembagaPengangkatRelatedByLembagaPengangkatId', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinLembagaPengangkatRelatedByLembagaPengangkatId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('LembagaPengangkatRelatedByLembagaPengangkatId', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinNegaraRelatedByKewarganegaraan($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('NegaraRelatedByKewarganegaraan', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinNegaraRelatedByKewarganegaraan($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('NegaraRelatedByKewarganegaraan', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinPangkatGolonganRelatedByPangkatGolonganId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('PangkatGolonganRelatedByPangkatGolonganId', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinPangkatGolonganRelatedByPangkatGolonganId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('PangkatGolonganRelatedByPangkatGolonganId', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinPekerjaanRelatedByPekerjaanSuamiIstri($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('PekerjaanRelatedByPekerjaanSuamiIstri', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinPekerjaanRelatedByPekerjaanSuamiIstri($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('PekerjaanRelatedByPekerjaanSuamiIstri', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinStatusKepegawaianRelatedByStatusKepegawaianId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('StatusKepegawaianRelatedByStatusKepegawaianId', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinStatusKepegawaianRelatedByStatusKepegawaianId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('StatusKepegawaianRelatedByStatusKepegawaianId', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinStatusKeaktifanPegawaiRelatedByStatusKeaktifanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('StatusKeaktifanPegawaiRelatedByStatusKeaktifanId', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinStatusKeaktifanPegawaiRelatedByStatusKeaktifanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('StatusKeaktifanPegawaiRelatedByStatusKeaktifanId', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinSumberGajiRelatedBySumberGajiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('SumberGajiRelatedBySumberGajiId', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinSumberGajiRelatedBySumberGajiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('SumberGajiRelatedBySumberGajiId', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }

    /**
     * Clears out the collPtksRelatedByKodeWilayah collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return MstWilayah The current object (for fluent API support)
     * @see        addPtksRelatedByKodeWilayah()
     */
    public function clearPtksRelatedByKodeWilayah()
    {
        $this->collPtksRelatedByKodeWilayah = null; // important to set this to null since that means it is uninitialized
        $this->collPtksRelatedByKodeWilayahPartial = null;

        return $this;
    }

    /**
     * reset is the collPtksRelatedByKodeWilayah collection loaded partially
     *
     * @return void
     */
    public function resetPartialPtksRelatedByKodeWilayah($v = true)
    {
        $this->collPtksRelatedByKodeWilayahPartial = $v;
    }

    /**
     * Initializes the collPtksRelatedByKodeWilayah collection.
     *
     * By default this just sets the collPtksRelatedByKodeWilayah collection to an empty array (like clearcollPtksRelatedByKodeWilayah());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPtksRelatedByKodeWilayah($overrideExisting = true)
    {
        if (null !== $this->collPtksRelatedByKodeWilayah && !$overrideExisting) {
            return;
        }
        $this->collPtksRelatedByKodeWilayah = new PropelObjectCollection();
        $this->collPtksRelatedByKodeWilayah->setModel('Ptk');
    }

    /**
     * Gets an array of Ptk objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this MstWilayah is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     * @throws PropelException
     */
    public function getPtksRelatedByKodeWilayah($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPtksRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collPtksRelatedByKodeWilayah || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPtksRelatedByKodeWilayah) {
                // return empty collection
                $this->initPtksRelatedByKodeWilayah();
            } else {
                $collPtksRelatedByKodeWilayah = PtkQuery::create(null, $criteria)
                    ->filterByMstWilayahRelatedByKodeWilayah($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPtksRelatedByKodeWilayahPartial && count($collPtksRelatedByKodeWilayah)) {
                      $this->initPtksRelatedByKodeWilayah(false);

                      foreach($collPtksRelatedByKodeWilayah as $obj) {
                        if (false == $this->collPtksRelatedByKodeWilayah->contains($obj)) {
                          $this->collPtksRelatedByKodeWilayah->append($obj);
                        }
                      }

                      $this->collPtksRelatedByKodeWilayahPartial = true;
                    }

                    $collPtksRelatedByKodeWilayah->getInternalIterator()->rewind();
                    return $collPtksRelatedByKodeWilayah;
                }

                if($partial && $this->collPtksRelatedByKodeWilayah) {
                    foreach($this->collPtksRelatedByKodeWilayah as $obj) {
                        if($obj->isNew()) {
                            $collPtksRelatedByKodeWilayah[] = $obj;
                        }
                    }
                }

                $this->collPtksRelatedByKodeWilayah = $collPtksRelatedByKodeWilayah;
                $this->collPtksRelatedByKodeWilayahPartial = false;
            }
        }

        return $this->collPtksRelatedByKodeWilayah;
    }

    /**
     * Sets a collection of PtkRelatedByKodeWilayah objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $ptksRelatedByKodeWilayah A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setPtksRelatedByKodeWilayah(PropelCollection $ptksRelatedByKodeWilayah, PropelPDO $con = null)
    {
        $ptksRelatedByKodeWilayahToDelete = $this->getPtksRelatedByKodeWilayah(new Criteria(), $con)->diff($ptksRelatedByKodeWilayah);

        $this->ptksRelatedByKodeWilayahScheduledForDeletion = unserialize(serialize($ptksRelatedByKodeWilayahToDelete));

        foreach ($ptksRelatedByKodeWilayahToDelete as $ptkRelatedByKodeWilayahRemoved) {
            $ptkRelatedByKodeWilayahRemoved->setMstWilayahRelatedByKodeWilayah(null);
        }

        $this->collPtksRelatedByKodeWilayah = null;
        foreach ($ptksRelatedByKodeWilayah as $ptkRelatedByKodeWilayah) {
            $this->addPtkRelatedByKodeWilayah($ptkRelatedByKodeWilayah);
        }

        $this->collPtksRelatedByKodeWilayah = $ptksRelatedByKodeWilayah;
        $this->collPtksRelatedByKodeWilayahPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Ptk objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Ptk objects.
     * @throws PropelException
     */
    public function countPtksRelatedByKodeWilayah(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPtksRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collPtksRelatedByKodeWilayah || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPtksRelatedByKodeWilayah) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPtksRelatedByKodeWilayah());
            }
            $query = PtkQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMstWilayahRelatedByKodeWilayah($this)
                ->count($con);
        }

        return count($this->collPtksRelatedByKodeWilayah);
    }

    /**
     * Method called to associate a Ptk object to this object
     * through the Ptk foreign key attribute.
     *
     * @param    Ptk $l Ptk
     * @return MstWilayah The current object (for fluent API support)
     */
    public function addPtkRelatedByKodeWilayah(Ptk $l)
    {
        if ($this->collPtksRelatedByKodeWilayah === null) {
            $this->initPtksRelatedByKodeWilayah();
            $this->collPtksRelatedByKodeWilayahPartial = true;
        }
        if (!in_array($l, $this->collPtksRelatedByKodeWilayah->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPtkRelatedByKodeWilayah($l);
        }

        return $this;
    }

    /**
     * @param	PtkRelatedByKodeWilayah $ptkRelatedByKodeWilayah The ptkRelatedByKodeWilayah object to add.
     */
    protected function doAddPtkRelatedByKodeWilayah($ptkRelatedByKodeWilayah)
    {
        $this->collPtksRelatedByKodeWilayah[]= $ptkRelatedByKodeWilayah;
        $ptkRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah($this);
    }

    /**
     * @param	PtkRelatedByKodeWilayah $ptkRelatedByKodeWilayah The ptkRelatedByKodeWilayah object to remove.
     * @return MstWilayah The current object (for fluent API support)
     */
    public function removePtkRelatedByKodeWilayah($ptkRelatedByKodeWilayah)
    {
        if ($this->getPtksRelatedByKodeWilayah()->contains($ptkRelatedByKodeWilayah)) {
            $this->collPtksRelatedByKodeWilayah->remove($this->collPtksRelatedByKodeWilayah->search($ptkRelatedByKodeWilayah));
            if (null === $this->ptksRelatedByKodeWilayahScheduledForDeletion) {
                $this->ptksRelatedByKodeWilayahScheduledForDeletion = clone $this->collPtksRelatedByKodeWilayah;
                $this->ptksRelatedByKodeWilayahScheduledForDeletion->clear();
            }
            $this->ptksRelatedByKodeWilayahScheduledForDeletion[]= clone $ptkRelatedByKodeWilayah;
            $ptkRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinSekolahRelatedByEntrySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedByEntrySekolahId', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinSekolahRelatedByEntrySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedByEntrySekolahId', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinAgamaRelatedByAgamaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('AgamaRelatedByAgamaId', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinAgamaRelatedByAgamaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('AgamaRelatedByAgamaId', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinBidangStudiRelatedByPengawasBidangStudiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('BidangStudiRelatedByPengawasBidangStudiId', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinBidangStudiRelatedByPengawasBidangStudiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('BidangStudiRelatedByPengawasBidangStudiId', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinJenisPtkRelatedByJenisPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('JenisPtkRelatedByJenisPtkId', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinJenisPtkRelatedByJenisPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('JenisPtkRelatedByJenisPtkId', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('KeahlianLaboratoriumRelatedByKeahlianLaboratoriumId', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('KeahlianLaboratoriumRelatedByKeahlianLaboratoriumId', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinKebutuhanKhususRelatedByMampuHandleKk($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByMampuHandleKk', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinKebutuhanKhususRelatedByMampuHandleKk($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByMampuHandleKk', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinLembagaPengangkatRelatedByLembagaPengangkatId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('LembagaPengangkatRelatedByLembagaPengangkatId', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinLembagaPengangkatRelatedByLembagaPengangkatId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('LembagaPengangkatRelatedByLembagaPengangkatId', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinNegaraRelatedByKewarganegaraan($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('NegaraRelatedByKewarganegaraan', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinNegaraRelatedByKewarganegaraan($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('NegaraRelatedByKewarganegaraan', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinPangkatGolonganRelatedByPangkatGolonganId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('PangkatGolonganRelatedByPangkatGolonganId', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinPangkatGolonganRelatedByPangkatGolonganId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('PangkatGolonganRelatedByPangkatGolonganId', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinPekerjaanRelatedByPekerjaanSuamiIstri($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('PekerjaanRelatedByPekerjaanSuamiIstri', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinPekerjaanRelatedByPekerjaanSuamiIstri($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('PekerjaanRelatedByPekerjaanSuamiIstri', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinStatusKepegawaianRelatedByStatusKepegawaianId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('StatusKepegawaianRelatedByStatusKepegawaianId', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinStatusKepegawaianRelatedByStatusKepegawaianId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('StatusKepegawaianRelatedByStatusKepegawaianId', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinStatusKeaktifanPegawaiRelatedByStatusKeaktifanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('StatusKeaktifanPegawaiRelatedByStatusKeaktifanId', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinStatusKeaktifanPegawaiRelatedByStatusKeaktifanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('StatusKeaktifanPegawaiRelatedByStatusKeaktifanId', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinSumberGajiRelatedBySumberGajiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('SumberGajiRelatedBySumberGajiId', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related PtksRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByKodeWilayahJoinSumberGajiRelatedBySumberGajiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('SumberGajiRelatedBySumberGajiId', $join_behavior);

        return $this->getPtksRelatedByKodeWilayah($query, $con);
    }

    /**
     * Clears out the collWtSrcSyncLogsRelatedByKodeWilayah collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return MstWilayah The current object (for fluent API support)
     * @see        addWtSrcSyncLogsRelatedByKodeWilayah()
     */
    public function clearWtSrcSyncLogsRelatedByKodeWilayah()
    {
        $this->collWtSrcSyncLogsRelatedByKodeWilayah = null; // important to set this to null since that means it is uninitialized
        $this->collWtSrcSyncLogsRelatedByKodeWilayahPartial = null;

        return $this;
    }

    /**
     * reset is the collWtSrcSyncLogsRelatedByKodeWilayah collection loaded partially
     *
     * @return void
     */
    public function resetPartialWtSrcSyncLogsRelatedByKodeWilayah($v = true)
    {
        $this->collWtSrcSyncLogsRelatedByKodeWilayahPartial = $v;
    }

    /**
     * Initializes the collWtSrcSyncLogsRelatedByKodeWilayah collection.
     *
     * By default this just sets the collWtSrcSyncLogsRelatedByKodeWilayah collection to an empty array (like clearcollWtSrcSyncLogsRelatedByKodeWilayah());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initWtSrcSyncLogsRelatedByKodeWilayah($overrideExisting = true)
    {
        if (null !== $this->collWtSrcSyncLogsRelatedByKodeWilayah && !$overrideExisting) {
            return;
        }
        $this->collWtSrcSyncLogsRelatedByKodeWilayah = new PropelObjectCollection();
        $this->collWtSrcSyncLogsRelatedByKodeWilayah->setModel('WtSrcSyncLog');
    }

    /**
     * Gets an array of WtSrcSyncLog objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this MstWilayah is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|WtSrcSyncLog[] List of WtSrcSyncLog objects
     * @throws PropelException
     */
    public function getWtSrcSyncLogsRelatedByKodeWilayah($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collWtSrcSyncLogsRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collWtSrcSyncLogsRelatedByKodeWilayah || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collWtSrcSyncLogsRelatedByKodeWilayah) {
                // return empty collection
                $this->initWtSrcSyncLogsRelatedByKodeWilayah();
            } else {
                $collWtSrcSyncLogsRelatedByKodeWilayah = WtSrcSyncLogQuery::create(null, $criteria)
                    ->filterByMstWilayahRelatedByKodeWilayah($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collWtSrcSyncLogsRelatedByKodeWilayahPartial && count($collWtSrcSyncLogsRelatedByKodeWilayah)) {
                      $this->initWtSrcSyncLogsRelatedByKodeWilayah(false);

                      foreach($collWtSrcSyncLogsRelatedByKodeWilayah as $obj) {
                        if (false == $this->collWtSrcSyncLogsRelatedByKodeWilayah->contains($obj)) {
                          $this->collWtSrcSyncLogsRelatedByKodeWilayah->append($obj);
                        }
                      }

                      $this->collWtSrcSyncLogsRelatedByKodeWilayahPartial = true;
                    }

                    $collWtSrcSyncLogsRelatedByKodeWilayah->getInternalIterator()->rewind();
                    return $collWtSrcSyncLogsRelatedByKodeWilayah;
                }

                if($partial && $this->collWtSrcSyncLogsRelatedByKodeWilayah) {
                    foreach($this->collWtSrcSyncLogsRelatedByKodeWilayah as $obj) {
                        if($obj->isNew()) {
                            $collWtSrcSyncLogsRelatedByKodeWilayah[] = $obj;
                        }
                    }
                }

                $this->collWtSrcSyncLogsRelatedByKodeWilayah = $collWtSrcSyncLogsRelatedByKodeWilayah;
                $this->collWtSrcSyncLogsRelatedByKodeWilayahPartial = false;
            }
        }

        return $this->collWtSrcSyncLogsRelatedByKodeWilayah;
    }

    /**
     * Sets a collection of WtSrcSyncLogRelatedByKodeWilayah objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $wtSrcSyncLogsRelatedByKodeWilayah A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setWtSrcSyncLogsRelatedByKodeWilayah(PropelCollection $wtSrcSyncLogsRelatedByKodeWilayah, PropelPDO $con = null)
    {
        $wtSrcSyncLogsRelatedByKodeWilayahToDelete = $this->getWtSrcSyncLogsRelatedByKodeWilayah(new Criteria(), $con)->diff($wtSrcSyncLogsRelatedByKodeWilayah);

        $this->wtSrcSyncLogsRelatedByKodeWilayahScheduledForDeletion = unserialize(serialize($wtSrcSyncLogsRelatedByKodeWilayahToDelete));

        foreach ($wtSrcSyncLogsRelatedByKodeWilayahToDelete as $wtSrcSyncLogRelatedByKodeWilayahRemoved) {
            $wtSrcSyncLogRelatedByKodeWilayahRemoved->setMstWilayahRelatedByKodeWilayah(null);
        }

        $this->collWtSrcSyncLogsRelatedByKodeWilayah = null;
        foreach ($wtSrcSyncLogsRelatedByKodeWilayah as $wtSrcSyncLogRelatedByKodeWilayah) {
            $this->addWtSrcSyncLogRelatedByKodeWilayah($wtSrcSyncLogRelatedByKodeWilayah);
        }

        $this->collWtSrcSyncLogsRelatedByKodeWilayah = $wtSrcSyncLogsRelatedByKodeWilayah;
        $this->collWtSrcSyncLogsRelatedByKodeWilayahPartial = false;

        return $this;
    }

    /**
     * Returns the number of related WtSrcSyncLog objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related WtSrcSyncLog objects.
     * @throws PropelException
     */
    public function countWtSrcSyncLogsRelatedByKodeWilayah(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collWtSrcSyncLogsRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collWtSrcSyncLogsRelatedByKodeWilayah || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collWtSrcSyncLogsRelatedByKodeWilayah) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getWtSrcSyncLogsRelatedByKodeWilayah());
            }
            $query = WtSrcSyncLogQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMstWilayahRelatedByKodeWilayah($this)
                ->count($con);
        }

        return count($this->collWtSrcSyncLogsRelatedByKodeWilayah);
    }

    /**
     * Method called to associate a WtSrcSyncLog object to this object
     * through the WtSrcSyncLog foreign key attribute.
     *
     * @param    WtSrcSyncLog $l WtSrcSyncLog
     * @return MstWilayah The current object (for fluent API support)
     */
    public function addWtSrcSyncLogRelatedByKodeWilayah(WtSrcSyncLog $l)
    {
        if ($this->collWtSrcSyncLogsRelatedByKodeWilayah === null) {
            $this->initWtSrcSyncLogsRelatedByKodeWilayah();
            $this->collWtSrcSyncLogsRelatedByKodeWilayahPartial = true;
        }
        if (!in_array($l, $this->collWtSrcSyncLogsRelatedByKodeWilayah->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddWtSrcSyncLogRelatedByKodeWilayah($l);
        }

        return $this;
    }

    /**
     * @param	WtSrcSyncLogRelatedByKodeWilayah $wtSrcSyncLogRelatedByKodeWilayah The wtSrcSyncLogRelatedByKodeWilayah object to add.
     */
    protected function doAddWtSrcSyncLogRelatedByKodeWilayah($wtSrcSyncLogRelatedByKodeWilayah)
    {
        $this->collWtSrcSyncLogsRelatedByKodeWilayah[]= $wtSrcSyncLogRelatedByKodeWilayah;
        $wtSrcSyncLogRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah($this);
    }

    /**
     * @param	WtSrcSyncLogRelatedByKodeWilayah $wtSrcSyncLogRelatedByKodeWilayah The wtSrcSyncLogRelatedByKodeWilayah object to remove.
     * @return MstWilayah The current object (for fluent API support)
     */
    public function removeWtSrcSyncLogRelatedByKodeWilayah($wtSrcSyncLogRelatedByKodeWilayah)
    {
        if ($this->getWtSrcSyncLogsRelatedByKodeWilayah()->contains($wtSrcSyncLogRelatedByKodeWilayah)) {
            $this->collWtSrcSyncLogsRelatedByKodeWilayah->remove($this->collWtSrcSyncLogsRelatedByKodeWilayah->search($wtSrcSyncLogRelatedByKodeWilayah));
            if (null === $this->wtSrcSyncLogsRelatedByKodeWilayahScheduledForDeletion) {
                $this->wtSrcSyncLogsRelatedByKodeWilayahScheduledForDeletion = clone $this->collWtSrcSyncLogsRelatedByKodeWilayah;
                $this->wtSrcSyncLogsRelatedByKodeWilayahScheduledForDeletion->clear();
            }
            $this->wtSrcSyncLogsRelatedByKodeWilayahScheduledForDeletion[]= clone $wtSrcSyncLogRelatedByKodeWilayah;
            $wtSrcSyncLogRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related WtSrcSyncLogsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|WtSrcSyncLog[] List of WtSrcSyncLog objects
     */
    public function getWtSrcSyncLogsRelatedByKodeWilayahJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = WtSrcSyncLogQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getWtSrcSyncLogsRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related WtSrcSyncLogsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|WtSrcSyncLog[] List of WtSrcSyncLog objects
     */
    public function getWtSrcSyncLogsRelatedByKodeWilayahJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = WtSrcSyncLogQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getWtSrcSyncLogsRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related WtSrcSyncLogsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|WtSrcSyncLog[] List of WtSrcSyncLog objects
     */
    public function getWtSrcSyncLogsRelatedByKodeWilayahJoinTableSyncRelatedByTableName($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = WtSrcSyncLogQuery::create(null, $criteria);
        $query->joinWith('TableSyncRelatedByTableName', $join_behavior);

        return $this->getWtSrcSyncLogsRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related WtSrcSyncLogsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|WtSrcSyncLog[] List of WtSrcSyncLog objects
     */
    public function getWtSrcSyncLogsRelatedByKodeWilayahJoinTableSyncRelatedByTableName($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = WtSrcSyncLogQuery::create(null, $criteria);
        $query->joinWith('TableSyncRelatedByTableName', $join_behavior);

        return $this->getWtSrcSyncLogsRelatedByKodeWilayah($query, $con);
    }

    /**
     * Clears out the collWtSrcSyncLogsRelatedByKodeWilayah collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return MstWilayah The current object (for fluent API support)
     * @see        addWtSrcSyncLogsRelatedByKodeWilayah()
     */
    public function clearWtSrcSyncLogsRelatedByKodeWilayah()
    {
        $this->collWtSrcSyncLogsRelatedByKodeWilayah = null; // important to set this to null since that means it is uninitialized
        $this->collWtSrcSyncLogsRelatedByKodeWilayahPartial = null;

        return $this;
    }

    /**
     * reset is the collWtSrcSyncLogsRelatedByKodeWilayah collection loaded partially
     *
     * @return void
     */
    public function resetPartialWtSrcSyncLogsRelatedByKodeWilayah($v = true)
    {
        $this->collWtSrcSyncLogsRelatedByKodeWilayahPartial = $v;
    }

    /**
     * Initializes the collWtSrcSyncLogsRelatedByKodeWilayah collection.
     *
     * By default this just sets the collWtSrcSyncLogsRelatedByKodeWilayah collection to an empty array (like clearcollWtSrcSyncLogsRelatedByKodeWilayah());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initWtSrcSyncLogsRelatedByKodeWilayah($overrideExisting = true)
    {
        if (null !== $this->collWtSrcSyncLogsRelatedByKodeWilayah && !$overrideExisting) {
            return;
        }
        $this->collWtSrcSyncLogsRelatedByKodeWilayah = new PropelObjectCollection();
        $this->collWtSrcSyncLogsRelatedByKodeWilayah->setModel('WtSrcSyncLog');
    }

    /**
     * Gets an array of WtSrcSyncLog objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this MstWilayah is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|WtSrcSyncLog[] List of WtSrcSyncLog objects
     * @throws PropelException
     */
    public function getWtSrcSyncLogsRelatedByKodeWilayah($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collWtSrcSyncLogsRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collWtSrcSyncLogsRelatedByKodeWilayah || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collWtSrcSyncLogsRelatedByKodeWilayah) {
                // return empty collection
                $this->initWtSrcSyncLogsRelatedByKodeWilayah();
            } else {
                $collWtSrcSyncLogsRelatedByKodeWilayah = WtSrcSyncLogQuery::create(null, $criteria)
                    ->filterByMstWilayahRelatedByKodeWilayah($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collWtSrcSyncLogsRelatedByKodeWilayahPartial && count($collWtSrcSyncLogsRelatedByKodeWilayah)) {
                      $this->initWtSrcSyncLogsRelatedByKodeWilayah(false);

                      foreach($collWtSrcSyncLogsRelatedByKodeWilayah as $obj) {
                        if (false == $this->collWtSrcSyncLogsRelatedByKodeWilayah->contains($obj)) {
                          $this->collWtSrcSyncLogsRelatedByKodeWilayah->append($obj);
                        }
                      }

                      $this->collWtSrcSyncLogsRelatedByKodeWilayahPartial = true;
                    }

                    $collWtSrcSyncLogsRelatedByKodeWilayah->getInternalIterator()->rewind();
                    return $collWtSrcSyncLogsRelatedByKodeWilayah;
                }

                if($partial && $this->collWtSrcSyncLogsRelatedByKodeWilayah) {
                    foreach($this->collWtSrcSyncLogsRelatedByKodeWilayah as $obj) {
                        if($obj->isNew()) {
                            $collWtSrcSyncLogsRelatedByKodeWilayah[] = $obj;
                        }
                    }
                }

                $this->collWtSrcSyncLogsRelatedByKodeWilayah = $collWtSrcSyncLogsRelatedByKodeWilayah;
                $this->collWtSrcSyncLogsRelatedByKodeWilayahPartial = false;
            }
        }

        return $this->collWtSrcSyncLogsRelatedByKodeWilayah;
    }

    /**
     * Sets a collection of WtSrcSyncLogRelatedByKodeWilayah objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $wtSrcSyncLogsRelatedByKodeWilayah A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setWtSrcSyncLogsRelatedByKodeWilayah(PropelCollection $wtSrcSyncLogsRelatedByKodeWilayah, PropelPDO $con = null)
    {
        $wtSrcSyncLogsRelatedByKodeWilayahToDelete = $this->getWtSrcSyncLogsRelatedByKodeWilayah(new Criteria(), $con)->diff($wtSrcSyncLogsRelatedByKodeWilayah);

        $this->wtSrcSyncLogsRelatedByKodeWilayahScheduledForDeletion = unserialize(serialize($wtSrcSyncLogsRelatedByKodeWilayahToDelete));

        foreach ($wtSrcSyncLogsRelatedByKodeWilayahToDelete as $wtSrcSyncLogRelatedByKodeWilayahRemoved) {
            $wtSrcSyncLogRelatedByKodeWilayahRemoved->setMstWilayahRelatedByKodeWilayah(null);
        }

        $this->collWtSrcSyncLogsRelatedByKodeWilayah = null;
        foreach ($wtSrcSyncLogsRelatedByKodeWilayah as $wtSrcSyncLogRelatedByKodeWilayah) {
            $this->addWtSrcSyncLogRelatedByKodeWilayah($wtSrcSyncLogRelatedByKodeWilayah);
        }

        $this->collWtSrcSyncLogsRelatedByKodeWilayah = $wtSrcSyncLogsRelatedByKodeWilayah;
        $this->collWtSrcSyncLogsRelatedByKodeWilayahPartial = false;

        return $this;
    }

    /**
     * Returns the number of related WtSrcSyncLog objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related WtSrcSyncLog objects.
     * @throws PropelException
     */
    public function countWtSrcSyncLogsRelatedByKodeWilayah(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collWtSrcSyncLogsRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collWtSrcSyncLogsRelatedByKodeWilayah || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collWtSrcSyncLogsRelatedByKodeWilayah) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getWtSrcSyncLogsRelatedByKodeWilayah());
            }
            $query = WtSrcSyncLogQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMstWilayahRelatedByKodeWilayah($this)
                ->count($con);
        }

        return count($this->collWtSrcSyncLogsRelatedByKodeWilayah);
    }

    /**
     * Method called to associate a WtSrcSyncLog object to this object
     * through the WtSrcSyncLog foreign key attribute.
     *
     * @param    WtSrcSyncLog $l WtSrcSyncLog
     * @return MstWilayah The current object (for fluent API support)
     */
    public function addWtSrcSyncLogRelatedByKodeWilayah(WtSrcSyncLog $l)
    {
        if ($this->collWtSrcSyncLogsRelatedByKodeWilayah === null) {
            $this->initWtSrcSyncLogsRelatedByKodeWilayah();
            $this->collWtSrcSyncLogsRelatedByKodeWilayahPartial = true;
        }
        if (!in_array($l, $this->collWtSrcSyncLogsRelatedByKodeWilayah->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddWtSrcSyncLogRelatedByKodeWilayah($l);
        }

        return $this;
    }

    /**
     * @param	WtSrcSyncLogRelatedByKodeWilayah $wtSrcSyncLogRelatedByKodeWilayah The wtSrcSyncLogRelatedByKodeWilayah object to add.
     */
    protected function doAddWtSrcSyncLogRelatedByKodeWilayah($wtSrcSyncLogRelatedByKodeWilayah)
    {
        $this->collWtSrcSyncLogsRelatedByKodeWilayah[]= $wtSrcSyncLogRelatedByKodeWilayah;
        $wtSrcSyncLogRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah($this);
    }

    /**
     * @param	WtSrcSyncLogRelatedByKodeWilayah $wtSrcSyncLogRelatedByKodeWilayah The wtSrcSyncLogRelatedByKodeWilayah object to remove.
     * @return MstWilayah The current object (for fluent API support)
     */
    public function removeWtSrcSyncLogRelatedByKodeWilayah($wtSrcSyncLogRelatedByKodeWilayah)
    {
        if ($this->getWtSrcSyncLogsRelatedByKodeWilayah()->contains($wtSrcSyncLogRelatedByKodeWilayah)) {
            $this->collWtSrcSyncLogsRelatedByKodeWilayah->remove($this->collWtSrcSyncLogsRelatedByKodeWilayah->search($wtSrcSyncLogRelatedByKodeWilayah));
            if (null === $this->wtSrcSyncLogsRelatedByKodeWilayahScheduledForDeletion) {
                $this->wtSrcSyncLogsRelatedByKodeWilayahScheduledForDeletion = clone $this->collWtSrcSyncLogsRelatedByKodeWilayah;
                $this->wtSrcSyncLogsRelatedByKodeWilayahScheduledForDeletion->clear();
            }
            $this->wtSrcSyncLogsRelatedByKodeWilayahScheduledForDeletion[]= clone $wtSrcSyncLogRelatedByKodeWilayah;
            $wtSrcSyncLogRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related WtSrcSyncLogsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|WtSrcSyncLog[] List of WtSrcSyncLog objects
     */
    public function getWtSrcSyncLogsRelatedByKodeWilayahJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = WtSrcSyncLogQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getWtSrcSyncLogsRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related WtSrcSyncLogsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|WtSrcSyncLog[] List of WtSrcSyncLog objects
     */
    public function getWtSrcSyncLogsRelatedByKodeWilayahJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = WtSrcSyncLogQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getWtSrcSyncLogsRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related WtSrcSyncLogsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|WtSrcSyncLog[] List of WtSrcSyncLog objects
     */
    public function getWtSrcSyncLogsRelatedByKodeWilayahJoinTableSyncRelatedByTableName($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = WtSrcSyncLogQuery::create(null, $criteria);
        $query->joinWith('TableSyncRelatedByTableName', $join_behavior);

        return $this->getWtSrcSyncLogsRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related WtSrcSyncLogsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|WtSrcSyncLog[] List of WtSrcSyncLog objects
     */
    public function getWtSrcSyncLogsRelatedByKodeWilayahJoinTableSyncRelatedByTableName($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = WtSrcSyncLogQuery::create(null, $criteria);
        $query->joinWith('TableSyncRelatedByTableName', $join_behavior);

        return $this->getWtSrcSyncLogsRelatedByKodeWilayah($query, $con);
    }

    /**
     * Clears out the collWtDstSyncLogsRelatedByKodeWilayah collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return MstWilayah The current object (for fluent API support)
     * @see        addWtDstSyncLogsRelatedByKodeWilayah()
     */
    public function clearWtDstSyncLogsRelatedByKodeWilayah()
    {
        $this->collWtDstSyncLogsRelatedByKodeWilayah = null; // important to set this to null since that means it is uninitialized
        $this->collWtDstSyncLogsRelatedByKodeWilayahPartial = null;

        return $this;
    }

    /**
     * reset is the collWtDstSyncLogsRelatedByKodeWilayah collection loaded partially
     *
     * @return void
     */
    public function resetPartialWtDstSyncLogsRelatedByKodeWilayah($v = true)
    {
        $this->collWtDstSyncLogsRelatedByKodeWilayahPartial = $v;
    }

    /**
     * Initializes the collWtDstSyncLogsRelatedByKodeWilayah collection.
     *
     * By default this just sets the collWtDstSyncLogsRelatedByKodeWilayah collection to an empty array (like clearcollWtDstSyncLogsRelatedByKodeWilayah());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initWtDstSyncLogsRelatedByKodeWilayah($overrideExisting = true)
    {
        if (null !== $this->collWtDstSyncLogsRelatedByKodeWilayah && !$overrideExisting) {
            return;
        }
        $this->collWtDstSyncLogsRelatedByKodeWilayah = new PropelObjectCollection();
        $this->collWtDstSyncLogsRelatedByKodeWilayah->setModel('WtDstSyncLog');
    }

    /**
     * Gets an array of WtDstSyncLog objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this MstWilayah is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|WtDstSyncLog[] List of WtDstSyncLog objects
     * @throws PropelException
     */
    public function getWtDstSyncLogsRelatedByKodeWilayah($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collWtDstSyncLogsRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collWtDstSyncLogsRelatedByKodeWilayah || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collWtDstSyncLogsRelatedByKodeWilayah) {
                // return empty collection
                $this->initWtDstSyncLogsRelatedByKodeWilayah();
            } else {
                $collWtDstSyncLogsRelatedByKodeWilayah = WtDstSyncLogQuery::create(null, $criteria)
                    ->filterByMstWilayahRelatedByKodeWilayah($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collWtDstSyncLogsRelatedByKodeWilayahPartial && count($collWtDstSyncLogsRelatedByKodeWilayah)) {
                      $this->initWtDstSyncLogsRelatedByKodeWilayah(false);

                      foreach($collWtDstSyncLogsRelatedByKodeWilayah as $obj) {
                        if (false == $this->collWtDstSyncLogsRelatedByKodeWilayah->contains($obj)) {
                          $this->collWtDstSyncLogsRelatedByKodeWilayah->append($obj);
                        }
                      }

                      $this->collWtDstSyncLogsRelatedByKodeWilayahPartial = true;
                    }

                    $collWtDstSyncLogsRelatedByKodeWilayah->getInternalIterator()->rewind();
                    return $collWtDstSyncLogsRelatedByKodeWilayah;
                }

                if($partial && $this->collWtDstSyncLogsRelatedByKodeWilayah) {
                    foreach($this->collWtDstSyncLogsRelatedByKodeWilayah as $obj) {
                        if($obj->isNew()) {
                            $collWtDstSyncLogsRelatedByKodeWilayah[] = $obj;
                        }
                    }
                }

                $this->collWtDstSyncLogsRelatedByKodeWilayah = $collWtDstSyncLogsRelatedByKodeWilayah;
                $this->collWtDstSyncLogsRelatedByKodeWilayahPartial = false;
            }
        }

        return $this->collWtDstSyncLogsRelatedByKodeWilayah;
    }

    /**
     * Sets a collection of WtDstSyncLogRelatedByKodeWilayah objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $wtDstSyncLogsRelatedByKodeWilayah A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setWtDstSyncLogsRelatedByKodeWilayah(PropelCollection $wtDstSyncLogsRelatedByKodeWilayah, PropelPDO $con = null)
    {
        $wtDstSyncLogsRelatedByKodeWilayahToDelete = $this->getWtDstSyncLogsRelatedByKodeWilayah(new Criteria(), $con)->diff($wtDstSyncLogsRelatedByKodeWilayah);

        $this->wtDstSyncLogsRelatedByKodeWilayahScheduledForDeletion = unserialize(serialize($wtDstSyncLogsRelatedByKodeWilayahToDelete));

        foreach ($wtDstSyncLogsRelatedByKodeWilayahToDelete as $wtDstSyncLogRelatedByKodeWilayahRemoved) {
            $wtDstSyncLogRelatedByKodeWilayahRemoved->setMstWilayahRelatedByKodeWilayah(null);
        }

        $this->collWtDstSyncLogsRelatedByKodeWilayah = null;
        foreach ($wtDstSyncLogsRelatedByKodeWilayah as $wtDstSyncLogRelatedByKodeWilayah) {
            $this->addWtDstSyncLogRelatedByKodeWilayah($wtDstSyncLogRelatedByKodeWilayah);
        }

        $this->collWtDstSyncLogsRelatedByKodeWilayah = $wtDstSyncLogsRelatedByKodeWilayah;
        $this->collWtDstSyncLogsRelatedByKodeWilayahPartial = false;

        return $this;
    }

    /**
     * Returns the number of related WtDstSyncLog objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related WtDstSyncLog objects.
     * @throws PropelException
     */
    public function countWtDstSyncLogsRelatedByKodeWilayah(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collWtDstSyncLogsRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collWtDstSyncLogsRelatedByKodeWilayah || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collWtDstSyncLogsRelatedByKodeWilayah) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getWtDstSyncLogsRelatedByKodeWilayah());
            }
            $query = WtDstSyncLogQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMstWilayahRelatedByKodeWilayah($this)
                ->count($con);
        }

        return count($this->collWtDstSyncLogsRelatedByKodeWilayah);
    }

    /**
     * Method called to associate a WtDstSyncLog object to this object
     * through the WtDstSyncLog foreign key attribute.
     *
     * @param    WtDstSyncLog $l WtDstSyncLog
     * @return MstWilayah The current object (for fluent API support)
     */
    public function addWtDstSyncLogRelatedByKodeWilayah(WtDstSyncLog $l)
    {
        if ($this->collWtDstSyncLogsRelatedByKodeWilayah === null) {
            $this->initWtDstSyncLogsRelatedByKodeWilayah();
            $this->collWtDstSyncLogsRelatedByKodeWilayahPartial = true;
        }
        if (!in_array($l, $this->collWtDstSyncLogsRelatedByKodeWilayah->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddWtDstSyncLogRelatedByKodeWilayah($l);
        }

        return $this;
    }

    /**
     * @param	WtDstSyncLogRelatedByKodeWilayah $wtDstSyncLogRelatedByKodeWilayah The wtDstSyncLogRelatedByKodeWilayah object to add.
     */
    protected function doAddWtDstSyncLogRelatedByKodeWilayah($wtDstSyncLogRelatedByKodeWilayah)
    {
        $this->collWtDstSyncLogsRelatedByKodeWilayah[]= $wtDstSyncLogRelatedByKodeWilayah;
        $wtDstSyncLogRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah($this);
    }

    /**
     * @param	WtDstSyncLogRelatedByKodeWilayah $wtDstSyncLogRelatedByKodeWilayah The wtDstSyncLogRelatedByKodeWilayah object to remove.
     * @return MstWilayah The current object (for fluent API support)
     */
    public function removeWtDstSyncLogRelatedByKodeWilayah($wtDstSyncLogRelatedByKodeWilayah)
    {
        if ($this->getWtDstSyncLogsRelatedByKodeWilayah()->contains($wtDstSyncLogRelatedByKodeWilayah)) {
            $this->collWtDstSyncLogsRelatedByKodeWilayah->remove($this->collWtDstSyncLogsRelatedByKodeWilayah->search($wtDstSyncLogRelatedByKodeWilayah));
            if (null === $this->wtDstSyncLogsRelatedByKodeWilayahScheduledForDeletion) {
                $this->wtDstSyncLogsRelatedByKodeWilayahScheduledForDeletion = clone $this->collWtDstSyncLogsRelatedByKodeWilayah;
                $this->wtDstSyncLogsRelatedByKodeWilayahScheduledForDeletion->clear();
            }
            $this->wtDstSyncLogsRelatedByKodeWilayahScheduledForDeletion[]= clone $wtDstSyncLogRelatedByKodeWilayah;
            $wtDstSyncLogRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related WtDstSyncLogsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|WtDstSyncLog[] List of WtDstSyncLog objects
     */
    public function getWtDstSyncLogsRelatedByKodeWilayahJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = WtDstSyncLogQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getWtDstSyncLogsRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related WtDstSyncLogsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|WtDstSyncLog[] List of WtDstSyncLog objects
     */
    public function getWtDstSyncLogsRelatedByKodeWilayahJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = WtDstSyncLogQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getWtDstSyncLogsRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related WtDstSyncLogsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|WtDstSyncLog[] List of WtDstSyncLog objects
     */
    public function getWtDstSyncLogsRelatedByKodeWilayahJoinTableSyncRelatedByTableName($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = WtDstSyncLogQuery::create(null, $criteria);
        $query->joinWith('TableSyncRelatedByTableName', $join_behavior);

        return $this->getWtDstSyncLogsRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related WtDstSyncLogsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|WtDstSyncLog[] List of WtDstSyncLog objects
     */
    public function getWtDstSyncLogsRelatedByKodeWilayahJoinTableSyncRelatedByTableName($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = WtDstSyncLogQuery::create(null, $criteria);
        $query->joinWith('TableSyncRelatedByTableName', $join_behavior);

        return $this->getWtDstSyncLogsRelatedByKodeWilayah($query, $con);
    }

    /**
     * Clears out the collWtDstSyncLogsRelatedByKodeWilayah collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return MstWilayah The current object (for fluent API support)
     * @see        addWtDstSyncLogsRelatedByKodeWilayah()
     */
    public function clearWtDstSyncLogsRelatedByKodeWilayah()
    {
        $this->collWtDstSyncLogsRelatedByKodeWilayah = null; // important to set this to null since that means it is uninitialized
        $this->collWtDstSyncLogsRelatedByKodeWilayahPartial = null;

        return $this;
    }

    /**
     * reset is the collWtDstSyncLogsRelatedByKodeWilayah collection loaded partially
     *
     * @return void
     */
    public function resetPartialWtDstSyncLogsRelatedByKodeWilayah($v = true)
    {
        $this->collWtDstSyncLogsRelatedByKodeWilayahPartial = $v;
    }

    /**
     * Initializes the collWtDstSyncLogsRelatedByKodeWilayah collection.
     *
     * By default this just sets the collWtDstSyncLogsRelatedByKodeWilayah collection to an empty array (like clearcollWtDstSyncLogsRelatedByKodeWilayah());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initWtDstSyncLogsRelatedByKodeWilayah($overrideExisting = true)
    {
        if (null !== $this->collWtDstSyncLogsRelatedByKodeWilayah && !$overrideExisting) {
            return;
        }
        $this->collWtDstSyncLogsRelatedByKodeWilayah = new PropelObjectCollection();
        $this->collWtDstSyncLogsRelatedByKodeWilayah->setModel('WtDstSyncLog');
    }

    /**
     * Gets an array of WtDstSyncLog objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this MstWilayah is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|WtDstSyncLog[] List of WtDstSyncLog objects
     * @throws PropelException
     */
    public function getWtDstSyncLogsRelatedByKodeWilayah($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collWtDstSyncLogsRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collWtDstSyncLogsRelatedByKodeWilayah || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collWtDstSyncLogsRelatedByKodeWilayah) {
                // return empty collection
                $this->initWtDstSyncLogsRelatedByKodeWilayah();
            } else {
                $collWtDstSyncLogsRelatedByKodeWilayah = WtDstSyncLogQuery::create(null, $criteria)
                    ->filterByMstWilayahRelatedByKodeWilayah($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collWtDstSyncLogsRelatedByKodeWilayahPartial && count($collWtDstSyncLogsRelatedByKodeWilayah)) {
                      $this->initWtDstSyncLogsRelatedByKodeWilayah(false);

                      foreach($collWtDstSyncLogsRelatedByKodeWilayah as $obj) {
                        if (false == $this->collWtDstSyncLogsRelatedByKodeWilayah->contains($obj)) {
                          $this->collWtDstSyncLogsRelatedByKodeWilayah->append($obj);
                        }
                      }

                      $this->collWtDstSyncLogsRelatedByKodeWilayahPartial = true;
                    }

                    $collWtDstSyncLogsRelatedByKodeWilayah->getInternalIterator()->rewind();
                    return $collWtDstSyncLogsRelatedByKodeWilayah;
                }

                if($partial && $this->collWtDstSyncLogsRelatedByKodeWilayah) {
                    foreach($this->collWtDstSyncLogsRelatedByKodeWilayah as $obj) {
                        if($obj->isNew()) {
                            $collWtDstSyncLogsRelatedByKodeWilayah[] = $obj;
                        }
                    }
                }

                $this->collWtDstSyncLogsRelatedByKodeWilayah = $collWtDstSyncLogsRelatedByKodeWilayah;
                $this->collWtDstSyncLogsRelatedByKodeWilayahPartial = false;
            }
        }

        return $this->collWtDstSyncLogsRelatedByKodeWilayah;
    }

    /**
     * Sets a collection of WtDstSyncLogRelatedByKodeWilayah objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $wtDstSyncLogsRelatedByKodeWilayah A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setWtDstSyncLogsRelatedByKodeWilayah(PropelCollection $wtDstSyncLogsRelatedByKodeWilayah, PropelPDO $con = null)
    {
        $wtDstSyncLogsRelatedByKodeWilayahToDelete = $this->getWtDstSyncLogsRelatedByKodeWilayah(new Criteria(), $con)->diff($wtDstSyncLogsRelatedByKodeWilayah);

        $this->wtDstSyncLogsRelatedByKodeWilayahScheduledForDeletion = unserialize(serialize($wtDstSyncLogsRelatedByKodeWilayahToDelete));

        foreach ($wtDstSyncLogsRelatedByKodeWilayahToDelete as $wtDstSyncLogRelatedByKodeWilayahRemoved) {
            $wtDstSyncLogRelatedByKodeWilayahRemoved->setMstWilayahRelatedByKodeWilayah(null);
        }

        $this->collWtDstSyncLogsRelatedByKodeWilayah = null;
        foreach ($wtDstSyncLogsRelatedByKodeWilayah as $wtDstSyncLogRelatedByKodeWilayah) {
            $this->addWtDstSyncLogRelatedByKodeWilayah($wtDstSyncLogRelatedByKodeWilayah);
        }

        $this->collWtDstSyncLogsRelatedByKodeWilayah = $wtDstSyncLogsRelatedByKodeWilayah;
        $this->collWtDstSyncLogsRelatedByKodeWilayahPartial = false;

        return $this;
    }

    /**
     * Returns the number of related WtDstSyncLog objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related WtDstSyncLog objects.
     * @throws PropelException
     */
    public function countWtDstSyncLogsRelatedByKodeWilayah(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collWtDstSyncLogsRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collWtDstSyncLogsRelatedByKodeWilayah || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collWtDstSyncLogsRelatedByKodeWilayah) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getWtDstSyncLogsRelatedByKodeWilayah());
            }
            $query = WtDstSyncLogQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMstWilayahRelatedByKodeWilayah($this)
                ->count($con);
        }

        return count($this->collWtDstSyncLogsRelatedByKodeWilayah);
    }

    /**
     * Method called to associate a WtDstSyncLog object to this object
     * through the WtDstSyncLog foreign key attribute.
     *
     * @param    WtDstSyncLog $l WtDstSyncLog
     * @return MstWilayah The current object (for fluent API support)
     */
    public function addWtDstSyncLogRelatedByKodeWilayah(WtDstSyncLog $l)
    {
        if ($this->collWtDstSyncLogsRelatedByKodeWilayah === null) {
            $this->initWtDstSyncLogsRelatedByKodeWilayah();
            $this->collWtDstSyncLogsRelatedByKodeWilayahPartial = true;
        }
        if (!in_array($l, $this->collWtDstSyncLogsRelatedByKodeWilayah->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddWtDstSyncLogRelatedByKodeWilayah($l);
        }

        return $this;
    }

    /**
     * @param	WtDstSyncLogRelatedByKodeWilayah $wtDstSyncLogRelatedByKodeWilayah The wtDstSyncLogRelatedByKodeWilayah object to add.
     */
    protected function doAddWtDstSyncLogRelatedByKodeWilayah($wtDstSyncLogRelatedByKodeWilayah)
    {
        $this->collWtDstSyncLogsRelatedByKodeWilayah[]= $wtDstSyncLogRelatedByKodeWilayah;
        $wtDstSyncLogRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah($this);
    }

    /**
     * @param	WtDstSyncLogRelatedByKodeWilayah $wtDstSyncLogRelatedByKodeWilayah The wtDstSyncLogRelatedByKodeWilayah object to remove.
     * @return MstWilayah The current object (for fluent API support)
     */
    public function removeWtDstSyncLogRelatedByKodeWilayah($wtDstSyncLogRelatedByKodeWilayah)
    {
        if ($this->getWtDstSyncLogsRelatedByKodeWilayah()->contains($wtDstSyncLogRelatedByKodeWilayah)) {
            $this->collWtDstSyncLogsRelatedByKodeWilayah->remove($this->collWtDstSyncLogsRelatedByKodeWilayah->search($wtDstSyncLogRelatedByKodeWilayah));
            if (null === $this->wtDstSyncLogsRelatedByKodeWilayahScheduledForDeletion) {
                $this->wtDstSyncLogsRelatedByKodeWilayahScheduledForDeletion = clone $this->collWtDstSyncLogsRelatedByKodeWilayah;
                $this->wtDstSyncLogsRelatedByKodeWilayahScheduledForDeletion->clear();
            }
            $this->wtDstSyncLogsRelatedByKodeWilayahScheduledForDeletion[]= clone $wtDstSyncLogRelatedByKodeWilayah;
            $wtDstSyncLogRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related WtDstSyncLogsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|WtDstSyncLog[] List of WtDstSyncLog objects
     */
    public function getWtDstSyncLogsRelatedByKodeWilayahJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = WtDstSyncLogQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getWtDstSyncLogsRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related WtDstSyncLogsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|WtDstSyncLog[] List of WtDstSyncLog objects
     */
    public function getWtDstSyncLogsRelatedByKodeWilayahJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = WtDstSyncLogQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getWtDstSyncLogsRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related WtDstSyncLogsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|WtDstSyncLog[] List of WtDstSyncLog objects
     */
    public function getWtDstSyncLogsRelatedByKodeWilayahJoinTableSyncRelatedByTableName($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = WtDstSyncLogQuery::create(null, $criteria);
        $query->joinWith('TableSyncRelatedByTableName', $join_behavior);

        return $this->getWtDstSyncLogsRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related WtDstSyncLogsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|WtDstSyncLog[] List of WtDstSyncLog objects
     */
    public function getWtDstSyncLogsRelatedByKodeWilayahJoinTableSyncRelatedByTableName($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = WtDstSyncLogQuery::create(null, $criteria);
        $query->joinWith('TableSyncRelatedByTableName', $join_behavior);

        return $this->getWtDstSyncLogsRelatedByKodeWilayah($query, $con);
    }

    /**
     * Clears out the collWsyncSessionsRelatedByKodeWilayah collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return MstWilayah The current object (for fluent API support)
     * @see        addWsyncSessionsRelatedByKodeWilayah()
     */
    public function clearWsyncSessionsRelatedByKodeWilayah()
    {
        $this->collWsyncSessionsRelatedByKodeWilayah = null; // important to set this to null since that means it is uninitialized
        $this->collWsyncSessionsRelatedByKodeWilayahPartial = null;

        return $this;
    }

    /**
     * reset is the collWsyncSessionsRelatedByKodeWilayah collection loaded partially
     *
     * @return void
     */
    public function resetPartialWsyncSessionsRelatedByKodeWilayah($v = true)
    {
        $this->collWsyncSessionsRelatedByKodeWilayahPartial = $v;
    }

    /**
     * Initializes the collWsyncSessionsRelatedByKodeWilayah collection.
     *
     * By default this just sets the collWsyncSessionsRelatedByKodeWilayah collection to an empty array (like clearcollWsyncSessionsRelatedByKodeWilayah());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initWsyncSessionsRelatedByKodeWilayah($overrideExisting = true)
    {
        if (null !== $this->collWsyncSessionsRelatedByKodeWilayah && !$overrideExisting) {
            return;
        }
        $this->collWsyncSessionsRelatedByKodeWilayah = new PropelObjectCollection();
        $this->collWsyncSessionsRelatedByKodeWilayah->setModel('WsyncSession');
    }

    /**
     * Gets an array of WsyncSession objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this MstWilayah is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|WsyncSession[] List of WsyncSession objects
     * @throws PropelException
     */
    public function getWsyncSessionsRelatedByKodeWilayah($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collWsyncSessionsRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collWsyncSessionsRelatedByKodeWilayah || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collWsyncSessionsRelatedByKodeWilayah) {
                // return empty collection
                $this->initWsyncSessionsRelatedByKodeWilayah();
            } else {
                $collWsyncSessionsRelatedByKodeWilayah = WsyncSessionQuery::create(null, $criteria)
                    ->filterByMstWilayahRelatedByKodeWilayah($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collWsyncSessionsRelatedByKodeWilayahPartial && count($collWsyncSessionsRelatedByKodeWilayah)) {
                      $this->initWsyncSessionsRelatedByKodeWilayah(false);

                      foreach($collWsyncSessionsRelatedByKodeWilayah as $obj) {
                        if (false == $this->collWsyncSessionsRelatedByKodeWilayah->contains($obj)) {
                          $this->collWsyncSessionsRelatedByKodeWilayah->append($obj);
                        }
                      }

                      $this->collWsyncSessionsRelatedByKodeWilayahPartial = true;
                    }

                    $collWsyncSessionsRelatedByKodeWilayah->getInternalIterator()->rewind();
                    return $collWsyncSessionsRelatedByKodeWilayah;
                }

                if($partial && $this->collWsyncSessionsRelatedByKodeWilayah) {
                    foreach($this->collWsyncSessionsRelatedByKodeWilayah as $obj) {
                        if($obj->isNew()) {
                            $collWsyncSessionsRelatedByKodeWilayah[] = $obj;
                        }
                    }
                }

                $this->collWsyncSessionsRelatedByKodeWilayah = $collWsyncSessionsRelatedByKodeWilayah;
                $this->collWsyncSessionsRelatedByKodeWilayahPartial = false;
            }
        }

        return $this->collWsyncSessionsRelatedByKodeWilayah;
    }

    /**
     * Sets a collection of WsyncSessionRelatedByKodeWilayah objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $wsyncSessionsRelatedByKodeWilayah A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setWsyncSessionsRelatedByKodeWilayah(PropelCollection $wsyncSessionsRelatedByKodeWilayah, PropelPDO $con = null)
    {
        $wsyncSessionsRelatedByKodeWilayahToDelete = $this->getWsyncSessionsRelatedByKodeWilayah(new Criteria(), $con)->diff($wsyncSessionsRelatedByKodeWilayah);

        $this->wsyncSessionsRelatedByKodeWilayahScheduledForDeletion = unserialize(serialize($wsyncSessionsRelatedByKodeWilayahToDelete));

        foreach ($wsyncSessionsRelatedByKodeWilayahToDelete as $wsyncSessionRelatedByKodeWilayahRemoved) {
            $wsyncSessionRelatedByKodeWilayahRemoved->setMstWilayahRelatedByKodeWilayah(null);
        }

        $this->collWsyncSessionsRelatedByKodeWilayah = null;
        foreach ($wsyncSessionsRelatedByKodeWilayah as $wsyncSessionRelatedByKodeWilayah) {
            $this->addWsyncSessionRelatedByKodeWilayah($wsyncSessionRelatedByKodeWilayah);
        }

        $this->collWsyncSessionsRelatedByKodeWilayah = $wsyncSessionsRelatedByKodeWilayah;
        $this->collWsyncSessionsRelatedByKodeWilayahPartial = false;

        return $this;
    }

    /**
     * Returns the number of related WsyncSession objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related WsyncSession objects.
     * @throws PropelException
     */
    public function countWsyncSessionsRelatedByKodeWilayah(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collWsyncSessionsRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collWsyncSessionsRelatedByKodeWilayah || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collWsyncSessionsRelatedByKodeWilayah) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getWsyncSessionsRelatedByKodeWilayah());
            }
            $query = WsyncSessionQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMstWilayahRelatedByKodeWilayah($this)
                ->count($con);
        }

        return count($this->collWsyncSessionsRelatedByKodeWilayah);
    }

    /**
     * Method called to associate a WsyncSession object to this object
     * through the WsyncSession foreign key attribute.
     *
     * @param    WsyncSession $l WsyncSession
     * @return MstWilayah The current object (for fluent API support)
     */
    public function addWsyncSessionRelatedByKodeWilayah(WsyncSession $l)
    {
        if ($this->collWsyncSessionsRelatedByKodeWilayah === null) {
            $this->initWsyncSessionsRelatedByKodeWilayah();
            $this->collWsyncSessionsRelatedByKodeWilayahPartial = true;
        }
        if (!in_array($l, $this->collWsyncSessionsRelatedByKodeWilayah->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddWsyncSessionRelatedByKodeWilayah($l);
        }

        return $this;
    }

    /**
     * @param	WsyncSessionRelatedByKodeWilayah $wsyncSessionRelatedByKodeWilayah The wsyncSessionRelatedByKodeWilayah object to add.
     */
    protected function doAddWsyncSessionRelatedByKodeWilayah($wsyncSessionRelatedByKodeWilayah)
    {
        $this->collWsyncSessionsRelatedByKodeWilayah[]= $wsyncSessionRelatedByKodeWilayah;
        $wsyncSessionRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah($this);
    }

    /**
     * @param	WsyncSessionRelatedByKodeWilayah $wsyncSessionRelatedByKodeWilayah The wsyncSessionRelatedByKodeWilayah object to remove.
     * @return MstWilayah The current object (for fluent API support)
     */
    public function removeWsyncSessionRelatedByKodeWilayah($wsyncSessionRelatedByKodeWilayah)
    {
        if ($this->getWsyncSessionsRelatedByKodeWilayah()->contains($wsyncSessionRelatedByKodeWilayah)) {
            $this->collWsyncSessionsRelatedByKodeWilayah->remove($this->collWsyncSessionsRelatedByKodeWilayah->search($wsyncSessionRelatedByKodeWilayah));
            if (null === $this->wsyncSessionsRelatedByKodeWilayahScheduledForDeletion) {
                $this->wsyncSessionsRelatedByKodeWilayahScheduledForDeletion = clone $this->collWsyncSessionsRelatedByKodeWilayah;
                $this->wsyncSessionsRelatedByKodeWilayahScheduledForDeletion->clear();
            }
            $this->wsyncSessionsRelatedByKodeWilayahScheduledForDeletion[]= clone $wsyncSessionRelatedByKodeWilayah;
            $wsyncSessionRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah(null);
        }

        return $this;
    }

    /**
     * Clears out the collWsyncSessionsRelatedByKodeWilayah collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return MstWilayah The current object (for fluent API support)
     * @see        addWsyncSessionsRelatedByKodeWilayah()
     */
    public function clearWsyncSessionsRelatedByKodeWilayah()
    {
        $this->collWsyncSessionsRelatedByKodeWilayah = null; // important to set this to null since that means it is uninitialized
        $this->collWsyncSessionsRelatedByKodeWilayahPartial = null;

        return $this;
    }

    /**
     * reset is the collWsyncSessionsRelatedByKodeWilayah collection loaded partially
     *
     * @return void
     */
    public function resetPartialWsyncSessionsRelatedByKodeWilayah($v = true)
    {
        $this->collWsyncSessionsRelatedByKodeWilayahPartial = $v;
    }

    /**
     * Initializes the collWsyncSessionsRelatedByKodeWilayah collection.
     *
     * By default this just sets the collWsyncSessionsRelatedByKodeWilayah collection to an empty array (like clearcollWsyncSessionsRelatedByKodeWilayah());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initWsyncSessionsRelatedByKodeWilayah($overrideExisting = true)
    {
        if (null !== $this->collWsyncSessionsRelatedByKodeWilayah && !$overrideExisting) {
            return;
        }
        $this->collWsyncSessionsRelatedByKodeWilayah = new PropelObjectCollection();
        $this->collWsyncSessionsRelatedByKodeWilayah->setModel('WsyncSession');
    }

    /**
     * Gets an array of WsyncSession objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this MstWilayah is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|WsyncSession[] List of WsyncSession objects
     * @throws PropelException
     */
    public function getWsyncSessionsRelatedByKodeWilayah($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collWsyncSessionsRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collWsyncSessionsRelatedByKodeWilayah || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collWsyncSessionsRelatedByKodeWilayah) {
                // return empty collection
                $this->initWsyncSessionsRelatedByKodeWilayah();
            } else {
                $collWsyncSessionsRelatedByKodeWilayah = WsyncSessionQuery::create(null, $criteria)
                    ->filterByMstWilayahRelatedByKodeWilayah($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collWsyncSessionsRelatedByKodeWilayahPartial && count($collWsyncSessionsRelatedByKodeWilayah)) {
                      $this->initWsyncSessionsRelatedByKodeWilayah(false);

                      foreach($collWsyncSessionsRelatedByKodeWilayah as $obj) {
                        if (false == $this->collWsyncSessionsRelatedByKodeWilayah->contains($obj)) {
                          $this->collWsyncSessionsRelatedByKodeWilayah->append($obj);
                        }
                      }

                      $this->collWsyncSessionsRelatedByKodeWilayahPartial = true;
                    }

                    $collWsyncSessionsRelatedByKodeWilayah->getInternalIterator()->rewind();
                    return $collWsyncSessionsRelatedByKodeWilayah;
                }

                if($partial && $this->collWsyncSessionsRelatedByKodeWilayah) {
                    foreach($this->collWsyncSessionsRelatedByKodeWilayah as $obj) {
                        if($obj->isNew()) {
                            $collWsyncSessionsRelatedByKodeWilayah[] = $obj;
                        }
                    }
                }

                $this->collWsyncSessionsRelatedByKodeWilayah = $collWsyncSessionsRelatedByKodeWilayah;
                $this->collWsyncSessionsRelatedByKodeWilayahPartial = false;
            }
        }

        return $this->collWsyncSessionsRelatedByKodeWilayah;
    }

    /**
     * Sets a collection of WsyncSessionRelatedByKodeWilayah objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $wsyncSessionsRelatedByKodeWilayah A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setWsyncSessionsRelatedByKodeWilayah(PropelCollection $wsyncSessionsRelatedByKodeWilayah, PropelPDO $con = null)
    {
        $wsyncSessionsRelatedByKodeWilayahToDelete = $this->getWsyncSessionsRelatedByKodeWilayah(new Criteria(), $con)->diff($wsyncSessionsRelatedByKodeWilayah);

        $this->wsyncSessionsRelatedByKodeWilayahScheduledForDeletion = unserialize(serialize($wsyncSessionsRelatedByKodeWilayahToDelete));

        foreach ($wsyncSessionsRelatedByKodeWilayahToDelete as $wsyncSessionRelatedByKodeWilayahRemoved) {
            $wsyncSessionRelatedByKodeWilayahRemoved->setMstWilayahRelatedByKodeWilayah(null);
        }

        $this->collWsyncSessionsRelatedByKodeWilayah = null;
        foreach ($wsyncSessionsRelatedByKodeWilayah as $wsyncSessionRelatedByKodeWilayah) {
            $this->addWsyncSessionRelatedByKodeWilayah($wsyncSessionRelatedByKodeWilayah);
        }

        $this->collWsyncSessionsRelatedByKodeWilayah = $wsyncSessionsRelatedByKodeWilayah;
        $this->collWsyncSessionsRelatedByKodeWilayahPartial = false;

        return $this;
    }

    /**
     * Returns the number of related WsyncSession objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related WsyncSession objects.
     * @throws PropelException
     */
    public function countWsyncSessionsRelatedByKodeWilayah(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collWsyncSessionsRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collWsyncSessionsRelatedByKodeWilayah || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collWsyncSessionsRelatedByKodeWilayah) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getWsyncSessionsRelatedByKodeWilayah());
            }
            $query = WsyncSessionQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMstWilayahRelatedByKodeWilayah($this)
                ->count($con);
        }

        return count($this->collWsyncSessionsRelatedByKodeWilayah);
    }

    /**
     * Method called to associate a WsyncSession object to this object
     * through the WsyncSession foreign key attribute.
     *
     * @param    WsyncSession $l WsyncSession
     * @return MstWilayah The current object (for fluent API support)
     */
    public function addWsyncSessionRelatedByKodeWilayah(WsyncSession $l)
    {
        if ($this->collWsyncSessionsRelatedByKodeWilayah === null) {
            $this->initWsyncSessionsRelatedByKodeWilayah();
            $this->collWsyncSessionsRelatedByKodeWilayahPartial = true;
        }
        if (!in_array($l, $this->collWsyncSessionsRelatedByKodeWilayah->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddWsyncSessionRelatedByKodeWilayah($l);
        }

        return $this;
    }

    /**
     * @param	WsyncSessionRelatedByKodeWilayah $wsyncSessionRelatedByKodeWilayah The wsyncSessionRelatedByKodeWilayah object to add.
     */
    protected function doAddWsyncSessionRelatedByKodeWilayah($wsyncSessionRelatedByKodeWilayah)
    {
        $this->collWsyncSessionsRelatedByKodeWilayah[]= $wsyncSessionRelatedByKodeWilayah;
        $wsyncSessionRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah($this);
    }

    /**
     * @param	WsyncSessionRelatedByKodeWilayah $wsyncSessionRelatedByKodeWilayah The wsyncSessionRelatedByKodeWilayah object to remove.
     * @return MstWilayah The current object (for fluent API support)
     */
    public function removeWsyncSessionRelatedByKodeWilayah($wsyncSessionRelatedByKodeWilayah)
    {
        if ($this->getWsyncSessionsRelatedByKodeWilayah()->contains($wsyncSessionRelatedByKodeWilayah)) {
            $this->collWsyncSessionsRelatedByKodeWilayah->remove($this->collWsyncSessionsRelatedByKodeWilayah->search($wsyncSessionRelatedByKodeWilayah));
            if (null === $this->wsyncSessionsRelatedByKodeWilayahScheduledForDeletion) {
                $this->wsyncSessionsRelatedByKodeWilayahScheduledForDeletion = clone $this->collWsyncSessionsRelatedByKodeWilayah;
                $this->wsyncSessionsRelatedByKodeWilayahScheduledForDeletion->clear();
            }
            $this->wsyncSessionsRelatedByKodeWilayahScheduledForDeletion[]= clone $wsyncSessionRelatedByKodeWilayah;
            $wsyncSessionRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah(null);
        }

        return $this;
    }

    /**
     * Clears out the collWsrcSyncLogsRelatedByKodeWilayah collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return MstWilayah The current object (for fluent API support)
     * @see        addWsrcSyncLogsRelatedByKodeWilayah()
     */
    public function clearWsrcSyncLogsRelatedByKodeWilayah()
    {
        $this->collWsrcSyncLogsRelatedByKodeWilayah = null; // important to set this to null since that means it is uninitialized
        $this->collWsrcSyncLogsRelatedByKodeWilayahPartial = null;

        return $this;
    }

    /**
     * reset is the collWsrcSyncLogsRelatedByKodeWilayah collection loaded partially
     *
     * @return void
     */
    public function resetPartialWsrcSyncLogsRelatedByKodeWilayah($v = true)
    {
        $this->collWsrcSyncLogsRelatedByKodeWilayahPartial = $v;
    }

    /**
     * Initializes the collWsrcSyncLogsRelatedByKodeWilayah collection.
     *
     * By default this just sets the collWsrcSyncLogsRelatedByKodeWilayah collection to an empty array (like clearcollWsrcSyncLogsRelatedByKodeWilayah());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initWsrcSyncLogsRelatedByKodeWilayah($overrideExisting = true)
    {
        if (null !== $this->collWsrcSyncLogsRelatedByKodeWilayah && !$overrideExisting) {
            return;
        }
        $this->collWsrcSyncLogsRelatedByKodeWilayah = new PropelObjectCollection();
        $this->collWsrcSyncLogsRelatedByKodeWilayah->setModel('WsrcSyncLog');
    }

    /**
     * Gets an array of WsrcSyncLog objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this MstWilayah is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|WsrcSyncLog[] List of WsrcSyncLog objects
     * @throws PropelException
     */
    public function getWsrcSyncLogsRelatedByKodeWilayah($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collWsrcSyncLogsRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collWsrcSyncLogsRelatedByKodeWilayah || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collWsrcSyncLogsRelatedByKodeWilayah) {
                // return empty collection
                $this->initWsrcSyncLogsRelatedByKodeWilayah();
            } else {
                $collWsrcSyncLogsRelatedByKodeWilayah = WsrcSyncLogQuery::create(null, $criteria)
                    ->filterByMstWilayahRelatedByKodeWilayah($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collWsrcSyncLogsRelatedByKodeWilayahPartial && count($collWsrcSyncLogsRelatedByKodeWilayah)) {
                      $this->initWsrcSyncLogsRelatedByKodeWilayah(false);

                      foreach($collWsrcSyncLogsRelatedByKodeWilayah as $obj) {
                        if (false == $this->collWsrcSyncLogsRelatedByKodeWilayah->contains($obj)) {
                          $this->collWsrcSyncLogsRelatedByKodeWilayah->append($obj);
                        }
                      }

                      $this->collWsrcSyncLogsRelatedByKodeWilayahPartial = true;
                    }

                    $collWsrcSyncLogsRelatedByKodeWilayah->getInternalIterator()->rewind();
                    return $collWsrcSyncLogsRelatedByKodeWilayah;
                }

                if($partial && $this->collWsrcSyncLogsRelatedByKodeWilayah) {
                    foreach($this->collWsrcSyncLogsRelatedByKodeWilayah as $obj) {
                        if($obj->isNew()) {
                            $collWsrcSyncLogsRelatedByKodeWilayah[] = $obj;
                        }
                    }
                }

                $this->collWsrcSyncLogsRelatedByKodeWilayah = $collWsrcSyncLogsRelatedByKodeWilayah;
                $this->collWsrcSyncLogsRelatedByKodeWilayahPartial = false;
            }
        }

        return $this->collWsrcSyncLogsRelatedByKodeWilayah;
    }

    /**
     * Sets a collection of WsrcSyncLogRelatedByKodeWilayah objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $wsrcSyncLogsRelatedByKodeWilayah A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setWsrcSyncLogsRelatedByKodeWilayah(PropelCollection $wsrcSyncLogsRelatedByKodeWilayah, PropelPDO $con = null)
    {
        $wsrcSyncLogsRelatedByKodeWilayahToDelete = $this->getWsrcSyncLogsRelatedByKodeWilayah(new Criteria(), $con)->diff($wsrcSyncLogsRelatedByKodeWilayah);

        $this->wsrcSyncLogsRelatedByKodeWilayahScheduledForDeletion = unserialize(serialize($wsrcSyncLogsRelatedByKodeWilayahToDelete));

        foreach ($wsrcSyncLogsRelatedByKodeWilayahToDelete as $wsrcSyncLogRelatedByKodeWilayahRemoved) {
            $wsrcSyncLogRelatedByKodeWilayahRemoved->setMstWilayahRelatedByKodeWilayah(null);
        }

        $this->collWsrcSyncLogsRelatedByKodeWilayah = null;
        foreach ($wsrcSyncLogsRelatedByKodeWilayah as $wsrcSyncLogRelatedByKodeWilayah) {
            $this->addWsrcSyncLogRelatedByKodeWilayah($wsrcSyncLogRelatedByKodeWilayah);
        }

        $this->collWsrcSyncLogsRelatedByKodeWilayah = $wsrcSyncLogsRelatedByKodeWilayah;
        $this->collWsrcSyncLogsRelatedByKodeWilayahPartial = false;

        return $this;
    }

    /**
     * Returns the number of related WsrcSyncLog objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related WsrcSyncLog objects.
     * @throws PropelException
     */
    public function countWsrcSyncLogsRelatedByKodeWilayah(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collWsrcSyncLogsRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collWsrcSyncLogsRelatedByKodeWilayah || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collWsrcSyncLogsRelatedByKodeWilayah) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getWsrcSyncLogsRelatedByKodeWilayah());
            }
            $query = WsrcSyncLogQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMstWilayahRelatedByKodeWilayah($this)
                ->count($con);
        }

        return count($this->collWsrcSyncLogsRelatedByKodeWilayah);
    }

    /**
     * Method called to associate a WsrcSyncLog object to this object
     * through the WsrcSyncLog foreign key attribute.
     *
     * @param    WsrcSyncLog $l WsrcSyncLog
     * @return MstWilayah The current object (for fluent API support)
     */
    public function addWsrcSyncLogRelatedByKodeWilayah(WsrcSyncLog $l)
    {
        if ($this->collWsrcSyncLogsRelatedByKodeWilayah === null) {
            $this->initWsrcSyncLogsRelatedByKodeWilayah();
            $this->collWsrcSyncLogsRelatedByKodeWilayahPartial = true;
        }
        if (!in_array($l, $this->collWsrcSyncLogsRelatedByKodeWilayah->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddWsrcSyncLogRelatedByKodeWilayah($l);
        }

        return $this;
    }

    /**
     * @param	WsrcSyncLogRelatedByKodeWilayah $wsrcSyncLogRelatedByKodeWilayah The wsrcSyncLogRelatedByKodeWilayah object to add.
     */
    protected function doAddWsrcSyncLogRelatedByKodeWilayah($wsrcSyncLogRelatedByKodeWilayah)
    {
        $this->collWsrcSyncLogsRelatedByKodeWilayah[]= $wsrcSyncLogRelatedByKodeWilayah;
        $wsrcSyncLogRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah($this);
    }

    /**
     * @param	WsrcSyncLogRelatedByKodeWilayah $wsrcSyncLogRelatedByKodeWilayah The wsrcSyncLogRelatedByKodeWilayah object to remove.
     * @return MstWilayah The current object (for fluent API support)
     */
    public function removeWsrcSyncLogRelatedByKodeWilayah($wsrcSyncLogRelatedByKodeWilayah)
    {
        if ($this->getWsrcSyncLogsRelatedByKodeWilayah()->contains($wsrcSyncLogRelatedByKodeWilayah)) {
            $this->collWsrcSyncLogsRelatedByKodeWilayah->remove($this->collWsrcSyncLogsRelatedByKodeWilayah->search($wsrcSyncLogRelatedByKodeWilayah));
            if (null === $this->wsrcSyncLogsRelatedByKodeWilayahScheduledForDeletion) {
                $this->wsrcSyncLogsRelatedByKodeWilayahScheduledForDeletion = clone $this->collWsrcSyncLogsRelatedByKodeWilayah;
                $this->wsrcSyncLogsRelatedByKodeWilayahScheduledForDeletion->clear();
            }
            $this->wsrcSyncLogsRelatedByKodeWilayahScheduledForDeletion[]= clone $wsrcSyncLogRelatedByKodeWilayah;
            $wsrcSyncLogRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related WsrcSyncLogsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|WsrcSyncLog[] List of WsrcSyncLog objects
     */
    public function getWsrcSyncLogsRelatedByKodeWilayahJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = WsrcSyncLogQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getWsrcSyncLogsRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related WsrcSyncLogsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|WsrcSyncLog[] List of WsrcSyncLog objects
     */
    public function getWsrcSyncLogsRelatedByKodeWilayahJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = WsrcSyncLogQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getWsrcSyncLogsRelatedByKodeWilayah($query, $con);
    }

    /**
     * Clears out the collWsrcSyncLogsRelatedByKodeWilayah collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return MstWilayah The current object (for fluent API support)
     * @see        addWsrcSyncLogsRelatedByKodeWilayah()
     */
    public function clearWsrcSyncLogsRelatedByKodeWilayah()
    {
        $this->collWsrcSyncLogsRelatedByKodeWilayah = null; // important to set this to null since that means it is uninitialized
        $this->collWsrcSyncLogsRelatedByKodeWilayahPartial = null;

        return $this;
    }

    /**
     * reset is the collWsrcSyncLogsRelatedByKodeWilayah collection loaded partially
     *
     * @return void
     */
    public function resetPartialWsrcSyncLogsRelatedByKodeWilayah($v = true)
    {
        $this->collWsrcSyncLogsRelatedByKodeWilayahPartial = $v;
    }

    /**
     * Initializes the collWsrcSyncLogsRelatedByKodeWilayah collection.
     *
     * By default this just sets the collWsrcSyncLogsRelatedByKodeWilayah collection to an empty array (like clearcollWsrcSyncLogsRelatedByKodeWilayah());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initWsrcSyncLogsRelatedByKodeWilayah($overrideExisting = true)
    {
        if (null !== $this->collWsrcSyncLogsRelatedByKodeWilayah && !$overrideExisting) {
            return;
        }
        $this->collWsrcSyncLogsRelatedByKodeWilayah = new PropelObjectCollection();
        $this->collWsrcSyncLogsRelatedByKodeWilayah->setModel('WsrcSyncLog');
    }

    /**
     * Gets an array of WsrcSyncLog objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this MstWilayah is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|WsrcSyncLog[] List of WsrcSyncLog objects
     * @throws PropelException
     */
    public function getWsrcSyncLogsRelatedByKodeWilayah($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collWsrcSyncLogsRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collWsrcSyncLogsRelatedByKodeWilayah || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collWsrcSyncLogsRelatedByKodeWilayah) {
                // return empty collection
                $this->initWsrcSyncLogsRelatedByKodeWilayah();
            } else {
                $collWsrcSyncLogsRelatedByKodeWilayah = WsrcSyncLogQuery::create(null, $criteria)
                    ->filterByMstWilayahRelatedByKodeWilayah($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collWsrcSyncLogsRelatedByKodeWilayahPartial && count($collWsrcSyncLogsRelatedByKodeWilayah)) {
                      $this->initWsrcSyncLogsRelatedByKodeWilayah(false);

                      foreach($collWsrcSyncLogsRelatedByKodeWilayah as $obj) {
                        if (false == $this->collWsrcSyncLogsRelatedByKodeWilayah->contains($obj)) {
                          $this->collWsrcSyncLogsRelatedByKodeWilayah->append($obj);
                        }
                      }

                      $this->collWsrcSyncLogsRelatedByKodeWilayahPartial = true;
                    }

                    $collWsrcSyncLogsRelatedByKodeWilayah->getInternalIterator()->rewind();
                    return $collWsrcSyncLogsRelatedByKodeWilayah;
                }

                if($partial && $this->collWsrcSyncLogsRelatedByKodeWilayah) {
                    foreach($this->collWsrcSyncLogsRelatedByKodeWilayah as $obj) {
                        if($obj->isNew()) {
                            $collWsrcSyncLogsRelatedByKodeWilayah[] = $obj;
                        }
                    }
                }

                $this->collWsrcSyncLogsRelatedByKodeWilayah = $collWsrcSyncLogsRelatedByKodeWilayah;
                $this->collWsrcSyncLogsRelatedByKodeWilayahPartial = false;
            }
        }

        return $this->collWsrcSyncLogsRelatedByKodeWilayah;
    }

    /**
     * Sets a collection of WsrcSyncLogRelatedByKodeWilayah objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $wsrcSyncLogsRelatedByKodeWilayah A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setWsrcSyncLogsRelatedByKodeWilayah(PropelCollection $wsrcSyncLogsRelatedByKodeWilayah, PropelPDO $con = null)
    {
        $wsrcSyncLogsRelatedByKodeWilayahToDelete = $this->getWsrcSyncLogsRelatedByKodeWilayah(new Criteria(), $con)->diff($wsrcSyncLogsRelatedByKodeWilayah);

        $this->wsrcSyncLogsRelatedByKodeWilayahScheduledForDeletion = unserialize(serialize($wsrcSyncLogsRelatedByKodeWilayahToDelete));

        foreach ($wsrcSyncLogsRelatedByKodeWilayahToDelete as $wsrcSyncLogRelatedByKodeWilayahRemoved) {
            $wsrcSyncLogRelatedByKodeWilayahRemoved->setMstWilayahRelatedByKodeWilayah(null);
        }

        $this->collWsrcSyncLogsRelatedByKodeWilayah = null;
        foreach ($wsrcSyncLogsRelatedByKodeWilayah as $wsrcSyncLogRelatedByKodeWilayah) {
            $this->addWsrcSyncLogRelatedByKodeWilayah($wsrcSyncLogRelatedByKodeWilayah);
        }

        $this->collWsrcSyncLogsRelatedByKodeWilayah = $wsrcSyncLogsRelatedByKodeWilayah;
        $this->collWsrcSyncLogsRelatedByKodeWilayahPartial = false;

        return $this;
    }

    /**
     * Returns the number of related WsrcSyncLog objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related WsrcSyncLog objects.
     * @throws PropelException
     */
    public function countWsrcSyncLogsRelatedByKodeWilayah(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collWsrcSyncLogsRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collWsrcSyncLogsRelatedByKodeWilayah || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collWsrcSyncLogsRelatedByKodeWilayah) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getWsrcSyncLogsRelatedByKodeWilayah());
            }
            $query = WsrcSyncLogQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMstWilayahRelatedByKodeWilayah($this)
                ->count($con);
        }

        return count($this->collWsrcSyncLogsRelatedByKodeWilayah);
    }

    /**
     * Method called to associate a WsrcSyncLog object to this object
     * through the WsrcSyncLog foreign key attribute.
     *
     * @param    WsrcSyncLog $l WsrcSyncLog
     * @return MstWilayah The current object (for fluent API support)
     */
    public function addWsrcSyncLogRelatedByKodeWilayah(WsrcSyncLog $l)
    {
        if ($this->collWsrcSyncLogsRelatedByKodeWilayah === null) {
            $this->initWsrcSyncLogsRelatedByKodeWilayah();
            $this->collWsrcSyncLogsRelatedByKodeWilayahPartial = true;
        }
        if (!in_array($l, $this->collWsrcSyncLogsRelatedByKodeWilayah->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddWsrcSyncLogRelatedByKodeWilayah($l);
        }

        return $this;
    }

    /**
     * @param	WsrcSyncLogRelatedByKodeWilayah $wsrcSyncLogRelatedByKodeWilayah The wsrcSyncLogRelatedByKodeWilayah object to add.
     */
    protected function doAddWsrcSyncLogRelatedByKodeWilayah($wsrcSyncLogRelatedByKodeWilayah)
    {
        $this->collWsrcSyncLogsRelatedByKodeWilayah[]= $wsrcSyncLogRelatedByKodeWilayah;
        $wsrcSyncLogRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah($this);
    }

    /**
     * @param	WsrcSyncLogRelatedByKodeWilayah $wsrcSyncLogRelatedByKodeWilayah The wsrcSyncLogRelatedByKodeWilayah object to remove.
     * @return MstWilayah The current object (for fluent API support)
     */
    public function removeWsrcSyncLogRelatedByKodeWilayah($wsrcSyncLogRelatedByKodeWilayah)
    {
        if ($this->getWsrcSyncLogsRelatedByKodeWilayah()->contains($wsrcSyncLogRelatedByKodeWilayah)) {
            $this->collWsrcSyncLogsRelatedByKodeWilayah->remove($this->collWsrcSyncLogsRelatedByKodeWilayah->search($wsrcSyncLogRelatedByKodeWilayah));
            if (null === $this->wsrcSyncLogsRelatedByKodeWilayahScheduledForDeletion) {
                $this->wsrcSyncLogsRelatedByKodeWilayahScheduledForDeletion = clone $this->collWsrcSyncLogsRelatedByKodeWilayah;
                $this->wsrcSyncLogsRelatedByKodeWilayahScheduledForDeletion->clear();
            }
            $this->wsrcSyncLogsRelatedByKodeWilayahScheduledForDeletion[]= clone $wsrcSyncLogRelatedByKodeWilayah;
            $wsrcSyncLogRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related WsrcSyncLogsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|WsrcSyncLog[] List of WsrcSyncLog objects
     */
    public function getWsrcSyncLogsRelatedByKodeWilayahJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = WsrcSyncLogQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getWsrcSyncLogsRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related WsrcSyncLogsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|WsrcSyncLog[] List of WsrcSyncLog objects
     */
    public function getWsrcSyncLogsRelatedByKodeWilayahJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = WsrcSyncLogQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getWsrcSyncLogsRelatedByKodeWilayah($query, $con);
    }

    /**
     * Clears out the collWdstSyncLogsRelatedByKodeWilayah collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return MstWilayah The current object (for fluent API support)
     * @see        addWdstSyncLogsRelatedByKodeWilayah()
     */
    public function clearWdstSyncLogsRelatedByKodeWilayah()
    {
        $this->collWdstSyncLogsRelatedByKodeWilayah = null; // important to set this to null since that means it is uninitialized
        $this->collWdstSyncLogsRelatedByKodeWilayahPartial = null;

        return $this;
    }

    /**
     * reset is the collWdstSyncLogsRelatedByKodeWilayah collection loaded partially
     *
     * @return void
     */
    public function resetPartialWdstSyncLogsRelatedByKodeWilayah($v = true)
    {
        $this->collWdstSyncLogsRelatedByKodeWilayahPartial = $v;
    }

    /**
     * Initializes the collWdstSyncLogsRelatedByKodeWilayah collection.
     *
     * By default this just sets the collWdstSyncLogsRelatedByKodeWilayah collection to an empty array (like clearcollWdstSyncLogsRelatedByKodeWilayah());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initWdstSyncLogsRelatedByKodeWilayah($overrideExisting = true)
    {
        if (null !== $this->collWdstSyncLogsRelatedByKodeWilayah && !$overrideExisting) {
            return;
        }
        $this->collWdstSyncLogsRelatedByKodeWilayah = new PropelObjectCollection();
        $this->collWdstSyncLogsRelatedByKodeWilayah->setModel('WdstSyncLog');
    }

    /**
     * Gets an array of WdstSyncLog objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this MstWilayah is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|WdstSyncLog[] List of WdstSyncLog objects
     * @throws PropelException
     */
    public function getWdstSyncLogsRelatedByKodeWilayah($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collWdstSyncLogsRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collWdstSyncLogsRelatedByKodeWilayah || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collWdstSyncLogsRelatedByKodeWilayah) {
                // return empty collection
                $this->initWdstSyncLogsRelatedByKodeWilayah();
            } else {
                $collWdstSyncLogsRelatedByKodeWilayah = WdstSyncLogQuery::create(null, $criteria)
                    ->filterByMstWilayahRelatedByKodeWilayah($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collWdstSyncLogsRelatedByKodeWilayahPartial && count($collWdstSyncLogsRelatedByKodeWilayah)) {
                      $this->initWdstSyncLogsRelatedByKodeWilayah(false);

                      foreach($collWdstSyncLogsRelatedByKodeWilayah as $obj) {
                        if (false == $this->collWdstSyncLogsRelatedByKodeWilayah->contains($obj)) {
                          $this->collWdstSyncLogsRelatedByKodeWilayah->append($obj);
                        }
                      }

                      $this->collWdstSyncLogsRelatedByKodeWilayahPartial = true;
                    }

                    $collWdstSyncLogsRelatedByKodeWilayah->getInternalIterator()->rewind();
                    return $collWdstSyncLogsRelatedByKodeWilayah;
                }

                if($partial && $this->collWdstSyncLogsRelatedByKodeWilayah) {
                    foreach($this->collWdstSyncLogsRelatedByKodeWilayah as $obj) {
                        if($obj->isNew()) {
                            $collWdstSyncLogsRelatedByKodeWilayah[] = $obj;
                        }
                    }
                }

                $this->collWdstSyncLogsRelatedByKodeWilayah = $collWdstSyncLogsRelatedByKodeWilayah;
                $this->collWdstSyncLogsRelatedByKodeWilayahPartial = false;
            }
        }

        return $this->collWdstSyncLogsRelatedByKodeWilayah;
    }

    /**
     * Sets a collection of WdstSyncLogRelatedByKodeWilayah objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $wdstSyncLogsRelatedByKodeWilayah A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setWdstSyncLogsRelatedByKodeWilayah(PropelCollection $wdstSyncLogsRelatedByKodeWilayah, PropelPDO $con = null)
    {
        $wdstSyncLogsRelatedByKodeWilayahToDelete = $this->getWdstSyncLogsRelatedByKodeWilayah(new Criteria(), $con)->diff($wdstSyncLogsRelatedByKodeWilayah);

        $this->wdstSyncLogsRelatedByKodeWilayahScheduledForDeletion = unserialize(serialize($wdstSyncLogsRelatedByKodeWilayahToDelete));

        foreach ($wdstSyncLogsRelatedByKodeWilayahToDelete as $wdstSyncLogRelatedByKodeWilayahRemoved) {
            $wdstSyncLogRelatedByKodeWilayahRemoved->setMstWilayahRelatedByKodeWilayah(null);
        }

        $this->collWdstSyncLogsRelatedByKodeWilayah = null;
        foreach ($wdstSyncLogsRelatedByKodeWilayah as $wdstSyncLogRelatedByKodeWilayah) {
            $this->addWdstSyncLogRelatedByKodeWilayah($wdstSyncLogRelatedByKodeWilayah);
        }

        $this->collWdstSyncLogsRelatedByKodeWilayah = $wdstSyncLogsRelatedByKodeWilayah;
        $this->collWdstSyncLogsRelatedByKodeWilayahPartial = false;

        return $this;
    }

    /**
     * Returns the number of related WdstSyncLog objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related WdstSyncLog objects.
     * @throws PropelException
     */
    public function countWdstSyncLogsRelatedByKodeWilayah(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collWdstSyncLogsRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collWdstSyncLogsRelatedByKodeWilayah || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collWdstSyncLogsRelatedByKodeWilayah) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getWdstSyncLogsRelatedByKodeWilayah());
            }
            $query = WdstSyncLogQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMstWilayahRelatedByKodeWilayah($this)
                ->count($con);
        }

        return count($this->collWdstSyncLogsRelatedByKodeWilayah);
    }

    /**
     * Method called to associate a WdstSyncLog object to this object
     * through the WdstSyncLog foreign key attribute.
     *
     * @param    WdstSyncLog $l WdstSyncLog
     * @return MstWilayah The current object (for fluent API support)
     */
    public function addWdstSyncLogRelatedByKodeWilayah(WdstSyncLog $l)
    {
        if ($this->collWdstSyncLogsRelatedByKodeWilayah === null) {
            $this->initWdstSyncLogsRelatedByKodeWilayah();
            $this->collWdstSyncLogsRelatedByKodeWilayahPartial = true;
        }
        if (!in_array($l, $this->collWdstSyncLogsRelatedByKodeWilayah->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddWdstSyncLogRelatedByKodeWilayah($l);
        }

        return $this;
    }

    /**
     * @param	WdstSyncLogRelatedByKodeWilayah $wdstSyncLogRelatedByKodeWilayah The wdstSyncLogRelatedByKodeWilayah object to add.
     */
    protected function doAddWdstSyncLogRelatedByKodeWilayah($wdstSyncLogRelatedByKodeWilayah)
    {
        $this->collWdstSyncLogsRelatedByKodeWilayah[]= $wdstSyncLogRelatedByKodeWilayah;
        $wdstSyncLogRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah($this);
    }

    /**
     * @param	WdstSyncLogRelatedByKodeWilayah $wdstSyncLogRelatedByKodeWilayah The wdstSyncLogRelatedByKodeWilayah object to remove.
     * @return MstWilayah The current object (for fluent API support)
     */
    public function removeWdstSyncLogRelatedByKodeWilayah($wdstSyncLogRelatedByKodeWilayah)
    {
        if ($this->getWdstSyncLogsRelatedByKodeWilayah()->contains($wdstSyncLogRelatedByKodeWilayah)) {
            $this->collWdstSyncLogsRelatedByKodeWilayah->remove($this->collWdstSyncLogsRelatedByKodeWilayah->search($wdstSyncLogRelatedByKodeWilayah));
            if (null === $this->wdstSyncLogsRelatedByKodeWilayahScheduledForDeletion) {
                $this->wdstSyncLogsRelatedByKodeWilayahScheduledForDeletion = clone $this->collWdstSyncLogsRelatedByKodeWilayah;
                $this->wdstSyncLogsRelatedByKodeWilayahScheduledForDeletion->clear();
            }
            $this->wdstSyncLogsRelatedByKodeWilayahScheduledForDeletion[]= clone $wdstSyncLogRelatedByKodeWilayah;
            $wdstSyncLogRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related WdstSyncLogsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|WdstSyncLog[] List of WdstSyncLog objects
     */
    public function getWdstSyncLogsRelatedByKodeWilayahJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = WdstSyncLogQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getWdstSyncLogsRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related WdstSyncLogsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|WdstSyncLog[] List of WdstSyncLog objects
     */
    public function getWdstSyncLogsRelatedByKodeWilayahJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = WdstSyncLogQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getWdstSyncLogsRelatedByKodeWilayah($query, $con);
    }

    /**
     * Clears out the collWdstSyncLogsRelatedByKodeWilayah collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return MstWilayah The current object (for fluent API support)
     * @see        addWdstSyncLogsRelatedByKodeWilayah()
     */
    public function clearWdstSyncLogsRelatedByKodeWilayah()
    {
        $this->collWdstSyncLogsRelatedByKodeWilayah = null; // important to set this to null since that means it is uninitialized
        $this->collWdstSyncLogsRelatedByKodeWilayahPartial = null;

        return $this;
    }

    /**
     * reset is the collWdstSyncLogsRelatedByKodeWilayah collection loaded partially
     *
     * @return void
     */
    public function resetPartialWdstSyncLogsRelatedByKodeWilayah($v = true)
    {
        $this->collWdstSyncLogsRelatedByKodeWilayahPartial = $v;
    }

    /**
     * Initializes the collWdstSyncLogsRelatedByKodeWilayah collection.
     *
     * By default this just sets the collWdstSyncLogsRelatedByKodeWilayah collection to an empty array (like clearcollWdstSyncLogsRelatedByKodeWilayah());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initWdstSyncLogsRelatedByKodeWilayah($overrideExisting = true)
    {
        if (null !== $this->collWdstSyncLogsRelatedByKodeWilayah && !$overrideExisting) {
            return;
        }
        $this->collWdstSyncLogsRelatedByKodeWilayah = new PropelObjectCollection();
        $this->collWdstSyncLogsRelatedByKodeWilayah->setModel('WdstSyncLog');
    }

    /**
     * Gets an array of WdstSyncLog objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this MstWilayah is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|WdstSyncLog[] List of WdstSyncLog objects
     * @throws PropelException
     */
    public function getWdstSyncLogsRelatedByKodeWilayah($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collWdstSyncLogsRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collWdstSyncLogsRelatedByKodeWilayah || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collWdstSyncLogsRelatedByKodeWilayah) {
                // return empty collection
                $this->initWdstSyncLogsRelatedByKodeWilayah();
            } else {
                $collWdstSyncLogsRelatedByKodeWilayah = WdstSyncLogQuery::create(null, $criteria)
                    ->filterByMstWilayahRelatedByKodeWilayah($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collWdstSyncLogsRelatedByKodeWilayahPartial && count($collWdstSyncLogsRelatedByKodeWilayah)) {
                      $this->initWdstSyncLogsRelatedByKodeWilayah(false);

                      foreach($collWdstSyncLogsRelatedByKodeWilayah as $obj) {
                        if (false == $this->collWdstSyncLogsRelatedByKodeWilayah->contains($obj)) {
                          $this->collWdstSyncLogsRelatedByKodeWilayah->append($obj);
                        }
                      }

                      $this->collWdstSyncLogsRelatedByKodeWilayahPartial = true;
                    }

                    $collWdstSyncLogsRelatedByKodeWilayah->getInternalIterator()->rewind();
                    return $collWdstSyncLogsRelatedByKodeWilayah;
                }

                if($partial && $this->collWdstSyncLogsRelatedByKodeWilayah) {
                    foreach($this->collWdstSyncLogsRelatedByKodeWilayah as $obj) {
                        if($obj->isNew()) {
                            $collWdstSyncLogsRelatedByKodeWilayah[] = $obj;
                        }
                    }
                }

                $this->collWdstSyncLogsRelatedByKodeWilayah = $collWdstSyncLogsRelatedByKodeWilayah;
                $this->collWdstSyncLogsRelatedByKodeWilayahPartial = false;
            }
        }

        return $this->collWdstSyncLogsRelatedByKodeWilayah;
    }

    /**
     * Sets a collection of WdstSyncLogRelatedByKodeWilayah objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $wdstSyncLogsRelatedByKodeWilayah A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setWdstSyncLogsRelatedByKodeWilayah(PropelCollection $wdstSyncLogsRelatedByKodeWilayah, PropelPDO $con = null)
    {
        $wdstSyncLogsRelatedByKodeWilayahToDelete = $this->getWdstSyncLogsRelatedByKodeWilayah(new Criteria(), $con)->diff($wdstSyncLogsRelatedByKodeWilayah);

        $this->wdstSyncLogsRelatedByKodeWilayahScheduledForDeletion = unserialize(serialize($wdstSyncLogsRelatedByKodeWilayahToDelete));

        foreach ($wdstSyncLogsRelatedByKodeWilayahToDelete as $wdstSyncLogRelatedByKodeWilayahRemoved) {
            $wdstSyncLogRelatedByKodeWilayahRemoved->setMstWilayahRelatedByKodeWilayah(null);
        }

        $this->collWdstSyncLogsRelatedByKodeWilayah = null;
        foreach ($wdstSyncLogsRelatedByKodeWilayah as $wdstSyncLogRelatedByKodeWilayah) {
            $this->addWdstSyncLogRelatedByKodeWilayah($wdstSyncLogRelatedByKodeWilayah);
        }

        $this->collWdstSyncLogsRelatedByKodeWilayah = $wdstSyncLogsRelatedByKodeWilayah;
        $this->collWdstSyncLogsRelatedByKodeWilayahPartial = false;

        return $this;
    }

    /**
     * Returns the number of related WdstSyncLog objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related WdstSyncLog objects.
     * @throws PropelException
     */
    public function countWdstSyncLogsRelatedByKodeWilayah(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collWdstSyncLogsRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collWdstSyncLogsRelatedByKodeWilayah || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collWdstSyncLogsRelatedByKodeWilayah) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getWdstSyncLogsRelatedByKodeWilayah());
            }
            $query = WdstSyncLogQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMstWilayahRelatedByKodeWilayah($this)
                ->count($con);
        }

        return count($this->collWdstSyncLogsRelatedByKodeWilayah);
    }

    /**
     * Method called to associate a WdstSyncLog object to this object
     * through the WdstSyncLog foreign key attribute.
     *
     * @param    WdstSyncLog $l WdstSyncLog
     * @return MstWilayah The current object (for fluent API support)
     */
    public function addWdstSyncLogRelatedByKodeWilayah(WdstSyncLog $l)
    {
        if ($this->collWdstSyncLogsRelatedByKodeWilayah === null) {
            $this->initWdstSyncLogsRelatedByKodeWilayah();
            $this->collWdstSyncLogsRelatedByKodeWilayahPartial = true;
        }
        if (!in_array($l, $this->collWdstSyncLogsRelatedByKodeWilayah->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddWdstSyncLogRelatedByKodeWilayah($l);
        }

        return $this;
    }

    /**
     * @param	WdstSyncLogRelatedByKodeWilayah $wdstSyncLogRelatedByKodeWilayah The wdstSyncLogRelatedByKodeWilayah object to add.
     */
    protected function doAddWdstSyncLogRelatedByKodeWilayah($wdstSyncLogRelatedByKodeWilayah)
    {
        $this->collWdstSyncLogsRelatedByKodeWilayah[]= $wdstSyncLogRelatedByKodeWilayah;
        $wdstSyncLogRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah($this);
    }

    /**
     * @param	WdstSyncLogRelatedByKodeWilayah $wdstSyncLogRelatedByKodeWilayah The wdstSyncLogRelatedByKodeWilayah object to remove.
     * @return MstWilayah The current object (for fluent API support)
     */
    public function removeWdstSyncLogRelatedByKodeWilayah($wdstSyncLogRelatedByKodeWilayah)
    {
        if ($this->getWdstSyncLogsRelatedByKodeWilayah()->contains($wdstSyncLogRelatedByKodeWilayah)) {
            $this->collWdstSyncLogsRelatedByKodeWilayah->remove($this->collWdstSyncLogsRelatedByKodeWilayah->search($wdstSyncLogRelatedByKodeWilayah));
            if (null === $this->wdstSyncLogsRelatedByKodeWilayahScheduledForDeletion) {
                $this->wdstSyncLogsRelatedByKodeWilayahScheduledForDeletion = clone $this->collWdstSyncLogsRelatedByKodeWilayah;
                $this->wdstSyncLogsRelatedByKodeWilayahScheduledForDeletion->clear();
            }
            $this->wdstSyncLogsRelatedByKodeWilayahScheduledForDeletion[]= clone $wdstSyncLogRelatedByKodeWilayah;
            $wdstSyncLogRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related WdstSyncLogsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|WdstSyncLog[] List of WdstSyncLog objects
     */
    public function getWdstSyncLogsRelatedByKodeWilayahJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = WdstSyncLogQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getWdstSyncLogsRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related WdstSyncLogsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|WdstSyncLog[] List of WdstSyncLog objects
     */
    public function getWdstSyncLogsRelatedByKodeWilayahJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = WdstSyncLogQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getWdstSyncLogsRelatedByKodeWilayah($query, $con);
    }

    /**
     * Clears out the collWPendingJobsRelatedByKodeWilayah collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return MstWilayah The current object (for fluent API support)
     * @see        addWPendingJobsRelatedByKodeWilayah()
     */
    public function clearWPendingJobsRelatedByKodeWilayah()
    {
        $this->collWPendingJobsRelatedByKodeWilayah = null; // important to set this to null since that means it is uninitialized
        $this->collWPendingJobsRelatedByKodeWilayahPartial = null;

        return $this;
    }

    /**
     * reset is the collWPendingJobsRelatedByKodeWilayah collection loaded partially
     *
     * @return void
     */
    public function resetPartialWPendingJobsRelatedByKodeWilayah($v = true)
    {
        $this->collWPendingJobsRelatedByKodeWilayahPartial = $v;
    }

    /**
     * Initializes the collWPendingJobsRelatedByKodeWilayah collection.
     *
     * By default this just sets the collWPendingJobsRelatedByKodeWilayah collection to an empty array (like clearcollWPendingJobsRelatedByKodeWilayah());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initWPendingJobsRelatedByKodeWilayah($overrideExisting = true)
    {
        if (null !== $this->collWPendingJobsRelatedByKodeWilayah && !$overrideExisting) {
            return;
        }
        $this->collWPendingJobsRelatedByKodeWilayah = new PropelObjectCollection();
        $this->collWPendingJobsRelatedByKodeWilayah->setModel('WPendingJob');
    }

    /**
     * Gets an array of WPendingJob objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this MstWilayah is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|WPendingJob[] List of WPendingJob objects
     * @throws PropelException
     */
    public function getWPendingJobsRelatedByKodeWilayah($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collWPendingJobsRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collWPendingJobsRelatedByKodeWilayah || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collWPendingJobsRelatedByKodeWilayah) {
                // return empty collection
                $this->initWPendingJobsRelatedByKodeWilayah();
            } else {
                $collWPendingJobsRelatedByKodeWilayah = WPendingJobQuery::create(null, $criteria)
                    ->filterByMstWilayahRelatedByKodeWilayah($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collWPendingJobsRelatedByKodeWilayahPartial && count($collWPendingJobsRelatedByKodeWilayah)) {
                      $this->initWPendingJobsRelatedByKodeWilayah(false);

                      foreach($collWPendingJobsRelatedByKodeWilayah as $obj) {
                        if (false == $this->collWPendingJobsRelatedByKodeWilayah->contains($obj)) {
                          $this->collWPendingJobsRelatedByKodeWilayah->append($obj);
                        }
                      }

                      $this->collWPendingJobsRelatedByKodeWilayahPartial = true;
                    }

                    $collWPendingJobsRelatedByKodeWilayah->getInternalIterator()->rewind();
                    return $collWPendingJobsRelatedByKodeWilayah;
                }

                if($partial && $this->collWPendingJobsRelatedByKodeWilayah) {
                    foreach($this->collWPendingJobsRelatedByKodeWilayah as $obj) {
                        if($obj->isNew()) {
                            $collWPendingJobsRelatedByKodeWilayah[] = $obj;
                        }
                    }
                }

                $this->collWPendingJobsRelatedByKodeWilayah = $collWPendingJobsRelatedByKodeWilayah;
                $this->collWPendingJobsRelatedByKodeWilayahPartial = false;
            }
        }

        return $this->collWPendingJobsRelatedByKodeWilayah;
    }

    /**
     * Sets a collection of WPendingJobRelatedByKodeWilayah objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $wPendingJobsRelatedByKodeWilayah A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setWPendingJobsRelatedByKodeWilayah(PropelCollection $wPendingJobsRelatedByKodeWilayah, PropelPDO $con = null)
    {
        $wPendingJobsRelatedByKodeWilayahToDelete = $this->getWPendingJobsRelatedByKodeWilayah(new Criteria(), $con)->diff($wPendingJobsRelatedByKodeWilayah);

        $this->wPendingJobsRelatedByKodeWilayahScheduledForDeletion = unserialize(serialize($wPendingJobsRelatedByKodeWilayahToDelete));

        foreach ($wPendingJobsRelatedByKodeWilayahToDelete as $wPendingJobRelatedByKodeWilayahRemoved) {
            $wPendingJobRelatedByKodeWilayahRemoved->setMstWilayahRelatedByKodeWilayah(null);
        }

        $this->collWPendingJobsRelatedByKodeWilayah = null;
        foreach ($wPendingJobsRelatedByKodeWilayah as $wPendingJobRelatedByKodeWilayah) {
            $this->addWPendingJobRelatedByKodeWilayah($wPendingJobRelatedByKodeWilayah);
        }

        $this->collWPendingJobsRelatedByKodeWilayah = $wPendingJobsRelatedByKodeWilayah;
        $this->collWPendingJobsRelatedByKodeWilayahPartial = false;

        return $this;
    }

    /**
     * Returns the number of related WPendingJob objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related WPendingJob objects.
     * @throws PropelException
     */
    public function countWPendingJobsRelatedByKodeWilayah(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collWPendingJobsRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collWPendingJobsRelatedByKodeWilayah || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collWPendingJobsRelatedByKodeWilayah) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getWPendingJobsRelatedByKodeWilayah());
            }
            $query = WPendingJobQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMstWilayahRelatedByKodeWilayah($this)
                ->count($con);
        }

        return count($this->collWPendingJobsRelatedByKodeWilayah);
    }

    /**
     * Method called to associate a WPendingJob object to this object
     * through the WPendingJob foreign key attribute.
     *
     * @param    WPendingJob $l WPendingJob
     * @return MstWilayah The current object (for fluent API support)
     */
    public function addWPendingJobRelatedByKodeWilayah(WPendingJob $l)
    {
        if ($this->collWPendingJobsRelatedByKodeWilayah === null) {
            $this->initWPendingJobsRelatedByKodeWilayah();
            $this->collWPendingJobsRelatedByKodeWilayahPartial = true;
        }
        if (!in_array($l, $this->collWPendingJobsRelatedByKodeWilayah->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddWPendingJobRelatedByKodeWilayah($l);
        }

        return $this;
    }

    /**
     * @param	WPendingJobRelatedByKodeWilayah $wPendingJobRelatedByKodeWilayah The wPendingJobRelatedByKodeWilayah object to add.
     */
    protected function doAddWPendingJobRelatedByKodeWilayah($wPendingJobRelatedByKodeWilayah)
    {
        $this->collWPendingJobsRelatedByKodeWilayah[]= $wPendingJobRelatedByKodeWilayah;
        $wPendingJobRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah($this);
    }

    /**
     * @param	WPendingJobRelatedByKodeWilayah $wPendingJobRelatedByKodeWilayah The wPendingJobRelatedByKodeWilayah object to remove.
     * @return MstWilayah The current object (for fluent API support)
     */
    public function removeWPendingJobRelatedByKodeWilayah($wPendingJobRelatedByKodeWilayah)
    {
        if ($this->getWPendingJobsRelatedByKodeWilayah()->contains($wPendingJobRelatedByKodeWilayah)) {
            $this->collWPendingJobsRelatedByKodeWilayah->remove($this->collWPendingJobsRelatedByKodeWilayah->search($wPendingJobRelatedByKodeWilayah));
            if (null === $this->wPendingJobsRelatedByKodeWilayahScheduledForDeletion) {
                $this->wPendingJobsRelatedByKodeWilayahScheduledForDeletion = clone $this->collWPendingJobsRelatedByKodeWilayah;
                $this->wPendingJobsRelatedByKodeWilayahScheduledForDeletion->clear();
            }
            $this->wPendingJobsRelatedByKodeWilayahScheduledForDeletion[]= clone $wPendingJobRelatedByKodeWilayah;
            $wPendingJobRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related WPendingJobsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|WPendingJob[] List of WPendingJob objects
     */
    public function getWPendingJobsRelatedByKodeWilayahJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = WPendingJobQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getWPendingJobsRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related WPendingJobsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|WPendingJob[] List of WPendingJob objects
     */
    public function getWPendingJobsRelatedByKodeWilayahJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = WPendingJobQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getWPendingJobsRelatedByKodeWilayah($query, $con);
    }

    /**
     * Clears out the collWPendingJobsRelatedByKodeWilayah collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return MstWilayah The current object (for fluent API support)
     * @see        addWPendingJobsRelatedByKodeWilayah()
     */
    public function clearWPendingJobsRelatedByKodeWilayah()
    {
        $this->collWPendingJobsRelatedByKodeWilayah = null; // important to set this to null since that means it is uninitialized
        $this->collWPendingJobsRelatedByKodeWilayahPartial = null;

        return $this;
    }

    /**
     * reset is the collWPendingJobsRelatedByKodeWilayah collection loaded partially
     *
     * @return void
     */
    public function resetPartialWPendingJobsRelatedByKodeWilayah($v = true)
    {
        $this->collWPendingJobsRelatedByKodeWilayahPartial = $v;
    }

    /**
     * Initializes the collWPendingJobsRelatedByKodeWilayah collection.
     *
     * By default this just sets the collWPendingJobsRelatedByKodeWilayah collection to an empty array (like clearcollWPendingJobsRelatedByKodeWilayah());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initWPendingJobsRelatedByKodeWilayah($overrideExisting = true)
    {
        if (null !== $this->collWPendingJobsRelatedByKodeWilayah && !$overrideExisting) {
            return;
        }
        $this->collWPendingJobsRelatedByKodeWilayah = new PropelObjectCollection();
        $this->collWPendingJobsRelatedByKodeWilayah->setModel('WPendingJob');
    }

    /**
     * Gets an array of WPendingJob objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this MstWilayah is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|WPendingJob[] List of WPendingJob objects
     * @throws PropelException
     */
    public function getWPendingJobsRelatedByKodeWilayah($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collWPendingJobsRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collWPendingJobsRelatedByKodeWilayah || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collWPendingJobsRelatedByKodeWilayah) {
                // return empty collection
                $this->initWPendingJobsRelatedByKodeWilayah();
            } else {
                $collWPendingJobsRelatedByKodeWilayah = WPendingJobQuery::create(null, $criteria)
                    ->filterByMstWilayahRelatedByKodeWilayah($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collWPendingJobsRelatedByKodeWilayahPartial && count($collWPendingJobsRelatedByKodeWilayah)) {
                      $this->initWPendingJobsRelatedByKodeWilayah(false);

                      foreach($collWPendingJobsRelatedByKodeWilayah as $obj) {
                        if (false == $this->collWPendingJobsRelatedByKodeWilayah->contains($obj)) {
                          $this->collWPendingJobsRelatedByKodeWilayah->append($obj);
                        }
                      }

                      $this->collWPendingJobsRelatedByKodeWilayahPartial = true;
                    }

                    $collWPendingJobsRelatedByKodeWilayah->getInternalIterator()->rewind();
                    return $collWPendingJobsRelatedByKodeWilayah;
                }

                if($partial && $this->collWPendingJobsRelatedByKodeWilayah) {
                    foreach($this->collWPendingJobsRelatedByKodeWilayah as $obj) {
                        if($obj->isNew()) {
                            $collWPendingJobsRelatedByKodeWilayah[] = $obj;
                        }
                    }
                }

                $this->collWPendingJobsRelatedByKodeWilayah = $collWPendingJobsRelatedByKodeWilayah;
                $this->collWPendingJobsRelatedByKodeWilayahPartial = false;
            }
        }

        return $this->collWPendingJobsRelatedByKodeWilayah;
    }

    /**
     * Sets a collection of WPendingJobRelatedByKodeWilayah objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $wPendingJobsRelatedByKodeWilayah A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setWPendingJobsRelatedByKodeWilayah(PropelCollection $wPendingJobsRelatedByKodeWilayah, PropelPDO $con = null)
    {
        $wPendingJobsRelatedByKodeWilayahToDelete = $this->getWPendingJobsRelatedByKodeWilayah(new Criteria(), $con)->diff($wPendingJobsRelatedByKodeWilayah);

        $this->wPendingJobsRelatedByKodeWilayahScheduledForDeletion = unserialize(serialize($wPendingJobsRelatedByKodeWilayahToDelete));

        foreach ($wPendingJobsRelatedByKodeWilayahToDelete as $wPendingJobRelatedByKodeWilayahRemoved) {
            $wPendingJobRelatedByKodeWilayahRemoved->setMstWilayahRelatedByKodeWilayah(null);
        }

        $this->collWPendingJobsRelatedByKodeWilayah = null;
        foreach ($wPendingJobsRelatedByKodeWilayah as $wPendingJobRelatedByKodeWilayah) {
            $this->addWPendingJobRelatedByKodeWilayah($wPendingJobRelatedByKodeWilayah);
        }

        $this->collWPendingJobsRelatedByKodeWilayah = $wPendingJobsRelatedByKodeWilayah;
        $this->collWPendingJobsRelatedByKodeWilayahPartial = false;

        return $this;
    }

    /**
     * Returns the number of related WPendingJob objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related WPendingJob objects.
     * @throws PropelException
     */
    public function countWPendingJobsRelatedByKodeWilayah(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collWPendingJobsRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collWPendingJobsRelatedByKodeWilayah || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collWPendingJobsRelatedByKodeWilayah) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getWPendingJobsRelatedByKodeWilayah());
            }
            $query = WPendingJobQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMstWilayahRelatedByKodeWilayah($this)
                ->count($con);
        }

        return count($this->collWPendingJobsRelatedByKodeWilayah);
    }

    /**
     * Method called to associate a WPendingJob object to this object
     * through the WPendingJob foreign key attribute.
     *
     * @param    WPendingJob $l WPendingJob
     * @return MstWilayah The current object (for fluent API support)
     */
    public function addWPendingJobRelatedByKodeWilayah(WPendingJob $l)
    {
        if ($this->collWPendingJobsRelatedByKodeWilayah === null) {
            $this->initWPendingJobsRelatedByKodeWilayah();
            $this->collWPendingJobsRelatedByKodeWilayahPartial = true;
        }
        if (!in_array($l, $this->collWPendingJobsRelatedByKodeWilayah->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddWPendingJobRelatedByKodeWilayah($l);
        }

        return $this;
    }

    /**
     * @param	WPendingJobRelatedByKodeWilayah $wPendingJobRelatedByKodeWilayah The wPendingJobRelatedByKodeWilayah object to add.
     */
    protected function doAddWPendingJobRelatedByKodeWilayah($wPendingJobRelatedByKodeWilayah)
    {
        $this->collWPendingJobsRelatedByKodeWilayah[]= $wPendingJobRelatedByKodeWilayah;
        $wPendingJobRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah($this);
    }

    /**
     * @param	WPendingJobRelatedByKodeWilayah $wPendingJobRelatedByKodeWilayah The wPendingJobRelatedByKodeWilayah object to remove.
     * @return MstWilayah The current object (for fluent API support)
     */
    public function removeWPendingJobRelatedByKodeWilayah($wPendingJobRelatedByKodeWilayah)
    {
        if ($this->getWPendingJobsRelatedByKodeWilayah()->contains($wPendingJobRelatedByKodeWilayah)) {
            $this->collWPendingJobsRelatedByKodeWilayah->remove($this->collWPendingJobsRelatedByKodeWilayah->search($wPendingJobRelatedByKodeWilayah));
            if (null === $this->wPendingJobsRelatedByKodeWilayahScheduledForDeletion) {
                $this->wPendingJobsRelatedByKodeWilayahScheduledForDeletion = clone $this->collWPendingJobsRelatedByKodeWilayah;
                $this->wPendingJobsRelatedByKodeWilayahScheduledForDeletion->clear();
            }
            $this->wPendingJobsRelatedByKodeWilayahScheduledForDeletion[]= clone $wPendingJobRelatedByKodeWilayah;
            $wPendingJobRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related WPendingJobsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|WPendingJob[] List of WPendingJob objects
     */
    public function getWPendingJobsRelatedByKodeWilayahJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = WPendingJobQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getWPendingJobsRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related WPendingJobsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|WPendingJob[] List of WPendingJob objects
     */
    public function getWPendingJobsRelatedByKodeWilayahJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = WPendingJobQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getWPendingJobsRelatedByKodeWilayah($query, $con);
    }

    /**
     * Clears out the collLembagaNonSekolahsRelatedByKodeWilayah collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return MstWilayah The current object (for fluent API support)
     * @see        addLembagaNonSekolahsRelatedByKodeWilayah()
     */
    public function clearLembagaNonSekolahsRelatedByKodeWilayah()
    {
        $this->collLembagaNonSekolahsRelatedByKodeWilayah = null; // important to set this to null since that means it is uninitialized
        $this->collLembagaNonSekolahsRelatedByKodeWilayahPartial = null;

        return $this;
    }

    /**
     * reset is the collLembagaNonSekolahsRelatedByKodeWilayah collection loaded partially
     *
     * @return void
     */
    public function resetPartialLembagaNonSekolahsRelatedByKodeWilayah($v = true)
    {
        $this->collLembagaNonSekolahsRelatedByKodeWilayahPartial = $v;
    }

    /**
     * Initializes the collLembagaNonSekolahsRelatedByKodeWilayah collection.
     *
     * By default this just sets the collLembagaNonSekolahsRelatedByKodeWilayah collection to an empty array (like clearcollLembagaNonSekolahsRelatedByKodeWilayah());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initLembagaNonSekolahsRelatedByKodeWilayah($overrideExisting = true)
    {
        if (null !== $this->collLembagaNonSekolahsRelatedByKodeWilayah && !$overrideExisting) {
            return;
        }
        $this->collLembagaNonSekolahsRelatedByKodeWilayah = new PropelObjectCollection();
        $this->collLembagaNonSekolahsRelatedByKodeWilayah->setModel('LembagaNonSekolah');
    }

    /**
     * Gets an array of LembagaNonSekolah objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this MstWilayah is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|LembagaNonSekolah[] List of LembagaNonSekolah objects
     * @throws PropelException
     */
    public function getLembagaNonSekolahsRelatedByKodeWilayah($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collLembagaNonSekolahsRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collLembagaNonSekolahsRelatedByKodeWilayah || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collLembagaNonSekolahsRelatedByKodeWilayah) {
                // return empty collection
                $this->initLembagaNonSekolahsRelatedByKodeWilayah();
            } else {
                $collLembagaNonSekolahsRelatedByKodeWilayah = LembagaNonSekolahQuery::create(null, $criteria)
                    ->filterByMstWilayahRelatedByKodeWilayah($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collLembagaNonSekolahsRelatedByKodeWilayahPartial && count($collLembagaNonSekolahsRelatedByKodeWilayah)) {
                      $this->initLembagaNonSekolahsRelatedByKodeWilayah(false);

                      foreach($collLembagaNonSekolahsRelatedByKodeWilayah as $obj) {
                        if (false == $this->collLembagaNonSekolahsRelatedByKodeWilayah->contains($obj)) {
                          $this->collLembagaNonSekolahsRelatedByKodeWilayah->append($obj);
                        }
                      }

                      $this->collLembagaNonSekolahsRelatedByKodeWilayahPartial = true;
                    }

                    $collLembagaNonSekolahsRelatedByKodeWilayah->getInternalIterator()->rewind();
                    return $collLembagaNonSekolahsRelatedByKodeWilayah;
                }

                if($partial && $this->collLembagaNonSekolahsRelatedByKodeWilayah) {
                    foreach($this->collLembagaNonSekolahsRelatedByKodeWilayah as $obj) {
                        if($obj->isNew()) {
                            $collLembagaNonSekolahsRelatedByKodeWilayah[] = $obj;
                        }
                    }
                }

                $this->collLembagaNonSekolahsRelatedByKodeWilayah = $collLembagaNonSekolahsRelatedByKodeWilayah;
                $this->collLembagaNonSekolahsRelatedByKodeWilayahPartial = false;
            }
        }

        return $this->collLembagaNonSekolahsRelatedByKodeWilayah;
    }

    /**
     * Sets a collection of LembagaNonSekolahRelatedByKodeWilayah objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $lembagaNonSekolahsRelatedByKodeWilayah A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setLembagaNonSekolahsRelatedByKodeWilayah(PropelCollection $lembagaNonSekolahsRelatedByKodeWilayah, PropelPDO $con = null)
    {
        $lembagaNonSekolahsRelatedByKodeWilayahToDelete = $this->getLembagaNonSekolahsRelatedByKodeWilayah(new Criteria(), $con)->diff($lembagaNonSekolahsRelatedByKodeWilayah);

        $this->lembagaNonSekolahsRelatedByKodeWilayahScheduledForDeletion = unserialize(serialize($lembagaNonSekolahsRelatedByKodeWilayahToDelete));

        foreach ($lembagaNonSekolahsRelatedByKodeWilayahToDelete as $lembagaNonSekolahRelatedByKodeWilayahRemoved) {
            $lembagaNonSekolahRelatedByKodeWilayahRemoved->setMstWilayahRelatedByKodeWilayah(null);
        }

        $this->collLembagaNonSekolahsRelatedByKodeWilayah = null;
        foreach ($lembagaNonSekolahsRelatedByKodeWilayah as $lembagaNonSekolahRelatedByKodeWilayah) {
            $this->addLembagaNonSekolahRelatedByKodeWilayah($lembagaNonSekolahRelatedByKodeWilayah);
        }

        $this->collLembagaNonSekolahsRelatedByKodeWilayah = $lembagaNonSekolahsRelatedByKodeWilayah;
        $this->collLembagaNonSekolahsRelatedByKodeWilayahPartial = false;

        return $this;
    }

    /**
     * Returns the number of related LembagaNonSekolah objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related LembagaNonSekolah objects.
     * @throws PropelException
     */
    public function countLembagaNonSekolahsRelatedByKodeWilayah(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collLembagaNonSekolahsRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collLembagaNonSekolahsRelatedByKodeWilayah || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collLembagaNonSekolahsRelatedByKodeWilayah) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getLembagaNonSekolahsRelatedByKodeWilayah());
            }
            $query = LembagaNonSekolahQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMstWilayahRelatedByKodeWilayah($this)
                ->count($con);
        }

        return count($this->collLembagaNonSekolahsRelatedByKodeWilayah);
    }

    /**
     * Method called to associate a LembagaNonSekolah object to this object
     * through the LembagaNonSekolah foreign key attribute.
     *
     * @param    LembagaNonSekolah $l LembagaNonSekolah
     * @return MstWilayah The current object (for fluent API support)
     */
    public function addLembagaNonSekolahRelatedByKodeWilayah(LembagaNonSekolah $l)
    {
        if ($this->collLembagaNonSekolahsRelatedByKodeWilayah === null) {
            $this->initLembagaNonSekolahsRelatedByKodeWilayah();
            $this->collLembagaNonSekolahsRelatedByKodeWilayahPartial = true;
        }
        if (!in_array($l, $this->collLembagaNonSekolahsRelatedByKodeWilayah->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddLembagaNonSekolahRelatedByKodeWilayah($l);
        }

        return $this;
    }

    /**
     * @param	LembagaNonSekolahRelatedByKodeWilayah $lembagaNonSekolahRelatedByKodeWilayah The lembagaNonSekolahRelatedByKodeWilayah object to add.
     */
    protected function doAddLembagaNonSekolahRelatedByKodeWilayah($lembagaNonSekolahRelatedByKodeWilayah)
    {
        $this->collLembagaNonSekolahsRelatedByKodeWilayah[]= $lembagaNonSekolahRelatedByKodeWilayah;
        $lembagaNonSekolahRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah($this);
    }

    /**
     * @param	LembagaNonSekolahRelatedByKodeWilayah $lembagaNonSekolahRelatedByKodeWilayah The lembagaNonSekolahRelatedByKodeWilayah object to remove.
     * @return MstWilayah The current object (for fluent API support)
     */
    public function removeLembagaNonSekolahRelatedByKodeWilayah($lembagaNonSekolahRelatedByKodeWilayah)
    {
        if ($this->getLembagaNonSekolahsRelatedByKodeWilayah()->contains($lembagaNonSekolahRelatedByKodeWilayah)) {
            $this->collLembagaNonSekolahsRelatedByKodeWilayah->remove($this->collLembagaNonSekolahsRelatedByKodeWilayah->search($lembagaNonSekolahRelatedByKodeWilayah));
            if (null === $this->lembagaNonSekolahsRelatedByKodeWilayahScheduledForDeletion) {
                $this->lembagaNonSekolahsRelatedByKodeWilayahScheduledForDeletion = clone $this->collLembagaNonSekolahsRelatedByKodeWilayah;
                $this->lembagaNonSekolahsRelatedByKodeWilayahScheduledForDeletion->clear();
            }
            $this->lembagaNonSekolahsRelatedByKodeWilayahScheduledForDeletion[]= clone $lembagaNonSekolahRelatedByKodeWilayah;
            $lembagaNonSekolahRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related LembagaNonSekolahsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|LembagaNonSekolah[] List of LembagaNonSekolah objects
     */
    public function getLembagaNonSekolahsRelatedByKodeWilayahJoinPenggunaRelatedByPenggunaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = LembagaNonSekolahQuery::create(null, $criteria);
        $query->joinWith('PenggunaRelatedByPenggunaId', $join_behavior);

        return $this->getLembagaNonSekolahsRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related LembagaNonSekolahsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|LembagaNonSekolah[] List of LembagaNonSekolah objects
     */
    public function getLembagaNonSekolahsRelatedByKodeWilayahJoinPenggunaRelatedByPenggunaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = LembagaNonSekolahQuery::create(null, $criteria);
        $query->joinWith('PenggunaRelatedByPenggunaId', $join_behavior);

        return $this->getLembagaNonSekolahsRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related LembagaNonSekolahsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|LembagaNonSekolah[] List of LembagaNonSekolah objects
     */
    public function getLembagaNonSekolahsRelatedByKodeWilayahJoinJenisLembagaRelatedByJenisLembagaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = LembagaNonSekolahQuery::create(null, $criteria);
        $query->joinWith('JenisLembagaRelatedByJenisLembagaId', $join_behavior);

        return $this->getLembagaNonSekolahsRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related LembagaNonSekolahsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|LembagaNonSekolah[] List of LembagaNonSekolah objects
     */
    public function getLembagaNonSekolahsRelatedByKodeWilayahJoinJenisLembagaRelatedByJenisLembagaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = LembagaNonSekolahQuery::create(null, $criteria);
        $query->joinWith('JenisLembagaRelatedByJenisLembagaId', $join_behavior);

        return $this->getLembagaNonSekolahsRelatedByKodeWilayah($query, $con);
    }

    /**
     * Clears out the collLembagaNonSekolahsRelatedByKodeWilayah collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return MstWilayah The current object (for fluent API support)
     * @see        addLembagaNonSekolahsRelatedByKodeWilayah()
     */
    public function clearLembagaNonSekolahsRelatedByKodeWilayah()
    {
        $this->collLembagaNonSekolahsRelatedByKodeWilayah = null; // important to set this to null since that means it is uninitialized
        $this->collLembagaNonSekolahsRelatedByKodeWilayahPartial = null;

        return $this;
    }

    /**
     * reset is the collLembagaNonSekolahsRelatedByKodeWilayah collection loaded partially
     *
     * @return void
     */
    public function resetPartialLembagaNonSekolahsRelatedByKodeWilayah($v = true)
    {
        $this->collLembagaNonSekolahsRelatedByKodeWilayahPartial = $v;
    }

    /**
     * Initializes the collLembagaNonSekolahsRelatedByKodeWilayah collection.
     *
     * By default this just sets the collLembagaNonSekolahsRelatedByKodeWilayah collection to an empty array (like clearcollLembagaNonSekolahsRelatedByKodeWilayah());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initLembagaNonSekolahsRelatedByKodeWilayah($overrideExisting = true)
    {
        if (null !== $this->collLembagaNonSekolahsRelatedByKodeWilayah && !$overrideExisting) {
            return;
        }
        $this->collLembagaNonSekolahsRelatedByKodeWilayah = new PropelObjectCollection();
        $this->collLembagaNonSekolahsRelatedByKodeWilayah->setModel('LembagaNonSekolah');
    }

    /**
     * Gets an array of LembagaNonSekolah objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this MstWilayah is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|LembagaNonSekolah[] List of LembagaNonSekolah objects
     * @throws PropelException
     */
    public function getLembagaNonSekolahsRelatedByKodeWilayah($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collLembagaNonSekolahsRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collLembagaNonSekolahsRelatedByKodeWilayah || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collLembagaNonSekolahsRelatedByKodeWilayah) {
                // return empty collection
                $this->initLembagaNonSekolahsRelatedByKodeWilayah();
            } else {
                $collLembagaNonSekolahsRelatedByKodeWilayah = LembagaNonSekolahQuery::create(null, $criteria)
                    ->filterByMstWilayahRelatedByKodeWilayah($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collLembagaNonSekolahsRelatedByKodeWilayahPartial && count($collLembagaNonSekolahsRelatedByKodeWilayah)) {
                      $this->initLembagaNonSekolahsRelatedByKodeWilayah(false);

                      foreach($collLembagaNonSekolahsRelatedByKodeWilayah as $obj) {
                        if (false == $this->collLembagaNonSekolahsRelatedByKodeWilayah->contains($obj)) {
                          $this->collLembagaNonSekolahsRelatedByKodeWilayah->append($obj);
                        }
                      }

                      $this->collLembagaNonSekolahsRelatedByKodeWilayahPartial = true;
                    }

                    $collLembagaNonSekolahsRelatedByKodeWilayah->getInternalIterator()->rewind();
                    return $collLembagaNonSekolahsRelatedByKodeWilayah;
                }

                if($partial && $this->collLembagaNonSekolahsRelatedByKodeWilayah) {
                    foreach($this->collLembagaNonSekolahsRelatedByKodeWilayah as $obj) {
                        if($obj->isNew()) {
                            $collLembagaNonSekolahsRelatedByKodeWilayah[] = $obj;
                        }
                    }
                }

                $this->collLembagaNonSekolahsRelatedByKodeWilayah = $collLembagaNonSekolahsRelatedByKodeWilayah;
                $this->collLembagaNonSekolahsRelatedByKodeWilayahPartial = false;
            }
        }

        return $this->collLembagaNonSekolahsRelatedByKodeWilayah;
    }

    /**
     * Sets a collection of LembagaNonSekolahRelatedByKodeWilayah objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $lembagaNonSekolahsRelatedByKodeWilayah A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setLembagaNonSekolahsRelatedByKodeWilayah(PropelCollection $lembagaNonSekolahsRelatedByKodeWilayah, PropelPDO $con = null)
    {
        $lembagaNonSekolahsRelatedByKodeWilayahToDelete = $this->getLembagaNonSekolahsRelatedByKodeWilayah(new Criteria(), $con)->diff($lembagaNonSekolahsRelatedByKodeWilayah);

        $this->lembagaNonSekolahsRelatedByKodeWilayahScheduledForDeletion = unserialize(serialize($lembagaNonSekolahsRelatedByKodeWilayahToDelete));

        foreach ($lembagaNonSekolahsRelatedByKodeWilayahToDelete as $lembagaNonSekolahRelatedByKodeWilayahRemoved) {
            $lembagaNonSekolahRelatedByKodeWilayahRemoved->setMstWilayahRelatedByKodeWilayah(null);
        }

        $this->collLembagaNonSekolahsRelatedByKodeWilayah = null;
        foreach ($lembagaNonSekolahsRelatedByKodeWilayah as $lembagaNonSekolahRelatedByKodeWilayah) {
            $this->addLembagaNonSekolahRelatedByKodeWilayah($lembagaNonSekolahRelatedByKodeWilayah);
        }

        $this->collLembagaNonSekolahsRelatedByKodeWilayah = $lembagaNonSekolahsRelatedByKodeWilayah;
        $this->collLembagaNonSekolahsRelatedByKodeWilayahPartial = false;

        return $this;
    }

    /**
     * Returns the number of related LembagaNonSekolah objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related LembagaNonSekolah objects.
     * @throws PropelException
     */
    public function countLembagaNonSekolahsRelatedByKodeWilayah(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collLembagaNonSekolahsRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collLembagaNonSekolahsRelatedByKodeWilayah || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collLembagaNonSekolahsRelatedByKodeWilayah) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getLembagaNonSekolahsRelatedByKodeWilayah());
            }
            $query = LembagaNonSekolahQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMstWilayahRelatedByKodeWilayah($this)
                ->count($con);
        }

        return count($this->collLembagaNonSekolahsRelatedByKodeWilayah);
    }

    /**
     * Method called to associate a LembagaNonSekolah object to this object
     * through the LembagaNonSekolah foreign key attribute.
     *
     * @param    LembagaNonSekolah $l LembagaNonSekolah
     * @return MstWilayah The current object (for fluent API support)
     */
    public function addLembagaNonSekolahRelatedByKodeWilayah(LembagaNonSekolah $l)
    {
        if ($this->collLembagaNonSekolahsRelatedByKodeWilayah === null) {
            $this->initLembagaNonSekolahsRelatedByKodeWilayah();
            $this->collLembagaNonSekolahsRelatedByKodeWilayahPartial = true;
        }
        if (!in_array($l, $this->collLembagaNonSekolahsRelatedByKodeWilayah->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddLembagaNonSekolahRelatedByKodeWilayah($l);
        }

        return $this;
    }

    /**
     * @param	LembagaNonSekolahRelatedByKodeWilayah $lembagaNonSekolahRelatedByKodeWilayah The lembagaNonSekolahRelatedByKodeWilayah object to add.
     */
    protected function doAddLembagaNonSekolahRelatedByKodeWilayah($lembagaNonSekolahRelatedByKodeWilayah)
    {
        $this->collLembagaNonSekolahsRelatedByKodeWilayah[]= $lembagaNonSekolahRelatedByKodeWilayah;
        $lembagaNonSekolahRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah($this);
    }

    /**
     * @param	LembagaNonSekolahRelatedByKodeWilayah $lembagaNonSekolahRelatedByKodeWilayah The lembagaNonSekolahRelatedByKodeWilayah object to remove.
     * @return MstWilayah The current object (for fluent API support)
     */
    public function removeLembagaNonSekolahRelatedByKodeWilayah($lembagaNonSekolahRelatedByKodeWilayah)
    {
        if ($this->getLembagaNonSekolahsRelatedByKodeWilayah()->contains($lembagaNonSekolahRelatedByKodeWilayah)) {
            $this->collLembagaNonSekolahsRelatedByKodeWilayah->remove($this->collLembagaNonSekolahsRelatedByKodeWilayah->search($lembagaNonSekolahRelatedByKodeWilayah));
            if (null === $this->lembagaNonSekolahsRelatedByKodeWilayahScheduledForDeletion) {
                $this->lembagaNonSekolahsRelatedByKodeWilayahScheduledForDeletion = clone $this->collLembagaNonSekolahsRelatedByKodeWilayah;
                $this->lembagaNonSekolahsRelatedByKodeWilayahScheduledForDeletion->clear();
            }
            $this->lembagaNonSekolahsRelatedByKodeWilayahScheduledForDeletion[]= clone $lembagaNonSekolahRelatedByKodeWilayah;
            $lembagaNonSekolahRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related LembagaNonSekolahsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|LembagaNonSekolah[] List of LembagaNonSekolah objects
     */
    public function getLembagaNonSekolahsRelatedByKodeWilayahJoinPenggunaRelatedByPenggunaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = LembagaNonSekolahQuery::create(null, $criteria);
        $query->joinWith('PenggunaRelatedByPenggunaId', $join_behavior);

        return $this->getLembagaNonSekolahsRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related LembagaNonSekolahsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|LembagaNonSekolah[] List of LembagaNonSekolah objects
     */
    public function getLembagaNonSekolahsRelatedByKodeWilayahJoinPenggunaRelatedByPenggunaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = LembagaNonSekolahQuery::create(null, $criteria);
        $query->joinWith('PenggunaRelatedByPenggunaId', $join_behavior);

        return $this->getLembagaNonSekolahsRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related LembagaNonSekolahsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|LembagaNonSekolah[] List of LembagaNonSekolah objects
     */
    public function getLembagaNonSekolahsRelatedByKodeWilayahJoinJenisLembagaRelatedByJenisLembagaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = LembagaNonSekolahQuery::create(null, $criteria);
        $query->joinWith('JenisLembagaRelatedByJenisLembagaId', $join_behavior);

        return $this->getLembagaNonSekolahsRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related LembagaNonSekolahsRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|LembagaNonSekolah[] List of LembagaNonSekolah objects
     */
    public function getLembagaNonSekolahsRelatedByKodeWilayahJoinJenisLembagaRelatedByJenisLembagaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = LembagaNonSekolahQuery::create(null, $criteria);
        $query->joinWith('JenisLembagaRelatedByJenisLembagaId', $join_behavior);

        return $this->getLembagaNonSekolahsRelatedByKodeWilayah($query, $con);
    }

    /**
     * Clears out the collDemografisRelatedByKodeWilayah collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return MstWilayah The current object (for fluent API support)
     * @see        addDemografisRelatedByKodeWilayah()
     */
    public function clearDemografisRelatedByKodeWilayah()
    {
        $this->collDemografisRelatedByKodeWilayah = null; // important to set this to null since that means it is uninitialized
        $this->collDemografisRelatedByKodeWilayahPartial = null;

        return $this;
    }

    /**
     * reset is the collDemografisRelatedByKodeWilayah collection loaded partially
     *
     * @return void
     */
    public function resetPartialDemografisRelatedByKodeWilayah($v = true)
    {
        $this->collDemografisRelatedByKodeWilayahPartial = $v;
    }

    /**
     * Initializes the collDemografisRelatedByKodeWilayah collection.
     *
     * By default this just sets the collDemografisRelatedByKodeWilayah collection to an empty array (like clearcollDemografisRelatedByKodeWilayah());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initDemografisRelatedByKodeWilayah($overrideExisting = true)
    {
        if (null !== $this->collDemografisRelatedByKodeWilayah && !$overrideExisting) {
            return;
        }
        $this->collDemografisRelatedByKodeWilayah = new PropelObjectCollection();
        $this->collDemografisRelatedByKodeWilayah->setModel('Demografi');
    }

    /**
     * Gets an array of Demografi objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this MstWilayah is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Demografi[] List of Demografi objects
     * @throws PropelException
     */
    public function getDemografisRelatedByKodeWilayah($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collDemografisRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collDemografisRelatedByKodeWilayah || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collDemografisRelatedByKodeWilayah) {
                // return empty collection
                $this->initDemografisRelatedByKodeWilayah();
            } else {
                $collDemografisRelatedByKodeWilayah = DemografiQuery::create(null, $criteria)
                    ->filterByMstWilayahRelatedByKodeWilayah($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collDemografisRelatedByKodeWilayahPartial && count($collDemografisRelatedByKodeWilayah)) {
                      $this->initDemografisRelatedByKodeWilayah(false);

                      foreach($collDemografisRelatedByKodeWilayah as $obj) {
                        if (false == $this->collDemografisRelatedByKodeWilayah->contains($obj)) {
                          $this->collDemografisRelatedByKodeWilayah->append($obj);
                        }
                      }

                      $this->collDemografisRelatedByKodeWilayahPartial = true;
                    }

                    $collDemografisRelatedByKodeWilayah->getInternalIterator()->rewind();
                    return $collDemografisRelatedByKodeWilayah;
                }

                if($partial && $this->collDemografisRelatedByKodeWilayah) {
                    foreach($this->collDemografisRelatedByKodeWilayah as $obj) {
                        if($obj->isNew()) {
                            $collDemografisRelatedByKodeWilayah[] = $obj;
                        }
                    }
                }

                $this->collDemografisRelatedByKodeWilayah = $collDemografisRelatedByKodeWilayah;
                $this->collDemografisRelatedByKodeWilayahPartial = false;
            }
        }

        return $this->collDemografisRelatedByKodeWilayah;
    }

    /**
     * Sets a collection of DemografiRelatedByKodeWilayah objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $demografisRelatedByKodeWilayah A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setDemografisRelatedByKodeWilayah(PropelCollection $demografisRelatedByKodeWilayah, PropelPDO $con = null)
    {
        $demografisRelatedByKodeWilayahToDelete = $this->getDemografisRelatedByKodeWilayah(new Criteria(), $con)->diff($demografisRelatedByKodeWilayah);

        $this->demografisRelatedByKodeWilayahScheduledForDeletion = unserialize(serialize($demografisRelatedByKodeWilayahToDelete));

        foreach ($demografisRelatedByKodeWilayahToDelete as $demografiRelatedByKodeWilayahRemoved) {
            $demografiRelatedByKodeWilayahRemoved->setMstWilayahRelatedByKodeWilayah(null);
        }

        $this->collDemografisRelatedByKodeWilayah = null;
        foreach ($demografisRelatedByKodeWilayah as $demografiRelatedByKodeWilayah) {
            $this->addDemografiRelatedByKodeWilayah($demografiRelatedByKodeWilayah);
        }

        $this->collDemografisRelatedByKodeWilayah = $demografisRelatedByKodeWilayah;
        $this->collDemografisRelatedByKodeWilayahPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Demografi objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Demografi objects.
     * @throws PropelException
     */
    public function countDemografisRelatedByKodeWilayah(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collDemografisRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collDemografisRelatedByKodeWilayah || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collDemografisRelatedByKodeWilayah) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getDemografisRelatedByKodeWilayah());
            }
            $query = DemografiQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMstWilayahRelatedByKodeWilayah($this)
                ->count($con);
        }

        return count($this->collDemografisRelatedByKodeWilayah);
    }

    /**
     * Method called to associate a Demografi object to this object
     * through the Demografi foreign key attribute.
     *
     * @param    Demografi $l Demografi
     * @return MstWilayah The current object (for fluent API support)
     */
    public function addDemografiRelatedByKodeWilayah(Demografi $l)
    {
        if ($this->collDemografisRelatedByKodeWilayah === null) {
            $this->initDemografisRelatedByKodeWilayah();
            $this->collDemografisRelatedByKodeWilayahPartial = true;
        }
        if (!in_array($l, $this->collDemografisRelatedByKodeWilayah->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddDemografiRelatedByKodeWilayah($l);
        }

        return $this;
    }

    /**
     * @param	DemografiRelatedByKodeWilayah $demografiRelatedByKodeWilayah The demografiRelatedByKodeWilayah object to add.
     */
    protected function doAddDemografiRelatedByKodeWilayah($demografiRelatedByKodeWilayah)
    {
        $this->collDemografisRelatedByKodeWilayah[]= $demografiRelatedByKodeWilayah;
        $demografiRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah($this);
    }

    /**
     * @param	DemografiRelatedByKodeWilayah $demografiRelatedByKodeWilayah The demografiRelatedByKodeWilayah object to remove.
     * @return MstWilayah The current object (for fluent API support)
     */
    public function removeDemografiRelatedByKodeWilayah($demografiRelatedByKodeWilayah)
    {
        if ($this->getDemografisRelatedByKodeWilayah()->contains($demografiRelatedByKodeWilayah)) {
            $this->collDemografisRelatedByKodeWilayah->remove($this->collDemografisRelatedByKodeWilayah->search($demografiRelatedByKodeWilayah));
            if (null === $this->demografisRelatedByKodeWilayahScheduledForDeletion) {
                $this->demografisRelatedByKodeWilayahScheduledForDeletion = clone $this->collDemografisRelatedByKodeWilayah;
                $this->demografisRelatedByKodeWilayahScheduledForDeletion->clear();
            }
            $this->demografisRelatedByKodeWilayahScheduledForDeletion[]= clone $demografiRelatedByKodeWilayah;
            $demografiRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related DemografisRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Demografi[] List of Demografi objects
     */
    public function getDemografisRelatedByKodeWilayahJoinTahunAjaranRelatedByTahunAjaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = DemografiQuery::create(null, $criteria);
        $query->joinWith('TahunAjaranRelatedByTahunAjaranId', $join_behavior);

        return $this->getDemografisRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related DemografisRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Demografi[] List of Demografi objects
     */
    public function getDemografisRelatedByKodeWilayahJoinTahunAjaranRelatedByTahunAjaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = DemografiQuery::create(null, $criteria);
        $query->joinWith('TahunAjaranRelatedByTahunAjaranId', $join_behavior);

        return $this->getDemografisRelatedByKodeWilayah($query, $con);
    }

    /**
     * Clears out the collDemografisRelatedByKodeWilayah collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return MstWilayah The current object (for fluent API support)
     * @see        addDemografisRelatedByKodeWilayah()
     */
    public function clearDemografisRelatedByKodeWilayah()
    {
        $this->collDemografisRelatedByKodeWilayah = null; // important to set this to null since that means it is uninitialized
        $this->collDemografisRelatedByKodeWilayahPartial = null;

        return $this;
    }

    /**
     * reset is the collDemografisRelatedByKodeWilayah collection loaded partially
     *
     * @return void
     */
    public function resetPartialDemografisRelatedByKodeWilayah($v = true)
    {
        $this->collDemografisRelatedByKodeWilayahPartial = $v;
    }

    /**
     * Initializes the collDemografisRelatedByKodeWilayah collection.
     *
     * By default this just sets the collDemografisRelatedByKodeWilayah collection to an empty array (like clearcollDemografisRelatedByKodeWilayah());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initDemografisRelatedByKodeWilayah($overrideExisting = true)
    {
        if (null !== $this->collDemografisRelatedByKodeWilayah && !$overrideExisting) {
            return;
        }
        $this->collDemografisRelatedByKodeWilayah = new PropelObjectCollection();
        $this->collDemografisRelatedByKodeWilayah->setModel('Demografi');
    }

    /**
     * Gets an array of Demografi objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this MstWilayah is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Demografi[] List of Demografi objects
     * @throws PropelException
     */
    public function getDemografisRelatedByKodeWilayah($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collDemografisRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collDemografisRelatedByKodeWilayah || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collDemografisRelatedByKodeWilayah) {
                // return empty collection
                $this->initDemografisRelatedByKodeWilayah();
            } else {
                $collDemografisRelatedByKodeWilayah = DemografiQuery::create(null, $criteria)
                    ->filterByMstWilayahRelatedByKodeWilayah($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collDemografisRelatedByKodeWilayahPartial && count($collDemografisRelatedByKodeWilayah)) {
                      $this->initDemografisRelatedByKodeWilayah(false);

                      foreach($collDemografisRelatedByKodeWilayah as $obj) {
                        if (false == $this->collDemografisRelatedByKodeWilayah->contains($obj)) {
                          $this->collDemografisRelatedByKodeWilayah->append($obj);
                        }
                      }

                      $this->collDemografisRelatedByKodeWilayahPartial = true;
                    }

                    $collDemografisRelatedByKodeWilayah->getInternalIterator()->rewind();
                    return $collDemografisRelatedByKodeWilayah;
                }

                if($partial && $this->collDemografisRelatedByKodeWilayah) {
                    foreach($this->collDemografisRelatedByKodeWilayah as $obj) {
                        if($obj->isNew()) {
                            $collDemografisRelatedByKodeWilayah[] = $obj;
                        }
                    }
                }

                $this->collDemografisRelatedByKodeWilayah = $collDemografisRelatedByKodeWilayah;
                $this->collDemografisRelatedByKodeWilayahPartial = false;
            }
        }

        return $this->collDemografisRelatedByKodeWilayah;
    }

    /**
     * Sets a collection of DemografiRelatedByKodeWilayah objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $demografisRelatedByKodeWilayah A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setDemografisRelatedByKodeWilayah(PropelCollection $demografisRelatedByKodeWilayah, PropelPDO $con = null)
    {
        $demografisRelatedByKodeWilayahToDelete = $this->getDemografisRelatedByKodeWilayah(new Criteria(), $con)->diff($demografisRelatedByKodeWilayah);

        $this->demografisRelatedByKodeWilayahScheduledForDeletion = unserialize(serialize($demografisRelatedByKodeWilayahToDelete));

        foreach ($demografisRelatedByKodeWilayahToDelete as $demografiRelatedByKodeWilayahRemoved) {
            $demografiRelatedByKodeWilayahRemoved->setMstWilayahRelatedByKodeWilayah(null);
        }

        $this->collDemografisRelatedByKodeWilayah = null;
        foreach ($demografisRelatedByKodeWilayah as $demografiRelatedByKodeWilayah) {
            $this->addDemografiRelatedByKodeWilayah($demografiRelatedByKodeWilayah);
        }

        $this->collDemografisRelatedByKodeWilayah = $demografisRelatedByKodeWilayah;
        $this->collDemografisRelatedByKodeWilayahPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Demografi objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Demografi objects.
     * @throws PropelException
     */
    public function countDemografisRelatedByKodeWilayah(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collDemografisRelatedByKodeWilayahPartial && !$this->isNew();
        if (null === $this->collDemografisRelatedByKodeWilayah || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collDemografisRelatedByKodeWilayah) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getDemografisRelatedByKodeWilayah());
            }
            $query = DemografiQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMstWilayahRelatedByKodeWilayah($this)
                ->count($con);
        }

        return count($this->collDemografisRelatedByKodeWilayah);
    }

    /**
     * Method called to associate a Demografi object to this object
     * through the Demografi foreign key attribute.
     *
     * @param    Demografi $l Demografi
     * @return MstWilayah The current object (for fluent API support)
     */
    public function addDemografiRelatedByKodeWilayah(Demografi $l)
    {
        if ($this->collDemografisRelatedByKodeWilayah === null) {
            $this->initDemografisRelatedByKodeWilayah();
            $this->collDemografisRelatedByKodeWilayahPartial = true;
        }
        if (!in_array($l, $this->collDemografisRelatedByKodeWilayah->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddDemografiRelatedByKodeWilayah($l);
        }

        return $this;
    }

    /**
     * @param	DemografiRelatedByKodeWilayah $demografiRelatedByKodeWilayah The demografiRelatedByKodeWilayah object to add.
     */
    protected function doAddDemografiRelatedByKodeWilayah($demografiRelatedByKodeWilayah)
    {
        $this->collDemografisRelatedByKodeWilayah[]= $demografiRelatedByKodeWilayah;
        $demografiRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah($this);
    }

    /**
     * @param	DemografiRelatedByKodeWilayah $demografiRelatedByKodeWilayah The demografiRelatedByKodeWilayah object to remove.
     * @return MstWilayah The current object (for fluent API support)
     */
    public function removeDemografiRelatedByKodeWilayah($demografiRelatedByKodeWilayah)
    {
        if ($this->getDemografisRelatedByKodeWilayah()->contains($demografiRelatedByKodeWilayah)) {
            $this->collDemografisRelatedByKodeWilayah->remove($this->collDemografisRelatedByKodeWilayah->search($demografiRelatedByKodeWilayah));
            if (null === $this->demografisRelatedByKodeWilayahScheduledForDeletion) {
                $this->demografisRelatedByKodeWilayahScheduledForDeletion = clone $this->collDemografisRelatedByKodeWilayah;
                $this->demografisRelatedByKodeWilayahScheduledForDeletion->clear();
            }
            $this->demografisRelatedByKodeWilayahScheduledForDeletion[]= clone $demografiRelatedByKodeWilayah;
            $demografiRelatedByKodeWilayah->setMstWilayahRelatedByKodeWilayah(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related DemografisRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Demografi[] List of Demografi objects
     */
    public function getDemografisRelatedByKodeWilayahJoinTahunAjaranRelatedByTahunAjaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = DemografiQuery::create(null, $criteria);
        $query->joinWith('TahunAjaranRelatedByTahunAjaranId', $join_behavior);

        return $this->getDemografisRelatedByKodeWilayah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MstWilayah is new, it will return
     * an empty collection; or if this MstWilayah has previously
     * been saved, it will retrieve related DemografisRelatedByKodeWilayah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MstWilayah.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Demografi[] List of Demografi objects
     */
    public function getDemografisRelatedByKodeWilayahJoinTahunAjaranRelatedByTahunAjaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = DemografiQuery::create(null, $criteria);
        $query->joinWith('TahunAjaranRelatedByTahunAjaranId', $join_behavior);

        return $this->getDemografisRelatedByKodeWilayah($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->kode_wilayah = null;
        $this->nama = null;
        $this->id_level_wilayah = null;
        $this->mst_kode_wilayah = null;
        $this->negara_id = null;
        $this->asal_wilayah = null;
        $this->kode_bps = null;
        $this->kode_dagri = null;
        $this->kode_keu = null;
        $this->create_date = null;
        $this->last_update = null;
        $this->expired_date = null;
        $this->last_sync = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collTetanggaKabkotasRelatedByKodeWilayah) {
                foreach ($this->collTetanggaKabkotasRelatedByKodeWilayah as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collTetanggaKabkotasRelatedByMstKodeWilayah) {
                foreach ($this->collTetanggaKabkotasRelatedByMstKodeWilayah as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPesertaDidiksRelatedByKodeWilayah) {
                foreach ($this->collPesertaDidiksRelatedByKodeWilayah as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPesertaDidiksRelatedByKodeWilayah) {
                foreach ($this->collPesertaDidiksRelatedByKodeWilayah as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collMstWilayahsRelatedByKodeWilayah) {
                foreach ($this->collMstWilayahsRelatedByKodeWilayah as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPenggunasRelatedByKodeWilayah) {
                foreach ($this->collPenggunasRelatedByKodeWilayah as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPenggunasRelatedByKodeWilayah) {
                foreach ($this->collPenggunasRelatedByKodeWilayah as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSekolahsRelatedByKodeWilayah) {
                foreach ($this->collSekolahsRelatedByKodeWilayah as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSekolahsRelatedByKodeWilayah) {
                foreach ($this->collSekolahsRelatedByKodeWilayah as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collYayasansRelatedByKodeWilayah) {
                foreach ($this->collYayasansRelatedByKodeWilayah as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collYayasansRelatedByKodeWilayah) {
                foreach ($this->collYayasansRelatedByKodeWilayah as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPtksRelatedByKodeWilayah) {
                foreach ($this->collPtksRelatedByKodeWilayah as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPtksRelatedByKodeWilayah) {
                foreach ($this->collPtksRelatedByKodeWilayah as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collWtSrcSyncLogsRelatedByKodeWilayah) {
                foreach ($this->collWtSrcSyncLogsRelatedByKodeWilayah as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collWtSrcSyncLogsRelatedByKodeWilayah) {
                foreach ($this->collWtSrcSyncLogsRelatedByKodeWilayah as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collWtDstSyncLogsRelatedByKodeWilayah) {
                foreach ($this->collWtDstSyncLogsRelatedByKodeWilayah as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collWtDstSyncLogsRelatedByKodeWilayah) {
                foreach ($this->collWtDstSyncLogsRelatedByKodeWilayah as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collWsyncSessionsRelatedByKodeWilayah) {
                foreach ($this->collWsyncSessionsRelatedByKodeWilayah as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collWsyncSessionsRelatedByKodeWilayah) {
                foreach ($this->collWsyncSessionsRelatedByKodeWilayah as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collWsrcSyncLogsRelatedByKodeWilayah) {
                foreach ($this->collWsrcSyncLogsRelatedByKodeWilayah as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collWsrcSyncLogsRelatedByKodeWilayah) {
                foreach ($this->collWsrcSyncLogsRelatedByKodeWilayah as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collWdstSyncLogsRelatedByKodeWilayah) {
                foreach ($this->collWdstSyncLogsRelatedByKodeWilayah as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collWdstSyncLogsRelatedByKodeWilayah) {
                foreach ($this->collWdstSyncLogsRelatedByKodeWilayah as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collWPendingJobsRelatedByKodeWilayah) {
                foreach ($this->collWPendingJobsRelatedByKodeWilayah as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collWPendingJobsRelatedByKodeWilayah) {
                foreach ($this->collWPendingJobsRelatedByKodeWilayah as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collLembagaNonSekolahsRelatedByKodeWilayah) {
                foreach ($this->collLembagaNonSekolahsRelatedByKodeWilayah as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collLembagaNonSekolahsRelatedByKodeWilayah) {
                foreach ($this->collLembagaNonSekolahsRelatedByKodeWilayah as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collDemografisRelatedByKodeWilayah) {
                foreach ($this->collDemografisRelatedByKodeWilayah as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collDemografisRelatedByKodeWilayah) {
                foreach ($this->collDemografisRelatedByKodeWilayah as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aLevelWilayah instanceof Persistent) {
              $this->aLevelWilayah->clearAllReferences($deep);
            }
            if ($this->aMstWilayahRelatedByMstKodeWilayah instanceof Persistent) {
              $this->aMstWilayahRelatedByMstKodeWilayah->clearAllReferences($deep);
            }
            if ($this->aNegara instanceof Persistent) {
              $this->aNegara->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collTetanggaKabkotasRelatedByKodeWilayah instanceof PropelCollection) {
            $this->collTetanggaKabkotasRelatedByKodeWilayah->clearIterator();
        }
        $this->collTetanggaKabkotasRelatedByKodeWilayah = null;
        if ($this->collTetanggaKabkotasRelatedByMstKodeWilayah instanceof PropelCollection) {
            $this->collTetanggaKabkotasRelatedByMstKodeWilayah->clearIterator();
        }
        $this->collTetanggaKabkotasRelatedByMstKodeWilayah = null;
        if ($this->collPesertaDidiksRelatedByKodeWilayah instanceof PropelCollection) {
            $this->collPesertaDidiksRelatedByKodeWilayah->clearIterator();
        }
        $this->collPesertaDidiksRelatedByKodeWilayah = null;
        if ($this->collPesertaDidiksRelatedByKodeWilayah instanceof PropelCollection) {
            $this->collPesertaDidiksRelatedByKodeWilayah->clearIterator();
        }
        $this->collPesertaDidiksRelatedByKodeWilayah = null;
        if ($this->collMstWilayahsRelatedByKodeWilayah instanceof PropelCollection) {
            $this->collMstWilayahsRelatedByKodeWilayah->clearIterator();
        }
        $this->collMstWilayahsRelatedByKodeWilayah = null;
        if ($this->collPenggunasRelatedByKodeWilayah instanceof PropelCollection) {
            $this->collPenggunasRelatedByKodeWilayah->clearIterator();
        }
        $this->collPenggunasRelatedByKodeWilayah = null;
        if ($this->collPenggunasRelatedByKodeWilayah instanceof PropelCollection) {
            $this->collPenggunasRelatedByKodeWilayah->clearIterator();
        }
        $this->collPenggunasRelatedByKodeWilayah = null;
        if ($this->collSekolahsRelatedByKodeWilayah instanceof PropelCollection) {
            $this->collSekolahsRelatedByKodeWilayah->clearIterator();
        }
        $this->collSekolahsRelatedByKodeWilayah = null;
        if ($this->collSekolahsRelatedByKodeWilayah instanceof PropelCollection) {
            $this->collSekolahsRelatedByKodeWilayah->clearIterator();
        }
        $this->collSekolahsRelatedByKodeWilayah = null;
        if ($this->collYayasansRelatedByKodeWilayah instanceof PropelCollection) {
            $this->collYayasansRelatedByKodeWilayah->clearIterator();
        }
        $this->collYayasansRelatedByKodeWilayah = null;
        if ($this->collYayasansRelatedByKodeWilayah instanceof PropelCollection) {
            $this->collYayasansRelatedByKodeWilayah->clearIterator();
        }
        $this->collYayasansRelatedByKodeWilayah = null;
        if ($this->collPtksRelatedByKodeWilayah instanceof PropelCollection) {
            $this->collPtksRelatedByKodeWilayah->clearIterator();
        }
        $this->collPtksRelatedByKodeWilayah = null;
        if ($this->collPtksRelatedByKodeWilayah instanceof PropelCollection) {
            $this->collPtksRelatedByKodeWilayah->clearIterator();
        }
        $this->collPtksRelatedByKodeWilayah = null;
        if ($this->collWtSrcSyncLogsRelatedByKodeWilayah instanceof PropelCollection) {
            $this->collWtSrcSyncLogsRelatedByKodeWilayah->clearIterator();
        }
        $this->collWtSrcSyncLogsRelatedByKodeWilayah = null;
        if ($this->collWtSrcSyncLogsRelatedByKodeWilayah instanceof PropelCollection) {
            $this->collWtSrcSyncLogsRelatedByKodeWilayah->clearIterator();
        }
        $this->collWtSrcSyncLogsRelatedByKodeWilayah = null;
        if ($this->collWtDstSyncLogsRelatedByKodeWilayah instanceof PropelCollection) {
            $this->collWtDstSyncLogsRelatedByKodeWilayah->clearIterator();
        }
        $this->collWtDstSyncLogsRelatedByKodeWilayah = null;
        if ($this->collWtDstSyncLogsRelatedByKodeWilayah instanceof PropelCollection) {
            $this->collWtDstSyncLogsRelatedByKodeWilayah->clearIterator();
        }
        $this->collWtDstSyncLogsRelatedByKodeWilayah = null;
        if ($this->collWsyncSessionsRelatedByKodeWilayah instanceof PropelCollection) {
            $this->collWsyncSessionsRelatedByKodeWilayah->clearIterator();
        }
        $this->collWsyncSessionsRelatedByKodeWilayah = null;
        if ($this->collWsyncSessionsRelatedByKodeWilayah instanceof PropelCollection) {
            $this->collWsyncSessionsRelatedByKodeWilayah->clearIterator();
        }
        $this->collWsyncSessionsRelatedByKodeWilayah = null;
        if ($this->collWsrcSyncLogsRelatedByKodeWilayah instanceof PropelCollection) {
            $this->collWsrcSyncLogsRelatedByKodeWilayah->clearIterator();
        }
        $this->collWsrcSyncLogsRelatedByKodeWilayah = null;
        if ($this->collWsrcSyncLogsRelatedByKodeWilayah instanceof PropelCollection) {
            $this->collWsrcSyncLogsRelatedByKodeWilayah->clearIterator();
        }
        $this->collWsrcSyncLogsRelatedByKodeWilayah = null;
        if ($this->collWdstSyncLogsRelatedByKodeWilayah instanceof PropelCollection) {
            $this->collWdstSyncLogsRelatedByKodeWilayah->clearIterator();
        }
        $this->collWdstSyncLogsRelatedByKodeWilayah = null;
        if ($this->collWdstSyncLogsRelatedByKodeWilayah instanceof PropelCollection) {
            $this->collWdstSyncLogsRelatedByKodeWilayah->clearIterator();
        }
        $this->collWdstSyncLogsRelatedByKodeWilayah = null;
        if ($this->collWPendingJobsRelatedByKodeWilayah instanceof PropelCollection) {
            $this->collWPendingJobsRelatedByKodeWilayah->clearIterator();
        }
        $this->collWPendingJobsRelatedByKodeWilayah = null;
        if ($this->collWPendingJobsRelatedByKodeWilayah instanceof PropelCollection) {
            $this->collWPendingJobsRelatedByKodeWilayah->clearIterator();
        }
        $this->collWPendingJobsRelatedByKodeWilayah = null;
        if ($this->collLembagaNonSekolahsRelatedByKodeWilayah instanceof PropelCollection) {
            $this->collLembagaNonSekolahsRelatedByKodeWilayah->clearIterator();
        }
        $this->collLembagaNonSekolahsRelatedByKodeWilayah = null;
        if ($this->collLembagaNonSekolahsRelatedByKodeWilayah instanceof PropelCollection) {
            $this->collLembagaNonSekolahsRelatedByKodeWilayah->clearIterator();
        }
        $this->collLembagaNonSekolahsRelatedByKodeWilayah = null;
        if ($this->collDemografisRelatedByKodeWilayah instanceof PropelCollection) {
            $this->collDemografisRelatedByKodeWilayah->clearIterator();
        }
        $this->collDemografisRelatedByKodeWilayah = null;
        if ($this->collDemografisRelatedByKodeWilayah instanceof PropelCollection) {
            $this->collDemografisRelatedByKodeWilayah->clearIterator();
        }
        $this->collDemografisRelatedByKodeWilayah = null;
        $this->aLevelWilayah = null;
        $this->aMstWilayahRelatedByMstKodeWilayah = null;
        $this->aNegara = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(MstWilayahPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
