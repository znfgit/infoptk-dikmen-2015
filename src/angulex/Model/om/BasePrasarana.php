<?php

namespace angulex\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelDateTime;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\BukuAlat;
use angulex\Model\BukuAlatQuery;
use angulex\Model\JenisPrasarana;
use angulex\Model\JenisPrasaranaQuery;
use angulex\Model\Prasarana;
use angulex\Model\PrasaranaLongitudinal;
use angulex\Model\PrasaranaLongitudinalQuery;
use angulex\Model\PrasaranaPeer;
use angulex\Model\PrasaranaQuery;
use angulex\Model\RombonganBelajar;
use angulex\Model\RombonganBelajarQuery;
use angulex\Model\Sarana;
use angulex\Model\SaranaQuery;
use angulex\Model\Sekolah;
use angulex\Model\SekolahQuery;
use angulex\Model\StatusKepemilikanSarpras;
use angulex\Model\StatusKepemilikanSarprasQuery;
use angulex\Model\VldPrasarana;
use angulex\Model\VldPrasaranaQuery;

/**
 * Base class that represents a row from the 'prasarana' table.
 *
 * 
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BasePrasarana extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'angulex\\Model\\PrasaranaPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        PrasaranaPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the prasarana_id field.
     * @var        string
     */
    protected $prasarana_id;

    /**
     * The value for the jenis_prasarana_id field.
     * @var        int
     */
    protected $jenis_prasarana_id;

    /**
     * The value for the sekolah_id field.
     * @var        string
     */
    protected $sekolah_id;

    /**
     * The value for the kepemilikan_sarpras_id field.
     * @var        string
     */
    protected $kepemilikan_sarpras_id;

    /**
     * The value for the nama field.
     * @var        string
     */
    protected $nama;

    /**
     * The value for the panjang field.
     * @var        double
     */
    protected $panjang;

    /**
     * The value for the lebar field.
     * @var        double
     */
    protected $lebar;

    /**
     * The value for the last_update field.
     * @var        string
     */
    protected $last_update;

    /**
     * The value for the soft_delete field.
     * @var        string
     */
    protected $soft_delete;

    /**
     * The value for the last_sync field.
     * @var        string
     */
    protected $last_sync;

    /**
     * The value for the updater_id field.
     * @var        string
     */
    protected $updater_id;

    /**
     * @var        Sekolah
     */
    protected $aSekolahRelatedBySekolahId;

    /**
     * @var        Sekolah
     */
    protected $aSekolahRelatedBySekolahId;

    /**
     * @var        JenisPrasarana
     */
    protected $aJenisPrasaranaRelatedByJenisPrasaranaId;

    /**
     * @var        JenisPrasarana
     */
    protected $aJenisPrasaranaRelatedByJenisPrasaranaId;

    /**
     * @var        StatusKepemilikanSarpras
     */
    protected $aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId;

    /**
     * @var        StatusKepemilikanSarpras
     */
    protected $aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId;

    /**
     * @var        PropelObjectCollection|BukuAlat[] Collection to store aggregation of BukuAlat objects.
     */
    protected $collBukuAlatsRelatedByPrasaranaId;
    protected $collBukuAlatsRelatedByPrasaranaIdPartial;

    /**
     * @var        PropelObjectCollection|BukuAlat[] Collection to store aggregation of BukuAlat objects.
     */
    protected $collBukuAlatsRelatedByPrasaranaId;
    protected $collBukuAlatsRelatedByPrasaranaIdPartial;

    /**
     * @var        PropelObjectCollection|VldPrasarana[] Collection to store aggregation of VldPrasarana objects.
     */
    protected $collVldPrasaranasRelatedByPrasaranaId;
    protected $collVldPrasaranasRelatedByPrasaranaIdPartial;

    /**
     * @var        PropelObjectCollection|VldPrasarana[] Collection to store aggregation of VldPrasarana objects.
     */
    protected $collVldPrasaranasRelatedByPrasaranaId;
    protected $collVldPrasaranasRelatedByPrasaranaIdPartial;

    /**
     * @var        PropelObjectCollection|Sarana[] Collection to store aggregation of Sarana objects.
     */
    protected $collSaranasRelatedByPrasaranaId;
    protected $collSaranasRelatedByPrasaranaIdPartial;

    /**
     * @var        PropelObjectCollection|Sarana[] Collection to store aggregation of Sarana objects.
     */
    protected $collSaranasRelatedByPrasaranaId;
    protected $collSaranasRelatedByPrasaranaIdPartial;

    /**
     * @var        PropelObjectCollection|PrasaranaLongitudinal[] Collection to store aggregation of PrasaranaLongitudinal objects.
     */
    protected $collPrasaranaLongitudinalsRelatedByPrasaranaId;
    protected $collPrasaranaLongitudinalsRelatedByPrasaranaIdPartial;

    /**
     * @var        PropelObjectCollection|PrasaranaLongitudinal[] Collection to store aggregation of PrasaranaLongitudinal objects.
     */
    protected $collPrasaranaLongitudinalsRelatedByPrasaranaId;
    protected $collPrasaranaLongitudinalsRelatedByPrasaranaIdPartial;

    /**
     * @var        PropelObjectCollection|RombonganBelajar[] Collection to store aggregation of RombonganBelajar objects.
     */
    protected $collRombonganBelajarsRelatedByPrasaranaId;
    protected $collRombonganBelajarsRelatedByPrasaranaIdPartial;

    /**
     * @var        PropelObjectCollection|RombonganBelajar[] Collection to store aggregation of RombonganBelajar objects.
     */
    protected $collRombonganBelajarsRelatedByPrasaranaId;
    protected $collRombonganBelajarsRelatedByPrasaranaIdPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $bukuAlatsRelatedByPrasaranaIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $bukuAlatsRelatedByPrasaranaIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldPrasaranasRelatedByPrasaranaIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldPrasaranasRelatedByPrasaranaIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $saranasRelatedByPrasaranaIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $saranasRelatedByPrasaranaIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $prasaranaLongitudinalsRelatedByPrasaranaIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $prasaranaLongitudinalsRelatedByPrasaranaIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rombonganBelajarsRelatedByPrasaranaIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rombonganBelajarsRelatedByPrasaranaIdScheduledForDeletion = null;

    /**
     * Get the [prasarana_id] column value.
     * 
     * @return string
     */
    public function getPrasaranaId()
    {
        return $this->prasarana_id;
    }

    /**
     * Get the [jenis_prasarana_id] column value.
     * 
     * @return int
     */
    public function getJenisPrasaranaId()
    {
        return $this->jenis_prasarana_id;
    }

    /**
     * Get the [sekolah_id] column value.
     * 
     * @return string
     */
    public function getSekolahId()
    {
        return $this->sekolah_id;
    }

    /**
     * Get the [kepemilikan_sarpras_id] column value.
     * 
     * @return string
     */
    public function getKepemilikanSarprasId()
    {
        return $this->kepemilikan_sarpras_id;
    }

    /**
     * Get the [nama] column value.
     * 
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Get the [panjang] column value.
     * 
     * @return double
     */
    public function getPanjang()
    {
        return $this->panjang;
    }

    /**
     * Get the [lebar] column value.
     * 
     * @return double
     */
    public function getLebar()
    {
        return $this->lebar;
    }

    /**
     * Get the [optionally formatted] temporal [last_update] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastUpdate($format = 'Y-m-d H:i:s')
    {
        if ($this->last_update === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_update);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_update, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [soft_delete] column value.
     * 
     * @return string
     */
    public function getSoftDelete()
    {
        return $this->soft_delete;
    }

    /**
     * Get the [optionally formatted] temporal [last_sync] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastSync($format = 'Y-m-d H:i:s')
    {
        if ($this->last_sync === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_sync);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_sync, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [updater_id] column value.
     * 
     * @return string
     */
    public function getUpdaterId()
    {
        return $this->updater_id;
    }

    /**
     * Set the value of [prasarana_id] column.
     * 
     * @param string $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setPrasaranaId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->prasarana_id !== $v) {
            $this->prasarana_id = $v;
            $this->modifiedColumns[] = PrasaranaPeer::PRASARANA_ID;
        }


        return $this;
    } // setPrasaranaId()

    /**
     * Set the value of [jenis_prasarana_id] column.
     * 
     * @param int $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setJenisPrasaranaId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->jenis_prasarana_id !== $v) {
            $this->jenis_prasarana_id = $v;
            $this->modifiedColumns[] = PrasaranaPeer::JENIS_PRASARANA_ID;
        }

        if ($this->aJenisPrasaranaRelatedByJenisPrasaranaId !== null && $this->aJenisPrasaranaRelatedByJenisPrasaranaId->getJenisPrasaranaId() !== $v) {
            $this->aJenisPrasaranaRelatedByJenisPrasaranaId = null;
        }

        if ($this->aJenisPrasaranaRelatedByJenisPrasaranaId !== null && $this->aJenisPrasaranaRelatedByJenisPrasaranaId->getJenisPrasaranaId() !== $v) {
            $this->aJenisPrasaranaRelatedByJenisPrasaranaId = null;
        }


        return $this;
    } // setJenisPrasaranaId()

    /**
     * Set the value of [sekolah_id] column.
     * 
     * @param string $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setSekolahId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->sekolah_id !== $v) {
            $this->sekolah_id = $v;
            $this->modifiedColumns[] = PrasaranaPeer::SEKOLAH_ID;
        }

        if ($this->aSekolahRelatedBySekolahId !== null && $this->aSekolahRelatedBySekolahId->getSekolahId() !== $v) {
            $this->aSekolahRelatedBySekolahId = null;
        }

        if ($this->aSekolahRelatedBySekolahId !== null && $this->aSekolahRelatedBySekolahId->getSekolahId() !== $v) {
            $this->aSekolahRelatedBySekolahId = null;
        }


        return $this;
    } // setSekolahId()

    /**
     * Set the value of [kepemilikan_sarpras_id] column.
     * 
     * @param string $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setKepemilikanSarprasId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->kepemilikan_sarpras_id !== $v) {
            $this->kepemilikan_sarpras_id = $v;
            $this->modifiedColumns[] = PrasaranaPeer::KEPEMILIKAN_SARPRAS_ID;
        }

        if ($this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId !== null && $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId->getKepemilikanSarprasId() !== $v) {
            $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId = null;
        }

        if ($this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId !== null && $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId->getKepemilikanSarprasId() !== $v) {
            $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId = null;
        }


        return $this;
    } // setKepemilikanSarprasId()

    /**
     * Set the value of [nama] column.
     * 
     * @param string $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setNama($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nama !== $v) {
            $this->nama = $v;
            $this->modifiedColumns[] = PrasaranaPeer::NAMA;
        }


        return $this;
    } // setNama()

    /**
     * Set the value of [panjang] column.
     * 
     * @param double $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setPanjang($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->panjang !== $v) {
            $this->panjang = $v;
            $this->modifiedColumns[] = PrasaranaPeer::PANJANG;
        }


        return $this;
    } // setPanjang()

    /**
     * Set the value of [lebar] column.
     * 
     * @param double $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setLebar($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->lebar !== $v) {
            $this->lebar = $v;
            $this->modifiedColumns[] = PrasaranaPeer::LEBAR;
        }


        return $this;
    } // setLebar()

    /**
     * Sets the value of [last_update] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Prasarana The current object (for fluent API support)
     */
    public function setLastUpdate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_update !== null || $dt !== null) {
            $currentDateAsString = ($this->last_update !== null && $tmpDt = new DateTime($this->last_update)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_update = $newDateAsString;
                $this->modifiedColumns[] = PrasaranaPeer::LAST_UPDATE;
            }
        } // if either are not null


        return $this;
    } // setLastUpdate()

    /**
     * Set the value of [soft_delete] column.
     * 
     * @param string $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setSoftDelete($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->soft_delete !== $v) {
            $this->soft_delete = $v;
            $this->modifiedColumns[] = PrasaranaPeer::SOFT_DELETE;
        }


        return $this;
    } // setSoftDelete()

    /**
     * Sets the value of [last_sync] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Prasarana The current object (for fluent API support)
     */
    public function setLastSync($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_sync !== null || $dt !== null) {
            $currentDateAsString = ($this->last_sync !== null && $tmpDt = new DateTime($this->last_sync)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_sync = $newDateAsString;
                $this->modifiedColumns[] = PrasaranaPeer::LAST_SYNC;
            }
        } // if either are not null


        return $this;
    } // setLastSync()

    /**
     * Set the value of [updater_id] column.
     * 
     * @param string $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setUpdaterId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->updater_id !== $v) {
            $this->updater_id = $v;
            $this->modifiedColumns[] = PrasaranaPeer::UPDATER_ID;
        }


        return $this;
    } // setUpdaterId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->prasarana_id = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
            $this->jenis_prasarana_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->sekolah_id = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->kepemilikan_sarpras_id = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->nama = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->panjang = ($row[$startcol + 5] !== null) ? (double) $row[$startcol + 5] : null;
            $this->lebar = ($row[$startcol + 6] !== null) ? (double) $row[$startcol + 6] : null;
            $this->last_update = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->soft_delete = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->last_sync = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->updater_id = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 11; // 11 = PrasaranaPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Prasarana object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aJenisPrasaranaRelatedByJenisPrasaranaId !== null && $this->jenis_prasarana_id !== $this->aJenisPrasaranaRelatedByJenisPrasaranaId->getJenisPrasaranaId()) {
            $this->aJenisPrasaranaRelatedByJenisPrasaranaId = null;
        }
        if ($this->aJenisPrasaranaRelatedByJenisPrasaranaId !== null && $this->jenis_prasarana_id !== $this->aJenisPrasaranaRelatedByJenisPrasaranaId->getJenisPrasaranaId()) {
            $this->aJenisPrasaranaRelatedByJenisPrasaranaId = null;
        }
        if ($this->aSekolahRelatedBySekolahId !== null && $this->sekolah_id !== $this->aSekolahRelatedBySekolahId->getSekolahId()) {
            $this->aSekolahRelatedBySekolahId = null;
        }
        if ($this->aSekolahRelatedBySekolahId !== null && $this->sekolah_id !== $this->aSekolahRelatedBySekolahId->getSekolahId()) {
            $this->aSekolahRelatedBySekolahId = null;
        }
        if ($this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId !== null && $this->kepemilikan_sarpras_id !== $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId->getKepemilikanSarprasId()) {
            $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId = null;
        }
        if ($this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId !== null && $this->kepemilikan_sarpras_id !== $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId->getKepemilikanSarprasId()) {
            $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PrasaranaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = PrasaranaPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aSekolahRelatedBySekolahId = null;
            $this->aSekolahRelatedBySekolahId = null;
            $this->aJenisPrasaranaRelatedByJenisPrasaranaId = null;
            $this->aJenisPrasaranaRelatedByJenisPrasaranaId = null;
            $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId = null;
            $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId = null;
            $this->collBukuAlatsRelatedByPrasaranaId = null;

            $this->collBukuAlatsRelatedByPrasaranaId = null;

            $this->collVldPrasaranasRelatedByPrasaranaId = null;

            $this->collVldPrasaranasRelatedByPrasaranaId = null;

            $this->collSaranasRelatedByPrasaranaId = null;

            $this->collSaranasRelatedByPrasaranaId = null;

            $this->collPrasaranaLongitudinalsRelatedByPrasaranaId = null;

            $this->collPrasaranaLongitudinalsRelatedByPrasaranaId = null;

            $this->collRombonganBelajarsRelatedByPrasaranaId = null;

            $this->collRombonganBelajarsRelatedByPrasaranaId = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PrasaranaPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = PrasaranaQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PrasaranaPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                PrasaranaPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if ($this->aSekolahRelatedBySekolahId->isModified() || $this->aSekolahRelatedBySekolahId->isNew()) {
                    $affectedRows += $this->aSekolahRelatedBySekolahId->save($con);
                }
                $this->setSekolahRelatedBySekolahId($this->aSekolahRelatedBySekolahId);
            }

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if ($this->aSekolahRelatedBySekolahId->isModified() || $this->aSekolahRelatedBySekolahId->isNew()) {
                    $affectedRows += $this->aSekolahRelatedBySekolahId->save($con);
                }
                $this->setSekolahRelatedBySekolahId($this->aSekolahRelatedBySekolahId);
            }

            if ($this->aJenisPrasaranaRelatedByJenisPrasaranaId !== null) {
                if ($this->aJenisPrasaranaRelatedByJenisPrasaranaId->isModified() || $this->aJenisPrasaranaRelatedByJenisPrasaranaId->isNew()) {
                    $affectedRows += $this->aJenisPrasaranaRelatedByJenisPrasaranaId->save($con);
                }
                $this->setJenisPrasaranaRelatedByJenisPrasaranaId($this->aJenisPrasaranaRelatedByJenisPrasaranaId);
            }

            if ($this->aJenisPrasaranaRelatedByJenisPrasaranaId !== null) {
                if ($this->aJenisPrasaranaRelatedByJenisPrasaranaId->isModified() || $this->aJenisPrasaranaRelatedByJenisPrasaranaId->isNew()) {
                    $affectedRows += $this->aJenisPrasaranaRelatedByJenisPrasaranaId->save($con);
                }
                $this->setJenisPrasaranaRelatedByJenisPrasaranaId($this->aJenisPrasaranaRelatedByJenisPrasaranaId);
            }

            if ($this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId !== null) {
                if ($this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId->isModified() || $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId->isNew()) {
                    $affectedRows += $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId->save($con);
                }
                $this->setStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId);
            }

            if ($this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId !== null) {
                if ($this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId->isModified() || $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId->isNew()) {
                    $affectedRows += $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId->save($con);
                }
                $this->setStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->bukuAlatsRelatedByPrasaranaIdScheduledForDeletion !== null) {
                if (!$this->bukuAlatsRelatedByPrasaranaIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->bukuAlatsRelatedByPrasaranaIdScheduledForDeletion as $bukuAlatRelatedByPrasaranaId) {
                        // need to save related object because we set the relation to null
                        $bukuAlatRelatedByPrasaranaId->save($con);
                    }
                    $this->bukuAlatsRelatedByPrasaranaIdScheduledForDeletion = null;
                }
            }

            if ($this->collBukuAlatsRelatedByPrasaranaId !== null) {
                foreach ($this->collBukuAlatsRelatedByPrasaranaId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->bukuAlatsRelatedByPrasaranaIdScheduledForDeletion !== null) {
                if (!$this->bukuAlatsRelatedByPrasaranaIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->bukuAlatsRelatedByPrasaranaIdScheduledForDeletion as $bukuAlatRelatedByPrasaranaId) {
                        // need to save related object because we set the relation to null
                        $bukuAlatRelatedByPrasaranaId->save($con);
                    }
                    $this->bukuAlatsRelatedByPrasaranaIdScheduledForDeletion = null;
                }
            }

            if ($this->collBukuAlatsRelatedByPrasaranaId !== null) {
                foreach ($this->collBukuAlatsRelatedByPrasaranaId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldPrasaranasRelatedByPrasaranaIdScheduledForDeletion !== null) {
                if (!$this->vldPrasaranasRelatedByPrasaranaIdScheduledForDeletion->isEmpty()) {
                    VldPrasaranaQuery::create()
                        ->filterByPrimaryKeys($this->vldPrasaranasRelatedByPrasaranaIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldPrasaranasRelatedByPrasaranaIdScheduledForDeletion = null;
                }
            }

            if ($this->collVldPrasaranasRelatedByPrasaranaId !== null) {
                foreach ($this->collVldPrasaranasRelatedByPrasaranaId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldPrasaranasRelatedByPrasaranaIdScheduledForDeletion !== null) {
                if (!$this->vldPrasaranasRelatedByPrasaranaIdScheduledForDeletion->isEmpty()) {
                    VldPrasaranaQuery::create()
                        ->filterByPrimaryKeys($this->vldPrasaranasRelatedByPrasaranaIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldPrasaranasRelatedByPrasaranaIdScheduledForDeletion = null;
                }
            }

            if ($this->collVldPrasaranasRelatedByPrasaranaId !== null) {
                foreach ($this->collVldPrasaranasRelatedByPrasaranaId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->saranasRelatedByPrasaranaIdScheduledForDeletion !== null) {
                if (!$this->saranasRelatedByPrasaranaIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->saranasRelatedByPrasaranaIdScheduledForDeletion as $saranaRelatedByPrasaranaId) {
                        // need to save related object because we set the relation to null
                        $saranaRelatedByPrasaranaId->save($con);
                    }
                    $this->saranasRelatedByPrasaranaIdScheduledForDeletion = null;
                }
            }

            if ($this->collSaranasRelatedByPrasaranaId !== null) {
                foreach ($this->collSaranasRelatedByPrasaranaId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->saranasRelatedByPrasaranaIdScheduledForDeletion !== null) {
                if (!$this->saranasRelatedByPrasaranaIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->saranasRelatedByPrasaranaIdScheduledForDeletion as $saranaRelatedByPrasaranaId) {
                        // need to save related object because we set the relation to null
                        $saranaRelatedByPrasaranaId->save($con);
                    }
                    $this->saranasRelatedByPrasaranaIdScheduledForDeletion = null;
                }
            }

            if ($this->collSaranasRelatedByPrasaranaId !== null) {
                foreach ($this->collSaranasRelatedByPrasaranaId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->prasaranaLongitudinalsRelatedByPrasaranaIdScheduledForDeletion !== null) {
                if (!$this->prasaranaLongitudinalsRelatedByPrasaranaIdScheduledForDeletion->isEmpty()) {
                    PrasaranaLongitudinalQuery::create()
                        ->filterByPrimaryKeys($this->prasaranaLongitudinalsRelatedByPrasaranaIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->prasaranaLongitudinalsRelatedByPrasaranaIdScheduledForDeletion = null;
                }
            }

            if ($this->collPrasaranaLongitudinalsRelatedByPrasaranaId !== null) {
                foreach ($this->collPrasaranaLongitudinalsRelatedByPrasaranaId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->prasaranaLongitudinalsRelatedByPrasaranaIdScheduledForDeletion !== null) {
                if (!$this->prasaranaLongitudinalsRelatedByPrasaranaIdScheduledForDeletion->isEmpty()) {
                    PrasaranaLongitudinalQuery::create()
                        ->filterByPrimaryKeys($this->prasaranaLongitudinalsRelatedByPrasaranaIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->prasaranaLongitudinalsRelatedByPrasaranaIdScheduledForDeletion = null;
                }
            }

            if ($this->collPrasaranaLongitudinalsRelatedByPrasaranaId !== null) {
                foreach ($this->collPrasaranaLongitudinalsRelatedByPrasaranaId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rombonganBelajarsRelatedByPrasaranaIdScheduledForDeletion !== null) {
                if (!$this->rombonganBelajarsRelatedByPrasaranaIdScheduledForDeletion->isEmpty()) {
                    RombonganBelajarQuery::create()
                        ->filterByPrimaryKeys($this->rombonganBelajarsRelatedByPrasaranaIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rombonganBelajarsRelatedByPrasaranaIdScheduledForDeletion = null;
                }
            }

            if ($this->collRombonganBelajarsRelatedByPrasaranaId !== null) {
                foreach ($this->collRombonganBelajarsRelatedByPrasaranaId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rombonganBelajarsRelatedByPrasaranaIdScheduledForDeletion !== null) {
                if (!$this->rombonganBelajarsRelatedByPrasaranaIdScheduledForDeletion->isEmpty()) {
                    RombonganBelajarQuery::create()
                        ->filterByPrimaryKeys($this->rombonganBelajarsRelatedByPrasaranaIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rombonganBelajarsRelatedByPrasaranaIdScheduledForDeletion = null;
                }
            }

            if ($this->collRombonganBelajarsRelatedByPrasaranaId !== null) {
                foreach ($this->collRombonganBelajarsRelatedByPrasaranaId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $criteria = $this->buildCriteria();
        $pk = BasePeer::doInsert($criteria, $con);
        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if (!$this->aSekolahRelatedBySekolahId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSekolahRelatedBySekolahId->getValidationFailures());
                }
            }

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if (!$this->aSekolahRelatedBySekolahId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSekolahRelatedBySekolahId->getValidationFailures());
                }
            }

            if ($this->aJenisPrasaranaRelatedByJenisPrasaranaId !== null) {
                if (!$this->aJenisPrasaranaRelatedByJenisPrasaranaId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aJenisPrasaranaRelatedByJenisPrasaranaId->getValidationFailures());
                }
            }

            if ($this->aJenisPrasaranaRelatedByJenisPrasaranaId !== null) {
                if (!$this->aJenisPrasaranaRelatedByJenisPrasaranaId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aJenisPrasaranaRelatedByJenisPrasaranaId->getValidationFailures());
                }
            }

            if ($this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId !== null) {
                if (!$this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId->getValidationFailures());
                }
            }

            if ($this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId !== null) {
                if (!$this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId->getValidationFailures());
                }
            }


            if (($retval = PrasaranaPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collBukuAlatsRelatedByPrasaranaId !== null) {
                    foreach ($this->collBukuAlatsRelatedByPrasaranaId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collBukuAlatsRelatedByPrasaranaId !== null) {
                    foreach ($this->collBukuAlatsRelatedByPrasaranaId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldPrasaranasRelatedByPrasaranaId !== null) {
                    foreach ($this->collVldPrasaranasRelatedByPrasaranaId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldPrasaranasRelatedByPrasaranaId !== null) {
                    foreach ($this->collVldPrasaranasRelatedByPrasaranaId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSaranasRelatedByPrasaranaId !== null) {
                    foreach ($this->collSaranasRelatedByPrasaranaId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSaranasRelatedByPrasaranaId !== null) {
                    foreach ($this->collSaranasRelatedByPrasaranaId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPrasaranaLongitudinalsRelatedByPrasaranaId !== null) {
                    foreach ($this->collPrasaranaLongitudinalsRelatedByPrasaranaId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPrasaranaLongitudinalsRelatedByPrasaranaId !== null) {
                    foreach ($this->collPrasaranaLongitudinalsRelatedByPrasaranaId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRombonganBelajarsRelatedByPrasaranaId !== null) {
                    foreach ($this->collRombonganBelajarsRelatedByPrasaranaId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRombonganBelajarsRelatedByPrasaranaId !== null) {
                    foreach ($this->collRombonganBelajarsRelatedByPrasaranaId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = PrasaranaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getPrasaranaId();
                break;
            case 1:
                return $this->getJenisPrasaranaId();
                break;
            case 2:
                return $this->getSekolahId();
                break;
            case 3:
                return $this->getKepemilikanSarprasId();
                break;
            case 4:
                return $this->getNama();
                break;
            case 5:
                return $this->getPanjang();
                break;
            case 6:
                return $this->getLebar();
                break;
            case 7:
                return $this->getLastUpdate();
                break;
            case 8:
                return $this->getSoftDelete();
                break;
            case 9:
                return $this->getLastSync();
                break;
            case 10:
                return $this->getUpdaterId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Prasarana'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Prasarana'][$this->getPrimaryKey()] = true;
        $keys = PrasaranaPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getPrasaranaId(),
            $keys[1] => $this->getJenisPrasaranaId(),
            $keys[2] => $this->getSekolahId(),
            $keys[3] => $this->getKepemilikanSarprasId(),
            $keys[4] => $this->getNama(),
            $keys[5] => $this->getPanjang(),
            $keys[6] => $this->getLebar(),
            $keys[7] => $this->getLastUpdate(),
            $keys[8] => $this->getSoftDelete(),
            $keys[9] => $this->getLastSync(),
            $keys[10] => $this->getUpdaterId(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aSekolahRelatedBySekolahId) {
                $result['SekolahRelatedBySekolahId'] = $this->aSekolahRelatedBySekolahId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSekolahRelatedBySekolahId) {
                $result['SekolahRelatedBySekolahId'] = $this->aSekolahRelatedBySekolahId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aJenisPrasaranaRelatedByJenisPrasaranaId) {
                $result['JenisPrasaranaRelatedByJenisPrasaranaId'] = $this->aJenisPrasaranaRelatedByJenisPrasaranaId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aJenisPrasaranaRelatedByJenisPrasaranaId) {
                $result['JenisPrasaranaRelatedByJenisPrasaranaId'] = $this->aJenisPrasaranaRelatedByJenisPrasaranaId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId) {
                $result['StatusKepemilikanSarprasRelatedByKepemilikanSarprasId'] = $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId) {
                $result['StatusKepemilikanSarprasRelatedByKepemilikanSarprasId'] = $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collBukuAlatsRelatedByPrasaranaId) {
                $result['BukuAlatsRelatedByPrasaranaId'] = $this->collBukuAlatsRelatedByPrasaranaId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collBukuAlatsRelatedByPrasaranaId) {
                $result['BukuAlatsRelatedByPrasaranaId'] = $this->collBukuAlatsRelatedByPrasaranaId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldPrasaranasRelatedByPrasaranaId) {
                $result['VldPrasaranasRelatedByPrasaranaId'] = $this->collVldPrasaranasRelatedByPrasaranaId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldPrasaranasRelatedByPrasaranaId) {
                $result['VldPrasaranasRelatedByPrasaranaId'] = $this->collVldPrasaranasRelatedByPrasaranaId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSaranasRelatedByPrasaranaId) {
                $result['SaranasRelatedByPrasaranaId'] = $this->collSaranasRelatedByPrasaranaId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSaranasRelatedByPrasaranaId) {
                $result['SaranasRelatedByPrasaranaId'] = $this->collSaranasRelatedByPrasaranaId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPrasaranaLongitudinalsRelatedByPrasaranaId) {
                $result['PrasaranaLongitudinalsRelatedByPrasaranaId'] = $this->collPrasaranaLongitudinalsRelatedByPrasaranaId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPrasaranaLongitudinalsRelatedByPrasaranaId) {
                $result['PrasaranaLongitudinalsRelatedByPrasaranaId'] = $this->collPrasaranaLongitudinalsRelatedByPrasaranaId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRombonganBelajarsRelatedByPrasaranaId) {
                $result['RombonganBelajarsRelatedByPrasaranaId'] = $this->collRombonganBelajarsRelatedByPrasaranaId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRombonganBelajarsRelatedByPrasaranaId) {
                $result['RombonganBelajarsRelatedByPrasaranaId'] = $this->collRombonganBelajarsRelatedByPrasaranaId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = PrasaranaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setPrasaranaId($value);
                break;
            case 1:
                $this->setJenisPrasaranaId($value);
                break;
            case 2:
                $this->setSekolahId($value);
                break;
            case 3:
                $this->setKepemilikanSarprasId($value);
                break;
            case 4:
                $this->setNama($value);
                break;
            case 5:
                $this->setPanjang($value);
                break;
            case 6:
                $this->setLebar($value);
                break;
            case 7:
                $this->setLastUpdate($value);
                break;
            case 8:
                $this->setSoftDelete($value);
                break;
            case 9:
                $this->setLastSync($value);
                break;
            case 10:
                $this->setUpdaterId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = PrasaranaPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setPrasaranaId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setJenisPrasaranaId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setSekolahId($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setKepemilikanSarprasId($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setNama($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setPanjang($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setLebar($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setLastUpdate($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setSoftDelete($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setLastSync($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setUpdaterId($arr[$keys[10]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(PrasaranaPeer::DATABASE_NAME);

        if ($this->isColumnModified(PrasaranaPeer::PRASARANA_ID)) $criteria->add(PrasaranaPeer::PRASARANA_ID, $this->prasarana_id);
        if ($this->isColumnModified(PrasaranaPeer::JENIS_PRASARANA_ID)) $criteria->add(PrasaranaPeer::JENIS_PRASARANA_ID, $this->jenis_prasarana_id);
        if ($this->isColumnModified(PrasaranaPeer::SEKOLAH_ID)) $criteria->add(PrasaranaPeer::SEKOLAH_ID, $this->sekolah_id);
        if ($this->isColumnModified(PrasaranaPeer::KEPEMILIKAN_SARPRAS_ID)) $criteria->add(PrasaranaPeer::KEPEMILIKAN_SARPRAS_ID, $this->kepemilikan_sarpras_id);
        if ($this->isColumnModified(PrasaranaPeer::NAMA)) $criteria->add(PrasaranaPeer::NAMA, $this->nama);
        if ($this->isColumnModified(PrasaranaPeer::PANJANG)) $criteria->add(PrasaranaPeer::PANJANG, $this->panjang);
        if ($this->isColumnModified(PrasaranaPeer::LEBAR)) $criteria->add(PrasaranaPeer::LEBAR, $this->lebar);
        if ($this->isColumnModified(PrasaranaPeer::LAST_UPDATE)) $criteria->add(PrasaranaPeer::LAST_UPDATE, $this->last_update);
        if ($this->isColumnModified(PrasaranaPeer::SOFT_DELETE)) $criteria->add(PrasaranaPeer::SOFT_DELETE, $this->soft_delete);
        if ($this->isColumnModified(PrasaranaPeer::LAST_SYNC)) $criteria->add(PrasaranaPeer::LAST_SYNC, $this->last_sync);
        if ($this->isColumnModified(PrasaranaPeer::UPDATER_ID)) $criteria->add(PrasaranaPeer::UPDATER_ID, $this->updater_id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(PrasaranaPeer::DATABASE_NAME);
        $criteria->add(PrasaranaPeer::PRASARANA_ID, $this->prasarana_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getPrasaranaId();
    }

    /**
     * Generic method to set the primary key (prasarana_id column).
     *
     * @param  string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setPrasaranaId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getPrasaranaId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Prasarana (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setJenisPrasaranaId($this->getJenisPrasaranaId());
        $copyObj->setSekolahId($this->getSekolahId());
        $copyObj->setKepemilikanSarprasId($this->getKepemilikanSarprasId());
        $copyObj->setNama($this->getNama());
        $copyObj->setPanjang($this->getPanjang());
        $copyObj->setLebar($this->getLebar());
        $copyObj->setLastUpdate($this->getLastUpdate());
        $copyObj->setSoftDelete($this->getSoftDelete());
        $copyObj->setLastSync($this->getLastSync());
        $copyObj->setUpdaterId($this->getUpdaterId());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getBukuAlatsRelatedByPrasaranaId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBukuAlatRelatedByPrasaranaId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getBukuAlatsRelatedByPrasaranaId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBukuAlatRelatedByPrasaranaId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldPrasaranasRelatedByPrasaranaId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldPrasaranaRelatedByPrasaranaId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldPrasaranasRelatedByPrasaranaId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldPrasaranaRelatedByPrasaranaId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSaranasRelatedByPrasaranaId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSaranaRelatedByPrasaranaId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSaranasRelatedByPrasaranaId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSaranaRelatedByPrasaranaId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPrasaranaLongitudinalsRelatedByPrasaranaId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPrasaranaLongitudinalRelatedByPrasaranaId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPrasaranaLongitudinalsRelatedByPrasaranaId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPrasaranaLongitudinalRelatedByPrasaranaId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRombonganBelajarsRelatedByPrasaranaId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRombonganBelajarRelatedByPrasaranaId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRombonganBelajarsRelatedByPrasaranaId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRombonganBelajarRelatedByPrasaranaId($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setPrasaranaId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Prasarana Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return PrasaranaPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new PrasaranaPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a Sekolah object.
     *
     * @param             Sekolah $v
     * @return Prasarana The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSekolahRelatedBySekolahId(Sekolah $v = null)
    {
        if ($v === null) {
            $this->setSekolahId(NULL);
        } else {
            $this->setSekolahId($v->getSekolahId());
        }

        $this->aSekolahRelatedBySekolahId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Sekolah object, it will not be re-added.
        if ($v !== null) {
            $v->addPrasaranaRelatedBySekolahId($this);
        }


        return $this;
    }


    /**
     * Get the associated Sekolah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Sekolah The associated Sekolah object.
     * @throws PropelException
     */
    public function getSekolahRelatedBySekolahId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSekolahRelatedBySekolahId === null && (($this->sekolah_id !== "" && $this->sekolah_id !== null)) && $doQuery) {
            $this->aSekolahRelatedBySekolahId = SekolahQuery::create()->findPk($this->sekolah_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSekolahRelatedBySekolahId->addPrasaranasRelatedBySekolahId($this);
             */
        }

        return $this->aSekolahRelatedBySekolahId;
    }

    /**
     * Declares an association between this object and a Sekolah object.
     *
     * @param             Sekolah $v
     * @return Prasarana The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSekolahRelatedBySekolahId(Sekolah $v = null)
    {
        if ($v === null) {
            $this->setSekolahId(NULL);
        } else {
            $this->setSekolahId($v->getSekolahId());
        }

        $this->aSekolahRelatedBySekolahId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Sekolah object, it will not be re-added.
        if ($v !== null) {
            $v->addPrasaranaRelatedBySekolahId($this);
        }


        return $this;
    }


    /**
     * Get the associated Sekolah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Sekolah The associated Sekolah object.
     * @throws PropelException
     */
    public function getSekolahRelatedBySekolahId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSekolahRelatedBySekolahId === null && (($this->sekolah_id !== "" && $this->sekolah_id !== null)) && $doQuery) {
            $this->aSekolahRelatedBySekolahId = SekolahQuery::create()->findPk($this->sekolah_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSekolahRelatedBySekolahId->addPrasaranasRelatedBySekolahId($this);
             */
        }

        return $this->aSekolahRelatedBySekolahId;
    }

    /**
     * Declares an association between this object and a JenisPrasarana object.
     *
     * @param             JenisPrasarana $v
     * @return Prasarana The current object (for fluent API support)
     * @throws PropelException
     */
    public function setJenisPrasaranaRelatedByJenisPrasaranaId(JenisPrasarana $v = null)
    {
        if ($v === null) {
            $this->setJenisPrasaranaId(NULL);
        } else {
            $this->setJenisPrasaranaId($v->getJenisPrasaranaId());
        }

        $this->aJenisPrasaranaRelatedByJenisPrasaranaId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the JenisPrasarana object, it will not be re-added.
        if ($v !== null) {
            $v->addPrasaranaRelatedByJenisPrasaranaId($this);
        }


        return $this;
    }


    /**
     * Get the associated JenisPrasarana object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return JenisPrasarana The associated JenisPrasarana object.
     * @throws PropelException
     */
    public function getJenisPrasaranaRelatedByJenisPrasaranaId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aJenisPrasaranaRelatedByJenisPrasaranaId === null && ($this->jenis_prasarana_id !== null) && $doQuery) {
            $this->aJenisPrasaranaRelatedByJenisPrasaranaId = JenisPrasaranaQuery::create()->findPk($this->jenis_prasarana_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aJenisPrasaranaRelatedByJenisPrasaranaId->addPrasaranasRelatedByJenisPrasaranaId($this);
             */
        }

        return $this->aJenisPrasaranaRelatedByJenisPrasaranaId;
    }

    /**
     * Declares an association between this object and a JenisPrasarana object.
     *
     * @param             JenisPrasarana $v
     * @return Prasarana The current object (for fluent API support)
     * @throws PropelException
     */
    public function setJenisPrasaranaRelatedByJenisPrasaranaId(JenisPrasarana $v = null)
    {
        if ($v === null) {
            $this->setJenisPrasaranaId(NULL);
        } else {
            $this->setJenisPrasaranaId($v->getJenisPrasaranaId());
        }

        $this->aJenisPrasaranaRelatedByJenisPrasaranaId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the JenisPrasarana object, it will not be re-added.
        if ($v !== null) {
            $v->addPrasaranaRelatedByJenisPrasaranaId($this);
        }


        return $this;
    }


    /**
     * Get the associated JenisPrasarana object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return JenisPrasarana The associated JenisPrasarana object.
     * @throws PropelException
     */
    public function getJenisPrasaranaRelatedByJenisPrasaranaId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aJenisPrasaranaRelatedByJenisPrasaranaId === null && ($this->jenis_prasarana_id !== null) && $doQuery) {
            $this->aJenisPrasaranaRelatedByJenisPrasaranaId = JenisPrasaranaQuery::create()->findPk($this->jenis_prasarana_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aJenisPrasaranaRelatedByJenisPrasaranaId->addPrasaranasRelatedByJenisPrasaranaId($this);
             */
        }

        return $this->aJenisPrasaranaRelatedByJenisPrasaranaId;
    }

    /**
     * Declares an association between this object and a StatusKepemilikanSarpras object.
     *
     * @param             StatusKepemilikanSarpras $v
     * @return Prasarana The current object (for fluent API support)
     * @throws PropelException
     */
    public function setStatusKepemilikanSarprasRelatedByKepemilikanSarprasId(StatusKepemilikanSarpras $v = null)
    {
        if ($v === null) {
            $this->setKepemilikanSarprasId(NULL);
        } else {
            $this->setKepemilikanSarprasId($v->getKepemilikanSarprasId());
        }

        $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the StatusKepemilikanSarpras object, it will not be re-added.
        if ($v !== null) {
            $v->addPrasaranaRelatedByKepemilikanSarprasId($this);
        }


        return $this;
    }


    /**
     * Get the associated StatusKepemilikanSarpras object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return StatusKepemilikanSarpras The associated StatusKepemilikanSarpras object.
     * @throws PropelException
     */
    public function getStatusKepemilikanSarprasRelatedByKepemilikanSarprasId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId === null && (($this->kepemilikan_sarpras_id !== "" && $this->kepemilikan_sarpras_id !== null)) && $doQuery) {
            $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId = StatusKepemilikanSarprasQuery::create()->findPk($this->kepemilikan_sarpras_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId->addPrasaranasRelatedByKepemilikanSarprasId($this);
             */
        }

        return $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId;
    }

    /**
     * Declares an association between this object and a StatusKepemilikanSarpras object.
     *
     * @param             StatusKepemilikanSarpras $v
     * @return Prasarana The current object (for fluent API support)
     * @throws PropelException
     */
    public function setStatusKepemilikanSarprasRelatedByKepemilikanSarprasId(StatusKepemilikanSarpras $v = null)
    {
        if ($v === null) {
            $this->setKepemilikanSarprasId(NULL);
        } else {
            $this->setKepemilikanSarprasId($v->getKepemilikanSarprasId());
        }

        $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the StatusKepemilikanSarpras object, it will not be re-added.
        if ($v !== null) {
            $v->addPrasaranaRelatedByKepemilikanSarprasId($this);
        }


        return $this;
    }


    /**
     * Get the associated StatusKepemilikanSarpras object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return StatusKepemilikanSarpras The associated StatusKepemilikanSarpras object.
     * @throws PropelException
     */
    public function getStatusKepemilikanSarprasRelatedByKepemilikanSarprasId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId === null && (($this->kepemilikan_sarpras_id !== "" && $this->kepemilikan_sarpras_id !== null)) && $doQuery) {
            $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId = StatusKepemilikanSarprasQuery::create()->findPk($this->kepemilikan_sarpras_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId->addPrasaranasRelatedByKepemilikanSarprasId($this);
             */
        }

        return $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('BukuAlatRelatedByPrasaranaId' == $relationName) {
            $this->initBukuAlatsRelatedByPrasaranaId();
        }
        if ('BukuAlatRelatedByPrasaranaId' == $relationName) {
            $this->initBukuAlatsRelatedByPrasaranaId();
        }
        if ('VldPrasaranaRelatedByPrasaranaId' == $relationName) {
            $this->initVldPrasaranasRelatedByPrasaranaId();
        }
        if ('VldPrasaranaRelatedByPrasaranaId' == $relationName) {
            $this->initVldPrasaranasRelatedByPrasaranaId();
        }
        if ('SaranaRelatedByPrasaranaId' == $relationName) {
            $this->initSaranasRelatedByPrasaranaId();
        }
        if ('SaranaRelatedByPrasaranaId' == $relationName) {
            $this->initSaranasRelatedByPrasaranaId();
        }
        if ('PrasaranaLongitudinalRelatedByPrasaranaId' == $relationName) {
            $this->initPrasaranaLongitudinalsRelatedByPrasaranaId();
        }
        if ('PrasaranaLongitudinalRelatedByPrasaranaId' == $relationName) {
            $this->initPrasaranaLongitudinalsRelatedByPrasaranaId();
        }
        if ('RombonganBelajarRelatedByPrasaranaId' == $relationName) {
            $this->initRombonganBelajarsRelatedByPrasaranaId();
        }
        if ('RombonganBelajarRelatedByPrasaranaId' == $relationName) {
            $this->initRombonganBelajarsRelatedByPrasaranaId();
        }
    }

    /**
     * Clears out the collBukuAlatsRelatedByPrasaranaId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Prasarana The current object (for fluent API support)
     * @see        addBukuAlatsRelatedByPrasaranaId()
     */
    public function clearBukuAlatsRelatedByPrasaranaId()
    {
        $this->collBukuAlatsRelatedByPrasaranaId = null; // important to set this to null since that means it is uninitialized
        $this->collBukuAlatsRelatedByPrasaranaIdPartial = null;

        return $this;
    }

    /**
     * reset is the collBukuAlatsRelatedByPrasaranaId collection loaded partially
     *
     * @return void
     */
    public function resetPartialBukuAlatsRelatedByPrasaranaId($v = true)
    {
        $this->collBukuAlatsRelatedByPrasaranaIdPartial = $v;
    }

    /**
     * Initializes the collBukuAlatsRelatedByPrasaranaId collection.
     *
     * By default this just sets the collBukuAlatsRelatedByPrasaranaId collection to an empty array (like clearcollBukuAlatsRelatedByPrasaranaId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBukuAlatsRelatedByPrasaranaId($overrideExisting = true)
    {
        if (null !== $this->collBukuAlatsRelatedByPrasaranaId && !$overrideExisting) {
            return;
        }
        $this->collBukuAlatsRelatedByPrasaranaId = new PropelObjectCollection();
        $this->collBukuAlatsRelatedByPrasaranaId->setModel('BukuAlat');
    }

    /**
     * Gets an array of BukuAlat objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Prasarana is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|BukuAlat[] List of BukuAlat objects
     * @throws PropelException
     */
    public function getBukuAlatsRelatedByPrasaranaId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collBukuAlatsRelatedByPrasaranaIdPartial && !$this->isNew();
        if (null === $this->collBukuAlatsRelatedByPrasaranaId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBukuAlatsRelatedByPrasaranaId) {
                // return empty collection
                $this->initBukuAlatsRelatedByPrasaranaId();
            } else {
                $collBukuAlatsRelatedByPrasaranaId = BukuAlatQuery::create(null, $criteria)
                    ->filterByPrasaranaRelatedByPrasaranaId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collBukuAlatsRelatedByPrasaranaIdPartial && count($collBukuAlatsRelatedByPrasaranaId)) {
                      $this->initBukuAlatsRelatedByPrasaranaId(false);

                      foreach($collBukuAlatsRelatedByPrasaranaId as $obj) {
                        if (false == $this->collBukuAlatsRelatedByPrasaranaId->contains($obj)) {
                          $this->collBukuAlatsRelatedByPrasaranaId->append($obj);
                        }
                      }

                      $this->collBukuAlatsRelatedByPrasaranaIdPartial = true;
                    }

                    $collBukuAlatsRelatedByPrasaranaId->getInternalIterator()->rewind();
                    return $collBukuAlatsRelatedByPrasaranaId;
                }

                if($partial && $this->collBukuAlatsRelatedByPrasaranaId) {
                    foreach($this->collBukuAlatsRelatedByPrasaranaId as $obj) {
                        if($obj->isNew()) {
                            $collBukuAlatsRelatedByPrasaranaId[] = $obj;
                        }
                    }
                }

                $this->collBukuAlatsRelatedByPrasaranaId = $collBukuAlatsRelatedByPrasaranaId;
                $this->collBukuAlatsRelatedByPrasaranaIdPartial = false;
            }
        }

        return $this->collBukuAlatsRelatedByPrasaranaId;
    }

    /**
     * Sets a collection of BukuAlatRelatedByPrasaranaId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $bukuAlatsRelatedByPrasaranaId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Prasarana The current object (for fluent API support)
     */
    public function setBukuAlatsRelatedByPrasaranaId(PropelCollection $bukuAlatsRelatedByPrasaranaId, PropelPDO $con = null)
    {
        $bukuAlatsRelatedByPrasaranaIdToDelete = $this->getBukuAlatsRelatedByPrasaranaId(new Criteria(), $con)->diff($bukuAlatsRelatedByPrasaranaId);

        $this->bukuAlatsRelatedByPrasaranaIdScheduledForDeletion = unserialize(serialize($bukuAlatsRelatedByPrasaranaIdToDelete));

        foreach ($bukuAlatsRelatedByPrasaranaIdToDelete as $bukuAlatRelatedByPrasaranaIdRemoved) {
            $bukuAlatRelatedByPrasaranaIdRemoved->setPrasaranaRelatedByPrasaranaId(null);
        }

        $this->collBukuAlatsRelatedByPrasaranaId = null;
        foreach ($bukuAlatsRelatedByPrasaranaId as $bukuAlatRelatedByPrasaranaId) {
            $this->addBukuAlatRelatedByPrasaranaId($bukuAlatRelatedByPrasaranaId);
        }

        $this->collBukuAlatsRelatedByPrasaranaId = $bukuAlatsRelatedByPrasaranaId;
        $this->collBukuAlatsRelatedByPrasaranaIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BukuAlat objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related BukuAlat objects.
     * @throws PropelException
     */
    public function countBukuAlatsRelatedByPrasaranaId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collBukuAlatsRelatedByPrasaranaIdPartial && !$this->isNew();
        if (null === $this->collBukuAlatsRelatedByPrasaranaId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBukuAlatsRelatedByPrasaranaId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getBukuAlatsRelatedByPrasaranaId());
            }
            $query = BukuAlatQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPrasaranaRelatedByPrasaranaId($this)
                ->count($con);
        }

        return count($this->collBukuAlatsRelatedByPrasaranaId);
    }

    /**
     * Method called to associate a BukuAlat object to this object
     * through the BukuAlat foreign key attribute.
     *
     * @param    BukuAlat $l BukuAlat
     * @return Prasarana The current object (for fluent API support)
     */
    public function addBukuAlatRelatedByPrasaranaId(BukuAlat $l)
    {
        if ($this->collBukuAlatsRelatedByPrasaranaId === null) {
            $this->initBukuAlatsRelatedByPrasaranaId();
            $this->collBukuAlatsRelatedByPrasaranaIdPartial = true;
        }
        if (!in_array($l, $this->collBukuAlatsRelatedByPrasaranaId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddBukuAlatRelatedByPrasaranaId($l);
        }

        return $this;
    }

    /**
     * @param	BukuAlatRelatedByPrasaranaId $bukuAlatRelatedByPrasaranaId The bukuAlatRelatedByPrasaranaId object to add.
     */
    protected function doAddBukuAlatRelatedByPrasaranaId($bukuAlatRelatedByPrasaranaId)
    {
        $this->collBukuAlatsRelatedByPrasaranaId[]= $bukuAlatRelatedByPrasaranaId;
        $bukuAlatRelatedByPrasaranaId->setPrasaranaRelatedByPrasaranaId($this);
    }

    /**
     * @param	BukuAlatRelatedByPrasaranaId $bukuAlatRelatedByPrasaranaId The bukuAlatRelatedByPrasaranaId object to remove.
     * @return Prasarana The current object (for fluent API support)
     */
    public function removeBukuAlatRelatedByPrasaranaId($bukuAlatRelatedByPrasaranaId)
    {
        if ($this->getBukuAlatsRelatedByPrasaranaId()->contains($bukuAlatRelatedByPrasaranaId)) {
            $this->collBukuAlatsRelatedByPrasaranaId->remove($this->collBukuAlatsRelatedByPrasaranaId->search($bukuAlatRelatedByPrasaranaId));
            if (null === $this->bukuAlatsRelatedByPrasaranaIdScheduledForDeletion) {
                $this->bukuAlatsRelatedByPrasaranaIdScheduledForDeletion = clone $this->collBukuAlatsRelatedByPrasaranaId;
                $this->bukuAlatsRelatedByPrasaranaIdScheduledForDeletion->clear();
            }
            $this->bukuAlatsRelatedByPrasaranaIdScheduledForDeletion[]= $bukuAlatRelatedByPrasaranaId;
            $bukuAlatRelatedByPrasaranaId->setPrasaranaRelatedByPrasaranaId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related BukuAlatsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BukuAlat[] List of BukuAlat objects
     */
    public function getBukuAlatsRelatedByPrasaranaIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BukuAlatQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getBukuAlatsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related BukuAlatsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BukuAlat[] List of BukuAlat objects
     */
    public function getBukuAlatsRelatedByPrasaranaIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BukuAlatQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getBukuAlatsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related BukuAlatsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BukuAlat[] List of BukuAlat objects
     */
    public function getBukuAlatsRelatedByPrasaranaIdJoinJenisBukuAlatRelatedByJenisBukuAlatId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BukuAlatQuery::create(null, $criteria);
        $query->joinWith('JenisBukuAlatRelatedByJenisBukuAlatId', $join_behavior);

        return $this->getBukuAlatsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related BukuAlatsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BukuAlat[] List of BukuAlat objects
     */
    public function getBukuAlatsRelatedByPrasaranaIdJoinJenisBukuAlatRelatedByJenisBukuAlatId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BukuAlatQuery::create(null, $criteria);
        $query->joinWith('JenisBukuAlatRelatedByJenisBukuAlatId', $join_behavior);

        return $this->getBukuAlatsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related BukuAlatsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BukuAlat[] List of BukuAlat objects
     */
    public function getBukuAlatsRelatedByPrasaranaIdJoinMataPelajaranRelatedByMataPelajaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BukuAlatQuery::create(null, $criteria);
        $query->joinWith('MataPelajaranRelatedByMataPelajaranId', $join_behavior);

        return $this->getBukuAlatsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related BukuAlatsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BukuAlat[] List of BukuAlat objects
     */
    public function getBukuAlatsRelatedByPrasaranaIdJoinMataPelajaranRelatedByMataPelajaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BukuAlatQuery::create(null, $criteria);
        $query->joinWith('MataPelajaranRelatedByMataPelajaranId', $join_behavior);

        return $this->getBukuAlatsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related BukuAlatsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BukuAlat[] List of BukuAlat objects
     */
    public function getBukuAlatsRelatedByPrasaranaIdJoinTingkatPendidikanRelatedByTingkatPendidikanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BukuAlatQuery::create(null, $criteria);
        $query->joinWith('TingkatPendidikanRelatedByTingkatPendidikanId', $join_behavior);

        return $this->getBukuAlatsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related BukuAlatsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BukuAlat[] List of BukuAlat objects
     */
    public function getBukuAlatsRelatedByPrasaranaIdJoinTingkatPendidikanRelatedByTingkatPendidikanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BukuAlatQuery::create(null, $criteria);
        $query->joinWith('TingkatPendidikanRelatedByTingkatPendidikanId', $join_behavior);

        return $this->getBukuAlatsRelatedByPrasaranaId($query, $con);
    }

    /**
     * Clears out the collBukuAlatsRelatedByPrasaranaId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Prasarana The current object (for fluent API support)
     * @see        addBukuAlatsRelatedByPrasaranaId()
     */
    public function clearBukuAlatsRelatedByPrasaranaId()
    {
        $this->collBukuAlatsRelatedByPrasaranaId = null; // important to set this to null since that means it is uninitialized
        $this->collBukuAlatsRelatedByPrasaranaIdPartial = null;

        return $this;
    }

    /**
     * reset is the collBukuAlatsRelatedByPrasaranaId collection loaded partially
     *
     * @return void
     */
    public function resetPartialBukuAlatsRelatedByPrasaranaId($v = true)
    {
        $this->collBukuAlatsRelatedByPrasaranaIdPartial = $v;
    }

    /**
     * Initializes the collBukuAlatsRelatedByPrasaranaId collection.
     *
     * By default this just sets the collBukuAlatsRelatedByPrasaranaId collection to an empty array (like clearcollBukuAlatsRelatedByPrasaranaId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBukuAlatsRelatedByPrasaranaId($overrideExisting = true)
    {
        if (null !== $this->collBukuAlatsRelatedByPrasaranaId && !$overrideExisting) {
            return;
        }
        $this->collBukuAlatsRelatedByPrasaranaId = new PropelObjectCollection();
        $this->collBukuAlatsRelatedByPrasaranaId->setModel('BukuAlat');
    }

    /**
     * Gets an array of BukuAlat objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Prasarana is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|BukuAlat[] List of BukuAlat objects
     * @throws PropelException
     */
    public function getBukuAlatsRelatedByPrasaranaId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collBukuAlatsRelatedByPrasaranaIdPartial && !$this->isNew();
        if (null === $this->collBukuAlatsRelatedByPrasaranaId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBukuAlatsRelatedByPrasaranaId) {
                // return empty collection
                $this->initBukuAlatsRelatedByPrasaranaId();
            } else {
                $collBukuAlatsRelatedByPrasaranaId = BukuAlatQuery::create(null, $criteria)
                    ->filterByPrasaranaRelatedByPrasaranaId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collBukuAlatsRelatedByPrasaranaIdPartial && count($collBukuAlatsRelatedByPrasaranaId)) {
                      $this->initBukuAlatsRelatedByPrasaranaId(false);

                      foreach($collBukuAlatsRelatedByPrasaranaId as $obj) {
                        if (false == $this->collBukuAlatsRelatedByPrasaranaId->contains($obj)) {
                          $this->collBukuAlatsRelatedByPrasaranaId->append($obj);
                        }
                      }

                      $this->collBukuAlatsRelatedByPrasaranaIdPartial = true;
                    }

                    $collBukuAlatsRelatedByPrasaranaId->getInternalIterator()->rewind();
                    return $collBukuAlatsRelatedByPrasaranaId;
                }

                if($partial && $this->collBukuAlatsRelatedByPrasaranaId) {
                    foreach($this->collBukuAlatsRelatedByPrasaranaId as $obj) {
                        if($obj->isNew()) {
                            $collBukuAlatsRelatedByPrasaranaId[] = $obj;
                        }
                    }
                }

                $this->collBukuAlatsRelatedByPrasaranaId = $collBukuAlatsRelatedByPrasaranaId;
                $this->collBukuAlatsRelatedByPrasaranaIdPartial = false;
            }
        }

        return $this->collBukuAlatsRelatedByPrasaranaId;
    }

    /**
     * Sets a collection of BukuAlatRelatedByPrasaranaId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $bukuAlatsRelatedByPrasaranaId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Prasarana The current object (for fluent API support)
     */
    public function setBukuAlatsRelatedByPrasaranaId(PropelCollection $bukuAlatsRelatedByPrasaranaId, PropelPDO $con = null)
    {
        $bukuAlatsRelatedByPrasaranaIdToDelete = $this->getBukuAlatsRelatedByPrasaranaId(new Criteria(), $con)->diff($bukuAlatsRelatedByPrasaranaId);

        $this->bukuAlatsRelatedByPrasaranaIdScheduledForDeletion = unserialize(serialize($bukuAlatsRelatedByPrasaranaIdToDelete));

        foreach ($bukuAlatsRelatedByPrasaranaIdToDelete as $bukuAlatRelatedByPrasaranaIdRemoved) {
            $bukuAlatRelatedByPrasaranaIdRemoved->setPrasaranaRelatedByPrasaranaId(null);
        }

        $this->collBukuAlatsRelatedByPrasaranaId = null;
        foreach ($bukuAlatsRelatedByPrasaranaId as $bukuAlatRelatedByPrasaranaId) {
            $this->addBukuAlatRelatedByPrasaranaId($bukuAlatRelatedByPrasaranaId);
        }

        $this->collBukuAlatsRelatedByPrasaranaId = $bukuAlatsRelatedByPrasaranaId;
        $this->collBukuAlatsRelatedByPrasaranaIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BukuAlat objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related BukuAlat objects.
     * @throws PropelException
     */
    public function countBukuAlatsRelatedByPrasaranaId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collBukuAlatsRelatedByPrasaranaIdPartial && !$this->isNew();
        if (null === $this->collBukuAlatsRelatedByPrasaranaId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBukuAlatsRelatedByPrasaranaId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getBukuAlatsRelatedByPrasaranaId());
            }
            $query = BukuAlatQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPrasaranaRelatedByPrasaranaId($this)
                ->count($con);
        }

        return count($this->collBukuAlatsRelatedByPrasaranaId);
    }

    /**
     * Method called to associate a BukuAlat object to this object
     * through the BukuAlat foreign key attribute.
     *
     * @param    BukuAlat $l BukuAlat
     * @return Prasarana The current object (for fluent API support)
     */
    public function addBukuAlatRelatedByPrasaranaId(BukuAlat $l)
    {
        if ($this->collBukuAlatsRelatedByPrasaranaId === null) {
            $this->initBukuAlatsRelatedByPrasaranaId();
            $this->collBukuAlatsRelatedByPrasaranaIdPartial = true;
        }
        if (!in_array($l, $this->collBukuAlatsRelatedByPrasaranaId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddBukuAlatRelatedByPrasaranaId($l);
        }

        return $this;
    }

    /**
     * @param	BukuAlatRelatedByPrasaranaId $bukuAlatRelatedByPrasaranaId The bukuAlatRelatedByPrasaranaId object to add.
     */
    protected function doAddBukuAlatRelatedByPrasaranaId($bukuAlatRelatedByPrasaranaId)
    {
        $this->collBukuAlatsRelatedByPrasaranaId[]= $bukuAlatRelatedByPrasaranaId;
        $bukuAlatRelatedByPrasaranaId->setPrasaranaRelatedByPrasaranaId($this);
    }

    /**
     * @param	BukuAlatRelatedByPrasaranaId $bukuAlatRelatedByPrasaranaId The bukuAlatRelatedByPrasaranaId object to remove.
     * @return Prasarana The current object (for fluent API support)
     */
    public function removeBukuAlatRelatedByPrasaranaId($bukuAlatRelatedByPrasaranaId)
    {
        if ($this->getBukuAlatsRelatedByPrasaranaId()->contains($bukuAlatRelatedByPrasaranaId)) {
            $this->collBukuAlatsRelatedByPrasaranaId->remove($this->collBukuAlatsRelatedByPrasaranaId->search($bukuAlatRelatedByPrasaranaId));
            if (null === $this->bukuAlatsRelatedByPrasaranaIdScheduledForDeletion) {
                $this->bukuAlatsRelatedByPrasaranaIdScheduledForDeletion = clone $this->collBukuAlatsRelatedByPrasaranaId;
                $this->bukuAlatsRelatedByPrasaranaIdScheduledForDeletion->clear();
            }
            $this->bukuAlatsRelatedByPrasaranaIdScheduledForDeletion[]= $bukuAlatRelatedByPrasaranaId;
            $bukuAlatRelatedByPrasaranaId->setPrasaranaRelatedByPrasaranaId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related BukuAlatsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BukuAlat[] List of BukuAlat objects
     */
    public function getBukuAlatsRelatedByPrasaranaIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BukuAlatQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getBukuAlatsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related BukuAlatsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BukuAlat[] List of BukuAlat objects
     */
    public function getBukuAlatsRelatedByPrasaranaIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BukuAlatQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getBukuAlatsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related BukuAlatsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BukuAlat[] List of BukuAlat objects
     */
    public function getBukuAlatsRelatedByPrasaranaIdJoinJenisBukuAlatRelatedByJenisBukuAlatId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BukuAlatQuery::create(null, $criteria);
        $query->joinWith('JenisBukuAlatRelatedByJenisBukuAlatId', $join_behavior);

        return $this->getBukuAlatsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related BukuAlatsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BukuAlat[] List of BukuAlat objects
     */
    public function getBukuAlatsRelatedByPrasaranaIdJoinJenisBukuAlatRelatedByJenisBukuAlatId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BukuAlatQuery::create(null, $criteria);
        $query->joinWith('JenisBukuAlatRelatedByJenisBukuAlatId', $join_behavior);

        return $this->getBukuAlatsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related BukuAlatsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BukuAlat[] List of BukuAlat objects
     */
    public function getBukuAlatsRelatedByPrasaranaIdJoinMataPelajaranRelatedByMataPelajaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BukuAlatQuery::create(null, $criteria);
        $query->joinWith('MataPelajaranRelatedByMataPelajaranId', $join_behavior);

        return $this->getBukuAlatsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related BukuAlatsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BukuAlat[] List of BukuAlat objects
     */
    public function getBukuAlatsRelatedByPrasaranaIdJoinMataPelajaranRelatedByMataPelajaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BukuAlatQuery::create(null, $criteria);
        $query->joinWith('MataPelajaranRelatedByMataPelajaranId', $join_behavior);

        return $this->getBukuAlatsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related BukuAlatsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BukuAlat[] List of BukuAlat objects
     */
    public function getBukuAlatsRelatedByPrasaranaIdJoinTingkatPendidikanRelatedByTingkatPendidikanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BukuAlatQuery::create(null, $criteria);
        $query->joinWith('TingkatPendidikanRelatedByTingkatPendidikanId', $join_behavior);

        return $this->getBukuAlatsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related BukuAlatsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BukuAlat[] List of BukuAlat objects
     */
    public function getBukuAlatsRelatedByPrasaranaIdJoinTingkatPendidikanRelatedByTingkatPendidikanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BukuAlatQuery::create(null, $criteria);
        $query->joinWith('TingkatPendidikanRelatedByTingkatPendidikanId', $join_behavior);

        return $this->getBukuAlatsRelatedByPrasaranaId($query, $con);
    }

    /**
     * Clears out the collVldPrasaranasRelatedByPrasaranaId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Prasarana The current object (for fluent API support)
     * @see        addVldPrasaranasRelatedByPrasaranaId()
     */
    public function clearVldPrasaranasRelatedByPrasaranaId()
    {
        $this->collVldPrasaranasRelatedByPrasaranaId = null; // important to set this to null since that means it is uninitialized
        $this->collVldPrasaranasRelatedByPrasaranaIdPartial = null;

        return $this;
    }

    /**
     * reset is the collVldPrasaranasRelatedByPrasaranaId collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldPrasaranasRelatedByPrasaranaId($v = true)
    {
        $this->collVldPrasaranasRelatedByPrasaranaIdPartial = $v;
    }

    /**
     * Initializes the collVldPrasaranasRelatedByPrasaranaId collection.
     *
     * By default this just sets the collVldPrasaranasRelatedByPrasaranaId collection to an empty array (like clearcollVldPrasaranasRelatedByPrasaranaId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldPrasaranasRelatedByPrasaranaId($overrideExisting = true)
    {
        if (null !== $this->collVldPrasaranasRelatedByPrasaranaId && !$overrideExisting) {
            return;
        }
        $this->collVldPrasaranasRelatedByPrasaranaId = new PropelObjectCollection();
        $this->collVldPrasaranasRelatedByPrasaranaId->setModel('VldPrasarana');
    }

    /**
     * Gets an array of VldPrasarana objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Prasarana is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldPrasarana[] List of VldPrasarana objects
     * @throws PropelException
     */
    public function getVldPrasaranasRelatedByPrasaranaId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldPrasaranasRelatedByPrasaranaIdPartial && !$this->isNew();
        if (null === $this->collVldPrasaranasRelatedByPrasaranaId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldPrasaranasRelatedByPrasaranaId) {
                // return empty collection
                $this->initVldPrasaranasRelatedByPrasaranaId();
            } else {
                $collVldPrasaranasRelatedByPrasaranaId = VldPrasaranaQuery::create(null, $criteria)
                    ->filterByPrasaranaRelatedByPrasaranaId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldPrasaranasRelatedByPrasaranaIdPartial && count($collVldPrasaranasRelatedByPrasaranaId)) {
                      $this->initVldPrasaranasRelatedByPrasaranaId(false);

                      foreach($collVldPrasaranasRelatedByPrasaranaId as $obj) {
                        if (false == $this->collVldPrasaranasRelatedByPrasaranaId->contains($obj)) {
                          $this->collVldPrasaranasRelatedByPrasaranaId->append($obj);
                        }
                      }

                      $this->collVldPrasaranasRelatedByPrasaranaIdPartial = true;
                    }

                    $collVldPrasaranasRelatedByPrasaranaId->getInternalIterator()->rewind();
                    return $collVldPrasaranasRelatedByPrasaranaId;
                }

                if($partial && $this->collVldPrasaranasRelatedByPrasaranaId) {
                    foreach($this->collVldPrasaranasRelatedByPrasaranaId as $obj) {
                        if($obj->isNew()) {
                            $collVldPrasaranasRelatedByPrasaranaId[] = $obj;
                        }
                    }
                }

                $this->collVldPrasaranasRelatedByPrasaranaId = $collVldPrasaranasRelatedByPrasaranaId;
                $this->collVldPrasaranasRelatedByPrasaranaIdPartial = false;
            }
        }

        return $this->collVldPrasaranasRelatedByPrasaranaId;
    }

    /**
     * Sets a collection of VldPrasaranaRelatedByPrasaranaId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldPrasaranasRelatedByPrasaranaId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Prasarana The current object (for fluent API support)
     */
    public function setVldPrasaranasRelatedByPrasaranaId(PropelCollection $vldPrasaranasRelatedByPrasaranaId, PropelPDO $con = null)
    {
        $vldPrasaranasRelatedByPrasaranaIdToDelete = $this->getVldPrasaranasRelatedByPrasaranaId(new Criteria(), $con)->diff($vldPrasaranasRelatedByPrasaranaId);

        $this->vldPrasaranasRelatedByPrasaranaIdScheduledForDeletion = unserialize(serialize($vldPrasaranasRelatedByPrasaranaIdToDelete));

        foreach ($vldPrasaranasRelatedByPrasaranaIdToDelete as $vldPrasaranaRelatedByPrasaranaIdRemoved) {
            $vldPrasaranaRelatedByPrasaranaIdRemoved->setPrasaranaRelatedByPrasaranaId(null);
        }

        $this->collVldPrasaranasRelatedByPrasaranaId = null;
        foreach ($vldPrasaranasRelatedByPrasaranaId as $vldPrasaranaRelatedByPrasaranaId) {
            $this->addVldPrasaranaRelatedByPrasaranaId($vldPrasaranaRelatedByPrasaranaId);
        }

        $this->collVldPrasaranasRelatedByPrasaranaId = $vldPrasaranasRelatedByPrasaranaId;
        $this->collVldPrasaranasRelatedByPrasaranaIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldPrasarana objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldPrasarana objects.
     * @throws PropelException
     */
    public function countVldPrasaranasRelatedByPrasaranaId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldPrasaranasRelatedByPrasaranaIdPartial && !$this->isNew();
        if (null === $this->collVldPrasaranasRelatedByPrasaranaId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldPrasaranasRelatedByPrasaranaId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldPrasaranasRelatedByPrasaranaId());
            }
            $query = VldPrasaranaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPrasaranaRelatedByPrasaranaId($this)
                ->count($con);
        }

        return count($this->collVldPrasaranasRelatedByPrasaranaId);
    }

    /**
     * Method called to associate a VldPrasarana object to this object
     * through the VldPrasarana foreign key attribute.
     *
     * @param    VldPrasarana $l VldPrasarana
     * @return Prasarana The current object (for fluent API support)
     */
    public function addVldPrasaranaRelatedByPrasaranaId(VldPrasarana $l)
    {
        if ($this->collVldPrasaranasRelatedByPrasaranaId === null) {
            $this->initVldPrasaranasRelatedByPrasaranaId();
            $this->collVldPrasaranasRelatedByPrasaranaIdPartial = true;
        }
        if (!in_array($l, $this->collVldPrasaranasRelatedByPrasaranaId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldPrasaranaRelatedByPrasaranaId($l);
        }

        return $this;
    }

    /**
     * @param	VldPrasaranaRelatedByPrasaranaId $vldPrasaranaRelatedByPrasaranaId The vldPrasaranaRelatedByPrasaranaId object to add.
     */
    protected function doAddVldPrasaranaRelatedByPrasaranaId($vldPrasaranaRelatedByPrasaranaId)
    {
        $this->collVldPrasaranasRelatedByPrasaranaId[]= $vldPrasaranaRelatedByPrasaranaId;
        $vldPrasaranaRelatedByPrasaranaId->setPrasaranaRelatedByPrasaranaId($this);
    }

    /**
     * @param	VldPrasaranaRelatedByPrasaranaId $vldPrasaranaRelatedByPrasaranaId The vldPrasaranaRelatedByPrasaranaId object to remove.
     * @return Prasarana The current object (for fluent API support)
     */
    public function removeVldPrasaranaRelatedByPrasaranaId($vldPrasaranaRelatedByPrasaranaId)
    {
        if ($this->getVldPrasaranasRelatedByPrasaranaId()->contains($vldPrasaranaRelatedByPrasaranaId)) {
            $this->collVldPrasaranasRelatedByPrasaranaId->remove($this->collVldPrasaranasRelatedByPrasaranaId->search($vldPrasaranaRelatedByPrasaranaId));
            if (null === $this->vldPrasaranasRelatedByPrasaranaIdScheduledForDeletion) {
                $this->vldPrasaranasRelatedByPrasaranaIdScheduledForDeletion = clone $this->collVldPrasaranasRelatedByPrasaranaId;
                $this->vldPrasaranasRelatedByPrasaranaIdScheduledForDeletion->clear();
            }
            $this->vldPrasaranasRelatedByPrasaranaIdScheduledForDeletion[]= clone $vldPrasaranaRelatedByPrasaranaId;
            $vldPrasaranaRelatedByPrasaranaId->setPrasaranaRelatedByPrasaranaId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related VldPrasaranasRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPrasarana[] List of VldPrasarana objects
     */
    public function getVldPrasaranasRelatedByPrasaranaIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPrasaranaQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldPrasaranasRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related VldPrasaranasRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPrasarana[] List of VldPrasarana objects
     */
    public function getVldPrasaranasRelatedByPrasaranaIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPrasaranaQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldPrasaranasRelatedByPrasaranaId($query, $con);
    }

    /**
     * Clears out the collVldPrasaranasRelatedByPrasaranaId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Prasarana The current object (for fluent API support)
     * @see        addVldPrasaranasRelatedByPrasaranaId()
     */
    public function clearVldPrasaranasRelatedByPrasaranaId()
    {
        $this->collVldPrasaranasRelatedByPrasaranaId = null; // important to set this to null since that means it is uninitialized
        $this->collVldPrasaranasRelatedByPrasaranaIdPartial = null;

        return $this;
    }

    /**
     * reset is the collVldPrasaranasRelatedByPrasaranaId collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldPrasaranasRelatedByPrasaranaId($v = true)
    {
        $this->collVldPrasaranasRelatedByPrasaranaIdPartial = $v;
    }

    /**
     * Initializes the collVldPrasaranasRelatedByPrasaranaId collection.
     *
     * By default this just sets the collVldPrasaranasRelatedByPrasaranaId collection to an empty array (like clearcollVldPrasaranasRelatedByPrasaranaId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldPrasaranasRelatedByPrasaranaId($overrideExisting = true)
    {
        if (null !== $this->collVldPrasaranasRelatedByPrasaranaId && !$overrideExisting) {
            return;
        }
        $this->collVldPrasaranasRelatedByPrasaranaId = new PropelObjectCollection();
        $this->collVldPrasaranasRelatedByPrasaranaId->setModel('VldPrasarana');
    }

    /**
     * Gets an array of VldPrasarana objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Prasarana is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldPrasarana[] List of VldPrasarana objects
     * @throws PropelException
     */
    public function getVldPrasaranasRelatedByPrasaranaId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldPrasaranasRelatedByPrasaranaIdPartial && !$this->isNew();
        if (null === $this->collVldPrasaranasRelatedByPrasaranaId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldPrasaranasRelatedByPrasaranaId) {
                // return empty collection
                $this->initVldPrasaranasRelatedByPrasaranaId();
            } else {
                $collVldPrasaranasRelatedByPrasaranaId = VldPrasaranaQuery::create(null, $criteria)
                    ->filterByPrasaranaRelatedByPrasaranaId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldPrasaranasRelatedByPrasaranaIdPartial && count($collVldPrasaranasRelatedByPrasaranaId)) {
                      $this->initVldPrasaranasRelatedByPrasaranaId(false);

                      foreach($collVldPrasaranasRelatedByPrasaranaId as $obj) {
                        if (false == $this->collVldPrasaranasRelatedByPrasaranaId->contains($obj)) {
                          $this->collVldPrasaranasRelatedByPrasaranaId->append($obj);
                        }
                      }

                      $this->collVldPrasaranasRelatedByPrasaranaIdPartial = true;
                    }

                    $collVldPrasaranasRelatedByPrasaranaId->getInternalIterator()->rewind();
                    return $collVldPrasaranasRelatedByPrasaranaId;
                }

                if($partial && $this->collVldPrasaranasRelatedByPrasaranaId) {
                    foreach($this->collVldPrasaranasRelatedByPrasaranaId as $obj) {
                        if($obj->isNew()) {
                            $collVldPrasaranasRelatedByPrasaranaId[] = $obj;
                        }
                    }
                }

                $this->collVldPrasaranasRelatedByPrasaranaId = $collVldPrasaranasRelatedByPrasaranaId;
                $this->collVldPrasaranasRelatedByPrasaranaIdPartial = false;
            }
        }

        return $this->collVldPrasaranasRelatedByPrasaranaId;
    }

    /**
     * Sets a collection of VldPrasaranaRelatedByPrasaranaId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldPrasaranasRelatedByPrasaranaId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Prasarana The current object (for fluent API support)
     */
    public function setVldPrasaranasRelatedByPrasaranaId(PropelCollection $vldPrasaranasRelatedByPrasaranaId, PropelPDO $con = null)
    {
        $vldPrasaranasRelatedByPrasaranaIdToDelete = $this->getVldPrasaranasRelatedByPrasaranaId(new Criteria(), $con)->diff($vldPrasaranasRelatedByPrasaranaId);

        $this->vldPrasaranasRelatedByPrasaranaIdScheduledForDeletion = unserialize(serialize($vldPrasaranasRelatedByPrasaranaIdToDelete));

        foreach ($vldPrasaranasRelatedByPrasaranaIdToDelete as $vldPrasaranaRelatedByPrasaranaIdRemoved) {
            $vldPrasaranaRelatedByPrasaranaIdRemoved->setPrasaranaRelatedByPrasaranaId(null);
        }

        $this->collVldPrasaranasRelatedByPrasaranaId = null;
        foreach ($vldPrasaranasRelatedByPrasaranaId as $vldPrasaranaRelatedByPrasaranaId) {
            $this->addVldPrasaranaRelatedByPrasaranaId($vldPrasaranaRelatedByPrasaranaId);
        }

        $this->collVldPrasaranasRelatedByPrasaranaId = $vldPrasaranasRelatedByPrasaranaId;
        $this->collVldPrasaranasRelatedByPrasaranaIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldPrasarana objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldPrasarana objects.
     * @throws PropelException
     */
    public function countVldPrasaranasRelatedByPrasaranaId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldPrasaranasRelatedByPrasaranaIdPartial && !$this->isNew();
        if (null === $this->collVldPrasaranasRelatedByPrasaranaId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldPrasaranasRelatedByPrasaranaId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldPrasaranasRelatedByPrasaranaId());
            }
            $query = VldPrasaranaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPrasaranaRelatedByPrasaranaId($this)
                ->count($con);
        }

        return count($this->collVldPrasaranasRelatedByPrasaranaId);
    }

    /**
     * Method called to associate a VldPrasarana object to this object
     * through the VldPrasarana foreign key attribute.
     *
     * @param    VldPrasarana $l VldPrasarana
     * @return Prasarana The current object (for fluent API support)
     */
    public function addVldPrasaranaRelatedByPrasaranaId(VldPrasarana $l)
    {
        if ($this->collVldPrasaranasRelatedByPrasaranaId === null) {
            $this->initVldPrasaranasRelatedByPrasaranaId();
            $this->collVldPrasaranasRelatedByPrasaranaIdPartial = true;
        }
        if (!in_array($l, $this->collVldPrasaranasRelatedByPrasaranaId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldPrasaranaRelatedByPrasaranaId($l);
        }

        return $this;
    }

    /**
     * @param	VldPrasaranaRelatedByPrasaranaId $vldPrasaranaRelatedByPrasaranaId The vldPrasaranaRelatedByPrasaranaId object to add.
     */
    protected function doAddVldPrasaranaRelatedByPrasaranaId($vldPrasaranaRelatedByPrasaranaId)
    {
        $this->collVldPrasaranasRelatedByPrasaranaId[]= $vldPrasaranaRelatedByPrasaranaId;
        $vldPrasaranaRelatedByPrasaranaId->setPrasaranaRelatedByPrasaranaId($this);
    }

    /**
     * @param	VldPrasaranaRelatedByPrasaranaId $vldPrasaranaRelatedByPrasaranaId The vldPrasaranaRelatedByPrasaranaId object to remove.
     * @return Prasarana The current object (for fluent API support)
     */
    public function removeVldPrasaranaRelatedByPrasaranaId($vldPrasaranaRelatedByPrasaranaId)
    {
        if ($this->getVldPrasaranasRelatedByPrasaranaId()->contains($vldPrasaranaRelatedByPrasaranaId)) {
            $this->collVldPrasaranasRelatedByPrasaranaId->remove($this->collVldPrasaranasRelatedByPrasaranaId->search($vldPrasaranaRelatedByPrasaranaId));
            if (null === $this->vldPrasaranasRelatedByPrasaranaIdScheduledForDeletion) {
                $this->vldPrasaranasRelatedByPrasaranaIdScheduledForDeletion = clone $this->collVldPrasaranasRelatedByPrasaranaId;
                $this->vldPrasaranasRelatedByPrasaranaIdScheduledForDeletion->clear();
            }
            $this->vldPrasaranasRelatedByPrasaranaIdScheduledForDeletion[]= clone $vldPrasaranaRelatedByPrasaranaId;
            $vldPrasaranaRelatedByPrasaranaId->setPrasaranaRelatedByPrasaranaId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related VldPrasaranasRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPrasarana[] List of VldPrasarana objects
     */
    public function getVldPrasaranasRelatedByPrasaranaIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPrasaranaQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldPrasaranasRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related VldPrasaranasRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPrasarana[] List of VldPrasarana objects
     */
    public function getVldPrasaranasRelatedByPrasaranaIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPrasaranaQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldPrasaranasRelatedByPrasaranaId($query, $con);
    }

    /**
     * Clears out the collSaranasRelatedByPrasaranaId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Prasarana The current object (for fluent API support)
     * @see        addSaranasRelatedByPrasaranaId()
     */
    public function clearSaranasRelatedByPrasaranaId()
    {
        $this->collSaranasRelatedByPrasaranaId = null; // important to set this to null since that means it is uninitialized
        $this->collSaranasRelatedByPrasaranaIdPartial = null;

        return $this;
    }

    /**
     * reset is the collSaranasRelatedByPrasaranaId collection loaded partially
     *
     * @return void
     */
    public function resetPartialSaranasRelatedByPrasaranaId($v = true)
    {
        $this->collSaranasRelatedByPrasaranaIdPartial = $v;
    }

    /**
     * Initializes the collSaranasRelatedByPrasaranaId collection.
     *
     * By default this just sets the collSaranasRelatedByPrasaranaId collection to an empty array (like clearcollSaranasRelatedByPrasaranaId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSaranasRelatedByPrasaranaId($overrideExisting = true)
    {
        if (null !== $this->collSaranasRelatedByPrasaranaId && !$overrideExisting) {
            return;
        }
        $this->collSaranasRelatedByPrasaranaId = new PropelObjectCollection();
        $this->collSaranasRelatedByPrasaranaId->setModel('Sarana');
    }

    /**
     * Gets an array of Sarana objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Prasarana is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Sarana[] List of Sarana objects
     * @throws PropelException
     */
    public function getSaranasRelatedByPrasaranaId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSaranasRelatedByPrasaranaIdPartial && !$this->isNew();
        if (null === $this->collSaranasRelatedByPrasaranaId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSaranasRelatedByPrasaranaId) {
                // return empty collection
                $this->initSaranasRelatedByPrasaranaId();
            } else {
                $collSaranasRelatedByPrasaranaId = SaranaQuery::create(null, $criteria)
                    ->filterByPrasaranaRelatedByPrasaranaId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSaranasRelatedByPrasaranaIdPartial && count($collSaranasRelatedByPrasaranaId)) {
                      $this->initSaranasRelatedByPrasaranaId(false);

                      foreach($collSaranasRelatedByPrasaranaId as $obj) {
                        if (false == $this->collSaranasRelatedByPrasaranaId->contains($obj)) {
                          $this->collSaranasRelatedByPrasaranaId->append($obj);
                        }
                      }

                      $this->collSaranasRelatedByPrasaranaIdPartial = true;
                    }

                    $collSaranasRelatedByPrasaranaId->getInternalIterator()->rewind();
                    return $collSaranasRelatedByPrasaranaId;
                }

                if($partial && $this->collSaranasRelatedByPrasaranaId) {
                    foreach($this->collSaranasRelatedByPrasaranaId as $obj) {
                        if($obj->isNew()) {
                            $collSaranasRelatedByPrasaranaId[] = $obj;
                        }
                    }
                }

                $this->collSaranasRelatedByPrasaranaId = $collSaranasRelatedByPrasaranaId;
                $this->collSaranasRelatedByPrasaranaIdPartial = false;
            }
        }

        return $this->collSaranasRelatedByPrasaranaId;
    }

    /**
     * Sets a collection of SaranaRelatedByPrasaranaId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $saranasRelatedByPrasaranaId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Prasarana The current object (for fluent API support)
     */
    public function setSaranasRelatedByPrasaranaId(PropelCollection $saranasRelatedByPrasaranaId, PropelPDO $con = null)
    {
        $saranasRelatedByPrasaranaIdToDelete = $this->getSaranasRelatedByPrasaranaId(new Criteria(), $con)->diff($saranasRelatedByPrasaranaId);

        $this->saranasRelatedByPrasaranaIdScheduledForDeletion = unserialize(serialize($saranasRelatedByPrasaranaIdToDelete));

        foreach ($saranasRelatedByPrasaranaIdToDelete as $saranaRelatedByPrasaranaIdRemoved) {
            $saranaRelatedByPrasaranaIdRemoved->setPrasaranaRelatedByPrasaranaId(null);
        }

        $this->collSaranasRelatedByPrasaranaId = null;
        foreach ($saranasRelatedByPrasaranaId as $saranaRelatedByPrasaranaId) {
            $this->addSaranaRelatedByPrasaranaId($saranaRelatedByPrasaranaId);
        }

        $this->collSaranasRelatedByPrasaranaId = $saranasRelatedByPrasaranaId;
        $this->collSaranasRelatedByPrasaranaIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Sarana objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Sarana objects.
     * @throws PropelException
     */
    public function countSaranasRelatedByPrasaranaId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSaranasRelatedByPrasaranaIdPartial && !$this->isNew();
        if (null === $this->collSaranasRelatedByPrasaranaId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSaranasRelatedByPrasaranaId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSaranasRelatedByPrasaranaId());
            }
            $query = SaranaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPrasaranaRelatedByPrasaranaId($this)
                ->count($con);
        }

        return count($this->collSaranasRelatedByPrasaranaId);
    }

    /**
     * Method called to associate a Sarana object to this object
     * through the Sarana foreign key attribute.
     *
     * @param    Sarana $l Sarana
     * @return Prasarana The current object (for fluent API support)
     */
    public function addSaranaRelatedByPrasaranaId(Sarana $l)
    {
        if ($this->collSaranasRelatedByPrasaranaId === null) {
            $this->initSaranasRelatedByPrasaranaId();
            $this->collSaranasRelatedByPrasaranaIdPartial = true;
        }
        if (!in_array($l, $this->collSaranasRelatedByPrasaranaId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSaranaRelatedByPrasaranaId($l);
        }

        return $this;
    }

    /**
     * @param	SaranaRelatedByPrasaranaId $saranaRelatedByPrasaranaId The saranaRelatedByPrasaranaId object to add.
     */
    protected function doAddSaranaRelatedByPrasaranaId($saranaRelatedByPrasaranaId)
    {
        $this->collSaranasRelatedByPrasaranaId[]= $saranaRelatedByPrasaranaId;
        $saranaRelatedByPrasaranaId->setPrasaranaRelatedByPrasaranaId($this);
    }

    /**
     * @param	SaranaRelatedByPrasaranaId $saranaRelatedByPrasaranaId The saranaRelatedByPrasaranaId object to remove.
     * @return Prasarana The current object (for fluent API support)
     */
    public function removeSaranaRelatedByPrasaranaId($saranaRelatedByPrasaranaId)
    {
        if ($this->getSaranasRelatedByPrasaranaId()->contains($saranaRelatedByPrasaranaId)) {
            $this->collSaranasRelatedByPrasaranaId->remove($this->collSaranasRelatedByPrasaranaId->search($saranaRelatedByPrasaranaId));
            if (null === $this->saranasRelatedByPrasaranaIdScheduledForDeletion) {
                $this->saranasRelatedByPrasaranaIdScheduledForDeletion = clone $this->collSaranasRelatedByPrasaranaId;
                $this->saranasRelatedByPrasaranaIdScheduledForDeletion->clear();
            }
            $this->saranasRelatedByPrasaranaIdScheduledForDeletion[]= $saranaRelatedByPrasaranaId;
            $saranaRelatedByPrasaranaId->setPrasaranaRelatedByPrasaranaId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related SaranasRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sarana[] List of Sarana objects
     */
    public function getSaranasRelatedByPrasaranaIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SaranaQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getSaranasRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related SaranasRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sarana[] List of Sarana objects
     */
    public function getSaranasRelatedByPrasaranaIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SaranaQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getSaranasRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related SaranasRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sarana[] List of Sarana objects
     */
    public function getSaranasRelatedByPrasaranaIdJoinJenisSaranaRelatedByJenisSaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SaranaQuery::create(null, $criteria);
        $query->joinWith('JenisSaranaRelatedByJenisSaranaId', $join_behavior);

        return $this->getSaranasRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related SaranasRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sarana[] List of Sarana objects
     */
    public function getSaranasRelatedByPrasaranaIdJoinJenisSaranaRelatedByJenisSaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SaranaQuery::create(null, $criteria);
        $query->joinWith('JenisSaranaRelatedByJenisSaranaId', $join_behavior);

        return $this->getSaranasRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related SaranasRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sarana[] List of Sarana objects
     */
    public function getSaranasRelatedByPrasaranaIdJoinStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SaranaQuery::create(null, $criteria);
        $query->joinWith('StatusKepemilikanSarprasRelatedByKepemilikanSarprasId', $join_behavior);

        return $this->getSaranasRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related SaranasRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sarana[] List of Sarana objects
     */
    public function getSaranasRelatedByPrasaranaIdJoinStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SaranaQuery::create(null, $criteria);
        $query->joinWith('StatusKepemilikanSarprasRelatedByKepemilikanSarprasId', $join_behavior);

        return $this->getSaranasRelatedByPrasaranaId($query, $con);
    }

    /**
     * Clears out the collSaranasRelatedByPrasaranaId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Prasarana The current object (for fluent API support)
     * @see        addSaranasRelatedByPrasaranaId()
     */
    public function clearSaranasRelatedByPrasaranaId()
    {
        $this->collSaranasRelatedByPrasaranaId = null; // important to set this to null since that means it is uninitialized
        $this->collSaranasRelatedByPrasaranaIdPartial = null;

        return $this;
    }

    /**
     * reset is the collSaranasRelatedByPrasaranaId collection loaded partially
     *
     * @return void
     */
    public function resetPartialSaranasRelatedByPrasaranaId($v = true)
    {
        $this->collSaranasRelatedByPrasaranaIdPartial = $v;
    }

    /**
     * Initializes the collSaranasRelatedByPrasaranaId collection.
     *
     * By default this just sets the collSaranasRelatedByPrasaranaId collection to an empty array (like clearcollSaranasRelatedByPrasaranaId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSaranasRelatedByPrasaranaId($overrideExisting = true)
    {
        if (null !== $this->collSaranasRelatedByPrasaranaId && !$overrideExisting) {
            return;
        }
        $this->collSaranasRelatedByPrasaranaId = new PropelObjectCollection();
        $this->collSaranasRelatedByPrasaranaId->setModel('Sarana');
    }

    /**
     * Gets an array of Sarana objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Prasarana is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Sarana[] List of Sarana objects
     * @throws PropelException
     */
    public function getSaranasRelatedByPrasaranaId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSaranasRelatedByPrasaranaIdPartial && !$this->isNew();
        if (null === $this->collSaranasRelatedByPrasaranaId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSaranasRelatedByPrasaranaId) {
                // return empty collection
                $this->initSaranasRelatedByPrasaranaId();
            } else {
                $collSaranasRelatedByPrasaranaId = SaranaQuery::create(null, $criteria)
                    ->filterByPrasaranaRelatedByPrasaranaId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSaranasRelatedByPrasaranaIdPartial && count($collSaranasRelatedByPrasaranaId)) {
                      $this->initSaranasRelatedByPrasaranaId(false);

                      foreach($collSaranasRelatedByPrasaranaId as $obj) {
                        if (false == $this->collSaranasRelatedByPrasaranaId->contains($obj)) {
                          $this->collSaranasRelatedByPrasaranaId->append($obj);
                        }
                      }

                      $this->collSaranasRelatedByPrasaranaIdPartial = true;
                    }

                    $collSaranasRelatedByPrasaranaId->getInternalIterator()->rewind();
                    return $collSaranasRelatedByPrasaranaId;
                }

                if($partial && $this->collSaranasRelatedByPrasaranaId) {
                    foreach($this->collSaranasRelatedByPrasaranaId as $obj) {
                        if($obj->isNew()) {
                            $collSaranasRelatedByPrasaranaId[] = $obj;
                        }
                    }
                }

                $this->collSaranasRelatedByPrasaranaId = $collSaranasRelatedByPrasaranaId;
                $this->collSaranasRelatedByPrasaranaIdPartial = false;
            }
        }

        return $this->collSaranasRelatedByPrasaranaId;
    }

    /**
     * Sets a collection of SaranaRelatedByPrasaranaId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $saranasRelatedByPrasaranaId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Prasarana The current object (for fluent API support)
     */
    public function setSaranasRelatedByPrasaranaId(PropelCollection $saranasRelatedByPrasaranaId, PropelPDO $con = null)
    {
        $saranasRelatedByPrasaranaIdToDelete = $this->getSaranasRelatedByPrasaranaId(new Criteria(), $con)->diff($saranasRelatedByPrasaranaId);

        $this->saranasRelatedByPrasaranaIdScheduledForDeletion = unserialize(serialize($saranasRelatedByPrasaranaIdToDelete));

        foreach ($saranasRelatedByPrasaranaIdToDelete as $saranaRelatedByPrasaranaIdRemoved) {
            $saranaRelatedByPrasaranaIdRemoved->setPrasaranaRelatedByPrasaranaId(null);
        }

        $this->collSaranasRelatedByPrasaranaId = null;
        foreach ($saranasRelatedByPrasaranaId as $saranaRelatedByPrasaranaId) {
            $this->addSaranaRelatedByPrasaranaId($saranaRelatedByPrasaranaId);
        }

        $this->collSaranasRelatedByPrasaranaId = $saranasRelatedByPrasaranaId;
        $this->collSaranasRelatedByPrasaranaIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Sarana objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Sarana objects.
     * @throws PropelException
     */
    public function countSaranasRelatedByPrasaranaId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSaranasRelatedByPrasaranaIdPartial && !$this->isNew();
        if (null === $this->collSaranasRelatedByPrasaranaId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSaranasRelatedByPrasaranaId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSaranasRelatedByPrasaranaId());
            }
            $query = SaranaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPrasaranaRelatedByPrasaranaId($this)
                ->count($con);
        }

        return count($this->collSaranasRelatedByPrasaranaId);
    }

    /**
     * Method called to associate a Sarana object to this object
     * through the Sarana foreign key attribute.
     *
     * @param    Sarana $l Sarana
     * @return Prasarana The current object (for fluent API support)
     */
    public function addSaranaRelatedByPrasaranaId(Sarana $l)
    {
        if ($this->collSaranasRelatedByPrasaranaId === null) {
            $this->initSaranasRelatedByPrasaranaId();
            $this->collSaranasRelatedByPrasaranaIdPartial = true;
        }
        if (!in_array($l, $this->collSaranasRelatedByPrasaranaId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSaranaRelatedByPrasaranaId($l);
        }

        return $this;
    }

    /**
     * @param	SaranaRelatedByPrasaranaId $saranaRelatedByPrasaranaId The saranaRelatedByPrasaranaId object to add.
     */
    protected function doAddSaranaRelatedByPrasaranaId($saranaRelatedByPrasaranaId)
    {
        $this->collSaranasRelatedByPrasaranaId[]= $saranaRelatedByPrasaranaId;
        $saranaRelatedByPrasaranaId->setPrasaranaRelatedByPrasaranaId($this);
    }

    /**
     * @param	SaranaRelatedByPrasaranaId $saranaRelatedByPrasaranaId The saranaRelatedByPrasaranaId object to remove.
     * @return Prasarana The current object (for fluent API support)
     */
    public function removeSaranaRelatedByPrasaranaId($saranaRelatedByPrasaranaId)
    {
        if ($this->getSaranasRelatedByPrasaranaId()->contains($saranaRelatedByPrasaranaId)) {
            $this->collSaranasRelatedByPrasaranaId->remove($this->collSaranasRelatedByPrasaranaId->search($saranaRelatedByPrasaranaId));
            if (null === $this->saranasRelatedByPrasaranaIdScheduledForDeletion) {
                $this->saranasRelatedByPrasaranaIdScheduledForDeletion = clone $this->collSaranasRelatedByPrasaranaId;
                $this->saranasRelatedByPrasaranaIdScheduledForDeletion->clear();
            }
            $this->saranasRelatedByPrasaranaIdScheduledForDeletion[]= $saranaRelatedByPrasaranaId;
            $saranaRelatedByPrasaranaId->setPrasaranaRelatedByPrasaranaId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related SaranasRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sarana[] List of Sarana objects
     */
    public function getSaranasRelatedByPrasaranaIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SaranaQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getSaranasRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related SaranasRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sarana[] List of Sarana objects
     */
    public function getSaranasRelatedByPrasaranaIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SaranaQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getSaranasRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related SaranasRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sarana[] List of Sarana objects
     */
    public function getSaranasRelatedByPrasaranaIdJoinJenisSaranaRelatedByJenisSaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SaranaQuery::create(null, $criteria);
        $query->joinWith('JenisSaranaRelatedByJenisSaranaId', $join_behavior);

        return $this->getSaranasRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related SaranasRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sarana[] List of Sarana objects
     */
    public function getSaranasRelatedByPrasaranaIdJoinJenisSaranaRelatedByJenisSaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SaranaQuery::create(null, $criteria);
        $query->joinWith('JenisSaranaRelatedByJenisSaranaId', $join_behavior);

        return $this->getSaranasRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related SaranasRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sarana[] List of Sarana objects
     */
    public function getSaranasRelatedByPrasaranaIdJoinStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SaranaQuery::create(null, $criteria);
        $query->joinWith('StatusKepemilikanSarprasRelatedByKepemilikanSarprasId', $join_behavior);

        return $this->getSaranasRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related SaranasRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sarana[] List of Sarana objects
     */
    public function getSaranasRelatedByPrasaranaIdJoinStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SaranaQuery::create(null, $criteria);
        $query->joinWith('StatusKepemilikanSarprasRelatedByKepemilikanSarprasId', $join_behavior);

        return $this->getSaranasRelatedByPrasaranaId($query, $con);
    }

    /**
     * Clears out the collPrasaranaLongitudinalsRelatedByPrasaranaId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Prasarana The current object (for fluent API support)
     * @see        addPrasaranaLongitudinalsRelatedByPrasaranaId()
     */
    public function clearPrasaranaLongitudinalsRelatedByPrasaranaId()
    {
        $this->collPrasaranaLongitudinalsRelatedByPrasaranaId = null; // important to set this to null since that means it is uninitialized
        $this->collPrasaranaLongitudinalsRelatedByPrasaranaIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPrasaranaLongitudinalsRelatedByPrasaranaId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPrasaranaLongitudinalsRelatedByPrasaranaId($v = true)
    {
        $this->collPrasaranaLongitudinalsRelatedByPrasaranaIdPartial = $v;
    }

    /**
     * Initializes the collPrasaranaLongitudinalsRelatedByPrasaranaId collection.
     *
     * By default this just sets the collPrasaranaLongitudinalsRelatedByPrasaranaId collection to an empty array (like clearcollPrasaranaLongitudinalsRelatedByPrasaranaId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPrasaranaLongitudinalsRelatedByPrasaranaId($overrideExisting = true)
    {
        if (null !== $this->collPrasaranaLongitudinalsRelatedByPrasaranaId && !$overrideExisting) {
            return;
        }
        $this->collPrasaranaLongitudinalsRelatedByPrasaranaId = new PropelObjectCollection();
        $this->collPrasaranaLongitudinalsRelatedByPrasaranaId->setModel('PrasaranaLongitudinal');
    }

    /**
     * Gets an array of PrasaranaLongitudinal objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Prasarana is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|PrasaranaLongitudinal[] List of PrasaranaLongitudinal objects
     * @throws PropelException
     */
    public function getPrasaranaLongitudinalsRelatedByPrasaranaId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPrasaranaLongitudinalsRelatedByPrasaranaIdPartial && !$this->isNew();
        if (null === $this->collPrasaranaLongitudinalsRelatedByPrasaranaId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPrasaranaLongitudinalsRelatedByPrasaranaId) {
                // return empty collection
                $this->initPrasaranaLongitudinalsRelatedByPrasaranaId();
            } else {
                $collPrasaranaLongitudinalsRelatedByPrasaranaId = PrasaranaLongitudinalQuery::create(null, $criteria)
                    ->filterByPrasaranaRelatedByPrasaranaId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPrasaranaLongitudinalsRelatedByPrasaranaIdPartial && count($collPrasaranaLongitudinalsRelatedByPrasaranaId)) {
                      $this->initPrasaranaLongitudinalsRelatedByPrasaranaId(false);

                      foreach($collPrasaranaLongitudinalsRelatedByPrasaranaId as $obj) {
                        if (false == $this->collPrasaranaLongitudinalsRelatedByPrasaranaId->contains($obj)) {
                          $this->collPrasaranaLongitudinalsRelatedByPrasaranaId->append($obj);
                        }
                      }

                      $this->collPrasaranaLongitudinalsRelatedByPrasaranaIdPartial = true;
                    }

                    $collPrasaranaLongitudinalsRelatedByPrasaranaId->getInternalIterator()->rewind();
                    return $collPrasaranaLongitudinalsRelatedByPrasaranaId;
                }

                if($partial && $this->collPrasaranaLongitudinalsRelatedByPrasaranaId) {
                    foreach($this->collPrasaranaLongitudinalsRelatedByPrasaranaId as $obj) {
                        if($obj->isNew()) {
                            $collPrasaranaLongitudinalsRelatedByPrasaranaId[] = $obj;
                        }
                    }
                }

                $this->collPrasaranaLongitudinalsRelatedByPrasaranaId = $collPrasaranaLongitudinalsRelatedByPrasaranaId;
                $this->collPrasaranaLongitudinalsRelatedByPrasaranaIdPartial = false;
            }
        }

        return $this->collPrasaranaLongitudinalsRelatedByPrasaranaId;
    }

    /**
     * Sets a collection of PrasaranaLongitudinalRelatedByPrasaranaId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $prasaranaLongitudinalsRelatedByPrasaranaId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Prasarana The current object (for fluent API support)
     */
    public function setPrasaranaLongitudinalsRelatedByPrasaranaId(PropelCollection $prasaranaLongitudinalsRelatedByPrasaranaId, PropelPDO $con = null)
    {
        $prasaranaLongitudinalsRelatedByPrasaranaIdToDelete = $this->getPrasaranaLongitudinalsRelatedByPrasaranaId(new Criteria(), $con)->diff($prasaranaLongitudinalsRelatedByPrasaranaId);

        $this->prasaranaLongitudinalsRelatedByPrasaranaIdScheduledForDeletion = unserialize(serialize($prasaranaLongitudinalsRelatedByPrasaranaIdToDelete));

        foreach ($prasaranaLongitudinalsRelatedByPrasaranaIdToDelete as $prasaranaLongitudinalRelatedByPrasaranaIdRemoved) {
            $prasaranaLongitudinalRelatedByPrasaranaIdRemoved->setPrasaranaRelatedByPrasaranaId(null);
        }

        $this->collPrasaranaLongitudinalsRelatedByPrasaranaId = null;
        foreach ($prasaranaLongitudinalsRelatedByPrasaranaId as $prasaranaLongitudinalRelatedByPrasaranaId) {
            $this->addPrasaranaLongitudinalRelatedByPrasaranaId($prasaranaLongitudinalRelatedByPrasaranaId);
        }

        $this->collPrasaranaLongitudinalsRelatedByPrasaranaId = $prasaranaLongitudinalsRelatedByPrasaranaId;
        $this->collPrasaranaLongitudinalsRelatedByPrasaranaIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PrasaranaLongitudinal objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related PrasaranaLongitudinal objects.
     * @throws PropelException
     */
    public function countPrasaranaLongitudinalsRelatedByPrasaranaId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPrasaranaLongitudinalsRelatedByPrasaranaIdPartial && !$this->isNew();
        if (null === $this->collPrasaranaLongitudinalsRelatedByPrasaranaId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPrasaranaLongitudinalsRelatedByPrasaranaId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPrasaranaLongitudinalsRelatedByPrasaranaId());
            }
            $query = PrasaranaLongitudinalQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPrasaranaRelatedByPrasaranaId($this)
                ->count($con);
        }

        return count($this->collPrasaranaLongitudinalsRelatedByPrasaranaId);
    }

    /**
     * Method called to associate a PrasaranaLongitudinal object to this object
     * through the PrasaranaLongitudinal foreign key attribute.
     *
     * @param    PrasaranaLongitudinal $l PrasaranaLongitudinal
     * @return Prasarana The current object (for fluent API support)
     */
    public function addPrasaranaLongitudinalRelatedByPrasaranaId(PrasaranaLongitudinal $l)
    {
        if ($this->collPrasaranaLongitudinalsRelatedByPrasaranaId === null) {
            $this->initPrasaranaLongitudinalsRelatedByPrasaranaId();
            $this->collPrasaranaLongitudinalsRelatedByPrasaranaIdPartial = true;
        }
        if (!in_array($l, $this->collPrasaranaLongitudinalsRelatedByPrasaranaId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPrasaranaLongitudinalRelatedByPrasaranaId($l);
        }

        return $this;
    }

    /**
     * @param	PrasaranaLongitudinalRelatedByPrasaranaId $prasaranaLongitudinalRelatedByPrasaranaId The prasaranaLongitudinalRelatedByPrasaranaId object to add.
     */
    protected function doAddPrasaranaLongitudinalRelatedByPrasaranaId($prasaranaLongitudinalRelatedByPrasaranaId)
    {
        $this->collPrasaranaLongitudinalsRelatedByPrasaranaId[]= $prasaranaLongitudinalRelatedByPrasaranaId;
        $prasaranaLongitudinalRelatedByPrasaranaId->setPrasaranaRelatedByPrasaranaId($this);
    }

    /**
     * @param	PrasaranaLongitudinalRelatedByPrasaranaId $prasaranaLongitudinalRelatedByPrasaranaId The prasaranaLongitudinalRelatedByPrasaranaId object to remove.
     * @return Prasarana The current object (for fluent API support)
     */
    public function removePrasaranaLongitudinalRelatedByPrasaranaId($prasaranaLongitudinalRelatedByPrasaranaId)
    {
        if ($this->getPrasaranaLongitudinalsRelatedByPrasaranaId()->contains($prasaranaLongitudinalRelatedByPrasaranaId)) {
            $this->collPrasaranaLongitudinalsRelatedByPrasaranaId->remove($this->collPrasaranaLongitudinalsRelatedByPrasaranaId->search($prasaranaLongitudinalRelatedByPrasaranaId));
            if (null === $this->prasaranaLongitudinalsRelatedByPrasaranaIdScheduledForDeletion) {
                $this->prasaranaLongitudinalsRelatedByPrasaranaIdScheduledForDeletion = clone $this->collPrasaranaLongitudinalsRelatedByPrasaranaId;
                $this->prasaranaLongitudinalsRelatedByPrasaranaIdScheduledForDeletion->clear();
            }
            $this->prasaranaLongitudinalsRelatedByPrasaranaIdScheduledForDeletion[]= clone $prasaranaLongitudinalRelatedByPrasaranaId;
            $prasaranaLongitudinalRelatedByPrasaranaId->setPrasaranaRelatedByPrasaranaId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related PrasaranaLongitudinalsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PrasaranaLongitudinal[] List of PrasaranaLongitudinal objects
     */
    public function getPrasaranaLongitudinalsRelatedByPrasaranaIdJoinSemesterRelatedBySemesterId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PrasaranaLongitudinalQuery::create(null, $criteria);
        $query->joinWith('SemesterRelatedBySemesterId', $join_behavior);

        return $this->getPrasaranaLongitudinalsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related PrasaranaLongitudinalsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PrasaranaLongitudinal[] List of PrasaranaLongitudinal objects
     */
    public function getPrasaranaLongitudinalsRelatedByPrasaranaIdJoinSemesterRelatedBySemesterId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PrasaranaLongitudinalQuery::create(null, $criteria);
        $query->joinWith('SemesterRelatedBySemesterId', $join_behavior);

        return $this->getPrasaranaLongitudinalsRelatedByPrasaranaId($query, $con);
    }

    /**
     * Clears out the collPrasaranaLongitudinalsRelatedByPrasaranaId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Prasarana The current object (for fluent API support)
     * @see        addPrasaranaLongitudinalsRelatedByPrasaranaId()
     */
    public function clearPrasaranaLongitudinalsRelatedByPrasaranaId()
    {
        $this->collPrasaranaLongitudinalsRelatedByPrasaranaId = null; // important to set this to null since that means it is uninitialized
        $this->collPrasaranaLongitudinalsRelatedByPrasaranaIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPrasaranaLongitudinalsRelatedByPrasaranaId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPrasaranaLongitudinalsRelatedByPrasaranaId($v = true)
    {
        $this->collPrasaranaLongitudinalsRelatedByPrasaranaIdPartial = $v;
    }

    /**
     * Initializes the collPrasaranaLongitudinalsRelatedByPrasaranaId collection.
     *
     * By default this just sets the collPrasaranaLongitudinalsRelatedByPrasaranaId collection to an empty array (like clearcollPrasaranaLongitudinalsRelatedByPrasaranaId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPrasaranaLongitudinalsRelatedByPrasaranaId($overrideExisting = true)
    {
        if (null !== $this->collPrasaranaLongitudinalsRelatedByPrasaranaId && !$overrideExisting) {
            return;
        }
        $this->collPrasaranaLongitudinalsRelatedByPrasaranaId = new PropelObjectCollection();
        $this->collPrasaranaLongitudinalsRelatedByPrasaranaId->setModel('PrasaranaLongitudinal');
    }

    /**
     * Gets an array of PrasaranaLongitudinal objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Prasarana is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|PrasaranaLongitudinal[] List of PrasaranaLongitudinal objects
     * @throws PropelException
     */
    public function getPrasaranaLongitudinalsRelatedByPrasaranaId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPrasaranaLongitudinalsRelatedByPrasaranaIdPartial && !$this->isNew();
        if (null === $this->collPrasaranaLongitudinalsRelatedByPrasaranaId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPrasaranaLongitudinalsRelatedByPrasaranaId) {
                // return empty collection
                $this->initPrasaranaLongitudinalsRelatedByPrasaranaId();
            } else {
                $collPrasaranaLongitudinalsRelatedByPrasaranaId = PrasaranaLongitudinalQuery::create(null, $criteria)
                    ->filterByPrasaranaRelatedByPrasaranaId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPrasaranaLongitudinalsRelatedByPrasaranaIdPartial && count($collPrasaranaLongitudinalsRelatedByPrasaranaId)) {
                      $this->initPrasaranaLongitudinalsRelatedByPrasaranaId(false);

                      foreach($collPrasaranaLongitudinalsRelatedByPrasaranaId as $obj) {
                        if (false == $this->collPrasaranaLongitudinalsRelatedByPrasaranaId->contains($obj)) {
                          $this->collPrasaranaLongitudinalsRelatedByPrasaranaId->append($obj);
                        }
                      }

                      $this->collPrasaranaLongitudinalsRelatedByPrasaranaIdPartial = true;
                    }

                    $collPrasaranaLongitudinalsRelatedByPrasaranaId->getInternalIterator()->rewind();
                    return $collPrasaranaLongitudinalsRelatedByPrasaranaId;
                }

                if($partial && $this->collPrasaranaLongitudinalsRelatedByPrasaranaId) {
                    foreach($this->collPrasaranaLongitudinalsRelatedByPrasaranaId as $obj) {
                        if($obj->isNew()) {
                            $collPrasaranaLongitudinalsRelatedByPrasaranaId[] = $obj;
                        }
                    }
                }

                $this->collPrasaranaLongitudinalsRelatedByPrasaranaId = $collPrasaranaLongitudinalsRelatedByPrasaranaId;
                $this->collPrasaranaLongitudinalsRelatedByPrasaranaIdPartial = false;
            }
        }

        return $this->collPrasaranaLongitudinalsRelatedByPrasaranaId;
    }

    /**
     * Sets a collection of PrasaranaLongitudinalRelatedByPrasaranaId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $prasaranaLongitudinalsRelatedByPrasaranaId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Prasarana The current object (for fluent API support)
     */
    public function setPrasaranaLongitudinalsRelatedByPrasaranaId(PropelCollection $prasaranaLongitudinalsRelatedByPrasaranaId, PropelPDO $con = null)
    {
        $prasaranaLongitudinalsRelatedByPrasaranaIdToDelete = $this->getPrasaranaLongitudinalsRelatedByPrasaranaId(new Criteria(), $con)->diff($prasaranaLongitudinalsRelatedByPrasaranaId);

        $this->prasaranaLongitudinalsRelatedByPrasaranaIdScheduledForDeletion = unserialize(serialize($prasaranaLongitudinalsRelatedByPrasaranaIdToDelete));

        foreach ($prasaranaLongitudinalsRelatedByPrasaranaIdToDelete as $prasaranaLongitudinalRelatedByPrasaranaIdRemoved) {
            $prasaranaLongitudinalRelatedByPrasaranaIdRemoved->setPrasaranaRelatedByPrasaranaId(null);
        }

        $this->collPrasaranaLongitudinalsRelatedByPrasaranaId = null;
        foreach ($prasaranaLongitudinalsRelatedByPrasaranaId as $prasaranaLongitudinalRelatedByPrasaranaId) {
            $this->addPrasaranaLongitudinalRelatedByPrasaranaId($prasaranaLongitudinalRelatedByPrasaranaId);
        }

        $this->collPrasaranaLongitudinalsRelatedByPrasaranaId = $prasaranaLongitudinalsRelatedByPrasaranaId;
        $this->collPrasaranaLongitudinalsRelatedByPrasaranaIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PrasaranaLongitudinal objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related PrasaranaLongitudinal objects.
     * @throws PropelException
     */
    public function countPrasaranaLongitudinalsRelatedByPrasaranaId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPrasaranaLongitudinalsRelatedByPrasaranaIdPartial && !$this->isNew();
        if (null === $this->collPrasaranaLongitudinalsRelatedByPrasaranaId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPrasaranaLongitudinalsRelatedByPrasaranaId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPrasaranaLongitudinalsRelatedByPrasaranaId());
            }
            $query = PrasaranaLongitudinalQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPrasaranaRelatedByPrasaranaId($this)
                ->count($con);
        }

        return count($this->collPrasaranaLongitudinalsRelatedByPrasaranaId);
    }

    /**
     * Method called to associate a PrasaranaLongitudinal object to this object
     * through the PrasaranaLongitudinal foreign key attribute.
     *
     * @param    PrasaranaLongitudinal $l PrasaranaLongitudinal
     * @return Prasarana The current object (for fluent API support)
     */
    public function addPrasaranaLongitudinalRelatedByPrasaranaId(PrasaranaLongitudinal $l)
    {
        if ($this->collPrasaranaLongitudinalsRelatedByPrasaranaId === null) {
            $this->initPrasaranaLongitudinalsRelatedByPrasaranaId();
            $this->collPrasaranaLongitudinalsRelatedByPrasaranaIdPartial = true;
        }
        if (!in_array($l, $this->collPrasaranaLongitudinalsRelatedByPrasaranaId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPrasaranaLongitudinalRelatedByPrasaranaId($l);
        }

        return $this;
    }

    /**
     * @param	PrasaranaLongitudinalRelatedByPrasaranaId $prasaranaLongitudinalRelatedByPrasaranaId The prasaranaLongitudinalRelatedByPrasaranaId object to add.
     */
    protected function doAddPrasaranaLongitudinalRelatedByPrasaranaId($prasaranaLongitudinalRelatedByPrasaranaId)
    {
        $this->collPrasaranaLongitudinalsRelatedByPrasaranaId[]= $prasaranaLongitudinalRelatedByPrasaranaId;
        $prasaranaLongitudinalRelatedByPrasaranaId->setPrasaranaRelatedByPrasaranaId($this);
    }

    /**
     * @param	PrasaranaLongitudinalRelatedByPrasaranaId $prasaranaLongitudinalRelatedByPrasaranaId The prasaranaLongitudinalRelatedByPrasaranaId object to remove.
     * @return Prasarana The current object (for fluent API support)
     */
    public function removePrasaranaLongitudinalRelatedByPrasaranaId($prasaranaLongitudinalRelatedByPrasaranaId)
    {
        if ($this->getPrasaranaLongitudinalsRelatedByPrasaranaId()->contains($prasaranaLongitudinalRelatedByPrasaranaId)) {
            $this->collPrasaranaLongitudinalsRelatedByPrasaranaId->remove($this->collPrasaranaLongitudinalsRelatedByPrasaranaId->search($prasaranaLongitudinalRelatedByPrasaranaId));
            if (null === $this->prasaranaLongitudinalsRelatedByPrasaranaIdScheduledForDeletion) {
                $this->prasaranaLongitudinalsRelatedByPrasaranaIdScheduledForDeletion = clone $this->collPrasaranaLongitudinalsRelatedByPrasaranaId;
                $this->prasaranaLongitudinalsRelatedByPrasaranaIdScheduledForDeletion->clear();
            }
            $this->prasaranaLongitudinalsRelatedByPrasaranaIdScheduledForDeletion[]= clone $prasaranaLongitudinalRelatedByPrasaranaId;
            $prasaranaLongitudinalRelatedByPrasaranaId->setPrasaranaRelatedByPrasaranaId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related PrasaranaLongitudinalsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PrasaranaLongitudinal[] List of PrasaranaLongitudinal objects
     */
    public function getPrasaranaLongitudinalsRelatedByPrasaranaIdJoinSemesterRelatedBySemesterId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PrasaranaLongitudinalQuery::create(null, $criteria);
        $query->joinWith('SemesterRelatedBySemesterId', $join_behavior);

        return $this->getPrasaranaLongitudinalsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related PrasaranaLongitudinalsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PrasaranaLongitudinal[] List of PrasaranaLongitudinal objects
     */
    public function getPrasaranaLongitudinalsRelatedByPrasaranaIdJoinSemesterRelatedBySemesterId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PrasaranaLongitudinalQuery::create(null, $criteria);
        $query->joinWith('SemesterRelatedBySemesterId', $join_behavior);

        return $this->getPrasaranaLongitudinalsRelatedByPrasaranaId($query, $con);
    }

    /**
     * Clears out the collRombonganBelajarsRelatedByPrasaranaId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Prasarana The current object (for fluent API support)
     * @see        addRombonganBelajarsRelatedByPrasaranaId()
     */
    public function clearRombonganBelajarsRelatedByPrasaranaId()
    {
        $this->collRombonganBelajarsRelatedByPrasaranaId = null; // important to set this to null since that means it is uninitialized
        $this->collRombonganBelajarsRelatedByPrasaranaIdPartial = null;

        return $this;
    }

    /**
     * reset is the collRombonganBelajarsRelatedByPrasaranaId collection loaded partially
     *
     * @return void
     */
    public function resetPartialRombonganBelajarsRelatedByPrasaranaId($v = true)
    {
        $this->collRombonganBelajarsRelatedByPrasaranaIdPartial = $v;
    }

    /**
     * Initializes the collRombonganBelajarsRelatedByPrasaranaId collection.
     *
     * By default this just sets the collRombonganBelajarsRelatedByPrasaranaId collection to an empty array (like clearcollRombonganBelajarsRelatedByPrasaranaId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRombonganBelajarsRelatedByPrasaranaId($overrideExisting = true)
    {
        if (null !== $this->collRombonganBelajarsRelatedByPrasaranaId && !$overrideExisting) {
            return;
        }
        $this->collRombonganBelajarsRelatedByPrasaranaId = new PropelObjectCollection();
        $this->collRombonganBelajarsRelatedByPrasaranaId->setModel('RombonganBelajar');
    }

    /**
     * Gets an array of RombonganBelajar objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Prasarana is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     * @throws PropelException
     */
    public function getRombonganBelajarsRelatedByPrasaranaId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRombonganBelajarsRelatedByPrasaranaIdPartial && !$this->isNew();
        if (null === $this->collRombonganBelajarsRelatedByPrasaranaId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRombonganBelajarsRelatedByPrasaranaId) {
                // return empty collection
                $this->initRombonganBelajarsRelatedByPrasaranaId();
            } else {
                $collRombonganBelajarsRelatedByPrasaranaId = RombonganBelajarQuery::create(null, $criteria)
                    ->filterByPrasaranaRelatedByPrasaranaId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRombonganBelajarsRelatedByPrasaranaIdPartial && count($collRombonganBelajarsRelatedByPrasaranaId)) {
                      $this->initRombonganBelajarsRelatedByPrasaranaId(false);

                      foreach($collRombonganBelajarsRelatedByPrasaranaId as $obj) {
                        if (false == $this->collRombonganBelajarsRelatedByPrasaranaId->contains($obj)) {
                          $this->collRombonganBelajarsRelatedByPrasaranaId->append($obj);
                        }
                      }

                      $this->collRombonganBelajarsRelatedByPrasaranaIdPartial = true;
                    }

                    $collRombonganBelajarsRelatedByPrasaranaId->getInternalIterator()->rewind();
                    return $collRombonganBelajarsRelatedByPrasaranaId;
                }

                if($partial && $this->collRombonganBelajarsRelatedByPrasaranaId) {
                    foreach($this->collRombonganBelajarsRelatedByPrasaranaId as $obj) {
                        if($obj->isNew()) {
                            $collRombonganBelajarsRelatedByPrasaranaId[] = $obj;
                        }
                    }
                }

                $this->collRombonganBelajarsRelatedByPrasaranaId = $collRombonganBelajarsRelatedByPrasaranaId;
                $this->collRombonganBelajarsRelatedByPrasaranaIdPartial = false;
            }
        }

        return $this->collRombonganBelajarsRelatedByPrasaranaId;
    }

    /**
     * Sets a collection of RombonganBelajarRelatedByPrasaranaId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rombonganBelajarsRelatedByPrasaranaId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Prasarana The current object (for fluent API support)
     */
    public function setRombonganBelajarsRelatedByPrasaranaId(PropelCollection $rombonganBelajarsRelatedByPrasaranaId, PropelPDO $con = null)
    {
        $rombonganBelajarsRelatedByPrasaranaIdToDelete = $this->getRombonganBelajarsRelatedByPrasaranaId(new Criteria(), $con)->diff($rombonganBelajarsRelatedByPrasaranaId);

        $this->rombonganBelajarsRelatedByPrasaranaIdScheduledForDeletion = unserialize(serialize($rombonganBelajarsRelatedByPrasaranaIdToDelete));

        foreach ($rombonganBelajarsRelatedByPrasaranaIdToDelete as $rombonganBelajarRelatedByPrasaranaIdRemoved) {
            $rombonganBelajarRelatedByPrasaranaIdRemoved->setPrasaranaRelatedByPrasaranaId(null);
        }

        $this->collRombonganBelajarsRelatedByPrasaranaId = null;
        foreach ($rombonganBelajarsRelatedByPrasaranaId as $rombonganBelajarRelatedByPrasaranaId) {
            $this->addRombonganBelajarRelatedByPrasaranaId($rombonganBelajarRelatedByPrasaranaId);
        }

        $this->collRombonganBelajarsRelatedByPrasaranaId = $rombonganBelajarsRelatedByPrasaranaId;
        $this->collRombonganBelajarsRelatedByPrasaranaIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RombonganBelajar objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RombonganBelajar objects.
     * @throws PropelException
     */
    public function countRombonganBelajarsRelatedByPrasaranaId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRombonganBelajarsRelatedByPrasaranaIdPartial && !$this->isNew();
        if (null === $this->collRombonganBelajarsRelatedByPrasaranaId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRombonganBelajarsRelatedByPrasaranaId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getRombonganBelajarsRelatedByPrasaranaId());
            }
            $query = RombonganBelajarQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPrasaranaRelatedByPrasaranaId($this)
                ->count($con);
        }

        return count($this->collRombonganBelajarsRelatedByPrasaranaId);
    }

    /**
     * Method called to associate a RombonganBelajar object to this object
     * through the RombonganBelajar foreign key attribute.
     *
     * @param    RombonganBelajar $l RombonganBelajar
     * @return Prasarana The current object (for fluent API support)
     */
    public function addRombonganBelajarRelatedByPrasaranaId(RombonganBelajar $l)
    {
        if ($this->collRombonganBelajarsRelatedByPrasaranaId === null) {
            $this->initRombonganBelajarsRelatedByPrasaranaId();
            $this->collRombonganBelajarsRelatedByPrasaranaIdPartial = true;
        }
        if (!in_array($l, $this->collRombonganBelajarsRelatedByPrasaranaId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRombonganBelajarRelatedByPrasaranaId($l);
        }

        return $this;
    }

    /**
     * @param	RombonganBelajarRelatedByPrasaranaId $rombonganBelajarRelatedByPrasaranaId The rombonganBelajarRelatedByPrasaranaId object to add.
     */
    protected function doAddRombonganBelajarRelatedByPrasaranaId($rombonganBelajarRelatedByPrasaranaId)
    {
        $this->collRombonganBelajarsRelatedByPrasaranaId[]= $rombonganBelajarRelatedByPrasaranaId;
        $rombonganBelajarRelatedByPrasaranaId->setPrasaranaRelatedByPrasaranaId($this);
    }

    /**
     * @param	RombonganBelajarRelatedByPrasaranaId $rombonganBelajarRelatedByPrasaranaId The rombonganBelajarRelatedByPrasaranaId object to remove.
     * @return Prasarana The current object (for fluent API support)
     */
    public function removeRombonganBelajarRelatedByPrasaranaId($rombonganBelajarRelatedByPrasaranaId)
    {
        if ($this->getRombonganBelajarsRelatedByPrasaranaId()->contains($rombonganBelajarRelatedByPrasaranaId)) {
            $this->collRombonganBelajarsRelatedByPrasaranaId->remove($this->collRombonganBelajarsRelatedByPrasaranaId->search($rombonganBelajarRelatedByPrasaranaId));
            if (null === $this->rombonganBelajarsRelatedByPrasaranaIdScheduledForDeletion) {
                $this->rombonganBelajarsRelatedByPrasaranaIdScheduledForDeletion = clone $this->collRombonganBelajarsRelatedByPrasaranaId;
                $this->rombonganBelajarsRelatedByPrasaranaIdScheduledForDeletion->clear();
            }
            $this->rombonganBelajarsRelatedByPrasaranaIdScheduledForDeletion[]= clone $rombonganBelajarRelatedByPrasaranaId;
            $rombonganBelajarRelatedByPrasaranaId->setPrasaranaRelatedByPrasaranaId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPrasaranaIdJoinJurusanSpRelatedByJurusanSpId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('JurusanSpRelatedByJurusanSpId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPrasaranaIdJoinJurusanSpRelatedByJurusanSpId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('JurusanSpRelatedByJurusanSpId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPrasaranaIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPrasaranaIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPrasaranaIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPrasaranaIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPrasaranaIdJoinKebutuhanKhususRelatedByKebutuhanKhususId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPrasaranaIdJoinKebutuhanKhususRelatedByKebutuhanKhususId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPrasaranaIdJoinKurikulumRelatedByKurikulumId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('KurikulumRelatedByKurikulumId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPrasaranaIdJoinKurikulumRelatedByKurikulumId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('KurikulumRelatedByKurikulumId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPrasaranaIdJoinSemesterRelatedBySemesterId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('SemesterRelatedBySemesterId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPrasaranaIdJoinSemesterRelatedBySemesterId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('SemesterRelatedBySemesterId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPrasaranaIdJoinTingkatPendidikanRelatedByTingkatPendidikanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('TingkatPendidikanRelatedByTingkatPendidikanId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPrasaranaIdJoinTingkatPendidikanRelatedByTingkatPendidikanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('TingkatPendidikanRelatedByTingkatPendidikanId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPrasaranaId($query, $con);
    }

    /**
     * Clears out the collRombonganBelajarsRelatedByPrasaranaId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Prasarana The current object (for fluent API support)
     * @see        addRombonganBelajarsRelatedByPrasaranaId()
     */
    public function clearRombonganBelajarsRelatedByPrasaranaId()
    {
        $this->collRombonganBelajarsRelatedByPrasaranaId = null; // important to set this to null since that means it is uninitialized
        $this->collRombonganBelajarsRelatedByPrasaranaIdPartial = null;

        return $this;
    }

    /**
     * reset is the collRombonganBelajarsRelatedByPrasaranaId collection loaded partially
     *
     * @return void
     */
    public function resetPartialRombonganBelajarsRelatedByPrasaranaId($v = true)
    {
        $this->collRombonganBelajarsRelatedByPrasaranaIdPartial = $v;
    }

    /**
     * Initializes the collRombonganBelajarsRelatedByPrasaranaId collection.
     *
     * By default this just sets the collRombonganBelajarsRelatedByPrasaranaId collection to an empty array (like clearcollRombonganBelajarsRelatedByPrasaranaId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRombonganBelajarsRelatedByPrasaranaId($overrideExisting = true)
    {
        if (null !== $this->collRombonganBelajarsRelatedByPrasaranaId && !$overrideExisting) {
            return;
        }
        $this->collRombonganBelajarsRelatedByPrasaranaId = new PropelObjectCollection();
        $this->collRombonganBelajarsRelatedByPrasaranaId->setModel('RombonganBelajar');
    }

    /**
     * Gets an array of RombonganBelajar objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Prasarana is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     * @throws PropelException
     */
    public function getRombonganBelajarsRelatedByPrasaranaId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRombonganBelajarsRelatedByPrasaranaIdPartial && !$this->isNew();
        if (null === $this->collRombonganBelajarsRelatedByPrasaranaId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRombonganBelajarsRelatedByPrasaranaId) {
                // return empty collection
                $this->initRombonganBelajarsRelatedByPrasaranaId();
            } else {
                $collRombonganBelajarsRelatedByPrasaranaId = RombonganBelajarQuery::create(null, $criteria)
                    ->filterByPrasaranaRelatedByPrasaranaId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRombonganBelajarsRelatedByPrasaranaIdPartial && count($collRombonganBelajarsRelatedByPrasaranaId)) {
                      $this->initRombonganBelajarsRelatedByPrasaranaId(false);

                      foreach($collRombonganBelajarsRelatedByPrasaranaId as $obj) {
                        if (false == $this->collRombonganBelajarsRelatedByPrasaranaId->contains($obj)) {
                          $this->collRombonganBelajarsRelatedByPrasaranaId->append($obj);
                        }
                      }

                      $this->collRombonganBelajarsRelatedByPrasaranaIdPartial = true;
                    }

                    $collRombonganBelajarsRelatedByPrasaranaId->getInternalIterator()->rewind();
                    return $collRombonganBelajarsRelatedByPrasaranaId;
                }

                if($partial && $this->collRombonganBelajarsRelatedByPrasaranaId) {
                    foreach($this->collRombonganBelajarsRelatedByPrasaranaId as $obj) {
                        if($obj->isNew()) {
                            $collRombonganBelajarsRelatedByPrasaranaId[] = $obj;
                        }
                    }
                }

                $this->collRombonganBelajarsRelatedByPrasaranaId = $collRombonganBelajarsRelatedByPrasaranaId;
                $this->collRombonganBelajarsRelatedByPrasaranaIdPartial = false;
            }
        }

        return $this->collRombonganBelajarsRelatedByPrasaranaId;
    }

    /**
     * Sets a collection of RombonganBelajarRelatedByPrasaranaId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rombonganBelajarsRelatedByPrasaranaId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Prasarana The current object (for fluent API support)
     */
    public function setRombonganBelajarsRelatedByPrasaranaId(PropelCollection $rombonganBelajarsRelatedByPrasaranaId, PropelPDO $con = null)
    {
        $rombonganBelajarsRelatedByPrasaranaIdToDelete = $this->getRombonganBelajarsRelatedByPrasaranaId(new Criteria(), $con)->diff($rombonganBelajarsRelatedByPrasaranaId);

        $this->rombonganBelajarsRelatedByPrasaranaIdScheduledForDeletion = unserialize(serialize($rombonganBelajarsRelatedByPrasaranaIdToDelete));

        foreach ($rombonganBelajarsRelatedByPrasaranaIdToDelete as $rombonganBelajarRelatedByPrasaranaIdRemoved) {
            $rombonganBelajarRelatedByPrasaranaIdRemoved->setPrasaranaRelatedByPrasaranaId(null);
        }

        $this->collRombonganBelajarsRelatedByPrasaranaId = null;
        foreach ($rombonganBelajarsRelatedByPrasaranaId as $rombonganBelajarRelatedByPrasaranaId) {
            $this->addRombonganBelajarRelatedByPrasaranaId($rombonganBelajarRelatedByPrasaranaId);
        }

        $this->collRombonganBelajarsRelatedByPrasaranaId = $rombonganBelajarsRelatedByPrasaranaId;
        $this->collRombonganBelajarsRelatedByPrasaranaIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RombonganBelajar objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RombonganBelajar objects.
     * @throws PropelException
     */
    public function countRombonganBelajarsRelatedByPrasaranaId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRombonganBelajarsRelatedByPrasaranaIdPartial && !$this->isNew();
        if (null === $this->collRombonganBelajarsRelatedByPrasaranaId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRombonganBelajarsRelatedByPrasaranaId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getRombonganBelajarsRelatedByPrasaranaId());
            }
            $query = RombonganBelajarQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPrasaranaRelatedByPrasaranaId($this)
                ->count($con);
        }

        return count($this->collRombonganBelajarsRelatedByPrasaranaId);
    }

    /**
     * Method called to associate a RombonganBelajar object to this object
     * through the RombonganBelajar foreign key attribute.
     *
     * @param    RombonganBelajar $l RombonganBelajar
     * @return Prasarana The current object (for fluent API support)
     */
    public function addRombonganBelajarRelatedByPrasaranaId(RombonganBelajar $l)
    {
        if ($this->collRombonganBelajarsRelatedByPrasaranaId === null) {
            $this->initRombonganBelajarsRelatedByPrasaranaId();
            $this->collRombonganBelajarsRelatedByPrasaranaIdPartial = true;
        }
        if (!in_array($l, $this->collRombonganBelajarsRelatedByPrasaranaId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRombonganBelajarRelatedByPrasaranaId($l);
        }

        return $this;
    }

    /**
     * @param	RombonganBelajarRelatedByPrasaranaId $rombonganBelajarRelatedByPrasaranaId The rombonganBelajarRelatedByPrasaranaId object to add.
     */
    protected function doAddRombonganBelajarRelatedByPrasaranaId($rombonganBelajarRelatedByPrasaranaId)
    {
        $this->collRombonganBelajarsRelatedByPrasaranaId[]= $rombonganBelajarRelatedByPrasaranaId;
        $rombonganBelajarRelatedByPrasaranaId->setPrasaranaRelatedByPrasaranaId($this);
    }

    /**
     * @param	RombonganBelajarRelatedByPrasaranaId $rombonganBelajarRelatedByPrasaranaId The rombonganBelajarRelatedByPrasaranaId object to remove.
     * @return Prasarana The current object (for fluent API support)
     */
    public function removeRombonganBelajarRelatedByPrasaranaId($rombonganBelajarRelatedByPrasaranaId)
    {
        if ($this->getRombonganBelajarsRelatedByPrasaranaId()->contains($rombonganBelajarRelatedByPrasaranaId)) {
            $this->collRombonganBelajarsRelatedByPrasaranaId->remove($this->collRombonganBelajarsRelatedByPrasaranaId->search($rombonganBelajarRelatedByPrasaranaId));
            if (null === $this->rombonganBelajarsRelatedByPrasaranaIdScheduledForDeletion) {
                $this->rombonganBelajarsRelatedByPrasaranaIdScheduledForDeletion = clone $this->collRombonganBelajarsRelatedByPrasaranaId;
                $this->rombonganBelajarsRelatedByPrasaranaIdScheduledForDeletion->clear();
            }
            $this->rombonganBelajarsRelatedByPrasaranaIdScheduledForDeletion[]= clone $rombonganBelajarRelatedByPrasaranaId;
            $rombonganBelajarRelatedByPrasaranaId->setPrasaranaRelatedByPrasaranaId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPrasaranaIdJoinJurusanSpRelatedByJurusanSpId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('JurusanSpRelatedByJurusanSpId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPrasaranaIdJoinJurusanSpRelatedByJurusanSpId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('JurusanSpRelatedByJurusanSpId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPrasaranaIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPrasaranaIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPrasaranaIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPrasaranaIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPrasaranaIdJoinKebutuhanKhususRelatedByKebutuhanKhususId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPrasaranaIdJoinKebutuhanKhususRelatedByKebutuhanKhususId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPrasaranaIdJoinKurikulumRelatedByKurikulumId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('KurikulumRelatedByKurikulumId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPrasaranaIdJoinKurikulumRelatedByKurikulumId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('KurikulumRelatedByKurikulumId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPrasaranaIdJoinSemesterRelatedBySemesterId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('SemesterRelatedBySemesterId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPrasaranaIdJoinSemesterRelatedBySemesterId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('SemesterRelatedBySemesterId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPrasaranaIdJoinTingkatPendidikanRelatedByTingkatPendidikanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('TingkatPendidikanRelatedByTingkatPendidikanId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPrasaranaIdJoinTingkatPendidikanRelatedByTingkatPendidikanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('TingkatPendidikanRelatedByTingkatPendidikanId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPrasaranaId($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->prasarana_id = null;
        $this->jenis_prasarana_id = null;
        $this->sekolah_id = null;
        $this->kepemilikan_sarpras_id = null;
        $this->nama = null;
        $this->panjang = null;
        $this->lebar = null;
        $this->last_update = null;
        $this->soft_delete = null;
        $this->last_sync = null;
        $this->updater_id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collBukuAlatsRelatedByPrasaranaId) {
                foreach ($this->collBukuAlatsRelatedByPrasaranaId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collBukuAlatsRelatedByPrasaranaId) {
                foreach ($this->collBukuAlatsRelatedByPrasaranaId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldPrasaranasRelatedByPrasaranaId) {
                foreach ($this->collVldPrasaranasRelatedByPrasaranaId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldPrasaranasRelatedByPrasaranaId) {
                foreach ($this->collVldPrasaranasRelatedByPrasaranaId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSaranasRelatedByPrasaranaId) {
                foreach ($this->collSaranasRelatedByPrasaranaId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSaranasRelatedByPrasaranaId) {
                foreach ($this->collSaranasRelatedByPrasaranaId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPrasaranaLongitudinalsRelatedByPrasaranaId) {
                foreach ($this->collPrasaranaLongitudinalsRelatedByPrasaranaId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPrasaranaLongitudinalsRelatedByPrasaranaId) {
                foreach ($this->collPrasaranaLongitudinalsRelatedByPrasaranaId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRombonganBelajarsRelatedByPrasaranaId) {
                foreach ($this->collRombonganBelajarsRelatedByPrasaranaId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRombonganBelajarsRelatedByPrasaranaId) {
                foreach ($this->collRombonganBelajarsRelatedByPrasaranaId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aSekolahRelatedBySekolahId instanceof Persistent) {
              $this->aSekolahRelatedBySekolahId->clearAllReferences($deep);
            }
            if ($this->aSekolahRelatedBySekolahId instanceof Persistent) {
              $this->aSekolahRelatedBySekolahId->clearAllReferences($deep);
            }
            if ($this->aJenisPrasaranaRelatedByJenisPrasaranaId instanceof Persistent) {
              $this->aJenisPrasaranaRelatedByJenisPrasaranaId->clearAllReferences($deep);
            }
            if ($this->aJenisPrasaranaRelatedByJenisPrasaranaId instanceof Persistent) {
              $this->aJenisPrasaranaRelatedByJenisPrasaranaId->clearAllReferences($deep);
            }
            if ($this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId instanceof Persistent) {
              $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId->clearAllReferences($deep);
            }
            if ($this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId instanceof Persistent) {
              $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collBukuAlatsRelatedByPrasaranaId instanceof PropelCollection) {
            $this->collBukuAlatsRelatedByPrasaranaId->clearIterator();
        }
        $this->collBukuAlatsRelatedByPrasaranaId = null;
        if ($this->collBukuAlatsRelatedByPrasaranaId instanceof PropelCollection) {
            $this->collBukuAlatsRelatedByPrasaranaId->clearIterator();
        }
        $this->collBukuAlatsRelatedByPrasaranaId = null;
        if ($this->collVldPrasaranasRelatedByPrasaranaId instanceof PropelCollection) {
            $this->collVldPrasaranasRelatedByPrasaranaId->clearIterator();
        }
        $this->collVldPrasaranasRelatedByPrasaranaId = null;
        if ($this->collVldPrasaranasRelatedByPrasaranaId instanceof PropelCollection) {
            $this->collVldPrasaranasRelatedByPrasaranaId->clearIterator();
        }
        $this->collVldPrasaranasRelatedByPrasaranaId = null;
        if ($this->collSaranasRelatedByPrasaranaId instanceof PropelCollection) {
            $this->collSaranasRelatedByPrasaranaId->clearIterator();
        }
        $this->collSaranasRelatedByPrasaranaId = null;
        if ($this->collSaranasRelatedByPrasaranaId instanceof PropelCollection) {
            $this->collSaranasRelatedByPrasaranaId->clearIterator();
        }
        $this->collSaranasRelatedByPrasaranaId = null;
        if ($this->collPrasaranaLongitudinalsRelatedByPrasaranaId instanceof PropelCollection) {
            $this->collPrasaranaLongitudinalsRelatedByPrasaranaId->clearIterator();
        }
        $this->collPrasaranaLongitudinalsRelatedByPrasaranaId = null;
        if ($this->collPrasaranaLongitudinalsRelatedByPrasaranaId instanceof PropelCollection) {
            $this->collPrasaranaLongitudinalsRelatedByPrasaranaId->clearIterator();
        }
        $this->collPrasaranaLongitudinalsRelatedByPrasaranaId = null;
        if ($this->collRombonganBelajarsRelatedByPrasaranaId instanceof PropelCollection) {
            $this->collRombonganBelajarsRelatedByPrasaranaId->clearIterator();
        }
        $this->collRombonganBelajarsRelatedByPrasaranaId = null;
        if ($this->collRombonganBelajarsRelatedByPrasaranaId instanceof PropelCollection) {
            $this->collRombonganBelajarsRelatedByPrasaranaId->clearIterator();
        }
        $this->collRombonganBelajarsRelatedByPrasaranaId = null;
        $this->aSekolahRelatedBySekolahId = null;
        $this->aSekolahRelatedBySekolahId = null;
        $this->aJenisPrasaranaRelatedByJenisPrasaranaId = null;
        $this->aJenisPrasaranaRelatedByJenisPrasaranaId = null;
        $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId = null;
        $this->aStatusKepemilikanSarprasRelatedByKepemilikanSarprasId = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(PrasaranaPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
