<?php

namespace angulex\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\BukuAlat;
use angulex\Model\BukuAlatLongitudinal;
use angulex\Model\BukuAlatPeer;
use angulex\Model\BukuAlatQuery;
use angulex\Model\JenisBukuAlat;
use angulex\Model\MataPelajaran;
use angulex\Model\Prasarana;
use angulex\Model\Sekolah;
use angulex\Model\TingkatPendidikan;

/**
 * Base class that represents a query for the 'buku_alat' table.
 *
 * 
 *
 * @method BukuAlatQuery orderByBukuAlatId($order = Criteria::ASC) Order by the buku_alat_id column
 * @method BukuAlatQuery orderByMataPelajaranId($order = Criteria::ASC) Order by the mata_pelajaran_id column
 * @method BukuAlatQuery orderByPrasaranaId($order = Criteria::ASC) Order by the prasarana_id column
 * @method BukuAlatQuery orderBySekolahId($order = Criteria::ASC) Order by the sekolah_id column
 * @method BukuAlatQuery orderByTingkatPendidikanId($order = Criteria::ASC) Order by the tingkat_pendidikan_id column
 * @method BukuAlatQuery orderByJenisBukuAlatId($order = Criteria::ASC) Order by the jenis_buku_alat_id column
 * @method BukuAlatQuery orderByBukuAlat($order = Criteria::ASC) Order by the buku_alat column
 * @method BukuAlatQuery orderByLastUpdate($order = Criteria::ASC) Order by the Last_update column
 * @method BukuAlatQuery orderBySoftDelete($order = Criteria::ASC) Order by the Soft_delete column
 * @method BukuAlatQuery orderByLastSync($order = Criteria::ASC) Order by the last_sync column
 * @method BukuAlatQuery orderByUpdaterId($order = Criteria::ASC) Order by the Updater_ID column
 *
 * @method BukuAlatQuery groupByBukuAlatId() Group by the buku_alat_id column
 * @method BukuAlatQuery groupByMataPelajaranId() Group by the mata_pelajaran_id column
 * @method BukuAlatQuery groupByPrasaranaId() Group by the prasarana_id column
 * @method BukuAlatQuery groupBySekolahId() Group by the sekolah_id column
 * @method BukuAlatQuery groupByTingkatPendidikanId() Group by the tingkat_pendidikan_id column
 * @method BukuAlatQuery groupByJenisBukuAlatId() Group by the jenis_buku_alat_id column
 * @method BukuAlatQuery groupByBukuAlat() Group by the buku_alat column
 * @method BukuAlatQuery groupByLastUpdate() Group by the Last_update column
 * @method BukuAlatQuery groupBySoftDelete() Group by the Soft_delete column
 * @method BukuAlatQuery groupByLastSync() Group by the last_sync column
 * @method BukuAlatQuery groupByUpdaterId() Group by the Updater_ID column
 *
 * @method BukuAlatQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method BukuAlatQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method BukuAlatQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method BukuAlatQuery leftJoinPrasaranaRelatedByPrasaranaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PrasaranaRelatedByPrasaranaId relation
 * @method BukuAlatQuery rightJoinPrasaranaRelatedByPrasaranaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PrasaranaRelatedByPrasaranaId relation
 * @method BukuAlatQuery innerJoinPrasaranaRelatedByPrasaranaId($relationAlias = null) Adds a INNER JOIN clause to the query using the PrasaranaRelatedByPrasaranaId relation
 *
 * @method BukuAlatQuery leftJoinPrasaranaRelatedByPrasaranaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PrasaranaRelatedByPrasaranaId relation
 * @method BukuAlatQuery rightJoinPrasaranaRelatedByPrasaranaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PrasaranaRelatedByPrasaranaId relation
 * @method BukuAlatQuery innerJoinPrasaranaRelatedByPrasaranaId($relationAlias = null) Adds a INNER JOIN clause to the query using the PrasaranaRelatedByPrasaranaId relation
 *
 * @method BukuAlatQuery leftJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method BukuAlatQuery rightJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method BukuAlatQuery innerJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the SekolahRelatedBySekolahId relation
 *
 * @method BukuAlatQuery leftJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method BukuAlatQuery rightJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method BukuAlatQuery innerJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the SekolahRelatedBySekolahId relation
 *
 * @method BukuAlatQuery leftJoinJenisBukuAlatRelatedByJenisBukuAlatId($relationAlias = null) Adds a LEFT JOIN clause to the query using the JenisBukuAlatRelatedByJenisBukuAlatId relation
 * @method BukuAlatQuery rightJoinJenisBukuAlatRelatedByJenisBukuAlatId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JenisBukuAlatRelatedByJenisBukuAlatId relation
 * @method BukuAlatQuery innerJoinJenisBukuAlatRelatedByJenisBukuAlatId($relationAlias = null) Adds a INNER JOIN clause to the query using the JenisBukuAlatRelatedByJenisBukuAlatId relation
 *
 * @method BukuAlatQuery leftJoinJenisBukuAlatRelatedByJenisBukuAlatId($relationAlias = null) Adds a LEFT JOIN clause to the query using the JenisBukuAlatRelatedByJenisBukuAlatId relation
 * @method BukuAlatQuery rightJoinJenisBukuAlatRelatedByJenisBukuAlatId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JenisBukuAlatRelatedByJenisBukuAlatId relation
 * @method BukuAlatQuery innerJoinJenisBukuAlatRelatedByJenisBukuAlatId($relationAlias = null) Adds a INNER JOIN clause to the query using the JenisBukuAlatRelatedByJenisBukuAlatId relation
 *
 * @method BukuAlatQuery leftJoinMataPelajaranRelatedByMataPelajaranId($relationAlias = null) Adds a LEFT JOIN clause to the query using the MataPelajaranRelatedByMataPelajaranId relation
 * @method BukuAlatQuery rightJoinMataPelajaranRelatedByMataPelajaranId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MataPelajaranRelatedByMataPelajaranId relation
 * @method BukuAlatQuery innerJoinMataPelajaranRelatedByMataPelajaranId($relationAlias = null) Adds a INNER JOIN clause to the query using the MataPelajaranRelatedByMataPelajaranId relation
 *
 * @method BukuAlatQuery leftJoinMataPelajaranRelatedByMataPelajaranId($relationAlias = null) Adds a LEFT JOIN clause to the query using the MataPelajaranRelatedByMataPelajaranId relation
 * @method BukuAlatQuery rightJoinMataPelajaranRelatedByMataPelajaranId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MataPelajaranRelatedByMataPelajaranId relation
 * @method BukuAlatQuery innerJoinMataPelajaranRelatedByMataPelajaranId($relationAlias = null) Adds a INNER JOIN clause to the query using the MataPelajaranRelatedByMataPelajaranId relation
 *
 * @method BukuAlatQuery leftJoinTingkatPendidikanRelatedByTingkatPendidikanId($relationAlias = null) Adds a LEFT JOIN clause to the query using the TingkatPendidikanRelatedByTingkatPendidikanId relation
 * @method BukuAlatQuery rightJoinTingkatPendidikanRelatedByTingkatPendidikanId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the TingkatPendidikanRelatedByTingkatPendidikanId relation
 * @method BukuAlatQuery innerJoinTingkatPendidikanRelatedByTingkatPendidikanId($relationAlias = null) Adds a INNER JOIN clause to the query using the TingkatPendidikanRelatedByTingkatPendidikanId relation
 *
 * @method BukuAlatQuery leftJoinTingkatPendidikanRelatedByTingkatPendidikanId($relationAlias = null) Adds a LEFT JOIN clause to the query using the TingkatPendidikanRelatedByTingkatPendidikanId relation
 * @method BukuAlatQuery rightJoinTingkatPendidikanRelatedByTingkatPendidikanId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the TingkatPendidikanRelatedByTingkatPendidikanId relation
 * @method BukuAlatQuery innerJoinTingkatPendidikanRelatedByTingkatPendidikanId($relationAlias = null) Adds a INNER JOIN clause to the query using the TingkatPendidikanRelatedByTingkatPendidikanId relation
 *
 * @method BukuAlatQuery leftJoinBukuAlatLongitudinalRelatedByBukuAlatId($relationAlias = null) Adds a LEFT JOIN clause to the query using the BukuAlatLongitudinalRelatedByBukuAlatId relation
 * @method BukuAlatQuery rightJoinBukuAlatLongitudinalRelatedByBukuAlatId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BukuAlatLongitudinalRelatedByBukuAlatId relation
 * @method BukuAlatQuery innerJoinBukuAlatLongitudinalRelatedByBukuAlatId($relationAlias = null) Adds a INNER JOIN clause to the query using the BukuAlatLongitudinalRelatedByBukuAlatId relation
 *
 * @method BukuAlatQuery leftJoinBukuAlatLongitudinalRelatedByBukuAlatId($relationAlias = null) Adds a LEFT JOIN clause to the query using the BukuAlatLongitudinalRelatedByBukuAlatId relation
 * @method BukuAlatQuery rightJoinBukuAlatLongitudinalRelatedByBukuAlatId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BukuAlatLongitudinalRelatedByBukuAlatId relation
 * @method BukuAlatQuery innerJoinBukuAlatLongitudinalRelatedByBukuAlatId($relationAlias = null) Adds a INNER JOIN clause to the query using the BukuAlatLongitudinalRelatedByBukuAlatId relation
 *
 * @method BukuAlat findOne(PropelPDO $con = null) Return the first BukuAlat matching the query
 * @method BukuAlat findOneOrCreate(PropelPDO $con = null) Return the first BukuAlat matching the query, or a new BukuAlat object populated from the query conditions when no match is found
 *
 * @method BukuAlat findOneByMataPelajaranId(int $mata_pelajaran_id) Return the first BukuAlat filtered by the mata_pelajaran_id column
 * @method BukuAlat findOneByPrasaranaId(string $prasarana_id) Return the first BukuAlat filtered by the prasarana_id column
 * @method BukuAlat findOneBySekolahId(string $sekolah_id) Return the first BukuAlat filtered by the sekolah_id column
 * @method BukuAlat findOneByTingkatPendidikanId(string $tingkat_pendidikan_id) Return the first BukuAlat filtered by the tingkat_pendidikan_id column
 * @method BukuAlat findOneByJenisBukuAlatId(string $jenis_buku_alat_id) Return the first BukuAlat filtered by the jenis_buku_alat_id column
 * @method BukuAlat findOneByBukuAlat(string $buku_alat) Return the first BukuAlat filtered by the buku_alat column
 * @method BukuAlat findOneByLastUpdate(string $Last_update) Return the first BukuAlat filtered by the Last_update column
 * @method BukuAlat findOneBySoftDelete(string $Soft_delete) Return the first BukuAlat filtered by the Soft_delete column
 * @method BukuAlat findOneByLastSync(string $last_sync) Return the first BukuAlat filtered by the last_sync column
 * @method BukuAlat findOneByUpdaterId(string $Updater_ID) Return the first BukuAlat filtered by the Updater_ID column
 *
 * @method array findByBukuAlatId(string $buku_alat_id) Return BukuAlat objects filtered by the buku_alat_id column
 * @method array findByMataPelajaranId(int $mata_pelajaran_id) Return BukuAlat objects filtered by the mata_pelajaran_id column
 * @method array findByPrasaranaId(string $prasarana_id) Return BukuAlat objects filtered by the prasarana_id column
 * @method array findBySekolahId(string $sekolah_id) Return BukuAlat objects filtered by the sekolah_id column
 * @method array findByTingkatPendidikanId(string $tingkat_pendidikan_id) Return BukuAlat objects filtered by the tingkat_pendidikan_id column
 * @method array findByJenisBukuAlatId(string $jenis_buku_alat_id) Return BukuAlat objects filtered by the jenis_buku_alat_id column
 * @method array findByBukuAlat(string $buku_alat) Return BukuAlat objects filtered by the buku_alat column
 * @method array findByLastUpdate(string $Last_update) Return BukuAlat objects filtered by the Last_update column
 * @method array findBySoftDelete(string $Soft_delete) Return BukuAlat objects filtered by the Soft_delete column
 * @method array findByLastSync(string $last_sync) Return BukuAlat objects filtered by the last_sync column
 * @method array findByUpdaterId(string $Updater_ID) Return BukuAlat objects filtered by the Updater_ID column
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseBukuAlatQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseBukuAlatQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Dapodikmen', $modelName = 'angulex\\Model\\BukuAlat', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new BukuAlatQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   BukuAlatQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return BukuAlatQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof BukuAlatQuery) {
            return $criteria;
        }
        $query = new BukuAlatQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   BukuAlat|BukuAlat[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = BukuAlatPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(BukuAlatPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 BukuAlat A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByBukuAlatId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 BukuAlat A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [buku_alat_id], [mata_pelajaran_id], [prasarana_id], [sekolah_id], [tingkat_pendidikan_id], [jenis_buku_alat_id], [buku_alat], [Last_update], [Soft_delete], [last_sync], [Updater_ID] FROM [buku_alat] WHERE [buku_alat_id] = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new BukuAlat();
            $obj->hydrate($row);
            BukuAlatPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return BukuAlat|BukuAlat[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|BukuAlat[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return BukuAlatQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(BukuAlatPeer::BUKU_ALAT_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return BukuAlatQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(BukuAlatPeer::BUKU_ALAT_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the buku_alat_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBukuAlatId('fooValue');   // WHERE buku_alat_id = 'fooValue'
     * $query->filterByBukuAlatId('%fooValue%'); // WHERE buku_alat_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $bukuAlatId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BukuAlatQuery The current query, for fluid interface
     */
    public function filterByBukuAlatId($bukuAlatId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($bukuAlatId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $bukuAlatId)) {
                $bukuAlatId = str_replace('*', '%', $bukuAlatId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BukuAlatPeer::BUKU_ALAT_ID, $bukuAlatId, $comparison);
    }

    /**
     * Filter the query on the mata_pelajaran_id column
     *
     * Example usage:
     * <code>
     * $query->filterByMataPelajaranId(1234); // WHERE mata_pelajaran_id = 1234
     * $query->filterByMataPelajaranId(array(12, 34)); // WHERE mata_pelajaran_id IN (12, 34)
     * $query->filterByMataPelajaranId(array('min' => 12)); // WHERE mata_pelajaran_id >= 12
     * $query->filterByMataPelajaranId(array('max' => 12)); // WHERE mata_pelajaran_id <= 12
     * </code>
     *
     * @see       filterByMataPelajaranRelatedByMataPelajaranId()
     *
     * @see       filterByMataPelajaranRelatedByMataPelajaranId()
     *
     * @param     mixed $mataPelajaranId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BukuAlatQuery The current query, for fluid interface
     */
    public function filterByMataPelajaranId($mataPelajaranId = null, $comparison = null)
    {
        if (is_array($mataPelajaranId)) {
            $useMinMax = false;
            if (isset($mataPelajaranId['min'])) {
                $this->addUsingAlias(BukuAlatPeer::MATA_PELAJARAN_ID, $mataPelajaranId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($mataPelajaranId['max'])) {
                $this->addUsingAlias(BukuAlatPeer::MATA_PELAJARAN_ID, $mataPelajaranId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BukuAlatPeer::MATA_PELAJARAN_ID, $mataPelajaranId, $comparison);
    }

    /**
     * Filter the query on the prasarana_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPrasaranaId('fooValue');   // WHERE prasarana_id = 'fooValue'
     * $query->filterByPrasaranaId('%fooValue%'); // WHERE prasarana_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $prasaranaId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BukuAlatQuery The current query, for fluid interface
     */
    public function filterByPrasaranaId($prasaranaId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($prasaranaId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $prasaranaId)) {
                $prasaranaId = str_replace('*', '%', $prasaranaId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BukuAlatPeer::PRASARANA_ID, $prasaranaId, $comparison);
    }

    /**
     * Filter the query on the sekolah_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySekolahId('fooValue');   // WHERE sekolah_id = 'fooValue'
     * $query->filterBySekolahId('%fooValue%'); // WHERE sekolah_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sekolahId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BukuAlatQuery The current query, for fluid interface
     */
    public function filterBySekolahId($sekolahId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sekolahId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $sekolahId)) {
                $sekolahId = str_replace('*', '%', $sekolahId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BukuAlatPeer::SEKOLAH_ID, $sekolahId, $comparison);
    }

    /**
     * Filter the query on the tingkat_pendidikan_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTingkatPendidikanId(1234); // WHERE tingkat_pendidikan_id = 1234
     * $query->filterByTingkatPendidikanId(array(12, 34)); // WHERE tingkat_pendidikan_id IN (12, 34)
     * $query->filterByTingkatPendidikanId(array('min' => 12)); // WHERE tingkat_pendidikan_id >= 12
     * $query->filterByTingkatPendidikanId(array('max' => 12)); // WHERE tingkat_pendidikan_id <= 12
     * </code>
     *
     * @see       filterByTingkatPendidikanRelatedByTingkatPendidikanId()
     *
     * @see       filterByTingkatPendidikanRelatedByTingkatPendidikanId()
     *
     * @param     mixed $tingkatPendidikanId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BukuAlatQuery The current query, for fluid interface
     */
    public function filterByTingkatPendidikanId($tingkatPendidikanId = null, $comparison = null)
    {
        if (is_array($tingkatPendidikanId)) {
            $useMinMax = false;
            if (isset($tingkatPendidikanId['min'])) {
                $this->addUsingAlias(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, $tingkatPendidikanId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tingkatPendidikanId['max'])) {
                $this->addUsingAlias(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, $tingkatPendidikanId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, $tingkatPendidikanId, $comparison);
    }

    /**
     * Filter the query on the jenis_buku_alat_id column
     *
     * Example usage:
     * <code>
     * $query->filterByJenisBukuAlatId(1234); // WHERE jenis_buku_alat_id = 1234
     * $query->filterByJenisBukuAlatId(array(12, 34)); // WHERE jenis_buku_alat_id IN (12, 34)
     * $query->filterByJenisBukuAlatId(array('min' => 12)); // WHERE jenis_buku_alat_id >= 12
     * $query->filterByJenisBukuAlatId(array('max' => 12)); // WHERE jenis_buku_alat_id <= 12
     * </code>
     *
     * @see       filterByJenisBukuAlatRelatedByJenisBukuAlatId()
     *
     * @see       filterByJenisBukuAlatRelatedByJenisBukuAlatId()
     *
     * @param     mixed $jenisBukuAlatId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BukuAlatQuery The current query, for fluid interface
     */
    public function filterByJenisBukuAlatId($jenisBukuAlatId = null, $comparison = null)
    {
        if (is_array($jenisBukuAlatId)) {
            $useMinMax = false;
            if (isset($jenisBukuAlatId['min'])) {
                $this->addUsingAlias(BukuAlatPeer::JENIS_BUKU_ALAT_ID, $jenisBukuAlatId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jenisBukuAlatId['max'])) {
                $this->addUsingAlias(BukuAlatPeer::JENIS_BUKU_ALAT_ID, $jenisBukuAlatId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BukuAlatPeer::JENIS_BUKU_ALAT_ID, $jenisBukuAlatId, $comparison);
    }

    /**
     * Filter the query on the buku_alat column
     *
     * Example usage:
     * <code>
     * $query->filterByBukuAlat('fooValue');   // WHERE buku_alat = 'fooValue'
     * $query->filterByBukuAlat('%fooValue%'); // WHERE buku_alat LIKE '%fooValue%'
     * </code>
     *
     * @param     string $bukuAlat The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BukuAlatQuery The current query, for fluid interface
     */
    public function filterByBukuAlat($bukuAlat = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($bukuAlat)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $bukuAlat)) {
                $bukuAlat = str_replace('*', '%', $bukuAlat);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BukuAlatPeer::BUKU_ALAT, $bukuAlat, $comparison);
    }

    /**
     * Filter the query on the Last_update column
     *
     * Example usage:
     * <code>
     * $query->filterByLastUpdate('2011-03-14'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate('now'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate(array('max' => 'yesterday')); // WHERE Last_update > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastUpdate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BukuAlatQuery The current query, for fluid interface
     */
    public function filterByLastUpdate($lastUpdate = null, $comparison = null)
    {
        if (is_array($lastUpdate)) {
            $useMinMax = false;
            if (isset($lastUpdate['min'])) {
                $this->addUsingAlias(BukuAlatPeer::LAST_UPDATE, $lastUpdate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastUpdate['max'])) {
                $this->addUsingAlias(BukuAlatPeer::LAST_UPDATE, $lastUpdate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BukuAlatPeer::LAST_UPDATE, $lastUpdate, $comparison);
    }

    /**
     * Filter the query on the Soft_delete column
     *
     * Example usage:
     * <code>
     * $query->filterBySoftDelete(1234); // WHERE Soft_delete = 1234
     * $query->filterBySoftDelete(array(12, 34)); // WHERE Soft_delete IN (12, 34)
     * $query->filterBySoftDelete(array('min' => 12)); // WHERE Soft_delete >= 12
     * $query->filterBySoftDelete(array('max' => 12)); // WHERE Soft_delete <= 12
     * </code>
     *
     * @param     mixed $softDelete The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BukuAlatQuery The current query, for fluid interface
     */
    public function filterBySoftDelete($softDelete = null, $comparison = null)
    {
        if (is_array($softDelete)) {
            $useMinMax = false;
            if (isset($softDelete['min'])) {
                $this->addUsingAlias(BukuAlatPeer::SOFT_DELETE, $softDelete['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($softDelete['max'])) {
                $this->addUsingAlias(BukuAlatPeer::SOFT_DELETE, $softDelete['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BukuAlatPeer::SOFT_DELETE, $softDelete, $comparison);
    }

    /**
     * Filter the query on the last_sync column
     *
     * Example usage:
     * <code>
     * $query->filterByLastSync('2011-03-14'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync('now'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync(array('max' => 'yesterday')); // WHERE last_sync > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastSync The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BukuAlatQuery The current query, for fluid interface
     */
    public function filterByLastSync($lastSync = null, $comparison = null)
    {
        if (is_array($lastSync)) {
            $useMinMax = false;
            if (isset($lastSync['min'])) {
                $this->addUsingAlias(BukuAlatPeer::LAST_SYNC, $lastSync['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastSync['max'])) {
                $this->addUsingAlias(BukuAlatPeer::LAST_SYNC, $lastSync['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BukuAlatPeer::LAST_SYNC, $lastSync, $comparison);
    }

    /**
     * Filter the query on the Updater_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdaterId('fooValue');   // WHERE Updater_ID = 'fooValue'
     * $query->filterByUpdaterId('%fooValue%'); // WHERE Updater_ID LIKE '%fooValue%'
     * </code>
     *
     * @param     string $updaterId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BukuAlatQuery The current query, for fluid interface
     */
    public function filterByUpdaterId($updaterId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($updaterId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $updaterId)) {
                $updaterId = str_replace('*', '%', $updaterId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BukuAlatPeer::UPDATER_ID, $updaterId, $comparison);
    }

    /**
     * Filter the query by a related Prasarana object
     *
     * @param   Prasarana|PropelObjectCollection $prasarana The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 BukuAlatQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPrasaranaRelatedByPrasaranaId($prasarana, $comparison = null)
    {
        if ($prasarana instanceof Prasarana) {
            return $this
                ->addUsingAlias(BukuAlatPeer::PRASARANA_ID, $prasarana->getPrasaranaId(), $comparison);
        } elseif ($prasarana instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BukuAlatPeer::PRASARANA_ID, $prasarana->toKeyValue('PrimaryKey', 'PrasaranaId'), $comparison);
        } else {
            throw new PropelException('filterByPrasaranaRelatedByPrasaranaId() only accepts arguments of type Prasarana or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PrasaranaRelatedByPrasaranaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return BukuAlatQuery The current query, for fluid interface
     */
    public function joinPrasaranaRelatedByPrasaranaId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PrasaranaRelatedByPrasaranaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PrasaranaRelatedByPrasaranaId');
        }

        return $this;
    }

    /**
     * Use the PrasaranaRelatedByPrasaranaId relation Prasarana object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PrasaranaQuery A secondary query class using the current class as primary query
     */
    public function usePrasaranaRelatedByPrasaranaIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPrasaranaRelatedByPrasaranaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PrasaranaRelatedByPrasaranaId', '\angulex\Model\PrasaranaQuery');
    }

    /**
     * Filter the query by a related Prasarana object
     *
     * @param   Prasarana|PropelObjectCollection $prasarana The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 BukuAlatQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPrasaranaRelatedByPrasaranaId($prasarana, $comparison = null)
    {
        if ($prasarana instanceof Prasarana) {
            return $this
                ->addUsingAlias(BukuAlatPeer::PRASARANA_ID, $prasarana->getPrasaranaId(), $comparison);
        } elseif ($prasarana instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BukuAlatPeer::PRASARANA_ID, $prasarana->toKeyValue('PrimaryKey', 'PrasaranaId'), $comparison);
        } else {
            throw new PropelException('filterByPrasaranaRelatedByPrasaranaId() only accepts arguments of type Prasarana or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PrasaranaRelatedByPrasaranaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return BukuAlatQuery The current query, for fluid interface
     */
    public function joinPrasaranaRelatedByPrasaranaId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PrasaranaRelatedByPrasaranaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PrasaranaRelatedByPrasaranaId');
        }

        return $this;
    }

    /**
     * Use the PrasaranaRelatedByPrasaranaId relation Prasarana object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PrasaranaQuery A secondary query class using the current class as primary query
     */
    public function usePrasaranaRelatedByPrasaranaIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPrasaranaRelatedByPrasaranaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PrasaranaRelatedByPrasaranaId', '\angulex\Model\PrasaranaQuery');
    }

    /**
     * Filter the query by a related Sekolah object
     *
     * @param   Sekolah|PropelObjectCollection $sekolah The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 BukuAlatQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySekolahRelatedBySekolahId($sekolah, $comparison = null)
    {
        if ($sekolah instanceof Sekolah) {
            return $this
                ->addUsingAlias(BukuAlatPeer::SEKOLAH_ID, $sekolah->getSekolahId(), $comparison);
        } elseif ($sekolah instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BukuAlatPeer::SEKOLAH_ID, $sekolah->toKeyValue('PrimaryKey', 'SekolahId'), $comparison);
        } else {
            throw new PropelException('filterBySekolahRelatedBySekolahId() only accepts arguments of type Sekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SekolahRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return BukuAlatQuery The current query, for fluid interface
     */
    public function joinSekolahRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SekolahRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SekolahRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the SekolahRelatedBySekolahId relation Sekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SekolahQuery A secondary query class using the current class as primary query
     */
    public function useSekolahRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSekolahRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SekolahRelatedBySekolahId', '\angulex\Model\SekolahQuery');
    }

    /**
     * Filter the query by a related Sekolah object
     *
     * @param   Sekolah|PropelObjectCollection $sekolah The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 BukuAlatQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySekolahRelatedBySekolahId($sekolah, $comparison = null)
    {
        if ($sekolah instanceof Sekolah) {
            return $this
                ->addUsingAlias(BukuAlatPeer::SEKOLAH_ID, $sekolah->getSekolahId(), $comparison);
        } elseif ($sekolah instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BukuAlatPeer::SEKOLAH_ID, $sekolah->toKeyValue('PrimaryKey', 'SekolahId'), $comparison);
        } else {
            throw new PropelException('filterBySekolahRelatedBySekolahId() only accepts arguments of type Sekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SekolahRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return BukuAlatQuery The current query, for fluid interface
     */
    public function joinSekolahRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SekolahRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SekolahRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the SekolahRelatedBySekolahId relation Sekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SekolahQuery A secondary query class using the current class as primary query
     */
    public function useSekolahRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSekolahRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SekolahRelatedBySekolahId', '\angulex\Model\SekolahQuery');
    }

    /**
     * Filter the query by a related JenisBukuAlat object
     *
     * @param   JenisBukuAlat|PropelObjectCollection $jenisBukuAlat The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 BukuAlatQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJenisBukuAlatRelatedByJenisBukuAlatId($jenisBukuAlat, $comparison = null)
    {
        if ($jenisBukuAlat instanceof JenisBukuAlat) {
            return $this
                ->addUsingAlias(BukuAlatPeer::JENIS_BUKU_ALAT_ID, $jenisBukuAlat->getJenisBukuAlatId(), $comparison);
        } elseif ($jenisBukuAlat instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BukuAlatPeer::JENIS_BUKU_ALAT_ID, $jenisBukuAlat->toKeyValue('PrimaryKey', 'JenisBukuAlatId'), $comparison);
        } else {
            throw new PropelException('filterByJenisBukuAlatRelatedByJenisBukuAlatId() only accepts arguments of type JenisBukuAlat or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the JenisBukuAlatRelatedByJenisBukuAlatId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return BukuAlatQuery The current query, for fluid interface
     */
    public function joinJenisBukuAlatRelatedByJenisBukuAlatId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('JenisBukuAlatRelatedByJenisBukuAlatId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'JenisBukuAlatRelatedByJenisBukuAlatId');
        }

        return $this;
    }

    /**
     * Use the JenisBukuAlatRelatedByJenisBukuAlatId relation JenisBukuAlat object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\JenisBukuAlatQuery A secondary query class using the current class as primary query
     */
    public function useJenisBukuAlatRelatedByJenisBukuAlatIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinJenisBukuAlatRelatedByJenisBukuAlatId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'JenisBukuAlatRelatedByJenisBukuAlatId', '\angulex\Model\JenisBukuAlatQuery');
    }

    /**
     * Filter the query by a related JenisBukuAlat object
     *
     * @param   JenisBukuAlat|PropelObjectCollection $jenisBukuAlat The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 BukuAlatQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJenisBukuAlatRelatedByJenisBukuAlatId($jenisBukuAlat, $comparison = null)
    {
        if ($jenisBukuAlat instanceof JenisBukuAlat) {
            return $this
                ->addUsingAlias(BukuAlatPeer::JENIS_BUKU_ALAT_ID, $jenisBukuAlat->getJenisBukuAlatId(), $comparison);
        } elseif ($jenisBukuAlat instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BukuAlatPeer::JENIS_BUKU_ALAT_ID, $jenisBukuAlat->toKeyValue('PrimaryKey', 'JenisBukuAlatId'), $comparison);
        } else {
            throw new PropelException('filterByJenisBukuAlatRelatedByJenisBukuAlatId() only accepts arguments of type JenisBukuAlat or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the JenisBukuAlatRelatedByJenisBukuAlatId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return BukuAlatQuery The current query, for fluid interface
     */
    public function joinJenisBukuAlatRelatedByJenisBukuAlatId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('JenisBukuAlatRelatedByJenisBukuAlatId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'JenisBukuAlatRelatedByJenisBukuAlatId');
        }

        return $this;
    }

    /**
     * Use the JenisBukuAlatRelatedByJenisBukuAlatId relation JenisBukuAlat object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\JenisBukuAlatQuery A secondary query class using the current class as primary query
     */
    public function useJenisBukuAlatRelatedByJenisBukuAlatIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinJenisBukuAlatRelatedByJenisBukuAlatId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'JenisBukuAlatRelatedByJenisBukuAlatId', '\angulex\Model\JenisBukuAlatQuery');
    }

    /**
     * Filter the query by a related MataPelajaran object
     *
     * @param   MataPelajaran|PropelObjectCollection $mataPelajaran The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 BukuAlatQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByMataPelajaranRelatedByMataPelajaranId($mataPelajaran, $comparison = null)
    {
        if ($mataPelajaran instanceof MataPelajaran) {
            return $this
                ->addUsingAlias(BukuAlatPeer::MATA_PELAJARAN_ID, $mataPelajaran->getMataPelajaranId(), $comparison);
        } elseif ($mataPelajaran instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BukuAlatPeer::MATA_PELAJARAN_ID, $mataPelajaran->toKeyValue('PrimaryKey', 'MataPelajaranId'), $comparison);
        } else {
            throw new PropelException('filterByMataPelajaranRelatedByMataPelajaranId() only accepts arguments of type MataPelajaran or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MataPelajaranRelatedByMataPelajaranId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return BukuAlatQuery The current query, for fluid interface
     */
    public function joinMataPelajaranRelatedByMataPelajaranId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MataPelajaranRelatedByMataPelajaranId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MataPelajaranRelatedByMataPelajaranId');
        }

        return $this;
    }

    /**
     * Use the MataPelajaranRelatedByMataPelajaranId relation MataPelajaran object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\MataPelajaranQuery A secondary query class using the current class as primary query
     */
    public function useMataPelajaranRelatedByMataPelajaranIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMataPelajaranRelatedByMataPelajaranId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MataPelajaranRelatedByMataPelajaranId', '\angulex\Model\MataPelajaranQuery');
    }

    /**
     * Filter the query by a related MataPelajaran object
     *
     * @param   MataPelajaran|PropelObjectCollection $mataPelajaran The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 BukuAlatQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByMataPelajaranRelatedByMataPelajaranId($mataPelajaran, $comparison = null)
    {
        if ($mataPelajaran instanceof MataPelajaran) {
            return $this
                ->addUsingAlias(BukuAlatPeer::MATA_PELAJARAN_ID, $mataPelajaran->getMataPelajaranId(), $comparison);
        } elseif ($mataPelajaran instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BukuAlatPeer::MATA_PELAJARAN_ID, $mataPelajaran->toKeyValue('PrimaryKey', 'MataPelajaranId'), $comparison);
        } else {
            throw new PropelException('filterByMataPelajaranRelatedByMataPelajaranId() only accepts arguments of type MataPelajaran or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MataPelajaranRelatedByMataPelajaranId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return BukuAlatQuery The current query, for fluid interface
     */
    public function joinMataPelajaranRelatedByMataPelajaranId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MataPelajaranRelatedByMataPelajaranId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MataPelajaranRelatedByMataPelajaranId');
        }

        return $this;
    }

    /**
     * Use the MataPelajaranRelatedByMataPelajaranId relation MataPelajaran object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\MataPelajaranQuery A secondary query class using the current class as primary query
     */
    public function useMataPelajaranRelatedByMataPelajaranIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMataPelajaranRelatedByMataPelajaranId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MataPelajaranRelatedByMataPelajaranId', '\angulex\Model\MataPelajaranQuery');
    }

    /**
     * Filter the query by a related TingkatPendidikan object
     *
     * @param   TingkatPendidikan|PropelObjectCollection $tingkatPendidikan The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 BukuAlatQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByTingkatPendidikanRelatedByTingkatPendidikanId($tingkatPendidikan, $comparison = null)
    {
        if ($tingkatPendidikan instanceof TingkatPendidikan) {
            return $this
                ->addUsingAlias(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, $tingkatPendidikan->getTingkatPendidikanId(), $comparison);
        } elseif ($tingkatPendidikan instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, $tingkatPendidikan->toKeyValue('PrimaryKey', 'TingkatPendidikanId'), $comparison);
        } else {
            throw new PropelException('filterByTingkatPendidikanRelatedByTingkatPendidikanId() only accepts arguments of type TingkatPendidikan or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the TingkatPendidikanRelatedByTingkatPendidikanId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return BukuAlatQuery The current query, for fluid interface
     */
    public function joinTingkatPendidikanRelatedByTingkatPendidikanId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('TingkatPendidikanRelatedByTingkatPendidikanId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'TingkatPendidikanRelatedByTingkatPendidikanId');
        }

        return $this;
    }

    /**
     * Use the TingkatPendidikanRelatedByTingkatPendidikanId relation TingkatPendidikan object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\TingkatPendidikanQuery A secondary query class using the current class as primary query
     */
    public function useTingkatPendidikanRelatedByTingkatPendidikanIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinTingkatPendidikanRelatedByTingkatPendidikanId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'TingkatPendidikanRelatedByTingkatPendidikanId', '\angulex\Model\TingkatPendidikanQuery');
    }

    /**
     * Filter the query by a related TingkatPendidikan object
     *
     * @param   TingkatPendidikan|PropelObjectCollection $tingkatPendidikan The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 BukuAlatQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByTingkatPendidikanRelatedByTingkatPendidikanId($tingkatPendidikan, $comparison = null)
    {
        if ($tingkatPendidikan instanceof TingkatPendidikan) {
            return $this
                ->addUsingAlias(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, $tingkatPendidikan->getTingkatPendidikanId(), $comparison);
        } elseif ($tingkatPendidikan instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, $tingkatPendidikan->toKeyValue('PrimaryKey', 'TingkatPendidikanId'), $comparison);
        } else {
            throw new PropelException('filterByTingkatPendidikanRelatedByTingkatPendidikanId() only accepts arguments of type TingkatPendidikan or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the TingkatPendidikanRelatedByTingkatPendidikanId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return BukuAlatQuery The current query, for fluid interface
     */
    public function joinTingkatPendidikanRelatedByTingkatPendidikanId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('TingkatPendidikanRelatedByTingkatPendidikanId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'TingkatPendidikanRelatedByTingkatPendidikanId');
        }

        return $this;
    }

    /**
     * Use the TingkatPendidikanRelatedByTingkatPendidikanId relation TingkatPendidikan object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\TingkatPendidikanQuery A secondary query class using the current class as primary query
     */
    public function useTingkatPendidikanRelatedByTingkatPendidikanIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinTingkatPendidikanRelatedByTingkatPendidikanId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'TingkatPendidikanRelatedByTingkatPendidikanId', '\angulex\Model\TingkatPendidikanQuery');
    }

    /**
     * Filter the query by a related BukuAlatLongitudinal object
     *
     * @param   BukuAlatLongitudinal|PropelObjectCollection $bukuAlatLongitudinal  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 BukuAlatQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByBukuAlatLongitudinalRelatedByBukuAlatId($bukuAlatLongitudinal, $comparison = null)
    {
        if ($bukuAlatLongitudinal instanceof BukuAlatLongitudinal) {
            return $this
                ->addUsingAlias(BukuAlatPeer::BUKU_ALAT_ID, $bukuAlatLongitudinal->getBukuAlatId(), $comparison);
        } elseif ($bukuAlatLongitudinal instanceof PropelObjectCollection) {
            return $this
                ->useBukuAlatLongitudinalRelatedByBukuAlatIdQuery()
                ->filterByPrimaryKeys($bukuAlatLongitudinal->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBukuAlatLongitudinalRelatedByBukuAlatId() only accepts arguments of type BukuAlatLongitudinal or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BukuAlatLongitudinalRelatedByBukuAlatId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return BukuAlatQuery The current query, for fluid interface
     */
    public function joinBukuAlatLongitudinalRelatedByBukuAlatId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BukuAlatLongitudinalRelatedByBukuAlatId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BukuAlatLongitudinalRelatedByBukuAlatId');
        }

        return $this;
    }

    /**
     * Use the BukuAlatLongitudinalRelatedByBukuAlatId relation BukuAlatLongitudinal object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\BukuAlatLongitudinalQuery A secondary query class using the current class as primary query
     */
    public function useBukuAlatLongitudinalRelatedByBukuAlatIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBukuAlatLongitudinalRelatedByBukuAlatId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BukuAlatLongitudinalRelatedByBukuAlatId', '\angulex\Model\BukuAlatLongitudinalQuery');
    }

    /**
     * Filter the query by a related BukuAlatLongitudinal object
     *
     * @param   BukuAlatLongitudinal|PropelObjectCollection $bukuAlatLongitudinal  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 BukuAlatQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByBukuAlatLongitudinalRelatedByBukuAlatId($bukuAlatLongitudinal, $comparison = null)
    {
        if ($bukuAlatLongitudinal instanceof BukuAlatLongitudinal) {
            return $this
                ->addUsingAlias(BukuAlatPeer::BUKU_ALAT_ID, $bukuAlatLongitudinal->getBukuAlatId(), $comparison);
        } elseif ($bukuAlatLongitudinal instanceof PropelObjectCollection) {
            return $this
                ->useBukuAlatLongitudinalRelatedByBukuAlatIdQuery()
                ->filterByPrimaryKeys($bukuAlatLongitudinal->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBukuAlatLongitudinalRelatedByBukuAlatId() only accepts arguments of type BukuAlatLongitudinal or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BukuAlatLongitudinalRelatedByBukuAlatId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return BukuAlatQuery The current query, for fluid interface
     */
    public function joinBukuAlatLongitudinalRelatedByBukuAlatId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BukuAlatLongitudinalRelatedByBukuAlatId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BukuAlatLongitudinalRelatedByBukuAlatId');
        }

        return $this;
    }

    /**
     * Use the BukuAlatLongitudinalRelatedByBukuAlatId relation BukuAlatLongitudinal object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\BukuAlatLongitudinalQuery A secondary query class using the current class as primary query
     */
    public function useBukuAlatLongitudinalRelatedByBukuAlatIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBukuAlatLongitudinalRelatedByBukuAlatId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BukuAlatLongitudinalRelatedByBukuAlatId', '\angulex\Model\BukuAlatLongitudinalQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   BukuAlat $bukuAlat Object to remove from the list of results
     *
     * @return BukuAlatQuery The current query, for fluid interface
     */
    public function prune($bukuAlat = null)
    {
        if ($bukuAlat) {
            $this->addUsingAlias(BukuAlatPeer::BUKU_ALAT_ID, $bukuAlat->getBukuAlatId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
