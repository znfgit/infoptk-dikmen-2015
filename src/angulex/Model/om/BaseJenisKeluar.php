<?php

namespace angulex\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelDateTime;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\JenisKeluar;
use angulex\Model\JenisKeluarPeer;
use angulex\Model\JenisKeluarQuery;
use angulex\Model\PengawasTerdaftar;
use angulex\Model\PengawasTerdaftarQuery;
use angulex\Model\PtkTerdaftar;
use angulex\Model\PtkTerdaftarQuery;
use angulex\Model\RegistrasiPesertaDidik;
use angulex\Model\RegistrasiPesertaDidikQuery;

/**
 * Base class that represents a row from the 'ref.jenis_keluar' table.
 *
 * 
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseJenisKeluar extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'angulex\\Model\\JenisKeluarPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        JenisKeluarPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the jenis_keluar_id field.
     * @var        string
     */
    protected $jenis_keluar_id;

    /**
     * The value for the ket_keluar field.
     * @var        string
     */
    protected $ket_keluar;

    /**
     * The value for the keluar_pd field.
     * @var        string
     */
    protected $keluar_pd;

    /**
     * The value for the keluar_ptk field.
     * @var        string
     */
    protected $keluar_ptk;

    /**
     * The value for the create_date field.
     * @var        string
     */
    protected $create_date;

    /**
     * The value for the last_update field.
     * @var        string
     */
    protected $last_update;

    /**
     * The value for the expired_date field.
     * @var        string
     */
    protected $expired_date;

    /**
     * The value for the last_sync field.
     * @var        string
     */
    protected $last_sync;

    /**
     * @var        PropelObjectCollection|RegistrasiPesertaDidik[] Collection to store aggregation of RegistrasiPesertaDidik objects.
     */
    protected $collRegistrasiPesertaDidiksRelatedByJenisKeluarId;
    protected $collRegistrasiPesertaDidiksRelatedByJenisKeluarIdPartial;

    /**
     * @var        PropelObjectCollection|RegistrasiPesertaDidik[] Collection to store aggregation of RegistrasiPesertaDidik objects.
     */
    protected $collRegistrasiPesertaDidiksRelatedByJenisKeluarId;
    protected $collRegistrasiPesertaDidiksRelatedByJenisKeluarIdPartial;

    /**
     * @var        PropelObjectCollection|PtkTerdaftar[] Collection to store aggregation of PtkTerdaftar objects.
     */
    protected $collPtkTerdaftarsRelatedByJenisKeluarId;
    protected $collPtkTerdaftarsRelatedByJenisKeluarIdPartial;

    /**
     * @var        PropelObjectCollection|PtkTerdaftar[] Collection to store aggregation of PtkTerdaftar objects.
     */
    protected $collPtkTerdaftarsRelatedByJenisKeluarId;
    protected $collPtkTerdaftarsRelatedByJenisKeluarIdPartial;

    /**
     * @var        PropelObjectCollection|PengawasTerdaftar[] Collection to store aggregation of PengawasTerdaftar objects.
     */
    protected $collPengawasTerdaftarsRelatedByJenisKeluarId;
    protected $collPengawasTerdaftarsRelatedByJenisKeluarIdPartial;

    /**
     * @var        PropelObjectCollection|PengawasTerdaftar[] Collection to store aggregation of PengawasTerdaftar objects.
     */
    protected $collPengawasTerdaftarsRelatedByJenisKeluarId;
    protected $collPengawasTerdaftarsRelatedByJenisKeluarIdPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $registrasiPesertaDidiksRelatedByJenisKeluarIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $registrasiPesertaDidiksRelatedByJenisKeluarIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $ptkTerdaftarsRelatedByJenisKeluarIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $ptkTerdaftarsRelatedByJenisKeluarIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $pengawasTerdaftarsRelatedByJenisKeluarIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $pengawasTerdaftarsRelatedByJenisKeluarIdScheduledForDeletion = null;

    /**
     * Get the [jenis_keluar_id] column value.
     * 
     * @return string
     */
    public function getJenisKeluarId()
    {
        return $this->jenis_keluar_id;
    }

    /**
     * Get the [ket_keluar] column value.
     * 
     * @return string
     */
    public function getKetKeluar()
    {
        return $this->ket_keluar;
    }

    /**
     * Get the [keluar_pd] column value.
     * 
     * @return string
     */
    public function getKeluarPd()
    {
        return $this->keluar_pd;
    }

    /**
     * Get the [keluar_ptk] column value.
     * 
     * @return string
     */
    public function getKeluarPtk()
    {
        return $this->keluar_ptk;
    }

    /**
     * Get the [optionally formatted] temporal [create_date] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreateDate($format = 'Y-m-d H:i:s')
    {
        if ($this->create_date === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->create_date);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->create_date, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [last_update] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastUpdate($format = 'Y-m-d H:i:s')
    {
        if ($this->last_update === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_update);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_update, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [expired_date] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getExpiredDate($format = 'Y-m-d H:i:s')
    {
        if ($this->expired_date === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->expired_date);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->expired_date, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [last_sync] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastSync($format = 'Y-m-d H:i:s')
    {
        if ($this->last_sync === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_sync);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_sync, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Set the value of [jenis_keluar_id] column.
     * 
     * @param string $v new value
     * @return JenisKeluar The current object (for fluent API support)
     */
    public function setJenisKeluarId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->jenis_keluar_id !== $v) {
            $this->jenis_keluar_id = $v;
            $this->modifiedColumns[] = JenisKeluarPeer::JENIS_KELUAR_ID;
        }


        return $this;
    } // setJenisKeluarId()

    /**
     * Set the value of [ket_keluar] column.
     * 
     * @param string $v new value
     * @return JenisKeluar The current object (for fluent API support)
     */
    public function setKetKeluar($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->ket_keluar !== $v) {
            $this->ket_keluar = $v;
            $this->modifiedColumns[] = JenisKeluarPeer::KET_KELUAR;
        }


        return $this;
    } // setKetKeluar()

    /**
     * Set the value of [keluar_pd] column.
     * 
     * @param string $v new value
     * @return JenisKeluar The current object (for fluent API support)
     */
    public function setKeluarPd($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->keluar_pd !== $v) {
            $this->keluar_pd = $v;
            $this->modifiedColumns[] = JenisKeluarPeer::KELUAR_PD;
        }


        return $this;
    } // setKeluarPd()

    /**
     * Set the value of [keluar_ptk] column.
     * 
     * @param string $v new value
     * @return JenisKeluar The current object (for fluent API support)
     */
    public function setKeluarPtk($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->keluar_ptk !== $v) {
            $this->keluar_ptk = $v;
            $this->modifiedColumns[] = JenisKeluarPeer::KELUAR_PTK;
        }


        return $this;
    } // setKeluarPtk()

    /**
     * Sets the value of [create_date] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return JenisKeluar The current object (for fluent API support)
     */
    public function setCreateDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->create_date !== null || $dt !== null) {
            $currentDateAsString = ($this->create_date !== null && $tmpDt = new DateTime($this->create_date)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->create_date = $newDateAsString;
                $this->modifiedColumns[] = JenisKeluarPeer::CREATE_DATE;
            }
        } // if either are not null


        return $this;
    } // setCreateDate()

    /**
     * Sets the value of [last_update] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return JenisKeluar The current object (for fluent API support)
     */
    public function setLastUpdate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_update !== null || $dt !== null) {
            $currentDateAsString = ($this->last_update !== null && $tmpDt = new DateTime($this->last_update)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_update = $newDateAsString;
                $this->modifiedColumns[] = JenisKeluarPeer::LAST_UPDATE;
            }
        } // if either are not null


        return $this;
    } // setLastUpdate()

    /**
     * Sets the value of [expired_date] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return JenisKeluar The current object (for fluent API support)
     */
    public function setExpiredDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->expired_date !== null || $dt !== null) {
            $currentDateAsString = ($this->expired_date !== null && $tmpDt = new DateTime($this->expired_date)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->expired_date = $newDateAsString;
                $this->modifiedColumns[] = JenisKeluarPeer::EXPIRED_DATE;
            }
        } // if either are not null


        return $this;
    } // setExpiredDate()

    /**
     * Sets the value of [last_sync] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return JenisKeluar The current object (for fluent API support)
     */
    public function setLastSync($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_sync !== null || $dt !== null) {
            $currentDateAsString = ($this->last_sync !== null && $tmpDt = new DateTime($this->last_sync)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_sync = $newDateAsString;
                $this->modifiedColumns[] = JenisKeluarPeer::LAST_SYNC;
            }
        } // if either are not null


        return $this;
    } // setLastSync()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->jenis_keluar_id = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
            $this->ket_keluar = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->keluar_pd = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->keluar_ptk = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->create_date = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->last_update = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->expired_date = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->last_sync = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 8; // 8 = JenisKeluarPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating JenisKeluar object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(JenisKeluarPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = JenisKeluarPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId = null;

            $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId = null;

            $this->collPtkTerdaftarsRelatedByJenisKeluarId = null;

            $this->collPtkTerdaftarsRelatedByJenisKeluarId = null;

            $this->collPengawasTerdaftarsRelatedByJenisKeluarId = null;

            $this->collPengawasTerdaftarsRelatedByJenisKeluarId = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(JenisKeluarPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = JenisKeluarQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(JenisKeluarPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                JenisKeluarPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->registrasiPesertaDidiksRelatedByJenisKeluarIdScheduledForDeletion !== null) {
                if (!$this->registrasiPesertaDidiksRelatedByJenisKeluarIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->registrasiPesertaDidiksRelatedByJenisKeluarIdScheduledForDeletion as $registrasiPesertaDidikRelatedByJenisKeluarId) {
                        // need to save related object because we set the relation to null
                        $registrasiPesertaDidikRelatedByJenisKeluarId->save($con);
                    }
                    $this->registrasiPesertaDidiksRelatedByJenisKeluarIdScheduledForDeletion = null;
                }
            }

            if ($this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId !== null) {
                foreach ($this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->registrasiPesertaDidiksRelatedByJenisKeluarIdScheduledForDeletion !== null) {
                if (!$this->registrasiPesertaDidiksRelatedByJenisKeluarIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->registrasiPesertaDidiksRelatedByJenisKeluarIdScheduledForDeletion as $registrasiPesertaDidikRelatedByJenisKeluarId) {
                        // need to save related object because we set the relation to null
                        $registrasiPesertaDidikRelatedByJenisKeluarId->save($con);
                    }
                    $this->registrasiPesertaDidiksRelatedByJenisKeluarIdScheduledForDeletion = null;
                }
            }

            if ($this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId !== null) {
                foreach ($this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->ptkTerdaftarsRelatedByJenisKeluarIdScheduledForDeletion !== null) {
                if (!$this->ptkTerdaftarsRelatedByJenisKeluarIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->ptkTerdaftarsRelatedByJenisKeluarIdScheduledForDeletion as $ptkTerdaftarRelatedByJenisKeluarId) {
                        // need to save related object because we set the relation to null
                        $ptkTerdaftarRelatedByJenisKeluarId->save($con);
                    }
                    $this->ptkTerdaftarsRelatedByJenisKeluarIdScheduledForDeletion = null;
                }
            }

            if ($this->collPtkTerdaftarsRelatedByJenisKeluarId !== null) {
                foreach ($this->collPtkTerdaftarsRelatedByJenisKeluarId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->ptkTerdaftarsRelatedByJenisKeluarIdScheduledForDeletion !== null) {
                if (!$this->ptkTerdaftarsRelatedByJenisKeluarIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->ptkTerdaftarsRelatedByJenisKeluarIdScheduledForDeletion as $ptkTerdaftarRelatedByJenisKeluarId) {
                        // need to save related object because we set the relation to null
                        $ptkTerdaftarRelatedByJenisKeluarId->save($con);
                    }
                    $this->ptkTerdaftarsRelatedByJenisKeluarIdScheduledForDeletion = null;
                }
            }

            if ($this->collPtkTerdaftarsRelatedByJenisKeluarId !== null) {
                foreach ($this->collPtkTerdaftarsRelatedByJenisKeluarId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->pengawasTerdaftarsRelatedByJenisKeluarIdScheduledForDeletion !== null) {
                if (!$this->pengawasTerdaftarsRelatedByJenisKeluarIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->pengawasTerdaftarsRelatedByJenisKeluarIdScheduledForDeletion as $pengawasTerdaftarRelatedByJenisKeluarId) {
                        // need to save related object because we set the relation to null
                        $pengawasTerdaftarRelatedByJenisKeluarId->save($con);
                    }
                    $this->pengawasTerdaftarsRelatedByJenisKeluarIdScheduledForDeletion = null;
                }
            }

            if ($this->collPengawasTerdaftarsRelatedByJenisKeluarId !== null) {
                foreach ($this->collPengawasTerdaftarsRelatedByJenisKeluarId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->pengawasTerdaftarsRelatedByJenisKeluarIdScheduledForDeletion !== null) {
                if (!$this->pengawasTerdaftarsRelatedByJenisKeluarIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->pengawasTerdaftarsRelatedByJenisKeluarIdScheduledForDeletion as $pengawasTerdaftarRelatedByJenisKeluarId) {
                        // need to save related object because we set the relation to null
                        $pengawasTerdaftarRelatedByJenisKeluarId->save($con);
                    }
                    $this->pengawasTerdaftarsRelatedByJenisKeluarIdScheduledForDeletion = null;
                }
            }

            if ($this->collPengawasTerdaftarsRelatedByJenisKeluarId !== null) {
                foreach ($this->collPengawasTerdaftarsRelatedByJenisKeluarId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $criteria = $this->buildCriteria();
        $pk = BasePeer::doInsert($criteria, $con);
        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = JenisKeluarPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId !== null) {
                    foreach ($this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId !== null) {
                    foreach ($this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPtkTerdaftarsRelatedByJenisKeluarId !== null) {
                    foreach ($this->collPtkTerdaftarsRelatedByJenisKeluarId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPtkTerdaftarsRelatedByJenisKeluarId !== null) {
                    foreach ($this->collPtkTerdaftarsRelatedByJenisKeluarId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPengawasTerdaftarsRelatedByJenisKeluarId !== null) {
                    foreach ($this->collPengawasTerdaftarsRelatedByJenisKeluarId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPengawasTerdaftarsRelatedByJenisKeluarId !== null) {
                    foreach ($this->collPengawasTerdaftarsRelatedByJenisKeluarId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = JenisKeluarPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getJenisKeluarId();
                break;
            case 1:
                return $this->getKetKeluar();
                break;
            case 2:
                return $this->getKeluarPd();
                break;
            case 3:
                return $this->getKeluarPtk();
                break;
            case 4:
                return $this->getCreateDate();
                break;
            case 5:
                return $this->getLastUpdate();
                break;
            case 6:
                return $this->getExpiredDate();
                break;
            case 7:
                return $this->getLastSync();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['JenisKeluar'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['JenisKeluar'][$this->getPrimaryKey()] = true;
        $keys = JenisKeluarPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getJenisKeluarId(),
            $keys[1] => $this->getKetKeluar(),
            $keys[2] => $this->getKeluarPd(),
            $keys[3] => $this->getKeluarPtk(),
            $keys[4] => $this->getCreateDate(),
            $keys[5] => $this->getLastUpdate(),
            $keys[6] => $this->getExpiredDate(),
            $keys[7] => $this->getLastSync(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId) {
                $result['RegistrasiPesertaDidiksRelatedByJenisKeluarId'] = $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId) {
                $result['RegistrasiPesertaDidiksRelatedByJenisKeluarId'] = $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPtkTerdaftarsRelatedByJenisKeluarId) {
                $result['PtkTerdaftarsRelatedByJenisKeluarId'] = $this->collPtkTerdaftarsRelatedByJenisKeluarId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPtkTerdaftarsRelatedByJenisKeluarId) {
                $result['PtkTerdaftarsRelatedByJenisKeluarId'] = $this->collPtkTerdaftarsRelatedByJenisKeluarId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPengawasTerdaftarsRelatedByJenisKeluarId) {
                $result['PengawasTerdaftarsRelatedByJenisKeluarId'] = $this->collPengawasTerdaftarsRelatedByJenisKeluarId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPengawasTerdaftarsRelatedByJenisKeluarId) {
                $result['PengawasTerdaftarsRelatedByJenisKeluarId'] = $this->collPengawasTerdaftarsRelatedByJenisKeluarId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = JenisKeluarPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setJenisKeluarId($value);
                break;
            case 1:
                $this->setKetKeluar($value);
                break;
            case 2:
                $this->setKeluarPd($value);
                break;
            case 3:
                $this->setKeluarPtk($value);
                break;
            case 4:
                $this->setCreateDate($value);
                break;
            case 5:
                $this->setLastUpdate($value);
                break;
            case 6:
                $this->setExpiredDate($value);
                break;
            case 7:
                $this->setLastSync($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = JenisKeluarPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setJenisKeluarId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setKetKeluar($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setKeluarPd($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setKeluarPtk($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setCreateDate($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setLastUpdate($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setExpiredDate($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setLastSync($arr[$keys[7]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(JenisKeluarPeer::DATABASE_NAME);

        if ($this->isColumnModified(JenisKeluarPeer::JENIS_KELUAR_ID)) $criteria->add(JenisKeluarPeer::JENIS_KELUAR_ID, $this->jenis_keluar_id);
        if ($this->isColumnModified(JenisKeluarPeer::KET_KELUAR)) $criteria->add(JenisKeluarPeer::KET_KELUAR, $this->ket_keluar);
        if ($this->isColumnModified(JenisKeluarPeer::KELUAR_PD)) $criteria->add(JenisKeluarPeer::KELUAR_PD, $this->keluar_pd);
        if ($this->isColumnModified(JenisKeluarPeer::KELUAR_PTK)) $criteria->add(JenisKeluarPeer::KELUAR_PTK, $this->keluar_ptk);
        if ($this->isColumnModified(JenisKeluarPeer::CREATE_DATE)) $criteria->add(JenisKeluarPeer::CREATE_DATE, $this->create_date);
        if ($this->isColumnModified(JenisKeluarPeer::LAST_UPDATE)) $criteria->add(JenisKeluarPeer::LAST_UPDATE, $this->last_update);
        if ($this->isColumnModified(JenisKeluarPeer::EXPIRED_DATE)) $criteria->add(JenisKeluarPeer::EXPIRED_DATE, $this->expired_date);
        if ($this->isColumnModified(JenisKeluarPeer::LAST_SYNC)) $criteria->add(JenisKeluarPeer::LAST_SYNC, $this->last_sync);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(JenisKeluarPeer::DATABASE_NAME);
        $criteria->add(JenisKeluarPeer::JENIS_KELUAR_ID, $this->jenis_keluar_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getJenisKeluarId();
    }

    /**
     * Generic method to set the primary key (jenis_keluar_id column).
     *
     * @param  string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setJenisKeluarId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getJenisKeluarId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of JenisKeluar (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setKetKeluar($this->getKetKeluar());
        $copyObj->setKeluarPd($this->getKeluarPd());
        $copyObj->setKeluarPtk($this->getKeluarPtk());
        $copyObj->setCreateDate($this->getCreateDate());
        $copyObj->setLastUpdate($this->getLastUpdate());
        $copyObj->setExpiredDate($this->getExpiredDate());
        $copyObj->setLastSync($this->getLastSync());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getRegistrasiPesertaDidiksRelatedByJenisKeluarId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRegistrasiPesertaDidikRelatedByJenisKeluarId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRegistrasiPesertaDidiksRelatedByJenisKeluarId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRegistrasiPesertaDidikRelatedByJenisKeluarId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPtkTerdaftarsRelatedByJenisKeluarId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPtkTerdaftarRelatedByJenisKeluarId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPtkTerdaftarsRelatedByJenisKeluarId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPtkTerdaftarRelatedByJenisKeluarId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPengawasTerdaftarsRelatedByJenisKeluarId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPengawasTerdaftarRelatedByJenisKeluarId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPengawasTerdaftarsRelatedByJenisKeluarId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPengawasTerdaftarRelatedByJenisKeluarId($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setJenisKeluarId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return JenisKeluar Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return JenisKeluarPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new JenisKeluarPeer();
        }

        return self::$peer;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('RegistrasiPesertaDidikRelatedByJenisKeluarId' == $relationName) {
            $this->initRegistrasiPesertaDidiksRelatedByJenisKeluarId();
        }
        if ('RegistrasiPesertaDidikRelatedByJenisKeluarId' == $relationName) {
            $this->initRegistrasiPesertaDidiksRelatedByJenisKeluarId();
        }
        if ('PtkTerdaftarRelatedByJenisKeluarId' == $relationName) {
            $this->initPtkTerdaftarsRelatedByJenisKeluarId();
        }
        if ('PtkTerdaftarRelatedByJenisKeluarId' == $relationName) {
            $this->initPtkTerdaftarsRelatedByJenisKeluarId();
        }
        if ('PengawasTerdaftarRelatedByJenisKeluarId' == $relationName) {
            $this->initPengawasTerdaftarsRelatedByJenisKeluarId();
        }
        if ('PengawasTerdaftarRelatedByJenisKeluarId' == $relationName) {
            $this->initPengawasTerdaftarsRelatedByJenisKeluarId();
        }
    }

    /**
     * Clears out the collRegistrasiPesertaDidiksRelatedByJenisKeluarId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return JenisKeluar The current object (for fluent API support)
     * @see        addRegistrasiPesertaDidiksRelatedByJenisKeluarId()
     */
    public function clearRegistrasiPesertaDidiksRelatedByJenisKeluarId()
    {
        $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId = null; // important to set this to null since that means it is uninitialized
        $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarIdPartial = null;

        return $this;
    }

    /**
     * reset is the collRegistrasiPesertaDidiksRelatedByJenisKeluarId collection loaded partially
     *
     * @return void
     */
    public function resetPartialRegistrasiPesertaDidiksRelatedByJenisKeluarId($v = true)
    {
        $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarIdPartial = $v;
    }

    /**
     * Initializes the collRegistrasiPesertaDidiksRelatedByJenisKeluarId collection.
     *
     * By default this just sets the collRegistrasiPesertaDidiksRelatedByJenisKeluarId collection to an empty array (like clearcollRegistrasiPesertaDidiksRelatedByJenisKeluarId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRegistrasiPesertaDidiksRelatedByJenisKeluarId($overrideExisting = true)
    {
        if (null !== $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId && !$overrideExisting) {
            return;
        }
        $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId = new PropelObjectCollection();
        $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId->setModel('RegistrasiPesertaDidik');
    }

    /**
     * Gets an array of RegistrasiPesertaDidik objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this JenisKeluar is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     * @throws PropelException
     */
    public function getRegistrasiPesertaDidiksRelatedByJenisKeluarId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarIdPartial && !$this->isNew();
        if (null === $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId) {
                // return empty collection
                $this->initRegistrasiPesertaDidiksRelatedByJenisKeluarId();
            } else {
                $collRegistrasiPesertaDidiksRelatedByJenisKeluarId = RegistrasiPesertaDidikQuery::create(null, $criteria)
                    ->filterByJenisKeluarRelatedByJenisKeluarId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarIdPartial && count($collRegistrasiPesertaDidiksRelatedByJenisKeluarId)) {
                      $this->initRegistrasiPesertaDidiksRelatedByJenisKeluarId(false);

                      foreach($collRegistrasiPesertaDidiksRelatedByJenisKeluarId as $obj) {
                        if (false == $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId->contains($obj)) {
                          $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId->append($obj);
                        }
                      }

                      $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarIdPartial = true;
                    }

                    $collRegistrasiPesertaDidiksRelatedByJenisKeluarId->getInternalIterator()->rewind();
                    return $collRegistrasiPesertaDidiksRelatedByJenisKeluarId;
                }

                if($partial && $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId) {
                    foreach($this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId as $obj) {
                        if($obj->isNew()) {
                            $collRegistrasiPesertaDidiksRelatedByJenisKeluarId[] = $obj;
                        }
                    }
                }

                $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId = $collRegistrasiPesertaDidiksRelatedByJenisKeluarId;
                $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarIdPartial = false;
            }
        }

        return $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId;
    }

    /**
     * Sets a collection of RegistrasiPesertaDidikRelatedByJenisKeluarId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $registrasiPesertaDidiksRelatedByJenisKeluarId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return JenisKeluar The current object (for fluent API support)
     */
    public function setRegistrasiPesertaDidiksRelatedByJenisKeluarId(PropelCollection $registrasiPesertaDidiksRelatedByJenisKeluarId, PropelPDO $con = null)
    {
        $registrasiPesertaDidiksRelatedByJenisKeluarIdToDelete = $this->getRegistrasiPesertaDidiksRelatedByJenisKeluarId(new Criteria(), $con)->diff($registrasiPesertaDidiksRelatedByJenisKeluarId);

        $this->registrasiPesertaDidiksRelatedByJenisKeluarIdScheduledForDeletion = unserialize(serialize($registrasiPesertaDidiksRelatedByJenisKeluarIdToDelete));

        foreach ($registrasiPesertaDidiksRelatedByJenisKeluarIdToDelete as $registrasiPesertaDidikRelatedByJenisKeluarIdRemoved) {
            $registrasiPesertaDidikRelatedByJenisKeluarIdRemoved->setJenisKeluarRelatedByJenisKeluarId(null);
        }

        $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId = null;
        foreach ($registrasiPesertaDidiksRelatedByJenisKeluarId as $registrasiPesertaDidikRelatedByJenisKeluarId) {
            $this->addRegistrasiPesertaDidikRelatedByJenisKeluarId($registrasiPesertaDidikRelatedByJenisKeluarId);
        }

        $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId = $registrasiPesertaDidiksRelatedByJenisKeluarId;
        $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RegistrasiPesertaDidik objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RegistrasiPesertaDidik objects.
     * @throws PropelException
     */
    public function countRegistrasiPesertaDidiksRelatedByJenisKeluarId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarIdPartial && !$this->isNew();
        if (null === $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getRegistrasiPesertaDidiksRelatedByJenisKeluarId());
            }
            $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByJenisKeluarRelatedByJenisKeluarId($this)
                ->count($con);
        }

        return count($this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId);
    }

    /**
     * Method called to associate a RegistrasiPesertaDidik object to this object
     * through the RegistrasiPesertaDidik foreign key attribute.
     *
     * @param    RegistrasiPesertaDidik $l RegistrasiPesertaDidik
     * @return JenisKeluar The current object (for fluent API support)
     */
    public function addRegistrasiPesertaDidikRelatedByJenisKeluarId(RegistrasiPesertaDidik $l)
    {
        if ($this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId === null) {
            $this->initRegistrasiPesertaDidiksRelatedByJenisKeluarId();
            $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarIdPartial = true;
        }
        if (!in_array($l, $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRegistrasiPesertaDidikRelatedByJenisKeluarId($l);
        }

        return $this;
    }

    /**
     * @param	RegistrasiPesertaDidikRelatedByJenisKeluarId $registrasiPesertaDidikRelatedByJenisKeluarId The registrasiPesertaDidikRelatedByJenisKeluarId object to add.
     */
    protected function doAddRegistrasiPesertaDidikRelatedByJenisKeluarId($registrasiPesertaDidikRelatedByJenisKeluarId)
    {
        $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId[]= $registrasiPesertaDidikRelatedByJenisKeluarId;
        $registrasiPesertaDidikRelatedByJenisKeluarId->setJenisKeluarRelatedByJenisKeluarId($this);
    }

    /**
     * @param	RegistrasiPesertaDidikRelatedByJenisKeluarId $registrasiPesertaDidikRelatedByJenisKeluarId The registrasiPesertaDidikRelatedByJenisKeluarId object to remove.
     * @return JenisKeluar The current object (for fluent API support)
     */
    public function removeRegistrasiPesertaDidikRelatedByJenisKeluarId($registrasiPesertaDidikRelatedByJenisKeluarId)
    {
        if ($this->getRegistrasiPesertaDidiksRelatedByJenisKeluarId()->contains($registrasiPesertaDidikRelatedByJenisKeluarId)) {
            $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId->remove($this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId->search($registrasiPesertaDidikRelatedByJenisKeluarId));
            if (null === $this->registrasiPesertaDidiksRelatedByJenisKeluarIdScheduledForDeletion) {
                $this->registrasiPesertaDidiksRelatedByJenisKeluarIdScheduledForDeletion = clone $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId;
                $this->registrasiPesertaDidiksRelatedByJenisKeluarIdScheduledForDeletion->clear();
            }
            $this->registrasiPesertaDidiksRelatedByJenisKeluarIdScheduledForDeletion[]= $registrasiPesertaDidikRelatedByJenisKeluarId;
            $registrasiPesertaDidikRelatedByJenisKeluarId->setJenisKeluarRelatedByJenisKeluarId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByJenisKeluarIdJoinJurusanSpRelatedByJurusanSpId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JurusanSpRelatedByJurusanSpId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByJenisKeluarIdJoinJurusanSpRelatedByJurusanSpId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JurusanSpRelatedByJurusanSpId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByJenisKeluarIdJoinPesertaDidikRelatedByPesertaDidikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PesertaDidikRelatedByPesertaDidikId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByJenisKeluarIdJoinPesertaDidikRelatedByPesertaDidikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PesertaDidikRelatedByPesertaDidikId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByJenisKeluarIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByJenisKeluarIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByJenisKeluarIdJoinJenisPendaftaranRelatedByJenisPendaftaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisPendaftaranRelatedByJenisPendaftaranId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByJenisKeluarIdJoinJenisPendaftaranRelatedByJenisPendaftaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisPendaftaranRelatedByJenisPendaftaranId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByJenisKeluarId($query, $con);
    }

    /**
     * Clears out the collRegistrasiPesertaDidiksRelatedByJenisKeluarId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return JenisKeluar The current object (for fluent API support)
     * @see        addRegistrasiPesertaDidiksRelatedByJenisKeluarId()
     */
    public function clearRegistrasiPesertaDidiksRelatedByJenisKeluarId()
    {
        $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId = null; // important to set this to null since that means it is uninitialized
        $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarIdPartial = null;

        return $this;
    }

    /**
     * reset is the collRegistrasiPesertaDidiksRelatedByJenisKeluarId collection loaded partially
     *
     * @return void
     */
    public function resetPartialRegistrasiPesertaDidiksRelatedByJenisKeluarId($v = true)
    {
        $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarIdPartial = $v;
    }

    /**
     * Initializes the collRegistrasiPesertaDidiksRelatedByJenisKeluarId collection.
     *
     * By default this just sets the collRegistrasiPesertaDidiksRelatedByJenisKeluarId collection to an empty array (like clearcollRegistrasiPesertaDidiksRelatedByJenisKeluarId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRegistrasiPesertaDidiksRelatedByJenisKeluarId($overrideExisting = true)
    {
        if (null !== $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId && !$overrideExisting) {
            return;
        }
        $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId = new PropelObjectCollection();
        $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId->setModel('RegistrasiPesertaDidik');
    }

    /**
     * Gets an array of RegistrasiPesertaDidik objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this JenisKeluar is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     * @throws PropelException
     */
    public function getRegistrasiPesertaDidiksRelatedByJenisKeluarId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarIdPartial && !$this->isNew();
        if (null === $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId) {
                // return empty collection
                $this->initRegistrasiPesertaDidiksRelatedByJenisKeluarId();
            } else {
                $collRegistrasiPesertaDidiksRelatedByJenisKeluarId = RegistrasiPesertaDidikQuery::create(null, $criteria)
                    ->filterByJenisKeluarRelatedByJenisKeluarId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarIdPartial && count($collRegistrasiPesertaDidiksRelatedByJenisKeluarId)) {
                      $this->initRegistrasiPesertaDidiksRelatedByJenisKeluarId(false);

                      foreach($collRegistrasiPesertaDidiksRelatedByJenisKeluarId as $obj) {
                        if (false == $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId->contains($obj)) {
                          $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId->append($obj);
                        }
                      }

                      $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarIdPartial = true;
                    }

                    $collRegistrasiPesertaDidiksRelatedByJenisKeluarId->getInternalIterator()->rewind();
                    return $collRegistrasiPesertaDidiksRelatedByJenisKeluarId;
                }

                if($partial && $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId) {
                    foreach($this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId as $obj) {
                        if($obj->isNew()) {
                            $collRegistrasiPesertaDidiksRelatedByJenisKeluarId[] = $obj;
                        }
                    }
                }

                $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId = $collRegistrasiPesertaDidiksRelatedByJenisKeluarId;
                $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarIdPartial = false;
            }
        }

        return $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId;
    }

    /**
     * Sets a collection of RegistrasiPesertaDidikRelatedByJenisKeluarId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $registrasiPesertaDidiksRelatedByJenisKeluarId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return JenisKeluar The current object (for fluent API support)
     */
    public function setRegistrasiPesertaDidiksRelatedByJenisKeluarId(PropelCollection $registrasiPesertaDidiksRelatedByJenisKeluarId, PropelPDO $con = null)
    {
        $registrasiPesertaDidiksRelatedByJenisKeluarIdToDelete = $this->getRegistrasiPesertaDidiksRelatedByJenisKeluarId(new Criteria(), $con)->diff($registrasiPesertaDidiksRelatedByJenisKeluarId);

        $this->registrasiPesertaDidiksRelatedByJenisKeluarIdScheduledForDeletion = unserialize(serialize($registrasiPesertaDidiksRelatedByJenisKeluarIdToDelete));

        foreach ($registrasiPesertaDidiksRelatedByJenisKeluarIdToDelete as $registrasiPesertaDidikRelatedByJenisKeluarIdRemoved) {
            $registrasiPesertaDidikRelatedByJenisKeluarIdRemoved->setJenisKeluarRelatedByJenisKeluarId(null);
        }

        $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId = null;
        foreach ($registrasiPesertaDidiksRelatedByJenisKeluarId as $registrasiPesertaDidikRelatedByJenisKeluarId) {
            $this->addRegistrasiPesertaDidikRelatedByJenisKeluarId($registrasiPesertaDidikRelatedByJenisKeluarId);
        }

        $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId = $registrasiPesertaDidiksRelatedByJenisKeluarId;
        $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RegistrasiPesertaDidik objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RegistrasiPesertaDidik objects.
     * @throws PropelException
     */
    public function countRegistrasiPesertaDidiksRelatedByJenisKeluarId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarIdPartial && !$this->isNew();
        if (null === $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getRegistrasiPesertaDidiksRelatedByJenisKeluarId());
            }
            $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByJenisKeluarRelatedByJenisKeluarId($this)
                ->count($con);
        }

        return count($this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId);
    }

    /**
     * Method called to associate a RegistrasiPesertaDidik object to this object
     * through the RegistrasiPesertaDidik foreign key attribute.
     *
     * @param    RegistrasiPesertaDidik $l RegistrasiPesertaDidik
     * @return JenisKeluar The current object (for fluent API support)
     */
    public function addRegistrasiPesertaDidikRelatedByJenisKeluarId(RegistrasiPesertaDidik $l)
    {
        if ($this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId === null) {
            $this->initRegistrasiPesertaDidiksRelatedByJenisKeluarId();
            $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarIdPartial = true;
        }
        if (!in_array($l, $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRegistrasiPesertaDidikRelatedByJenisKeluarId($l);
        }

        return $this;
    }

    /**
     * @param	RegistrasiPesertaDidikRelatedByJenisKeluarId $registrasiPesertaDidikRelatedByJenisKeluarId The registrasiPesertaDidikRelatedByJenisKeluarId object to add.
     */
    protected function doAddRegistrasiPesertaDidikRelatedByJenisKeluarId($registrasiPesertaDidikRelatedByJenisKeluarId)
    {
        $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId[]= $registrasiPesertaDidikRelatedByJenisKeluarId;
        $registrasiPesertaDidikRelatedByJenisKeluarId->setJenisKeluarRelatedByJenisKeluarId($this);
    }

    /**
     * @param	RegistrasiPesertaDidikRelatedByJenisKeluarId $registrasiPesertaDidikRelatedByJenisKeluarId The registrasiPesertaDidikRelatedByJenisKeluarId object to remove.
     * @return JenisKeluar The current object (for fluent API support)
     */
    public function removeRegistrasiPesertaDidikRelatedByJenisKeluarId($registrasiPesertaDidikRelatedByJenisKeluarId)
    {
        if ($this->getRegistrasiPesertaDidiksRelatedByJenisKeluarId()->contains($registrasiPesertaDidikRelatedByJenisKeluarId)) {
            $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId->remove($this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId->search($registrasiPesertaDidikRelatedByJenisKeluarId));
            if (null === $this->registrasiPesertaDidiksRelatedByJenisKeluarIdScheduledForDeletion) {
                $this->registrasiPesertaDidiksRelatedByJenisKeluarIdScheduledForDeletion = clone $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId;
                $this->registrasiPesertaDidiksRelatedByJenisKeluarIdScheduledForDeletion->clear();
            }
            $this->registrasiPesertaDidiksRelatedByJenisKeluarIdScheduledForDeletion[]= $registrasiPesertaDidikRelatedByJenisKeluarId;
            $registrasiPesertaDidikRelatedByJenisKeluarId->setJenisKeluarRelatedByJenisKeluarId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByJenisKeluarIdJoinJurusanSpRelatedByJurusanSpId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JurusanSpRelatedByJurusanSpId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByJenisKeluarIdJoinJurusanSpRelatedByJurusanSpId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JurusanSpRelatedByJurusanSpId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByJenisKeluarIdJoinPesertaDidikRelatedByPesertaDidikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PesertaDidikRelatedByPesertaDidikId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByJenisKeluarIdJoinPesertaDidikRelatedByPesertaDidikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PesertaDidikRelatedByPesertaDidikId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByJenisKeluarIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByJenisKeluarIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByJenisKeluarIdJoinJenisPendaftaranRelatedByJenisPendaftaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisPendaftaranRelatedByJenisPendaftaranId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByJenisKeluarIdJoinJenisPendaftaranRelatedByJenisPendaftaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisPendaftaranRelatedByJenisPendaftaranId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByJenisKeluarId($query, $con);
    }

    /**
     * Clears out the collPtkTerdaftarsRelatedByJenisKeluarId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return JenisKeluar The current object (for fluent API support)
     * @see        addPtkTerdaftarsRelatedByJenisKeluarId()
     */
    public function clearPtkTerdaftarsRelatedByJenisKeluarId()
    {
        $this->collPtkTerdaftarsRelatedByJenisKeluarId = null; // important to set this to null since that means it is uninitialized
        $this->collPtkTerdaftarsRelatedByJenisKeluarIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPtkTerdaftarsRelatedByJenisKeluarId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPtkTerdaftarsRelatedByJenisKeluarId($v = true)
    {
        $this->collPtkTerdaftarsRelatedByJenisKeluarIdPartial = $v;
    }

    /**
     * Initializes the collPtkTerdaftarsRelatedByJenisKeluarId collection.
     *
     * By default this just sets the collPtkTerdaftarsRelatedByJenisKeluarId collection to an empty array (like clearcollPtkTerdaftarsRelatedByJenisKeluarId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPtkTerdaftarsRelatedByJenisKeluarId($overrideExisting = true)
    {
        if (null !== $this->collPtkTerdaftarsRelatedByJenisKeluarId && !$overrideExisting) {
            return;
        }
        $this->collPtkTerdaftarsRelatedByJenisKeluarId = new PropelObjectCollection();
        $this->collPtkTerdaftarsRelatedByJenisKeluarId->setModel('PtkTerdaftar');
    }

    /**
     * Gets an array of PtkTerdaftar objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this JenisKeluar is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     * @throws PropelException
     */
    public function getPtkTerdaftarsRelatedByJenisKeluarId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPtkTerdaftarsRelatedByJenisKeluarIdPartial && !$this->isNew();
        if (null === $this->collPtkTerdaftarsRelatedByJenisKeluarId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPtkTerdaftarsRelatedByJenisKeluarId) {
                // return empty collection
                $this->initPtkTerdaftarsRelatedByJenisKeluarId();
            } else {
                $collPtkTerdaftarsRelatedByJenisKeluarId = PtkTerdaftarQuery::create(null, $criteria)
                    ->filterByJenisKeluarRelatedByJenisKeluarId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPtkTerdaftarsRelatedByJenisKeluarIdPartial && count($collPtkTerdaftarsRelatedByJenisKeluarId)) {
                      $this->initPtkTerdaftarsRelatedByJenisKeluarId(false);

                      foreach($collPtkTerdaftarsRelatedByJenisKeluarId as $obj) {
                        if (false == $this->collPtkTerdaftarsRelatedByJenisKeluarId->contains($obj)) {
                          $this->collPtkTerdaftarsRelatedByJenisKeluarId->append($obj);
                        }
                      }

                      $this->collPtkTerdaftarsRelatedByJenisKeluarIdPartial = true;
                    }

                    $collPtkTerdaftarsRelatedByJenisKeluarId->getInternalIterator()->rewind();
                    return $collPtkTerdaftarsRelatedByJenisKeluarId;
                }

                if($partial && $this->collPtkTerdaftarsRelatedByJenisKeluarId) {
                    foreach($this->collPtkTerdaftarsRelatedByJenisKeluarId as $obj) {
                        if($obj->isNew()) {
                            $collPtkTerdaftarsRelatedByJenisKeluarId[] = $obj;
                        }
                    }
                }

                $this->collPtkTerdaftarsRelatedByJenisKeluarId = $collPtkTerdaftarsRelatedByJenisKeluarId;
                $this->collPtkTerdaftarsRelatedByJenisKeluarIdPartial = false;
            }
        }

        return $this->collPtkTerdaftarsRelatedByJenisKeluarId;
    }

    /**
     * Sets a collection of PtkTerdaftarRelatedByJenisKeluarId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $ptkTerdaftarsRelatedByJenisKeluarId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return JenisKeluar The current object (for fluent API support)
     */
    public function setPtkTerdaftarsRelatedByJenisKeluarId(PropelCollection $ptkTerdaftarsRelatedByJenisKeluarId, PropelPDO $con = null)
    {
        $ptkTerdaftarsRelatedByJenisKeluarIdToDelete = $this->getPtkTerdaftarsRelatedByJenisKeluarId(new Criteria(), $con)->diff($ptkTerdaftarsRelatedByJenisKeluarId);

        $this->ptkTerdaftarsRelatedByJenisKeluarIdScheduledForDeletion = unserialize(serialize($ptkTerdaftarsRelatedByJenisKeluarIdToDelete));

        foreach ($ptkTerdaftarsRelatedByJenisKeluarIdToDelete as $ptkTerdaftarRelatedByJenisKeluarIdRemoved) {
            $ptkTerdaftarRelatedByJenisKeluarIdRemoved->setJenisKeluarRelatedByJenisKeluarId(null);
        }

        $this->collPtkTerdaftarsRelatedByJenisKeluarId = null;
        foreach ($ptkTerdaftarsRelatedByJenisKeluarId as $ptkTerdaftarRelatedByJenisKeluarId) {
            $this->addPtkTerdaftarRelatedByJenisKeluarId($ptkTerdaftarRelatedByJenisKeluarId);
        }

        $this->collPtkTerdaftarsRelatedByJenisKeluarId = $ptkTerdaftarsRelatedByJenisKeluarId;
        $this->collPtkTerdaftarsRelatedByJenisKeluarIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PtkTerdaftar objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related PtkTerdaftar objects.
     * @throws PropelException
     */
    public function countPtkTerdaftarsRelatedByJenisKeluarId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPtkTerdaftarsRelatedByJenisKeluarIdPartial && !$this->isNew();
        if (null === $this->collPtkTerdaftarsRelatedByJenisKeluarId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPtkTerdaftarsRelatedByJenisKeluarId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPtkTerdaftarsRelatedByJenisKeluarId());
            }
            $query = PtkTerdaftarQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByJenisKeluarRelatedByJenisKeluarId($this)
                ->count($con);
        }

        return count($this->collPtkTerdaftarsRelatedByJenisKeluarId);
    }

    /**
     * Method called to associate a PtkTerdaftar object to this object
     * through the PtkTerdaftar foreign key attribute.
     *
     * @param    PtkTerdaftar $l PtkTerdaftar
     * @return JenisKeluar The current object (for fluent API support)
     */
    public function addPtkTerdaftarRelatedByJenisKeluarId(PtkTerdaftar $l)
    {
        if ($this->collPtkTerdaftarsRelatedByJenisKeluarId === null) {
            $this->initPtkTerdaftarsRelatedByJenisKeluarId();
            $this->collPtkTerdaftarsRelatedByJenisKeluarIdPartial = true;
        }
        if (!in_array($l, $this->collPtkTerdaftarsRelatedByJenisKeluarId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPtkTerdaftarRelatedByJenisKeluarId($l);
        }

        return $this;
    }

    /**
     * @param	PtkTerdaftarRelatedByJenisKeluarId $ptkTerdaftarRelatedByJenisKeluarId The ptkTerdaftarRelatedByJenisKeluarId object to add.
     */
    protected function doAddPtkTerdaftarRelatedByJenisKeluarId($ptkTerdaftarRelatedByJenisKeluarId)
    {
        $this->collPtkTerdaftarsRelatedByJenisKeluarId[]= $ptkTerdaftarRelatedByJenisKeluarId;
        $ptkTerdaftarRelatedByJenisKeluarId->setJenisKeluarRelatedByJenisKeluarId($this);
    }

    /**
     * @param	PtkTerdaftarRelatedByJenisKeluarId $ptkTerdaftarRelatedByJenisKeluarId The ptkTerdaftarRelatedByJenisKeluarId object to remove.
     * @return JenisKeluar The current object (for fluent API support)
     */
    public function removePtkTerdaftarRelatedByJenisKeluarId($ptkTerdaftarRelatedByJenisKeluarId)
    {
        if ($this->getPtkTerdaftarsRelatedByJenisKeluarId()->contains($ptkTerdaftarRelatedByJenisKeluarId)) {
            $this->collPtkTerdaftarsRelatedByJenisKeluarId->remove($this->collPtkTerdaftarsRelatedByJenisKeluarId->search($ptkTerdaftarRelatedByJenisKeluarId));
            if (null === $this->ptkTerdaftarsRelatedByJenisKeluarIdScheduledForDeletion) {
                $this->ptkTerdaftarsRelatedByJenisKeluarIdScheduledForDeletion = clone $this->collPtkTerdaftarsRelatedByJenisKeluarId;
                $this->ptkTerdaftarsRelatedByJenisKeluarIdScheduledForDeletion->clear();
            }
            $this->ptkTerdaftarsRelatedByJenisKeluarIdScheduledForDeletion[]= $ptkTerdaftarRelatedByJenisKeluarId;
            $ptkTerdaftarRelatedByJenisKeluarId->setJenisKeluarRelatedByJenisKeluarId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related PtkTerdaftarsRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     */
    public function getPtkTerdaftarsRelatedByJenisKeluarIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkTerdaftarQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getPtkTerdaftarsRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related PtkTerdaftarsRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     */
    public function getPtkTerdaftarsRelatedByJenisKeluarIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkTerdaftarQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getPtkTerdaftarsRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related PtkTerdaftarsRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     */
    public function getPtkTerdaftarsRelatedByJenisKeluarIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkTerdaftarQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPtkTerdaftarsRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related PtkTerdaftarsRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     */
    public function getPtkTerdaftarsRelatedByJenisKeluarIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkTerdaftarQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPtkTerdaftarsRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related PtkTerdaftarsRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     */
    public function getPtkTerdaftarsRelatedByJenisKeluarIdJoinTahunAjaranRelatedByTahunAjaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkTerdaftarQuery::create(null, $criteria);
        $query->joinWith('TahunAjaranRelatedByTahunAjaranId', $join_behavior);

        return $this->getPtkTerdaftarsRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related PtkTerdaftarsRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     */
    public function getPtkTerdaftarsRelatedByJenisKeluarIdJoinTahunAjaranRelatedByTahunAjaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkTerdaftarQuery::create(null, $criteria);
        $query->joinWith('TahunAjaranRelatedByTahunAjaranId', $join_behavior);

        return $this->getPtkTerdaftarsRelatedByJenisKeluarId($query, $con);
    }

    /**
     * Clears out the collPtkTerdaftarsRelatedByJenisKeluarId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return JenisKeluar The current object (for fluent API support)
     * @see        addPtkTerdaftarsRelatedByJenisKeluarId()
     */
    public function clearPtkTerdaftarsRelatedByJenisKeluarId()
    {
        $this->collPtkTerdaftarsRelatedByJenisKeluarId = null; // important to set this to null since that means it is uninitialized
        $this->collPtkTerdaftarsRelatedByJenisKeluarIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPtkTerdaftarsRelatedByJenisKeluarId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPtkTerdaftarsRelatedByJenisKeluarId($v = true)
    {
        $this->collPtkTerdaftarsRelatedByJenisKeluarIdPartial = $v;
    }

    /**
     * Initializes the collPtkTerdaftarsRelatedByJenisKeluarId collection.
     *
     * By default this just sets the collPtkTerdaftarsRelatedByJenisKeluarId collection to an empty array (like clearcollPtkTerdaftarsRelatedByJenisKeluarId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPtkTerdaftarsRelatedByJenisKeluarId($overrideExisting = true)
    {
        if (null !== $this->collPtkTerdaftarsRelatedByJenisKeluarId && !$overrideExisting) {
            return;
        }
        $this->collPtkTerdaftarsRelatedByJenisKeluarId = new PropelObjectCollection();
        $this->collPtkTerdaftarsRelatedByJenisKeluarId->setModel('PtkTerdaftar');
    }

    /**
     * Gets an array of PtkTerdaftar objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this JenisKeluar is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     * @throws PropelException
     */
    public function getPtkTerdaftarsRelatedByJenisKeluarId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPtkTerdaftarsRelatedByJenisKeluarIdPartial && !$this->isNew();
        if (null === $this->collPtkTerdaftarsRelatedByJenisKeluarId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPtkTerdaftarsRelatedByJenisKeluarId) {
                // return empty collection
                $this->initPtkTerdaftarsRelatedByJenisKeluarId();
            } else {
                $collPtkTerdaftarsRelatedByJenisKeluarId = PtkTerdaftarQuery::create(null, $criteria)
                    ->filterByJenisKeluarRelatedByJenisKeluarId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPtkTerdaftarsRelatedByJenisKeluarIdPartial && count($collPtkTerdaftarsRelatedByJenisKeluarId)) {
                      $this->initPtkTerdaftarsRelatedByJenisKeluarId(false);

                      foreach($collPtkTerdaftarsRelatedByJenisKeluarId as $obj) {
                        if (false == $this->collPtkTerdaftarsRelatedByJenisKeluarId->contains($obj)) {
                          $this->collPtkTerdaftarsRelatedByJenisKeluarId->append($obj);
                        }
                      }

                      $this->collPtkTerdaftarsRelatedByJenisKeluarIdPartial = true;
                    }

                    $collPtkTerdaftarsRelatedByJenisKeluarId->getInternalIterator()->rewind();
                    return $collPtkTerdaftarsRelatedByJenisKeluarId;
                }

                if($partial && $this->collPtkTerdaftarsRelatedByJenisKeluarId) {
                    foreach($this->collPtkTerdaftarsRelatedByJenisKeluarId as $obj) {
                        if($obj->isNew()) {
                            $collPtkTerdaftarsRelatedByJenisKeluarId[] = $obj;
                        }
                    }
                }

                $this->collPtkTerdaftarsRelatedByJenisKeluarId = $collPtkTerdaftarsRelatedByJenisKeluarId;
                $this->collPtkTerdaftarsRelatedByJenisKeluarIdPartial = false;
            }
        }

        return $this->collPtkTerdaftarsRelatedByJenisKeluarId;
    }

    /**
     * Sets a collection of PtkTerdaftarRelatedByJenisKeluarId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $ptkTerdaftarsRelatedByJenisKeluarId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return JenisKeluar The current object (for fluent API support)
     */
    public function setPtkTerdaftarsRelatedByJenisKeluarId(PropelCollection $ptkTerdaftarsRelatedByJenisKeluarId, PropelPDO $con = null)
    {
        $ptkTerdaftarsRelatedByJenisKeluarIdToDelete = $this->getPtkTerdaftarsRelatedByJenisKeluarId(new Criteria(), $con)->diff($ptkTerdaftarsRelatedByJenisKeluarId);

        $this->ptkTerdaftarsRelatedByJenisKeluarIdScheduledForDeletion = unserialize(serialize($ptkTerdaftarsRelatedByJenisKeluarIdToDelete));

        foreach ($ptkTerdaftarsRelatedByJenisKeluarIdToDelete as $ptkTerdaftarRelatedByJenisKeluarIdRemoved) {
            $ptkTerdaftarRelatedByJenisKeluarIdRemoved->setJenisKeluarRelatedByJenisKeluarId(null);
        }

        $this->collPtkTerdaftarsRelatedByJenisKeluarId = null;
        foreach ($ptkTerdaftarsRelatedByJenisKeluarId as $ptkTerdaftarRelatedByJenisKeluarId) {
            $this->addPtkTerdaftarRelatedByJenisKeluarId($ptkTerdaftarRelatedByJenisKeluarId);
        }

        $this->collPtkTerdaftarsRelatedByJenisKeluarId = $ptkTerdaftarsRelatedByJenisKeluarId;
        $this->collPtkTerdaftarsRelatedByJenisKeluarIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PtkTerdaftar objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related PtkTerdaftar objects.
     * @throws PropelException
     */
    public function countPtkTerdaftarsRelatedByJenisKeluarId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPtkTerdaftarsRelatedByJenisKeluarIdPartial && !$this->isNew();
        if (null === $this->collPtkTerdaftarsRelatedByJenisKeluarId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPtkTerdaftarsRelatedByJenisKeluarId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPtkTerdaftarsRelatedByJenisKeluarId());
            }
            $query = PtkTerdaftarQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByJenisKeluarRelatedByJenisKeluarId($this)
                ->count($con);
        }

        return count($this->collPtkTerdaftarsRelatedByJenisKeluarId);
    }

    /**
     * Method called to associate a PtkTerdaftar object to this object
     * through the PtkTerdaftar foreign key attribute.
     *
     * @param    PtkTerdaftar $l PtkTerdaftar
     * @return JenisKeluar The current object (for fluent API support)
     */
    public function addPtkTerdaftarRelatedByJenisKeluarId(PtkTerdaftar $l)
    {
        if ($this->collPtkTerdaftarsRelatedByJenisKeluarId === null) {
            $this->initPtkTerdaftarsRelatedByJenisKeluarId();
            $this->collPtkTerdaftarsRelatedByJenisKeluarIdPartial = true;
        }
        if (!in_array($l, $this->collPtkTerdaftarsRelatedByJenisKeluarId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPtkTerdaftarRelatedByJenisKeluarId($l);
        }

        return $this;
    }

    /**
     * @param	PtkTerdaftarRelatedByJenisKeluarId $ptkTerdaftarRelatedByJenisKeluarId The ptkTerdaftarRelatedByJenisKeluarId object to add.
     */
    protected function doAddPtkTerdaftarRelatedByJenisKeluarId($ptkTerdaftarRelatedByJenisKeluarId)
    {
        $this->collPtkTerdaftarsRelatedByJenisKeluarId[]= $ptkTerdaftarRelatedByJenisKeluarId;
        $ptkTerdaftarRelatedByJenisKeluarId->setJenisKeluarRelatedByJenisKeluarId($this);
    }

    /**
     * @param	PtkTerdaftarRelatedByJenisKeluarId $ptkTerdaftarRelatedByJenisKeluarId The ptkTerdaftarRelatedByJenisKeluarId object to remove.
     * @return JenisKeluar The current object (for fluent API support)
     */
    public function removePtkTerdaftarRelatedByJenisKeluarId($ptkTerdaftarRelatedByJenisKeluarId)
    {
        if ($this->getPtkTerdaftarsRelatedByJenisKeluarId()->contains($ptkTerdaftarRelatedByJenisKeluarId)) {
            $this->collPtkTerdaftarsRelatedByJenisKeluarId->remove($this->collPtkTerdaftarsRelatedByJenisKeluarId->search($ptkTerdaftarRelatedByJenisKeluarId));
            if (null === $this->ptkTerdaftarsRelatedByJenisKeluarIdScheduledForDeletion) {
                $this->ptkTerdaftarsRelatedByJenisKeluarIdScheduledForDeletion = clone $this->collPtkTerdaftarsRelatedByJenisKeluarId;
                $this->ptkTerdaftarsRelatedByJenisKeluarIdScheduledForDeletion->clear();
            }
            $this->ptkTerdaftarsRelatedByJenisKeluarIdScheduledForDeletion[]= $ptkTerdaftarRelatedByJenisKeluarId;
            $ptkTerdaftarRelatedByJenisKeluarId->setJenisKeluarRelatedByJenisKeluarId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related PtkTerdaftarsRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     */
    public function getPtkTerdaftarsRelatedByJenisKeluarIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkTerdaftarQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getPtkTerdaftarsRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related PtkTerdaftarsRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     */
    public function getPtkTerdaftarsRelatedByJenisKeluarIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkTerdaftarQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getPtkTerdaftarsRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related PtkTerdaftarsRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     */
    public function getPtkTerdaftarsRelatedByJenisKeluarIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkTerdaftarQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPtkTerdaftarsRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related PtkTerdaftarsRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     */
    public function getPtkTerdaftarsRelatedByJenisKeluarIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkTerdaftarQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPtkTerdaftarsRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related PtkTerdaftarsRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     */
    public function getPtkTerdaftarsRelatedByJenisKeluarIdJoinTahunAjaranRelatedByTahunAjaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkTerdaftarQuery::create(null, $criteria);
        $query->joinWith('TahunAjaranRelatedByTahunAjaranId', $join_behavior);

        return $this->getPtkTerdaftarsRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related PtkTerdaftarsRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     */
    public function getPtkTerdaftarsRelatedByJenisKeluarIdJoinTahunAjaranRelatedByTahunAjaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkTerdaftarQuery::create(null, $criteria);
        $query->joinWith('TahunAjaranRelatedByTahunAjaranId', $join_behavior);

        return $this->getPtkTerdaftarsRelatedByJenisKeluarId($query, $con);
    }

    /**
     * Clears out the collPengawasTerdaftarsRelatedByJenisKeluarId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return JenisKeluar The current object (for fluent API support)
     * @see        addPengawasTerdaftarsRelatedByJenisKeluarId()
     */
    public function clearPengawasTerdaftarsRelatedByJenisKeluarId()
    {
        $this->collPengawasTerdaftarsRelatedByJenisKeluarId = null; // important to set this to null since that means it is uninitialized
        $this->collPengawasTerdaftarsRelatedByJenisKeluarIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPengawasTerdaftarsRelatedByJenisKeluarId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPengawasTerdaftarsRelatedByJenisKeluarId($v = true)
    {
        $this->collPengawasTerdaftarsRelatedByJenisKeluarIdPartial = $v;
    }

    /**
     * Initializes the collPengawasTerdaftarsRelatedByJenisKeluarId collection.
     *
     * By default this just sets the collPengawasTerdaftarsRelatedByJenisKeluarId collection to an empty array (like clearcollPengawasTerdaftarsRelatedByJenisKeluarId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPengawasTerdaftarsRelatedByJenisKeluarId($overrideExisting = true)
    {
        if (null !== $this->collPengawasTerdaftarsRelatedByJenisKeluarId && !$overrideExisting) {
            return;
        }
        $this->collPengawasTerdaftarsRelatedByJenisKeluarId = new PropelObjectCollection();
        $this->collPengawasTerdaftarsRelatedByJenisKeluarId->setModel('PengawasTerdaftar');
    }

    /**
     * Gets an array of PengawasTerdaftar objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this JenisKeluar is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     * @throws PropelException
     */
    public function getPengawasTerdaftarsRelatedByJenisKeluarId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPengawasTerdaftarsRelatedByJenisKeluarIdPartial && !$this->isNew();
        if (null === $this->collPengawasTerdaftarsRelatedByJenisKeluarId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPengawasTerdaftarsRelatedByJenisKeluarId) {
                // return empty collection
                $this->initPengawasTerdaftarsRelatedByJenisKeluarId();
            } else {
                $collPengawasTerdaftarsRelatedByJenisKeluarId = PengawasTerdaftarQuery::create(null, $criteria)
                    ->filterByJenisKeluarRelatedByJenisKeluarId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPengawasTerdaftarsRelatedByJenisKeluarIdPartial && count($collPengawasTerdaftarsRelatedByJenisKeluarId)) {
                      $this->initPengawasTerdaftarsRelatedByJenisKeluarId(false);

                      foreach($collPengawasTerdaftarsRelatedByJenisKeluarId as $obj) {
                        if (false == $this->collPengawasTerdaftarsRelatedByJenisKeluarId->contains($obj)) {
                          $this->collPengawasTerdaftarsRelatedByJenisKeluarId->append($obj);
                        }
                      }

                      $this->collPengawasTerdaftarsRelatedByJenisKeluarIdPartial = true;
                    }

                    $collPengawasTerdaftarsRelatedByJenisKeluarId->getInternalIterator()->rewind();
                    return $collPengawasTerdaftarsRelatedByJenisKeluarId;
                }

                if($partial && $this->collPengawasTerdaftarsRelatedByJenisKeluarId) {
                    foreach($this->collPengawasTerdaftarsRelatedByJenisKeluarId as $obj) {
                        if($obj->isNew()) {
                            $collPengawasTerdaftarsRelatedByJenisKeluarId[] = $obj;
                        }
                    }
                }

                $this->collPengawasTerdaftarsRelatedByJenisKeluarId = $collPengawasTerdaftarsRelatedByJenisKeluarId;
                $this->collPengawasTerdaftarsRelatedByJenisKeluarIdPartial = false;
            }
        }

        return $this->collPengawasTerdaftarsRelatedByJenisKeluarId;
    }

    /**
     * Sets a collection of PengawasTerdaftarRelatedByJenisKeluarId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $pengawasTerdaftarsRelatedByJenisKeluarId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return JenisKeluar The current object (for fluent API support)
     */
    public function setPengawasTerdaftarsRelatedByJenisKeluarId(PropelCollection $pengawasTerdaftarsRelatedByJenisKeluarId, PropelPDO $con = null)
    {
        $pengawasTerdaftarsRelatedByJenisKeluarIdToDelete = $this->getPengawasTerdaftarsRelatedByJenisKeluarId(new Criteria(), $con)->diff($pengawasTerdaftarsRelatedByJenisKeluarId);

        $this->pengawasTerdaftarsRelatedByJenisKeluarIdScheduledForDeletion = unserialize(serialize($pengawasTerdaftarsRelatedByJenisKeluarIdToDelete));

        foreach ($pengawasTerdaftarsRelatedByJenisKeluarIdToDelete as $pengawasTerdaftarRelatedByJenisKeluarIdRemoved) {
            $pengawasTerdaftarRelatedByJenisKeluarIdRemoved->setJenisKeluarRelatedByJenisKeluarId(null);
        }

        $this->collPengawasTerdaftarsRelatedByJenisKeluarId = null;
        foreach ($pengawasTerdaftarsRelatedByJenisKeluarId as $pengawasTerdaftarRelatedByJenisKeluarId) {
            $this->addPengawasTerdaftarRelatedByJenisKeluarId($pengawasTerdaftarRelatedByJenisKeluarId);
        }

        $this->collPengawasTerdaftarsRelatedByJenisKeluarId = $pengawasTerdaftarsRelatedByJenisKeluarId;
        $this->collPengawasTerdaftarsRelatedByJenisKeluarIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PengawasTerdaftar objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related PengawasTerdaftar objects.
     * @throws PropelException
     */
    public function countPengawasTerdaftarsRelatedByJenisKeluarId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPengawasTerdaftarsRelatedByJenisKeluarIdPartial && !$this->isNew();
        if (null === $this->collPengawasTerdaftarsRelatedByJenisKeluarId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPengawasTerdaftarsRelatedByJenisKeluarId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPengawasTerdaftarsRelatedByJenisKeluarId());
            }
            $query = PengawasTerdaftarQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByJenisKeluarRelatedByJenisKeluarId($this)
                ->count($con);
        }

        return count($this->collPengawasTerdaftarsRelatedByJenisKeluarId);
    }

    /**
     * Method called to associate a PengawasTerdaftar object to this object
     * through the PengawasTerdaftar foreign key attribute.
     *
     * @param    PengawasTerdaftar $l PengawasTerdaftar
     * @return JenisKeluar The current object (for fluent API support)
     */
    public function addPengawasTerdaftarRelatedByJenisKeluarId(PengawasTerdaftar $l)
    {
        if ($this->collPengawasTerdaftarsRelatedByJenisKeluarId === null) {
            $this->initPengawasTerdaftarsRelatedByJenisKeluarId();
            $this->collPengawasTerdaftarsRelatedByJenisKeluarIdPartial = true;
        }
        if (!in_array($l, $this->collPengawasTerdaftarsRelatedByJenisKeluarId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPengawasTerdaftarRelatedByJenisKeluarId($l);
        }

        return $this;
    }

    /**
     * @param	PengawasTerdaftarRelatedByJenisKeluarId $pengawasTerdaftarRelatedByJenisKeluarId The pengawasTerdaftarRelatedByJenisKeluarId object to add.
     */
    protected function doAddPengawasTerdaftarRelatedByJenisKeluarId($pengawasTerdaftarRelatedByJenisKeluarId)
    {
        $this->collPengawasTerdaftarsRelatedByJenisKeluarId[]= $pengawasTerdaftarRelatedByJenisKeluarId;
        $pengawasTerdaftarRelatedByJenisKeluarId->setJenisKeluarRelatedByJenisKeluarId($this);
    }

    /**
     * @param	PengawasTerdaftarRelatedByJenisKeluarId $pengawasTerdaftarRelatedByJenisKeluarId The pengawasTerdaftarRelatedByJenisKeluarId object to remove.
     * @return JenisKeluar The current object (for fluent API support)
     */
    public function removePengawasTerdaftarRelatedByJenisKeluarId($pengawasTerdaftarRelatedByJenisKeluarId)
    {
        if ($this->getPengawasTerdaftarsRelatedByJenisKeluarId()->contains($pengawasTerdaftarRelatedByJenisKeluarId)) {
            $this->collPengawasTerdaftarsRelatedByJenisKeluarId->remove($this->collPengawasTerdaftarsRelatedByJenisKeluarId->search($pengawasTerdaftarRelatedByJenisKeluarId));
            if (null === $this->pengawasTerdaftarsRelatedByJenisKeluarIdScheduledForDeletion) {
                $this->pengawasTerdaftarsRelatedByJenisKeluarIdScheduledForDeletion = clone $this->collPengawasTerdaftarsRelatedByJenisKeluarId;
                $this->pengawasTerdaftarsRelatedByJenisKeluarIdScheduledForDeletion->clear();
            }
            $this->pengawasTerdaftarsRelatedByJenisKeluarIdScheduledForDeletion[]= $pengawasTerdaftarRelatedByJenisKeluarId;
            $pengawasTerdaftarRelatedByJenisKeluarId->setJenisKeluarRelatedByJenisKeluarId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByJenisKeluarIdJoinLembagaNonSekolahRelatedByLembagaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('LembagaNonSekolahRelatedByLembagaId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByJenisKeluarIdJoinLembagaNonSekolahRelatedByLembagaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('LembagaNonSekolahRelatedByLembagaId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByJenisKeluarIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByJenisKeluarIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByJenisKeluarIdJoinBidangStudiRelatedByBidangStudiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('BidangStudiRelatedByBidangStudiId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByJenisKeluarIdJoinBidangStudiRelatedByBidangStudiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('BidangStudiRelatedByBidangStudiId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByJenisKeluarIdJoinJenjangKepengawasanRelatedByJenjangKepengawasanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('JenjangKepengawasanRelatedByJenjangKepengawasanId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByJenisKeluarIdJoinJenjangKepengawasanRelatedByJenjangKepengawasanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('JenjangKepengawasanRelatedByJenjangKepengawasanId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByJenisKeluarIdJoinMataPelajaranRelatedByMataPelajaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('MataPelajaranRelatedByMataPelajaranId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByJenisKeluarIdJoinMataPelajaranRelatedByMataPelajaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('MataPelajaranRelatedByMataPelajaranId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByJenisKeluarIdJoinTahunAjaranRelatedByTahunAjaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('TahunAjaranRelatedByTahunAjaranId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByJenisKeluarIdJoinTahunAjaranRelatedByTahunAjaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('TahunAjaranRelatedByTahunAjaranId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByJenisKeluarId($query, $con);
    }

    /**
     * Clears out the collPengawasTerdaftarsRelatedByJenisKeluarId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return JenisKeluar The current object (for fluent API support)
     * @see        addPengawasTerdaftarsRelatedByJenisKeluarId()
     */
    public function clearPengawasTerdaftarsRelatedByJenisKeluarId()
    {
        $this->collPengawasTerdaftarsRelatedByJenisKeluarId = null; // important to set this to null since that means it is uninitialized
        $this->collPengawasTerdaftarsRelatedByJenisKeluarIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPengawasTerdaftarsRelatedByJenisKeluarId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPengawasTerdaftarsRelatedByJenisKeluarId($v = true)
    {
        $this->collPengawasTerdaftarsRelatedByJenisKeluarIdPartial = $v;
    }

    /**
     * Initializes the collPengawasTerdaftarsRelatedByJenisKeluarId collection.
     *
     * By default this just sets the collPengawasTerdaftarsRelatedByJenisKeluarId collection to an empty array (like clearcollPengawasTerdaftarsRelatedByJenisKeluarId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPengawasTerdaftarsRelatedByJenisKeluarId($overrideExisting = true)
    {
        if (null !== $this->collPengawasTerdaftarsRelatedByJenisKeluarId && !$overrideExisting) {
            return;
        }
        $this->collPengawasTerdaftarsRelatedByJenisKeluarId = new PropelObjectCollection();
        $this->collPengawasTerdaftarsRelatedByJenisKeluarId->setModel('PengawasTerdaftar');
    }

    /**
     * Gets an array of PengawasTerdaftar objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this JenisKeluar is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     * @throws PropelException
     */
    public function getPengawasTerdaftarsRelatedByJenisKeluarId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPengawasTerdaftarsRelatedByJenisKeluarIdPartial && !$this->isNew();
        if (null === $this->collPengawasTerdaftarsRelatedByJenisKeluarId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPengawasTerdaftarsRelatedByJenisKeluarId) {
                // return empty collection
                $this->initPengawasTerdaftarsRelatedByJenisKeluarId();
            } else {
                $collPengawasTerdaftarsRelatedByJenisKeluarId = PengawasTerdaftarQuery::create(null, $criteria)
                    ->filterByJenisKeluarRelatedByJenisKeluarId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPengawasTerdaftarsRelatedByJenisKeluarIdPartial && count($collPengawasTerdaftarsRelatedByJenisKeluarId)) {
                      $this->initPengawasTerdaftarsRelatedByJenisKeluarId(false);

                      foreach($collPengawasTerdaftarsRelatedByJenisKeluarId as $obj) {
                        if (false == $this->collPengawasTerdaftarsRelatedByJenisKeluarId->contains($obj)) {
                          $this->collPengawasTerdaftarsRelatedByJenisKeluarId->append($obj);
                        }
                      }

                      $this->collPengawasTerdaftarsRelatedByJenisKeluarIdPartial = true;
                    }

                    $collPengawasTerdaftarsRelatedByJenisKeluarId->getInternalIterator()->rewind();
                    return $collPengawasTerdaftarsRelatedByJenisKeluarId;
                }

                if($partial && $this->collPengawasTerdaftarsRelatedByJenisKeluarId) {
                    foreach($this->collPengawasTerdaftarsRelatedByJenisKeluarId as $obj) {
                        if($obj->isNew()) {
                            $collPengawasTerdaftarsRelatedByJenisKeluarId[] = $obj;
                        }
                    }
                }

                $this->collPengawasTerdaftarsRelatedByJenisKeluarId = $collPengawasTerdaftarsRelatedByJenisKeluarId;
                $this->collPengawasTerdaftarsRelatedByJenisKeluarIdPartial = false;
            }
        }

        return $this->collPengawasTerdaftarsRelatedByJenisKeluarId;
    }

    /**
     * Sets a collection of PengawasTerdaftarRelatedByJenisKeluarId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $pengawasTerdaftarsRelatedByJenisKeluarId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return JenisKeluar The current object (for fluent API support)
     */
    public function setPengawasTerdaftarsRelatedByJenisKeluarId(PropelCollection $pengawasTerdaftarsRelatedByJenisKeluarId, PropelPDO $con = null)
    {
        $pengawasTerdaftarsRelatedByJenisKeluarIdToDelete = $this->getPengawasTerdaftarsRelatedByJenisKeluarId(new Criteria(), $con)->diff($pengawasTerdaftarsRelatedByJenisKeluarId);

        $this->pengawasTerdaftarsRelatedByJenisKeluarIdScheduledForDeletion = unserialize(serialize($pengawasTerdaftarsRelatedByJenisKeluarIdToDelete));

        foreach ($pengawasTerdaftarsRelatedByJenisKeluarIdToDelete as $pengawasTerdaftarRelatedByJenisKeluarIdRemoved) {
            $pengawasTerdaftarRelatedByJenisKeluarIdRemoved->setJenisKeluarRelatedByJenisKeluarId(null);
        }

        $this->collPengawasTerdaftarsRelatedByJenisKeluarId = null;
        foreach ($pengawasTerdaftarsRelatedByJenisKeluarId as $pengawasTerdaftarRelatedByJenisKeluarId) {
            $this->addPengawasTerdaftarRelatedByJenisKeluarId($pengawasTerdaftarRelatedByJenisKeluarId);
        }

        $this->collPengawasTerdaftarsRelatedByJenisKeluarId = $pengawasTerdaftarsRelatedByJenisKeluarId;
        $this->collPengawasTerdaftarsRelatedByJenisKeluarIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PengawasTerdaftar objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related PengawasTerdaftar objects.
     * @throws PropelException
     */
    public function countPengawasTerdaftarsRelatedByJenisKeluarId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPengawasTerdaftarsRelatedByJenisKeluarIdPartial && !$this->isNew();
        if (null === $this->collPengawasTerdaftarsRelatedByJenisKeluarId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPengawasTerdaftarsRelatedByJenisKeluarId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPengawasTerdaftarsRelatedByJenisKeluarId());
            }
            $query = PengawasTerdaftarQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByJenisKeluarRelatedByJenisKeluarId($this)
                ->count($con);
        }

        return count($this->collPengawasTerdaftarsRelatedByJenisKeluarId);
    }

    /**
     * Method called to associate a PengawasTerdaftar object to this object
     * through the PengawasTerdaftar foreign key attribute.
     *
     * @param    PengawasTerdaftar $l PengawasTerdaftar
     * @return JenisKeluar The current object (for fluent API support)
     */
    public function addPengawasTerdaftarRelatedByJenisKeluarId(PengawasTerdaftar $l)
    {
        if ($this->collPengawasTerdaftarsRelatedByJenisKeluarId === null) {
            $this->initPengawasTerdaftarsRelatedByJenisKeluarId();
            $this->collPengawasTerdaftarsRelatedByJenisKeluarIdPartial = true;
        }
        if (!in_array($l, $this->collPengawasTerdaftarsRelatedByJenisKeluarId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPengawasTerdaftarRelatedByJenisKeluarId($l);
        }

        return $this;
    }

    /**
     * @param	PengawasTerdaftarRelatedByJenisKeluarId $pengawasTerdaftarRelatedByJenisKeluarId The pengawasTerdaftarRelatedByJenisKeluarId object to add.
     */
    protected function doAddPengawasTerdaftarRelatedByJenisKeluarId($pengawasTerdaftarRelatedByJenisKeluarId)
    {
        $this->collPengawasTerdaftarsRelatedByJenisKeluarId[]= $pengawasTerdaftarRelatedByJenisKeluarId;
        $pengawasTerdaftarRelatedByJenisKeluarId->setJenisKeluarRelatedByJenisKeluarId($this);
    }

    /**
     * @param	PengawasTerdaftarRelatedByJenisKeluarId $pengawasTerdaftarRelatedByJenisKeluarId The pengawasTerdaftarRelatedByJenisKeluarId object to remove.
     * @return JenisKeluar The current object (for fluent API support)
     */
    public function removePengawasTerdaftarRelatedByJenisKeluarId($pengawasTerdaftarRelatedByJenisKeluarId)
    {
        if ($this->getPengawasTerdaftarsRelatedByJenisKeluarId()->contains($pengawasTerdaftarRelatedByJenisKeluarId)) {
            $this->collPengawasTerdaftarsRelatedByJenisKeluarId->remove($this->collPengawasTerdaftarsRelatedByJenisKeluarId->search($pengawasTerdaftarRelatedByJenisKeluarId));
            if (null === $this->pengawasTerdaftarsRelatedByJenisKeluarIdScheduledForDeletion) {
                $this->pengawasTerdaftarsRelatedByJenisKeluarIdScheduledForDeletion = clone $this->collPengawasTerdaftarsRelatedByJenisKeluarId;
                $this->pengawasTerdaftarsRelatedByJenisKeluarIdScheduledForDeletion->clear();
            }
            $this->pengawasTerdaftarsRelatedByJenisKeluarIdScheduledForDeletion[]= $pengawasTerdaftarRelatedByJenisKeluarId;
            $pengawasTerdaftarRelatedByJenisKeluarId->setJenisKeluarRelatedByJenisKeluarId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByJenisKeluarIdJoinLembagaNonSekolahRelatedByLembagaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('LembagaNonSekolahRelatedByLembagaId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByJenisKeluarIdJoinLembagaNonSekolahRelatedByLembagaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('LembagaNonSekolahRelatedByLembagaId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByJenisKeluarIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByJenisKeluarIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByJenisKeluarIdJoinBidangStudiRelatedByBidangStudiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('BidangStudiRelatedByBidangStudiId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByJenisKeluarIdJoinBidangStudiRelatedByBidangStudiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('BidangStudiRelatedByBidangStudiId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByJenisKeluarIdJoinJenjangKepengawasanRelatedByJenjangKepengawasanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('JenjangKepengawasanRelatedByJenjangKepengawasanId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByJenisKeluarIdJoinJenjangKepengawasanRelatedByJenjangKepengawasanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('JenjangKepengawasanRelatedByJenjangKepengawasanId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByJenisKeluarIdJoinMataPelajaranRelatedByMataPelajaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('MataPelajaranRelatedByMataPelajaranId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByJenisKeluarIdJoinMataPelajaranRelatedByMataPelajaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('MataPelajaranRelatedByMataPelajaranId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByJenisKeluarIdJoinTahunAjaranRelatedByTahunAjaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('TahunAjaranRelatedByTahunAjaranId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByJenisKeluarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisKeluar is new, it will return
     * an empty collection; or if this JenisKeluar has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByJenisKeluarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisKeluar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByJenisKeluarIdJoinTahunAjaranRelatedByTahunAjaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('TahunAjaranRelatedByTahunAjaranId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByJenisKeluarId($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->jenis_keluar_id = null;
        $this->ket_keluar = null;
        $this->keluar_pd = null;
        $this->keluar_ptk = null;
        $this->create_date = null;
        $this->last_update = null;
        $this->expired_date = null;
        $this->last_sync = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId) {
                foreach ($this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId) {
                foreach ($this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPtkTerdaftarsRelatedByJenisKeluarId) {
                foreach ($this->collPtkTerdaftarsRelatedByJenisKeluarId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPtkTerdaftarsRelatedByJenisKeluarId) {
                foreach ($this->collPtkTerdaftarsRelatedByJenisKeluarId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPengawasTerdaftarsRelatedByJenisKeluarId) {
                foreach ($this->collPengawasTerdaftarsRelatedByJenisKeluarId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPengawasTerdaftarsRelatedByJenisKeluarId) {
                foreach ($this->collPengawasTerdaftarsRelatedByJenisKeluarId as $o) {
                    $o->clearAllReferences($deep);
                }
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId instanceof PropelCollection) {
            $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId->clearIterator();
        }
        $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId = null;
        if ($this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId instanceof PropelCollection) {
            $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId->clearIterator();
        }
        $this->collRegistrasiPesertaDidiksRelatedByJenisKeluarId = null;
        if ($this->collPtkTerdaftarsRelatedByJenisKeluarId instanceof PropelCollection) {
            $this->collPtkTerdaftarsRelatedByJenisKeluarId->clearIterator();
        }
        $this->collPtkTerdaftarsRelatedByJenisKeluarId = null;
        if ($this->collPtkTerdaftarsRelatedByJenisKeluarId instanceof PropelCollection) {
            $this->collPtkTerdaftarsRelatedByJenisKeluarId->clearIterator();
        }
        $this->collPtkTerdaftarsRelatedByJenisKeluarId = null;
        if ($this->collPengawasTerdaftarsRelatedByJenisKeluarId instanceof PropelCollection) {
            $this->collPengawasTerdaftarsRelatedByJenisKeluarId->clearIterator();
        }
        $this->collPengawasTerdaftarsRelatedByJenisKeluarId = null;
        if ($this->collPengawasTerdaftarsRelatedByJenisKeluarId instanceof PropelCollection) {
            $this->collPengawasTerdaftarsRelatedByJenisKeluarId->clearIterator();
        }
        $this->collPengawasTerdaftarsRelatedByJenisKeluarId = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(JenisKeluarPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
