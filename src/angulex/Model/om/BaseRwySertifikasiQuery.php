<?php

namespace angulex\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\BidangStudi;
use angulex\Model\JenisSertifikasi;
use angulex\Model\Ptk;
use angulex\Model\RwySertifikasi;
use angulex\Model\RwySertifikasiPeer;
use angulex\Model\RwySertifikasiQuery;
use angulex\Model\VldRwySertifikasi;

/**
 * Base class that represents a query for the 'rwy_sertifikasi' table.
 *
 * 
 *
 * @method RwySertifikasiQuery orderByRiwayatSertifikasiId($order = Criteria::ASC) Order by the riwayat_sertifikasi_id column
 * @method RwySertifikasiQuery orderByPtkId($order = Criteria::ASC) Order by the ptk_id column
 * @method RwySertifikasiQuery orderByBidangStudiId($order = Criteria::ASC) Order by the bidang_studi_id column
 * @method RwySertifikasiQuery orderByIdJenisSertifikasi($order = Criteria::ASC) Order by the id_jenis_sertifikasi column
 * @method RwySertifikasiQuery orderByTahunSertifikasi($order = Criteria::ASC) Order by the tahun_sertifikasi column
 * @method RwySertifikasiQuery orderByNomorSertifikat($order = Criteria::ASC) Order by the nomor_sertifikat column
 * @method RwySertifikasiQuery orderByNrg($order = Criteria::ASC) Order by the nrg column
 * @method RwySertifikasiQuery orderByNomorPeserta($order = Criteria::ASC) Order by the nomor_peserta column
 * @method RwySertifikasiQuery orderByLastUpdate($order = Criteria::ASC) Order by the Last_update column
 * @method RwySertifikasiQuery orderBySoftDelete($order = Criteria::ASC) Order by the Soft_delete column
 * @method RwySertifikasiQuery orderByLastSync($order = Criteria::ASC) Order by the last_sync column
 * @method RwySertifikasiQuery orderByUpdaterId($order = Criteria::ASC) Order by the Updater_ID column
 *
 * @method RwySertifikasiQuery groupByRiwayatSertifikasiId() Group by the riwayat_sertifikasi_id column
 * @method RwySertifikasiQuery groupByPtkId() Group by the ptk_id column
 * @method RwySertifikasiQuery groupByBidangStudiId() Group by the bidang_studi_id column
 * @method RwySertifikasiQuery groupByIdJenisSertifikasi() Group by the id_jenis_sertifikasi column
 * @method RwySertifikasiQuery groupByTahunSertifikasi() Group by the tahun_sertifikasi column
 * @method RwySertifikasiQuery groupByNomorSertifikat() Group by the nomor_sertifikat column
 * @method RwySertifikasiQuery groupByNrg() Group by the nrg column
 * @method RwySertifikasiQuery groupByNomorPeserta() Group by the nomor_peserta column
 * @method RwySertifikasiQuery groupByLastUpdate() Group by the Last_update column
 * @method RwySertifikasiQuery groupBySoftDelete() Group by the Soft_delete column
 * @method RwySertifikasiQuery groupByLastSync() Group by the last_sync column
 * @method RwySertifikasiQuery groupByUpdaterId() Group by the Updater_ID column
 *
 * @method RwySertifikasiQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method RwySertifikasiQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method RwySertifikasiQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method RwySertifikasiQuery leftJoinPtkRelatedByPtkId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PtkRelatedByPtkId relation
 * @method RwySertifikasiQuery rightJoinPtkRelatedByPtkId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PtkRelatedByPtkId relation
 * @method RwySertifikasiQuery innerJoinPtkRelatedByPtkId($relationAlias = null) Adds a INNER JOIN clause to the query using the PtkRelatedByPtkId relation
 *
 * @method RwySertifikasiQuery leftJoinPtkRelatedByPtkId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PtkRelatedByPtkId relation
 * @method RwySertifikasiQuery rightJoinPtkRelatedByPtkId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PtkRelatedByPtkId relation
 * @method RwySertifikasiQuery innerJoinPtkRelatedByPtkId($relationAlias = null) Adds a INNER JOIN clause to the query using the PtkRelatedByPtkId relation
 *
 * @method RwySertifikasiQuery leftJoinBidangStudiRelatedByBidangStudiId($relationAlias = null) Adds a LEFT JOIN clause to the query using the BidangStudiRelatedByBidangStudiId relation
 * @method RwySertifikasiQuery rightJoinBidangStudiRelatedByBidangStudiId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BidangStudiRelatedByBidangStudiId relation
 * @method RwySertifikasiQuery innerJoinBidangStudiRelatedByBidangStudiId($relationAlias = null) Adds a INNER JOIN clause to the query using the BidangStudiRelatedByBidangStudiId relation
 *
 * @method RwySertifikasiQuery leftJoinBidangStudiRelatedByBidangStudiId($relationAlias = null) Adds a LEFT JOIN clause to the query using the BidangStudiRelatedByBidangStudiId relation
 * @method RwySertifikasiQuery rightJoinBidangStudiRelatedByBidangStudiId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BidangStudiRelatedByBidangStudiId relation
 * @method RwySertifikasiQuery innerJoinBidangStudiRelatedByBidangStudiId($relationAlias = null) Adds a INNER JOIN clause to the query using the BidangStudiRelatedByBidangStudiId relation
 *
 * @method RwySertifikasiQuery leftJoinJenisSertifikasiRelatedByIdJenisSertifikasi($relationAlias = null) Adds a LEFT JOIN clause to the query using the JenisSertifikasiRelatedByIdJenisSertifikasi relation
 * @method RwySertifikasiQuery rightJoinJenisSertifikasiRelatedByIdJenisSertifikasi($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JenisSertifikasiRelatedByIdJenisSertifikasi relation
 * @method RwySertifikasiQuery innerJoinJenisSertifikasiRelatedByIdJenisSertifikasi($relationAlias = null) Adds a INNER JOIN clause to the query using the JenisSertifikasiRelatedByIdJenisSertifikasi relation
 *
 * @method RwySertifikasiQuery leftJoinJenisSertifikasiRelatedByIdJenisSertifikasi($relationAlias = null) Adds a LEFT JOIN clause to the query using the JenisSertifikasiRelatedByIdJenisSertifikasi relation
 * @method RwySertifikasiQuery rightJoinJenisSertifikasiRelatedByIdJenisSertifikasi($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JenisSertifikasiRelatedByIdJenisSertifikasi relation
 * @method RwySertifikasiQuery innerJoinJenisSertifikasiRelatedByIdJenisSertifikasi($relationAlias = null) Adds a INNER JOIN clause to the query using the JenisSertifikasiRelatedByIdJenisSertifikasi relation
 *
 * @method RwySertifikasiQuery leftJoinVldRwySertifikasiRelatedByRiwayatSertifikasiId($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldRwySertifikasiRelatedByRiwayatSertifikasiId relation
 * @method RwySertifikasiQuery rightJoinVldRwySertifikasiRelatedByRiwayatSertifikasiId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldRwySertifikasiRelatedByRiwayatSertifikasiId relation
 * @method RwySertifikasiQuery innerJoinVldRwySertifikasiRelatedByRiwayatSertifikasiId($relationAlias = null) Adds a INNER JOIN clause to the query using the VldRwySertifikasiRelatedByRiwayatSertifikasiId relation
 *
 * @method RwySertifikasiQuery leftJoinVldRwySertifikasiRelatedByRiwayatSertifikasiId($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldRwySertifikasiRelatedByRiwayatSertifikasiId relation
 * @method RwySertifikasiQuery rightJoinVldRwySertifikasiRelatedByRiwayatSertifikasiId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldRwySertifikasiRelatedByRiwayatSertifikasiId relation
 * @method RwySertifikasiQuery innerJoinVldRwySertifikasiRelatedByRiwayatSertifikasiId($relationAlias = null) Adds a INNER JOIN clause to the query using the VldRwySertifikasiRelatedByRiwayatSertifikasiId relation
 *
 * @method RwySertifikasi findOne(PropelPDO $con = null) Return the first RwySertifikasi matching the query
 * @method RwySertifikasi findOneOrCreate(PropelPDO $con = null) Return the first RwySertifikasi matching the query, or a new RwySertifikasi object populated from the query conditions when no match is found
 *
 * @method RwySertifikasi findOneByPtkId(string $ptk_id) Return the first RwySertifikasi filtered by the ptk_id column
 * @method RwySertifikasi findOneByBidangStudiId(int $bidang_studi_id) Return the first RwySertifikasi filtered by the bidang_studi_id column
 * @method RwySertifikasi findOneByIdJenisSertifikasi(string $id_jenis_sertifikasi) Return the first RwySertifikasi filtered by the id_jenis_sertifikasi column
 * @method RwySertifikasi findOneByTahunSertifikasi(string $tahun_sertifikasi) Return the first RwySertifikasi filtered by the tahun_sertifikasi column
 * @method RwySertifikasi findOneByNomorSertifikat(string $nomor_sertifikat) Return the first RwySertifikasi filtered by the nomor_sertifikat column
 * @method RwySertifikasi findOneByNrg(string $nrg) Return the first RwySertifikasi filtered by the nrg column
 * @method RwySertifikasi findOneByNomorPeserta(string $nomor_peserta) Return the first RwySertifikasi filtered by the nomor_peserta column
 * @method RwySertifikasi findOneByLastUpdate(string $Last_update) Return the first RwySertifikasi filtered by the Last_update column
 * @method RwySertifikasi findOneBySoftDelete(string $Soft_delete) Return the first RwySertifikasi filtered by the Soft_delete column
 * @method RwySertifikasi findOneByLastSync(string $last_sync) Return the first RwySertifikasi filtered by the last_sync column
 * @method RwySertifikasi findOneByUpdaterId(string $Updater_ID) Return the first RwySertifikasi filtered by the Updater_ID column
 *
 * @method array findByRiwayatSertifikasiId(string $riwayat_sertifikasi_id) Return RwySertifikasi objects filtered by the riwayat_sertifikasi_id column
 * @method array findByPtkId(string $ptk_id) Return RwySertifikasi objects filtered by the ptk_id column
 * @method array findByBidangStudiId(int $bidang_studi_id) Return RwySertifikasi objects filtered by the bidang_studi_id column
 * @method array findByIdJenisSertifikasi(string $id_jenis_sertifikasi) Return RwySertifikasi objects filtered by the id_jenis_sertifikasi column
 * @method array findByTahunSertifikasi(string $tahun_sertifikasi) Return RwySertifikasi objects filtered by the tahun_sertifikasi column
 * @method array findByNomorSertifikat(string $nomor_sertifikat) Return RwySertifikasi objects filtered by the nomor_sertifikat column
 * @method array findByNrg(string $nrg) Return RwySertifikasi objects filtered by the nrg column
 * @method array findByNomorPeserta(string $nomor_peserta) Return RwySertifikasi objects filtered by the nomor_peserta column
 * @method array findByLastUpdate(string $Last_update) Return RwySertifikasi objects filtered by the Last_update column
 * @method array findBySoftDelete(string $Soft_delete) Return RwySertifikasi objects filtered by the Soft_delete column
 * @method array findByLastSync(string $last_sync) Return RwySertifikasi objects filtered by the last_sync column
 * @method array findByUpdaterId(string $Updater_ID) Return RwySertifikasi objects filtered by the Updater_ID column
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseRwySertifikasiQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseRwySertifikasiQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Dapodikmen', $modelName = 'angulex\\Model\\RwySertifikasi', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new RwySertifikasiQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   RwySertifikasiQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return RwySertifikasiQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof RwySertifikasiQuery) {
            return $criteria;
        }
        $query = new RwySertifikasiQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   RwySertifikasi|RwySertifikasi[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = RwySertifikasiPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(RwySertifikasiPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 RwySertifikasi A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByRiwayatSertifikasiId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 RwySertifikasi A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [riwayat_sertifikasi_id], [ptk_id], [bidang_studi_id], [id_jenis_sertifikasi], [tahun_sertifikasi], [nomor_sertifikat], [nrg], [nomor_peserta], [Last_update], [Soft_delete], [last_sync], [Updater_ID] FROM [rwy_sertifikasi] WHERE [riwayat_sertifikasi_id] = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new RwySertifikasi();
            $obj->hydrate($row);
            RwySertifikasiPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return RwySertifikasi|RwySertifikasi[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|RwySertifikasi[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return RwySertifikasiQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(RwySertifikasiPeer::RIWAYAT_SERTIFIKASI_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return RwySertifikasiQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(RwySertifikasiPeer::RIWAYAT_SERTIFIKASI_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the riwayat_sertifikasi_id column
     *
     * Example usage:
     * <code>
     * $query->filterByRiwayatSertifikasiId('fooValue');   // WHERE riwayat_sertifikasi_id = 'fooValue'
     * $query->filterByRiwayatSertifikasiId('%fooValue%'); // WHERE riwayat_sertifikasi_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $riwayatSertifikasiId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwySertifikasiQuery The current query, for fluid interface
     */
    public function filterByRiwayatSertifikasiId($riwayatSertifikasiId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($riwayatSertifikasiId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $riwayatSertifikasiId)) {
                $riwayatSertifikasiId = str_replace('*', '%', $riwayatSertifikasiId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RwySertifikasiPeer::RIWAYAT_SERTIFIKASI_ID, $riwayatSertifikasiId, $comparison);
    }

    /**
     * Filter the query on the ptk_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPtkId('fooValue');   // WHERE ptk_id = 'fooValue'
     * $query->filterByPtkId('%fooValue%'); // WHERE ptk_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ptkId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwySertifikasiQuery The current query, for fluid interface
     */
    public function filterByPtkId($ptkId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ptkId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ptkId)) {
                $ptkId = str_replace('*', '%', $ptkId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RwySertifikasiPeer::PTK_ID, $ptkId, $comparison);
    }

    /**
     * Filter the query on the bidang_studi_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBidangStudiId(1234); // WHERE bidang_studi_id = 1234
     * $query->filterByBidangStudiId(array(12, 34)); // WHERE bidang_studi_id IN (12, 34)
     * $query->filterByBidangStudiId(array('min' => 12)); // WHERE bidang_studi_id >= 12
     * $query->filterByBidangStudiId(array('max' => 12)); // WHERE bidang_studi_id <= 12
     * </code>
     *
     * @see       filterByBidangStudiRelatedByBidangStudiId()
     *
     * @see       filterByBidangStudiRelatedByBidangStudiId()
     *
     * @param     mixed $bidangStudiId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwySertifikasiQuery The current query, for fluid interface
     */
    public function filterByBidangStudiId($bidangStudiId = null, $comparison = null)
    {
        if (is_array($bidangStudiId)) {
            $useMinMax = false;
            if (isset($bidangStudiId['min'])) {
                $this->addUsingAlias(RwySertifikasiPeer::BIDANG_STUDI_ID, $bidangStudiId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($bidangStudiId['max'])) {
                $this->addUsingAlias(RwySertifikasiPeer::BIDANG_STUDI_ID, $bidangStudiId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RwySertifikasiPeer::BIDANG_STUDI_ID, $bidangStudiId, $comparison);
    }

    /**
     * Filter the query on the id_jenis_sertifikasi column
     *
     * Example usage:
     * <code>
     * $query->filterByIdJenisSertifikasi(1234); // WHERE id_jenis_sertifikasi = 1234
     * $query->filterByIdJenisSertifikasi(array(12, 34)); // WHERE id_jenis_sertifikasi IN (12, 34)
     * $query->filterByIdJenisSertifikasi(array('min' => 12)); // WHERE id_jenis_sertifikasi >= 12
     * $query->filterByIdJenisSertifikasi(array('max' => 12)); // WHERE id_jenis_sertifikasi <= 12
     * </code>
     *
     * @see       filterByJenisSertifikasiRelatedByIdJenisSertifikasi()
     *
     * @see       filterByJenisSertifikasiRelatedByIdJenisSertifikasi()
     *
     * @param     mixed $idJenisSertifikasi The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwySertifikasiQuery The current query, for fluid interface
     */
    public function filterByIdJenisSertifikasi($idJenisSertifikasi = null, $comparison = null)
    {
        if (is_array($idJenisSertifikasi)) {
            $useMinMax = false;
            if (isset($idJenisSertifikasi['min'])) {
                $this->addUsingAlias(RwySertifikasiPeer::ID_JENIS_SERTIFIKASI, $idJenisSertifikasi['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idJenisSertifikasi['max'])) {
                $this->addUsingAlias(RwySertifikasiPeer::ID_JENIS_SERTIFIKASI, $idJenisSertifikasi['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RwySertifikasiPeer::ID_JENIS_SERTIFIKASI, $idJenisSertifikasi, $comparison);
    }

    /**
     * Filter the query on the tahun_sertifikasi column
     *
     * Example usage:
     * <code>
     * $query->filterByTahunSertifikasi(1234); // WHERE tahun_sertifikasi = 1234
     * $query->filterByTahunSertifikasi(array(12, 34)); // WHERE tahun_sertifikasi IN (12, 34)
     * $query->filterByTahunSertifikasi(array('min' => 12)); // WHERE tahun_sertifikasi >= 12
     * $query->filterByTahunSertifikasi(array('max' => 12)); // WHERE tahun_sertifikasi <= 12
     * </code>
     *
     * @param     mixed $tahunSertifikasi The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwySertifikasiQuery The current query, for fluid interface
     */
    public function filterByTahunSertifikasi($tahunSertifikasi = null, $comparison = null)
    {
        if (is_array($tahunSertifikasi)) {
            $useMinMax = false;
            if (isset($tahunSertifikasi['min'])) {
                $this->addUsingAlias(RwySertifikasiPeer::TAHUN_SERTIFIKASI, $tahunSertifikasi['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tahunSertifikasi['max'])) {
                $this->addUsingAlias(RwySertifikasiPeer::TAHUN_SERTIFIKASI, $tahunSertifikasi['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RwySertifikasiPeer::TAHUN_SERTIFIKASI, $tahunSertifikasi, $comparison);
    }

    /**
     * Filter the query on the nomor_sertifikat column
     *
     * Example usage:
     * <code>
     * $query->filterByNomorSertifikat('fooValue');   // WHERE nomor_sertifikat = 'fooValue'
     * $query->filterByNomorSertifikat('%fooValue%'); // WHERE nomor_sertifikat LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomorSertifikat The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwySertifikasiQuery The current query, for fluid interface
     */
    public function filterByNomorSertifikat($nomorSertifikat = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomorSertifikat)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nomorSertifikat)) {
                $nomorSertifikat = str_replace('*', '%', $nomorSertifikat);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RwySertifikasiPeer::NOMOR_SERTIFIKAT, $nomorSertifikat, $comparison);
    }

    /**
     * Filter the query on the nrg column
     *
     * Example usage:
     * <code>
     * $query->filterByNrg('fooValue');   // WHERE nrg = 'fooValue'
     * $query->filterByNrg('%fooValue%'); // WHERE nrg LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nrg The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwySertifikasiQuery The current query, for fluid interface
     */
    public function filterByNrg($nrg = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nrg)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nrg)) {
                $nrg = str_replace('*', '%', $nrg);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RwySertifikasiPeer::NRG, $nrg, $comparison);
    }

    /**
     * Filter the query on the nomor_peserta column
     *
     * Example usage:
     * <code>
     * $query->filterByNomorPeserta('fooValue');   // WHERE nomor_peserta = 'fooValue'
     * $query->filterByNomorPeserta('%fooValue%'); // WHERE nomor_peserta LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomorPeserta The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwySertifikasiQuery The current query, for fluid interface
     */
    public function filterByNomorPeserta($nomorPeserta = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomorPeserta)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nomorPeserta)) {
                $nomorPeserta = str_replace('*', '%', $nomorPeserta);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RwySertifikasiPeer::NOMOR_PESERTA, $nomorPeserta, $comparison);
    }

    /**
     * Filter the query on the Last_update column
     *
     * Example usage:
     * <code>
     * $query->filterByLastUpdate('2011-03-14'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate('now'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate(array('max' => 'yesterday')); // WHERE Last_update > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastUpdate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwySertifikasiQuery The current query, for fluid interface
     */
    public function filterByLastUpdate($lastUpdate = null, $comparison = null)
    {
        if (is_array($lastUpdate)) {
            $useMinMax = false;
            if (isset($lastUpdate['min'])) {
                $this->addUsingAlias(RwySertifikasiPeer::LAST_UPDATE, $lastUpdate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastUpdate['max'])) {
                $this->addUsingAlias(RwySertifikasiPeer::LAST_UPDATE, $lastUpdate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RwySertifikasiPeer::LAST_UPDATE, $lastUpdate, $comparison);
    }

    /**
     * Filter the query on the Soft_delete column
     *
     * Example usage:
     * <code>
     * $query->filterBySoftDelete(1234); // WHERE Soft_delete = 1234
     * $query->filterBySoftDelete(array(12, 34)); // WHERE Soft_delete IN (12, 34)
     * $query->filterBySoftDelete(array('min' => 12)); // WHERE Soft_delete >= 12
     * $query->filterBySoftDelete(array('max' => 12)); // WHERE Soft_delete <= 12
     * </code>
     *
     * @param     mixed $softDelete The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwySertifikasiQuery The current query, for fluid interface
     */
    public function filterBySoftDelete($softDelete = null, $comparison = null)
    {
        if (is_array($softDelete)) {
            $useMinMax = false;
            if (isset($softDelete['min'])) {
                $this->addUsingAlias(RwySertifikasiPeer::SOFT_DELETE, $softDelete['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($softDelete['max'])) {
                $this->addUsingAlias(RwySertifikasiPeer::SOFT_DELETE, $softDelete['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RwySertifikasiPeer::SOFT_DELETE, $softDelete, $comparison);
    }

    /**
     * Filter the query on the last_sync column
     *
     * Example usage:
     * <code>
     * $query->filterByLastSync('2011-03-14'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync('now'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync(array('max' => 'yesterday')); // WHERE last_sync > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastSync The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwySertifikasiQuery The current query, for fluid interface
     */
    public function filterByLastSync($lastSync = null, $comparison = null)
    {
        if (is_array($lastSync)) {
            $useMinMax = false;
            if (isset($lastSync['min'])) {
                $this->addUsingAlias(RwySertifikasiPeer::LAST_SYNC, $lastSync['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastSync['max'])) {
                $this->addUsingAlias(RwySertifikasiPeer::LAST_SYNC, $lastSync['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RwySertifikasiPeer::LAST_SYNC, $lastSync, $comparison);
    }

    /**
     * Filter the query on the Updater_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdaterId('fooValue');   // WHERE Updater_ID = 'fooValue'
     * $query->filterByUpdaterId('%fooValue%'); // WHERE Updater_ID LIKE '%fooValue%'
     * </code>
     *
     * @param     string $updaterId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwySertifikasiQuery The current query, for fluid interface
     */
    public function filterByUpdaterId($updaterId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($updaterId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $updaterId)) {
                $updaterId = str_replace('*', '%', $updaterId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RwySertifikasiPeer::UPDATER_ID, $updaterId, $comparison);
    }

    /**
     * Filter the query by a related Ptk object
     *
     * @param   Ptk|PropelObjectCollection $ptk The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RwySertifikasiQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPtkRelatedByPtkId($ptk, $comparison = null)
    {
        if ($ptk instanceof Ptk) {
            return $this
                ->addUsingAlias(RwySertifikasiPeer::PTK_ID, $ptk->getPtkId(), $comparison);
        } elseif ($ptk instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RwySertifikasiPeer::PTK_ID, $ptk->toKeyValue('PrimaryKey', 'PtkId'), $comparison);
        } else {
            throw new PropelException('filterByPtkRelatedByPtkId() only accepts arguments of type Ptk or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PtkRelatedByPtkId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RwySertifikasiQuery The current query, for fluid interface
     */
    public function joinPtkRelatedByPtkId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PtkRelatedByPtkId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PtkRelatedByPtkId');
        }

        return $this;
    }

    /**
     * Use the PtkRelatedByPtkId relation Ptk object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PtkQuery A secondary query class using the current class as primary query
     */
    public function usePtkRelatedByPtkIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPtkRelatedByPtkId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PtkRelatedByPtkId', '\angulex\Model\PtkQuery');
    }

    /**
     * Filter the query by a related Ptk object
     *
     * @param   Ptk|PropelObjectCollection $ptk The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RwySertifikasiQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPtkRelatedByPtkId($ptk, $comparison = null)
    {
        if ($ptk instanceof Ptk) {
            return $this
                ->addUsingAlias(RwySertifikasiPeer::PTK_ID, $ptk->getPtkId(), $comparison);
        } elseif ($ptk instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RwySertifikasiPeer::PTK_ID, $ptk->toKeyValue('PrimaryKey', 'PtkId'), $comparison);
        } else {
            throw new PropelException('filterByPtkRelatedByPtkId() only accepts arguments of type Ptk or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PtkRelatedByPtkId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RwySertifikasiQuery The current query, for fluid interface
     */
    public function joinPtkRelatedByPtkId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PtkRelatedByPtkId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PtkRelatedByPtkId');
        }

        return $this;
    }

    /**
     * Use the PtkRelatedByPtkId relation Ptk object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PtkQuery A secondary query class using the current class as primary query
     */
    public function usePtkRelatedByPtkIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPtkRelatedByPtkId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PtkRelatedByPtkId', '\angulex\Model\PtkQuery');
    }

    /**
     * Filter the query by a related BidangStudi object
     *
     * @param   BidangStudi|PropelObjectCollection $bidangStudi The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RwySertifikasiQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByBidangStudiRelatedByBidangStudiId($bidangStudi, $comparison = null)
    {
        if ($bidangStudi instanceof BidangStudi) {
            return $this
                ->addUsingAlias(RwySertifikasiPeer::BIDANG_STUDI_ID, $bidangStudi->getBidangStudiId(), $comparison);
        } elseif ($bidangStudi instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RwySertifikasiPeer::BIDANG_STUDI_ID, $bidangStudi->toKeyValue('PrimaryKey', 'BidangStudiId'), $comparison);
        } else {
            throw new PropelException('filterByBidangStudiRelatedByBidangStudiId() only accepts arguments of type BidangStudi or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BidangStudiRelatedByBidangStudiId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RwySertifikasiQuery The current query, for fluid interface
     */
    public function joinBidangStudiRelatedByBidangStudiId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BidangStudiRelatedByBidangStudiId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BidangStudiRelatedByBidangStudiId');
        }

        return $this;
    }

    /**
     * Use the BidangStudiRelatedByBidangStudiId relation BidangStudi object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\BidangStudiQuery A secondary query class using the current class as primary query
     */
    public function useBidangStudiRelatedByBidangStudiIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBidangStudiRelatedByBidangStudiId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BidangStudiRelatedByBidangStudiId', '\angulex\Model\BidangStudiQuery');
    }

    /**
     * Filter the query by a related BidangStudi object
     *
     * @param   BidangStudi|PropelObjectCollection $bidangStudi The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RwySertifikasiQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByBidangStudiRelatedByBidangStudiId($bidangStudi, $comparison = null)
    {
        if ($bidangStudi instanceof BidangStudi) {
            return $this
                ->addUsingAlias(RwySertifikasiPeer::BIDANG_STUDI_ID, $bidangStudi->getBidangStudiId(), $comparison);
        } elseif ($bidangStudi instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RwySertifikasiPeer::BIDANG_STUDI_ID, $bidangStudi->toKeyValue('PrimaryKey', 'BidangStudiId'), $comparison);
        } else {
            throw new PropelException('filterByBidangStudiRelatedByBidangStudiId() only accepts arguments of type BidangStudi or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BidangStudiRelatedByBidangStudiId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RwySertifikasiQuery The current query, for fluid interface
     */
    public function joinBidangStudiRelatedByBidangStudiId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BidangStudiRelatedByBidangStudiId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BidangStudiRelatedByBidangStudiId');
        }

        return $this;
    }

    /**
     * Use the BidangStudiRelatedByBidangStudiId relation BidangStudi object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\BidangStudiQuery A secondary query class using the current class as primary query
     */
    public function useBidangStudiRelatedByBidangStudiIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBidangStudiRelatedByBidangStudiId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BidangStudiRelatedByBidangStudiId', '\angulex\Model\BidangStudiQuery');
    }

    /**
     * Filter the query by a related JenisSertifikasi object
     *
     * @param   JenisSertifikasi|PropelObjectCollection $jenisSertifikasi The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RwySertifikasiQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJenisSertifikasiRelatedByIdJenisSertifikasi($jenisSertifikasi, $comparison = null)
    {
        if ($jenisSertifikasi instanceof JenisSertifikasi) {
            return $this
                ->addUsingAlias(RwySertifikasiPeer::ID_JENIS_SERTIFIKASI, $jenisSertifikasi->getIdJenisSertifikasi(), $comparison);
        } elseif ($jenisSertifikasi instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RwySertifikasiPeer::ID_JENIS_SERTIFIKASI, $jenisSertifikasi->toKeyValue('PrimaryKey', 'IdJenisSertifikasi'), $comparison);
        } else {
            throw new PropelException('filterByJenisSertifikasiRelatedByIdJenisSertifikasi() only accepts arguments of type JenisSertifikasi or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the JenisSertifikasiRelatedByIdJenisSertifikasi relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RwySertifikasiQuery The current query, for fluid interface
     */
    public function joinJenisSertifikasiRelatedByIdJenisSertifikasi($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('JenisSertifikasiRelatedByIdJenisSertifikasi');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'JenisSertifikasiRelatedByIdJenisSertifikasi');
        }

        return $this;
    }

    /**
     * Use the JenisSertifikasiRelatedByIdJenisSertifikasi relation JenisSertifikasi object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\JenisSertifikasiQuery A secondary query class using the current class as primary query
     */
    public function useJenisSertifikasiRelatedByIdJenisSertifikasiQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinJenisSertifikasiRelatedByIdJenisSertifikasi($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'JenisSertifikasiRelatedByIdJenisSertifikasi', '\angulex\Model\JenisSertifikasiQuery');
    }

    /**
     * Filter the query by a related JenisSertifikasi object
     *
     * @param   JenisSertifikasi|PropelObjectCollection $jenisSertifikasi The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RwySertifikasiQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJenisSertifikasiRelatedByIdJenisSertifikasi($jenisSertifikasi, $comparison = null)
    {
        if ($jenisSertifikasi instanceof JenisSertifikasi) {
            return $this
                ->addUsingAlias(RwySertifikasiPeer::ID_JENIS_SERTIFIKASI, $jenisSertifikasi->getIdJenisSertifikasi(), $comparison);
        } elseif ($jenisSertifikasi instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RwySertifikasiPeer::ID_JENIS_SERTIFIKASI, $jenisSertifikasi->toKeyValue('PrimaryKey', 'IdJenisSertifikasi'), $comparison);
        } else {
            throw new PropelException('filterByJenisSertifikasiRelatedByIdJenisSertifikasi() only accepts arguments of type JenisSertifikasi or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the JenisSertifikasiRelatedByIdJenisSertifikasi relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RwySertifikasiQuery The current query, for fluid interface
     */
    public function joinJenisSertifikasiRelatedByIdJenisSertifikasi($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('JenisSertifikasiRelatedByIdJenisSertifikasi');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'JenisSertifikasiRelatedByIdJenisSertifikasi');
        }

        return $this;
    }

    /**
     * Use the JenisSertifikasiRelatedByIdJenisSertifikasi relation JenisSertifikasi object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\JenisSertifikasiQuery A secondary query class using the current class as primary query
     */
    public function useJenisSertifikasiRelatedByIdJenisSertifikasiQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinJenisSertifikasiRelatedByIdJenisSertifikasi($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'JenisSertifikasiRelatedByIdJenisSertifikasi', '\angulex\Model\JenisSertifikasiQuery');
    }

    /**
     * Filter the query by a related VldRwySertifikasi object
     *
     * @param   VldRwySertifikasi|PropelObjectCollection $vldRwySertifikasi  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RwySertifikasiQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldRwySertifikasiRelatedByRiwayatSertifikasiId($vldRwySertifikasi, $comparison = null)
    {
        if ($vldRwySertifikasi instanceof VldRwySertifikasi) {
            return $this
                ->addUsingAlias(RwySertifikasiPeer::RIWAYAT_SERTIFIKASI_ID, $vldRwySertifikasi->getRiwayatSertifikasiId(), $comparison);
        } elseif ($vldRwySertifikasi instanceof PropelObjectCollection) {
            return $this
                ->useVldRwySertifikasiRelatedByRiwayatSertifikasiIdQuery()
                ->filterByPrimaryKeys($vldRwySertifikasi->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldRwySertifikasiRelatedByRiwayatSertifikasiId() only accepts arguments of type VldRwySertifikasi or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldRwySertifikasiRelatedByRiwayatSertifikasiId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RwySertifikasiQuery The current query, for fluid interface
     */
    public function joinVldRwySertifikasiRelatedByRiwayatSertifikasiId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldRwySertifikasiRelatedByRiwayatSertifikasiId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldRwySertifikasiRelatedByRiwayatSertifikasiId');
        }

        return $this;
    }

    /**
     * Use the VldRwySertifikasiRelatedByRiwayatSertifikasiId relation VldRwySertifikasi object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldRwySertifikasiQuery A secondary query class using the current class as primary query
     */
    public function useVldRwySertifikasiRelatedByRiwayatSertifikasiIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldRwySertifikasiRelatedByRiwayatSertifikasiId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldRwySertifikasiRelatedByRiwayatSertifikasiId', '\angulex\Model\VldRwySertifikasiQuery');
    }

    /**
     * Filter the query by a related VldRwySertifikasi object
     *
     * @param   VldRwySertifikasi|PropelObjectCollection $vldRwySertifikasi  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RwySertifikasiQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldRwySertifikasiRelatedByRiwayatSertifikasiId($vldRwySertifikasi, $comparison = null)
    {
        if ($vldRwySertifikasi instanceof VldRwySertifikasi) {
            return $this
                ->addUsingAlias(RwySertifikasiPeer::RIWAYAT_SERTIFIKASI_ID, $vldRwySertifikasi->getRiwayatSertifikasiId(), $comparison);
        } elseif ($vldRwySertifikasi instanceof PropelObjectCollection) {
            return $this
                ->useVldRwySertifikasiRelatedByRiwayatSertifikasiIdQuery()
                ->filterByPrimaryKeys($vldRwySertifikasi->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldRwySertifikasiRelatedByRiwayatSertifikasiId() only accepts arguments of type VldRwySertifikasi or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldRwySertifikasiRelatedByRiwayatSertifikasiId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RwySertifikasiQuery The current query, for fluid interface
     */
    public function joinVldRwySertifikasiRelatedByRiwayatSertifikasiId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldRwySertifikasiRelatedByRiwayatSertifikasiId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldRwySertifikasiRelatedByRiwayatSertifikasiId');
        }

        return $this;
    }

    /**
     * Use the VldRwySertifikasiRelatedByRiwayatSertifikasiId relation VldRwySertifikasi object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldRwySertifikasiQuery A secondary query class using the current class as primary query
     */
    public function useVldRwySertifikasiRelatedByRiwayatSertifikasiIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldRwySertifikasiRelatedByRiwayatSertifikasiId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldRwySertifikasiRelatedByRiwayatSertifikasiId', '\angulex\Model\VldRwySertifikasiQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   RwySertifikasi $rwySertifikasi Object to remove from the list of results
     *
     * @return RwySertifikasiQuery The current query, for fluid interface
     */
    public function prune($rwySertifikasi = null)
    {
        if ($rwySertifikasi) {
            $this->addUsingAlias(RwySertifikasiPeer::RIWAYAT_SERTIFIKASI_ID, $rwySertifikasi->getRiwayatSertifikasiId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
