<?php

namespace angulex\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\JenisTest;
use angulex\Model\NilaiTest;
use angulex\Model\NilaiTestPeer;
use angulex\Model\NilaiTestQuery;
use angulex\Model\Ptk;
use angulex\Model\VldNilaiTest;

/**
 * Base class that represents a query for the 'nilai_test' table.
 *
 * 
 *
 * @method NilaiTestQuery orderByNilaiTestId($order = Criteria::ASC) Order by the nilai_test_id column
 * @method NilaiTestQuery orderByJenisTestId($order = Criteria::ASC) Order by the jenis_test_id column
 * @method NilaiTestQuery orderByPtkId($order = Criteria::ASC) Order by the ptk_id column
 * @method NilaiTestQuery orderByNama($order = Criteria::ASC) Order by the nama column
 * @method NilaiTestQuery orderByPenyelenggara($order = Criteria::ASC) Order by the penyelenggara column
 * @method NilaiTestQuery orderByTahun($order = Criteria::ASC) Order by the tahun column
 * @method NilaiTestQuery orderBySkor($order = Criteria::ASC) Order by the skor column
 * @method NilaiTestQuery orderByLastUpdate($order = Criteria::ASC) Order by the Last_update column
 * @method NilaiTestQuery orderBySoftDelete($order = Criteria::ASC) Order by the Soft_delete column
 * @method NilaiTestQuery orderByLastSync($order = Criteria::ASC) Order by the last_sync column
 * @method NilaiTestQuery orderByUpdaterId($order = Criteria::ASC) Order by the Updater_ID column
 *
 * @method NilaiTestQuery groupByNilaiTestId() Group by the nilai_test_id column
 * @method NilaiTestQuery groupByJenisTestId() Group by the jenis_test_id column
 * @method NilaiTestQuery groupByPtkId() Group by the ptk_id column
 * @method NilaiTestQuery groupByNama() Group by the nama column
 * @method NilaiTestQuery groupByPenyelenggara() Group by the penyelenggara column
 * @method NilaiTestQuery groupByTahun() Group by the tahun column
 * @method NilaiTestQuery groupBySkor() Group by the skor column
 * @method NilaiTestQuery groupByLastUpdate() Group by the Last_update column
 * @method NilaiTestQuery groupBySoftDelete() Group by the Soft_delete column
 * @method NilaiTestQuery groupByLastSync() Group by the last_sync column
 * @method NilaiTestQuery groupByUpdaterId() Group by the Updater_ID column
 *
 * @method NilaiTestQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method NilaiTestQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method NilaiTestQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method NilaiTestQuery leftJoinPtkRelatedByPtkId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PtkRelatedByPtkId relation
 * @method NilaiTestQuery rightJoinPtkRelatedByPtkId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PtkRelatedByPtkId relation
 * @method NilaiTestQuery innerJoinPtkRelatedByPtkId($relationAlias = null) Adds a INNER JOIN clause to the query using the PtkRelatedByPtkId relation
 *
 * @method NilaiTestQuery leftJoinPtkRelatedByPtkId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PtkRelatedByPtkId relation
 * @method NilaiTestQuery rightJoinPtkRelatedByPtkId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PtkRelatedByPtkId relation
 * @method NilaiTestQuery innerJoinPtkRelatedByPtkId($relationAlias = null) Adds a INNER JOIN clause to the query using the PtkRelatedByPtkId relation
 *
 * @method NilaiTestQuery leftJoinJenisTestRelatedByJenisTestId($relationAlias = null) Adds a LEFT JOIN clause to the query using the JenisTestRelatedByJenisTestId relation
 * @method NilaiTestQuery rightJoinJenisTestRelatedByJenisTestId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JenisTestRelatedByJenisTestId relation
 * @method NilaiTestQuery innerJoinJenisTestRelatedByJenisTestId($relationAlias = null) Adds a INNER JOIN clause to the query using the JenisTestRelatedByJenisTestId relation
 *
 * @method NilaiTestQuery leftJoinJenisTestRelatedByJenisTestId($relationAlias = null) Adds a LEFT JOIN clause to the query using the JenisTestRelatedByJenisTestId relation
 * @method NilaiTestQuery rightJoinJenisTestRelatedByJenisTestId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JenisTestRelatedByJenisTestId relation
 * @method NilaiTestQuery innerJoinJenisTestRelatedByJenisTestId($relationAlias = null) Adds a INNER JOIN clause to the query using the JenisTestRelatedByJenisTestId relation
 *
 * @method NilaiTestQuery leftJoinVldNilaiTestRelatedByNilaiTestId($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldNilaiTestRelatedByNilaiTestId relation
 * @method NilaiTestQuery rightJoinVldNilaiTestRelatedByNilaiTestId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldNilaiTestRelatedByNilaiTestId relation
 * @method NilaiTestQuery innerJoinVldNilaiTestRelatedByNilaiTestId($relationAlias = null) Adds a INNER JOIN clause to the query using the VldNilaiTestRelatedByNilaiTestId relation
 *
 * @method NilaiTestQuery leftJoinVldNilaiTestRelatedByNilaiTestId($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldNilaiTestRelatedByNilaiTestId relation
 * @method NilaiTestQuery rightJoinVldNilaiTestRelatedByNilaiTestId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldNilaiTestRelatedByNilaiTestId relation
 * @method NilaiTestQuery innerJoinVldNilaiTestRelatedByNilaiTestId($relationAlias = null) Adds a INNER JOIN clause to the query using the VldNilaiTestRelatedByNilaiTestId relation
 *
 * @method NilaiTest findOne(PropelPDO $con = null) Return the first NilaiTest matching the query
 * @method NilaiTest findOneOrCreate(PropelPDO $con = null) Return the first NilaiTest matching the query, or a new NilaiTest object populated from the query conditions when no match is found
 *
 * @method NilaiTest findOneByJenisTestId(string $jenis_test_id) Return the first NilaiTest filtered by the jenis_test_id column
 * @method NilaiTest findOneByPtkId(string $ptk_id) Return the first NilaiTest filtered by the ptk_id column
 * @method NilaiTest findOneByNama(string $nama) Return the first NilaiTest filtered by the nama column
 * @method NilaiTest findOneByPenyelenggara(string $penyelenggara) Return the first NilaiTest filtered by the penyelenggara column
 * @method NilaiTest findOneByTahun(string $tahun) Return the first NilaiTest filtered by the tahun column
 * @method NilaiTest findOneBySkor(string $skor) Return the first NilaiTest filtered by the skor column
 * @method NilaiTest findOneByLastUpdate(string $Last_update) Return the first NilaiTest filtered by the Last_update column
 * @method NilaiTest findOneBySoftDelete(string $Soft_delete) Return the first NilaiTest filtered by the Soft_delete column
 * @method NilaiTest findOneByLastSync(string $last_sync) Return the first NilaiTest filtered by the last_sync column
 * @method NilaiTest findOneByUpdaterId(string $Updater_ID) Return the first NilaiTest filtered by the Updater_ID column
 *
 * @method array findByNilaiTestId(string $nilai_test_id) Return NilaiTest objects filtered by the nilai_test_id column
 * @method array findByJenisTestId(string $jenis_test_id) Return NilaiTest objects filtered by the jenis_test_id column
 * @method array findByPtkId(string $ptk_id) Return NilaiTest objects filtered by the ptk_id column
 * @method array findByNama(string $nama) Return NilaiTest objects filtered by the nama column
 * @method array findByPenyelenggara(string $penyelenggara) Return NilaiTest objects filtered by the penyelenggara column
 * @method array findByTahun(string $tahun) Return NilaiTest objects filtered by the tahun column
 * @method array findBySkor(string $skor) Return NilaiTest objects filtered by the skor column
 * @method array findByLastUpdate(string $Last_update) Return NilaiTest objects filtered by the Last_update column
 * @method array findBySoftDelete(string $Soft_delete) Return NilaiTest objects filtered by the Soft_delete column
 * @method array findByLastSync(string $last_sync) Return NilaiTest objects filtered by the last_sync column
 * @method array findByUpdaterId(string $Updater_ID) Return NilaiTest objects filtered by the Updater_ID column
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseNilaiTestQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseNilaiTestQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Dapodikmen', $modelName = 'angulex\\Model\\NilaiTest', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new NilaiTestQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   NilaiTestQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return NilaiTestQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof NilaiTestQuery) {
            return $criteria;
        }
        $query = new NilaiTestQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   NilaiTest|NilaiTest[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = NilaiTestPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(NilaiTestPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 NilaiTest A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByNilaiTestId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 NilaiTest A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [nilai_test_id], [jenis_test_id], [ptk_id], [nama], [penyelenggara], [tahun], [skor], [Last_update], [Soft_delete], [last_sync], [Updater_ID] FROM [nilai_test] WHERE [nilai_test_id] = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new NilaiTest();
            $obj->hydrate($row);
            NilaiTestPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return NilaiTest|NilaiTest[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|NilaiTest[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return NilaiTestQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(NilaiTestPeer::NILAI_TEST_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return NilaiTestQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(NilaiTestPeer::NILAI_TEST_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the nilai_test_id column
     *
     * Example usage:
     * <code>
     * $query->filterByNilaiTestId('fooValue');   // WHERE nilai_test_id = 'fooValue'
     * $query->filterByNilaiTestId('%fooValue%'); // WHERE nilai_test_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nilaiTestId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return NilaiTestQuery The current query, for fluid interface
     */
    public function filterByNilaiTestId($nilaiTestId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nilaiTestId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nilaiTestId)) {
                $nilaiTestId = str_replace('*', '%', $nilaiTestId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(NilaiTestPeer::NILAI_TEST_ID, $nilaiTestId, $comparison);
    }

    /**
     * Filter the query on the jenis_test_id column
     *
     * Example usage:
     * <code>
     * $query->filterByJenisTestId(1234); // WHERE jenis_test_id = 1234
     * $query->filterByJenisTestId(array(12, 34)); // WHERE jenis_test_id IN (12, 34)
     * $query->filterByJenisTestId(array('min' => 12)); // WHERE jenis_test_id >= 12
     * $query->filterByJenisTestId(array('max' => 12)); // WHERE jenis_test_id <= 12
     * </code>
     *
     * @see       filterByJenisTestRelatedByJenisTestId()
     *
     * @see       filterByJenisTestRelatedByJenisTestId()
     *
     * @param     mixed $jenisTestId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return NilaiTestQuery The current query, for fluid interface
     */
    public function filterByJenisTestId($jenisTestId = null, $comparison = null)
    {
        if (is_array($jenisTestId)) {
            $useMinMax = false;
            if (isset($jenisTestId['min'])) {
                $this->addUsingAlias(NilaiTestPeer::JENIS_TEST_ID, $jenisTestId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jenisTestId['max'])) {
                $this->addUsingAlias(NilaiTestPeer::JENIS_TEST_ID, $jenisTestId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NilaiTestPeer::JENIS_TEST_ID, $jenisTestId, $comparison);
    }

    /**
     * Filter the query on the ptk_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPtkId('fooValue');   // WHERE ptk_id = 'fooValue'
     * $query->filterByPtkId('%fooValue%'); // WHERE ptk_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ptkId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return NilaiTestQuery The current query, for fluid interface
     */
    public function filterByPtkId($ptkId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ptkId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ptkId)) {
                $ptkId = str_replace('*', '%', $ptkId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(NilaiTestPeer::PTK_ID, $ptkId, $comparison);
    }

    /**
     * Filter the query on the nama column
     *
     * Example usage:
     * <code>
     * $query->filterByNama('fooValue');   // WHERE nama = 'fooValue'
     * $query->filterByNama('%fooValue%'); // WHERE nama LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nama The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return NilaiTestQuery The current query, for fluid interface
     */
    public function filterByNama($nama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nama)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nama)) {
                $nama = str_replace('*', '%', $nama);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(NilaiTestPeer::NAMA, $nama, $comparison);
    }

    /**
     * Filter the query on the penyelenggara column
     *
     * Example usage:
     * <code>
     * $query->filterByPenyelenggara('fooValue');   // WHERE penyelenggara = 'fooValue'
     * $query->filterByPenyelenggara('%fooValue%'); // WHERE penyelenggara LIKE '%fooValue%'
     * </code>
     *
     * @param     string $penyelenggara The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return NilaiTestQuery The current query, for fluid interface
     */
    public function filterByPenyelenggara($penyelenggara = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($penyelenggara)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $penyelenggara)) {
                $penyelenggara = str_replace('*', '%', $penyelenggara);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(NilaiTestPeer::PENYELENGGARA, $penyelenggara, $comparison);
    }

    /**
     * Filter the query on the tahun column
     *
     * Example usage:
     * <code>
     * $query->filterByTahun(1234); // WHERE tahun = 1234
     * $query->filterByTahun(array(12, 34)); // WHERE tahun IN (12, 34)
     * $query->filterByTahun(array('min' => 12)); // WHERE tahun >= 12
     * $query->filterByTahun(array('max' => 12)); // WHERE tahun <= 12
     * </code>
     *
     * @param     mixed $tahun The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return NilaiTestQuery The current query, for fluid interface
     */
    public function filterByTahun($tahun = null, $comparison = null)
    {
        if (is_array($tahun)) {
            $useMinMax = false;
            if (isset($tahun['min'])) {
                $this->addUsingAlias(NilaiTestPeer::TAHUN, $tahun['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tahun['max'])) {
                $this->addUsingAlias(NilaiTestPeer::TAHUN, $tahun['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NilaiTestPeer::TAHUN, $tahun, $comparison);
    }

    /**
     * Filter the query on the skor column
     *
     * Example usage:
     * <code>
     * $query->filterBySkor(1234); // WHERE skor = 1234
     * $query->filterBySkor(array(12, 34)); // WHERE skor IN (12, 34)
     * $query->filterBySkor(array('min' => 12)); // WHERE skor >= 12
     * $query->filterBySkor(array('max' => 12)); // WHERE skor <= 12
     * </code>
     *
     * @param     mixed $skor The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return NilaiTestQuery The current query, for fluid interface
     */
    public function filterBySkor($skor = null, $comparison = null)
    {
        if (is_array($skor)) {
            $useMinMax = false;
            if (isset($skor['min'])) {
                $this->addUsingAlias(NilaiTestPeer::SKOR, $skor['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($skor['max'])) {
                $this->addUsingAlias(NilaiTestPeer::SKOR, $skor['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NilaiTestPeer::SKOR, $skor, $comparison);
    }

    /**
     * Filter the query on the Last_update column
     *
     * Example usage:
     * <code>
     * $query->filterByLastUpdate('2011-03-14'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate('now'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate(array('max' => 'yesterday')); // WHERE Last_update > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastUpdate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return NilaiTestQuery The current query, for fluid interface
     */
    public function filterByLastUpdate($lastUpdate = null, $comparison = null)
    {
        if (is_array($lastUpdate)) {
            $useMinMax = false;
            if (isset($lastUpdate['min'])) {
                $this->addUsingAlias(NilaiTestPeer::LAST_UPDATE, $lastUpdate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastUpdate['max'])) {
                $this->addUsingAlias(NilaiTestPeer::LAST_UPDATE, $lastUpdate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NilaiTestPeer::LAST_UPDATE, $lastUpdate, $comparison);
    }

    /**
     * Filter the query on the Soft_delete column
     *
     * Example usage:
     * <code>
     * $query->filterBySoftDelete(1234); // WHERE Soft_delete = 1234
     * $query->filterBySoftDelete(array(12, 34)); // WHERE Soft_delete IN (12, 34)
     * $query->filterBySoftDelete(array('min' => 12)); // WHERE Soft_delete >= 12
     * $query->filterBySoftDelete(array('max' => 12)); // WHERE Soft_delete <= 12
     * </code>
     *
     * @param     mixed $softDelete The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return NilaiTestQuery The current query, for fluid interface
     */
    public function filterBySoftDelete($softDelete = null, $comparison = null)
    {
        if (is_array($softDelete)) {
            $useMinMax = false;
            if (isset($softDelete['min'])) {
                $this->addUsingAlias(NilaiTestPeer::SOFT_DELETE, $softDelete['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($softDelete['max'])) {
                $this->addUsingAlias(NilaiTestPeer::SOFT_DELETE, $softDelete['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NilaiTestPeer::SOFT_DELETE, $softDelete, $comparison);
    }

    /**
     * Filter the query on the last_sync column
     *
     * Example usage:
     * <code>
     * $query->filterByLastSync('2011-03-14'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync('now'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync(array('max' => 'yesterday')); // WHERE last_sync > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastSync The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return NilaiTestQuery The current query, for fluid interface
     */
    public function filterByLastSync($lastSync = null, $comparison = null)
    {
        if (is_array($lastSync)) {
            $useMinMax = false;
            if (isset($lastSync['min'])) {
                $this->addUsingAlias(NilaiTestPeer::LAST_SYNC, $lastSync['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastSync['max'])) {
                $this->addUsingAlias(NilaiTestPeer::LAST_SYNC, $lastSync['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NilaiTestPeer::LAST_SYNC, $lastSync, $comparison);
    }

    /**
     * Filter the query on the Updater_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdaterId('fooValue');   // WHERE Updater_ID = 'fooValue'
     * $query->filterByUpdaterId('%fooValue%'); // WHERE Updater_ID LIKE '%fooValue%'
     * </code>
     *
     * @param     string $updaterId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return NilaiTestQuery The current query, for fluid interface
     */
    public function filterByUpdaterId($updaterId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($updaterId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $updaterId)) {
                $updaterId = str_replace('*', '%', $updaterId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(NilaiTestPeer::UPDATER_ID, $updaterId, $comparison);
    }

    /**
     * Filter the query by a related Ptk object
     *
     * @param   Ptk|PropelObjectCollection $ptk The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 NilaiTestQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPtkRelatedByPtkId($ptk, $comparison = null)
    {
        if ($ptk instanceof Ptk) {
            return $this
                ->addUsingAlias(NilaiTestPeer::PTK_ID, $ptk->getPtkId(), $comparison);
        } elseif ($ptk instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(NilaiTestPeer::PTK_ID, $ptk->toKeyValue('PrimaryKey', 'PtkId'), $comparison);
        } else {
            throw new PropelException('filterByPtkRelatedByPtkId() only accepts arguments of type Ptk or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PtkRelatedByPtkId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return NilaiTestQuery The current query, for fluid interface
     */
    public function joinPtkRelatedByPtkId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PtkRelatedByPtkId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PtkRelatedByPtkId');
        }

        return $this;
    }

    /**
     * Use the PtkRelatedByPtkId relation Ptk object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PtkQuery A secondary query class using the current class as primary query
     */
    public function usePtkRelatedByPtkIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPtkRelatedByPtkId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PtkRelatedByPtkId', '\angulex\Model\PtkQuery');
    }

    /**
     * Filter the query by a related Ptk object
     *
     * @param   Ptk|PropelObjectCollection $ptk The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 NilaiTestQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPtkRelatedByPtkId($ptk, $comparison = null)
    {
        if ($ptk instanceof Ptk) {
            return $this
                ->addUsingAlias(NilaiTestPeer::PTK_ID, $ptk->getPtkId(), $comparison);
        } elseif ($ptk instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(NilaiTestPeer::PTK_ID, $ptk->toKeyValue('PrimaryKey', 'PtkId'), $comparison);
        } else {
            throw new PropelException('filterByPtkRelatedByPtkId() only accepts arguments of type Ptk or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PtkRelatedByPtkId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return NilaiTestQuery The current query, for fluid interface
     */
    public function joinPtkRelatedByPtkId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PtkRelatedByPtkId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PtkRelatedByPtkId');
        }

        return $this;
    }

    /**
     * Use the PtkRelatedByPtkId relation Ptk object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PtkQuery A secondary query class using the current class as primary query
     */
    public function usePtkRelatedByPtkIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPtkRelatedByPtkId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PtkRelatedByPtkId', '\angulex\Model\PtkQuery');
    }

    /**
     * Filter the query by a related JenisTest object
     *
     * @param   JenisTest|PropelObjectCollection $jenisTest The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 NilaiTestQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJenisTestRelatedByJenisTestId($jenisTest, $comparison = null)
    {
        if ($jenisTest instanceof JenisTest) {
            return $this
                ->addUsingAlias(NilaiTestPeer::JENIS_TEST_ID, $jenisTest->getJenisTestId(), $comparison);
        } elseif ($jenisTest instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(NilaiTestPeer::JENIS_TEST_ID, $jenisTest->toKeyValue('PrimaryKey', 'JenisTestId'), $comparison);
        } else {
            throw new PropelException('filterByJenisTestRelatedByJenisTestId() only accepts arguments of type JenisTest or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the JenisTestRelatedByJenisTestId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return NilaiTestQuery The current query, for fluid interface
     */
    public function joinJenisTestRelatedByJenisTestId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('JenisTestRelatedByJenisTestId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'JenisTestRelatedByJenisTestId');
        }

        return $this;
    }

    /**
     * Use the JenisTestRelatedByJenisTestId relation JenisTest object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\JenisTestQuery A secondary query class using the current class as primary query
     */
    public function useJenisTestRelatedByJenisTestIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinJenisTestRelatedByJenisTestId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'JenisTestRelatedByJenisTestId', '\angulex\Model\JenisTestQuery');
    }

    /**
     * Filter the query by a related JenisTest object
     *
     * @param   JenisTest|PropelObjectCollection $jenisTest The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 NilaiTestQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJenisTestRelatedByJenisTestId($jenisTest, $comparison = null)
    {
        if ($jenisTest instanceof JenisTest) {
            return $this
                ->addUsingAlias(NilaiTestPeer::JENIS_TEST_ID, $jenisTest->getJenisTestId(), $comparison);
        } elseif ($jenisTest instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(NilaiTestPeer::JENIS_TEST_ID, $jenisTest->toKeyValue('PrimaryKey', 'JenisTestId'), $comparison);
        } else {
            throw new PropelException('filterByJenisTestRelatedByJenisTestId() only accepts arguments of type JenisTest or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the JenisTestRelatedByJenisTestId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return NilaiTestQuery The current query, for fluid interface
     */
    public function joinJenisTestRelatedByJenisTestId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('JenisTestRelatedByJenisTestId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'JenisTestRelatedByJenisTestId');
        }

        return $this;
    }

    /**
     * Use the JenisTestRelatedByJenisTestId relation JenisTest object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\JenisTestQuery A secondary query class using the current class as primary query
     */
    public function useJenisTestRelatedByJenisTestIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinJenisTestRelatedByJenisTestId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'JenisTestRelatedByJenisTestId', '\angulex\Model\JenisTestQuery');
    }

    /**
     * Filter the query by a related VldNilaiTest object
     *
     * @param   VldNilaiTest|PropelObjectCollection $vldNilaiTest  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 NilaiTestQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldNilaiTestRelatedByNilaiTestId($vldNilaiTest, $comparison = null)
    {
        if ($vldNilaiTest instanceof VldNilaiTest) {
            return $this
                ->addUsingAlias(NilaiTestPeer::NILAI_TEST_ID, $vldNilaiTest->getNilaiTestId(), $comparison);
        } elseif ($vldNilaiTest instanceof PropelObjectCollection) {
            return $this
                ->useVldNilaiTestRelatedByNilaiTestIdQuery()
                ->filterByPrimaryKeys($vldNilaiTest->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldNilaiTestRelatedByNilaiTestId() only accepts arguments of type VldNilaiTest or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldNilaiTestRelatedByNilaiTestId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return NilaiTestQuery The current query, for fluid interface
     */
    public function joinVldNilaiTestRelatedByNilaiTestId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldNilaiTestRelatedByNilaiTestId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldNilaiTestRelatedByNilaiTestId');
        }

        return $this;
    }

    /**
     * Use the VldNilaiTestRelatedByNilaiTestId relation VldNilaiTest object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldNilaiTestQuery A secondary query class using the current class as primary query
     */
    public function useVldNilaiTestRelatedByNilaiTestIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldNilaiTestRelatedByNilaiTestId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldNilaiTestRelatedByNilaiTestId', '\angulex\Model\VldNilaiTestQuery');
    }

    /**
     * Filter the query by a related VldNilaiTest object
     *
     * @param   VldNilaiTest|PropelObjectCollection $vldNilaiTest  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 NilaiTestQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldNilaiTestRelatedByNilaiTestId($vldNilaiTest, $comparison = null)
    {
        if ($vldNilaiTest instanceof VldNilaiTest) {
            return $this
                ->addUsingAlias(NilaiTestPeer::NILAI_TEST_ID, $vldNilaiTest->getNilaiTestId(), $comparison);
        } elseif ($vldNilaiTest instanceof PropelObjectCollection) {
            return $this
                ->useVldNilaiTestRelatedByNilaiTestIdQuery()
                ->filterByPrimaryKeys($vldNilaiTest->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldNilaiTestRelatedByNilaiTestId() only accepts arguments of type VldNilaiTest or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldNilaiTestRelatedByNilaiTestId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return NilaiTestQuery The current query, for fluid interface
     */
    public function joinVldNilaiTestRelatedByNilaiTestId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldNilaiTestRelatedByNilaiTestId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldNilaiTestRelatedByNilaiTestId');
        }

        return $this;
    }

    /**
     * Use the VldNilaiTestRelatedByNilaiTestId relation VldNilaiTest object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldNilaiTestQuery A secondary query class using the current class as primary query
     */
    public function useVldNilaiTestRelatedByNilaiTestIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldNilaiTestRelatedByNilaiTestId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldNilaiTestRelatedByNilaiTestId', '\angulex\Model\VldNilaiTestQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   NilaiTest $nilaiTest Object to remove from the list of results
     *
     * @return NilaiTestQuery The current query, for fluid interface
     */
    public function prune($nilaiTest = null)
    {
        if ($nilaiTest) {
            $this->addUsingAlias(NilaiTestPeer::NILAI_TEST_ID, $nilaiTest->getNilaiTestId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
