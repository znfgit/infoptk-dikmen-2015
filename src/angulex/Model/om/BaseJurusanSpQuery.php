<?php

namespace angulex\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\AkreditasiProdi;
use angulex\Model\Jurusan;
use angulex\Model\JurusanKerjasama;
use angulex\Model\JurusanSp;
use angulex\Model\JurusanSpPeer;
use angulex\Model\JurusanSpQuery;
use angulex\Model\KebutuhanKhusus;
use angulex\Model\RegistrasiPesertaDidik;
use angulex\Model\RombonganBelajar;
use angulex\Model\Sekolah;
use angulex\Model\VldJurusanSp;

/**
 * Base class that represents a query for the 'jurusan_sp' table.
 *
 * 
 *
 * @method JurusanSpQuery orderByJurusanSpId($order = Criteria::ASC) Order by the jurusan_sp_id column
 * @method JurusanSpQuery orderBySekolahId($order = Criteria::ASC) Order by the sekolah_id column
 * @method JurusanSpQuery orderByKebutuhanKhususId($order = Criteria::ASC) Order by the kebutuhan_khusus_id column
 * @method JurusanSpQuery orderByJurusanId($order = Criteria::ASC) Order by the jurusan_id column
 * @method JurusanSpQuery orderByNamaJurusanSp($order = Criteria::ASC) Order by the nama_jurusan_sp column
 * @method JurusanSpQuery orderBySkIzin($order = Criteria::ASC) Order by the sk_izin column
 * @method JurusanSpQuery orderByTanggalSkIzin($order = Criteria::ASC) Order by the tanggal_sk_izin column
 * @method JurusanSpQuery orderByLastUpdate($order = Criteria::ASC) Order by the Last_update column
 * @method JurusanSpQuery orderBySoftDelete($order = Criteria::ASC) Order by the Soft_delete column
 * @method JurusanSpQuery orderByLastSync($order = Criteria::ASC) Order by the last_sync column
 * @method JurusanSpQuery orderByUpdaterId($order = Criteria::ASC) Order by the Updater_ID column
 *
 * @method JurusanSpQuery groupByJurusanSpId() Group by the jurusan_sp_id column
 * @method JurusanSpQuery groupBySekolahId() Group by the sekolah_id column
 * @method JurusanSpQuery groupByKebutuhanKhususId() Group by the kebutuhan_khusus_id column
 * @method JurusanSpQuery groupByJurusanId() Group by the jurusan_id column
 * @method JurusanSpQuery groupByNamaJurusanSp() Group by the nama_jurusan_sp column
 * @method JurusanSpQuery groupBySkIzin() Group by the sk_izin column
 * @method JurusanSpQuery groupByTanggalSkIzin() Group by the tanggal_sk_izin column
 * @method JurusanSpQuery groupByLastUpdate() Group by the Last_update column
 * @method JurusanSpQuery groupBySoftDelete() Group by the Soft_delete column
 * @method JurusanSpQuery groupByLastSync() Group by the last_sync column
 * @method JurusanSpQuery groupByUpdaterId() Group by the Updater_ID column
 *
 * @method JurusanSpQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method JurusanSpQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method JurusanSpQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method JurusanSpQuery leftJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method JurusanSpQuery rightJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method JurusanSpQuery innerJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the SekolahRelatedBySekolahId relation
 *
 * @method JurusanSpQuery leftJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method JurusanSpQuery rightJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method JurusanSpQuery innerJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the SekolahRelatedBySekolahId relation
 *
 * @method JurusanSpQuery leftJoinJurusanRelatedByJurusanId($relationAlias = null) Adds a LEFT JOIN clause to the query using the JurusanRelatedByJurusanId relation
 * @method JurusanSpQuery rightJoinJurusanRelatedByJurusanId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JurusanRelatedByJurusanId relation
 * @method JurusanSpQuery innerJoinJurusanRelatedByJurusanId($relationAlias = null) Adds a INNER JOIN clause to the query using the JurusanRelatedByJurusanId relation
 *
 * @method JurusanSpQuery leftJoinJurusanRelatedByJurusanId($relationAlias = null) Adds a LEFT JOIN clause to the query using the JurusanRelatedByJurusanId relation
 * @method JurusanSpQuery rightJoinJurusanRelatedByJurusanId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JurusanRelatedByJurusanId relation
 * @method JurusanSpQuery innerJoinJurusanRelatedByJurusanId($relationAlias = null) Adds a INNER JOIN clause to the query using the JurusanRelatedByJurusanId relation
 *
 * @method JurusanSpQuery leftJoinKebutuhanKhususRelatedByKebutuhanKhususId($relationAlias = null) Adds a LEFT JOIN clause to the query using the KebutuhanKhususRelatedByKebutuhanKhususId relation
 * @method JurusanSpQuery rightJoinKebutuhanKhususRelatedByKebutuhanKhususId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the KebutuhanKhususRelatedByKebutuhanKhususId relation
 * @method JurusanSpQuery innerJoinKebutuhanKhususRelatedByKebutuhanKhususId($relationAlias = null) Adds a INNER JOIN clause to the query using the KebutuhanKhususRelatedByKebutuhanKhususId relation
 *
 * @method JurusanSpQuery leftJoinKebutuhanKhususRelatedByKebutuhanKhususId($relationAlias = null) Adds a LEFT JOIN clause to the query using the KebutuhanKhususRelatedByKebutuhanKhususId relation
 * @method JurusanSpQuery rightJoinKebutuhanKhususRelatedByKebutuhanKhususId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the KebutuhanKhususRelatedByKebutuhanKhususId relation
 * @method JurusanSpQuery innerJoinKebutuhanKhususRelatedByKebutuhanKhususId($relationAlias = null) Adds a INNER JOIN clause to the query using the KebutuhanKhususRelatedByKebutuhanKhususId relation
 *
 * @method JurusanSpQuery leftJoinRegistrasiPesertaDidikRelatedByJurusanSpId($relationAlias = null) Adds a LEFT JOIN clause to the query using the RegistrasiPesertaDidikRelatedByJurusanSpId relation
 * @method JurusanSpQuery rightJoinRegistrasiPesertaDidikRelatedByJurusanSpId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RegistrasiPesertaDidikRelatedByJurusanSpId relation
 * @method JurusanSpQuery innerJoinRegistrasiPesertaDidikRelatedByJurusanSpId($relationAlias = null) Adds a INNER JOIN clause to the query using the RegistrasiPesertaDidikRelatedByJurusanSpId relation
 *
 * @method JurusanSpQuery leftJoinRegistrasiPesertaDidikRelatedByJurusanSpId($relationAlias = null) Adds a LEFT JOIN clause to the query using the RegistrasiPesertaDidikRelatedByJurusanSpId relation
 * @method JurusanSpQuery rightJoinRegistrasiPesertaDidikRelatedByJurusanSpId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RegistrasiPesertaDidikRelatedByJurusanSpId relation
 * @method JurusanSpQuery innerJoinRegistrasiPesertaDidikRelatedByJurusanSpId($relationAlias = null) Adds a INNER JOIN clause to the query using the RegistrasiPesertaDidikRelatedByJurusanSpId relation
 *
 * @method JurusanSpQuery leftJoinVldJurusanSpRelatedByJurusanSpId($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldJurusanSpRelatedByJurusanSpId relation
 * @method JurusanSpQuery rightJoinVldJurusanSpRelatedByJurusanSpId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldJurusanSpRelatedByJurusanSpId relation
 * @method JurusanSpQuery innerJoinVldJurusanSpRelatedByJurusanSpId($relationAlias = null) Adds a INNER JOIN clause to the query using the VldJurusanSpRelatedByJurusanSpId relation
 *
 * @method JurusanSpQuery leftJoinVldJurusanSpRelatedByJurusanSpId($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldJurusanSpRelatedByJurusanSpId relation
 * @method JurusanSpQuery rightJoinVldJurusanSpRelatedByJurusanSpId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldJurusanSpRelatedByJurusanSpId relation
 * @method JurusanSpQuery innerJoinVldJurusanSpRelatedByJurusanSpId($relationAlias = null) Adds a INNER JOIN clause to the query using the VldJurusanSpRelatedByJurusanSpId relation
 *
 * @method JurusanSpQuery leftJoinJurusanKerjasamaRelatedByJurusanSpId($relationAlias = null) Adds a LEFT JOIN clause to the query using the JurusanKerjasamaRelatedByJurusanSpId relation
 * @method JurusanSpQuery rightJoinJurusanKerjasamaRelatedByJurusanSpId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JurusanKerjasamaRelatedByJurusanSpId relation
 * @method JurusanSpQuery innerJoinJurusanKerjasamaRelatedByJurusanSpId($relationAlias = null) Adds a INNER JOIN clause to the query using the JurusanKerjasamaRelatedByJurusanSpId relation
 *
 * @method JurusanSpQuery leftJoinJurusanKerjasamaRelatedByJurusanSpId($relationAlias = null) Adds a LEFT JOIN clause to the query using the JurusanKerjasamaRelatedByJurusanSpId relation
 * @method JurusanSpQuery rightJoinJurusanKerjasamaRelatedByJurusanSpId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JurusanKerjasamaRelatedByJurusanSpId relation
 * @method JurusanSpQuery innerJoinJurusanKerjasamaRelatedByJurusanSpId($relationAlias = null) Adds a INNER JOIN clause to the query using the JurusanKerjasamaRelatedByJurusanSpId relation
 *
 * @method JurusanSpQuery leftJoinRombonganBelajarRelatedByJurusanSpId($relationAlias = null) Adds a LEFT JOIN clause to the query using the RombonganBelajarRelatedByJurusanSpId relation
 * @method JurusanSpQuery rightJoinRombonganBelajarRelatedByJurusanSpId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RombonganBelajarRelatedByJurusanSpId relation
 * @method JurusanSpQuery innerJoinRombonganBelajarRelatedByJurusanSpId($relationAlias = null) Adds a INNER JOIN clause to the query using the RombonganBelajarRelatedByJurusanSpId relation
 *
 * @method JurusanSpQuery leftJoinRombonganBelajarRelatedByJurusanSpId($relationAlias = null) Adds a LEFT JOIN clause to the query using the RombonganBelajarRelatedByJurusanSpId relation
 * @method JurusanSpQuery rightJoinRombonganBelajarRelatedByJurusanSpId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RombonganBelajarRelatedByJurusanSpId relation
 * @method JurusanSpQuery innerJoinRombonganBelajarRelatedByJurusanSpId($relationAlias = null) Adds a INNER JOIN clause to the query using the RombonganBelajarRelatedByJurusanSpId relation
 *
 * @method JurusanSpQuery leftJoinAkreditasiProdiRelatedByJurusanSpId($relationAlias = null) Adds a LEFT JOIN clause to the query using the AkreditasiProdiRelatedByJurusanSpId relation
 * @method JurusanSpQuery rightJoinAkreditasiProdiRelatedByJurusanSpId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the AkreditasiProdiRelatedByJurusanSpId relation
 * @method JurusanSpQuery innerJoinAkreditasiProdiRelatedByJurusanSpId($relationAlias = null) Adds a INNER JOIN clause to the query using the AkreditasiProdiRelatedByJurusanSpId relation
 *
 * @method JurusanSpQuery leftJoinAkreditasiProdiRelatedByJurusanSpId($relationAlias = null) Adds a LEFT JOIN clause to the query using the AkreditasiProdiRelatedByJurusanSpId relation
 * @method JurusanSpQuery rightJoinAkreditasiProdiRelatedByJurusanSpId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the AkreditasiProdiRelatedByJurusanSpId relation
 * @method JurusanSpQuery innerJoinAkreditasiProdiRelatedByJurusanSpId($relationAlias = null) Adds a INNER JOIN clause to the query using the AkreditasiProdiRelatedByJurusanSpId relation
 *
 * @method JurusanSp findOne(PropelPDO $con = null) Return the first JurusanSp matching the query
 * @method JurusanSp findOneOrCreate(PropelPDO $con = null) Return the first JurusanSp matching the query, or a new JurusanSp object populated from the query conditions when no match is found
 *
 * @method JurusanSp findOneBySekolahId(string $sekolah_id) Return the first JurusanSp filtered by the sekolah_id column
 * @method JurusanSp findOneByKebutuhanKhususId(int $kebutuhan_khusus_id) Return the first JurusanSp filtered by the kebutuhan_khusus_id column
 * @method JurusanSp findOneByJurusanId(string $jurusan_id) Return the first JurusanSp filtered by the jurusan_id column
 * @method JurusanSp findOneByNamaJurusanSp(string $nama_jurusan_sp) Return the first JurusanSp filtered by the nama_jurusan_sp column
 * @method JurusanSp findOneBySkIzin(string $sk_izin) Return the first JurusanSp filtered by the sk_izin column
 * @method JurusanSp findOneByTanggalSkIzin(string $tanggal_sk_izin) Return the first JurusanSp filtered by the tanggal_sk_izin column
 * @method JurusanSp findOneByLastUpdate(string $Last_update) Return the first JurusanSp filtered by the Last_update column
 * @method JurusanSp findOneBySoftDelete(string $Soft_delete) Return the first JurusanSp filtered by the Soft_delete column
 * @method JurusanSp findOneByLastSync(string $last_sync) Return the first JurusanSp filtered by the last_sync column
 * @method JurusanSp findOneByUpdaterId(string $Updater_ID) Return the first JurusanSp filtered by the Updater_ID column
 *
 * @method array findByJurusanSpId(string $jurusan_sp_id) Return JurusanSp objects filtered by the jurusan_sp_id column
 * @method array findBySekolahId(string $sekolah_id) Return JurusanSp objects filtered by the sekolah_id column
 * @method array findByKebutuhanKhususId(int $kebutuhan_khusus_id) Return JurusanSp objects filtered by the kebutuhan_khusus_id column
 * @method array findByJurusanId(string $jurusan_id) Return JurusanSp objects filtered by the jurusan_id column
 * @method array findByNamaJurusanSp(string $nama_jurusan_sp) Return JurusanSp objects filtered by the nama_jurusan_sp column
 * @method array findBySkIzin(string $sk_izin) Return JurusanSp objects filtered by the sk_izin column
 * @method array findByTanggalSkIzin(string $tanggal_sk_izin) Return JurusanSp objects filtered by the tanggal_sk_izin column
 * @method array findByLastUpdate(string $Last_update) Return JurusanSp objects filtered by the Last_update column
 * @method array findBySoftDelete(string $Soft_delete) Return JurusanSp objects filtered by the Soft_delete column
 * @method array findByLastSync(string $last_sync) Return JurusanSp objects filtered by the last_sync column
 * @method array findByUpdaterId(string $Updater_ID) Return JurusanSp objects filtered by the Updater_ID column
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseJurusanSpQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseJurusanSpQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Dapodikmen', $modelName = 'angulex\\Model\\JurusanSp', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new JurusanSpQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   JurusanSpQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return JurusanSpQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof JurusanSpQuery) {
            return $criteria;
        }
        $query = new JurusanSpQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   JurusanSp|JurusanSp[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = JurusanSpPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(JurusanSpPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 JurusanSp A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByJurusanSpId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 JurusanSp A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [jurusan_sp_id], [sekolah_id], [kebutuhan_khusus_id], [jurusan_id], [nama_jurusan_sp], [sk_izin], [tanggal_sk_izin], [Last_update], [Soft_delete], [last_sync], [Updater_ID] FROM [jurusan_sp] WHERE [jurusan_sp_id] = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new JurusanSp();
            $obj->hydrate($row);
            JurusanSpPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return JurusanSp|JurusanSp[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|JurusanSp[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return JurusanSpQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(JurusanSpPeer::JURUSAN_SP_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return JurusanSpQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(JurusanSpPeer::JURUSAN_SP_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the jurusan_sp_id column
     *
     * Example usage:
     * <code>
     * $query->filterByJurusanSpId('fooValue');   // WHERE jurusan_sp_id = 'fooValue'
     * $query->filterByJurusanSpId('%fooValue%'); // WHERE jurusan_sp_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $jurusanSpId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JurusanSpQuery The current query, for fluid interface
     */
    public function filterByJurusanSpId($jurusanSpId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($jurusanSpId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $jurusanSpId)) {
                $jurusanSpId = str_replace('*', '%', $jurusanSpId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(JurusanSpPeer::JURUSAN_SP_ID, $jurusanSpId, $comparison);
    }

    /**
     * Filter the query on the sekolah_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySekolahId('fooValue');   // WHERE sekolah_id = 'fooValue'
     * $query->filterBySekolahId('%fooValue%'); // WHERE sekolah_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sekolahId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JurusanSpQuery The current query, for fluid interface
     */
    public function filterBySekolahId($sekolahId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sekolahId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $sekolahId)) {
                $sekolahId = str_replace('*', '%', $sekolahId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(JurusanSpPeer::SEKOLAH_ID, $sekolahId, $comparison);
    }

    /**
     * Filter the query on the kebutuhan_khusus_id column
     *
     * Example usage:
     * <code>
     * $query->filterByKebutuhanKhususId(1234); // WHERE kebutuhan_khusus_id = 1234
     * $query->filterByKebutuhanKhususId(array(12, 34)); // WHERE kebutuhan_khusus_id IN (12, 34)
     * $query->filterByKebutuhanKhususId(array('min' => 12)); // WHERE kebutuhan_khusus_id >= 12
     * $query->filterByKebutuhanKhususId(array('max' => 12)); // WHERE kebutuhan_khusus_id <= 12
     * </code>
     *
     * @see       filterByKebutuhanKhususRelatedByKebutuhanKhususId()
     *
     * @see       filterByKebutuhanKhususRelatedByKebutuhanKhususId()
     *
     * @param     mixed $kebutuhanKhususId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JurusanSpQuery The current query, for fluid interface
     */
    public function filterByKebutuhanKhususId($kebutuhanKhususId = null, $comparison = null)
    {
        if (is_array($kebutuhanKhususId)) {
            $useMinMax = false;
            if (isset($kebutuhanKhususId['min'])) {
                $this->addUsingAlias(JurusanSpPeer::KEBUTUHAN_KHUSUS_ID, $kebutuhanKhususId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($kebutuhanKhususId['max'])) {
                $this->addUsingAlias(JurusanSpPeer::KEBUTUHAN_KHUSUS_ID, $kebutuhanKhususId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JurusanSpPeer::KEBUTUHAN_KHUSUS_ID, $kebutuhanKhususId, $comparison);
    }

    /**
     * Filter the query on the jurusan_id column
     *
     * Example usage:
     * <code>
     * $query->filterByJurusanId('fooValue');   // WHERE jurusan_id = 'fooValue'
     * $query->filterByJurusanId('%fooValue%'); // WHERE jurusan_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $jurusanId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JurusanSpQuery The current query, for fluid interface
     */
    public function filterByJurusanId($jurusanId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($jurusanId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $jurusanId)) {
                $jurusanId = str_replace('*', '%', $jurusanId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(JurusanSpPeer::JURUSAN_ID, $jurusanId, $comparison);
    }

    /**
     * Filter the query on the nama_jurusan_sp column
     *
     * Example usage:
     * <code>
     * $query->filterByNamaJurusanSp('fooValue');   // WHERE nama_jurusan_sp = 'fooValue'
     * $query->filterByNamaJurusanSp('%fooValue%'); // WHERE nama_jurusan_sp LIKE '%fooValue%'
     * </code>
     *
     * @param     string $namaJurusanSp The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JurusanSpQuery The current query, for fluid interface
     */
    public function filterByNamaJurusanSp($namaJurusanSp = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($namaJurusanSp)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $namaJurusanSp)) {
                $namaJurusanSp = str_replace('*', '%', $namaJurusanSp);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(JurusanSpPeer::NAMA_JURUSAN_SP, $namaJurusanSp, $comparison);
    }

    /**
     * Filter the query on the sk_izin column
     *
     * Example usage:
     * <code>
     * $query->filterBySkIzin('fooValue');   // WHERE sk_izin = 'fooValue'
     * $query->filterBySkIzin('%fooValue%'); // WHERE sk_izin LIKE '%fooValue%'
     * </code>
     *
     * @param     string $skIzin The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JurusanSpQuery The current query, for fluid interface
     */
    public function filterBySkIzin($skIzin = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($skIzin)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $skIzin)) {
                $skIzin = str_replace('*', '%', $skIzin);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(JurusanSpPeer::SK_IZIN, $skIzin, $comparison);
    }

    /**
     * Filter the query on the tanggal_sk_izin column
     *
     * Example usage:
     * <code>
     * $query->filterByTanggalSkIzin('fooValue');   // WHERE tanggal_sk_izin = 'fooValue'
     * $query->filterByTanggalSkIzin('%fooValue%'); // WHERE tanggal_sk_izin LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tanggalSkIzin The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JurusanSpQuery The current query, for fluid interface
     */
    public function filterByTanggalSkIzin($tanggalSkIzin = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tanggalSkIzin)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tanggalSkIzin)) {
                $tanggalSkIzin = str_replace('*', '%', $tanggalSkIzin);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(JurusanSpPeer::TANGGAL_SK_IZIN, $tanggalSkIzin, $comparison);
    }

    /**
     * Filter the query on the Last_update column
     *
     * Example usage:
     * <code>
     * $query->filterByLastUpdate('2011-03-14'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate('now'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate(array('max' => 'yesterday')); // WHERE Last_update > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastUpdate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JurusanSpQuery The current query, for fluid interface
     */
    public function filterByLastUpdate($lastUpdate = null, $comparison = null)
    {
        if (is_array($lastUpdate)) {
            $useMinMax = false;
            if (isset($lastUpdate['min'])) {
                $this->addUsingAlias(JurusanSpPeer::LAST_UPDATE, $lastUpdate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastUpdate['max'])) {
                $this->addUsingAlias(JurusanSpPeer::LAST_UPDATE, $lastUpdate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JurusanSpPeer::LAST_UPDATE, $lastUpdate, $comparison);
    }

    /**
     * Filter the query on the Soft_delete column
     *
     * Example usage:
     * <code>
     * $query->filterBySoftDelete(1234); // WHERE Soft_delete = 1234
     * $query->filterBySoftDelete(array(12, 34)); // WHERE Soft_delete IN (12, 34)
     * $query->filterBySoftDelete(array('min' => 12)); // WHERE Soft_delete >= 12
     * $query->filterBySoftDelete(array('max' => 12)); // WHERE Soft_delete <= 12
     * </code>
     *
     * @param     mixed $softDelete The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JurusanSpQuery The current query, for fluid interface
     */
    public function filterBySoftDelete($softDelete = null, $comparison = null)
    {
        if (is_array($softDelete)) {
            $useMinMax = false;
            if (isset($softDelete['min'])) {
                $this->addUsingAlias(JurusanSpPeer::SOFT_DELETE, $softDelete['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($softDelete['max'])) {
                $this->addUsingAlias(JurusanSpPeer::SOFT_DELETE, $softDelete['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JurusanSpPeer::SOFT_DELETE, $softDelete, $comparison);
    }

    /**
     * Filter the query on the last_sync column
     *
     * Example usage:
     * <code>
     * $query->filterByLastSync('2011-03-14'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync('now'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync(array('max' => 'yesterday')); // WHERE last_sync > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastSync The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JurusanSpQuery The current query, for fluid interface
     */
    public function filterByLastSync($lastSync = null, $comparison = null)
    {
        if (is_array($lastSync)) {
            $useMinMax = false;
            if (isset($lastSync['min'])) {
                $this->addUsingAlias(JurusanSpPeer::LAST_SYNC, $lastSync['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastSync['max'])) {
                $this->addUsingAlias(JurusanSpPeer::LAST_SYNC, $lastSync['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JurusanSpPeer::LAST_SYNC, $lastSync, $comparison);
    }

    /**
     * Filter the query on the Updater_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdaterId('fooValue');   // WHERE Updater_ID = 'fooValue'
     * $query->filterByUpdaterId('%fooValue%'); // WHERE Updater_ID LIKE '%fooValue%'
     * </code>
     *
     * @param     string $updaterId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JurusanSpQuery The current query, for fluid interface
     */
    public function filterByUpdaterId($updaterId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($updaterId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $updaterId)) {
                $updaterId = str_replace('*', '%', $updaterId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(JurusanSpPeer::UPDATER_ID, $updaterId, $comparison);
    }

    /**
     * Filter the query by a related Sekolah object
     *
     * @param   Sekolah|PropelObjectCollection $sekolah The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 JurusanSpQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySekolahRelatedBySekolahId($sekolah, $comparison = null)
    {
        if ($sekolah instanceof Sekolah) {
            return $this
                ->addUsingAlias(JurusanSpPeer::SEKOLAH_ID, $sekolah->getSekolahId(), $comparison);
        } elseif ($sekolah instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(JurusanSpPeer::SEKOLAH_ID, $sekolah->toKeyValue('PrimaryKey', 'SekolahId'), $comparison);
        } else {
            throw new PropelException('filterBySekolahRelatedBySekolahId() only accepts arguments of type Sekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SekolahRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return JurusanSpQuery The current query, for fluid interface
     */
    public function joinSekolahRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SekolahRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SekolahRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the SekolahRelatedBySekolahId relation Sekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SekolahQuery A secondary query class using the current class as primary query
     */
    public function useSekolahRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSekolahRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SekolahRelatedBySekolahId', '\angulex\Model\SekolahQuery');
    }

    /**
     * Filter the query by a related Sekolah object
     *
     * @param   Sekolah|PropelObjectCollection $sekolah The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 JurusanSpQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySekolahRelatedBySekolahId($sekolah, $comparison = null)
    {
        if ($sekolah instanceof Sekolah) {
            return $this
                ->addUsingAlias(JurusanSpPeer::SEKOLAH_ID, $sekolah->getSekolahId(), $comparison);
        } elseif ($sekolah instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(JurusanSpPeer::SEKOLAH_ID, $sekolah->toKeyValue('PrimaryKey', 'SekolahId'), $comparison);
        } else {
            throw new PropelException('filterBySekolahRelatedBySekolahId() only accepts arguments of type Sekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SekolahRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return JurusanSpQuery The current query, for fluid interface
     */
    public function joinSekolahRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SekolahRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SekolahRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the SekolahRelatedBySekolahId relation Sekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SekolahQuery A secondary query class using the current class as primary query
     */
    public function useSekolahRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSekolahRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SekolahRelatedBySekolahId', '\angulex\Model\SekolahQuery');
    }

    /**
     * Filter the query by a related Jurusan object
     *
     * @param   Jurusan|PropelObjectCollection $jurusan The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 JurusanSpQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJurusanRelatedByJurusanId($jurusan, $comparison = null)
    {
        if ($jurusan instanceof Jurusan) {
            return $this
                ->addUsingAlias(JurusanSpPeer::JURUSAN_ID, $jurusan->getJurusanId(), $comparison);
        } elseif ($jurusan instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(JurusanSpPeer::JURUSAN_ID, $jurusan->toKeyValue('PrimaryKey', 'JurusanId'), $comparison);
        } else {
            throw new PropelException('filterByJurusanRelatedByJurusanId() only accepts arguments of type Jurusan or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the JurusanRelatedByJurusanId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return JurusanSpQuery The current query, for fluid interface
     */
    public function joinJurusanRelatedByJurusanId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('JurusanRelatedByJurusanId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'JurusanRelatedByJurusanId');
        }

        return $this;
    }

    /**
     * Use the JurusanRelatedByJurusanId relation Jurusan object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\JurusanQuery A secondary query class using the current class as primary query
     */
    public function useJurusanRelatedByJurusanIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinJurusanRelatedByJurusanId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'JurusanRelatedByJurusanId', '\angulex\Model\JurusanQuery');
    }

    /**
     * Filter the query by a related Jurusan object
     *
     * @param   Jurusan|PropelObjectCollection $jurusan The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 JurusanSpQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJurusanRelatedByJurusanId($jurusan, $comparison = null)
    {
        if ($jurusan instanceof Jurusan) {
            return $this
                ->addUsingAlias(JurusanSpPeer::JURUSAN_ID, $jurusan->getJurusanId(), $comparison);
        } elseif ($jurusan instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(JurusanSpPeer::JURUSAN_ID, $jurusan->toKeyValue('PrimaryKey', 'JurusanId'), $comparison);
        } else {
            throw new PropelException('filterByJurusanRelatedByJurusanId() only accepts arguments of type Jurusan or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the JurusanRelatedByJurusanId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return JurusanSpQuery The current query, for fluid interface
     */
    public function joinJurusanRelatedByJurusanId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('JurusanRelatedByJurusanId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'JurusanRelatedByJurusanId');
        }

        return $this;
    }

    /**
     * Use the JurusanRelatedByJurusanId relation Jurusan object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\JurusanQuery A secondary query class using the current class as primary query
     */
    public function useJurusanRelatedByJurusanIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinJurusanRelatedByJurusanId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'JurusanRelatedByJurusanId', '\angulex\Model\JurusanQuery');
    }

    /**
     * Filter the query by a related KebutuhanKhusus object
     *
     * @param   KebutuhanKhusus|PropelObjectCollection $kebutuhanKhusus The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 JurusanSpQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByKebutuhanKhususRelatedByKebutuhanKhususId($kebutuhanKhusus, $comparison = null)
    {
        if ($kebutuhanKhusus instanceof KebutuhanKhusus) {
            return $this
                ->addUsingAlias(JurusanSpPeer::KEBUTUHAN_KHUSUS_ID, $kebutuhanKhusus->getKebutuhanKhususId(), $comparison);
        } elseif ($kebutuhanKhusus instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(JurusanSpPeer::KEBUTUHAN_KHUSUS_ID, $kebutuhanKhusus->toKeyValue('PrimaryKey', 'KebutuhanKhususId'), $comparison);
        } else {
            throw new PropelException('filterByKebutuhanKhususRelatedByKebutuhanKhususId() only accepts arguments of type KebutuhanKhusus or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the KebutuhanKhususRelatedByKebutuhanKhususId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return JurusanSpQuery The current query, for fluid interface
     */
    public function joinKebutuhanKhususRelatedByKebutuhanKhususId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('KebutuhanKhususRelatedByKebutuhanKhususId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'KebutuhanKhususRelatedByKebutuhanKhususId');
        }

        return $this;
    }

    /**
     * Use the KebutuhanKhususRelatedByKebutuhanKhususId relation KebutuhanKhusus object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\KebutuhanKhususQuery A secondary query class using the current class as primary query
     */
    public function useKebutuhanKhususRelatedByKebutuhanKhususIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinKebutuhanKhususRelatedByKebutuhanKhususId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'KebutuhanKhususRelatedByKebutuhanKhususId', '\angulex\Model\KebutuhanKhususQuery');
    }

    /**
     * Filter the query by a related KebutuhanKhusus object
     *
     * @param   KebutuhanKhusus|PropelObjectCollection $kebutuhanKhusus The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 JurusanSpQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByKebutuhanKhususRelatedByKebutuhanKhususId($kebutuhanKhusus, $comparison = null)
    {
        if ($kebutuhanKhusus instanceof KebutuhanKhusus) {
            return $this
                ->addUsingAlias(JurusanSpPeer::KEBUTUHAN_KHUSUS_ID, $kebutuhanKhusus->getKebutuhanKhususId(), $comparison);
        } elseif ($kebutuhanKhusus instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(JurusanSpPeer::KEBUTUHAN_KHUSUS_ID, $kebutuhanKhusus->toKeyValue('PrimaryKey', 'KebutuhanKhususId'), $comparison);
        } else {
            throw new PropelException('filterByKebutuhanKhususRelatedByKebutuhanKhususId() only accepts arguments of type KebutuhanKhusus or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the KebutuhanKhususRelatedByKebutuhanKhususId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return JurusanSpQuery The current query, for fluid interface
     */
    public function joinKebutuhanKhususRelatedByKebutuhanKhususId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('KebutuhanKhususRelatedByKebutuhanKhususId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'KebutuhanKhususRelatedByKebutuhanKhususId');
        }

        return $this;
    }

    /**
     * Use the KebutuhanKhususRelatedByKebutuhanKhususId relation KebutuhanKhusus object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\KebutuhanKhususQuery A secondary query class using the current class as primary query
     */
    public function useKebutuhanKhususRelatedByKebutuhanKhususIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinKebutuhanKhususRelatedByKebutuhanKhususId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'KebutuhanKhususRelatedByKebutuhanKhususId', '\angulex\Model\KebutuhanKhususQuery');
    }

    /**
     * Filter the query by a related RegistrasiPesertaDidik object
     *
     * @param   RegistrasiPesertaDidik|PropelObjectCollection $registrasiPesertaDidik  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 JurusanSpQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRegistrasiPesertaDidikRelatedByJurusanSpId($registrasiPesertaDidik, $comparison = null)
    {
        if ($registrasiPesertaDidik instanceof RegistrasiPesertaDidik) {
            return $this
                ->addUsingAlias(JurusanSpPeer::JURUSAN_SP_ID, $registrasiPesertaDidik->getJurusanSpId(), $comparison);
        } elseif ($registrasiPesertaDidik instanceof PropelObjectCollection) {
            return $this
                ->useRegistrasiPesertaDidikRelatedByJurusanSpIdQuery()
                ->filterByPrimaryKeys($registrasiPesertaDidik->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRegistrasiPesertaDidikRelatedByJurusanSpId() only accepts arguments of type RegistrasiPesertaDidik or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RegistrasiPesertaDidikRelatedByJurusanSpId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return JurusanSpQuery The current query, for fluid interface
     */
    public function joinRegistrasiPesertaDidikRelatedByJurusanSpId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RegistrasiPesertaDidikRelatedByJurusanSpId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RegistrasiPesertaDidikRelatedByJurusanSpId');
        }

        return $this;
    }

    /**
     * Use the RegistrasiPesertaDidikRelatedByJurusanSpId relation RegistrasiPesertaDidik object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\RegistrasiPesertaDidikQuery A secondary query class using the current class as primary query
     */
    public function useRegistrasiPesertaDidikRelatedByJurusanSpIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinRegistrasiPesertaDidikRelatedByJurusanSpId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RegistrasiPesertaDidikRelatedByJurusanSpId', '\angulex\Model\RegistrasiPesertaDidikQuery');
    }

    /**
     * Filter the query by a related RegistrasiPesertaDidik object
     *
     * @param   RegistrasiPesertaDidik|PropelObjectCollection $registrasiPesertaDidik  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 JurusanSpQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRegistrasiPesertaDidikRelatedByJurusanSpId($registrasiPesertaDidik, $comparison = null)
    {
        if ($registrasiPesertaDidik instanceof RegistrasiPesertaDidik) {
            return $this
                ->addUsingAlias(JurusanSpPeer::JURUSAN_SP_ID, $registrasiPesertaDidik->getJurusanSpId(), $comparison);
        } elseif ($registrasiPesertaDidik instanceof PropelObjectCollection) {
            return $this
                ->useRegistrasiPesertaDidikRelatedByJurusanSpIdQuery()
                ->filterByPrimaryKeys($registrasiPesertaDidik->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRegistrasiPesertaDidikRelatedByJurusanSpId() only accepts arguments of type RegistrasiPesertaDidik or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RegistrasiPesertaDidikRelatedByJurusanSpId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return JurusanSpQuery The current query, for fluid interface
     */
    public function joinRegistrasiPesertaDidikRelatedByJurusanSpId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RegistrasiPesertaDidikRelatedByJurusanSpId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RegistrasiPesertaDidikRelatedByJurusanSpId');
        }

        return $this;
    }

    /**
     * Use the RegistrasiPesertaDidikRelatedByJurusanSpId relation RegistrasiPesertaDidik object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\RegistrasiPesertaDidikQuery A secondary query class using the current class as primary query
     */
    public function useRegistrasiPesertaDidikRelatedByJurusanSpIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinRegistrasiPesertaDidikRelatedByJurusanSpId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RegistrasiPesertaDidikRelatedByJurusanSpId', '\angulex\Model\RegistrasiPesertaDidikQuery');
    }

    /**
     * Filter the query by a related VldJurusanSp object
     *
     * @param   VldJurusanSp|PropelObjectCollection $vldJurusanSp  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 JurusanSpQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldJurusanSpRelatedByJurusanSpId($vldJurusanSp, $comparison = null)
    {
        if ($vldJurusanSp instanceof VldJurusanSp) {
            return $this
                ->addUsingAlias(JurusanSpPeer::JURUSAN_SP_ID, $vldJurusanSp->getJurusanSpId(), $comparison);
        } elseif ($vldJurusanSp instanceof PropelObjectCollection) {
            return $this
                ->useVldJurusanSpRelatedByJurusanSpIdQuery()
                ->filterByPrimaryKeys($vldJurusanSp->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldJurusanSpRelatedByJurusanSpId() only accepts arguments of type VldJurusanSp or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldJurusanSpRelatedByJurusanSpId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return JurusanSpQuery The current query, for fluid interface
     */
    public function joinVldJurusanSpRelatedByJurusanSpId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldJurusanSpRelatedByJurusanSpId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldJurusanSpRelatedByJurusanSpId');
        }

        return $this;
    }

    /**
     * Use the VldJurusanSpRelatedByJurusanSpId relation VldJurusanSp object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldJurusanSpQuery A secondary query class using the current class as primary query
     */
    public function useVldJurusanSpRelatedByJurusanSpIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldJurusanSpRelatedByJurusanSpId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldJurusanSpRelatedByJurusanSpId', '\angulex\Model\VldJurusanSpQuery');
    }

    /**
     * Filter the query by a related VldJurusanSp object
     *
     * @param   VldJurusanSp|PropelObjectCollection $vldJurusanSp  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 JurusanSpQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldJurusanSpRelatedByJurusanSpId($vldJurusanSp, $comparison = null)
    {
        if ($vldJurusanSp instanceof VldJurusanSp) {
            return $this
                ->addUsingAlias(JurusanSpPeer::JURUSAN_SP_ID, $vldJurusanSp->getJurusanSpId(), $comparison);
        } elseif ($vldJurusanSp instanceof PropelObjectCollection) {
            return $this
                ->useVldJurusanSpRelatedByJurusanSpIdQuery()
                ->filterByPrimaryKeys($vldJurusanSp->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldJurusanSpRelatedByJurusanSpId() only accepts arguments of type VldJurusanSp or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldJurusanSpRelatedByJurusanSpId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return JurusanSpQuery The current query, for fluid interface
     */
    public function joinVldJurusanSpRelatedByJurusanSpId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldJurusanSpRelatedByJurusanSpId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldJurusanSpRelatedByJurusanSpId');
        }

        return $this;
    }

    /**
     * Use the VldJurusanSpRelatedByJurusanSpId relation VldJurusanSp object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldJurusanSpQuery A secondary query class using the current class as primary query
     */
    public function useVldJurusanSpRelatedByJurusanSpIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldJurusanSpRelatedByJurusanSpId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldJurusanSpRelatedByJurusanSpId', '\angulex\Model\VldJurusanSpQuery');
    }

    /**
     * Filter the query by a related JurusanKerjasama object
     *
     * @param   JurusanKerjasama|PropelObjectCollection $jurusanKerjasama  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 JurusanSpQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJurusanKerjasamaRelatedByJurusanSpId($jurusanKerjasama, $comparison = null)
    {
        if ($jurusanKerjasama instanceof JurusanKerjasama) {
            return $this
                ->addUsingAlias(JurusanSpPeer::JURUSAN_SP_ID, $jurusanKerjasama->getJurusanSpId(), $comparison);
        } elseif ($jurusanKerjasama instanceof PropelObjectCollection) {
            return $this
                ->useJurusanKerjasamaRelatedByJurusanSpIdQuery()
                ->filterByPrimaryKeys($jurusanKerjasama->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByJurusanKerjasamaRelatedByJurusanSpId() only accepts arguments of type JurusanKerjasama or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the JurusanKerjasamaRelatedByJurusanSpId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return JurusanSpQuery The current query, for fluid interface
     */
    public function joinJurusanKerjasamaRelatedByJurusanSpId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('JurusanKerjasamaRelatedByJurusanSpId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'JurusanKerjasamaRelatedByJurusanSpId');
        }

        return $this;
    }

    /**
     * Use the JurusanKerjasamaRelatedByJurusanSpId relation JurusanKerjasama object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\JurusanKerjasamaQuery A secondary query class using the current class as primary query
     */
    public function useJurusanKerjasamaRelatedByJurusanSpIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinJurusanKerjasamaRelatedByJurusanSpId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'JurusanKerjasamaRelatedByJurusanSpId', '\angulex\Model\JurusanKerjasamaQuery');
    }

    /**
     * Filter the query by a related JurusanKerjasama object
     *
     * @param   JurusanKerjasama|PropelObjectCollection $jurusanKerjasama  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 JurusanSpQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJurusanKerjasamaRelatedByJurusanSpId($jurusanKerjasama, $comparison = null)
    {
        if ($jurusanKerjasama instanceof JurusanKerjasama) {
            return $this
                ->addUsingAlias(JurusanSpPeer::JURUSAN_SP_ID, $jurusanKerjasama->getJurusanSpId(), $comparison);
        } elseif ($jurusanKerjasama instanceof PropelObjectCollection) {
            return $this
                ->useJurusanKerjasamaRelatedByJurusanSpIdQuery()
                ->filterByPrimaryKeys($jurusanKerjasama->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByJurusanKerjasamaRelatedByJurusanSpId() only accepts arguments of type JurusanKerjasama or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the JurusanKerjasamaRelatedByJurusanSpId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return JurusanSpQuery The current query, for fluid interface
     */
    public function joinJurusanKerjasamaRelatedByJurusanSpId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('JurusanKerjasamaRelatedByJurusanSpId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'JurusanKerjasamaRelatedByJurusanSpId');
        }

        return $this;
    }

    /**
     * Use the JurusanKerjasamaRelatedByJurusanSpId relation JurusanKerjasama object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\JurusanKerjasamaQuery A secondary query class using the current class as primary query
     */
    public function useJurusanKerjasamaRelatedByJurusanSpIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinJurusanKerjasamaRelatedByJurusanSpId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'JurusanKerjasamaRelatedByJurusanSpId', '\angulex\Model\JurusanKerjasamaQuery');
    }

    /**
     * Filter the query by a related RombonganBelajar object
     *
     * @param   RombonganBelajar|PropelObjectCollection $rombonganBelajar  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 JurusanSpQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRombonganBelajarRelatedByJurusanSpId($rombonganBelajar, $comparison = null)
    {
        if ($rombonganBelajar instanceof RombonganBelajar) {
            return $this
                ->addUsingAlias(JurusanSpPeer::JURUSAN_SP_ID, $rombonganBelajar->getJurusanSpId(), $comparison);
        } elseif ($rombonganBelajar instanceof PropelObjectCollection) {
            return $this
                ->useRombonganBelajarRelatedByJurusanSpIdQuery()
                ->filterByPrimaryKeys($rombonganBelajar->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRombonganBelajarRelatedByJurusanSpId() only accepts arguments of type RombonganBelajar or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RombonganBelajarRelatedByJurusanSpId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return JurusanSpQuery The current query, for fluid interface
     */
    public function joinRombonganBelajarRelatedByJurusanSpId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RombonganBelajarRelatedByJurusanSpId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RombonganBelajarRelatedByJurusanSpId');
        }

        return $this;
    }

    /**
     * Use the RombonganBelajarRelatedByJurusanSpId relation RombonganBelajar object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\RombonganBelajarQuery A secondary query class using the current class as primary query
     */
    public function useRombonganBelajarRelatedByJurusanSpIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinRombonganBelajarRelatedByJurusanSpId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RombonganBelajarRelatedByJurusanSpId', '\angulex\Model\RombonganBelajarQuery');
    }

    /**
     * Filter the query by a related RombonganBelajar object
     *
     * @param   RombonganBelajar|PropelObjectCollection $rombonganBelajar  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 JurusanSpQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRombonganBelajarRelatedByJurusanSpId($rombonganBelajar, $comparison = null)
    {
        if ($rombonganBelajar instanceof RombonganBelajar) {
            return $this
                ->addUsingAlias(JurusanSpPeer::JURUSAN_SP_ID, $rombonganBelajar->getJurusanSpId(), $comparison);
        } elseif ($rombonganBelajar instanceof PropelObjectCollection) {
            return $this
                ->useRombonganBelajarRelatedByJurusanSpIdQuery()
                ->filterByPrimaryKeys($rombonganBelajar->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRombonganBelajarRelatedByJurusanSpId() only accepts arguments of type RombonganBelajar or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RombonganBelajarRelatedByJurusanSpId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return JurusanSpQuery The current query, for fluid interface
     */
    public function joinRombonganBelajarRelatedByJurusanSpId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RombonganBelajarRelatedByJurusanSpId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RombonganBelajarRelatedByJurusanSpId');
        }

        return $this;
    }

    /**
     * Use the RombonganBelajarRelatedByJurusanSpId relation RombonganBelajar object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\RombonganBelajarQuery A secondary query class using the current class as primary query
     */
    public function useRombonganBelajarRelatedByJurusanSpIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinRombonganBelajarRelatedByJurusanSpId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RombonganBelajarRelatedByJurusanSpId', '\angulex\Model\RombonganBelajarQuery');
    }

    /**
     * Filter the query by a related AkreditasiProdi object
     *
     * @param   AkreditasiProdi|PropelObjectCollection $akreditasiProdi  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 JurusanSpQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByAkreditasiProdiRelatedByJurusanSpId($akreditasiProdi, $comparison = null)
    {
        if ($akreditasiProdi instanceof AkreditasiProdi) {
            return $this
                ->addUsingAlias(JurusanSpPeer::JURUSAN_SP_ID, $akreditasiProdi->getJurusanSpId(), $comparison);
        } elseif ($akreditasiProdi instanceof PropelObjectCollection) {
            return $this
                ->useAkreditasiProdiRelatedByJurusanSpIdQuery()
                ->filterByPrimaryKeys($akreditasiProdi->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByAkreditasiProdiRelatedByJurusanSpId() only accepts arguments of type AkreditasiProdi or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the AkreditasiProdiRelatedByJurusanSpId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return JurusanSpQuery The current query, for fluid interface
     */
    public function joinAkreditasiProdiRelatedByJurusanSpId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('AkreditasiProdiRelatedByJurusanSpId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'AkreditasiProdiRelatedByJurusanSpId');
        }

        return $this;
    }

    /**
     * Use the AkreditasiProdiRelatedByJurusanSpId relation AkreditasiProdi object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\AkreditasiProdiQuery A secondary query class using the current class as primary query
     */
    public function useAkreditasiProdiRelatedByJurusanSpIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinAkreditasiProdiRelatedByJurusanSpId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'AkreditasiProdiRelatedByJurusanSpId', '\angulex\Model\AkreditasiProdiQuery');
    }

    /**
     * Filter the query by a related AkreditasiProdi object
     *
     * @param   AkreditasiProdi|PropelObjectCollection $akreditasiProdi  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 JurusanSpQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByAkreditasiProdiRelatedByJurusanSpId($akreditasiProdi, $comparison = null)
    {
        if ($akreditasiProdi instanceof AkreditasiProdi) {
            return $this
                ->addUsingAlias(JurusanSpPeer::JURUSAN_SP_ID, $akreditasiProdi->getJurusanSpId(), $comparison);
        } elseif ($akreditasiProdi instanceof PropelObjectCollection) {
            return $this
                ->useAkreditasiProdiRelatedByJurusanSpIdQuery()
                ->filterByPrimaryKeys($akreditasiProdi->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByAkreditasiProdiRelatedByJurusanSpId() only accepts arguments of type AkreditasiProdi or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the AkreditasiProdiRelatedByJurusanSpId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return JurusanSpQuery The current query, for fluid interface
     */
    public function joinAkreditasiProdiRelatedByJurusanSpId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('AkreditasiProdiRelatedByJurusanSpId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'AkreditasiProdiRelatedByJurusanSpId');
        }

        return $this;
    }

    /**
     * Use the AkreditasiProdiRelatedByJurusanSpId relation AkreditasiProdi object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\AkreditasiProdiQuery A secondary query class using the current class as primary query
     */
    public function useAkreditasiProdiRelatedByJurusanSpIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinAkreditasiProdiRelatedByJurusanSpId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'AkreditasiProdiRelatedByJurusanSpId', '\angulex\Model\AkreditasiProdiQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   JurusanSp $jurusanSp Object to remove from the list of results
     *
     * @return JurusanSpQuery The current query, for fluid interface
     */
    public function prune($jurusanSp = null)
    {
        if ($jurusanSp) {
            $this->addUsingAlias(JurusanSpPeer::JURUSAN_SP_ID, $jurusanSp->getJurusanSpId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
