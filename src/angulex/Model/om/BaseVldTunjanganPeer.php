<?php

namespace angulex\Model\om;

use \BasePeer;
use \Criteria;
use \PDO;
use \PDOStatement;
use \Propel;
use \PropelException;
use \PropelPDO;
use angulex\Model\ErrortypePeer;
use angulex\Model\TunjanganPeer;
use angulex\Model\VldTunjangan;
use angulex\Model\VldTunjanganPeer;
use angulex\Model\map\VldTunjanganTableMap;

/**
 * Base static class for performing query and update operations on the 'vld_tunjangan' table.
 *
 * 
 *
 * @package propel.generator.angulex.Model.om
 */
abstract class BaseVldTunjanganPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'Dapodikmen';

    /** the table name for this class */
    const TABLE_NAME = 'vld_tunjangan';

    /** the related Propel class for this table */
    const OM_CLASS = 'angulex\\Model\\VldTunjangan';

    /** the related TableMap class for this table */
    const TM_CLASS = 'VldTunjanganTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 13;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 13;

    /** the column name for the tunjangan_id field */
    const TUNJANGAN_ID = 'vld_tunjangan.tunjangan_id';

    /** the column name for the logid field */
    const LOGID = 'vld_tunjangan.logid';

    /** the column name for the idtype field */
    const IDTYPE = 'vld_tunjangan.idtype';

    /** the column name for the status_validasi field */
    const STATUS_VALIDASI = 'vld_tunjangan.status_validasi';

    /** the column name for the status_verifikasi field */
    const STATUS_VERIFIKASI = 'vld_tunjangan.status_verifikasi';

    /** the column name for the field_error field */
    const FIELD_ERROR = 'vld_tunjangan.field_error';

    /** the column name for the error_message field */
    const ERROR_MESSAGE = 'vld_tunjangan.error_message';

    /** the column name for the keterangan field */
    const KETERANGAN = 'vld_tunjangan.keterangan';

    /** the column name for the create_date field */
    const CREATE_DATE = 'vld_tunjangan.create_date';

    /** the column name for the last_update field */
    const LAST_UPDATE = 'vld_tunjangan.last_update';

    /** the column name for the expired_date field */
    const EXPIRED_DATE = 'vld_tunjangan.expired_date';

    /** the column name for the last_sync field */
    const LAST_SYNC = 'vld_tunjangan.last_sync';

    /** the column name for the app_username field */
    const APP_USERNAME = 'vld_tunjangan.app_username';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identiy map to hold any loaded instances of VldTunjangan objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array VldTunjangan[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. VldTunjanganPeer::$fieldNames[VldTunjanganPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('TunjanganId', 'Logid', 'Idtype', 'StatusValidasi', 'StatusVerifikasi', 'FieldError', 'ErrorMessage', 'Keterangan', 'CreateDate', 'LastUpdate', 'ExpiredDate', 'LastSync', 'AppUsername', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('tunjanganId', 'logid', 'idtype', 'statusValidasi', 'statusVerifikasi', 'fieldError', 'errorMessage', 'keterangan', 'createDate', 'lastUpdate', 'expiredDate', 'lastSync', 'appUsername', ),
        BasePeer::TYPE_COLNAME => array (VldTunjanganPeer::TUNJANGAN_ID, VldTunjanganPeer::LOGID, VldTunjanganPeer::IDTYPE, VldTunjanganPeer::STATUS_VALIDASI, VldTunjanganPeer::STATUS_VERIFIKASI, VldTunjanganPeer::FIELD_ERROR, VldTunjanganPeer::ERROR_MESSAGE, VldTunjanganPeer::KETERANGAN, VldTunjanganPeer::CREATE_DATE, VldTunjanganPeer::LAST_UPDATE, VldTunjanganPeer::EXPIRED_DATE, VldTunjanganPeer::LAST_SYNC, VldTunjanganPeer::APP_USERNAME, ),
        BasePeer::TYPE_RAW_COLNAME => array ('TUNJANGAN_ID', 'LOGID', 'IDTYPE', 'STATUS_VALIDASI', 'STATUS_VERIFIKASI', 'FIELD_ERROR', 'ERROR_MESSAGE', 'KETERANGAN', 'CREATE_DATE', 'LAST_UPDATE', 'EXPIRED_DATE', 'LAST_SYNC', 'APP_USERNAME', ),
        BasePeer::TYPE_FIELDNAME => array ('tunjangan_id', 'logid', 'idtype', 'status_validasi', 'status_verifikasi', 'field_error', 'error_message', 'keterangan', 'create_date', 'last_update', 'expired_date', 'last_sync', 'app_username', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. VldTunjanganPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('TunjanganId' => 0, 'Logid' => 1, 'Idtype' => 2, 'StatusValidasi' => 3, 'StatusVerifikasi' => 4, 'FieldError' => 5, 'ErrorMessage' => 6, 'Keterangan' => 7, 'CreateDate' => 8, 'LastUpdate' => 9, 'ExpiredDate' => 10, 'LastSync' => 11, 'AppUsername' => 12, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('tunjanganId' => 0, 'logid' => 1, 'idtype' => 2, 'statusValidasi' => 3, 'statusVerifikasi' => 4, 'fieldError' => 5, 'errorMessage' => 6, 'keterangan' => 7, 'createDate' => 8, 'lastUpdate' => 9, 'expiredDate' => 10, 'lastSync' => 11, 'appUsername' => 12, ),
        BasePeer::TYPE_COLNAME => array (VldTunjanganPeer::TUNJANGAN_ID => 0, VldTunjanganPeer::LOGID => 1, VldTunjanganPeer::IDTYPE => 2, VldTunjanganPeer::STATUS_VALIDASI => 3, VldTunjanganPeer::STATUS_VERIFIKASI => 4, VldTunjanganPeer::FIELD_ERROR => 5, VldTunjanganPeer::ERROR_MESSAGE => 6, VldTunjanganPeer::KETERANGAN => 7, VldTunjanganPeer::CREATE_DATE => 8, VldTunjanganPeer::LAST_UPDATE => 9, VldTunjanganPeer::EXPIRED_DATE => 10, VldTunjanganPeer::LAST_SYNC => 11, VldTunjanganPeer::APP_USERNAME => 12, ),
        BasePeer::TYPE_RAW_COLNAME => array ('TUNJANGAN_ID' => 0, 'LOGID' => 1, 'IDTYPE' => 2, 'STATUS_VALIDASI' => 3, 'STATUS_VERIFIKASI' => 4, 'FIELD_ERROR' => 5, 'ERROR_MESSAGE' => 6, 'KETERANGAN' => 7, 'CREATE_DATE' => 8, 'LAST_UPDATE' => 9, 'EXPIRED_DATE' => 10, 'LAST_SYNC' => 11, 'APP_USERNAME' => 12, ),
        BasePeer::TYPE_FIELDNAME => array ('tunjangan_id' => 0, 'logid' => 1, 'idtype' => 2, 'status_validasi' => 3, 'status_verifikasi' => 4, 'field_error' => 5, 'error_message' => 6, 'keterangan' => 7, 'create_date' => 8, 'last_update' => 9, 'expired_date' => 10, 'last_sync' => 11, 'app_username' => 12, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = VldTunjanganPeer::getFieldNames($toType);
        $key = isset(VldTunjanganPeer::$fieldKeys[$fromType][$name]) ? VldTunjanganPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(VldTunjanganPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, VldTunjanganPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return VldTunjanganPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. VldTunjanganPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(VldTunjanganPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(VldTunjanganPeer::TUNJANGAN_ID);
            $criteria->addSelectColumn(VldTunjanganPeer::LOGID);
            $criteria->addSelectColumn(VldTunjanganPeer::IDTYPE);
            $criteria->addSelectColumn(VldTunjanganPeer::STATUS_VALIDASI);
            $criteria->addSelectColumn(VldTunjanganPeer::STATUS_VERIFIKASI);
            $criteria->addSelectColumn(VldTunjanganPeer::FIELD_ERROR);
            $criteria->addSelectColumn(VldTunjanganPeer::ERROR_MESSAGE);
            $criteria->addSelectColumn(VldTunjanganPeer::KETERANGAN);
            $criteria->addSelectColumn(VldTunjanganPeer::CREATE_DATE);
            $criteria->addSelectColumn(VldTunjanganPeer::LAST_UPDATE);
            $criteria->addSelectColumn(VldTunjanganPeer::EXPIRED_DATE);
            $criteria->addSelectColumn(VldTunjanganPeer::LAST_SYNC);
            $criteria->addSelectColumn(VldTunjanganPeer::APP_USERNAME);
        } else {
            $criteria->addSelectColumn($alias . '.tunjangan_id');
            $criteria->addSelectColumn($alias . '.logid');
            $criteria->addSelectColumn($alias . '.idtype');
            $criteria->addSelectColumn($alias . '.status_validasi');
            $criteria->addSelectColumn($alias . '.status_verifikasi');
            $criteria->addSelectColumn($alias . '.field_error');
            $criteria->addSelectColumn($alias . '.error_message');
            $criteria->addSelectColumn($alias . '.keterangan');
            $criteria->addSelectColumn($alias . '.create_date');
            $criteria->addSelectColumn($alias . '.last_update');
            $criteria->addSelectColumn($alias . '.expired_date');
            $criteria->addSelectColumn($alias . '.last_sync');
            $criteria->addSelectColumn($alias . '.app_username');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(VldTunjanganPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            VldTunjanganPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(VldTunjanganPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(VldTunjanganPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 VldTunjangan
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = VldTunjanganPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return VldTunjanganPeer::populateObjects(VldTunjanganPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(VldTunjanganPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            VldTunjanganPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(VldTunjanganPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      VldTunjangan $obj A VldTunjangan object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = serialize(array((string) $obj->getTunjanganId(), (string) $obj->getLogid()));
            } // if key === null
            VldTunjanganPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A VldTunjangan object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof VldTunjangan) {
                $key = serialize(array((string) $value->getTunjanganId(), (string) $value->getLogid()));
            } elseif (is_array($value) && count($value) === 2) {
                // assume we've been passed a primary key
                $key = serialize(array((string) $value[0], (string) $value[1]));
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or VldTunjangan object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(VldTunjanganPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   VldTunjangan Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(VldTunjanganPeer::$instances[$key])) {
                return VldTunjanganPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }
    
    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references)
      {
        foreach (VldTunjanganPeer::$instances as $instance)
        {
          $instance->clearAllReferences(true);
        }
      }
        VldTunjanganPeer::$instances = array();
    }
    
    /**
     * Method to invalidate the instance pool of all tables related to vld_tunjangan
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null && $row[$startcol + 1] === null) {
            return null;
        }

        return serialize(array((string) $row[$startcol], (string) $row[$startcol + 1]));
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return array((string) $row[$startcol], (string) $row[$startcol + 1]);
    }
    
    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();
    
        // set the class once to avoid overhead in the loop
        $cls = VldTunjanganPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = VldTunjanganPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = VldTunjanganPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                VldTunjanganPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (VldTunjangan object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = VldTunjanganPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = VldTunjanganPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + VldTunjanganPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = VldTunjanganPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            VldTunjanganPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related TunjanganRelatedByTunjanganId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinTunjanganRelatedByTunjanganId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(VldTunjanganPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            VldTunjanganPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(VldTunjanganPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(VldTunjanganPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(VldTunjanganPeer::TUNJANGAN_ID, TunjanganPeer::TUNJANGAN_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related TunjanganRelatedByTunjanganId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinTunjanganRelatedByTunjanganId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(VldTunjanganPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            VldTunjanganPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(VldTunjanganPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(VldTunjanganPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(VldTunjanganPeer::TUNJANGAN_ID, TunjanganPeer::TUNJANGAN_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related ErrortypeRelatedByIdtype table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinErrortypeRelatedByIdtype(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(VldTunjanganPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            VldTunjanganPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(VldTunjanganPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(VldTunjanganPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(VldTunjanganPeer::IDTYPE, ErrortypePeer::IDTYPE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related ErrortypeRelatedByIdtype table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinErrortypeRelatedByIdtype(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(VldTunjanganPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            VldTunjanganPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(VldTunjanganPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(VldTunjanganPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(VldTunjanganPeer::IDTYPE, ErrortypePeer::IDTYPE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of VldTunjangan objects pre-filled with their Tunjangan objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of VldTunjangan objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinTunjanganRelatedByTunjanganId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(VldTunjanganPeer::DATABASE_NAME);
        }

        VldTunjanganPeer::addSelectColumns($criteria);
        $startcol = VldTunjanganPeer::NUM_HYDRATE_COLUMNS;
        TunjanganPeer::addSelectColumns($criteria);

        $criteria->addJoin(VldTunjanganPeer::TUNJANGAN_ID, TunjanganPeer::TUNJANGAN_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = VldTunjanganPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = VldTunjanganPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = VldTunjanganPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                VldTunjanganPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = TunjanganPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = TunjanganPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = TunjanganPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    TunjanganPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (VldTunjangan) to $obj2 (Tunjangan)
                $obj2->addVldTunjanganRelatedByTunjanganId($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of VldTunjangan objects pre-filled with their Tunjangan objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of VldTunjangan objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinTunjanganRelatedByTunjanganId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(VldTunjanganPeer::DATABASE_NAME);
        }

        VldTunjanganPeer::addSelectColumns($criteria);
        $startcol = VldTunjanganPeer::NUM_HYDRATE_COLUMNS;
        TunjanganPeer::addSelectColumns($criteria);

        $criteria->addJoin(VldTunjanganPeer::TUNJANGAN_ID, TunjanganPeer::TUNJANGAN_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = VldTunjanganPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = VldTunjanganPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = VldTunjanganPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                VldTunjanganPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = TunjanganPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = TunjanganPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = TunjanganPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    TunjanganPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (VldTunjangan) to $obj2 (Tunjangan)
                $obj2->addVldTunjanganRelatedByTunjanganId($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of VldTunjangan objects pre-filled with their Errortype objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of VldTunjangan objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinErrortypeRelatedByIdtype(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(VldTunjanganPeer::DATABASE_NAME);
        }

        VldTunjanganPeer::addSelectColumns($criteria);
        $startcol = VldTunjanganPeer::NUM_HYDRATE_COLUMNS;
        ErrortypePeer::addSelectColumns($criteria);

        $criteria->addJoin(VldTunjanganPeer::IDTYPE, ErrortypePeer::IDTYPE, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = VldTunjanganPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = VldTunjanganPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = VldTunjanganPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                VldTunjanganPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = ErrortypePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = ErrortypePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = ErrortypePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    ErrortypePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (VldTunjangan) to $obj2 (Errortype)
                $obj2->addVldTunjanganRelatedByIdtype($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of VldTunjangan objects pre-filled with their Errortype objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of VldTunjangan objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinErrortypeRelatedByIdtype(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(VldTunjanganPeer::DATABASE_NAME);
        }

        VldTunjanganPeer::addSelectColumns($criteria);
        $startcol = VldTunjanganPeer::NUM_HYDRATE_COLUMNS;
        ErrortypePeer::addSelectColumns($criteria);

        $criteria->addJoin(VldTunjanganPeer::IDTYPE, ErrortypePeer::IDTYPE, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = VldTunjanganPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = VldTunjanganPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = VldTunjanganPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                VldTunjanganPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = ErrortypePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = ErrortypePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = ErrortypePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    ErrortypePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (VldTunjangan) to $obj2 (Errortype)
                $obj2->addVldTunjanganRelatedByIdtype($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(VldTunjanganPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            VldTunjanganPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(VldTunjanganPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(VldTunjanganPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(VldTunjanganPeer::TUNJANGAN_ID, TunjanganPeer::TUNJANGAN_ID, $join_behavior);

        $criteria->addJoin(VldTunjanganPeer::TUNJANGAN_ID, TunjanganPeer::TUNJANGAN_ID, $join_behavior);

        $criteria->addJoin(VldTunjanganPeer::IDTYPE, ErrortypePeer::IDTYPE, $join_behavior);

        $criteria->addJoin(VldTunjanganPeer::IDTYPE, ErrortypePeer::IDTYPE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of VldTunjangan objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of VldTunjangan objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(VldTunjanganPeer::DATABASE_NAME);
        }

        VldTunjanganPeer::addSelectColumns($criteria);
        $startcol2 = VldTunjanganPeer::NUM_HYDRATE_COLUMNS;

        TunjanganPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + TunjanganPeer::NUM_HYDRATE_COLUMNS;

        TunjanganPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + TunjanganPeer::NUM_HYDRATE_COLUMNS;

        ErrortypePeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + ErrortypePeer::NUM_HYDRATE_COLUMNS;

        ErrortypePeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + ErrortypePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(VldTunjanganPeer::TUNJANGAN_ID, TunjanganPeer::TUNJANGAN_ID, $join_behavior);

        $criteria->addJoin(VldTunjanganPeer::TUNJANGAN_ID, TunjanganPeer::TUNJANGAN_ID, $join_behavior);

        $criteria->addJoin(VldTunjanganPeer::IDTYPE, ErrortypePeer::IDTYPE, $join_behavior);

        $criteria->addJoin(VldTunjanganPeer::IDTYPE, ErrortypePeer::IDTYPE, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = VldTunjanganPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = VldTunjanganPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = VldTunjanganPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                VldTunjanganPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined Tunjangan rows

            $key2 = TunjanganPeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = TunjanganPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = TunjanganPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    TunjanganPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (VldTunjangan) to the collection in $obj2 (Tunjangan)
                $obj2->addVldTunjanganRelatedByTunjanganId($obj1);
            } // if joined row not null

            // Add objects for joined Tunjangan rows

            $key3 = TunjanganPeer::getPrimaryKeyHashFromRow($row, $startcol3);
            if ($key3 !== null) {
                $obj3 = TunjanganPeer::getInstanceFromPool($key3);
                if (!$obj3) {

                    $cls = TunjanganPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    TunjanganPeer::addInstanceToPool($obj3, $key3);
                } // if obj3 loaded

                // Add the $obj1 (VldTunjangan) to the collection in $obj3 (Tunjangan)
                $obj3->addVldTunjanganRelatedByTunjanganId($obj1);
            } // if joined row not null

            // Add objects for joined Errortype rows

            $key4 = ErrortypePeer::getPrimaryKeyHashFromRow($row, $startcol4);
            if ($key4 !== null) {
                $obj4 = ErrortypePeer::getInstanceFromPool($key4);
                if (!$obj4) {

                    $cls = ErrortypePeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    ErrortypePeer::addInstanceToPool($obj4, $key4);
                } // if obj4 loaded

                // Add the $obj1 (VldTunjangan) to the collection in $obj4 (Errortype)
                $obj4->addVldTunjanganRelatedByIdtype($obj1);
            } // if joined row not null

            // Add objects for joined Errortype rows

            $key5 = ErrortypePeer::getPrimaryKeyHashFromRow($row, $startcol5);
            if ($key5 !== null) {
                $obj5 = ErrortypePeer::getInstanceFromPool($key5);
                if (!$obj5) {

                    $cls = ErrortypePeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    ErrortypePeer::addInstanceToPool($obj5, $key5);
                } // if obj5 loaded

                // Add the $obj1 (VldTunjangan) to the collection in $obj5 (Errortype)
                $obj5->addVldTunjanganRelatedByIdtype($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining the related TunjanganRelatedByTunjanganId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptTunjanganRelatedByTunjanganId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(VldTunjanganPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            VldTunjanganPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(VldTunjanganPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(VldTunjanganPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addJoin(VldTunjanganPeer::IDTYPE, ErrortypePeer::IDTYPE, $join_behavior);

        $criteria->addJoin(VldTunjanganPeer::IDTYPE, ErrortypePeer::IDTYPE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related TunjanganRelatedByTunjanganId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptTunjanganRelatedByTunjanganId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(VldTunjanganPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            VldTunjanganPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(VldTunjanganPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(VldTunjanganPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addJoin(VldTunjanganPeer::IDTYPE, ErrortypePeer::IDTYPE, $join_behavior);

        $criteria->addJoin(VldTunjanganPeer::IDTYPE, ErrortypePeer::IDTYPE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related ErrortypeRelatedByIdtype table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptErrortypeRelatedByIdtype(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(VldTunjanganPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            VldTunjanganPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(VldTunjanganPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(VldTunjanganPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addJoin(VldTunjanganPeer::TUNJANGAN_ID, TunjanganPeer::TUNJANGAN_ID, $join_behavior);

        $criteria->addJoin(VldTunjanganPeer::TUNJANGAN_ID, TunjanganPeer::TUNJANGAN_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related ErrortypeRelatedByIdtype table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptErrortypeRelatedByIdtype(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(VldTunjanganPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            VldTunjanganPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(VldTunjanganPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(VldTunjanganPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addJoin(VldTunjanganPeer::TUNJANGAN_ID, TunjanganPeer::TUNJANGAN_ID, $join_behavior);

        $criteria->addJoin(VldTunjanganPeer::TUNJANGAN_ID, TunjanganPeer::TUNJANGAN_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of VldTunjangan objects pre-filled with all related objects except TunjanganRelatedByTunjanganId.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of VldTunjangan objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptTunjanganRelatedByTunjanganId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(VldTunjanganPeer::DATABASE_NAME);
        }

        VldTunjanganPeer::addSelectColumns($criteria);
        $startcol2 = VldTunjanganPeer::NUM_HYDRATE_COLUMNS;

        ErrortypePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + ErrortypePeer::NUM_HYDRATE_COLUMNS;

        ErrortypePeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + ErrortypePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(VldTunjanganPeer::IDTYPE, ErrortypePeer::IDTYPE, $join_behavior);

        $criteria->addJoin(VldTunjanganPeer::IDTYPE, ErrortypePeer::IDTYPE, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = VldTunjanganPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = VldTunjanganPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = VldTunjanganPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                VldTunjanganPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Errortype rows

                $key2 = ErrortypePeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = ErrortypePeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = ErrortypePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    ErrortypePeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (VldTunjangan) to the collection in $obj2 (Errortype)
                $obj2->addVldTunjanganRelatedByIdtype($obj1);

            } // if joined row is not null

                // Add objects for joined Errortype rows

                $key3 = ErrortypePeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = ErrortypePeer::getInstanceFromPool($key3);
                    if (!$obj3) {
    
                        $cls = ErrortypePeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    ErrortypePeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (VldTunjangan) to the collection in $obj3 (Errortype)
                $obj3->addVldTunjanganRelatedByIdtype($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of VldTunjangan objects pre-filled with all related objects except TunjanganRelatedByTunjanganId.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of VldTunjangan objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptTunjanganRelatedByTunjanganId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(VldTunjanganPeer::DATABASE_NAME);
        }

        VldTunjanganPeer::addSelectColumns($criteria);
        $startcol2 = VldTunjanganPeer::NUM_HYDRATE_COLUMNS;

        ErrortypePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + ErrortypePeer::NUM_HYDRATE_COLUMNS;

        ErrortypePeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + ErrortypePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(VldTunjanganPeer::IDTYPE, ErrortypePeer::IDTYPE, $join_behavior);

        $criteria->addJoin(VldTunjanganPeer::IDTYPE, ErrortypePeer::IDTYPE, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = VldTunjanganPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = VldTunjanganPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = VldTunjanganPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                VldTunjanganPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Errortype rows

                $key2 = ErrortypePeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = ErrortypePeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = ErrortypePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    ErrortypePeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (VldTunjangan) to the collection in $obj2 (Errortype)
                $obj2->addVldTunjanganRelatedByIdtype($obj1);

            } // if joined row is not null

                // Add objects for joined Errortype rows

                $key3 = ErrortypePeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = ErrortypePeer::getInstanceFromPool($key3);
                    if (!$obj3) {
    
                        $cls = ErrortypePeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    ErrortypePeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (VldTunjangan) to the collection in $obj3 (Errortype)
                $obj3->addVldTunjanganRelatedByIdtype($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of VldTunjangan objects pre-filled with all related objects except ErrortypeRelatedByIdtype.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of VldTunjangan objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptErrortypeRelatedByIdtype(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(VldTunjanganPeer::DATABASE_NAME);
        }

        VldTunjanganPeer::addSelectColumns($criteria);
        $startcol2 = VldTunjanganPeer::NUM_HYDRATE_COLUMNS;

        TunjanganPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + TunjanganPeer::NUM_HYDRATE_COLUMNS;

        TunjanganPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + TunjanganPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(VldTunjanganPeer::TUNJANGAN_ID, TunjanganPeer::TUNJANGAN_ID, $join_behavior);

        $criteria->addJoin(VldTunjanganPeer::TUNJANGAN_ID, TunjanganPeer::TUNJANGAN_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = VldTunjanganPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = VldTunjanganPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = VldTunjanganPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                VldTunjanganPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Tunjangan rows

                $key2 = TunjanganPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = TunjanganPeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = TunjanganPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    TunjanganPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (VldTunjangan) to the collection in $obj2 (Tunjangan)
                $obj2->addVldTunjanganRelatedByTunjanganId($obj1);

            } // if joined row is not null

                // Add objects for joined Tunjangan rows

                $key3 = TunjanganPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = TunjanganPeer::getInstanceFromPool($key3);
                    if (!$obj3) {
    
                        $cls = TunjanganPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    TunjanganPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (VldTunjangan) to the collection in $obj3 (Tunjangan)
                $obj3->addVldTunjanganRelatedByTunjanganId($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of VldTunjangan objects pre-filled with all related objects except ErrortypeRelatedByIdtype.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of VldTunjangan objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptErrortypeRelatedByIdtype(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(VldTunjanganPeer::DATABASE_NAME);
        }

        VldTunjanganPeer::addSelectColumns($criteria);
        $startcol2 = VldTunjanganPeer::NUM_HYDRATE_COLUMNS;

        TunjanganPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + TunjanganPeer::NUM_HYDRATE_COLUMNS;

        TunjanganPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + TunjanganPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(VldTunjanganPeer::TUNJANGAN_ID, TunjanganPeer::TUNJANGAN_ID, $join_behavior);

        $criteria->addJoin(VldTunjanganPeer::TUNJANGAN_ID, TunjanganPeer::TUNJANGAN_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = VldTunjanganPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = VldTunjanganPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = VldTunjanganPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                VldTunjanganPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Tunjangan rows

                $key2 = TunjanganPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = TunjanganPeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = TunjanganPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    TunjanganPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (VldTunjangan) to the collection in $obj2 (Tunjangan)
                $obj2->addVldTunjanganRelatedByTunjanganId($obj1);

            } // if joined row is not null

                // Add objects for joined Tunjangan rows

                $key3 = TunjanganPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = TunjanganPeer::getInstanceFromPool($key3);
                    if (!$obj3) {
    
                        $cls = TunjanganPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    TunjanganPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (VldTunjangan) to the collection in $obj3 (Tunjangan)
                $obj3->addVldTunjanganRelatedByTunjanganId($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(VldTunjanganPeer::DATABASE_NAME)->getTable(VldTunjanganPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseVldTunjanganPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseVldTunjanganPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new VldTunjanganTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass()
    {
        return VldTunjanganPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a VldTunjangan or Criteria object.
     *
     * @param      mixed $values Criteria or VldTunjangan object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(VldTunjanganPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from VldTunjangan object
        }


        // Set the correct dbName
        $criteria->setDbName(VldTunjanganPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a VldTunjangan or Criteria object.
     *
     * @param      mixed $values Criteria or VldTunjangan object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(VldTunjanganPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(VldTunjanganPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(VldTunjanganPeer::TUNJANGAN_ID);
            $value = $criteria->remove(VldTunjanganPeer::TUNJANGAN_ID);
            if ($value) {
                $selectCriteria->add(VldTunjanganPeer::TUNJANGAN_ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(VldTunjanganPeer::TABLE_NAME);
            }

            $comparison = $criteria->getComparison(VldTunjanganPeer::LOGID);
            $value = $criteria->remove(VldTunjanganPeer::LOGID);
            if ($value) {
                $selectCriteria->add(VldTunjanganPeer::LOGID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(VldTunjanganPeer::TABLE_NAME);
            }

        } else { // $values is VldTunjangan object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(VldTunjanganPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the vld_tunjangan table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(VldTunjanganPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(VldTunjanganPeer::TABLE_NAME, $con, VldTunjanganPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            VldTunjanganPeer::clearInstancePool();
            VldTunjanganPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a VldTunjangan or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or VldTunjangan object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(VldTunjanganPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            VldTunjanganPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof VldTunjangan) { // it's a model object
            // invalidate the cache for this single object
            VldTunjanganPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(VldTunjanganPeer::DATABASE_NAME);
            // primary key is composite; we therefore, expect
            // the primary key passed to be an array of pkey values
            if (count($values) == count($values, COUNT_RECURSIVE)) {
                // array is not multi-dimensional
                $values = array($values);
            }
            foreach ($values as $value) {
                $criterion = $criteria->getNewCriterion(VldTunjanganPeer::TUNJANGAN_ID, $value[0]);
                $criterion->addAnd($criteria->getNewCriterion(VldTunjanganPeer::LOGID, $value[1]));
                $criteria->addOr($criterion);
                // we can invalidate the cache for this single PK
                VldTunjanganPeer::removeInstanceFromPool($value);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(VldTunjanganPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            
            $affectedRows += BasePeer::doDelete($criteria, $con);
            VldTunjanganPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given VldTunjangan object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      VldTunjangan $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(VldTunjanganPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(VldTunjanganPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(VldTunjanganPeer::DATABASE_NAME, VldTunjanganPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve object using using composite pkey values.
     * @param   string $tunjangan_id
     * @param   string $logid
     * @param      PropelPDO $con
     * @return   VldTunjangan
     */
    public static function retrieveByPK($tunjangan_id, $logid, PropelPDO $con = null) {
        $_instancePoolKey = serialize(array((string) $tunjangan_id, (string) $logid));
         if (null !== ($obj = VldTunjanganPeer::getInstanceFromPool($_instancePoolKey))) {
             return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(VldTunjanganPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $criteria = new Criteria(VldTunjanganPeer::DATABASE_NAME);
        $criteria->add(VldTunjanganPeer::TUNJANGAN_ID, $tunjangan_id);
        $criteria->add(VldTunjanganPeer::LOGID, $logid);
        $v = VldTunjanganPeer::doSelect($criteria, $con);

        return !empty($v) ? $v[0] : null;
    }
} // BaseVldTunjanganPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseVldTunjanganPeer::buildTableMap();

