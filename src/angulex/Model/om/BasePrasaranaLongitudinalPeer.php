<?php

namespace angulex\Model\om;

use \BasePeer;
use \Criteria;
use \PDO;
use \PDOStatement;
use \Propel;
use \PropelException;
use \PropelPDO;
use angulex\Model\PrasaranaLongitudinal;
use angulex\Model\PrasaranaLongitudinalPeer;
use angulex\Model\PrasaranaPeer;
use angulex\Model\SemesterPeer;
use angulex\Model\map\PrasaranaLongitudinalTableMap;

/**
 * Base static class for performing query and update operations on the 'prasarana_longitudinal' table.
 *
 * 
 *
 * @package propel.generator.angulex.Model.om
 */
abstract class BasePrasaranaLongitudinalPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'Dapodikmen';

    /** the table name for this class */
    const TABLE_NAME = 'prasarana_longitudinal';

    /** the related Propel class for this table */
    const OM_CLASS = 'angulex\\Model\\PrasaranaLongitudinal';

    /** the related TableMap class for this table */
    const TM_CLASS = 'PrasaranaLongitudinalTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 25;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 25;

    /** the column name for the prasarana_id field */
    const PRASARANA_ID = 'prasarana_longitudinal.prasarana_id';

    /** the column name for the semester_id field */
    const SEMESTER_ID = 'prasarana_longitudinal.semester_id';

    /** the column name for the rusak_penutup_atap field */
    const RUSAK_PENUTUP_ATAP = 'prasarana_longitudinal.rusak_penutup_atap';

    /** the column name for the rusak_rangka_atap field */
    const RUSAK_RANGKA_ATAP = 'prasarana_longitudinal.rusak_rangka_atap';

    /** the column name for the rusak_lisplang_talang field */
    const RUSAK_LISPLANG_TALANG = 'prasarana_longitudinal.rusak_lisplang_talang';

    /** the column name for the rusak_rangka_plafon field */
    const RUSAK_RANGKA_PLAFON = 'prasarana_longitudinal.rusak_rangka_plafon';

    /** the column name for the rusak_penutup_listplafon field */
    const RUSAK_PENUTUP_LISTPLAFON = 'prasarana_longitudinal.rusak_penutup_listplafon';

    /** the column name for the rusak_cat_plafon field */
    const RUSAK_CAT_PLAFON = 'prasarana_longitudinal.rusak_cat_plafon';

    /** the column name for the rusak_kolom_ringbalok field */
    const RUSAK_KOLOM_RINGBALOK = 'prasarana_longitudinal.rusak_kolom_ringbalok';

    /** the column name for the rusak_bata_dindingpengisi field */
    const RUSAK_BATA_DINDINGPENGISI = 'prasarana_longitudinal.rusak_bata_dindingpengisi';

    /** the column name for the rusak_cat_dinding field */
    const RUSAK_CAT_DINDING = 'prasarana_longitudinal.rusak_cat_dinding';

    /** the column name for the rusak_kusen field */
    const RUSAK_KUSEN = 'prasarana_longitudinal.rusak_kusen';

    /** the column name for the rusak_daun_pintu field */
    const RUSAK_DAUN_PINTU = 'prasarana_longitudinal.rusak_daun_pintu';

    /** the column name for the rusak_daun_jendela field */
    const RUSAK_DAUN_JENDELA = 'prasarana_longitudinal.rusak_daun_jendela';

    /** the column name for the rusak_struktur_bawah field */
    const RUSAK_STRUKTUR_BAWAH = 'prasarana_longitudinal.rusak_struktur_bawah';

    /** the column name for the rusak_penutup_lantai field */
    const RUSAK_PENUTUP_LANTAI = 'prasarana_longitudinal.rusak_penutup_lantai';

    /** the column name for the rusak_pondasi field */
    const RUSAK_PONDASI = 'prasarana_longitudinal.rusak_pondasi';

    /** the column name for the rusak_sloof field */
    const RUSAK_SLOOF = 'prasarana_longitudinal.rusak_sloof';

    /** the column name for the rusak_listrik field */
    const RUSAK_LISTRIK = 'prasarana_longitudinal.rusak_listrik';

    /** the column name for the rusak_airhujan_rabatan field */
    const RUSAK_AIRHUJAN_RABATAN = 'prasarana_longitudinal.rusak_airhujan_rabatan';

    /** the column name for the blob_id field */
    const BLOB_ID = 'prasarana_longitudinal.blob_id';

    /** the column name for the Last_update field */
    const LAST_UPDATE = 'prasarana_longitudinal.Last_update';

    /** the column name for the Soft_delete field */
    const SOFT_DELETE = 'prasarana_longitudinal.Soft_delete';

    /** the column name for the last_sync field */
    const LAST_SYNC = 'prasarana_longitudinal.last_sync';

    /** the column name for the Updater_ID field */
    const UPDATER_ID = 'prasarana_longitudinal.Updater_ID';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identiy map to hold any loaded instances of PrasaranaLongitudinal objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array PrasaranaLongitudinal[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. PrasaranaLongitudinalPeer::$fieldNames[PrasaranaLongitudinalPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('PrasaranaId', 'SemesterId', 'RusakPenutupAtap', 'RusakRangkaAtap', 'RusakLisplangTalang', 'RusakRangkaPlafon', 'RusakPenutupListplafon', 'RusakCatPlafon', 'RusakKolomRingbalok', 'RusakBataDindingpengisi', 'RusakCatDinding', 'RusakKusen', 'RusakDaunPintu', 'RusakDaunJendela', 'RusakStrukturBawah', 'RusakPenutupLantai', 'RusakPondasi', 'RusakSloof', 'RusakListrik', 'RusakAirhujanRabatan', 'BlobId', 'LastUpdate', 'SoftDelete', 'LastSync', 'UpdaterId', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('prasaranaId', 'semesterId', 'rusakPenutupAtap', 'rusakRangkaAtap', 'rusakLisplangTalang', 'rusakRangkaPlafon', 'rusakPenutupListplafon', 'rusakCatPlafon', 'rusakKolomRingbalok', 'rusakBataDindingpengisi', 'rusakCatDinding', 'rusakKusen', 'rusakDaunPintu', 'rusakDaunJendela', 'rusakStrukturBawah', 'rusakPenutupLantai', 'rusakPondasi', 'rusakSloof', 'rusakListrik', 'rusakAirhujanRabatan', 'blobId', 'lastUpdate', 'softDelete', 'lastSync', 'updaterId', ),
        BasePeer::TYPE_COLNAME => array (PrasaranaLongitudinalPeer::PRASARANA_ID, PrasaranaLongitudinalPeer::SEMESTER_ID, PrasaranaLongitudinalPeer::RUSAK_PENUTUP_ATAP, PrasaranaLongitudinalPeer::RUSAK_RANGKA_ATAP, PrasaranaLongitudinalPeer::RUSAK_LISPLANG_TALANG, PrasaranaLongitudinalPeer::RUSAK_RANGKA_PLAFON, PrasaranaLongitudinalPeer::RUSAK_PENUTUP_LISTPLAFON, PrasaranaLongitudinalPeer::RUSAK_CAT_PLAFON, PrasaranaLongitudinalPeer::RUSAK_KOLOM_RINGBALOK, PrasaranaLongitudinalPeer::RUSAK_BATA_DINDINGPENGISI, PrasaranaLongitudinalPeer::RUSAK_CAT_DINDING, PrasaranaLongitudinalPeer::RUSAK_KUSEN, PrasaranaLongitudinalPeer::RUSAK_DAUN_PINTU, PrasaranaLongitudinalPeer::RUSAK_DAUN_JENDELA, PrasaranaLongitudinalPeer::RUSAK_STRUKTUR_BAWAH, PrasaranaLongitudinalPeer::RUSAK_PENUTUP_LANTAI, PrasaranaLongitudinalPeer::RUSAK_PONDASI, PrasaranaLongitudinalPeer::RUSAK_SLOOF, PrasaranaLongitudinalPeer::RUSAK_LISTRIK, PrasaranaLongitudinalPeer::RUSAK_AIRHUJAN_RABATAN, PrasaranaLongitudinalPeer::BLOB_ID, PrasaranaLongitudinalPeer::LAST_UPDATE, PrasaranaLongitudinalPeer::SOFT_DELETE, PrasaranaLongitudinalPeer::LAST_SYNC, PrasaranaLongitudinalPeer::UPDATER_ID, ),
        BasePeer::TYPE_RAW_COLNAME => array ('PRASARANA_ID', 'SEMESTER_ID', 'RUSAK_PENUTUP_ATAP', 'RUSAK_RANGKA_ATAP', 'RUSAK_LISPLANG_TALANG', 'RUSAK_RANGKA_PLAFON', 'RUSAK_PENUTUP_LISTPLAFON', 'RUSAK_CAT_PLAFON', 'RUSAK_KOLOM_RINGBALOK', 'RUSAK_BATA_DINDINGPENGISI', 'RUSAK_CAT_DINDING', 'RUSAK_KUSEN', 'RUSAK_DAUN_PINTU', 'RUSAK_DAUN_JENDELA', 'RUSAK_STRUKTUR_BAWAH', 'RUSAK_PENUTUP_LANTAI', 'RUSAK_PONDASI', 'RUSAK_SLOOF', 'RUSAK_LISTRIK', 'RUSAK_AIRHUJAN_RABATAN', 'BLOB_ID', 'LAST_UPDATE', 'SOFT_DELETE', 'LAST_SYNC', 'UPDATER_ID', ),
        BasePeer::TYPE_FIELDNAME => array ('prasarana_id', 'semester_id', 'rusak_penutup_atap', 'rusak_rangka_atap', 'rusak_lisplang_talang', 'rusak_rangka_plafon', 'rusak_penutup_listplafon', 'rusak_cat_plafon', 'rusak_kolom_ringbalok', 'rusak_bata_dindingpengisi', 'rusak_cat_dinding', 'rusak_kusen', 'rusak_daun_pintu', 'rusak_daun_jendela', 'rusak_struktur_bawah', 'rusak_penutup_lantai', 'rusak_pondasi', 'rusak_sloof', 'rusak_listrik', 'rusak_airhujan_rabatan', 'blob_id', 'Last_update', 'Soft_delete', 'last_sync', 'Updater_ID', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. PrasaranaLongitudinalPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('PrasaranaId' => 0, 'SemesterId' => 1, 'RusakPenutupAtap' => 2, 'RusakRangkaAtap' => 3, 'RusakLisplangTalang' => 4, 'RusakRangkaPlafon' => 5, 'RusakPenutupListplafon' => 6, 'RusakCatPlafon' => 7, 'RusakKolomRingbalok' => 8, 'RusakBataDindingpengisi' => 9, 'RusakCatDinding' => 10, 'RusakKusen' => 11, 'RusakDaunPintu' => 12, 'RusakDaunJendela' => 13, 'RusakStrukturBawah' => 14, 'RusakPenutupLantai' => 15, 'RusakPondasi' => 16, 'RusakSloof' => 17, 'RusakListrik' => 18, 'RusakAirhujanRabatan' => 19, 'BlobId' => 20, 'LastUpdate' => 21, 'SoftDelete' => 22, 'LastSync' => 23, 'UpdaterId' => 24, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('prasaranaId' => 0, 'semesterId' => 1, 'rusakPenutupAtap' => 2, 'rusakRangkaAtap' => 3, 'rusakLisplangTalang' => 4, 'rusakRangkaPlafon' => 5, 'rusakPenutupListplafon' => 6, 'rusakCatPlafon' => 7, 'rusakKolomRingbalok' => 8, 'rusakBataDindingpengisi' => 9, 'rusakCatDinding' => 10, 'rusakKusen' => 11, 'rusakDaunPintu' => 12, 'rusakDaunJendela' => 13, 'rusakStrukturBawah' => 14, 'rusakPenutupLantai' => 15, 'rusakPondasi' => 16, 'rusakSloof' => 17, 'rusakListrik' => 18, 'rusakAirhujanRabatan' => 19, 'blobId' => 20, 'lastUpdate' => 21, 'softDelete' => 22, 'lastSync' => 23, 'updaterId' => 24, ),
        BasePeer::TYPE_COLNAME => array (PrasaranaLongitudinalPeer::PRASARANA_ID => 0, PrasaranaLongitudinalPeer::SEMESTER_ID => 1, PrasaranaLongitudinalPeer::RUSAK_PENUTUP_ATAP => 2, PrasaranaLongitudinalPeer::RUSAK_RANGKA_ATAP => 3, PrasaranaLongitudinalPeer::RUSAK_LISPLANG_TALANG => 4, PrasaranaLongitudinalPeer::RUSAK_RANGKA_PLAFON => 5, PrasaranaLongitudinalPeer::RUSAK_PENUTUP_LISTPLAFON => 6, PrasaranaLongitudinalPeer::RUSAK_CAT_PLAFON => 7, PrasaranaLongitudinalPeer::RUSAK_KOLOM_RINGBALOK => 8, PrasaranaLongitudinalPeer::RUSAK_BATA_DINDINGPENGISI => 9, PrasaranaLongitudinalPeer::RUSAK_CAT_DINDING => 10, PrasaranaLongitudinalPeer::RUSAK_KUSEN => 11, PrasaranaLongitudinalPeer::RUSAK_DAUN_PINTU => 12, PrasaranaLongitudinalPeer::RUSAK_DAUN_JENDELA => 13, PrasaranaLongitudinalPeer::RUSAK_STRUKTUR_BAWAH => 14, PrasaranaLongitudinalPeer::RUSAK_PENUTUP_LANTAI => 15, PrasaranaLongitudinalPeer::RUSAK_PONDASI => 16, PrasaranaLongitudinalPeer::RUSAK_SLOOF => 17, PrasaranaLongitudinalPeer::RUSAK_LISTRIK => 18, PrasaranaLongitudinalPeer::RUSAK_AIRHUJAN_RABATAN => 19, PrasaranaLongitudinalPeer::BLOB_ID => 20, PrasaranaLongitudinalPeer::LAST_UPDATE => 21, PrasaranaLongitudinalPeer::SOFT_DELETE => 22, PrasaranaLongitudinalPeer::LAST_SYNC => 23, PrasaranaLongitudinalPeer::UPDATER_ID => 24, ),
        BasePeer::TYPE_RAW_COLNAME => array ('PRASARANA_ID' => 0, 'SEMESTER_ID' => 1, 'RUSAK_PENUTUP_ATAP' => 2, 'RUSAK_RANGKA_ATAP' => 3, 'RUSAK_LISPLANG_TALANG' => 4, 'RUSAK_RANGKA_PLAFON' => 5, 'RUSAK_PENUTUP_LISTPLAFON' => 6, 'RUSAK_CAT_PLAFON' => 7, 'RUSAK_KOLOM_RINGBALOK' => 8, 'RUSAK_BATA_DINDINGPENGISI' => 9, 'RUSAK_CAT_DINDING' => 10, 'RUSAK_KUSEN' => 11, 'RUSAK_DAUN_PINTU' => 12, 'RUSAK_DAUN_JENDELA' => 13, 'RUSAK_STRUKTUR_BAWAH' => 14, 'RUSAK_PENUTUP_LANTAI' => 15, 'RUSAK_PONDASI' => 16, 'RUSAK_SLOOF' => 17, 'RUSAK_LISTRIK' => 18, 'RUSAK_AIRHUJAN_RABATAN' => 19, 'BLOB_ID' => 20, 'LAST_UPDATE' => 21, 'SOFT_DELETE' => 22, 'LAST_SYNC' => 23, 'UPDATER_ID' => 24, ),
        BasePeer::TYPE_FIELDNAME => array ('prasarana_id' => 0, 'semester_id' => 1, 'rusak_penutup_atap' => 2, 'rusak_rangka_atap' => 3, 'rusak_lisplang_talang' => 4, 'rusak_rangka_plafon' => 5, 'rusak_penutup_listplafon' => 6, 'rusak_cat_plafon' => 7, 'rusak_kolom_ringbalok' => 8, 'rusak_bata_dindingpengisi' => 9, 'rusak_cat_dinding' => 10, 'rusak_kusen' => 11, 'rusak_daun_pintu' => 12, 'rusak_daun_jendela' => 13, 'rusak_struktur_bawah' => 14, 'rusak_penutup_lantai' => 15, 'rusak_pondasi' => 16, 'rusak_sloof' => 17, 'rusak_listrik' => 18, 'rusak_airhujan_rabatan' => 19, 'blob_id' => 20, 'Last_update' => 21, 'Soft_delete' => 22, 'last_sync' => 23, 'Updater_ID' => 24, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = PrasaranaLongitudinalPeer::getFieldNames($toType);
        $key = isset(PrasaranaLongitudinalPeer::$fieldKeys[$fromType][$name]) ? PrasaranaLongitudinalPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(PrasaranaLongitudinalPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, PrasaranaLongitudinalPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return PrasaranaLongitudinalPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. PrasaranaLongitudinalPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(PrasaranaLongitudinalPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::PRASARANA_ID);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::SEMESTER_ID);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::RUSAK_PENUTUP_ATAP);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::RUSAK_RANGKA_ATAP);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::RUSAK_LISPLANG_TALANG);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::RUSAK_RANGKA_PLAFON);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::RUSAK_PENUTUP_LISTPLAFON);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::RUSAK_CAT_PLAFON);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::RUSAK_KOLOM_RINGBALOK);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::RUSAK_BATA_DINDINGPENGISI);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::RUSAK_CAT_DINDING);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::RUSAK_KUSEN);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::RUSAK_DAUN_PINTU);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::RUSAK_DAUN_JENDELA);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::RUSAK_STRUKTUR_BAWAH);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::RUSAK_PENUTUP_LANTAI);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::RUSAK_PONDASI);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::RUSAK_SLOOF);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::RUSAK_LISTRIK);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::RUSAK_AIRHUJAN_RABATAN);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::BLOB_ID);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::LAST_UPDATE);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::SOFT_DELETE);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::LAST_SYNC);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::UPDATER_ID);
        } else {
            $criteria->addSelectColumn($alias . '.prasarana_id');
            $criteria->addSelectColumn($alias . '.semester_id');
            $criteria->addSelectColumn($alias . '.rusak_penutup_atap');
            $criteria->addSelectColumn($alias . '.rusak_rangka_atap');
            $criteria->addSelectColumn($alias . '.rusak_lisplang_talang');
            $criteria->addSelectColumn($alias . '.rusak_rangka_plafon');
            $criteria->addSelectColumn($alias . '.rusak_penutup_listplafon');
            $criteria->addSelectColumn($alias . '.rusak_cat_plafon');
            $criteria->addSelectColumn($alias . '.rusak_kolom_ringbalok');
            $criteria->addSelectColumn($alias . '.rusak_bata_dindingpengisi');
            $criteria->addSelectColumn($alias . '.rusak_cat_dinding');
            $criteria->addSelectColumn($alias . '.rusak_kusen');
            $criteria->addSelectColumn($alias . '.rusak_daun_pintu');
            $criteria->addSelectColumn($alias . '.rusak_daun_jendela');
            $criteria->addSelectColumn($alias . '.rusak_struktur_bawah');
            $criteria->addSelectColumn($alias . '.rusak_penutup_lantai');
            $criteria->addSelectColumn($alias . '.rusak_pondasi');
            $criteria->addSelectColumn($alias . '.rusak_sloof');
            $criteria->addSelectColumn($alias . '.rusak_listrik');
            $criteria->addSelectColumn($alias . '.rusak_airhujan_rabatan');
            $criteria->addSelectColumn($alias . '.blob_id');
            $criteria->addSelectColumn($alias . '.Last_update');
            $criteria->addSelectColumn($alias . '.Soft_delete');
            $criteria->addSelectColumn($alias . '.last_sync');
            $criteria->addSelectColumn($alias . '.Updater_ID');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(PrasaranaLongitudinalPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            PrasaranaLongitudinalPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(PrasaranaLongitudinalPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(PrasaranaLongitudinalPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 PrasaranaLongitudinal
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = PrasaranaLongitudinalPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return PrasaranaLongitudinalPeer::populateObjects(PrasaranaLongitudinalPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(PrasaranaLongitudinalPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            PrasaranaLongitudinalPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(PrasaranaLongitudinalPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      PrasaranaLongitudinal $obj A PrasaranaLongitudinal object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = serialize(array((string) $obj->getPrasaranaId(), (string) $obj->getSemesterId()));
            } // if key === null
            PrasaranaLongitudinalPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A PrasaranaLongitudinal object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof PrasaranaLongitudinal) {
                $key = serialize(array((string) $value->getPrasaranaId(), (string) $value->getSemesterId()));
            } elseif (is_array($value) && count($value) === 2) {
                // assume we've been passed a primary key
                $key = serialize(array((string) $value[0], (string) $value[1]));
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or PrasaranaLongitudinal object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(PrasaranaLongitudinalPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   PrasaranaLongitudinal Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(PrasaranaLongitudinalPeer::$instances[$key])) {
                return PrasaranaLongitudinalPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }
    
    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references)
      {
        foreach (PrasaranaLongitudinalPeer::$instances as $instance)
        {
          $instance->clearAllReferences(true);
        }
      }
        PrasaranaLongitudinalPeer::$instances = array();
    }
    
    /**
     * Method to invalidate the instance pool of all tables related to prasarana_longitudinal
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null && $row[$startcol + 1] === null) {
            return null;
        }

        return serialize(array((string) $row[$startcol], (string) $row[$startcol + 1]));
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return array((string) $row[$startcol], (string) $row[$startcol + 1]);
    }
    
    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();
    
        // set the class once to avoid overhead in the loop
        $cls = PrasaranaLongitudinalPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = PrasaranaLongitudinalPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = PrasaranaLongitudinalPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                PrasaranaLongitudinalPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (PrasaranaLongitudinal object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = PrasaranaLongitudinalPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = PrasaranaLongitudinalPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + PrasaranaLongitudinalPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = PrasaranaLongitudinalPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            PrasaranaLongitudinalPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related PrasaranaRelatedByPrasaranaId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinPrasaranaRelatedByPrasaranaId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(PrasaranaLongitudinalPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            PrasaranaLongitudinalPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(PrasaranaLongitudinalPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(PrasaranaLongitudinalPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(PrasaranaLongitudinalPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related PrasaranaRelatedByPrasaranaId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinPrasaranaRelatedByPrasaranaId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(PrasaranaLongitudinalPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            PrasaranaLongitudinalPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(PrasaranaLongitudinalPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(PrasaranaLongitudinalPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(PrasaranaLongitudinalPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SemesterRelatedBySemesterId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinSemesterRelatedBySemesterId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(PrasaranaLongitudinalPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            PrasaranaLongitudinalPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(PrasaranaLongitudinalPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(PrasaranaLongitudinalPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(PrasaranaLongitudinalPeer::SEMESTER_ID, SemesterPeer::SEMESTER_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SemesterRelatedBySemesterId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinSemesterRelatedBySemesterId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(PrasaranaLongitudinalPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            PrasaranaLongitudinalPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(PrasaranaLongitudinalPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(PrasaranaLongitudinalPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(PrasaranaLongitudinalPeer::SEMESTER_ID, SemesterPeer::SEMESTER_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of PrasaranaLongitudinal objects pre-filled with their Prasarana objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of PrasaranaLongitudinal objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinPrasaranaRelatedByPrasaranaId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(PrasaranaLongitudinalPeer::DATABASE_NAME);
        }

        PrasaranaLongitudinalPeer::addSelectColumns($criteria);
        $startcol = PrasaranaLongitudinalPeer::NUM_HYDRATE_COLUMNS;
        PrasaranaPeer::addSelectColumns($criteria);

        $criteria->addJoin(PrasaranaLongitudinalPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = PrasaranaLongitudinalPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = PrasaranaLongitudinalPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = PrasaranaLongitudinalPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                PrasaranaLongitudinalPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = PrasaranaPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = PrasaranaPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = PrasaranaPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    PrasaranaPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (PrasaranaLongitudinal) to $obj2 (Prasarana)
                $obj2->addPrasaranaLongitudinalRelatedByPrasaranaId($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of PrasaranaLongitudinal objects pre-filled with their Prasarana objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of PrasaranaLongitudinal objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinPrasaranaRelatedByPrasaranaId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(PrasaranaLongitudinalPeer::DATABASE_NAME);
        }

        PrasaranaLongitudinalPeer::addSelectColumns($criteria);
        $startcol = PrasaranaLongitudinalPeer::NUM_HYDRATE_COLUMNS;
        PrasaranaPeer::addSelectColumns($criteria);

        $criteria->addJoin(PrasaranaLongitudinalPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = PrasaranaLongitudinalPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = PrasaranaLongitudinalPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = PrasaranaLongitudinalPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                PrasaranaLongitudinalPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = PrasaranaPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = PrasaranaPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = PrasaranaPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    PrasaranaPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (PrasaranaLongitudinal) to $obj2 (Prasarana)
                $obj2->addPrasaranaLongitudinalRelatedByPrasaranaId($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of PrasaranaLongitudinal objects pre-filled with their Semester objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of PrasaranaLongitudinal objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinSemesterRelatedBySemesterId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(PrasaranaLongitudinalPeer::DATABASE_NAME);
        }

        PrasaranaLongitudinalPeer::addSelectColumns($criteria);
        $startcol = PrasaranaLongitudinalPeer::NUM_HYDRATE_COLUMNS;
        SemesterPeer::addSelectColumns($criteria);

        $criteria->addJoin(PrasaranaLongitudinalPeer::SEMESTER_ID, SemesterPeer::SEMESTER_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = PrasaranaLongitudinalPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = PrasaranaLongitudinalPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = PrasaranaLongitudinalPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                PrasaranaLongitudinalPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = SemesterPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = SemesterPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = SemesterPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    SemesterPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (PrasaranaLongitudinal) to $obj2 (Semester)
                $obj2->addPrasaranaLongitudinalRelatedBySemesterId($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of PrasaranaLongitudinal objects pre-filled with their Semester objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of PrasaranaLongitudinal objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinSemesterRelatedBySemesterId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(PrasaranaLongitudinalPeer::DATABASE_NAME);
        }

        PrasaranaLongitudinalPeer::addSelectColumns($criteria);
        $startcol = PrasaranaLongitudinalPeer::NUM_HYDRATE_COLUMNS;
        SemesterPeer::addSelectColumns($criteria);

        $criteria->addJoin(PrasaranaLongitudinalPeer::SEMESTER_ID, SemesterPeer::SEMESTER_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = PrasaranaLongitudinalPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = PrasaranaLongitudinalPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = PrasaranaLongitudinalPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                PrasaranaLongitudinalPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = SemesterPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = SemesterPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = SemesterPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    SemesterPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (PrasaranaLongitudinal) to $obj2 (Semester)
                $obj2->addPrasaranaLongitudinalRelatedBySemesterId($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(PrasaranaLongitudinalPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            PrasaranaLongitudinalPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(PrasaranaLongitudinalPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(PrasaranaLongitudinalPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(PrasaranaLongitudinalPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(PrasaranaLongitudinalPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(PrasaranaLongitudinalPeer::SEMESTER_ID, SemesterPeer::SEMESTER_ID, $join_behavior);

        $criteria->addJoin(PrasaranaLongitudinalPeer::SEMESTER_ID, SemesterPeer::SEMESTER_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of PrasaranaLongitudinal objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of PrasaranaLongitudinal objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(PrasaranaLongitudinalPeer::DATABASE_NAME);
        }

        PrasaranaLongitudinalPeer::addSelectColumns($criteria);
        $startcol2 = PrasaranaLongitudinalPeer::NUM_HYDRATE_COLUMNS;

        PrasaranaPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + PrasaranaPeer::NUM_HYDRATE_COLUMNS;

        PrasaranaPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + PrasaranaPeer::NUM_HYDRATE_COLUMNS;

        SemesterPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + SemesterPeer::NUM_HYDRATE_COLUMNS;

        SemesterPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + SemesterPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(PrasaranaLongitudinalPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(PrasaranaLongitudinalPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(PrasaranaLongitudinalPeer::SEMESTER_ID, SemesterPeer::SEMESTER_ID, $join_behavior);

        $criteria->addJoin(PrasaranaLongitudinalPeer::SEMESTER_ID, SemesterPeer::SEMESTER_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = PrasaranaLongitudinalPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = PrasaranaLongitudinalPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = PrasaranaLongitudinalPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                PrasaranaLongitudinalPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined Prasarana rows

            $key2 = PrasaranaPeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = PrasaranaPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = PrasaranaPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    PrasaranaPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (PrasaranaLongitudinal) to the collection in $obj2 (Prasarana)
                $obj2->addPrasaranaLongitudinalRelatedByPrasaranaId($obj1);
            } // if joined row not null

            // Add objects for joined Prasarana rows

            $key3 = PrasaranaPeer::getPrimaryKeyHashFromRow($row, $startcol3);
            if ($key3 !== null) {
                $obj3 = PrasaranaPeer::getInstanceFromPool($key3);
                if (!$obj3) {

                    $cls = PrasaranaPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    PrasaranaPeer::addInstanceToPool($obj3, $key3);
                } // if obj3 loaded

                // Add the $obj1 (PrasaranaLongitudinal) to the collection in $obj3 (Prasarana)
                $obj3->addPrasaranaLongitudinalRelatedByPrasaranaId($obj1);
            } // if joined row not null

            // Add objects for joined Semester rows

            $key4 = SemesterPeer::getPrimaryKeyHashFromRow($row, $startcol4);
            if ($key4 !== null) {
                $obj4 = SemesterPeer::getInstanceFromPool($key4);
                if (!$obj4) {

                    $cls = SemesterPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    SemesterPeer::addInstanceToPool($obj4, $key4);
                } // if obj4 loaded

                // Add the $obj1 (PrasaranaLongitudinal) to the collection in $obj4 (Semester)
                $obj4->addPrasaranaLongitudinalRelatedBySemesterId($obj1);
            } // if joined row not null

            // Add objects for joined Semester rows

            $key5 = SemesterPeer::getPrimaryKeyHashFromRow($row, $startcol5);
            if ($key5 !== null) {
                $obj5 = SemesterPeer::getInstanceFromPool($key5);
                if (!$obj5) {

                    $cls = SemesterPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    SemesterPeer::addInstanceToPool($obj5, $key5);
                } // if obj5 loaded

                // Add the $obj1 (PrasaranaLongitudinal) to the collection in $obj5 (Semester)
                $obj5->addPrasaranaLongitudinalRelatedBySemesterId($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining the related PrasaranaRelatedByPrasaranaId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptPrasaranaRelatedByPrasaranaId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(PrasaranaLongitudinalPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            PrasaranaLongitudinalPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(PrasaranaLongitudinalPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(PrasaranaLongitudinalPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addJoin(PrasaranaLongitudinalPeer::SEMESTER_ID, SemesterPeer::SEMESTER_ID, $join_behavior);

        $criteria->addJoin(PrasaranaLongitudinalPeer::SEMESTER_ID, SemesterPeer::SEMESTER_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related PrasaranaRelatedByPrasaranaId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptPrasaranaRelatedByPrasaranaId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(PrasaranaLongitudinalPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            PrasaranaLongitudinalPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(PrasaranaLongitudinalPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(PrasaranaLongitudinalPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addJoin(PrasaranaLongitudinalPeer::SEMESTER_ID, SemesterPeer::SEMESTER_ID, $join_behavior);

        $criteria->addJoin(PrasaranaLongitudinalPeer::SEMESTER_ID, SemesterPeer::SEMESTER_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SemesterRelatedBySemesterId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptSemesterRelatedBySemesterId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(PrasaranaLongitudinalPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            PrasaranaLongitudinalPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(PrasaranaLongitudinalPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(PrasaranaLongitudinalPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addJoin(PrasaranaLongitudinalPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(PrasaranaLongitudinalPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SemesterRelatedBySemesterId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptSemesterRelatedBySemesterId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(PrasaranaLongitudinalPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            PrasaranaLongitudinalPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(PrasaranaLongitudinalPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(PrasaranaLongitudinalPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addJoin(PrasaranaLongitudinalPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(PrasaranaLongitudinalPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of PrasaranaLongitudinal objects pre-filled with all related objects except PrasaranaRelatedByPrasaranaId.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of PrasaranaLongitudinal objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptPrasaranaRelatedByPrasaranaId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(PrasaranaLongitudinalPeer::DATABASE_NAME);
        }

        PrasaranaLongitudinalPeer::addSelectColumns($criteria);
        $startcol2 = PrasaranaLongitudinalPeer::NUM_HYDRATE_COLUMNS;

        SemesterPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SemesterPeer::NUM_HYDRATE_COLUMNS;

        SemesterPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + SemesterPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(PrasaranaLongitudinalPeer::SEMESTER_ID, SemesterPeer::SEMESTER_ID, $join_behavior);

        $criteria->addJoin(PrasaranaLongitudinalPeer::SEMESTER_ID, SemesterPeer::SEMESTER_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = PrasaranaLongitudinalPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = PrasaranaLongitudinalPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = PrasaranaLongitudinalPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                PrasaranaLongitudinalPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Semester rows

                $key2 = SemesterPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = SemesterPeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = SemesterPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SemesterPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (PrasaranaLongitudinal) to the collection in $obj2 (Semester)
                $obj2->addPrasaranaLongitudinalRelatedBySemesterId($obj1);

            } // if joined row is not null

                // Add objects for joined Semester rows

                $key3 = SemesterPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = SemesterPeer::getInstanceFromPool($key3);
                    if (!$obj3) {
    
                        $cls = SemesterPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    SemesterPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (PrasaranaLongitudinal) to the collection in $obj3 (Semester)
                $obj3->addPrasaranaLongitudinalRelatedBySemesterId($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of PrasaranaLongitudinal objects pre-filled with all related objects except PrasaranaRelatedByPrasaranaId.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of PrasaranaLongitudinal objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptPrasaranaRelatedByPrasaranaId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(PrasaranaLongitudinalPeer::DATABASE_NAME);
        }

        PrasaranaLongitudinalPeer::addSelectColumns($criteria);
        $startcol2 = PrasaranaLongitudinalPeer::NUM_HYDRATE_COLUMNS;

        SemesterPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SemesterPeer::NUM_HYDRATE_COLUMNS;

        SemesterPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + SemesterPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(PrasaranaLongitudinalPeer::SEMESTER_ID, SemesterPeer::SEMESTER_ID, $join_behavior);

        $criteria->addJoin(PrasaranaLongitudinalPeer::SEMESTER_ID, SemesterPeer::SEMESTER_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = PrasaranaLongitudinalPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = PrasaranaLongitudinalPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = PrasaranaLongitudinalPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                PrasaranaLongitudinalPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Semester rows

                $key2 = SemesterPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = SemesterPeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = SemesterPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SemesterPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (PrasaranaLongitudinal) to the collection in $obj2 (Semester)
                $obj2->addPrasaranaLongitudinalRelatedBySemesterId($obj1);

            } // if joined row is not null

                // Add objects for joined Semester rows

                $key3 = SemesterPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = SemesterPeer::getInstanceFromPool($key3);
                    if (!$obj3) {
    
                        $cls = SemesterPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    SemesterPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (PrasaranaLongitudinal) to the collection in $obj3 (Semester)
                $obj3->addPrasaranaLongitudinalRelatedBySemesterId($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of PrasaranaLongitudinal objects pre-filled with all related objects except SemesterRelatedBySemesterId.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of PrasaranaLongitudinal objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptSemesterRelatedBySemesterId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(PrasaranaLongitudinalPeer::DATABASE_NAME);
        }

        PrasaranaLongitudinalPeer::addSelectColumns($criteria);
        $startcol2 = PrasaranaLongitudinalPeer::NUM_HYDRATE_COLUMNS;

        PrasaranaPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + PrasaranaPeer::NUM_HYDRATE_COLUMNS;

        PrasaranaPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + PrasaranaPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(PrasaranaLongitudinalPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(PrasaranaLongitudinalPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = PrasaranaLongitudinalPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = PrasaranaLongitudinalPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = PrasaranaLongitudinalPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                PrasaranaLongitudinalPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Prasarana rows

                $key2 = PrasaranaPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = PrasaranaPeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = PrasaranaPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    PrasaranaPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (PrasaranaLongitudinal) to the collection in $obj2 (Prasarana)
                $obj2->addPrasaranaLongitudinalRelatedByPrasaranaId($obj1);

            } // if joined row is not null

                // Add objects for joined Prasarana rows

                $key3 = PrasaranaPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = PrasaranaPeer::getInstanceFromPool($key3);
                    if (!$obj3) {
    
                        $cls = PrasaranaPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    PrasaranaPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (PrasaranaLongitudinal) to the collection in $obj3 (Prasarana)
                $obj3->addPrasaranaLongitudinalRelatedByPrasaranaId($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of PrasaranaLongitudinal objects pre-filled with all related objects except SemesterRelatedBySemesterId.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of PrasaranaLongitudinal objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptSemesterRelatedBySemesterId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(PrasaranaLongitudinalPeer::DATABASE_NAME);
        }

        PrasaranaLongitudinalPeer::addSelectColumns($criteria);
        $startcol2 = PrasaranaLongitudinalPeer::NUM_HYDRATE_COLUMNS;

        PrasaranaPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + PrasaranaPeer::NUM_HYDRATE_COLUMNS;

        PrasaranaPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + PrasaranaPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(PrasaranaLongitudinalPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(PrasaranaLongitudinalPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = PrasaranaLongitudinalPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = PrasaranaLongitudinalPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = PrasaranaLongitudinalPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                PrasaranaLongitudinalPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Prasarana rows

                $key2 = PrasaranaPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = PrasaranaPeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = PrasaranaPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    PrasaranaPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (PrasaranaLongitudinal) to the collection in $obj2 (Prasarana)
                $obj2->addPrasaranaLongitudinalRelatedByPrasaranaId($obj1);

            } // if joined row is not null

                // Add objects for joined Prasarana rows

                $key3 = PrasaranaPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = PrasaranaPeer::getInstanceFromPool($key3);
                    if (!$obj3) {
    
                        $cls = PrasaranaPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    PrasaranaPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (PrasaranaLongitudinal) to the collection in $obj3 (Prasarana)
                $obj3->addPrasaranaLongitudinalRelatedByPrasaranaId($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(PrasaranaLongitudinalPeer::DATABASE_NAME)->getTable(PrasaranaLongitudinalPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BasePrasaranaLongitudinalPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BasePrasaranaLongitudinalPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new PrasaranaLongitudinalTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass()
    {
        return PrasaranaLongitudinalPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a PrasaranaLongitudinal or Criteria object.
     *
     * @param      mixed $values Criteria or PrasaranaLongitudinal object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(PrasaranaLongitudinalPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from PrasaranaLongitudinal object
        }


        // Set the correct dbName
        $criteria->setDbName(PrasaranaLongitudinalPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a PrasaranaLongitudinal or Criteria object.
     *
     * @param      mixed $values Criteria or PrasaranaLongitudinal object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(PrasaranaLongitudinalPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(PrasaranaLongitudinalPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(PrasaranaLongitudinalPeer::PRASARANA_ID);
            $value = $criteria->remove(PrasaranaLongitudinalPeer::PRASARANA_ID);
            if ($value) {
                $selectCriteria->add(PrasaranaLongitudinalPeer::PRASARANA_ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(PrasaranaLongitudinalPeer::TABLE_NAME);
            }

            $comparison = $criteria->getComparison(PrasaranaLongitudinalPeer::SEMESTER_ID);
            $value = $criteria->remove(PrasaranaLongitudinalPeer::SEMESTER_ID);
            if ($value) {
                $selectCriteria->add(PrasaranaLongitudinalPeer::SEMESTER_ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(PrasaranaLongitudinalPeer::TABLE_NAME);
            }

        } else { // $values is PrasaranaLongitudinal object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(PrasaranaLongitudinalPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the prasarana_longitudinal table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(PrasaranaLongitudinalPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(PrasaranaLongitudinalPeer::TABLE_NAME, $con, PrasaranaLongitudinalPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PrasaranaLongitudinalPeer::clearInstancePool();
            PrasaranaLongitudinalPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a PrasaranaLongitudinal or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or PrasaranaLongitudinal object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(PrasaranaLongitudinalPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            PrasaranaLongitudinalPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof PrasaranaLongitudinal) { // it's a model object
            // invalidate the cache for this single object
            PrasaranaLongitudinalPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(PrasaranaLongitudinalPeer::DATABASE_NAME);
            // primary key is composite; we therefore, expect
            // the primary key passed to be an array of pkey values
            if (count($values) == count($values, COUNT_RECURSIVE)) {
                // array is not multi-dimensional
                $values = array($values);
            }
            foreach ($values as $value) {
                $criterion = $criteria->getNewCriterion(PrasaranaLongitudinalPeer::PRASARANA_ID, $value[0]);
                $criterion->addAnd($criteria->getNewCriterion(PrasaranaLongitudinalPeer::SEMESTER_ID, $value[1]));
                $criteria->addOr($criterion);
                // we can invalidate the cache for this single PK
                PrasaranaLongitudinalPeer::removeInstanceFromPool($value);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(PrasaranaLongitudinalPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            
            $affectedRows += BasePeer::doDelete($criteria, $con);
            PrasaranaLongitudinalPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given PrasaranaLongitudinal object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      PrasaranaLongitudinal $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(PrasaranaLongitudinalPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(PrasaranaLongitudinalPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(PrasaranaLongitudinalPeer::DATABASE_NAME, PrasaranaLongitudinalPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve object using using composite pkey values.
     * @param   string $prasarana_id
     * @param   string $semester_id
     * @param      PropelPDO $con
     * @return   PrasaranaLongitudinal
     */
    public static function retrieveByPK($prasarana_id, $semester_id, PropelPDO $con = null) {
        $_instancePoolKey = serialize(array((string) $prasarana_id, (string) $semester_id));
         if (null !== ($obj = PrasaranaLongitudinalPeer::getInstanceFromPool($_instancePoolKey))) {
             return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(PrasaranaLongitudinalPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $criteria = new Criteria(PrasaranaLongitudinalPeer::DATABASE_NAME);
        $criteria->add(PrasaranaLongitudinalPeer::PRASARANA_ID, $prasarana_id);
        $criteria->add(PrasaranaLongitudinalPeer::SEMESTER_ID, $semester_id);
        $v = PrasaranaLongitudinalPeer::doSelect($criteria, $con);

        return !empty($v) ? $v[0] : null;
    }
} // BasePrasaranaLongitudinalPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BasePrasaranaLongitudinalPeer::buildTableMap();

