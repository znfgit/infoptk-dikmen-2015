<?php

namespace angulex\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\Prasarana;
use angulex\Model\PrasaranaLongitudinal;
use angulex\Model\PrasaranaLongitudinalPeer;
use angulex\Model\PrasaranaLongitudinalQuery;
use angulex\Model\Semester;

/**
 * Base class that represents a query for the 'prasarana_longitudinal' table.
 *
 * 
 *
 * @method PrasaranaLongitudinalQuery orderByPrasaranaId($order = Criteria::ASC) Order by the prasarana_id column
 * @method PrasaranaLongitudinalQuery orderBySemesterId($order = Criteria::ASC) Order by the semester_id column
 * @method PrasaranaLongitudinalQuery orderByRusakPenutupAtap($order = Criteria::ASC) Order by the rusak_penutup_atap column
 * @method PrasaranaLongitudinalQuery orderByRusakRangkaAtap($order = Criteria::ASC) Order by the rusak_rangka_atap column
 * @method PrasaranaLongitudinalQuery orderByRusakLisplangTalang($order = Criteria::ASC) Order by the rusak_lisplang_talang column
 * @method PrasaranaLongitudinalQuery orderByRusakRangkaPlafon($order = Criteria::ASC) Order by the rusak_rangka_plafon column
 * @method PrasaranaLongitudinalQuery orderByRusakPenutupListplafon($order = Criteria::ASC) Order by the rusak_penutup_listplafon column
 * @method PrasaranaLongitudinalQuery orderByRusakCatPlafon($order = Criteria::ASC) Order by the rusak_cat_plafon column
 * @method PrasaranaLongitudinalQuery orderByRusakKolomRingbalok($order = Criteria::ASC) Order by the rusak_kolom_ringbalok column
 * @method PrasaranaLongitudinalQuery orderByRusakBataDindingpengisi($order = Criteria::ASC) Order by the rusak_bata_dindingpengisi column
 * @method PrasaranaLongitudinalQuery orderByRusakCatDinding($order = Criteria::ASC) Order by the rusak_cat_dinding column
 * @method PrasaranaLongitudinalQuery orderByRusakKusen($order = Criteria::ASC) Order by the rusak_kusen column
 * @method PrasaranaLongitudinalQuery orderByRusakDaunPintu($order = Criteria::ASC) Order by the rusak_daun_pintu column
 * @method PrasaranaLongitudinalQuery orderByRusakDaunJendela($order = Criteria::ASC) Order by the rusak_daun_jendela column
 * @method PrasaranaLongitudinalQuery orderByRusakStrukturBawah($order = Criteria::ASC) Order by the rusak_struktur_bawah column
 * @method PrasaranaLongitudinalQuery orderByRusakPenutupLantai($order = Criteria::ASC) Order by the rusak_penutup_lantai column
 * @method PrasaranaLongitudinalQuery orderByRusakPondasi($order = Criteria::ASC) Order by the rusak_pondasi column
 * @method PrasaranaLongitudinalQuery orderByRusakSloof($order = Criteria::ASC) Order by the rusak_sloof column
 * @method PrasaranaLongitudinalQuery orderByRusakListrik($order = Criteria::ASC) Order by the rusak_listrik column
 * @method PrasaranaLongitudinalQuery orderByRusakAirhujanRabatan($order = Criteria::ASC) Order by the rusak_airhujan_rabatan column
 * @method PrasaranaLongitudinalQuery orderByBlobId($order = Criteria::ASC) Order by the blob_id column
 * @method PrasaranaLongitudinalQuery orderByLastUpdate($order = Criteria::ASC) Order by the Last_update column
 * @method PrasaranaLongitudinalQuery orderBySoftDelete($order = Criteria::ASC) Order by the Soft_delete column
 * @method PrasaranaLongitudinalQuery orderByLastSync($order = Criteria::ASC) Order by the last_sync column
 * @method PrasaranaLongitudinalQuery orderByUpdaterId($order = Criteria::ASC) Order by the Updater_ID column
 *
 * @method PrasaranaLongitudinalQuery groupByPrasaranaId() Group by the prasarana_id column
 * @method PrasaranaLongitudinalQuery groupBySemesterId() Group by the semester_id column
 * @method PrasaranaLongitudinalQuery groupByRusakPenutupAtap() Group by the rusak_penutup_atap column
 * @method PrasaranaLongitudinalQuery groupByRusakRangkaAtap() Group by the rusak_rangka_atap column
 * @method PrasaranaLongitudinalQuery groupByRusakLisplangTalang() Group by the rusak_lisplang_talang column
 * @method PrasaranaLongitudinalQuery groupByRusakRangkaPlafon() Group by the rusak_rangka_plafon column
 * @method PrasaranaLongitudinalQuery groupByRusakPenutupListplafon() Group by the rusak_penutup_listplafon column
 * @method PrasaranaLongitudinalQuery groupByRusakCatPlafon() Group by the rusak_cat_plafon column
 * @method PrasaranaLongitudinalQuery groupByRusakKolomRingbalok() Group by the rusak_kolom_ringbalok column
 * @method PrasaranaLongitudinalQuery groupByRusakBataDindingpengisi() Group by the rusak_bata_dindingpengisi column
 * @method PrasaranaLongitudinalQuery groupByRusakCatDinding() Group by the rusak_cat_dinding column
 * @method PrasaranaLongitudinalQuery groupByRusakKusen() Group by the rusak_kusen column
 * @method PrasaranaLongitudinalQuery groupByRusakDaunPintu() Group by the rusak_daun_pintu column
 * @method PrasaranaLongitudinalQuery groupByRusakDaunJendela() Group by the rusak_daun_jendela column
 * @method PrasaranaLongitudinalQuery groupByRusakStrukturBawah() Group by the rusak_struktur_bawah column
 * @method PrasaranaLongitudinalQuery groupByRusakPenutupLantai() Group by the rusak_penutup_lantai column
 * @method PrasaranaLongitudinalQuery groupByRusakPondasi() Group by the rusak_pondasi column
 * @method PrasaranaLongitudinalQuery groupByRusakSloof() Group by the rusak_sloof column
 * @method PrasaranaLongitudinalQuery groupByRusakListrik() Group by the rusak_listrik column
 * @method PrasaranaLongitudinalQuery groupByRusakAirhujanRabatan() Group by the rusak_airhujan_rabatan column
 * @method PrasaranaLongitudinalQuery groupByBlobId() Group by the blob_id column
 * @method PrasaranaLongitudinalQuery groupByLastUpdate() Group by the Last_update column
 * @method PrasaranaLongitudinalQuery groupBySoftDelete() Group by the Soft_delete column
 * @method PrasaranaLongitudinalQuery groupByLastSync() Group by the last_sync column
 * @method PrasaranaLongitudinalQuery groupByUpdaterId() Group by the Updater_ID column
 *
 * @method PrasaranaLongitudinalQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method PrasaranaLongitudinalQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method PrasaranaLongitudinalQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method PrasaranaLongitudinalQuery leftJoinPrasaranaRelatedByPrasaranaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PrasaranaRelatedByPrasaranaId relation
 * @method PrasaranaLongitudinalQuery rightJoinPrasaranaRelatedByPrasaranaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PrasaranaRelatedByPrasaranaId relation
 * @method PrasaranaLongitudinalQuery innerJoinPrasaranaRelatedByPrasaranaId($relationAlias = null) Adds a INNER JOIN clause to the query using the PrasaranaRelatedByPrasaranaId relation
 *
 * @method PrasaranaLongitudinalQuery leftJoinPrasaranaRelatedByPrasaranaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PrasaranaRelatedByPrasaranaId relation
 * @method PrasaranaLongitudinalQuery rightJoinPrasaranaRelatedByPrasaranaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PrasaranaRelatedByPrasaranaId relation
 * @method PrasaranaLongitudinalQuery innerJoinPrasaranaRelatedByPrasaranaId($relationAlias = null) Adds a INNER JOIN clause to the query using the PrasaranaRelatedByPrasaranaId relation
 *
 * @method PrasaranaLongitudinalQuery leftJoinSemesterRelatedBySemesterId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SemesterRelatedBySemesterId relation
 * @method PrasaranaLongitudinalQuery rightJoinSemesterRelatedBySemesterId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SemesterRelatedBySemesterId relation
 * @method PrasaranaLongitudinalQuery innerJoinSemesterRelatedBySemesterId($relationAlias = null) Adds a INNER JOIN clause to the query using the SemesterRelatedBySemesterId relation
 *
 * @method PrasaranaLongitudinalQuery leftJoinSemesterRelatedBySemesterId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SemesterRelatedBySemesterId relation
 * @method PrasaranaLongitudinalQuery rightJoinSemesterRelatedBySemesterId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SemesterRelatedBySemesterId relation
 * @method PrasaranaLongitudinalQuery innerJoinSemesterRelatedBySemesterId($relationAlias = null) Adds a INNER JOIN clause to the query using the SemesterRelatedBySemesterId relation
 *
 * @method PrasaranaLongitudinal findOne(PropelPDO $con = null) Return the first PrasaranaLongitudinal matching the query
 * @method PrasaranaLongitudinal findOneOrCreate(PropelPDO $con = null) Return the first PrasaranaLongitudinal matching the query, or a new PrasaranaLongitudinal object populated from the query conditions when no match is found
 *
 * @method PrasaranaLongitudinal findOneByPrasaranaId(string $prasarana_id) Return the first PrasaranaLongitudinal filtered by the prasarana_id column
 * @method PrasaranaLongitudinal findOneBySemesterId(string $semester_id) Return the first PrasaranaLongitudinal filtered by the semester_id column
 * @method PrasaranaLongitudinal findOneByRusakPenutupAtap(string $rusak_penutup_atap) Return the first PrasaranaLongitudinal filtered by the rusak_penutup_atap column
 * @method PrasaranaLongitudinal findOneByRusakRangkaAtap(string $rusak_rangka_atap) Return the first PrasaranaLongitudinal filtered by the rusak_rangka_atap column
 * @method PrasaranaLongitudinal findOneByRusakLisplangTalang(string $rusak_lisplang_talang) Return the first PrasaranaLongitudinal filtered by the rusak_lisplang_talang column
 * @method PrasaranaLongitudinal findOneByRusakRangkaPlafon(string $rusak_rangka_plafon) Return the first PrasaranaLongitudinal filtered by the rusak_rangka_plafon column
 * @method PrasaranaLongitudinal findOneByRusakPenutupListplafon(string $rusak_penutup_listplafon) Return the first PrasaranaLongitudinal filtered by the rusak_penutup_listplafon column
 * @method PrasaranaLongitudinal findOneByRusakCatPlafon(string $rusak_cat_plafon) Return the first PrasaranaLongitudinal filtered by the rusak_cat_plafon column
 * @method PrasaranaLongitudinal findOneByRusakKolomRingbalok(string $rusak_kolom_ringbalok) Return the first PrasaranaLongitudinal filtered by the rusak_kolom_ringbalok column
 * @method PrasaranaLongitudinal findOneByRusakBataDindingpengisi(string $rusak_bata_dindingpengisi) Return the first PrasaranaLongitudinal filtered by the rusak_bata_dindingpengisi column
 * @method PrasaranaLongitudinal findOneByRusakCatDinding(string $rusak_cat_dinding) Return the first PrasaranaLongitudinal filtered by the rusak_cat_dinding column
 * @method PrasaranaLongitudinal findOneByRusakKusen(string $rusak_kusen) Return the first PrasaranaLongitudinal filtered by the rusak_kusen column
 * @method PrasaranaLongitudinal findOneByRusakDaunPintu(string $rusak_daun_pintu) Return the first PrasaranaLongitudinal filtered by the rusak_daun_pintu column
 * @method PrasaranaLongitudinal findOneByRusakDaunJendela(string $rusak_daun_jendela) Return the first PrasaranaLongitudinal filtered by the rusak_daun_jendela column
 * @method PrasaranaLongitudinal findOneByRusakStrukturBawah(string $rusak_struktur_bawah) Return the first PrasaranaLongitudinal filtered by the rusak_struktur_bawah column
 * @method PrasaranaLongitudinal findOneByRusakPenutupLantai(string $rusak_penutup_lantai) Return the first PrasaranaLongitudinal filtered by the rusak_penutup_lantai column
 * @method PrasaranaLongitudinal findOneByRusakPondasi(string $rusak_pondasi) Return the first PrasaranaLongitudinal filtered by the rusak_pondasi column
 * @method PrasaranaLongitudinal findOneByRusakSloof(string $rusak_sloof) Return the first PrasaranaLongitudinal filtered by the rusak_sloof column
 * @method PrasaranaLongitudinal findOneByRusakListrik(string $rusak_listrik) Return the first PrasaranaLongitudinal filtered by the rusak_listrik column
 * @method PrasaranaLongitudinal findOneByRusakAirhujanRabatan(string $rusak_airhujan_rabatan) Return the first PrasaranaLongitudinal filtered by the rusak_airhujan_rabatan column
 * @method PrasaranaLongitudinal findOneByBlobId(string $blob_id) Return the first PrasaranaLongitudinal filtered by the blob_id column
 * @method PrasaranaLongitudinal findOneByLastUpdate(string $Last_update) Return the first PrasaranaLongitudinal filtered by the Last_update column
 * @method PrasaranaLongitudinal findOneBySoftDelete(string $Soft_delete) Return the first PrasaranaLongitudinal filtered by the Soft_delete column
 * @method PrasaranaLongitudinal findOneByLastSync(string $last_sync) Return the first PrasaranaLongitudinal filtered by the last_sync column
 * @method PrasaranaLongitudinal findOneByUpdaterId(string $Updater_ID) Return the first PrasaranaLongitudinal filtered by the Updater_ID column
 *
 * @method array findByPrasaranaId(string $prasarana_id) Return PrasaranaLongitudinal objects filtered by the prasarana_id column
 * @method array findBySemesterId(string $semester_id) Return PrasaranaLongitudinal objects filtered by the semester_id column
 * @method array findByRusakPenutupAtap(string $rusak_penutup_atap) Return PrasaranaLongitudinal objects filtered by the rusak_penutup_atap column
 * @method array findByRusakRangkaAtap(string $rusak_rangka_atap) Return PrasaranaLongitudinal objects filtered by the rusak_rangka_atap column
 * @method array findByRusakLisplangTalang(string $rusak_lisplang_talang) Return PrasaranaLongitudinal objects filtered by the rusak_lisplang_talang column
 * @method array findByRusakRangkaPlafon(string $rusak_rangka_plafon) Return PrasaranaLongitudinal objects filtered by the rusak_rangka_plafon column
 * @method array findByRusakPenutupListplafon(string $rusak_penutup_listplafon) Return PrasaranaLongitudinal objects filtered by the rusak_penutup_listplafon column
 * @method array findByRusakCatPlafon(string $rusak_cat_plafon) Return PrasaranaLongitudinal objects filtered by the rusak_cat_plafon column
 * @method array findByRusakKolomRingbalok(string $rusak_kolom_ringbalok) Return PrasaranaLongitudinal objects filtered by the rusak_kolom_ringbalok column
 * @method array findByRusakBataDindingpengisi(string $rusak_bata_dindingpengisi) Return PrasaranaLongitudinal objects filtered by the rusak_bata_dindingpengisi column
 * @method array findByRusakCatDinding(string $rusak_cat_dinding) Return PrasaranaLongitudinal objects filtered by the rusak_cat_dinding column
 * @method array findByRusakKusen(string $rusak_kusen) Return PrasaranaLongitudinal objects filtered by the rusak_kusen column
 * @method array findByRusakDaunPintu(string $rusak_daun_pintu) Return PrasaranaLongitudinal objects filtered by the rusak_daun_pintu column
 * @method array findByRusakDaunJendela(string $rusak_daun_jendela) Return PrasaranaLongitudinal objects filtered by the rusak_daun_jendela column
 * @method array findByRusakStrukturBawah(string $rusak_struktur_bawah) Return PrasaranaLongitudinal objects filtered by the rusak_struktur_bawah column
 * @method array findByRusakPenutupLantai(string $rusak_penutup_lantai) Return PrasaranaLongitudinal objects filtered by the rusak_penutup_lantai column
 * @method array findByRusakPondasi(string $rusak_pondasi) Return PrasaranaLongitudinal objects filtered by the rusak_pondasi column
 * @method array findByRusakSloof(string $rusak_sloof) Return PrasaranaLongitudinal objects filtered by the rusak_sloof column
 * @method array findByRusakListrik(string $rusak_listrik) Return PrasaranaLongitudinal objects filtered by the rusak_listrik column
 * @method array findByRusakAirhujanRabatan(string $rusak_airhujan_rabatan) Return PrasaranaLongitudinal objects filtered by the rusak_airhujan_rabatan column
 * @method array findByBlobId(string $blob_id) Return PrasaranaLongitudinal objects filtered by the blob_id column
 * @method array findByLastUpdate(string $Last_update) Return PrasaranaLongitudinal objects filtered by the Last_update column
 * @method array findBySoftDelete(string $Soft_delete) Return PrasaranaLongitudinal objects filtered by the Soft_delete column
 * @method array findByLastSync(string $last_sync) Return PrasaranaLongitudinal objects filtered by the last_sync column
 * @method array findByUpdaterId(string $Updater_ID) Return PrasaranaLongitudinal objects filtered by the Updater_ID column
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BasePrasaranaLongitudinalQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BasePrasaranaLongitudinalQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Dapodikmen', $modelName = 'angulex\\Model\\PrasaranaLongitudinal', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new PrasaranaLongitudinalQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   PrasaranaLongitudinalQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return PrasaranaLongitudinalQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof PrasaranaLongitudinalQuery) {
            return $criteria;
        }
        $query = new PrasaranaLongitudinalQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34), $con);
     * </code>
     *
     * @param array $key Primary key to use for the query 
                         A Primary key composition: [$prasarana_id, $semester_id]
     * @param     PropelPDO $con an optional connection object
     *
     * @return   PrasaranaLongitudinal|PrasaranaLongitudinal[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = PrasaranaLongitudinalPeer::getInstanceFromPool(serialize(array((string) $key[0], (string) $key[1]))))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(PrasaranaLongitudinalPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 PrasaranaLongitudinal A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [prasarana_id], [semester_id], [rusak_penutup_atap], [rusak_rangka_atap], [rusak_lisplang_talang], [rusak_rangka_plafon], [rusak_penutup_listplafon], [rusak_cat_plafon], [rusak_kolom_ringbalok], [rusak_bata_dindingpengisi], [rusak_cat_dinding], [rusak_kusen], [rusak_daun_pintu], [rusak_daun_jendela], [rusak_struktur_bawah], [rusak_penutup_lantai], [rusak_pondasi], [rusak_sloof], [rusak_listrik], [rusak_airhujan_rabatan], [blob_id], [Last_update], [Soft_delete], [last_sync], [Updater_ID] FROM [prasarana_longitudinal] WHERE [prasarana_id] = :p0 AND [semester_id] = :p1';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_STR);			
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new PrasaranaLongitudinal();
            $obj->hydrate($row);
            PrasaranaLongitudinalPeer::addInstanceToPool($obj, serialize(array((string) $key[0], (string) $key[1])));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return PrasaranaLongitudinal|PrasaranaLongitudinal[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|PrasaranaLongitudinal[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(PrasaranaLongitudinalPeer::PRASARANA_ID, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(PrasaranaLongitudinalPeer::SEMESTER_ID, $key[1], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(PrasaranaLongitudinalPeer::PRASARANA_ID, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(PrasaranaLongitudinalPeer::SEMESTER_ID, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the prasarana_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPrasaranaId('fooValue');   // WHERE prasarana_id = 'fooValue'
     * $query->filterByPrasaranaId('%fooValue%'); // WHERE prasarana_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $prasaranaId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByPrasaranaId($prasaranaId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($prasaranaId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $prasaranaId)) {
                $prasaranaId = str_replace('*', '%', $prasaranaId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::PRASARANA_ID, $prasaranaId, $comparison);
    }

    /**
     * Filter the query on the semester_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySemesterId('fooValue');   // WHERE semester_id = 'fooValue'
     * $query->filterBySemesterId('%fooValue%'); // WHERE semester_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $semesterId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterBySemesterId($semesterId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($semesterId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $semesterId)) {
                $semesterId = str_replace('*', '%', $semesterId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::SEMESTER_ID, $semesterId, $comparison);
    }

    /**
     * Filter the query on the rusak_penutup_atap column
     *
     * Example usage:
     * <code>
     * $query->filterByRusakPenutupAtap(1234); // WHERE rusak_penutup_atap = 1234
     * $query->filterByRusakPenutupAtap(array(12, 34)); // WHERE rusak_penutup_atap IN (12, 34)
     * $query->filterByRusakPenutupAtap(array('min' => 12)); // WHERE rusak_penutup_atap >= 12
     * $query->filterByRusakPenutupAtap(array('max' => 12)); // WHERE rusak_penutup_atap <= 12
     * </code>
     *
     * @param     mixed $rusakPenutupAtap The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByRusakPenutupAtap($rusakPenutupAtap = null, $comparison = null)
    {
        if (is_array($rusakPenutupAtap)) {
            $useMinMax = false;
            if (isset($rusakPenutupAtap['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_PENUTUP_ATAP, $rusakPenutupAtap['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rusakPenutupAtap['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_PENUTUP_ATAP, $rusakPenutupAtap['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_PENUTUP_ATAP, $rusakPenutupAtap, $comparison);
    }

    /**
     * Filter the query on the rusak_rangka_atap column
     *
     * Example usage:
     * <code>
     * $query->filterByRusakRangkaAtap(1234); // WHERE rusak_rangka_atap = 1234
     * $query->filterByRusakRangkaAtap(array(12, 34)); // WHERE rusak_rangka_atap IN (12, 34)
     * $query->filterByRusakRangkaAtap(array('min' => 12)); // WHERE rusak_rangka_atap >= 12
     * $query->filterByRusakRangkaAtap(array('max' => 12)); // WHERE rusak_rangka_atap <= 12
     * </code>
     *
     * @param     mixed $rusakRangkaAtap The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByRusakRangkaAtap($rusakRangkaAtap = null, $comparison = null)
    {
        if (is_array($rusakRangkaAtap)) {
            $useMinMax = false;
            if (isset($rusakRangkaAtap['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_RANGKA_ATAP, $rusakRangkaAtap['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rusakRangkaAtap['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_RANGKA_ATAP, $rusakRangkaAtap['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_RANGKA_ATAP, $rusakRangkaAtap, $comparison);
    }

    /**
     * Filter the query on the rusak_lisplang_talang column
     *
     * Example usage:
     * <code>
     * $query->filterByRusakLisplangTalang(1234); // WHERE rusak_lisplang_talang = 1234
     * $query->filterByRusakLisplangTalang(array(12, 34)); // WHERE rusak_lisplang_talang IN (12, 34)
     * $query->filterByRusakLisplangTalang(array('min' => 12)); // WHERE rusak_lisplang_talang >= 12
     * $query->filterByRusakLisplangTalang(array('max' => 12)); // WHERE rusak_lisplang_talang <= 12
     * </code>
     *
     * @param     mixed $rusakLisplangTalang The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByRusakLisplangTalang($rusakLisplangTalang = null, $comparison = null)
    {
        if (is_array($rusakLisplangTalang)) {
            $useMinMax = false;
            if (isset($rusakLisplangTalang['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_LISPLANG_TALANG, $rusakLisplangTalang['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rusakLisplangTalang['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_LISPLANG_TALANG, $rusakLisplangTalang['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_LISPLANG_TALANG, $rusakLisplangTalang, $comparison);
    }

    /**
     * Filter the query on the rusak_rangka_plafon column
     *
     * Example usage:
     * <code>
     * $query->filterByRusakRangkaPlafon(1234); // WHERE rusak_rangka_plafon = 1234
     * $query->filterByRusakRangkaPlafon(array(12, 34)); // WHERE rusak_rangka_plafon IN (12, 34)
     * $query->filterByRusakRangkaPlafon(array('min' => 12)); // WHERE rusak_rangka_plafon >= 12
     * $query->filterByRusakRangkaPlafon(array('max' => 12)); // WHERE rusak_rangka_plafon <= 12
     * </code>
     *
     * @param     mixed $rusakRangkaPlafon The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByRusakRangkaPlafon($rusakRangkaPlafon = null, $comparison = null)
    {
        if (is_array($rusakRangkaPlafon)) {
            $useMinMax = false;
            if (isset($rusakRangkaPlafon['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_RANGKA_PLAFON, $rusakRangkaPlafon['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rusakRangkaPlafon['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_RANGKA_PLAFON, $rusakRangkaPlafon['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_RANGKA_PLAFON, $rusakRangkaPlafon, $comparison);
    }

    /**
     * Filter the query on the rusak_penutup_listplafon column
     *
     * Example usage:
     * <code>
     * $query->filterByRusakPenutupListplafon(1234); // WHERE rusak_penutup_listplafon = 1234
     * $query->filterByRusakPenutupListplafon(array(12, 34)); // WHERE rusak_penutup_listplafon IN (12, 34)
     * $query->filterByRusakPenutupListplafon(array('min' => 12)); // WHERE rusak_penutup_listplafon >= 12
     * $query->filterByRusakPenutupListplafon(array('max' => 12)); // WHERE rusak_penutup_listplafon <= 12
     * </code>
     *
     * @param     mixed $rusakPenutupListplafon The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByRusakPenutupListplafon($rusakPenutupListplafon = null, $comparison = null)
    {
        if (is_array($rusakPenutupListplafon)) {
            $useMinMax = false;
            if (isset($rusakPenutupListplafon['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_PENUTUP_LISTPLAFON, $rusakPenutupListplafon['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rusakPenutupListplafon['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_PENUTUP_LISTPLAFON, $rusakPenutupListplafon['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_PENUTUP_LISTPLAFON, $rusakPenutupListplafon, $comparison);
    }

    /**
     * Filter the query on the rusak_cat_plafon column
     *
     * Example usage:
     * <code>
     * $query->filterByRusakCatPlafon(1234); // WHERE rusak_cat_plafon = 1234
     * $query->filterByRusakCatPlafon(array(12, 34)); // WHERE rusak_cat_plafon IN (12, 34)
     * $query->filterByRusakCatPlafon(array('min' => 12)); // WHERE rusak_cat_plafon >= 12
     * $query->filterByRusakCatPlafon(array('max' => 12)); // WHERE rusak_cat_plafon <= 12
     * </code>
     *
     * @param     mixed $rusakCatPlafon The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByRusakCatPlafon($rusakCatPlafon = null, $comparison = null)
    {
        if (is_array($rusakCatPlafon)) {
            $useMinMax = false;
            if (isset($rusakCatPlafon['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_CAT_PLAFON, $rusakCatPlafon['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rusakCatPlafon['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_CAT_PLAFON, $rusakCatPlafon['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_CAT_PLAFON, $rusakCatPlafon, $comparison);
    }

    /**
     * Filter the query on the rusak_kolom_ringbalok column
     *
     * Example usage:
     * <code>
     * $query->filterByRusakKolomRingbalok(1234); // WHERE rusak_kolom_ringbalok = 1234
     * $query->filterByRusakKolomRingbalok(array(12, 34)); // WHERE rusak_kolom_ringbalok IN (12, 34)
     * $query->filterByRusakKolomRingbalok(array('min' => 12)); // WHERE rusak_kolom_ringbalok >= 12
     * $query->filterByRusakKolomRingbalok(array('max' => 12)); // WHERE rusak_kolom_ringbalok <= 12
     * </code>
     *
     * @param     mixed $rusakKolomRingbalok The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByRusakKolomRingbalok($rusakKolomRingbalok = null, $comparison = null)
    {
        if (is_array($rusakKolomRingbalok)) {
            $useMinMax = false;
            if (isset($rusakKolomRingbalok['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_KOLOM_RINGBALOK, $rusakKolomRingbalok['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rusakKolomRingbalok['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_KOLOM_RINGBALOK, $rusakKolomRingbalok['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_KOLOM_RINGBALOK, $rusakKolomRingbalok, $comparison);
    }

    /**
     * Filter the query on the rusak_bata_dindingpengisi column
     *
     * Example usage:
     * <code>
     * $query->filterByRusakBataDindingpengisi(1234); // WHERE rusak_bata_dindingpengisi = 1234
     * $query->filterByRusakBataDindingpengisi(array(12, 34)); // WHERE rusak_bata_dindingpengisi IN (12, 34)
     * $query->filterByRusakBataDindingpengisi(array('min' => 12)); // WHERE rusak_bata_dindingpengisi >= 12
     * $query->filterByRusakBataDindingpengisi(array('max' => 12)); // WHERE rusak_bata_dindingpengisi <= 12
     * </code>
     *
     * @param     mixed $rusakBataDindingpengisi The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByRusakBataDindingpengisi($rusakBataDindingpengisi = null, $comparison = null)
    {
        if (is_array($rusakBataDindingpengisi)) {
            $useMinMax = false;
            if (isset($rusakBataDindingpengisi['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_BATA_DINDINGPENGISI, $rusakBataDindingpengisi['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rusakBataDindingpengisi['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_BATA_DINDINGPENGISI, $rusakBataDindingpengisi['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_BATA_DINDINGPENGISI, $rusakBataDindingpengisi, $comparison);
    }

    /**
     * Filter the query on the rusak_cat_dinding column
     *
     * Example usage:
     * <code>
     * $query->filterByRusakCatDinding(1234); // WHERE rusak_cat_dinding = 1234
     * $query->filterByRusakCatDinding(array(12, 34)); // WHERE rusak_cat_dinding IN (12, 34)
     * $query->filterByRusakCatDinding(array('min' => 12)); // WHERE rusak_cat_dinding >= 12
     * $query->filterByRusakCatDinding(array('max' => 12)); // WHERE rusak_cat_dinding <= 12
     * </code>
     *
     * @param     mixed $rusakCatDinding The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByRusakCatDinding($rusakCatDinding = null, $comparison = null)
    {
        if (is_array($rusakCatDinding)) {
            $useMinMax = false;
            if (isset($rusakCatDinding['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_CAT_DINDING, $rusakCatDinding['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rusakCatDinding['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_CAT_DINDING, $rusakCatDinding['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_CAT_DINDING, $rusakCatDinding, $comparison);
    }

    /**
     * Filter the query on the rusak_kusen column
     *
     * Example usage:
     * <code>
     * $query->filterByRusakKusen(1234); // WHERE rusak_kusen = 1234
     * $query->filterByRusakKusen(array(12, 34)); // WHERE rusak_kusen IN (12, 34)
     * $query->filterByRusakKusen(array('min' => 12)); // WHERE rusak_kusen >= 12
     * $query->filterByRusakKusen(array('max' => 12)); // WHERE rusak_kusen <= 12
     * </code>
     *
     * @param     mixed $rusakKusen The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByRusakKusen($rusakKusen = null, $comparison = null)
    {
        if (is_array($rusakKusen)) {
            $useMinMax = false;
            if (isset($rusakKusen['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_KUSEN, $rusakKusen['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rusakKusen['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_KUSEN, $rusakKusen['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_KUSEN, $rusakKusen, $comparison);
    }

    /**
     * Filter the query on the rusak_daun_pintu column
     *
     * Example usage:
     * <code>
     * $query->filterByRusakDaunPintu(1234); // WHERE rusak_daun_pintu = 1234
     * $query->filterByRusakDaunPintu(array(12, 34)); // WHERE rusak_daun_pintu IN (12, 34)
     * $query->filterByRusakDaunPintu(array('min' => 12)); // WHERE rusak_daun_pintu >= 12
     * $query->filterByRusakDaunPintu(array('max' => 12)); // WHERE rusak_daun_pintu <= 12
     * </code>
     *
     * @param     mixed $rusakDaunPintu The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByRusakDaunPintu($rusakDaunPintu = null, $comparison = null)
    {
        if (is_array($rusakDaunPintu)) {
            $useMinMax = false;
            if (isset($rusakDaunPintu['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_DAUN_PINTU, $rusakDaunPintu['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rusakDaunPintu['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_DAUN_PINTU, $rusakDaunPintu['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_DAUN_PINTU, $rusakDaunPintu, $comparison);
    }

    /**
     * Filter the query on the rusak_daun_jendela column
     *
     * Example usage:
     * <code>
     * $query->filterByRusakDaunJendela(1234); // WHERE rusak_daun_jendela = 1234
     * $query->filterByRusakDaunJendela(array(12, 34)); // WHERE rusak_daun_jendela IN (12, 34)
     * $query->filterByRusakDaunJendela(array('min' => 12)); // WHERE rusak_daun_jendela >= 12
     * $query->filterByRusakDaunJendela(array('max' => 12)); // WHERE rusak_daun_jendela <= 12
     * </code>
     *
     * @param     mixed $rusakDaunJendela The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByRusakDaunJendela($rusakDaunJendela = null, $comparison = null)
    {
        if (is_array($rusakDaunJendela)) {
            $useMinMax = false;
            if (isset($rusakDaunJendela['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_DAUN_JENDELA, $rusakDaunJendela['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rusakDaunJendela['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_DAUN_JENDELA, $rusakDaunJendela['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_DAUN_JENDELA, $rusakDaunJendela, $comparison);
    }

    /**
     * Filter the query on the rusak_struktur_bawah column
     *
     * Example usage:
     * <code>
     * $query->filterByRusakStrukturBawah(1234); // WHERE rusak_struktur_bawah = 1234
     * $query->filterByRusakStrukturBawah(array(12, 34)); // WHERE rusak_struktur_bawah IN (12, 34)
     * $query->filterByRusakStrukturBawah(array('min' => 12)); // WHERE rusak_struktur_bawah >= 12
     * $query->filterByRusakStrukturBawah(array('max' => 12)); // WHERE rusak_struktur_bawah <= 12
     * </code>
     *
     * @param     mixed $rusakStrukturBawah The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByRusakStrukturBawah($rusakStrukturBawah = null, $comparison = null)
    {
        if (is_array($rusakStrukturBawah)) {
            $useMinMax = false;
            if (isset($rusakStrukturBawah['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_STRUKTUR_BAWAH, $rusakStrukturBawah['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rusakStrukturBawah['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_STRUKTUR_BAWAH, $rusakStrukturBawah['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_STRUKTUR_BAWAH, $rusakStrukturBawah, $comparison);
    }

    /**
     * Filter the query on the rusak_penutup_lantai column
     *
     * Example usage:
     * <code>
     * $query->filterByRusakPenutupLantai(1234); // WHERE rusak_penutup_lantai = 1234
     * $query->filterByRusakPenutupLantai(array(12, 34)); // WHERE rusak_penutup_lantai IN (12, 34)
     * $query->filterByRusakPenutupLantai(array('min' => 12)); // WHERE rusak_penutup_lantai >= 12
     * $query->filterByRusakPenutupLantai(array('max' => 12)); // WHERE rusak_penutup_lantai <= 12
     * </code>
     *
     * @param     mixed $rusakPenutupLantai The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByRusakPenutupLantai($rusakPenutupLantai = null, $comparison = null)
    {
        if (is_array($rusakPenutupLantai)) {
            $useMinMax = false;
            if (isset($rusakPenutupLantai['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_PENUTUP_LANTAI, $rusakPenutupLantai['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rusakPenutupLantai['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_PENUTUP_LANTAI, $rusakPenutupLantai['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_PENUTUP_LANTAI, $rusakPenutupLantai, $comparison);
    }

    /**
     * Filter the query on the rusak_pondasi column
     *
     * Example usage:
     * <code>
     * $query->filterByRusakPondasi(1234); // WHERE rusak_pondasi = 1234
     * $query->filterByRusakPondasi(array(12, 34)); // WHERE rusak_pondasi IN (12, 34)
     * $query->filterByRusakPondasi(array('min' => 12)); // WHERE rusak_pondasi >= 12
     * $query->filterByRusakPondasi(array('max' => 12)); // WHERE rusak_pondasi <= 12
     * </code>
     *
     * @param     mixed $rusakPondasi The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByRusakPondasi($rusakPondasi = null, $comparison = null)
    {
        if (is_array($rusakPondasi)) {
            $useMinMax = false;
            if (isset($rusakPondasi['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_PONDASI, $rusakPondasi['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rusakPondasi['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_PONDASI, $rusakPondasi['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_PONDASI, $rusakPondasi, $comparison);
    }

    /**
     * Filter the query on the rusak_sloof column
     *
     * Example usage:
     * <code>
     * $query->filterByRusakSloof(1234); // WHERE rusak_sloof = 1234
     * $query->filterByRusakSloof(array(12, 34)); // WHERE rusak_sloof IN (12, 34)
     * $query->filterByRusakSloof(array('min' => 12)); // WHERE rusak_sloof >= 12
     * $query->filterByRusakSloof(array('max' => 12)); // WHERE rusak_sloof <= 12
     * </code>
     *
     * @param     mixed $rusakSloof The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByRusakSloof($rusakSloof = null, $comparison = null)
    {
        if (is_array($rusakSloof)) {
            $useMinMax = false;
            if (isset($rusakSloof['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_SLOOF, $rusakSloof['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rusakSloof['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_SLOOF, $rusakSloof['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_SLOOF, $rusakSloof, $comparison);
    }

    /**
     * Filter the query on the rusak_listrik column
     *
     * Example usage:
     * <code>
     * $query->filterByRusakListrik(1234); // WHERE rusak_listrik = 1234
     * $query->filterByRusakListrik(array(12, 34)); // WHERE rusak_listrik IN (12, 34)
     * $query->filterByRusakListrik(array('min' => 12)); // WHERE rusak_listrik >= 12
     * $query->filterByRusakListrik(array('max' => 12)); // WHERE rusak_listrik <= 12
     * </code>
     *
     * @param     mixed $rusakListrik The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByRusakListrik($rusakListrik = null, $comparison = null)
    {
        if (is_array($rusakListrik)) {
            $useMinMax = false;
            if (isset($rusakListrik['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_LISTRIK, $rusakListrik['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rusakListrik['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_LISTRIK, $rusakListrik['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_LISTRIK, $rusakListrik, $comparison);
    }

    /**
     * Filter the query on the rusak_airhujan_rabatan column
     *
     * Example usage:
     * <code>
     * $query->filterByRusakAirhujanRabatan(1234); // WHERE rusak_airhujan_rabatan = 1234
     * $query->filterByRusakAirhujanRabatan(array(12, 34)); // WHERE rusak_airhujan_rabatan IN (12, 34)
     * $query->filterByRusakAirhujanRabatan(array('min' => 12)); // WHERE rusak_airhujan_rabatan >= 12
     * $query->filterByRusakAirhujanRabatan(array('max' => 12)); // WHERE rusak_airhujan_rabatan <= 12
     * </code>
     *
     * @param     mixed $rusakAirhujanRabatan The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByRusakAirhujanRabatan($rusakAirhujanRabatan = null, $comparison = null)
    {
        if (is_array($rusakAirhujanRabatan)) {
            $useMinMax = false;
            if (isset($rusakAirhujanRabatan['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_AIRHUJAN_RABATAN, $rusakAirhujanRabatan['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rusakAirhujanRabatan['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_AIRHUJAN_RABATAN, $rusakAirhujanRabatan['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_AIRHUJAN_RABATAN, $rusakAirhujanRabatan, $comparison);
    }

    /**
     * Filter the query on the blob_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBlobId('fooValue');   // WHERE blob_id = 'fooValue'
     * $query->filterByBlobId('%fooValue%'); // WHERE blob_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $blobId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByBlobId($blobId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($blobId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $blobId)) {
                $blobId = str_replace('*', '%', $blobId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::BLOB_ID, $blobId, $comparison);
    }

    /**
     * Filter the query on the Last_update column
     *
     * Example usage:
     * <code>
     * $query->filterByLastUpdate('2011-03-14'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate('now'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate(array('max' => 'yesterday')); // WHERE Last_update > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastUpdate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByLastUpdate($lastUpdate = null, $comparison = null)
    {
        if (is_array($lastUpdate)) {
            $useMinMax = false;
            if (isset($lastUpdate['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::LAST_UPDATE, $lastUpdate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastUpdate['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::LAST_UPDATE, $lastUpdate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::LAST_UPDATE, $lastUpdate, $comparison);
    }

    /**
     * Filter the query on the Soft_delete column
     *
     * Example usage:
     * <code>
     * $query->filterBySoftDelete(1234); // WHERE Soft_delete = 1234
     * $query->filterBySoftDelete(array(12, 34)); // WHERE Soft_delete IN (12, 34)
     * $query->filterBySoftDelete(array('min' => 12)); // WHERE Soft_delete >= 12
     * $query->filterBySoftDelete(array('max' => 12)); // WHERE Soft_delete <= 12
     * </code>
     *
     * @param     mixed $softDelete The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterBySoftDelete($softDelete = null, $comparison = null)
    {
        if (is_array($softDelete)) {
            $useMinMax = false;
            if (isset($softDelete['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::SOFT_DELETE, $softDelete['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($softDelete['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::SOFT_DELETE, $softDelete['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::SOFT_DELETE, $softDelete, $comparison);
    }

    /**
     * Filter the query on the last_sync column
     *
     * Example usage:
     * <code>
     * $query->filterByLastSync('2011-03-14'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync('now'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync(array('max' => 'yesterday')); // WHERE last_sync > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastSync The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByLastSync($lastSync = null, $comparison = null)
    {
        if (is_array($lastSync)) {
            $useMinMax = false;
            if (isset($lastSync['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::LAST_SYNC, $lastSync['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastSync['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::LAST_SYNC, $lastSync['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::LAST_SYNC, $lastSync, $comparison);
    }

    /**
     * Filter the query on the Updater_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdaterId('fooValue');   // WHERE Updater_ID = 'fooValue'
     * $query->filterByUpdaterId('%fooValue%'); // WHERE Updater_ID LIKE '%fooValue%'
     * </code>
     *
     * @param     string $updaterId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByUpdaterId($updaterId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($updaterId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $updaterId)) {
                $updaterId = str_replace('*', '%', $updaterId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::UPDATER_ID, $updaterId, $comparison);
    }

    /**
     * Filter the query by a related Prasarana object
     *
     * @param   Prasarana|PropelObjectCollection $prasarana The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PrasaranaLongitudinalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPrasaranaRelatedByPrasaranaId($prasarana, $comparison = null)
    {
        if ($prasarana instanceof Prasarana) {
            return $this
                ->addUsingAlias(PrasaranaLongitudinalPeer::PRASARANA_ID, $prasarana->getPrasaranaId(), $comparison);
        } elseif ($prasarana instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PrasaranaLongitudinalPeer::PRASARANA_ID, $prasarana->toKeyValue('PrimaryKey', 'PrasaranaId'), $comparison);
        } else {
            throw new PropelException('filterByPrasaranaRelatedByPrasaranaId() only accepts arguments of type Prasarana or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PrasaranaRelatedByPrasaranaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function joinPrasaranaRelatedByPrasaranaId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PrasaranaRelatedByPrasaranaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PrasaranaRelatedByPrasaranaId');
        }

        return $this;
    }

    /**
     * Use the PrasaranaRelatedByPrasaranaId relation Prasarana object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PrasaranaQuery A secondary query class using the current class as primary query
     */
    public function usePrasaranaRelatedByPrasaranaIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPrasaranaRelatedByPrasaranaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PrasaranaRelatedByPrasaranaId', '\angulex\Model\PrasaranaQuery');
    }

    /**
     * Filter the query by a related Prasarana object
     *
     * @param   Prasarana|PropelObjectCollection $prasarana The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PrasaranaLongitudinalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPrasaranaRelatedByPrasaranaId($prasarana, $comparison = null)
    {
        if ($prasarana instanceof Prasarana) {
            return $this
                ->addUsingAlias(PrasaranaLongitudinalPeer::PRASARANA_ID, $prasarana->getPrasaranaId(), $comparison);
        } elseif ($prasarana instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PrasaranaLongitudinalPeer::PRASARANA_ID, $prasarana->toKeyValue('PrimaryKey', 'PrasaranaId'), $comparison);
        } else {
            throw new PropelException('filterByPrasaranaRelatedByPrasaranaId() only accepts arguments of type Prasarana or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PrasaranaRelatedByPrasaranaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function joinPrasaranaRelatedByPrasaranaId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PrasaranaRelatedByPrasaranaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PrasaranaRelatedByPrasaranaId');
        }

        return $this;
    }

    /**
     * Use the PrasaranaRelatedByPrasaranaId relation Prasarana object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PrasaranaQuery A secondary query class using the current class as primary query
     */
    public function usePrasaranaRelatedByPrasaranaIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPrasaranaRelatedByPrasaranaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PrasaranaRelatedByPrasaranaId', '\angulex\Model\PrasaranaQuery');
    }

    /**
     * Filter the query by a related Semester object
     *
     * @param   Semester|PropelObjectCollection $semester The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PrasaranaLongitudinalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySemesterRelatedBySemesterId($semester, $comparison = null)
    {
        if ($semester instanceof Semester) {
            return $this
                ->addUsingAlias(PrasaranaLongitudinalPeer::SEMESTER_ID, $semester->getSemesterId(), $comparison);
        } elseif ($semester instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PrasaranaLongitudinalPeer::SEMESTER_ID, $semester->toKeyValue('PrimaryKey', 'SemesterId'), $comparison);
        } else {
            throw new PropelException('filterBySemesterRelatedBySemesterId() only accepts arguments of type Semester or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SemesterRelatedBySemesterId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function joinSemesterRelatedBySemesterId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SemesterRelatedBySemesterId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SemesterRelatedBySemesterId');
        }

        return $this;
    }

    /**
     * Use the SemesterRelatedBySemesterId relation Semester object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SemesterQuery A secondary query class using the current class as primary query
     */
    public function useSemesterRelatedBySemesterIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSemesterRelatedBySemesterId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SemesterRelatedBySemesterId', '\angulex\Model\SemesterQuery');
    }

    /**
     * Filter the query by a related Semester object
     *
     * @param   Semester|PropelObjectCollection $semester The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PrasaranaLongitudinalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySemesterRelatedBySemesterId($semester, $comparison = null)
    {
        if ($semester instanceof Semester) {
            return $this
                ->addUsingAlias(PrasaranaLongitudinalPeer::SEMESTER_ID, $semester->getSemesterId(), $comparison);
        } elseif ($semester instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PrasaranaLongitudinalPeer::SEMESTER_ID, $semester->toKeyValue('PrimaryKey', 'SemesterId'), $comparison);
        } else {
            throw new PropelException('filterBySemesterRelatedBySemesterId() only accepts arguments of type Semester or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SemesterRelatedBySemesterId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function joinSemesterRelatedBySemesterId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SemesterRelatedBySemesterId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SemesterRelatedBySemesterId');
        }

        return $this;
    }

    /**
     * Use the SemesterRelatedBySemesterId relation Semester object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SemesterQuery A secondary query class using the current class as primary query
     */
    public function useSemesterRelatedBySemesterIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSemesterRelatedBySemesterId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SemesterRelatedBySemesterId', '\angulex\Model\SemesterQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   PrasaranaLongitudinal $prasaranaLongitudinal Object to remove from the list of results
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function prune($prasaranaLongitudinal = null)
    {
        if ($prasaranaLongitudinal) {
            $this->addCond('pruneCond0', $this->getAliasedColName(PrasaranaLongitudinalPeer::PRASARANA_ID), $prasaranaLongitudinal->getPrasaranaId(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(PrasaranaLongitudinalPeer::SEMESTER_ID), $prasaranaLongitudinal->getSemesterId(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

}
