<?php

namespace angulex\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelDateTime;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\JenisKeluar;
use angulex\Model\JenisKeluarQuery;
use angulex\Model\Pembelajaran;
use angulex\Model\PembelajaranQuery;
use angulex\Model\Ptk;
use angulex\Model\PtkQuery;
use angulex\Model\PtkTerdaftar;
use angulex\Model\PtkTerdaftarPeer;
use angulex\Model\PtkTerdaftarQuery;
use angulex\Model\Sekolah;
use angulex\Model\SekolahQuery;
use angulex\Model\TahunAjaran;
use angulex\Model\TahunAjaranQuery;

/**
 * Base class that represents a row from the 'ptk_terdaftar' table.
 *
 * 
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BasePtkTerdaftar extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'angulex\\Model\\PtkTerdaftarPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        PtkTerdaftarPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the ptk_terdaftar_id field.
     * @var        string
     */
    protected $ptk_terdaftar_id;

    /**
     * The value for the ptk_id field.
     * @var        string
     */
    protected $ptk_id;

    /**
     * The value for the sekolah_id field.
     * @var        string
     */
    protected $sekolah_id;

    /**
     * The value for the tahun_ajaran_id field.
     * @var        string
     */
    protected $tahun_ajaran_id;

    /**
     * The value for the nomor_surat_tugas field.
     * @var        string
     */
    protected $nomor_surat_tugas;

    /**
     * The value for the tanggal_surat_tugas field.
     * @var        string
     */
    protected $tanggal_surat_tugas;

    /**
     * The value for the tmt_tugas field.
     * @var        string
     */
    protected $tmt_tugas;

    /**
     * The value for the ptk_induk field.
     * @var        string
     */
    protected $ptk_induk;

    /**
     * The value for the aktif_bulan_01 field.
     * Note: this column has a database default value of: '((0))'
     * @var        string
     */
    protected $aktif_bulan_01;

    /**
     * The value for the aktif_bulan_02 field.
     * Note: this column has a database default value of: '((0))'
     * @var        string
     */
    protected $aktif_bulan_02;

    /**
     * The value for the aktif_bulan_03 field.
     * Note: this column has a database default value of: '((0))'
     * @var        string
     */
    protected $aktif_bulan_03;

    /**
     * The value for the aktif_bulan_04 field.
     * Note: this column has a database default value of: '((0))'
     * @var        string
     */
    protected $aktif_bulan_04;

    /**
     * The value for the aktif_bulan_05 field.
     * Note: this column has a database default value of: '((0))'
     * @var        string
     */
    protected $aktif_bulan_05;

    /**
     * The value for the aktif_bulan_06 field.
     * Note: this column has a database default value of: '((0))'
     * @var        string
     */
    protected $aktif_bulan_06;

    /**
     * The value for the aktif_bulan_07 field.
     * Note: this column has a database default value of: '((0))'
     * @var        string
     */
    protected $aktif_bulan_07;

    /**
     * The value for the aktif_bulan_08 field.
     * Note: this column has a database default value of: '((0))'
     * @var        string
     */
    protected $aktif_bulan_08;

    /**
     * The value for the aktif_bulan_09 field.
     * Note: this column has a database default value of: '((0))'
     * @var        string
     */
    protected $aktif_bulan_09;

    /**
     * The value for the aktif_bulan_10 field.
     * Note: this column has a database default value of: '((0))'
     * @var        string
     */
    protected $aktif_bulan_10;

    /**
     * The value for the aktif_bulan_11 field.
     * Note: this column has a database default value of: '((0))'
     * @var        string
     */
    protected $aktif_bulan_11;

    /**
     * The value for the aktif_bulan_12 field.
     * Note: this column has a database default value of: '((0))'
     * @var        string
     */
    protected $aktif_bulan_12;

    /**
     * The value for the jenis_keluar_id field.
     * @var        string
     */
    protected $jenis_keluar_id;

    /**
     * The value for the tgl_ptk_keluar field.
     * @var        string
     */
    protected $tgl_ptk_keluar;

    /**
     * The value for the last_update field.
     * @var        string
     */
    protected $last_update;

    /**
     * The value for the soft_delete field.
     * @var        string
     */
    protected $soft_delete;

    /**
     * The value for the last_sync field.
     * @var        string
     */
    protected $last_sync;

    /**
     * The value for the updater_id field.
     * @var        string
     */
    protected $updater_id;

    /**
     * @var        Ptk
     */
    protected $aPtkRelatedByPtkId;

    /**
     * @var        Ptk
     */
    protected $aPtkRelatedByPtkId;

    /**
     * @var        Sekolah
     */
    protected $aSekolahRelatedBySekolahId;

    /**
     * @var        Sekolah
     */
    protected $aSekolahRelatedBySekolahId;

    /**
     * @var        JenisKeluar
     */
    protected $aJenisKeluarRelatedByJenisKeluarId;

    /**
     * @var        JenisKeluar
     */
    protected $aJenisKeluarRelatedByJenisKeluarId;

    /**
     * @var        TahunAjaran
     */
    protected $aTahunAjaranRelatedByTahunAjaranId;

    /**
     * @var        TahunAjaran
     */
    protected $aTahunAjaranRelatedByTahunAjaranId;

    /**
     * @var        PropelObjectCollection|Pembelajaran[] Collection to store aggregation of Pembelajaran objects.
     */
    protected $collPembelajaransRelatedByPtkTerdaftarId;
    protected $collPembelajaransRelatedByPtkTerdaftarIdPartial;

    /**
     * @var        PropelObjectCollection|Pembelajaran[] Collection to store aggregation of Pembelajaran objects.
     */
    protected $collPembelajaransRelatedByPtkTerdaftarId;
    protected $collPembelajaransRelatedByPtkTerdaftarIdPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $pembelajaransRelatedByPtkTerdaftarIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $pembelajaransRelatedByPtkTerdaftarIdScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->aktif_bulan_01 = '((0))';
        $this->aktif_bulan_02 = '((0))';
        $this->aktif_bulan_03 = '((0))';
        $this->aktif_bulan_04 = '((0))';
        $this->aktif_bulan_05 = '((0))';
        $this->aktif_bulan_06 = '((0))';
        $this->aktif_bulan_07 = '((0))';
        $this->aktif_bulan_08 = '((0))';
        $this->aktif_bulan_09 = '((0))';
        $this->aktif_bulan_10 = '((0))';
        $this->aktif_bulan_11 = '((0))';
        $this->aktif_bulan_12 = '((0))';
    }

    /**
     * Initializes internal state of BasePtkTerdaftar object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [ptk_terdaftar_id] column value.
     * 
     * @return string
     */
    public function getPtkTerdaftarId()
    {
        return $this->ptk_terdaftar_id;
    }

    /**
     * Get the [ptk_id] column value.
     * 
     * @return string
     */
    public function getPtkId()
    {
        return $this->ptk_id;
    }

    /**
     * Get the [sekolah_id] column value.
     * 
     * @return string
     */
    public function getSekolahId()
    {
        return $this->sekolah_id;
    }

    /**
     * Get the [tahun_ajaran_id] column value.
     * 
     * @return string
     */
    public function getTahunAjaranId()
    {
        return $this->tahun_ajaran_id;
    }

    /**
     * Get the [nomor_surat_tugas] column value.
     * 
     * @return string
     */
    public function getNomorSuratTugas()
    {
        return $this->nomor_surat_tugas;
    }

    /**
     * Get the [tanggal_surat_tugas] column value.
     * 
     * @return string
     */
    public function getTanggalSuratTugas()
    {
        return $this->tanggal_surat_tugas;
    }

    /**
     * Get the [tmt_tugas] column value.
     * 
     * @return string
     */
    public function getTmtTugas()
    {
        return $this->tmt_tugas;
    }

    /**
     * Get the [ptk_induk] column value.
     * 
     * @return string
     */
    public function getPtkInduk()
    {
        return $this->ptk_induk;
    }

    /**
     * Get the [aktif_bulan_01] column value.
     * 
     * @return string
     */
    public function getAktifBulan01()
    {
        return $this->aktif_bulan_01;
    }

    /**
     * Get the [aktif_bulan_02] column value.
     * 
     * @return string
     */
    public function getAktifBulan02()
    {
        return $this->aktif_bulan_02;
    }

    /**
     * Get the [aktif_bulan_03] column value.
     * 
     * @return string
     */
    public function getAktifBulan03()
    {
        return $this->aktif_bulan_03;
    }

    /**
     * Get the [aktif_bulan_04] column value.
     * 
     * @return string
     */
    public function getAktifBulan04()
    {
        return $this->aktif_bulan_04;
    }

    /**
     * Get the [aktif_bulan_05] column value.
     * 
     * @return string
     */
    public function getAktifBulan05()
    {
        return $this->aktif_bulan_05;
    }

    /**
     * Get the [aktif_bulan_06] column value.
     * 
     * @return string
     */
    public function getAktifBulan06()
    {
        return $this->aktif_bulan_06;
    }

    /**
     * Get the [aktif_bulan_07] column value.
     * 
     * @return string
     */
    public function getAktifBulan07()
    {
        return $this->aktif_bulan_07;
    }

    /**
     * Get the [aktif_bulan_08] column value.
     * 
     * @return string
     */
    public function getAktifBulan08()
    {
        return $this->aktif_bulan_08;
    }

    /**
     * Get the [aktif_bulan_09] column value.
     * 
     * @return string
     */
    public function getAktifBulan09()
    {
        return $this->aktif_bulan_09;
    }

    /**
     * Get the [aktif_bulan_10] column value.
     * 
     * @return string
     */
    public function getAktifBulan10()
    {
        return $this->aktif_bulan_10;
    }

    /**
     * Get the [aktif_bulan_11] column value.
     * 
     * @return string
     */
    public function getAktifBulan11()
    {
        return $this->aktif_bulan_11;
    }

    /**
     * Get the [aktif_bulan_12] column value.
     * 
     * @return string
     */
    public function getAktifBulan12()
    {
        return $this->aktif_bulan_12;
    }

    /**
     * Get the [jenis_keluar_id] column value.
     * 
     * @return string
     */
    public function getJenisKeluarId()
    {
        return $this->jenis_keluar_id;
    }

    /**
     * Get the [tgl_ptk_keluar] column value.
     * 
     * @return string
     */
    public function getTglPtkKeluar()
    {
        return $this->tgl_ptk_keluar;
    }

    /**
     * Get the [optionally formatted] temporal [last_update] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastUpdate($format = 'Y-m-d H:i:s')
    {
        if ($this->last_update === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_update);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_update, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [soft_delete] column value.
     * 
     * @return string
     */
    public function getSoftDelete()
    {
        return $this->soft_delete;
    }

    /**
     * Get the [optionally formatted] temporal [last_sync] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastSync($format = 'Y-m-d H:i:s')
    {
        if ($this->last_sync === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_sync);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_sync, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [updater_id] column value.
     * 
     * @return string
     */
    public function getUpdaterId()
    {
        return $this->updater_id;
    }

    /**
     * Set the value of [ptk_terdaftar_id] column.
     * 
     * @param string $v new value
     * @return PtkTerdaftar The current object (for fluent API support)
     */
    public function setPtkTerdaftarId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->ptk_terdaftar_id !== $v) {
            $this->ptk_terdaftar_id = $v;
            $this->modifiedColumns[] = PtkTerdaftarPeer::PTK_TERDAFTAR_ID;
        }


        return $this;
    } // setPtkTerdaftarId()

    /**
     * Set the value of [ptk_id] column.
     * 
     * @param string $v new value
     * @return PtkTerdaftar The current object (for fluent API support)
     */
    public function setPtkId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->ptk_id !== $v) {
            $this->ptk_id = $v;
            $this->modifiedColumns[] = PtkTerdaftarPeer::PTK_ID;
        }

        if ($this->aPtkRelatedByPtkId !== null && $this->aPtkRelatedByPtkId->getPtkId() !== $v) {
            $this->aPtkRelatedByPtkId = null;
        }

        if ($this->aPtkRelatedByPtkId !== null && $this->aPtkRelatedByPtkId->getPtkId() !== $v) {
            $this->aPtkRelatedByPtkId = null;
        }


        return $this;
    } // setPtkId()

    /**
     * Set the value of [sekolah_id] column.
     * 
     * @param string $v new value
     * @return PtkTerdaftar The current object (for fluent API support)
     */
    public function setSekolahId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->sekolah_id !== $v) {
            $this->sekolah_id = $v;
            $this->modifiedColumns[] = PtkTerdaftarPeer::SEKOLAH_ID;
        }

        if ($this->aSekolahRelatedBySekolahId !== null && $this->aSekolahRelatedBySekolahId->getSekolahId() !== $v) {
            $this->aSekolahRelatedBySekolahId = null;
        }

        if ($this->aSekolahRelatedBySekolahId !== null && $this->aSekolahRelatedBySekolahId->getSekolahId() !== $v) {
            $this->aSekolahRelatedBySekolahId = null;
        }


        return $this;
    } // setSekolahId()

    /**
     * Set the value of [tahun_ajaran_id] column.
     * 
     * @param string $v new value
     * @return PtkTerdaftar The current object (for fluent API support)
     */
    public function setTahunAjaranId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tahun_ajaran_id !== $v) {
            $this->tahun_ajaran_id = $v;
            $this->modifiedColumns[] = PtkTerdaftarPeer::TAHUN_AJARAN_ID;
        }

        if ($this->aTahunAjaranRelatedByTahunAjaranId !== null && $this->aTahunAjaranRelatedByTahunAjaranId->getTahunAjaranId() !== $v) {
            $this->aTahunAjaranRelatedByTahunAjaranId = null;
        }

        if ($this->aTahunAjaranRelatedByTahunAjaranId !== null && $this->aTahunAjaranRelatedByTahunAjaranId->getTahunAjaranId() !== $v) {
            $this->aTahunAjaranRelatedByTahunAjaranId = null;
        }


        return $this;
    } // setTahunAjaranId()

    /**
     * Set the value of [nomor_surat_tugas] column.
     * 
     * @param string $v new value
     * @return PtkTerdaftar The current object (for fluent API support)
     */
    public function setNomorSuratTugas($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nomor_surat_tugas !== $v) {
            $this->nomor_surat_tugas = $v;
            $this->modifiedColumns[] = PtkTerdaftarPeer::NOMOR_SURAT_TUGAS;
        }


        return $this;
    } // setNomorSuratTugas()

    /**
     * Set the value of [tanggal_surat_tugas] column.
     * 
     * @param string $v new value
     * @return PtkTerdaftar The current object (for fluent API support)
     */
    public function setTanggalSuratTugas($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tanggal_surat_tugas !== $v) {
            $this->tanggal_surat_tugas = $v;
            $this->modifiedColumns[] = PtkTerdaftarPeer::TANGGAL_SURAT_TUGAS;
        }


        return $this;
    } // setTanggalSuratTugas()

    /**
     * Set the value of [tmt_tugas] column.
     * 
     * @param string $v new value
     * @return PtkTerdaftar The current object (for fluent API support)
     */
    public function setTmtTugas($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tmt_tugas !== $v) {
            $this->tmt_tugas = $v;
            $this->modifiedColumns[] = PtkTerdaftarPeer::TMT_TUGAS;
        }


        return $this;
    } // setTmtTugas()

    /**
     * Set the value of [ptk_induk] column.
     * 
     * @param string $v new value
     * @return PtkTerdaftar The current object (for fluent API support)
     */
    public function setPtkInduk($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->ptk_induk !== $v) {
            $this->ptk_induk = $v;
            $this->modifiedColumns[] = PtkTerdaftarPeer::PTK_INDUK;
        }


        return $this;
    } // setPtkInduk()

    /**
     * Set the value of [aktif_bulan_01] column.
     * 
     * @param string $v new value
     * @return PtkTerdaftar The current object (for fluent API support)
     */
    public function setAktifBulan01($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->aktif_bulan_01 !== $v) {
            $this->aktif_bulan_01 = $v;
            $this->modifiedColumns[] = PtkTerdaftarPeer::AKTIF_BULAN_01;
        }


        return $this;
    } // setAktifBulan01()

    /**
     * Set the value of [aktif_bulan_02] column.
     * 
     * @param string $v new value
     * @return PtkTerdaftar The current object (for fluent API support)
     */
    public function setAktifBulan02($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->aktif_bulan_02 !== $v) {
            $this->aktif_bulan_02 = $v;
            $this->modifiedColumns[] = PtkTerdaftarPeer::AKTIF_BULAN_02;
        }


        return $this;
    } // setAktifBulan02()

    /**
     * Set the value of [aktif_bulan_03] column.
     * 
     * @param string $v new value
     * @return PtkTerdaftar The current object (for fluent API support)
     */
    public function setAktifBulan03($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->aktif_bulan_03 !== $v) {
            $this->aktif_bulan_03 = $v;
            $this->modifiedColumns[] = PtkTerdaftarPeer::AKTIF_BULAN_03;
        }


        return $this;
    } // setAktifBulan03()

    /**
     * Set the value of [aktif_bulan_04] column.
     * 
     * @param string $v new value
     * @return PtkTerdaftar The current object (for fluent API support)
     */
    public function setAktifBulan04($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->aktif_bulan_04 !== $v) {
            $this->aktif_bulan_04 = $v;
            $this->modifiedColumns[] = PtkTerdaftarPeer::AKTIF_BULAN_04;
        }


        return $this;
    } // setAktifBulan04()

    /**
     * Set the value of [aktif_bulan_05] column.
     * 
     * @param string $v new value
     * @return PtkTerdaftar The current object (for fluent API support)
     */
    public function setAktifBulan05($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->aktif_bulan_05 !== $v) {
            $this->aktif_bulan_05 = $v;
            $this->modifiedColumns[] = PtkTerdaftarPeer::AKTIF_BULAN_05;
        }


        return $this;
    } // setAktifBulan05()

    /**
     * Set the value of [aktif_bulan_06] column.
     * 
     * @param string $v new value
     * @return PtkTerdaftar The current object (for fluent API support)
     */
    public function setAktifBulan06($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->aktif_bulan_06 !== $v) {
            $this->aktif_bulan_06 = $v;
            $this->modifiedColumns[] = PtkTerdaftarPeer::AKTIF_BULAN_06;
        }


        return $this;
    } // setAktifBulan06()

    /**
     * Set the value of [aktif_bulan_07] column.
     * 
     * @param string $v new value
     * @return PtkTerdaftar The current object (for fluent API support)
     */
    public function setAktifBulan07($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->aktif_bulan_07 !== $v) {
            $this->aktif_bulan_07 = $v;
            $this->modifiedColumns[] = PtkTerdaftarPeer::AKTIF_BULAN_07;
        }


        return $this;
    } // setAktifBulan07()

    /**
     * Set the value of [aktif_bulan_08] column.
     * 
     * @param string $v new value
     * @return PtkTerdaftar The current object (for fluent API support)
     */
    public function setAktifBulan08($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->aktif_bulan_08 !== $v) {
            $this->aktif_bulan_08 = $v;
            $this->modifiedColumns[] = PtkTerdaftarPeer::AKTIF_BULAN_08;
        }


        return $this;
    } // setAktifBulan08()

    /**
     * Set the value of [aktif_bulan_09] column.
     * 
     * @param string $v new value
     * @return PtkTerdaftar The current object (for fluent API support)
     */
    public function setAktifBulan09($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->aktif_bulan_09 !== $v) {
            $this->aktif_bulan_09 = $v;
            $this->modifiedColumns[] = PtkTerdaftarPeer::AKTIF_BULAN_09;
        }


        return $this;
    } // setAktifBulan09()

    /**
     * Set the value of [aktif_bulan_10] column.
     * 
     * @param string $v new value
     * @return PtkTerdaftar The current object (for fluent API support)
     */
    public function setAktifBulan10($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->aktif_bulan_10 !== $v) {
            $this->aktif_bulan_10 = $v;
            $this->modifiedColumns[] = PtkTerdaftarPeer::AKTIF_BULAN_10;
        }


        return $this;
    } // setAktifBulan10()

    /**
     * Set the value of [aktif_bulan_11] column.
     * 
     * @param string $v new value
     * @return PtkTerdaftar The current object (for fluent API support)
     */
    public function setAktifBulan11($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->aktif_bulan_11 !== $v) {
            $this->aktif_bulan_11 = $v;
            $this->modifiedColumns[] = PtkTerdaftarPeer::AKTIF_BULAN_11;
        }


        return $this;
    } // setAktifBulan11()

    /**
     * Set the value of [aktif_bulan_12] column.
     * 
     * @param string $v new value
     * @return PtkTerdaftar The current object (for fluent API support)
     */
    public function setAktifBulan12($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->aktif_bulan_12 !== $v) {
            $this->aktif_bulan_12 = $v;
            $this->modifiedColumns[] = PtkTerdaftarPeer::AKTIF_BULAN_12;
        }


        return $this;
    } // setAktifBulan12()

    /**
     * Set the value of [jenis_keluar_id] column.
     * 
     * @param string $v new value
     * @return PtkTerdaftar The current object (for fluent API support)
     */
    public function setJenisKeluarId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->jenis_keluar_id !== $v) {
            $this->jenis_keluar_id = $v;
            $this->modifiedColumns[] = PtkTerdaftarPeer::JENIS_KELUAR_ID;
        }

        if ($this->aJenisKeluarRelatedByJenisKeluarId !== null && $this->aJenisKeluarRelatedByJenisKeluarId->getJenisKeluarId() !== $v) {
            $this->aJenisKeluarRelatedByJenisKeluarId = null;
        }

        if ($this->aJenisKeluarRelatedByJenisKeluarId !== null && $this->aJenisKeluarRelatedByJenisKeluarId->getJenisKeluarId() !== $v) {
            $this->aJenisKeluarRelatedByJenisKeluarId = null;
        }


        return $this;
    } // setJenisKeluarId()

    /**
     * Set the value of [tgl_ptk_keluar] column.
     * 
     * @param string $v new value
     * @return PtkTerdaftar The current object (for fluent API support)
     */
    public function setTglPtkKeluar($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tgl_ptk_keluar !== $v) {
            $this->tgl_ptk_keluar = $v;
            $this->modifiedColumns[] = PtkTerdaftarPeer::TGL_PTK_KELUAR;
        }


        return $this;
    } // setTglPtkKeluar()

    /**
     * Sets the value of [last_update] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return PtkTerdaftar The current object (for fluent API support)
     */
    public function setLastUpdate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_update !== null || $dt !== null) {
            $currentDateAsString = ($this->last_update !== null && $tmpDt = new DateTime($this->last_update)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_update = $newDateAsString;
                $this->modifiedColumns[] = PtkTerdaftarPeer::LAST_UPDATE;
            }
        } // if either are not null


        return $this;
    } // setLastUpdate()

    /**
     * Set the value of [soft_delete] column.
     * 
     * @param string $v new value
     * @return PtkTerdaftar The current object (for fluent API support)
     */
    public function setSoftDelete($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->soft_delete !== $v) {
            $this->soft_delete = $v;
            $this->modifiedColumns[] = PtkTerdaftarPeer::SOFT_DELETE;
        }


        return $this;
    } // setSoftDelete()

    /**
     * Sets the value of [last_sync] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return PtkTerdaftar The current object (for fluent API support)
     */
    public function setLastSync($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_sync !== null || $dt !== null) {
            $currentDateAsString = ($this->last_sync !== null && $tmpDt = new DateTime($this->last_sync)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_sync = $newDateAsString;
                $this->modifiedColumns[] = PtkTerdaftarPeer::LAST_SYNC;
            }
        } // if either are not null


        return $this;
    } // setLastSync()

    /**
     * Set the value of [updater_id] column.
     * 
     * @param string $v new value
     * @return PtkTerdaftar The current object (for fluent API support)
     */
    public function setUpdaterId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->updater_id !== $v) {
            $this->updater_id = $v;
            $this->modifiedColumns[] = PtkTerdaftarPeer::UPDATER_ID;
        }


        return $this;
    } // setUpdaterId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->aktif_bulan_01 !== '((0))') {
                return false;
            }

            if ($this->aktif_bulan_02 !== '((0))') {
                return false;
            }

            if ($this->aktif_bulan_03 !== '((0))') {
                return false;
            }

            if ($this->aktif_bulan_04 !== '((0))') {
                return false;
            }

            if ($this->aktif_bulan_05 !== '((0))') {
                return false;
            }

            if ($this->aktif_bulan_06 !== '((0))') {
                return false;
            }

            if ($this->aktif_bulan_07 !== '((0))') {
                return false;
            }

            if ($this->aktif_bulan_08 !== '((0))') {
                return false;
            }

            if ($this->aktif_bulan_09 !== '((0))') {
                return false;
            }

            if ($this->aktif_bulan_10 !== '((0))') {
                return false;
            }

            if ($this->aktif_bulan_11 !== '((0))') {
                return false;
            }

            if ($this->aktif_bulan_12 !== '((0))') {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->ptk_terdaftar_id = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
            $this->ptk_id = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->sekolah_id = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->tahun_ajaran_id = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->nomor_surat_tugas = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->tanggal_surat_tugas = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->tmt_tugas = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->ptk_induk = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->aktif_bulan_01 = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->aktif_bulan_02 = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->aktif_bulan_03 = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->aktif_bulan_04 = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->aktif_bulan_05 = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->aktif_bulan_06 = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->aktif_bulan_07 = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
            $this->aktif_bulan_08 = ($row[$startcol + 15] !== null) ? (string) $row[$startcol + 15] : null;
            $this->aktif_bulan_09 = ($row[$startcol + 16] !== null) ? (string) $row[$startcol + 16] : null;
            $this->aktif_bulan_10 = ($row[$startcol + 17] !== null) ? (string) $row[$startcol + 17] : null;
            $this->aktif_bulan_11 = ($row[$startcol + 18] !== null) ? (string) $row[$startcol + 18] : null;
            $this->aktif_bulan_12 = ($row[$startcol + 19] !== null) ? (string) $row[$startcol + 19] : null;
            $this->jenis_keluar_id = ($row[$startcol + 20] !== null) ? (string) $row[$startcol + 20] : null;
            $this->tgl_ptk_keluar = ($row[$startcol + 21] !== null) ? (string) $row[$startcol + 21] : null;
            $this->last_update = ($row[$startcol + 22] !== null) ? (string) $row[$startcol + 22] : null;
            $this->soft_delete = ($row[$startcol + 23] !== null) ? (string) $row[$startcol + 23] : null;
            $this->last_sync = ($row[$startcol + 24] !== null) ? (string) $row[$startcol + 24] : null;
            $this->updater_id = ($row[$startcol + 25] !== null) ? (string) $row[$startcol + 25] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 26; // 26 = PtkTerdaftarPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating PtkTerdaftar object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aPtkRelatedByPtkId !== null && $this->ptk_id !== $this->aPtkRelatedByPtkId->getPtkId()) {
            $this->aPtkRelatedByPtkId = null;
        }
        if ($this->aPtkRelatedByPtkId !== null && $this->ptk_id !== $this->aPtkRelatedByPtkId->getPtkId()) {
            $this->aPtkRelatedByPtkId = null;
        }
        if ($this->aSekolahRelatedBySekolahId !== null && $this->sekolah_id !== $this->aSekolahRelatedBySekolahId->getSekolahId()) {
            $this->aSekolahRelatedBySekolahId = null;
        }
        if ($this->aSekolahRelatedBySekolahId !== null && $this->sekolah_id !== $this->aSekolahRelatedBySekolahId->getSekolahId()) {
            $this->aSekolahRelatedBySekolahId = null;
        }
        if ($this->aTahunAjaranRelatedByTahunAjaranId !== null && $this->tahun_ajaran_id !== $this->aTahunAjaranRelatedByTahunAjaranId->getTahunAjaranId()) {
            $this->aTahunAjaranRelatedByTahunAjaranId = null;
        }
        if ($this->aTahunAjaranRelatedByTahunAjaranId !== null && $this->tahun_ajaran_id !== $this->aTahunAjaranRelatedByTahunAjaranId->getTahunAjaranId()) {
            $this->aTahunAjaranRelatedByTahunAjaranId = null;
        }
        if ($this->aJenisKeluarRelatedByJenisKeluarId !== null && $this->jenis_keluar_id !== $this->aJenisKeluarRelatedByJenisKeluarId->getJenisKeluarId()) {
            $this->aJenisKeluarRelatedByJenisKeluarId = null;
        }
        if ($this->aJenisKeluarRelatedByJenisKeluarId !== null && $this->jenis_keluar_id !== $this->aJenisKeluarRelatedByJenisKeluarId->getJenisKeluarId()) {
            $this->aJenisKeluarRelatedByJenisKeluarId = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PtkTerdaftarPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = PtkTerdaftarPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aPtkRelatedByPtkId = null;
            $this->aPtkRelatedByPtkId = null;
            $this->aSekolahRelatedBySekolahId = null;
            $this->aSekolahRelatedBySekolahId = null;
            $this->aJenisKeluarRelatedByJenisKeluarId = null;
            $this->aJenisKeluarRelatedByJenisKeluarId = null;
            $this->aTahunAjaranRelatedByTahunAjaranId = null;
            $this->aTahunAjaranRelatedByTahunAjaranId = null;
            $this->collPembelajaransRelatedByPtkTerdaftarId = null;

            $this->collPembelajaransRelatedByPtkTerdaftarId = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PtkTerdaftarPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = PtkTerdaftarQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PtkTerdaftarPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                PtkTerdaftarPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPtkRelatedByPtkId !== null) {
                if ($this->aPtkRelatedByPtkId->isModified() || $this->aPtkRelatedByPtkId->isNew()) {
                    $affectedRows += $this->aPtkRelatedByPtkId->save($con);
                }
                $this->setPtkRelatedByPtkId($this->aPtkRelatedByPtkId);
            }

            if ($this->aPtkRelatedByPtkId !== null) {
                if ($this->aPtkRelatedByPtkId->isModified() || $this->aPtkRelatedByPtkId->isNew()) {
                    $affectedRows += $this->aPtkRelatedByPtkId->save($con);
                }
                $this->setPtkRelatedByPtkId($this->aPtkRelatedByPtkId);
            }

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if ($this->aSekolahRelatedBySekolahId->isModified() || $this->aSekolahRelatedBySekolahId->isNew()) {
                    $affectedRows += $this->aSekolahRelatedBySekolahId->save($con);
                }
                $this->setSekolahRelatedBySekolahId($this->aSekolahRelatedBySekolahId);
            }

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if ($this->aSekolahRelatedBySekolahId->isModified() || $this->aSekolahRelatedBySekolahId->isNew()) {
                    $affectedRows += $this->aSekolahRelatedBySekolahId->save($con);
                }
                $this->setSekolahRelatedBySekolahId($this->aSekolahRelatedBySekolahId);
            }

            if ($this->aJenisKeluarRelatedByJenisKeluarId !== null) {
                if ($this->aJenisKeluarRelatedByJenisKeluarId->isModified() || $this->aJenisKeluarRelatedByJenisKeluarId->isNew()) {
                    $affectedRows += $this->aJenisKeluarRelatedByJenisKeluarId->save($con);
                }
                $this->setJenisKeluarRelatedByJenisKeluarId($this->aJenisKeluarRelatedByJenisKeluarId);
            }

            if ($this->aJenisKeluarRelatedByJenisKeluarId !== null) {
                if ($this->aJenisKeluarRelatedByJenisKeluarId->isModified() || $this->aJenisKeluarRelatedByJenisKeluarId->isNew()) {
                    $affectedRows += $this->aJenisKeluarRelatedByJenisKeluarId->save($con);
                }
                $this->setJenisKeluarRelatedByJenisKeluarId($this->aJenisKeluarRelatedByJenisKeluarId);
            }

            if ($this->aTahunAjaranRelatedByTahunAjaranId !== null) {
                if ($this->aTahunAjaranRelatedByTahunAjaranId->isModified() || $this->aTahunAjaranRelatedByTahunAjaranId->isNew()) {
                    $affectedRows += $this->aTahunAjaranRelatedByTahunAjaranId->save($con);
                }
                $this->setTahunAjaranRelatedByTahunAjaranId($this->aTahunAjaranRelatedByTahunAjaranId);
            }

            if ($this->aTahunAjaranRelatedByTahunAjaranId !== null) {
                if ($this->aTahunAjaranRelatedByTahunAjaranId->isModified() || $this->aTahunAjaranRelatedByTahunAjaranId->isNew()) {
                    $affectedRows += $this->aTahunAjaranRelatedByTahunAjaranId->save($con);
                }
                $this->setTahunAjaranRelatedByTahunAjaranId($this->aTahunAjaranRelatedByTahunAjaranId);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->pembelajaransRelatedByPtkTerdaftarIdScheduledForDeletion !== null) {
                if (!$this->pembelajaransRelatedByPtkTerdaftarIdScheduledForDeletion->isEmpty()) {
                    PembelajaranQuery::create()
                        ->filterByPrimaryKeys($this->pembelajaransRelatedByPtkTerdaftarIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->pembelajaransRelatedByPtkTerdaftarIdScheduledForDeletion = null;
                }
            }

            if ($this->collPembelajaransRelatedByPtkTerdaftarId !== null) {
                foreach ($this->collPembelajaransRelatedByPtkTerdaftarId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->pembelajaransRelatedByPtkTerdaftarIdScheduledForDeletion !== null) {
                if (!$this->pembelajaransRelatedByPtkTerdaftarIdScheduledForDeletion->isEmpty()) {
                    PembelajaranQuery::create()
                        ->filterByPrimaryKeys($this->pembelajaransRelatedByPtkTerdaftarIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->pembelajaransRelatedByPtkTerdaftarIdScheduledForDeletion = null;
                }
            }

            if ($this->collPembelajaransRelatedByPtkTerdaftarId !== null) {
                foreach ($this->collPembelajaransRelatedByPtkTerdaftarId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $criteria = $this->buildCriteria();
        $pk = BasePeer::doInsert($criteria, $con);
        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPtkRelatedByPtkId !== null) {
                if (!$this->aPtkRelatedByPtkId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPtkRelatedByPtkId->getValidationFailures());
                }
            }

            if ($this->aPtkRelatedByPtkId !== null) {
                if (!$this->aPtkRelatedByPtkId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPtkRelatedByPtkId->getValidationFailures());
                }
            }

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if (!$this->aSekolahRelatedBySekolahId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSekolahRelatedBySekolahId->getValidationFailures());
                }
            }

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if (!$this->aSekolahRelatedBySekolahId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSekolahRelatedBySekolahId->getValidationFailures());
                }
            }

            if ($this->aJenisKeluarRelatedByJenisKeluarId !== null) {
                if (!$this->aJenisKeluarRelatedByJenisKeluarId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aJenisKeluarRelatedByJenisKeluarId->getValidationFailures());
                }
            }

            if ($this->aJenisKeluarRelatedByJenisKeluarId !== null) {
                if (!$this->aJenisKeluarRelatedByJenisKeluarId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aJenisKeluarRelatedByJenisKeluarId->getValidationFailures());
                }
            }

            if ($this->aTahunAjaranRelatedByTahunAjaranId !== null) {
                if (!$this->aTahunAjaranRelatedByTahunAjaranId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aTahunAjaranRelatedByTahunAjaranId->getValidationFailures());
                }
            }

            if ($this->aTahunAjaranRelatedByTahunAjaranId !== null) {
                if (!$this->aTahunAjaranRelatedByTahunAjaranId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aTahunAjaranRelatedByTahunAjaranId->getValidationFailures());
                }
            }


            if (($retval = PtkTerdaftarPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collPembelajaransRelatedByPtkTerdaftarId !== null) {
                    foreach ($this->collPembelajaransRelatedByPtkTerdaftarId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPembelajaransRelatedByPtkTerdaftarId !== null) {
                    foreach ($this->collPembelajaransRelatedByPtkTerdaftarId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = PtkTerdaftarPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getPtkTerdaftarId();
                break;
            case 1:
                return $this->getPtkId();
                break;
            case 2:
                return $this->getSekolahId();
                break;
            case 3:
                return $this->getTahunAjaranId();
                break;
            case 4:
                return $this->getNomorSuratTugas();
                break;
            case 5:
                return $this->getTanggalSuratTugas();
                break;
            case 6:
                return $this->getTmtTugas();
                break;
            case 7:
                return $this->getPtkInduk();
                break;
            case 8:
                return $this->getAktifBulan01();
                break;
            case 9:
                return $this->getAktifBulan02();
                break;
            case 10:
                return $this->getAktifBulan03();
                break;
            case 11:
                return $this->getAktifBulan04();
                break;
            case 12:
                return $this->getAktifBulan05();
                break;
            case 13:
                return $this->getAktifBulan06();
                break;
            case 14:
                return $this->getAktifBulan07();
                break;
            case 15:
                return $this->getAktifBulan08();
                break;
            case 16:
                return $this->getAktifBulan09();
                break;
            case 17:
                return $this->getAktifBulan10();
                break;
            case 18:
                return $this->getAktifBulan11();
                break;
            case 19:
                return $this->getAktifBulan12();
                break;
            case 20:
                return $this->getJenisKeluarId();
                break;
            case 21:
                return $this->getTglPtkKeluar();
                break;
            case 22:
                return $this->getLastUpdate();
                break;
            case 23:
                return $this->getSoftDelete();
                break;
            case 24:
                return $this->getLastSync();
                break;
            case 25:
                return $this->getUpdaterId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['PtkTerdaftar'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['PtkTerdaftar'][$this->getPrimaryKey()] = true;
        $keys = PtkTerdaftarPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getPtkTerdaftarId(),
            $keys[1] => $this->getPtkId(),
            $keys[2] => $this->getSekolahId(),
            $keys[3] => $this->getTahunAjaranId(),
            $keys[4] => $this->getNomorSuratTugas(),
            $keys[5] => $this->getTanggalSuratTugas(),
            $keys[6] => $this->getTmtTugas(),
            $keys[7] => $this->getPtkInduk(),
            $keys[8] => $this->getAktifBulan01(),
            $keys[9] => $this->getAktifBulan02(),
            $keys[10] => $this->getAktifBulan03(),
            $keys[11] => $this->getAktifBulan04(),
            $keys[12] => $this->getAktifBulan05(),
            $keys[13] => $this->getAktifBulan06(),
            $keys[14] => $this->getAktifBulan07(),
            $keys[15] => $this->getAktifBulan08(),
            $keys[16] => $this->getAktifBulan09(),
            $keys[17] => $this->getAktifBulan10(),
            $keys[18] => $this->getAktifBulan11(),
            $keys[19] => $this->getAktifBulan12(),
            $keys[20] => $this->getJenisKeluarId(),
            $keys[21] => $this->getTglPtkKeluar(),
            $keys[22] => $this->getLastUpdate(),
            $keys[23] => $this->getSoftDelete(),
            $keys[24] => $this->getLastSync(),
            $keys[25] => $this->getUpdaterId(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aPtkRelatedByPtkId) {
                $result['PtkRelatedByPtkId'] = $this->aPtkRelatedByPtkId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPtkRelatedByPtkId) {
                $result['PtkRelatedByPtkId'] = $this->aPtkRelatedByPtkId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSekolahRelatedBySekolahId) {
                $result['SekolahRelatedBySekolahId'] = $this->aSekolahRelatedBySekolahId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSekolahRelatedBySekolahId) {
                $result['SekolahRelatedBySekolahId'] = $this->aSekolahRelatedBySekolahId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aJenisKeluarRelatedByJenisKeluarId) {
                $result['JenisKeluarRelatedByJenisKeluarId'] = $this->aJenisKeluarRelatedByJenisKeluarId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aJenisKeluarRelatedByJenisKeluarId) {
                $result['JenisKeluarRelatedByJenisKeluarId'] = $this->aJenisKeluarRelatedByJenisKeluarId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aTahunAjaranRelatedByTahunAjaranId) {
                $result['TahunAjaranRelatedByTahunAjaranId'] = $this->aTahunAjaranRelatedByTahunAjaranId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aTahunAjaranRelatedByTahunAjaranId) {
                $result['TahunAjaranRelatedByTahunAjaranId'] = $this->aTahunAjaranRelatedByTahunAjaranId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collPembelajaransRelatedByPtkTerdaftarId) {
                $result['PembelajaransRelatedByPtkTerdaftarId'] = $this->collPembelajaransRelatedByPtkTerdaftarId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPembelajaransRelatedByPtkTerdaftarId) {
                $result['PembelajaransRelatedByPtkTerdaftarId'] = $this->collPembelajaransRelatedByPtkTerdaftarId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = PtkTerdaftarPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setPtkTerdaftarId($value);
                break;
            case 1:
                $this->setPtkId($value);
                break;
            case 2:
                $this->setSekolahId($value);
                break;
            case 3:
                $this->setTahunAjaranId($value);
                break;
            case 4:
                $this->setNomorSuratTugas($value);
                break;
            case 5:
                $this->setTanggalSuratTugas($value);
                break;
            case 6:
                $this->setTmtTugas($value);
                break;
            case 7:
                $this->setPtkInduk($value);
                break;
            case 8:
                $this->setAktifBulan01($value);
                break;
            case 9:
                $this->setAktifBulan02($value);
                break;
            case 10:
                $this->setAktifBulan03($value);
                break;
            case 11:
                $this->setAktifBulan04($value);
                break;
            case 12:
                $this->setAktifBulan05($value);
                break;
            case 13:
                $this->setAktifBulan06($value);
                break;
            case 14:
                $this->setAktifBulan07($value);
                break;
            case 15:
                $this->setAktifBulan08($value);
                break;
            case 16:
                $this->setAktifBulan09($value);
                break;
            case 17:
                $this->setAktifBulan10($value);
                break;
            case 18:
                $this->setAktifBulan11($value);
                break;
            case 19:
                $this->setAktifBulan12($value);
                break;
            case 20:
                $this->setJenisKeluarId($value);
                break;
            case 21:
                $this->setTglPtkKeluar($value);
                break;
            case 22:
                $this->setLastUpdate($value);
                break;
            case 23:
                $this->setSoftDelete($value);
                break;
            case 24:
                $this->setLastSync($value);
                break;
            case 25:
                $this->setUpdaterId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = PtkTerdaftarPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setPtkTerdaftarId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setPtkId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setSekolahId($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setTahunAjaranId($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setNomorSuratTugas($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setTanggalSuratTugas($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setTmtTugas($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setPtkInduk($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setAktifBulan01($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setAktifBulan02($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setAktifBulan03($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setAktifBulan04($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setAktifBulan05($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setAktifBulan06($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setAktifBulan07($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setAktifBulan08($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setAktifBulan09($arr[$keys[16]]);
        if (array_key_exists($keys[17], $arr)) $this->setAktifBulan10($arr[$keys[17]]);
        if (array_key_exists($keys[18], $arr)) $this->setAktifBulan11($arr[$keys[18]]);
        if (array_key_exists($keys[19], $arr)) $this->setAktifBulan12($arr[$keys[19]]);
        if (array_key_exists($keys[20], $arr)) $this->setJenisKeluarId($arr[$keys[20]]);
        if (array_key_exists($keys[21], $arr)) $this->setTglPtkKeluar($arr[$keys[21]]);
        if (array_key_exists($keys[22], $arr)) $this->setLastUpdate($arr[$keys[22]]);
        if (array_key_exists($keys[23], $arr)) $this->setSoftDelete($arr[$keys[23]]);
        if (array_key_exists($keys[24], $arr)) $this->setLastSync($arr[$keys[24]]);
        if (array_key_exists($keys[25], $arr)) $this->setUpdaterId($arr[$keys[25]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(PtkTerdaftarPeer::DATABASE_NAME);

        if ($this->isColumnModified(PtkTerdaftarPeer::PTK_TERDAFTAR_ID)) $criteria->add(PtkTerdaftarPeer::PTK_TERDAFTAR_ID, $this->ptk_terdaftar_id);
        if ($this->isColumnModified(PtkTerdaftarPeer::PTK_ID)) $criteria->add(PtkTerdaftarPeer::PTK_ID, $this->ptk_id);
        if ($this->isColumnModified(PtkTerdaftarPeer::SEKOLAH_ID)) $criteria->add(PtkTerdaftarPeer::SEKOLAH_ID, $this->sekolah_id);
        if ($this->isColumnModified(PtkTerdaftarPeer::TAHUN_AJARAN_ID)) $criteria->add(PtkTerdaftarPeer::TAHUN_AJARAN_ID, $this->tahun_ajaran_id);
        if ($this->isColumnModified(PtkTerdaftarPeer::NOMOR_SURAT_TUGAS)) $criteria->add(PtkTerdaftarPeer::NOMOR_SURAT_TUGAS, $this->nomor_surat_tugas);
        if ($this->isColumnModified(PtkTerdaftarPeer::TANGGAL_SURAT_TUGAS)) $criteria->add(PtkTerdaftarPeer::TANGGAL_SURAT_TUGAS, $this->tanggal_surat_tugas);
        if ($this->isColumnModified(PtkTerdaftarPeer::TMT_TUGAS)) $criteria->add(PtkTerdaftarPeer::TMT_TUGAS, $this->tmt_tugas);
        if ($this->isColumnModified(PtkTerdaftarPeer::PTK_INDUK)) $criteria->add(PtkTerdaftarPeer::PTK_INDUK, $this->ptk_induk);
        if ($this->isColumnModified(PtkTerdaftarPeer::AKTIF_BULAN_01)) $criteria->add(PtkTerdaftarPeer::AKTIF_BULAN_01, $this->aktif_bulan_01);
        if ($this->isColumnModified(PtkTerdaftarPeer::AKTIF_BULAN_02)) $criteria->add(PtkTerdaftarPeer::AKTIF_BULAN_02, $this->aktif_bulan_02);
        if ($this->isColumnModified(PtkTerdaftarPeer::AKTIF_BULAN_03)) $criteria->add(PtkTerdaftarPeer::AKTIF_BULAN_03, $this->aktif_bulan_03);
        if ($this->isColumnModified(PtkTerdaftarPeer::AKTIF_BULAN_04)) $criteria->add(PtkTerdaftarPeer::AKTIF_BULAN_04, $this->aktif_bulan_04);
        if ($this->isColumnModified(PtkTerdaftarPeer::AKTIF_BULAN_05)) $criteria->add(PtkTerdaftarPeer::AKTIF_BULAN_05, $this->aktif_bulan_05);
        if ($this->isColumnModified(PtkTerdaftarPeer::AKTIF_BULAN_06)) $criteria->add(PtkTerdaftarPeer::AKTIF_BULAN_06, $this->aktif_bulan_06);
        if ($this->isColumnModified(PtkTerdaftarPeer::AKTIF_BULAN_07)) $criteria->add(PtkTerdaftarPeer::AKTIF_BULAN_07, $this->aktif_bulan_07);
        if ($this->isColumnModified(PtkTerdaftarPeer::AKTIF_BULAN_08)) $criteria->add(PtkTerdaftarPeer::AKTIF_BULAN_08, $this->aktif_bulan_08);
        if ($this->isColumnModified(PtkTerdaftarPeer::AKTIF_BULAN_09)) $criteria->add(PtkTerdaftarPeer::AKTIF_BULAN_09, $this->aktif_bulan_09);
        if ($this->isColumnModified(PtkTerdaftarPeer::AKTIF_BULAN_10)) $criteria->add(PtkTerdaftarPeer::AKTIF_BULAN_10, $this->aktif_bulan_10);
        if ($this->isColumnModified(PtkTerdaftarPeer::AKTIF_BULAN_11)) $criteria->add(PtkTerdaftarPeer::AKTIF_BULAN_11, $this->aktif_bulan_11);
        if ($this->isColumnModified(PtkTerdaftarPeer::AKTIF_BULAN_12)) $criteria->add(PtkTerdaftarPeer::AKTIF_BULAN_12, $this->aktif_bulan_12);
        if ($this->isColumnModified(PtkTerdaftarPeer::JENIS_KELUAR_ID)) $criteria->add(PtkTerdaftarPeer::JENIS_KELUAR_ID, $this->jenis_keluar_id);
        if ($this->isColumnModified(PtkTerdaftarPeer::TGL_PTK_KELUAR)) $criteria->add(PtkTerdaftarPeer::TGL_PTK_KELUAR, $this->tgl_ptk_keluar);
        if ($this->isColumnModified(PtkTerdaftarPeer::LAST_UPDATE)) $criteria->add(PtkTerdaftarPeer::LAST_UPDATE, $this->last_update);
        if ($this->isColumnModified(PtkTerdaftarPeer::SOFT_DELETE)) $criteria->add(PtkTerdaftarPeer::SOFT_DELETE, $this->soft_delete);
        if ($this->isColumnModified(PtkTerdaftarPeer::LAST_SYNC)) $criteria->add(PtkTerdaftarPeer::LAST_SYNC, $this->last_sync);
        if ($this->isColumnModified(PtkTerdaftarPeer::UPDATER_ID)) $criteria->add(PtkTerdaftarPeer::UPDATER_ID, $this->updater_id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(PtkTerdaftarPeer::DATABASE_NAME);
        $criteria->add(PtkTerdaftarPeer::PTK_TERDAFTAR_ID, $this->ptk_terdaftar_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getPtkTerdaftarId();
    }

    /**
     * Generic method to set the primary key (ptk_terdaftar_id column).
     *
     * @param  string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setPtkTerdaftarId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getPtkTerdaftarId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of PtkTerdaftar (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setPtkId($this->getPtkId());
        $copyObj->setSekolahId($this->getSekolahId());
        $copyObj->setTahunAjaranId($this->getTahunAjaranId());
        $copyObj->setNomorSuratTugas($this->getNomorSuratTugas());
        $copyObj->setTanggalSuratTugas($this->getTanggalSuratTugas());
        $copyObj->setTmtTugas($this->getTmtTugas());
        $copyObj->setPtkInduk($this->getPtkInduk());
        $copyObj->setAktifBulan01($this->getAktifBulan01());
        $copyObj->setAktifBulan02($this->getAktifBulan02());
        $copyObj->setAktifBulan03($this->getAktifBulan03());
        $copyObj->setAktifBulan04($this->getAktifBulan04());
        $copyObj->setAktifBulan05($this->getAktifBulan05());
        $copyObj->setAktifBulan06($this->getAktifBulan06());
        $copyObj->setAktifBulan07($this->getAktifBulan07());
        $copyObj->setAktifBulan08($this->getAktifBulan08());
        $copyObj->setAktifBulan09($this->getAktifBulan09());
        $copyObj->setAktifBulan10($this->getAktifBulan10());
        $copyObj->setAktifBulan11($this->getAktifBulan11());
        $copyObj->setAktifBulan12($this->getAktifBulan12());
        $copyObj->setJenisKeluarId($this->getJenisKeluarId());
        $copyObj->setTglPtkKeluar($this->getTglPtkKeluar());
        $copyObj->setLastUpdate($this->getLastUpdate());
        $copyObj->setSoftDelete($this->getSoftDelete());
        $copyObj->setLastSync($this->getLastSync());
        $copyObj->setUpdaterId($this->getUpdaterId());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getPembelajaransRelatedByPtkTerdaftarId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPembelajaranRelatedByPtkTerdaftarId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPembelajaransRelatedByPtkTerdaftarId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPembelajaranRelatedByPtkTerdaftarId($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setPtkTerdaftarId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return PtkTerdaftar Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return PtkTerdaftarPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new PtkTerdaftarPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a Ptk object.
     *
     * @param             Ptk $v
     * @return PtkTerdaftar The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPtkRelatedByPtkId(Ptk $v = null)
    {
        if ($v === null) {
            $this->setPtkId(NULL);
        } else {
            $this->setPtkId($v->getPtkId());
        }

        $this->aPtkRelatedByPtkId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Ptk object, it will not be re-added.
        if ($v !== null) {
            $v->addPtkTerdaftarRelatedByPtkId($this);
        }


        return $this;
    }


    /**
     * Get the associated Ptk object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Ptk The associated Ptk object.
     * @throws PropelException
     */
    public function getPtkRelatedByPtkId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPtkRelatedByPtkId === null && (($this->ptk_id !== "" && $this->ptk_id !== null)) && $doQuery) {
            $this->aPtkRelatedByPtkId = PtkQuery::create()->findPk($this->ptk_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPtkRelatedByPtkId->addPtkTerdaftarsRelatedByPtkId($this);
             */
        }

        return $this->aPtkRelatedByPtkId;
    }

    /**
     * Declares an association between this object and a Ptk object.
     *
     * @param             Ptk $v
     * @return PtkTerdaftar The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPtkRelatedByPtkId(Ptk $v = null)
    {
        if ($v === null) {
            $this->setPtkId(NULL);
        } else {
            $this->setPtkId($v->getPtkId());
        }

        $this->aPtkRelatedByPtkId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Ptk object, it will not be re-added.
        if ($v !== null) {
            $v->addPtkTerdaftarRelatedByPtkId($this);
        }


        return $this;
    }


    /**
     * Get the associated Ptk object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Ptk The associated Ptk object.
     * @throws PropelException
     */
    public function getPtkRelatedByPtkId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPtkRelatedByPtkId === null && (($this->ptk_id !== "" && $this->ptk_id !== null)) && $doQuery) {
            $this->aPtkRelatedByPtkId = PtkQuery::create()->findPk($this->ptk_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPtkRelatedByPtkId->addPtkTerdaftarsRelatedByPtkId($this);
             */
        }

        return $this->aPtkRelatedByPtkId;
    }

    /**
     * Declares an association between this object and a Sekolah object.
     *
     * @param             Sekolah $v
     * @return PtkTerdaftar The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSekolahRelatedBySekolahId(Sekolah $v = null)
    {
        if ($v === null) {
            $this->setSekolahId(NULL);
        } else {
            $this->setSekolahId($v->getSekolahId());
        }

        $this->aSekolahRelatedBySekolahId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Sekolah object, it will not be re-added.
        if ($v !== null) {
            $v->addPtkTerdaftarRelatedBySekolahId($this);
        }


        return $this;
    }


    /**
     * Get the associated Sekolah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Sekolah The associated Sekolah object.
     * @throws PropelException
     */
    public function getSekolahRelatedBySekolahId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSekolahRelatedBySekolahId === null && (($this->sekolah_id !== "" && $this->sekolah_id !== null)) && $doQuery) {
            $this->aSekolahRelatedBySekolahId = SekolahQuery::create()->findPk($this->sekolah_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSekolahRelatedBySekolahId->addPtkTerdaftarsRelatedBySekolahId($this);
             */
        }

        return $this->aSekolahRelatedBySekolahId;
    }

    /**
     * Declares an association between this object and a Sekolah object.
     *
     * @param             Sekolah $v
     * @return PtkTerdaftar The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSekolahRelatedBySekolahId(Sekolah $v = null)
    {
        if ($v === null) {
            $this->setSekolahId(NULL);
        } else {
            $this->setSekolahId($v->getSekolahId());
        }

        $this->aSekolahRelatedBySekolahId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Sekolah object, it will not be re-added.
        if ($v !== null) {
            $v->addPtkTerdaftarRelatedBySekolahId($this);
        }


        return $this;
    }


    /**
     * Get the associated Sekolah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Sekolah The associated Sekolah object.
     * @throws PropelException
     */
    public function getSekolahRelatedBySekolahId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSekolahRelatedBySekolahId === null && (($this->sekolah_id !== "" && $this->sekolah_id !== null)) && $doQuery) {
            $this->aSekolahRelatedBySekolahId = SekolahQuery::create()->findPk($this->sekolah_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSekolahRelatedBySekolahId->addPtkTerdaftarsRelatedBySekolahId($this);
             */
        }

        return $this->aSekolahRelatedBySekolahId;
    }

    /**
     * Declares an association between this object and a JenisKeluar object.
     *
     * @param             JenisKeluar $v
     * @return PtkTerdaftar The current object (for fluent API support)
     * @throws PropelException
     */
    public function setJenisKeluarRelatedByJenisKeluarId(JenisKeluar $v = null)
    {
        if ($v === null) {
            $this->setJenisKeluarId(NULL);
        } else {
            $this->setJenisKeluarId($v->getJenisKeluarId());
        }

        $this->aJenisKeluarRelatedByJenisKeluarId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the JenisKeluar object, it will not be re-added.
        if ($v !== null) {
            $v->addPtkTerdaftarRelatedByJenisKeluarId($this);
        }


        return $this;
    }


    /**
     * Get the associated JenisKeluar object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return JenisKeluar The associated JenisKeluar object.
     * @throws PropelException
     */
    public function getJenisKeluarRelatedByJenisKeluarId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aJenisKeluarRelatedByJenisKeluarId === null && (($this->jenis_keluar_id !== "" && $this->jenis_keluar_id !== null)) && $doQuery) {
            $this->aJenisKeluarRelatedByJenisKeluarId = JenisKeluarQuery::create()->findPk($this->jenis_keluar_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aJenisKeluarRelatedByJenisKeluarId->addPtkTerdaftarsRelatedByJenisKeluarId($this);
             */
        }

        return $this->aJenisKeluarRelatedByJenisKeluarId;
    }

    /**
     * Declares an association between this object and a JenisKeluar object.
     *
     * @param             JenisKeluar $v
     * @return PtkTerdaftar The current object (for fluent API support)
     * @throws PropelException
     */
    public function setJenisKeluarRelatedByJenisKeluarId(JenisKeluar $v = null)
    {
        if ($v === null) {
            $this->setJenisKeluarId(NULL);
        } else {
            $this->setJenisKeluarId($v->getJenisKeluarId());
        }

        $this->aJenisKeluarRelatedByJenisKeluarId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the JenisKeluar object, it will not be re-added.
        if ($v !== null) {
            $v->addPtkTerdaftarRelatedByJenisKeluarId($this);
        }


        return $this;
    }


    /**
     * Get the associated JenisKeluar object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return JenisKeluar The associated JenisKeluar object.
     * @throws PropelException
     */
    public function getJenisKeluarRelatedByJenisKeluarId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aJenisKeluarRelatedByJenisKeluarId === null && (($this->jenis_keluar_id !== "" && $this->jenis_keluar_id !== null)) && $doQuery) {
            $this->aJenisKeluarRelatedByJenisKeluarId = JenisKeluarQuery::create()->findPk($this->jenis_keluar_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aJenisKeluarRelatedByJenisKeluarId->addPtkTerdaftarsRelatedByJenisKeluarId($this);
             */
        }

        return $this->aJenisKeluarRelatedByJenisKeluarId;
    }

    /**
     * Declares an association between this object and a TahunAjaran object.
     *
     * @param             TahunAjaran $v
     * @return PtkTerdaftar The current object (for fluent API support)
     * @throws PropelException
     */
    public function setTahunAjaranRelatedByTahunAjaranId(TahunAjaran $v = null)
    {
        if ($v === null) {
            $this->setTahunAjaranId(NULL);
        } else {
            $this->setTahunAjaranId($v->getTahunAjaranId());
        }

        $this->aTahunAjaranRelatedByTahunAjaranId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the TahunAjaran object, it will not be re-added.
        if ($v !== null) {
            $v->addPtkTerdaftarRelatedByTahunAjaranId($this);
        }


        return $this;
    }


    /**
     * Get the associated TahunAjaran object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return TahunAjaran The associated TahunAjaran object.
     * @throws PropelException
     */
    public function getTahunAjaranRelatedByTahunAjaranId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aTahunAjaranRelatedByTahunAjaranId === null && (($this->tahun_ajaran_id !== "" && $this->tahun_ajaran_id !== null)) && $doQuery) {
            $this->aTahunAjaranRelatedByTahunAjaranId = TahunAjaranQuery::create()->findPk($this->tahun_ajaran_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aTahunAjaranRelatedByTahunAjaranId->addPtkTerdaftarsRelatedByTahunAjaranId($this);
             */
        }

        return $this->aTahunAjaranRelatedByTahunAjaranId;
    }

    /**
     * Declares an association between this object and a TahunAjaran object.
     *
     * @param             TahunAjaran $v
     * @return PtkTerdaftar The current object (for fluent API support)
     * @throws PropelException
     */
    public function setTahunAjaranRelatedByTahunAjaranId(TahunAjaran $v = null)
    {
        if ($v === null) {
            $this->setTahunAjaranId(NULL);
        } else {
            $this->setTahunAjaranId($v->getTahunAjaranId());
        }

        $this->aTahunAjaranRelatedByTahunAjaranId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the TahunAjaran object, it will not be re-added.
        if ($v !== null) {
            $v->addPtkTerdaftarRelatedByTahunAjaranId($this);
        }


        return $this;
    }


    /**
     * Get the associated TahunAjaran object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return TahunAjaran The associated TahunAjaran object.
     * @throws PropelException
     */
    public function getTahunAjaranRelatedByTahunAjaranId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aTahunAjaranRelatedByTahunAjaranId === null && (($this->tahun_ajaran_id !== "" && $this->tahun_ajaran_id !== null)) && $doQuery) {
            $this->aTahunAjaranRelatedByTahunAjaranId = TahunAjaranQuery::create()->findPk($this->tahun_ajaran_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aTahunAjaranRelatedByTahunAjaranId->addPtkTerdaftarsRelatedByTahunAjaranId($this);
             */
        }

        return $this->aTahunAjaranRelatedByTahunAjaranId;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('PembelajaranRelatedByPtkTerdaftarId' == $relationName) {
            $this->initPembelajaransRelatedByPtkTerdaftarId();
        }
        if ('PembelajaranRelatedByPtkTerdaftarId' == $relationName) {
            $this->initPembelajaransRelatedByPtkTerdaftarId();
        }
    }

    /**
     * Clears out the collPembelajaransRelatedByPtkTerdaftarId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return PtkTerdaftar The current object (for fluent API support)
     * @see        addPembelajaransRelatedByPtkTerdaftarId()
     */
    public function clearPembelajaransRelatedByPtkTerdaftarId()
    {
        $this->collPembelajaransRelatedByPtkTerdaftarId = null; // important to set this to null since that means it is uninitialized
        $this->collPembelajaransRelatedByPtkTerdaftarIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPembelajaransRelatedByPtkTerdaftarId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPembelajaransRelatedByPtkTerdaftarId($v = true)
    {
        $this->collPembelajaransRelatedByPtkTerdaftarIdPartial = $v;
    }

    /**
     * Initializes the collPembelajaransRelatedByPtkTerdaftarId collection.
     *
     * By default this just sets the collPembelajaransRelatedByPtkTerdaftarId collection to an empty array (like clearcollPembelajaransRelatedByPtkTerdaftarId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPembelajaransRelatedByPtkTerdaftarId($overrideExisting = true)
    {
        if (null !== $this->collPembelajaransRelatedByPtkTerdaftarId && !$overrideExisting) {
            return;
        }
        $this->collPembelajaransRelatedByPtkTerdaftarId = new PropelObjectCollection();
        $this->collPembelajaransRelatedByPtkTerdaftarId->setModel('Pembelajaran');
    }

    /**
     * Gets an array of Pembelajaran objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this PtkTerdaftar is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     * @throws PropelException
     */
    public function getPembelajaransRelatedByPtkTerdaftarId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPembelajaransRelatedByPtkTerdaftarIdPartial && !$this->isNew();
        if (null === $this->collPembelajaransRelatedByPtkTerdaftarId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPembelajaransRelatedByPtkTerdaftarId) {
                // return empty collection
                $this->initPembelajaransRelatedByPtkTerdaftarId();
            } else {
                $collPembelajaransRelatedByPtkTerdaftarId = PembelajaranQuery::create(null, $criteria)
                    ->filterByPtkTerdaftarRelatedByPtkTerdaftarId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPembelajaransRelatedByPtkTerdaftarIdPartial && count($collPembelajaransRelatedByPtkTerdaftarId)) {
                      $this->initPembelajaransRelatedByPtkTerdaftarId(false);

                      foreach($collPembelajaransRelatedByPtkTerdaftarId as $obj) {
                        if (false == $this->collPembelajaransRelatedByPtkTerdaftarId->contains($obj)) {
                          $this->collPembelajaransRelatedByPtkTerdaftarId->append($obj);
                        }
                      }

                      $this->collPembelajaransRelatedByPtkTerdaftarIdPartial = true;
                    }

                    $collPembelajaransRelatedByPtkTerdaftarId->getInternalIterator()->rewind();
                    return $collPembelajaransRelatedByPtkTerdaftarId;
                }

                if($partial && $this->collPembelajaransRelatedByPtkTerdaftarId) {
                    foreach($this->collPembelajaransRelatedByPtkTerdaftarId as $obj) {
                        if($obj->isNew()) {
                            $collPembelajaransRelatedByPtkTerdaftarId[] = $obj;
                        }
                    }
                }

                $this->collPembelajaransRelatedByPtkTerdaftarId = $collPembelajaransRelatedByPtkTerdaftarId;
                $this->collPembelajaransRelatedByPtkTerdaftarIdPartial = false;
            }
        }

        return $this->collPembelajaransRelatedByPtkTerdaftarId;
    }

    /**
     * Sets a collection of PembelajaranRelatedByPtkTerdaftarId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $pembelajaransRelatedByPtkTerdaftarId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return PtkTerdaftar The current object (for fluent API support)
     */
    public function setPembelajaransRelatedByPtkTerdaftarId(PropelCollection $pembelajaransRelatedByPtkTerdaftarId, PropelPDO $con = null)
    {
        $pembelajaransRelatedByPtkTerdaftarIdToDelete = $this->getPembelajaransRelatedByPtkTerdaftarId(new Criteria(), $con)->diff($pembelajaransRelatedByPtkTerdaftarId);

        $this->pembelajaransRelatedByPtkTerdaftarIdScheduledForDeletion = unserialize(serialize($pembelajaransRelatedByPtkTerdaftarIdToDelete));

        foreach ($pembelajaransRelatedByPtkTerdaftarIdToDelete as $pembelajaranRelatedByPtkTerdaftarIdRemoved) {
            $pembelajaranRelatedByPtkTerdaftarIdRemoved->setPtkTerdaftarRelatedByPtkTerdaftarId(null);
        }

        $this->collPembelajaransRelatedByPtkTerdaftarId = null;
        foreach ($pembelajaransRelatedByPtkTerdaftarId as $pembelajaranRelatedByPtkTerdaftarId) {
            $this->addPembelajaranRelatedByPtkTerdaftarId($pembelajaranRelatedByPtkTerdaftarId);
        }

        $this->collPembelajaransRelatedByPtkTerdaftarId = $pembelajaransRelatedByPtkTerdaftarId;
        $this->collPembelajaransRelatedByPtkTerdaftarIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Pembelajaran objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Pembelajaran objects.
     * @throws PropelException
     */
    public function countPembelajaransRelatedByPtkTerdaftarId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPembelajaransRelatedByPtkTerdaftarIdPartial && !$this->isNew();
        if (null === $this->collPembelajaransRelatedByPtkTerdaftarId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPembelajaransRelatedByPtkTerdaftarId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPembelajaransRelatedByPtkTerdaftarId());
            }
            $query = PembelajaranQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkTerdaftarRelatedByPtkTerdaftarId($this)
                ->count($con);
        }

        return count($this->collPembelajaransRelatedByPtkTerdaftarId);
    }

    /**
     * Method called to associate a Pembelajaran object to this object
     * through the Pembelajaran foreign key attribute.
     *
     * @param    Pembelajaran $l Pembelajaran
     * @return PtkTerdaftar The current object (for fluent API support)
     */
    public function addPembelajaranRelatedByPtkTerdaftarId(Pembelajaran $l)
    {
        if ($this->collPembelajaransRelatedByPtkTerdaftarId === null) {
            $this->initPembelajaransRelatedByPtkTerdaftarId();
            $this->collPembelajaransRelatedByPtkTerdaftarIdPartial = true;
        }
        if (!in_array($l, $this->collPembelajaransRelatedByPtkTerdaftarId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPembelajaranRelatedByPtkTerdaftarId($l);
        }

        return $this;
    }

    /**
     * @param	PembelajaranRelatedByPtkTerdaftarId $pembelajaranRelatedByPtkTerdaftarId The pembelajaranRelatedByPtkTerdaftarId object to add.
     */
    protected function doAddPembelajaranRelatedByPtkTerdaftarId($pembelajaranRelatedByPtkTerdaftarId)
    {
        $this->collPembelajaransRelatedByPtkTerdaftarId[]= $pembelajaranRelatedByPtkTerdaftarId;
        $pembelajaranRelatedByPtkTerdaftarId->setPtkTerdaftarRelatedByPtkTerdaftarId($this);
    }

    /**
     * @param	PembelajaranRelatedByPtkTerdaftarId $pembelajaranRelatedByPtkTerdaftarId The pembelajaranRelatedByPtkTerdaftarId object to remove.
     * @return PtkTerdaftar The current object (for fluent API support)
     */
    public function removePembelajaranRelatedByPtkTerdaftarId($pembelajaranRelatedByPtkTerdaftarId)
    {
        if ($this->getPembelajaransRelatedByPtkTerdaftarId()->contains($pembelajaranRelatedByPtkTerdaftarId)) {
            $this->collPembelajaransRelatedByPtkTerdaftarId->remove($this->collPembelajaransRelatedByPtkTerdaftarId->search($pembelajaranRelatedByPtkTerdaftarId));
            if (null === $this->pembelajaransRelatedByPtkTerdaftarIdScheduledForDeletion) {
                $this->pembelajaransRelatedByPtkTerdaftarIdScheduledForDeletion = clone $this->collPembelajaransRelatedByPtkTerdaftarId;
                $this->pembelajaransRelatedByPtkTerdaftarIdScheduledForDeletion->clear();
            }
            $this->pembelajaransRelatedByPtkTerdaftarIdScheduledForDeletion[]= clone $pembelajaranRelatedByPtkTerdaftarId;
            $pembelajaranRelatedByPtkTerdaftarId->setPtkTerdaftarRelatedByPtkTerdaftarId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PtkTerdaftar is new, it will return
     * an empty collection; or if this PtkTerdaftar has previously
     * been saved, it will retrieve related PembelajaransRelatedByPtkTerdaftarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PtkTerdaftar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     */
    public function getPembelajaransRelatedByPtkTerdaftarIdJoinRombonganBelajarRelatedByRombonganBelajarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PembelajaranQuery::create(null, $criteria);
        $query->joinWith('RombonganBelajarRelatedByRombonganBelajarId', $join_behavior);

        return $this->getPembelajaransRelatedByPtkTerdaftarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PtkTerdaftar is new, it will return
     * an empty collection; or if this PtkTerdaftar has previously
     * been saved, it will retrieve related PembelajaransRelatedByPtkTerdaftarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PtkTerdaftar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     */
    public function getPembelajaransRelatedByPtkTerdaftarIdJoinRombonganBelajarRelatedByRombonganBelajarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PembelajaranQuery::create(null, $criteria);
        $query->joinWith('RombonganBelajarRelatedByRombonganBelajarId', $join_behavior);

        return $this->getPembelajaransRelatedByPtkTerdaftarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PtkTerdaftar is new, it will return
     * an empty collection; or if this PtkTerdaftar has previously
     * been saved, it will retrieve related PembelajaransRelatedByPtkTerdaftarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PtkTerdaftar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     */
    public function getPembelajaransRelatedByPtkTerdaftarIdJoinMataPelajaranRelatedByMataPelajaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PembelajaranQuery::create(null, $criteria);
        $query->joinWith('MataPelajaranRelatedByMataPelajaranId', $join_behavior);

        return $this->getPembelajaransRelatedByPtkTerdaftarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PtkTerdaftar is new, it will return
     * an empty collection; or if this PtkTerdaftar has previously
     * been saved, it will retrieve related PembelajaransRelatedByPtkTerdaftarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PtkTerdaftar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     */
    public function getPembelajaransRelatedByPtkTerdaftarIdJoinMataPelajaranRelatedByMataPelajaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PembelajaranQuery::create(null, $criteria);
        $query->joinWith('MataPelajaranRelatedByMataPelajaranId', $join_behavior);

        return $this->getPembelajaransRelatedByPtkTerdaftarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PtkTerdaftar is new, it will return
     * an empty collection; or if this PtkTerdaftar has previously
     * been saved, it will retrieve related PembelajaransRelatedByPtkTerdaftarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PtkTerdaftar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     */
    public function getPembelajaransRelatedByPtkTerdaftarIdJoinSemesterRelatedBySemesterId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PembelajaranQuery::create(null, $criteria);
        $query->joinWith('SemesterRelatedBySemesterId', $join_behavior);

        return $this->getPembelajaransRelatedByPtkTerdaftarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PtkTerdaftar is new, it will return
     * an empty collection; or if this PtkTerdaftar has previously
     * been saved, it will retrieve related PembelajaransRelatedByPtkTerdaftarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PtkTerdaftar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     */
    public function getPembelajaransRelatedByPtkTerdaftarIdJoinSemesterRelatedBySemesterId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PembelajaranQuery::create(null, $criteria);
        $query->joinWith('SemesterRelatedBySemesterId', $join_behavior);

        return $this->getPembelajaransRelatedByPtkTerdaftarId($query, $con);
    }

    /**
     * Clears out the collPembelajaransRelatedByPtkTerdaftarId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return PtkTerdaftar The current object (for fluent API support)
     * @see        addPembelajaransRelatedByPtkTerdaftarId()
     */
    public function clearPembelajaransRelatedByPtkTerdaftarId()
    {
        $this->collPembelajaransRelatedByPtkTerdaftarId = null; // important to set this to null since that means it is uninitialized
        $this->collPembelajaransRelatedByPtkTerdaftarIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPembelajaransRelatedByPtkTerdaftarId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPembelajaransRelatedByPtkTerdaftarId($v = true)
    {
        $this->collPembelajaransRelatedByPtkTerdaftarIdPartial = $v;
    }

    /**
     * Initializes the collPembelajaransRelatedByPtkTerdaftarId collection.
     *
     * By default this just sets the collPembelajaransRelatedByPtkTerdaftarId collection to an empty array (like clearcollPembelajaransRelatedByPtkTerdaftarId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPembelajaransRelatedByPtkTerdaftarId($overrideExisting = true)
    {
        if (null !== $this->collPembelajaransRelatedByPtkTerdaftarId && !$overrideExisting) {
            return;
        }
        $this->collPembelajaransRelatedByPtkTerdaftarId = new PropelObjectCollection();
        $this->collPembelajaransRelatedByPtkTerdaftarId->setModel('Pembelajaran');
    }

    /**
     * Gets an array of Pembelajaran objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this PtkTerdaftar is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     * @throws PropelException
     */
    public function getPembelajaransRelatedByPtkTerdaftarId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPembelajaransRelatedByPtkTerdaftarIdPartial && !$this->isNew();
        if (null === $this->collPembelajaransRelatedByPtkTerdaftarId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPembelajaransRelatedByPtkTerdaftarId) {
                // return empty collection
                $this->initPembelajaransRelatedByPtkTerdaftarId();
            } else {
                $collPembelajaransRelatedByPtkTerdaftarId = PembelajaranQuery::create(null, $criteria)
                    ->filterByPtkTerdaftarRelatedByPtkTerdaftarId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPembelajaransRelatedByPtkTerdaftarIdPartial && count($collPembelajaransRelatedByPtkTerdaftarId)) {
                      $this->initPembelajaransRelatedByPtkTerdaftarId(false);

                      foreach($collPembelajaransRelatedByPtkTerdaftarId as $obj) {
                        if (false == $this->collPembelajaransRelatedByPtkTerdaftarId->contains($obj)) {
                          $this->collPembelajaransRelatedByPtkTerdaftarId->append($obj);
                        }
                      }

                      $this->collPembelajaransRelatedByPtkTerdaftarIdPartial = true;
                    }

                    $collPembelajaransRelatedByPtkTerdaftarId->getInternalIterator()->rewind();
                    return $collPembelajaransRelatedByPtkTerdaftarId;
                }

                if($partial && $this->collPembelajaransRelatedByPtkTerdaftarId) {
                    foreach($this->collPembelajaransRelatedByPtkTerdaftarId as $obj) {
                        if($obj->isNew()) {
                            $collPembelajaransRelatedByPtkTerdaftarId[] = $obj;
                        }
                    }
                }

                $this->collPembelajaransRelatedByPtkTerdaftarId = $collPembelajaransRelatedByPtkTerdaftarId;
                $this->collPembelajaransRelatedByPtkTerdaftarIdPartial = false;
            }
        }

        return $this->collPembelajaransRelatedByPtkTerdaftarId;
    }

    /**
     * Sets a collection of PembelajaranRelatedByPtkTerdaftarId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $pembelajaransRelatedByPtkTerdaftarId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return PtkTerdaftar The current object (for fluent API support)
     */
    public function setPembelajaransRelatedByPtkTerdaftarId(PropelCollection $pembelajaransRelatedByPtkTerdaftarId, PropelPDO $con = null)
    {
        $pembelajaransRelatedByPtkTerdaftarIdToDelete = $this->getPembelajaransRelatedByPtkTerdaftarId(new Criteria(), $con)->diff($pembelajaransRelatedByPtkTerdaftarId);

        $this->pembelajaransRelatedByPtkTerdaftarIdScheduledForDeletion = unserialize(serialize($pembelajaransRelatedByPtkTerdaftarIdToDelete));

        foreach ($pembelajaransRelatedByPtkTerdaftarIdToDelete as $pembelajaranRelatedByPtkTerdaftarIdRemoved) {
            $pembelajaranRelatedByPtkTerdaftarIdRemoved->setPtkTerdaftarRelatedByPtkTerdaftarId(null);
        }

        $this->collPembelajaransRelatedByPtkTerdaftarId = null;
        foreach ($pembelajaransRelatedByPtkTerdaftarId as $pembelajaranRelatedByPtkTerdaftarId) {
            $this->addPembelajaranRelatedByPtkTerdaftarId($pembelajaranRelatedByPtkTerdaftarId);
        }

        $this->collPembelajaransRelatedByPtkTerdaftarId = $pembelajaransRelatedByPtkTerdaftarId;
        $this->collPembelajaransRelatedByPtkTerdaftarIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Pembelajaran objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Pembelajaran objects.
     * @throws PropelException
     */
    public function countPembelajaransRelatedByPtkTerdaftarId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPembelajaransRelatedByPtkTerdaftarIdPartial && !$this->isNew();
        if (null === $this->collPembelajaransRelatedByPtkTerdaftarId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPembelajaransRelatedByPtkTerdaftarId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPembelajaransRelatedByPtkTerdaftarId());
            }
            $query = PembelajaranQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkTerdaftarRelatedByPtkTerdaftarId($this)
                ->count($con);
        }

        return count($this->collPembelajaransRelatedByPtkTerdaftarId);
    }

    /**
     * Method called to associate a Pembelajaran object to this object
     * through the Pembelajaran foreign key attribute.
     *
     * @param    Pembelajaran $l Pembelajaran
     * @return PtkTerdaftar The current object (for fluent API support)
     */
    public function addPembelajaranRelatedByPtkTerdaftarId(Pembelajaran $l)
    {
        if ($this->collPembelajaransRelatedByPtkTerdaftarId === null) {
            $this->initPembelajaransRelatedByPtkTerdaftarId();
            $this->collPembelajaransRelatedByPtkTerdaftarIdPartial = true;
        }
        if (!in_array($l, $this->collPembelajaransRelatedByPtkTerdaftarId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPembelajaranRelatedByPtkTerdaftarId($l);
        }

        return $this;
    }

    /**
     * @param	PembelajaranRelatedByPtkTerdaftarId $pembelajaranRelatedByPtkTerdaftarId The pembelajaranRelatedByPtkTerdaftarId object to add.
     */
    protected function doAddPembelajaranRelatedByPtkTerdaftarId($pembelajaranRelatedByPtkTerdaftarId)
    {
        $this->collPembelajaransRelatedByPtkTerdaftarId[]= $pembelajaranRelatedByPtkTerdaftarId;
        $pembelajaranRelatedByPtkTerdaftarId->setPtkTerdaftarRelatedByPtkTerdaftarId($this);
    }

    /**
     * @param	PembelajaranRelatedByPtkTerdaftarId $pembelajaranRelatedByPtkTerdaftarId The pembelajaranRelatedByPtkTerdaftarId object to remove.
     * @return PtkTerdaftar The current object (for fluent API support)
     */
    public function removePembelajaranRelatedByPtkTerdaftarId($pembelajaranRelatedByPtkTerdaftarId)
    {
        if ($this->getPembelajaransRelatedByPtkTerdaftarId()->contains($pembelajaranRelatedByPtkTerdaftarId)) {
            $this->collPembelajaransRelatedByPtkTerdaftarId->remove($this->collPembelajaransRelatedByPtkTerdaftarId->search($pembelajaranRelatedByPtkTerdaftarId));
            if (null === $this->pembelajaransRelatedByPtkTerdaftarIdScheduledForDeletion) {
                $this->pembelajaransRelatedByPtkTerdaftarIdScheduledForDeletion = clone $this->collPembelajaransRelatedByPtkTerdaftarId;
                $this->pembelajaransRelatedByPtkTerdaftarIdScheduledForDeletion->clear();
            }
            $this->pembelajaransRelatedByPtkTerdaftarIdScheduledForDeletion[]= clone $pembelajaranRelatedByPtkTerdaftarId;
            $pembelajaranRelatedByPtkTerdaftarId->setPtkTerdaftarRelatedByPtkTerdaftarId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PtkTerdaftar is new, it will return
     * an empty collection; or if this PtkTerdaftar has previously
     * been saved, it will retrieve related PembelajaransRelatedByPtkTerdaftarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PtkTerdaftar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     */
    public function getPembelajaransRelatedByPtkTerdaftarIdJoinRombonganBelajarRelatedByRombonganBelajarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PembelajaranQuery::create(null, $criteria);
        $query->joinWith('RombonganBelajarRelatedByRombonganBelajarId', $join_behavior);

        return $this->getPembelajaransRelatedByPtkTerdaftarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PtkTerdaftar is new, it will return
     * an empty collection; or if this PtkTerdaftar has previously
     * been saved, it will retrieve related PembelajaransRelatedByPtkTerdaftarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PtkTerdaftar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     */
    public function getPembelajaransRelatedByPtkTerdaftarIdJoinRombonganBelajarRelatedByRombonganBelajarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PembelajaranQuery::create(null, $criteria);
        $query->joinWith('RombonganBelajarRelatedByRombonganBelajarId', $join_behavior);

        return $this->getPembelajaransRelatedByPtkTerdaftarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PtkTerdaftar is new, it will return
     * an empty collection; or if this PtkTerdaftar has previously
     * been saved, it will retrieve related PembelajaransRelatedByPtkTerdaftarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PtkTerdaftar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     */
    public function getPembelajaransRelatedByPtkTerdaftarIdJoinMataPelajaranRelatedByMataPelajaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PembelajaranQuery::create(null, $criteria);
        $query->joinWith('MataPelajaranRelatedByMataPelajaranId', $join_behavior);

        return $this->getPembelajaransRelatedByPtkTerdaftarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PtkTerdaftar is new, it will return
     * an empty collection; or if this PtkTerdaftar has previously
     * been saved, it will retrieve related PembelajaransRelatedByPtkTerdaftarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PtkTerdaftar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     */
    public function getPembelajaransRelatedByPtkTerdaftarIdJoinMataPelajaranRelatedByMataPelajaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PembelajaranQuery::create(null, $criteria);
        $query->joinWith('MataPelajaranRelatedByMataPelajaranId', $join_behavior);

        return $this->getPembelajaransRelatedByPtkTerdaftarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PtkTerdaftar is new, it will return
     * an empty collection; or if this PtkTerdaftar has previously
     * been saved, it will retrieve related PembelajaransRelatedByPtkTerdaftarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PtkTerdaftar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     */
    public function getPembelajaransRelatedByPtkTerdaftarIdJoinSemesterRelatedBySemesterId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PembelajaranQuery::create(null, $criteria);
        $query->joinWith('SemesterRelatedBySemesterId', $join_behavior);

        return $this->getPembelajaransRelatedByPtkTerdaftarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PtkTerdaftar is new, it will return
     * an empty collection; or if this PtkTerdaftar has previously
     * been saved, it will retrieve related PembelajaransRelatedByPtkTerdaftarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PtkTerdaftar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     */
    public function getPembelajaransRelatedByPtkTerdaftarIdJoinSemesterRelatedBySemesterId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PembelajaranQuery::create(null, $criteria);
        $query->joinWith('SemesterRelatedBySemesterId', $join_behavior);

        return $this->getPembelajaransRelatedByPtkTerdaftarId($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->ptk_terdaftar_id = null;
        $this->ptk_id = null;
        $this->sekolah_id = null;
        $this->tahun_ajaran_id = null;
        $this->nomor_surat_tugas = null;
        $this->tanggal_surat_tugas = null;
        $this->tmt_tugas = null;
        $this->ptk_induk = null;
        $this->aktif_bulan_01 = null;
        $this->aktif_bulan_02 = null;
        $this->aktif_bulan_03 = null;
        $this->aktif_bulan_04 = null;
        $this->aktif_bulan_05 = null;
        $this->aktif_bulan_06 = null;
        $this->aktif_bulan_07 = null;
        $this->aktif_bulan_08 = null;
        $this->aktif_bulan_09 = null;
        $this->aktif_bulan_10 = null;
        $this->aktif_bulan_11 = null;
        $this->aktif_bulan_12 = null;
        $this->jenis_keluar_id = null;
        $this->tgl_ptk_keluar = null;
        $this->last_update = null;
        $this->soft_delete = null;
        $this->last_sync = null;
        $this->updater_id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collPembelajaransRelatedByPtkTerdaftarId) {
                foreach ($this->collPembelajaransRelatedByPtkTerdaftarId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPembelajaransRelatedByPtkTerdaftarId) {
                foreach ($this->collPembelajaransRelatedByPtkTerdaftarId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aPtkRelatedByPtkId instanceof Persistent) {
              $this->aPtkRelatedByPtkId->clearAllReferences($deep);
            }
            if ($this->aPtkRelatedByPtkId instanceof Persistent) {
              $this->aPtkRelatedByPtkId->clearAllReferences($deep);
            }
            if ($this->aSekolahRelatedBySekolahId instanceof Persistent) {
              $this->aSekolahRelatedBySekolahId->clearAllReferences($deep);
            }
            if ($this->aSekolahRelatedBySekolahId instanceof Persistent) {
              $this->aSekolahRelatedBySekolahId->clearAllReferences($deep);
            }
            if ($this->aJenisKeluarRelatedByJenisKeluarId instanceof Persistent) {
              $this->aJenisKeluarRelatedByJenisKeluarId->clearAllReferences($deep);
            }
            if ($this->aJenisKeluarRelatedByJenisKeluarId instanceof Persistent) {
              $this->aJenisKeluarRelatedByJenisKeluarId->clearAllReferences($deep);
            }
            if ($this->aTahunAjaranRelatedByTahunAjaranId instanceof Persistent) {
              $this->aTahunAjaranRelatedByTahunAjaranId->clearAllReferences($deep);
            }
            if ($this->aTahunAjaranRelatedByTahunAjaranId instanceof Persistent) {
              $this->aTahunAjaranRelatedByTahunAjaranId->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collPembelajaransRelatedByPtkTerdaftarId instanceof PropelCollection) {
            $this->collPembelajaransRelatedByPtkTerdaftarId->clearIterator();
        }
        $this->collPembelajaransRelatedByPtkTerdaftarId = null;
        if ($this->collPembelajaransRelatedByPtkTerdaftarId instanceof PropelCollection) {
            $this->collPembelajaransRelatedByPtkTerdaftarId->clearIterator();
        }
        $this->collPembelajaransRelatedByPtkTerdaftarId = null;
        $this->aPtkRelatedByPtkId = null;
        $this->aPtkRelatedByPtkId = null;
        $this->aSekolahRelatedBySekolahId = null;
        $this->aSekolahRelatedBySekolahId = null;
        $this->aJenisKeluarRelatedByJenisKeluarId = null;
        $this->aJenisKeluarRelatedByJenisKeluarId = null;
        $this->aTahunAjaranRelatedByTahunAjaranId = null;
        $this->aTahunAjaranRelatedByTahunAjaranId = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(PtkTerdaftarPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
