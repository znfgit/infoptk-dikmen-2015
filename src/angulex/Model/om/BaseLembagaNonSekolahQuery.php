<?php

namespace angulex\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\JenisLembaga;
use angulex\Model\LembagaNonSekolah;
use angulex\Model\LembagaNonSekolahPeer;
use angulex\Model\LembagaNonSekolahQuery;
use angulex\Model\MstWilayah;
use angulex\Model\PengawasTerdaftar;
use angulex\Model\Pengguna;
use angulex\Model\VldNonsekolah;

/**
 * Base class that represents a query for the 'lembaga_non_sekolah' table.
 *
 * 
 *
 * @method LembagaNonSekolahQuery orderByLembagaId($order = Criteria::ASC) Order by the lembaga_id column
 * @method LembagaNonSekolahQuery orderByNama($order = Criteria::ASC) Order by the nama column
 * @method LembagaNonSekolahQuery orderBySingkatan($order = Criteria::ASC) Order by the singkatan column
 * @method LembagaNonSekolahQuery orderByJenisLembagaId($order = Criteria::ASC) Order by the jenis_lembaga_id column
 * @method LembagaNonSekolahQuery orderByAlamatJalan($order = Criteria::ASC) Order by the alamat_jalan column
 * @method LembagaNonSekolahQuery orderByRt($order = Criteria::ASC) Order by the rt column
 * @method LembagaNonSekolahQuery orderByRw($order = Criteria::ASC) Order by the rw column
 * @method LembagaNonSekolahQuery orderByNamaDusun($order = Criteria::ASC) Order by the nama_dusun column
 * @method LembagaNonSekolahQuery orderByDesaKelurahan($order = Criteria::ASC) Order by the desa_kelurahan column
 * @method LembagaNonSekolahQuery orderByKodeWilayah($order = Criteria::ASC) Order by the kode_wilayah column
 * @method LembagaNonSekolahQuery orderByKodePos($order = Criteria::ASC) Order by the kode_pos column
 * @method LembagaNonSekolahQuery orderByLintang($order = Criteria::ASC) Order by the lintang column
 * @method LembagaNonSekolahQuery orderByBujur($order = Criteria::ASC) Order by the bujur column
 * @method LembagaNonSekolahQuery orderByNomorTelepon($order = Criteria::ASC) Order by the nomor_telepon column
 * @method LembagaNonSekolahQuery orderByNomorFax($order = Criteria::ASC) Order by the nomor_fax column
 * @method LembagaNonSekolahQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method LembagaNonSekolahQuery orderByWebsite($order = Criteria::ASC) Order by the website column
 * @method LembagaNonSekolahQuery orderByPenggunaId($order = Criteria::ASC) Order by the pengguna_id column
 * @method LembagaNonSekolahQuery orderByLastUpdate($order = Criteria::ASC) Order by the Last_update column
 * @method LembagaNonSekolahQuery orderBySoftDelete($order = Criteria::ASC) Order by the Soft_delete column
 * @method LembagaNonSekolahQuery orderByLastSync($order = Criteria::ASC) Order by the last_sync column
 * @method LembagaNonSekolahQuery orderByUpdaterId($order = Criteria::ASC) Order by the Updater_ID column
 *
 * @method LembagaNonSekolahQuery groupByLembagaId() Group by the lembaga_id column
 * @method LembagaNonSekolahQuery groupByNama() Group by the nama column
 * @method LembagaNonSekolahQuery groupBySingkatan() Group by the singkatan column
 * @method LembagaNonSekolahQuery groupByJenisLembagaId() Group by the jenis_lembaga_id column
 * @method LembagaNonSekolahQuery groupByAlamatJalan() Group by the alamat_jalan column
 * @method LembagaNonSekolahQuery groupByRt() Group by the rt column
 * @method LembagaNonSekolahQuery groupByRw() Group by the rw column
 * @method LembagaNonSekolahQuery groupByNamaDusun() Group by the nama_dusun column
 * @method LembagaNonSekolahQuery groupByDesaKelurahan() Group by the desa_kelurahan column
 * @method LembagaNonSekolahQuery groupByKodeWilayah() Group by the kode_wilayah column
 * @method LembagaNonSekolahQuery groupByKodePos() Group by the kode_pos column
 * @method LembagaNonSekolahQuery groupByLintang() Group by the lintang column
 * @method LembagaNonSekolahQuery groupByBujur() Group by the bujur column
 * @method LembagaNonSekolahQuery groupByNomorTelepon() Group by the nomor_telepon column
 * @method LembagaNonSekolahQuery groupByNomorFax() Group by the nomor_fax column
 * @method LembagaNonSekolahQuery groupByEmail() Group by the email column
 * @method LembagaNonSekolahQuery groupByWebsite() Group by the website column
 * @method LembagaNonSekolahQuery groupByPenggunaId() Group by the pengguna_id column
 * @method LembagaNonSekolahQuery groupByLastUpdate() Group by the Last_update column
 * @method LembagaNonSekolahQuery groupBySoftDelete() Group by the Soft_delete column
 * @method LembagaNonSekolahQuery groupByLastSync() Group by the last_sync column
 * @method LembagaNonSekolahQuery groupByUpdaterId() Group by the Updater_ID column
 *
 * @method LembagaNonSekolahQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method LembagaNonSekolahQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method LembagaNonSekolahQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method LembagaNonSekolahQuery leftJoinPenggunaRelatedByPenggunaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PenggunaRelatedByPenggunaId relation
 * @method LembagaNonSekolahQuery rightJoinPenggunaRelatedByPenggunaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PenggunaRelatedByPenggunaId relation
 * @method LembagaNonSekolahQuery innerJoinPenggunaRelatedByPenggunaId($relationAlias = null) Adds a INNER JOIN clause to the query using the PenggunaRelatedByPenggunaId relation
 *
 * @method LembagaNonSekolahQuery leftJoinPenggunaRelatedByPenggunaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PenggunaRelatedByPenggunaId relation
 * @method LembagaNonSekolahQuery rightJoinPenggunaRelatedByPenggunaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PenggunaRelatedByPenggunaId relation
 * @method LembagaNonSekolahQuery innerJoinPenggunaRelatedByPenggunaId($relationAlias = null) Adds a INNER JOIN clause to the query using the PenggunaRelatedByPenggunaId relation
 *
 * @method LembagaNonSekolahQuery leftJoinJenisLembagaRelatedByJenisLembagaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the JenisLembagaRelatedByJenisLembagaId relation
 * @method LembagaNonSekolahQuery rightJoinJenisLembagaRelatedByJenisLembagaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JenisLembagaRelatedByJenisLembagaId relation
 * @method LembagaNonSekolahQuery innerJoinJenisLembagaRelatedByJenisLembagaId($relationAlias = null) Adds a INNER JOIN clause to the query using the JenisLembagaRelatedByJenisLembagaId relation
 *
 * @method LembagaNonSekolahQuery leftJoinJenisLembagaRelatedByJenisLembagaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the JenisLembagaRelatedByJenisLembagaId relation
 * @method LembagaNonSekolahQuery rightJoinJenisLembagaRelatedByJenisLembagaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JenisLembagaRelatedByJenisLembagaId relation
 * @method LembagaNonSekolahQuery innerJoinJenisLembagaRelatedByJenisLembagaId($relationAlias = null) Adds a INNER JOIN clause to the query using the JenisLembagaRelatedByJenisLembagaId relation
 *
 * @method LembagaNonSekolahQuery leftJoinMstWilayahRelatedByKodeWilayah($relationAlias = null) Adds a LEFT JOIN clause to the query using the MstWilayahRelatedByKodeWilayah relation
 * @method LembagaNonSekolahQuery rightJoinMstWilayahRelatedByKodeWilayah($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MstWilayahRelatedByKodeWilayah relation
 * @method LembagaNonSekolahQuery innerJoinMstWilayahRelatedByKodeWilayah($relationAlias = null) Adds a INNER JOIN clause to the query using the MstWilayahRelatedByKodeWilayah relation
 *
 * @method LembagaNonSekolahQuery leftJoinMstWilayahRelatedByKodeWilayah($relationAlias = null) Adds a LEFT JOIN clause to the query using the MstWilayahRelatedByKodeWilayah relation
 * @method LembagaNonSekolahQuery rightJoinMstWilayahRelatedByKodeWilayah($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MstWilayahRelatedByKodeWilayah relation
 * @method LembagaNonSekolahQuery innerJoinMstWilayahRelatedByKodeWilayah($relationAlias = null) Adds a INNER JOIN clause to the query using the MstWilayahRelatedByKodeWilayah relation
 *
 * @method LembagaNonSekolahQuery leftJoinPenggunaRelatedByLembagaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PenggunaRelatedByLembagaId relation
 * @method LembagaNonSekolahQuery rightJoinPenggunaRelatedByLembagaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PenggunaRelatedByLembagaId relation
 * @method LembagaNonSekolahQuery innerJoinPenggunaRelatedByLembagaId($relationAlias = null) Adds a INNER JOIN clause to the query using the PenggunaRelatedByLembagaId relation
 *
 * @method LembagaNonSekolahQuery leftJoinPenggunaRelatedByLembagaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PenggunaRelatedByLembagaId relation
 * @method LembagaNonSekolahQuery rightJoinPenggunaRelatedByLembagaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PenggunaRelatedByLembagaId relation
 * @method LembagaNonSekolahQuery innerJoinPenggunaRelatedByLembagaId($relationAlias = null) Adds a INNER JOIN clause to the query using the PenggunaRelatedByLembagaId relation
 *
 * @method LembagaNonSekolahQuery leftJoinVldNonsekolahRelatedByLembagaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldNonsekolahRelatedByLembagaId relation
 * @method LembagaNonSekolahQuery rightJoinVldNonsekolahRelatedByLembagaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldNonsekolahRelatedByLembagaId relation
 * @method LembagaNonSekolahQuery innerJoinVldNonsekolahRelatedByLembagaId($relationAlias = null) Adds a INNER JOIN clause to the query using the VldNonsekolahRelatedByLembagaId relation
 *
 * @method LembagaNonSekolahQuery leftJoinVldNonsekolahRelatedByLembagaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldNonsekolahRelatedByLembagaId relation
 * @method LembagaNonSekolahQuery rightJoinVldNonsekolahRelatedByLembagaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldNonsekolahRelatedByLembagaId relation
 * @method LembagaNonSekolahQuery innerJoinVldNonsekolahRelatedByLembagaId($relationAlias = null) Adds a INNER JOIN clause to the query using the VldNonsekolahRelatedByLembagaId relation
 *
 * @method LembagaNonSekolahQuery leftJoinPengawasTerdaftarRelatedByLembagaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PengawasTerdaftarRelatedByLembagaId relation
 * @method LembagaNonSekolahQuery rightJoinPengawasTerdaftarRelatedByLembagaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PengawasTerdaftarRelatedByLembagaId relation
 * @method LembagaNonSekolahQuery innerJoinPengawasTerdaftarRelatedByLembagaId($relationAlias = null) Adds a INNER JOIN clause to the query using the PengawasTerdaftarRelatedByLembagaId relation
 *
 * @method LembagaNonSekolahQuery leftJoinPengawasTerdaftarRelatedByLembagaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PengawasTerdaftarRelatedByLembagaId relation
 * @method LembagaNonSekolahQuery rightJoinPengawasTerdaftarRelatedByLembagaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PengawasTerdaftarRelatedByLembagaId relation
 * @method LembagaNonSekolahQuery innerJoinPengawasTerdaftarRelatedByLembagaId($relationAlias = null) Adds a INNER JOIN clause to the query using the PengawasTerdaftarRelatedByLembagaId relation
 *
 * @method LembagaNonSekolah findOne(PropelPDO $con = null) Return the first LembagaNonSekolah matching the query
 * @method LembagaNonSekolah findOneOrCreate(PropelPDO $con = null) Return the first LembagaNonSekolah matching the query, or a new LembagaNonSekolah object populated from the query conditions when no match is found
 *
 * @method LembagaNonSekolah findOneByNama(string $nama) Return the first LembagaNonSekolah filtered by the nama column
 * @method LembagaNonSekolah findOneBySingkatan(string $singkatan) Return the first LembagaNonSekolah filtered by the singkatan column
 * @method LembagaNonSekolah findOneByJenisLembagaId(string $jenis_lembaga_id) Return the first LembagaNonSekolah filtered by the jenis_lembaga_id column
 * @method LembagaNonSekolah findOneByAlamatJalan(string $alamat_jalan) Return the first LembagaNonSekolah filtered by the alamat_jalan column
 * @method LembagaNonSekolah findOneByRt(string $rt) Return the first LembagaNonSekolah filtered by the rt column
 * @method LembagaNonSekolah findOneByRw(string $rw) Return the first LembagaNonSekolah filtered by the rw column
 * @method LembagaNonSekolah findOneByNamaDusun(string $nama_dusun) Return the first LembagaNonSekolah filtered by the nama_dusun column
 * @method LembagaNonSekolah findOneByDesaKelurahan(string $desa_kelurahan) Return the first LembagaNonSekolah filtered by the desa_kelurahan column
 * @method LembagaNonSekolah findOneByKodeWilayah(string $kode_wilayah) Return the first LembagaNonSekolah filtered by the kode_wilayah column
 * @method LembagaNonSekolah findOneByKodePos(string $kode_pos) Return the first LembagaNonSekolah filtered by the kode_pos column
 * @method LembagaNonSekolah findOneByLintang(string $lintang) Return the first LembagaNonSekolah filtered by the lintang column
 * @method LembagaNonSekolah findOneByBujur(string $bujur) Return the first LembagaNonSekolah filtered by the bujur column
 * @method LembagaNonSekolah findOneByNomorTelepon(string $nomor_telepon) Return the first LembagaNonSekolah filtered by the nomor_telepon column
 * @method LembagaNonSekolah findOneByNomorFax(string $nomor_fax) Return the first LembagaNonSekolah filtered by the nomor_fax column
 * @method LembagaNonSekolah findOneByEmail(string $email) Return the first LembagaNonSekolah filtered by the email column
 * @method LembagaNonSekolah findOneByWebsite(string $website) Return the first LembagaNonSekolah filtered by the website column
 * @method LembagaNonSekolah findOneByPenggunaId(string $pengguna_id) Return the first LembagaNonSekolah filtered by the pengguna_id column
 * @method LembagaNonSekolah findOneByLastUpdate(string $Last_update) Return the first LembagaNonSekolah filtered by the Last_update column
 * @method LembagaNonSekolah findOneBySoftDelete(string $Soft_delete) Return the first LembagaNonSekolah filtered by the Soft_delete column
 * @method LembagaNonSekolah findOneByLastSync(string $last_sync) Return the first LembagaNonSekolah filtered by the last_sync column
 * @method LembagaNonSekolah findOneByUpdaterId(string $Updater_ID) Return the first LembagaNonSekolah filtered by the Updater_ID column
 *
 * @method array findByLembagaId(string $lembaga_id) Return LembagaNonSekolah objects filtered by the lembaga_id column
 * @method array findByNama(string $nama) Return LembagaNonSekolah objects filtered by the nama column
 * @method array findBySingkatan(string $singkatan) Return LembagaNonSekolah objects filtered by the singkatan column
 * @method array findByJenisLembagaId(string $jenis_lembaga_id) Return LembagaNonSekolah objects filtered by the jenis_lembaga_id column
 * @method array findByAlamatJalan(string $alamat_jalan) Return LembagaNonSekolah objects filtered by the alamat_jalan column
 * @method array findByRt(string $rt) Return LembagaNonSekolah objects filtered by the rt column
 * @method array findByRw(string $rw) Return LembagaNonSekolah objects filtered by the rw column
 * @method array findByNamaDusun(string $nama_dusun) Return LembagaNonSekolah objects filtered by the nama_dusun column
 * @method array findByDesaKelurahan(string $desa_kelurahan) Return LembagaNonSekolah objects filtered by the desa_kelurahan column
 * @method array findByKodeWilayah(string $kode_wilayah) Return LembagaNonSekolah objects filtered by the kode_wilayah column
 * @method array findByKodePos(string $kode_pos) Return LembagaNonSekolah objects filtered by the kode_pos column
 * @method array findByLintang(string $lintang) Return LembagaNonSekolah objects filtered by the lintang column
 * @method array findByBujur(string $bujur) Return LembagaNonSekolah objects filtered by the bujur column
 * @method array findByNomorTelepon(string $nomor_telepon) Return LembagaNonSekolah objects filtered by the nomor_telepon column
 * @method array findByNomorFax(string $nomor_fax) Return LembagaNonSekolah objects filtered by the nomor_fax column
 * @method array findByEmail(string $email) Return LembagaNonSekolah objects filtered by the email column
 * @method array findByWebsite(string $website) Return LembagaNonSekolah objects filtered by the website column
 * @method array findByPenggunaId(string $pengguna_id) Return LembagaNonSekolah objects filtered by the pengguna_id column
 * @method array findByLastUpdate(string $Last_update) Return LembagaNonSekolah objects filtered by the Last_update column
 * @method array findBySoftDelete(string $Soft_delete) Return LembagaNonSekolah objects filtered by the Soft_delete column
 * @method array findByLastSync(string $last_sync) Return LembagaNonSekolah objects filtered by the last_sync column
 * @method array findByUpdaterId(string $Updater_ID) Return LembagaNonSekolah objects filtered by the Updater_ID column
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseLembagaNonSekolahQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseLembagaNonSekolahQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Dapodikmen', $modelName = 'angulex\\Model\\LembagaNonSekolah', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new LembagaNonSekolahQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   LembagaNonSekolahQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return LembagaNonSekolahQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof LembagaNonSekolahQuery) {
            return $criteria;
        }
        $query = new LembagaNonSekolahQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   LembagaNonSekolah|LembagaNonSekolah[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = LembagaNonSekolahPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(LembagaNonSekolahPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 LembagaNonSekolah A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByLembagaId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 LembagaNonSekolah A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [lembaga_id], [nama], [singkatan], [jenis_lembaga_id], [alamat_jalan], [rt], [rw], [nama_dusun], [desa_kelurahan], [kode_wilayah], [kode_pos], [lintang], [bujur], [nomor_telepon], [nomor_fax], [email], [website], [pengguna_id], [Last_update], [Soft_delete], [last_sync], [Updater_ID] FROM [lembaga_non_sekolah] WHERE [lembaga_id] = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new LembagaNonSekolah();
            $obj->hydrate($row);
            LembagaNonSekolahPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return LembagaNonSekolah|LembagaNonSekolah[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|LembagaNonSekolah[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return LembagaNonSekolahQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(LembagaNonSekolahPeer::LEMBAGA_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return LembagaNonSekolahQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(LembagaNonSekolahPeer::LEMBAGA_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the lembaga_id column
     *
     * Example usage:
     * <code>
     * $query->filterByLembagaId('fooValue');   // WHERE lembaga_id = 'fooValue'
     * $query->filterByLembagaId('%fooValue%'); // WHERE lembaga_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $lembagaId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LembagaNonSekolahQuery The current query, for fluid interface
     */
    public function filterByLembagaId($lembagaId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($lembagaId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $lembagaId)) {
                $lembagaId = str_replace('*', '%', $lembagaId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LembagaNonSekolahPeer::LEMBAGA_ID, $lembagaId, $comparison);
    }

    /**
     * Filter the query on the nama column
     *
     * Example usage:
     * <code>
     * $query->filterByNama('fooValue');   // WHERE nama = 'fooValue'
     * $query->filterByNama('%fooValue%'); // WHERE nama LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nama The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LembagaNonSekolahQuery The current query, for fluid interface
     */
    public function filterByNama($nama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nama)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nama)) {
                $nama = str_replace('*', '%', $nama);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LembagaNonSekolahPeer::NAMA, $nama, $comparison);
    }

    /**
     * Filter the query on the singkatan column
     *
     * Example usage:
     * <code>
     * $query->filterBySingkatan('fooValue');   // WHERE singkatan = 'fooValue'
     * $query->filterBySingkatan('%fooValue%'); // WHERE singkatan LIKE '%fooValue%'
     * </code>
     *
     * @param     string $singkatan The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LembagaNonSekolahQuery The current query, for fluid interface
     */
    public function filterBySingkatan($singkatan = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($singkatan)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $singkatan)) {
                $singkatan = str_replace('*', '%', $singkatan);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LembagaNonSekolahPeer::SINGKATAN, $singkatan, $comparison);
    }

    /**
     * Filter the query on the jenis_lembaga_id column
     *
     * Example usage:
     * <code>
     * $query->filterByJenisLembagaId(1234); // WHERE jenis_lembaga_id = 1234
     * $query->filterByJenisLembagaId(array(12, 34)); // WHERE jenis_lembaga_id IN (12, 34)
     * $query->filterByJenisLembagaId(array('min' => 12)); // WHERE jenis_lembaga_id >= 12
     * $query->filterByJenisLembagaId(array('max' => 12)); // WHERE jenis_lembaga_id <= 12
     * </code>
     *
     * @see       filterByJenisLembagaRelatedByJenisLembagaId()
     *
     * @see       filterByJenisLembagaRelatedByJenisLembagaId()
     *
     * @param     mixed $jenisLembagaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LembagaNonSekolahQuery The current query, for fluid interface
     */
    public function filterByJenisLembagaId($jenisLembagaId = null, $comparison = null)
    {
        if (is_array($jenisLembagaId)) {
            $useMinMax = false;
            if (isset($jenisLembagaId['min'])) {
                $this->addUsingAlias(LembagaNonSekolahPeer::JENIS_LEMBAGA_ID, $jenisLembagaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jenisLembagaId['max'])) {
                $this->addUsingAlias(LembagaNonSekolahPeer::JENIS_LEMBAGA_ID, $jenisLembagaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LembagaNonSekolahPeer::JENIS_LEMBAGA_ID, $jenisLembagaId, $comparison);
    }

    /**
     * Filter the query on the alamat_jalan column
     *
     * Example usage:
     * <code>
     * $query->filterByAlamatJalan('fooValue');   // WHERE alamat_jalan = 'fooValue'
     * $query->filterByAlamatJalan('%fooValue%'); // WHERE alamat_jalan LIKE '%fooValue%'
     * </code>
     *
     * @param     string $alamatJalan The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LembagaNonSekolahQuery The current query, for fluid interface
     */
    public function filterByAlamatJalan($alamatJalan = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($alamatJalan)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $alamatJalan)) {
                $alamatJalan = str_replace('*', '%', $alamatJalan);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LembagaNonSekolahPeer::ALAMAT_JALAN, $alamatJalan, $comparison);
    }

    /**
     * Filter the query on the rt column
     *
     * Example usage:
     * <code>
     * $query->filterByRt(1234); // WHERE rt = 1234
     * $query->filterByRt(array(12, 34)); // WHERE rt IN (12, 34)
     * $query->filterByRt(array('min' => 12)); // WHERE rt >= 12
     * $query->filterByRt(array('max' => 12)); // WHERE rt <= 12
     * </code>
     *
     * @param     mixed $rt The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LembagaNonSekolahQuery The current query, for fluid interface
     */
    public function filterByRt($rt = null, $comparison = null)
    {
        if (is_array($rt)) {
            $useMinMax = false;
            if (isset($rt['min'])) {
                $this->addUsingAlias(LembagaNonSekolahPeer::RT, $rt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rt['max'])) {
                $this->addUsingAlias(LembagaNonSekolahPeer::RT, $rt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LembagaNonSekolahPeer::RT, $rt, $comparison);
    }

    /**
     * Filter the query on the rw column
     *
     * Example usage:
     * <code>
     * $query->filterByRw(1234); // WHERE rw = 1234
     * $query->filterByRw(array(12, 34)); // WHERE rw IN (12, 34)
     * $query->filterByRw(array('min' => 12)); // WHERE rw >= 12
     * $query->filterByRw(array('max' => 12)); // WHERE rw <= 12
     * </code>
     *
     * @param     mixed $rw The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LembagaNonSekolahQuery The current query, for fluid interface
     */
    public function filterByRw($rw = null, $comparison = null)
    {
        if (is_array($rw)) {
            $useMinMax = false;
            if (isset($rw['min'])) {
                $this->addUsingAlias(LembagaNonSekolahPeer::RW, $rw['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rw['max'])) {
                $this->addUsingAlias(LembagaNonSekolahPeer::RW, $rw['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LembagaNonSekolahPeer::RW, $rw, $comparison);
    }

    /**
     * Filter the query on the nama_dusun column
     *
     * Example usage:
     * <code>
     * $query->filterByNamaDusun('fooValue');   // WHERE nama_dusun = 'fooValue'
     * $query->filterByNamaDusun('%fooValue%'); // WHERE nama_dusun LIKE '%fooValue%'
     * </code>
     *
     * @param     string $namaDusun The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LembagaNonSekolahQuery The current query, for fluid interface
     */
    public function filterByNamaDusun($namaDusun = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($namaDusun)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $namaDusun)) {
                $namaDusun = str_replace('*', '%', $namaDusun);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LembagaNonSekolahPeer::NAMA_DUSUN, $namaDusun, $comparison);
    }

    /**
     * Filter the query on the desa_kelurahan column
     *
     * Example usage:
     * <code>
     * $query->filterByDesaKelurahan('fooValue');   // WHERE desa_kelurahan = 'fooValue'
     * $query->filterByDesaKelurahan('%fooValue%'); // WHERE desa_kelurahan LIKE '%fooValue%'
     * </code>
     *
     * @param     string $desaKelurahan The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LembagaNonSekolahQuery The current query, for fluid interface
     */
    public function filterByDesaKelurahan($desaKelurahan = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($desaKelurahan)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $desaKelurahan)) {
                $desaKelurahan = str_replace('*', '%', $desaKelurahan);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LembagaNonSekolahPeer::DESA_KELURAHAN, $desaKelurahan, $comparison);
    }

    /**
     * Filter the query on the kode_wilayah column
     *
     * Example usage:
     * <code>
     * $query->filterByKodeWilayah('fooValue');   // WHERE kode_wilayah = 'fooValue'
     * $query->filterByKodeWilayah('%fooValue%'); // WHERE kode_wilayah LIKE '%fooValue%'
     * </code>
     *
     * @param     string $kodeWilayah The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LembagaNonSekolahQuery The current query, for fluid interface
     */
    public function filterByKodeWilayah($kodeWilayah = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($kodeWilayah)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $kodeWilayah)) {
                $kodeWilayah = str_replace('*', '%', $kodeWilayah);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LembagaNonSekolahPeer::KODE_WILAYAH, $kodeWilayah, $comparison);
    }

    /**
     * Filter the query on the kode_pos column
     *
     * Example usage:
     * <code>
     * $query->filterByKodePos('fooValue');   // WHERE kode_pos = 'fooValue'
     * $query->filterByKodePos('%fooValue%'); // WHERE kode_pos LIKE '%fooValue%'
     * </code>
     *
     * @param     string $kodePos The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LembagaNonSekolahQuery The current query, for fluid interface
     */
    public function filterByKodePos($kodePos = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($kodePos)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $kodePos)) {
                $kodePos = str_replace('*', '%', $kodePos);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LembagaNonSekolahPeer::KODE_POS, $kodePos, $comparison);
    }

    /**
     * Filter the query on the lintang column
     *
     * Example usage:
     * <code>
     * $query->filterByLintang(1234); // WHERE lintang = 1234
     * $query->filterByLintang(array(12, 34)); // WHERE lintang IN (12, 34)
     * $query->filterByLintang(array('min' => 12)); // WHERE lintang >= 12
     * $query->filterByLintang(array('max' => 12)); // WHERE lintang <= 12
     * </code>
     *
     * @param     mixed $lintang The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LembagaNonSekolahQuery The current query, for fluid interface
     */
    public function filterByLintang($lintang = null, $comparison = null)
    {
        if (is_array($lintang)) {
            $useMinMax = false;
            if (isset($lintang['min'])) {
                $this->addUsingAlias(LembagaNonSekolahPeer::LINTANG, $lintang['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lintang['max'])) {
                $this->addUsingAlias(LembagaNonSekolahPeer::LINTANG, $lintang['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LembagaNonSekolahPeer::LINTANG, $lintang, $comparison);
    }

    /**
     * Filter the query on the bujur column
     *
     * Example usage:
     * <code>
     * $query->filterByBujur(1234); // WHERE bujur = 1234
     * $query->filterByBujur(array(12, 34)); // WHERE bujur IN (12, 34)
     * $query->filterByBujur(array('min' => 12)); // WHERE bujur >= 12
     * $query->filterByBujur(array('max' => 12)); // WHERE bujur <= 12
     * </code>
     *
     * @param     mixed $bujur The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LembagaNonSekolahQuery The current query, for fluid interface
     */
    public function filterByBujur($bujur = null, $comparison = null)
    {
        if (is_array($bujur)) {
            $useMinMax = false;
            if (isset($bujur['min'])) {
                $this->addUsingAlias(LembagaNonSekolahPeer::BUJUR, $bujur['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($bujur['max'])) {
                $this->addUsingAlias(LembagaNonSekolahPeer::BUJUR, $bujur['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LembagaNonSekolahPeer::BUJUR, $bujur, $comparison);
    }

    /**
     * Filter the query on the nomor_telepon column
     *
     * Example usage:
     * <code>
     * $query->filterByNomorTelepon('fooValue');   // WHERE nomor_telepon = 'fooValue'
     * $query->filterByNomorTelepon('%fooValue%'); // WHERE nomor_telepon LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomorTelepon The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LembagaNonSekolahQuery The current query, for fluid interface
     */
    public function filterByNomorTelepon($nomorTelepon = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomorTelepon)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nomorTelepon)) {
                $nomorTelepon = str_replace('*', '%', $nomorTelepon);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LembagaNonSekolahPeer::NOMOR_TELEPON, $nomorTelepon, $comparison);
    }

    /**
     * Filter the query on the nomor_fax column
     *
     * Example usage:
     * <code>
     * $query->filterByNomorFax('fooValue');   // WHERE nomor_fax = 'fooValue'
     * $query->filterByNomorFax('%fooValue%'); // WHERE nomor_fax LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomorFax The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LembagaNonSekolahQuery The current query, for fluid interface
     */
    public function filterByNomorFax($nomorFax = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomorFax)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nomorFax)) {
                $nomorFax = str_replace('*', '%', $nomorFax);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LembagaNonSekolahPeer::NOMOR_FAX, $nomorFax, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%'); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LembagaNonSekolahQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $email)) {
                $email = str_replace('*', '%', $email);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LembagaNonSekolahPeer::EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the website column
     *
     * Example usage:
     * <code>
     * $query->filterByWebsite('fooValue');   // WHERE website = 'fooValue'
     * $query->filterByWebsite('%fooValue%'); // WHERE website LIKE '%fooValue%'
     * </code>
     *
     * @param     string $website The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LembagaNonSekolahQuery The current query, for fluid interface
     */
    public function filterByWebsite($website = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($website)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $website)) {
                $website = str_replace('*', '%', $website);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LembagaNonSekolahPeer::WEBSITE, $website, $comparison);
    }

    /**
     * Filter the query on the pengguna_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPenggunaId('fooValue');   // WHERE pengguna_id = 'fooValue'
     * $query->filterByPenggunaId('%fooValue%'); // WHERE pengguna_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $penggunaId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LembagaNonSekolahQuery The current query, for fluid interface
     */
    public function filterByPenggunaId($penggunaId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($penggunaId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $penggunaId)) {
                $penggunaId = str_replace('*', '%', $penggunaId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LembagaNonSekolahPeer::PENGGUNA_ID, $penggunaId, $comparison);
    }

    /**
     * Filter the query on the Last_update column
     *
     * Example usage:
     * <code>
     * $query->filterByLastUpdate('2011-03-14'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate('now'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate(array('max' => 'yesterday')); // WHERE Last_update > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastUpdate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LembagaNonSekolahQuery The current query, for fluid interface
     */
    public function filterByLastUpdate($lastUpdate = null, $comparison = null)
    {
        if (is_array($lastUpdate)) {
            $useMinMax = false;
            if (isset($lastUpdate['min'])) {
                $this->addUsingAlias(LembagaNonSekolahPeer::LAST_UPDATE, $lastUpdate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastUpdate['max'])) {
                $this->addUsingAlias(LembagaNonSekolahPeer::LAST_UPDATE, $lastUpdate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LembagaNonSekolahPeer::LAST_UPDATE, $lastUpdate, $comparison);
    }

    /**
     * Filter the query on the Soft_delete column
     *
     * Example usage:
     * <code>
     * $query->filterBySoftDelete(1234); // WHERE Soft_delete = 1234
     * $query->filterBySoftDelete(array(12, 34)); // WHERE Soft_delete IN (12, 34)
     * $query->filterBySoftDelete(array('min' => 12)); // WHERE Soft_delete >= 12
     * $query->filterBySoftDelete(array('max' => 12)); // WHERE Soft_delete <= 12
     * </code>
     *
     * @param     mixed $softDelete The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LembagaNonSekolahQuery The current query, for fluid interface
     */
    public function filterBySoftDelete($softDelete = null, $comparison = null)
    {
        if (is_array($softDelete)) {
            $useMinMax = false;
            if (isset($softDelete['min'])) {
                $this->addUsingAlias(LembagaNonSekolahPeer::SOFT_DELETE, $softDelete['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($softDelete['max'])) {
                $this->addUsingAlias(LembagaNonSekolahPeer::SOFT_DELETE, $softDelete['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LembagaNonSekolahPeer::SOFT_DELETE, $softDelete, $comparison);
    }

    /**
     * Filter the query on the last_sync column
     *
     * Example usage:
     * <code>
     * $query->filterByLastSync('2011-03-14'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync('now'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync(array('max' => 'yesterday')); // WHERE last_sync > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastSync The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LembagaNonSekolahQuery The current query, for fluid interface
     */
    public function filterByLastSync($lastSync = null, $comparison = null)
    {
        if (is_array($lastSync)) {
            $useMinMax = false;
            if (isset($lastSync['min'])) {
                $this->addUsingAlias(LembagaNonSekolahPeer::LAST_SYNC, $lastSync['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastSync['max'])) {
                $this->addUsingAlias(LembagaNonSekolahPeer::LAST_SYNC, $lastSync['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LembagaNonSekolahPeer::LAST_SYNC, $lastSync, $comparison);
    }

    /**
     * Filter the query on the Updater_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdaterId('fooValue');   // WHERE Updater_ID = 'fooValue'
     * $query->filterByUpdaterId('%fooValue%'); // WHERE Updater_ID LIKE '%fooValue%'
     * </code>
     *
     * @param     string $updaterId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LembagaNonSekolahQuery The current query, for fluid interface
     */
    public function filterByUpdaterId($updaterId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($updaterId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $updaterId)) {
                $updaterId = str_replace('*', '%', $updaterId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LembagaNonSekolahPeer::UPDATER_ID, $updaterId, $comparison);
    }

    /**
     * Filter the query by a related Pengguna object
     *
     * @param   Pengguna|PropelObjectCollection $pengguna The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 LembagaNonSekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPenggunaRelatedByPenggunaId($pengguna, $comparison = null)
    {
        if ($pengguna instanceof Pengguna) {
            return $this
                ->addUsingAlias(LembagaNonSekolahPeer::PENGGUNA_ID, $pengguna->getPenggunaId(), $comparison);
        } elseif ($pengguna instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(LembagaNonSekolahPeer::PENGGUNA_ID, $pengguna->toKeyValue('PrimaryKey', 'PenggunaId'), $comparison);
        } else {
            throw new PropelException('filterByPenggunaRelatedByPenggunaId() only accepts arguments of type Pengguna or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PenggunaRelatedByPenggunaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return LembagaNonSekolahQuery The current query, for fluid interface
     */
    public function joinPenggunaRelatedByPenggunaId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PenggunaRelatedByPenggunaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PenggunaRelatedByPenggunaId');
        }

        return $this;
    }

    /**
     * Use the PenggunaRelatedByPenggunaId relation Pengguna object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PenggunaQuery A secondary query class using the current class as primary query
     */
    public function usePenggunaRelatedByPenggunaIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPenggunaRelatedByPenggunaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PenggunaRelatedByPenggunaId', '\angulex\Model\PenggunaQuery');
    }

    /**
     * Filter the query by a related Pengguna object
     *
     * @param   Pengguna|PropelObjectCollection $pengguna The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 LembagaNonSekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPenggunaRelatedByPenggunaId($pengguna, $comparison = null)
    {
        if ($pengguna instanceof Pengguna) {
            return $this
                ->addUsingAlias(LembagaNonSekolahPeer::PENGGUNA_ID, $pengguna->getPenggunaId(), $comparison);
        } elseif ($pengguna instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(LembagaNonSekolahPeer::PENGGUNA_ID, $pengguna->toKeyValue('PrimaryKey', 'PenggunaId'), $comparison);
        } else {
            throw new PropelException('filterByPenggunaRelatedByPenggunaId() only accepts arguments of type Pengguna or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PenggunaRelatedByPenggunaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return LembagaNonSekolahQuery The current query, for fluid interface
     */
    public function joinPenggunaRelatedByPenggunaId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PenggunaRelatedByPenggunaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PenggunaRelatedByPenggunaId');
        }

        return $this;
    }

    /**
     * Use the PenggunaRelatedByPenggunaId relation Pengguna object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PenggunaQuery A secondary query class using the current class as primary query
     */
    public function usePenggunaRelatedByPenggunaIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPenggunaRelatedByPenggunaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PenggunaRelatedByPenggunaId', '\angulex\Model\PenggunaQuery');
    }

    /**
     * Filter the query by a related JenisLembaga object
     *
     * @param   JenisLembaga|PropelObjectCollection $jenisLembaga The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 LembagaNonSekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJenisLembagaRelatedByJenisLembagaId($jenisLembaga, $comparison = null)
    {
        if ($jenisLembaga instanceof JenisLembaga) {
            return $this
                ->addUsingAlias(LembagaNonSekolahPeer::JENIS_LEMBAGA_ID, $jenisLembaga->getJenisLembagaId(), $comparison);
        } elseif ($jenisLembaga instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(LembagaNonSekolahPeer::JENIS_LEMBAGA_ID, $jenisLembaga->toKeyValue('PrimaryKey', 'JenisLembagaId'), $comparison);
        } else {
            throw new PropelException('filterByJenisLembagaRelatedByJenisLembagaId() only accepts arguments of type JenisLembaga or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the JenisLembagaRelatedByJenisLembagaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return LembagaNonSekolahQuery The current query, for fluid interface
     */
    public function joinJenisLembagaRelatedByJenisLembagaId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('JenisLembagaRelatedByJenisLembagaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'JenisLembagaRelatedByJenisLembagaId');
        }

        return $this;
    }

    /**
     * Use the JenisLembagaRelatedByJenisLembagaId relation JenisLembaga object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\JenisLembagaQuery A secondary query class using the current class as primary query
     */
    public function useJenisLembagaRelatedByJenisLembagaIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinJenisLembagaRelatedByJenisLembagaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'JenisLembagaRelatedByJenisLembagaId', '\angulex\Model\JenisLembagaQuery');
    }

    /**
     * Filter the query by a related JenisLembaga object
     *
     * @param   JenisLembaga|PropelObjectCollection $jenisLembaga The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 LembagaNonSekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJenisLembagaRelatedByJenisLembagaId($jenisLembaga, $comparison = null)
    {
        if ($jenisLembaga instanceof JenisLembaga) {
            return $this
                ->addUsingAlias(LembagaNonSekolahPeer::JENIS_LEMBAGA_ID, $jenisLembaga->getJenisLembagaId(), $comparison);
        } elseif ($jenisLembaga instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(LembagaNonSekolahPeer::JENIS_LEMBAGA_ID, $jenisLembaga->toKeyValue('PrimaryKey', 'JenisLembagaId'), $comparison);
        } else {
            throw new PropelException('filterByJenisLembagaRelatedByJenisLembagaId() only accepts arguments of type JenisLembaga or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the JenisLembagaRelatedByJenisLembagaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return LembagaNonSekolahQuery The current query, for fluid interface
     */
    public function joinJenisLembagaRelatedByJenisLembagaId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('JenisLembagaRelatedByJenisLembagaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'JenisLembagaRelatedByJenisLembagaId');
        }

        return $this;
    }

    /**
     * Use the JenisLembagaRelatedByJenisLembagaId relation JenisLembaga object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\JenisLembagaQuery A secondary query class using the current class as primary query
     */
    public function useJenisLembagaRelatedByJenisLembagaIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinJenisLembagaRelatedByJenisLembagaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'JenisLembagaRelatedByJenisLembagaId', '\angulex\Model\JenisLembagaQuery');
    }

    /**
     * Filter the query by a related MstWilayah object
     *
     * @param   MstWilayah|PropelObjectCollection $mstWilayah The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 LembagaNonSekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByMstWilayahRelatedByKodeWilayah($mstWilayah, $comparison = null)
    {
        if ($mstWilayah instanceof MstWilayah) {
            return $this
                ->addUsingAlias(LembagaNonSekolahPeer::KODE_WILAYAH, $mstWilayah->getKodeWilayah(), $comparison);
        } elseif ($mstWilayah instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(LembagaNonSekolahPeer::KODE_WILAYAH, $mstWilayah->toKeyValue('PrimaryKey', 'KodeWilayah'), $comparison);
        } else {
            throw new PropelException('filterByMstWilayahRelatedByKodeWilayah() only accepts arguments of type MstWilayah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MstWilayahRelatedByKodeWilayah relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return LembagaNonSekolahQuery The current query, for fluid interface
     */
    public function joinMstWilayahRelatedByKodeWilayah($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MstWilayahRelatedByKodeWilayah');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MstWilayahRelatedByKodeWilayah');
        }

        return $this;
    }

    /**
     * Use the MstWilayahRelatedByKodeWilayah relation MstWilayah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\MstWilayahQuery A secondary query class using the current class as primary query
     */
    public function useMstWilayahRelatedByKodeWilayahQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMstWilayahRelatedByKodeWilayah($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MstWilayahRelatedByKodeWilayah', '\angulex\Model\MstWilayahQuery');
    }

    /**
     * Filter the query by a related MstWilayah object
     *
     * @param   MstWilayah|PropelObjectCollection $mstWilayah The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 LembagaNonSekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByMstWilayahRelatedByKodeWilayah($mstWilayah, $comparison = null)
    {
        if ($mstWilayah instanceof MstWilayah) {
            return $this
                ->addUsingAlias(LembagaNonSekolahPeer::KODE_WILAYAH, $mstWilayah->getKodeWilayah(), $comparison);
        } elseif ($mstWilayah instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(LembagaNonSekolahPeer::KODE_WILAYAH, $mstWilayah->toKeyValue('PrimaryKey', 'KodeWilayah'), $comparison);
        } else {
            throw new PropelException('filterByMstWilayahRelatedByKodeWilayah() only accepts arguments of type MstWilayah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MstWilayahRelatedByKodeWilayah relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return LembagaNonSekolahQuery The current query, for fluid interface
     */
    public function joinMstWilayahRelatedByKodeWilayah($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MstWilayahRelatedByKodeWilayah');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MstWilayahRelatedByKodeWilayah');
        }

        return $this;
    }

    /**
     * Use the MstWilayahRelatedByKodeWilayah relation MstWilayah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\MstWilayahQuery A secondary query class using the current class as primary query
     */
    public function useMstWilayahRelatedByKodeWilayahQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMstWilayahRelatedByKodeWilayah($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MstWilayahRelatedByKodeWilayah', '\angulex\Model\MstWilayahQuery');
    }

    /**
     * Filter the query by a related Pengguna object
     *
     * @param   Pengguna|PropelObjectCollection $pengguna  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 LembagaNonSekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPenggunaRelatedByLembagaId($pengguna, $comparison = null)
    {
        if ($pengguna instanceof Pengguna) {
            return $this
                ->addUsingAlias(LembagaNonSekolahPeer::LEMBAGA_ID, $pengguna->getLembagaId(), $comparison);
        } elseif ($pengguna instanceof PropelObjectCollection) {
            return $this
                ->usePenggunaRelatedByLembagaIdQuery()
                ->filterByPrimaryKeys($pengguna->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPenggunaRelatedByLembagaId() only accepts arguments of type Pengguna or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PenggunaRelatedByLembagaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return LembagaNonSekolahQuery The current query, for fluid interface
     */
    public function joinPenggunaRelatedByLembagaId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PenggunaRelatedByLembagaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PenggunaRelatedByLembagaId');
        }

        return $this;
    }

    /**
     * Use the PenggunaRelatedByLembagaId relation Pengguna object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PenggunaQuery A secondary query class using the current class as primary query
     */
    public function usePenggunaRelatedByLembagaIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPenggunaRelatedByLembagaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PenggunaRelatedByLembagaId', '\angulex\Model\PenggunaQuery');
    }

    /**
     * Filter the query by a related Pengguna object
     *
     * @param   Pengguna|PropelObjectCollection $pengguna  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 LembagaNonSekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPenggunaRelatedByLembagaId($pengguna, $comparison = null)
    {
        if ($pengguna instanceof Pengguna) {
            return $this
                ->addUsingAlias(LembagaNonSekolahPeer::LEMBAGA_ID, $pengguna->getLembagaId(), $comparison);
        } elseif ($pengguna instanceof PropelObjectCollection) {
            return $this
                ->usePenggunaRelatedByLembagaIdQuery()
                ->filterByPrimaryKeys($pengguna->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPenggunaRelatedByLembagaId() only accepts arguments of type Pengguna or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PenggunaRelatedByLembagaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return LembagaNonSekolahQuery The current query, for fluid interface
     */
    public function joinPenggunaRelatedByLembagaId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PenggunaRelatedByLembagaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PenggunaRelatedByLembagaId');
        }

        return $this;
    }

    /**
     * Use the PenggunaRelatedByLembagaId relation Pengguna object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PenggunaQuery A secondary query class using the current class as primary query
     */
    public function usePenggunaRelatedByLembagaIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPenggunaRelatedByLembagaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PenggunaRelatedByLembagaId', '\angulex\Model\PenggunaQuery');
    }

    /**
     * Filter the query by a related VldNonsekolah object
     *
     * @param   VldNonsekolah|PropelObjectCollection $vldNonsekolah  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 LembagaNonSekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldNonsekolahRelatedByLembagaId($vldNonsekolah, $comparison = null)
    {
        if ($vldNonsekolah instanceof VldNonsekolah) {
            return $this
                ->addUsingAlias(LembagaNonSekolahPeer::LEMBAGA_ID, $vldNonsekolah->getLembagaId(), $comparison);
        } elseif ($vldNonsekolah instanceof PropelObjectCollection) {
            return $this
                ->useVldNonsekolahRelatedByLembagaIdQuery()
                ->filterByPrimaryKeys($vldNonsekolah->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldNonsekolahRelatedByLembagaId() only accepts arguments of type VldNonsekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldNonsekolahRelatedByLembagaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return LembagaNonSekolahQuery The current query, for fluid interface
     */
    public function joinVldNonsekolahRelatedByLembagaId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldNonsekolahRelatedByLembagaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldNonsekolahRelatedByLembagaId');
        }

        return $this;
    }

    /**
     * Use the VldNonsekolahRelatedByLembagaId relation VldNonsekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldNonsekolahQuery A secondary query class using the current class as primary query
     */
    public function useVldNonsekolahRelatedByLembagaIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldNonsekolahRelatedByLembagaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldNonsekolahRelatedByLembagaId', '\angulex\Model\VldNonsekolahQuery');
    }

    /**
     * Filter the query by a related VldNonsekolah object
     *
     * @param   VldNonsekolah|PropelObjectCollection $vldNonsekolah  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 LembagaNonSekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldNonsekolahRelatedByLembagaId($vldNonsekolah, $comparison = null)
    {
        if ($vldNonsekolah instanceof VldNonsekolah) {
            return $this
                ->addUsingAlias(LembagaNonSekolahPeer::LEMBAGA_ID, $vldNonsekolah->getLembagaId(), $comparison);
        } elseif ($vldNonsekolah instanceof PropelObjectCollection) {
            return $this
                ->useVldNonsekolahRelatedByLembagaIdQuery()
                ->filterByPrimaryKeys($vldNonsekolah->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldNonsekolahRelatedByLembagaId() only accepts arguments of type VldNonsekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldNonsekolahRelatedByLembagaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return LembagaNonSekolahQuery The current query, for fluid interface
     */
    public function joinVldNonsekolahRelatedByLembagaId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldNonsekolahRelatedByLembagaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldNonsekolahRelatedByLembagaId');
        }

        return $this;
    }

    /**
     * Use the VldNonsekolahRelatedByLembagaId relation VldNonsekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldNonsekolahQuery A secondary query class using the current class as primary query
     */
    public function useVldNonsekolahRelatedByLembagaIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldNonsekolahRelatedByLembagaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldNonsekolahRelatedByLembagaId', '\angulex\Model\VldNonsekolahQuery');
    }

    /**
     * Filter the query by a related PengawasTerdaftar object
     *
     * @param   PengawasTerdaftar|PropelObjectCollection $pengawasTerdaftar  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 LembagaNonSekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPengawasTerdaftarRelatedByLembagaId($pengawasTerdaftar, $comparison = null)
    {
        if ($pengawasTerdaftar instanceof PengawasTerdaftar) {
            return $this
                ->addUsingAlias(LembagaNonSekolahPeer::LEMBAGA_ID, $pengawasTerdaftar->getLembagaId(), $comparison);
        } elseif ($pengawasTerdaftar instanceof PropelObjectCollection) {
            return $this
                ->usePengawasTerdaftarRelatedByLembagaIdQuery()
                ->filterByPrimaryKeys($pengawasTerdaftar->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPengawasTerdaftarRelatedByLembagaId() only accepts arguments of type PengawasTerdaftar or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PengawasTerdaftarRelatedByLembagaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return LembagaNonSekolahQuery The current query, for fluid interface
     */
    public function joinPengawasTerdaftarRelatedByLembagaId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PengawasTerdaftarRelatedByLembagaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PengawasTerdaftarRelatedByLembagaId');
        }

        return $this;
    }

    /**
     * Use the PengawasTerdaftarRelatedByLembagaId relation PengawasTerdaftar object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PengawasTerdaftarQuery A secondary query class using the current class as primary query
     */
    public function usePengawasTerdaftarRelatedByLembagaIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPengawasTerdaftarRelatedByLembagaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PengawasTerdaftarRelatedByLembagaId', '\angulex\Model\PengawasTerdaftarQuery');
    }

    /**
     * Filter the query by a related PengawasTerdaftar object
     *
     * @param   PengawasTerdaftar|PropelObjectCollection $pengawasTerdaftar  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 LembagaNonSekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPengawasTerdaftarRelatedByLembagaId($pengawasTerdaftar, $comparison = null)
    {
        if ($pengawasTerdaftar instanceof PengawasTerdaftar) {
            return $this
                ->addUsingAlias(LembagaNonSekolahPeer::LEMBAGA_ID, $pengawasTerdaftar->getLembagaId(), $comparison);
        } elseif ($pengawasTerdaftar instanceof PropelObjectCollection) {
            return $this
                ->usePengawasTerdaftarRelatedByLembagaIdQuery()
                ->filterByPrimaryKeys($pengawasTerdaftar->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPengawasTerdaftarRelatedByLembagaId() only accepts arguments of type PengawasTerdaftar or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PengawasTerdaftarRelatedByLembagaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return LembagaNonSekolahQuery The current query, for fluid interface
     */
    public function joinPengawasTerdaftarRelatedByLembagaId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PengawasTerdaftarRelatedByLembagaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PengawasTerdaftarRelatedByLembagaId');
        }

        return $this;
    }

    /**
     * Use the PengawasTerdaftarRelatedByLembagaId relation PengawasTerdaftar object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PengawasTerdaftarQuery A secondary query class using the current class as primary query
     */
    public function usePengawasTerdaftarRelatedByLembagaIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPengawasTerdaftarRelatedByLembagaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PengawasTerdaftarRelatedByLembagaId', '\angulex\Model\PengawasTerdaftarQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   LembagaNonSekolah $lembagaNonSekolah Object to remove from the list of results
     *
     * @return LembagaNonSekolahQuery The current query, for fluid interface
     */
    public function prune($lembagaNonSekolah = null)
    {
        if ($lembagaNonSekolah) {
            $this->addUsingAlias(LembagaNonSekolahPeer::LEMBAGA_ID, $lembagaNonSekolah->getLembagaId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
