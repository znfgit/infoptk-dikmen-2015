<?php

namespace angulex\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\MstWilayah;
use angulex\Model\Sekolah;
use angulex\Model\WdstSyncLog;
use angulex\Model\WdstSyncLogPeer;
use angulex\Model\WdstSyncLogQuery;

/**
 * Base class that represents a query for the 'wdst_sync_log' table.
 *
 * 
 *
 * @method WdstSyncLogQuery orderByKodeWilayah($order = Criteria::ASC) Order by the kode_wilayah column
 * @method WdstSyncLogQuery orderBySekolahId($order = Criteria::ASC) Order by the sekolah_id column
 * @method WdstSyncLogQuery orderByBeginSync($order = Criteria::ASC) Order by the begin_sync column
 * @method WdstSyncLogQuery orderByEndSync($order = Criteria::ASC) Order by the end_sync column
 * @method WdstSyncLogQuery orderBySyncMedia($order = Criteria::ASC) Order by the sync_media column
 * @method WdstSyncLogQuery orderByIsSuccess($order = Criteria::ASC) Order by the is_success column
 * @method WdstSyncLogQuery orderBySelisihWaktuServer($order = Criteria::ASC) Order by the selisih_waktu_server column
 * @method WdstSyncLogQuery orderByAlamatIp($order = Criteria::ASC) Order by the alamat_ip column
 * @method WdstSyncLogQuery orderByPenggunaId($order = Criteria::ASC) Order by the pengguna_id column
 *
 * @method WdstSyncLogQuery groupByKodeWilayah() Group by the kode_wilayah column
 * @method WdstSyncLogQuery groupBySekolahId() Group by the sekolah_id column
 * @method WdstSyncLogQuery groupByBeginSync() Group by the begin_sync column
 * @method WdstSyncLogQuery groupByEndSync() Group by the end_sync column
 * @method WdstSyncLogQuery groupBySyncMedia() Group by the sync_media column
 * @method WdstSyncLogQuery groupByIsSuccess() Group by the is_success column
 * @method WdstSyncLogQuery groupBySelisihWaktuServer() Group by the selisih_waktu_server column
 * @method WdstSyncLogQuery groupByAlamatIp() Group by the alamat_ip column
 * @method WdstSyncLogQuery groupByPenggunaId() Group by the pengguna_id column
 *
 * @method WdstSyncLogQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method WdstSyncLogQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method WdstSyncLogQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method WdstSyncLogQuery leftJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method WdstSyncLogQuery rightJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method WdstSyncLogQuery innerJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the SekolahRelatedBySekolahId relation
 *
 * @method WdstSyncLogQuery leftJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method WdstSyncLogQuery rightJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method WdstSyncLogQuery innerJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the SekolahRelatedBySekolahId relation
 *
 * @method WdstSyncLogQuery leftJoinMstWilayahRelatedByKodeWilayah($relationAlias = null) Adds a LEFT JOIN clause to the query using the MstWilayahRelatedByKodeWilayah relation
 * @method WdstSyncLogQuery rightJoinMstWilayahRelatedByKodeWilayah($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MstWilayahRelatedByKodeWilayah relation
 * @method WdstSyncLogQuery innerJoinMstWilayahRelatedByKodeWilayah($relationAlias = null) Adds a INNER JOIN clause to the query using the MstWilayahRelatedByKodeWilayah relation
 *
 * @method WdstSyncLogQuery leftJoinMstWilayahRelatedByKodeWilayah($relationAlias = null) Adds a LEFT JOIN clause to the query using the MstWilayahRelatedByKodeWilayah relation
 * @method WdstSyncLogQuery rightJoinMstWilayahRelatedByKodeWilayah($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MstWilayahRelatedByKodeWilayah relation
 * @method WdstSyncLogQuery innerJoinMstWilayahRelatedByKodeWilayah($relationAlias = null) Adds a INNER JOIN clause to the query using the MstWilayahRelatedByKodeWilayah relation
 *
 * @method WdstSyncLog findOne(PropelPDO $con = null) Return the first WdstSyncLog matching the query
 * @method WdstSyncLog findOneOrCreate(PropelPDO $con = null) Return the first WdstSyncLog matching the query, or a new WdstSyncLog object populated from the query conditions when no match is found
 *
 * @method WdstSyncLog findOneByKodeWilayah(string $kode_wilayah) Return the first WdstSyncLog filtered by the kode_wilayah column
 * @method WdstSyncLog findOneBySekolahId(string $sekolah_id) Return the first WdstSyncLog filtered by the sekolah_id column
 * @method WdstSyncLog findOneByBeginSync(string $begin_sync) Return the first WdstSyncLog filtered by the begin_sync column
 * @method WdstSyncLog findOneByEndSync(string $end_sync) Return the first WdstSyncLog filtered by the end_sync column
 * @method WdstSyncLog findOneBySyncMedia(string $sync_media) Return the first WdstSyncLog filtered by the sync_media column
 * @method WdstSyncLog findOneByIsSuccess(string $is_success) Return the first WdstSyncLog filtered by the is_success column
 * @method WdstSyncLog findOneBySelisihWaktuServer(string $selisih_waktu_server) Return the first WdstSyncLog filtered by the selisih_waktu_server column
 * @method WdstSyncLog findOneByAlamatIp(string $alamat_ip) Return the first WdstSyncLog filtered by the alamat_ip column
 * @method WdstSyncLog findOneByPenggunaId(string $pengguna_id) Return the first WdstSyncLog filtered by the pengguna_id column
 *
 * @method array findByKodeWilayah(string $kode_wilayah) Return WdstSyncLog objects filtered by the kode_wilayah column
 * @method array findBySekolahId(string $sekolah_id) Return WdstSyncLog objects filtered by the sekolah_id column
 * @method array findByBeginSync(string $begin_sync) Return WdstSyncLog objects filtered by the begin_sync column
 * @method array findByEndSync(string $end_sync) Return WdstSyncLog objects filtered by the end_sync column
 * @method array findBySyncMedia(string $sync_media) Return WdstSyncLog objects filtered by the sync_media column
 * @method array findByIsSuccess(string $is_success) Return WdstSyncLog objects filtered by the is_success column
 * @method array findBySelisihWaktuServer(string $selisih_waktu_server) Return WdstSyncLog objects filtered by the selisih_waktu_server column
 * @method array findByAlamatIp(string $alamat_ip) Return WdstSyncLog objects filtered by the alamat_ip column
 * @method array findByPenggunaId(string $pengguna_id) Return WdstSyncLog objects filtered by the pengguna_id column
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseWdstSyncLogQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseWdstSyncLogQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Dapodikmen', $modelName = 'angulex\\Model\\WdstSyncLog', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new WdstSyncLogQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   WdstSyncLogQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return WdstSyncLogQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof WdstSyncLogQuery) {
            return $criteria;
        }
        $query = new WdstSyncLogQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34, 56), $con);
     * </code>
     *
     * @param array $key Primary key to use for the query 
                         A Primary key composition: [$kode_wilayah, $sekolah_id, $begin_sync]
     * @param     PropelPDO $con an optional connection object
     *
     * @return   WdstSyncLog|WdstSyncLog[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = WdstSyncLogPeer::getInstanceFromPool(serialize(array((string) $key[0], (string) $key[1], (string) $key[2]))))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(WdstSyncLogPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 WdstSyncLog A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [kode_wilayah], [sekolah_id], [begin_sync], [end_sync], [sync_media], [is_success], [selisih_waktu_server], [alamat_ip], [pengguna_id] FROM [wdst_sync_log] WHERE [kode_wilayah] = :p0 AND [sekolah_id] = :p1 AND [begin_sync] = :p2';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_STR);			
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_STR);			
            $stmt->bindValue(':p2', $key[2], PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new WdstSyncLog();
            $obj->hydrate($row);
            WdstSyncLogPeer::addInstanceToPool($obj, serialize(array((string) $key[0], (string) $key[1], (string) $key[2])));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return WdstSyncLog|WdstSyncLog[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|WdstSyncLog[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return WdstSyncLogQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(WdstSyncLogPeer::KODE_WILAYAH, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(WdstSyncLogPeer::SEKOLAH_ID, $key[1], Criteria::EQUAL);
        $this->addUsingAlias(WdstSyncLogPeer::BEGIN_SYNC, $key[2], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return WdstSyncLogQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(WdstSyncLogPeer::KODE_WILAYAH, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(WdstSyncLogPeer::SEKOLAH_ID, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $cton2 = $this->getNewCriterion(WdstSyncLogPeer::BEGIN_SYNC, $key[2], Criteria::EQUAL);
            $cton0->addAnd($cton2);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the kode_wilayah column
     *
     * Example usage:
     * <code>
     * $query->filterByKodeWilayah('fooValue');   // WHERE kode_wilayah = 'fooValue'
     * $query->filterByKodeWilayah('%fooValue%'); // WHERE kode_wilayah LIKE '%fooValue%'
     * </code>
     *
     * @param     string $kodeWilayah The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return WdstSyncLogQuery The current query, for fluid interface
     */
    public function filterByKodeWilayah($kodeWilayah = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($kodeWilayah)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $kodeWilayah)) {
                $kodeWilayah = str_replace('*', '%', $kodeWilayah);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(WdstSyncLogPeer::KODE_WILAYAH, $kodeWilayah, $comparison);
    }

    /**
     * Filter the query on the sekolah_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySekolahId('fooValue');   // WHERE sekolah_id = 'fooValue'
     * $query->filterBySekolahId('%fooValue%'); // WHERE sekolah_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sekolahId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return WdstSyncLogQuery The current query, for fluid interface
     */
    public function filterBySekolahId($sekolahId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sekolahId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $sekolahId)) {
                $sekolahId = str_replace('*', '%', $sekolahId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(WdstSyncLogPeer::SEKOLAH_ID, $sekolahId, $comparison);
    }

    /**
     * Filter the query on the begin_sync column
     *
     * Example usage:
     * <code>
     * $query->filterByBeginSync('2011-03-14'); // WHERE begin_sync = '2011-03-14'
     * $query->filterByBeginSync('now'); // WHERE begin_sync = '2011-03-14'
     * $query->filterByBeginSync(array('max' => 'yesterday')); // WHERE begin_sync > '2011-03-13'
     * </code>
     *
     * @param     mixed $beginSync The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return WdstSyncLogQuery The current query, for fluid interface
     */
    public function filterByBeginSync($beginSync = null, $comparison = null)
    {
        if (is_array($beginSync)) {
            $useMinMax = false;
            if (isset($beginSync['min'])) {
                $this->addUsingAlias(WdstSyncLogPeer::BEGIN_SYNC, $beginSync['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($beginSync['max'])) {
                $this->addUsingAlias(WdstSyncLogPeer::BEGIN_SYNC, $beginSync['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WdstSyncLogPeer::BEGIN_SYNC, $beginSync, $comparison);
    }

    /**
     * Filter the query on the end_sync column
     *
     * Example usage:
     * <code>
     * $query->filterByEndSync('2011-03-14'); // WHERE end_sync = '2011-03-14'
     * $query->filterByEndSync('now'); // WHERE end_sync = '2011-03-14'
     * $query->filterByEndSync(array('max' => 'yesterday')); // WHERE end_sync > '2011-03-13'
     * </code>
     *
     * @param     mixed $endSync The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return WdstSyncLogQuery The current query, for fluid interface
     */
    public function filterByEndSync($endSync = null, $comparison = null)
    {
        if (is_array($endSync)) {
            $useMinMax = false;
            if (isset($endSync['min'])) {
                $this->addUsingAlias(WdstSyncLogPeer::END_SYNC, $endSync['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($endSync['max'])) {
                $this->addUsingAlias(WdstSyncLogPeer::END_SYNC, $endSync['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WdstSyncLogPeer::END_SYNC, $endSync, $comparison);
    }

    /**
     * Filter the query on the sync_media column
     *
     * Example usage:
     * <code>
     * $query->filterBySyncMedia('fooValue');   // WHERE sync_media = 'fooValue'
     * $query->filterBySyncMedia('%fooValue%'); // WHERE sync_media LIKE '%fooValue%'
     * </code>
     *
     * @param     string $syncMedia The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return WdstSyncLogQuery The current query, for fluid interface
     */
    public function filterBySyncMedia($syncMedia = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($syncMedia)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $syncMedia)) {
                $syncMedia = str_replace('*', '%', $syncMedia);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(WdstSyncLogPeer::SYNC_MEDIA, $syncMedia, $comparison);
    }

    /**
     * Filter the query on the is_success column
     *
     * Example usage:
     * <code>
     * $query->filterByIsSuccess(1234); // WHERE is_success = 1234
     * $query->filterByIsSuccess(array(12, 34)); // WHERE is_success IN (12, 34)
     * $query->filterByIsSuccess(array('min' => 12)); // WHERE is_success >= 12
     * $query->filterByIsSuccess(array('max' => 12)); // WHERE is_success <= 12
     * </code>
     *
     * @param     mixed $isSuccess The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return WdstSyncLogQuery The current query, for fluid interface
     */
    public function filterByIsSuccess($isSuccess = null, $comparison = null)
    {
        if (is_array($isSuccess)) {
            $useMinMax = false;
            if (isset($isSuccess['min'])) {
                $this->addUsingAlias(WdstSyncLogPeer::IS_SUCCESS, $isSuccess['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($isSuccess['max'])) {
                $this->addUsingAlias(WdstSyncLogPeer::IS_SUCCESS, $isSuccess['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WdstSyncLogPeer::IS_SUCCESS, $isSuccess, $comparison);
    }

    /**
     * Filter the query on the selisih_waktu_server column
     *
     * Example usage:
     * <code>
     * $query->filterBySelisihWaktuServer(1234); // WHERE selisih_waktu_server = 1234
     * $query->filterBySelisihWaktuServer(array(12, 34)); // WHERE selisih_waktu_server IN (12, 34)
     * $query->filterBySelisihWaktuServer(array('min' => 12)); // WHERE selisih_waktu_server >= 12
     * $query->filterBySelisihWaktuServer(array('max' => 12)); // WHERE selisih_waktu_server <= 12
     * </code>
     *
     * @param     mixed $selisihWaktuServer The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return WdstSyncLogQuery The current query, for fluid interface
     */
    public function filterBySelisihWaktuServer($selisihWaktuServer = null, $comparison = null)
    {
        if (is_array($selisihWaktuServer)) {
            $useMinMax = false;
            if (isset($selisihWaktuServer['min'])) {
                $this->addUsingAlias(WdstSyncLogPeer::SELISIH_WAKTU_SERVER, $selisihWaktuServer['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($selisihWaktuServer['max'])) {
                $this->addUsingAlias(WdstSyncLogPeer::SELISIH_WAKTU_SERVER, $selisihWaktuServer['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WdstSyncLogPeer::SELISIH_WAKTU_SERVER, $selisihWaktuServer, $comparison);
    }

    /**
     * Filter the query on the alamat_ip column
     *
     * Example usage:
     * <code>
     * $query->filterByAlamatIp('fooValue');   // WHERE alamat_ip = 'fooValue'
     * $query->filterByAlamatIp('%fooValue%'); // WHERE alamat_ip LIKE '%fooValue%'
     * </code>
     *
     * @param     string $alamatIp The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return WdstSyncLogQuery The current query, for fluid interface
     */
    public function filterByAlamatIp($alamatIp = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($alamatIp)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $alamatIp)) {
                $alamatIp = str_replace('*', '%', $alamatIp);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(WdstSyncLogPeer::ALAMAT_IP, $alamatIp, $comparison);
    }

    /**
     * Filter the query on the pengguna_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPenggunaId('fooValue');   // WHERE pengguna_id = 'fooValue'
     * $query->filterByPenggunaId('%fooValue%'); // WHERE pengguna_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $penggunaId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return WdstSyncLogQuery The current query, for fluid interface
     */
    public function filterByPenggunaId($penggunaId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($penggunaId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $penggunaId)) {
                $penggunaId = str_replace('*', '%', $penggunaId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(WdstSyncLogPeer::PENGGUNA_ID, $penggunaId, $comparison);
    }

    /**
     * Filter the query by a related Sekolah object
     *
     * @param   Sekolah|PropelObjectCollection $sekolah The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 WdstSyncLogQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySekolahRelatedBySekolahId($sekolah, $comparison = null)
    {
        if ($sekolah instanceof Sekolah) {
            return $this
                ->addUsingAlias(WdstSyncLogPeer::SEKOLAH_ID, $sekolah->getSekolahId(), $comparison);
        } elseif ($sekolah instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(WdstSyncLogPeer::SEKOLAH_ID, $sekolah->toKeyValue('PrimaryKey', 'SekolahId'), $comparison);
        } else {
            throw new PropelException('filterBySekolahRelatedBySekolahId() only accepts arguments of type Sekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SekolahRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return WdstSyncLogQuery The current query, for fluid interface
     */
    public function joinSekolahRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SekolahRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SekolahRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the SekolahRelatedBySekolahId relation Sekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SekolahQuery A secondary query class using the current class as primary query
     */
    public function useSekolahRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSekolahRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SekolahRelatedBySekolahId', '\angulex\Model\SekolahQuery');
    }

    /**
     * Filter the query by a related Sekolah object
     *
     * @param   Sekolah|PropelObjectCollection $sekolah The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 WdstSyncLogQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySekolahRelatedBySekolahId($sekolah, $comparison = null)
    {
        if ($sekolah instanceof Sekolah) {
            return $this
                ->addUsingAlias(WdstSyncLogPeer::SEKOLAH_ID, $sekolah->getSekolahId(), $comparison);
        } elseif ($sekolah instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(WdstSyncLogPeer::SEKOLAH_ID, $sekolah->toKeyValue('PrimaryKey', 'SekolahId'), $comparison);
        } else {
            throw new PropelException('filterBySekolahRelatedBySekolahId() only accepts arguments of type Sekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SekolahRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return WdstSyncLogQuery The current query, for fluid interface
     */
    public function joinSekolahRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SekolahRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SekolahRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the SekolahRelatedBySekolahId relation Sekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SekolahQuery A secondary query class using the current class as primary query
     */
    public function useSekolahRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSekolahRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SekolahRelatedBySekolahId', '\angulex\Model\SekolahQuery');
    }

    /**
     * Filter the query by a related MstWilayah object
     *
     * @param   MstWilayah|PropelObjectCollection $mstWilayah The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 WdstSyncLogQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByMstWilayahRelatedByKodeWilayah($mstWilayah, $comparison = null)
    {
        if ($mstWilayah instanceof MstWilayah) {
            return $this
                ->addUsingAlias(WdstSyncLogPeer::KODE_WILAYAH, $mstWilayah->getKodeWilayah(), $comparison);
        } elseif ($mstWilayah instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(WdstSyncLogPeer::KODE_WILAYAH, $mstWilayah->toKeyValue('PrimaryKey', 'KodeWilayah'), $comparison);
        } else {
            throw new PropelException('filterByMstWilayahRelatedByKodeWilayah() only accepts arguments of type MstWilayah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MstWilayahRelatedByKodeWilayah relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return WdstSyncLogQuery The current query, for fluid interface
     */
    public function joinMstWilayahRelatedByKodeWilayah($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MstWilayahRelatedByKodeWilayah');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MstWilayahRelatedByKodeWilayah');
        }

        return $this;
    }

    /**
     * Use the MstWilayahRelatedByKodeWilayah relation MstWilayah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\MstWilayahQuery A secondary query class using the current class as primary query
     */
    public function useMstWilayahRelatedByKodeWilayahQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMstWilayahRelatedByKodeWilayah($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MstWilayahRelatedByKodeWilayah', '\angulex\Model\MstWilayahQuery');
    }

    /**
     * Filter the query by a related MstWilayah object
     *
     * @param   MstWilayah|PropelObjectCollection $mstWilayah The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 WdstSyncLogQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByMstWilayahRelatedByKodeWilayah($mstWilayah, $comparison = null)
    {
        if ($mstWilayah instanceof MstWilayah) {
            return $this
                ->addUsingAlias(WdstSyncLogPeer::KODE_WILAYAH, $mstWilayah->getKodeWilayah(), $comparison);
        } elseif ($mstWilayah instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(WdstSyncLogPeer::KODE_WILAYAH, $mstWilayah->toKeyValue('PrimaryKey', 'KodeWilayah'), $comparison);
        } else {
            throw new PropelException('filterByMstWilayahRelatedByKodeWilayah() only accepts arguments of type MstWilayah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MstWilayahRelatedByKodeWilayah relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return WdstSyncLogQuery The current query, for fluid interface
     */
    public function joinMstWilayahRelatedByKodeWilayah($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MstWilayahRelatedByKodeWilayah');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MstWilayahRelatedByKodeWilayah');
        }

        return $this;
    }

    /**
     * Use the MstWilayahRelatedByKodeWilayah relation MstWilayah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\MstWilayahQuery A secondary query class using the current class as primary query
     */
    public function useMstWilayahRelatedByKodeWilayahQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMstWilayahRelatedByKodeWilayah($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MstWilayahRelatedByKodeWilayah', '\angulex\Model\MstWilayahQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   WdstSyncLog $wdstSyncLog Object to remove from the list of results
     *
     * @return WdstSyncLogQuery The current query, for fluid interface
     */
    public function prune($wdstSyncLog = null)
    {
        if ($wdstSyncLog) {
            $this->addCond('pruneCond0', $this->getAliasedColName(WdstSyncLogPeer::KODE_WILAYAH), $wdstSyncLog->getKodeWilayah(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(WdstSyncLogPeer::SEKOLAH_ID), $wdstSyncLog->getSekolahId(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond2', $this->getAliasedColName(WdstSyncLogPeer::BEGIN_SYNC), $wdstSyncLog->getBeginSync(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1', 'pruneCond2'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

}
