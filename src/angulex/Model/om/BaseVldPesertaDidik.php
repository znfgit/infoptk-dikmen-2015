<?php

namespace angulex\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelDateTime;
use \PropelException;
use \PropelPDO;
use angulex\Model\Errortype;
use angulex\Model\ErrortypeQuery;
use angulex\Model\PesertaDidik;
use angulex\Model\PesertaDidikQuery;
use angulex\Model\VldPesertaDidik;
use angulex\Model\VldPesertaDidikPeer;
use angulex\Model\VldPesertaDidikQuery;

/**
 * Base class that represents a row from the 'vld_peserta_didik' table.
 *
 * 
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseVldPesertaDidik extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'angulex\\Model\\VldPesertaDidikPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        VldPesertaDidikPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the peserta_didik_id field.
     * @var        string
     */
    protected $peserta_didik_id;

    /**
     * The value for the logid field.
     * @var        string
     */
    protected $logid;

    /**
     * The value for the idtype field.
     * @var        int
     */
    protected $idtype;

    /**
     * The value for the status_validasi field.
     * @var        string
     */
    protected $status_validasi;

    /**
     * The value for the status_verifikasi field.
     * @var        string
     */
    protected $status_verifikasi;

    /**
     * The value for the field_error field.
     * @var        string
     */
    protected $field_error;

    /**
     * The value for the error_message field.
     * @var        string
     */
    protected $error_message;

    /**
     * The value for the create_date field.
     * @var        string
     */
    protected $create_date;

    /**
     * The value for the last_update field.
     * @var        string
     */
    protected $last_update;

    /**
     * The value for the expired_date field.
     * @var        string
     */
    protected $expired_date;

    /**
     * The value for the last_sync field.
     * @var        string
     */
    protected $last_sync;

    /**
     * The value for the app_username field.
     * @var        string
     */
    protected $app_username;

    /**
     * @var        PesertaDidik
     */
    protected $aPesertaDidikRelatedByPesertaDidikId;

    /**
     * @var        PesertaDidik
     */
    protected $aPesertaDidikRelatedByPesertaDidikId;

    /**
     * @var        Errortype
     */
    protected $aErrortypeRelatedByIdtype;

    /**
     * @var        Errortype
     */
    protected $aErrortypeRelatedByIdtype;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Get the [peserta_didik_id] column value.
     * 
     * @return string
     */
    public function getPesertaDidikId()
    {
        return $this->peserta_didik_id;
    }

    /**
     * Get the [logid] column value.
     * 
     * @return string
     */
    public function getLogid()
    {
        return $this->logid;
    }

    /**
     * Get the [idtype] column value.
     * 
     * @return int
     */
    public function getIdtype()
    {
        return $this->idtype;
    }

    /**
     * Get the [status_validasi] column value.
     * 
     * @return string
     */
    public function getStatusValidasi()
    {
        return $this->status_validasi;
    }

    /**
     * Get the [status_verifikasi] column value.
     * 
     * @return string
     */
    public function getStatusVerifikasi()
    {
        return $this->status_verifikasi;
    }

    /**
     * Get the [field_error] column value.
     * 
     * @return string
     */
    public function getFieldError()
    {
        return $this->field_error;
    }

    /**
     * Get the [error_message] column value.
     * 
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->error_message;
    }

    /**
     * Get the [optionally formatted] temporal [create_date] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreateDate($format = 'Y-m-d H:i:s')
    {
        if ($this->create_date === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->create_date);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->create_date, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [last_update] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastUpdate($format = 'Y-m-d H:i:s')
    {
        if ($this->last_update === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_update);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_update, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [expired_date] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getExpiredDate($format = 'Y-m-d H:i:s')
    {
        if ($this->expired_date === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->expired_date);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->expired_date, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [last_sync] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastSync($format = 'Y-m-d H:i:s')
    {
        if ($this->last_sync === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_sync);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_sync, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [app_username] column value.
     * 
     * @return string
     */
    public function getAppUsername()
    {
        return $this->app_username;
    }

    /**
     * Set the value of [peserta_didik_id] column.
     * 
     * @param string $v new value
     * @return VldPesertaDidik The current object (for fluent API support)
     */
    public function setPesertaDidikId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->peserta_didik_id !== $v) {
            $this->peserta_didik_id = $v;
            $this->modifiedColumns[] = VldPesertaDidikPeer::PESERTA_DIDIK_ID;
        }

        if ($this->aPesertaDidikRelatedByPesertaDidikId !== null && $this->aPesertaDidikRelatedByPesertaDidikId->getPesertaDidikId() !== $v) {
            $this->aPesertaDidikRelatedByPesertaDidikId = null;
        }

        if ($this->aPesertaDidikRelatedByPesertaDidikId !== null && $this->aPesertaDidikRelatedByPesertaDidikId->getPesertaDidikId() !== $v) {
            $this->aPesertaDidikRelatedByPesertaDidikId = null;
        }


        return $this;
    } // setPesertaDidikId()

    /**
     * Set the value of [logid] column.
     * 
     * @param string $v new value
     * @return VldPesertaDidik The current object (for fluent API support)
     */
    public function setLogid($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->logid !== $v) {
            $this->logid = $v;
            $this->modifiedColumns[] = VldPesertaDidikPeer::LOGID;
        }


        return $this;
    } // setLogid()

    /**
     * Set the value of [idtype] column.
     * 
     * @param int $v new value
     * @return VldPesertaDidik The current object (for fluent API support)
     */
    public function setIdtype($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->idtype !== $v) {
            $this->idtype = $v;
            $this->modifiedColumns[] = VldPesertaDidikPeer::IDTYPE;
        }

        if ($this->aErrortypeRelatedByIdtype !== null && $this->aErrortypeRelatedByIdtype->getIdtype() !== $v) {
            $this->aErrortypeRelatedByIdtype = null;
        }

        if ($this->aErrortypeRelatedByIdtype !== null && $this->aErrortypeRelatedByIdtype->getIdtype() !== $v) {
            $this->aErrortypeRelatedByIdtype = null;
        }


        return $this;
    } // setIdtype()

    /**
     * Set the value of [status_validasi] column.
     * 
     * @param string $v new value
     * @return VldPesertaDidik The current object (for fluent API support)
     */
    public function setStatusValidasi($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->status_validasi !== $v) {
            $this->status_validasi = $v;
            $this->modifiedColumns[] = VldPesertaDidikPeer::STATUS_VALIDASI;
        }


        return $this;
    } // setStatusValidasi()

    /**
     * Set the value of [status_verifikasi] column.
     * 
     * @param string $v new value
     * @return VldPesertaDidik The current object (for fluent API support)
     */
    public function setStatusVerifikasi($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->status_verifikasi !== $v) {
            $this->status_verifikasi = $v;
            $this->modifiedColumns[] = VldPesertaDidikPeer::STATUS_VERIFIKASI;
        }


        return $this;
    } // setStatusVerifikasi()

    /**
     * Set the value of [field_error] column.
     * 
     * @param string $v new value
     * @return VldPesertaDidik The current object (for fluent API support)
     */
    public function setFieldError($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->field_error !== $v) {
            $this->field_error = $v;
            $this->modifiedColumns[] = VldPesertaDidikPeer::FIELD_ERROR;
        }


        return $this;
    } // setFieldError()

    /**
     * Set the value of [error_message] column.
     * 
     * @param string $v new value
     * @return VldPesertaDidik The current object (for fluent API support)
     */
    public function setErrorMessage($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->error_message !== $v) {
            $this->error_message = $v;
            $this->modifiedColumns[] = VldPesertaDidikPeer::ERROR_MESSAGE;
        }


        return $this;
    } // setErrorMessage()

    /**
     * Sets the value of [create_date] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return VldPesertaDidik The current object (for fluent API support)
     */
    public function setCreateDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->create_date !== null || $dt !== null) {
            $currentDateAsString = ($this->create_date !== null && $tmpDt = new DateTime($this->create_date)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->create_date = $newDateAsString;
                $this->modifiedColumns[] = VldPesertaDidikPeer::CREATE_DATE;
            }
        } // if either are not null


        return $this;
    } // setCreateDate()

    /**
     * Sets the value of [last_update] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return VldPesertaDidik The current object (for fluent API support)
     */
    public function setLastUpdate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_update !== null || $dt !== null) {
            $currentDateAsString = ($this->last_update !== null && $tmpDt = new DateTime($this->last_update)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_update = $newDateAsString;
                $this->modifiedColumns[] = VldPesertaDidikPeer::LAST_UPDATE;
            }
        } // if either are not null


        return $this;
    } // setLastUpdate()

    /**
     * Sets the value of [expired_date] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return VldPesertaDidik The current object (for fluent API support)
     */
    public function setExpiredDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->expired_date !== null || $dt !== null) {
            $currentDateAsString = ($this->expired_date !== null && $tmpDt = new DateTime($this->expired_date)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->expired_date = $newDateAsString;
                $this->modifiedColumns[] = VldPesertaDidikPeer::EXPIRED_DATE;
            }
        } // if either are not null


        return $this;
    } // setExpiredDate()

    /**
     * Sets the value of [last_sync] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return VldPesertaDidik The current object (for fluent API support)
     */
    public function setLastSync($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_sync !== null || $dt !== null) {
            $currentDateAsString = ($this->last_sync !== null && $tmpDt = new DateTime($this->last_sync)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_sync = $newDateAsString;
                $this->modifiedColumns[] = VldPesertaDidikPeer::LAST_SYNC;
            }
        } // if either are not null


        return $this;
    } // setLastSync()

    /**
     * Set the value of [app_username] column.
     * 
     * @param string $v new value
     * @return VldPesertaDidik The current object (for fluent API support)
     */
    public function setAppUsername($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->app_username !== $v) {
            $this->app_username = $v;
            $this->modifiedColumns[] = VldPesertaDidikPeer::APP_USERNAME;
        }


        return $this;
    } // setAppUsername()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->peserta_didik_id = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
            $this->logid = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->idtype = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->status_validasi = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->status_verifikasi = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->field_error = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->error_message = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->create_date = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->last_update = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->expired_date = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->last_sync = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->app_username = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 12; // 12 = VldPesertaDidikPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating VldPesertaDidik object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aPesertaDidikRelatedByPesertaDidikId !== null && $this->peserta_didik_id !== $this->aPesertaDidikRelatedByPesertaDidikId->getPesertaDidikId()) {
            $this->aPesertaDidikRelatedByPesertaDidikId = null;
        }
        if ($this->aPesertaDidikRelatedByPesertaDidikId !== null && $this->peserta_didik_id !== $this->aPesertaDidikRelatedByPesertaDidikId->getPesertaDidikId()) {
            $this->aPesertaDidikRelatedByPesertaDidikId = null;
        }
        if ($this->aErrortypeRelatedByIdtype !== null && $this->idtype !== $this->aErrortypeRelatedByIdtype->getIdtype()) {
            $this->aErrortypeRelatedByIdtype = null;
        }
        if ($this->aErrortypeRelatedByIdtype !== null && $this->idtype !== $this->aErrortypeRelatedByIdtype->getIdtype()) {
            $this->aErrortypeRelatedByIdtype = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(VldPesertaDidikPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = VldPesertaDidikPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aPesertaDidikRelatedByPesertaDidikId = null;
            $this->aPesertaDidikRelatedByPesertaDidikId = null;
            $this->aErrortypeRelatedByIdtype = null;
            $this->aErrortypeRelatedByIdtype = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(VldPesertaDidikPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = VldPesertaDidikQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(VldPesertaDidikPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                VldPesertaDidikPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPesertaDidikRelatedByPesertaDidikId !== null) {
                if ($this->aPesertaDidikRelatedByPesertaDidikId->isModified() || $this->aPesertaDidikRelatedByPesertaDidikId->isNew()) {
                    $affectedRows += $this->aPesertaDidikRelatedByPesertaDidikId->save($con);
                }
                $this->setPesertaDidikRelatedByPesertaDidikId($this->aPesertaDidikRelatedByPesertaDidikId);
            }

            if ($this->aPesertaDidikRelatedByPesertaDidikId !== null) {
                if ($this->aPesertaDidikRelatedByPesertaDidikId->isModified() || $this->aPesertaDidikRelatedByPesertaDidikId->isNew()) {
                    $affectedRows += $this->aPesertaDidikRelatedByPesertaDidikId->save($con);
                }
                $this->setPesertaDidikRelatedByPesertaDidikId($this->aPesertaDidikRelatedByPesertaDidikId);
            }

            if ($this->aErrortypeRelatedByIdtype !== null) {
                if ($this->aErrortypeRelatedByIdtype->isModified() || $this->aErrortypeRelatedByIdtype->isNew()) {
                    $affectedRows += $this->aErrortypeRelatedByIdtype->save($con);
                }
                $this->setErrortypeRelatedByIdtype($this->aErrortypeRelatedByIdtype);
            }

            if ($this->aErrortypeRelatedByIdtype !== null) {
                if ($this->aErrortypeRelatedByIdtype->isModified() || $this->aErrortypeRelatedByIdtype->isNew()) {
                    $affectedRows += $this->aErrortypeRelatedByIdtype->save($con);
                }
                $this->setErrortypeRelatedByIdtype($this->aErrortypeRelatedByIdtype);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $criteria = $this->buildCriteria();
        $pk = BasePeer::doInsert($criteria, $con);
        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPesertaDidikRelatedByPesertaDidikId !== null) {
                if (!$this->aPesertaDidikRelatedByPesertaDidikId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPesertaDidikRelatedByPesertaDidikId->getValidationFailures());
                }
            }

            if ($this->aPesertaDidikRelatedByPesertaDidikId !== null) {
                if (!$this->aPesertaDidikRelatedByPesertaDidikId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPesertaDidikRelatedByPesertaDidikId->getValidationFailures());
                }
            }

            if ($this->aErrortypeRelatedByIdtype !== null) {
                if (!$this->aErrortypeRelatedByIdtype->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aErrortypeRelatedByIdtype->getValidationFailures());
                }
            }

            if ($this->aErrortypeRelatedByIdtype !== null) {
                if (!$this->aErrortypeRelatedByIdtype->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aErrortypeRelatedByIdtype->getValidationFailures());
                }
            }


            if (($retval = VldPesertaDidikPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = VldPesertaDidikPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getPesertaDidikId();
                break;
            case 1:
                return $this->getLogid();
                break;
            case 2:
                return $this->getIdtype();
                break;
            case 3:
                return $this->getStatusValidasi();
                break;
            case 4:
                return $this->getStatusVerifikasi();
                break;
            case 5:
                return $this->getFieldError();
                break;
            case 6:
                return $this->getErrorMessage();
                break;
            case 7:
                return $this->getCreateDate();
                break;
            case 8:
                return $this->getLastUpdate();
                break;
            case 9:
                return $this->getExpiredDate();
                break;
            case 10:
                return $this->getLastSync();
                break;
            case 11:
                return $this->getAppUsername();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['VldPesertaDidik'][serialize($this->getPrimaryKey())])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['VldPesertaDidik'][serialize($this->getPrimaryKey())] = true;
        $keys = VldPesertaDidikPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getPesertaDidikId(),
            $keys[1] => $this->getLogid(),
            $keys[2] => $this->getIdtype(),
            $keys[3] => $this->getStatusValidasi(),
            $keys[4] => $this->getStatusVerifikasi(),
            $keys[5] => $this->getFieldError(),
            $keys[6] => $this->getErrorMessage(),
            $keys[7] => $this->getCreateDate(),
            $keys[8] => $this->getLastUpdate(),
            $keys[9] => $this->getExpiredDate(),
            $keys[10] => $this->getLastSync(),
            $keys[11] => $this->getAppUsername(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aPesertaDidikRelatedByPesertaDidikId) {
                $result['PesertaDidikRelatedByPesertaDidikId'] = $this->aPesertaDidikRelatedByPesertaDidikId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPesertaDidikRelatedByPesertaDidikId) {
                $result['PesertaDidikRelatedByPesertaDidikId'] = $this->aPesertaDidikRelatedByPesertaDidikId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aErrortypeRelatedByIdtype) {
                $result['ErrortypeRelatedByIdtype'] = $this->aErrortypeRelatedByIdtype->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aErrortypeRelatedByIdtype) {
                $result['ErrortypeRelatedByIdtype'] = $this->aErrortypeRelatedByIdtype->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = VldPesertaDidikPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setPesertaDidikId($value);
                break;
            case 1:
                $this->setLogid($value);
                break;
            case 2:
                $this->setIdtype($value);
                break;
            case 3:
                $this->setStatusValidasi($value);
                break;
            case 4:
                $this->setStatusVerifikasi($value);
                break;
            case 5:
                $this->setFieldError($value);
                break;
            case 6:
                $this->setErrorMessage($value);
                break;
            case 7:
                $this->setCreateDate($value);
                break;
            case 8:
                $this->setLastUpdate($value);
                break;
            case 9:
                $this->setExpiredDate($value);
                break;
            case 10:
                $this->setLastSync($value);
                break;
            case 11:
                $this->setAppUsername($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = VldPesertaDidikPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setPesertaDidikId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setLogid($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setIdtype($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setStatusValidasi($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setStatusVerifikasi($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setFieldError($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setErrorMessage($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setCreateDate($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setLastUpdate($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setExpiredDate($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setLastSync($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setAppUsername($arr[$keys[11]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(VldPesertaDidikPeer::DATABASE_NAME);

        if ($this->isColumnModified(VldPesertaDidikPeer::PESERTA_DIDIK_ID)) $criteria->add(VldPesertaDidikPeer::PESERTA_DIDIK_ID, $this->peserta_didik_id);
        if ($this->isColumnModified(VldPesertaDidikPeer::LOGID)) $criteria->add(VldPesertaDidikPeer::LOGID, $this->logid);
        if ($this->isColumnModified(VldPesertaDidikPeer::IDTYPE)) $criteria->add(VldPesertaDidikPeer::IDTYPE, $this->idtype);
        if ($this->isColumnModified(VldPesertaDidikPeer::STATUS_VALIDASI)) $criteria->add(VldPesertaDidikPeer::STATUS_VALIDASI, $this->status_validasi);
        if ($this->isColumnModified(VldPesertaDidikPeer::STATUS_VERIFIKASI)) $criteria->add(VldPesertaDidikPeer::STATUS_VERIFIKASI, $this->status_verifikasi);
        if ($this->isColumnModified(VldPesertaDidikPeer::FIELD_ERROR)) $criteria->add(VldPesertaDidikPeer::FIELD_ERROR, $this->field_error);
        if ($this->isColumnModified(VldPesertaDidikPeer::ERROR_MESSAGE)) $criteria->add(VldPesertaDidikPeer::ERROR_MESSAGE, $this->error_message);
        if ($this->isColumnModified(VldPesertaDidikPeer::CREATE_DATE)) $criteria->add(VldPesertaDidikPeer::CREATE_DATE, $this->create_date);
        if ($this->isColumnModified(VldPesertaDidikPeer::LAST_UPDATE)) $criteria->add(VldPesertaDidikPeer::LAST_UPDATE, $this->last_update);
        if ($this->isColumnModified(VldPesertaDidikPeer::EXPIRED_DATE)) $criteria->add(VldPesertaDidikPeer::EXPIRED_DATE, $this->expired_date);
        if ($this->isColumnModified(VldPesertaDidikPeer::LAST_SYNC)) $criteria->add(VldPesertaDidikPeer::LAST_SYNC, $this->last_sync);
        if ($this->isColumnModified(VldPesertaDidikPeer::APP_USERNAME)) $criteria->add(VldPesertaDidikPeer::APP_USERNAME, $this->app_username);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(VldPesertaDidikPeer::DATABASE_NAME);
        $criteria->add(VldPesertaDidikPeer::PESERTA_DIDIK_ID, $this->peserta_didik_id);
        $criteria->add(VldPesertaDidikPeer::LOGID, $this->logid);

        return $criteria;
    }

    /**
     * Returns the composite primary key for this object.
     * The array elements will be in same order as specified in XML.
     * @return array
     */
    public function getPrimaryKey()
    {
        $pks = array();
        $pks[0] = $this->getPesertaDidikId();
        $pks[1] = $this->getLogid();

        return $pks;
    }

    /**
     * Set the [composite] primary key.
     *
     * @param array $keys The elements of the composite key (order must match the order in XML file).
     * @return void
     */
    public function setPrimaryKey($keys)
    {
        $this->setPesertaDidikId($keys[0]);
        $this->setLogid($keys[1]);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return (null === $this->getPesertaDidikId()) && (null === $this->getLogid());
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of VldPesertaDidik (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setPesertaDidikId($this->getPesertaDidikId());
        $copyObj->setLogid($this->getLogid());
        $copyObj->setIdtype($this->getIdtype());
        $copyObj->setStatusValidasi($this->getStatusValidasi());
        $copyObj->setStatusVerifikasi($this->getStatusVerifikasi());
        $copyObj->setFieldError($this->getFieldError());
        $copyObj->setErrorMessage($this->getErrorMessage());
        $copyObj->setCreateDate($this->getCreateDate());
        $copyObj->setLastUpdate($this->getLastUpdate());
        $copyObj->setExpiredDate($this->getExpiredDate());
        $copyObj->setLastSync($this->getLastSync());
        $copyObj->setAppUsername($this->getAppUsername());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return VldPesertaDidik Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return VldPesertaDidikPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new VldPesertaDidikPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a PesertaDidik object.
     *
     * @param             PesertaDidik $v
     * @return VldPesertaDidik The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPesertaDidikRelatedByPesertaDidikId(PesertaDidik $v = null)
    {
        if ($v === null) {
            $this->setPesertaDidikId(NULL);
        } else {
            $this->setPesertaDidikId($v->getPesertaDidikId());
        }

        $this->aPesertaDidikRelatedByPesertaDidikId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the PesertaDidik object, it will not be re-added.
        if ($v !== null) {
            $v->addVldPesertaDidikRelatedByPesertaDidikId($this);
        }


        return $this;
    }


    /**
     * Get the associated PesertaDidik object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return PesertaDidik The associated PesertaDidik object.
     * @throws PropelException
     */
    public function getPesertaDidikRelatedByPesertaDidikId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPesertaDidikRelatedByPesertaDidikId === null && (($this->peserta_didik_id !== "" && $this->peserta_didik_id !== null)) && $doQuery) {
            $this->aPesertaDidikRelatedByPesertaDidikId = PesertaDidikQuery::create()->findPk($this->peserta_didik_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPesertaDidikRelatedByPesertaDidikId->addVldPesertaDidiksRelatedByPesertaDidikId($this);
             */
        }

        return $this->aPesertaDidikRelatedByPesertaDidikId;
    }

    /**
     * Declares an association between this object and a PesertaDidik object.
     *
     * @param             PesertaDidik $v
     * @return VldPesertaDidik The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPesertaDidikRelatedByPesertaDidikId(PesertaDidik $v = null)
    {
        if ($v === null) {
            $this->setPesertaDidikId(NULL);
        } else {
            $this->setPesertaDidikId($v->getPesertaDidikId());
        }

        $this->aPesertaDidikRelatedByPesertaDidikId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the PesertaDidik object, it will not be re-added.
        if ($v !== null) {
            $v->addVldPesertaDidikRelatedByPesertaDidikId($this);
        }


        return $this;
    }


    /**
     * Get the associated PesertaDidik object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return PesertaDidik The associated PesertaDidik object.
     * @throws PropelException
     */
    public function getPesertaDidikRelatedByPesertaDidikId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPesertaDidikRelatedByPesertaDidikId === null && (($this->peserta_didik_id !== "" && $this->peserta_didik_id !== null)) && $doQuery) {
            $this->aPesertaDidikRelatedByPesertaDidikId = PesertaDidikQuery::create()->findPk($this->peserta_didik_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPesertaDidikRelatedByPesertaDidikId->addVldPesertaDidiksRelatedByPesertaDidikId($this);
             */
        }

        return $this->aPesertaDidikRelatedByPesertaDidikId;
    }

    /**
     * Declares an association between this object and a Errortype object.
     *
     * @param             Errortype $v
     * @return VldPesertaDidik The current object (for fluent API support)
     * @throws PropelException
     */
    public function setErrortypeRelatedByIdtype(Errortype $v = null)
    {
        if ($v === null) {
            $this->setIdtype(NULL);
        } else {
            $this->setIdtype($v->getIdtype());
        }

        $this->aErrortypeRelatedByIdtype = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Errortype object, it will not be re-added.
        if ($v !== null) {
            $v->addVldPesertaDidikRelatedByIdtype($this);
        }


        return $this;
    }


    /**
     * Get the associated Errortype object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Errortype The associated Errortype object.
     * @throws PropelException
     */
    public function getErrortypeRelatedByIdtype(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aErrortypeRelatedByIdtype === null && ($this->idtype !== null) && $doQuery) {
            $this->aErrortypeRelatedByIdtype = ErrortypeQuery::create()->findPk($this->idtype, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aErrortypeRelatedByIdtype->addVldPesertaDidiksRelatedByIdtype($this);
             */
        }

        return $this->aErrortypeRelatedByIdtype;
    }

    /**
     * Declares an association between this object and a Errortype object.
     *
     * @param             Errortype $v
     * @return VldPesertaDidik The current object (for fluent API support)
     * @throws PropelException
     */
    public function setErrortypeRelatedByIdtype(Errortype $v = null)
    {
        if ($v === null) {
            $this->setIdtype(NULL);
        } else {
            $this->setIdtype($v->getIdtype());
        }

        $this->aErrortypeRelatedByIdtype = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Errortype object, it will not be re-added.
        if ($v !== null) {
            $v->addVldPesertaDidikRelatedByIdtype($this);
        }


        return $this;
    }


    /**
     * Get the associated Errortype object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Errortype The associated Errortype object.
     * @throws PropelException
     */
    public function getErrortypeRelatedByIdtype(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aErrortypeRelatedByIdtype === null && ($this->idtype !== null) && $doQuery) {
            $this->aErrortypeRelatedByIdtype = ErrortypeQuery::create()->findPk($this->idtype, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aErrortypeRelatedByIdtype->addVldPesertaDidiksRelatedByIdtype($this);
             */
        }

        return $this->aErrortypeRelatedByIdtype;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->peserta_didik_id = null;
        $this->logid = null;
        $this->idtype = null;
        $this->status_validasi = null;
        $this->status_verifikasi = null;
        $this->field_error = null;
        $this->error_message = null;
        $this->create_date = null;
        $this->last_update = null;
        $this->expired_date = null;
        $this->last_sync = null;
        $this->app_username = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->aPesertaDidikRelatedByPesertaDidikId instanceof Persistent) {
              $this->aPesertaDidikRelatedByPesertaDidikId->clearAllReferences($deep);
            }
            if ($this->aPesertaDidikRelatedByPesertaDidikId instanceof Persistent) {
              $this->aPesertaDidikRelatedByPesertaDidikId->clearAllReferences($deep);
            }
            if ($this->aErrortypeRelatedByIdtype instanceof Persistent) {
              $this->aErrortypeRelatedByIdtype->clearAllReferences($deep);
            }
            if ($this->aErrortypeRelatedByIdtype instanceof Persistent) {
              $this->aErrortypeRelatedByIdtype->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        $this->aPesertaDidikRelatedByPesertaDidikId = null;
        $this->aPesertaDidikRelatedByPesertaDidikId = null;
        $this->aErrortypeRelatedByIdtype = null;
        $this->aErrortypeRelatedByIdtype = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(VldPesertaDidikPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
