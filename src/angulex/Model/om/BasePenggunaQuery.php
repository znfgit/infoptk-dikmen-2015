<?php

namespace angulex\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\LembagaNonSekolah;
use angulex\Model\LogPengguna;
use angulex\Model\MstWilayah;
use angulex\Model\Pengguna;
use angulex\Model\PenggunaPeer;
use angulex\Model\PenggunaQuery;
use angulex\Model\Peran;
use angulex\Model\SasaranSurvey;
use angulex\Model\Sekolah;

/**
 * Base class that represents a query for the 'pengguna' table.
 *
 * 
 *
 * @method PenggunaQuery orderByPenggunaId($order = Criteria::ASC) Order by the pengguna_id column
 * @method PenggunaQuery orderBySekolahId($order = Criteria::ASC) Order by the sekolah_id column
 * @method PenggunaQuery orderByLembagaId($order = Criteria::ASC) Order by the lembaga_id column
 * @method PenggunaQuery orderByYayasanId($order = Criteria::ASC) Order by the yayasan_id column
 * @method PenggunaQuery orderByLaId($order = Criteria::ASC) Order by the la_id column
 * @method PenggunaQuery orderByPeranId($order = Criteria::ASC) Order by the peran_id column
 * @method PenggunaQuery orderByUsername($order = Criteria::ASC) Order by the username column
 * @method PenggunaQuery orderByPassword($order = Criteria::ASC) Order by the password column
 * @method PenggunaQuery orderByNama($order = Criteria::ASC) Order by the nama column
 * @method PenggunaQuery orderByNipNim($order = Criteria::ASC) Order by the nip_nim column
 * @method PenggunaQuery orderByJabatanLembaga($order = Criteria::ASC) Order by the jabatan_lembaga column
 * @method PenggunaQuery orderByYm($order = Criteria::ASC) Order by the ym column
 * @method PenggunaQuery orderBySkype($order = Criteria::ASC) Order by the skype column
 * @method PenggunaQuery orderByAlamat($order = Criteria::ASC) Order by the alamat column
 * @method PenggunaQuery orderByKodeWilayah($order = Criteria::ASC) Order by the kode_wilayah column
 * @method PenggunaQuery orderByNoTelepon($order = Criteria::ASC) Order by the no_telepon column
 * @method PenggunaQuery orderByNoHp($order = Criteria::ASC) Order by the no_hp column
 * @method PenggunaQuery orderByAktif($order = Criteria::ASC) Order by the aktif column
 * @method PenggunaQuery orderByLastUpdate($order = Criteria::ASC) Order by the Last_update column
 * @method PenggunaQuery orderBySoftDelete($order = Criteria::ASC) Order by the Soft_delete column
 * @method PenggunaQuery orderByLastSync($order = Criteria::ASC) Order by the last_sync column
 * @method PenggunaQuery orderByUpdaterId($order = Criteria::ASC) Order by the Updater_ID column
 *
 * @method PenggunaQuery groupByPenggunaId() Group by the pengguna_id column
 * @method PenggunaQuery groupBySekolahId() Group by the sekolah_id column
 * @method PenggunaQuery groupByLembagaId() Group by the lembaga_id column
 * @method PenggunaQuery groupByYayasanId() Group by the yayasan_id column
 * @method PenggunaQuery groupByLaId() Group by the la_id column
 * @method PenggunaQuery groupByPeranId() Group by the peran_id column
 * @method PenggunaQuery groupByUsername() Group by the username column
 * @method PenggunaQuery groupByPassword() Group by the password column
 * @method PenggunaQuery groupByNama() Group by the nama column
 * @method PenggunaQuery groupByNipNim() Group by the nip_nim column
 * @method PenggunaQuery groupByJabatanLembaga() Group by the jabatan_lembaga column
 * @method PenggunaQuery groupByYm() Group by the ym column
 * @method PenggunaQuery groupBySkype() Group by the skype column
 * @method PenggunaQuery groupByAlamat() Group by the alamat column
 * @method PenggunaQuery groupByKodeWilayah() Group by the kode_wilayah column
 * @method PenggunaQuery groupByNoTelepon() Group by the no_telepon column
 * @method PenggunaQuery groupByNoHp() Group by the no_hp column
 * @method PenggunaQuery groupByAktif() Group by the aktif column
 * @method PenggunaQuery groupByLastUpdate() Group by the Last_update column
 * @method PenggunaQuery groupBySoftDelete() Group by the Soft_delete column
 * @method PenggunaQuery groupByLastSync() Group by the last_sync column
 * @method PenggunaQuery groupByUpdaterId() Group by the Updater_ID column
 *
 * @method PenggunaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method PenggunaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method PenggunaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method PenggunaQuery leftJoinLembagaNonSekolahRelatedByLembagaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the LembagaNonSekolahRelatedByLembagaId relation
 * @method PenggunaQuery rightJoinLembagaNonSekolahRelatedByLembagaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the LembagaNonSekolahRelatedByLembagaId relation
 * @method PenggunaQuery innerJoinLembagaNonSekolahRelatedByLembagaId($relationAlias = null) Adds a INNER JOIN clause to the query using the LembagaNonSekolahRelatedByLembagaId relation
 *
 * @method PenggunaQuery leftJoinLembagaNonSekolahRelatedByLembagaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the LembagaNonSekolahRelatedByLembagaId relation
 * @method PenggunaQuery rightJoinLembagaNonSekolahRelatedByLembagaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the LembagaNonSekolahRelatedByLembagaId relation
 * @method PenggunaQuery innerJoinLembagaNonSekolahRelatedByLembagaId($relationAlias = null) Adds a INNER JOIN clause to the query using the LembagaNonSekolahRelatedByLembagaId relation
 *
 * @method PenggunaQuery leftJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method PenggunaQuery rightJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method PenggunaQuery innerJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the SekolahRelatedBySekolahId relation
 *
 * @method PenggunaQuery leftJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method PenggunaQuery rightJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method PenggunaQuery innerJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the SekolahRelatedBySekolahId relation
 *
 * @method PenggunaQuery leftJoinMstWilayahRelatedByKodeWilayah($relationAlias = null) Adds a LEFT JOIN clause to the query using the MstWilayahRelatedByKodeWilayah relation
 * @method PenggunaQuery rightJoinMstWilayahRelatedByKodeWilayah($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MstWilayahRelatedByKodeWilayah relation
 * @method PenggunaQuery innerJoinMstWilayahRelatedByKodeWilayah($relationAlias = null) Adds a INNER JOIN clause to the query using the MstWilayahRelatedByKodeWilayah relation
 *
 * @method PenggunaQuery leftJoinMstWilayahRelatedByKodeWilayah($relationAlias = null) Adds a LEFT JOIN clause to the query using the MstWilayahRelatedByKodeWilayah relation
 * @method PenggunaQuery rightJoinMstWilayahRelatedByKodeWilayah($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MstWilayahRelatedByKodeWilayah relation
 * @method PenggunaQuery innerJoinMstWilayahRelatedByKodeWilayah($relationAlias = null) Adds a INNER JOIN clause to the query using the MstWilayahRelatedByKodeWilayah relation
 *
 * @method PenggunaQuery leftJoinPeranRelatedByPeranId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PeranRelatedByPeranId relation
 * @method PenggunaQuery rightJoinPeranRelatedByPeranId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PeranRelatedByPeranId relation
 * @method PenggunaQuery innerJoinPeranRelatedByPeranId($relationAlias = null) Adds a INNER JOIN clause to the query using the PeranRelatedByPeranId relation
 *
 * @method PenggunaQuery leftJoinPeranRelatedByPeranId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PeranRelatedByPeranId relation
 * @method PenggunaQuery rightJoinPeranRelatedByPeranId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PeranRelatedByPeranId relation
 * @method PenggunaQuery innerJoinPeranRelatedByPeranId($relationAlias = null) Adds a INNER JOIN clause to the query using the PeranRelatedByPeranId relation
 *
 * @method PenggunaQuery leftJoinLogPenggunaRelatedByPenggunaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the LogPenggunaRelatedByPenggunaId relation
 * @method PenggunaQuery rightJoinLogPenggunaRelatedByPenggunaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the LogPenggunaRelatedByPenggunaId relation
 * @method PenggunaQuery innerJoinLogPenggunaRelatedByPenggunaId($relationAlias = null) Adds a INNER JOIN clause to the query using the LogPenggunaRelatedByPenggunaId relation
 *
 * @method PenggunaQuery leftJoinLogPenggunaRelatedByPenggunaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the LogPenggunaRelatedByPenggunaId relation
 * @method PenggunaQuery rightJoinLogPenggunaRelatedByPenggunaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the LogPenggunaRelatedByPenggunaId relation
 * @method PenggunaQuery innerJoinLogPenggunaRelatedByPenggunaId($relationAlias = null) Adds a INNER JOIN clause to the query using the LogPenggunaRelatedByPenggunaId relation
 *
 * @method PenggunaQuery leftJoinSasaranSurveyRelatedByPenggunaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SasaranSurveyRelatedByPenggunaId relation
 * @method PenggunaQuery rightJoinSasaranSurveyRelatedByPenggunaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SasaranSurveyRelatedByPenggunaId relation
 * @method PenggunaQuery innerJoinSasaranSurveyRelatedByPenggunaId($relationAlias = null) Adds a INNER JOIN clause to the query using the SasaranSurveyRelatedByPenggunaId relation
 *
 * @method PenggunaQuery leftJoinSasaranSurveyRelatedByPenggunaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SasaranSurveyRelatedByPenggunaId relation
 * @method PenggunaQuery rightJoinSasaranSurveyRelatedByPenggunaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SasaranSurveyRelatedByPenggunaId relation
 * @method PenggunaQuery innerJoinSasaranSurveyRelatedByPenggunaId($relationAlias = null) Adds a INNER JOIN clause to the query using the SasaranSurveyRelatedByPenggunaId relation
 *
 * @method PenggunaQuery leftJoinLembagaNonSekolahRelatedByPenggunaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the LembagaNonSekolahRelatedByPenggunaId relation
 * @method PenggunaQuery rightJoinLembagaNonSekolahRelatedByPenggunaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the LembagaNonSekolahRelatedByPenggunaId relation
 * @method PenggunaQuery innerJoinLembagaNonSekolahRelatedByPenggunaId($relationAlias = null) Adds a INNER JOIN clause to the query using the LembagaNonSekolahRelatedByPenggunaId relation
 *
 * @method PenggunaQuery leftJoinLembagaNonSekolahRelatedByPenggunaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the LembagaNonSekolahRelatedByPenggunaId relation
 * @method PenggunaQuery rightJoinLembagaNonSekolahRelatedByPenggunaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the LembagaNonSekolahRelatedByPenggunaId relation
 * @method PenggunaQuery innerJoinLembagaNonSekolahRelatedByPenggunaId($relationAlias = null) Adds a INNER JOIN clause to the query using the LembagaNonSekolahRelatedByPenggunaId relation
 *
 * @method Pengguna findOne(PropelPDO $con = null) Return the first Pengguna matching the query
 * @method Pengguna findOneOrCreate(PropelPDO $con = null) Return the first Pengguna matching the query, or a new Pengguna object populated from the query conditions when no match is found
 *
 * @method Pengguna findOneBySekolahId(string $sekolah_id) Return the first Pengguna filtered by the sekolah_id column
 * @method Pengguna findOneByLembagaId(string $lembaga_id) Return the first Pengguna filtered by the lembaga_id column
 * @method Pengguna findOneByYayasanId(string $yayasan_id) Return the first Pengguna filtered by the yayasan_id column
 * @method Pengguna findOneByLaId(string $la_id) Return the first Pengguna filtered by the la_id column
 * @method Pengguna findOneByPeranId(int $peran_id) Return the first Pengguna filtered by the peran_id column
 * @method Pengguna findOneByUsername(string $username) Return the first Pengguna filtered by the username column
 * @method Pengguna findOneByPassword(string $password) Return the first Pengguna filtered by the password column
 * @method Pengguna findOneByNama(string $nama) Return the first Pengguna filtered by the nama column
 * @method Pengguna findOneByNipNim(string $nip_nim) Return the first Pengguna filtered by the nip_nim column
 * @method Pengguna findOneByJabatanLembaga(string $jabatan_lembaga) Return the first Pengguna filtered by the jabatan_lembaga column
 * @method Pengguna findOneByYm(string $ym) Return the first Pengguna filtered by the ym column
 * @method Pengguna findOneBySkype(string $skype) Return the first Pengguna filtered by the skype column
 * @method Pengguna findOneByAlamat(string $alamat) Return the first Pengguna filtered by the alamat column
 * @method Pengguna findOneByKodeWilayah(string $kode_wilayah) Return the first Pengguna filtered by the kode_wilayah column
 * @method Pengguna findOneByNoTelepon(string $no_telepon) Return the first Pengguna filtered by the no_telepon column
 * @method Pengguna findOneByNoHp(string $no_hp) Return the first Pengguna filtered by the no_hp column
 * @method Pengguna findOneByAktif(string $aktif) Return the first Pengguna filtered by the aktif column
 * @method Pengguna findOneByLastUpdate(string $Last_update) Return the first Pengguna filtered by the Last_update column
 * @method Pengguna findOneBySoftDelete(string $Soft_delete) Return the first Pengguna filtered by the Soft_delete column
 * @method Pengguna findOneByLastSync(string $last_sync) Return the first Pengguna filtered by the last_sync column
 * @method Pengguna findOneByUpdaterId(string $Updater_ID) Return the first Pengguna filtered by the Updater_ID column
 *
 * @method array findByPenggunaId(string $pengguna_id) Return Pengguna objects filtered by the pengguna_id column
 * @method array findBySekolahId(string $sekolah_id) Return Pengguna objects filtered by the sekolah_id column
 * @method array findByLembagaId(string $lembaga_id) Return Pengguna objects filtered by the lembaga_id column
 * @method array findByYayasanId(string $yayasan_id) Return Pengguna objects filtered by the yayasan_id column
 * @method array findByLaId(string $la_id) Return Pengguna objects filtered by the la_id column
 * @method array findByPeranId(int $peran_id) Return Pengguna objects filtered by the peran_id column
 * @method array findByUsername(string $username) Return Pengguna objects filtered by the username column
 * @method array findByPassword(string $password) Return Pengguna objects filtered by the password column
 * @method array findByNama(string $nama) Return Pengguna objects filtered by the nama column
 * @method array findByNipNim(string $nip_nim) Return Pengguna objects filtered by the nip_nim column
 * @method array findByJabatanLembaga(string $jabatan_lembaga) Return Pengguna objects filtered by the jabatan_lembaga column
 * @method array findByYm(string $ym) Return Pengguna objects filtered by the ym column
 * @method array findBySkype(string $skype) Return Pengguna objects filtered by the skype column
 * @method array findByAlamat(string $alamat) Return Pengguna objects filtered by the alamat column
 * @method array findByKodeWilayah(string $kode_wilayah) Return Pengguna objects filtered by the kode_wilayah column
 * @method array findByNoTelepon(string $no_telepon) Return Pengguna objects filtered by the no_telepon column
 * @method array findByNoHp(string $no_hp) Return Pengguna objects filtered by the no_hp column
 * @method array findByAktif(string $aktif) Return Pengguna objects filtered by the aktif column
 * @method array findByLastUpdate(string $Last_update) Return Pengguna objects filtered by the Last_update column
 * @method array findBySoftDelete(string $Soft_delete) Return Pengguna objects filtered by the Soft_delete column
 * @method array findByLastSync(string $last_sync) Return Pengguna objects filtered by the last_sync column
 * @method array findByUpdaterId(string $Updater_ID) Return Pengguna objects filtered by the Updater_ID column
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BasePenggunaQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BasePenggunaQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Dapodikmen', $modelName = 'angulex\\Model\\Pengguna', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new PenggunaQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   PenggunaQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return PenggunaQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof PenggunaQuery) {
            return $criteria;
        }
        $query = new PenggunaQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Pengguna|Pengguna[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = PenggunaPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(PenggunaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Pengguna A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByPenggunaId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Pengguna A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [pengguna_id], [sekolah_id], [lembaga_id], [yayasan_id], [la_id], [peran_id], [username], [password], [nama], [nip_nim], [jabatan_lembaga], [ym], [skype], [alamat], [kode_wilayah], [no_telepon], [no_hp], [aktif], [Last_update], [Soft_delete], [last_sync], [Updater_ID] FROM [pengguna] WHERE [pengguna_id] = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Pengguna();
            $obj->hydrate($row);
            PenggunaPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Pengguna|Pengguna[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Pengguna[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PenggunaPeer::PENGGUNA_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PenggunaPeer::PENGGUNA_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the pengguna_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPenggunaId('fooValue');   // WHERE pengguna_id = 'fooValue'
     * $query->filterByPenggunaId('%fooValue%'); // WHERE pengguna_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $penggunaId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function filterByPenggunaId($penggunaId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($penggunaId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $penggunaId)) {
                $penggunaId = str_replace('*', '%', $penggunaId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PenggunaPeer::PENGGUNA_ID, $penggunaId, $comparison);
    }

    /**
     * Filter the query on the sekolah_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySekolahId('fooValue');   // WHERE sekolah_id = 'fooValue'
     * $query->filterBySekolahId('%fooValue%'); // WHERE sekolah_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sekolahId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function filterBySekolahId($sekolahId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sekolahId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $sekolahId)) {
                $sekolahId = str_replace('*', '%', $sekolahId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PenggunaPeer::SEKOLAH_ID, $sekolahId, $comparison);
    }

    /**
     * Filter the query on the lembaga_id column
     *
     * Example usage:
     * <code>
     * $query->filterByLembagaId('fooValue');   // WHERE lembaga_id = 'fooValue'
     * $query->filterByLembagaId('%fooValue%'); // WHERE lembaga_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $lembagaId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function filterByLembagaId($lembagaId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($lembagaId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $lembagaId)) {
                $lembagaId = str_replace('*', '%', $lembagaId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PenggunaPeer::LEMBAGA_ID, $lembagaId, $comparison);
    }

    /**
     * Filter the query on the yayasan_id column
     *
     * Example usage:
     * <code>
     * $query->filterByYayasanId('fooValue');   // WHERE yayasan_id = 'fooValue'
     * $query->filterByYayasanId('%fooValue%'); // WHERE yayasan_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $yayasanId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function filterByYayasanId($yayasanId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($yayasanId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $yayasanId)) {
                $yayasanId = str_replace('*', '%', $yayasanId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PenggunaPeer::YAYASAN_ID, $yayasanId, $comparison);
    }

    /**
     * Filter the query on the la_id column
     *
     * Example usage:
     * <code>
     * $query->filterByLaId('fooValue');   // WHERE la_id = 'fooValue'
     * $query->filterByLaId('%fooValue%'); // WHERE la_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $laId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function filterByLaId($laId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($laId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $laId)) {
                $laId = str_replace('*', '%', $laId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PenggunaPeer::LA_ID, $laId, $comparison);
    }

    /**
     * Filter the query on the peran_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPeranId(1234); // WHERE peran_id = 1234
     * $query->filterByPeranId(array(12, 34)); // WHERE peran_id IN (12, 34)
     * $query->filterByPeranId(array('min' => 12)); // WHERE peran_id >= 12
     * $query->filterByPeranId(array('max' => 12)); // WHERE peran_id <= 12
     * </code>
     *
     * @see       filterByPeranRelatedByPeranId()
     *
     * @see       filterByPeranRelatedByPeranId()
     *
     * @param     mixed $peranId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function filterByPeranId($peranId = null, $comparison = null)
    {
        if (is_array($peranId)) {
            $useMinMax = false;
            if (isset($peranId['min'])) {
                $this->addUsingAlias(PenggunaPeer::PERAN_ID, $peranId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($peranId['max'])) {
                $this->addUsingAlias(PenggunaPeer::PERAN_ID, $peranId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PenggunaPeer::PERAN_ID, $peranId, $comparison);
    }

    /**
     * Filter the query on the username column
     *
     * Example usage:
     * <code>
     * $query->filterByUsername('fooValue');   // WHERE username = 'fooValue'
     * $query->filterByUsername('%fooValue%'); // WHERE username LIKE '%fooValue%'
     * </code>
     *
     * @param     string $username The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function filterByUsername($username = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($username)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $username)) {
                $username = str_replace('*', '%', $username);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PenggunaPeer::USERNAME, $username, $comparison);
    }

    /**
     * Filter the query on the password column
     *
     * Example usage:
     * <code>
     * $query->filterByPassword('fooValue');   // WHERE password = 'fooValue'
     * $query->filterByPassword('%fooValue%'); // WHERE password LIKE '%fooValue%'
     * </code>
     *
     * @param     string $password The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function filterByPassword($password = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($password)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $password)) {
                $password = str_replace('*', '%', $password);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PenggunaPeer::PASSWORD, $password, $comparison);
    }

    /**
     * Filter the query on the nama column
     *
     * Example usage:
     * <code>
     * $query->filterByNama('fooValue');   // WHERE nama = 'fooValue'
     * $query->filterByNama('%fooValue%'); // WHERE nama LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nama The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function filterByNama($nama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nama)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nama)) {
                $nama = str_replace('*', '%', $nama);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PenggunaPeer::NAMA, $nama, $comparison);
    }

    /**
     * Filter the query on the nip_nim column
     *
     * Example usage:
     * <code>
     * $query->filterByNipNim('fooValue');   // WHERE nip_nim = 'fooValue'
     * $query->filterByNipNim('%fooValue%'); // WHERE nip_nim LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nipNim The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function filterByNipNim($nipNim = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nipNim)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nipNim)) {
                $nipNim = str_replace('*', '%', $nipNim);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PenggunaPeer::NIP_NIM, $nipNim, $comparison);
    }

    /**
     * Filter the query on the jabatan_lembaga column
     *
     * Example usage:
     * <code>
     * $query->filterByJabatanLembaga('fooValue');   // WHERE jabatan_lembaga = 'fooValue'
     * $query->filterByJabatanLembaga('%fooValue%'); // WHERE jabatan_lembaga LIKE '%fooValue%'
     * </code>
     *
     * @param     string $jabatanLembaga The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function filterByJabatanLembaga($jabatanLembaga = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($jabatanLembaga)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $jabatanLembaga)) {
                $jabatanLembaga = str_replace('*', '%', $jabatanLembaga);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PenggunaPeer::JABATAN_LEMBAGA, $jabatanLembaga, $comparison);
    }

    /**
     * Filter the query on the ym column
     *
     * Example usage:
     * <code>
     * $query->filterByYm('fooValue');   // WHERE ym = 'fooValue'
     * $query->filterByYm('%fooValue%'); // WHERE ym LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ym The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function filterByYm($ym = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ym)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ym)) {
                $ym = str_replace('*', '%', $ym);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PenggunaPeer::YM, $ym, $comparison);
    }

    /**
     * Filter the query on the skype column
     *
     * Example usage:
     * <code>
     * $query->filterBySkype('fooValue');   // WHERE skype = 'fooValue'
     * $query->filterBySkype('%fooValue%'); // WHERE skype LIKE '%fooValue%'
     * </code>
     *
     * @param     string $skype The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function filterBySkype($skype = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($skype)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $skype)) {
                $skype = str_replace('*', '%', $skype);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PenggunaPeer::SKYPE, $skype, $comparison);
    }

    /**
     * Filter the query on the alamat column
     *
     * Example usage:
     * <code>
     * $query->filterByAlamat('fooValue');   // WHERE alamat = 'fooValue'
     * $query->filterByAlamat('%fooValue%'); // WHERE alamat LIKE '%fooValue%'
     * </code>
     *
     * @param     string $alamat The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function filterByAlamat($alamat = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($alamat)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $alamat)) {
                $alamat = str_replace('*', '%', $alamat);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PenggunaPeer::ALAMAT, $alamat, $comparison);
    }

    /**
     * Filter the query on the kode_wilayah column
     *
     * Example usage:
     * <code>
     * $query->filterByKodeWilayah('fooValue');   // WHERE kode_wilayah = 'fooValue'
     * $query->filterByKodeWilayah('%fooValue%'); // WHERE kode_wilayah LIKE '%fooValue%'
     * </code>
     *
     * @param     string $kodeWilayah The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function filterByKodeWilayah($kodeWilayah = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($kodeWilayah)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $kodeWilayah)) {
                $kodeWilayah = str_replace('*', '%', $kodeWilayah);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PenggunaPeer::KODE_WILAYAH, $kodeWilayah, $comparison);
    }

    /**
     * Filter the query on the no_telepon column
     *
     * Example usage:
     * <code>
     * $query->filterByNoTelepon('fooValue');   // WHERE no_telepon = 'fooValue'
     * $query->filterByNoTelepon('%fooValue%'); // WHERE no_telepon LIKE '%fooValue%'
     * </code>
     *
     * @param     string $noTelepon The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function filterByNoTelepon($noTelepon = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($noTelepon)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $noTelepon)) {
                $noTelepon = str_replace('*', '%', $noTelepon);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PenggunaPeer::NO_TELEPON, $noTelepon, $comparison);
    }

    /**
     * Filter the query on the no_hp column
     *
     * Example usage:
     * <code>
     * $query->filterByNoHp('fooValue');   // WHERE no_hp = 'fooValue'
     * $query->filterByNoHp('%fooValue%'); // WHERE no_hp LIKE '%fooValue%'
     * </code>
     *
     * @param     string $noHp The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function filterByNoHp($noHp = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($noHp)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $noHp)) {
                $noHp = str_replace('*', '%', $noHp);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PenggunaPeer::NO_HP, $noHp, $comparison);
    }

    /**
     * Filter the query on the aktif column
     *
     * Example usage:
     * <code>
     * $query->filterByAktif(1234); // WHERE aktif = 1234
     * $query->filterByAktif(array(12, 34)); // WHERE aktif IN (12, 34)
     * $query->filterByAktif(array('min' => 12)); // WHERE aktif >= 12
     * $query->filterByAktif(array('max' => 12)); // WHERE aktif <= 12
     * </code>
     *
     * @param     mixed $aktif The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function filterByAktif($aktif = null, $comparison = null)
    {
        if (is_array($aktif)) {
            $useMinMax = false;
            if (isset($aktif['min'])) {
                $this->addUsingAlias(PenggunaPeer::AKTIF, $aktif['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($aktif['max'])) {
                $this->addUsingAlias(PenggunaPeer::AKTIF, $aktif['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PenggunaPeer::AKTIF, $aktif, $comparison);
    }

    /**
     * Filter the query on the Last_update column
     *
     * Example usage:
     * <code>
     * $query->filterByLastUpdate('2011-03-14'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate('now'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate(array('max' => 'yesterday')); // WHERE Last_update > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastUpdate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function filterByLastUpdate($lastUpdate = null, $comparison = null)
    {
        if (is_array($lastUpdate)) {
            $useMinMax = false;
            if (isset($lastUpdate['min'])) {
                $this->addUsingAlias(PenggunaPeer::LAST_UPDATE, $lastUpdate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastUpdate['max'])) {
                $this->addUsingAlias(PenggunaPeer::LAST_UPDATE, $lastUpdate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PenggunaPeer::LAST_UPDATE, $lastUpdate, $comparison);
    }

    /**
     * Filter the query on the Soft_delete column
     *
     * Example usage:
     * <code>
     * $query->filterBySoftDelete(1234); // WHERE Soft_delete = 1234
     * $query->filterBySoftDelete(array(12, 34)); // WHERE Soft_delete IN (12, 34)
     * $query->filterBySoftDelete(array('min' => 12)); // WHERE Soft_delete >= 12
     * $query->filterBySoftDelete(array('max' => 12)); // WHERE Soft_delete <= 12
     * </code>
     *
     * @param     mixed $softDelete The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function filterBySoftDelete($softDelete = null, $comparison = null)
    {
        if (is_array($softDelete)) {
            $useMinMax = false;
            if (isset($softDelete['min'])) {
                $this->addUsingAlias(PenggunaPeer::SOFT_DELETE, $softDelete['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($softDelete['max'])) {
                $this->addUsingAlias(PenggunaPeer::SOFT_DELETE, $softDelete['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PenggunaPeer::SOFT_DELETE, $softDelete, $comparison);
    }

    /**
     * Filter the query on the last_sync column
     *
     * Example usage:
     * <code>
     * $query->filterByLastSync('2011-03-14'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync('now'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync(array('max' => 'yesterday')); // WHERE last_sync > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastSync The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function filterByLastSync($lastSync = null, $comparison = null)
    {
        if (is_array($lastSync)) {
            $useMinMax = false;
            if (isset($lastSync['min'])) {
                $this->addUsingAlias(PenggunaPeer::LAST_SYNC, $lastSync['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastSync['max'])) {
                $this->addUsingAlias(PenggunaPeer::LAST_SYNC, $lastSync['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PenggunaPeer::LAST_SYNC, $lastSync, $comparison);
    }

    /**
     * Filter the query on the Updater_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdaterId('fooValue');   // WHERE Updater_ID = 'fooValue'
     * $query->filterByUpdaterId('%fooValue%'); // WHERE Updater_ID LIKE '%fooValue%'
     * </code>
     *
     * @param     string $updaterId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function filterByUpdaterId($updaterId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($updaterId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $updaterId)) {
                $updaterId = str_replace('*', '%', $updaterId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PenggunaPeer::UPDATER_ID, $updaterId, $comparison);
    }

    /**
     * Filter the query by a related LembagaNonSekolah object
     *
     * @param   LembagaNonSekolah|PropelObjectCollection $lembagaNonSekolah The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PenggunaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByLembagaNonSekolahRelatedByLembagaId($lembagaNonSekolah, $comparison = null)
    {
        if ($lembagaNonSekolah instanceof LembagaNonSekolah) {
            return $this
                ->addUsingAlias(PenggunaPeer::LEMBAGA_ID, $lembagaNonSekolah->getLembagaId(), $comparison);
        } elseif ($lembagaNonSekolah instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PenggunaPeer::LEMBAGA_ID, $lembagaNonSekolah->toKeyValue('PrimaryKey', 'LembagaId'), $comparison);
        } else {
            throw new PropelException('filterByLembagaNonSekolahRelatedByLembagaId() only accepts arguments of type LembagaNonSekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the LembagaNonSekolahRelatedByLembagaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function joinLembagaNonSekolahRelatedByLembagaId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('LembagaNonSekolahRelatedByLembagaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'LembagaNonSekolahRelatedByLembagaId');
        }

        return $this;
    }

    /**
     * Use the LembagaNonSekolahRelatedByLembagaId relation LembagaNonSekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\LembagaNonSekolahQuery A secondary query class using the current class as primary query
     */
    public function useLembagaNonSekolahRelatedByLembagaIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinLembagaNonSekolahRelatedByLembagaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'LembagaNonSekolahRelatedByLembagaId', '\angulex\Model\LembagaNonSekolahQuery');
    }

    /**
     * Filter the query by a related LembagaNonSekolah object
     *
     * @param   LembagaNonSekolah|PropelObjectCollection $lembagaNonSekolah The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PenggunaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByLembagaNonSekolahRelatedByLembagaId($lembagaNonSekolah, $comparison = null)
    {
        if ($lembagaNonSekolah instanceof LembagaNonSekolah) {
            return $this
                ->addUsingAlias(PenggunaPeer::LEMBAGA_ID, $lembagaNonSekolah->getLembagaId(), $comparison);
        } elseif ($lembagaNonSekolah instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PenggunaPeer::LEMBAGA_ID, $lembagaNonSekolah->toKeyValue('PrimaryKey', 'LembagaId'), $comparison);
        } else {
            throw new PropelException('filterByLembagaNonSekolahRelatedByLembagaId() only accepts arguments of type LembagaNonSekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the LembagaNonSekolahRelatedByLembagaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function joinLembagaNonSekolahRelatedByLembagaId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('LembagaNonSekolahRelatedByLembagaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'LembagaNonSekolahRelatedByLembagaId');
        }

        return $this;
    }

    /**
     * Use the LembagaNonSekolahRelatedByLembagaId relation LembagaNonSekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\LembagaNonSekolahQuery A secondary query class using the current class as primary query
     */
    public function useLembagaNonSekolahRelatedByLembagaIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinLembagaNonSekolahRelatedByLembagaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'LembagaNonSekolahRelatedByLembagaId', '\angulex\Model\LembagaNonSekolahQuery');
    }

    /**
     * Filter the query by a related Sekolah object
     *
     * @param   Sekolah|PropelObjectCollection $sekolah The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PenggunaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySekolahRelatedBySekolahId($sekolah, $comparison = null)
    {
        if ($sekolah instanceof Sekolah) {
            return $this
                ->addUsingAlias(PenggunaPeer::SEKOLAH_ID, $sekolah->getSekolahId(), $comparison);
        } elseif ($sekolah instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PenggunaPeer::SEKOLAH_ID, $sekolah->toKeyValue('PrimaryKey', 'SekolahId'), $comparison);
        } else {
            throw new PropelException('filterBySekolahRelatedBySekolahId() only accepts arguments of type Sekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SekolahRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function joinSekolahRelatedBySekolahId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SekolahRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SekolahRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the SekolahRelatedBySekolahId relation Sekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SekolahQuery A secondary query class using the current class as primary query
     */
    public function useSekolahRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSekolahRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SekolahRelatedBySekolahId', '\angulex\Model\SekolahQuery');
    }

    /**
     * Filter the query by a related Sekolah object
     *
     * @param   Sekolah|PropelObjectCollection $sekolah The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PenggunaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySekolahRelatedBySekolahId($sekolah, $comparison = null)
    {
        if ($sekolah instanceof Sekolah) {
            return $this
                ->addUsingAlias(PenggunaPeer::SEKOLAH_ID, $sekolah->getSekolahId(), $comparison);
        } elseif ($sekolah instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PenggunaPeer::SEKOLAH_ID, $sekolah->toKeyValue('PrimaryKey', 'SekolahId'), $comparison);
        } else {
            throw new PropelException('filterBySekolahRelatedBySekolahId() only accepts arguments of type Sekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SekolahRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function joinSekolahRelatedBySekolahId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SekolahRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SekolahRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the SekolahRelatedBySekolahId relation Sekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SekolahQuery A secondary query class using the current class as primary query
     */
    public function useSekolahRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSekolahRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SekolahRelatedBySekolahId', '\angulex\Model\SekolahQuery');
    }

    /**
     * Filter the query by a related MstWilayah object
     *
     * @param   MstWilayah|PropelObjectCollection $mstWilayah The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PenggunaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByMstWilayahRelatedByKodeWilayah($mstWilayah, $comparison = null)
    {
        if ($mstWilayah instanceof MstWilayah) {
            return $this
                ->addUsingAlias(PenggunaPeer::KODE_WILAYAH, $mstWilayah->getKodeWilayah(), $comparison);
        } elseif ($mstWilayah instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PenggunaPeer::KODE_WILAYAH, $mstWilayah->toKeyValue('PrimaryKey', 'KodeWilayah'), $comparison);
        } else {
            throw new PropelException('filterByMstWilayahRelatedByKodeWilayah() only accepts arguments of type MstWilayah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MstWilayahRelatedByKodeWilayah relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function joinMstWilayahRelatedByKodeWilayah($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MstWilayahRelatedByKodeWilayah');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MstWilayahRelatedByKodeWilayah');
        }

        return $this;
    }

    /**
     * Use the MstWilayahRelatedByKodeWilayah relation MstWilayah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\MstWilayahQuery A secondary query class using the current class as primary query
     */
    public function useMstWilayahRelatedByKodeWilayahQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMstWilayahRelatedByKodeWilayah($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MstWilayahRelatedByKodeWilayah', '\angulex\Model\MstWilayahQuery');
    }

    /**
     * Filter the query by a related MstWilayah object
     *
     * @param   MstWilayah|PropelObjectCollection $mstWilayah The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PenggunaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByMstWilayahRelatedByKodeWilayah($mstWilayah, $comparison = null)
    {
        if ($mstWilayah instanceof MstWilayah) {
            return $this
                ->addUsingAlias(PenggunaPeer::KODE_WILAYAH, $mstWilayah->getKodeWilayah(), $comparison);
        } elseif ($mstWilayah instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PenggunaPeer::KODE_WILAYAH, $mstWilayah->toKeyValue('PrimaryKey', 'KodeWilayah'), $comparison);
        } else {
            throw new PropelException('filterByMstWilayahRelatedByKodeWilayah() only accepts arguments of type MstWilayah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MstWilayahRelatedByKodeWilayah relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function joinMstWilayahRelatedByKodeWilayah($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MstWilayahRelatedByKodeWilayah');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MstWilayahRelatedByKodeWilayah');
        }

        return $this;
    }

    /**
     * Use the MstWilayahRelatedByKodeWilayah relation MstWilayah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\MstWilayahQuery A secondary query class using the current class as primary query
     */
    public function useMstWilayahRelatedByKodeWilayahQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMstWilayahRelatedByKodeWilayah($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MstWilayahRelatedByKodeWilayah', '\angulex\Model\MstWilayahQuery');
    }

    /**
     * Filter the query by a related Peran object
     *
     * @param   Peran|PropelObjectCollection $peran The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PenggunaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPeranRelatedByPeranId($peran, $comparison = null)
    {
        if ($peran instanceof Peran) {
            return $this
                ->addUsingAlias(PenggunaPeer::PERAN_ID, $peran->getPeranId(), $comparison);
        } elseif ($peran instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PenggunaPeer::PERAN_ID, $peran->toKeyValue('PrimaryKey', 'PeranId'), $comparison);
        } else {
            throw new PropelException('filterByPeranRelatedByPeranId() only accepts arguments of type Peran or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PeranRelatedByPeranId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function joinPeranRelatedByPeranId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PeranRelatedByPeranId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PeranRelatedByPeranId');
        }

        return $this;
    }

    /**
     * Use the PeranRelatedByPeranId relation Peran object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PeranQuery A secondary query class using the current class as primary query
     */
    public function usePeranRelatedByPeranIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPeranRelatedByPeranId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PeranRelatedByPeranId', '\angulex\Model\PeranQuery');
    }

    /**
     * Filter the query by a related Peran object
     *
     * @param   Peran|PropelObjectCollection $peran The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PenggunaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPeranRelatedByPeranId($peran, $comparison = null)
    {
        if ($peran instanceof Peran) {
            return $this
                ->addUsingAlias(PenggunaPeer::PERAN_ID, $peran->getPeranId(), $comparison);
        } elseif ($peran instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PenggunaPeer::PERAN_ID, $peran->toKeyValue('PrimaryKey', 'PeranId'), $comparison);
        } else {
            throw new PropelException('filterByPeranRelatedByPeranId() only accepts arguments of type Peran or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PeranRelatedByPeranId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function joinPeranRelatedByPeranId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PeranRelatedByPeranId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PeranRelatedByPeranId');
        }

        return $this;
    }

    /**
     * Use the PeranRelatedByPeranId relation Peran object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PeranQuery A secondary query class using the current class as primary query
     */
    public function usePeranRelatedByPeranIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPeranRelatedByPeranId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PeranRelatedByPeranId', '\angulex\Model\PeranQuery');
    }

    /**
     * Filter the query by a related LogPengguna object
     *
     * @param   LogPengguna|PropelObjectCollection $logPengguna  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PenggunaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByLogPenggunaRelatedByPenggunaId($logPengguna, $comparison = null)
    {
        if ($logPengguna instanceof LogPengguna) {
            return $this
                ->addUsingAlias(PenggunaPeer::PENGGUNA_ID, $logPengguna->getPenggunaId(), $comparison);
        } elseif ($logPengguna instanceof PropelObjectCollection) {
            return $this
                ->useLogPenggunaRelatedByPenggunaIdQuery()
                ->filterByPrimaryKeys($logPengguna->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByLogPenggunaRelatedByPenggunaId() only accepts arguments of type LogPengguna or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the LogPenggunaRelatedByPenggunaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function joinLogPenggunaRelatedByPenggunaId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('LogPenggunaRelatedByPenggunaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'LogPenggunaRelatedByPenggunaId');
        }

        return $this;
    }

    /**
     * Use the LogPenggunaRelatedByPenggunaId relation LogPengguna object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\LogPenggunaQuery A secondary query class using the current class as primary query
     */
    public function useLogPenggunaRelatedByPenggunaIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinLogPenggunaRelatedByPenggunaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'LogPenggunaRelatedByPenggunaId', '\angulex\Model\LogPenggunaQuery');
    }

    /**
     * Filter the query by a related LogPengguna object
     *
     * @param   LogPengguna|PropelObjectCollection $logPengguna  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PenggunaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByLogPenggunaRelatedByPenggunaId($logPengguna, $comparison = null)
    {
        if ($logPengguna instanceof LogPengguna) {
            return $this
                ->addUsingAlias(PenggunaPeer::PENGGUNA_ID, $logPengguna->getPenggunaId(), $comparison);
        } elseif ($logPengguna instanceof PropelObjectCollection) {
            return $this
                ->useLogPenggunaRelatedByPenggunaIdQuery()
                ->filterByPrimaryKeys($logPengguna->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByLogPenggunaRelatedByPenggunaId() only accepts arguments of type LogPengguna or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the LogPenggunaRelatedByPenggunaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function joinLogPenggunaRelatedByPenggunaId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('LogPenggunaRelatedByPenggunaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'LogPenggunaRelatedByPenggunaId');
        }

        return $this;
    }

    /**
     * Use the LogPenggunaRelatedByPenggunaId relation LogPengguna object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\LogPenggunaQuery A secondary query class using the current class as primary query
     */
    public function useLogPenggunaRelatedByPenggunaIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinLogPenggunaRelatedByPenggunaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'LogPenggunaRelatedByPenggunaId', '\angulex\Model\LogPenggunaQuery');
    }

    /**
     * Filter the query by a related SasaranSurvey object
     *
     * @param   SasaranSurvey|PropelObjectCollection $sasaranSurvey  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PenggunaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySasaranSurveyRelatedByPenggunaId($sasaranSurvey, $comparison = null)
    {
        if ($sasaranSurvey instanceof SasaranSurvey) {
            return $this
                ->addUsingAlias(PenggunaPeer::PENGGUNA_ID, $sasaranSurvey->getPenggunaId(), $comparison);
        } elseif ($sasaranSurvey instanceof PropelObjectCollection) {
            return $this
                ->useSasaranSurveyRelatedByPenggunaIdQuery()
                ->filterByPrimaryKeys($sasaranSurvey->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySasaranSurveyRelatedByPenggunaId() only accepts arguments of type SasaranSurvey or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SasaranSurveyRelatedByPenggunaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function joinSasaranSurveyRelatedByPenggunaId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SasaranSurveyRelatedByPenggunaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SasaranSurveyRelatedByPenggunaId');
        }

        return $this;
    }

    /**
     * Use the SasaranSurveyRelatedByPenggunaId relation SasaranSurvey object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SasaranSurveyQuery A secondary query class using the current class as primary query
     */
    public function useSasaranSurveyRelatedByPenggunaIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSasaranSurveyRelatedByPenggunaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SasaranSurveyRelatedByPenggunaId', '\angulex\Model\SasaranSurveyQuery');
    }

    /**
     * Filter the query by a related SasaranSurvey object
     *
     * @param   SasaranSurvey|PropelObjectCollection $sasaranSurvey  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PenggunaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySasaranSurveyRelatedByPenggunaId($sasaranSurvey, $comparison = null)
    {
        if ($sasaranSurvey instanceof SasaranSurvey) {
            return $this
                ->addUsingAlias(PenggunaPeer::PENGGUNA_ID, $sasaranSurvey->getPenggunaId(), $comparison);
        } elseif ($sasaranSurvey instanceof PropelObjectCollection) {
            return $this
                ->useSasaranSurveyRelatedByPenggunaIdQuery()
                ->filterByPrimaryKeys($sasaranSurvey->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySasaranSurveyRelatedByPenggunaId() only accepts arguments of type SasaranSurvey or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SasaranSurveyRelatedByPenggunaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function joinSasaranSurveyRelatedByPenggunaId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SasaranSurveyRelatedByPenggunaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SasaranSurveyRelatedByPenggunaId');
        }

        return $this;
    }

    /**
     * Use the SasaranSurveyRelatedByPenggunaId relation SasaranSurvey object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SasaranSurveyQuery A secondary query class using the current class as primary query
     */
    public function useSasaranSurveyRelatedByPenggunaIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSasaranSurveyRelatedByPenggunaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SasaranSurveyRelatedByPenggunaId', '\angulex\Model\SasaranSurveyQuery');
    }

    /**
     * Filter the query by a related LembagaNonSekolah object
     *
     * @param   LembagaNonSekolah|PropelObjectCollection $lembagaNonSekolah  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PenggunaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByLembagaNonSekolahRelatedByPenggunaId($lembagaNonSekolah, $comparison = null)
    {
        if ($lembagaNonSekolah instanceof LembagaNonSekolah) {
            return $this
                ->addUsingAlias(PenggunaPeer::PENGGUNA_ID, $lembagaNonSekolah->getPenggunaId(), $comparison);
        } elseif ($lembagaNonSekolah instanceof PropelObjectCollection) {
            return $this
                ->useLembagaNonSekolahRelatedByPenggunaIdQuery()
                ->filterByPrimaryKeys($lembagaNonSekolah->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByLembagaNonSekolahRelatedByPenggunaId() only accepts arguments of type LembagaNonSekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the LembagaNonSekolahRelatedByPenggunaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function joinLembagaNonSekolahRelatedByPenggunaId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('LembagaNonSekolahRelatedByPenggunaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'LembagaNonSekolahRelatedByPenggunaId');
        }

        return $this;
    }

    /**
     * Use the LembagaNonSekolahRelatedByPenggunaId relation LembagaNonSekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\LembagaNonSekolahQuery A secondary query class using the current class as primary query
     */
    public function useLembagaNonSekolahRelatedByPenggunaIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinLembagaNonSekolahRelatedByPenggunaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'LembagaNonSekolahRelatedByPenggunaId', '\angulex\Model\LembagaNonSekolahQuery');
    }

    /**
     * Filter the query by a related LembagaNonSekolah object
     *
     * @param   LembagaNonSekolah|PropelObjectCollection $lembagaNonSekolah  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PenggunaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByLembagaNonSekolahRelatedByPenggunaId($lembagaNonSekolah, $comparison = null)
    {
        if ($lembagaNonSekolah instanceof LembagaNonSekolah) {
            return $this
                ->addUsingAlias(PenggunaPeer::PENGGUNA_ID, $lembagaNonSekolah->getPenggunaId(), $comparison);
        } elseif ($lembagaNonSekolah instanceof PropelObjectCollection) {
            return $this
                ->useLembagaNonSekolahRelatedByPenggunaIdQuery()
                ->filterByPrimaryKeys($lembagaNonSekolah->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByLembagaNonSekolahRelatedByPenggunaId() only accepts arguments of type LembagaNonSekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the LembagaNonSekolahRelatedByPenggunaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function joinLembagaNonSekolahRelatedByPenggunaId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('LembagaNonSekolahRelatedByPenggunaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'LembagaNonSekolahRelatedByPenggunaId');
        }

        return $this;
    }

    /**
     * Use the LembagaNonSekolahRelatedByPenggunaId relation LembagaNonSekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\LembagaNonSekolahQuery A secondary query class using the current class as primary query
     */
    public function useLembagaNonSekolahRelatedByPenggunaIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinLembagaNonSekolahRelatedByPenggunaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'LembagaNonSekolahRelatedByPenggunaId', '\angulex\Model\LembagaNonSekolahQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Pengguna $pengguna Object to remove from the list of results
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function prune($pengguna = null)
    {
        if ($pengguna) {
            $this->addUsingAlias(PenggunaPeer::PENGGUNA_ID, $pengguna->getPenggunaId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
