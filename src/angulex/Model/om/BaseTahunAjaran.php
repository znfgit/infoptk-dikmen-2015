<?php

namespace angulex\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelDateTime;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\BeasiswaPesertaDidik;
use angulex\Model\BeasiswaPesertaDidikQuery;
use angulex\Model\Demografi;
use angulex\Model\DemografiQuery;
use angulex\Model\PengawasTerdaftar;
use angulex\Model\PengawasTerdaftarQuery;
use angulex\Model\PesertaDidikBaru;
use angulex\Model\PesertaDidikBaruQuery;
use angulex\Model\PtkBaru;
use angulex\Model\PtkBaruQuery;
use angulex\Model\PtkTerdaftar;
use angulex\Model\PtkTerdaftarQuery;
use angulex\Model\Semester;
use angulex\Model\SemesterQuery;
use angulex\Model\TahunAjaran;
use angulex\Model\TahunAjaranPeer;
use angulex\Model\TahunAjaranQuery;
use angulex\Model\TemplateUn;
use angulex\Model\TemplateUnQuery;

/**
 * Base class that represents a row from the 'ref.tahun_ajaran' table.
 *
 * 
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseTahunAjaran extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'angulex\\Model\\TahunAjaranPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        TahunAjaranPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the tahun_ajaran_id field.
     * @var        string
     */
    protected $tahun_ajaran_id;

    /**
     * The value for the nama field.
     * @var        string
     */
    protected $nama;

    /**
     * The value for the periode_aktif field.
     * @var        string
     */
    protected $periode_aktif;

    /**
     * The value for the tanggal_mulai field.
     * @var        string
     */
    protected $tanggal_mulai;

    /**
     * The value for the tanggal_selesai field.
     * @var        string
     */
    protected $tanggal_selesai;

    /**
     * The value for the create_date field.
     * @var        string
     */
    protected $create_date;

    /**
     * The value for the last_update field.
     * @var        string
     */
    protected $last_update;

    /**
     * The value for the expired_date field.
     * @var        string
     */
    protected $expired_date;

    /**
     * The value for the last_sync field.
     * @var        string
     */
    protected $last_sync;

    /**
     * @var        PropelObjectCollection|TemplateUn[] Collection to store aggregation of TemplateUn objects.
     */
    protected $collTemplateUns;
    protected $collTemplateUnsPartial;

    /**
     * @var        PropelObjectCollection|BeasiswaPesertaDidik[] Collection to store aggregation of BeasiswaPesertaDidik objects.
     */
    protected $collBeasiswaPesertaDidiksRelatedByTahunSelesai;
    protected $collBeasiswaPesertaDidiksRelatedByTahunSelesaiPartial;

    /**
     * @var        PropelObjectCollection|BeasiswaPesertaDidik[] Collection to store aggregation of BeasiswaPesertaDidik objects.
     */
    protected $collBeasiswaPesertaDidiksRelatedByTahunMulai;
    protected $collBeasiswaPesertaDidiksRelatedByTahunMulaiPartial;

    /**
     * @var        PropelObjectCollection|BeasiswaPesertaDidik[] Collection to store aggregation of BeasiswaPesertaDidik objects.
     */
    protected $collBeasiswaPesertaDidiksRelatedByTahunSelesai;
    protected $collBeasiswaPesertaDidiksRelatedByTahunSelesaiPartial;

    /**
     * @var        PropelObjectCollection|BeasiswaPesertaDidik[] Collection to store aggregation of BeasiswaPesertaDidik objects.
     */
    protected $collBeasiswaPesertaDidiksRelatedByTahunMulai;
    protected $collBeasiswaPesertaDidiksRelatedByTahunMulaiPartial;

    /**
     * @var        PropelObjectCollection|PesertaDidikBaru[] Collection to store aggregation of PesertaDidikBaru objects.
     */
    protected $collPesertaDidikBarusRelatedByTahunAjaranId;
    protected $collPesertaDidikBarusRelatedByTahunAjaranIdPartial;

    /**
     * @var        PropelObjectCollection|PesertaDidikBaru[] Collection to store aggregation of PesertaDidikBaru objects.
     */
    protected $collPesertaDidikBarusRelatedByTahunAjaranId;
    protected $collPesertaDidikBarusRelatedByTahunAjaranIdPartial;

    /**
     * @var        PropelObjectCollection|PtkTerdaftar[] Collection to store aggregation of PtkTerdaftar objects.
     */
    protected $collPtkTerdaftarsRelatedByTahunAjaranId;
    protected $collPtkTerdaftarsRelatedByTahunAjaranIdPartial;

    /**
     * @var        PropelObjectCollection|PtkTerdaftar[] Collection to store aggregation of PtkTerdaftar objects.
     */
    protected $collPtkTerdaftarsRelatedByTahunAjaranId;
    protected $collPtkTerdaftarsRelatedByTahunAjaranIdPartial;

    /**
     * @var        PropelObjectCollection|Semester[] Collection to store aggregation of Semester objects.
     */
    protected $collSemesters;
    protected $collSemestersPartial;

    /**
     * @var        PropelObjectCollection|PengawasTerdaftar[] Collection to store aggregation of PengawasTerdaftar objects.
     */
    protected $collPengawasTerdaftarsRelatedByTahunAjaranId;
    protected $collPengawasTerdaftarsRelatedByTahunAjaranIdPartial;

    /**
     * @var        PropelObjectCollection|PengawasTerdaftar[] Collection to store aggregation of PengawasTerdaftar objects.
     */
    protected $collPengawasTerdaftarsRelatedByTahunAjaranId;
    protected $collPengawasTerdaftarsRelatedByTahunAjaranIdPartial;

    /**
     * @var        PropelObjectCollection|PtkBaru[] Collection to store aggregation of PtkBaru objects.
     */
    protected $collPtkBarusRelatedByTahunAjaranId;
    protected $collPtkBarusRelatedByTahunAjaranIdPartial;

    /**
     * @var        PropelObjectCollection|PtkBaru[] Collection to store aggregation of PtkBaru objects.
     */
    protected $collPtkBarusRelatedByTahunAjaranId;
    protected $collPtkBarusRelatedByTahunAjaranIdPartial;

    /**
     * @var        PropelObjectCollection|Demografi[] Collection to store aggregation of Demografi objects.
     */
    protected $collDemografisRelatedByTahunAjaranId;
    protected $collDemografisRelatedByTahunAjaranIdPartial;

    /**
     * @var        PropelObjectCollection|Demografi[] Collection to store aggregation of Demografi objects.
     */
    protected $collDemografisRelatedByTahunAjaranId;
    protected $collDemografisRelatedByTahunAjaranIdPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $templateUnsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $beasiswaPesertaDidiksRelatedByTahunSelesaiScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $beasiswaPesertaDidiksRelatedByTahunMulaiScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $beasiswaPesertaDidiksRelatedByTahunSelesaiScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $beasiswaPesertaDidiksRelatedByTahunMulaiScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $pesertaDidikBarusRelatedByTahunAjaranIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $pesertaDidikBarusRelatedByTahunAjaranIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $ptkTerdaftarsRelatedByTahunAjaranIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $ptkTerdaftarsRelatedByTahunAjaranIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $semestersScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $pengawasTerdaftarsRelatedByTahunAjaranIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $pengawasTerdaftarsRelatedByTahunAjaranIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $ptkBarusRelatedByTahunAjaranIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $ptkBarusRelatedByTahunAjaranIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $demografisRelatedByTahunAjaranIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $demografisRelatedByTahunAjaranIdScheduledForDeletion = null;

    /**
     * Get the [tahun_ajaran_id] column value.
     * 
     * @return string
     */
    public function getTahunAjaranId()
    {
        return $this->tahun_ajaran_id;
    }

    /**
     * Get the [nama] column value.
     * 
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Get the [periode_aktif] column value.
     * 
     * @return string
     */
    public function getPeriodeAktif()
    {
        return $this->periode_aktif;
    }

    /**
     * Get the [tanggal_mulai] column value.
     * 
     * @return string
     */
    public function getTanggalMulai()
    {
        return $this->tanggal_mulai;
    }

    /**
     * Get the [tanggal_selesai] column value.
     * 
     * @return string
     */
    public function getTanggalSelesai()
    {
        return $this->tanggal_selesai;
    }

    /**
     * Get the [optionally formatted] temporal [create_date] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreateDate($format = 'Y-m-d H:i:s')
    {
        if ($this->create_date === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->create_date);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->create_date, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [last_update] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastUpdate($format = 'Y-m-d H:i:s')
    {
        if ($this->last_update === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_update);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_update, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [expired_date] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getExpiredDate($format = 'Y-m-d H:i:s')
    {
        if ($this->expired_date === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->expired_date);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->expired_date, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [last_sync] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastSync($format = 'Y-m-d H:i:s')
    {
        if ($this->last_sync === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_sync);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_sync, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Set the value of [tahun_ajaran_id] column.
     * 
     * @param string $v new value
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function setTahunAjaranId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tahun_ajaran_id !== $v) {
            $this->tahun_ajaran_id = $v;
            $this->modifiedColumns[] = TahunAjaranPeer::TAHUN_AJARAN_ID;
        }


        return $this;
    } // setTahunAjaranId()

    /**
     * Set the value of [nama] column.
     * 
     * @param string $v new value
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function setNama($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nama !== $v) {
            $this->nama = $v;
            $this->modifiedColumns[] = TahunAjaranPeer::NAMA;
        }


        return $this;
    } // setNama()

    /**
     * Set the value of [periode_aktif] column.
     * 
     * @param string $v new value
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function setPeriodeAktif($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->periode_aktif !== $v) {
            $this->periode_aktif = $v;
            $this->modifiedColumns[] = TahunAjaranPeer::PERIODE_AKTIF;
        }


        return $this;
    } // setPeriodeAktif()

    /**
     * Set the value of [tanggal_mulai] column.
     * 
     * @param string $v new value
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function setTanggalMulai($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tanggal_mulai !== $v) {
            $this->tanggal_mulai = $v;
            $this->modifiedColumns[] = TahunAjaranPeer::TANGGAL_MULAI;
        }


        return $this;
    } // setTanggalMulai()

    /**
     * Set the value of [tanggal_selesai] column.
     * 
     * @param string $v new value
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function setTanggalSelesai($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tanggal_selesai !== $v) {
            $this->tanggal_selesai = $v;
            $this->modifiedColumns[] = TahunAjaranPeer::TANGGAL_SELESAI;
        }


        return $this;
    } // setTanggalSelesai()

    /**
     * Sets the value of [create_date] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function setCreateDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->create_date !== null || $dt !== null) {
            $currentDateAsString = ($this->create_date !== null && $tmpDt = new DateTime($this->create_date)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->create_date = $newDateAsString;
                $this->modifiedColumns[] = TahunAjaranPeer::CREATE_DATE;
            }
        } // if either are not null


        return $this;
    } // setCreateDate()

    /**
     * Sets the value of [last_update] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function setLastUpdate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_update !== null || $dt !== null) {
            $currentDateAsString = ($this->last_update !== null && $tmpDt = new DateTime($this->last_update)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_update = $newDateAsString;
                $this->modifiedColumns[] = TahunAjaranPeer::LAST_UPDATE;
            }
        } // if either are not null


        return $this;
    } // setLastUpdate()

    /**
     * Sets the value of [expired_date] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function setExpiredDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->expired_date !== null || $dt !== null) {
            $currentDateAsString = ($this->expired_date !== null && $tmpDt = new DateTime($this->expired_date)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->expired_date = $newDateAsString;
                $this->modifiedColumns[] = TahunAjaranPeer::EXPIRED_DATE;
            }
        } // if either are not null


        return $this;
    } // setExpiredDate()

    /**
     * Sets the value of [last_sync] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function setLastSync($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_sync !== null || $dt !== null) {
            $currentDateAsString = ($this->last_sync !== null && $tmpDt = new DateTime($this->last_sync)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_sync = $newDateAsString;
                $this->modifiedColumns[] = TahunAjaranPeer::LAST_SYNC;
            }
        } // if either are not null


        return $this;
    } // setLastSync()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->tahun_ajaran_id = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
            $this->nama = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->periode_aktif = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->tanggal_mulai = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->tanggal_selesai = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->create_date = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->last_update = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->expired_date = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->last_sync = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 9; // 9 = TahunAjaranPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating TahunAjaran object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(TahunAjaranPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = TahunAjaranPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collTemplateUns = null;

            $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai = null;

            $this->collBeasiswaPesertaDidiksRelatedByTahunMulai = null;

            $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai = null;

            $this->collBeasiswaPesertaDidiksRelatedByTahunMulai = null;

            $this->collPesertaDidikBarusRelatedByTahunAjaranId = null;

            $this->collPesertaDidikBarusRelatedByTahunAjaranId = null;

            $this->collPtkTerdaftarsRelatedByTahunAjaranId = null;

            $this->collPtkTerdaftarsRelatedByTahunAjaranId = null;

            $this->collSemesters = null;

            $this->collPengawasTerdaftarsRelatedByTahunAjaranId = null;

            $this->collPengawasTerdaftarsRelatedByTahunAjaranId = null;

            $this->collPtkBarusRelatedByTahunAjaranId = null;

            $this->collPtkBarusRelatedByTahunAjaranId = null;

            $this->collDemografisRelatedByTahunAjaranId = null;

            $this->collDemografisRelatedByTahunAjaranId = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(TahunAjaranPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = TahunAjaranQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(TahunAjaranPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                TahunAjaranPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->templateUnsScheduledForDeletion !== null) {
                if (!$this->templateUnsScheduledForDeletion->isEmpty()) {
                    TemplateUnQuery::create()
                        ->filterByPrimaryKeys($this->templateUnsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->templateUnsScheduledForDeletion = null;
                }
            }

            if ($this->collTemplateUns !== null) {
                foreach ($this->collTemplateUns as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->beasiswaPesertaDidiksRelatedByTahunSelesaiScheduledForDeletion !== null) {
                if (!$this->beasiswaPesertaDidiksRelatedByTahunSelesaiScheduledForDeletion->isEmpty()) {
                    BeasiswaPesertaDidikQuery::create()
                        ->filterByPrimaryKeys($this->beasiswaPesertaDidiksRelatedByTahunSelesaiScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->beasiswaPesertaDidiksRelatedByTahunSelesaiScheduledForDeletion = null;
                }
            }

            if ($this->collBeasiswaPesertaDidiksRelatedByTahunSelesai !== null) {
                foreach ($this->collBeasiswaPesertaDidiksRelatedByTahunSelesai as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->beasiswaPesertaDidiksRelatedByTahunMulaiScheduledForDeletion !== null) {
                if (!$this->beasiswaPesertaDidiksRelatedByTahunMulaiScheduledForDeletion->isEmpty()) {
                    BeasiswaPesertaDidikQuery::create()
                        ->filterByPrimaryKeys($this->beasiswaPesertaDidiksRelatedByTahunMulaiScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->beasiswaPesertaDidiksRelatedByTahunMulaiScheduledForDeletion = null;
                }
            }

            if ($this->collBeasiswaPesertaDidiksRelatedByTahunMulai !== null) {
                foreach ($this->collBeasiswaPesertaDidiksRelatedByTahunMulai as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->beasiswaPesertaDidiksRelatedByTahunSelesaiScheduledForDeletion !== null) {
                if (!$this->beasiswaPesertaDidiksRelatedByTahunSelesaiScheduledForDeletion->isEmpty()) {
                    BeasiswaPesertaDidikQuery::create()
                        ->filterByPrimaryKeys($this->beasiswaPesertaDidiksRelatedByTahunSelesaiScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->beasiswaPesertaDidiksRelatedByTahunSelesaiScheduledForDeletion = null;
                }
            }

            if ($this->collBeasiswaPesertaDidiksRelatedByTahunSelesai !== null) {
                foreach ($this->collBeasiswaPesertaDidiksRelatedByTahunSelesai as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->beasiswaPesertaDidiksRelatedByTahunMulaiScheduledForDeletion !== null) {
                if (!$this->beasiswaPesertaDidiksRelatedByTahunMulaiScheduledForDeletion->isEmpty()) {
                    BeasiswaPesertaDidikQuery::create()
                        ->filterByPrimaryKeys($this->beasiswaPesertaDidiksRelatedByTahunMulaiScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->beasiswaPesertaDidiksRelatedByTahunMulaiScheduledForDeletion = null;
                }
            }

            if ($this->collBeasiswaPesertaDidiksRelatedByTahunMulai !== null) {
                foreach ($this->collBeasiswaPesertaDidiksRelatedByTahunMulai as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->pesertaDidikBarusRelatedByTahunAjaranIdScheduledForDeletion !== null) {
                if (!$this->pesertaDidikBarusRelatedByTahunAjaranIdScheduledForDeletion->isEmpty()) {
                    PesertaDidikBaruQuery::create()
                        ->filterByPrimaryKeys($this->pesertaDidikBarusRelatedByTahunAjaranIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->pesertaDidikBarusRelatedByTahunAjaranIdScheduledForDeletion = null;
                }
            }

            if ($this->collPesertaDidikBarusRelatedByTahunAjaranId !== null) {
                foreach ($this->collPesertaDidikBarusRelatedByTahunAjaranId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->pesertaDidikBarusRelatedByTahunAjaranIdScheduledForDeletion !== null) {
                if (!$this->pesertaDidikBarusRelatedByTahunAjaranIdScheduledForDeletion->isEmpty()) {
                    PesertaDidikBaruQuery::create()
                        ->filterByPrimaryKeys($this->pesertaDidikBarusRelatedByTahunAjaranIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->pesertaDidikBarusRelatedByTahunAjaranIdScheduledForDeletion = null;
                }
            }

            if ($this->collPesertaDidikBarusRelatedByTahunAjaranId !== null) {
                foreach ($this->collPesertaDidikBarusRelatedByTahunAjaranId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->ptkTerdaftarsRelatedByTahunAjaranIdScheduledForDeletion !== null) {
                if (!$this->ptkTerdaftarsRelatedByTahunAjaranIdScheduledForDeletion->isEmpty()) {
                    PtkTerdaftarQuery::create()
                        ->filterByPrimaryKeys($this->ptkTerdaftarsRelatedByTahunAjaranIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->ptkTerdaftarsRelatedByTahunAjaranIdScheduledForDeletion = null;
                }
            }

            if ($this->collPtkTerdaftarsRelatedByTahunAjaranId !== null) {
                foreach ($this->collPtkTerdaftarsRelatedByTahunAjaranId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->ptkTerdaftarsRelatedByTahunAjaranIdScheduledForDeletion !== null) {
                if (!$this->ptkTerdaftarsRelatedByTahunAjaranIdScheduledForDeletion->isEmpty()) {
                    PtkTerdaftarQuery::create()
                        ->filterByPrimaryKeys($this->ptkTerdaftarsRelatedByTahunAjaranIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->ptkTerdaftarsRelatedByTahunAjaranIdScheduledForDeletion = null;
                }
            }

            if ($this->collPtkTerdaftarsRelatedByTahunAjaranId !== null) {
                foreach ($this->collPtkTerdaftarsRelatedByTahunAjaranId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->semestersScheduledForDeletion !== null) {
                if (!$this->semestersScheduledForDeletion->isEmpty()) {
                    SemesterQuery::create()
                        ->filterByPrimaryKeys($this->semestersScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->semestersScheduledForDeletion = null;
                }
            }

            if ($this->collSemesters !== null) {
                foreach ($this->collSemesters as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->pengawasTerdaftarsRelatedByTahunAjaranIdScheduledForDeletion !== null) {
                if (!$this->pengawasTerdaftarsRelatedByTahunAjaranIdScheduledForDeletion->isEmpty()) {
                    PengawasTerdaftarQuery::create()
                        ->filterByPrimaryKeys($this->pengawasTerdaftarsRelatedByTahunAjaranIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->pengawasTerdaftarsRelatedByTahunAjaranIdScheduledForDeletion = null;
                }
            }

            if ($this->collPengawasTerdaftarsRelatedByTahunAjaranId !== null) {
                foreach ($this->collPengawasTerdaftarsRelatedByTahunAjaranId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->pengawasTerdaftarsRelatedByTahunAjaranIdScheduledForDeletion !== null) {
                if (!$this->pengawasTerdaftarsRelatedByTahunAjaranIdScheduledForDeletion->isEmpty()) {
                    PengawasTerdaftarQuery::create()
                        ->filterByPrimaryKeys($this->pengawasTerdaftarsRelatedByTahunAjaranIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->pengawasTerdaftarsRelatedByTahunAjaranIdScheduledForDeletion = null;
                }
            }

            if ($this->collPengawasTerdaftarsRelatedByTahunAjaranId !== null) {
                foreach ($this->collPengawasTerdaftarsRelatedByTahunAjaranId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->ptkBarusRelatedByTahunAjaranIdScheduledForDeletion !== null) {
                if (!$this->ptkBarusRelatedByTahunAjaranIdScheduledForDeletion->isEmpty()) {
                    PtkBaruQuery::create()
                        ->filterByPrimaryKeys($this->ptkBarusRelatedByTahunAjaranIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->ptkBarusRelatedByTahunAjaranIdScheduledForDeletion = null;
                }
            }

            if ($this->collPtkBarusRelatedByTahunAjaranId !== null) {
                foreach ($this->collPtkBarusRelatedByTahunAjaranId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->ptkBarusRelatedByTahunAjaranIdScheduledForDeletion !== null) {
                if (!$this->ptkBarusRelatedByTahunAjaranIdScheduledForDeletion->isEmpty()) {
                    PtkBaruQuery::create()
                        ->filterByPrimaryKeys($this->ptkBarusRelatedByTahunAjaranIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->ptkBarusRelatedByTahunAjaranIdScheduledForDeletion = null;
                }
            }

            if ($this->collPtkBarusRelatedByTahunAjaranId !== null) {
                foreach ($this->collPtkBarusRelatedByTahunAjaranId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->demografisRelatedByTahunAjaranIdScheduledForDeletion !== null) {
                if (!$this->demografisRelatedByTahunAjaranIdScheduledForDeletion->isEmpty()) {
                    DemografiQuery::create()
                        ->filterByPrimaryKeys($this->demografisRelatedByTahunAjaranIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->demografisRelatedByTahunAjaranIdScheduledForDeletion = null;
                }
            }

            if ($this->collDemografisRelatedByTahunAjaranId !== null) {
                foreach ($this->collDemografisRelatedByTahunAjaranId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->demografisRelatedByTahunAjaranIdScheduledForDeletion !== null) {
                if (!$this->demografisRelatedByTahunAjaranIdScheduledForDeletion->isEmpty()) {
                    DemografiQuery::create()
                        ->filterByPrimaryKeys($this->demografisRelatedByTahunAjaranIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->demografisRelatedByTahunAjaranIdScheduledForDeletion = null;
                }
            }

            if ($this->collDemografisRelatedByTahunAjaranId !== null) {
                foreach ($this->collDemografisRelatedByTahunAjaranId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $criteria = $this->buildCriteria();
        $pk = BasePeer::doInsert($criteria, $con);
        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = TahunAjaranPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collTemplateUns !== null) {
                    foreach ($this->collTemplateUns as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collBeasiswaPesertaDidiksRelatedByTahunSelesai !== null) {
                    foreach ($this->collBeasiswaPesertaDidiksRelatedByTahunSelesai as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collBeasiswaPesertaDidiksRelatedByTahunMulai !== null) {
                    foreach ($this->collBeasiswaPesertaDidiksRelatedByTahunMulai as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collBeasiswaPesertaDidiksRelatedByTahunSelesai !== null) {
                    foreach ($this->collBeasiswaPesertaDidiksRelatedByTahunSelesai as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collBeasiswaPesertaDidiksRelatedByTahunMulai !== null) {
                    foreach ($this->collBeasiswaPesertaDidiksRelatedByTahunMulai as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPesertaDidikBarusRelatedByTahunAjaranId !== null) {
                    foreach ($this->collPesertaDidikBarusRelatedByTahunAjaranId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPesertaDidikBarusRelatedByTahunAjaranId !== null) {
                    foreach ($this->collPesertaDidikBarusRelatedByTahunAjaranId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPtkTerdaftarsRelatedByTahunAjaranId !== null) {
                    foreach ($this->collPtkTerdaftarsRelatedByTahunAjaranId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPtkTerdaftarsRelatedByTahunAjaranId !== null) {
                    foreach ($this->collPtkTerdaftarsRelatedByTahunAjaranId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSemesters !== null) {
                    foreach ($this->collSemesters as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPengawasTerdaftarsRelatedByTahunAjaranId !== null) {
                    foreach ($this->collPengawasTerdaftarsRelatedByTahunAjaranId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPengawasTerdaftarsRelatedByTahunAjaranId !== null) {
                    foreach ($this->collPengawasTerdaftarsRelatedByTahunAjaranId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPtkBarusRelatedByTahunAjaranId !== null) {
                    foreach ($this->collPtkBarusRelatedByTahunAjaranId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPtkBarusRelatedByTahunAjaranId !== null) {
                    foreach ($this->collPtkBarusRelatedByTahunAjaranId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collDemografisRelatedByTahunAjaranId !== null) {
                    foreach ($this->collDemografisRelatedByTahunAjaranId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collDemografisRelatedByTahunAjaranId !== null) {
                    foreach ($this->collDemografisRelatedByTahunAjaranId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = TahunAjaranPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getTahunAjaranId();
                break;
            case 1:
                return $this->getNama();
                break;
            case 2:
                return $this->getPeriodeAktif();
                break;
            case 3:
                return $this->getTanggalMulai();
                break;
            case 4:
                return $this->getTanggalSelesai();
                break;
            case 5:
                return $this->getCreateDate();
                break;
            case 6:
                return $this->getLastUpdate();
                break;
            case 7:
                return $this->getExpiredDate();
                break;
            case 8:
                return $this->getLastSync();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['TahunAjaran'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['TahunAjaran'][$this->getPrimaryKey()] = true;
        $keys = TahunAjaranPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getTahunAjaranId(),
            $keys[1] => $this->getNama(),
            $keys[2] => $this->getPeriodeAktif(),
            $keys[3] => $this->getTanggalMulai(),
            $keys[4] => $this->getTanggalSelesai(),
            $keys[5] => $this->getCreateDate(),
            $keys[6] => $this->getLastUpdate(),
            $keys[7] => $this->getExpiredDate(),
            $keys[8] => $this->getLastSync(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->collTemplateUns) {
                $result['TemplateUns'] = $this->collTemplateUns->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai) {
                $result['BeasiswaPesertaDidiksRelatedByTahunSelesai'] = $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collBeasiswaPesertaDidiksRelatedByTahunMulai) {
                $result['BeasiswaPesertaDidiksRelatedByTahunMulai'] = $this->collBeasiswaPesertaDidiksRelatedByTahunMulai->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai) {
                $result['BeasiswaPesertaDidiksRelatedByTahunSelesai'] = $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collBeasiswaPesertaDidiksRelatedByTahunMulai) {
                $result['BeasiswaPesertaDidiksRelatedByTahunMulai'] = $this->collBeasiswaPesertaDidiksRelatedByTahunMulai->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPesertaDidikBarusRelatedByTahunAjaranId) {
                $result['PesertaDidikBarusRelatedByTahunAjaranId'] = $this->collPesertaDidikBarusRelatedByTahunAjaranId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPesertaDidikBarusRelatedByTahunAjaranId) {
                $result['PesertaDidikBarusRelatedByTahunAjaranId'] = $this->collPesertaDidikBarusRelatedByTahunAjaranId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPtkTerdaftarsRelatedByTahunAjaranId) {
                $result['PtkTerdaftarsRelatedByTahunAjaranId'] = $this->collPtkTerdaftarsRelatedByTahunAjaranId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPtkTerdaftarsRelatedByTahunAjaranId) {
                $result['PtkTerdaftarsRelatedByTahunAjaranId'] = $this->collPtkTerdaftarsRelatedByTahunAjaranId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSemesters) {
                $result['Semesters'] = $this->collSemesters->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPengawasTerdaftarsRelatedByTahunAjaranId) {
                $result['PengawasTerdaftarsRelatedByTahunAjaranId'] = $this->collPengawasTerdaftarsRelatedByTahunAjaranId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPengawasTerdaftarsRelatedByTahunAjaranId) {
                $result['PengawasTerdaftarsRelatedByTahunAjaranId'] = $this->collPengawasTerdaftarsRelatedByTahunAjaranId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPtkBarusRelatedByTahunAjaranId) {
                $result['PtkBarusRelatedByTahunAjaranId'] = $this->collPtkBarusRelatedByTahunAjaranId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPtkBarusRelatedByTahunAjaranId) {
                $result['PtkBarusRelatedByTahunAjaranId'] = $this->collPtkBarusRelatedByTahunAjaranId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collDemografisRelatedByTahunAjaranId) {
                $result['DemografisRelatedByTahunAjaranId'] = $this->collDemografisRelatedByTahunAjaranId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collDemografisRelatedByTahunAjaranId) {
                $result['DemografisRelatedByTahunAjaranId'] = $this->collDemografisRelatedByTahunAjaranId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = TahunAjaranPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setTahunAjaranId($value);
                break;
            case 1:
                $this->setNama($value);
                break;
            case 2:
                $this->setPeriodeAktif($value);
                break;
            case 3:
                $this->setTanggalMulai($value);
                break;
            case 4:
                $this->setTanggalSelesai($value);
                break;
            case 5:
                $this->setCreateDate($value);
                break;
            case 6:
                $this->setLastUpdate($value);
                break;
            case 7:
                $this->setExpiredDate($value);
                break;
            case 8:
                $this->setLastSync($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = TahunAjaranPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setTahunAjaranId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setNama($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setPeriodeAktif($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setTanggalMulai($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setTanggalSelesai($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setCreateDate($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setLastUpdate($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setExpiredDate($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setLastSync($arr[$keys[8]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(TahunAjaranPeer::DATABASE_NAME);

        if ($this->isColumnModified(TahunAjaranPeer::TAHUN_AJARAN_ID)) $criteria->add(TahunAjaranPeer::TAHUN_AJARAN_ID, $this->tahun_ajaran_id);
        if ($this->isColumnModified(TahunAjaranPeer::NAMA)) $criteria->add(TahunAjaranPeer::NAMA, $this->nama);
        if ($this->isColumnModified(TahunAjaranPeer::PERIODE_AKTIF)) $criteria->add(TahunAjaranPeer::PERIODE_AKTIF, $this->periode_aktif);
        if ($this->isColumnModified(TahunAjaranPeer::TANGGAL_MULAI)) $criteria->add(TahunAjaranPeer::TANGGAL_MULAI, $this->tanggal_mulai);
        if ($this->isColumnModified(TahunAjaranPeer::TANGGAL_SELESAI)) $criteria->add(TahunAjaranPeer::TANGGAL_SELESAI, $this->tanggal_selesai);
        if ($this->isColumnModified(TahunAjaranPeer::CREATE_DATE)) $criteria->add(TahunAjaranPeer::CREATE_DATE, $this->create_date);
        if ($this->isColumnModified(TahunAjaranPeer::LAST_UPDATE)) $criteria->add(TahunAjaranPeer::LAST_UPDATE, $this->last_update);
        if ($this->isColumnModified(TahunAjaranPeer::EXPIRED_DATE)) $criteria->add(TahunAjaranPeer::EXPIRED_DATE, $this->expired_date);
        if ($this->isColumnModified(TahunAjaranPeer::LAST_SYNC)) $criteria->add(TahunAjaranPeer::LAST_SYNC, $this->last_sync);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(TahunAjaranPeer::DATABASE_NAME);
        $criteria->add(TahunAjaranPeer::TAHUN_AJARAN_ID, $this->tahun_ajaran_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getTahunAjaranId();
    }

    /**
     * Generic method to set the primary key (tahun_ajaran_id column).
     *
     * @param  string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setTahunAjaranId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getTahunAjaranId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of TahunAjaran (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setNama($this->getNama());
        $copyObj->setPeriodeAktif($this->getPeriodeAktif());
        $copyObj->setTanggalMulai($this->getTanggalMulai());
        $copyObj->setTanggalSelesai($this->getTanggalSelesai());
        $copyObj->setCreateDate($this->getCreateDate());
        $copyObj->setLastUpdate($this->getLastUpdate());
        $copyObj->setExpiredDate($this->getExpiredDate());
        $copyObj->setLastSync($this->getLastSync());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getTemplateUns() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addTemplateUn($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getBeasiswaPesertaDidiksRelatedByTahunSelesai() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBeasiswaPesertaDidikRelatedByTahunSelesai($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getBeasiswaPesertaDidiksRelatedByTahunMulai() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBeasiswaPesertaDidikRelatedByTahunMulai($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getBeasiswaPesertaDidiksRelatedByTahunSelesai() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBeasiswaPesertaDidikRelatedByTahunSelesai($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getBeasiswaPesertaDidiksRelatedByTahunMulai() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBeasiswaPesertaDidikRelatedByTahunMulai($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPesertaDidikBarusRelatedByTahunAjaranId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPesertaDidikBaruRelatedByTahunAjaranId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPesertaDidikBarusRelatedByTahunAjaranId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPesertaDidikBaruRelatedByTahunAjaranId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPtkTerdaftarsRelatedByTahunAjaranId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPtkTerdaftarRelatedByTahunAjaranId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPtkTerdaftarsRelatedByTahunAjaranId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPtkTerdaftarRelatedByTahunAjaranId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSemesters() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSemester($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPengawasTerdaftarsRelatedByTahunAjaranId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPengawasTerdaftarRelatedByTahunAjaranId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPengawasTerdaftarsRelatedByTahunAjaranId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPengawasTerdaftarRelatedByTahunAjaranId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPtkBarusRelatedByTahunAjaranId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPtkBaruRelatedByTahunAjaranId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPtkBarusRelatedByTahunAjaranId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPtkBaruRelatedByTahunAjaranId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getDemografisRelatedByTahunAjaranId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addDemografiRelatedByTahunAjaranId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getDemografisRelatedByTahunAjaranId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addDemografiRelatedByTahunAjaranId($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setTahunAjaranId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return TahunAjaran Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return TahunAjaranPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new TahunAjaranPeer();
        }

        return self::$peer;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('TemplateUn' == $relationName) {
            $this->initTemplateUns();
        }
        if ('BeasiswaPesertaDidikRelatedByTahunSelesai' == $relationName) {
            $this->initBeasiswaPesertaDidiksRelatedByTahunSelesai();
        }
        if ('BeasiswaPesertaDidikRelatedByTahunMulai' == $relationName) {
            $this->initBeasiswaPesertaDidiksRelatedByTahunMulai();
        }
        if ('BeasiswaPesertaDidikRelatedByTahunSelesai' == $relationName) {
            $this->initBeasiswaPesertaDidiksRelatedByTahunSelesai();
        }
        if ('BeasiswaPesertaDidikRelatedByTahunMulai' == $relationName) {
            $this->initBeasiswaPesertaDidiksRelatedByTahunMulai();
        }
        if ('PesertaDidikBaruRelatedByTahunAjaranId' == $relationName) {
            $this->initPesertaDidikBarusRelatedByTahunAjaranId();
        }
        if ('PesertaDidikBaruRelatedByTahunAjaranId' == $relationName) {
            $this->initPesertaDidikBarusRelatedByTahunAjaranId();
        }
        if ('PtkTerdaftarRelatedByTahunAjaranId' == $relationName) {
            $this->initPtkTerdaftarsRelatedByTahunAjaranId();
        }
        if ('PtkTerdaftarRelatedByTahunAjaranId' == $relationName) {
            $this->initPtkTerdaftarsRelatedByTahunAjaranId();
        }
        if ('Semester' == $relationName) {
            $this->initSemesters();
        }
        if ('PengawasTerdaftarRelatedByTahunAjaranId' == $relationName) {
            $this->initPengawasTerdaftarsRelatedByTahunAjaranId();
        }
        if ('PengawasTerdaftarRelatedByTahunAjaranId' == $relationName) {
            $this->initPengawasTerdaftarsRelatedByTahunAjaranId();
        }
        if ('PtkBaruRelatedByTahunAjaranId' == $relationName) {
            $this->initPtkBarusRelatedByTahunAjaranId();
        }
        if ('PtkBaruRelatedByTahunAjaranId' == $relationName) {
            $this->initPtkBarusRelatedByTahunAjaranId();
        }
        if ('DemografiRelatedByTahunAjaranId' == $relationName) {
            $this->initDemografisRelatedByTahunAjaranId();
        }
        if ('DemografiRelatedByTahunAjaranId' == $relationName) {
            $this->initDemografisRelatedByTahunAjaranId();
        }
    }

    /**
     * Clears out the collTemplateUns collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return TahunAjaran The current object (for fluent API support)
     * @see        addTemplateUns()
     */
    public function clearTemplateUns()
    {
        $this->collTemplateUns = null; // important to set this to null since that means it is uninitialized
        $this->collTemplateUnsPartial = null;

        return $this;
    }

    /**
     * reset is the collTemplateUns collection loaded partially
     *
     * @return void
     */
    public function resetPartialTemplateUns($v = true)
    {
        $this->collTemplateUnsPartial = $v;
    }

    /**
     * Initializes the collTemplateUns collection.
     *
     * By default this just sets the collTemplateUns collection to an empty array (like clearcollTemplateUns());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initTemplateUns($overrideExisting = true)
    {
        if (null !== $this->collTemplateUns && !$overrideExisting) {
            return;
        }
        $this->collTemplateUns = new PropelObjectCollection();
        $this->collTemplateUns->setModel('TemplateUn');
    }

    /**
     * Gets an array of TemplateUn objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this TahunAjaran is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|TemplateUn[] List of TemplateUn objects
     * @throws PropelException
     */
    public function getTemplateUns($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collTemplateUnsPartial && !$this->isNew();
        if (null === $this->collTemplateUns || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collTemplateUns) {
                // return empty collection
                $this->initTemplateUns();
            } else {
                $collTemplateUns = TemplateUnQuery::create(null, $criteria)
                    ->filterByTahunAjaran($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collTemplateUnsPartial && count($collTemplateUns)) {
                      $this->initTemplateUns(false);

                      foreach($collTemplateUns as $obj) {
                        if (false == $this->collTemplateUns->contains($obj)) {
                          $this->collTemplateUns->append($obj);
                        }
                      }

                      $this->collTemplateUnsPartial = true;
                    }

                    $collTemplateUns->getInternalIterator()->rewind();
                    return $collTemplateUns;
                }

                if($partial && $this->collTemplateUns) {
                    foreach($this->collTemplateUns as $obj) {
                        if($obj->isNew()) {
                            $collTemplateUns[] = $obj;
                        }
                    }
                }

                $this->collTemplateUns = $collTemplateUns;
                $this->collTemplateUnsPartial = false;
            }
        }

        return $this->collTemplateUns;
    }

    /**
     * Sets a collection of TemplateUn objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $templateUns A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function setTemplateUns(PropelCollection $templateUns, PropelPDO $con = null)
    {
        $templateUnsToDelete = $this->getTemplateUns(new Criteria(), $con)->diff($templateUns);

        $this->templateUnsScheduledForDeletion = unserialize(serialize($templateUnsToDelete));

        foreach ($templateUnsToDelete as $templateUnRemoved) {
            $templateUnRemoved->setTahunAjaran(null);
        }

        $this->collTemplateUns = null;
        foreach ($templateUns as $templateUn) {
            $this->addTemplateUn($templateUn);
        }

        $this->collTemplateUns = $templateUns;
        $this->collTemplateUnsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related TemplateUn objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related TemplateUn objects.
     * @throws PropelException
     */
    public function countTemplateUns(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collTemplateUnsPartial && !$this->isNew();
        if (null === $this->collTemplateUns || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collTemplateUns) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getTemplateUns());
            }
            $query = TemplateUnQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByTahunAjaran($this)
                ->count($con);
        }

        return count($this->collTemplateUns);
    }

    /**
     * Method called to associate a TemplateUn object to this object
     * through the TemplateUn foreign key attribute.
     *
     * @param    TemplateUn $l TemplateUn
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function addTemplateUn(TemplateUn $l)
    {
        if ($this->collTemplateUns === null) {
            $this->initTemplateUns();
            $this->collTemplateUnsPartial = true;
        }
        if (!in_array($l, $this->collTemplateUns->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddTemplateUn($l);
        }

        return $this;
    }

    /**
     * @param	TemplateUn $templateUn The templateUn object to add.
     */
    protected function doAddTemplateUn($templateUn)
    {
        $this->collTemplateUns[]= $templateUn;
        $templateUn->setTahunAjaran($this);
    }

    /**
     * @param	TemplateUn $templateUn The templateUn object to remove.
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function removeTemplateUn($templateUn)
    {
        if ($this->getTemplateUns()->contains($templateUn)) {
            $this->collTemplateUns->remove($this->collTemplateUns->search($templateUn));
            if (null === $this->templateUnsScheduledForDeletion) {
                $this->templateUnsScheduledForDeletion = clone $this->collTemplateUns;
                $this->templateUnsScheduledForDeletion->clear();
            }
            $this->templateUnsScheduledForDeletion[]= clone $templateUn;
            $templateUn->setTahunAjaran(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related TemplateUns from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|TemplateUn[] List of TemplateUn objects
     */
    public function getTemplateUnsJoinJenjangPendidikan($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = TemplateUnQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikan', $join_behavior);

        return $this->getTemplateUns($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related TemplateUns from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|TemplateUn[] List of TemplateUn objects
     */
    public function getTemplateUnsJoinJurusan($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = TemplateUnQuery::create(null, $criteria);
        $query->joinWith('Jurusan', $join_behavior);

        return $this->getTemplateUns($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related TemplateUns from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|TemplateUn[] List of TemplateUn objects
     */
    public function getTemplateUnsJoinMataPelajaranRelatedByMp3Id($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = TemplateUnQuery::create(null, $criteria);
        $query->joinWith('MataPelajaranRelatedByMp3Id', $join_behavior);

        return $this->getTemplateUns($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related TemplateUns from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|TemplateUn[] List of TemplateUn objects
     */
    public function getTemplateUnsJoinMataPelajaranRelatedByMp4Id($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = TemplateUnQuery::create(null, $criteria);
        $query->joinWith('MataPelajaranRelatedByMp4Id', $join_behavior);

        return $this->getTemplateUns($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related TemplateUns from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|TemplateUn[] List of TemplateUn objects
     */
    public function getTemplateUnsJoinMataPelajaranRelatedByMp7Id($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = TemplateUnQuery::create(null, $criteria);
        $query->joinWith('MataPelajaranRelatedByMp7Id', $join_behavior);

        return $this->getTemplateUns($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related TemplateUns from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|TemplateUn[] List of TemplateUn objects
     */
    public function getTemplateUnsJoinMataPelajaranRelatedByMp5Id($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = TemplateUnQuery::create(null, $criteria);
        $query->joinWith('MataPelajaranRelatedByMp5Id', $join_behavior);

        return $this->getTemplateUns($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related TemplateUns from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|TemplateUn[] List of TemplateUn objects
     */
    public function getTemplateUnsJoinMataPelajaranRelatedByMp1Id($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = TemplateUnQuery::create(null, $criteria);
        $query->joinWith('MataPelajaranRelatedByMp1Id', $join_behavior);

        return $this->getTemplateUns($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related TemplateUns from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|TemplateUn[] List of TemplateUn objects
     */
    public function getTemplateUnsJoinMataPelajaranRelatedByMp2Id($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = TemplateUnQuery::create(null, $criteria);
        $query->joinWith('MataPelajaranRelatedByMp2Id', $join_behavior);

        return $this->getTemplateUns($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related TemplateUns from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|TemplateUn[] List of TemplateUn objects
     */
    public function getTemplateUnsJoinMataPelajaranRelatedByMp6Id($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = TemplateUnQuery::create(null, $criteria);
        $query->joinWith('MataPelajaranRelatedByMp6Id', $join_behavior);

        return $this->getTemplateUns($query, $con);
    }

    /**
     * Clears out the collBeasiswaPesertaDidiksRelatedByTahunSelesai collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return TahunAjaran The current object (for fluent API support)
     * @see        addBeasiswaPesertaDidiksRelatedByTahunSelesai()
     */
    public function clearBeasiswaPesertaDidiksRelatedByTahunSelesai()
    {
        $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai = null; // important to set this to null since that means it is uninitialized
        $this->collBeasiswaPesertaDidiksRelatedByTahunSelesaiPartial = null;

        return $this;
    }

    /**
     * reset is the collBeasiswaPesertaDidiksRelatedByTahunSelesai collection loaded partially
     *
     * @return void
     */
    public function resetPartialBeasiswaPesertaDidiksRelatedByTahunSelesai($v = true)
    {
        $this->collBeasiswaPesertaDidiksRelatedByTahunSelesaiPartial = $v;
    }

    /**
     * Initializes the collBeasiswaPesertaDidiksRelatedByTahunSelesai collection.
     *
     * By default this just sets the collBeasiswaPesertaDidiksRelatedByTahunSelesai collection to an empty array (like clearcollBeasiswaPesertaDidiksRelatedByTahunSelesai());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBeasiswaPesertaDidiksRelatedByTahunSelesai($overrideExisting = true)
    {
        if (null !== $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai && !$overrideExisting) {
            return;
        }
        $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai = new PropelObjectCollection();
        $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai->setModel('BeasiswaPesertaDidik');
    }

    /**
     * Gets an array of BeasiswaPesertaDidik objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this TahunAjaran is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|BeasiswaPesertaDidik[] List of BeasiswaPesertaDidik objects
     * @throws PropelException
     */
    public function getBeasiswaPesertaDidiksRelatedByTahunSelesai($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collBeasiswaPesertaDidiksRelatedByTahunSelesaiPartial && !$this->isNew();
        if (null === $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai) {
                // return empty collection
                $this->initBeasiswaPesertaDidiksRelatedByTahunSelesai();
            } else {
                $collBeasiswaPesertaDidiksRelatedByTahunSelesai = BeasiswaPesertaDidikQuery::create(null, $criteria)
                    ->filterByTahunAjaranRelatedByTahunSelesai($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collBeasiswaPesertaDidiksRelatedByTahunSelesaiPartial && count($collBeasiswaPesertaDidiksRelatedByTahunSelesai)) {
                      $this->initBeasiswaPesertaDidiksRelatedByTahunSelesai(false);

                      foreach($collBeasiswaPesertaDidiksRelatedByTahunSelesai as $obj) {
                        if (false == $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai->contains($obj)) {
                          $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai->append($obj);
                        }
                      }

                      $this->collBeasiswaPesertaDidiksRelatedByTahunSelesaiPartial = true;
                    }

                    $collBeasiswaPesertaDidiksRelatedByTahunSelesai->getInternalIterator()->rewind();
                    return $collBeasiswaPesertaDidiksRelatedByTahunSelesai;
                }

                if($partial && $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai) {
                    foreach($this->collBeasiswaPesertaDidiksRelatedByTahunSelesai as $obj) {
                        if($obj->isNew()) {
                            $collBeasiswaPesertaDidiksRelatedByTahunSelesai[] = $obj;
                        }
                    }
                }

                $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai = $collBeasiswaPesertaDidiksRelatedByTahunSelesai;
                $this->collBeasiswaPesertaDidiksRelatedByTahunSelesaiPartial = false;
            }
        }

        return $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai;
    }

    /**
     * Sets a collection of BeasiswaPesertaDidikRelatedByTahunSelesai objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $beasiswaPesertaDidiksRelatedByTahunSelesai A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function setBeasiswaPesertaDidiksRelatedByTahunSelesai(PropelCollection $beasiswaPesertaDidiksRelatedByTahunSelesai, PropelPDO $con = null)
    {
        $beasiswaPesertaDidiksRelatedByTahunSelesaiToDelete = $this->getBeasiswaPesertaDidiksRelatedByTahunSelesai(new Criteria(), $con)->diff($beasiswaPesertaDidiksRelatedByTahunSelesai);

        $this->beasiswaPesertaDidiksRelatedByTahunSelesaiScheduledForDeletion = unserialize(serialize($beasiswaPesertaDidiksRelatedByTahunSelesaiToDelete));

        foreach ($beasiswaPesertaDidiksRelatedByTahunSelesaiToDelete as $beasiswaPesertaDidikRelatedByTahunSelesaiRemoved) {
            $beasiswaPesertaDidikRelatedByTahunSelesaiRemoved->setTahunAjaranRelatedByTahunSelesai(null);
        }

        $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai = null;
        foreach ($beasiswaPesertaDidiksRelatedByTahunSelesai as $beasiswaPesertaDidikRelatedByTahunSelesai) {
            $this->addBeasiswaPesertaDidikRelatedByTahunSelesai($beasiswaPesertaDidikRelatedByTahunSelesai);
        }

        $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai = $beasiswaPesertaDidiksRelatedByTahunSelesai;
        $this->collBeasiswaPesertaDidiksRelatedByTahunSelesaiPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BeasiswaPesertaDidik objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related BeasiswaPesertaDidik objects.
     * @throws PropelException
     */
    public function countBeasiswaPesertaDidiksRelatedByTahunSelesai(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collBeasiswaPesertaDidiksRelatedByTahunSelesaiPartial && !$this->isNew();
        if (null === $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getBeasiswaPesertaDidiksRelatedByTahunSelesai());
            }
            $query = BeasiswaPesertaDidikQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByTahunAjaranRelatedByTahunSelesai($this)
                ->count($con);
        }

        return count($this->collBeasiswaPesertaDidiksRelatedByTahunSelesai);
    }

    /**
     * Method called to associate a BeasiswaPesertaDidik object to this object
     * through the BeasiswaPesertaDidik foreign key attribute.
     *
     * @param    BeasiswaPesertaDidik $l BeasiswaPesertaDidik
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function addBeasiswaPesertaDidikRelatedByTahunSelesai(BeasiswaPesertaDidik $l)
    {
        if ($this->collBeasiswaPesertaDidiksRelatedByTahunSelesai === null) {
            $this->initBeasiswaPesertaDidiksRelatedByTahunSelesai();
            $this->collBeasiswaPesertaDidiksRelatedByTahunSelesaiPartial = true;
        }
        if (!in_array($l, $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddBeasiswaPesertaDidikRelatedByTahunSelesai($l);
        }

        return $this;
    }

    /**
     * @param	BeasiswaPesertaDidikRelatedByTahunSelesai $beasiswaPesertaDidikRelatedByTahunSelesai The beasiswaPesertaDidikRelatedByTahunSelesai object to add.
     */
    protected function doAddBeasiswaPesertaDidikRelatedByTahunSelesai($beasiswaPesertaDidikRelatedByTahunSelesai)
    {
        $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai[]= $beasiswaPesertaDidikRelatedByTahunSelesai;
        $beasiswaPesertaDidikRelatedByTahunSelesai->setTahunAjaranRelatedByTahunSelesai($this);
    }

    /**
     * @param	BeasiswaPesertaDidikRelatedByTahunSelesai $beasiswaPesertaDidikRelatedByTahunSelesai The beasiswaPesertaDidikRelatedByTahunSelesai object to remove.
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function removeBeasiswaPesertaDidikRelatedByTahunSelesai($beasiswaPesertaDidikRelatedByTahunSelesai)
    {
        if ($this->getBeasiswaPesertaDidiksRelatedByTahunSelesai()->contains($beasiswaPesertaDidikRelatedByTahunSelesai)) {
            $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai->remove($this->collBeasiswaPesertaDidiksRelatedByTahunSelesai->search($beasiswaPesertaDidikRelatedByTahunSelesai));
            if (null === $this->beasiswaPesertaDidiksRelatedByTahunSelesaiScheduledForDeletion) {
                $this->beasiswaPesertaDidiksRelatedByTahunSelesaiScheduledForDeletion = clone $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai;
                $this->beasiswaPesertaDidiksRelatedByTahunSelesaiScheduledForDeletion->clear();
            }
            $this->beasiswaPesertaDidiksRelatedByTahunSelesaiScheduledForDeletion[]= clone $beasiswaPesertaDidikRelatedByTahunSelesai;
            $beasiswaPesertaDidikRelatedByTahunSelesai->setTahunAjaranRelatedByTahunSelesai(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related BeasiswaPesertaDidiksRelatedByTahunSelesai from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BeasiswaPesertaDidik[] List of BeasiswaPesertaDidik objects
     */
    public function getBeasiswaPesertaDidiksRelatedByTahunSelesaiJoinPesertaDidikRelatedByPesertaDidikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BeasiswaPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PesertaDidikRelatedByPesertaDidikId', $join_behavior);

        return $this->getBeasiswaPesertaDidiksRelatedByTahunSelesai($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related BeasiswaPesertaDidiksRelatedByTahunSelesai from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BeasiswaPesertaDidik[] List of BeasiswaPesertaDidik objects
     */
    public function getBeasiswaPesertaDidiksRelatedByTahunSelesaiJoinPesertaDidikRelatedByPesertaDidikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BeasiswaPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PesertaDidikRelatedByPesertaDidikId', $join_behavior);

        return $this->getBeasiswaPesertaDidiksRelatedByTahunSelesai($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related BeasiswaPesertaDidiksRelatedByTahunSelesai from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BeasiswaPesertaDidik[] List of BeasiswaPesertaDidik objects
     */
    public function getBeasiswaPesertaDidiksRelatedByTahunSelesaiJoinJenisBeasiswaRelatedByJenisBeasiswaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BeasiswaPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisBeasiswaRelatedByJenisBeasiswaId', $join_behavior);

        return $this->getBeasiswaPesertaDidiksRelatedByTahunSelesai($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related BeasiswaPesertaDidiksRelatedByTahunSelesai from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BeasiswaPesertaDidik[] List of BeasiswaPesertaDidik objects
     */
    public function getBeasiswaPesertaDidiksRelatedByTahunSelesaiJoinJenisBeasiswaRelatedByJenisBeasiswaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BeasiswaPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisBeasiswaRelatedByJenisBeasiswaId', $join_behavior);

        return $this->getBeasiswaPesertaDidiksRelatedByTahunSelesai($query, $con);
    }

    /**
     * Clears out the collBeasiswaPesertaDidiksRelatedByTahunMulai collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return TahunAjaran The current object (for fluent API support)
     * @see        addBeasiswaPesertaDidiksRelatedByTahunMulai()
     */
    public function clearBeasiswaPesertaDidiksRelatedByTahunMulai()
    {
        $this->collBeasiswaPesertaDidiksRelatedByTahunMulai = null; // important to set this to null since that means it is uninitialized
        $this->collBeasiswaPesertaDidiksRelatedByTahunMulaiPartial = null;

        return $this;
    }

    /**
     * reset is the collBeasiswaPesertaDidiksRelatedByTahunMulai collection loaded partially
     *
     * @return void
     */
    public function resetPartialBeasiswaPesertaDidiksRelatedByTahunMulai($v = true)
    {
        $this->collBeasiswaPesertaDidiksRelatedByTahunMulaiPartial = $v;
    }

    /**
     * Initializes the collBeasiswaPesertaDidiksRelatedByTahunMulai collection.
     *
     * By default this just sets the collBeasiswaPesertaDidiksRelatedByTahunMulai collection to an empty array (like clearcollBeasiswaPesertaDidiksRelatedByTahunMulai());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBeasiswaPesertaDidiksRelatedByTahunMulai($overrideExisting = true)
    {
        if (null !== $this->collBeasiswaPesertaDidiksRelatedByTahunMulai && !$overrideExisting) {
            return;
        }
        $this->collBeasiswaPesertaDidiksRelatedByTahunMulai = new PropelObjectCollection();
        $this->collBeasiswaPesertaDidiksRelatedByTahunMulai->setModel('BeasiswaPesertaDidik');
    }

    /**
     * Gets an array of BeasiswaPesertaDidik objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this TahunAjaran is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|BeasiswaPesertaDidik[] List of BeasiswaPesertaDidik objects
     * @throws PropelException
     */
    public function getBeasiswaPesertaDidiksRelatedByTahunMulai($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collBeasiswaPesertaDidiksRelatedByTahunMulaiPartial && !$this->isNew();
        if (null === $this->collBeasiswaPesertaDidiksRelatedByTahunMulai || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBeasiswaPesertaDidiksRelatedByTahunMulai) {
                // return empty collection
                $this->initBeasiswaPesertaDidiksRelatedByTahunMulai();
            } else {
                $collBeasiswaPesertaDidiksRelatedByTahunMulai = BeasiswaPesertaDidikQuery::create(null, $criteria)
                    ->filterByTahunAjaranRelatedByTahunMulai($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collBeasiswaPesertaDidiksRelatedByTahunMulaiPartial && count($collBeasiswaPesertaDidiksRelatedByTahunMulai)) {
                      $this->initBeasiswaPesertaDidiksRelatedByTahunMulai(false);

                      foreach($collBeasiswaPesertaDidiksRelatedByTahunMulai as $obj) {
                        if (false == $this->collBeasiswaPesertaDidiksRelatedByTahunMulai->contains($obj)) {
                          $this->collBeasiswaPesertaDidiksRelatedByTahunMulai->append($obj);
                        }
                      }

                      $this->collBeasiswaPesertaDidiksRelatedByTahunMulaiPartial = true;
                    }

                    $collBeasiswaPesertaDidiksRelatedByTahunMulai->getInternalIterator()->rewind();
                    return $collBeasiswaPesertaDidiksRelatedByTahunMulai;
                }

                if($partial && $this->collBeasiswaPesertaDidiksRelatedByTahunMulai) {
                    foreach($this->collBeasiswaPesertaDidiksRelatedByTahunMulai as $obj) {
                        if($obj->isNew()) {
                            $collBeasiswaPesertaDidiksRelatedByTahunMulai[] = $obj;
                        }
                    }
                }

                $this->collBeasiswaPesertaDidiksRelatedByTahunMulai = $collBeasiswaPesertaDidiksRelatedByTahunMulai;
                $this->collBeasiswaPesertaDidiksRelatedByTahunMulaiPartial = false;
            }
        }

        return $this->collBeasiswaPesertaDidiksRelatedByTahunMulai;
    }

    /**
     * Sets a collection of BeasiswaPesertaDidikRelatedByTahunMulai objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $beasiswaPesertaDidiksRelatedByTahunMulai A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function setBeasiswaPesertaDidiksRelatedByTahunMulai(PropelCollection $beasiswaPesertaDidiksRelatedByTahunMulai, PropelPDO $con = null)
    {
        $beasiswaPesertaDidiksRelatedByTahunMulaiToDelete = $this->getBeasiswaPesertaDidiksRelatedByTahunMulai(new Criteria(), $con)->diff($beasiswaPesertaDidiksRelatedByTahunMulai);

        $this->beasiswaPesertaDidiksRelatedByTahunMulaiScheduledForDeletion = unserialize(serialize($beasiswaPesertaDidiksRelatedByTahunMulaiToDelete));

        foreach ($beasiswaPesertaDidiksRelatedByTahunMulaiToDelete as $beasiswaPesertaDidikRelatedByTahunMulaiRemoved) {
            $beasiswaPesertaDidikRelatedByTahunMulaiRemoved->setTahunAjaranRelatedByTahunMulai(null);
        }

        $this->collBeasiswaPesertaDidiksRelatedByTahunMulai = null;
        foreach ($beasiswaPesertaDidiksRelatedByTahunMulai as $beasiswaPesertaDidikRelatedByTahunMulai) {
            $this->addBeasiswaPesertaDidikRelatedByTahunMulai($beasiswaPesertaDidikRelatedByTahunMulai);
        }

        $this->collBeasiswaPesertaDidiksRelatedByTahunMulai = $beasiswaPesertaDidiksRelatedByTahunMulai;
        $this->collBeasiswaPesertaDidiksRelatedByTahunMulaiPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BeasiswaPesertaDidik objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related BeasiswaPesertaDidik objects.
     * @throws PropelException
     */
    public function countBeasiswaPesertaDidiksRelatedByTahunMulai(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collBeasiswaPesertaDidiksRelatedByTahunMulaiPartial && !$this->isNew();
        if (null === $this->collBeasiswaPesertaDidiksRelatedByTahunMulai || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBeasiswaPesertaDidiksRelatedByTahunMulai) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getBeasiswaPesertaDidiksRelatedByTahunMulai());
            }
            $query = BeasiswaPesertaDidikQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByTahunAjaranRelatedByTahunMulai($this)
                ->count($con);
        }

        return count($this->collBeasiswaPesertaDidiksRelatedByTahunMulai);
    }

    /**
     * Method called to associate a BeasiswaPesertaDidik object to this object
     * through the BeasiswaPesertaDidik foreign key attribute.
     *
     * @param    BeasiswaPesertaDidik $l BeasiswaPesertaDidik
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function addBeasiswaPesertaDidikRelatedByTahunMulai(BeasiswaPesertaDidik $l)
    {
        if ($this->collBeasiswaPesertaDidiksRelatedByTahunMulai === null) {
            $this->initBeasiswaPesertaDidiksRelatedByTahunMulai();
            $this->collBeasiswaPesertaDidiksRelatedByTahunMulaiPartial = true;
        }
        if (!in_array($l, $this->collBeasiswaPesertaDidiksRelatedByTahunMulai->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddBeasiswaPesertaDidikRelatedByTahunMulai($l);
        }

        return $this;
    }

    /**
     * @param	BeasiswaPesertaDidikRelatedByTahunMulai $beasiswaPesertaDidikRelatedByTahunMulai The beasiswaPesertaDidikRelatedByTahunMulai object to add.
     */
    protected function doAddBeasiswaPesertaDidikRelatedByTahunMulai($beasiswaPesertaDidikRelatedByTahunMulai)
    {
        $this->collBeasiswaPesertaDidiksRelatedByTahunMulai[]= $beasiswaPesertaDidikRelatedByTahunMulai;
        $beasiswaPesertaDidikRelatedByTahunMulai->setTahunAjaranRelatedByTahunMulai($this);
    }

    /**
     * @param	BeasiswaPesertaDidikRelatedByTahunMulai $beasiswaPesertaDidikRelatedByTahunMulai The beasiswaPesertaDidikRelatedByTahunMulai object to remove.
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function removeBeasiswaPesertaDidikRelatedByTahunMulai($beasiswaPesertaDidikRelatedByTahunMulai)
    {
        if ($this->getBeasiswaPesertaDidiksRelatedByTahunMulai()->contains($beasiswaPesertaDidikRelatedByTahunMulai)) {
            $this->collBeasiswaPesertaDidiksRelatedByTahunMulai->remove($this->collBeasiswaPesertaDidiksRelatedByTahunMulai->search($beasiswaPesertaDidikRelatedByTahunMulai));
            if (null === $this->beasiswaPesertaDidiksRelatedByTahunMulaiScheduledForDeletion) {
                $this->beasiswaPesertaDidiksRelatedByTahunMulaiScheduledForDeletion = clone $this->collBeasiswaPesertaDidiksRelatedByTahunMulai;
                $this->beasiswaPesertaDidiksRelatedByTahunMulaiScheduledForDeletion->clear();
            }
            $this->beasiswaPesertaDidiksRelatedByTahunMulaiScheduledForDeletion[]= clone $beasiswaPesertaDidikRelatedByTahunMulai;
            $beasiswaPesertaDidikRelatedByTahunMulai->setTahunAjaranRelatedByTahunMulai(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related BeasiswaPesertaDidiksRelatedByTahunMulai from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BeasiswaPesertaDidik[] List of BeasiswaPesertaDidik objects
     */
    public function getBeasiswaPesertaDidiksRelatedByTahunMulaiJoinPesertaDidikRelatedByPesertaDidikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BeasiswaPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PesertaDidikRelatedByPesertaDidikId', $join_behavior);

        return $this->getBeasiswaPesertaDidiksRelatedByTahunMulai($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related BeasiswaPesertaDidiksRelatedByTahunMulai from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BeasiswaPesertaDidik[] List of BeasiswaPesertaDidik objects
     */
    public function getBeasiswaPesertaDidiksRelatedByTahunMulaiJoinPesertaDidikRelatedByPesertaDidikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BeasiswaPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PesertaDidikRelatedByPesertaDidikId', $join_behavior);

        return $this->getBeasiswaPesertaDidiksRelatedByTahunMulai($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related BeasiswaPesertaDidiksRelatedByTahunMulai from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BeasiswaPesertaDidik[] List of BeasiswaPesertaDidik objects
     */
    public function getBeasiswaPesertaDidiksRelatedByTahunMulaiJoinJenisBeasiswaRelatedByJenisBeasiswaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BeasiswaPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisBeasiswaRelatedByJenisBeasiswaId', $join_behavior);

        return $this->getBeasiswaPesertaDidiksRelatedByTahunMulai($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related BeasiswaPesertaDidiksRelatedByTahunMulai from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BeasiswaPesertaDidik[] List of BeasiswaPesertaDidik objects
     */
    public function getBeasiswaPesertaDidiksRelatedByTahunMulaiJoinJenisBeasiswaRelatedByJenisBeasiswaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BeasiswaPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisBeasiswaRelatedByJenisBeasiswaId', $join_behavior);

        return $this->getBeasiswaPesertaDidiksRelatedByTahunMulai($query, $con);
    }

    /**
     * Clears out the collBeasiswaPesertaDidiksRelatedByTahunSelesai collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return TahunAjaran The current object (for fluent API support)
     * @see        addBeasiswaPesertaDidiksRelatedByTahunSelesai()
     */
    public function clearBeasiswaPesertaDidiksRelatedByTahunSelesai()
    {
        $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai = null; // important to set this to null since that means it is uninitialized
        $this->collBeasiswaPesertaDidiksRelatedByTahunSelesaiPartial = null;

        return $this;
    }

    /**
     * reset is the collBeasiswaPesertaDidiksRelatedByTahunSelesai collection loaded partially
     *
     * @return void
     */
    public function resetPartialBeasiswaPesertaDidiksRelatedByTahunSelesai($v = true)
    {
        $this->collBeasiswaPesertaDidiksRelatedByTahunSelesaiPartial = $v;
    }

    /**
     * Initializes the collBeasiswaPesertaDidiksRelatedByTahunSelesai collection.
     *
     * By default this just sets the collBeasiswaPesertaDidiksRelatedByTahunSelesai collection to an empty array (like clearcollBeasiswaPesertaDidiksRelatedByTahunSelesai());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBeasiswaPesertaDidiksRelatedByTahunSelesai($overrideExisting = true)
    {
        if (null !== $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai && !$overrideExisting) {
            return;
        }
        $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai = new PropelObjectCollection();
        $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai->setModel('BeasiswaPesertaDidik');
    }

    /**
     * Gets an array of BeasiswaPesertaDidik objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this TahunAjaran is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|BeasiswaPesertaDidik[] List of BeasiswaPesertaDidik objects
     * @throws PropelException
     */
    public function getBeasiswaPesertaDidiksRelatedByTahunSelesai($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collBeasiswaPesertaDidiksRelatedByTahunSelesaiPartial && !$this->isNew();
        if (null === $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai) {
                // return empty collection
                $this->initBeasiswaPesertaDidiksRelatedByTahunSelesai();
            } else {
                $collBeasiswaPesertaDidiksRelatedByTahunSelesai = BeasiswaPesertaDidikQuery::create(null, $criteria)
                    ->filterByTahunAjaranRelatedByTahunSelesai($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collBeasiswaPesertaDidiksRelatedByTahunSelesaiPartial && count($collBeasiswaPesertaDidiksRelatedByTahunSelesai)) {
                      $this->initBeasiswaPesertaDidiksRelatedByTahunSelesai(false);

                      foreach($collBeasiswaPesertaDidiksRelatedByTahunSelesai as $obj) {
                        if (false == $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai->contains($obj)) {
                          $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai->append($obj);
                        }
                      }

                      $this->collBeasiswaPesertaDidiksRelatedByTahunSelesaiPartial = true;
                    }

                    $collBeasiswaPesertaDidiksRelatedByTahunSelesai->getInternalIterator()->rewind();
                    return $collBeasiswaPesertaDidiksRelatedByTahunSelesai;
                }

                if($partial && $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai) {
                    foreach($this->collBeasiswaPesertaDidiksRelatedByTahunSelesai as $obj) {
                        if($obj->isNew()) {
                            $collBeasiswaPesertaDidiksRelatedByTahunSelesai[] = $obj;
                        }
                    }
                }

                $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai = $collBeasiswaPesertaDidiksRelatedByTahunSelesai;
                $this->collBeasiswaPesertaDidiksRelatedByTahunSelesaiPartial = false;
            }
        }

        return $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai;
    }

    /**
     * Sets a collection of BeasiswaPesertaDidikRelatedByTahunSelesai objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $beasiswaPesertaDidiksRelatedByTahunSelesai A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function setBeasiswaPesertaDidiksRelatedByTahunSelesai(PropelCollection $beasiswaPesertaDidiksRelatedByTahunSelesai, PropelPDO $con = null)
    {
        $beasiswaPesertaDidiksRelatedByTahunSelesaiToDelete = $this->getBeasiswaPesertaDidiksRelatedByTahunSelesai(new Criteria(), $con)->diff($beasiswaPesertaDidiksRelatedByTahunSelesai);

        $this->beasiswaPesertaDidiksRelatedByTahunSelesaiScheduledForDeletion = unserialize(serialize($beasiswaPesertaDidiksRelatedByTahunSelesaiToDelete));

        foreach ($beasiswaPesertaDidiksRelatedByTahunSelesaiToDelete as $beasiswaPesertaDidikRelatedByTahunSelesaiRemoved) {
            $beasiswaPesertaDidikRelatedByTahunSelesaiRemoved->setTahunAjaranRelatedByTahunSelesai(null);
        }

        $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai = null;
        foreach ($beasiswaPesertaDidiksRelatedByTahunSelesai as $beasiswaPesertaDidikRelatedByTahunSelesai) {
            $this->addBeasiswaPesertaDidikRelatedByTahunSelesai($beasiswaPesertaDidikRelatedByTahunSelesai);
        }

        $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai = $beasiswaPesertaDidiksRelatedByTahunSelesai;
        $this->collBeasiswaPesertaDidiksRelatedByTahunSelesaiPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BeasiswaPesertaDidik objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related BeasiswaPesertaDidik objects.
     * @throws PropelException
     */
    public function countBeasiswaPesertaDidiksRelatedByTahunSelesai(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collBeasiswaPesertaDidiksRelatedByTahunSelesaiPartial && !$this->isNew();
        if (null === $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getBeasiswaPesertaDidiksRelatedByTahunSelesai());
            }
            $query = BeasiswaPesertaDidikQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByTahunAjaranRelatedByTahunSelesai($this)
                ->count($con);
        }

        return count($this->collBeasiswaPesertaDidiksRelatedByTahunSelesai);
    }

    /**
     * Method called to associate a BeasiswaPesertaDidik object to this object
     * through the BeasiswaPesertaDidik foreign key attribute.
     *
     * @param    BeasiswaPesertaDidik $l BeasiswaPesertaDidik
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function addBeasiswaPesertaDidikRelatedByTahunSelesai(BeasiswaPesertaDidik $l)
    {
        if ($this->collBeasiswaPesertaDidiksRelatedByTahunSelesai === null) {
            $this->initBeasiswaPesertaDidiksRelatedByTahunSelesai();
            $this->collBeasiswaPesertaDidiksRelatedByTahunSelesaiPartial = true;
        }
        if (!in_array($l, $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddBeasiswaPesertaDidikRelatedByTahunSelesai($l);
        }

        return $this;
    }

    /**
     * @param	BeasiswaPesertaDidikRelatedByTahunSelesai $beasiswaPesertaDidikRelatedByTahunSelesai The beasiswaPesertaDidikRelatedByTahunSelesai object to add.
     */
    protected function doAddBeasiswaPesertaDidikRelatedByTahunSelesai($beasiswaPesertaDidikRelatedByTahunSelesai)
    {
        $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai[]= $beasiswaPesertaDidikRelatedByTahunSelesai;
        $beasiswaPesertaDidikRelatedByTahunSelesai->setTahunAjaranRelatedByTahunSelesai($this);
    }

    /**
     * @param	BeasiswaPesertaDidikRelatedByTahunSelesai $beasiswaPesertaDidikRelatedByTahunSelesai The beasiswaPesertaDidikRelatedByTahunSelesai object to remove.
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function removeBeasiswaPesertaDidikRelatedByTahunSelesai($beasiswaPesertaDidikRelatedByTahunSelesai)
    {
        if ($this->getBeasiswaPesertaDidiksRelatedByTahunSelesai()->contains($beasiswaPesertaDidikRelatedByTahunSelesai)) {
            $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai->remove($this->collBeasiswaPesertaDidiksRelatedByTahunSelesai->search($beasiswaPesertaDidikRelatedByTahunSelesai));
            if (null === $this->beasiswaPesertaDidiksRelatedByTahunSelesaiScheduledForDeletion) {
                $this->beasiswaPesertaDidiksRelatedByTahunSelesaiScheduledForDeletion = clone $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai;
                $this->beasiswaPesertaDidiksRelatedByTahunSelesaiScheduledForDeletion->clear();
            }
            $this->beasiswaPesertaDidiksRelatedByTahunSelesaiScheduledForDeletion[]= clone $beasiswaPesertaDidikRelatedByTahunSelesai;
            $beasiswaPesertaDidikRelatedByTahunSelesai->setTahunAjaranRelatedByTahunSelesai(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related BeasiswaPesertaDidiksRelatedByTahunSelesai from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BeasiswaPesertaDidik[] List of BeasiswaPesertaDidik objects
     */
    public function getBeasiswaPesertaDidiksRelatedByTahunSelesaiJoinPesertaDidikRelatedByPesertaDidikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BeasiswaPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PesertaDidikRelatedByPesertaDidikId', $join_behavior);

        return $this->getBeasiswaPesertaDidiksRelatedByTahunSelesai($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related BeasiswaPesertaDidiksRelatedByTahunSelesai from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BeasiswaPesertaDidik[] List of BeasiswaPesertaDidik objects
     */
    public function getBeasiswaPesertaDidiksRelatedByTahunSelesaiJoinPesertaDidikRelatedByPesertaDidikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BeasiswaPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PesertaDidikRelatedByPesertaDidikId', $join_behavior);

        return $this->getBeasiswaPesertaDidiksRelatedByTahunSelesai($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related BeasiswaPesertaDidiksRelatedByTahunSelesai from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BeasiswaPesertaDidik[] List of BeasiswaPesertaDidik objects
     */
    public function getBeasiswaPesertaDidiksRelatedByTahunSelesaiJoinJenisBeasiswaRelatedByJenisBeasiswaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BeasiswaPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisBeasiswaRelatedByJenisBeasiswaId', $join_behavior);

        return $this->getBeasiswaPesertaDidiksRelatedByTahunSelesai($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related BeasiswaPesertaDidiksRelatedByTahunSelesai from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BeasiswaPesertaDidik[] List of BeasiswaPesertaDidik objects
     */
    public function getBeasiswaPesertaDidiksRelatedByTahunSelesaiJoinJenisBeasiswaRelatedByJenisBeasiswaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BeasiswaPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisBeasiswaRelatedByJenisBeasiswaId', $join_behavior);

        return $this->getBeasiswaPesertaDidiksRelatedByTahunSelesai($query, $con);
    }

    /**
     * Clears out the collBeasiswaPesertaDidiksRelatedByTahunMulai collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return TahunAjaran The current object (for fluent API support)
     * @see        addBeasiswaPesertaDidiksRelatedByTahunMulai()
     */
    public function clearBeasiswaPesertaDidiksRelatedByTahunMulai()
    {
        $this->collBeasiswaPesertaDidiksRelatedByTahunMulai = null; // important to set this to null since that means it is uninitialized
        $this->collBeasiswaPesertaDidiksRelatedByTahunMulaiPartial = null;

        return $this;
    }

    /**
     * reset is the collBeasiswaPesertaDidiksRelatedByTahunMulai collection loaded partially
     *
     * @return void
     */
    public function resetPartialBeasiswaPesertaDidiksRelatedByTahunMulai($v = true)
    {
        $this->collBeasiswaPesertaDidiksRelatedByTahunMulaiPartial = $v;
    }

    /**
     * Initializes the collBeasiswaPesertaDidiksRelatedByTahunMulai collection.
     *
     * By default this just sets the collBeasiswaPesertaDidiksRelatedByTahunMulai collection to an empty array (like clearcollBeasiswaPesertaDidiksRelatedByTahunMulai());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBeasiswaPesertaDidiksRelatedByTahunMulai($overrideExisting = true)
    {
        if (null !== $this->collBeasiswaPesertaDidiksRelatedByTahunMulai && !$overrideExisting) {
            return;
        }
        $this->collBeasiswaPesertaDidiksRelatedByTahunMulai = new PropelObjectCollection();
        $this->collBeasiswaPesertaDidiksRelatedByTahunMulai->setModel('BeasiswaPesertaDidik');
    }

    /**
     * Gets an array of BeasiswaPesertaDidik objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this TahunAjaran is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|BeasiswaPesertaDidik[] List of BeasiswaPesertaDidik objects
     * @throws PropelException
     */
    public function getBeasiswaPesertaDidiksRelatedByTahunMulai($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collBeasiswaPesertaDidiksRelatedByTahunMulaiPartial && !$this->isNew();
        if (null === $this->collBeasiswaPesertaDidiksRelatedByTahunMulai || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBeasiswaPesertaDidiksRelatedByTahunMulai) {
                // return empty collection
                $this->initBeasiswaPesertaDidiksRelatedByTahunMulai();
            } else {
                $collBeasiswaPesertaDidiksRelatedByTahunMulai = BeasiswaPesertaDidikQuery::create(null, $criteria)
                    ->filterByTahunAjaranRelatedByTahunMulai($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collBeasiswaPesertaDidiksRelatedByTahunMulaiPartial && count($collBeasiswaPesertaDidiksRelatedByTahunMulai)) {
                      $this->initBeasiswaPesertaDidiksRelatedByTahunMulai(false);

                      foreach($collBeasiswaPesertaDidiksRelatedByTahunMulai as $obj) {
                        if (false == $this->collBeasiswaPesertaDidiksRelatedByTahunMulai->contains($obj)) {
                          $this->collBeasiswaPesertaDidiksRelatedByTahunMulai->append($obj);
                        }
                      }

                      $this->collBeasiswaPesertaDidiksRelatedByTahunMulaiPartial = true;
                    }

                    $collBeasiswaPesertaDidiksRelatedByTahunMulai->getInternalIterator()->rewind();
                    return $collBeasiswaPesertaDidiksRelatedByTahunMulai;
                }

                if($partial && $this->collBeasiswaPesertaDidiksRelatedByTahunMulai) {
                    foreach($this->collBeasiswaPesertaDidiksRelatedByTahunMulai as $obj) {
                        if($obj->isNew()) {
                            $collBeasiswaPesertaDidiksRelatedByTahunMulai[] = $obj;
                        }
                    }
                }

                $this->collBeasiswaPesertaDidiksRelatedByTahunMulai = $collBeasiswaPesertaDidiksRelatedByTahunMulai;
                $this->collBeasiswaPesertaDidiksRelatedByTahunMulaiPartial = false;
            }
        }

        return $this->collBeasiswaPesertaDidiksRelatedByTahunMulai;
    }

    /**
     * Sets a collection of BeasiswaPesertaDidikRelatedByTahunMulai objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $beasiswaPesertaDidiksRelatedByTahunMulai A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function setBeasiswaPesertaDidiksRelatedByTahunMulai(PropelCollection $beasiswaPesertaDidiksRelatedByTahunMulai, PropelPDO $con = null)
    {
        $beasiswaPesertaDidiksRelatedByTahunMulaiToDelete = $this->getBeasiswaPesertaDidiksRelatedByTahunMulai(new Criteria(), $con)->diff($beasiswaPesertaDidiksRelatedByTahunMulai);

        $this->beasiswaPesertaDidiksRelatedByTahunMulaiScheduledForDeletion = unserialize(serialize($beasiswaPesertaDidiksRelatedByTahunMulaiToDelete));

        foreach ($beasiswaPesertaDidiksRelatedByTahunMulaiToDelete as $beasiswaPesertaDidikRelatedByTahunMulaiRemoved) {
            $beasiswaPesertaDidikRelatedByTahunMulaiRemoved->setTahunAjaranRelatedByTahunMulai(null);
        }

        $this->collBeasiswaPesertaDidiksRelatedByTahunMulai = null;
        foreach ($beasiswaPesertaDidiksRelatedByTahunMulai as $beasiswaPesertaDidikRelatedByTahunMulai) {
            $this->addBeasiswaPesertaDidikRelatedByTahunMulai($beasiswaPesertaDidikRelatedByTahunMulai);
        }

        $this->collBeasiswaPesertaDidiksRelatedByTahunMulai = $beasiswaPesertaDidiksRelatedByTahunMulai;
        $this->collBeasiswaPesertaDidiksRelatedByTahunMulaiPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BeasiswaPesertaDidik objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related BeasiswaPesertaDidik objects.
     * @throws PropelException
     */
    public function countBeasiswaPesertaDidiksRelatedByTahunMulai(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collBeasiswaPesertaDidiksRelatedByTahunMulaiPartial && !$this->isNew();
        if (null === $this->collBeasiswaPesertaDidiksRelatedByTahunMulai || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBeasiswaPesertaDidiksRelatedByTahunMulai) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getBeasiswaPesertaDidiksRelatedByTahunMulai());
            }
            $query = BeasiswaPesertaDidikQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByTahunAjaranRelatedByTahunMulai($this)
                ->count($con);
        }

        return count($this->collBeasiswaPesertaDidiksRelatedByTahunMulai);
    }

    /**
     * Method called to associate a BeasiswaPesertaDidik object to this object
     * through the BeasiswaPesertaDidik foreign key attribute.
     *
     * @param    BeasiswaPesertaDidik $l BeasiswaPesertaDidik
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function addBeasiswaPesertaDidikRelatedByTahunMulai(BeasiswaPesertaDidik $l)
    {
        if ($this->collBeasiswaPesertaDidiksRelatedByTahunMulai === null) {
            $this->initBeasiswaPesertaDidiksRelatedByTahunMulai();
            $this->collBeasiswaPesertaDidiksRelatedByTahunMulaiPartial = true;
        }
        if (!in_array($l, $this->collBeasiswaPesertaDidiksRelatedByTahunMulai->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddBeasiswaPesertaDidikRelatedByTahunMulai($l);
        }

        return $this;
    }

    /**
     * @param	BeasiswaPesertaDidikRelatedByTahunMulai $beasiswaPesertaDidikRelatedByTahunMulai The beasiswaPesertaDidikRelatedByTahunMulai object to add.
     */
    protected function doAddBeasiswaPesertaDidikRelatedByTahunMulai($beasiswaPesertaDidikRelatedByTahunMulai)
    {
        $this->collBeasiswaPesertaDidiksRelatedByTahunMulai[]= $beasiswaPesertaDidikRelatedByTahunMulai;
        $beasiswaPesertaDidikRelatedByTahunMulai->setTahunAjaranRelatedByTahunMulai($this);
    }

    /**
     * @param	BeasiswaPesertaDidikRelatedByTahunMulai $beasiswaPesertaDidikRelatedByTahunMulai The beasiswaPesertaDidikRelatedByTahunMulai object to remove.
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function removeBeasiswaPesertaDidikRelatedByTahunMulai($beasiswaPesertaDidikRelatedByTahunMulai)
    {
        if ($this->getBeasiswaPesertaDidiksRelatedByTahunMulai()->contains($beasiswaPesertaDidikRelatedByTahunMulai)) {
            $this->collBeasiswaPesertaDidiksRelatedByTahunMulai->remove($this->collBeasiswaPesertaDidiksRelatedByTahunMulai->search($beasiswaPesertaDidikRelatedByTahunMulai));
            if (null === $this->beasiswaPesertaDidiksRelatedByTahunMulaiScheduledForDeletion) {
                $this->beasiswaPesertaDidiksRelatedByTahunMulaiScheduledForDeletion = clone $this->collBeasiswaPesertaDidiksRelatedByTahunMulai;
                $this->beasiswaPesertaDidiksRelatedByTahunMulaiScheduledForDeletion->clear();
            }
            $this->beasiswaPesertaDidiksRelatedByTahunMulaiScheduledForDeletion[]= clone $beasiswaPesertaDidikRelatedByTahunMulai;
            $beasiswaPesertaDidikRelatedByTahunMulai->setTahunAjaranRelatedByTahunMulai(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related BeasiswaPesertaDidiksRelatedByTahunMulai from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BeasiswaPesertaDidik[] List of BeasiswaPesertaDidik objects
     */
    public function getBeasiswaPesertaDidiksRelatedByTahunMulaiJoinPesertaDidikRelatedByPesertaDidikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BeasiswaPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PesertaDidikRelatedByPesertaDidikId', $join_behavior);

        return $this->getBeasiswaPesertaDidiksRelatedByTahunMulai($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related BeasiswaPesertaDidiksRelatedByTahunMulai from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BeasiswaPesertaDidik[] List of BeasiswaPesertaDidik objects
     */
    public function getBeasiswaPesertaDidiksRelatedByTahunMulaiJoinPesertaDidikRelatedByPesertaDidikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BeasiswaPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PesertaDidikRelatedByPesertaDidikId', $join_behavior);

        return $this->getBeasiswaPesertaDidiksRelatedByTahunMulai($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related BeasiswaPesertaDidiksRelatedByTahunMulai from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BeasiswaPesertaDidik[] List of BeasiswaPesertaDidik objects
     */
    public function getBeasiswaPesertaDidiksRelatedByTahunMulaiJoinJenisBeasiswaRelatedByJenisBeasiswaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BeasiswaPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisBeasiswaRelatedByJenisBeasiswaId', $join_behavior);

        return $this->getBeasiswaPesertaDidiksRelatedByTahunMulai($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related BeasiswaPesertaDidiksRelatedByTahunMulai from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BeasiswaPesertaDidik[] List of BeasiswaPesertaDidik objects
     */
    public function getBeasiswaPesertaDidiksRelatedByTahunMulaiJoinJenisBeasiswaRelatedByJenisBeasiswaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BeasiswaPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisBeasiswaRelatedByJenisBeasiswaId', $join_behavior);

        return $this->getBeasiswaPesertaDidiksRelatedByTahunMulai($query, $con);
    }

    /**
     * Clears out the collPesertaDidikBarusRelatedByTahunAjaranId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return TahunAjaran The current object (for fluent API support)
     * @see        addPesertaDidikBarusRelatedByTahunAjaranId()
     */
    public function clearPesertaDidikBarusRelatedByTahunAjaranId()
    {
        $this->collPesertaDidikBarusRelatedByTahunAjaranId = null; // important to set this to null since that means it is uninitialized
        $this->collPesertaDidikBarusRelatedByTahunAjaranIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPesertaDidikBarusRelatedByTahunAjaranId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPesertaDidikBarusRelatedByTahunAjaranId($v = true)
    {
        $this->collPesertaDidikBarusRelatedByTahunAjaranIdPartial = $v;
    }

    /**
     * Initializes the collPesertaDidikBarusRelatedByTahunAjaranId collection.
     *
     * By default this just sets the collPesertaDidikBarusRelatedByTahunAjaranId collection to an empty array (like clearcollPesertaDidikBarusRelatedByTahunAjaranId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPesertaDidikBarusRelatedByTahunAjaranId($overrideExisting = true)
    {
        if (null !== $this->collPesertaDidikBarusRelatedByTahunAjaranId && !$overrideExisting) {
            return;
        }
        $this->collPesertaDidikBarusRelatedByTahunAjaranId = new PropelObjectCollection();
        $this->collPesertaDidikBarusRelatedByTahunAjaranId->setModel('PesertaDidikBaru');
    }

    /**
     * Gets an array of PesertaDidikBaru objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this TahunAjaran is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|PesertaDidikBaru[] List of PesertaDidikBaru objects
     * @throws PropelException
     */
    public function getPesertaDidikBarusRelatedByTahunAjaranId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPesertaDidikBarusRelatedByTahunAjaranIdPartial && !$this->isNew();
        if (null === $this->collPesertaDidikBarusRelatedByTahunAjaranId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPesertaDidikBarusRelatedByTahunAjaranId) {
                // return empty collection
                $this->initPesertaDidikBarusRelatedByTahunAjaranId();
            } else {
                $collPesertaDidikBarusRelatedByTahunAjaranId = PesertaDidikBaruQuery::create(null, $criteria)
                    ->filterByTahunAjaranRelatedByTahunAjaranId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPesertaDidikBarusRelatedByTahunAjaranIdPartial && count($collPesertaDidikBarusRelatedByTahunAjaranId)) {
                      $this->initPesertaDidikBarusRelatedByTahunAjaranId(false);

                      foreach($collPesertaDidikBarusRelatedByTahunAjaranId as $obj) {
                        if (false == $this->collPesertaDidikBarusRelatedByTahunAjaranId->contains($obj)) {
                          $this->collPesertaDidikBarusRelatedByTahunAjaranId->append($obj);
                        }
                      }

                      $this->collPesertaDidikBarusRelatedByTahunAjaranIdPartial = true;
                    }

                    $collPesertaDidikBarusRelatedByTahunAjaranId->getInternalIterator()->rewind();
                    return $collPesertaDidikBarusRelatedByTahunAjaranId;
                }

                if($partial && $this->collPesertaDidikBarusRelatedByTahunAjaranId) {
                    foreach($this->collPesertaDidikBarusRelatedByTahunAjaranId as $obj) {
                        if($obj->isNew()) {
                            $collPesertaDidikBarusRelatedByTahunAjaranId[] = $obj;
                        }
                    }
                }

                $this->collPesertaDidikBarusRelatedByTahunAjaranId = $collPesertaDidikBarusRelatedByTahunAjaranId;
                $this->collPesertaDidikBarusRelatedByTahunAjaranIdPartial = false;
            }
        }

        return $this->collPesertaDidikBarusRelatedByTahunAjaranId;
    }

    /**
     * Sets a collection of PesertaDidikBaruRelatedByTahunAjaranId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $pesertaDidikBarusRelatedByTahunAjaranId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function setPesertaDidikBarusRelatedByTahunAjaranId(PropelCollection $pesertaDidikBarusRelatedByTahunAjaranId, PropelPDO $con = null)
    {
        $pesertaDidikBarusRelatedByTahunAjaranIdToDelete = $this->getPesertaDidikBarusRelatedByTahunAjaranId(new Criteria(), $con)->diff($pesertaDidikBarusRelatedByTahunAjaranId);

        $this->pesertaDidikBarusRelatedByTahunAjaranIdScheduledForDeletion = unserialize(serialize($pesertaDidikBarusRelatedByTahunAjaranIdToDelete));

        foreach ($pesertaDidikBarusRelatedByTahunAjaranIdToDelete as $pesertaDidikBaruRelatedByTahunAjaranIdRemoved) {
            $pesertaDidikBaruRelatedByTahunAjaranIdRemoved->setTahunAjaranRelatedByTahunAjaranId(null);
        }

        $this->collPesertaDidikBarusRelatedByTahunAjaranId = null;
        foreach ($pesertaDidikBarusRelatedByTahunAjaranId as $pesertaDidikBaruRelatedByTahunAjaranId) {
            $this->addPesertaDidikBaruRelatedByTahunAjaranId($pesertaDidikBaruRelatedByTahunAjaranId);
        }

        $this->collPesertaDidikBarusRelatedByTahunAjaranId = $pesertaDidikBarusRelatedByTahunAjaranId;
        $this->collPesertaDidikBarusRelatedByTahunAjaranIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PesertaDidikBaru objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related PesertaDidikBaru objects.
     * @throws PropelException
     */
    public function countPesertaDidikBarusRelatedByTahunAjaranId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPesertaDidikBarusRelatedByTahunAjaranIdPartial && !$this->isNew();
        if (null === $this->collPesertaDidikBarusRelatedByTahunAjaranId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPesertaDidikBarusRelatedByTahunAjaranId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPesertaDidikBarusRelatedByTahunAjaranId());
            }
            $query = PesertaDidikBaruQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByTahunAjaranRelatedByTahunAjaranId($this)
                ->count($con);
        }

        return count($this->collPesertaDidikBarusRelatedByTahunAjaranId);
    }

    /**
     * Method called to associate a PesertaDidikBaru object to this object
     * through the PesertaDidikBaru foreign key attribute.
     *
     * @param    PesertaDidikBaru $l PesertaDidikBaru
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function addPesertaDidikBaruRelatedByTahunAjaranId(PesertaDidikBaru $l)
    {
        if ($this->collPesertaDidikBarusRelatedByTahunAjaranId === null) {
            $this->initPesertaDidikBarusRelatedByTahunAjaranId();
            $this->collPesertaDidikBarusRelatedByTahunAjaranIdPartial = true;
        }
        if (!in_array($l, $this->collPesertaDidikBarusRelatedByTahunAjaranId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPesertaDidikBaruRelatedByTahunAjaranId($l);
        }

        return $this;
    }

    /**
     * @param	PesertaDidikBaruRelatedByTahunAjaranId $pesertaDidikBaruRelatedByTahunAjaranId The pesertaDidikBaruRelatedByTahunAjaranId object to add.
     */
    protected function doAddPesertaDidikBaruRelatedByTahunAjaranId($pesertaDidikBaruRelatedByTahunAjaranId)
    {
        $this->collPesertaDidikBarusRelatedByTahunAjaranId[]= $pesertaDidikBaruRelatedByTahunAjaranId;
        $pesertaDidikBaruRelatedByTahunAjaranId->setTahunAjaranRelatedByTahunAjaranId($this);
    }

    /**
     * @param	PesertaDidikBaruRelatedByTahunAjaranId $pesertaDidikBaruRelatedByTahunAjaranId The pesertaDidikBaruRelatedByTahunAjaranId object to remove.
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function removePesertaDidikBaruRelatedByTahunAjaranId($pesertaDidikBaruRelatedByTahunAjaranId)
    {
        if ($this->getPesertaDidikBarusRelatedByTahunAjaranId()->contains($pesertaDidikBaruRelatedByTahunAjaranId)) {
            $this->collPesertaDidikBarusRelatedByTahunAjaranId->remove($this->collPesertaDidikBarusRelatedByTahunAjaranId->search($pesertaDidikBaruRelatedByTahunAjaranId));
            if (null === $this->pesertaDidikBarusRelatedByTahunAjaranIdScheduledForDeletion) {
                $this->pesertaDidikBarusRelatedByTahunAjaranIdScheduledForDeletion = clone $this->collPesertaDidikBarusRelatedByTahunAjaranId;
                $this->pesertaDidikBarusRelatedByTahunAjaranIdScheduledForDeletion->clear();
            }
            $this->pesertaDidikBarusRelatedByTahunAjaranIdScheduledForDeletion[]= clone $pesertaDidikBaruRelatedByTahunAjaranId;
            $pesertaDidikBaruRelatedByTahunAjaranId->setTahunAjaranRelatedByTahunAjaranId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PesertaDidikBarusRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidikBaru[] List of PesertaDidikBaru objects
     */
    public function getPesertaDidikBarusRelatedByTahunAjaranIdJoinPesertaDidikRelatedByPesertaDidikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikBaruQuery::create(null, $criteria);
        $query->joinWith('PesertaDidikRelatedByPesertaDidikId', $join_behavior);

        return $this->getPesertaDidikBarusRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PesertaDidikBarusRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidikBaru[] List of PesertaDidikBaru objects
     */
    public function getPesertaDidikBarusRelatedByTahunAjaranIdJoinPesertaDidikRelatedByPesertaDidikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikBaruQuery::create(null, $criteria);
        $query->joinWith('PesertaDidikRelatedByPesertaDidikId', $join_behavior);

        return $this->getPesertaDidikBarusRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PesertaDidikBarusRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidikBaru[] List of PesertaDidikBaru objects
     */
    public function getPesertaDidikBarusRelatedByTahunAjaranIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikBaruQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPesertaDidikBarusRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PesertaDidikBarusRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidikBaru[] List of PesertaDidikBaru objects
     */
    public function getPesertaDidikBarusRelatedByTahunAjaranIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikBaruQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPesertaDidikBarusRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PesertaDidikBarusRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidikBaru[] List of PesertaDidikBaru objects
     */
    public function getPesertaDidikBarusRelatedByTahunAjaranIdJoinJenisPendaftaranRelatedByJenisPendaftaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikBaruQuery::create(null, $criteria);
        $query->joinWith('JenisPendaftaranRelatedByJenisPendaftaranId', $join_behavior);

        return $this->getPesertaDidikBarusRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PesertaDidikBarusRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidikBaru[] List of PesertaDidikBaru objects
     */
    public function getPesertaDidikBarusRelatedByTahunAjaranIdJoinJenisPendaftaranRelatedByJenisPendaftaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikBaruQuery::create(null, $criteria);
        $query->joinWith('JenisPendaftaranRelatedByJenisPendaftaranId', $join_behavior);

        return $this->getPesertaDidikBarusRelatedByTahunAjaranId($query, $con);
    }

    /**
     * Clears out the collPesertaDidikBarusRelatedByTahunAjaranId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return TahunAjaran The current object (for fluent API support)
     * @see        addPesertaDidikBarusRelatedByTahunAjaranId()
     */
    public function clearPesertaDidikBarusRelatedByTahunAjaranId()
    {
        $this->collPesertaDidikBarusRelatedByTahunAjaranId = null; // important to set this to null since that means it is uninitialized
        $this->collPesertaDidikBarusRelatedByTahunAjaranIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPesertaDidikBarusRelatedByTahunAjaranId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPesertaDidikBarusRelatedByTahunAjaranId($v = true)
    {
        $this->collPesertaDidikBarusRelatedByTahunAjaranIdPartial = $v;
    }

    /**
     * Initializes the collPesertaDidikBarusRelatedByTahunAjaranId collection.
     *
     * By default this just sets the collPesertaDidikBarusRelatedByTahunAjaranId collection to an empty array (like clearcollPesertaDidikBarusRelatedByTahunAjaranId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPesertaDidikBarusRelatedByTahunAjaranId($overrideExisting = true)
    {
        if (null !== $this->collPesertaDidikBarusRelatedByTahunAjaranId && !$overrideExisting) {
            return;
        }
        $this->collPesertaDidikBarusRelatedByTahunAjaranId = new PropelObjectCollection();
        $this->collPesertaDidikBarusRelatedByTahunAjaranId->setModel('PesertaDidikBaru');
    }

    /**
     * Gets an array of PesertaDidikBaru objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this TahunAjaran is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|PesertaDidikBaru[] List of PesertaDidikBaru objects
     * @throws PropelException
     */
    public function getPesertaDidikBarusRelatedByTahunAjaranId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPesertaDidikBarusRelatedByTahunAjaranIdPartial && !$this->isNew();
        if (null === $this->collPesertaDidikBarusRelatedByTahunAjaranId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPesertaDidikBarusRelatedByTahunAjaranId) {
                // return empty collection
                $this->initPesertaDidikBarusRelatedByTahunAjaranId();
            } else {
                $collPesertaDidikBarusRelatedByTahunAjaranId = PesertaDidikBaruQuery::create(null, $criteria)
                    ->filterByTahunAjaranRelatedByTahunAjaranId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPesertaDidikBarusRelatedByTahunAjaranIdPartial && count($collPesertaDidikBarusRelatedByTahunAjaranId)) {
                      $this->initPesertaDidikBarusRelatedByTahunAjaranId(false);

                      foreach($collPesertaDidikBarusRelatedByTahunAjaranId as $obj) {
                        if (false == $this->collPesertaDidikBarusRelatedByTahunAjaranId->contains($obj)) {
                          $this->collPesertaDidikBarusRelatedByTahunAjaranId->append($obj);
                        }
                      }

                      $this->collPesertaDidikBarusRelatedByTahunAjaranIdPartial = true;
                    }

                    $collPesertaDidikBarusRelatedByTahunAjaranId->getInternalIterator()->rewind();
                    return $collPesertaDidikBarusRelatedByTahunAjaranId;
                }

                if($partial && $this->collPesertaDidikBarusRelatedByTahunAjaranId) {
                    foreach($this->collPesertaDidikBarusRelatedByTahunAjaranId as $obj) {
                        if($obj->isNew()) {
                            $collPesertaDidikBarusRelatedByTahunAjaranId[] = $obj;
                        }
                    }
                }

                $this->collPesertaDidikBarusRelatedByTahunAjaranId = $collPesertaDidikBarusRelatedByTahunAjaranId;
                $this->collPesertaDidikBarusRelatedByTahunAjaranIdPartial = false;
            }
        }

        return $this->collPesertaDidikBarusRelatedByTahunAjaranId;
    }

    /**
     * Sets a collection of PesertaDidikBaruRelatedByTahunAjaranId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $pesertaDidikBarusRelatedByTahunAjaranId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function setPesertaDidikBarusRelatedByTahunAjaranId(PropelCollection $pesertaDidikBarusRelatedByTahunAjaranId, PropelPDO $con = null)
    {
        $pesertaDidikBarusRelatedByTahunAjaranIdToDelete = $this->getPesertaDidikBarusRelatedByTahunAjaranId(new Criteria(), $con)->diff($pesertaDidikBarusRelatedByTahunAjaranId);

        $this->pesertaDidikBarusRelatedByTahunAjaranIdScheduledForDeletion = unserialize(serialize($pesertaDidikBarusRelatedByTahunAjaranIdToDelete));

        foreach ($pesertaDidikBarusRelatedByTahunAjaranIdToDelete as $pesertaDidikBaruRelatedByTahunAjaranIdRemoved) {
            $pesertaDidikBaruRelatedByTahunAjaranIdRemoved->setTahunAjaranRelatedByTahunAjaranId(null);
        }

        $this->collPesertaDidikBarusRelatedByTahunAjaranId = null;
        foreach ($pesertaDidikBarusRelatedByTahunAjaranId as $pesertaDidikBaruRelatedByTahunAjaranId) {
            $this->addPesertaDidikBaruRelatedByTahunAjaranId($pesertaDidikBaruRelatedByTahunAjaranId);
        }

        $this->collPesertaDidikBarusRelatedByTahunAjaranId = $pesertaDidikBarusRelatedByTahunAjaranId;
        $this->collPesertaDidikBarusRelatedByTahunAjaranIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PesertaDidikBaru objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related PesertaDidikBaru objects.
     * @throws PropelException
     */
    public function countPesertaDidikBarusRelatedByTahunAjaranId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPesertaDidikBarusRelatedByTahunAjaranIdPartial && !$this->isNew();
        if (null === $this->collPesertaDidikBarusRelatedByTahunAjaranId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPesertaDidikBarusRelatedByTahunAjaranId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPesertaDidikBarusRelatedByTahunAjaranId());
            }
            $query = PesertaDidikBaruQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByTahunAjaranRelatedByTahunAjaranId($this)
                ->count($con);
        }

        return count($this->collPesertaDidikBarusRelatedByTahunAjaranId);
    }

    /**
     * Method called to associate a PesertaDidikBaru object to this object
     * through the PesertaDidikBaru foreign key attribute.
     *
     * @param    PesertaDidikBaru $l PesertaDidikBaru
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function addPesertaDidikBaruRelatedByTahunAjaranId(PesertaDidikBaru $l)
    {
        if ($this->collPesertaDidikBarusRelatedByTahunAjaranId === null) {
            $this->initPesertaDidikBarusRelatedByTahunAjaranId();
            $this->collPesertaDidikBarusRelatedByTahunAjaranIdPartial = true;
        }
        if (!in_array($l, $this->collPesertaDidikBarusRelatedByTahunAjaranId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPesertaDidikBaruRelatedByTahunAjaranId($l);
        }

        return $this;
    }

    /**
     * @param	PesertaDidikBaruRelatedByTahunAjaranId $pesertaDidikBaruRelatedByTahunAjaranId The pesertaDidikBaruRelatedByTahunAjaranId object to add.
     */
    protected function doAddPesertaDidikBaruRelatedByTahunAjaranId($pesertaDidikBaruRelatedByTahunAjaranId)
    {
        $this->collPesertaDidikBarusRelatedByTahunAjaranId[]= $pesertaDidikBaruRelatedByTahunAjaranId;
        $pesertaDidikBaruRelatedByTahunAjaranId->setTahunAjaranRelatedByTahunAjaranId($this);
    }

    /**
     * @param	PesertaDidikBaruRelatedByTahunAjaranId $pesertaDidikBaruRelatedByTahunAjaranId The pesertaDidikBaruRelatedByTahunAjaranId object to remove.
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function removePesertaDidikBaruRelatedByTahunAjaranId($pesertaDidikBaruRelatedByTahunAjaranId)
    {
        if ($this->getPesertaDidikBarusRelatedByTahunAjaranId()->contains($pesertaDidikBaruRelatedByTahunAjaranId)) {
            $this->collPesertaDidikBarusRelatedByTahunAjaranId->remove($this->collPesertaDidikBarusRelatedByTahunAjaranId->search($pesertaDidikBaruRelatedByTahunAjaranId));
            if (null === $this->pesertaDidikBarusRelatedByTahunAjaranIdScheduledForDeletion) {
                $this->pesertaDidikBarusRelatedByTahunAjaranIdScheduledForDeletion = clone $this->collPesertaDidikBarusRelatedByTahunAjaranId;
                $this->pesertaDidikBarusRelatedByTahunAjaranIdScheduledForDeletion->clear();
            }
            $this->pesertaDidikBarusRelatedByTahunAjaranIdScheduledForDeletion[]= clone $pesertaDidikBaruRelatedByTahunAjaranId;
            $pesertaDidikBaruRelatedByTahunAjaranId->setTahunAjaranRelatedByTahunAjaranId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PesertaDidikBarusRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidikBaru[] List of PesertaDidikBaru objects
     */
    public function getPesertaDidikBarusRelatedByTahunAjaranIdJoinPesertaDidikRelatedByPesertaDidikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikBaruQuery::create(null, $criteria);
        $query->joinWith('PesertaDidikRelatedByPesertaDidikId', $join_behavior);

        return $this->getPesertaDidikBarusRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PesertaDidikBarusRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidikBaru[] List of PesertaDidikBaru objects
     */
    public function getPesertaDidikBarusRelatedByTahunAjaranIdJoinPesertaDidikRelatedByPesertaDidikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikBaruQuery::create(null, $criteria);
        $query->joinWith('PesertaDidikRelatedByPesertaDidikId', $join_behavior);

        return $this->getPesertaDidikBarusRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PesertaDidikBarusRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidikBaru[] List of PesertaDidikBaru objects
     */
    public function getPesertaDidikBarusRelatedByTahunAjaranIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikBaruQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPesertaDidikBarusRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PesertaDidikBarusRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidikBaru[] List of PesertaDidikBaru objects
     */
    public function getPesertaDidikBarusRelatedByTahunAjaranIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikBaruQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPesertaDidikBarusRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PesertaDidikBarusRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidikBaru[] List of PesertaDidikBaru objects
     */
    public function getPesertaDidikBarusRelatedByTahunAjaranIdJoinJenisPendaftaranRelatedByJenisPendaftaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikBaruQuery::create(null, $criteria);
        $query->joinWith('JenisPendaftaranRelatedByJenisPendaftaranId', $join_behavior);

        return $this->getPesertaDidikBarusRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PesertaDidikBarusRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidikBaru[] List of PesertaDidikBaru objects
     */
    public function getPesertaDidikBarusRelatedByTahunAjaranIdJoinJenisPendaftaranRelatedByJenisPendaftaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikBaruQuery::create(null, $criteria);
        $query->joinWith('JenisPendaftaranRelatedByJenisPendaftaranId', $join_behavior);

        return $this->getPesertaDidikBarusRelatedByTahunAjaranId($query, $con);
    }

    /**
     * Clears out the collPtkTerdaftarsRelatedByTahunAjaranId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return TahunAjaran The current object (for fluent API support)
     * @see        addPtkTerdaftarsRelatedByTahunAjaranId()
     */
    public function clearPtkTerdaftarsRelatedByTahunAjaranId()
    {
        $this->collPtkTerdaftarsRelatedByTahunAjaranId = null; // important to set this to null since that means it is uninitialized
        $this->collPtkTerdaftarsRelatedByTahunAjaranIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPtkTerdaftarsRelatedByTahunAjaranId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPtkTerdaftarsRelatedByTahunAjaranId($v = true)
    {
        $this->collPtkTerdaftarsRelatedByTahunAjaranIdPartial = $v;
    }

    /**
     * Initializes the collPtkTerdaftarsRelatedByTahunAjaranId collection.
     *
     * By default this just sets the collPtkTerdaftarsRelatedByTahunAjaranId collection to an empty array (like clearcollPtkTerdaftarsRelatedByTahunAjaranId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPtkTerdaftarsRelatedByTahunAjaranId($overrideExisting = true)
    {
        if (null !== $this->collPtkTerdaftarsRelatedByTahunAjaranId && !$overrideExisting) {
            return;
        }
        $this->collPtkTerdaftarsRelatedByTahunAjaranId = new PropelObjectCollection();
        $this->collPtkTerdaftarsRelatedByTahunAjaranId->setModel('PtkTerdaftar');
    }

    /**
     * Gets an array of PtkTerdaftar objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this TahunAjaran is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     * @throws PropelException
     */
    public function getPtkTerdaftarsRelatedByTahunAjaranId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPtkTerdaftarsRelatedByTahunAjaranIdPartial && !$this->isNew();
        if (null === $this->collPtkTerdaftarsRelatedByTahunAjaranId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPtkTerdaftarsRelatedByTahunAjaranId) {
                // return empty collection
                $this->initPtkTerdaftarsRelatedByTahunAjaranId();
            } else {
                $collPtkTerdaftarsRelatedByTahunAjaranId = PtkTerdaftarQuery::create(null, $criteria)
                    ->filterByTahunAjaranRelatedByTahunAjaranId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPtkTerdaftarsRelatedByTahunAjaranIdPartial && count($collPtkTerdaftarsRelatedByTahunAjaranId)) {
                      $this->initPtkTerdaftarsRelatedByTahunAjaranId(false);

                      foreach($collPtkTerdaftarsRelatedByTahunAjaranId as $obj) {
                        if (false == $this->collPtkTerdaftarsRelatedByTahunAjaranId->contains($obj)) {
                          $this->collPtkTerdaftarsRelatedByTahunAjaranId->append($obj);
                        }
                      }

                      $this->collPtkTerdaftarsRelatedByTahunAjaranIdPartial = true;
                    }

                    $collPtkTerdaftarsRelatedByTahunAjaranId->getInternalIterator()->rewind();
                    return $collPtkTerdaftarsRelatedByTahunAjaranId;
                }

                if($partial && $this->collPtkTerdaftarsRelatedByTahunAjaranId) {
                    foreach($this->collPtkTerdaftarsRelatedByTahunAjaranId as $obj) {
                        if($obj->isNew()) {
                            $collPtkTerdaftarsRelatedByTahunAjaranId[] = $obj;
                        }
                    }
                }

                $this->collPtkTerdaftarsRelatedByTahunAjaranId = $collPtkTerdaftarsRelatedByTahunAjaranId;
                $this->collPtkTerdaftarsRelatedByTahunAjaranIdPartial = false;
            }
        }

        return $this->collPtkTerdaftarsRelatedByTahunAjaranId;
    }

    /**
     * Sets a collection of PtkTerdaftarRelatedByTahunAjaranId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $ptkTerdaftarsRelatedByTahunAjaranId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function setPtkTerdaftarsRelatedByTahunAjaranId(PropelCollection $ptkTerdaftarsRelatedByTahunAjaranId, PropelPDO $con = null)
    {
        $ptkTerdaftarsRelatedByTahunAjaranIdToDelete = $this->getPtkTerdaftarsRelatedByTahunAjaranId(new Criteria(), $con)->diff($ptkTerdaftarsRelatedByTahunAjaranId);

        $this->ptkTerdaftarsRelatedByTahunAjaranIdScheduledForDeletion = unserialize(serialize($ptkTerdaftarsRelatedByTahunAjaranIdToDelete));

        foreach ($ptkTerdaftarsRelatedByTahunAjaranIdToDelete as $ptkTerdaftarRelatedByTahunAjaranIdRemoved) {
            $ptkTerdaftarRelatedByTahunAjaranIdRemoved->setTahunAjaranRelatedByTahunAjaranId(null);
        }

        $this->collPtkTerdaftarsRelatedByTahunAjaranId = null;
        foreach ($ptkTerdaftarsRelatedByTahunAjaranId as $ptkTerdaftarRelatedByTahunAjaranId) {
            $this->addPtkTerdaftarRelatedByTahunAjaranId($ptkTerdaftarRelatedByTahunAjaranId);
        }

        $this->collPtkTerdaftarsRelatedByTahunAjaranId = $ptkTerdaftarsRelatedByTahunAjaranId;
        $this->collPtkTerdaftarsRelatedByTahunAjaranIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PtkTerdaftar objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related PtkTerdaftar objects.
     * @throws PropelException
     */
    public function countPtkTerdaftarsRelatedByTahunAjaranId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPtkTerdaftarsRelatedByTahunAjaranIdPartial && !$this->isNew();
        if (null === $this->collPtkTerdaftarsRelatedByTahunAjaranId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPtkTerdaftarsRelatedByTahunAjaranId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPtkTerdaftarsRelatedByTahunAjaranId());
            }
            $query = PtkTerdaftarQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByTahunAjaranRelatedByTahunAjaranId($this)
                ->count($con);
        }

        return count($this->collPtkTerdaftarsRelatedByTahunAjaranId);
    }

    /**
     * Method called to associate a PtkTerdaftar object to this object
     * through the PtkTerdaftar foreign key attribute.
     *
     * @param    PtkTerdaftar $l PtkTerdaftar
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function addPtkTerdaftarRelatedByTahunAjaranId(PtkTerdaftar $l)
    {
        if ($this->collPtkTerdaftarsRelatedByTahunAjaranId === null) {
            $this->initPtkTerdaftarsRelatedByTahunAjaranId();
            $this->collPtkTerdaftarsRelatedByTahunAjaranIdPartial = true;
        }
        if (!in_array($l, $this->collPtkTerdaftarsRelatedByTahunAjaranId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPtkTerdaftarRelatedByTahunAjaranId($l);
        }

        return $this;
    }

    /**
     * @param	PtkTerdaftarRelatedByTahunAjaranId $ptkTerdaftarRelatedByTahunAjaranId The ptkTerdaftarRelatedByTahunAjaranId object to add.
     */
    protected function doAddPtkTerdaftarRelatedByTahunAjaranId($ptkTerdaftarRelatedByTahunAjaranId)
    {
        $this->collPtkTerdaftarsRelatedByTahunAjaranId[]= $ptkTerdaftarRelatedByTahunAjaranId;
        $ptkTerdaftarRelatedByTahunAjaranId->setTahunAjaranRelatedByTahunAjaranId($this);
    }

    /**
     * @param	PtkTerdaftarRelatedByTahunAjaranId $ptkTerdaftarRelatedByTahunAjaranId The ptkTerdaftarRelatedByTahunAjaranId object to remove.
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function removePtkTerdaftarRelatedByTahunAjaranId($ptkTerdaftarRelatedByTahunAjaranId)
    {
        if ($this->getPtkTerdaftarsRelatedByTahunAjaranId()->contains($ptkTerdaftarRelatedByTahunAjaranId)) {
            $this->collPtkTerdaftarsRelatedByTahunAjaranId->remove($this->collPtkTerdaftarsRelatedByTahunAjaranId->search($ptkTerdaftarRelatedByTahunAjaranId));
            if (null === $this->ptkTerdaftarsRelatedByTahunAjaranIdScheduledForDeletion) {
                $this->ptkTerdaftarsRelatedByTahunAjaranIdScheduledForDeletion = clone $this->collPtkTerdaftarsRelatedByTahunAjaranId;
                $this->ptkTerdaftarsRelatedByTahunAjaranIdScheduledForDeletion->clear();
            }
            $this->ptkTerdaftarsRelatedByTahunAjaranIdScheduledForDeletion[]= clone $ptkTerdaftarRelatedByTahunAjaranId;
            $ptkTerdaftarRelatedByTahunAjaranId->setTahunAjaranRelatedByTahunAjaranId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PtkTerdaftarsRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     */
    public function getPtkTerdaftarsRelatedByTahunAjaranIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkTerdaftarQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getPtkTerdaftarsRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PtkTerdaftarsRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     */
    public function getPtkTerdaftarsRelatedByTahunAjaranIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkTerdaftarQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getPtkTerdaftarsRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PtkTerdaftarsRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     */
    public function getPtkTerdaftarsRelatedByTahunAjaranIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkTerdaftarQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPtkTerdaftarsRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PtkTerdaftarsRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     */
    public function getPtkTerdaftarsRelatedByTahunAjaranIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkTerdaftarQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPtkTerdaftarsRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PtkTerdaftarsRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     */
    public function getPtkTerdaftarsRelatedByTahunAjaranIdJoinJenisKeluarRelatedByJenisKeluarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkTerdaftarQuery::create(null, $criteria);
        $query->joinWith('JenisKeluarRelatedByJenisKeluarId', $join_behavior);

        return $this->getPtkTerdaftarsRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PtkTerdaftarsRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     */
    public function getPtkTerdaftarsRelatedByTahunAjaranIdJoinJenisKeluarRelatedByJenisKeluarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkTerdaftarQuery::create(null, $criteria);
        $query->joinWith('JenisKeluarRelatedByJenisKeluarId', $join_behavior);

        return $this->getPtkTerdaftarsRelatedByTahunAjaranId($query, $con);
    }

    /**
     * Clears out the collPtkTerdaftarsRelatedByTahunAjaranId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return TahunAjaran The current object (for fluent API support)
     * @see        addPtkTerdaftarsRelatedByTahunAjaranId()
     */
    public function clearPtkTerdaftarsRelatedByTahunAjaranId()
    {
        $this->collPtkTerdaftarsRelatedByTahunAjaranId = null; // important to set this to null since that means it is uninitialized
        $this->collPtkTerdaftarsRelatedByTahunAjaranIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPtkTerdaftarsRelatedByTahunAjaranId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPtkTerdaftarsRelatedByTahunAjaranId($v = true)
    {
        $this->collPtkTerdaftarsRelatedByTahunAjaranIdPartial = $v;
    }

    /**
     * Initializes the collPtkTerdaftarsRelatedByTahunAjaranId collection.
     *
     * By default this just sets the collPtkTerdaftarsRelatedByTahunAjaranId collection to an empty array (like clearcollPtkTerdaftarsRelatedByTahunAjaranId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPtkTerdaftarsRelatedByTahunAjaranId($overrideExisting = true)
    {
        if (null !== $this->collPtkTerdaftarsRelatedByTahunAjaranId && !$overrideExisting) {
            return;
        }
        $this->collPtkTerdaftarsRelatedByTahunAjaranId = new PropelObjectCollection();
        $this->collPtkTerdaftarsRelatedByTahunAjaranId->setModel('PtkTerdaftar');
    }

    /**
     * Gets an array of PtkTerdaftar objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this TahunAjaran is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     * @throws PropelException
     */
    public function getPtkTerdaftarsRelatedByTahunAjaranId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPtkTerdaftarsRelatedByTahunAjaranIdPartial && !$this->isNew();
        if (null === $this->collPtkTerdaftarsRelatedByTahunAjaranId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPtkTerdaftarsRelatedByTahunAjaranId) {
                // return empty collection
                $this->initPtkTerdaftarsRelatedByTahunAjaranId();
            } else {
                $collPtkTerdaftarsRelatedByTahunAjaranId = PtkTerdaftarQuery::create(null, $criteria)
                    ->filterByTahunAjaranRelatedByTahunAjaranId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPtkTerdaftarsRelatedByTahunAjaranIdPartial && count($collPtkTerdaftarsRelatedByTahunAjaranId)) {
                      $this->initPtkTerdaftarsRelatedByTahunAjaranId(false);

                      foreach($collPtkTerdaftarsRelatedByTahunAjaranId as $obj) {
                        if (false == $this->collPtkTerdaftarsRelatedByTahunAjaranId->contains($obj)) {
                          $this->collPtkTerdaftarsRelatedByTahunAjaranId->append($obj);
                        }
                      }

                      $this->collPtkTerdaftarsRelatedByTahunAjaranIdPartial = true;
                    }

                    $collPtkTerdaftarsRelatedByTahunAjaranId->getInternalIterator()->rewind();
                    return $collPtkTerdaftarsRelatedByTahunAjaranId;
                }

                if($partial && $this->collPtkTerdaftarsRelatedByTahunAjaranId) {
                    foreach($this->collPtkTerdaftarsRelatedByTahunAjaranId as $obj) {
                        if($obj->isNew()) {
                            $collPtkTerdaftarsRelatedByTahunAjaranId[] = $obj;
                        }
                    }
                }

                $this->collPtkTerdaftarsRelatedByTahunAjaranId = $collPtkTerdaftarsRelatedByTahunAjaranId;
                $this->collPtkTerdaftarsRelatedByTahunAjaranIdPartial = false;
            }
        }

        return $this->collPtkTerdaftarsRelatedByTahunAjaranId;
    }

    /**
     * Sets a collection of PtkTerdaftarRelatedByTahunAjaranId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $ptkTerdaftarsRelatedByTahunAjaranId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function setPtkTerdaftarsRelatedByTahunAjaranId(PropelCollection $ptkTerdaftarsRelatedByTahunAjaranId, PropelPDO $con = null)
    {
        $ptkTerdaftarsRelatedByTahunAjaranIdToDelete = $this->getPtkTerdaftarsRelatedByTahunAjaranId(new Criteria(), $con)->diff($ptkTerdaftarsRelatedByTahunAjaranId);

        $this->ptkTerdaftarsRelatedByTahunAjaranIdScheduledForDeletion = unserialize(serialize($ptkTerdaftarsRelatedByTahunAjaranIdToDelete));

        foreach ($ptkTerdaftarsRelatedByTahunAjaranIdToDelete as $ptkTerdaftarRelatedByTahunAjaranIdRemoved) {
            $ptkTerdaftarRelatedByTahunAjaranIdRemoved->setTahunAjaranRelatedByTahunAjaranId(null);
        }

        $this->collPtkTerdaftarsRelatedByTahunAjaranId = null;
        foreach ($ptkTerdaftarsRelatedByTahunAjaranId as $ptkTerdaftarRelatedByTahunAjaranId) {
            $this->addPtkTerdaftarRelatedByTahunAjaranId($ptkTerdaftarRelatedByTahunAjaranId);
        }

        $this->collPtkTerdaftarsRelatedByTahunAjaranId = $ptkTerdaftarsRelatedByTahunAjaranId;
        $this->collPtkTerdaftarsRelatedByTahunAjaranIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PtkTerdaftar objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related PtkTerdaftar objects.
     * @throws PropelException
     */
    public function countPtkTerdaftarsRelatedByTahunAjaranId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPtkTerdaftarsRelatedByTahunAjaranIdPartial && !$this->isNew();
        if (null === $this->collPtkTerdaftarsRelatedByTahunAjaranId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPtkTerdaftarsRelatedByTahunAjaranId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPtkTerdaftarsRelatedByTahunAjaranId());
            }
            $query = PtkTerdaftarQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByTahunAjaranRelatedByTahunAjaranId($this)
                ->count($con);
        }

        return count($this->collPtkTerdaftarsRelatedByTahunAjaranId);
    }

    /**
     * Method called to associate a PtkTerdaftar object to this object
     * through the PtkTerdaftar foreign key attribute.
     *
     * @param    PtkTerdaftar $l PtkTerdaftar
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function addPtkTerdaftarRelatedByTahunAjaranId(PtkTerdaftar $l)
    {
        if ($this->collPtkTerdaftarsRelatedByTahunAjaranId === null) {
            $this->initPtkTerdaftarsRelatedByTahunAjaranId();
            $this->collPtkTerdaftarsRelatedByTahunAjaranIdPartial = true;
        }
        if (!in_array($l, $this->collPtkTerdaftarsRelatedByTahunAjaranId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPtkTerdaftarRelatedByTahunAjaranId($l);
        }

        return $this;
    }

    /**
     * @param	PtkTerdaftarRelatedByTahunAjaranId $ptkTerdaftarRelatedByTahunAjaranId The ptkTerdaftarRelatedByTahunAjaranId object to add.
     */
    protected function doAddPtkTerdaftarRelatedByTahunAjaranId($ptkTerdaftarRelatedByTahunAjaranId)
    {
        $this->collPtkTerdaftarsRelatedByTahunAjaranId[]= $ptkTerdaftarRelatedByTahunAjaranId;
        $ptkTerdaftarRelatedByTahunAjaranId->setTahunAjaranRelatedByTahunAjaranId($this);
    }

    /**
     * @param	PtkTerdaftarRelatedByTahunAjaranId $ptkTerdaftarRelatedByTahunAjaranId The ptkTerdaftarRelatedByTahunAjaranId object to remove.
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function removePtkTerdaftarRelatedByTahunAjaranId($ptkTerdaftarRelatedByTahunAjaranId)
    {
        if ($this->getPtkTerdaftarsRelatedByTahunAjaranId()->contains($ptkTerdaftarRelatedByTahunAjaranId)) {
            $this->collPtkTerdaftarsRelatedByTahunAjaranId->remove($this->collPtkTerdaftarsRelatedByTahunAjaranId->search($ptkTerdaftarRelatedByTahunAjaranId));
            if (null === $this->ptkTerdaftarsRelatedByTahunAjaranIdScheduledForDeletion) {
                $this->ptkTerdaftarsRelatedByTahunAjaranIdScheduledForDeletion = clone $this->collPtkTerdaftarsRelatedByTahunAjaranId;
                $this->ptkTerdaftarsRelatedByTahunAjaranIdScheduledForDeletion->clear();
            }
            $this->ptkTerdaftarsRelatedByTahunAjaranIdScheduledForDeletion[]= clone $ptkTerdaftarRelatedByTahunAjaranId;
            $ptkTerdaftarRelatedByTahunAjaranId->setTahunAjaranRelatedByTahunAjaranId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PtkTerdaftarsRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     */
    public function getPtkTerdaftarsRelatedByTahunAjaranIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkTerdaftarQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getPtkTerdaftarsRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PtkTerdaftarsRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     */
    public function getPtkTerdaftarsRelatedByTahunAjaranIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkTerdaftarQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getPtkTerdaftarsRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PtkTerdaftarsRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     */
    public function getPtkTerdaftarsRelatedByTahunAjaranIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkTerdaftarQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPtkTerdaftarsRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PtkTerdaftarsRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     */
    public function getPtkTerdaftarsRelatedByTahunAjaranIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkTerdaftarQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPtkTerdaftarsRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PtkTerdaftarsRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     */
    public function getPtkTerdaftarsRelatedByTahunAjaranIdJoinJenisKeluarRelatedByJenisKeluarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkTerdaftarQuery::create(null, $criteria);
        $query->joinWith('JenisKeluarRelatedByJenisKeluarId', $join_behavior);

        return $this->getPtkTerdaftarsRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PtkTerdaftarsRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     */
    public function getPtkTerdaftarsRelatedByTahunAjaranIdJoinJenisKeluarRelatedByJenisKeluarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkTerdaftarQuery::create(null, $criteria);
        $query->joinWith('JenisKeluarRelatedByJenisKeluarId', $join_behavior);

        return $this->getPtkTerdaftarsRelatedByTahunAjaranId($query, $con);
    }

    /**
     * Clears out the collSemesters collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return TahunAjaran The current object (for fluent API support)
     * @see        addSemesters()
     */
    public function clearSemesters()
    {
        $this->collSemesters = null; // important to set this to null since that means it is uninitialized
        $this->collSemestersPartial = null;

        return $this;
    }

    /**
     * reset is the collSemesters collection loaded partially
     *
     * @return void
     */
    public function resetPartialSemesters($v = true)
    {
        $this->collSemestersPartial = $v;
    }

    /**
     * Initializes the collSemesters collection.
     *
     * By default this just sets the collSemesters collection to an empty array (like clearcollSemesters());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSemesters($overrideExisting = true)
    {
        if (null !== $this->collSemesters && !$overrideExisting) {
            return;
        }
        $this->collSemesters = new PropelObjectCollection();
        $this->collSemesters->setModel('Semester');
    }

    /**
     * Gets an array of Semester objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this TahunAjaran is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Semester[] List of Semester objects
     * @throws PropelException
     */
    public function getSemesters($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSemestersPartial && !$this->isNew();
        if (null === $this->collSemesters || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSemesters) {
                // return empty collection
                $this->initSemesters();
            } else {
                $collSemesters = SemesterQuery::create(null, $criteria)
                    ->filterByTahunAjaran($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSemestersPartial && count($collSemesters)) {
                      $this->initSemesters(false);

                      foreach($collSemesters as $obj) {
                        if (false == $this->collSemesters->contains($obj)) {
                          $this->collSemesters->append($obj);
                        }
                      }

                      $this->collSemestersPartial = true;
                    }

                    $collSemesters->getInternalIterator()->rewind();
                    return $collSemesters;
                }

                if($partial && $this->collSemesters) {
                    foreach($this->collSemesters as $obj) {
                        if($obj->isNew()) {
                            $collSemesters[] = $obj;
                        }
                    }
                }

                $this->collSemesters = $collSemesters;
                $this->collSemestersPartial = false;
            }
        }

        return $this->collSemesters;
    }

    /**
     * Sets a collection of Semester objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $semesters A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function setSemesters(PropelCollection $semesters, PropelPDO $con = null)
    {
        $semestersToDelete = $this->getSemesters(new Criteria(), $con)->diff($semesters);

        $this->semestersScheduledForDeletion = unserialize(serialize($semestersToDelete));

        foreach ($semestersToDelete as $semesterRemoved) {
            $semesterRemoved->setTahunAjaran(null);
        }

        $this->collSemesters = null;
        foreach ($semesters as $semester) {
            $this->addSemester($semester);
        }

        $this->collSemesters = $semesters;
        $this->collSemestersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Semester objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Semester objects.
     * @throws PropelException
     */
    public function countSemesters(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSemestersPartial && !$this->isNew();
        if (null === $this->collSemesters || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSemesters) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSemesters());
            }
            $query = SemesterQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByTahunAjaran($this)
                ->count($con);
        }

        return count($this->collSemesters);
    }

    /**
     * Method called to associate a Semester object to this object
     * through the Semester foreign key attribute.
     *
     * @param    Semester $l Semester
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function addSemester(Semester $l)
    {
        if ($this->collSemesters === null) {
            $this->initSemesters();
            $this->collSemestersPartial = true;
        }
        if (!in_array($l, $this->collSemesters->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSemester($l);
        }

        return $this;
    }

    /**
     * @param	Semester $semester The semester object to add.
     */
    protected function doAddSemester($semester)
    {
        $this->collSemesters[]= $semester;
        $semester->setTahunAjaran($this);
    }

    /**
     * @param	Semester $semester The semester object to remove.
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function removeSemester($semester)
    {
        if ($this->getSemesters()->contains($semester)) {
            $this->collSemesters->remove($this->collSemesters->search($semester));
            if (null === $this->semestersScheduledForDeletion) {
                $this->semestersScheduledForDeletion = clone $this->collSemesters;
                $this->semestersScheduledForDeletion->clear();
            }
            $this->semestersScheduledForDeletion[]= clone $semester;
            $semester->setTahunAjaran(null);
        }

        return $this;
    }

    /**
     * Clears out the collPengawasTerdaftarsRelatedByTahunAjaranId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return TahunAjaran The current object (for fluent API support)
     * @see        addPengawasTerdaftarsRelatedByTahunAjaranId()
     */
    public function clearPengawasTerdaftarsRelatedByTahunAjaranId()
    {
        $this->collPengawasTerdaftarsRelatedByTahunAjaranId = null; // important to set this to null since that means it is uninitialized
        $this->collPengawasTerdaftarsRelatedByTahunAjaranIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPengawasTerdaftarsRelatedByTahunAjaranId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPengawasTerdaftarsRelatedByTahunAjaranId($v = true)
    {
        $this->collPengawasTerdaftarsRelatedByTahunAjaranIdPartial = $v;
    }

    /**
     * Initializes the collPengawasTerdaftarsRelatedByTahunAjaranId collection.
     *
     * By default this just sets the collPengawasTerdaftarsRelatedByTahunAjaranId collection to an empty array (like clearcollPengawasTerdaftarsRelatedByTahunAjaranId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPengawasTerdaftarsRelatedByTahunAjaranId($overrideExisting = true)
    {
        if (null !== $this->collPengawasTerdaftarsRelatedByTahunAjaranId && !$overrideExisting) {
            return;
        }
        $this->collPengawasTerdaftarsRelatedByTahunAjaranId = new PropelObjectCollection();
        $this->collPengawasTerdaftarsRelatedByTahunAjaranId->setModel('PengawasTerdaftar');
    }

    /**
     * Gets an array of PengawasTerdaftar objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this TahunAjaran is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     * @throws PropelException
     */
    public function getPengawasTerdaftarsRelatedByTahunAjaranId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPengawasTerdaftarsRelatedByTahunAjaranIdPartial && !$this->isNew();
        if (null === $this->collPengawasTerdaftarsRelatedByTahunAjaranId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPengawasTerdaftarsRelatedByTahunAjaranId) {
                // return empty collection
                $this->initPengawasTerdaftarsRelatedByTahunAjaranId();
            } else {
                $collPengawasTerdaftarsRelatedByTahunAjaranId = PengawasTerdaftarQuery::create(null, $criteria)
                    ->filterByTahunAjaranRelatedByTahunAjaranId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPengawasTerdaftarsRelatedByTahunAjaranIdPartial && count($collPengawasTerdaftarsRelatedByTahunAjaranId)) {
                      $this->initPengawasTerdaftarsRelatedByTahunAjaranId(false);

                      foreach($collPengawasTerdaftarsRelatedByTahunAjaranId as $obj) {
                        if (false == $this->collPengawasTerdaftarsRelatedByTahunAjaranId->contains($obj)) {
                          $this->collPengawasTerdaftarsRelatedByTahunAjaranId->append($obj);
                        }
                      }

                      $this->collPengawasTerdaftarsRelatedByTahunAjaranIdPartial = true;
                    }

                    $collPengawasTerdaftarsRelatedByTahunAjaranId->getInternalIterator()->rewind();
                    return $collPengawasTerdaftarsRelatedByTahunAjaranId;
                }

                if($partial && $this->collPengawasTerdaftarsRelatedByTahunAjaranId) {
                    foreach($this->collPengawasTerdaftarsRelatedByTahunAjaranId as $obj) {
                        if($obj->isNew()) {
                            $collPengawasTerdaftarsRelatedByTahunAjaranId[] = $obj;
                        }
                    }
                }

                $this->collPengawasTerdaftarsRelatedByTahunAjaranId = $collPengawasTerdaftarsRelatedByTahunAjaranId;
                $this->collPengawasTerdaftarsRelatedByTahunAjaranIdPartial = false;
            }
        }

        return $this->collPengawasTerdaftarsRelatedByTahunAjaranId;
    }

    /**
     * Sets a collection of PengawasTerdaftarRelatedByTahunAjaranId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $pengawasTerdaftarsRelatedByTahunAjaranId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function setPengawasTerdaftarsRelatedByTahunAjaranId(PropelCollection $pengawasTerdaftarsRelatedByTahunAjaranId, PropelPDO $con = null)
    {
        $pengawasTerdaftarsRelatedByTahunAjaranIdToDelete = $this->getPengawasTerdaftarsRelatedByTahunAjaranId(new Criteria(), $con)->diff($pengawasTerdaftarsRelatedByTahunAjaranId);

        $this->pengawasTerdaftarsRelatedByTahunAjaranIdScheduledForDeletion = unserialize(serialize($pengawasTerdaftarsRelatedByTahunAjaranIdToDelete));

        foreach ($pengawasTerdaftarsRelatedByTahunAjaranIdToDelete as $pengawasTerdaftarRelatedByTahunAjaranIdRemoved) {
            $pengawasTerdaftarRelatedByTahunAjaranIdRemoved->setTahunAjaranRelatedByTahunAjaranId(null);
        }

        $this->collPengawasTerdaftarsRelatedByTahunAjaranId = null;
        foreach ($pengawasTerdaftarsRelatedByTahunAjaranId as $pengawasTerdaftarRelatedByTahunAjaranId) {
            $this->addPengawasTerdaftarRelatedByTahunAjaranId($pengawasTerdaftarRelatedByTahunAjaranId);
        }

        $this->collPengawasTerdaftarsRelatedByTahunAjaranId = $pengawasTerdaftarsRelatedByTahunAjaranId;
        $this->collPengawasTerdaftarsRelatedByTahunAjaranIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PengawasTerdaftar objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related PengawasTerdaftar objects.
     * @throws PropelException
     */
    public function countPengawasTerdaftarsRelatedByTahunAjaranId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPengawasTerdaftarsRelatedByTahunAjaranIdPartial && !$this->isNew();
        if (null === $this->collPengawasTerdaftarsRelatedByTahunAjaranId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPengawasTerdaftarsRelatedByTahunAjaranId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPengawasTerdaftarsRelatedByTahunAjaranId());
            }
            $query = PengawasTerdaftarQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByTahunAjaranRelatedByTahunAjaranId($this)
                ->count($con);
        }

        return count($this->collPengawasTerdaftarsRelatedByTahunAjaranId);
    }

    /**
     * Method called to associate a PengawasTerdaftar object to this object
     * through the PengawasTerdaftar foreign key attribute.
     *
     * @param    PengawasTerdaftar $l PengawasTerdaftar
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function addPengawasTerdaftarRelatedByTahunAjaranId(PengawasTerdaftar $l)
    {
        if ($this->collPengawasTerdaftarsRelatedByTahunAjaranId === null) {
            $this->initPengawasTerdaftarsRelatedByTahunAjaranId();
            $this->collPengawasTerdaftarsRelatedByTahunAjaranIdPartial = true;
        }
        if (!in_array($l, $this->collPengawasTerdaftarsRelatedByTahunAjaranId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPengawasTerdaftarRelatedByTahunAjaranId($l);
        }

        return $this;
    }

    /**
     * @param	PengawasTerdaftarRelatedByTahunAjaranId $pengawasTerdaftarRelatedByTahunAjaranId The pengawasTerdaftarRelatedByTahunAjaranId object to add.
     */
    protected function doAddPengawasTerdaftarRelatedByTahunAjaranId($pengawasTerdaftarRelatedByTahunAjaranId)
    {
        $this->collPengawasTerdaftarsRelatedByTahunAjaranId[]= $pengawasTerdaftarRelatedByTahunAjaranId;
        $pengawasTerdaftarRelatedByTahunAjaranId->setTahunAjaranRelatedByTahunAjaranId($this);
    }

    /**
     * @param	PengawasTerdaftarRelatedByTahunAjaranId $pengawasTerdaftarRelatedByTahunAjaranId The pengawasTerdaftarRelatedByTahunAjaranId object to remove.
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function removePengawasTerdaftarRelatedByTahunAjaranId($pengawasTerdaftarRelatedByTahunAjaranId)
    {
        if ($this->getPengawasTerdaftarsRelatedByTahunAjaranId()->contains($pengawasTerdaftarRelatedByTahunAjaranId)) {
            $this->collPengawasTerdaftarsRelatedByTahunAjaranId->remove($this->collPengawasTerdaftarsRelatedByTahunAjaranId->search($pengawasTerdaftarRelatedByTahunAjaranId));
            if (null === $this->pengawasTerdaftarsRelatedByTahunAjaranIdScheduledForDeletion) {
                $this->pengawasTerdaftarsRelatedByTahunAjaranIdScheduledForDeletion = clone $this->collPengawasTerdaftarsRelatedByTahunAjaranId;
                $this->pengawasTerdaftarsRelatedByTahunAjaranIdScheduledForDeletion->clear();
            }
            $this->pengawasTerdaftarsRelatedByTahunAjaranIdScheduledForDeletion[]= clone $pengawasTerdaftarRelatedByTahunAjaranId;
            $pengawasTerdaftarRelatedByTahunAjaranId->setTahunAjaranRelatedByTahunAjaranId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByTahunAjaranIdJoinLembagaNonSekolahRelatedByLembagaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('LembagaNonSekolahRelatedByLembagaId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByTahunAjaranIdJoinLembagaNonSekolahRelatedByLembagaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('LembagaNonSekolahRelatedByLembagaId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByTahunAjaranIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByTahunAjaranIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByTahunAjaranIdJoinBidangStudiRelatedByBidangStudiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('BidangStudiRelatedByBidangStudiId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByTahunAjaranIdJoinBidangStudiRelatedByBidangStudiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('BidangStudiRelatedByBidangStudiId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByTahunAjaranIdJoinJenisKeluarRelatedByJenisKeluarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('JenisKeluarRelatedByJenisKeluarId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByTahunAjaranIdJoinJenisKeluarRelatedByJenisKeluarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('JenisKeluarRelatedByJenisKeluarId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByTahunAjaranIdJoinJenjangKepengawasanRelatedByJenjangKepengawasanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('JenjangKepengawasanRelatedByJenjangKepengawasanId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByTahunAjaranIdJoinJenjangKepengawasanRelatedByJenjangKepengawasanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('JenjangKepengawasanRelatedByJenjangKepengawasanId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByTahunAjaranIdJoinMataPelajaranRelatedByMataPelajaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('MataPelajaranRelatedByMataPelajaranId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByTahunAjaranIdJoinMataPelajaranRelatedByMataPelajaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('MataPelajaranRelatedByMataPelajaranId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByTahunAjaranId($query, $con);
    }

    /**
     * Clears out the collPengawasTerdaftarsRelatedByTahunAjaranId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return TahunAjaran The current object (for fluent API support)
     * @see        addPengawasTerdaftarsRelatedByTahunAjaranId()
     */
    public function clearPengawasTerdaftarsRelatedByTahunAjaranId()
    {
        $this->collPengawasTerdaftarsRelatedByTahunAjaranId = null; // important to set this to null since that means it is uninitialized
        $this->collPengawasTerdaftarsRelatedByTahunAjaranIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPengawasTerdaftarsRelatedByTahunAjaranId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPengawasTerdaftarsRelatedByTahunAjaranId($v = true)
    {
        $this->collPengawasTerdaftarsRelatedByTahunAjaranIdPartial = $v;
    }

    /**
     * Initializes the collPengawasTerdaftarsRelatedByTahunAjaranId collection.
     *
     * By default this just sets the collPengawasTerdaftarsRelatedByTahunAjaranId collection to an empty array (like clearcollPengawasTerdaftarsRelatedByTahunAjaranId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPengawasTerdaftarsRelatedByTahunAjaranId($overrideExisting = true)
    {
        if (null !== $this->collPengawasTerdaftarsRelatedByTahunAjaranId && !$overrideExisting) {
            return;
        }
        $this->collPengawasTerdaftarsRelatedByTahunAjaranId = new PropelObjectCollection();
        $this->collPengawasTerdaftarsRelatedByTahunAjaranId->setModel('PengawasTerdaftar');
    }

    /**
     * Gets an array of PengawasTerdaftar objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this TahunAjaran is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     * @throws PropelException
     */
    public function getPengawasTerdaftarsRelatedByTahunAjaranId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPengawasTerdaftarsRelatedByTahunAjaranIdPartial && !$this->isNew();
        if (null === $this->collPengawasTerdaftarsRelatedByTahunAjaranId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPengawasTerdaftarsRelatedByTahunAjaranId) {
                // return empty collection
                $this->initPengawasTerdaftarsRelatedByTahunAjaranId();
            } else {
                $collPengawasTerdaftarsRelatedByTahunAjaranId = PengawasTerdaftarQuery::create(null, $criteria)
                    ->filterByTahunAjaranRelatedByTahunAjaranId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPengawasTerdaftarsRelatedByTahunAjaranIdPartial && count($collPengawasTerdaftarsRelatedByTahunAjaranId)) {
                      $this->initPengawasTerdaftarsRelatedByTahunAjaranId(false);

                      foreach($collPengawasTerdaftarsRelatedByTahunAjaranId as $obj) {
                        if (false == $this->collPengawasTerdaftarsRelatedByTahunAjaranId->contains($obj)) {
                          $this->collPengawasTerdaftarsRelatedByTahunAjaranId->append($obj);
                        }
                      }

                      $this->collPengawasTerdaftarsRelatedByTahunAjaranIdPartial = true;
                    }

                    $collPengawasTerdaftarsRelatedByTahunAjaranId->getInternalIterator()->rewind();
                    return $collPengawasTerdaftarsRelatedByTahunAjaranId;
                }

                if($partial && $this->collPengawasTerdaftarsRelatedByTahunAjaranId) {
                    foreach($this->collPengawasTerdaftarsRelatedByTahunAjaranId as $obj) {
                        if($obj->isNew()) {
                            $collPengawasTerdaftarsRelatedByTahunAjaranId[] = $obj;
                        }
                    }
                }

                $this->collPengawasTerdaftarsRelatedByTahunAjaranId = $collPengawasTerdaftarsRelatedByTahunAjaranId;
                $this->collPengawasTerdaftarsRelatedByTahunAjaranIdPartial = false;
            }
        }

        return $this->collPengawasTerdaftarsRelatedByTahunAjaranId;
    }

    /**
     * Sets a collection of PengawasTerdaftarRelatedByTahunAjaranId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $pengawasTerdaftarsRelatedByTahunAjaranId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function setPengawasTerdaftarsRelatedByTahunAjaranId(PropelCollection $pengawasTerdaftarsRelatedByTahunAjaranId, PropelPDO $con = null)
    {
        $pengawasTerdaftarsRelatedByTahunAjaranIdToDelete = $this->getPengawasTerdaftarsRelatedByTahunAjaranId(new Criteria(), $con)->diff($pengawasTerdaftarsRelatedByTahunAjaranId);

        $this->pengawasTerdaftarsRelatedByTahunAjaranIdScheduledForDeletion = unserialize(serialize($pengawasTerdaftarsRelatedByTahunAjaranIdToDelete));

        foreach ($pengawasTerdaftarsRelatedByTahunAjaranIdToDelete as $pengawasTerdaftarRelatedByTahunAjaranIdRemoved) {
            $pengawasTerdaftarRelatedByTahunAjaranIdRemoved->setTahunAjaranRelatedByTahunAjaranId(null);
        }

        $this->collPengawasTerdaftarsRelatedByTahunAjaranId = null;
        foreach ($pengawasTerdaftarsRelatedByTahunAjaranId as $pengawasTerdaftarRelatedByTahunAjaranId) {
            $this->addPengawasTerdaftarRelatedByTahunAjaranId($pengawasTerdaftarRelatedByTahunAjaranId);
        }

        $this->collPengawasTerdaftarsRelatedByTahunAjaranId = $pengawasTerdaftarsRelatedByTahunAjaranId;
        $this->collPengawasTerdaftarsRelatedByTahunAjaranIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PengawasTerdaftar objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related PengawasTerdaftar objects.
     * @throws PropelException
     */
    public function countPengawasTerdaftarsRelatedByTahunAjaranId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPengawasTerdaftarsRelatedByTahunAjaranIdPartial && !$this->isNew();
        if (null === $this->collPengawasTerdaftarsRelatedByTahunAjaranId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPengawasTerdaftarsRelatedByTahunAjaranId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPengawasTerdaftarsRelatedByTahunAjaranId());
            }
            $query = PengawasTerdaftarQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByTahunAjaranRelatedByTahunAjaranId($this)
                ->count($con);
        }

        return count($this->collPengawasTerdaftarsRelatedByTahunAjaranId);
    }

    /**
     * Method called to associate a PengawasTerdaftar object to this object
     * through the PengawasTerdaftar foreign key attribute.
     *
     * @param    PengawasTerdaftar $l PengawasTerdaftar
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function addPengawasTerdaftarRelatedByTahunAjaranId(PengawasTerdaftar $l)
    {
        if ($this->collPengawasTerdaftarsRelatedByTahunAjaranId === null) {
            $this->initPengawasTerdaftarsRelatedByTahunAjaranId();
            $this->collPengawasTerdaftarsRelatedByTahunAjaranIdPartial = true;
        }
        if (!in_array($l, $this->collPengawasTerdaftarsRelatedByTahunAjaranId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPengawasTerdaftarRelatedByTahunAjaranId($l);
        }

        return $this;
    }

    /**
     * @param	PengawasTerdaftarRelatedByTahunAjaranId $pengawasTerdaftarRelatedByTahunAjaranId The pengawasTerdaftarRelatedByTahunAjaranId object to add.
     */
    protected function doAddPengawasTerdaftarRelatedByTahunAjaranId($pengawasTerdaftarRelatedByTahunAjaranId)
    {
        $this->collPengawasTerdaftarsRelatedByTahunAjaranId[]= $pengawasTerdaftarRelatedByTahunAjaranId;
        $pengawasTerdaftarRelatedByTahunAjaranId->setTahunAjaranRelatedByTahunAjaranId($this);
    }

    /**
     * @param	PengawasTerdaftarRelatedByTahunAjaranId $pengawasTerdaftarRelatedByTahunAjaranId The pengawasTerdaftarRelatedByTahunAjaranId object to remove.
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function removePengawasTerdaftarRelatedByTahunAjaranId($pengawasTerdaftarRelatedByTahunAjaranId)
    {
        if ($this->getPengawasTerdaftarsRelatedByTahunAjaranId()->contains($pengawasTerdaftarRelatedByTahunAjaranId)) {
            $this->collPengawasTerdaftarsRelatedByTahunAjaranId->remove($this->collPengawasTerdaftarsRelatedByTahunAjaranId->search($pengawasTerdaftarRelatedByTahunAjaranId));
            if (null === $this->pengawasTerdaftarsRelatedByTahunAjaranIdScheduledForDeletion) {
                $this->pengawasTerdaftarsRelatedByTahunAjaranIdScheduledForDeletion = clone $this->collPengawasTerdaftarsRelatedByTahunAjaranId;
                $this->pengawasTerdaftarsRelatedByTahunAjaranIdScheduledForDeletion->clear();
            }
            $this->pengawasTerdaftarsRelatedByTahunAjaranIdScheduledForDeletion[]= clone $pengawasTerdaftarRelatedByTahunAjaranId;
            $pengawasTerdaftarRelatedByTahunAjaranId->setTahunAjaranRelatedByTahunAjaranId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByTahunAjaranIdJoinLembagaNonSekolahRelatedByLembagaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('LembagaNonSekolahRelatedByLembagaId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByTahunAjaranIdJoinLembagaNonSekolahRelatedByLembagaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('LembagaNonSekolahRelatedByLembagaId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByTahunAjaranIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByTahunAjaranIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByTahunAjaranIdJoinBidangStudiRelatedByBidangStudiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('BidangStudiRelatedByBidangStudiId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByTahunAjaranIdJoinBidangStudiRelatedByBidangStudiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('BidangStudiRelatedByBidangStudiId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByTahunAjaranIdJoinJenisKeluarRelatedByJenisKeluarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('JenisKeluarRelatedByJenisKeluarId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByTahunAjaranIdJoinJenisKeluarRelatedByJenisKeluarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('JenisKeluarRelatedByJenisKeluarId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByTahunAjaranIdJoinJenjangKepengawasanRelatedByJenjangKepengawasanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('JenjangKepengawasanRelatedByJenjangKepengawasanId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByTahunAjaranIdJoinJenjangKepengawasanRelatedByJenjangKepengawasanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('JenjangKepengawasanRelatedByJenjangKepengawasanId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByTahunAjaranIdJoinMataPelajaranRelatedByMataPelajaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('MataPelajaranRelatedByMataPelajaranId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByTahunAjaranIdJoinMataPelajaranRelatedByMataPelajaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('MataPelajaranRelatedByMataPelajaranId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByTahunAjaranId($query, $con);
    }

    /**
     * Clears out the collPtkBarusRelatedByTahunAjaranId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return TahunAjaran The current object (for fluent API support)
     * @see        addPtkBarusRelatedByTahunAjaranId()
     */
    public function clearPtkBarusRelatedByTahunAjaranId()
    {
        $this->collPtkBarusRelatedByTahunAjaranId = null; // important to set this to null since that means it is uninitialized
        $this->collPtkBarusRelatedByTahunAjaranIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPtkBarusRelatedByTahunAjaranId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPtkBarusRelatedByTahunAjaranId($v = true)
    {
        $this->collPtkBarusRelatedByTahunAjaranIdPartial = $v;
    }

    /**
     * Initializes the collPtkBarusRelatedByTahunAjaranId collection.
     *
     * By default this just sets the collPtkBarusRelatedByTahunAjaranId collection to an empty array (like clearcollPtkBarusRelatedByTahunAjaranId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPtkBarusRelatedByTahunAjaranId($overrideExisting = true)
    {
        if (null !== $this->collPtkBarusRelatedByTahunAjaranId && !$overrideExisting) {
            return;
        }
        $this->collPtkBarusRelatedByTahunAjaranId = new PropelObjectCollection();
        $this->collPtkBarusRelatedByTahunAjaranId->setModel('PtkBaru');
    }

    /**
     * Gets an array of PtkBaru objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this TahunAjaran is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|PtkBaru[] List of PtkBaru objects
     * @throws PropelException
     */
    public function getPtkBarusRelatedByTahunAjaranId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPtkBarusRelatedByTahunAjaranIdPartial && !$this->isNew();
        if (null === $this->collPtkBarusRelatedByTahunAjaranId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPtkBarusRelatedByTahunAjaranId) {
                // return empty collection
                $this->initPtkBarusRelatedByTahunAjaranId();
            } else {
                $collPtkBarusRelatedByTahunAjaranId = PtkBaruQuery::create(null, $criteria)
                    ->filterByTahunAjaranRelatedByTahunAjaranId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPtkBarusRelatedByTahunAjaranIdPartial && count($collPtkBarusRelatedByTahunAjaranId)) {
                      $this->initPtkBarusRelatedByTahunAjaranId(false);

                      foreach($collPtkBarusRelatedByTahunAjaranId as $obj) {
                        if (false == $this->collPtkBarusRelatedByTahunAjaranId->contains($obj)) {
                          $this->collPtkBarusRelatedByTahunAjaranId->append($obj);
                        }
                      }

                      $this->collPtkBarusRelatedByTahunAjaranIdPartial = true;
                    }

                    $collPtkBarusRelatedByTahunAjaranId->getInternalIterator()->rewind();
                    return $collPtkBarusRelatedByTahunAjaranId;
                }

                if($partial && $this->collPtkBarusRelatedByTahunAjaranId) {
                    foreach($this->collPtkBarusRelatedByTahunAjaranId as $obj) {
                        if($obj->isNew()) {
                            $collPtkBarusRelatedByTahunAjaranId[] = $obj;
                        }
                    }
                }

                $this->collPtkBarusRelatedByTahunAjaranId = $collPtkBarusRelatedByTahunAjaranId;
                $this->collPtkBarusRelatedByTahunAjaranIdPartial = false;
            }
        }

        return $this->collPtkBarusRelatedByTahunAjaranId;
    }

    /**
     * Sets a collection of PtkBaruRelatedByTahunAjaranId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $ptkBarusRelatedByTahunAjaranId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function setPtkBarusRelatedByTahunAjaranId(PropelCollection $ptkBarusRelatedByTahunAjaranId, PropelPDO $con = null)
    {
        $ptkBarusRelatedByTahunAjaranIdToDelete = $this->getPtkBarusRelatedByTahunAjaranId(new Criteria(), $con)->diff($ptkBarusRelatedByTahunAjaranId);

        $this->ptkBarusRelatedByTahunAjaranIdScheduledForDeletion = unserialize(serialize($ptkBarusRelatedByTahunAjaranIdToDelete));

        foreach ($ptkBarusRelatedByTahunAjaranIdToDelete as $ptkBaruRelatedByTahunAjaranIdRemoved) {
            $ptkBaruRelatedByTahunAjaranIdRemoved->setTahunAjaranRelatedByTahunAjaranId(null);
        }

        $this->collPtkBarusRelatedByTahunAjaranId = null;
        foreach ($ptkBarusRelatedByTahunAjaranId as $ptkBaruRelatedByTahunAjaranId) {
            $this->addPtkBaruRelatedByTahunAjaranId($ptkBaruRelatedByTahunAjaranId);
        }

        $this->collPtkBarusRelatedByTahunAjaranId = $ptkBarusRelatedByTahunAjaranId;
        $this->collPtkBarusRelatedByTahunAjaranIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PtkBaru objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related PtkBaru objects.
     * @throws PropelException
     */
    public function countPtkBarusRelatedByTahunAjaranId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPtkBarusRelatedByTahunAjaranIdPartial && !$this->isNew();
        if (null === $this->collPtkBarusRelatedByTahunAjaranId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPtkBarusRelatedByTahunAjaranId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPtkBarusRelatedByTahunAjaranId());
            }
            $query = PtkBaruQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByTahunAjaranRelatedByTahunAjaranId($this)
                ->count($con);
        }

        return count($this->collPtkBarusRelatedByTahunAjaranId);
    }

    /**
     * Method called to associate a PtkBaru object to this object
     * through the PtkBaru foreign key attribute.
     *
     * @param    PtkBaru $l PtkBaru
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function addPtkBaruRelatedByTahunAjaranId(PtkBaru $l)
    {
        if ($this->collPtkBarusRelatedByTahunAjaranId === null) {
            $this->initPtkBarusRelatedByTahunAjaranId();
            $this->collPtkBarusRelatedByTahunAjaranIdPartial = true;
        }
        if (!in_array($l, $this->collPtkBarusRelatedByTahunAjaranId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPtkBaruRelatedByTahunAjaranId($l);
        }

        return $this;
    }

    /**
     * @param	PtkBaruRelatedByTahunAjaranId $ptkBaruRelatedByTahunAjaranId The ptkBaruRelatedByTahunAjaranId object to add.
     */
    protected function doAddPtkBaruRelatedByTahunAjaranId($ptkBaruRelatedByTahunAjaranId)
    {
        $this->collPtkBarusRelatedByTahunAjaranId[]= $ptkBaruRelatedByTahunAjaranId;
        $ptkBaruRelatedByTahunAjaranId->setTahunAjaranRelatedByTahunAjaranId($this);
    }

    /**
     * @param	PtkBaruRelatedByTahunAjaranId $ptkBaruRelatedByTahunAjaranId The ptkBaruRelatedByTahunAjaranId object to remove.
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function removePtkBaruRelatedByTahunAjaranId($ptkBaruRelatedByTahunAjaranId)
    {
        if ($this->getPtkBarusRelatedByTahunAjaranId()->contains($ptkBaruRelatedByTahunAjaranId)) {
            $this->collPtkBarusRelatedByTahunAjaranId->remove($this->collPtkBarusRelatedByTahunAjaranId->search($ptkBaruRelatedByTahunAjaranId));
            if (null === $this->ptkBarusRelatedByTahunAjaranIdScheduledForDeletion) {
                $this->ptkBarusRelatedByTahunAjaranIdScheduledForDeletion = clone $this->collPtkBarusRelatedByTahunAjaranId;
                $this->ptkBarusRelatedByTahunAjaranIdScheduledForDeletion->clear();
            }
            $this->ptkBarusRelatedByTahunAjaranIdScheduledForDeletion[]= clone $ptkBaruRelatedByTahunAjaranId;
            $ptkBaruRelatedByTahunAjaranId->setTahunAjaranRelatedByTahunAjaranId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PtkBarusRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkBaru[] List of PtkBaru objects
     */
    public function getPtkBarusRelatedByTahunAjaranIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkBaruQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getPtkBarusRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PtkBarusRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkBaru[] List of PtkBaru objects
     */
    public function getPtkBarusRelatedByTahunAjaranIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkBaruQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getPtkBarusRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PtkBarusRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkBaru[] List of PtkBaru objects
     */
    public function getPtkBarusRelatedByTahunAjaranIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkBaruQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPtkBarusRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PtkBarusRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkBaru[] List of PtkBaru objects
     */
    public function getPtkBarusRelatedByTahunAjaranIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkBaruQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPtkBarusRelatedByTahunAjaranId($query, $con);
    }

    /**
     * Clears out the collPtkBarusRelatedByTahunAjaranId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return TahunAjaran The current object (for fluent API support)
     * @see        addPtkBarusRelatedByTahunAjaranId()
     */
    public function clearPtkBarusRelatedByTahunAjaranId()
    {
        $this->collPtkBarusRelatedByTahunAjaranId = null; // important to set this to null since that means it is uninitialized
        $this->collPtkBarusRelatedByTahunAjaranIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPtkBarusRelatedByTahunAjaranId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPtkBarusRelatedByTahunAjaranId($v = true)
    {
        $this->collPtkBarusRelatedByTahunAjaranIdPartial = $v;
    }

    /**
     * Initializes the collPtkBarusRelatedByTahunAjaranId collection.
     *
     * By default this just sets the collPtkBarusRelatedByTahunAjaranId collection to an empty array (like clearcollPtkBarusRelatedByTahunAjaranId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPtkBarusRelatedByTahunAjaranId($overrideExisting = true)
    {
        if (null !== $this->collPtkBarusRelatedByTahunAjaranId && !$overrideExisting) {
            return;
        }
        $this->collPtkBarusRelatedByTahunAjaranId = new PropelObjectCollection();
        $this->collPtkBarusRelatedByTahunAjaranId->setModel('PtkBaru');
    }

    /**
     * Gets an array of PtkBaru objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this TahunAjaran is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|PtkBaru[] List of PtkBaru objects
     * @throws PropelException
     */
    public function getPtkBarusRelatedByTahunAjaranId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPtkBarusRelatedByTahunAjaranIdPartial && !$this->isNew();
        if (null === $this->collPtkBarusRelatedByTahunAjaranId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPtkBarusRelatedByTahunAjaranId) {
                // return empty collection
                $this->initPtkBarusRelatedByTahunAjaranId();
            } else {
                $collPtkBarusRelatedByTahunAjaranId = PtkBaruQuery::create(null, $criteria)
                    ->filterByTahunAjaranRelatedByTahunAjaranId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPtkBarusRelatedByTahunAjaranIdPartial && count($collPtkBarusRelatedByTahunAjaranId)) {
                      $this->initPtkBarusRelatedByTahunAjaranId(false);

                      foreach($collPtkBarusRelatedByTahunAjaranId as $obj) {
                        if (false == $this->collPtkBarusRelatedByTahunAjaranId->contains($obj)) {
                          $this->collPtkBarusRelatedByTahunAjaranId->append($obj);
                        }
                      }

                      $this->collPtkBarusRelatedByTahunAjaranIdPartial = true;
                    }

                    $collPtkBarusRelatedByTahunAjaranId->getInternalIterator()->rewind();
                    return $collPtkBarusRelatedByTahunAjaranId;
                }

                if($partial && $this->collPtkBarusRelatedByTahunAjaranId) {
                    foreach($this->collPtkBarusRelatedByTahunAjaranId as $obj) {
                        if($obj->isNew()) {
                            $collPtkBarusRelatedByTahunAjaranId[] = $obj;
                        }
                    }
                }

                $this->collPtkBarusRelatedByTahunAjaranId = $collPtkBarusRelatedByTahunAjaranId;
                $this->collPtkBarusRelatedByTahunAjaranIdPartial = false;
            }
        }

        return $this->collPtkBarusRelatedByTahunAjaranId;
    }

    /**
     * Sets a collection of PtkBaruRelatedByTahunAjaranId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $ptkBarusRelatedByTahunAjaranId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function setPtkBarusRelatedByTahunAjaranId(PropelCollection $ptkBarusRelatedByTahunAjaranId, PropelPDO $con = null)
    {
        $ptkBarusRelatedByTahunAjaranIdToDelete = $this->getPtkBarusRelatedByTahunAjaranId(new Criteria(), $con)->diff($ptkBarusRelatedByTahunAjaranId);

        $this->ptkBarusRelatedByTahunAjaranIdScheduledForDeletion = unserialize(serialize($ptkBarusRelatedByTahunAjaranIdToDelete));

        foreach ($ptkBarusRelatedByTahunAjaranIdToDelete as $ptkBaruRelatedByTahunAjaranIdRemoved) {
            $ptkBaruRelatedByTahunAjaranIdRemoved->setTahunAjaranRelatedByTahunAjaranId(null);
        }

        $this->collPtkBarusRelatedByTahunAjaranId = null;
        foreach ($ptkBarusRelatedByTahunAjaranId as $ptkBaruRelatedByTahunAjaranId) {
            $this->addPtkBaruRelatedByTahunAjaranId($ptkBaruRelatedByTahunAjaranId);
        }

        $this->collPtkBarusRelatedByTahunAjaranId = $ptkBarusRelatedByTahunAjaranId;
        $this->collPtkBarusRelatedByTahunAjaranIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PtkBaru objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related PtkBaru objects.
     * @throws PropelException
     */
    public function countPtkBarusRelatedByTahunAjaranId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPtkBarusRelatedByTahunAjaranIdPartial && !$this->isNew();
        if (null === $this->collPtkBarusRelatedByTahunAjaranId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPtkBarusRelatedByTahunAjaranId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPtkBarusRelatedByTahunAjaranId());
            }
            $query = PtkBaruQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByTahunAjaranRelatedByTahunAjaranId($this)
                ->count($con);
        }

        return count($this->collPtkBarusRelatedByTahunAjaranId);
    }

    /**
     * Method called to associate a PtkBaru object to this object
     * through the PtkBaru foreign key attribute.
     *
     * @param    PtkBaru $l PtkBaru
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function addPtkBaruRelatedByTahunAjaranId(PtkBaru $l)
    {
        if ($this->collPtkBarusRelatedByTahunAjaranId === null) {
            $this->initPtkBarusRelatedByTahunAjaranId();
            $this->collPtkBarusRelatedByTahunAjaranIdPartial = true;
        }
        if (!in_array($l, $this->collPtkBarusRelatedByTahunAjaranId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPtkBaruRelatedByTahunAjaranId($l);
        }

        return $this;
    }

    /**
     * @param	PtkBaruRelatedByTahunAjaranId $ptkBaruRelatedByTahunAjaranId The ptkBaruRelatedByTahunAjaranId object to add.
     */
    protected function doAddPtkBaruRelatedByTahunAjaranId($ptkBaruRelatedByTahunAjaranId)
    {
        $this->collPtkBarusRelatedByTahunAjaranId[]= $ptkBaruRelatedByTahunAjaranId;
        $ptkBaruRelatedByTahunAjaranId->setTahunAjaranRelatedByTahunAjaranId($this);
    }

    /**
     * @param	PtkBaruRelatedByTahunAjaranId $ptkBaruRelatedByTahunAjaranId The ptkBaruRelatedByTahunAjaranId object to remove.
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function removePtkBaruRelatedByTahunAjaranId($ptkBaruRelatedByTahunAjaranId)
    {
        if ($this->getPtkBarusRelatedByTahunAjaranId()->contains($ptkBaruRelatedByTahunAjaranId)) {
            $this->collPtkBarusRelatedByTahunAjaranId->remove($this->collPtkBarusRelatedByTahunAjaranId->search($ptkBaruRelatedByTahunAjaranId));
            if (null === $this->ptkBarusRelatedByTahunAjaranIdScheduledForDeletion) {
                $this->ptkBarusRelatedByTahunAjaranIdScheduledForDeletion = clone $this->collPtkBarusRelatedByTahunAjaranId;
                $this->ptkBarusRelatedByTahunAjaranIdScheduledForDeletion->clear();
            }
            $this->ptkBarusRelatedByTahunAjaranIdScheduledForDeletion[]= clone $ptkBaruRelatedByTahunAjaranId;
            $ptkBaruRelatedByTahunAjaranId->setTahunAjaranRelatedByTahunAjaranId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PtkBarusRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkBaru[] List of PtkBaru objects
     */
    public function getPtkBarusRelatedByTahunAjaranIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkBaruQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getPtkBarusRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PtkBarusRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkBaru[] List of PtkBaru objects
     */
    public function getPtkBarusRelatedByTahunAjaranIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkBaruQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getPtkBarusRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PtkBarusRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkBaru[] List of PtkBaru objects
     */
    public function getPtkBarusRelatedByTahunAjaranIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkBaruQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPtkBarusRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related PtkBarusRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkBaru[] List of PtkBaru objects
     */
    public function getPtkBarusRelatedByTahunAjaranIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkBaruQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPtkBarusRelatedByTahunAjaranId($query, $con);
    }

    /**
     * Clears out the collDemografisRelatedByTahunAjaranId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return TahunAjaran The current object (for fluent API support)
     * @see        addDemografisRelatedByTahunAjaranId()
     */
    public function clearDemografisRelatedByTahunAjaranId()
    {
        $this->collDemografisRelatedByTahunAjaranId = null; // important to set this to null since that means it is uninitialized
        $this->collDemografisRelatedByTahunAjaranIdPartial = null;

        return $this;
    }

    /**
     * reset is the collDemografisRelatedByTahunAjaranId collection loaded partially
     *
     * @return void
     */
    public function resetPartialDemografisRelatedByTahunAjaranId($v = true)
    {
        $this->collDemografisRelatedByTahunAjaranIdPartial = $v;
    }

    /**
     * Initializes the collDemografisRelatedByTahunAjaranId collection.
     *
     * By default this just sets the collDemografisRelatedByTahunAjaranId collection to an empty array (like clearcollDemografisRelatedByTahunAjaranId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initDemografisRelatedByTahunAjaranId($overrideExisting = true)
    {
        if (null !== $this->collDemografisRelatedByTahunAjaranId && !$overrideExisting) {
            return;
        }
        $this->collDemografisRelatedByTahunAjaranId = new PropelObjectCollection();
        $this->collDemografisRelatedByTahunAjaranId->setModel('Demografi');
    }

    /**
     * Gets an array of Demografi objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this TahunAjaran is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Demografi[] List of Demografi objects
     * @throws PropelException
     */
    public function getDemografisRelatedByTahunAjaranId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collDemografisRelatedByTahunAjaranIdPartial && !$this->isNew();
        if (null === $this->collDemografisRelatedByTahunAjaranId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collDemografisRelatedByTahunAjaranId) {
                // return empty collection
                $this->initDemografisRelatedByTahunAjaranId();
            } else {
                $collDemografisRelatedByTahunAjaranId = DemografiQuery::create(null, $criteria)
                    ->filterByTahunAjaranRelatedByTahunAjaranId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collDemografisRelatedByTahunAjaranIdPartial && count($collDemografisRelatedByTahunAjaranId)) {
                      $this->initDemografisRelatedByTahunAjaranId(false);

                      foreach($collDemografisRelatedByTahunAjaranId as $obj) {
                        if (false == $this->collDemografisRelatedByTahunAjaranId->contains($obj)) {
                          $this->collDemografisRelatedByTahunAjaranId->append($obj);
                        }
                      }

                      $this->collDemografisRelatedByTahunAjaranIdPartial = true;
                    }

                    $collDemografisRelatedByTahunAjaranId->getInternalIterator()->rewind();
                    return $collDemografisRelatedByTahunAjaranId;
                }

                if($partial && $this->collDemografisRelatedByTahunAjaranId) {
                    foreach($this->collDemografisRelatedByTahunAjaranId as $obj) {
                        if($obj->isNew()) {
                            $collDemografisRelatedByTahunAjaranId[] = $obj;
                        }
                    }
                }

                $this->collDemografisRelatedByTahunAjaranId = $collDemografisRelatedByTahunAjaranId;
                $this->collDemografisRelatedByTahunAjaranIdPartial = false;
            }
        }

        return $this->collDemografisRelatedByTahunAjaranId;
    }

    /**
     * Sets a collection of DemografiRelatedByTahunAjaranId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $demografisRelatedByTahunAjaranId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function setDemografisRelatedByTahunAjaranId(PropelCollection $demografisRelatedByTahunAjaranId, PropelPDO $con = null)
    {
        $demografisRelatedByTahunAjaranIdToDelete = $this->getDemografisRelatedByTahunAjaranId(new Criteria(), $con)->diff($demografisRelatedByTahunAjaranId);

        $this->demografisRelatedByTahunAjaranIdScheduledForDeletion = unserialize(serialize($demografisRelatedByTahunAjaranIdToDelete));

        foreach ($demografisRelatedByTahunAjaranIdToDelete as $demografiRelatedByTahunAjaranIdRemoved) {
            $demografiRelatedByTahunAjaranIdRemoved->setTahunAjaranRelatedByTahunAjaranId(null);
        }

        $this->collDemografisRelatedByTahunAjaranId = null;
        foreach ($demografisRelatedByTahunAjaranId as $demografiRelatedByTahunAjaranId) {
            $this->addDemografiRelatedByTahunAjaranId($demografiRelatedByTahunAjaranId);
        }

        $this->collDemografisRelatedByTahunAjaranId = $demografisRelatedByTahunAjaranId;
        $this->collDemografisRelatedByTahunAjaranIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Demografi objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Demografi objects.
     * @throws PropelException
     */
    public function countDemografisRelatedByTahunAjaranId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collDemografisRelatedByTahunAjaranIdPartial && !$this->isNew();
        if (null === $this->collDemografisRelatedByTahunAjaranId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collDemografisRelatedByTahunAjaranId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getDemografisRelatedByTahunAjaranId());
            }
            $query = DemografiQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByTahunAjaranRelatedByTahunAjaranId($this)
                ->count($con);
        }

        return count($this->collDemografisRelatedByTahunAjaranId);
    }

    /**
     * Method called to associate a Demografi object to this object
     * through the Demografi foreign key attribute.
     *
     * @param    Demografi $l Demografi
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function addDemografiRelatedByTahunAjaranId(Demografi $l)
    {
        if ($this->collDemografisRelatedByTahunAjaranId === null) {
            $this->initDemografisRelatedByTahunAjaranId();
            $this->collDemografisRelatedByTahunAjaranIdPartial = true;
        }
        if (!in_array($l, $this->collDemografisRelatedByTahunAjaranId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddDemografiRelatedByTahunAjaranId($l);
        }

        return $this;
    }

    /**
     * @param	DemografiRelatedByTahunAjaranId $demografiRelatedByTahunAjaranId The demografiRelatedByTahunAjaranId object to add.
     */
    protected function doAddDemografiRelatedByTahunAjaranId($demografiRelatedByTahunAjaranId)
    {
        $this->collDemografisRelatedByTahunAjaranId[]= $demografiRelatedByTahunAjaranId;
        $demografiRelatedByTahunAjaranId->setTahunAjaranRelatedByTahunAjaranId($this);
    }

    /**
     * @param	DemografiRelatedByTahunAjaranId $demografiRelatedByTahunAjaranId The demografiRelatedByTahunAjaranId object to remove.
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function removeDemografiRelatedByTahunAjaranId($demografiRelatedByTahunAjaranId)
    {
        if ($this->getDemografisRelatedByTahunAjaranId()->contains($demografiRelatedByTahunAjaranId)) {
            $this->collDemografisRelatedByTahunAjaranId->remove($this->collDemografisRelatedByTahunAjaranId->search($demografiRelatedByTahunAjaranId));
            if (null === $this->demografisRelatedByTahunAjaranIdScheduledForDeletion) {
                $this->demografisRelatedByTahunAjaranIdScheduledForDeletion = clone $this->collDemografisRelatedByTahunAjaranId;
                $this->demografisRelatedByTahunAjaranIdScheduledForDeletion->clear();
            }
            $this->demografisRelatedByTahunAjaranIdScheduledForDeletion[]= clone $demografiRelatedByTahunAjaranId;
            $demografiRelatedByTahunAjaranId->setTahunAjaranRelatedByTahunAjaranId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related DemografisRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Demografi[] List of Demografi objects
     */
    public function getDemografisRelatedByTahunAjaranIdJoinMstWilayahRelatedByKodeWilayah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = DemografiQuery::create(null, $criteria);
        $query->joinWith('MstWilayahRelatedByKodeWilayah', $join_behavior);

        return $this->getDemografisRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related DemografisRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Demografi[] List of Demografi objects
     */
    public function getDemografisRelatedByTahunAjaranIdJoinMstWilayahRelatedByKodeWilayah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = DemografiQuery::create(null, $criteria);
        $query->joinWith('MstWilayahRelatedByKodeWilayah', $join_behavior);

        return $this->getDemografisRelatedByTahunAjaranId($query, $con);
    }

    /**
     * Clears out the collDemografisRelatedByTahunAjaranId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return TahunAjaran The current object (for fluent API support)
     * @see        addDemografisRelatedByTahunAjaranId()
     */
    public function clearDemografisRelatedByTahunAjaranId()
    {
        $this->collDemografisRelatedByTahunAjaranId = null; // important to set this to null since that means it is uninitialized
        $this->collDemografisRelatedByTahunAjaranIdPartial = null;

        return $this;
    }

    /**
     * reset is the collDemografisRelatedByTahunAjaranId collection loaded partially
     *
     * @return void
     */
    public function resetPartialDemografisRelatedByTahunAjaranId($v = true)
    {
        $this->collDemografisRelatedByTahunAjaranIdPartial = $v;
    }

    /**
     * Initializes the collDemografisRelatedByTahunAjaranId collection.
     *
     * By default this just sets the collDemografisRelatedByTahunAjaranId collection to an empty array (like clearcollDemografisRelatedByTahunAjaranId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initDemografisRelatedByTahunAjaranId($overrideExisting = true)
    {
        if (null !== $this->collDemografisRelatedByTahunAjaranId && !$overrideExisting) {
            return;
        }
        $this->collDemografisRelatedByTahunAjaranId = new PropelObjectCollection();
        $this->collDemografisRelatedByTahunAjaranId->setModel('Demografi');
    }

    /**
     * Gets an array of Demografi objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this TahunAjaran is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Demografi[] List of Demografi objects
     * @throws PropelException
     */
    public function getDemografisRelatedByTahunAjaranId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collDemografisRelatedByTahunAjaranIdPartial && !$this->isNew();
        if (null === $this->collDemografisRelatedByTahunAjaranId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collDemografisRelatedByTahunAjaranId) {
                // return empty collection
                $this->initDemografisRelatedByTahunAjaranId();
            } else {
                $collDemografisRelatedByTahunAjaranId = DemografiQuery::create(null, $criteria)
                    ->filterByTahunAjaranRelatedByTahunAjaranId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collDemografisRelatedByTahunAjaranIdPartial && count($collDemografisRelatedByTahunAjaranId)) {
                      $this->initDemografisRelatedByTahunAjaranId(false);

                      foreach($collDemografisRelatedByTahunAjaranId as $obj) {
                        if (false == $this->collDemografisRelatedByTahunAjaranId->contains($obj)) {
                          $this->collDemografisRelatedByTahunAjaranId->append($obj);
                        }
                      }

                      $this->collDemografisRelatedByTahunAjaranIdPartial = true;
                    }

                    $collDemografisRelatedByTahunAjaranId->getInternalIterator()->rewind();
                    return $collDemografisRelatedByTahunAjaranId;
                }

                if($partial && $this->collDemografisRelatedByTahunAjaranId) {
                    foreach($this->collDemografisRelatedByTahunAjaranId as $obj) {
                        if($obj->isNew()) {
                            $collDemografisRelatedByTahunAjaranId[] = $obj;
                        }
                    }
                }

                $this->collDemografisRelatedByTahunAjaranId = $collDemografisRelatedByTahunAjaranId;
                $this->collDemografisRelatedByTahunAjaranIdPartial = false;
            }
        }

        return $this->collDemografisRelatedByTahunAjaranId;
    }

    /**
     * Sets a collection of DemografiRelatedByTahunAjaranId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $demografisRelatedByTahunAjaranId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function setDemografisRelatedByTahunAjaranId(PropelCollection $demografisRelatedByTahunAjaranId, PropelPDO $con = null)
    {
        $demografisRelatedByTahunAjaranIdToDelete = $this->getDemografisRelatedByTahunAjaranId(new Criteria(), $con)->diff($demografisRelatedByTahunAjaranId);

        $this->demografisRelatedByTahunAjaranIdScheduledForDeletion = unserialize(serialize($demografisRelatedByTahunAjaranIdToDelete));

        foreach ($demografisRelatedByTahunAjaranIdToDelete as $demografiRelatedByTahunAjaranIdRemoved) {
            $demografiRelatedByTahunAjaranIdRemoved->setTahunAjaranRelatedByTahunAjaranId(null);
        }

        $this->collDemografisRelatedByTahunAjaranId = null;
        foreach ($demografisRelatedByTahunAjaranId as $demografiRelatedByTahunAjaranId) {
            $this->addDemografiRelatedByTahunAjaranId($demografiRelatedByTahunAjaranId);
        }

        $this->collDemografisRelatedByTahunAjaranId = $demografisRelatedByTahunAjaranId;
        $this->collDemografisRelatedByTahunAjaranIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Demografi objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Demografi objects.
     * @throws PropelException
     */
    public function countDemografisRelatedByTahunAjaranId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collDemografisRelatedByTahunAjaranIdPartial && !$this->isNew();
        if (null === $this->collDemografisRelatedByTahunAjaranId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collDemografisRelatedByTahunAjaranId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getDemografisRelatedByTahunAjaranId());
            }
            $query = DemografiQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByTahunAjaranRelatedByTahunAjaranId($this)
                ->count($con);
        }

        return count($this->collDemografisRelatedByTahunAjaranId);
    }

    /**
     * Method called to associate a Demografi object to this object
     * through the Demografi foreign key attribute.
     *
     * @param    Demografi $l Demografi
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function addDemografiRelatedByTahunAjaranId(Demografi $l)
    {
        if ($this->collDemografisRelatedByTahunAjaranId === null) {
            $this->initDemografisRelatedByTahunAjaranId();
            $this->collDemografisRelatedByTahunAjaranIdPartial = true;
        }
        if (!in_array($l, $this->collDemografisRelatedByTahunAjaranId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddDemografiRelatedByTahunAjaranId($l);
        }

        return $this;
    }

    /**
     * @param	DemografiRelatedByTahunAjaranId $demografiRelatedByTahunAjaranId The demografiRelatedByTahunAjaranId object to add.
     */
    protected function doAddDemografiRelatedByTahunAjaranId($demografiRelatedByTahunAjaranId)
    {
        $this->collDemografisRelatedByTahunAjaranId[]= $demografiRelatedByTahunAjaranId;
        $demografiRelatedByTahunAjaranId->setTahunAjaranRelatedByTahunAjaranId($this);
    }

    /**
     * @param	DemografiRelatedByTahunAjaranId $demografiRelatedByTahunAjaranId The demografiRelatedByTahunAjaranId object to remove.
     * @return TahunAjaran The current object (for fluent API support)
     */
    public function removeDemografiRelatedByTahunAjaranId($demografiRelatedByTahunAjaranId)
    {
        if ($this->getDemografisRelatedByTahunAjaranId()->contains($demografiRelatedByTahunAjaranId)) {
            $this->collDemografisRelatedByTahunAjaranId->remove($this->collDemografisRelatedByTahunAjaranId->search($demografiRelatedByTahunAjaranId));
            if (null === $this->demografisRelatedByTahunAjaranIdScheduledForDeletion) {
                $this->demografisRelatedByTahunAjaranIdScheduledForDeletion = clone $this->collDemografisRelatedByTahunAjaranId;
                $this->demografisRelatedByTahunAjaranIdScheduledForDeletion->clear();
            }
            $this->demografisRelatedByTahunAjaranIdScheduledForDeletion[]= clone $demografiRelatedByTahunAjaranId;
            $demografiRelatedByTahunAjaranId->setTahunAjaranRelatedByTahunAjaranId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related DemografisRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Demografi[] List of Demografi objects
     */
    public function getDemografisRelatedByTahunAjaranIdJoinMstWilayahRelatedByKodeWilayah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = DemografiQuery::create(null, $criteria);
        $query->joinWith('MstWilayahRelatedByKodeWilayah', $join_behavior);

        return $this->getDemografisRelatedByTahunAjaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TahunAjaran is new, it will return
     * an empty collection; or if this TahunAjaran has previously
     * been saved, it will retrieve related DemografisRelatedByTahunAjaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TahunAjaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Demografi[] List of Demografi objects
     */
    public function getDemografisRelatedByTahunAjaranIdJoinMstWilayahRelatedByKodeWilayah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = DemografiQuery::create(null, $criteria);
        $query->joinWith('MstWilayahRelatedByKodeWilayah', $join_behavior);

        return $this->getDemografisRelatedByTahunAjaranId($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->tahun_ajaran_id = null;
        $this->nama = null;
        $this->periode_aktif = null;
        $this->tanggal_mulai = null;
        $this->tanggal_selesai = null;
        $this->create_date = null;
        $this->last_update = null;
        $this->expired_date = null;
        $this->last_sync = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collTemplateUns) {
                foreach ($this->collTemplateUns as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collBeasiswaPesertaDidiksRelatedByTahunSelesai) {
                foreach ($this->collBeasiswaPesertaDidiksRelatedByTahunSelesai as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collBeasiswaPesertaDidiksRelatedByTahunMulai) {
                foreach ($this->collBeasiswaPesertaDidiksRelatedByTahunMulai as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collBeasiswaPesertaDidiksRelatedByTahunSelesai) {
                foreach ($this->collBeasiswaPesertaDidiksRelatedByTahunSelesai as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collBeasiswaPesertaDidiksRelatedByTahunMulai) {
                foreach ($this->collBeasiswaPesertaDidiksRelatedByTahunMulai as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPesertaDidikBarusRelatedByTahunAjaranId) {
                foreach ($this->collPesertaDidikBarusRelatedByTahunAjaranId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPesertaDidikBarusRelatedByTahunAjaranId) {
                foreach ($this->collPesertaDidikBarusRelatedByTahunAjaranId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPtkTerdaftarsRelatedByTahunAjaranId) {
                foreach ($this->collPtkTerdaftarsRelatedByTahunAjaranId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPtkTerdaftarsRelatedByTahunAjaranId) {
                foreach ($this->collPtkTerdaftarsRelatedByTahunAjaranId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSemesters) {
                foreach ($this->collSemesters as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPengawasTerdaftarsRelatedByTahunAjaranId) {
                foreach ($this->collPengawasTerdaftarsRelatedByTahunAjaranId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPengawasTerdaftarsRelatedByTahunAjaranId) {
                foreach ($this->collPengawasTerdaftarsRelatedByTahunAjaranId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPtkBarusRelatedByTahunAjaranId) {
                foreach ($this->collPtkBarusRelatedByTahunAjaranId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPtkBarusRelatedByTahunAjaranId) {
                foreach ($this->collPtkBarusRelatedByTahunAjaranId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collDemografisRelatedByTahunAjaranId) {
                foreach ($this->collDemografisRelatedByTahunAjaranId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collDemografisRelatedByTahunAjaranId) {
                foreach ($this->collDemografisRelatedByTahunAjaranId as $o) {
                    $o->clearAllReferences($deep);
                }
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collTemplateUns instanceof PropelCollection) {
            $this->collTemplateUns->clearIterator();
        }
        $this->collTemplateUns = null;
        if ($this->collBeasiswaPesertaDidiksRelatedByTahunSelesai instanceof PropelCollection) {
            $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai->clearIterator();
        }
        $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai = null;
        if ($this->collBeasiswaPesertaDidiksRelatedByTahunMulai instanceof PropelCollection) {
            $this->collBeasiswaPesertaDidiksRelatedByTahunMulai->clearIterator();
        }
        $this->collBeasiswaPesertaDidiksRelatedByTahunMulai = null;
        if ($this->collBeasiswaPesertaDidiksRelatedByTahunSelesai instanceof PropelCollection) {
            $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai->clearIterator();
        }
        $this->collBeasiswaPesertaDidiksRelatedByTahunSelesai = null;
        if ($this->collBeasiswaPesertaDidiksRelatedByTahunMulai instanceof PropelCollection) {
            $this->collBeasiswaPesertaDidiksRelatedByTahunMulai->clearIterator();
        }
        $this->collBeasiswaPesertaDidiksRelatedByTahunMulai = null;
        if ($this->collPesertaDidikBarusRelatedByTahunAjaranId instanceof PropelCollection) {
            $this->collPesertaDidikBarusRelatedByTahunAjaranId->clearIterator();
        }
        $this->collPesertaDidikBarusRelatedByTahunAjaranId = null;
        if ($this->collPesertaDidikBarusRelatedByTahunAjaranId instanceof PropelCollection) {
            $this->collPesertaDidikBarusRelatedByTahunAjaranId->clearIterator();
        }
        $this->collPesertaDidikBarusRelatedByTahunAjaranId = null;
        if ($this->collPtkTerdaftarsRelatedByTahunAjaranId instanceof PropelCollection) {
            $this->collPtkTerdaftarsRelatedByTahunAjaranId->clearIterator();
        }
        $this->collPtkTerdaftarsRelatedByTahunAjaranId = null;
        if ($this->collPtkTerdaftarsRelatedByTahunAjaranId instanceof PropelCollection) {
            $this->collPtkTerdaftarsRelatedByTahunAjaranId->clearIterator();
        }
        $this->collPtkTerdaftarsRelatedByTahunAjaranId = null;
        if ($this->collSemesters instanceof PropelCollection) {
            $this->collSemesters->clearIterator();
        }
        $this->collSemesters = null;
        if ($this->collPengawasTerdaftarsRelatedByTahunAjaranId instanceof PropelCollection) {
            $this->collPengawasTerdaftarsRelatedByTahunAjaranId->clearIterator();
        }
        $this->collPengawasTerdaftarsRelatedByTahunAjaranId = null;
        if ($this->collPengawasTerdaftarsRelatedByTahunAjaranId instanceof PropelCollection) {
            $this->collPengawasTerdaftarsRelatedByTahunAjaranId->clearIterator();
        }
        $this->collPengawasTerdaftarsRelatedByTahunAjaranId = null;
        if ($this->collPtkBarusRelatedByTahunAjaranId instanceof PropelCollection) {
            $this->collPtkBarusRelatedByTahunAjaranId->clearIterator();
        }
        $this->collPtkBarusRelatedByTahunAjaranId = null;
        if ($this->collPtkBarusRelatedByTahunAjaranId instanceof PropelCollection) {
            $this->collPtkBarusRelatedByTahunAjaranId->clearIterator();
        }
        $this->collPtkBarusRelatedByTahunAjaranId = null;
        if ($this->collDemografisRelatedByTahunAjaranId instanceof PropelCollection) {
            $this->collDemografisRelatedByTahunAjaranId->clearIterator();
        }
        $this->collDemografisRelatedByTahunAjaranId = null;
        if ($this->collDemografisRelatedByTahunAjaranId instanceof PropelCollection) {
            $this->collDemografisRelatedByTahunAjaranId->clearIterator();
        }
        $this->collDemografisRelatedByTahunAjaranId = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(TahunAjaranPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
