<?php

namespace angulex\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelDateTime;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\AnggotaRombel;
use angulex\Model\AnggotaRombelQuery;
use angulex\Model\JurusanSp;
use angulex\Model\JurusanSpQuery;
use angulex\Model\KebutuhanKhusus;
use angulex\Model\KebutuhanKhususQuery;
use angulex\Model\Kurikulum;
use angulex\Model\KurikulumQuery;
use angulex\Model\Pembelajaran;
use angulex\Model\PembelajaranQuery;
use angulex\Model\Prasarana;
use angulex\Model\PrasaranaQuery;
use angulex\Model\Ptk;
use angulex\Model\PtkQuery;
use angulex\Model\RombonganBelajar;
use angulex\Model\RombonganBelajarPeer;
use angulex\Model\RombonganBelajarQuery;
use angulex\Model\Sekolah;
use angulex\Model\SekolahQuery;
use angulex\Model\Semester;
use angulex\Model\SemesterQuery;
use angulex\Model\TingkatPendidikan;
use angulex\Model\TingkatPendidikanQuery;
use angulex\Model\VldRombel;
use angulex\Model\VldRombelQuery;

/**
 * Base class that represents a row from the 'rombongan_belajar' table.
 *
 * 
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseRombonganBelajar extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'angulex\\Model\\RombonganBelajarPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        RombonganBelajarPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the rombongan_belajar_id field.
     * @var        string
     */
    protected $rombongan_belajar_id;

    /**
     * The value for the semester_id field.
     * @var        string
     */
    protected $semester_id;

    /**
     * The value for the sekolah_id field.
     * @var        string
     */
    protected $sekolah_id;

    /**
     * The value for the tingkat_pendidikan_id field.
     * @var        string
     */
    protected $tingkat_pendidikan_id;

    /**
     * The value for the jurusan_sp_id field.
     * @var        string
     */
    protected $jurusan_sp_id;

    /**
     * The value for the kurikulum_id field.
     * @var        int
     */
    protected $kurikulum_id;

    /**
     * The value for the nama field.
     * @var        string
     */
    protected $nama;

    /**
     * The value for the ptk_id field.
     * @var        string
     */
    protected $ptk_id;

    /**
     * The value for the prasarana_id field.
     * @var        string
     */
    protected $prasarana_id;

    /**
     * The value for the moving_class field.
     * @var        string
     */
    protected $moving_class;

    /**
     * The value for the jenis_rombel field.
     * Note: this column has a database default value of: '((1))'
     * @var        string
     */
    protected $jenis_rombel;

    /**
     * The value for the sks field.
     * Note: this column has a database default value of: '((0))'
     * @var        string
     */
    protected $sks;

    /**
     * The value for the kebutuhan_khusus_id field.
     * @var        int
     */
    protected $kebutuhan_khusus_id;

    /**
     * The value for the last_update field.
     * @var        string
     */
    protected $last_update;

    /**
     * The value for the soft_delete field.
     * @var        string
     */
    protected $soft_delete;

    /**
     * The value for the last_sync field.
     * @var        string
     */
    protected $last_sync;

    /**
     * The value for the updater_id field.
     * @var        string
     */
    protected $updater_id;

    /**
     * @var        JurusanSp
     */
    protected $aJurusanSpRelatedByJurusanSpId;

    /**
     * @var        JurusanSp
     */
    protected $aJurusanSpRelatedByJurusanSpId;

    /**
     * @var        Prasarana
     */
    protected $aPrasaranaRelatedByPrasaranaId;

    /**
     * @var        Prasarana
     */
    protected $aPrasaranaRelatedByPrasaranaId;

    /**
     * @var        Ptk
     */
    protected $aPtkRelatedByPtkId;

    /**
     * @var        Ptk
     */
    protected $aPtkRelatedByPtkId;

    /**
     * @var        Sekolah
     */
    protected $aSekolahRelatedBySekolahId;

    /**
     * @var        Sekolah
     */
    protected $aSekolahRelatedBySekolahId;

    /**
     * @var        KebutuhanKhusus
     */
    protected $aKebutuhanKhususRelatedByKebutuhanKhususId;

    /**
     * @var        KebutuhanKhusus
     */
    protected $aKebutuhanKhususRelatedByKebutuhanKhususId;

    /**
     * @var        Kurikulum
     */
    protected $aKurikulumRelatedByKurikulumId;

    /**
     * @var        Kurikulum
     */
    protected $aKurikulumRelatedByKurikulumId;

    /**
     * @var        Semester
     */
    protected $aSemesterRelatedBySemesterId;

    /**
     * @var        Semester
     */
    protected $aSemesterRelatedBySemesterId;

    /**
     * @var        TingkatPendidikan
     */
    protected $aTingkatPendidikanRelatedByTingkatPendidikanId;

    /**
     * @var        TingkatPendidikan
     */
    protected $aTingkatPendidikanRelatedByTingkatPendidikanId;

    /**
     * @var        PropelObjectCollection|VldRombel[] Collection to store aggregation of VldRombel objects.
     */
    protected $collVldRombelsRelatedByRombonganBelajarId;
    protected $collVldRombelsRelatedByRombonganBelajarIdPartial;

    /**
     * @var        PropelObjectCollection|VldRombel[] Collection to store aggregation of VldRombel objects.
     */
    protected $collVldRombelsRelatedByRombonganBelajarId;
    protected $collVldRombelsRelatedByRombonganBelajarIdPartial;

    /**
     * @var        PropelObjectCollection|AnggotaRombel[] Collection to store aggregation of AnggotaRombel objects.
     */
    protected $collAnggotaRombelsRelatedByRombonganBelajarId;
    protected $collAnggotaRombelsRelatedByRombonganBelajarIdPartial;

    /**
     * @var        PropelObjectCollection|AnggotaRombel[] Collection to store aggregation of AnggotaRombel objects.
     */
    protected $collAnggotaRombelsRelatedByRombonganBelajarId;
    protected $collAnggotaRombelsRelatedByRombonganBelajarIdPartial;

    /**
     * @var        PropelObjectCollection|Pembelajaran[] Collection to store aggregation of Pembelajaran objects.
     */
    protected $collPembelajaransRelatedByRombonganBelajarId;
    protected $collPembelajaransRelatedByRombonganBelajarIdPartial;

    /**
     * @var        PropelObjectCollection|Pembelajaran[] Collection to store aggregation of Pembelajaran objects.
     */
    protected $collPembelajaransRelatedByRombonganBelajarId;
    protected $collPembelajaransRelatedByRombonganBelajarIdPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldRombelsRelatedByRombonganBelajarIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldRombelsRelatedByRombonganBelajarIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $anggotaRombelsRelatedByRombonganBelajarIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $anggotaRombelsRelatedByRombonganBelajarIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $pembelajaransRelatedByRombonganBelajarIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $pembelajaransRelatedByRombonganBelajarIdScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->jenis_rombel = '((1))';
        $this->sks = '((0))';
    }

    /**
     * Initializes internal state of BaseRombonganBelajar object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [rombongan_belajar_id] column value.
     * 
     * @return string
     */
    public function getRombonganBelajarId()
    {
        return $this->rombongan_belajar_id;
    }

    /**
     * Get the [semester_id] column value.
     * 
     * @return string
     */
    public function getSemesterId()
    {
        return $this->semester_id;
    }

    /**
     * Get the [sekolah_id] column value.
     * 
     * @return string
     */
    public function getSekolahId()
    {
        return $this->sekolah_id;
    }

    /**
     * Get the [tingkat_pendidikan_id] column value.
     * 
     * @return string
     */
    public function getTingkatPendidikanId()
    {
        return $this->tingkat_pendidikan_id;
    }

    /**
     * Get the [jurusan_sp_id] column value.
     * 
     * @return string
     */
    public function getJurusanSpId()
    {
        return $this->jurusan_sp_id;
    }

    /**
     * Get the [kurikulum_id] column value.
     * 
     * @return int
     */
    public function getKurikulumId()
    {
        return $this->kurikulum_id;
    }

    /**
     * Get the [nama] column value.
     * 
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Get the [ptk_id] column value.
     * 
     * @return string
     */
    public function getPtkId()
    {
        return $this->ptk_id;
    }

    /**
     * Get the [prasarana_id] column value.
     * 
     * @return string
     */
    public function getPrasaranaId()
    {
        return $this->prasarana_id;
    }

    /**
     * Get the [moving_class] column value.
     * 
     * @return string
     */
    public function getMovingClass()
    {
        return $this->moving_class;
    }

    /**
     * Get the [jenis_rombel] column value.
     * 
     * @return string
     */
    public function getJenisRombel()
    {
        return $this->jenis_rombel;
    }

    /**
     * Get the [sks] column value.
     * 
     * @return string
     */
    public function getSks()
    {
        return $this->sks;
    }

    /**
     * Get the [kebutuhan_khusus_id] column value.
     * 
     * @return int
     */
    public function getKebutuhanKhususId()
    {
        return $this->kebutuhan_khusus_id;
    }

    /**
     * Get the [optionally formatted] temporal [last_update] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastUpdate($format = 'Y-m-d H:i:s')
    {
        if ($this->last_update === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_update);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_update, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [soft_delete] column value.
     * 
     * @return string
     */
    public function getSoftDelete()
    {
        return $this->soft_delete;
    }

    /**
     * Get the [optionally formatted] temporal [last_sync] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastSync($format = 'Y-m-d H:i:s')
    {
        if ($this->last_sync === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_sync);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_sync, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [updater_id] column value.
     * 
     * @return string
     */
    public function getUpdaterId()
    {
        return $this->updater_id;
    }

    /**
     * Set the value of [rombongan_belajar_id] column.
     * 
     * @param string $v new value
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setRombonganBelajarId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->rombongan_belajar_id !== $v) {
            $this->rombongan_belajar_id = $v;
            $this->modifiedColumns[] = RombonganBelajarPeer::ROMBONGAN_BELAJAR_ID;
        }


        return $this;
    } // setRombonganBelajarId()

    /**
     * Set the value of [semester_id] column.
     * 
     * @param string $v new value
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setSemesterId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->semester_id !== $v) {
            $this->semester_id = $v;
            $this->modifiedColumns[] = RombonganBelajarPeer::SEMESTER_ID;
        }

        if ($this->aSemesterRelatedBySemesterId !== null && $this->aSemesterRelatedBySemesterId->getSemesterId() !== $v) {
            $this->aSemesterRelatedBySemesterId = null;
        }

        if ($this->aSemesterRelatedBySemesterId !== null && $this->aSemesterRelatedBySemesterId->getSemesterId() !== $v) {
            $this->aSemesterRelatedBySemesterId = null;
        }


        return $this;
    } // setSemesterId()

    /**
     * Set the value of [sekolah_id] column.
     * 
     * @param string $v new value
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setSekolahId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->sekolah_id !== $v) {
            $this->sekolah_id = $v;
            $this->modifiedColumns[] = RombonganBelajarPeer::SEKOLAH_ID;
        }

        if ($this->aSekolahRelatedBySekolahId !== null && $this->aSekolahRelatedBySekolahId->getSekolahId() !== $v) {
            $this->aSekolahRelatedBySekolahId = null;
        }

        if ($this->aSekolahRelatedBySekolahId !== null && $this->aSekolahRelatedBySekolahId->getSekolahId() !== $v) {
            $this->aSekolahRelatedBySekolahId = null;
        }


        return $this;
    } // setSekolahId()

    /**
     * Set the value of [tingkat_pendidikan_id] column.
     * 
     * @param string $v new value
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setTingkatPendidikanId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tingkat_pendidikan_id !== $v) {
            $this->tingkat_pendidikan_id = $v;
            $this->modifiedColumns[] = RombonganBelajarPeer::TINGKAT_PENDIDIKAN_ID;
        }

        if ($this->aTingkatPendidikanRelatedByTingkatPendidikanId !== null && $this->aTingkatPendidikanRelatedByTingkatPendidikanId->getTingkatPendidikanId() !== $v) {
            $this->aTingkatPendidikanRelatedByTingkatPendidikanId = null;
        }

        if ($this->aTingkatPendidikanRelatedByTingkatPendidikanId !== null && $this->aTingkatPendidikanRelatedByTingkatPendidikanId->getTingkatPendidikanId() !== $v) {
            $this->aTingkatPendidikanRelatedByTingkatPendidikanId = null;
        }


        return $this;
    } // setTingkatPendidikanId()

    /**
     * Set the value of [jurusan_sp_id] column.
     * 
     * @param string $v new value
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setJurusanSpId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->jurusan_sp_id !== $v) {
            $this->jurusan_sp_id = $v;
            $this->modifiedColumns[] = RombonganBelajarPeer::JURUSAN_SP_ID;
        }

        if ($this->aJurusanSpRelatedByJurusanSpId !== null && $this->aJurusanSpRelatedByJurusanSpId->getJurusanSpId() !== $v) {
            $this->aJurusanSpRelatedByJurusanSpId = null;
        }

        if ($this->aJurusanSpRelatedByJurusanSpId !== null && $this->aJurusanSpRelatedByJurusanSpId->getJurusanSpId() !== $v) {
            $this->aJurusanSpRelatedByJurusanSpId = null;
        }


        return $this;
    } // setJurusanSpId()

    /**
     * Set the value of [kurikulum_id] column.
     * 
     * @param int $v new value
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setKurikulumId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->kurikulum_id !== $v) {
            $this->kurikulum_id = $v;
            $this->modifiedColumns[] = RombonganBelajarPeer::KURIKULUM_ID;
        }

        if ($this->aKurikulumRelatedByKurikulumId !== null && $this->aKurikulumRelatedByKurikulumId->getKurikulumId() !== $v) {
            $this->aKurikulumRelatedByKurikulumId = null;
        }

        if ($this->aKurikulumRelatedByKurikulumId !== null && $this->aKurikulumRelatedByKurikulumId->getKurikulumId() !== $v) {
            $this->aKurikulumRelatedByKurikulumId = null;
        }


        return $this;
    } // setKurikulumId()

    /**
     * Set the value of [nama] column.
     * 
     * @param string $v new value
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setNama($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nama !== $v) {
            $this->nama = $v;
            $this->modifiedColumns[] = RombonganBelajarPeer::NAMA;
        }


        return $this;
    } // setNama()

    /**
     * Set the value of [ptk_id] column.
     * 
     * @param string $v new value
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setPtkId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->ptk_id !== $v) {
            $this->ptk_id = $v;
            $this->modifiedColumns[] = RombonganBelajarPeer::PTK_ID;
        }

        if ($this->aPtkRelatedByPtkId !== null && $this->aPtkRelatedByPtkId->getPtkId() !== $v) {
            $this->aPtkRelatedByPtkId = null;
        }

        if ($this->aPtkRelatedByPtkId !== null && $this->aPtkRelatedByPtkId->getPtkId() !== $v) {
            $this->aPtkRelatedByPtkId = null;
        }


        return $this;
    } // setPtkId()

    /**
     * Set the value of [prasarana_id] column.
     * 
     * @param string $v new value
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setPrasaranaId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->prasarana_id !== $v) {
            $this->prasarana_id = $v;
            $this->modifiedColumns[] = RombonganBelajarPeer::PRASARANA_ID;
        }

        if ($this->aPrasaranaRelatedByPrasaranaId !== null && $this->aPrasaranaRelatedByPrasaranaId->getPrasaranaId() !== $v) {
            $this->aPrasaranaRelatedByPrasaranaId = null;
        }

        if ($this->aPrasaranaRelatedByPrasaranaId !== null && $this->aPrasaranaRelatedByPrasaranaId->getPrasaranaId() !== $v) {
            $this->aPrasaranaRelatedByPrasaranaId = null;
        }


        return $this;
    } // setPrasaranaId()

    /**
     * Set the value of [moving_class] column.
     * 
     * @param string $v new value
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setMovingClass($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->moving_class !== $v) {
            $this->moving_class = $v;
            $this->modifiedColumns[] = RombonganBelajarPeer::MOVING_CLASS;
        }


        return $this;
    } // setMovingClass()

    /**
     * Set the value of [jenis_rombel] column.
     * 
     * @param string $v new value
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setJenisRombel($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->jenis_rombel !== $v) {
            $this->jenis_rombel = $v;
            $this->modifiedColumns[] = RombonganBelajarPeer::JENIS_ROMBEL;
        }


        return $this;
    } // setJenisRombel()

    /**
     * Set the value of [sks] column.
     * 
     * @param string $v new value
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setSks($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->sks !== $v) {
            $this->sks = $v;
            $this->modifiedColumns[] = RombonganBelajarPeer::SKS;
        }


        return $this;
    } // setSks()

    /**
     * Set the value of [kebutuhan_khusus_id] column.
     * 
     * @param int $v new value
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setKebutuhanKhususId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->kebutuhan_khusus_id !== $v) {
            $this->kebutuhan_khusus_id = $v;
            $this->modifiedColumns[] = RombonganBelajarPeer::KEBUTUHAN_KHUSUS_ID;
        }

        if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId !== null && $this->aKebutuhanKhususRelatedByKebutuhanKhususId->getKebutuhanKhususId() !== $v) {
            $this->aKebutuhanKhususRelatedByKebutuhanKhususId = null;
        }

        if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId !== null && $this->aKebutuhanKhususRelatedByKebutuhanKhususId->getKebutuhanKhususId() !== $v) {
            $this->aKebutuhanKhususRelatedByKebutuhanKhususId = null;
        }


        return $this;
    } // setKebutuhanKhususId()

    /**
     * Sets the value of [last_update] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setLastUpdate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_update !== null || $dt !== null) {
            $currentDateAsString = ($this->last_update !== null && $tmpDt = new DateTime($this->last_update)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_update = $newDateAsString;
                $this->modifiedColumns[] = RombonganBelajarPeer::LAST_UPDATE;
            }
        } // if either are not null


        return $this;
    } // setLastUpdate()

    /**
     * Set the value of [soft_delete] column.
     * 
     * @param string $v new value
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setSoftDelete($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->soft_delete !== $v) {
            $this->soft_delete = $v;
            $this->modifiedColumns[] = RombonganBelajarPeer::SOFT_DELETE;
        }


        return $this;
    } // setSoftDelete()

    /**
     * Sets the value of [last_sync] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setLastSync($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_sync !== null || $dt !== null) {
            $currentDateAsString = ($this->last_sync !== null && $tmpDt = new DateTime($this->last_sync)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_sync = $newDateAsString;
                $this->modifiedColumns[] = RombonganBelajarPeer::LAST_SYNC;
            }
        } // if either are not null


        return $this;
    } // setLastSync()

    /**
     * Set the value of [updater_id] column.
     * 
     * @param string $v new value
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setUpdaterId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->updater_id !== $v) {
            $this->updater_id = $v;
            $this->modifiedColumns[] = RombonganBelajarPeer::UPDATER_ID;
        }


        return $this;
    } // setUpdaterId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->jenis_rombel !== '((1))') {
                return false;
            }

            if ($this->sks !== '((0))') {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->rombongan_belajar_id = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
            $this->semester_id = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->sekolah_id = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->tingkat_pendidikan_id = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->jurusan_sp_id = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->kurikulum_id = ($row[$startcol + 5] !== null) ? (int) $row[$startcol + 5] : null;
            $this->nama = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->ptk_id = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->prasarana_id = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->moving_class = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->jenis_rombel = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->sks = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->kebutuhan_khusus_id = ($row[$startcol + 12] !== null) ? (int) $row[$startcol + 12] : null;
            $this->last_update = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->soft_delete = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
            $this->last_sync = ($row[$startcol + 15] !== null) ? (string) $row[$startcol + 15] : null;
            $this->updater_id = ($row[$startcol + 16] !== null) ? (string) $row[$startcol + 16] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 17; // 17 = RombonganBelajarPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating RombonganBelajar object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aSemesterRelatedBySemesterId !== null && $this->semester_id !== $this->aSemesterRelatedBySemesterId->getSemesterId()) {
            $this->aSemesterRelatedBySemesterId = null;
        }
        if ($this->aSemesterRelatedBySemesterId !== null && $this->semester_id !== $this->aSemesterRelatedBySemesterId->getSemesterId()) {
            $this->aSemesterRelatedBySemesterId = null;
        }
        if ($this->aSekolahRelatedBySekolahId !== null && $this->sekolah_id !== $this->aSekolahRelatedBySekolahId->getSekolahId()) {
            $this->aSekolahRelatedBySekolahId = null;
        }
        if ($this->aSekolahRelatedBySekolahId !== null && $this->sekolah_id !== $this->aSekolahRelatedBySekolahId->getSekolahId()) {
            $this->aSekolahRelatedBySekolahId = null;
        }
        if ($this->aTingkatPendidikanRelatedByTingkatPendidikanId !== null && $this->tingkat_pendidikan_id !== $this->aTingkatPendidikanRelatedByTingkatPendidikanId->getTingkatPendidikanId()) {
            $this->aTingkatPendidikanRelatedByTingkatPendidikanId = null;
        }
        if ($this->aTingkatPendidikanRelatedByTingkatPendidikanId !== null && $this->tingkat_pendidikan_id !== $this->aTingkatPendidikanRelatedByTingkatPendidikanId->getTingkatPendidikanId()) {
            $this->aTingkatPendidikanRelatedByTingkatPendidikanId = null;
        }
        if ($this->aJurusanSpRelatedByJurusanSpId !== null && $this->jurusan_sp_id !== $this->aJurusanSpRelatedByJurusanSpId->getJurusanSpId()) {
            $this->aJurusanSpRelatedByJurusanSpId = null;
        }
        if ($this->aJurusanSpRelatedByJurusanSpId !== null && $this->jurusan_sp_id !== $this->aJurusanSpRelatedByJurusanSpId->getJurusanSpId()) {
            $this->aJurusanSpRelatedByJurusanSpId = null;
        }
        if ($this->aKurikulumRelatedByKurikulumId !== null && $this->kurikulum_id !== $this->aKurikulumRelatedByKurikulumId->getKurikulumId()) {
            $this->aKurikulumRelatedByKurikulumId = null;
        }
        if ($this->aKurikulumRelatedByKurikulumId !== null && $this->kurikulum_id !== $this->aKurikulumRelatedByKurikulumId->getKurikulumId()) {
            $this->aKurikulumRelatedByKurikulumId = null;
        }
        if ($this->aPtkRelatedByPtkId !== null && $this->ptk_id !== $this->aPtkRelatedByPtkId->getPtkId()) {
            $this->aPtkRelatedByPtkId = null;
        }
        if ($this->aPtkRelatedByPtkId !== null && $this->ptk_id !== $this->aPtkRelatedByPtkId->getPtkId()) {
            $this->aPtkRelatedByPtkId = null;
        }
        if ($this->aPrasaranaRelatedByPrasaranaId !== null && $this->prasarana_id !== $this->aPrasaranaRelatedByPrasaranaId->getPrasaranaId()) {
            $this->aPrasaranaRelatedByPrasaranaId = null;
        }
        if ($this->aPrasaranaRelatedByPrasaranaId !== null && $this->prasarana_id !== $this->aPrasaranaRelatedByPrasaranaId->getPrasaranaId()) {
            $this->aPrasaranaRelatedByPrasaranaId = null;
        }
        if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId !== null && $this->kebutuhan_khusus_id !== $this->aKebutuhanKhususRelatedByKebutuhanKhususId->getKebutuhanKhususId()) {
            $this->aKebutuhanKhususRelatedByKebutuhanKhususId = null;
        }
        if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId !== null && $this->kebutuhan_khusus_id !== $this->aKebutuhanKhususRelatedByKebutuhanKhususId->getKebutuhanKhususId()) {
            $this->aKebutuhanKhususRelatedByKebutuhanKhususId = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(RombonganBelajarPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = RombonganBelajarPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aJurusanSpRelatedByJurusanSpId = null;
            $this->aJurusanSpRelatedByJurusanSpId = null;
            $this->aPrasaranaRelatedByPrasaranaId = null;
            $this->aPrasaranaRelatedByPrasaranaId = null;
            $this->aPtkRelatedByPtkId = null;
            $this->aPtkRelatedByPtkId = null;
            $this->aSekolahRelatedBySekolahId = null;
            $this->aSekolahRelatedBySekolahId = null;
            $this->aKebutuhanKhususRelatedByKebutuhanKhususId = null;
            $this->aKebutuhanKhususRelatedByKebutuhanKhususId = null;
            $this->aKurikulumRelatedByKurikulumId = null;
            $this->aKurikulumRelatedByKurikulumId = null;
            $this->aSemesterRelatedBySemesterId = null;
            $this->aSemesterRelatedBySemesterId = null;
            $this->aTingkatPendidikanRelatedByTingkatPendidikanId = null;
            $this->aTingkatPendidikanRelatedByTingkatPendidikanId = null;
            $this->collVldRombelsRelatedByRombonganBelajarId = null;

            $this->collVldRombelsRelatedByRombonganBelajarId = null;

            $this->collAnggotaRombelsRelatedByRombonganBelajarId = null;

            $this->collAnggotaRombelsRelatedByRombonganBelajarId = null;

            $this->collPembelajaransRelatedByRombonganBelajarId = null;

            $this->collPembelajaransRelatedByRombonganBelajarId = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(RombonganBelajarPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = RombonganBelajarQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(RombonganBelajarPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                RombonganBelajarPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aJurusanSpRelatedByJurusanSpId !== null) {
                if ($this->aJurusanSpRelatedByJurusanSpId->isModified() || $this->aJurusanSpRelatedByJurusanSpId->isNew()) {
                    $affectedRows += $this->aJurusanSpRelatedByJurusanSpId->save($con);
                }
                $this->setJurusanSpRelatedByJurusanSpId($this->aJurusanSpRelatedByJurusanSpId);
            }

            if ($this->aJurusanSpRelatedByJurusanSpId !== null) {
                if ($this->aJurusanSpRelatedByJurusanSpId->isModified() || $this->aJurusanSpRelatedByJurusanSpId->isNew()) {
                    $affectedRows += $this->aJurusanSpRelatedByJurusanSpId->save($con);
                }
                $this->setJurusanSpRelatedByJurusanSpId($this->aJurusanSpRelatedByJurusanSpId);
            }

            if ($this->aPrasaranaRelatedByPrasaranaId !== null) {
                if ($this->aPrasaranaRelatedByPrasaranaId->isModified() || $this->aPrasaranaRelatedByPrasaranaId->isNew()) {
                    $affectedRows += $this->aPrasaranaRelatedByPrasaranaId->save($con);
                }
                $this->setPrasaranaRelatedByPrasaranaId($this->aPrasaranaRelatedByPrasaranaId);
            }

            if ($this->aPrasaranaRelatedByPrasaranaId !== null) {
                if ($this->aPrasaranaRelatedByPrasaranaId->isModified() || $this->aPrasaranaRelatedByPrasaranaId->isNew()) {
                    $affectedRows += $this->aPrasaranaRelatedByPrasaranaId->save($con);
                }
                $this->setPrasaranaRelatedByPrasaranaId($this->aPrasaranaRelatedByPrasaranaId);
            }

            if ($this->aPtkRelatedByPtkId !== null) {
                if ($this->aPtkRelatedByPtkId->isModified() || $this->aPtkRelatedByPtkId->isNew()) {
                    $affectedRows += $this->aPtkRelatedByPtkId->save($con);
                }
                $this->setPtkRelatedByPtkId($this->aPtkRelatedByPtkId);
            }

            if ($this->aPtkRelatedByPtkId !== null) {
                if ($this->aPtkRelatedByPtkId->isModified() || $this->aPtkRelatedByPtkId->isNew()) {
                    $affectedRows += $this->aPtkRelatedByPtkId->save($con);
                }
                $this->setPtkRelatedByPtkId($this->aPtkRelatedByPtkId);
            }

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if ($this->aSekolahRelatedBySekolahId->isModified() || $this->aSekolahRelatedBySekolahId->isNew()) {
                    $affectedRows += $this->aSekolahRelatedBySekolahId->save($con);
                }
                $this->setSekolahRelatedBySekolahId($this->aSekolahRelatedBySekolahId);
            }

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if ($this->aSekolahRelatedBySekolahId->isModified() || $this->aSekolahRelatedBySekolahId->isNew()) {
                    $affectedRows += $this->aSekolahRelatedBySekolahId->save($con);
                }
                $this->setSekolahRelatedBySekolahId($this->aSekolahRelatedBySekolahId);
            }

            if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId !== null) {
                if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId->isModified() || $this->aKebutuhanKhususRelatedByKebutuhanKhususId->isNew()) {
                    $affectedRows += $this->aKebutuhanKhususRelatedByKebutuhanKhususId->save($con);
                }
                $this->setKebutuhanKhususRelatedByKebutuhanKhususId($this->aKebutuhanKhususRelatedByKebutuhanKhususId);
            }

            if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId !== null) {
                if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId->isModified() || $this->aKebutuhanKhususRelatedByKebutuhanKhususId->isNew()) {
                    $affectedRows += $this->aKebutuhanKhususRelatedByKebutuhanKhususId->save($con);
                }
                $this->setKebutuhanKhususRelatedByKebutuhanKhususId($this->aKebutuhanKhususRelatedByKebutuhanKhususId);
            }

            if ($this->aKurikulumRelatedByKurikulumId !== null) {
                if ($this->aKurikulumRelatedByKurikulumId->isModified() || $this->aKurikulumRelatedByKurikulumId->isNew()) {
                    $affectedRows += $this->aKurikulumRelatedByKurikulumId->save($con);
                }
                $this->setKurikulumRelatedByKurikulumId($this->aKurikulumRelatedByKurikulumId);
            }

            if ($this->aKurikulumRelatedByKurikulumId !== null) {
                if ($this->aKurikulumRelatedByKurikulumId->isModified() || $this->aKurikulumRelatedByKurikulumId->isNew()) {
                    $affectedRows += $this->aKurikulumRelatedByKurikulumId->save($con);
                }
                $this->setKurikulumRelatedByKurikulumId($this->aKurikulumRelatedByKurikulumId);
            }

            if ($this->aSemesterRelatedBySemesterId !== null) {
                if ($this->aSemesterRelatedBySemesterId->isModified() || $this->aSemesterRelatedBySemesterId->isNew()) {
                    $affectedRows += $this->aSemesterRelatedBySemesterId->save($con);
                }
                $this->setSemesterRelatedBySemesterId($this->aSemesterRelatedBySemesterId);
            }

            if ($this->aSemesterRelatedBySemesterId !== null) {
                if ($this->aSemesterRelatedBySemesterId->isModified() || $this->aSemesterRelatedBySemesterId->isNew()) {
                    $affectedRows += $this->aSemesterRelatedBySemesterId->save($con);
                }
                $this->setSemesterRelatedBySemesterId($this->aSemesterRelatedBySemesterId);
            }

            if ($this->aTingkatPendidikanRelatedByTingkatPendidikanId !== null) {
                if ($this->aTingkatPendidikanRelatedByTingkatPendidikanId->isModified() || $this->aTingkatPendidikanRelatedByTingkatPendidikanId->isNew()) {
                    $affectedRows += $this->aTingkatPendidikanRelatedByTingkatPendidikanId->save($con);
                }
                $this->setTingkatPendidikanRelatedByTingkatPendidikanId($this->aTingkatPendidikanRelatedByTingkatPendidikanId);
            }

            if ($this->aTingkatPendidikanRelatedByTingkatPendidikanId !== null) {
                if ($this->aTingkatPendidikanRelatedByTingkatPendidikanId->isModified() || $this->aTingkatPendidikanRelatedByTingkatPendidikanId->isNew()) {
                    $affectedRows += $this->aTingkatPendidikanRelatedByTingkatPendidikanId->save($con);
                }
                $this->setTingkatPendidikanRelatedByTingkatPendidikanId($this->aTingkatPendidikanRelatedByTingkatPendidikanId);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->vldRombelsRelatedByRombonganBelajarIdScheduledForDeletion !== null) {
                if (!$this->vldRombelsRelatedByRombonganBelajarIdScheduledForDeletion->isEmpty()) {
                    VldRombelQuery::create()
                        ->filterByPrimaryKeys($this->vldRombelsRelatedByRombonganBelajarIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldRombelsRelatedByRombonganBelajarIdScheduledForDeletion = null;
                }
            }

            if ($this->collVldRombelsRelatedByRombonganBelajarId !== null) {
                foreach ($this->collVldRombelsRelatedByRombonganBelajarId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldRombelsRelatedByRombonganBelajarIdScheduledForDeletion !== null) {
                if (!$this->vldRombelsRelatedByRombonganBelajarIdScheduledForDeletion->isEmpty()) {
                    VldRombelQuery::create()
                        ->filterByPrimaryKeys($this->vldRombelsRelatedByRombonganBelajarIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldRombelsRelatedByRombonganBelajarIdScheduledForDeletion = null;
                }
            }

            if ($this->collVldRombelsRelatedByRombonganBelajarId !== null) {
                foreach ($this->collVldRombelsRelatedByRombonganBelajarId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->anggotaRombelsRelatedByRombonganBelajarIdScheduledForDeletion !== null) {
                if (!$this->anggotaRombelsRelatedByRombonganBelajarIdScheduledForDeletion->isEmpty()) {
                    AnggotaRombelQuery::create()
                        ->filterByPrimaryKeys($this->anggotaRombelsRelatedByRombonganBelajarIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->anggotaRombelsRelatedByRombonganBelajarIdScheduledForDeletion = null;
                }
            }

            if ($this->collAnggotaRombelsRelatedByRombonganBelajarId !== null) {
                foreach ($this->collAnggotaRombelsRelatedByRombonganBelajarId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->anggotaRombelsRelatedByRombonganBelajarIdScheduledForDeletion !== null) {
                if (!$this->anggotaRombelsRelatedByRombonganBelajarIdScheduledForDeletion->isEmpty()) {
                    AnggotaRombelQuery::create()
                        ->filterByPrimaryKeys($this->anggotaRombelsRelatedByRombonganBelajarIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->anggotaRombelsRelatedByRombonganBelajarIdScheduledForDeletion = null;
                }
            }

            if ($this->collAnggotaRombelsRelatedByRombonganBelajarId !== null) {
                foreach ($this->collAnggotaRombelsRelatedByRombonganBelajarId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->pembelajaransRelatedByRombonganBelajarIdScheduledForDeletion !== null) {
                if (!$this->pembelajaransRelatedByRombonganBelajarIdScheduledForDeletion->isEmpty()) {
                    PembelajaranQuery::create()
                        ->filterByPrimaryKeys($this->pembelajaransRelatedByRombonganBelajarIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->pembelajaransRelatedByRombonganBelajarIdScheduledForDeletion = null;
                }
            }

            if ($this->collPembelajaransRelatedByRombonganBelajarId !== null) {
                foreach ($this->collPembelajaransRelatedByRombonganBelajarId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->pembelajaransRelatedByRombonganBelajarIdScheduledForDeletion !== null) {
                if (!$this->pembelajaransRelatedByRombonganBelajarIdScheduledForDeletion->isEmpty()) {
                    PembelajaranQuery::create()
                        ->filterByPrimaryKeys($this->pembelajaransRelatedByRombonganBelajarIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->pembelajaransRelatedByRombonganBelajarIdScheduledForDeletion = null;
                }
            }

            if ($this->collPembelajaransRelatedByRombonganBelajarId !== null) {
                foreach ($this->collPembelajaransRelatedByRombonganBelajarId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $criteria = $this->buildCriteria();
        $pk = BasePeer::doInsert($criteria, $con);
        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aJurusanSpRelatedByJurusanSpId !== null) {
                if (!$this->aJurusanSpRelatedByJurusanSpId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aJurusanSpRelatedByJurusanSpId->getValidationFailures());
                }
            }

            if ($this->aJurusanSpRelatedByJurusanSpId !== null) {
                if (!$this->aJurusanSpRelatedByJurusanSpId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aJurusanSpRelatedByJurusanSpId->getValidationFailures());
                }
            }

            if ($this->aPrasaranaRelatedByPrasaranaId !== null) {
                if (!$this->aPrasaranaRelatedByPrasaranaId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPrasaranaRelatedByPrasaranaId->getValidationFailures());
                }
            }

            if ($this->aPrasaranaRelatedByPrasaranaId !== null) {
                if (!$this->aPrasaranaRelatedByPrasaranaId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPrasaranaRelatedByPrasaranaId->getValidationFailures());
                }
            }

            if ($this->aPtkRelatedByPtkId !== null) {
                if (!$this->aPtkRelatedByPtkId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPtkRelatedByPtkId->getValidationFailures());
                }
            }

            if ($this->aPtkRelatedByPtkId !== null) {
                if (!$this->aPtkRelatedByPtkId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPtkRelatedByPtkId->getValidationFailures());
                }
            }

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if (!$this->aSekolahRelatedBySekolahId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSekolahRelatedBySekolahId->getValidationFailures());
                }
            }

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if (!$this->aSekolahRelatedBySekolahId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSekolahRelatedBySekolahId->getValidationFailures());
                }
            }

            if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId !== null) {
                if (!$this->aKebutuhanKhususRelatedByKebutuhanKhususId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aKebutuhanKhususRelatedByKebutuhanKhususId->getValidationFailures());
                }
            }

            if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId !== null) {
                if (!$this->aKebutuhanKhususRelatedByKebutuhanKhususId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aKebutuhanKhususRelatedByKebutuhanKhususId->getValidationFailures());
                }
            }

            if ($this->aKurikulumRelatedByKurikulumId !== null) {
                if (!$this->aKurikulumRelatedByKurikulumId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aKurikulumRelatedByKurikulumId->getValidationFailures());
                }
            }

            if ($this->aKurikulumRelatedByKurikulumId !== null) {
                if (!$this->aKurikulumRelatedByKurikulumId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aKurikulumRelatedByKurikulumId->getValidationFailures());
                }
            }

            if ($this->aSemesterRelatedBySemesterId !== null) {
                if (!$this->aSemesterRelatedBySemesterId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSemesterRelatedBySemesterId->getValidationFailures());
                }
            }

            if ($this->aSemesterRelatedBySemesterId !== null) {
                if (!$this->aSemesterRelatedBySemesterId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSemesterRelatedBySemesterId->getValidationFailures());
                }
            }

            if ($this->aTingkatPendidikanRelatedByTingkatPendidikanId !== null) {
                if (!$this->aTingkatPendidikanRelatedByTingkatPendidikanId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aTingkatPendidikanRelatedByTingkatPendidikanId->getValidationFailures());
                }
            }

            if ($this->aTingkatPendidikanRelatedByTingkatPendidikanId !== null) {
                if (!$this->aTingkatPendidikanRelatedByTingkatPendidikanId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aTingkatPendidikanRelatedByTingkatPendidikanId->getValidationFailures());
                }
            }


            if (($retval = RombonganBelajarPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collVldRombelsRelatedByRombonganBelajarId !== null) {
                    foreach ($this->collVldRombelsRelatedByRombonganBelajarId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldRombelsRelatedByRombonganBelajarId !== null) {
                    foreach ($this->collVldRombelsRelatedByRombonganBelajarId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collAnggotaRombelsRelatedByRombonganBelajarId !== null) {
                    foreach ($this->collAnggotaRombelsRelatedByRombonganBelajarId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collAnggotaRombelsRelatedByRombonganBelajarId !== null) {
                    foreach ($this->collAnggotaRombelsRelatedByRombonganBelajarId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPembelajaransRelatedByRombonganBelajarId !== null) {
                    foreach ($this->collPembelajaransRelatedByRombonganBelajarId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPembelajaransRelatedByRombonganBelajarId !== null) {
                    foreach ($this->collPembelajaransRelatedByRombonganBelajarId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = RombonganBelajarPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getRombonganBelajarId();
                break;
            case 1:
                return $this->getSemesterId();
                break;
            case 2:
                return $this->getSekolahId();
                break;
            case 3:
                return $this->getTingkatPendidikanId();
                break;
            case 4:
                return $this->getJurusanSpId();
                break;
            case 5:
                return $this->getKurikulumId();
                break;
            case 6:
                return $this->getNama();
                break;
            case 7:
                return $this->getPtkId();
                break;
            case 8:
                return $this->getPrasaranaId();
                break;
            case 9:
                return $this->getMovingClass();
                break;
            case 10:
                return $this->getJenisRombel();
                break;
            case 11:
                return $this->getSks();
                break;
            case 12:
                return $this->getKebutuhanKhususId();
                break;
            case 13:
                return $this->getLastUpdate();
                break;
            case 14:
                return $this->getSoftDelete();
                break;
            case 15:
                return $this->getLastSync();
                break;
            case 16:
                return $this->getUpdaterId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['RombonganBelajar'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['RombonganBelajar'][$this->getPrimaryKey()] = true;
        $keys = RombonganBelajarPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getRombonganBelajarId(),
            $keys[1] => $this->getSemesterId(),
            $keys[2] => $this->getSekolahId(),
            $keys[3] => $this->getTingkatPendidikanId(),
            $keys[4] => $this->getJurusanSpId(),
            $keys[5] => $this->getKurikulumId(),
            $keys[6] => $this->getNama(),
            $keys[7] => $this->getPtkId(),
            $keys[8] => $this->getPrasaranaId(),
            $keys[9] => $this->getMovingClass(),
            $keys[10] => $this->getJenisRombel(),
            $keys[11] => $this->getSks(),
            $keys[12] => $this->getKebutuhanKhususId(),
            $keys[13] => $this->getLastUpdate(),
            $keys[14] => $this->getSoftDelete(),
            $keys[15] => $this->getLastSync(),
            $keys[16] => $this->getUpdaterId(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aJurusanSpRelatedByJurusanSpId) {
                $result['JurusanSpRelatedByJurusanSpId'] = $this->aJurusanSpRelatedByJurusanSpId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aJurusanSpRelatedByJurusanSpId) {
                $result['JurusanSpRelatedByJurusanSpId'] = $this->aJurusanSpRelatedByJurusanSpId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPrasaranaRelatedByPrasaranaId) {
                $result['PrasaranaRelatedByPrasaranaId'] = $this->aPrasaranaRelatedByPrasaranaId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPrasaranaRelatedByPrasaranaId) {
                $result['PrasaranaRelatedByPrasaranaId'] = $this->aPrasaranaRelatedByPrasaranaId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPtkRelatedByPtkId) {
                $result['PtkRelatedByPtkId'] = $this->aPtkRelatedByPtkId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPtkRelatedByPtkId) {
                $result['PtkRelatedByPtkId'] = $this->aPtkRelatedByPtkId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSekolahRelatedBySekolahId) {
                $result['SekolahRelatedBySekolahId'] = $this->aSekolahRelatedBySekolahId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSekolahRelatedBySekolahId) {
                $result['SekolahRelatedBySekolahId'] = $this->aSekolahRelatedBySekolahId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aKebutuhanKhususRelatedByKebutuhanKhususId) {
                $result['KebutuhanKhususRelatedByKebutuhanKhususId'] = $this->aKebutuhanKhususRelatedByKebutuhanKhususId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aKebutuhanKhususRelatedByKebutuhanKhususId) {
                $result['KebutuhanKhususRelatedByKebutuhanKhususId'] = $this->aKebutuhanKhususRelatedByKebutuhanKhususId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aKurikulumRelatedByKurikulumId) {
                $result['KurikulumRelatedByKurikulumId'] = $this->aKurikulumRelatedByKurikulumId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aKurikulumRelatedByKurikulumId) {
                $result['KurikulumRelatedByKurikulumId'] = $this->aKurikulumRelatedByKurikulumId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSemesterRelatedBySemesterId) {
                $result['SemesterRelatedBySemesterId'] = $this->aSemesterRelatedBySemesterId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSemesterRelatedBySemesterId) {
                $result['SemesterRelatedBySemesterId'] = $this->aSemesterRelatedBySemesterId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aTingkatPendidikanRelatedByTingkatPendidikanId) {
                $result['TingkatPendidikanRelatedByTingkatPendidikanId'] = $this->aTingkatPendidikanRelatedByTingkatPendidikanId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aTingkatPendidikanRelatedByTingkatPendidikanId) {
                $result['TingkatPendidikanRelatedByTingkatPendidikanId'] = $this->aTingkatPendidikanRelatedByTingkatPendidikanId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collVldRombelsRelatedByRombonganBelajarId) {
                $result['VldRombelsRelatedByRombonganBelajarId'] = $this->collVldRombelsRelatedByRombonganBelajarId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldRombelsRelatedByRombonganBelajarId) {
                $result['VldRombelsRelatedByRombonganBelajarId'] = $this->collVldRombelsRelatedByRombonganBelajarId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collAnggotaRombelsRelatedByRombonganBelajarId) {
                $result['AnggotaRombelsRelatedByRombonganBelajarId'] = $this->collAnggotaRombelsRelatedByRombonganBelajarId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collAnggotaRombelsRelatedByRombonganBelajarId) {
                $result['AnggotaRombelsRelatedByRombonganBelajarId'] = $this->collAnggotaRombelsRelatedByRombonganBelajarId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPembelajaransRelatedByRombonganBelajarId) {
                $result['PembelajaransRelatedByRombonganBelajarId'] = $this->collPembelajaransRelatedByRombonganBelajarId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPembelajaransRelatedByRombonganBelajarId) {
                $result['PembelajaransRelatedByRombonganBelajarId'] = $this->collPembelajaransRelatedByRombonganBelajarId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = RombonganBelajarPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setRombonganBelajarId($value);
                break;
            case 1:
                $this->setSemesterId($value);
                break;
            case 2:
                $this->setSekolahId($value);
                break;
            case 3:
                $this->setTingkatPendidikanId($value);
                break;
            case 4:
                $this->setJurusanSpId($value);
                break;
            case 5:
                $this->setKurikulumId($value);
                break;
            case 6:
                $this->setNama($value);
                break;
            case 7:
                $this->setPtkId($value);
                break;
            case 8:
                $this->setPrasaranaId($value);
                break;
            case 9:
                $this->setMovingClass($value);
                break;
            case 10:
                $this->setJenisRombel($value);
                break;
            case 11:
                $this->setSks($value);
                break;
            case 12:
                $this->setKebutuhanKhususId($value);
                break;
            case 13:
                $this->setLastUpdate($value);
                break;
            case 14:
                $this->setSoftDelete($value);
                break;
            case 15:
                $this->setLastSync($value);
                break;
            case 16:
                $this->setUpdaterId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = RombonganBelajarPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setRombonganBelajarId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setSemesterId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setSekolahId($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setTingkatPendidikanId($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setJurusanSpId($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setKurikulumId($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setNama($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setPtkId($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setPrasaranaId($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setMovingClass($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setJenisRombel($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setSks($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setKebutuhanKhususId($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setLastUpdate($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setSoftDelete($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setLastSync($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setUpdaterId($arr[$keys[16]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(RombonganBelajarPeer::DATABASE_NAME);

        if ($this->isColumnModified(RombonganBelajarPeer::ROMBONGAN_BELAJAR_ID)) $criteria->add(RombonganBelajarPeer::ROMBONGAN_BELAJAR_ID, $this->rombongan_belajar_id);
        if ($this->isColumnModified(RombonganBelajarPeer::SEMESTER_ID)) $criteria->add(RombonganBelajarPeer::SEMESTER_ID, $this->semester_id);
        if ($this->isColumnModified(RombonganBelajarPeer::SEKOLAH_ID)) $criteria->add(RombonganBelajarPeer::SEKOLAH_ID, $this->sekolah_id);
        if ($this->isColumnModified(RombonganBelajarPeer::TINGKAT_PENDIDIKAN_ID)) $criteria->add(RombonganBelajarPeer::TINGKAT_PENDIDIKAN_ID, $this->tingkat_pendidikan_id);
        if ($this->isColumnModified(RombonganBelajarPeer::JURUSAN_SP_ID)) $criteria->add(RombonganBelajarPeer::JURUSAN_SP_ID, $this->jurusan_sp_id);
        if ($this->isColumnModified(RombonganBelajarPeer::KURIKULUM_ID)) $criteria->add(RombonganBelajarPeer::KURIKULUM_ID, $this->kurikulum_id);
        if ($this->isColumnModified(RombonganBelajarPeer::NAMA)) $criteria->add(RombonganBelajarPeer::NAMA, $this->nama);
        if ($this->isColumnModified(RombonganBelajarPeer::PTK_ID)) $criteria->add(RombonganBelajarPeer::PTK_ID, $this->ptk_id);
        if ($this->isColumnModified(RombonganBelajarPeer::PRASARANA_ID)) $criteria->add(RombonganBelajarPeer::PRASARANA_ID, $this->prasarana_id);
        if ($this->isColumnModified(RombonganBelajarPeer::MOVING_CLASS)) $criteria->add(RombonganBelajarPeer::MOVING_CLASS, $this->moving_class);
        if ($this->isColumnModified(RombonganBelajarPeer::JENIS_ROMBEL)) $criteria->add(RombonganBelajarPeer::JENIS_ROMBEL, $this->jenis_rombel);
        if ($this->isColumnModified(RombonganBelajarPeer::SKS)) $criteria->add(RombonganBelajarPeer::SKS, $this->sks);
        if ($this->isColumnModified(RombonganBelajarPeer::KEBUTUHAN_KHUSUS_ID)) $criteria->add(RombonganBelajarPeer::KEBUTUHAN_KHUSUS_ID, $this->kebutuhan_khusus_id);
        if ($this->isColumnModified(RombonganBelajarPeer::LAST_UPDATE)) $criteria->add(RombonganBelajarPeer::LAST_UPDATE, $this->last_update);
        if ($this->isColumnModified(RombonganBelajarPeer::SOFT_DELETE)) $criteria->add(RombonganBelajarPeer::SOFT_DELETE, $this->soft_delete);
        if ($this->isColumnModified(RombonganBelajarPeer::LAST_SYNC)) $criteria->add(RombonganBelajarPeer::LAST_SYNC, $this->last_sync);
        if ($this->isColumnModified(RombonganBelajarPeer::UPDATER_ID)) $criteria->add(RombonganBelajarPeer::UPDATER_ID, $this->updater_id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(RombonganBelajarPeer::DATABASE_NAME);
        $criteria->add(RombonganBelajarPeer::ROMBONGAN_BELAJAR_ID, $this->rombongan_belajar_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getRombonganBelajarId();
    }

    /**
     * Generic method to set the primary key (rombongan_belajar_id column).
     *
     * @param  string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setRombonganBelajarId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getRombonganBelajarId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of RombonganBelajar (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setSemesterId($this->getSemesterId());
        $copyObj->setSekolahId($this->getSekolahId());
        $copyObj->setTingkatPendidikanId($this->getTingkatPendidikanId());
        $copyObj->setJurusanSpId($this->getJurusanSpId());
        $copyObj->setKurikulumId($this->getKurikulumId());
        $copyObj->setNama($this->getNama());
        $copyObj->setPtkId($this->getPtkId());
        $copyObj->setPrasaranaId($this->getPrasaranaId());
        $copyObj->setMovingClass($this->getMovingClass());
        $copyObj->setJenisRombel($this->getJenisRombel());
        $copyObj->setSks($this->getSks());
        $copyObj->setKebutuhanKhususId($this->getKebutuhanKhususId());
        $copyObj->setLastUpdate($this->getLastUpdate());
        $copyObj->setSoftDelete($this->getSoftDelete());
        $copyObj->setLastSync($this->getLastSync());
        $copyObj->setUpdaterId($this->getUpdaterId());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getVldRombelsRelatedByRombonganBelajarId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldRombelRelatedByRombonganBelajarId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldRombelsRelatedByRombonganBelajarId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldRombelRelatedByRombonganBelajarId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getAnggotaRombelsRelatedByRombonganBelajarId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addAnggotaRombelRelatedByRombonganBelajarId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getAnggotaRombelsRelatedByRombonganBelajarId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addAnggotaRombelRelatedByRombonganBelajarId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPembelajaransRelatedByRombonganBelajarId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPembelajaranRelatedByRombonganBelajarId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPembelajaransRelatedByRombonganBelajarId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPembelajaranRelatedByRombonganBelajarId($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setRombonganBelajarId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return RombonganBelajar Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return RombonganBelajarPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new RombonganBelajarPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a JurusanSp object.
     *
     * @param             JurusanSp $v
     * @return RombonganBelajar The current object (for fluent API support)
     * @throws PropelException
     */
    public function setJurusanSpRelatedByJurusanSpId(JurusanSp $v = null)
    {
        if ($v === null) {
            $this->setJurusanSpId(NULL);
        } else {
            $this->setJurusanSpId($v->getJurusanSpId());
        }

        $this->aJurusanSpRelatedByJurusanSpId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the JurusanSp object, it will not be re-added.
        if ($v !== null) {
            $v->addRombonganBelajarRelatedByJurusanSpId($this);
        }


        return $this;
    }


    /**
     * Get the associated JurusanSp object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return JurusanSp The associated JurusanSp object.
     * @throws PropelException
     */
    public function getJurusanSpRelatedByJurusanSpId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aJurusanSpRelatedByJurusanSpId === null && (($this->jurusan_sp_id !== "" && $this->jurusan_sp_id !== null)) && $doQuery) {
            $this->aJurusanSpRelatedByJurusanSpId = JurusanSpQuery::create()->findPk($this->jurusan_sp_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aJurusanSpRelatedByJurusanSpId->addRombonganBelajarsRelatedByJurusanSpId($this);
             */
        }

        return $this->aJurusanSpRelatedByJurusanSpId;
    }

    /**
     * Declares an association between this object and a JurusanSp object.
     *
     * @param             JurusanSp $v
     * @return RombonganBelajar The current object (for fluent API support)
     * @throws PropelException
     */
    public function setJurusanSpRelatedByJurusanSpId(JurusanSp $v = null)
    {
        if ($v === null) {
            $this->setJurusanSpId(NULL);
        } else {
            $this->setJurusanSpId($v->getJurusanSpId());
        }

        $this->aJurusanSpRelatedByJurusanSpId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the JurusanSp object, it will not be re-added.
        if ($v !== null) {
            $v->addRombonganBelajarRelatedByJurusanSpId($this);
        }


        return $this;
    }


    /**
     * Get the associated JurusanSp object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return JurusanSp The associated JurusanSp object.
     * @throws PropelException
     */
    public function getJurusanSpRelatedByJurusanSpId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aJurusanSpRelatedByJurusanSpId === null && (($this->jurusan_sp_id !== "" && $this->jurusan_sp_id !== null)) && $doQuery) {
            $this->aJurusanSpRelatedByJurusanSpId = JurusanSpQuery::create()->findPk($this->jurusan_sp_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aJurusanSpRelatedByJurusanSpId->addRombonganBelajarsRelatedByJurusanSpId($this);
             */
        }

        return $this->aJurusanSpRelatedByJurusanSpId;
    }

    /**
     * Declares an association between this object and a Prasarana object.
     *
     * @param             Prasarana $v
     * @return RombonganBelajar The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPrasaranaRelatedByPrasaranaId(Prasarana $v = null)
    {
        if ($v === null) {
            $this->setPrasaranaId(NULL);
        } else {
            $this->setPrasaranaId($v->getPrasaranaId());
        }

        $this->aPrasaranaRelatedByPrasaranaId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Prasarana object, it will not be re-added.
        if ($v !== null) {
            $v->addRombonganBelajarRelatedByPrasaranaId($this);
        }


        return $this;
    }


    /**
     * Get the associated Prasarana object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Prasarana The associated Prasarana object.
     * @throws PropelException
     */
    public function getPrasaranaRelatedByPrasaranaId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPrasaranaRelatedByPrasaranaId === null && (($this->prasarana_id !== "" && $this->prasarana_id !== null)) && $doQuery) {
            $this->aPrasaranaRelatedByPrasaranaId = PrasaranaQuery::create()->findPk($this->prasarana_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPrasaranaRelatedByPrasaranaId->addRombonganBelajarsRelatedByPrasaranaId($this);
             */
        }

        return $this->aPrasaranaRelatedByPrasaranaId;
    }

    /**
     * Declares an association between this object and a Prasarana object.
     *
     * @param             Prasarana $v
     * @return RombonganBelajar The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPrasaranaRelatedByPrasaranaId(Prasarana $v = null)
    {
        if ($v === null) {
            $this->setPrasaranaId(NULL);
        } else {
            $this->setPrasaranaId($v->getPrasaranaId());
        }

        $this->aPrasaranaRelatedByPrasaranaId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Prasarana object, it will not be re-added.
        if ($v !== null) {
            $v->addRombonganBelajarRelatedByPrasaranaId($this);
        }


        return $this;
    }


    /**
     * Get the associated Prasarana object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Prasarana The associated Prasarana object.
     * @throws PropelException
     */
    public function getPrasaranaRelatedByPrasaranaId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPrasaranaRelatedByPrasaranaId === null && (($this->prasarana_id !== "" && $this->prasarana_id !== null)) && $doQuery) {
            $this->aPrasaranaRelatedByPrasaranaId = PrasaranaQuery::create()->findPk($this->prasarana_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPrasaranaRelatedByPrasaranaId->addRombonganBelajarsRelatedByPrasaranaId($this);
             */
        }

        return $this->aPrasaranaRelatedByPrasaranaId;
    }

    /**
     * Declares an association between this object and a Ptk object.
     *
     * @param             Ptk $v
     * @return RombonganBelajar The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPtkRelatedByPtkId(Ptk $v = null)
    {
        if ($v === null) {
            $this->setPtkId(NULL);
        } else {
            $this->setPtkId($v->getPtkId());
        }

        $this->aPtkRelatedByPtkId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Ptk object, it will not be re-added.
        if ($v !== null) {
            $v->addRombonganBelajarRelatedByPtkId($this);
        }


        return $this;
    }


    /**
     * Get the associated Ptk object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Ptk The associated Ptk object.
     * @throws PropelException
     */
    public function getPtkRelatedByPtkId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPtkRelatedByPtkId === null && (($this->ptk_id !== "" && $this->ptk_id !== null)) && $doQuery) {
            $this->aPtkRelatedByPtkId = PtkQuery::create()->findPk($this->ptk_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPtkRelatedByPtkId->addRombonganBelajarsRelatedByPtkId($this);
             */
        }

        return $this->aPtkRelatedByPtkId;
    }

    /**
     * Declares an association between this object and a Ptk object.
     *
     * @param             Ptk $v
     * @return RombonganBelajar The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPtkRelatedByPtkId(Ptk $v = null)
    {
        if ($v === null) {
            $this->setPtkId(NULL);
        } else {
            $this->setPtkId($v->getPtkId());
        }

        $this->aPtkRelatedByPtkId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Ptk object, it will not be re-added.
        if ($v !== null) {
            $v->addRombonganBelajarRelatedByPtkId($this);
        }


        return $this;
    }


    /**
     * Get the associated Ptk object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Ptk The associated Ptk object.
     * @throws PropelException
     */
    public function getPtkRelatedByPtkId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPtkRelatedByPtkId === null && (($this->ptk_id !== "" && $this->ptk_id !== null)) && $doQuery) {
            $this->aPtkRelatedByPtkId = PtkQuery::create()->findPk($this->ptk_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPtkRelatedByPtkId->addRombonganBelajarsRelatedByPtkId($this);
             */
        }

        return $this->aPtkRelatedByPtkId;
    }

    /**
     * Declares an association between this object and a Sekolah object.
     *
     * @param             Sekolah $v
     * @return RombonganBelajar The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSekolahRelatedBySekolahId(Sekolah $v = null)
    {
        if ($v === null) {
            $this->setSekolahId(NULL);
        } else {
            $this->setSekolahId($v->getSekolahId());
        }

        $this->aSekolahRelatedBySekolahId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Sekolah object, it will not be re-added.
        if ($v !== null) {
            $v->addRombonganBelajarRelatedBySekolahId($this);
        }


        return $this;
    }


    /**
     * Get the associated Sekolah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Sekolah The associated Sekolah object.
     * @throws PropelException
     */
    public function getSekolahRelatedBySekolahId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSekolahRelatedBySekolahId === null && (($this->sekolah_id !== "" && $this->sekolah_id !== null)) && $doQuery) {
            $this->aSekolahRelatedBySekolahId = SekolahQuery::create()->findPk($this->sekolah_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSekolahRelatedBySekolahId->addRombonganBelajarsRelatedBySekolahId($this);
             */
        }

        return $this->aSekolahRelatedBySekolahId;
    }

    /**
     * Declares an association between this object and a Sekolah object.
     *
     * @param             Sekolah $v
     * @return RombonganBelajar The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSekolahRelatedBySekolahId(Sekolah $v = null)
    {
        if ($v === null) {
            $this->setSekolahId(NULL);
        } else {
            $this->setSekolahId($v->getSekolahId());
        }

        $this->aSekolahRelatedBySekolahId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Sekolah object, it will not be re-added.
        if ($v !== null) {
            $v->addRombonganBelajarRelatedBySekolahId($this);
        }


        return $this;
    }


    /**
     * Get the associated Sekolah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Sekolah The associated Sekolah object.
     * @throws PropelException
     */
    public function getSekolahRelatedBySekolahId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSekolahRelatedBySekolahId === null && (($this->sekolah_id !== "" && $this->sekolah_id !== null)) && $doQuery) {
            $this->aSekolahRelatedBySekolahId = SekolahQuery::create()->findPk($this->sekolah_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSekolahRelatedBySekolahId->addRombonganBelajarsRelatedBySekolahId($this);
             */
        }

        return $this->aSekolahRelatedBySekolahId;
    }

    /**
     * Declares an association between this object and a KebutuhanKhusus object.
     *
     * @param             KebutuhanKhusus $v
     * @return RombonganBelajar The current object (for fluent API support)
     * @throws PropelException
     */
    public function setKebutuhanKhususRelatedByKebutuhanKhususId(KebutuhanKhusus $v = null)
    {
        if ($v === null) {
            $this->setKebutuhanKhususId(NULL);
        } else {
            $this->setKebutuhanKhususId($v->getKebutuhanKhususId());
        }

        $this->aKebutuhanKhususRelatedByKebutuhanKhususId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the KebutuhanKhusus object, it will not be re-added.
        if ($v !== null) {
            $v->addRombonganBelajarRelatedByKebutuhanKhususId($this);
        }


        return $this;
    }


    /**
     * Get the associated KebutuhanKhusus object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return KebutuhanKhusus The associated KebutuhanKhusus object.
     * @throws PropelException
     */
    public function getKebutuhanKhususRelatedByKebutuhanKhususId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId === null && ($this->kebutuhan_khusus_id !== null) && $doQuery) {
            $this->aKebutuhanKhususRelatedByKebutuhanKhususId = KebutuhanKhususQuery::create()->findPk($this->kebutuhan_khusus_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aKebutuhanKhususRelatedByKebutuhanKhususId->addRombonganBelajarsRelatedByKebutuhanKhususId($this);
             */
        }

        return $this->aKebutuhanKhususRelatedByKebutuhanKhususId;
    }

    /**
     * Declares an association between this object and a KebutuhanKhusus object.
     *
     * @param             KebutuhanKhusus $v
     * @return RombonganBelajar The current object (for fluent API support)
     * @throws PropelException
     */
    public function setKebutuhanKhususRelatedByKebutuhanKhususId(KebutuhanKhusus $v = null)
    {
        if ($v === null) {
            $this->setKebutuhanKhususId(NULL);
        } else {
            $this->setKebutuhanKhususId($v->getKebutuhanKhususId());
        }

        $this->aKebutuhanKhususRelatedByKebutuhanKhususId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the KebutuhanKhusus object, it will not be re-added.
        if ($v !== null) {
            $v->addRombonganBelajarRelatedByKebutuhanKhususId($this);
        }


        return $this;
    }


    /**
     * Get the associated KebutuhanKhusus object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return KebutuhanKhusus The associated KebutuhanKhusus object.
     * @throws PropelException
     */
    public function getKebutuhanKhususRelatedByKebutuhanKhususId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId === null && ($this->kebutuhan_khusus_id !== null) && $doQuery) {
            $this->aKebutuhanKhususRelatedByKebutuhanKhususId = KebutuhanKhususQuery::create()->findPk($this->kebutuhan_khusus_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aKebutuhanKhususRelatedByKebutuhanKhususId->addRombonganBelajarsRelatedByKebutuhanKhususId($this);
             */
        }

        return $this->aKebutuhanKhususRelatedByKebutuhanKhususId;
    }

    /**
     * Declares an association between this object and a Kurikulum object.
     *
     * @param             Kurikulum $v
     * @return RombonganBelajar The current object (for fluent API support)
     * @throws PropelException
     */
    public function setKurikulumRelatedByKurikulumId(Kurikulum $v = null)
    {
        if ($v === null) {
            $this->setKurikulumId(NULL);
        } else {
            $this->setKurikulumId($v->getKurikulumId());
        }

        $this->aKurikulumRelatedByKurikulumId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Kurikulum object, it will not be re-added.
        if ($v !== null) {
            $v->addRombonganBelajarRelatedByKurikulumId($this);
        }


        return $this;
    }


    /**
     * Get the associated Kurikulum object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Kurikulum The associated Kurikulum object.
     * @throws PropelException
     */
    public function getKurikulumRelatedByKurikulumId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aKurikulumRelatedByKurikulumId === null && ($this->kurikulum_id !== null) && $doQuery) {
            $this->aKurikulumRelatedByKurikulumId = KurikulumQuery::create()->findPk($this->kurikulum_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aKurikulumRelatedByKurikulumId->addRombonganBelajarsRelatedByKurikulumId($this);
             */
        }

        return $this->aKurikulumRelatedByKurikulumId;
    }

    /**
     * Declares an association between this object and a Kurikulum object.
     *
     * @param             Kurikulum $v
     * @return RombonganBelajar The current object (for fluent API support)
     * @throws PropelException
     */
    public function setKurikulumRelatedByKurikulumId(Kurikulum $v = null)
    {
        if ($v === null) {
            $this->setKurikulumId(NULL);
        } else {
            $this->setKurikulumId($v->getKurikulumId());
        }

        $this->aKurikulumRelatedByKurikulumId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Kurikulum object, it will not be re-added.
        if ($v !== null) {
            $v->addRombonganBelajarRelatedByKurikulumId($this);
        }


        return $this;
    }


    /**
     * Get the associated Kurikulum object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Kurikulum The associated Kurikulum object.
     * @throws PropelException
     */
    public function getKurikulumRelatedByKurikulumId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aKurikulumRelatedByKurikulumId === null && ($this->kurikulum_id !== null) && $doQuery) {
            $this->aKurikulumRelatedByKurikulumId = KurikulumQuery::create()->findPk($this->kurikulum_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aKurikulumRelatedByKurikulumId->addRombonganBelajarsRelatedByKurikulumId($this);
             */
        }

        return $this->aKurikulumRelatedByKurikulumId;
    }

    /**
     * Declares an association between this object and a Semester object.
     *
     * @param             Semester $v
     * @return RombonganBelajar The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSemesterRelatedBySemesterId(Semester $v = null)
    {
        if ($v === null) {
            $this->setSemesterId(NULL);
        } else {
            $this->setSemesterId($v->getSemesterId());
        }

        $this->aSemesterRelatedBySemesterId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Semester object, it will not be re-added.
        if ($v !== null) {
            $v->addRombonganBelajarRelatedBySemesterId($this);
        }


        return $this;
    }


    /**
     * Get the associated Semester object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Semester The associated Semester object.
     * @throws PropelException
     */
    public function getSemesterRelatedBySemesterId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSemesterRelatedBySemesterId === null && (($this->semester_id !== "" && $this->semester_id !== null)) && $doQuery) {
            $this->aSemesterRelatedBySemesterId = SemesterQuery::create()->findPk($this->semester_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSemesterRelatedBySemesterId->addRombonganBelajarsRelatedBySemesterId($this);
             */
        }

        return $this->aSemesterRelatedBySemesterId;
    }

    /**
     * Declares an association between this object and a Semester object.
     *
     * @param             Semester $v
     * @return RombonganBelajar The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSemesterRelatedBySemesterId(Semester $v = null)
    {
        if ($v === null) {
            $this->setSemesterId(NULL);
        } else {
            $this->setSemesterId($v->getSemesterId());
        }

        $this->aSemesterRelatedBySemesterId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Semester object, it will not be re-added.
        if ($v !== null) {
            $v->addRombonganBelajarRelatedBySemesterId($this);
        }


        return $this;
    }


    /**
     * Get the associated Semester object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Semester The associated Semester object.
     * @throws PropelException
     */
    public function getSemesterRelatedBySemesterId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSemesterRelatedBySemesterId === null && (($this->semester_id !== "" && $this->semester_id !== null)) && $doQuery) {
            $this->aSemesterRelatedBySemesterId = SemesterQuery::create()->findPk($this->semester_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSemesterRelatedBySemesterId->addRombonganBelajarsRelatedBySemesterId($this);
             */
        }

        return $this->aSemesterRelatedBySemesterId;
    }

    /**
     * Declares an association between this object and a TingkatPendidikan object.
     *
     * @param             TingkatPendidikan $v
     * @return RombonganBelajar The current object (for fluent API support)
     * @throws PropelException
     */
    public function setTingkatPendidikanRelatedByTingkatPendidikanId(TingkatPendidikan $v = null)
    {
        if ($v === null) {
            $this->setTingkatPendidikanId(NULL);
        } else {
            $this->setTingkatPendidikanId($v->getTingkatPendidikanId());
        }

        $this->aTingkatPendidikanRelatedByTingkatPendidikanId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the TingkatPendidikan object, it will not be re-added.
        if ($v !== null) {
            $v->addRombonganBelajarRelatedByTingkatPendidikanId($this);
        }


        return $this;
    }


    /**
     * Get the associated TingkatPendidikan object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return TingkatPendidikan The associated TingkatPendidikan object.
     * @throws PropelException
     */
    public function getTingkatPendidikanRelatedByTingkatPendidikanId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aTingkatPendidikanRelatedByTingkatPendidikanId === null && (($this->tingkat_pendidikan_id !== "" && $this->tingkat_pendidikan_id !== null)) && $doQuery) {
            $this->aTingkatPendidikanRelatedByTingkatPendidikanId = TingkatPendidikanQuery::create()->findPk($this->tingkat_pendidikan_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aTingkatPendidikanRelatedByTingkatPendidikanId->addRombonganBelajarsRelatedByTingkatPendidikanId($this);
             */
        }

        return $this->aTingkatPendidikanRelatedByTingkatPendidikanId;
    }

    /**
     * Declares an association between this object and a TingkatPendidikan object.
     *
     * @param             TingkatPendidikan $v
     * @return RombonganBelajar The current object (for fluent API support)
     * @throws PropelException
     */
    public function setTingkatPendidikanRelatedByTingkatPendidikanId(TingkatPendidikan $v = null)
    {
        if ($v === null) {
            $this->setTingkatPendidikanId(NULL);
        } else {
            $this->setTingkatPendidikanId($v->getTingkatPendidikanId());
        }

        $this->aTingkatPendidikanRelatedByTingkatPendidikanId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the TingkatPendidikan object, it will not be re-added.
        if ($v !== null) {
            $v->addRombonganBelajarRelatedByTingkatPendidikanId($this);
        }


        return $this;
    }


    /**
     * Get the associated TingkatPendidikan object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return TingkatPendidikan The associated TingkatPendidikan object.
     * @throws PropelException
     */
    public function getTingkatPendidikanRelatedByTingkatPendidikanId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aTingkatPendidikanRelatedByTingkatPendidikanId === null && (($this->tingkat_pendidikan_id !== "" && $this->tingkat_pendidikan_id !== null)) && $doQuery) {
            $this->aTingkatPendidikanRelatedByTingkatPendidikanId = TingkatPendidikanQuery::create()->findPk($this->tingkat_pendidikan_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aTingkatPendidikanRelatedByTingkatPendidikanId->addRombonganBelajarsRelatedByTingkatPendidikanId($this);
             */
        }

        return $this->aTingkatPendidikanRelatedByTingkatPendidikanId;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('VldRombelRelatedByRombonganBelajarId' == $relationName) {
            $this->initVldRombelsRelatedByRombonganBelajarId();
        }
        if ('VldRombelRelatedByRombonganBelajarId' == $relationName) {
            $this->initVldRombelsRelatedByRombonganBelajarId();
        }
        if ('AnggotaRombelRelatedByRombonganBelajarId' == $relationName) {
            $this->initAnggotaRombelsRelatedByRombonganBelajarId();
        }
        if ('AnggotaRombelRelatedByRombonganBelajarId' == $relationName) {
            $this->initAnggotaRombelsRelatedByRombonganBelajarId();
        }
        if ('PembelajaranRelatedByRombonganBelajarId' == $relationName) {
            $this->initPembelajaransRelatedByRombonganBelajarId();
        }
        if ('PembelajaranRelatedByRombonganBelajarId' == $relationName) {
            $this->initPembelajaransRelatedByRombonganBelajarId();
        }
    }

    /**
     * Clears out the collVldRombelsRelatedByRombonganBelajarId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return RombonganBelajar The current object (for fluent API support)
     * @see        addVldRombelsRelatedByRombonganBelajarId()
     */
    public function clearVldRombelsRelatedByRombonganBelajarId()
    {
        $this->collVldRombelsRelatedByRombonganBelajarId = null; // important to set this to null since that means it is uninitialized
        $this->collVldRombelsRelatedByRombonganBelajarIdPartial = null;

        return $this;
    }

    /**
     * reset is the collVldRombelsRelatedByRombonganBelajarId collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldRombelsRelatedByRombonganBelajarId($v = true)
    {
        $this->collVldRombelsRelatedByRombonganBelajarIdPartial = $v;
    }

    /**
     * Initializes the collVldRombelsRelatedByRombonganBelajarId collection.
     *
     * By default this just sets the collVldRombelsRelatedByRombonganBelajarId collection to an empty array (like clearcollVldRombelsRelatedByRombonganBelajarId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldRombelsRelatedByRombonganBelajarId($overrideExisting = true)
    {
        if (null !== $this->collVldRombelsRelatedByRombonganBelajarId && !$overrideExisting) {
            return;
        }
        $this->collVldRombelsRelatedByRombonganBelajarId = new PropelObjectCollection();
        $this->collVldRombelsRelatedByRombonganBelajarId->setModel('VldRombel');
    }

    /**
     * Gets an array of VldRombel objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this RombonganBelajar is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldRombel[] List of VldRombel objects
     * @throws PropelException
     */
    public function getVldRombelsRelatedByRombonganBelajarId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldRombelsRelatedByRombonganBelajarIdPartial && !$this->isNew();
        if (null === $this->collVldRombelsRelatedByRombonganBelajarId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldRombelsRelatedByRombonganBelajarId) {
                // return empty collection
                $this->initVldRombelsRelatedByRombonganBelajarId();
            } else {
                $collVldRombelsRelatedByRombonganBelajarId = VldRombelQuery::create(null, $criteria)
                    ->filterByRombonganBelajarRelatedByRombonganBelajarId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldRombelsRelatedByRombonganBelajarIdPartial && count($collVldRombelsRelatedByRombonganBelajarId)) {
                      $this->initVldRombelsRelatedByRombonganBelajarId(false);

                      foreach($collVldRombelsRelatedByRombonganBelajarId as $obj) {
                        if (false == $this->collVldRombelsRelatedByRombonganBelajarId->contains($obj)) {
                          $this->collVldRombelsRelatedByRombonganBelajarId->append($obj);
                        }
                      }

                      $this->collVldRombelsRelatedByRombonganBelajarIdPartial = true;
                    }

                    $collVldRombelsRelatedByRombonganBelajarId->getInternalIterator()->rewind();
                    return $collVldRombelsRelatedByRombonganBelajarId;
                }

                if($partial && $this->collVldRombelsRelatedByRombonganBelajarId) {
                    foreach($this->collVldRombelsRelatedByRombonganBelajarId as $obj) {
                        if($obj->isNew()) {
                            $collVldRombelsRelatedByRombonganBelajarId[] = $obj;
                        }
                    }
                }

                $this->collVldRombelsRelatedByRombonganBelajarId = $collVldRombelsRelatedByRombonganBelajarId;
                $this->collVldRombelsRelatedByRombonganBelajarIdPartial = false;
            }
        }

        return $this->collVldRombelsRelatedByRombonganBelajarId;
    }

    /**
     * Sets a collection of VldRombelRelatedByRombonganBelajarId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldRombelsRelatedByRombonganBelajarId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setVldRombelsRelatedByRombonganBelajarId(PropelCollection $vldRombelsRelatedByRombonganBelajarId, PropelPDO $con = null)
    {
        $vldRombelsRelatedByRombonganBelajarIdToDelete = $this->getVldRombelsRelatedByRombonganBelajarId(new Criteria(), $con)->diff($vldRombelsRelatedByRombonganBelajarId);

        $this->vldRombelsRelatedByRombonganBelajarIdScheduledForDeletion = unserialize(serialize($vldRombelsRelatedByRombonganBelajarIdToDelete));

        foreach ($vldRombelsRelatedByRombonganBelajarIdToDelete as $vldRombelRelatedByRombonganBelajarIdRemoved) {
            $vldRombelRelatedByRombonganBelajarIdRemoved->setRombonganBelajarRelatedByRombonganBelajarId(null);
        }

        $this->collVldRombelsRelatedByRombonganBelajarId = null;
        foreach ($vldRombelsRelatedByRombonganBelajarId as $vldRombelRelatedByRombonganBelajarId) {
            $this->addVldRombelRelatedByRombonganBelajarId($vldRombelRelatedByRombonganBelajarId);
        }

        $this->collVldRombelsRelatedByRombonganBelajarId = $vldRombelsRelatedByRombonganBelajarId;
        $this->collVldRombelsRelatedByRombonganBelajarIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldRombel objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldRombel objects.
     * @throws PropelException
     */
    public function countVldRombelsRelatedByRombonganBelajarId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldRombelsRelatedByRombonganBelajarIdPartial && !$this->isNew();
        if (null === $this->collVldRombelsRelatedByRombonganBelajarId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldRombelsRelatedByRombonganBelajarId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldRombelsRelatedByRombonganBelajarId());
            }
            $query = VldRombelQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByRombonganBelajarRelatedByRombonganBelajarId($this)
                ->count($con);
        }

        return count($this->collVldRombelsRelatedByRombonganBelajarId);
    }

    /**
     * Method called to associate a VldRombel object to this object
     * through the VldRombel foreign key attribute.
     *
     * @param    VldRombel $l VldRombel
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function addVldRombelRelatedByRombonganBelajarId(VldRombel $l)
    {
        if ($this->collVldRombelsRelatedByRombonganBelajarId === null) {
            $this->initVldRombelsRelatedByRombonganBelajarId();
            $this->collVldRombelsRelatedByRombonganBelajarIdPartial = true;
        }
        if (!in_array($l, $this->collVldRombelsRelatedByRombonganBelajarId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldRombelRelatedByRombonganBelajarId($l);
        }

        return $this;
    }

    /**
     * @param	VldRombelRelatedByRombonganBelajarId $vldRombelRelatedByRombonganBelajarId The vldRombelRelatedByRombonganBelajarId object to add.
     */
    protected function doAddVldRombelRelatedByRombonganBelajarId($vldRombelRelatedByRombonganBelajarId)
    {
        $this->collVldRombelsRelatedByRombonganBelajarId[]= $vldRombelRelatedByRombonganBelajarId;
        $vldRombelRelatedByRombonganBelajarId->setRombonganBelajarRelatedByRombonganBelajarId($this);
    }

    /**
     * @param	VldRombelRelatedByRombonganBelajarId $vldRombelRelatedByRombonganBelajarId The vldRombelRelatedByRombonganBelajarId object to remove.
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function removeVldRombelRelatedByRombonganBelajarId($vldRombelRelatedByRombonganBelajarId)
    {
        if ($this->getVldRombelsRelatedByRombonganBelajarId()->contains($vldRombelRelatedByRombonganBelajarId)) {
            $this->collVldRombelsRelatedByRombonganBelajarId->remove($this->collVldRombelsRelatedByRombonganBelajarId->search($vldRombelRelatedByRombonganBelajarId));
            if (null === $this->vldRombelsRelatedByRombonganBelajarIdScheduledForDeletion) {
                $this->vldRombelsRelatedByRombonganBelajarIdScheduledForDeletion = clone $this->collVldRombelsRelatedByRombonganBelajarId;
                $this->vldRombelsRelatedByRombonganBelajarIdScheduledForDeletion->clear();
            }
            $this->vldRombelsRelatedByRombonganBelajarIdScheduledForDeletion[]= clone $vldRombelRelatedByRombonganBelajarId;
            $vldRombelRelatedByRombonganBelajarId->setRombonganBelajarRelatedByRombonganBelajarId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this RombonganBelajar is new, it will return
     * an empty collection; or if this RombonganBelajar has previously
     * been saved, it will retrieve related VldRombelsRelatedByRombonganBelajarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in RombonganBelajar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldRombel[] List of VldRombel objects
     */
    public function getVldRombelsRelatedByRombonganBelajarIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldRombelQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldRombelsRelatedByRombonganBelajarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this RombonganBelajar is new, it will return
     * an empty collection; or if this RombonganBelajar has previously
     * been saved, it will retrieve related VldRombelsRelatedByRombonganBelajarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in RombonganBelajar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldRombel[] List of VldRombel objects
     */
    public function getVldRombelsRelatedByRombonganBelajarIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldRombelQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldRombelsRelatedByRombonganBelajarId($query, $con);
    }

    /**
     * Clears out the collVldRombelsRelatedByRombonganBelajarId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return RombonganBelajar The current object (for fluent API support)
     * @see        addVldRombelsRelatedByRombonganBelajarId()
     */
    public function clearVldRombelsRelatedByRombonganBelajarId()
    {
        $this->collVldRombelsRelatedByRombonganBelajarId = null; // important to set this to null since that means it is uninitialized
        $this->collVldRombelsRelatedByRombonganBelajarIdPartial = null;

        return $this;
    }

    /**
     * reset is the collVldRombelsRelatedByRombonganBelajarId collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldRombelsRelatedByRombonganBelajarId($v = true)
    {
        $this->collVldRombelsRelatedByRombonganBelajarIdPartial = $v;
    }

    /**
     * Initializes the collVldRombelsRelatedByRombonganBelajarId collection.
     *
     * By default this just sets the collVldRombelsRelatedByRombonganBelajarId collection to an empty array (like clearcollVldRombelsRelatedByRombonganBelajarId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldRombelsRelatedByRombonganBelajarId($overrideExisting = true)
    {
        if (null !== $this->collVldRombelsRelatedByRombonganBelajarId && !$overrideExisting) {
            return;
        }
        $this->collVldRombelsRelatedByRombonganBelajarId = new PropelObjectCollection();
        $this->collVldRombelsRelatedByRombonganBelajarId->setModel('VldRombel');
    }

    /**
     * Gets an array of VldRombel objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this RombonganBelajar is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldRombel[] List of VldRombel objects
     * @throws PropelException
     */
    public function getVldRombelsRelatedByRombonganBelajarId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldRombelsRelatedByRombonganBelajarIdPartial && !$this->isNew();
        if (null === $this->collVldRombelsRelatedByRombonganBelajarId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldRombelsRelatedByRombonganBelajarId) {
                // return empty collection
                $this->initVldRombelsRelatedByRombonganBelajarId();
            } else {
                $collVldRombelsRelatedByRombonganBelajarId = VldRombelQuery::create(null, $criteria)
                    ->filterByRombonganBelajarRelatedByRombonganBelajarId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldRombelsRelatedByRombonganBelajarIdPartial && count($collVldRombelsRelatedByRombonganBelajarId)) {
                      $this->initVldRombelsRelatedByRombonganBelajarId(false);

                      foreach($collVldRombelsRelatedByRombonganBelajarId as $obj) {
                        if (false == $this->collVldRombelsRelatedByRombonganBelajarId->contains($obj)) {
                          $this->collVldRombelsRelatedByRombonganBelajarId->append($obj);
                        }
                      }

                      $this->collVldRombelsRelatedByRombonganBelajarIdPartial = true;
                    }

                    $collVldRombelsRelatedByRombonganBelajarId->getInternalIterator()->rewind();
                    return $collVldRombelsRelatedByRombonganBelajarId;
                }

                if($partial && $this->collVldRombelsRelatedByRombonganBelajarId) {
                    foreach($this->collVldRombelsRelatedByRombonganBelajarId as $obj) {
                        if($obj->isNew()) {
                            $collVldRombelsRelatedByRombonganBelajarId[] = $obj;
                        }
                    }
                }

                $this->collVldRombelsRelatedByRombonganBelajarId = $collVldRombelsRelatedByRombonganBelajarId;
                $this->collVldRombelsRelatedByRombonganBelajarIdPartial = false;
            }
        }

        return $this->collVldRombelsRelatedByRombonganBelajarId;
    }

    /**
     * Sets a collection of VldRombelRelatedByRombonganBelajarId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldRombelsRelatedByRombonganBelajarId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setVldRombelsRelatedByRombonganBelajarId(PropelCollection $vldRombelsRelatedByRombonganBelajarId, PropelPDO $con = null)
    {
        $vldRombelsRelatedByRombonganBelajarIdToDelete = $this->getVldRombelsRelatedByRombonganBelajarId(new Criteria(), $con)->diff($vldRombelsRelatedByRombonganBelajarId);

        $this->vldRombelsRelatedByRombonganBelajarIdScheduledForDeletion = unserialize(serialize($vldRombelsRelatedByRombonganBelajarIdToDelete));

        foreach ($vldRombelsRelatedByRombonganBelajarIdToDelete as $vldRombelRelatedByRombonganBelajarIdRemoved) {
            $vldRombelRelatedByRombonganBelajarIdRemoved->setRombonganBelajarRelatedByRombonganBelajarId(null);
        }

        $this->collVldRombelsRelatedByRombonganBelajarId = null;
        foreach ($vldRombelsRelatedByRombonganBelajarId as $vldRombelRelatedByRombonganBelajarId) {
            $this->addVldRombelRelatedByRombonganBelajarId($vldRombelRelatedByRombonganBelajarId);
        }

        $this->collVldRombelsRelatedByRombonganBelajarId = $vldRombelsRelatedByRombonganBelajarId;
        $this->collVldRombelsRelatedByRombonganBelajarIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldRombel objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldRombel objects.
     * @throws PropelException
     */
    public function countVldRombelsRelatedByRombonganBelajarId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldRombelsRelatedByRombonganBelajarIdPartial && !$this->isNew();
        if (null === $this->collVldRombelsRelatedByRombonganBelajarId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldRombelsRelatedByRombonganBelajarId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldRombelsRelatedByRombonganBelajarId());
            }
            $query = VldRombelQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByRombonganBelajarRelatedByRombonganBelajarId($this)
                ->count($con);
        }

        return count($this->collVldRombelsRelatedByRombonganBelajarId);
    }

    /**
     * Method called to associate a VldRombel object to this object
     * through the VldRombel foreign key attribute.
     *
     * @param    VldRombel $l VldRombel
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function addVldRombelRelatedByRombonganBelajarId(VldRombel $l)
    {
        if ($this->collVldRombelsRelatedByRombonganBelajarId === null) {
            $this->initVldRombelsRelatedByRombonganBelajarId();
            $this->collVldRombelsRelatedByRombonganBelajarIdPartial = true;
        }
        if (!in_array($l, $this->collVldRombelsRelatedByRombonganBelajarId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldRombelRelatedByRombonganBelajarId($l);
        }

        return $this;
    }

    /**
     * @param	VldRombelRelatedByRombonganBelajarId $vldRombelRelatedByRombonganBelajarId The vldRombelRelatedByRombonganBelajarId object to add.
     */
    protected function doAddVldRombelRelatedByRombonganBelajarId($vldRombelRelatedByRombonganBelajarId)
    {
        $this->collVldRombelsRelatedByRombonganBelajarId[]= $vldRombelRelatedByRombonganBelajarId;
        $vldRombelRelatedByRombonganBelajarId->setRombonganBelajarRelatedByRombonganBelajarId($this);
    }

    /**
     * @param	VldRombelRelatedByRombonganBelajarId $vldRombelRelatedByRombonganBelajarId The vldRombelRelatedByRombonganBelajarId object to remove.
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function removeVldRombelRelatedByRombonganBelajarId($vldRombelRelatedByRombonganBelajarId)
    {
        if ($this->getVldRombelsRelatedByRombonganBelajarId()->contains($vldRombelRelatedByRombonganBelajarId)) {
            $this->collVldRombelsRelatedByRombonganBelajarId->remove($this->collVldRombelsRelatedByRombonganBelajarId->search($vldRombelRelatedByRombonganBelajarId));
            if (null === $this->vldRombelsRelatedByRombonganBelajarIdScheduledForDeletion) {
                $this->vldRombelsRelatedByRombonganBelajarIdScheduledForDeletion = clone $this->collVldRombelsRelatedByRombonganBelajarId;
                $this->vldRombelsRelatedByRombonganBelajarIdScheduledForDeletion->clear();
            }
            $this->vldRombelsRelatedByRombonganBelajarIdScheduledForDeletion[]= clone $vldRombelRelatedByRombonganBelajarId;
            $vldRombelRelatedByRombonganBelajarId->setRombonganBelajarRelatedByRombonganBelajarId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this RombonganBelajar is new, it will return
     * an empty collection; or if this RombonganBelajar has previously
     * been saved, it will retrieve related VldRombelsRelatedByRombonganBelajarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in RombonganBelajar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldRombel[] List of VldRombel objects
     */
    public function getVldRombelsRelatedByRombonganBelajarIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldRombelQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldRombelsRelatedByRombonganBelajarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this RombonganBelajar is new, it will return
     * an empty collection; or if this RombonganBelajar has previously
     * been saved, it will retrieve related VldRombelsRelatedByRombonganBelajarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in RombonganBelajar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldRombel[] List of VldRombel objects
     */
    public function getVldRombelsRelatedByRombonganBelajarIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldRombelQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldRombelsRelatedByRombonganBelajarId($query, $con);
    }

    /**
     * Clears out the collAnggotaRombelsRelatedByRombonganBelajarId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return RombonganBelajar The current object (for fluent API support)
     * @see        addAnggotaRombelsRelatedByRombonganBelajarId()
     */
    public function clearAnggotaRombelsRelatedByRombonganBelajarId()
    {
        $this->collAnggotaRombelsRelatedByRombonganBelajarId = null; // important to set this to null since that means it is uninitialized
        $this->collAnggotaRombelsRelatedByRombonganBelajarIdPartial = null;

        return $this;
    }

    /**
     * reset is the collAnggotaRombelsRelatedByRombonganBelajarId collection loaded partially
     *
     * @return void
     */
    public function resetPartialAnggotaRombelsRelatedByRombonganBelajarId($v = true)
    {
        $this->collAnggotaRombelsRelatedByRombonganBelajarIdPartial = $v;
    }

    /**
     * Initializes the collAnggotaRombelsRelatedByRombonganBelajarId collection.
     *
     * By default this just sets the collAnggotaRombelsRelatedByRombonganBelajarId collection to an empty array (like clearcollAnggotaRombelsRelatedByRombonganBelajarId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initAnggotaRombelsRelatedByRombonganBelajarId($overrideExisting = true)
    {
        if (null !== $this->collAnggotaRombelsRelatedByRombonganBelajarId && !$overrideExisting) {
            return;
        }
        $this->collAnggotaRombelsRelatedByRombonganBelajarId = new PropelObjectCollection();
        $this->collAnggotaRombelsRelatedByRombonganBelajarId->setModel('AnggotaRombel');
    }

    /**
     * Gets an array of AnggotaRombel objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this RombonganBelajar is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|AnggotaRombel[] List of AnggotaRombel objects
     * @throws PropelException
     */
    public function getAnggotaRombelsRelatedByRombonganBelajarId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collAnggotaRombelsRelatedByRombonganBelajarIdPartial && !$this->isNew();
        if (null === $this->collAnggotaRombelsRelatedByRombonganBelajarId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collAnggotaRombelsRelatedByRombonganBelajarId) {
                // return empty collection
                $this->initAnggotaRombelsRelatedByRombonganBelajarId();
            } else {
                $collAnggotaRombelsRelatedByRombonganBelajarId = AnggotaRombelQuery::create(null, $criteria)
                    ->filterByRombonganBelajarRelatedByRombonganBelajarId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collAnggotaRombelsRelatedByRombonganBelajarIdPartial && count($collAnggotaRombelsRelatedByRombonganBelajarId)) {
                      $this->initAnggotaRombelsRelatedByRombonganBelajarId(false);

                      foreach($collAnggotaRombelsRelatedByRombonganBelajarId as $obj) {
                        if (false == $this->collAnggotaRombelsRelatedByRombonganBelajarId->contains($obj)) {
                          $this->collAnggotaRombelsRelatedByRombonganBelajarId->append($obj);
                        }
                      }

                      $this->collAnggotaRombelsRelatedByRombonganBelajarIdPartial = true;
                    }

                    $collAnggotaRombelsRelatedByRombonganBelajarId->getInternalIterator()->rewind();
                    return $collAnggotaRombelsRelatedByRombonganBelajarId;
                }

                if($partial && $this->collAnggotaRombelsRelatedByRombonganBelajarId) {
                    foreach($this->collAnggotaRombelsRelatedByRombonganBelajarId as $obj) {
                        if($obj->isNew()) {
                            $collAnggotaRombelsRelatedByRombonganBelajarId[] = $obj;
                        }
                    }
                }

                $this->collAnggotaRombelsRelatedByRombonganBelajarId = $collAnggotaRombelsRelatedByRombonganBelajarId;
                $this->collAnggotaRombelsRelatedByRombonganBelajarIdPartial = false;
            }
        }

        return $this->collAnggotaRombelsRelatedByRombonganBelajarId;
    }

    /**
     * Sets a collection of AnggotaRombelRelatedByRombonganBelajarId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $anggotaRombelsRelatedByRombonganBelajarId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setAnggotaRombelsRelatedByRombonganBelajarId(PropelCollection $anggotaRombelsRelatedByRombonganBelajarId, PropelPDO $con = null)
    {
        $anggotaRombelsRelatedByRombonganBelajarIdToDelete = $this->getAnggotaRombelsRelatedByRombonganBelajarId(new Criteria(), $con)->diff($anggotaRombelsRelatedByRombonganBelajarId);

        $this->anggotaRombelsRelatedByRombonganBelajarIdScheduledForDeletion = unserialize(serialize($anggotaRombelsRelatedByRombonganBelajarIdToDelete));

        foreach ($anggotaRombelsRelatedByRombonganBelajarIdToDelete as $anggotaRombelRelatedByRombonganBelajarIdRemoved) {
            $anggotaRombelRelatedByRombonganBelajarIdRemoved->setRombonganBelajarRelatedByRombonganBelajarId(null);
        }

        $this->collAnggotaRombelsRelatedByRombonganBelajarId = null;
        foreach ($anggotaRombelsRelatedByRombonganBelajarId as $anggotaRombelRelatedByRombonganBelajarId) {
            $this->addAnggotaRombelRelatedByRombonganBelajarId($anggotaRombelRelatedByRombonganBelajarId);
        }

        $this->collAnggotaRombelsRelatedByRombonganBelajarId = $anggotaRombelsRelatedByRombonganBelajarId;
        $this->collAnggotaRombelsRelatedByRombonganBelajarIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related AnggotaRombel objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related AnggotaRombel objects.
     * @throws PropelException
     */
    public function countAnggotaRombelsRelatedByRombonganBelajarId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collAnggotaRombelsRelatedByRombonganBelajarIdPartial && !$this->isNew();
        if (null === $this->collAnggotaRombelsRelatedByRombonganBelajarId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collAnggotaRombelsRelatedByRombonganBelajarId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getAnggotaRombelsRelatedByRombonganBelajarId());
            }
            $query = AnggotaRombelQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByRombonganBelajarRelatedByRombonganBelajarId($this)
                ->count($con);
        }

        return count($this->collAnggotaRombelsRelatedByRombonganBelajarId);
    }

    /**
     * Method called to associate a AnggotaRombel object to this object
     * through the AnggotaRombel foreign key attribute.
     *
     * @param    AnggotaRombel $l AnggotaRombel
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function addAnggotaRombelRelatedByRombonganBelajarId(AnggotaRombel $l)
    {
        if ($this->collAnggotaRombelsRelatedByRombonganBelajarId === null) {
            $this->initAnggotaRombelsRelatedByRombonganBelajarId();
            $this->collAnggotaRombelsRelatedByRombonganBelajarIdPartial = true;
        }
        if (!in_array($l, $this->collAnggotaRombelsRelatedByRombonganBelajarId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddAnggotaRombelRelatedByRombonganBelajarId($l);
        }

        return $this;
    }

    /**
     * @param	AnggotaRombelRelatedByRombonganBelajarId $anggotaRombelRelatedByRombonganBelajarId The anggotaRombelRelatedByRombonganBelajarId object to add.
     */
    protected function doAddAnggotaRombelRelatedByRombonganBelajarId($anggotaRombelRelatedByRombonganBelajarId)
    {
        $this->collAnggotaRombelsRelatedByRombonganBelajarId[]= $anggotaRombelRelatedByRombonganBelajarId;
        $anggotaRombelRelatedByRombonganBelajarId->setRombonganBelajarRelatedByRombonganBelajarId($this);
    }

    /**
     * @param	AnggotaRombelRelatedByRombonganBelajarId $anggotaRombelRelatedByRombonganBelajarId The anggotaRombelRelatedByRombonganBelajarId object to remove.
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function removeAnggotaRombelRelatedByRombonganBelajarId($anggotaRombelRelatedByRombonganBelajarId)
    {
        if ($this->getAnggotaRombelsRelatedByRombonganBelajarId()->contains($anggotaRombelRelatedByRombonganBelajarId)) {
            $this->collAnggotaRombelsRelatedByRombonganBelajarId->remove($this->collAnggotaRombelsRelatedByRombonganBelajarId->search($anggotaRombelRelatedByRombonganBelajarId));
            if (null === $this->anggotaRombelsRelatedByRombonganBelajarIdScheduledForDeletion) {
                $this->anggotaRombelsRelatedByRombonganBelajarIdScheduledForDeletion = clone $this->collAnggotaRombelsRelatedByRombonganBelajarId;
                $this->anggotaRombelsRelatedByRombonganBelajarIdScheduledForDeletion->clear();
            }
            $this->anggotaRombelsRelatedByRombonganBelajarIdScheduledForDeletion[]= clone $anggotaRombelRelatedByRombonganBelajarId;
            $anggotaRombelRelatedByRombonganBelajarId->setRombonganBelajarRelatedByRombonganBelajarId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this RombonganBelajar is new, it will return
     * an empty collection; or if this RombonganBelajar has previously
     * been saved, it will retrieve related AnggotaRombelsRelatedByRombonganBelajarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in RombonganBelajar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|AnggotaRombel[] List of AnggotaRombel objects
     */
    public function getAnggotaRombelsRelatedByRombonganBelajarIdJoinPesertaDidikRelatedByPesertaDidikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AnggotaRombelQuery::create(null, $criteria);
        $query->joinWith('PesertaDidikRelatedByPesertaDidikId', $join_behavior);

        return $this->getAnggotaRombelsRelatedByRombonganBelajarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this RombonganBelajar is new, it will return
     * an empty collection; or if this RombonganBelajar has previously
     * been saved, it will retrieve related AnggotaRombelsRelatedByRombonganBelajarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in RombonganBelajar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|AnggotaRombel[] List of AnggotaRombel objects
     */
    public function getAnggotaRombelsRelatedByRombonganBelajarIdJoinPesertaDidikRelatedByPesertaDidikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AnggotaRombelQuery::create(null, $criteria);
        $query->joinWith('PesertaDidikRelatedByPesertaDidikId', $join_behavior);

        return $this->getAnggotaRombelsRelatedByRombonganBelajarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this RombonganBelajar is new, it will return
     * an empty collection; or if this RombonganBelajar has previously
     * been saved, it will retrieve related AnggotaRombelsRelatedByRombonganBelajarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in RombonganBelajar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|AnggotaRombel[] List of AnggotaRombel objects
     */
    public function getAnggotaRombelsRelatedByRombonganBelajarIdJoinJenisPendaftaranRelatedByJenisPendaftaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AnggotaRombelQuery::create(null, $criteria);
        $query->joinWith('JenisPendaftaranRelatedByJenisPendaftaranId', $join_behavior);

        return $this->getAnggotaRombelsRelatedByRombonganBelajarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this RombonganBelajar is new, it will return
     * an empty collection; or if this RombonganBelajar has previously
     * been saved, it will retrieve related AnggotaRombelsRelatedByRombonganBelajarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in RombonganBelajar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|AnggotaRombel[] List of AnggotaRombel objects
     */
    public function getAnggotaRombelsRelatedByRombonganBelajarIdJoinJenisPendaftaranRelatedByJenisPendaftaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AnggotaRombelQuery::create(null, $criteria);
        $query->joinWith('JenisPendaftaranRelatedByJenisPendaftaranId', $join_behavior);

        return $this->getAnggotaRombelsRelatedByRombonganBelajarId($query, $con);
    }

    /**
     * Clears out the collAnggotaRombelsRelatedByRombonganBelajarId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return RombonganBelajar The current object (for fluent API support)
     * @see        addAnggotaRombelsRelatedByRombonganBelajarId()
     */
    public function clearAnggotaRombelsRelatedByRombonganBelajarId()
    {
        $this->collAnggotaRombelsRelatedByRombonganBelajarId = null; // important to set this to null since that means it is uninitialized
        $this->collAnggotaRombelsRelatedByRombonganBelajarIdPartial = null;

        return $this;
    }

    /**
     * reset is the collAnggotaRombelsRelatedByRombonganBelajarId collection loaded partially
     *
     * @return void
     */
    public function resetPartialAnggotaRombelsRelatedByRombonganBelajarId($v = true)
    {
        $this->collAnggotaRombelsRelatedByRombonganBelajarIdPartial = $v;
    }

    /**
     * Initializes the collAnggotaRombelsRelatedByRombonganBelajarId collection.
     *
     * By default this just sets the collAnggotaRombelsRelatedByRombonganBelajarId collection to an empty array (like clearcollAnggotaRombelsRelatedByRombonganBelajarId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initAnggotaRombelsRelatedByRombonganBelajarId($overrideExisting = true)
    {
        if (null !== $this->collAnggotaRombelsRelatedByRombonganBelajarId && !$overrideExisting) {
            return;
        }
        $this->collAnggotaRombelsRelatedByRombonganBelajarId = new PropelObjectCollection();
        $this->collAnggotaRombelsRelatedByRombonganBelajarId->setModel('AnggotaRombel');
    }

    /**
     * Gets an array of AnggotaRombel objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this RombonganBelajar is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|AnggotaRombel[] List of AnggotaRombel objects
     * @throws PropelException
     */
    public function getAnggotaRombelsRelatedByRombonganBelajarId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collAnggotaRombelsRelatedByRombonganBelajarIdPartial && !$this->isNew();
        if (null === $this->collAnggotaRombelsRelatedByRombonganBelajarId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collAnggotaRombelsRelatedByRombonganBelajarId) {
                // return empty collection
                $this->initAnggotaRombelsRelatedByRombonganBelajarId();
            } else {
                $collAnggotaRombelsRelatedByRombonganBelajarId = AnggotaRombelQuery::create(null, $criteria)
                    ->filterByRombonganBelajarRelatedByRombonganBelajarId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collAnggotaRombelsRelatedByRombonganBelajarIdPartial && count($collAnggotaRombelsRelatedByRombonganBelajarId)) {
                      $this->initAnggotaRombelsRelatedByRombonganBelajarId(false);

                      foreach($collAnggotaRombelsRelatedByRombonganBelajarId as $obj) {
                        if (false == $this->collAnggotaRombelsRelatedByRombonganBelajarId->contains($obj)) {
                          $this->collAnggotaRombelsRelatedByRombonganBelajarId->append($obj);
                        }
                      }

                      $this->collAnggotaRombelsRelatedByRombonganBelajarIdPartial = true;
                    }

                    $collAnggotaRombelsRelatedByRombonganBelajarId->getInternalIterator()->rewind();
                    return $collAnggotaRombelsRelatedByRombonganBelajarId;
                }

                if($partial && $this->collAnggotaRombelsRelatedByRombonganBelajarId) {
                    foreach($this->collAnggotaRombelsRelatedByRombonganBelajarId as $obj) {
                        if($obj->isNew()) {
                            $collAnggotaRombelsRelatedByRombonganBelajarId[] = $obj;
                        }
                    }
                }

                $this->collAnggotaRombelsRelatedByRombonganBelajarId = $collAnggotaRombelsRelatedByRombonganBelajarId;
                $this->collAnggotaRombelsRelatedByRombonganBelajarIdPartial = false;
            }
        }

        return $this->collAnggotaRombelsRelatedByRombonganBelajarId;
    }

    /**
     * Sets a collection of AnggotaRombelRelatedByRombonganBelajarId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $anggotaRombelsRelatedByRombonganBelajarId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setAnggotaRombelsRelatedByRombonganBelajarId(PropelCollection $anggotaRombelsRelatedByRombonganBelajarId, PropelPDO $con = null)
    {
        $anggotaRombelsRelatedByRombonganBelajarIdToDelete = $this->getAnggotaRombelsRelatedByRombonganBelajarId(new Criteria(), $con)->diff($anggotaRombelsRelatedByRombonganBelajarId);

        $this->anggotaRombelsRelatedByRombonganBelajarIdScheduledForDeletion = unserialize(serialize($anggotaRombelsRelatedByRombonganBelajarIdToDelete));

        foreach ($anggotaRombelsRelatedByRombonganBelajarIdToDelete as $anggotaRombelRelatedByRombonganBelajarIdRemoved) {
            $anggotaRombelRelatedByRombonganBelajarIdRemoved->setRombonganBelajarRelatedByRombonganBelajarId(null);
        }

        $this->collAnggotaRombelsRelatedByRombonganBelajarId = null;
        foreach ($anggotaRombelsRelatedByRombonganBelajarId as $anggotaRombelRelatedByRombonganBelajarId) {
            $this->addAnggotaRombelRelatedByRombonganBelajarId($anggotaRombelRelatedByRombonganBelajarId);
        }

        $this->collAnggotaRombelsRelatedByRombonganBelajarId = $anggotaRombelsRelatedByRombonganBelajarId;
        $this->collAnggotaRombelsRelatedByRombonganBelajarIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related AnggotaRombel objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related AnggotaRombel objects.
     * @throws PropelException
     */
    public function countAnggotaRombelsRelatedByRombonganBelajarId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collAnggotaRombelsRelatedByRombonganBelajarIdPartial && !$this->isNew();
        if (null === $this->collAnggotaRombelsRelatedByRombonganBelajarId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collAnggotaRombelsRelatedByRombonganBelajarId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getAnggotaRombelsRelatedByRombonganBelajarId());
            }
            $query = AnggotaRombelQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByRombonganBelajarRelatedByRombonganBelajarId($this)
                ->count($con);
        }

        return count($this->collAnggotaRombelsRelatedByRombonganBelajarId);
    }

    /**
     * Method called to associate a AnggotaRombel object to this object
     * through the AnggotaRombel foreign key attribute.
     *
     * @param    AnggotaRombel $l AnggotaRombel
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function addAnggotaRombelRelatedByRombonganBelajarId(AnggotaRombel $l)
    {
        if ($this->collAnggotaRombelsRelatedByRombonganBelajarId === null) {
            $this->initAnggotaRombelsRelatedByRombonganBelajarId();
            $this->collAnggotaRombelsRelatedByRombonganBelajarIdPartial = true;
        }
        if (!in_array($l, $this->collAnggotaRombelsRelatedByRombonganBelajarId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddAnggotaRombelRelatedByRombonganBelajarId($l);
        }

        return $this;
    }

    /**
     * @param	AnggotaRombelRelatedByRombonganBelajarId $anggotaRombelRelatedByRombonganBelajarId The anggotaRombelRelatedByRombonganBelajarId object to add.
     */
    protected function doAddAnggotaRombelRelatedByRombonganBelajarId($anggotaRombelRelatedByRombonganBelajarId)
    {
        $this->collAnggotaRombelsRelatedByRombonganBelajarId[]= $anggotaRombelRelatedByRombonganBelajarId;
        $anggotaRombelRelatedByRombonganBelajarId->setRombonganBelajarRelatedByRombonganBelajarId($this);
    }

    /**
     * @param	AnggotaRombelRelatedByRombonganBelajarId $anggotaRombelRelatedByRombonganBelajarId The anggotaRombelRelatedByRombonganBelajarId object to remove.
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function removeAnggotaRombelRelatedByRombonganBelajarId($anggotaRombelRelatedByRombonganBelajarId)
    {
        if ($this->getAnggotaRombelsRelatedByRombonganBelajarId()->contains($anggotaRombelRelatedByRombonganBelajarId)) {
            $this->collAnggotaRombelsRelatedByRombonganBelajarId->remove($this->collAnggotaRombelsRelatedByRombonganBelajarId->search($anggotaRombelRelatedByRombonganBelajarId));
            if (null === $this->anggotaRombelsRelatedByRombonganBelajarIdScheduledForDeletion) {
                $this->anggotaRombelsRelatedByRombonganBelajarIdScheduledForDeletion = clone $this->collAnggotaRombelsRelatedByRombonganBelajarId;
                $this->anggotaRombelsRelatedByRombonganBelajarIdScheduledForDeletion->clear();
            }
            $this->anggotaRombelsRelatedByRombonganBelajarIdScheduledForDeletion[]= clone $anggotaRombelRelatedByRombonganBelajarId;
            $anggotaRombelRelatedByRombonganBelajarId->setRombonganBelajarRelatedByRombonganBelajarId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this RombonganBelajar is new, it will return
     * an empty collection; or if this RombonganBelajar has previously
     * been saved, it will retrieve related AnggotaRombelsRelatedByRombonganBelajarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in RombonganBelajar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|AnggotaRombel[] List of AnggotaRombel objects
     */
    public function getAnggotaRombelsRelatedByRombonganBelajarIdJoinPesertaDidikRelatedByPesertaDidikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AnggotaRombelQuery::create(null, $criteria);
        $query->joinWith('PesertaDidikRelatedByPesertaDidikId', $join_behavior);

        return $this->getAnggotaRombelsRelatedByRombonganBelajarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this RombonganBelajar is new, it will return
     * an empty collection; or if this RombonganBelajar has previously
     * been saved, it will retrieve related AnggotaRombelsRelatedByRombonganBelajarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in RombonganBelajar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|AnggotaRombel[] List of AnggotaRombel objects
     */
    public function getAnggotaRombelsRelatedByRombonganBelajarIdJoinPesertaDidikRelatedByPesertaDidikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AnggotaRombelQuery::create(null, $criteria);
        $query->joinWith('PesertaDidikRelatedByPesertaDidikId', $join_behavior);

        return $this->getAnggotaRombelsRelatedByRombonganBelajarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this RombonganBelajar is new, it will return
     * an empty collection; or if this RombonganBelajar has previously
     * been saved, it will retrieve related AnggotaRombelsRelatedByRombonganBelajarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in RombonganBelajar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|AnggotaRombel[] List of AnggotaRombel objects
     */
    public function getAnggotaRombelsRelatedByRombonganBelajarIdJoinJenisPendaftaranRelatedByJenisPendaftaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AnggotaRombelQuery::create(null, $criteria);
        $query->joinWith('JenisPendaftaranRelatedByJenisPendaftaranId', $join_behavior);

        return $this->getAnggotaRombelsRelatedByRombonganBelajarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this RombonganBelajar is new, it will return
     * an empty collection; or if this RombonganBelajar has previously
     * been saved, it will retrieve related AnggotaRombelsRelatedByRombonganBelajarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in RombonganBelajar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|AnggotaRombel[] List of AnggotaRombel objects
     */
    public function getAnggotaRombelsRelatedByRombonganBelajarIdJoinJenisPendaftaranRelatedByJenisPendaftaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AnggotaRombelQuery::create(null, $criteria);
        $query->joinWith('JenisPendaftaranRelatedByJenisPendaftaranId', $join_behavior);

        return $this->getAnggotaRombelsRelatedByRombonganBelajarId($query, $con);
    }

    /**
     * Clears out the collPembelajaransRelatedByRombonganBelajarId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return RombonganBelajar The current object (for fluent API support)
     * @see        addPembelajaransRelatedByRombonganBelajarId()
     */
    public function clearPembelajaransRelatedByRombonganBelajarId()
    {
        $this->collPembelajaransRelatedByRombonganBelajarId = null; // important to set this to null since that means it is uninitialized
        $this->collPembelajaransRelatedByRombonganBelajarIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPembelajaransRelatedByRombonganBelajarId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPembelajaransRelatedByRombonganBelajarId($v = true)
    {
        $this->collPembelajaransRelatedByRombonganBelajarIdPartial = $v;
    }

    /**
     * Initializes the collPembelajaransRelatedByRombonganBelajarId collection.
     *
     * By default this just sets the collPembelajaransRelatedByRombonganBelajarId collection to an empty array (like clearcollPembelajaransRelatedByRombonganBelajarId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPembelajaransRelatedByRombonganBelajarId($overrideExisting = true)
    {
        if (null !== $this->collPembelajaransRelatedByRombonganBelajarId && !$overrideExisting) {
            return;
        }
        $this->collPembelajaransRelatedByRombonganBelajarId = new PropelObjectCollection();
        $this->collPembelajaransRelatedByRombonganBelajarId->setModel('Pembelajaran');
    }

    /**
     * Gets an array of Pembelajaran objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this RombonganBelajar is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     * @throws PropelException
     */
    public function getPembelajaransRelatedByRombonganBelajarId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPembelajaransRelatedByRombonganBelajarIdPartial && !$this->isNew();
        if (null === $this->collPembelajaransRelatedByRombonganBelajarId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPembelajaransRelatedByRombonganBelajarId) {
                // return empty collection
                $this->initPembelajaransRelatedByRombonganBelajarId();
            } else {
                $collPembelajaransRelatedByRombonganBelajarId = PembelajaranQuery::create(null, $criteria)
                    ->filterByRombonganBelajarRelatedByRombonganBelajarId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPembelajaransRelatedByRombonganBelajarIdPartial && count($collPembelajaransRelatedByRombonganBelajarId)) {
                      $this->initPembelajaransRelatedByRombonganBelajarId(false);

                      foreach($collPembelajaransRelatedByRombonganBelajarId as $obj) {
                        if (false == $this->collPembelajaransRelatedByRombonganBelajarId->contains($obj)) {
                          $this->collPembelajaransRelatedByRombonganBelajarId->append($obj);
                        }
                      }

                      $this->collPembelajaransRelatedByRombonganBelajarIdPartial = true;
                    }

                    $collPembelajaransRelatedByRombonganBelajarId->getInternalIterator()->rewind();
                    return $collPembelajaransRelatedByRombonganBelajarId;
                }

                if($partial && $this->collPembelajaransRelatedByRombonganBelajarId) {
                    foreach($this->collPembelajaransRelatedByRombonganBelajarId as $obj) {
                        if($obj->isNew()) {
                            $collPembelajaransRelatedByRombonganBelajarId[] = $obj;
                        }
                    }
                }

                $this->collPembelajaransRelatedByRombonganBelajarId = $collPembelajaransRelatedByRombonganBelajarId;
                $this->collPembelajaransRelatedByRombonganBelajarIdPartial = false;
            }
        }

        return $this->collPembelajaransRelatedByRombonganBelajarId;
    }

    /**
     * Sets a collection of PembelajaranRelatedByRombonganBelajarId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $pembelajaransRelatedByRombonganBelajarId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setPembelajaransRelatedByRombonganBelajarId(PropelCollection $pembelajaransRelatedByRombonganBelajarId, PropelPDO $con = null)
    {
        $pembelajaransRelatedByRombonganBelajarIdToDelete = $this->getPembelajaransRelatedByRombonganBelajarId(new Criteria(), $con)->diff($pembelajaransRelatedByRombonganBelajarId);

        $this->pembelajaransRelatedByRombonganBelajarIdScheduledForDeletion = unserialize(serialize($pembelajaransRelatedByRombonganBelajarIdToDelete));

        foreach ($pembelajaransRelatedByRombonganBelajarIdToDelete as $pembelajaranRelatedByRombonganBelajarIdRemoved) {
            $pembelajaranRelatedByRombonganBelajarIdRemoved->setRombonganBelajarRelatedByRombonganBelajarId(null);
        }

        $this->collPembelajaransRelatedByRombonganBelajarId = null;
        foreach ($pembelajaransRelatedByRombonganBelajarId as $pembelajaranRelatedByRombonganBelajarId) {
            $this->addPembelajaranRelatedByRombonganBelajarId($pembelajaranRelatedByRombonganBelajarId);
        }

        $this->collPembelajaransRelatedByRombonganBelajarId = $pembelajaransRelatedByRombonganBelajarId;
        $this->collPembelajaransRelatedByRombonganBelajarIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Pembelajaran objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Pembelajaran objects.
     * @throws PropelException
     */
    public function countPembelajaransRelatedByRombonganBelajarId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPembelajaransRelatedByRombonganBelajarIdPartial && !$this->isNew();
        if (null === $this->collPembelajaransRelatedByRombonganBelajarId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPembelajaransRelatedByRombonganBelajarId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPembelajaransRelatedByRombonganBelajarId());
            }
            $query = PembelajaranQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByRombonganBelajarRelatedByRombonganBelajarId($this)
                ->count($con);
        }

        return count($this->collPembelajaransRelatedByRombonganBelajarId);
    }

    /**
     * Method called to associate a Pembelajaran object to this object
     * through the Pembelajaran foreign key attribute.
     *
     * @param    Pembelajaran $l Pembelajaran
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function addPembelajaranRelatedByRombonganBelajarId(Pembelajaran $l)
    {
        if ($this->collPembelajaransRelatedByRombonganBelajarId === null) {
            $this->initPembelajaransRelatedByRombonganBelajarId();
            $this->collPembelajaransRelatedByRombonganBelajarIdPartial = true;
        }
        if (!in_array($l, $this->collPembelajaransRelatedByRombonganBelajarId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPembelajaranRelatedByRombonganBelajarId($l);
        }

        return $this;
    }

    /**
     * @param	PembelajaranRelatedByRombonganBelajarId $pembelajaranRelatedByRombonganBelajarId The pembelajaranRelatedByRombonganBelajarId object to add.
     */
    protected function doAddPembelajaranRelatedByRombonganBelajarId($pembelajaranRelatedByRombonganBelajarId)
    {
        $this->collPembelajaransRelatedByRombonganBelajarId[]= $pembelajaranRelatedByRombonganBelajarId;
        $pembelajaranRelatedByRombonganBelajarId->setRombonganBelajarRelatedByRombonganBelajarId($this);
    }

    /**
     * @param	PembelajaranRelatedByRombonganBelajarId $pembelajaranRelatedByRombonganBelajarId The pembelajaranRelatedByRombonganBelajarId object to remove.
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function removePembelajaranRelatedByRombonganBelajarId($pembelajaranRelatedByRombonganBelajarId)
    {
        if ($this->getPembelajaransRelatedByRombonganBelajarId()->contains($pembelajaranRelatedByRombonganBelajarId)) {
            $this->collPembelajaransRelatedByRombonganBelajarId->remove($this->collPembelajaransRelatedByRombonganBelajarId->search($pembelajaranRelatedByRombonganBelajarId));
            if (null === $this->pembelajaransRelatedByRombonganBelajarIdScheduledForDeletion) {
                $this->pembelajaransRelatedByRombonganBelajarIdScheduledForDeletion = clone $this->collPembelajaransRelatedByRombonganBelajarId;
                $this->pembelajaransRelatedByRombonganBelajarIdScheduledForDeletion->clear();
            }
            $this->pembelajaransRelatedByRombonganBelajarIdScheduledForDeletion[]= clone $pembelajaranRelatedByRombonganBelajarId;
            $pembelajaranRelatedByRombonganBelajarId->setRombonganBelajarRelatedByRombonganBelajarId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this RombonganBelajar is new, it will return
     * an empty collection; or if this RombonganBelajar has previously
     * been saved, it will retrieve related PembelajaransRelatedByRombonganBelajarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in RombonganBelajar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     */
    public function getPembelajaransRelatedByRombonganBelajarIdJoinPtkTerdaftarRelatedByPtkTerdaftarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PembelajaranQuery::create(null, $criteria);
        $query->joinWith('PtkTerdaftarRelatedByPtkTerdaftarId', $join_behavior);

        return $this->getPembelajaransRelatedByRombonganBelajarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this RombonganBelajar is new, it will return
     * an empty collection; or if this RombonganBelajar has previously
     * been saved, it will retrieve related PembelajaransRelatedByRombonganBelajarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in RombonganBelajar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     */
    public function getPembelajaransRelatedByRombonganBelajarIdJoinPtkTerdaftarRelatedByPtkTerdaftarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PembelajaranQuery::create(null, $criteria);
        $query->joinWith('PtkTerdaftarRelatedByPtkTerdaftarId', $join_behavior);

        return $this->getPembelajaransRelatedByRombonganBelajarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this RombonganBelajar is new, it will return
     * an empty collection; or if this RombonganBelajar has previously
     * been saved, it will retrieve related PembelajaransRelatedByRombonganBelajarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in RombonganBelajar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     */
    public function getPembelajaransRelatedByRombonganBelajarIdJoinMataPelajaranRelatedByMataPelajaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PembelajaranQuery::create(null, $criteria);
        $query->joinWith('MataPelajaranRelatedByMataPelajaranId', $join_behavior);

        return $this->getPembelajaransRelatedByRombonganBelajarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this RombonganBelajar is new, it will return
     * an empty collection; or if this RombonganBelajar has previously
     * been saved, it will retrieve related PembelajaransRelatedByRombonganBelajarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in RombonganBelajar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     */
    public function getPembelajaransRelatedByRombonganBelajarIdJoinMataPelajaranRelatedByMataPelajaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PembelajaranQuery::create(null, $criteria);
        $query->joinWith('MataPelajaranRelatedByMataPelajaranId', $join_behavior);

        return $this->getPembelajaransRelatedByRombonganBelajarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this RombonganBelajar is new, it will return
     * an empty collection; or if this RombonganBelajar has previously
     * been saved, it will retrieve related PembelajaransRelatedByRombonganBelajarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in RombonganBelajar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     */
    public function getPembelajaransRelatedByRombonganBelajarIdJoinSemesterRelatedBySemesterId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PembelajaranQuery::create(null, $criteria);
        $query->joinWith('SemesterRelatedBySemesterId', $join_behavior);

        return $this->getPembelajaransRelatedByRombonganBelajarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this RombonganBelajar is new, it will return
     * an empty collection; or if this RombonganBelajar has previously
     * been saved, it will retrieve related PembelajaransRelatedByRombonganBelajarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in RombonganBelajar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     */
    public function getPembelajaransRelatedByRombonganBelajarIdJoinSemesterRelatedBySemesterId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PembelajaranQuery::create(null, $criteria);
        $query->joinWith('SemesterRelatedBySemesterId', $join_behavior);

        return $this->getPembelajaransRelatedByRombonganBelajarId($query, $con);
    }

    /**
     * Clears out the collPembelajaransRelatedByRombonganBelajarId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return RombonganBelajar The current object (for fluent API support)
     * @see        addPembelajaransRelatedByRombonganBelajarId()
     */
    public function clearPembelajaransRelatedByRombonganBelajarId()
    {
        $this->collPembelajaransRelatedByRombonganBelajarId = null; // important to set this to null since that means it is uninitialized
        $this->collPembelajaransRelatedByRombonganBelajarIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPembelajaransRelatedByRombonganBelajarId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPembelajaransRelatedByRombonganBelajarId($v = true)
    {
        $this->collPembelajaransRelatedByRombonganBelajarIdPartial = $v;
    }

    /**
     * Initializes the collPembelajaransRelatedByRombonganBelajarId collection.
     *
     * By default this just sets the collPembelajaransRelatedByRombonganBelajarId collection to an empty array (like clearcollPembelajaransRelatedByRombonganBelajarId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPembelajaransRelatedByRombonganBelajarId($overrideExisting = true)
    {
        if (null !== $this->collPembelajaransRelatedByRombonganBelajarId && !$overrideExisting) {
            return;
        }
        $this->collPembelajaransRelatedByRombonganBelajarId = new PropelObjectCollection();
        $this->collPembelajaransRelatedByRombonganBelajarId->setModel('Pembelajaran');
    }

    /**
     * Gets an array of Pembelajaran objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this RombonganBelajar is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     * @throws PropelException
     */
    public function getPembelajaransRelatedByRombonganBelajarId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPembelajaransRelatedByRombonganBelajarIdPartial && !$this->isNew();
        if (null === $this->collPembelajaransRelatedByRombonganBelajarId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPembelajaransRelatedByRombonganBelajarId) {
                // return empty collection
                $this->initPembelajaransRelatedByRombonganBelajarId();
            } else {
                $collPembelajaransRelatedByRombonganBelajarId = PembelajaranQuery::create(null, $criteria)
                    ->filterByRombonganBelajarRelatedByRombonganBelajarId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPembelajaransRelatedByRombonganBelajarIdPartial && count($collPembelajaransRelatedByRombonganBelajarId)) {
                      $this->initPembelajaransRelatedByRombonganBelajarId(false);

                      foreach($collPembelajaransRelatedByRombonganBelajarId as $obj) {
                        if (false == $this->collPembelajaransRelatedByRombonganBelajarId->contains($obj)) {
                          $this->collPembelajaransRelatedByRombonganBelajarId->append($obj);
                        }
                      }

                      $this->collPembelajaransRelatedByRombonganBelajarIdPartial = true;
                    }

                    $collPembelajaransRelatedByRombonganBelajarId->getInternalIterator()->rewind();
                    return $collPembelajaransRelatedByRombonganBelajarId;
                }

                if($partial && $this->collPembelajaransRelatedByRombonganBelajarId) {
                    foreach($this->collPembelajaransRelatedByRombonganBelajarId as $obj) {
                        if($obj->isNew()) {
                            $collPembelajaransRelatedByRombonganBelajarId[] = $obj;
                        }
                    }
                }

                $this->collPembelajaransRelatedByRombonganBelajarId = $collPembelajaransRelatedByRombonganBelajarId;
                $this->collPembelajaransRelatedByRombonganBelajarIdPartial = false;
            }
        }

        return $this->collPembelajaransRelatedByRombonganBelajarId;
    }

    /**
     * Sets a collection of PembelajaranRelatedByRombonganBelajarId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $pembelajaransRelatedByRombonganBelajarId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setPembelajaransRelatedByRombonganBelajarId(PropelCollection $pembelajaransRelatedByRombonganBelajarId, PropelPDO $con = null)
    {
        $pembelajaransRelatedByRombonganBelajarIdToDelete = $this->getPembelajaransRelatedByRombonganBelajarId(new Criteria(), $con)->diff($pembelajaransRelatedByRombonganBelajarId);

        $this->pembelajaransRelatedByRombonganBelajarIdScheduledForDeletion = unserialize(serialize($pembelajaransRelatedByRombonganBelajarIdToDelete));

        foreach ($pembelajaransRelatedByRombonganBelajarIdToDelete as $pembelajaranRelatedByRombonganBelajarIdRemoved) {
            $pembelajaranRelatedByRombonganBelajarIdRemoved->setRombonganBelajarRelatedByRombonganBelajarId(null);
        }

        $this->collPembelajaransRelatedByRombonganBelajarId = null;
        foreach ($pembelajaransRelatedByRombonganBelajarId as $pembelajaranRelatedByRombonganBelajarId) {
            $this->addPembelajaranRelatedByRombonganBelajarId($pembelajaranRelatedByRombonganBelajarId);
        }

        $this->collPembelajaransRelatedByRombonganBelajarId = $pembelajaransRelatedByRombonganBelajarId;
        $this->collPembelajaransRelatedByRombonganBelajarIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Pembelajaran objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Pembelajaran objects.
     * @throws PropelException
     */
    public function countPembelajaransRelatedByRombonganBelajarId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPembelajaransRelatedByRombonganBelajarIdPartial && !$this->isNew();
        if (null === $this->collPembelajaransRelatedByRombonganBelajarId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPembelajaransRelatedByRombonganBelajarId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPembelajaransRelatedByRombonganBelajarId());
            }
            $query = PembelajaranQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByRombonganBelajarRelatedByRombonganBelajarId($this)
                ->count($con);
        }

        return count($this->collPembelajaransRelatedByRombonganBelajarId);
    }

    /**
     * Method called to associate a Pembelajaran object to this object
     * through the Pembelajaran foreign key attribute.
     *
     * @param    Pembelajaran $l Pembelajaran
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function addPembelajaranRelatedByRombonganBelajarId(Pembelajaran $l)
    {
        if ($this->collPembelajaransRelatedByRombonganBelajarId === null) {
            $this->initPembelajaransRelatedByRombonganBelajarId();
            $this->collPembelajaransRelatedByRombonganBelajarIdPartial = true;
        }
        if (!in_array($l, $this->collPembelajaransRelatedByRombonganBelajarId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPembelajaranRelatedByRombonganBelajarId($l);
        }

        return $this;
    }

    /**
     * @param	PembelajaranRelatedByRombonganBelajarId $pembelajaranRelatedByRombonganBelajarId The pembelajaranRelatedByRombonganBelajarId object to add.
     */
    protected function doAddPembelajaranRelatedByRombonganBelajarId($pembelajaranRelatedByRombonganBelajarId)
    {
        $this->collPembelajaransRelatedByRombonganBelajarId[]= $pembelajaranRelatedByRombonganBelajarId;
        $pembelajaranRelatedByRombonganBelajarId->setRombonganBelajarRelatedByRombonganBelajarId($this);
    }

    /**
     * @param	PembelajaranRelatedByRombonganBelajarId $pembelajaranRelatedByRombonganBelajarId The pembelajaranRelatedByRombonganBelajarId object to remove.
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function removePembelajaranRelatedByRombonganBelajarId($pembelajaranRelatedByRombonganBelajarId)
    {
        if ($this->getPembelajaransRelatedByRombonganBelajarId()->contains($pembelajaranRelatedByRombonganBelajarId)) {
            $this->collPembelajaransRelatedByRombonganBelajarId->remove($this->collPembelajaransRelatedByRombonganBelajarId->search($pembelajaranRelatedByRombonganBelajarId));
            if (null === $this->pembelajaransRelatedByRombonganBelajarIdScheduledForDeletion) {
                $this->pembelajaransRelatedByRombonganBelajarIdScheduledForDeletion = clone $this->collPembelajaransRelatedByRombonganBelajarId;
                $this->pembelajaransRelatedByRombonganBelajarIdScheduledForDeletion->clear();
            }
            $this->pembelajaransRelatedByRombonganBelajarIdScheduledForDeletion[]= clone $pembelajaranRelatedByRombonganBelajarId;
            $pembelajaranRelatedByRombonganBelajarId->setRombonganBelajarRelatedByRombonganBelajarId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this RombonganBelajar is new, it will return
     * an empty collection; or if this RombonganBelajar has previously
     * been saved, it will retrieve related PembelajaransRelatedByRombonganBelajarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in RombonganBelajar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     */
    public function getPembelajaransRelatedByRombonganBelajarIdJoinPtkTerdaftarRelatedByPtkTerdaftarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PembelajaranQuery::create(null, $criteria);
        $query->joinWith('PtkTerdaftarRelatedByPtkTerdaftarId', $join_behavior);

        return $this->getPembelajaransRelatedByRombonganBelajarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this RombonganBelajar is new, it will return
     * an empty collection; or if this RombonganBelajar has previously
     * been saved, it will retrieve related PembelajaransRelatedByRombonganBelajarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in RombonganBelajar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     */
    public function getPembelajaransRelatedByRombonganBelajarIdJoinPtkTerdaftarRelatedByPtkTerdaftarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PembelajaranQuery::create(null, $criteria);
        $query->joinWith('PtkTerdaftarRelatedByPtkTerdaftarId', $join_behavior);

        return $this->getPembelajaransRelatedByRombonganBelajarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this RombonganBelajar is new, it will return
     * an empty collection; or if this RombonganBelajar has previously
     * been saved, it will retrieve related PembelajaransRelatedByRombonganBelajarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in RombonganBelajar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     */
    public function getPembelajaransRelatedByRombonganBelajarIdJoinMataPelajaranRelatedByMataPelajaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PembelajaranQuery::create(null, $criteria);
        $query->joinWith('MataPelajaranRelatedByMataPelajaranId', $join_behavior);

        return $this->getPembelajaransRelatedByRombonganBelajarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this RombonganBelajar is new, it will return
     * an empty collection; or if this RombonganBelajar has previously
     * been saved, it will retrieve related PembelajaransRelatedByRombonganBelajarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in RombonganBelajar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     */
    public function getPembelajaransRelatedByRombonganBelajarIdJoinMataPelajaranRelatedByMataPelajaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PembelajaranQuery::create(null, $criteria);
        $query->joinWith('MataPelajaranRelatedByMataPelajaranId', $join_behavior);

        return $this->getPembelajaransRelatedByRombonganBelajarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this RombonganBelajar is new, it will return
     * an empty collection; or if this RombonganBelajar has previously
     * been saved, it will retrieve related PembelajaransRelatedByRombonganBelajarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in RombonganBelajar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     */
    public function getPembelajaransRelatedByRombonganBelajarIdJoinSemesterRelatedBySemesterId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PembelajaranQuery::create(null, $criteria);
        $query->joinWith('SemesterRelatedBySemesterId', $join_behavior);

        return $this->getPembelajaransRelatedByRombonganBelajarId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this RombonganBelajar is new, it will return
     * an empty collection; or if this RombonganBelajar has previously
     * been saved, it will retrieve related PembelajaransRelatedByRombonganBelajarId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in RombonganBelajar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     */
    public function getPembelajaransRelatedByRombonganBelajarIdJoinSemesterRelatedBySemesterId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PembelajaranQuery::create(null, $criteria);
        $query->joinWith('SemesterRelatedBySemesterId', $join_behavior);

        return $this->getPembelajaransRelatedByRombonganBelajarId($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->rombongan_belajar_id = null;
        $this->semester_id = null;
        $this->sekolah_id = null;
        $this->tingkat_pendidikan_id = null;
        $this->jurusan_sp_id = null;
        $this->kurikulum_id = null;
        $this->nama = null;
        $this->ptk_id = null;
        $this->prasarana_id = null;
        $this->moving_class = null;
        $this->jenis_rombel = null;
        $this->sks = null;
        $this->kebutuhan_khusus_id = null;
        $this->last_update = null;
        $this->soft_delete = null;
        $this->last_sync = null;
        $this->updater_id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collVldRombelsRelatedByRombonganBelajarId) {
                foreach ($this->collVldRombelsRelatedByRombonganBelajarId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldRombelsRelatedByRombonganBelajarId) {
                foreach ($this->collVldRombelsRelatedByRombonganBelajarId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collAnggotaRombelsRelatedByRombonganBelajarId) {
                foreach ($this->collAnggotaRombelsRelatedByRombonganBelajarId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collAnggotaRombelsRelatedByRombonganBelajarId) {
                foreach ($this->collAnggotaRombelsRelatedByRombonganBelajarId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPembelajaransRelatedByRombonganBelajarId) {
                foreach ($this->collPembelajaransRelatedByRombonganBelajarId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPembelajaransRelatedByRombonganBelajarId) {
                foreach ($this->collPembelajaransRelatedByRombonganBelajarId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aJurusanSpRelatedByJurusanSpId instanceof Persistent) {
              $this->aJurusanSpRelatedByJurusanSpId->clearAllReferences($deep);
            }
            if ($this->aJurusanSpRelatedByJurusanSpId instanceof Persistent) {
              $this->aJurusanSpRelatedByJurusanSpId->clearAllReferences($deep);
            }
            if ($this->aPrasaranaRelatedByPrasaranaId instanceof Persistent) {
              $this->aPrasaranaRelatedByPrasaranaId->clearAllReferences($deep);
            }
            if ($this->aPrasaranaRelatedByPrasaranaId instanceof Persistent) {
              $this->aPrasaranaRelatedByPrasaranaId->clearAllReferences($deep);
            }
            if ($this->aPtkRelatedByPtkId instanceof Persistent) {
              $this->aPtkRelatedByPtkId->clearAllReferences($deep);
            }
            if ($this->aPtkRelatedByPtkId instanceof Persistent) {
              $this->aPtkRelatedByPtkId->clearAllReferences($deep);
            }
            if ($this->aSekolahRelatedBySekolahId instanceof Persistent) {
              $this->aSekolahRelatedBySekolahId->clearAllReferences($deep);
            }
            if ($this->aSekolahRelatedBySekolahId instanceof Persistent) {
              $this->aSekolahRelatedBySekolahId->clearAllReferences($deep);
            }
            if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId instanceof Persistent) {
              $this->aKebutuhanKhususRelatedByKebutuhanKhususId->clearAllReferences($deep);
            }
            if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId instanceof Persistent) {
              $this->aKebutuhanKhususRelatedByKebutuhanKhususId->clearAllReferences($deep);
            }
            if ($this->aKurikulumRelatedByKurikulumId instanceof Persistent) {
              $this->aKurikulumRelatedByKurikulumId->clearAllReferences($deep);
            }
            if ($this->aKurikulumRelatedByKurikulumId instanceof Persistent) {
              $this->aKurikulumRelatedByKurikulumId->clearAllReferences($deep);
            }
            if ($this->aSemesterRelatedBySemesterId instanceof Persistent) {
              $this->aSemesterRelatedBySemesterId->clearAllReferences($deep);
            }
            if ($this->aSemesterRelatedBySemesterId instanceof Persistent) {
              $this->aSemesterRelatedBySemesterId->clearAllReferences($deep);
            }
            if ($this->aTingkatPendidikanRelatedByTingkatPendidikanId instanceof Persistent) {
              $this->aTingkatPendidikanRelatedByTingkatPendidikanId->clearAllReferences($deep);
            }
            if ($this->aTingkatPendidikanRelatedByTingkatPendidikanId instanceof Persistent) {
              $this->aTingkatPendidikanRelatedByTingkatPendidikanId->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collVldRombelsRelatedByRombonganBelajarId instanceof PropelCollection) {
            $this->collVldRombelsRelatedByRombonganBelajarId->clearIterator();
        }
        $this->collVldRombelsRelatedByRombonganBelajarId = null;
        if ($this->collVldRombelsRelatedByRombonganBelajarId instanceof PropelCollection) {
            $this->collVldRombelsRelatedByRombonganBelajarId->clearIterator();
        }
        $this->collVldRombelsRelatedByRombonganBelajarId = null;
        if ($this->collAnggotaRombelsRelatedByRombonganBelajarId instanceof PropelCollection) {
            $this->collAnggotaRombelsRelatedByRombonganBelajarId->clearIterator();
        }
        $this->collAnggotaRombelsRelatedByRombonganBelajarId = null;
        if ($this->collAnggotaRombelsRelatedByRombonganBelajarId instanceof PropelCollection) {
            $this->collAnggotaRombelsRelatedByRombonganBelajarId->clearIterator();
        }
        $this->collAnggotaRombelsRelatedByRombonganBelajarId = null;
        if ($this->collPembelajaransRelatedByRombonganBelajarId instanceof PropelCollection) {
            $this->collPembelajaransRelatedByRombonganBelajarId->clearIterator();
        }
        $this->collPembelajaransRelatedByRombonganBelajarId = null;
        if ($this->collPembelajaransRelatedByRombonganBelajarId instanceof PropelCollection) {
            $this->collPembelajaransRelatedByRombonganBelajarId->clearIterator();
        }
        $this->collPembelajaransRelatedByRombonganBelajarId = null;
        $this->aJurusanSpRelatedByJurusanSpId = null;
        $this->aJurusanSpRelatedByJurusanSpId = null;
        $this->aPrasaranaRelatedByPrasaranaId = null;
        $this->aPrasaranaRelatedByPrasaranaId = null;
        $this->aPtkRelatedByPtkId = null;
        $this->aPtkRelatedByPtkId = null;
        $this->aSekolahRelatedBySekolahId = null;
        $this->aSekolahRelatedBySekolahId = null;
        $this->aKebutuhanKhususRelatedByKebutuhanKhususId = null;
        $this->aKebutuhanKhususRelatedByKebutuhanKhususId = null;
        $this->aKurikulumRelatedByKurikulumId = null;
        $this->aKurikulumRelatedByKurikulumId = null;
        $this->aSemesterRelatedBySemesterId = null;
        $this->aSemesterRelatedBySemesterId = null;
        $this->aTingkatPendidikanRelatedByTingkatPendidikanId = null;
        $this->aTingkatPendidikanRelatedByTingkatPendidikanId = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(RombonganBelajarPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
