<?php

namespace angulex\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelDateTime;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\BukuPtk;
use angulex\Model\BukuPtkPeer;
use angulex\Model\BukuPtkQuery;
use angulex\Model\Ptk;
use angulex\Model\PtkQuery;
use angulex\Model\VldBukuPtk;
use angulex\Model\VldBukuPtkQuery;

/**
 * Base class that represents a row from the 'buku_ptk' table.
 *
 * 
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseBukuPtk extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'angulex\\Model\\BukuPtkPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        BukuPtkPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the buku_id field.
     * @var        string
     */
    protected $buku_id;

    /**
     * The value for the ptk_id field.
     * @var        string
     */
    protected $ptk_id;

    /**
     * The value for the judul_buku field.
     * @var        string
     */
    protected $judul_buku;

    /**
     * The value for the tahun field.
     * @var        string
     */
    protected $tahun;

    /**
     * The value for the penerbit field.
     * @var        string
     */
    protected $penerbit;

    /**
     * The value for the last_update field.
     * @var        string
     */
    protected $last_update;

    /**
     * The value for the soft_delete field.
     * @var        string
     */
    protected $soft_delete;

    /**
     * The value for the last_sync field.
     * @var        string
     */
    protected $last_sync;

    /**
     * The value for the updater_id field.
     * @var        string
     */
    protected $updater_id;

    /**
     * @var        Ptk
     */
    protected $aPtkRelatedByPtkId;

    /**
     * @var        Ptk
     */
    protected $aPtkRelatedByPtkId;

    /**
     * @var        PropelObjectCollection|VldBukuPtk[] Collection to store aggregation of VldBukuPtk objects.
     */
    protected $collVldBukuPtksRelatedByBukuId;
    protected $collVldBukuPtksRelatedByBukuIdPartial;

    /**
     * @var        PropelObjectCollection|VldBukuPtk[] Collection to store aggregation of VldBukuPtk objects.
     */
    protected $collVldBukuPtksRelatedByBukuId;
    protected $collVldBukuPtksRelatedByBukuIdPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldBukuPtksRelatedByBukuIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldBukuPtksRelatedByBukuIdScheduledForDeletion = null;

    /**
     * Get the [buku_id] column value.
     * 
     * @return string
     */
    public function getBukuId()
    {
        return $this->buku_id;
    }

    /**
     * Get the [ptk_id] column value.
     * 
     * @return string
     */
    public function getPtkId()
    {
        return $this->ptk_id;
    }

    /**
     * Get the [judul_buku] column value.
     * 
     * @return string
     */
    public function getJudulBuku()
    {
        return $this->judul_buku;
    }

    /**
     * Get the [tahun] column value.
     * 
     * @return string
     */
    public function getTahun()
    {
        return $this->tahun;
    }

    /**
     * Get the [penerbit] column value.
     * 
     * @return string
     */
    public function getPenerbit()
    {
        return $this->penerbit;
    }

    /**
     * Get the [optionally formatted] temporal [last_update] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastUpdate($format = 'Y-m-d H:i:s')
    {
        if ($this->last_update === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_update);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_update, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [soft_delete] column value.
     * 
     * @return string
     */
    public function getSoftDelete()
    {
        return $this->soft_delete;
    }

    /**
     * Get the [optionally formatted] temporal [last_sync] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastSync($format = 'Y-m-d H:i:s')
    {
        if ($this->last_sync === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_sync);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_sync, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [updater_id] column value.
     * 
     * @return string
     */
    public function getUpdaterId()
    {
        return $this->updater_id;
    }

    /**
     * Set the value of [buku_id] column.
     * 
     * @param string $v new value
     * @return BukuPtk The current object (for fluent API support)
     */
    public function setBukuId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->buku_id !== $v) {
            $this->buku_id = $v;
            $this->modifiedColumns[] = BukuPtkPeer::BUKU_ID;
        }


        return $this;
    } // setBukuId()

    /**
     * Set the value of [ptk_id] column.
     * 
     * @param string $v new value
     * @return BukuPtk The current object (for fluent API support)
     */
    public function setPtkId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->ptk_id !== $v) {
            $this->ptk_id = $v;
            $this->modifiedColumns[] = BukuPtkPeer::PTK_ID;
        }

        if ($this->aPtkRelatedByPtkId !== null && $this->aPtkRelatedByPtkId->getPtkId() !== $v) {
            $this->aPtkRelatedByPtkId = null;
        }

        if ($this->aPtkRelatedByPtkId !== null && $this->aPtkRelatedByPtkId->getPtkId() !== $v) {
            $this->aPtkRelatedByPtkId = null;
        }


        return $this;
    } // setPtkId()

    /**
     * Set the value of [judul_buku] column.
     * 
     * @param string $v new value
     * @return BukuPtk The current object (for fluent API support)
     */
    public function setJudulBuku($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->judul_buku !== $v) {
            $this->judul_buku = $v;
            $this->modifiedColumns[] = BukuPtkPeer::JUDUL_BUKU;
        }


        return $this;
    } // setJudulBuku()

    /**
     * Set the value of [tahun] column.
     * 
     * @param string $v new value
     * @return BukuPtk The current object (for fluent API support)
     */
    public function setTahun($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tahun !== $v) {
            $this->tahun = $v;
            $this->modifiedColumns[] = BukuPtkPeer::TAHUN;
        }


        return $this;
    } // setTahun()

    /**
     * Set the value of [penerbit] column.
     * 
     * @param string $v new value
     * @return BukuPtk The current object (for fluent API support)
     */
    public function setPenerbit($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->penerbit !== $v) {
            $this->penerbit = $v;
            $this->modifiedColumns[] = BukuPtkPeer::PENERBIT;
        }


        return $this;
    } // setPenerbit()

    /**
     * Sets the value of [last_update] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return BukuPtk The current object (for fluent API support)
     */
    public function setLastUpdate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_update !== null || $dt !== null) {
            $currentDateAsString = ($this->last_update !== null && $tmpDt = new DateTime($this->last_update)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_update = $newDateAsString;
                $this->modifiedColumns[] = BukuPtkPeer::LAST_UPDATE;
            }
        } // if either are not null


        return $this;
    } // setLastUpdate()

    /**
     * Set the value of [soft_delete] column.
     * 
     * @param string $v new value
     * @return BukuPtk The current object (for fluent API support)
     */
    public function setSoftDelete($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->soft_delete !== $v) {
            $this->soft_delete = $v;
            $this->modifiedColumns[] = BukuPtkPeer::SOFT_DELETE;
        }


        return $this;
    } // setSoftDelete()

    /**
     * Sets the value of [last_sync] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return BukuPtk The current object (for fluent API support)
     */
    public function setLastSync($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_sync !== null || $dt !== null) {
            $currentDateAsString = ($this->last_sync !== null && $tmpDt = new DateTime($this->last_sync)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_sync = $newDateAsString;
                $this->modifiedColumns[] = BukuPtkPeer::LAST_SYNC;
            }
        } // if either are not null


        return $this;
    } // setLastSync()

    /**
     * Set the value of [updater_id] column.
     * 
     * @param string $v new value
     * @return BukuPtk The current object (for fluent API support)
     */
    public function setUpdaterId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->updater_id !== $v) {
            $this->updater_id = $v;
            $this->modifiedColumns[] = BukuPtkPeer::UPDATER_ID;
        }


        return $this;
    } // setUpdaterId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->buku_id = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
            $this->ptk_id = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->judul_buku = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->tahun = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->penerbit = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->last_update = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->soft_delete = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->last_sync = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->updater_id = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 9; // 9 = BukuPtkPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating BukuPtk object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aPtkRelatedByPtkId !== null && $this->ptk_id !== $this->aPtkRelatedByPtkId->getPtkId()) {
            $this->aPtkRelatedByPtkId = null;
        }
        if ($this->aPtkRelatedByPtkId !== null && $this->ptk_id !== $this->aPtkRelatedByPtkId->getPtkId()) {
            $this->aPtkRelatedByPtkId = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(BukuPtkPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = BukuPtkPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aPtkRelatedByPtkId = null;
            $this->aPtkRelatedByPtkId = null;
            $this->collVldBukuPtksRelatedByBukuId = null;

            $this->collVldBukuPtksRelatedByBukuId = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(BukuPtkPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = BukuPtkQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(BukuPtkPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                BukuPtkPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPtkRelatedByPtkId !== null) {
                if ($this->aPtkRelatedByPtkId->isModified() || $this->aPtkRelatedByPtkId->isNew()) {
                    $affectedRows += $this->aPtkRelatedByPtkId->save($con);
                }
                $this->setPtkRelatedByPtkId($this->aPtkRelatedByPtkId);
            }

            if ($this->aPtkRelatedByPtkId !== null) {
                if ($this->aPtkRelatedByPtkId->isModified() || $this->aPtkRelatedByPtkId->isNew()) {
                    $affectedRows += $this->aPtkRelatedByPtkId->save($con);
                }
                $this->setPtkRelatedByPtkId($this->aPtkRelatedByPtkId);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->vldBukuPtksRelatedByBukuIdScheduledForDeletion !== null) {
                if (!$this->vldBukuPtksRelatedByBukuIdScheduledForDeletion->isEmpty()) {
                    VldBukuPtkQuery::create()
                        ->filterByPrimaryKeys($this->vldBukuPtksRelatedByBukuIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldBukuPtksRelatedByBukuIdScheduledForDeletion = null;
                }
            }

            if ($this->collVldBukuPtksRelatedByBukuId !== null) {
                foreach ($this->collVldBukuPtksRelatedByBukuId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldBukuPtksRelatedByBukuIdScheduledForDeletion !== null) {
                if (!$this->vldBukuPtksRelatedByBukuIdScheduledForDeletion->isEmpty()) {
                    VldBukuPtkQuery::create()
                        ->filterByPrimaryKeys($this->vldBukuPtksRelatedByBukuIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldBukuPtksRelatedByBukuIdScheduledForDeletion = null;
                }
            }

            if ($this->collVldBukuPtksRelatedByBukuId !== null) {
                foreach ($this->collVldBukuPtksRelatedByBukuId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $criteria = $this->buildCriteria();
        $pk = BasePeer::doInsert($criteria, $con);
        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPtkRelatedByPtkId !== null) {
                if (!$this->aPtkRelatedByPtkId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPtkRelatedByPtkId->getValidationFailures());
                }
            }

            if ($this->aPtkRelatedByPtkId !== null) {
                if (!$this->aPtkRelatedByPtkId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPtkRelatedByPtkId->getValidationFailures());
                }
            }


            if (($retval = BukuPtkPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collVldBukuPtksRelatedByBukuId !== null) {
                    foreach ($this->collVldBukuPtksRelatedByBukuId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldBukuPtksRelatedByBukuId !== null) {
                    foreach ($this->collVldBukuPtksRelatedByBukuId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = BukuPtkPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getBukuId();
                break;
            case 1:
                return $this->getPtkId();
                break;
            case 2:
                return $this->getJudulBuku();
                break;
            case 3:
                return $this->getTahun();
                break;
            case 4:
                return $this->getPenerbit();
                break;
            case 5:
                return $this->getLastUpdate();
                break;
            case 6:
                return $this->getSoftDelete();
                break;
            case 7:
                return $this->getLastSync();
                break;
            case 8:
                return $this->getUpdaterId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['BukuPtk'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['BukuPtk'][$this->getPrimaryKey()] = true;
        $keys = BukuPtkPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getBukuId(),
            $keys[1] => $this->getPtkId(),
            $keys[2] => $this->getJudulBuku(),
            $keys[3] => $this->getTahun(),
            $keys[4] => $this->getPenerbit(),
            $keys[5] => $this->getLastUpdate(),
            $keys[6] => $this->getSoftDelete(),
            $keys[7] => $this->getLastSync(),
            $keys[8] => $this->getUpdaterId(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aPtkRelatedByPtkId) {
                $result['PtkRelatedByPtkId'] = $this->aPtkRelatedByPtkId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPtkRelatedByPtkId) {
                $result['PtkRelatedByPtkId'] = $this->aPtkRelatedByPtkId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collVldBukuPtksRelatedByBukuId) {
                $result['VldBukuPtksRelatedByBukuId'] = $this->collVldBukuPtksRelatedByBukuId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldBukuPtksRelatedByBukuId) {
                $result['VldBukuPtksRelatedByBukuId'] = $this->collVldBukuPtksRelatedByBukuId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = BukuPtkPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setBukuId($value);
                break;
            case 1:
                $this->setPtkId($value);
                break;
            case 2:
                $this->setJudulBuku($value);
                break;
            case 3:
                $this->setTahun($value);
                break;
            case 4:
                $this->setPenerbit($value);
                break;
            case 5:
                $this->setLastUpdate($value);
                break;
            case 6:
                $this->setSoftDelete($value);
                break;
            case 7:
                $this->setLastSync($value);
                break;
            case 8:
                $this->setUpdaterId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = BukuPtkPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setBukuId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setPtkId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setJudulBuku($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setTahun($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setPenerbit($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setLastUpdate($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setSoftDelete($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setLastSync($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setUpdaterId($arr[$keys[8]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(BukuPtkPeer::DATABASE_NAME);

        if ($this->isColumnModified(BukuPtkPeer::BUKU_ID)) $criteria->add(BukuPtkPeer::BUKU_ID, $this->buku_id);
        if ($this->isColumnModified(BukuPtkPeer::PTK_ID)) $criteria->add(BukuPtkPeer::PTK_ID, $this->ptk_id);
        if ($this->isColumnModified(BukuPtkPeer::JUDUL_BUKU)) $criteria->add(BukuPtkPeer::JUDUL_BUKU, $this->judul_buku);
        if ($this->isColumnModified(BukuPtkPeer::TAHUN)) $criteria->add(BukuPtkPeer::TAHUN, $this->tahun);
        if ($this->isColumnModified(BukuPtkPeer::PENERBIT)) $criteria->add(BukuPtkPeer::PENERBIT, $this->penerbit);
        if ($this->isColumnModified(BukuPtkPeer::LAST_UPDATE)) $criteria->add(BukuPtkPeer::LAST_UPDATE, $this->last_update);
        if ($this->isColumnModified(BukuPtkPeer::SOFT_DELETE)) $criteria->add(BukuPtkPeer::SOFT_DELETE, $this->soft_delete);
        if ($this->isColumnModified(BukuPtkPeer::LAST_SYNC)) $criteria->add(BukuPtkPeer::LAST_SYNC, $this->last_sync);
        if ($this->isColumnModified(BukuPtkPeer::UPDATER_ID)) $criteria->add(BukuPtkPeer::UPDATER_ID, $this->updater_id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(BukuPtkPeer::DATABASE_NAME);
        $criteria->add(BukuPtkPeer::BUKU_ID, $this->buku_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getBukuId();
    }

    /**
     * Generic method to set the primary key (buku_id column).
     *
     * @param  string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setBukuId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getBukuId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of BukuPtk (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setPtkId($this->getPtkId());
        $copyObj->setJudulBuku($this->getJudulBuku());
        $copyObj->setTahun($this->getTahun());
        $copyObj->setPenerbit($this->getPenerbit());
        $copyObj->setLastUpdate($this->getLastUpdate());
        $copyObj->setSoftDelete($this->getSoftDelete());
        $copyObj->setLastSync($this->getLastSync());
        $copyObj->setUpdaterId($this->getUpdaterId());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getVldBukuPtksRelatedByBukuId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldBukuPtkRelatedByBukuId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldBukuPtksRelatedByBukuId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldBukuPtkRelatedByBukuId($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setBukuId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return BukuPtk Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return BukuPtkPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new BukuPtkPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a Ptk object.
     *
     * @param             Ptk $v
     * @return BukuPtk The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPtkRelatedByPtkId(Ptk $v = null)
    {
        if ($v === null) {
            $this->setPtkId(NULL);
        } else {
            $this->setPtkId($v->getPtkId());
        }

        $this->aPtkRelatedByPtkId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Ptk object, it will not be re-added.
        if ($v !== null) {
            $v->addBukuPtkRelatedByPtkId($this);
        }


        return $this;
    }


    /**
     * Get the associated Ptk object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Ptk The associated Ptk object.
     * @throws PropelException
     */
    public function getPtkRelatedByPtkId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPtkRelatedByPtkId === null && (($this->ptk_id !== "" && $this->ptk_id !== null)) && $doQuery) {
            $this->aPtkRelatedByPtkId = PtkQuery::create()->findPk($this->ptk_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPtkRelatedByPtkId->addBukuPtksRelatedByPtkId($this);
             */
        }

        return $this->aPtkRelatedByPtkId;
    }

    /**
     * Declares an association between this object and a Ptk object.
     *
     * @param             Ptk $v
     * @return BukuPtk The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPtkRelatedByPtkId(Ptk $v = null)
    {
        if ($v === null) {
            $this->setPtkId(NULL);
        } else {
            $this->setPtkId($v->getPtkId());
        }

        $this->aPtkRelatedByPtkId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Ptk object, it will not be re-added.
        if ($v !== null) {
            $v->addBukuPtkRelatedByPtkId($this);
        }


        return $this;
    }


    /**
     * Get the associated Ptk object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Ptk The associated Ptk object.
     * @throws PropelException
     */
    public function getPtkRelatedByPtkId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPtkRelatedByPtkId === null && (($this->ptk_id !== "" && $this->ptk_id !== null)) && $doQuery) {
            $this->aPtkRelatedByPtkId = PtkQuery::create()->findPk($this->ptk_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPtkRelatedByPtkId->addBukuPtksRelatedByPtkId($this);
             */
        }

        return $this->aPtkRelatedByPtkId;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('VldBukuPtkRelatedByBukuId' == $relationName) {
            $this->initVldBukuPtksRelatedByBukuId();
        }
        if ('VldBukuPtkRelatedByBukuId' == $relationName) {
            $this->initVldBukuPtksRelatedByBukuId();
        }
    }

    /**
     * Clears out the collVldBukuPtksRelatedByBukuId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return BukuPtk The current object (for fluent API support)
     * @see        addVldBukuPtksRelatedByBukuId()
     */
    public function clearVldBukuPtksRelatedByBukuId()
    {
        $this->collVldBukuPtksRelatedByBukuId = null; // important to set this to null since that means it is uninitialized
        $this->collVldBukuPtksRelatedByBukuIdPartial = null;

        return $this;
    }

    /**
     * reset is the collVldBukuPtksRelatedByBukuId collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldBukuPtksRelatedByBukuId($v = true)
    {
        $this->collVldBukuPtksRelatedByBukuIdPartial = $v;
    }

    /**
     * Initializes the collVldBukuPtksRelatedByBukuId collection.
     *
     * By default this just sets the collVldBukuPtksRelatedByBukuId collection to an empty array (like clearcollVldBukuPtksRelatedByBukuId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldBukuPtksRelatedByBukuId($overrideExisting = true)
    {
        if (null !== $this->collVldBukuPtksRelatedByBukuId && !$overrideExisting) {
            return;
        }
        $this->collVldBukuPtksRelatedByBukuId = new PropelObjectCollection();
        $this->collVldBukuPtksRelatedByBukuId->setModel('VldBukuPtk');
    }

    /**
     * Gets an array of VldBukuPtk objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this BukuPtk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldBukuPtk[] List of VldBukuPtk objects
     * @throws PropelException
     */
    public function getVldBukuPtksRelatedByBukuId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldBukuPtksRelatedByBukuIdPartial && !$this->isNew();
        if (null === $this->collVldBukuPtksRelatedByBukuId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldBukuPtksRelatedByBukuId) {
                // return empty collection
                $this->initVldBukuPtksRelatedByBukuId();
            } else {
                $collVldBukuPtksRelatedByBukuId = VldBukuPtkQuery::create(null, $criteria)
                    ->filterByBukuPtkRelatedByBukuId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldBukuPtksRelatedByBukuIdPartial && count($collVldBukuPtksRelatedByBukuId)) {
                      $this->initVldBukuPtksRelatedByBukuId(false);

                      foreach($collVldBukuPtksRelatedByBukuId as $obj) {
                        if (false == $this->collVldBukuPtksRelatedByBukuId->contains($obj)) {
                          $this->collVldBukuPtksRelatedByBukuId->append($obj);
                        }
                      }

                      $this->collVldBukuPtksRelatedByBukuIdPartial = true;
                    }

                    $collVldBukuPtksRelatedByBukuId->getInternalIterator()->rewind();
                    return $collVldBukuPtksRelatedByBukuId;
                }

                if($partial && $this->collVldBukuPtksRelatedByBukuId) {
                    foreach($this->collVldBukuPtksRelatedByBukuId as $obj) {
                        if($obj->isNew()) {
                            $collVldBukuPtksRelatedByBukuId[] = $obj;
                        }
                    }
                }

                $this->collVldBukuPtksRelatedByBukuId = $collVldBukuPtksRelatedByBukuId;
                $this->collVldBukuPtksRelatedByBukuIdPartial = false;
            }
        }

        return $this->collVldBukuPtksRelatedByBukuId;
    }

    /**
     * Sets a collection of VldBukuPtkRelatedByBukuId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldBukuPtksRelatedByBukuId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return BukuPtk The current object (for fluent API support)
     */
    public function setVldBukuPtksRelatedByBukuId(PropelCollection $vldBukuPtksRelatedByBukuId, PropelPDO $con = null)
    {
        $vldBukuPtksRelatedByBukuIdToDelete = $this->getVldBukuPtksRelatedByBukuId(new Criteria(), $con)->diff($vldBukuPtksRelatedByBukuId);

        $this->vldBukuPtksRelatedByBukuIdScheduledForDeletion = unserialize(serialize($vldBukuPtksRelatedByBukuIdToDelete));

        foreach ($vldBukuPtksRelatedByBukuIdToDelete as $vldBukuPtkRelatedByBukuIdRemoved) {
            $vldBukuPtkRelatedByBukuIdRemoved->setBukuPtkRelatedByBukuId(null);
        }

        $this->collVldBukuPtksRelatedByBukuId = null;
        foreach ($vldBukuPtksRelatedByBukuId as $vldBukuPtkRelatedByBukuId) {
            $this->addVldBukuPtkRelatedByBukuId($vldBukuPtkRelatedByBukuId);
        }

        $this->collVldBukuPtksRelatedByBukuId = $vldBukuPtksRelatedByBukuId;
        $this->collVldBukuPtksRelatedByBukuIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldBukuPtk objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldBukuPtk objects.
     * @throws PropelException
     */
    public function countVldBukuPtksRelatedByBukuId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldBukuPtksRelatedByBukuIdPartial && !$this->isNew();
        if (null === $this->collVldBukuPtksRelatedByBukuId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldBukuPtksRelatedByBukuId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldBukuPtksRelatedByBukuId());
            }
            $query = VldBukuPtkQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByBukuPtkRelatedByBukuId($this)
                ->count($con);
        }

        return count($this->collVldBukuPtksRelatedByBukuId);
    }

    /**
     * Method called to associate a VldBukuPtk object to this object
     * through the VldBukuPtk foreign key attribute.
     *
     * @param    VldBukuPtk $l VldBukuPtk
     * @return BukuPtk The current object (for fluent API support)
     */
    public function addVldBukuPtkRelatedByBukuId(VldBukuPtk $l)
    {
        if ($this->collVldBukuPtksRelatedByBukuId === null) {
            $this->initVldBukuPtksRelatedByBukuId();
            $this->collVldBukuPtksRelatedByBukuIdPartial = true;
        }
        if (!in_array($l, $this->collVldBukuPtksRelatedByBukuId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldBukuPtkRelatedByBukuId($l);
        }

        return $this;
    }

    /**
     * @param	VldBukuPtkRelatedByBukuId $vldBukuPtkRelatedByBukuId The vldBukuPtkRelatedByBukuId object to add.
     */
    protected function doAddVldBukuPtkRelatedByBukuId($vldBukuPtkRelatedByBukuId)
    {
        $this->collVldBukuPtksRelatedByBukuId[]= $vldBukuPtkRelatedByBukuId;
        $vldBukuPtkRelatedByBukuId->setBukuPtkRelatedByBukuId($this);
    }

    /**
     * @param	VldBukuPtkRelatedByBukuId $vldBukuPtkRelatedByBukuId The vldBukuPtkRelatedByBukuId object to remove.
     * @return BukuPtk The current object (for fluent API support)
     */
    public function removeVldBukuPtkRelatedByBukuId($vldBukuPtkRelatedByBukuId)
    {
        if ($this->getVldBukuPtksRelatedByBukuId()->contains($vldBukuPtkRelatedByBukuId)) {
            $this->collVldBukuPtksRelatedByBukuId->remove($this->collVldBukuPtksRelatedByBukuId->search($vldBukuPtkRelatedByBukuId));
            if (null === $this->vldBukuPtksRelatedByBukuIdScheduledForDeletion) {
                $this->vldBukuPtksRelatedByBukuIdScheduledForDeletion = clone $this->collVldBukuPtksRelatedByBukuId;
                $this->vldBukuPtksRelatedByBukuIdScheduledForDeletion->clear();
            }
            $this->vldBukuPtksRelatedByBukuIdScheduledForDeletion[]= clone $vldBukuPtkRelatedByBukuId;
            $vldBukuPtkRelatedByBukuId->setBukuPtkRelatedByBukuId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BukuPtk is new, it will return
     * an empty collection; or if this BukuPtk has previously
     * been saved, it will retrieve related VldBukuPtksRelatedByBukuId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BukuPtk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldBukuPtk[] List of VldBukuPtk objects
     */
    public function getVldBukuPtksRelatedByBukuIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldBukuPtkQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldBukuPtksRelatedByBukuId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BukuPtk is new, it will return
     * an empty collection; or if this BukuPtk has previously
     * been saved, it will retrieve related VldBukuPtksRelatedByBukuId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BukuPtk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldBukuPtk[] List of VldBukuPtk objects
     */
    public function getVldBukuPtksRelatedByBukuIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldBukuPtkQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldBukuPtksRelatedByBukuId($query, $con);
    }

    /**
     * Clears out the collVldBukuPtksRelatedByBukuId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return BukuPtk The current object (for fluent API support)
     * @see        addVldBukuPtksRelatedByBukuId()
     */
    public function clearVldBukuPtksRelatedByBukuId()
    {
        $this->collVldBukuPtksRelatedByBukuId = null; // important to set this to null since that means it is uninitialized
        $this->collVldBukuPtksRelatedByBukuIdPartial = null;

        return $this;
    }

    /**
     * reset is the collVldBukuPtksRelatedByBukuId collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldBukuPtksRelatedByBukuId($v = true)
    {
        $this->collVldBukuPtksRelatedByBukuIdPartial = $v;
    }

    /**
     * Initializes the collVldBukuPtksRelatedByBukuId collection.
     *
     * By default this just sets the collVldBukuPtksRelatedByBukuId collection to an empty array (like clearcollVldBukuPtksRelatedByBukuId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldBukuPtksRelatedByBukuId($overrideExisting = true)
    {
        if (null !== $this->collVldBukuPtksRelatedByBukuId && !$overrideExisting) {
            return;
        }
        $this->collVldBukuPtksRelatedByBukuId = new PropelObjectCollection();
        $this->collVldBukuPtksRelatedByBukuId->setModel('VldBukuPtk');
    }

    /**
     * Gets an array of VldBukuPtk objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this BukuPtk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldBukuPtk[] List of VldBukuPtk objects
     * @throws PropelException
     */
    public function getVldBukuPtksRelatedByBukuId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldBukuPtksRelatedByBukuIdPartial && !$this->isNew();
        if (null === $this->collVldBukuPtksRelatedByBukuId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldBukuPtksRelatedByBukuId) {
                // return empty collection
                $this->initVldBukuPtksRelatedByBukuId();
            } else {
                $collVldBukuPtksRelatedByBukuId = VldBukuPtkQuery::create(null, $criteria)
                    ->filterByBukuPtkRelatedByBukuId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldBukuPtksRelatedByBukuIdPartial && count($collVldBukuPtksRelatedByBukuId)) {
                      $this->initVldBukuPtksRelatedByBukuId(false);

                      foreach($collVldBukuPtksRelatedByBukuId as $obj) {
                        if (false == $this->collVldBukuPtksRelatedByBukuId->contains($obj)) {
                          $this->collVldBukuPtksRelatedByBukuId->append($obj);
                        }
                      }

                      $this->collVldBukuPtksRelatedByBukuIdPartial = true;
                    }

                    $collVldBukuPtksRelatedByBukuId->getInternalIterator()->rewind();
                    return $collVldBukuPtksRelatedByBukuId;
                }

                if($partial && $this->collVldBukuPtksRelatedByBukuId) {
                    foreach($this->collVldBukuPtksRelatedByBukuId as $obj) {
                        if($obj->isNew()) {
                            $collVldBukuPtksRelatedByBukuId[] = $obj;
                        }
                    }
                }

                $this->collVldBukuPtksRelatedByBukuId = $collVldBukuPtksRelatedByBukuId;
                $this->collVldBukuPtksRelatedByBukuIdPartial = false;
            }
        }

        return $this->collVldBukuPtksRelatedByBukuId;
    }

    /**
     * Sets a collection of VldBukuPtkRelatedByBukuId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldBukuPtksRelatedByBukuId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return BukuPtk The current object (for fluent API support)
     */
    public function setVldBukuPtksRelatedByBukuId(PropelCollection $vldBukuPtksRelatedByBukuId, PropelPDO $con = null)
    {
        $vldBukuPtksRelatedByBukuIdToDelete = $this->getVldBukuPtksRelatedByBukuId(new Criteria(), $con)->diff($vldBukuPtksRelatedByBukuId);

        $this->vldBukuPtksRelatedByBukuIdScheduledForDeletion = unserialize(serialize($vldBukuPtksRelatedByBukuIdToDelete));

        foreach ($vldBukuPtksRelatedByBukuIdToDelete as $vldBukuPtkRelatedByBukuIdRemoved) {
            $vldBukuPtkRelatedByBukuIdRemoved->setBukuPtkRelatedByBukuId(null);
        }

        $this->collVldBukuPtksRelatedByBukuId = null;
        foreach ($vldBukuPtksRelatedByBukuId as $vldBukuPtkRelatedByBukuId) {
            $this->addVldBukuPtkRelatedByBukuId($vldBukuPtkRelatedByBukuId);
        }

        $this->collVldBukuPtksRelatedByBukuId = $vldBukuPtksRelatedByBukuId;
        $this->collVldBukuPtksRelatedByBukuIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldBukuPtk objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldBukuPtk objects.
     * @throws PropelException
     */
    public function countVldBukuPtksRelatedByBukuId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldBukuPtksRelatedByBukuIdPartial && !$this->isNew();
        if (null === $this->collVldBukuPtksRelatedByBukuId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldBukuPtksRelatedByBukuId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldBukuPtksRelatedByBukuId());
            }
            $query = VldBukuPtkQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByBukuPtkRelatedByBukuId($this)
                ->count($con);
        }

        return count($this->collVldBukuPtksRelatedByBukuId);
    }

    /**
     * Method called to associate a VldBukuPtk object to this object
     * through the VldBukuPtk foreign key attribute.
     *
     * @param    VldBukuPtk $l VldBukuPtk
     * @return BukuPtk The current object (for fluent API support)
     */
    public function addVldBukuPtkRelatedByBukuId(VldBukuPtk $l)
    {
        if ($this->collVldBukuPtksRelatedByBukuId === null) {
            $this->initVldBukuPtksRelatedByBukuId();
            $this->collVldBukuPtksRelatedByBukuIdPartial = true;
        }
        if (!in_array($l, $this->collVldBukuPtksRelatedByBukuId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldBukuPtkRelatedByBukuId($l);
        }

        return $this;
    }

    /**
     * @param	VldBukuPtkRelatedByBukuId $vldBukuPtkRelatedByBukuId The vldBukuPtkRelatedByBukuId object to add.
     */
    protected function doAddVldBukuPtkRelatedByBukuId($vldBukuPtkRelatedByBukuId)
    {
        $this->collVldBukuPtksRelatedByBukuId[]= $vldBukuPtkRelatedByBukuId;
        $vldBukuPtkRelatedByBukuId->setBukuPtkRelatedByBukuId($this);
    }

    /**
     * @param	VldBukuPtkRelatedByBukuId $vldBukuPtkRelatedByBukuId The vldBukuPtkRelatedByBukuId object to remove.
     * @return BukuPtk The current object (for fluent API support)
     */
    public function removeVldBukuPtkRelatedByBukuId($vldBukuPtkRelatedByBukuId)
    {
        if ($this->getVldBukuPtksRelatedByBukuId()->contains($vldBukuPtkRelatedByBukuId)) {
            $this->collVldBukuPtksRelatedByBukuId->remove($this->collVldBukuPtksRelatedByBukuId->search($vldBukuPtkRelatedByBukuId));
            if (null === $this->vldBukuPtksRelatedByBukuIdScheduledForDeletion) {
                $this->vldBukuPtksRelatedByBukuIdScheduledForDeletion = clone $this->collVldBukuPtksRelatedByBukuId;
                $this->vldBukuPtksRelatedByBukuIdScheduledForDeletion->clear();
            }
            $this->vldBukuPtksRelatedByBukuIdScheduledForDeletion[]= clone $vldBukuPtkRelatedByBukuId;
            $vldBukuPtkRelatedByBukuId->setBukuPtkRelatedByBukuId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BukuPtk is new, it will return
     * an empty collection; or if this BukuPtk has previously
     * been saved, it will retrieve related VldBukuPtksRelatedByBukuId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BukuPtk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldBukuPtk[] List of VldBukuPtk objects
     */
    public function getVldBukuPtksRelatedByBukuIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldBukuPtkQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldBukuPtksRelatedByBukuId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BukuPtk is new, it will return
     * an empty collection; or if this BukuPtk has previously
     * been saved, it will retrieve related VldBukuPtksRelatedByBukuId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BukuPtk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldBukuPtk[] List of VldBukuPtk objects
     */
    public function getVldBukuPtksRelatedByBukuIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldBukuPtkQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldBukuPtksRelatedByBukuId($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->buku_id = null;
        $this->ptk_id = null;
        $this->judul_buku = null;
        $this->tahun = null;
        $this->penerbit = null;
        $this->last_update = null;
        $this->soft_delete = null;
        $this->last_sync = null;
        $this->updater_id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collVldBukuPtksRelatedByBukuId) {
                foreach ($this->collVldBukuPtksRelatedByBukuId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldBukuPtksRelatedByBukuId) {
                foreach ($this->collVldBukuPtksRelatedByBukuId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aPtkRelatedByPtkId instanceof Persistent) {
              $this->aPtkRelatedByPtkId->clearAllReferences($deep);
            }
            if ($this->aPtkRelatedByPtkId instanceof Persistent) {
              $this->aPtkRelatedByPtkId->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collVldBukuPtksRelatedByBukuId instanceof PropelCollection) {
            $this->collVldBukuPtksRelatedByBukuId->clearIterator();
        }
        $this->collVldBukuPtksRelatedByBukuId = null;
        if ($this->collVldBukuPtksRelatedByBukuId instanceof PropelCollection) {
            $this->collVldBukuPtksRelatedByBukuId->clearIterator();
        }
        $this->collVldBukuPtksRelatedByBukuId = null;
        $this->aPtkRelatedByPtkId = null;
        $this->aPtkRelatedByPtkId = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(BukuPtkPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
