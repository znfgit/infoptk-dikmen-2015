<?php

namespace angulex\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelDateTime;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\BatasWaktuRapor;
use angulex\Model\BatasWaktuRaporQuery;
use angulex\Model\BukuAlatLongitudinal;
use angulex\Model\BukuAlatLongitudinalQuery;
use angulex\Model\Pembelajaran;
use angulex\Model\PembelajaranQuery;
use angulex\Model\PesertaDidikLongitudinal;
use angulex\Model\PesertaDidikLongitudinalQuery;
use angulex\Model\PrasaranaLongitudinal;
use angulex\Model\PrasaranaLongitudinalQuery;
use angulex\Model\RombonganBelajar;
use angulex\Model\RombonganBelajarQuery;
use angulex\Model\Sanitasi;
use angulex\Model\SanitasiQuery;
use angulex\Model\SaranaLongitudinal;
use angulex\Model\SaranaLongitudinalQuery;
use angulex\Model\SekolahLongitudinal;
use angulex\Model\SekolahLongitudinalQuery;
use angulex\Model\Semester;
use angulex\Model\SemesterPeer;
use angulex\Model\SemesterQuery;
use angulex\Model\TahunAjaran;
use angulex\Model\TahunAjaranQuery;

/**
 * Base class that represents a row from the 'ref.semester' table.
 *
 * 
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseSemester extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'angulex\\Model\\SemesterPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        SemesterPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the semester_id field.
     * @var        string
     */
    protected $semester_id;

    /**
     * The value for the tahun_ajaran_id field.
     * @var        string
     */
    protected $tahun_ajaran_id;

    /**
     * The value for the nama field.
     * @var        string
     */
    protected $nama;

    /**
     * The value for the semester field.
     * @var        string
     */
    protected $semester;

    /**
     * The value for the periode_aktif field.
     * @var        string
     */
    protected $periode_aktif;

    /**
     * The value for the tanggal_mulai field.
     * @var        string
     */
    protected $tanggal_mulai;

    /**
     * The value for the tanggal_selesai field.
     * @var        string
     */
    protected $tanggal_selesai;

    /**
     * The value for the create_date field.
     * @var        string
     */
    protected $create_date;

    /**
     * The value for the last_update field.
     * @var        string
     */
    protected $last_update;

    /**
     * The value for the expired_date field.
     * @var        string
     */
    protected $expired_date;

    /**
     * The value for the last_sync field.
     * @var        string
     */
    protected $last_sync;

    /**
     * @var        TahunAjaran
     */
    protected $aTahunAjaran;

    /**
     * @var        PropelObjectCollection|SekolahLongitudinal[] Collection to store aggregation of SekolahLongitudinal objects.
     */
    protected $collSekolahLongitudinalsRelatedBySemesterId;
    protected $collSekolahLongitudinalsRelatedBySemesterIdPartial;

    /**
     * @var        PropelObjectCollection|SekolahLongitudinal[] Collection to store aggregation of SekolahLongitudinal objects.
     */
    protected $collSekolahLongitudinalsRelatedBySemesterId;
    protected $collSekolahLongitudinalsRelatedBySemesterIdPartial;

    /**
     * @var        PropelObjectCollection|PesertaDidikLongitudinal[] Collection to store aggregation of PesertaDidikLongitudinal objects.
     */
    protected $collPesertaDidikLongitudinalsRelatedBySemesterId;
    protected $collPesertaDidikLongitudinalsRelatedBySemesterIdPartial;

    /**
     * @var        PropelObjectCollection|PesertaDidikLongitudinal[] Collection to store aggregation of PesertaDidikLongitudinal objects.
     */
    protected $collPesertaDidikLongitudinalsRelatedBySemesterId;
    protected $collPesertaDidikLongitudinalsRelatedBySemesterIdPartial;

    /**
     * @var        PropelObjectCollection|Sanitasi[] Collection to store aggregation of Sanitasi objects.
     */
    protected $collSanitasisRelatedBySemesterId;
    protected $collSanitasisRelatedBySemesterIdPartial;

    /**
     * @var        PropelObjectCollection|Sanitasi[] Collection to store aggregation of Sanitasi objects.
     */
    protected $collSanitasisRelatedBySemesterId;
    protected $collSanitasisRelatedBySemesterIdPartial;

    /**
     * @var        PropelObjectCollection|SaranaLongitudinal[] Collection to store aggregation of SaranaLongitudinal objects.
     */
    protected $collSaranaLongitudinalsRelatedBySemesterId;
    protected $collSaranaLongitudinalsRelatedBySemesterIdPartial;

    /**
     * @var        PropelObjectCollection|SaranaLongitudinal[] Collection to store aggregation of SaranaLongitudinal objects.
     */
    protected $collSaranaLongitudinalsRelatedBySemesterId;
    protected $collSaranaLongitudinalsRelatedBySemesterIdPartial;

    /**
     * @var        BatasWaktuRapor one-to-one related BatasWaktuRapor object
     */
    protected $singleBatasWaktuRapor;

    /**
     * @var        PropelObjectCollection|Pembelajaran[] Collection to store aggregation of Pembelajaran objects.
     */
    protected $collPembelajaransRelatedBySemesterId;
    protected $collPembelajaransRelatedBySemesterIdPartial;

    /**
     * @var        PropelObjectCollection|Pembelajaran[] Collection to store aggregation of Pembelajaran objects.
     */
    protected $collPembelajaransRelatedBySemesterId;
    protected $collPembelajaransRelatedBySemesterIdPartial;

    /**
     * @var        PropelObjectCollection|PrasaranaLongitudinal[] Collection to store aggregation of PrasaranaLongitudinal objects.
     */
    protected $collPrasaranaLongitudinalsRelatedBySemesterId;
    protected $collPrasaranaLongitudinalsRelatedBySemesterIdPartial;

    /**
     * @var        PropelObjectCollection|PrasaranaLongitudinal[] Collection to store aggregation of PrasaranaLongitudinal objects.
     */
    protected $collPrasaranaLongitudinalsRelatedBySemesterId;
    protected $collPrasaranaLongitudinalsRelatedBySemesterIdPartial;

    /**
     * @var        PropelObjectCollection|RombonganBelajar[] Collection to store aggregation of RombonganBelajar objects.
     */
    protected $collRombonganBelajarsRelatedBySemesterId;
    protected $collRombonganBelajarsRelatedBySemesterIdPartial;

    /**
     * @var        PropelObjectCollection|RombonganBelajar[] Collection to store aggregation of RombonganBelajar objects.
     */
    protected $collRombonganBelajarsRelatedBySemesterId;
    protected $collRombonganBelajarsRelatedBySemesterIdPartial;

    /**
     * @var        PropelObjectCollection|BukuAlatLongitudinal[] Collection to store aggregation of BukuAlatLongitudinal objects.
     */
    protected $collBukuAlatLongitudinalsRelatedBySemesterId;
    protected $collBukuAlatLongitudinalsRelatedBySemesterIdPartial;

    /**
     * @var        PropelObjectCollection|BukuAlatLongitudinal[] Collection to store aggregation of BukuAlatLongitudinal objects.
     */
    protected $collBukuAlatLongitudinalsRelatedBySemesterId;
    protected $collBukuAlatLongitudinalsRelatedBySemesterIdPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $sekolahLongitudinalsRelatedBySemesterIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $sekolahLongitudinalsRelatedBySemesterIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $pesertaDidikLongitudinalsRelatedBySemesterIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $pesertaDidikLongitudinalsRelatedBySemesterIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $sanitasisRelatedBySemesterIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $sanitasisRelatedBySemesterIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $saranaLongitudinalsRelatedBySemesterIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $saranaLongitudinalsRelatedBySemesterIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $batasWaktuRaporsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $pembelajaransRelatedBySemesterIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $pembelajaransRelatedBySemesterIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $prasaranaLongitudinalsRelatedBySemesterIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $prasaranaLongitudinalsRelatedBySemesterIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rombonganBelajarsRelatedBySemesterIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rombonganBelajarsRelatedBySemesterIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $bukuAlatLongitudinalsRelatedBySemesterIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $bukuAlatLongitudinalsRelatedBySemesterIdScheduledForDeletion = null;

    /**
     * Get the [semester_id] column value.
     * 
     * @return string
     */
    public function getSemesterId()
    {
        return $this->semester_id;
    }

    /**
     * Get the [tahun_ajaran_id] column value.
     * 
     * @return string
     */
    public function getTahunAjaranId()
    {
        return $this->tahun_ajaran_id;
    }

    /**
     * Get the [nama] column value.
     * 
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Get the [semester] column value.
     * 
     * @return string
     */
    public function getSemester()
    {
        return $this->semester;
    }

    /**
     * Get the [periode_aktif] column value.
     * 
     * @return string
     */
    public function getPeriodeAktif()
    {
        return $this->periode_aktif;
    }

    /**
     * Get the [tanggal_mulai] column value.
     * 
     * @return string
     */
    public function getTanggalMulai()
    {
        return $this->tanggal_mulai;
    }

    /**
     * Get the [tanggal_selesai] column value.
     * 
     * @return string
     */
    public function getTanggalSelesai()
    {
        return $this->tanggal_selesai;
    }

    /**
     * Get the [optionally formatted] temporal [create_date] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreateDate($format = 'Y-m-d H:i:s')
    {
        if ($this->create_date === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->create_date);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->create_date, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [last_update] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastUpdate($format = 'Y-m-d H:i:s')
    {
        if ($this->last_update === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_update);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_update, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [expired_date] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getExpiredDate($format = 'Y-m-d H:i:s')
    {
        if ($this->expired_date === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->expired_date);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->expired_date, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [last_sync] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastSync($format = 'Y-m-d H:i:s')
    {
        if ($this->last_sync === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_sync);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_sync, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Set the value of [semester_id] column.
     * 
     * @param string $v new value
     * @return Semester The current object (for fluent API support)
     */
    public function setSemesterId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->semester_id !== $v) {
            $this->semester_id = $v;
            $this->modifiedColumns[] = SemesterPeer::SEMESTER_ID;
        }


        return $this;
    } // setSemesterId()

    /**
     * Set the value of [tahun_ajaran_id] column.
     * 
     * @param string $v new value
     * @return Semester The current object (for fluent API support)
     */
    public function setTahunAjaranId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tahun_ajaran_id !== $v) {
            $this->tahun_ajaran_id = $v;
            $this->modifiedColumns[] = SemesterPeer::TAHUN_AJARAN_ID;
        }

        if ($this->aTahunAjaran !== null && $this->aTahunAjaran->getTahunAjaranId() !== $v) {
            $this->aTahunAjaran = null;
        }


        return $this;
    } // setTahunAjaranId()

    /**
     * Set the value of [nama] column.
     * 
     * @param string $v new value
     * @return Semester The current object (for fluent API support)
     */
    public function setNama($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nama !== $v) {
            $this->nama = $v;
            $this->modifiedColumns[] = SemesterPeer::NAMA;
        }


        return $this;
    } // setNama()

    /**
     * Set the value of [semester] column.
     * 
     * @param string $v new value
     * @return Semester The current object (for fluent API support)
     */
    public function setSemester($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->semester !== $v) {
            $this->semester = $v;
            $this->modifiedColumns[] = SemesterPeer::SEMESTER;
        }


        return $this;
    } // setSemester()

    /**
     * Set the value of [periode_aktif] column.
     * 
     * @param string $v new value
     * @return Semester The current object (for fluent API support)
     */
    public function setPeriodeAktif($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->periode_aktif !== $v) {
            $this->periode_aktif = $v;
            $this->modifiedColumns[] = SemesterPeer::PERIODE_AKTIF;
        }


        return $this;
    } // setPeriodeAktif()

    /**
     * Set the value of [tanggal_mulai] column.
     * 
     * @param string $v new value
     * @return Semester The current object (for fluent API support)
     */
    public function setTanggalMulai($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tanggal_mulai !== $v) {
            $this->tanggal_mulai = $v;
            $this->modifiedColumns[] = SemesterPeer::TANGGAL_MULAI;
        }


        return $this;
    } // setTanggalMulai()

    /**
     * Set the value of [tanggal_selesai] column.
     * 
     * @param string $v new value
     * @return Semester The current object (for fluent API support)
     */
    public function setTanggalSelesai($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tanggal_selesai !== $v) {
            $this->tanggal_selesai = $v;
            $this->modifiedColumns[] = SemesterPeer::TANGGAL_SELESAI;
        }


        return $this;
    } // setTanggalSelesai()

    /**
     * Sets the value of [create_date] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Semester The current object (for fluent API support)
     */
    public function setCreateDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->create_date !== null || $dt !== null) {
            $currentDateAsString = ($this->create_date !== null && $tmpDt = new DateTime($this->create_date)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->create_date = $newDateAsString;
                $this->modifiedColumns[] = SemesterPeer::CREATE_DATE;
            }
        } // if either are not null


        return $this;
    } // setCreateDate()

    /**
     * Sets the value of [last_update] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Semester The current object (for fluent API support)
     */
    public function setLastUpdate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_update !== null || $dt !== null) {
            $currentDateAsString = ($this->last_update !== null && $tmpDt = new DateTime($this->last_update)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_update = $newDateAsString;
                $this->modifiedColumns[] = SemesterPeer::LAST_UPDATE;
            }
        } // if either are not null


        return $this;
    } // setLastUpdate()

    /**
     * Sets the value of [expired_date] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Semester The current object (for fluent API support)
     */
    public function setExpiredDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->expired_date !== null || $dt !== null) {
            $currentDateAsString = ($this->expired_date !== null && $tmpDt = new DateTime($this->expired_date)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->expired_date = $newDateAsString;
                $this->modifiedColumns[] = SemesterPeer::EXPIRED_DATE;
            }
        } // if either are not null


        return $this;
    } // setExpiredDate()

    /**
     * Sets the value of [last_sync] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Semester The current object (for fluent API support)
     */
    public function setLastSync($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_sync !== null || $dt !== null) {
            $currentDateAsString = ($this->last_sync !== null && $tmpDt = new DateTime($this->last_sync)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_sync = $newDateAsString;
                $this->modifiedColumns[] = SemesterPeer::LAST_SYNC;
            }
        } // if either are not null


        return $this;
    } // setLastSync()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->semester_id = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
            $this->tahun_ajaran_id = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->nama = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->semester = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->periode_aktif = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->tanggal_mulai = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->tanggal_selesai = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->create_date = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->last_update = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->expired_date = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->last_sync = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 11; // 11 = SemesterPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Semester object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aTahunAjaran !== null && $this->tahun_ajaran_id !== $this->aTahunAjaran->getTahunAjaranId()) {
            $this->aTahunAjaran = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SemesterPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = SemesterPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aTahunAjaran = null;
            $this->collSekolahLongitudinalsRelatedBySemesterId = null;

            $this->collSekolahLongitudinalsRelatedBySemesterId = null;

            $this->collPesertaDidikLongitudinalsRelatedBySemesterId = null;

            $this->collPesertaDidikLongitudinalsRelatedBySemesterId = null;

            $this->collSanitasisRelatedBySemesterId = null;

            $this->collSanitasisRelatedBySemesterId = null;

            $this->collSaranaLongitudinalsRelatedBySemesterId = null;

            $this->collSaranaLongitudinalsRelatedBySemesterId = null;

            $this->singleBatasWaktuRapor = null;

            $this->collPembelajaransRelatedBySemesterId = null;

            $this->collPembelajaransRelatedBySemesterId = null;

            $this->collPrasaranaLongitudinalsRelatedBySemesterId = null;

            $this->collPrasaranaLongitudinalsRelatedBySemesterId = null;

            $this->collRombonganBelajarsRelatedBySemesterId = null;

            $this->collRombonganBelajarsRelatedBySemesterId = null;

            $this->collBukuAlatLongitudinalsRelatedBySemesterId = null;

            $this->collBukuAlatLongitudinalsRelatedBySemesterId = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SemesterPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = SemesterQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SemesterPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                SemesterPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aTahunAjaran !== null) {
                if ($this->aTahunAjaran->isModified() || $this->aTahunAjaran->isNew()) {
                    $affectedRows += $this->aTahunAjaran->save($con);
                }
                $this->setTahunAjaran($this->aTahunAjaran);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->sekolahLongitudinalsRelatedBySemesterIdScheduledForDeletion !== null) {
                if (!$this->sekolahLongitudinalsRelatedBySemesterIdScheduledForDeletion->isEmpty()) {
                    SekolahLongitudinalQuery::create()
                        ->filterByPrimaryKeys($this->sekolahLongitudinalsRelatedBySemesterIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->sekolahLongitudinalsRelatedBySemesterIdScheduledForDeletion = null;
                }
            }

            if ($this->collSekolahLongitudinalsRelatedBySemesterId !== null) {
                foreach ($this->collSekolahLongitudinalsRelatedBySemesterId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->sekolahLongitudinalsRelatedBySemesterIdScheduledForDeletion !== null) {
                if (!$this->sekolahLongitudinalsRelatedBySemesterIdScheduledForDeletion->isEmpty()) {
                    SekolahLongitudinalQuery::create()
                        ->filterByPrimaryKeys($this->sekolahLongitudinalsRelatedBySemesterIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->sekolahLongitudinalsRelatedBySemesterIdScheduledForDeletion = null;
                }
            }

            if ($this->collSekolahLongitudinalsRelatedBySemesterId !== null) {
                foreach ($this->collSekolahLongitudinalsRelatedBySemesterId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->pesertaDidikLongitudinalsRelatedBySemesterIdScheduledForDeletion !== null) {
                if (!$this->pesertaDidikLongitudinalsRelatedBySemesterIdScheduledForDeletion->isEmpty()) {
                    PesertaDidikLongitudinalQuery::create()
                        ->filterByPrimaryKeys($this->pesertaDidikLongitudinalsRelatedBySemesterIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->pesertaDidikLongitudinalsRelatedBySemesterIdScheduledForDeletion = null;
                }
            }

            if ($this->collPesertaDidikLongitudinalsRelatedBySemesterId !== null) {
                foreach ($this->collPesertaDidikLongitudinalsRelatedBySemesterId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->pesertaDidikLongitudinalsRelatedBySemesterIdScheduledForDeletion !== null) {
                if (!$this->pesertaDidikLongitudinalsRelatedBySemesterIdScheduledForDeletion->isEmpty()) {
                    PesertaDidikLongitudinalQuery::create()
                        ->filterByPrimaryKeys($this->pesertaDidikLongitudinalsRelatedBySemesterIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->pesertaDidikLongitudinalsRelatedBySemesterIdScheduledForDeletion = null;
                }
            }

            if ($this->collPesertaDidikLongitudinalsRelatedBySemesterId !== null) {
                foreach ($this->collPesertaDidikLongitudinalsRelatedBySemesterId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->sanitasisRelatedBySemesterIdScheduledForDeletion !== null) {
                if (!$this->sanitasisRelatedBySemesterIdScheduledForDeletion->isEmpty()) {
                    SanitasiQuery::create()
                        ->filterByPrimaryKeys($this->sanitasisRelatedBySemesterIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->sanitasisRelatedBySemesterIdScheduledForDeletion = null;
                }
            }

            if ($this->collSanitasisRelatedBySemesterId !== null) {
                foreach ($this->collSanitasisRelatedBySemesterId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->sanitasisRelatedBySemesterIdScheduledForDeletion !== null) {
                if (!$this->sanitasisRelatedBySemesterIdScheduledForDeletion->isEmpty()) {
                    SanitasiQuery::create()
                        ->filterByPrimaryKeys($this->sanitasisRelatedBySemesterIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->sanitasisRelatedBySemesterIdScheduledForDeletion = null;
                }
            }

            if ($this->collSanitasisRelatedBySemesterId !== null) {
                foreach ($this->collSanitasisRelatedBySemesterId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->saranaLongitudinalsRelatedBySemesterIdScheduledForDeletion !== null) {
                if (!$this->saranaLongitudinalsRelatedBySemesterIdScheduledForDeletion->isEmpty()) {
                    SaranaLongitudinalQuery::create()
                        ->filterByPrimaryKeys($this->saranaLongitudinalsRelatedBySemesterIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->saranaLongitudinalsRelatedBySemesterIdScheduledForDeletion = null;
                }
            }

            if ($this->collSaranaLongitudinalsRelatedBySemesterId !== null) {
                foreach ($this->collSaranaLongitudinalsRelatedBySemesterId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->saranaLongitudinalsRelatedBySemesterIdScheduledForDeletion !== null) {
                if (!$this->saranaLongitudinalsRelatedBySemesterIdScheduledForDeletion->isEmpty()) {
                    SaranaLongitudinalQuery::create()
                        ->filterByPrimaryKeys($this->saranaLongitudinalsRelatedBySemesterIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->saranaLongitudinalsRelatedBySemesterIdScheduledForDeletion = null;
                }
            }

            if ($this->collSaranaLongitudinalsRelatedBySemesterId !== null) {
                foreach ($this->collSaranaLongitudinalsRelatedBySemesterId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->batasWaktuRaporsScheduledForDeletion !== null) {
                if (!$this->batasWaktuRaporsScheduledForDeletion->isEmpty()) {
                    BatasWaktuRaporQuery::create()
                        ->filterByPrimaryKeys($this->batasWaktuRaporsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->batasWaktuRaporsScheduledForDeletion = null;
                }
            }

            if ($this->singleBatasWaktuRapor !== null) {
                if (!$this->singleBatasWaktuRapor->isDeleted() && ($this->singleBatasWaktuRapor->isNew() || $this->singleBatasWaktuRapor->isModified())) {
                        $affectedRows += $this->singleBatasWaktuRapor->save($con);
                }
            }

            if ($this->pembelajaransRelatedBySemesterIdScheduledForDeletion !== null) {
                if (!$this->pembelajaransRelatedBySemesterIdScheduledForDeletion->isEmpty()) {
                    PembelajaranQuery::create()
                        ->filterByPrimaryKeys($this->pembelajaransRelatedBySemesterIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->pembelajaransRelatedBySemesterIdScheduledForDeletion = null;
                }
            }

            if ($this->collPembelajaransRelatedBySemesterId !== null) {
                foreach ($this->collPembelajaransRelatedBySemesterId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->pembelajaransRelatedBySemesterIdScheduledForDeletion !== null) {
                if (!$this->pembelajaransRelatedBySemesterIdScheduledForDeletion->isEmpty()) {
                    PembelajaranQuery::create()
                        ->filterByPrimaryKeys($this->pembelajaransRelatedBySemesterIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->pembelajaransRelatedBySemesterIdScheduledForDeletion = null;
                }
            }

            if ($this->collPembelajaransRelatedBySemesterId !== null) {
                foreach ($this->collPembelajaransRelatedBySemesterId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->prasaranaLongitudinalsRelatedBySemesterIdScheduledForDeletion !== null) {
                if (!$this->prasaranaLongitudinalsRelatedBySemesterIdScheduledForDeletion->isEmpty()) {
                    PrasaranaLongitudinalQuery::create()
                        ->filterByPrimaryKeys($this->prasaranaLongitudinalsRelatedBySemesterIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->prasaranaLongitudinalsRelatedBySemesterIdScheduledForDeletion = null;
                }
            }

            if ($this->collPrasaranaLongitudinalsRelatedBySemesterId !== null) {
                foreach ($this->collPrasaranaLongitudinalsRelatedBySemesterId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->prasaranaLongitudinalsRelatedBySemesterIdScheduledForDeletion !== null) {
                if (!$this->prasaranaLongitudinalsRelatedBySemesterIdScheduledForDeletion->isEmpty()) {
                    PrasaranaLongitudinalQuery::create()
                        ->filterByPrimaryKeys($this->prasaranaLongitudinalsRelatedBySemesterIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->prasaranaLongitudinalsRelatedBySemesterIdScheduledForDeletion = null;
                }
            }

            if ($this->collPrasaranaLongitudinalsRelatedBySemesterId !== null) {
                foreach ($this->collPrasaranaLongitudinalsRelatedBySemesterId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rombonganBelajarsRelatedBySemesterIdScheduledForDeletion !== null) {
                if (!$this->rombonganBelajarsRelatedBySemesterIdScheduledForDeletion->isEmpty()) {
                    RombonganBelajarQuery::create()
                        ->filterByPrimaryKeys($this->rombonganBelajarsRelatedBySemesterIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rombonganBelajarsRelatedBySemesterIdScheduledForDeletion = null;
                }
            }

            if ($this->collRombonganBelajarsRelatedBySemesterId !== null) {
                foreach ($this->collRombonganBelajarsRelatedBySemesterId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rombonganBelajarsRelatedBySemesterIdScheduledForDeletion !== null) {
                if (!$this->rombonganBelajarsRelatedBySemesterIdScheduledForDeletion->isEmpty()) {
                    RombonganBelajarQuery::create()
                        ->filterByPrimaryKeys($this->rombonganBelajarsRelatedBySemesterIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rombonganBelajarsRelatedBySemesterIdScheduledForDeletion = null;
                }
            }

            if ($this->collRombonganBelajarsRelatedBySemesterId !== null) {
                foreach ($this->collRombonganBelajarsRelatedBySemesterId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->bukuAlatLongitudinalsRelatedBySemesterIdScheduledForDeletion !== null) {
                if (!$this->bukuAlatLongitudinalsRelatedBySemesterIdScheduledForDeletion->isEmpty()) {
                    BukuAlatLongitudinalQuery::create()
                        ->filterByPrimaryKeys($this->bukuAlatLongitudinalsRelatedBySemesterIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->bukuAlatLongitudinalsRelatedBySemesterIdScheduledForDeletion = null;
                }
            }

            if ($this->collBukuAlatLongitudinalsRelatedBySemesterId !== null) {
                foreach ($this->collBukuAlatLongitudinalsRelatedBySemesterId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->bukuAlatLongitudinalsRelatedBySemesterIdScheduledForDeletion !== null) {
                if (!$this->bukuAlatLongitudinalsRelatedBySemesterIdScheduledForDeletion->isEmpty()) {
                    BukuAlatLongitudinalQuery::create()
                        ->filterByPrimaryKeys($this->bukuAlatLongitudinalsRelatedBySemesterIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->bukuAlatLongitudinalsRelatedBySemesterIdScheduledForDeletion = null;
                }
            }

            if ($this->collBukuAlatLongitudinalsRelatedBySemesterId !== null) {
                foreach ($this->collBukuAlatLongitudinalsRelatedBySemesterId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $criteria = $this->buildCriteria();
        $pk = BasePeer::doInsert($criteria, $con);
        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aTahunAjaran !== null) {
                if (!$this->aTahunAjaran->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aTahunAjaran->getValidationFailures());
                }
            }


            if (($retval = SemesterPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collSekolahLongitudinalsRelatedBySemesterId !== null) {
                    foreach ($this->collSekolahLongitudinalsRelatedBySemesterId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSekolahLongitudinalsRelatedBySemesterId !== null) {
                    foreach ($this->collSekolahLongitudinalsRelatedBySemesterId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPesertaDidikLongitudinalsRelatedBySemesterId !== null) {
                    foreach ($this->collPesertaDidikLongitudinalsRelatedBySemesterId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPesertaDidikLongitudinalsRelatedBySemesterId !== null) {
                    foreach ($this->collPesertaDidikLongitudinalsRelatedBySemesterId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSanitasisRelatedBySemesterId !== null) {
                    foreach ($this->collSanitasisRelatedBySemesterId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSanitasisRelatedBySemesterId !== null) {
                    foreach ($this->collSanitasisRelatedBySemesterId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSaranaLongitudinalsRelatedBySemesterId !== null) {
                    foreach ($this->collSaranaLongitudinalsRelatedBySemesterId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSaranaLongitudinalsRelatedBySemesterId !== null) {
                    foreach ($this->collSaranaLongitudinalsRelatedBySemesterId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->singleBatasWaktuRapor !== null) {
                    if (!$this->singleBatasWaktuRapor->validate($columns)) {
                        $failureMap = array_merge($failureMap, $this->singleBatasWaktuRapor->getValidationFailures());
                    }
                }

                if ($this->collPembelajaransRelatedBySemesterId !== null) {
                    foreach ($this->collPembelajaransRelatedBySemesterId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPembelajaransRelatedBySemesterId !== null) {
                    foreach ($this->collPembelajaransRelatedBySemesterId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPrasaranaLongitudinalsRelatedBySemesterId !== null) {
                    foreach ($this->collPrasaranaLongitudinalsRelatedBySemesterId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPrasaranaLongitudinalsRelatedBySemesterId !== null) {
                    foreach ($this->collPrasaranaLongitudinalsRelatedBySemesterId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRombonganBelajarsRelatedBySemesterId !== null) {
                    foreach ($this->collRombonganBelajarsRelatedBySemesterId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRombonganBelajarsRelatedBySemesterId !== null) {
                    foreach ($this->collRombonganBelajarsRelatedBySemesterId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collBukuAlatLongitudinalsRelatedBySemesterId !== null) {
                    foreach ($this->collBukuAlatLongitudinalsRelatedBySemesterId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collBukuAlatLongitudinalsRelatedBySemesterId !== null) {
                    foreach ($this->collBukuAlatLongitudinalsRelatedBySemesterId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SemesterPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getSemesterId();
                break;
            case 1:
                return $this->getTahunAjaranId();
                break;
            case 2:
                return $this->getNama();
                break;
            case 3:
                return $this->getSemester();
                break;
            case 4:
                return $this->getPeriodeAktif();
                break;
            case 5:
                return $this->getTanggalMulai();
                break;
            case 6:
                return $this->getTanggalSelesai();
                break;
            case 7:
                return $this->getCreateDate();
                break;
            case 8:
                return $this->getLastUpdate();
                break;
            case 9:
                return $this->getExpiredDate();
                break;
            case 10:
                return $this->getLastSync();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Semester'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Semester'][$this->getPrimaryKey()] = true;
        $keys = SemesterPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getSemesterId(),
            $keys[1] => $this->getTahunAjaranId(),
            $keys[2] => $this->getNama(),
            $keys[3] => $this->getSemester(),
            $keys[4] => $this->getPeriodeAktif(),
            $keys[5] => $this->getTanggalMulai(),
            $keys[6] => $this->getTanggalSelesai(),
            $keys[7] => $this->getCreateDate(),
            $keys[8] => $this->getLastUpdate(),
            $keys[9] => $this->getExpiredDate(),
            $keys[10] => $this->getLastSync(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aTahunAjaran) {
                $result['TahunAjaran'] = $this->aTahunAjaran->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collSekolahLongitudinalsRelatedBySemesterId) {
                $result['SekolahLongitudinalsRelatedBySemesterId'] = $this->collSekolahLongitudinalsRelatedBySemesterId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSekolahLongitudinalsRelatedBySemesterId) {
                $result['SekolahLongitudinalsRelatedBySemesterId'] = $this->collSekolahLongitudinalsRelatedBySemesterId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPesertaDidikLongitudinalsRelatedBySemesterId) {
                $result['PesertaDidikLongitudinalsRelatedBySemesterId'] = $this->collPesertaDidikLongitudinalsRelatedBySemesterId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPesertaDidikLongitudinalsRelatedBySemesterId) {
                $result['PesertaDidikLongitudinalsRelatedBySemesterId'] = $this->collPesertaDidikLongitudinalsRelatedBySemesterId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSanitasisRelatedBySemesterId) {
                $result['SanitasisRelatedBySemesterId'] = $this->collSanitasisRelatedBySemesterId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSanitasisRelatedBySemesterId) {
                $result['SanitasisRelatedBySemesterId'] = $this->collSanitasisRelatedBySemesterId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSaranaLongitudinalsRelatedBySemesterId) {
                $result['SaranaLongitudinalsRelatedBySemesterId'] = $this->collSaranaLongitudinalsRelatedBySemesterId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSaranaLongitudinalsRelatedBySemesterId) {
                $result['SaranaLongitudinalsRelatedBySemesterId'] = $this->collSaranaLongitudinalsRelatedBySemesterId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->singleBatasWaktuRapor) {
                $result['BatasWaktuRapor'] = $this->singleBatasWaktuRapor->toArray($keyType, $includeLazyLoadColumns, $alreadyDumpedObjects, true);
            }
            if (null !== $this->collPembelajaransRelatedBySemesterId) {
                $result['PembelajaransRelatedBySemesterId'] = $this->collPembelajaransRelatedBySemesterId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPembelajaransRelatedBySemesterId) {
                $result['PembelajaransRelatedBySemesterId'] = $this->collPembelajaransRelatedBySemesterId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPrasaranaLongitudinalsRelatedBySemesterId) {
                $result['PrasaranaLongitudinalsRelatedBySemesterId'] = $this->collPrasaranaLongitudinalsRelatedBySemesterId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPrasaranaLongitudinalsRelatedBySemesterId) {
                $result['PrasaranaLongitudinalsRelatedBySemesterId'] = $this->collPrasaranaLongitudinalsRelatedBySemesterId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRombonganBelajarsRelatedBySemesterId) {
                $result['RombonganBelajarsRelatedBySemesterId'] = $this->collRombonganBelajarsRelatedBySemesterId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRombonganBelajarsRelatedBySemesterId) {
                $result['RombonganBelajarsRelatedBySemesterId'] = $this->collRombonganBelajarsRelatedBySemesterId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collBukuAlatLongitudinalsRelatedBySemesterId) {
                $result['BukuAlatLongitudinalsRelatedBySemesterId'] = $this->collBukuAlatLongitudinalsRelatedBySemesterId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collBukuAlatLongitudinalsRelatedBySemesterId) {
                $result['BukuAlatLongitudinalsRelatedBySemesterId'] = $this->collBukuAlatLongitudinalsRelatedBySemesterId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SemesterPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setSemesterId($value);
                break;
            case 1:
                $this->setTahunAjaranId($value);
                break;
            case 2:
                $this->setNama($value);
                break;
            case 3:
                $this->setSemester($value);
                break;
            case 4:
                $this->setPeriodeAktif($value);
                break;
            case 5:
                $this->setTanggalMulai($value);
                break;
            case 6:
                $this->setTanggalSelesai($value);
                break;
            case 7:
                $this->setCreateDate($value);
                break;
            case 8:
                $this->setLastUpdate($value);
                break;
            case 9:
                $this->setExpiredDate($value);
                break;
            case 10:
                $this->setLastSync($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = SemesterPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setSemesterId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setTahunAjaranId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setNama($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setSemester($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setPeriodeAktif($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setTanggalMulai($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setTanggalSelesai($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setCreateDate($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setLastUpdate($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setExpiredDate($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setLastSync($arr[$keys[10]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(SemesterPeer::DATABASE_NAME);

        if ($this->isColumnModified(SemesterPeer::SEMESTER_ID)) $criteria->add(SemesterPeer::SEMESTER_ID, $this->semester_id);
        if ($this->isColumnModified(SemesterPeer::TAHUN_AJARAN_ID)) $criteria->add(SemesterPeer::TAHUN_AJARAN_ID, $this->tahun_ajaran_id);
        if ($this->isColumnModified(SemesterPeer::NAMA)) $criteria->add(SemesterPeer::NAMA, $this->nama);
        if ($this->isColumnModified(SemesterPeer::SEMESTER)) $criteria->add(SemesterPeer::SEMESTER, $this->semester);
        if ($this->isColumnModified(SemesterPeer::PERIODE_AKTIF)) $criteria->add(SemesterPeer::PERIODE_AKTIF, $this->periode_aktif);
        if ($this->isColumnModified(SemesterPeer::TANGGAL_MULAI)) $criteria->add(SemesterPeer::TANGGAL_MULAI, $this->tanggal_mulai);
        if ($this->isColumnModified(SemesterPeer::TANGGAL_SELESAI)) $criteria->add(SemesterPeer::TANGGAL_SELESAI, $this->tanggal_selesai);
        if ($this->isColumnModified(SemesterPeer::CREATE_DATE)) $criteria->add(SemesterPeer::CREATE_DATE, $this->create_date);
        if ($this->isColumnModified(SemesterPeer::LAST_UPDATE)) $criteria->add(SemesterPeer::LAST_UPDATE, $this->last_update);
        if ($this->isColumnModified(SemesterPeer::EXPIRED_DATE)) $criteria->add(SemesterPeer::EXPIRED_DATE, $this->expired_date);
        if ($this->isColumnModified(SemesterPeer::LAST_SYNC)) $criteria->add(SemesterPeer::LAST_SYNC, $this->last_sync);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(SemesterPeer::DATABASE_NAME);
        $criteria->add(SemesterPeer::SEMESTER_ID, $this->semester_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getSemesterId();
    }

    /**
     * Generic method to set the primary key (semester_id column).
     *
     * @param  string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setSemesterId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getSemesterId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Semester (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setTahunAjaranId($this->getTahunAjaranId());
        $copyObj->setNama($this->getNama());
        $copyObj->setSemester($this->getSemester());
        $copyObj->setPeriodeAktif($this->getPeriodeAktif());
        $copyObj->setTanggalMulai($this->getTanggalMulai());
        $copyObj->setTanggalSelesai($this->getTanggalSelesai());
        $copyObj->setCreateDate($this->getCreateDate());
        $copyObj->setLastUpdate($this->getLastUpdate());
        $copyObj->setExpiredDate($this->getExpiredDate());
        $copyObj->setLastSync($this->getLastSync());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getSekolahLongitudinalsRelatedBySemesterId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSekolahLongitudinalRelatedBySemesterId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSekolahLongitudinalsRelatedBySemesterId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSekolahLongitudinalRelatedBySemesterId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPesertaDidikLongitudinalsRelatedBySemesterId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPesertaDidikLongitudinalRelatedBySemesterId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPesertaDidikLongitudinalsRelatedBySemesterId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPesertaDidikLongitudinalRelatedBySemesterId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSanitasisRelatedBySemesterId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSanitasiRelatedBySemesterId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSanitasisRelatedBySemesterId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSanitasiRelatedBySemesterId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSaranaLongitudinalsRelatedBySemesterId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSaranaLongitudinalRelatedBySemesterId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSaranaLongitudinalsRelatedBySemesterId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSaranaLongitudinalRelatedBySemesterId($relObj->copy($deepCopy));
                }
            }

            $relObj = $this->getBatasWaktuRapor();
            if ($relObj) {
                $copyObj->setBatasWaktuRapor($relObj->copy($deepCopy));
            }

            foreach ($this->getPembelajaransRelatedBySemesterId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPembelajaranRelatedBySemesterId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPembelajaransRelatedBySemesterId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPembelajaranRelatedBySemesterId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPrasaranaLongitudinalsRelatedBySemesterId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPrasaranaLongitudinalRelatedBySemesterId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPrasaranaLongitudinalsRelatedBySemesterId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPrasaranaLongitudinalRelatedBySemesterId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRombonganBelajarsRelatedBySemesterId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRombonganBelajarRelatedBySemesterId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRombonganBelajarsRelatedBySemesterId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRombonganBelajarRelatedBySemesterId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getBukuAlatLongitudinalsRelatedBySemesterId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBukuAlatLongitudinalRelatedBySemesterId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getBukuAlatLongitudinalsRelatedBySemesterId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBukuAlatLongitudinalRelatedBySemesterId($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setSemesterId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Semester Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return SemesterPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new SemesterPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a TahunAjaran object.
     *
     * @param             TahunAjaran $v
     * @return Semester The current object (for fluent API support)
     * @throws PropelException
     */
    public function setTahunAjaran(TahunAjaran $v = null)
    {
        if ($v === null) {
            $this->setTahunAjaranId(NULL);
        } else {
            $this->setTahunAjaranId($v->getTahunAjaranId());
        }

        $this->aTahunAjaran = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the TahunAjaran object, it will not be re-added.
        if ($v !== null) {
            $v->addSemester($this);
        }


        return $this;
    }


    /**
     * Get the associated TahunAjaran object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return TahunAjaran The associated TahunAjaran object.
     * @throws PropelException
     */
    public function getTahunAjaran(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aTahunAjaran === null && (($this->tahun_ajaran_id !== "" && $this->tahun_ajaran_id !== null)) && $doQuery) {
            $this->aTahunAjaran = TahunAjaranQuery::create()->findPk($this->tahun_ajaran_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aTahunAjaran->addSemesters($this);
             */
        }

        return $this->aTahunAjaran;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('SekolahLongitudinalRelatedBySemesterId' == $relationName) {
            $this->initSekolahLongitudinalsRelatedBySemesterId();
        }
        if ('SekolahLongitudinalRelatedBySemesterId' == $relationName) {
            $this->initSekolahLongitudinalsRelatedBySemesterId();
        }
        if ('PesertaDidikLongitudinalRelatedBySemesterId' == $relationName) {
            $this->initPesertaDidikLongitudinalsRelatedBySemesterId();
        }
        if ('PesertaDidikLongitudinalRelatedBySemesterId' == $relationName) {
            $this->initPesertaDidikLongitudinalsRelatedBySemesterId();
        }
        if ('SanitasiRelatedBySemesterId' == $relationName) {
            $this->initSanitasisRelatedBySemesterId();
        }
        if ('SanitasiRelatedBySemesterId' == $relationName) {
            $this->initSanitasisRelatedBySemesterId();
        }
        if ('SaranaLongitudinalRelatedBySemesterId' == $relationName) {
            $this->initSaranaLongitudinalsRelatedBySemesterId();
        }
        if ('SaranaLongitudinalRelatedBySemesterId' == $relationName) {
            $this->initSaranaLongitudinalsRelatedBySemesterId();
        }
        if ('PembelajaranRelatedBySemesterId' == $relationName) {
            $this->initPembelajaransRelatedBySemesterId();
        }
        if ('PembelajaranRelatedBySemesterId' == $relationName) {
            $this->initPembelajaransRelatedBySemesterId();
        }
        if ('PrasaranaLongitudinalRelatedBySemesterId' == $relationName) {
            $this->initPrasaranaLongitudinalsRelatedBySemesterId();
        }
        if ('PrasaranaLongitudinalRelatedBySemesterId' == $relationName) {
            $this->initPrasaranaLongitudinalsRelatedBySemesterId();
        }
        if ('RombonganBelajarRelatedBySemesterId' == $relationName) {
            $this->initRombonganBelajarsRelatedBySemesterId();
        }
        if ('RombonganBelajarRelatedBySemesterId' == $relationName) {
            $this->initRombonganBelajarsRelatedBySemesterId();
        }
        if ('BukuAlatLongitudinalRelatedBySemesterId' == $relationName) {
            $this->initBukuAlatLongitudinalsRelatedBySemesterId();
        }
        if ('BukuAlatLongitudinalRelatedBySemesterId' == $relationName) {
            $this->initBukuAlatLongitudinalsRelatedBySemesterId();
        }
    }

    /**
     * Clears out the collSekolahLongitudinalsRelatedBySemesterId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Semester The current object (for fluent API support)
     * @see        addSekolahLongitudinalsRelatedBySemesterId()
     */
    public function clearSekolahLongitudinalsRelatedBySemesterId()
    {
        $this->collSekolahLongitudinalsRelatedBySemesterId = null; // important to set this to null since that means it is uninitialized
        $this->collSekolahLongitudinalsRelatedBySemesterIdPartial = null;

        return $this;
    }

    /**
     * reset is the collSekolahLongitudinalsRelatedBySemesterId collection loaded partially
     *
     * @return void
     */
    public function resetPartialSekolahLongitudinalsRelatedBySemesterId($v = true)
    {
        $this->collSekolahLongitudinalsRelatedBySemesterIdPartial = $v;
    }

    /**
     * Initializes the collSekolahLongitudinalsRelatedBySemesterId collection.
     *
     * By default this just sets the collSekolahLongitudinalsRelatedBySemesterId collection to an empty array (like clearcollSekolahLongitudinalsRelatedBySemesterId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSekolahLongitudinalsRelatedBySemesterId($overrideExisting = true)
    {
        if (null !== $this->collSekolahLongitudinalsRelatedBySemesterId && !$overrideExisting) {
            return;
        }
        $this->collSekolahLongitudinalsRelatedBySemesterId = new PropelObjectCollection();
        $this->collSekolahLongitudinalsRelatedBySemesterId->setModel('SekolahLongitudinal');
    }

    /**
     * Gets an array of SekolahLongitudinal objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Semester is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SekolahLongitudinal[] List of SekolahLongitudinal objects
     * @throws PropelException
     */
    public function getSekolahLongitudinalsRelatedBySemesterId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSekolahLongitudinalsRelatedBySemesterIdPartial && !$this->isNew();
        if (null === $this->collSekolahLongitudinalsRelatedBySemesterId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSekolahLongitudinalsRelatedBySemesterId) {
                // return empty collection
                $this->initSekolahLongitudinalsRelatedBySemesterId();
            } else {
                $collSekolahLongitudinalsRelatedBySemesterId = SekolahLongitudinalQuery::create(null, $criteria)
                    ->filterBySemesterRelatedBySemesterId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSekolahLongitudinalsRelatedBySemesterIdPartial && count($collSekolahLongitudinalsRelatedBySemesterId)) {
                      $this->initSekolahLongitudinalsRelatedBySemesterId(false);

                      foreach($collSekolahLongitudinalsRelatedBySemesterId as $obj) {
                        if (false == $this->collSekolahLongitudinalsRelatedBySemesterId->contains($obj)) {
                          $this->collSekolahLongitudinalsRelatedBySemesterId->append($obj);
                        }
                      }

                      $this->collSekolahLongitudinalsRelatedBySemesterIdPartial = true;
                    }

                    $collSekolahLongitudinalsRelatedBySemesterId->getInternalIterator()->rewind();
                    return $collSekolahLongitudinalsRelatedBySemesterId;
                }

                if($partial && $this->collSekolahLongitudinalsRelatedBySemesterId) {
                    foreach($this->collSekolahLongitudinalsRelatedBySemesterId as $obj) {
                        if($obj->isNew()) {
                            $collSekolahLongitudinalsRelatedBySemesterId[] = $obj;
                        }
                    }
                }

                $this->collSekolahLongitudinalsRelatedBySemesterId = $collSekolahLongitudinalsRelatedBySemesterId;
                $this->collSekolahLongitudinalsRelatedBySemesterIdPartial = false;
            }
        }

        return $this->collSekolahLongitudinalsRelatedBySemesterId;
    }

    /**
     * Sets a collection of SekolahLongitudinalRelatedBySemesterId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $sekolahLongitudinalsRelatedBySemesterId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Semester The current object (for fluent API support)
     */
    public function setSekolahLongitudinalsRelatedBySemesterId(PropelCollection $sekolahLongitudinalsRelatedBySemesterId, PropelPDO $con = null)
    {
        $sekolahLongitudinalsRelatedBySemesterIdToDelete = $this->getSekolahLongitudinalsRelatedBySemesterId(new Criteria(), $con)->diff($sekolahLongitudinalsRelatedBySemesterId);

        $this->sekolahLongitudinalsRelatedBySemesterIdScheduledForDeletion = unserialize(serialize($sekolahLongitudinalsRelatedBySemesterIdToDelete));

        foreach ($sekolahLongitudinalsRelatedBySemesterIdToDelete as $sekolahLongitudinalRelatedBySemesterIdRemoved) {
            $sekolahLongitudinalRelatedBySemesterIdRemoved->setSemesterRelatedBySemesterId(null);
        }

        $this->collSekolahLongitudinalsRelatedBySemesterId = null;
        foreach ($sekolahLongitudinalsRelatedBySemesterId as $sekolahLongitudinalRelatedBySemesterId) {
            $this->addSekolahLongitudinalRelatedBySemesterId($sekolahLongitudinalRelatedBySemesterId);
        }

        $this->collSekolahLongitudinalsRelatedBySemesterId = $sekolahLongitudinalsRelatedBySemesterId;
        $this->collSekolahLongitudinalsRelatedBySemesterIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SekolahLongitudinal objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SekolahLongitudinal objects.
     * @throws PropelException
     */
    public function countSekolahLongitudinalsRelatedBySemesterId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSekolahLongitudinalsRelatedBySemesterIdPartial && !$this->isNew();
        if (null === $this->collSekolahLongitudinalsRelatedBySemesterId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSekolahLongitudinalsRelatedBySemesterId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSekolahLongitudinalsRelatedBySemesterId());
            }
            $query = SekolahLongitudinalQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySemesterRelatedBySemesterId($this)
                ->count($con);
        }

        return count($this->collSekolahLongitudinalsRelatedBySemesterId);
    }

    /**
     * Method called to associate a SekolahLongitudinal object to this object
     * through the SekolahLongitudinal foreign key attribute.
     *
     * @param    SekolahLongitudinal $l SekolahLongitudinal
     * @return Semester The current object (for fluent API support)
     */
    public function addSekolahLongitudinalRelatedBySemesterId(SekolahLongitudinal $l)
    {
        if ($this->collSekolahLongitudinalsRelatedBySemesterId === null) {
            $this->initSekolahLongitudinalsRelatedBySemesterId();
            $this->collSekolahLongitudinalsRelatedBySemesterIdPartial = true;
        }
        if (!in_array($l, $this->collSekolahLongitudinalsRelatedBySemesterId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSekolahLongitudinalRelatedBySemesterId($l);
        }

        return $this;
    }

    /**
     * @param	SekolahLongitudinalRelatedBySemesterId $sekolahLongitudinalRelatedBySemesterId The sekolahLongitudinalRelatedBySemesterId object to add.
     */
    protected function doAddSekolahLongitudinalRelatedBySemesterId($sekolahLongitudinalRelatedBySemesterId)
    {
        $this->collSekolahLongitudinalsRelatedBySemesterId[]= $sekolahLongitudinalRelatedBySemesterId;
        $sekolahLongitudinalRelatedBySemesterId->setSemesterRelatedBySemesterId($this);
    }

    /**
     * @param	SekolahLongitudinalRelatedBySemesterId $sekolahLongitudinalRelatedBySemesterId The sekolahLongitudinalRelatedBySemesterId object to remove.
     * @return Semester The current object (for fluent API support)
     */
    public function removeSekolahLongitudinalRelatedBySemesterId($sekolahLongitudinalRelatedBySemesterId)
    {
        if ($this->getSekolahLongitudinalsRelatedBySemesterId()->contains($sekolahLongitudinalRelatedBySemesterId)) {
            $this->collSekolahLongitudinalsRelatedBySemesterId->remove($this->collSekolahLongitudinalsRelatedBySemesterId->search($sekolahLongitudinalRelatedBySemesterId));
            if (null === $this->sekolahLongitudinalsRelatedBySemesterIdScheduledForDeletion) {
                $this->sekolahLongitudinalsRelatedBySemesterIdScheduledForDeletion = clone $this->collSekolahLongitudinalsRelatedBySemesterId;
                $this->sekolahLongitudinalsRelatedBySemesterIdScheduledForDeletion->clear();
            }
            $this->sekolahLongitudinalsRelatedBySemesterIdScheduledForDeletion[]= clone $sekolahLongitudinalRelatedBySemesterId;
            $sekolahLongitudinalRelatedBySemesterId->setSemesterRelatedBySemesterId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related SekolahLongitudinalsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SekolahLongitudinal[] List of SekolahLongitudinal objects
     */
    public function getSekolahLongitudinalsRelatedBySemesterIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SekolahLongitudinalQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getSekolahLongitudinalsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related SekolahLongitudinalsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SekolahLongitudinal[] List of SekolahLongitudinal objects
     */
    public function getSekolahLongitudinalsRelatedBySemesterIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SekolahLongitudinalQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getSekolahLongitudinalsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related SekolahLongitudinalsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SekolahLongitudinal[] List of SekolahLongitudinal objects
     */
    public function getSekolahLongitudinalsRelatedBySemesterIdJoinAksesInternetRelatedByAksesInternetId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SekolahLongitudinalQuery::create(null, $criteria);
        $query->joinWith('AksesInternetRelatedByAksesInternetId', $join_behavior);

        return $this->getSekolahLongitudinalsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related SekolahLongitudinalsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SekolahLongitudinal[] List of SekolahLongitudinal objects
     */
    public function getSekolahLongitudinalsRelatedBySemesterIdJoinAksesInternetRelatedByAksesInternet2Id($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SekolahLongitudinalQuery::create(null, $criteria);
        $query->joinWith('AksesInternetRelatedByAksesInternet2Id', $join_behavior);

        return $this->getSekolahLongitudinalsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related SekolahLongitudinalsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SekolahLongitudinal[] List of SekolahLongitudinal objects
     */
    public function getSekolahLongitudinalsRelatedBySemesterIdJoinAksesInternetRelatedByAksesInternetId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SekolahLongitudinalQuery::create(null, $criteria);
        $query->joinWith('AksesInternetRelatedByAksesInternetId', $join_behavior);

        return $this->getSekolahLongitudinalsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related SekolahLongitudinalsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SekolahLongitudinal[] List of SekolahLongitudinal objects
     */
    public function getSekolahLongitudinalsRelatedBySemesterIdJoinAksesInternetRelatedByAksesInternet2Id($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SekolahLongitudinalQuery::create(null, $criteria);
        $query->joinWith('AksesInternetRelatedByAksesInternet2Id', $join_behavior);

        return $this->getSekolahLongitudinalsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related SekolahLongitudinalsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SekolahLongitudinal[] List of SekolahLongitudinal objects
     */
    public function getSekolahLongitudinalsRelatedBySemesterIdJoinSertifikasiIsoRelatedBySertifikasiIsoId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SekolahLongitudinalQuery::create(null, $criteria);
        $query->joinWith('SertifikasiIsoRelatedBySertifikasiIsoId', $join_behavior);

        return $this->getSekolahLongitudinalsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related SekolahLongitudinalsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SekolahLongitudinal[] List of SekolahLongitudinal objects
     */
    public function getSekolahLongitudinalsRelatedBySemesterIdJoinSertifikasiIsoRelatedBySertifikasiIsoId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SekolahLongitudinalQuery::create(null, $criteria);
        $query->joinWith('SertifikasiIsoRelatedBySertifikasiIsoId', $join_behavior);

        return $this->getSekolahLongitudinalsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related SekolahLongitudinalsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SekolahLongitudinal[] List of SekolahLongitudinal objects
     */
    public function getSekolahLongitudinalsRelatedBySemesterIdJoinSumberListrikRelatedBySumberListrikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SekolahLongitudinalQuery::create(null, $criteria);
        $query->joinWith('SumberListrikRelatedBySumberListrikId', $join_behavior);

        return $this->getSekolahLongitudinalsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related SekolahLongitudinalsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SekolahLongitudinal[] List of SekolahLongitudinal objects
     */
    public function getSekolahLongitudinalsRelatedBySemesterIdJoinSumberListrikRelatedBySumberListrikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SekolahLongitudinalQuery::create(null, $criteria);
        $query->joinWith('SumberListrikRelatedBySumberListrikId', $join_behavior);

        return $this->getSekolahLongitudinalsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related SekolahLongitudinalsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SekolahLongitudinal[] List of SekolahLongitudinal objects
     */
    public function getSekolahLongitudinalsRelatedBySemesterIdJoinWaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SekolahLongitudinalQuery::create(null, $criteria);
        $query->joinWith('WaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId', $join_behavior);

        return $this->getSekolahLongitudinalsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related SekolahLongitudinalsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SekolahLongitudinal[] List of SekolahLongitudinal objects
     */
    public function getSekolahLongitudinalsRelatedBySemesterIdJoinWaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SekolahLongitudinalQuery::create(null, $criteria);
        $query->joinWith('WaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId', $join_behavior);

        return $this->getSekolahLongitudinalsRelatedBySemesterId($query, $con);
    }

    /**
     * Clears out the collSekolahLongitudinalsRelatedBySemesterId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Semester The current object (for fluent API support)
     * @see        addSekolahLongitudinalsRelatedBySemesterId()
     */
    public function clearSekolahLongitudinalsRelatedBySemesterId()
    {
        $this->collSekolahLongitudinalsRelatedBySemesterId = null; // important to set this to null since that means it is uninitialized
        $this->collSekolahLongitudinalsRelatedBySemesterIdPartial = null;

        return $this;
    }

    /**
     * reset is the collSekolahLongitudinalsRelatedBySemesterId collection loaded partially
     *
     * @return void
     */
    public function resetPartialSekolahLongitudinalsRelatedBySemesterId($v = true)
    {
        $this->collSekolahLongitudinalsRelatedBySemesterIdPartial = $v;
    }

    /**
     * Initializes the collSekolahLongitudinalsRelatedBySemesterId collection.
     *
     * By default this just sets the collSekolahLongitudinalsRelatedBySemesterId collection to an empty array (like clearcollSekolahLongitudinalsRelatedBySemesterId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSekolahLongitudinalsRelatedBySemesterId($overrideExisting = true)
    {
        if (null !== $this->collSekolahLongitudinalsRelatedBySemesterId && !$overrideExisting) {
            return;
        }
        $this->collSekolahLongitudinalsRelatedBySemesterId = new PropelObjectCollection();
        $this->collSekolahLongitudinalsRelatedBySemesterId->setModel('SekolahLongitudinal');
    }

    /**
     * Gets an array of SekolahLongitudinal objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Semester is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SekolahLongitudinal[] List of SekolahLongitudinal objects
     * @throws PropelException
     */
    public function getSekolahLongitudinalsRelatedBySemesterId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSekolahLongitudinalsRelatedBySemesterIdPartial && !$this->isNew();
        if (null === $this->collSekolahLongitudinalsRelatedBySemesterId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSekolahLongitudinalsRelatedBySemesterId) {
                // return empty collection
                $this->initSekolahLongitudinalsRelatedBySemesterId();
            } else {
                $collSekolahLongitudinalsRelatedBySemesterId = SekolahLongitudinalQuery::create(null, $criteria)
                    ->filterBySemesterRelatedBySemesterId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSekolahLongitudinalsRelatedBySemesterIdPartial && count($collSekolahLongitudinalsRelatedBySemesterId)) {
                      $this->initSekolahLongitudinalsRelatedBySemesterId(false);

                      foreach($collSekolahLongitudinalsRelatedBySemesterId as $obj) {
                        if (false == $this->collSekolahLongitudinalsRelatedBySemesterId->contains($obj)) {
                          $this->collSekolahLongitudinalsRelatedBySemesterId->append($obj);
                        }
                      }

                      $this->collSekolahLongitudinalsRelatedBySemesterIdPartial = true;
                    }

                    $collSekolahLongitudinalsRelatedBySemesterId->getInternalIterator()->rewind();
                    return $collSekolahLongitudinalsRelatedBySemesterId;
                }

                if($partial && $this->collSekolahLongitudinalsRelatedBySemesterId) {
                    foreach($this->collSekolahLongitudinalsRelatedBySemesterId as $obj) {
                        if($obj->isNew()) {
                            $collSekolahLongitudinalsRelatedBySemesterId[] = $obj;
                        }
                    }
                }

                $this->collSekolahLongitudinalsRelatedBySemesterId = $collSekolahLongitudinalsRelatedBySemesterId;
                $this->collSekolahLongitudinalsRelatedBySemesterIdPartial = false;
            }
        }

        return $this->collSekolahLongitudinalsRelatedBySemesterId;
    }

    /**
     * Sets a collection of SekolahLongitudinalRelatedBySemesterId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $sekolahLongitudinalsRelatedBySemesterId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Semester The current object (for fluent API support)
     */
    public function setSekolahLongitudinalsRelatedBySemesterId(PropelCollection $sekolahLongitudinalsRelatedBySemesterId, PropelPDO $con = null)
    {
        $sekolahLongitudinalsRelatedBySemesterIdToDelete = $this->getSekolahLongitudinalsRelatedBySemesterId(new Criteria(), $con)->diff($sekolahLongitudinalsRelatedBySemesterId);

        $this->sekolahLongitudinalsRelatedBySemesterIdScheduledForDeletion = unserialize(serialize($sekolahLongitudinalsRelatedBySemesterIdToDelete));

        foreach ($sekolahLongitudinalsRelatedBySemesterIdToDelete as $sekolahLongitudinalRelatedBySemesterIdRemoved) {
            $sekolahLongitudinalRelatedBySemesterIdRemoved->setSemesterRelatedBySemesterId(null);
        }

        $this->collSekolahLongitudinalsRelatedBySemesterId = null;
        foreach ($sekolahLongitudinalsRelatedBySemesterId as $sekolahLongitudinalRelatedBySemesterId) {
            $this->addSekolahLongitudinalRelatedBySemesterId($sekolahLongitudinalRelatedBySemesterId);
        }

        $this->collSekolahLongitudinalsRelatedBySemesterId = $sekolahLongitudinalsRelatedBySemesterId;
        $this->collSekolahLongitudinalsRelatedBySemesterIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SekolahLongitudinal objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SekolahLongitudinal objects.
     * @throws PropelException
     */
    public function countSekolahLongitudinalsRelatedBySemesterId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSekolahLongitudinalsRelatedBySemesterIdPartial && !$this->isNew();
        if (null === $this->collSekolahLongitudinalsRelatedBySemesterId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSekolahLongitudinalsRelatedBySemesterId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSekolahLongitudinalsRelatedBySemesterId());
            }
            $query = SekolahLongitudinalQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySemesterRelatedBySemesterId($this)
                ->count($con);
        }

        return count($this->collSekolahLongitudinalsRelatedBySemesterId);
    }

    /**
     * Method called to associate a SekolahLongitudinal object to this object
     * through the SekolahLongitudinal foreign key attribute.
     *
     * @param    SekolahLongitudinal $l SekolahLongitudinal
     * @return Semester The current object (for fluent API support)
     */
    public function addSekolahLongitudinalRelatedBySemesterId(SekolahLongitudinal $l)
    {
        if ($this->collSekolahLongitudinalsRelatedBySemesterId === null) {
            $this->initSekolahLongitudinalsRelatedBySemesterId();
            $this->collSekolahLongitudinalsRelatedBySemesterIdPartial = true;
        }
        if (!in_array($l, $this->collSekolahLongitudinalsRelatedBySemesterId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSekolahLongitudinalRelatedBySemesterId($l);
        }

        return $this;
    }

    /**
     * @param	SekolahLongitudinalRelatedBySemesterId $sekolahLongitudinalRelatedBySemesterId The sekolahLongitudinalRelatedBySemesterId object to add.
     */
    protected function doAddSekolahLongitudinalRelatedBySemesterId($sekolahLongitudinalRelatedBySemesterId)
    {
        $this->collSekolahLongitudinalsRelatedBySemesterId[]= $sekolahLongitudinalRelatedBySemesterId;
        $sekolahLongitudinalRelatedBySemesterId->setSemesterRelatedBySemesterId($this);
    }

    /**
     * @param	SekolahLongitudinalRelatedBySemesterId $sekolahLongitudinalRelatedBySemesterId The sekolahLongitudinalRelatedBySemesterId object to remove.
     * @return Semester The current object (for fluent API support)
     */
    public function removeSekolahLongitudinalRelatedBySemesterId($sekolahLongitudinalRelatedBySemesterId)
    {
        if ($this->getSekolahLongitudinalsRelatedBySemesterId()->contains($sekolahLongitudinalRelatedBySemesterId)) {
            $this->collSekolahLongitudinalsRelatedBySemesterId->remove($this->collSekolahLongitudinalsRelatedBySemesterId->search($sekolahLongitudinalRelatedBySemesterId));
            if (null === $this->sekolahLongitudinalsRelatedBySemesterIdScheduledForDeletion) {
                $this->sekolahLongitudinalsRelatedBySemesterIdScheduledForDeletion = clone $this->collSekolahLongitudinalsRelatedBySemesterId;
                $this->sekolahLongitudinalsRelatedBySemesterIdScheduledForDeletion->clear();
            }
            $this->sekolahLongitudinalsRelatedBySemesterIdScheduledForDeletion[]= clone $sekolahLongitudinalRelatedBySemesterId;
            $sekolahLongitudinalRelatedBySemesterId->setSemesterRelatedBySemesterId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related SekolahLongitudinalsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SekolahLongitudinal[] List of SekolahLongitudinal objects
     */
    public function getSekolahLongitudinalsRelatedBySemesterIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SekolahLongitudinalQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getSekolahLongitudinalsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related SekolahLongitudinalsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SekolahLongitudinal[] List of SekolahLongitudinal objects
     */
    public function getSekolahLongitudinalsRelatedBySemesterIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SekolahLongitudinalQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getSekolahLongitudinalsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related SekolahLongitudinalsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SekolahLongitudinal[] List of SekolahLongitudinal objects
     */
    public function getSekolahLongitudinalsRelatedBySemesterIdJoinAksesInternetRelatedByAksesInternetId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SekolahLongitudinalQuery::create(null, $criteria);
        $query->joinWith('AksesInternetRelatedByAksesInternetId', $join_behavior);

        return $this->getSekolahLongitudinalsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related SekolahLongitudinalsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SekolahLongitudinal[] List of SekolahLongitudinal objects
     */
    public function getSekolahLongitudinalsRelatedBySemesterIdJoinAksesInternetRelatedByAksesInternet2Id($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SekolahLongitudinalQuery::create(null, $criteria);
        $query->joinWith('AksesInternetRelatedByAksesInternet2Id', $join_behavior);

        return $this->getSekolahLongitudinalsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related SekolahLongitudinalsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SekolahLongitudinal[] List of SekolahLongitudinal objects
     */
    public function getSekolahLongitudinalsRelatedBySemesterIdJoinAksesInternetRelatedByAksesInternetId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SekolahLongitudinalQuery::create(null, $criteria);
        $query->joinWith('AksesInternetRelatedByAksesInternetId', $join_behavior);

        return $this->getSekolahLongitudinalsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related SekolahLongitudinalsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SekolahLongitudinal[] List of SekolahLongitudinal objects
     */
    public function getSekolahLongitudinalsRelatedBySemesterIdJoinAksesInternetRelatedByAksesInternet2Id($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SekolahLongitudinalQuery::create(null, $criteria);
        $query->joinWith('AksesInternetRelatedByAksesInternet2Id', $join_behavior);

        return $this->getSekolahLongitudinalsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related SekolahLongitudinalsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SekolahLongitudinal[] List of SekolahLongitudinal objects
     */
    public function getSekolahLongitudinalsRelatedBySemesterIdJoinSertifikasiIsoRelatedBySertifikasiIsoId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SekolahLongitudinalQuery::create(null, $criteria);
        $query->joinWith('SertifikasiIsoRelatedBySertifikasiIsoId', $join_behavior);

        return $this->getSekolahLongitudinalsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related SekolahLongitudinalsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SekolahLongitudinal[] List of SekolahLongitudinal objects
     */
    public function getSekolahLongitudinalsRelatedBySemesterIdJoinSertifikasiIsoRelatedBySertifikasiIsoId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SekolahLongitudinalQuery::create(null, $criteria);
        $query->joinWith('SertifikasiIsoRelatedBySertifikasiIsoId', $join_behavior);

        return $this->getSekolahLongitudinalsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related SekolahLongitudinalsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SekolahLongitudinal[] List of SekolahLongitudinal objects
     */
    public function getSekolahLongitudinalsRelatedBySemesterIdJoinSumberListrikRelatedBySumberListrikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SekolahLongitudinalQuery::create(null, $criteria);
        $query->joinWith('SumberListrikRelatedBySumberListrikId', $join_behavior);

        return $this->getSekolahLongitudinalsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related SekolahLongitudinalsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SekolahLongitudinal[] List of SekolahLongitudinal objects
     */
    public function getSekolahLongitudinalsRelatedBySemesterIdJoinSumberListrikRelatedBySumberListrikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SekolahLongitudinalQuery::create(null, $criteria);
        $query->joinWith('SumberListrikRelatedBySumberListrikId', $join_behavior);

        return $this->getSekolahLongitudinalsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related SekolahLongitudinalsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SekolahLongitudinal[] List of SekolahLongitudinal objects
     */
    public function getSekolahLongitudinalsRelatedBySemesterIdJoinWaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SekolahLongitudinalQuery::create(null, $criteria);
        $query->joinWith('WaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId', $join_behavior);

        return $this->getSekolahLongitudinalsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related SekolahLongitudinalsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SekolahLongitudinal[] List of SekolahLongitudinal objects
     */
    public function getSekolahLongitudinalsRelatedBySemesterIdJoinWaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SekolahLongitudinalQuery::create(null, $criteria);
        $query->joinWith('WaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId', $join_behavior);

        return $this->getSekolahLongitudinalsRelatedBySemesterId($query, $con);
    }

    /**
     * Clears out the collPesertaDidikLongitudinalsRelatedBySemesterId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Semester The current object (for fluent API support)
     * @see        addPesertaDidikLongitudinalsRelatedBySemesterId()
     */
    public function clearPesertaDidikLongitudinalsRelatedBySemesterId()
    {
        $this->collPesertaDidikLongitudinalsRelatedBySemesterId = null; // important to set this to null since that means it is uninitialized
        $this->collPesertaDidikLongitudinalsRelatedBySemesterIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPesertaDidikLongitudinalsRelatedBySemesterId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPesertaDidikLongitudinalsRelatedBySemesterId($v = true)
    {
        $this->collPesertaDidikLongitudinalsRelatedBySemesterIdPartial = $v;
    }

    /**
     * Initializes the collPesertaDidikLongitudinalsRelatedBySemesterId collection.
     *
     * By default this just sets the collPesertaDidikLongitudinalsRelatedBySemesterId collection to an empty array (like clearcollPesertaDidikLongitudinalsRelatedBySemesterId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPesertaDidikLongitudinalsRelatedBySemesterId($overrideExisting = true)
    {
        if (null !== $this->collPesertaDidikLongitudinalsRelatedBySemesterId && !$overrideExisting) {
            return;
        }
        $this->collPesertaDidikLongitudinalsRelatedBySemesterId = new PropelObjectCollection();
        $this->collPesertaDidikLongitudinalsRelatedBySemesterId->setModel('PesertaDidikLongitudinal');
    }

    /**
     * Gets an array of PesertaDidikLongitudinal objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Semester is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|PesertaDidikLongitudinal[] List of PesertaDidikLongitudinal objects
     * @throws PropelException
     */
    public function getPesertaDidikLongitudinalsRelatedBySemesterId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPesertaDidikLongitudinalsRelatedBySemesterIdPartial && !$this->isNew();
        if (null === $this->collPesertaDidikLongitudinalsRelatedBySemesterId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPesertaDidikLongitudinalsRelatedBySemesterId) {
                // return empty collection
                $this->initPesertaDidikLongitudinalsRelatedBySemesterId();
            } else {
                $collPesertaDidikLongitudinalsRelatedBySemesterId = PesertaDidikLongitudinalQuery::create(null, $criteria)
                    ->filterBySemesterRelatedBySemesterId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPesertaDidikLongitudinalsRelatedBySemesterIdPartial && count($collPesertaDidikLongitudinalsRelatedBySemesterId)) {
                      $this->initPesertaDidikLongitudinalsRelatedBySemesterId(false);

                      foreach($collPesertaDidikLongitudinalsRelatedBySemesterId as $obj) {
                        if (false == $this->collPesertaDidikLongitudinalsRelatedBySemesterId->contains($obj)) {
                          $this->collPesertaDidikLongitudinalsRelatedBySemesterId->append($obj);
                        }
                      }

                      $this->collPesertaDidikLongitudinalsRelatedBySemesterIdPartial = true;
                    }

                    $collPesertaDidikLongitudinalsRelatedBySemesterId->getInternalIterator()->rewind();
                    return $collPesertaDidikLongitudinalsRelatedBySemesterId;
                }

                if($partial && $this->collPesertaDidikLongitudinalsRelatedBySemesterId) {
                    foreach($this->collPesertaDidikLongitudinalsRelatedBySemesterId as $obj) {
                        if($obj->isNew()) {
                            $collPesertaDidikLongitudinalsRelatedBySemesterId[] = $obj;
                        }
                    }
                }

                $this->collPesertaDidikLongitudinalsRelatedBySemesterId = $collPesertaDidikLongitudinalsRelatedBySemesterId;
                $this->collPesertaDidikLongitudinalsRelatedBySemesterIdPartial = false;
            }
        }

        return $this->collPesertaDidikLongitudinalsRelatedBySemesterId;
    }

    /**
     * Sets a collection of PesertaDidikLongitudinalRelatedBySemesterId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $pesertaDidikLongitudinalsRelatedBySemesterId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Semester The current object (for fluent API support)
     */
    public function setPesertaDidikLongitudinalsRelatedBySemesterId(PropelCollection $pesertaDidikLongitudinalsRelatedBySemesterId, PropelPDO $con = null)
    {
        $pesertaDidikLongitudinalsRelatedBySemesterIdToDelete = $this->getPesertaDidikLongitudinalsRelatedBySemesterId(new Criteria(), $con)->diff($pesertaDidikLongitudinalsRelatedBySemesterId);

        $this->pesertaDidikLongitudinalsRelatedBySemesterIdScheduledForDeletion = unserialize(serialize($pesertaDidikLongitudinalsRelatedBySemesterIdToDelete));

        foreach ($pesertaDidikLongitudinalsRelatedBySemesterIdToDelete as $pesertaDidikLongitudinalRelatedBySemesterIdRemoved) {
            $pesertaDidikLongitudinalRelatedBySemesterIdRemoved->setSemesterRelatedBySemesterId(null);
        }

        $this->collPesertaDidikLongitudinalsRelatedBySemesterId = null;
        foreach ($pesertaDidikLongitudinalsRelatedBySemesterId as $pesertaDidikLongitudinalRelatedBySemesterId) {
            $this->addPesertaDidikLongitudinalRelatedBySemesterId($pesertaDidikLongitudinalRelatedBySemesterId);
        }

        $this->collPesertaDidikLongitudinalsRelatedBySemesterId = $pesertaDidikLongitudinalsRelatedBySemesterId;
        $this->collPesertaDidikLongitudinalsRelatedBySemesterIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PesertaDidikLongitudinal objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related PesertaDidikLongitudinal objects.
     * @throws PropelException
     */
    public function countPesertaDidikLongitudinalsRelatedBySemesterId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPesertaDidikLongitudinalsRelatedBySemesterIdPartial && !$this->isNew();
        if (null === $this->collPesertaDidikLongitudinalsRelatedBySemesterId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPesertaDidikLongitudinalsRelatedBySemesterId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPesertaDidikLongitudinalsRelatedBySemesterId());
            }
            $query = PesertaDidikLongitudinalQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySemesterRelatedBySemesterId($this)
                ->count($con);
        }

        return count($this->collPesertaDidikLongitudinalsRelatedBySemesterId);
    }

    /**
     * Method called to associate a PesertaDidikLongitudinal object to this object
     * through the PesertaDidikLongitudinal foreign key attribute.
     *
     * @param    PesertaDidikLongitudinal $l PesertaDidikLongitudinal
     * @return Semester The current object (for fluent API support)
     */
    public function addPesertaDidikLongitudinalRelatedBySemesterId(PesertaDidikLongitudinal $l)
    {
        if ($this->collPesertaDidikLongitudinalsRelatedBySemesterId === null) {
            $this->initPesertaDidikLongitudinalsRelatedBySemesterId();
            $this->collPesertaDidikLongitudinalsRelatedBySemesterIdPartial = true;
        }
        if (!in_array($l, $this->collPesertaDidikLongitudinalsRelatedBySemesterId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPesertaDidikLongitudinalRelatedBySemesterId($l);
        }

        return $this;
    }

    /**
     * @param	PesertaDidikLongitudinalRelatedBySemesterId $pesertaDidikLongitudinalRelatedBySemesterId The pesertaDidikLongitudinalRelatedBySemesterId object to add.
     */
    protected function doAddPesertaDidikLongitudinalRelatedBySemesterId($pesertaDidikLongitudinalRelatedBySemesterId)
    {
        $this->collPesertaDidikLongitudinalsRelatedBySemesterId[]= $pesertaDidikLongitudinalRelatedBySemesterId;
        $pesertaDidikLongitudinalRelatedBySemesterId->setSemesterRelatedBySemesterId($this);
    }

    /**
     * @param	PesertaDidikLongitudinalRelatedBySemesterId $pesertaDidikLongitudinalRelatedBySemesterId The pesertaDidikLongitudinalRelatedBySemesterId object to remove.
     * @return Semester The current object (for fluent API support)
     */
    public function removePesertaDidikLongitudinalRelatedBySemesterId($pesertaDidikLongitudinalRelatedBySemesterId)
    {
        if ($this->getPesertaDidikLongitudinalsRelatedBySemesterId()->contains($pesertaDidikLongitudinalRelatedBySemesterId)) {
            $this->collPesertaDidikLongitudinalsRelatedBySemesterId->remove($this->collPesertaDidikLongitudinalsRelatedBySemesterId->search($pesertaDidikLongitudinalRelatedBySemesterId));
            if (null === $this->pesertaDidikLongitudinalsRelatedBySemesterIdScheduledForDeletion) {
                $this->pesertaDidikLongitudinalsRelatedBySemesterIdScheduledForDeletion = clone $this->collPesertaDidikLongitudinalsRelatedBySemesterId;
                $this->pesertaDidikLongitudinalsRelatedBySemesterIdScheduledForDeletion->clear();
            }
            $this->pesertaDidikLongitudinalsRelatedBySemesterIdScheduledForDeletion[]= clone $pesertaDidikLongitudinalRelatedBySemesterId;
            $pesertaDidikLongitudinalRelatedBySemesterId->setSemesterRelatedBySemesterId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related PesertaDidikLongitudinalsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidikLongitudinal[] List of PesertaDidikLongitudinal objects
     */
    public function getPesertaDidikLongitudinalsRelatedBySemesterIdJoinPesertaDidikRelatedByPesertaDidikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikLongitudinalQuery::create(null, $criteria);
        $query->joinWith('PesertaDidikRelatedByPesertaDidikId', $join_behavior);

        return $this->getPesertaDidikLongitudinalsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related PesertaDidikLongitudinalsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidikLongitudinal[] List of PesertaDidikLongitudinal objects
     */
    public function getPesertaDidikLongitudinalsRelatedBySemesterIdJoinPesertaDidikRelatedByPesertaDidikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikLongitudinalQuery::create(null, $criteria);
        $query->joinWith('PesertaDidikRelatedByPesertaDidikId', $join_behavior);

        return $this->getPesertaDidikLongitudinalsRelatedBySemesterId($query, $con);
    }

    /**
     * Clears out the collPesertaDidikLongitudinalsRelatedBySemesterId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Semester The current object (for fluent API support)
     * @see        addPesertaDidikLongitudinalsRelatedBySemesterId()
     */
    public function clearPesertaDidikLongitudinalsRelatedBySemesterId()
    {
        $this->collPesertaDidikLongitudinalsRelatedBySemesterId = null; // important to set this to null since that means it is uninitialized
        $this->collPesertaDidikLongitudinalsRelatedBySemesterIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPesertaDidikLongitudinalsRelatedBySemesterId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPesertaDidikLongitudinalsRelatedBySemesterId($v = true)
    {
        $this->collPesertaDidikLongitudinalsRelatedBySemesterIdPartial = $v;
    }

    /**
     * Initializes the collPesertaDidikLongitudinalsRelatedBySemesterId collection.
     *
     * By default this just sets the collPesertaDidikLongitudinalsRelatedBySemesterId collection to an empty array (like clearcollPesertaDidikLongitudinalsRelatedBySemesterId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPesertaDidikLongitudinalsRelatedBySemesterId($overrideExisting = true)
    {
        if (null !== $this->collPesertaDidikLongitudinalsRelatedBySemesterId && !$overrideExisting) {
            return;
        }
        $this->collPesertaDidikLongitudinalsRelatedBySemesterId = new PropelObjectCollection();
        $this->collPesertaDidikLongitudinalsRelatedBySemesterId->setModel('PesertaDidikLongitudinal');
    }

    /**
     * Gets an array of PesertaDidikLongitudinal objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Semester is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|PesertaDidikLongitudinal[] List of PesertaDidikLongitudinal objects
     * @throws PropelException
     */
    public function getPesertaDidikLongitudinalsRelatedBySemesterId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPesertaDidikLongitudinalsRelatedBySemesterIdPartial && !$this->isNew();
        if (null === $this->collPesertaDidikLongitudinalsRelatedBySemesterId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPesertaDidikLongitudinalsRelatedBySemesterId) {
                // return empty collection
                $this->initPesertaDidikLongitudinalsRelatedBySemesterId();
            } else {
                $collPesertaDidikLongitudinalsRelatedBySemesterId = PesertaDidikLongitudinalQuery::create(null, $criteria)
                    ->filterBySemesterRelatedBySemesterId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPesertaDidikLongitudinalsRelatedBySemesterIdPartial && count($collPesertaDidikLongitudinalsRelatedBySemesterId)) {
                      $this->initPesertaDidikLongitudinalsRelatedBySemesterId(false);

                      foreach($collPesertaDidikLongitudinalsRelatedBySemesterId as $obj) {
                        if (false == $this->collPesertaDidikLongitudinalsRelatedBySemesterId->contains($obj)) {
                          $this->collPesertaDidikLongitudinalsRelatedBySemesterId->append($obj);
                        }
                      }

                      $this->collPesertaDidikLongitudinalsRelatedBySemesterIdPartial = true;
                    }

                    $collPesertaDidikLongitudinalsRelatedBySemesterId->getInternalIterator()->rewind();
                    return $collPesertaDidikLongitudinalsRelatedBySemesterId;
                }

                if($partial && $this->collPesertaDidikLongitudinalsRelatedBySemesterId) {
                    foreach($this->collPesertaDidikLongitudinalsRelatedBySemesterId as $obj) {
                        if($obj->isNew()) {
                            $collPesertaDidikLongitudinalsRelatedBySemesterId[] = $obj;
                        }
                    }
                }

                $this->collPesertaDidikLongitudinalsRelatedBySemesterId = $collPesertaDidikLongitudinalsRelatedBySemesterId;
                $this->collPesertaDidikLongitudinalsRelatedBySemesterIdPartial = false;
            }
        }

        return $this->collPesertaDidikLongitudinalsRelatedBySemesterId;
    }

    /**
     * Sets a collection of PesertaDidikLongitudinalRelatedBySemesterId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $pesertaDidikLongitudinalsRelatedBySemesterId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Semester The current object (for fluent API support)
     */
    public function setPesertaDidikLongitudinalsRelatedBySemesterId(PropelCollection $pesertaDidikLongitudinalsRelatedBySemesterId, PropelPDO $con = null)
    {
        $pesertaDidikLongitudinalsRelatedBySemesterIdToDelete = $this->getPesertaDidikLongitudinalsRelatedBySemesterId(new Criteria(), $con)->diff($pesertaDidikLongitudinalsRelatedBySemesterId);

        $this->pesertaDidikLongitudinalsRelatedBySemesterIdScheduledForDeletion = unserialize(serialize($pesertaDidikLongitudinalsRelatedBySemesterIdToDelete));

        foreach ($pesertaDidikLongitudinalsRelatedBySemesterIdToDelete as $pesertaDidikLongitudinalRelatedBySemesterIdRemoved) {
            $pesertaDidikLongitudinalRelatedBySemesterIdRemoved->setSemesterRelatedBySemesterId(null);
        }

        $this->collPesertaDidikLongitudinalsRelatedBySemesterId = null;
        foreach ($pesertaDidikLongitudinalsRelatedBySemesterId as $pesertaDidikLongitudinalRelatedBySemesterId) {
            $this->addPesertaDidikLongitudinalRelatedBySemesterId($pesertaDidikLongitudinalRelatedBySemesterId);
        }

        $this->collPesertaDidikLongitudinalsRelatedBySemesterId = $pesertaDidikLongitudinalsRelatedBySemesterId;
        $this->collPesertaDidikLongitudinalsRelatedBySemesterIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PesertaDidikLongitudinal objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related PesertaDidikLongitudinal objects.
     * @throws PropelException
     */
    public function countPesertaDidikLongitudinalsRelatedBySemesterId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPesertaDidikLongitudinalsRelatedBySemesterIdPartial && !$this->isNew();
        if (null === $this->collPesertaDidikLongitudinalsRelatedBySemesterId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPesertaDidikLongitudinalsRelatedBySemesterId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPesertaDidikLongitudinalsRelatedBySemesterId());
            }
            $query = PesertaDidikLongitudinalQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySemesterRelatedBySemesterId($this)
                ->count($con);
        }

        return count($this->collPesertaDidikLongitudinalsRelatedBySemesterId);
    }

    /**
     * Method called to associate a PesertaDidikLongitudinal object to this object
     * through the PesertaDidikLongitudinal foreign key attribute.
     *
     * @param    PesertaDidikLongitudinal $l PesertaDidikLongitudinal
     * @return Semester The current object (for fluent API support)
     */
    public function addPesertaDidikLongitudinalRelatedBySemesterId(PesertaDidikLongitudinal $l)
    {
        if ($this->collPesertaDidikLongitudinalsRelatedBySemesterId === null) {
            $this->initPesertaDidikLongitudinalsRelatedBySemesterId();
            $this->collPesertaDidikLongitudinalsRelatedBySemesterIdPartial = true;
        }
        if (!in_array($l, $this->collPesertaDidikLongitudinalsRelatedBySemesterId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPesertaDidikLongitudinalRelatedBySemesterId($l);
        }

        return $this;
    }

    /**
     * @param	PesertaDidikLongitudinalRelatedBySemesterId $pesertaDidikLongitudinalRelatedBySemesterId The pesertaDidikLongitudinalRelatedBySemesterId object to add.
     */
    protected function doAddPesertaDidikLongitudinalRelatedBySemesterId($pesertaDidikLongitudinalRelatedBySemesterId)
    {
        $this->collPesertaDidikLongitudinalsRelatedBySemesterId[]= $pesertaDidikLongitudinalRelatedBySemesterId;
        $pesertaDidikLongitudinalRelatedBySemesterId->setSemesterRelatedBySemesterId($this);
    }

    /**
     * @param	PesertaDidikLongitudinalRelatedBySemesterId $pesertaDidikLongitudinalRelatedBySemesterId The pesertaDidikLongitudinalRelatedBySemesterId object to remove.
     * @return Semester The current object (for fluent API support)
     */
    public function removePesertaDidikLongitudinalRelatedBySemesterId($pesertaDidikLongitudinalRelatedBySemesterId)
    {
        if ($this->getPesertaDidikLongitudinalsRelatedBySemesterId()->contains($pesertaDidikLongitudinalRelatedBySemesterId)) {
            $this->collPesertaDidikLongitudinalsRelatedBySemesterId->remove($this->collPesertaDidikLongitudinalsRelatedBySemesterId->search($pesertaDidikLongitudinalRelatedBySemesterId));
            if (null === $this->pesertaDidikLongitudinalsRelatedBySemesterIdScheduledForDeletion) {
                $this->pesertaDidikLongitudinalsRelatedBySemesterIdScheduledForDeletion = clone $this->collPesertaDidikLongitudinalsRelatedBySemesterId;
                $this->pesertaDidikLongitudinalsRelatedBySemesterIdScheduledForDeletion->clear();
            }
            $this->pesertaDidikLongitudinalsRelatedBySemesterIdScheduledForDeletion[]= clone $pesertaDidikLongitudinalRelatedBySemesterId;
            $pesertaDidikLongitudinalRelatedBySemesterId->setSemesterRelatedBySemesterId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related PesertaDidikLongitudinalsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidikLongitudinal[] List of PesertaDidikLongitudinal objects
     */
    public function getPesertaDidikLongitudinalsRelatedBySemesterIdJoinPesertaDidikRelatedByPesertaDidikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikLongitudinalQuery::create(null, $criteria);
        $query->joinWith('PesertaDidikRelatedByPesertaDidikId', $join_behavior);

        return $this->getPesertaDidikLongitudinalsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related PesertaDidikLongitudinalsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidikLongitudinal[] List of PesertaDidikLongitudinal objects
     */
    public function getPesertaDidikLongitudinalsRelatedBySemesterIdJoinPesertaDidikRelatedByPesertaDidikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikLongitudinalQuery::create(null, $criteria);
        $query->joinWith('PesertaDidikRelatedByPesertaDidikId', $join_behavior);

        return $this->getPesertaDidikLongitudinalsRelatedBySemesterId($query, $con);
    }

    /**
     * Clears out the collSanitasisRelatedBySemesterId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Semester The current object (for fluent API support)
     * @see        addSanitasisRelatedBySemesterId()
     */
    public function clearSanitasisRelatedBySemesterId()
    {
        $this->collSanitasisRelatedBySemesterId = null; // important to set this to null since that means it is uninitialized
        $this->collSanitasisRelatedBySemesterIdPartial = null;

        return $this;
    }

    /**
     * reset is the collSanitasisRelatedBySemesterId collection loaded partially
     *
     * @return void
     */
    public function resetPartialSanitasisRelatedBySemesterId($v = true)
    {
        $this->collSanitasisRelatedBySemesterIdPartial = $v;
    }

    /**
     * Initializes the collSanitasisRelatedBySemesterId collection.
     *
     * By default this just sets the collSanitasisRelatedBySemesterId collection to an empty array (like clearcollSanitasisRelatedBySemesterId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSanitasisRelatedBySemesterId($overrideExisting = true)
    {
        if (null !== $this->collSanitasisRelatedBySemesterId && !$overrideExisting) {
            return;
        }
        $this->collSanitasisRelatedBySemesterId = new PropelObjectCollection();
        $this->collSanitasisRelatedBySemesterId->setModel('Sanitasi');
    }

    /**
     * Gets an array of Sanitasi objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Semester is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Sanitasi[] List of Sanitasi objects
     * @throws PropelException
     */
    public function getSanitasisRelatedBySemesterId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSanitasisRelatedBySemesterIdPartial && !$this->isNew();
        if (null === $this->collSanitasisRelatedBySemesterId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSanitasisRelatedBySemesterId) {
                // return empty collection
                $this->initSanitasisRelatedBySemesterId();
            } else {
                $collSanitasisRelatedBySemesterId = SanitasiQuery::create(null, $criteria)
                    ->filterBySemesterRelatedBySemesterId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSanitasisRelatedBySemesterIdPartial && count($collSanitasisRelatedBySemesterId)) {
                      $this->initSanitasisRelatedBySemesterId(false);

                      foreach($collSanitasisRelatedBySemesterId as $obj) {
                        if (false == $this->collSanitasisRelatedBySemesterId->contains($obj)) {
                          $this->collSanitasisRelatedBySemesterId->append($obj);
                        }
                      }

                      $this->collSanitasisRelatedBySemesterIdPartial = true;
                    }

                    $collSanitasisRelatedBySemesterId->getInternalIterator()->rewind();
                    return $collSanitasisRelatedBySemesterId;
                }

                if($partial && $this->collSanitasisRelatedBySemesterId) {
                    foreach($this->collSanitasisRelatedBySemesterId as $obj) {
                        if($obj->isNew()) {
                            $collSanitasisRelatedBySemesterId[] = $obj;
                        }
                    }
                }

                $this->collSanitasisRelatedBySemesterId = $collSanitasisRelatedBySemesterId;
                $this->collSanitasisRelatedBySemesterIdPartial = false;
            }
        }

        return $this->collSanitasisRelatedBySemesterId;
    }

    /**
     * Sets a collection of SanitasiRelatedBySemesterId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $sanitasisRelatedBySemesterId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Semester The current object (for fluent API support)
     */
    public function setSanitasisRelatedBySemesterId(PropelCollection $sanitasisRelatedBySemesterId, PropelPDO $con = null)
    {
        $sanitasisRelatedBySemesterIdToDelete = $this->getSanitasisRelatedBySemesterId(new Criteria(), $con)->diff($sanitasisRelatedBySemesterId);

        $this->sanitasisRelatedBySemesterIdScheduledForDeletion = unserialize(serialize($sanitasisRelatedBySemesterIdToDelete));

        foreach ($sanitasisRelatedBySemesterIdToDelete as $sanitasiRelatedBySemesterIdRemoved) {
            $sanitasiRelatedBySemesterIdRemoved->setSemesterRelatedBySemesterId(null);
        }

        $this->collSanitasisRelatedBySemesterId = null;
        foreach ($sanitasisRelatedBySemesterId as $sanitasiRelatedBySemesterId) {
            $this->addSanitasiRelatedBySemesterId($sanitasiRelatedBySemesterId);
        }

        $this->collSanitasisRelatedBySemesterId = $sanitasisRelatedBySemesterId;
        $this->collSanitasisRelatedBySemesterIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Sanitasi objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Sanitasi objects.
     * @throws PropelException
     */
    public function countSanitasisRelatedBySemesterId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSanitasisRelatedBySemesterIdPartial && !$this->isNew();
        if (null === $this->collSanitasisRelatedBySemesterId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSanitasisRelatedBySemesterId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSanitasisRelatedBySemesterId());
            }
            $query = SanitasiQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySemesterRelatedBySemesterId($this)
                ->count($con);
        }

        return count($this->collSanitasisRelatedBySemesterId);
    }

    /**
     * Method called to associate a Sanitasi object to this object
     * through the Sanitasi foreign key attribute.
     *
     * @param    Sanitasi $l Sanitasi
     * @return Semester The current object (for fluent API support)
     */
    public function addSanitasiRelatedBySemesterId(Sanitasi $l)
    {
        if ($this->collSanitasisRelatedBySemesterId === null) {
            $this->initSanitasisRelatedBySemesterId();
            $this->collSanitasisRelatedBySemesterIdPartial = true;
        }
        if (!in_array($l, $this->collSanitasisRelatedBySemesterId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSanitasiRelatedBySemesterId($l);
        }

        return $this;
    }

    /**
     * @param	SanitasiRelatedBySemesterId $sanitasiRelatedBySemesterId The sanitasiRelatedBySemesterId object to add.
     */
    protected function doAddSanitasiRelatedBySemesterId($sanitasiRelatedBySemesterId)
    {
        $this->collSanitasisRelatedBySemesterId[]= $sanitasiRelatedBySemesterId;
        $sanitasiRelatedBySemesterId->setSemesterRelatedBySemesterId($this);
    }

    /**
     * @param	SanitasiRelatedBySemesterId $sanitasiRelatedBySemesterId The sanitasiRelatedBySemesterId object to remove.
     * @return Semester The current object (for fluent API support)
     */
    public function removeSanitasiRelatedBySemesterId($sanitasiRelatedBySemesterId)
    {
        if ($this->getSanitasisRelatedBySemesterId()->contains($sanitasiRelatedBySemesterId)) {
            $this->collSanitasisRelatedBySemesterId->remove($this->collSanitasisRelatedBySemesterId->search($sanitasiRelatedBySemesterId));
            if (null === $this->sanitasisRelatedBySemesterIdScheduledForDeletion) {
                $this->sanitasisRelatedBySemesterIdScheduledForDeletion = clone $this->collSanitasisRelatedBySemesterId;
                $this->sanitasisRelatedBySemesterIdScheduledForDeletion->clear();
            }
            $this->sanitasisRelatedBySemesterIdScheduledForDeletion[]= clone $sanitasiRelatedBySemesterId;
            $sanitasiRelatedBySemesterId->setSemesterRelatedBySemesterId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related SanitasisRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sanitasi[] List of Sanitasi objects
     */
    public function getSanitasisRelatedBySemesterIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SanitasiQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getSanitasisRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related SanitasisRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sanitasi[] List of Sanitasi objects
     */
    public function getSanitasisRelatedBySemesterIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SanitasiQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getSanitasisRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related SanitasisRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sanitasi[] List of Sanitasi objects
     */
    public function getSanitasisRelatedBySemesterIdJoinSumberAirRelatedBySumberAirId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SanitasiQuery::create(null, $criteria);
        $query->joinWith('SumberAirRelatedBySumberAirId', $join_behavior);

        return $this->getSanitasisRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related SanitasisRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sanitasi[] List of Sanitasi objects
     */
    public function getSanitasisRelatedBySemesterIdJoinSumberAirRelatedBySumberAirId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SanitasiQuery::create(null, $criteria);
        $query->joinWith('SumberAirRelatedBySumberAirId', $join_behavior);

        return $this->getSanitasisRelatedBySemesterId($query, $con);
    }

    /**
     * Clears out the collSanitasisRelatedBySemesterId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Semester The current object (for fluent API support)
     * @see        addSanitasisRelatedBySemesterId()
     */
    public function clearSanitasisRelatedBySemesterId()
    {
        $this->collSanitasisRelatedBySemesterId = null; // important to set this to null since that means it is uninitialized
        $this->collSanitasisRelatedBySemesterIdPartial = null;

        return $this;
    }

    /**
     * reset is the collSanitasisRelatedBySemesterId collection loaded partially
     *
     * @return void
     */
    public function resetPartialSanitasisRelatedBySemesterId($v = true)
    {
        $this->collSanitasisRelatedBySemesterIdPartial = $v;
    }

    /**
     * Initializes the collSanitasisRelatedBySemesterId collection.
     *
     * By default this just sets the collSanitasisRelatedBySemesterId collection to an empty array (like clearcollSanitasisRelatedBySemesterId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSanitasisRelatedBySemesterId($overrideExisting = true)
    {
        if (null !== $this->collSanitasisRelatedBySemesterId && !$overrideExisting) {
            return;
        }
        $this->collSanitasisRelatedBySemesterId = new PropelObjectCollection();
        $this->collSanitasisRelatedBySemesterId->setModel('Sanitasi');
    }

    /**
     * Gets an array of Sanitasi objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Semester is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Sanitasi[] List of Sanitasi objects
     * @throws PropelException
     */
    public function getSanitasisRelatedBySemesterId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSanitasisRelatedBySemesterIdPartial && !$this->isNew();
        if (null === $this->collSanitasisRelatedBySemesterId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSanitasisRelatedBySemesterId) {
                // return empty collection
                $this->initSanitasisRelatedBySemesterId();
            } else {
                $collSanitasisRelatedBySemesterId = SanitasiQuery::create(null, $criteria)
                    ->filterBySemesterRelatedBySemesterId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSanitasisRelatedBySemesterIdPartial && count($collSanitasisRelatedBySemesterId)) {
                      $this->initSanitasisRelatedBySemesterId(false);

                      foreach($collSanitasisRelatedBySemesterId as $obj) {
                        if (false == $this->collSanitasisRelatedBySemesterId->contains($obj)) {
                          $this->collSanitasisRelatedBySemesterId->append($obj);
                        }
                      }

                      $this->collSanitasisRelatedBySemesterIdPartial = true;
                    }

                    $collSanitasisRelatedBySemesterId->getInternalIterator()->rewind();
                    return $collSanitasisRelatedBySemesterId;
                }

                if($partial && $this->collSanitasisRelatedBySemesterId) {
                    foreach($this->collSanitasisRelatedBySemesterId as $obj) {
                        if($obj->isNew()) {
                            $collSanitasisRelatedBySemesterId[] = $obj;
                        }
                    }
                }

                $this->collSanitasisRelatedBySemesterId = $collSanitasisRelatedBySemesterId;
                $this->collSanitasisRelatedBySemesterIdPartial = false;
            }
        }

        return $this->collSanitasisRelatedBySemesterId;
    }

    /**
     * Sets a collection of SanitasiRelatedBySemesterId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $sanitasisRelatedBySemesterId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Semester The current object (for fluent API support)
     */
    public function setSanitasisRelatedBySemesterId(PropelCollection $sanitasisRelatedBySemesterId, PropelPDO $con = null)
    {
        $sanitasisRelatedBySemesterIdToDelete = $this->getSanitasisRelatedBySemesterId(new Criteria(), $con)->diff($sanitasisRelatedBySemesterId);

        $this->sanitasisRelatedBySemesterIdScheduledForDeletion = unserialize(serialize($sanitasisRelatedBySemesterIdToDelete));

        foreach ($sanitasisRelatedBySemesterIdToDelete as $sanitasiRelatedBySemesterIdRemoved) {
            $sanitasiRelatedBySemesterIdRemoved->setSemesterRelatedBySemesterId(null);
        }

        $this->collSanitasisRelatedBySemesterId = null;
        foreach ($sanitasisRelatedBySemesterId as $sanitasiRelatedBySemesterId) {
            $this->addSanitasiRelatedBySemesterId($sanitasiRelatedBySemesterId);
        }

        $this->collSanitasisRelatedBySemesterId = $sanitasisRelatedBySemesterId;
        $this->collSanitasisRelatedBySemesterIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Sanitasi objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Sanitasi objects.
     * @throws PropelException
     */
    public function countSanitasisRelatedBySemesterId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSanitasisRelatedBySemesterIdPartial && !$this->isNew();
        if (null === $this->collSanitasisRelatedBySemesterId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSanitasisRelatedBySemesterId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSanitasisRelatedBySemesterId());
            }
            $query = SanitasiQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySemesterRelatedBySemesterId($this)
                ->count($con);
        }

        return count($this->collSanitasisRelatedBySemesterId);
    }

    /**
     * Method called to associate a Sanitasi object to this object
     * through the Sanitasi foreign key attribute.
     *
     * @param    Sanitasi $l Sanitasi
     * @return Semester The current object (for fluent API support)
     */
    public function addSanitasiRelatedBySemesterId(Sanitasi $l)
    {
        if ($this->collSanitasisRelatedBySemesterId === null) {
            $this->initSanitasisRelatedBySemesterId();
            $this->collSanitasisRelatedBySemesterIdPartial = true;
        }
        if (!in_array($l, $this->collSanitasisRelatedBySemesterId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSanitasiRelatedBySemesterId($l);
        }

        return $this;
    }

    /**
     * @param	SanitasiRelatedBySemesterId $sanitasiRelatedBySemesterId The sanitasiRelatedBySemesterId object to add.
     */
    protected function doAddSanitasiRelatedBySemesterId($sanitasiRelatedBySemesterId)
    {
        $this->collSanitasisRelatedBySemesterId[]= $sanitasiRelatedBySemesterId;
        $sanitasiRelatedBySemesterId->setSemesterRelatedBySemesterId($this);
    }

    /**
     * @param	SanitasiRelatedBySemesterId $sanitasiRelatedBySemesterId The sanitasiRelatedBySemesterId object to remove.
     * @return Semester The current object (for fluent API support)
     */
    public function removeSanitasiRelatedBySemesterId($sanitasiRelatedBySemesterId)
    {
        if ($this->getSanitasisRelatedBySemesterId()->contains($sanitasiRelatedBySemesterId)) {
            $this->collSanitasisRelatedBySemesterId->remove($this->collSanitasisRelatedBySemesterId->search($sanitasiRelatedBySemesterId));
            if (null === $this->sanitasisRelatedBySemesterIdScheduledForDeletion) {
                $this->sanitasisRelatedBySemesterIdScheduledForDeletion = clone $this->collSanitasisRelatedBySemesterId;
                $this->sanitasisRelatedBySemesterIdScheduledForDeletion->clear();
            }
            $this->sanitasisRelatedBySemesterIdScheduledForDeletion[]= clone $sanitasiRelatedBySemesterId;
            $sanitasiRelatedBySemesterId->setSemesterRelatedBySemesterId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related SanitasisRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sanitasi[] List of Sanitasi objects
     */
    public function getSanitasisRelatedBySemesterIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SanitasiQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getSanitasisRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related SanitasisRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sanitasi[] List of Sanitasi objects
     */
    public function getSanitasisRelatedBySemesterIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SanitasiQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getSanitasisRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related SanitasisRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sanitasi[] List of Sanitasi objects
     */
    public function getSanitasisRelatedBySemesterIdJoinSumberAirRelatedBySumberAirId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SanitasiQuery::create(null, $criteria);
        $query->joinWith('SumberAirRelatedBySumberAirId', $join_behavior);

        return $this->getSanitasisRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related SanitasisRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sanitasi[] List of Sanitasi objects
     */
    public function getSanitasisRelatedBySemesterIdJoinSumberAirRelatedBySumberAirId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SanitasiQuery::create(null, $criteria);
        $query->joinWith('SumberAirRelatedBySumberAirId', $join_behavior);

        return $this->getSanitasisRelatedBySemesterId($query, $con);
    }

    /**
     * Clears out the collSaranaLongitudinalsRelatedBySemesterId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Semester The current object (for fluent API support)
     * @see        addSaranaLongitudinalsRelatedBySemesterId()
     */
    public function clearSaranaLongitudinalsRelatedBySemesterId()
    {
        $this->collSaranaLongitudinalsRelatedBySemesterId = null; // important to set this to null since that means it is uninitialized
        $this->collSaranaLongitudinalsRelatedBySemesterIdPartial = null;

        return $this;
    }

    /**
     * reset is the collSaranaLongitudinalsRelatedBySemesterId collection loaded partially
     *
     * @return void
     */
    public function resetPartialSaranaLongitudinalsRelatedBySemesterId($v = true)
    {
        $this->collSaranaLongitudinalsRelatedBySemesterIdPartial = $v;
    }

    /**
     * Initializes the collSaranaLongitudinalsRelatedBySemesterId collection.
     *
     * By default this just sets the collSaranaLongitudinalsRelatedBySemesterId collection to an empty array (like clearcollSaranaLongitudinalsRelatedBySemesterId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSaranaLongitudinalsRelatedBySemesterId($overrideExisting = true)
    {
        if (null !== $this->collSaranaLongitudinalsRelatedBySemesterId && !$overrideExisting) {
            return;
        }
        $this->collSaranaLongitudinalsRelatedBySemesterId = new PropelObjectCollection();
        $this->collSaranaLongitudinalsRelatedBySemesterId->setModel('SaranaLongitudinal');
    }

    /**
     * Gets an array of SaranaLongitudinal objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Semester is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SaranaLongitudinal[] List of SaranaLongitudinal objects
     * @throws PropelException
     */
    public function getSaranaLongitudinalsRelatedBySemesterId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSaranaLongitudinalsRelatedBySemesterIdPartial && !$this->isNew();
        if (null === $this->collSaranaLongitudinalsRelatedBySemesterId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSaranaLongitudinalsRelatedBySemesterId) {
                // return empty collection
                $this->initSaranaLongitudinalsRelatedBySemesterId();
            } else {
                $collSaranaLongitudinalsRelatedBySemesterId = SaranaLongitudinalQuery::create(null, $criteria)
                    ->filterBySemesterRelatedBySemesterId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSaranaLongitudinalsRelatedBySemesterIdPartial && count($collSaranaLongitudinalsRelatedBySemesterId)) {
                      $this->initSaranaLongitudinalsRelatedBySemesterId(false);

                      foreach($collSaranaLongitudinalsRelatedBySemesterId as $obj) {
                        if (false == $this->collSaranaLongitudinalsRelatedBySemesterId->contains($obj)) {
                          $this->collSaranaLongitudinalsRelatedBySemesterId->append($obj);
                        }
                      }

                      $this->collSaranaLongitudinalsRelatedBySemesterIdPartial = true;
                    }

                    $collSaranaLongitudinalsRelatedBySemesterId->getInternalIterator()->rewind();
                    return $collSaranaLongitudinalsRelatedBySemesterId;
                }

                if($partial && $this->collSaranaLongitudinalsRelatedBySemesterId) {
                    foreach($this->collSaranaLongitudinalsRelatedBySemesterId as $obj) {
                        if($obj->isNew()) {
                            $collSaranaLongitudinalsRelatedBySemesterId[] = $obj;
                        }
                    }
                }

                $this->collSaranaLongitudinalsRelatedBySemesterId = $collSaranaLongitudinalsRelatedBySemesterId;
                $this->collSaranaLongitudinalsRelatedBySemesterIdPartial = false;
            }
        }

        return $this->collSaranaLongitudinalsRelatedBySemesterId;
    }

    /**
     * Sets a collection of SaranaLongitudinalRelatedBySemesterId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $saranaLongitudinalsRelatedBySemesterId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Semester The current object (for fluent API support)
     */
    public function setSaranaLongitudinalsRelatedBySemesterId(PropelCollection $saranaLongitudinalsRelatedBySemesterId, PropelPDO $con = null)
    {
        $saranaLongitudinalsRelatedBySemesterIdToDelete = $this->getSaranaLongitudinalsRelatedBySemesterId(new Criteria(), $con)->diff($saranaLongitudinalsRelatedBySemesterId);

        $this->saranaLongitudinalsRelatedBySemesterIdScheduledForDeletion = unserialize(serialize($saranaLongitudinalsRelatedBySemesterIdToDelete));

        foreach ($saranaLongitudinalsRelatedBySemesterIdToDelete as $saranaLongitudinalRelatedBySemesterIdRemoved) {
            $saranaLongitudinalRelatedBySemesterIdRemoved->setSemesterRelatedBySemesterId(null);
        }

        $this->collSaranaLongitudinalsRelatedBySemesterId = null;
        foreach ($saranaLongitudinalsRelatedBySemesterId as $saranaLongitudinalRelatedBySemesterId) {
            $this->addSaranaLongitudinalRelatedBySemesterId($saranaLongitudinalRelatedBySemesterId);
        }

        $this->collSaranaLongitudinalsRelatedBySemesterId = $saranaLongitudinalsRelatedBySemesterId;
        $this->collSaranaLongitudinalsRelatedBySemesterIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SaranaLongitudinal objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SaranaLongitudinal objects.
     * @throws PropelException
     */
    public function countSaranaLongitudinalsRelatedBySemesterId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSaranaLongitudinalsRelatedBySemesterIdPartial && !$this->isNew();
        if (null === $this->collSaranaLongitudinalsRelatedBySemesterId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSaranaLongitudinalsRelatedBySemesterId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSaranaLongitudinalsRelatedBySemesterId());
            }
            $query = SaranaLongitudinalQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySemesterRelatedBySemesterId($this)
                ->count($con);
        }

        return count($this->collSaranaLongitudinalsRelatedBySemesterId);
    }

    /**
     * Method called to associate a SaranaLongitudinal object to this object
     * through the SaranaLongitudinal foreign key attribute.
     *
     * @param    SaranaLongitudinal $l SaranaLongitudinal
     * @return Semester The current object (for fluent API support)
     */
    public function addSaranaLongitudinalRelatedBySemesterId(SaranaLongitudinal $l)
    {
        if ($this->collSaranaLongitudinalsRelatedBySemesterId === null) {
            $this->initSaranaLongitudinalsRelatedBySemesterId();
            $this->collSaranaLongitudinalsRelatedBySemesterIdPartial = true;
        }
        if (!in_array($l, $this->collSaranaLongitudinalsRelatedBySemesterId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSaranaLongitudinalRelatedBySemesterId($l);
        }

        return $this;
    }

    /**
     * @param	SaranaLongitudinalRelatedBySemesterId $saranaLongitudinalRelatedBySemesterId The saranaLongitudinalRelatedBySemesterId object to add.
     */
    protected function doAddSaranaLongitudinalRelatedBySemesterId($saranaLongitudinalRelatedBySemesterId)
    {
        $this->collSaranaLongitudinalsRelatedBySemesterId[]= $saranaLongitudinalRelatedBySemesterId;
        $saranaLongitudinalRelatedBySemesterId->setSemesterRelatedBySemesterId($this);
    }

    /**
     * @param	SaranaLongitudinalRelatedBySemesterId $saranaLongitudinalRelatedBySemesterId The saranaLongitudinalRelatedBySemesterId object to remove.
     * @return Semester The current object (for fluent API support)
     */
    public function removeSaranaLongitudinalRelatedBySemesterId($saranaLongitudinalRelatedBySemesterId)
    {
        if ($this->getSaranaLongitudinalsRelatedBySemesterId()->contains($saranaLongitudinalRelatedBySemesterId)) {
            $this->collSaranaLongitudinalsRelatedBySemesterId->remove($this->collSaranaLongitudinalsRelatedBySemesterId->search($saranaLongitudinalRelatedBySemesterId));
            if (null === $this->saranaLongitudinalsRelatedBySemesterIdScheduledForDeletion) {
                $this->saranaLongitudinalsRelatedBySemesterIdScheduledForDeletion = clone $this->collSaranaLongitudinalsRelatedBySemesterId;
                $this->saranaLongitudinalsRelatedBySemesterIdScheduledForDeletion->clear();
            }
            $this->saranaLongitudinalsRelatedBySemesterIdScheduledForDeletion[]= clone $saranaLongitudinalRelatedBySemesterId;
            $saranaLongitudinalRelatedBySemesterId->setSemesterRelatedBySemesterId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related SaranaLongitudinalsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SaranaLongitudinal[] List of SaranaLongitudinal objects
     */
    public function getSaranaLongitudinalsRelatedBySemesterIdJoinSaranaRelatedBySaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SaranaLongitudinalQuery::create(null, $criteria);
        $query->joinWith('SaranaRelatedBySaranaId', $join_behavior);

        return $this->getSaranaLongitudinalsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related SaranaLongitudinalsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SaranaLongitudinal[] List of SaranaLongitudinal objects
     */
    public function getSaranaLongitudinalsRelatedBySemesterIdJoinSaranaRelatedBySaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SaranaLongitudinalQuery::create(null, $criteria);
        $query->joinWith('SaranaRelatedBySaranaId', $join_behavior);

        return $this->getSaranaLongitudinalsRelatedBySemesterId($query, $con);
    }

    /**
     * Clears out the collSaranaLongitudinalsRelatedBySemesterId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Semester The current object (for fluent API support)
     * @see        addSaranaLongitudinalsRelatedBySemesterId()
     */
    public function clearSaranaLongitudinalsRelatedBySemesterId()
    {
        $this->collSaranaLongitudinalsRelatedBySemesterId = null; // important to set this to null since that means it is uninitialized
        $this->collSaranaLongitudinalsRelatedBySemesterIdPartial = null;

        return $this;
    }

    /**
     * reset is the collSaranaLongitudinalsRelatedBySemesterId collection loaded partially
     *
     * @return void
     */
    public function resetPartialSaranaLongitudinalsRelatedBySemesterId($v = true)
    {
        $this->collSaranaLongitudinalsRelatedBySemesterIdPartial = $v;
    }

    /**
     * Initializes the collSaranaLongitudinalsRelatedBySemesterId collection.
     *
     * By default this just sets the collSaranaLongitudinalsRelatedBySemesterId collection to an empty array (like clearcollSaranaLongitudinalsRelatedBySemesterId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSaranaLongitudinalsRelatedBySemesterId($overrideExisting = true)
    {
        if (null !== $this->collSaranaLongitudinalsRelatedBySemesterId && !$overrideExisting) {
            return;
        }
        $this->collSaranaLongitudinalsRelatedBySemesterId = new PropelObjectCollection();
        $this->collSaranaLongitudinalsRelatedBySemesterId->setModel('SaranaLongitudinal');
    }

    /**
     * Gets an array of SaranaLongitudinal objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Semester is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SaranaLongitudinal[] List of SaranaLongitudinal objects
     * @throws PropelException
     */
    public function getSaranaLongitudinalsRelatedBySemesterId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSaranaLongitudinalsRelatedBySemesterIdPartial && !$this->isNew();
        if (null === $this->collSaranaLongitudinalsRelatedBySemesterId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSaranaLongitudinalsRelatedBySemesterId) {
                // return empty collection
                $this->initSaranaLongitudinalsRelatedBySemesterId();
            } else {
                $collSaranaLongitudinalsRelatedBySemesterId = SaranaLongitudinalQuery::create(null, $criteria)
                    ->filterBySemesterRelatedBySemesterId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSaranaLongitudinalsRelatedBySemesterIdPartial && count($collSaranaLongitudinalsRelatedBySemesterId)) {
                      $this->initSaranaLongitudinalsRelatedBySemesterId(false);

                      foreach($collSaranaLongitudinalsRelatedBySemesterId as $obj) {
                        if (false == $this->collSaranaLongitudinalsRelatedBySemesterId->contains($obj)) {
                          $this->collSaranaLongitudinalsRelatedBySemesterId->append($obj);
                        }
                      }

                      $this->collSaranaLongitudinalsRelatedBySemesterIdPartial = true;
                    }

                    $collSaranaLongitudinalsRelatedBySemesterId->getInternalIterator()->rewind();
                    return $collSaranaLongitudinalsRelatedBySemesterId;
                }

                if($partial && $this->collSaranaLongitudinalsRelatedBySemesterId) {
                    foreach($this->collSaranaLongitudinalsRelatedBySemesterId as $obj) {
                        if($obj->isNew()) {
                            $collSaranaLongitudinalsRelatedBySemesterId[] = $obj;
                        }
                    }
                }

                $this->collSaranaLongitudinalsRelatedBySemesterId = $collSaranaLongitudinalsRelatedBySemesterId;
                $this->collSaranaLongitudinalsRelatedBySemesterIdPartial = false;
            }
        }

        return $this->collSaranaLongitudinalsRelatedBySemesterId;
    }

    /**
     * Sets a collection of SaranaLongitudinalRelatedBySemesterId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $saranaLongitudinalsRelatedBySemesterId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Semester The current object (for fluent API support)
     */
    public function setSaranaLongitudinalsRelatedBySemesterId(PropelCollection $saranaLongitudinalsRelatedBySemesterId, PropelPDO $con = null)
    {
        $saranaLongitudinalsRelatedBySemesterIdToDelete = $this->getSaranaLongitudinalsRelatedBySemesterId(new Criteria(), $con)->diff($saranaLongitudinalsRelatedBySemesterId);

        $this->saranaLongitudinalsRelatedBySemesterIdScheduledForDeletion = unserialize(serialize($saranaLongitudinalsRelatedBySemesterIdToDelete));

        foreach ($saranaLongitudinalsRelatedBySemesterIdToDelete as $saranaLongitudinalRelatedBySemesterIdRemoved) {
            $saranaLongitudinalRelatedBySemesterIdRemoved->setSemesterRelatedBySemesterId(null);
        }

        $this->collSaranaLongitudinalsRelatedBySemesterId = null;
        foreach ($saranaLongitudinalsRelatedBySemesterId as $saranaLongitudinalRelatedBySemesterId) {
            $this->addSaranaLongitudinalRelatedBySemesterId($saranaLongitudinalRelatedBySemesterId);
        }

        $this->collSaranaLongitudinalsRelatedBySemesterId = $saranaLongitudinalsRelatedBySemesterId;
        $this->collSaranaLongitudinalsRelatedBySemesterIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SaranaLongitudinal objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SaranaLongitudinal objects.
     * @throws PropelException
     */
    public function countSaranaLongitudinalsRelatedBySemesterId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSaranaLongitudinalsRelatedBySemesterIdPartial && !$this->isNew();
        if (null === $this->collSaranaLongitudinalsRelatedBySemesterId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSaranaLongitudinalsRelatedBySemesterId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSaranaLongitudinalsRelatedBySemesterId());
            }
            $query = SaranaLongitudinalQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySemesterRelatedBySemesterId($this)
                ->count($con);
        }

        return count($this->collSaranaLongitudinalsRelatedBySemesterId);
    }

    /**
     * Method called to associate a SaranaLongitudinal object to this object
     * through the SaranaLongitudinal foreign key attribute.
     *
     * @param    SaranaLongitudinal $l SaranaLongitudinal
     * @return Semester The current object (for fluent API support)
     */
    public function addSaranaLongitudinalRelatedBySemesterId(SaranaLongitudinal $l)
    {
        if ($this->collSaranaLongitudinalsRelatedBySemesterId === null) {
            $this->initSaranaLongitudinalsRelatedBySemesterId();
            $this->collSaranaLongitudinalsRelatedBySemesterIdPartial = true;
        }
        if (!in_array($l, $this->collSaranaLongitudinalsRelatedBySemesterId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSaranaLongitudinalRelatedBySemesterId($l);
        }

        return $this;
    }

    /**
     * @param	SaranaLongitudinalRelatedBySemesterId $saranaLongitudinalRelatedBySemesterId The saranaLongitudinalRelatedBySemesterId object to add.
     */
    protected function doAddSaranaLongitudinalRelatedBySemesterId($saranaLongitudinalRelatedBySemesterId)
    {
        $this->collSaranaLongitudinalsRelatedBySemesterId[]= $saranaLongitudinalRelatedBySemesterId;
        $saranaLongitudinalRelatedBySemesterId->setSemesterRelatedBySemesterId($this);
    }

    /**
     * @param	SaranaLongitudinalRelatedBySemesterId $saranaLongitudinalRelatedBySemesterId The saranaLongitudinalRelatedBySemesterId object to remove.
     * @return Semester The current object (for fluent API support)
     */
    public function removeSaranaLongitudinalRelatedBySemesterId($saranaLongitudinalRelatedBySemesterId)
    {
        if ($this->getSaranaLongitudinalsRelatedBySemesterId()->contains($saranaLongitudinalRelatedBySemesterId)) {
            $this->collSaranaLongitudinalsRelatedBySemesterId->remove($this->collSaranaLongitudinalsRelatedBySemesterId->search($saranaLongitudinalRelatedBySemesterId));
            if (null === $this->saranaLongitudinalsRelatedBySemesterIdScheduledForDeletion) {
                $this->saranaLongitudinalsRelatedBySemesterIdScheduledForDeletion = clone $this->collSaranaLongitudinalsRelatedBySemesterId;
                $this->saranaLongitudinalsRelatedBySemesterIdScheduledForDeletion->clear();
            }
            $this->saranaLongitudinalsRelatedBySemesterIdScheduledForDeletion[]= clone $saranaLongitudinalRelatedBySemesterId;
            $saranaLongitudinalRelatedBySemesterId->setSemesterRelatedBySemesterId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related SaranaLongitudinalsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SaranaLongitudinal[] List of SaranaLongitudinal objects
     */
    public function getSaranaLongitudinalsRelatedBySemesterIdJoinSaranaRelatedBySaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SaranaLongitudinalQuery::create(null, $criteria);
        $query->joinWith('SaranaRelatedBySaranaId', $join_behavior);

        return $this->getSaranaLongitudinalsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related SaranaLongitudinalsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SaranaLongitudinal[] List of SaranaLongitudinal objects
     */
    public function getSaranaLongitudinalsRelatedBySemesterIdJoinSaranaRelatedBySaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SaranaLongitudinalQuery::create(null, $criteria);
        $query->joinWith('SaranaRelatedBySaranaId', $join_behavior);

        return $this->getSaranaLongitudinalsRelatedBySemesterId($query, $con);
    }

    /**
     * Gets a single BatasWaktuRapor object, which is related to this object by a one-to-one relationship.
     *
     * @param PropelPDO $con optional connection object
     * @return BatasWaktuRapor
     * @throws PropelException
     */
    public function getBatasWaktuRapor(PropelPDO $con = null)
    {

        if ($this->singleBatasWaktuRapor === null && !$this->isNew()) {
            $this->singleBatasWaktuRapor = BatasWaktuRaporQuery::create()->findPk($this->getPrimaryKey(), $con);
        }

        return $this->singleBatasWaktuRapor;
    }

    /**
     * Sets a single BatasWaktuRapor object as related to this object by a one-to-one relationship.
     *
     * @param             BatasWaktuRapor $v BatasWaktuRapor
     * @return Semester The current object (for fluent API support)
     * @throws PropelException
     */
    public function setBatasWaktuRapor(BatasWaktuRapor $v = null)
    {
        $this->singleBatasWaktuRapor = $v;

        // Make sure that that the passed-in BatasWaktuRapor isn't already associated with this object
        if ($v !== null && $v->getSemester(null, false) === null) {
            $v->setSemester($this);
        }

        return $this;
    }

    /**
     * Clears out the collPembelajaransRelatedBySemesterId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Semester The current object (for fluent API support)
     * @see        addPembelajaransRelatedBySemesterId()
     */
    public function clearPembelajaransRelatedBySemesterId()
    {
        $this->collPembelajaransRelatedBySemesterId = null; // important to set this to null since that means it is uninitialized
        $this->collPembelajaransRelatedBySemesterIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPembelajaransRelatedBySemesterId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPembelajaransRelatedBySemesterId($v = true)
    {
        $this->collPembelajaransRelatedBySemesterIdPartial = $v;
    }

    /**
     * Initializes the collPembelajaransRelatedBySemesterId collection.
     *
     * By default this just sets the collPembelajaransRelatedBySemesterId collection to an empty array (like clearcollPembelajaransRelatedBySemesterId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPembelajaransRelatedBySemesterId($overrideExisting = true)
    {
        if (null !== $this->collPembelajaransRelatedBySemesterId && !$overrideExisting) {
            return;
        }
        $this->collPembelajaransRelatedBySemesterId = new PropelObjectCollection();
        $this->collPembelajaransRelatedBySemesterId->setModel('Pembelajaran');
    }

    /**
     * Gets an array of Pembelajaran objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Semester is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     * @throws PropelException
     */
    public function getPembelajaransRelatedBySemesterId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPembelajaransRelatedBySemesterIdPartial && !$this->isNew();
        if (null === $this->collPembelajaransRelatedBySemesterId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPembelajaransRelatedBySemesterId) {
                // return empty collection
                $this->initPembelajaransRelatedBySemesterId();
            } else {
                $collPembelajaransRelatedBySemesterId = PembelajaranQuery::create(null, $criteria)
                    ->filterBySemesterRelatedBySemesterId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPembelajaransRelatedBySemesterIdPartial && count($collPembelajaransRelatedBySemesterId)) {
                      $this->initPembelajaransRelatedBySemesterId(false);

                      foreach($collPembelajaransRelatedBySemesterId as $obj) {
                        if (false == $this->collPembelajaransRelatedBySemesterId->contains($obj)) {
                          $this->collPembelajaransRelatedBySemesterId->append($obj);
                        }
                      }

                      $this->collPembelajaransRelatedBySemesterIdPartial = true;
                    }

                    $collPembelajaransRelatedBySemesterId->getInternalIterator()->rewind();
                    return $collPembelajaransRelatedBySemesterId;
                }

                if($partial && $this->collPembelajaransRelatedBySemesterId) {
                    foreach($this->collPembelajaransRelatedBySemesterId as $obj) {
                        if($obj->isNew()) {
                            $collPembelajaransRelatedBySemesterId[] = $obj;
                        }
                    }
                }

                $this->collPembelajaransRelatedBySemesterId = $collPembelajaransRelatedBySemesterId;
                $this->collPembelajaransRelatedBySemesterIdPartial = false;
            }
        }

        return $this->collPembelajaransRelatedBySemesterId;
    }

    /**
     * Sets a collection of PembelajaranRelatedBySemesterId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $pembelajaransRelatedBySemesterId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Semester The current object (for fluent API support)
     */
    public function setPembelajaransRelatedBySemesterId(PropelCollection $pembelajaransRelatedBySemesterId, PropelPDO $con = null)
    {
        $pembelajaransRelatedBySemesterIdToDelete = $this->getPembelajaransRelatedBySemesterId(new Criteria(), $con)->diff($pembelajaransRelatedBySemesterId);

        $this->pembelajaransRelatedBySemesterIdScheduledForDeletion = unserialize(serialize($pembelajaransRelatedBySemesterIdToDelete));

        foreach ($pembelajaransRelatedBySemesterIdToDelete as $pembelajaranRelatedBySemesterIdRemoved) {
            $pembelajaranRelatedBySemesterIdRemoved->setSemesterRelatedBySemesterId(null);
        }

        $this->collPembelajaransRelatedBySemesterId = null;
        foreach ($pembelajaransRelatedBySemesterId as $pembelajaranRelatedBySemesterId) {
            $this->addPembelajaranRelatedBySemesterId($pembelajaranRelatedBySemesterId);
        }

        $this->collPembelajaransRelatedBySemesterId = $pembelajaransRelatedBySemesterId;
        $this->collPembelajaransRelatedBySemesterIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Pembelajaran objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Pembelajaran objects.
     * @throws PropelException
     */
    public function countPembelajaransRelatedBySemesterId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPembelajaransRelatedBySemesterIdPartial && !$this->isNew();
        if (null === $this->collPembelajaransRelatedBySemesterId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPembelajaransRelatedBySemesterId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPembelajaransRelatedBySemesterId());
            }
            $query = PembelajaranQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySemesterRelatedBySemesterId($this)
                ->count($con);
        }

        return count($this->collPembelajaransRelatedBySemesterId);
    }

    /**
     * Method called to associate a Pembelajaran object to this object
     * through the Pembelajaran foreign key attribute.
     *
     * @param    Pembelajaran $l Pembelajaran
     * @return Semester The current object (for fluent API support)
     */
    public function addPembelajaranRelatedBySemesterId(Pembelajaran $l)
    {
        if ($this->collPembelajaransRelatedBySemesterId === null) {
            $this->initPembelajaransRelatedBySemesterId();
            $this->collPembelajaransRelatedBySemesterIdPartial = true;
        }
        if (!in_array($l, $this->collPembelajaransRelatedBySemesterId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPembelajaranRelatedBySemesterId($l);
        }

        return $this;
    }

    /**
     * @param	PembelajaranRelatedBySemesterId $pembelajaranRelatedBySemesterId The pembelajaranRelatedBySemesterId object to add.
     */
    protected function doAddPembelajaranRelatedBySemesterId($pembelajaranRelatedBySemesterId)
    {
        $this->collPembelajaransRelatedBySemesterId[]= $pembelajaranRelatedBySemesterId;
        $pembelajaranRelatedBySemesterId->setSemesterRelatedBySemesterId($this);
    }

    /**
     * @param	PembelajaranRelatedBySemesterId $pembelajaranRelatedBySemesterId The pembelajaranRelatedBySemesterId object to remove.
     * @return Semester The current object (for fluent API support)
     */
    public function removePembelajaranRelatedBySemesterId($pembelajaranRelatedBySemesterId)
    {
        if ($this->getPembelajaransRelatedBySemesterId()->contains($pembelajaranRelatedBySemesterId)) {
            $this->collPembelajaransRelatedBySemesterId->remove($this->collPembelajaransRelatedBySemesterId->search($pembelajaranRelatedBySemesterId));
            if (null === $this->pembelajaransRelatedBySemesterIdScheduledForDeletion) {
                $this->pembelajaransRelatedBySemesterIdScheduledForDeletion = clone $this->collPembelajaransRelatedBySemesterId;
                $this->pembelajaransRelatedBySemesterIdScheduledForDeletion->clear();
            }
            $this->pembelajaransRelatedBySemesterIdScheduledForDeletion[]= clone $pembelajaranRelatedBySemesterId;
            $pembelajaranRelatedBySemesterId->setSemesterRelatedBySemesterId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related PembelajaransRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     */
    public function getPembelajaransRelatedBySemesterIdJoinPtkTerdaftarRelatedByPtkTerdaftarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PembelajaranQuery::create(null, $criteria);
        $query->joinWith('PtkTerdaftarRelatedByPtkTerdaftarId', $join_behavior);

        return $this->getPembelajaransRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related PembelajaransRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     */
    public function getPembelajaransRelatedBySemesterIdJoinPtkTerdaftarRelatedByPtkTerdaftarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PembelajaranQuery::create(null, $criteria);
        $query->joinWith('PtkTerdaftarRelatedByPtkTerdaftarId', $join_behavior);

        return $this->getPembelajaransRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related PembelajaransRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     */
    public function getPembelajaransRelatedBySemesterIdJoinRombonganBelajarRelatedByRombonganBelajarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PembelajaranQuery::create(null, $criteria);
        $query->joinWith('RombonganBelajarRelatedByRombonganBelajarId', $join_behavior);

        return $this->getPembelajaransRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related PembelajaransRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     */
    public function getPembelajaransRelatedBySemesterIdJoinRombonganBelajarRelatedByRombonganBelajarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PembelajaranQuery::create(null, $criteria);
        $query->joinWith('RombonganBelajarRelatedByRombonganBelajarId', $join_behavior);

        return $this->getPembelajaransRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related PembelajaransRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     */
    public function getPembelajaransRelatedBySemesterIdJoinMataPelajaranRelatedByMataPelajaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PembelajaranQuery::create(null, $criteria);
        $query->joinWith('MataPelajaranRelatedByMataPelajaranId', $join_behavior);

        return $this->getPembelajaransRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related PembelajaransRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     */
    public function getPembelajaransRelatedBySemesterIdJoinMataPelajaranRelatedByMataPelajaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PembelajaranQuery::create(null, $criteria);
        $query->joinWith('MataPelajaranRelatedByMataPelajaranId', $join_behavior);

        return $this->getPembelajaransRelatedBySemesterId($query, $con);
    }

    /**
     * Clears out the collPembelajaransRelatedBySemesterId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Semester The current object (for fluent API support)
     * @see        addPembelajaransRelatedBySemesterId()
     */
    public function clearPembelajaransRelatedBySemesterId()
    {
        $this->collPembelajaransRelatedBySemesterId = null; // important to set this to null since that means it is uninitialized
        $this->collPembelajaransRelatedBySemesterIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPembelajaransRelatedBySemesterId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPembelajaransRelatedBySemesterId($v = true)
    {
        $this->collPembelajaransRelatedBySemesterIdPartial = $v;
    }

    /**
     * Initializes the collPembelajaransRelatedBySemesterId collection.
     *
     * By default this just sets the collPembelajaransRelatedBySemesterId collection to an empty array (like clearcollPembelajaransRelatedBySemesterId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPembelajaransRelatedBySemesterId($overrideExisting = true)
    {
        if (null !== $this->collPembelajaransRelatedBySemesterId && !$overrideExisting) {
            return;
        }
        $this->collPembelajaransRelatedBySemesterId = new PropelObjectCollection();
        $this->collPembelajaransRelatedBySemesterId->setModel('Pembelajaran');
    }

    /**
     * Gets an array of Pembelajaran objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Semester is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     * @throws PropelException
     */
    public function getPembelajaransRelatedBySemesterId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPembelajaransRelatedBySemesterIdPartial && !$this->isNew();
        if (null === $this->collPembelajaransRelatedBySemesterId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPembelajaransRelatedBySemesterId) {
                // return empty collection
                $this->initPembelajaransRelatedBySemesterId();
            } else {
                $collPembelajaransRelatedBySemesterId = PembelajaranQuery::create(null, $criteria)
                    ->filterBySemesterRelatedBySemesterId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPembelajaransRelatedBySemesterIdPartial && count($collPembelajaransRelatedBySemesterId)) {
                      $this->initPembelajaransRelatedBySemesterId(false);

                      foreach($collPembelajaransRelatedBySemesterId as $obj) {
                        if (false == $this->collPembelajaransRelatedBySemesterId->contains($obj)) {
                          $this->collPembelajaransRelatedBySemesterId->append($obj);
                        }
                      }

                      $this->collPembelajaransRelatedBySemesterIdPartial = true;
                    }

                    $collPembelajaransRelatedBySemesterId->getInternalIterator()->rewind();
                    return $collPembelajaransRelatedBySemesterId;
                }

                if($partial && $this->collPembelajaransRelatedBySemesterId) {
                    foreach($this->collPembelajaransRelatedBySemesterId as $obj) {
                        if($obj->isNew()) {
                            $collPembelajaransRelatedBySemesterId[] = $obj;
                        }
                    }
                }

                $this->collPembelajaransRelatedBySemesterId = $collPembelajaransRelatedBySemesterId;
                $this->collPembelajaransRelatedBySemesterIdPartial = false;
            }
        }

        return $this->collPembelajaransRelatedBySemesterId;
    }

    /**
     * Sets a collection of PembelajaranRelatedBySemesterId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $pembelajaransRelatedBySemesterId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Semester The current object (for fluent API support)
     */
    public function setPembelajaransRelatedBySemesterId(PropelCollection $pembelajaransRelatedBySemesterId, PropelPDO $con = null)
    {
        $pembelajaransRelatedBySemesterIdToDelete = $this->getPembelajaransRelatedBySemesterId(new Criteria(), $con)->diff($pembelajaransRelatedBySemesterId);

        $this->pembelajaransRelatedBySemesterIdScheduledForDeletion = unserialize(serialize($pembelajaransRelatedBySemesterIdToDelete));

        foreach ($pembelajaransRelatedBySemesterIdToDelete as $pembelajaranRelatedBySemesterIdRemoved) {
            $pembelajaranRelatedBySemesterIdRemoved->setSemesterRelatedBySemesterId(null);
        }

        $this->collPembelajaransRelatedBySemesterId = null;
        foreach ($pembelajaransRelatedBySemesterId as $pembelajaranRelatedBySemesterId) {
            $this->addPembelajaranRelatedBySemesterId($pembelajaranRelatedBySemesterId);
        }

        $this->collPembelajaransRelatedBySemesterId = $pembelajaransRelatedBySemesterId;
        $this->collPembelajaransRelatedBySemesterIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Pembelajaran objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Pembelajaran objects.
     * @throws PropelException
     */
    public function countPembelajaransRelatedBySemesterId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPembelajaransRelatedBySemesterIdPartial && !$this->isNew();
        if (null === $this->collPembelajaransRelatedBySemesterId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPembelajaransRelatedBySemesterId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPembelajaransRelatedBySemesterId());
            }
            $query = PembelajaranQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySemesterRelatedBySemesterId($this)
                ->count($con);
        }

        return count($this->collPembelajaransRelatedBySemesterId);
    }

    /**
     * Method called to associate a Pembelajaran object to this object
     * through the Pembelajaran foreign key attribute.
     *
     * @param    Pembelajaran $l Pembelajaran
     * @return Semester The current object (for fluent API support)
     */
    public function addPembelajaranRelatedBySemesterId(Pembelajaran $l)
    {
        if ($this->collPembelajaransRelatedBySemesterId === null) {
            $this->initPembelajaransRelatedBySemesterId();
            $this->collPembelajaransRelatedBySemesterIdPartial = true;
        }
        if (!in_array($l, $this->collPembelajaransRelatedBySemesterId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPembelajaranRelatedBySemesterId($l);
        }

        return $this;
    }

    /**
     * @param	PembelajaranRelatedBySemesterId $pembelajaranRelatedBySemesterId The pembelajaranRelatedBySemesterId object to add.
     */
    protected function doAddPembelajaranRelatedBySemesterId($pembelajaranRelatedBySemesterId)
    {
        $this->collPembelajaransRelatedBySemesterId[]= $pembelajaranRelatedBySemesterId;
        $pembelajaranRelatedBySemesterId->setSemesterRelatedBySemesterId($this);
    }

    /**
     * @param	PembelajaranRelatedBySemesterId $pembelajaranRelatedBySemesterId The pembelajaranRelatedBySemesterId object to remove.
     * @return Semester The current object (for fluent API support)
     */
    public function removePembelajaranRelatedBySemesterId($pembelajaranRelatedBySemesterId)
    {
        if ($this->getPembelajaransRelatedBySemesterId()->contains($pembelajaranRelatedBySemesterId)) {
            $this->collPembelajaransRelatedBySemesterId->remove($this->collPembelajaransRelatedBySemesterId->search($pembelajaranRelatedBySemesterId));
            if (null === $this->pembelajaransRelatedBySemesterIdScheduledForDeletion) {
                $this->pembelajaransRelatedBySemesterIdScheduledForDeletion = clone $this->collPembelajaransRelatedBySemesterId;
                $this->pembelajaransRelatedBySemesterIdScheduledForDeletion->clear();
            }
            $this->pembelajaransRelatedBySemesterIdScheduledForDeletion[]= clone $pembelajaranRelatedBySemesterId;
            $pembelajaranRelatedBySemesterId->setSemesterRelatedBySemesterId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related PembelajaransRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     */
    public function getPembelajaransRelatedBySemesterIdJoinPtkTerdaftarRelatedByPtkTerdaftarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PembelajaranQuery::create(null, $criteria);
        $query->joinWith('PtkTerdaftarRelatedByPtkTerdaftarId', $join_behavior);

        return $this->getPembelajaransRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related PembelajaransRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     */
    public function getPembelajaransRelatedBySemesterIdJoinPtkTerdaftarRelatedByPtkTerdaftarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PembelajaranQuery::create(null, $criteria);
        $query->joinWith('PtkTerdaftarRelatedByPtkTerdaftarId', $join_behavior);

        return $this->getPembelajaransRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related PembelajaransRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     */
    public function getPembelajaransRelatedBySemesterIdJoinRombonganBelajarRelatedByRombonganBelajarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PembelajaranQuery::create(null, $criteria);
        $query->joinWith('RombonganBelajarRelatedByRombonganBelajarId', $join_behavior);

        return $this->getPembelajaransRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related PembelajaransRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     */
    public function getPembelajaransRelatedBySemesterIdJoinRombonganBelajarRelatedByRombonganBelajarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PembelajaranQuery::create(null, $criteria);
        $query->joinWith('RombonganBelajarRelatedByRombonganBelajarId', $join_behavior);

        return $this->getPembelajaransRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related PembelajaransRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     */
    public function getPembelajaransRelatedBySemesterIdJoinMataPelajaranRelatedByMataPelajaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PembelajaranQuery::create(null, $criteria);
        $query->joinWith('MataPelajaranRelatedByMataPelajaranId', $join_behavior);

        return $this->getPembelajaransRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related PembelajaransRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     */
    public function getPembelajaransRelatedBySemesterIdJoinMataPelajaranRelatedByMataPelajaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PembelajaranQuery::create(null, $criteria);
        $query->joinWith('MataPelajaranRelatedByMataPelajaranId', $join_behavior);

        return $this->getPembelajaransRelatedBySemesterId($query, $con);
    }

    /**
     * Clears out the collPrasaranaLongitudinalsRelatedBySemesterId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Semester The current object (for fluent API support)
     * @see        addPrasaranaLongitudinalsRelatedBySemesterId()
     */
    public function clearPrasaranaLongitudinalsRelatedBySemesterId()
    {
        $this->collPrasaranaLongitudinalsRelatedBySemesterId = null; // important to set this to null since that means it is uninitialized
        $this->collPrasaranaLongitudinalsRelatedBySemesterIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPrasaranaLongitudinalsRelatedBySemesterId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPrasaranaLongitudinalsRelatedBySemesterId($v = true)
    {
        $this->collPrasaranaLongitudinalsRelatedBySemesterIdPartial = $v;
    }

    /**
     * Initializes the collPrasaranaLongitudinalsRelatedBySemesterId collection.
     *
     * By default this just sets the collPrasaranaLongitudinalsRelatedBySemesterId collection to an empty array (like clearcollPrasaranaLongitudinalsRelatedBySemesterId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPrasaranaLongitudinalsRelatedBySemesterId($overrideExisting = true)
    {
        if (null !== $this->collPrasaranaLongitudinalsRelatedBySemesterId && !$overrideExisting) {
            return;
        }
        $this->collPrasaranaLongitudinalsRelatedBySemesterId = new PropelObjectCollection();
        $this->collPrasaranaLongitudinalsRelatedBySemesterId->setModel('PrasaranaLongitudinal');
    }

    /**
     * Gets an array of PrasaranaLongitudinal objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Semester is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|PrasaranaLongitudinal[] List of PrasaranaLongitudinal objects
     * @throws PropelException
     */
    public function getPrasaranaLongitudinalsRelatedBySemesterId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPrasaranaLongitudinalsRelatedBySemesterIdPartial && !$this->isNew();
        if (null === $this->collPrasaranaLongitudinalsRelatedBySemesterId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPrasaranaLongitudinalsRelatedBySemesterId) {
                // return empty collection
                $this->initPrasaranaLongitudinalsRelatedBySemesterId();
            } else {
                $collPrasaranaLongitudinalsRelatedBySemesterId = PrasaranaLongitudinalQuery::create(null, $criteria)
                    ->filterBySemesterRelatedBySemesterId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPrasaranaLongitudinalsRelatedBySemesterIdPartial && count($collPrasaranaLongitudinalsRelatedBySemesterId)) {
                      $this->initPrasaranaLongitudinalsRelatedBySemesterId(false);

                      foreach($collPrasaranaLongitudinalsRelatedBySemesterId as $obj) {
                        if (false == $this->collPrasaranaLongitudinalsRelatedBySemesterId->contains($obj)) {
                          $this->collPrasaranaLongitudinalsRelatedBySemesterId->append($obj);
                        }
                      }

                      $this->collPrasaranaLongitudinalsRelatedBySemesterIdPartial = true;
                    }

                    $collPrasaranaLongitudinalsRelatedBySemesterId->getInternalIterator()->rewind();
                    return $collPrasaranaLongitudinalsRelatedBySemesterId;
                }

                if($partial && $this->collPrasaranaLongitudinalsRelatedBySemesterId) {
                    foreach($this->collPrasaranaLongitudinalsRelatedBySemesterId as $obj) {
                        if($obj->isNew()) {
                            $collPrasaranaLongitudinalsRelatedBySemesterId[] = $obj;
                        }
                    }
                }

                $this->collPrasaranaLongitudinalsRelatedBySemesterId = $collPrasaranaLongitudinalsRelatedBySemesterId;
                $this->collPrasaranaLongitudinalsRelatedBySemesterIdPartial = false;
            }
        }

        return $this->collPrasaranaLongitudinalsRelatedBySemesterId;
    }

    /**
     * Sets a collection of PrasaranaLongitudinalRelatedBySemesterId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $prasaranaLongitudinalsRelatedBySemesterId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Semester The current object (for fluent API support)
     */
    public function setPrasaranaLongitudinalsRelatedBySemesterId(PropelCollection $prasaranaLongitudinalsRelatedBySemesterId, PropelPDO $con = null)
    {
        $prasaranaLongitudinalsRelatedBySemesterIdToDelete = $this->getPrasaranaLongitudinalsRelatedBySemesterId(new Criteria(), $con)->diff($prasaranaLongitudinalsRelatedBySemesterId);

        $this->prasaranaLongitudinalsRelatedBySemesterIdScheduledForDeletion = unserialize(serialize($prasaranaLongitudinalsRelatedBySemesterIdToDelete));

        foreach ($prasaranaLongitudinalsRelatedBySemesterIdToDelete as $prasaranaLongitudinalRelatedBySemesterIdRemoved) {
            $prasaranaLongitudinalRelatedBySemesterIdRemoved->setSemesterRelatedBySemesterId(null);
        }

        $this->collPrasaranaLongitudinalsRelatedBySemesterId = null;
        foreach ($prasaranaLongitudinalsRelatedBySemesterId as $prasaranaLongitudinalRelatedBySemesterId) {
            $this->addPrasaranaLongitudinalRelatedBySemesterId($prasaranaLongitudinalRelatedBySemesterId);
        }

        $this->collPrasaranaLongitudinalsRelatedBySemesterId = $prasaranaLongitudinalsRelatedBySemesterId;
        $this->collPrasaranaLongitudinalsRelatedBySemesterIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PrasaranaLongitudinal objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related PrasaranaLongitudinal objects.
     * @throws PropelException
     */
    public function countPrasaranaLongitudinalsRelatedBySemesterId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPrasaranaLongitudinalsRelatedBySemesterIdPartial && !$this->isNew();
        if (null === $this->collPrasaranaLongitudinalsRelatedBySemesterId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPrasaranaLongitudinalsRelatedBySemesterId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPrasaranaLongitudinalsRelatedBySemesterId());
            }
            $query = PrasaranaLongitudinalQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySemesterRelatedBySemesterId($this)
                ->count($con);
        }

        return count($this->collPrasaranaLongitudinalsRelatedBySemesterId);
    }

    /**
     * Method called to associate a PrasaranaLongitudinal object to this object
     * through the PrasaranaLongitudinal foreign key attribute.
     *
     * @param    PrasaranaLongitudinal $l PrasaranaLongitudinal
     * @return Semester The current object (for fluent API support)
     */
    public function addPrasaranaLongitudinalRelatedBySemesterId(PrasaranaLongitudinal $l)
    {
        if ($this->collPrasaranaLongitudinalsRelatedBySemesterId === null) {
            $this->initPrasaranaLongitudinalsRelatedBySemesterId();
            $this->collPrasaranaLongitudinalsRelatedBySemesterIdPartial = true;
        }
        if (!in_array($l, $this->collPrasaranaLongitudinalsRelatedBySemesterId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPrasaranaLongitudinalRelatedBySemesterId($l);
        }

        return $this;
    }

    /**
     * @param	PrasaranaLongitudinalRelatedBySemesterId $prasaranaLongitudinalRelatedBySemesterId The prasaranaLongitudinalRelatedBySemesterId object to add.
     */
    protected function doAddPrasaranaLongitudinalRelatedBySemesterId($prasaranaLongitudinalRelatedBySemesterId)
    {
        $this->collPrasaranaLongitudinalsRelatedBySemesterId[]= $prasaranaLongitudinalRelatedBySemesterId;
        $prasaranaLongitudinalRelatedBySemesterId->setSemesterRelatedBySemesterId($this);
    }

    /**
     * @param	PrasaranaLongitudinalRelatedBySemesterId $prasaranaLongitudinalRelatedBySemesterId The prasaranaLongitudinalRelatedBySemesterId object to remove.
     * @return Semester The current object (for fluent API support)
     */
    public function removePrasaranaLongitudinalRelatedBySemesterId($prasaranaLongitudinalRelatedBySemesterId)
    {
        if ($this->getPrasaranaLongitudinalsRelatedBySemesterId()->contains($prasaranaLongitudinalRelatedBySemesterId)) {
            $this->collPrasaranaLongitudinalsRelatedBySemesterId->remove($this->collPrasaranaLongitudinalsRelatedBySemesterId->search($prasaranaLongitudinalRelatedBySemesterId));
            if (null === $this->prasaranaLongitudinalsRelatedBySemesterIdScheduledForDeletion) {
                $this->prasaranaLongitudinalsRelatedBySemesterIdScheduledForDeletion = clone $this->collPrasaranaLongitudinalsRelatedBySemesterId;
                $this->prasaranaLongitudinalsRelatedBySemesterIdScheduledForDeletion->clear();
            }
            $this->prasaranaLongitudinalsRelatedBySemesterIdScheduledForDeletion[]= clone $prasaranaLongitudinalRelatedBySemesterId;
            $prasaranaLongitudinalRelatedBySemesterId->setSemesterRelatedBySemesterId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related PrasaranaLongitudinalsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PrasaranaLongitudinal[] List of PrasaranaLongitudinal objects
     */
    public function getPrasaranaLongitudinalsRelatedBySemesterIdJoinPrasaranaRelatedByPrasaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PrasaranaLongitudinalQuery::create(null, $criteria);
        $query->joinWith('PrasaranaRelatedByPrasaranaId', $join_behavior);

        return $this->getPrasaranaLongitudinalsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related PrasaranaLongitudinalsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PrasaranaLongitudinal[] List of PrasaranaLongitudinal objects
     */
    public function getPrasaranaLongitudinalsRelatedBySemesterIdJoinPrasaranaRelatedByPrasaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PrasaranaLongitudinalQuery::create(null, $criteria);
        $query->joinWith('PrasaranaRelatedByPrasaranaId', $join_behavior);

        return $this->getPrasaranaLongitudinalsRelatedBySemesterId($query, $con);
    }

    /**
     * Clears out the collPrasaranaLongitudinalsRelatedBySemesterId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Semester The current object (for fluent API support)
     * @see        addPrasaranaLongitudinalsRelatedBySemesterId()
     */
    public function clearPrasaranaLongitudinalsRelatedBySemesterId()
    {
        $this->collPrasaranaLongitudinalsRelatedBySemesterId = null; // important to set this to null since that means it is uninitialized
        $this->collPrasaranaLongitudinalsRelatedBySemesterIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPrasaranaLongitudinalsRelatedBySemesterId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPrasaranaLongitudinalsRelatedBySemesterId($v = true)
    {
        $this->collPrasaranaLongitudinalsRelatedBySemesterIdPartial = $v;
    }

    /**
     * Initializes the collPrasaranaLongitudinalsRelatedBySemesterId collection.
     *
     * By default this just sets the collPrasaranaLongitudinalsRelatedBySemesterId collection to an empty array (like clearcollPrasaranaLongitudinalsRelatedBySemesterId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPrasaranaLongitudinalsRelatedBySemesterId($overrideExisting = true)
    {
        if (null !== $this->collPrasaranaLongitudinalsRelatedBySemesterId && !$overrideExisting) {
            return;
        }
        $this->collPrasaranaLongitudinalsRelatedBySemesterId = new PropelObjectCollection();
        $this->collPrasaranaLongitudinalsRelatedBySemesterId->setModel('PrasaranaLongitudinal');
    }

    /**
     * Gets an array of PrasaranaLongitudinal objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Semester is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|PrasaranaLongitudinal[] List of PrasaranaLongitudinal objects
     * @throws PropelException
     */
    public function getPrasaranaLongitudinalsRelatedBySemesterId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPrasaranaLongitudinalsRelatedBySemesterIdPartial && !$this->isNew();
        if (null === $this->collPrasaranaLongitudinalsRelatedBySemesterId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPrasaranaLongitudinalsRelatedBySemesterId) {
                // return empty collection
                $this->initPrasaranaLongitudinalsRelatedBySemesterId();
            } else {
                $collPrasaranaLongitudinalsRelatedBySemesterId = PrasaranaLongitudinalQuery::create(null, $criteria)
                    ->filterBySemesterRelatedBySemesterId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPrasaranaLongitudinalsRelatedBySemesterIdPartial && count($collPrasaranaLongitudinalsRelatedBySemesterId)) {
                      $this->initPrasaranaLongitudinalsRelatedBySemesterId(false);

                      foreach($collPrasaranaLongitudinalsRelatedBySemesterId as $obj) {
                        if (false == $this->collPrasaranaLongitudinalsRelatedBySemesterId->contains($obj)) {
                          $this->collPrasaranaLongitudinalsRelatedBySemesterId->append($obj);
                        }
                      }

                      $this->collPrasaranaLongitudinalsRelatedBySemesterIdPartial = true;
                    }

                    $collPrasaranaLongitudinalsRelatedBySemesterId->getInternalIterator()->rewind();
                    return $collPrasaranaLongitudinalsRelatedBySemesterId;
                }

                if($partial && $this->collPrasaranaLongitudinalsRelatedBySemesterId) {
                    foreach($this->collPrasaranaLongitudinalsRelatedBySemesterId as $obj) {
                        if($obj->isNew()) {
                            $collPrasaranaLongitudinalsRelatedBySemesterId[] = $obj;
                        }
                    }
                }

                $this->collPrasaranaLongitudinalsRelatedBySemesterId = $collPrasaranaLongitudinalsRelatedBySemesterId;
                $this->collPrasaranaLongitudinalsRelatedBySemesterIdPartial = false;
            }
        }

        return $this->collPrasaranaLongitudinalsRelatedBySemesterId;
    }

    /**
     * Sets a collection of PrasaranaLongitudinalRelatedBySemesterId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $prasaranaLongitudinalsRelatedBySemesterId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Semester The current object (for fluent API support)
     */
    public function setPrasaranaLongitudinalsRelatedBySemesterId(PropelCollection $prasaranaLongitudinalsRelatedBySemesterId, PropelPDO $con = null)
    {
        $prasaranaLongitudinalsRelatedBySemesterIdToDelete = $this->getPrasaranaLongitudinalsRelatedBySemesterId(new Criteria(), $con)->diff($prasaranaLongitudinalsRelatedBySemesterId);

        $this->prasaranaLongitudinalsRelatedBySemesterIdScheduledForDeletion = unserialize(serialize($prasaranaLongitudinalsRelatedBySemesterIdToDelete));

        foreach ($prasaranaLongitudinalsRelatedBySemesterIdToDelete as $prasaranaLongitudinalRelatedBySemesterIdRemoved) {
            $prasaranaLongitudinalRelatedBySemesterIdRemoved->setSemesterRelatedBySemesterId(null);
        }

        $this->collPrasaranaLongitudinalsRelatedBySemesterId = null;
        foreach ($prasaranaLongitudinalsRelatedBySemesterId as $prasaranaLongitudinalRelatedBySemesterId) {
            $this->addPrasaranaLongitudinalRelatedBySemesterId($prasaranaLongitudinalRelatedBySemesterId);
        }

        $this->collPrasaranaLongitudinalsRelatedBySemesterId = $prasaranaLongitudinalsRelatedBySemesterId;
        $this->collPrasaranaLongitudinalsRelatedBySemesterIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PrasaranaLongitudinal objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related PrasaranaLongitudinal objects.
     * @throws PropelException
     */
    public function countPrasaranaLongitudinalsRelatedBySemesterId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPrasaranaLongitudinalsRelatedBySemesterIdPartial && !$this->isNew();
        if (null === $this->collPrasaranaLongitudinalsRelatedBySemesterId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPrasaranaLongitudinalsRelatedBySemesterId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPrasaranaLongitudinalsRelatedBySemesterId());
            }
            $query = PrasaranaLongitudinalQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySemesterRelatedBySemesterId($this)
                ->count($con);
        }

        return count($this->collPrasaranaLongitudinalsRelatedBySemesterId);
    }

    /**
     * Method called to associate a PrasaranaLongitudinal object to this object
     * through the PrasaranaLongitudinal foreign key attribute.
     *
     * @param    PrasaranaLongitudinal $l PrasaranaLongitudinal
     * @return Semester The current object (for fluent API support)
     */
    public function addPrasaranaLongitudinalRelatedBySemesterId(PrasaranaLongitudinal $l)
    {
        if ($this->collPrasaranaLongitudinalsRelatedBySemesterId === null) {
            $this->initPrasaranaLongitudinalsRelatedBySemesterId();
            $this->collPrasaranaLongitudinalsRelatedBySemesterIdPartial = true;
        }
        if (!in_array($l, $this->collPrasaranaLongitudinalsRelatedBySemesterId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPrasaranaLongitudinalRelatedBySemesterId($l);
        }

        return $this;
    }

    /**
     * @param	PrasaranaLongitudinalRelatedBySemesterId $prasaranaLongitudinalRelatedBySemesterId The prasaranaLongitudinalRelatedBySemesterId object to add.
     */
    protected function doAddPrasaranaLongitudinalRelatedBySemesterId($prasaranaLongitudinalRelatedBySemesterId)
    {
        $this->collPrasaranaLongitudinalsRelatedBySemesterId[]= $prasaranaLongitudinalRelatedBySemesterId;
        $prasaranaLongitudinalRelatedBySemesterId->setSemesterRelatedBySemesterId($this);
    }

    /**
     * @param	PrasaranaLongitudinalRelatedBySemesterId $prasaranaLongitudinalRelatedBySemesterId The prasaranaLongitudinalRelatedBySemesterId object to remove.
     * @return Semester The current object (for fluent API support)
     */
    public function removePrasaranaLongitudinalRelatedBySemesterId($prasaranaLongitudinalRelatedBySemesterId)
    {
        if ($this->getPrasaranaLongitudinalsRelatedBySemesterId()->contains($prasaranaLongitudinalRelatedBySemesterId)) {
            $this->collPrasaranaLongitudinalsRelatedBySemesterId->remove($this->collPrasaranaLongitudinalsRelatedBySemesterId->search($prasaranaLongitudinalRelatedBySemesterId));
            if (null === $this->prasaranaLongitudinalsRelatedBySemesterIdScheduledForDeletion) {
                $this->prasaranaLongitudinalsRelatedBySemesterIdScheduledForDeletion = clone $this->collPrasaranaLongitudinalsRelatedBySemesterId;
                $this->prasaranaLongitudinalsRelatedBySemesterIdScheduledForDeletion->clear();
            }
            $this->prasaranaLongitudinalsRelatedBySemesterIdScheduledForDeletion[]= clone $prasaranaLongitudinalRelatedBySemesterId;
            $prasaranaLongitudinalRelatedBySemesterId->setSemesterRelatedBySemesterId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related PrasaranaLongitudinalsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PrasaranaLongitudinal[] List of PrasaranaLongitudinal objects
     */
    public function getPrasaranaLongitudinalsRelatedBySemesterIdJoinPrasaranaRelatedByPrasaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PrasaranaLongitudinalQuery::create(null, $criteria);
        $query->joinWith('PrasaranaRelatedByPrasaranaId', $join_behavior);

        return $this->getPrasaranaLongitudinalsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related PrasaranaLongitudinalsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PrasaranaLongitudinal[] List of PrasaranaLongitudinal objects
     */
    public function getPrasaranaLongitudinalsRelatedBySemesterIdJoinPrasaranaRelatedByPrasaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PrasaranaLongitudinalQuery::create(null, $criteria);
        $query->joinWith('PrasaranaRelatedByPrasaranaId', $join_behavior);

        return $this->getPrasaranaLongitudinalsRelatedBySemesterId($query, $con);
    }

    /**
     * Clears out the collRombonganBelajarsRelatedBySemesterId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Semester The current object (for fluent API support)
     * @see        addRombonganBelajarsRelatedBySemesterId()
     */
    public function clearRombonganBelajarsRelatedBySemesterId()
    {
        $this->collRombonganBelajarsRelatedBySemesterId = null; // important to set this to null since that means it is uninitialized
        $this->collRombonganBelajarsRelatedBySemesterIdPartial = null;

        return $this;
    }

    /**
     * reset is the collRombonganBelajarsRelatedBySemesterId collection loaded partially
     *
     * @return void
     */
    public function resetPartialRombonganBelajarsRelatedBySemesterId($v = true)
    {
        $this->collRombonganBelajarsRelatedBySemesterIdPartial = $v;
    }

    /**
     * Initializes the collRombonganBelajarsRelatedBySemesterId collection.
     *
     * By default this just sets the collRombonganBelajarsRelatedBySemesterId collection to an empty array (like clearcollRombonganBelajarsRelatedBySemesterId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRombonganBelajarsRelatedBySemesterId($overrideExisting = true)
    {
        if (null !== $this->collRombonganBelajarsRelatedBySemesterId && !$overrideExisting) {
            return;
        }
        $this->collRombonganBelajarsRelatedBySemesterId = new PropelObjectCollection();
        $this->collRombonganBelajarsRelatedBySemesterId->setModel('RombonganBelajar');
    }

    /**
     * Gets an array of RombonganBelajar objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Semester is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     * @throws PropelException
     */
    public function getRombonganBelajarsRelatedBySemesterId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRombonganBelajarsRelatedBySemesterIdPartial && !$this->isNew();
        if (null === $this->collRombonganBelajarsRelatedBySemesterId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRombonganBelajarsRelatedBySemesterId) {
                // return empty collection
                $this->initRombonganBelajarsRelatedBySemesterId();
            } else {
                $collRombonganBelajarsRelatedBySemesterId = RombonganBelajarQuery::create(null, $criteria)
                    ->filterBySemesterRelatedBySemesterId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRombonganBelajarsRelatedBySemesterIdPartial && count($collRombonganBelajarsRelatedBySemesterId)) {
                      $this->initRombonganBelajarsRelatedBySemesterId(false);

                      foreach($collRombonganBelajarsRelatedBySemesterId as $obj) {
                        if (false == $this->collRombonganBelajarsRelatedBySemesterId->contains($obj)) {
                          $this->collRombonganBelajarsRelatedBySemesterId->append($obj);
                        }
                      }

                      $this->collRombonganBelajarsRelatedBySemesterIdPartial = true;
                    }

                    $collRombonganBelajarsRelatedBySemesterId->getInternalIterator()->rewind();
                    return $collRombonganBelajarsRelatedBySemesterId;
                }

                if($partial && $this->collRombonganBelajarsRelatedBySemesterId) {
                    foreach($this->collRombonganBelajarsRelatedBySemesterId as $obj) {
                        if($obj->isNew()) {
                            $collRombonganBelajarsRelatedBySemesterId[] = $obj;
                        }
                    }
                }

                $this->collRombonganBelajarsRelatedBySemesterId = $collRombonganBelajarsRelatedBySemesterId;
                $this->collRombonganBelajarsRelatedBySemesterIdPartial = false;
            }
        }

        return $this->collRombonganBelajarsRelatedBySemesterId;
    }

    /**
     * Sets a collection of RombonganBelajarRelatedBySemesterId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rombonganBelajarsRelatedBySemesterId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Semester The current object (for fluent API support)
     */
    public function setRombonganBelajarsRelatedBySemesterId(PropelCollection $rombonganBelajarsRelatedBySemesterId, PropelPDO $con = null)
    {
        $rombonganBelajarsRelatedBySemesterIdToDelete = $this->getRombonganBelajarsRelatedBySemesterId(new Criteria(), $con)->diff($rombonganBelajarsRelatedBySemesterId);

        $this->rombonganBelajarsRelatedBySemesterIdScheduledForDeletion = unserialize(serialize($rombonganBelajarsRelatedBySemesterIdToDelete));

        foreach ($rombonganBelajarsRelatedBySemesterIdToDelete as $rombonganBelajarRelatedBySemesterIdRemoved) {
            $rombonganBelajarRelatedBySemesterIdRemoved->setSemesterRelatedBySemesterId(null);
        }

        $this->collRombonganBelajarsRelatedBySemesterId = null;
        foreach ($rombonganBelajarsRelatedBySemesterId as $rombonganBelajarRelatedBySemesterId) {
            $this->addRombonganBelajarRelatedBySemesterId($rombonganBelajarRelatedBySemesterId);
        }

        $this->collRombonganBelajarsRelatedBySemesterId = $rombonganBelajarsRelatedBySemesterId;
        $this->collRombonganBelajarsRelatedBySemesterIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RombonganBelajar objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RombonganBelajar objects.
     * @throws PropelException
     */
    public function countRombonganBelajarsRelatedBySemesterId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRombonganBelajarsRelatedBySemesterIdPartial && !$this->isNew();
        if (null === $this->collRombonganBelajarsRelatedBySemesterId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRombonganBelajarsRelatedBySemesterId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getRombonganBelajarsRelatedBySemesterId());
            }
            $query = RombonganBelajarQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySemesterRelatedBySemesterId($this)
                ->count($con);
        }

        return count($this->collRombonganBelajarsRelatedBySemesterId);
    }

    /**
     * Method called to associate a RombonganBelajar object to this object
     * through the RombonganBelajar foreign key attribute.
     *
     * @param    RombonganBelajar $l RombonganBelajar
     * @return Semester The current object (for fluent API support)
     */
    public function addRombonganBelajarRelatedBySemesterId(RombonganBelajar $l)
    {
        if ($this->collRombonganBelajarsRelatedBySemesterId === null) {
            $this->initRombonganBelajarsRelatedBySemesterId();
            $this->collRombonganBelajarsRelatedBySemesterIdPartial = true;
        }
        if (!in_array($l, $this->collRombonganBelajarsRelatedBySemesterId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRombonganBelajarRelatedBySemesterId($l);
        }

        return $this;
    }

    /**
     * @param	RombonganBelajarRelatedBySemesterId $rombonganBelajarRelatedBySemesterId The rombonganBelajarRelatedBySemesterId object to add.
     */
    protected function doAddRombonganBelajarRelatedBySemesterId($rombonganBelajarRelatedBySemesterId)
    {
        $this->collRombonganBelajarsRelatedBySemesterId[]= $rombonganBelajarRelatedBySemesterId;
        $rombonganBelajarRelatedBySemesterId->setSemesterRelatedBySemesterId($this);
    }

    /**
     * @param	RombonganBelajarRelatedBySemesterId $rombonganBelajarRelatedBySemesterId The rombonganBelajarRelatedBySemesterId object to remove.
     * @return Semester The current object (for fluent API support)
     */
    public function removeRombonganBelajarRelatedBySemesterId($rombonganBelajarRelatedBySemesterId)
    {
        if ($this->getRombonganBelajarsRelatedBySemesterId()->contains($rombonganBelajarRelatedBySemesterId)) {
            $this->collRombonganBelajarsRelatedBySemesterId->remove($this->collRombonganBelajarsRelatedBySemesterId->search($rombonganBelajarRelatedBySemesterId));
            if (null === $this->rombonganBelajarsRelatedBySemesterIdScheduledForDeletion) {
                $this->rombonganBelajarsRelatedBySemesterIdScheduledForDeletion = clone $this->collRombonganBelajarsRelatedBySemesterId;
                $this->rombonganBelajarsRelatedBySemesterIdScheduledForDeletion->clear();
            }
            $this->rombonganBelajarsRelatedBySemesterIdScheduledForDeletion[]= clone $rombonganBelajarRelatedBySemesterId;
            $rombonganBelajarRelatedBySemesterId->setSemesterRelatedBySemesterId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedBySemesterIdJoinJurusanSpRelatedByJurusanSpId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('JurusanSpRelatedByJurusanSpId', $join_behavior);

        return $this->getRombonganBelajarsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedBySemesterIdJoinJurusanSpRelatedByJurusanSpId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('JurusanSpRelatedByJurusanSpId', $join_behavior);

        return $this->getRombonganBelajarsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedBySemesterIdJoinPrasaranaRelatedByPrasaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('PrasaranaRelatedByPrasaranaId', $join_behavior);

        return $this->getRombonganBelajarsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedBySemesterIdJoinPrasaranaRelatedByPrasaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('PrasaranaRelatedByPrasaranaId', $join_behavior);

        return $this->getRombonganBelajarsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedBySemesterIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getRombonganBelajarsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedBySemesterIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getRombonganBelajarsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedBySemesterIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getRombonganBelajarsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedBySemesterIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getRombonganBelajarsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedBySemesterIdJoinKebutuhanKhususRelatedByKebutuhanKhususId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususId', $join_behavior);

        return $this->getRombonganBelajarsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedBySemesterIdJoinKebutuhanKhususRelatedByKebutuhanKhususId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususId', $join_behavior);

        return $this->getRombonganBelajarsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedBySemesterIdJoinKurikulumRelatedByKurikulumId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('KurikulumRelatedByKurikulumId', $join_behavior);

        return $this->getRombonganBelajarsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedBySemesterIdJoinKurikulumRelatedByKurikulumId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('KurikulumRelatedByKurikulumId', $join_behavior);

        return $this->getRombonganBelajarsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedBySemesterIdJoinTingkatPendidikanRelatedByTingkatPendidikanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('TingkatPendidikanRelatedByTingkatPendidikanId', $join_behavior);

        return $this->getRombonganBelajarsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedBySemesterIdJoinTingkatPendidikanRelatedByTingkatPendidikanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('TingkatPendidikanRelatedByTingkatPendidikanId', $join_behavior);

        return $this->getRombonganBelajarsRelatedBySemesterId($query, $con);
    }

    /**
     * Clears out the collRombonganBelajarsRelatedBySemesterId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Semester The current object (for fluent API support)
     * @see        addRombonganBelajarsRelatedBySemesterId()
     */
    public function clearRombonganBelajarsRelatedBySemesterId()
    {
        $this->collRombonganBelajarsRelatedBySemesterId = null; // important to set this to null since that means it is uninitialized
        $this->collRombonganBelajarsRelatedBySemesterIdPartial = null;

        return $this;
    }

    /**
     * reset is the collRombonganBelajarsRelatedBySemesterId collection loaded partially
     *
     * @return void
     */
    public function resetPartialRombonganBelajarsRelatedBySemesterId($v = true)
    {
        $this->collRombonganBelajarsRelatedBySemesterIdPartial = $v;
    }

    /**
     * Initializes the collRombonganBelajarsRelatedBySemesterId collection.
     *
     * By default this just sets the collRombonganBelajarsRelatedBySemesterId collection to an empty array (like clearcollRombonganBelajarsRelatedBySemesterId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRombonganBelajarsRelatedBySemesterId($overrideExisting = true)
    {
        if (null !== $this->collRombonganBelajarsRelatedBySemesterId && !$overrideExisting) {
            return;
        }
        $this->collRombonganBelajarsRelatedBySemesterId = new PropelObjectCollection();
        $this->collRombonganBelajarsRelatedBySemesterId->setModel('RombonganBelajar');
    }

    /**
     * Gets an array of RombonganBelajar objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Semester is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     * @throws PropelException
     */
    public function getRombonganBelajarsRelatedBySemesterId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRombonganBelajarsRelatedBySemesterIdPartial && !$this->isNew();
        if (null === $this->collRombonganBelajarsRelatedBySemesterId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRombonganBelajarsRelatedBySemesterId) {
                // return empty collection
                $this->initRombonganBelajarsRelatedBySemesterId();
            } else {
                $collRombonganBelajarsRelatedBySemesterId = RombonganBelajarQuery::create(null, $criteria)
                    ->filterBySemesterRelatedBySemesterId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRombonganBelajarsRelatedBySemesterIdPartial && count($collRombonganBelajarsRelatedBySemesterId)) {
                      $this->initRombonganBelajarsRelatedBySemesterId(false);

                      foreach($collRombonganBelajarsRelatedBySemesterId as $obj) {
                        if (false == $this->collRombonganBelajarsRelatedBySemesterId->contains($obj)) {
                          $this->collRombonganBelajarsRelatedBySemesterId->append($obj);
                        }
                      }

                      $this->collRombonganBelajarsRelatedBySemesterIdPartial = true;
                    }

                    $collRombonganBelajarsRelatedBySemesterId->getInternalIterator()->rewind();
                    return $collRombonganBelajarsRelatedBySemesterId;
                }

                if($partial && $this->collRombonganBelajarsRelatedBySemesterId) {
                    foreach($this->collRombonganBelajarsRelatedBySemesterId as $obj) {
                        if($obj->isNew()) {
                            $collRombonganBelajarsRelatedBySemesterId[] = $obj;
                        }
                    }
                }

                $this->collRombonganBelajarsRelatedBySemesterId = $collRombonganBelajarsRelatedBySemesterId;
                $this->collRombonganBelajarsRelatedBySemesterIdPartial = false;
            }
        }

        return $this->collRombonganBelajarsRelatedBySemesterId;
    }

    /**
     * Sets a collection of RombonganBelajarRelatedBySemesterId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rombonganBelajarsRelatedBySemesterId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Semester The current object (for fluent API support)
     */
    public function setRombonganBelajarsRelatedBySemesterId(PropelCollection $rombonganBelajarsRelatedBySemesterId, PropelPDO $con = null)
    {
        $rombonganBelajarsRelatedBySemesterIdToDelete = $this->getRombonganBelajarsRelatedBySemesterId(new Criteria(), $con)->diff($rombonganBelajarsRelatedBySemesterId);

        $this->rombonganBelajarsRelatedBySemesterIdScheduledForDeletion = unserialize(serialize($rombonganBelajarsRelatedBySemesterIdToDelete));

        foreach ($rombonganBelajarsRelatedBySemesterIdToDelete as $rombonganBelajarRelatedBySemesterIdRemoved) {
            $rombonganBelajarRelatedBySemesterIdRemoved->setSemesterRelatedBySemesterId(null);
        }

        $this->collRombonganBelajarsRelatedBySemesterId = null;
        foreach ($rombonganBelajarsRelatedBySemesterId as $rombonganBelajarRelatedBySemesterId) {
            $this->addRombonganBelajarRelatedBySemesterId($rombonganBelajarRelatedBySemesterId);
        }

        $this->collRombonganBelajarsRelatedBySemesterId = $rombonganBelajarsRelatedBySemesterId;
        $this->collRombonganBelajarsRelatedBySemesterIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RombonganBelajar objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RombonganBelajar objects.
     * @throws PropelException
     */
    public function countRombonganBelajarsRelatedBySemesterId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRombonganBelajarsRelatedBySemesterIdPartial && !$this->isNew();
        if (null === $this->collRombonganBelajarsRelatedBySemesterId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRombonganBelajarsRelatedBySemesterId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getRombonganBelajarsRelatedBySemesterId());
            }
            $query = RombonganBelajarQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySemesterRelatedBySemesterId($this)
                ->count($con);
        }

        return count($this->collRombonganBelajarsRelatedBySemesterId);
    }

    /**
     * Method called to associate a RombonganBelajar object to this object
     * through the RombonganBelajar foreign key attribute.
     *
     * @param    RombonganBelajar $l RombonganBelajar
     * @return Semester The current object (for fluent API support)
     */
    public function addRombonganBelajarRelatedBySemesterId(RombonganBelajar $l)
    {
        if ($this->collRombonganBelajarsRelatedBySemesterId === null) {
            $this->initRombonganBelajarsRelatedBySemesterId();
            $this->collRombonganBelajarsRelatedBySemesterIdPartial = true;
        }
        if (!in_array($l, $this->collRombonganBelajarsRelatedBySemesterId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRombonganBelajarRelatedBySemesterId($l);
        }

        return $this;
    }

    /**
     * @param	RombonganBelajarRelatedBySemesterId $rombonganBelajarRelatedBySemesterId The rombonganBelajarRelatedBySemesterId object to add.
     */
    protected function doAddRombonganBelajarRelatedBySemesterId($rombonganBelajarRelatedBySemesterId)
    {
        $this->collRombonganBelajarsRelatedBySemesterId[]= $rombonganBelajarRelatedBySemesterId;
        $rombonganBelajarRelatedBySemesterId->setSemesterRelatedBySemesterId($this);
    }

    /**
     * @param	RombonganBelajarRelatedBySemesterId $rombonganBelajarRelatedBySemesterId The rombonganBelajarRelatedBySemesterId object to remove.
     * @return Semester The current object (for fluent API support)
     */
    public function removeRombonganBelajarRelatedBySemesterId($rombonganBelajarRelatedBySemesterId)
    {
        if ($this->getRombonganBelajarsRelatedBySemesterId()->contains($rombonganBelajarRelatedBySemesterId)) {
            $this->collRombonganBelajarsRelatedBySemesterId->remove($this->collRombonganBelajarsRelatedBySemesterId->search($rombonganBelajarRelatedBySemesterId));
            if (null === $this->rombonganBelajarsRelatedBySemesterIdScheduledForDeletion) {
                $this->rombonganBelajarsRelatedBySemesterIdScheduledForDeletion = clone $this->collRombonganBelajarsRelatedBySemesterId;
                $this->rombonganBelajarsRelatedBySemesterIdScheduledForDeletion->clear();
            }
            $this->rombonganBelajarsRelatedBySemesterIdScheduledForDeletion[]= clone $rombonganBelajarRelatedBySemesterId;
            $rombonganBelajarRelatedBySemesterId->setSemesterRelatedBySemesterId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedBySemesterIdJoinJurusanSpRelatedByJurusanSpId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('JurusanSpRelatedByJurusanSpId', $join_behavior);

        return $this->getRombonganBelajarsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedBySemesterIdJoinJurusanSpRelatedByJurusanSpId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('JurusanSpRelatedByJurusanSpId', $join_behavior);

        return $this->getRombonganBelajarsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedBySemesterIdJoinPrasaranaRelatedByPrasaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('PrasaranaRelatedByPrasaranaId', $join_behavior);

        return $this->getRombonganBelajarsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedBySemesterIdJoinPrasaranaRelatedByPrasaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('PrasaranaRelatedByPrasaranaId', $join_behavior);

        return $this->getRombonganBelajarsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedBySemesterIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getRombonganBelajarsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedBySemesterIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getRombonganBelajarsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedBySemesterIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getRombonganBelajarsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedBySemesterIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getRombonganBelajarsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedBySemesterIdJoinKebutuhanKhususRelatedByKebutuhanKhususId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususId', $join_behavior);

        return $this->getRombonganBelajarsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedBySemesterIdJoinKebutuhanKhususRelatedByKebutuhanKhususId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususId', $join_behavior);

        return $this->getRombonganBelajarsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedBySemesterIdJoinKurikulumRelatedByKurikulumId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('KurikulumRelatedByKurikulumId', $join_behavior);

        return $this->getRombonganBelajarsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedBySemesterIdJoinKurikulumRelatedByKurikulumId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('KurikulumRelatedByKurikulumId', $join_behavior);

        return $this->getRombonganBelajarsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedBySemesterIdJoinTingkatPendidikanRelatedByTingkatPendidikanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('TingkatPendidikanRelatedByTingkatPendidikanId', $join_behavior);

        return $this->getRombonganBelajarsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedBySemesterIdJoinTingkatPendidikanRelatedByTingkatPendidikanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('TingkatPendidikanRelatedByTingkatPendidikanId', $join_behavior);

        return $this->getRombonganBelajarsRelatedBySemesterId($query, $con);
    }

    /**
     * Clears out the collBukuAlatLongitudinalsRelatedBySemesterId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Semester The current object (for fluent API support)
     * @see        addBukuAlatLongitudinalsRelatedBySemesterId()
     */
    public function clearBukuAlatLongitudinalsRelatedBySemesterId()
    {
        $this->collBukuAlatLongitudinalsRelatedBySemesterId = null; // important to set this to null since that means it is uninitialized
        $this->collBukuAlatLongitudinalsRelatedBySemesterIdPartial = null;

        return $this;
    }

    /**
     * reset is the collBukuAlatLongitudinalsRelatedBySemesterId collection loaded partially
     *
     * @return void
     */
    public function resetPartialBukuAlatLongitudinalsRelatedBySemesterId($v = true)
    {
        $this->collBukuAlatLongitudinalsRelatedBySemesterIdPartial = $v;
    }

    /**
     * Initializes the collBukuAlatLongitudinalsRelatedBySemesterId collection.
     *
     * By default this just sets the collBukuAlatLongitudinalsRelatedBySemesterId collection to an empty array (like clearcollBukuAlatLongitudinalsRelatedBySemesterId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBukuAlatLongitudinalsRelatedBySemesterId($overrideExisting = true)
    {
        if (null !== $this->collBukuAlatLongitudinalsRelatedBySemesterId && !$overrideExisting) {
            return;
        }
        $this->collBukuAlatLongitudinalsRelatedBySemesterId = new PropelObjectCollection();
        $this->collBukuAlatLongitudinalsRelatedBySemesterId->setModel('BukuAlatLongitudinal');
    }

    /**
     * Gets an array of BukuAlatLongitudinal objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Semester is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|BukuAlatLongitudinal[] List of BukuAlatLongitudinal objects
     * @throws PropelException
     */
    public function getBukuAlatLongitudinalsRelatedBySemesterId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collBukuAlatLongitudinalsRelatedBySemesterIdPartial && !$this->isNew();
        if (null === $this->collBukuAlatLongitudinalsRelatedBySemesterId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBukuAlatLongitudinalsRelatedBySemesterId) {
                // return empty collection
                $this->initBukuAlatLongitudinalsRelatedBySemesterId();
            } else {
                $collBukuAlatLongitudinalsRelatedBySemesterId = BukuAlatLongitudinalQuery::create(null, $criteria)
                    ->filterBySemesterRelatedBySemesterId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collBukuAlatLongitudinalsRelatedBySemesterIdPartial && count($collBukuAlatLongitudinalsRelatedBySemesterId)) {
                      $this->initBukuAlatLongitudinalsRelatedBySemesterId(false);

                      foreach($collBukuAlatLongitudinalsRelatedBySemesterId as $obj) {
                        if (false == $this->collBukuAlatLongitudinalsRelatedBySemesterId->contains($obj)) {
                          $this->collBukuAlatLongitudinalsRelatedBySemesterId->append($obj);
                        }
                      }

                      $this->collBukuAlatLongitudinalsRelatedBySemesterIdPartial = true;
                    }

                    $collBukuAlatLongitudinalsRelatedBySemesterId->getInternalIterator()->rewind();
                    return $collBukuAlatLongitudinalsRelatedBySemesterId;
                }

                if($partial && $this->collBukuAlatLongitudinalsRelatedBySemesterId) {
                    foreach($this->collBukuAlatLongitudinalsRelatedBySemesterId as $obj) {
                        if($obj->isNew()) {
                            $collBukuAlatLongitudinalsRelatedBySemesterId[] = $obj;
                        }
                    }
                }

                $this->collBukuAlatLongitudinalsRelatedBySemesterId = $collBukuAlatLongitudinalsRelatedBySemesterId;
                $this->collBukuAlatLongitudinalsRelatedBySemesterIdPartial = false;
            }
        }

        return $this->collBukuAlatLongitudinalsRelatedBySemesterId;
    }

    /**
     * Sets a collection of BukuAlatLongitudinalRelatedBySemesterId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $bukuAlatLongitudinalsRelatedBySemesterId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Semester The current object (for fluent API support)
     */
    public function setBukuAlatLongitudinalsRelatedBySemesterId(PropelCollection $bukuAlatLongitudinalsRelatedBySemesterId, PropelPDO $con = null)
    {
        $bukuAlatLongitudinalsRelatedBySemesterIdToDelete = $this->getBukuAlatLongitudinalsRelatedBySemesterId(new Criteria(), $con)->diff($bukuAlatLongitudinalsRelatedBySemesterId);

        $this->bukuAlatLongitudinalsRelatedBySemesterIdScheduledForDeletion = unserialize(serialize($bukuAlatLongitudinalsRelatedBySemesterIdToDelete));

        foreach ($bukuAlatLongitudinalsRelatedBySemesterIdToDelete as $bukuAlatLongitudinalRelatedBySemesterIdRemoved) {
            $bukuAlatLongitudinalRelatedBySemesterIdRemoved->setSemesterRelatedBySemesterId(null);
        }

        $this->collBukuAlatLongitudinalsRelatedBySemesterId = null;
        foreach ($bukuAlatLongitudinalsRelatedBySemesterId as $bukuAlatLongitudinalRelatedBySemesterId) {
            $this->addBukuAlatLongitudinalRelatedBySemesterId($bukuAlatLongitudinalRelatedBySemesterId);
        }

        $this->collBukuAlatLongitudinalsRelatedBySemesterId = $bukuAlatLongitudinalsRelatedBySemesterId;
        $this->collBukuAlatLongitudinalsRelatedBySemesterIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BukuAlatLongitudinal objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related BukuAlatLongitudinal objects.
     * @throws PropelException
     */
    public function countBukuAlatLongitudinalsRelatedBySemesterId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collBukuAlatLongitudinalsRelatedBySemesterIdPartial && !$this->isNew();
        if (null === $this->collBukuAlatLongitudinalsRelatedBySemesterId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBukuAlatLongitudinalsRelatedBySemesterId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getBukuAlatLongitudinalsRelatedBySemesterId());
            }
            $query = BukuAlatLongitudinalQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySemesterRelatedBySemesterId($this)
                ->count($con);
        }

        return count($this->collBukuAlatLongitudinalsRelatedBySemesterId);
    }

    /**
     * Method called to associate a BukuAlatLongitudinal object to this object
     * through the BukuAlatLongitudinal foreign key attribute.
     *
     * @param    BukuAlatLongitudinal $l BukuAlatLongitudinal
     * @return Semester The current object (for fluent API support)
     */
    public function addBukuAlatLongitudinalRelatedBySemesterId(BukuAlatLongitudinal $l)
    {
        if ($this->collBukuAlatLongitudinalsRelatedBySemesterId === null) {
            $this->initBukuAlatLongitudinalsRelatedBySemesterId();
            $this->collBukuAlatLongitudinalsRelatedBySemesterIdPartial = true;
        }
        if (!in_array($l, $this->collBukuAlatLongitudinalsRelatedBySemesterId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddBukuAlatLongitudinalRelatedBySemesterId($l);
        }

        return $this;
    }

    /**
     * @param	BukuAlatLongitudinalRelatedBySemesterId $bukuAlatLongitudinalRelatedBySemesterId The bukuAlatLongitudinalRelatedBySemesterId object to add.
     */
    protected function doAddBukuAlatLongitudinalRelatedBySemesterId($bukuAlatLongitudinalRelatedBySemesterId)
    {
        $this->collBukuAlatLongitudinalsRelatedBySemesterId[]= $bukuAlatLongitudinalRelatedBySemesterId;
        $bukuAlatLongitudinalRelatedBySemesterId->setSemesterRelatedBySemesterId($this);
    }

    /**
     * @param	BukuAlatLongitudinalRelatedBySemesterId $bukuAlatLongitudinalRelatedBySemesterId The bukuAlatLongitudinalRelatedBySemesterId object to remove.
     * @return Semester The current object (for fluent API support)
     */
    public function removeBukuAlatLongitudinalRelatedBySemesterId($bukuAlatLongitudinalRelatedBySemesterId)
    {
        if ($this->getBukuAlatLongitudinalsRelatedBySemesterId()->contains($bukuAlatLongitudinalRelatedBySemesterId)) {
            $this->collBukuAlatLongitudinalsRelatedBySemesterId->remove($this->collBukuAlatLongitudinalsRelatedBySemesterId->search($bukuAlatLongitudinalRelatedBySemesterId));
            if (null === $this->bukuAlatLongitudinalsRelatedBySemesterIdScheduledForDeletion) {
                $this->bukuAlatLongitudinalsRelatedBySemesterIdScheduledForDeletion = clone $this->collBukuAlatLongitudinalsRelatedBySemesterId;
                $this->bukuAlatLongitudinalsRelatedBySemesterIdScheduledForDeletion->clear();
            }
            $this->bukuAlatLongitudinalsRelatedBySemesterIdScheduledForDeletion[]= clone $bukuAlatLongitudinalRelatedBySemesterId;
            $bukuAlatLongitudinalRelatedBySemesterId->setSemesterRelatedBySemesterId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related BukuAlatLongitudinalsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BukuAlatLongitudinal[] List of BukuAlatLongitudinal objects
     */
    public function getBukuAlatLongitudinalsRelatedBySemesterIdJoinBukuAlatRelatedByBukuAlatId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BukuAlatLongitudinalQuery::create(null, $criteria);
        $query->joinWith('BukuAlatRelatedByBukuAlatId', $join_behavior);

        return $this->getBukuAlatLongitudinalsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related BukuAlatLongitudinalsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BukuAlatLongitudinal[] List of BukuAlatLongitudinal objects
     */
    public function getBukuAlatLongitudinalsRelatedBySemesterIdJoinBukuAlatRelatedByBukuAlatId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BukuAlatLongitudinalQuery::create(null, $criteria);
        $query->joinWith('BukuAlatRelatedByBukuAlatId', $join_behavior);

        return $this->getBukuAlatLongitudinalsRelatedBySemesterId($query, $con);
    }

    /**
     * Clears out the collBukuAlatLongitudinalsRelatedBySemesterId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Semester The current object (for fluent API support)
     * @see        addBukuAlatLongitudinalsRelatedBySemesterId()
     */
    public function clearBukuAlatLongitudinalsRelatedBySemesterId()
    {
        $this->collBukuAlatLongitudinalsRelatedBySemesterId = null; // important to set this to null since that means it is uninitialized
        $this->collBukuAlatLongitudinalsRelatedBySemesterIdPartial = null;

        return $this;
    }

    /**
     * reset is the collBukuAlatLongitudinalsRelatedBySemesterId collection loaded partially
     *
     * @return void
     */
    public function resetPartialBukuAlatLongitudinalsRelatedBySemesterId($v = true)
    {
        $this->collBukuAlatLongitudinalsRelatedBySemesterIdPartial = $v;
    }

    /**
     * Initializes the collBukuAlatLongitudinalsRelatedBySemesterId collection.
     *
     * By default this just sets the collBukuAlatLongitudinalsRelatedBySemesterId collection to an empty array (like clearcollBukuAlatLongitudinalsRelatedBySemesterId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBukuAlatLongitudinalsRelatedBySemesterId($overrideExisting = true)
    {
        if (null !== $this->collBukuAlatLongitudinalsRelatedBySemesterId && !$overrideExisting) {
            return;
        }
        $this->collBukuAlatLongitudinalsRelatedBySemesterId = new PropelObjectCollection();
        $this->collBukuAlatLongitudinalsRelatedBySemesterId->setModel('BukuAlatLongitudinal');
    }

    /**
     * Gets an array of BukuAlatLongitudinal objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Semester is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|BukuAlatLongitudinal[] List of BukuAlatLongitudinal objects
     * @throws PropelException
     */
    public function getBukuAlatLongitudinalsRelatedBySemesterId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collBukuAlatLongitudinalsRelatedBySemesterIdPartial && !$this->isNew();
        if (null === $this->collBukuAlatLongitudinalsRelatedBySemesterId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBukuAlatLongitudinalsRelatedBySemesterId) {
                // return empty collection
                $this->initBukuAlatLongitudinalsRelatedBySemesterId();
            } else {
                $collBukuAlatLongitudinalsRelatedBySemesterId = BukuAlatLongitudinalQuery::create(null, $criteria)
                    ->filterBySemesterRelatedBySemesterId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collBukuAlatLongitudinalsRelatedBySemesterIdPartial && count($collBukuAlatLongitudinalsRelatedBySemesterId)) {
                      $this->initBukuAlatLongitudinalsRelatedBySemesterId(false);

                      foreach($collBukuAlatLongitudinalsRelatedBySemesterId as $obj) {
                        if (false == $this->collBukuAlatLongitudinalsRelatedBySemesterId->contains($obj)) {
                          $this->collBukuAlatLongitudinalsRelatedBySemesterId->append($obj);
                        }
                      }

                      $this->collBukuAlatLongitudinalsRelatedBySemesterIdPartial = true;
                    }

                    $collBukuAlatLongitudinalsRelatedBySemesterId->getInternalIterator()->rewind();
                    return $collBukuAlatLongitudinalsRelatedBySemesterId;
                }

                if($partial && $this->collBukuAlatLongitudinalsRelatedBySemesterId) {
                    foreach($this->collBukuAlatLongitudinalsRelatedBySemesterId as $obj) {
                        if($obj->isNew()) {
                            $collBukuAlatLongitudinalsRelatedBySemesterId[] = $obj;
                        }
                    }
                }

                $this->collBukuAlatLongitudinalsRelatedBySemesterId = $collBukuAlatLongitudinalsRelatedBySemesterId;
                $this->collBukuAlatLongitudinalsRelatedBySemesterIdPartial = false;
            }
        }

        return $this->collBukuAlatLongitudinalsRelatedBySemesterId;
    }

    /**
     * Sets a collection of BukuAlatLongitudinalRelatedBySemesterId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $bukuAlatLongitudinalsRelatedBySemesterId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Semester The current object (for fluent API support)
     */
    public function setBukuAlatLongitudinalsRelatedBySemesterId(PropelCollection $bukuAlatLongitudinalsRelatedBySemesterId, PropelPDO $con = null)
    {
        $bukuAlatLongitudinalsRelatedBySemesterIdToDelete = $this->getBukuAlatLongitudinalsRelatedBySemesterId(new Criteria(), $con)->diff($bukuAlatLongitudinalsRelatedBySemesterId);

        $this->bukuAlatLongitudinalsRelatedBySemesterIdScheduledForDeletion = unserialize(serialize($bukuAlatLongitudinalsRelatedBySemesterIdToDelete));

        foreach ($bukuAlatLongitudinalsRelatedBySemesterIdToDelete as $bukuAlatLongitudinalRelatedBySemesterIdRemoved) {
            $bukuAlatLongitudinalRelatedBySemesterIdRemoved->setSemesterRelatedBySemesterId(null);
        }

        $this->collBukuAlatLongitudinalsRelatedBySemesterId = null;
        foreach ($bukuAlatLongitudinalsRelatedBySemesterId as $bukuAlatLongitudinalRelatedBySemesterId) {
            $this->addBukuAlatLongitudinalRelatedBySemesterId($bukuAlatLongitudinalRelatedBySemesterId);
        }

        $this->collBukuAlatLongitudinalsRelatedBySemesterId = $bukuAlatLongitudinalsRelatedBySemesterId;
        $this->collBukuAlatLongitudinalsRelatedBySemesterIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BukuAlatLongitudinal objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related BukuAlatLongitudinal objects.
     * @throws PropelException
     */
    public function countBukuAlatLongitudinalsRelatedBySemesterId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collBukuAlatLongitudinalsRelatedBySemesterIdPartial && !$this->isNew();
        if (null === $this->collBukuAlatLongitudinalsRelatedBySemesterId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBukuAlatLongitudinalsRelatedBySemesterId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getBukuAlatLongitudinalsRelatedBySemesterId());
            }
            $query = BukuAlatLongitudinalQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySemesterRelatedBySemesterId($this)
                ->count($con);
        }

        return count($this->collBukuAlatLongitudinalsRelatedBySemesterId);
    }

    /**
     * Method called to associate a BukuAlatLongitudinal object to this object
     * through the BukuAlatLongitudinal foreign key attribute.
     *
     * @param    BukuAlatLongitudinal $l BukuAlatLongitudinal
     * @return Semester The current object (for fluent API support)
     */
    public function addBukuAlatLongitudinalRelatedBySemesterId(BukuAlatLongitudinal $l)
    {
        if ($this->collBukuAlatLongitudinalsRelatedBySemesterId === null) {
            $this->initBukuAlatLongitudinalsRelatedBySemesterId();
            $this->collBukuAlatLongitudinalsRelatedBySemesterIdPartial = true;
        }
        if (!in_array($l, $this->collBukuAlatLongitudinalsRelatedBySemesterId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddBukuAlatLongitudinalRelatedBySemesterId($l);
        }

        return $this;
    }

    /**
     * @param	BukuAlatLongitudinalRelatedBySemesterId $bukuAlatLongitudinalRelatedBySemesterId The bukuAlatLongitudinalRelatedBySemesterId object to add.
     */
    protected function doAddBukuAlatLongitudinalRelatedBySemesterId($bukuAlatLongitudinalRelatedBySemesterId)
    {
        $this->collBukuAlatLongitudinalsRelatedBySemesterId[]= $bukuAlatLongitudinalRelatedBySemesterId;
        $bukuAlatLongitudinalRelatedBySemesterId->setSemesterRelatedBySemesterId($this);
    }

    /**
     * @param	BukuAlatLongitudinalRelatedBySemesterId $bukuAlatLongitudinalRelatedBySemesterId The bukuAlatLongitudinalRelatedBySemesterId object to remove.
     * @return Semester The current object (for fluent API support)
     */
    public function removeBukuAlatLongitudinalRelatedBySemesterId($bukuAlatLongitudinalRelatedBySemesterId)
    {
        if ($this->getBukuAlatLongitudinalsRelatedBySemesterId()->contains($bukuAlatLongitudinalRelatedBySemesterId)) {
            $this->collBukuAlatLongitudinalsRelatedBySemesterId->remove($this->collBukuAlatLongitudinalsRelatedBySemesterId->search($bukuAlatLongitudinalRelatedBySemesterId));
            if (null === $this->bukuAlatLongitudinalsRelatedBySemesterIdScheduledForDeletion) {
                $this->bukuAlatLongitudinalsRelatedBySemesterIdScheduledForDeletion = clone $this->collBukuAlatLongitudinalsRelatedBySemesterId;
                $this->bukuAlatLongitudinalsRelatedBySemesterIdScheduledForDeletion->clear();
            }
            $this->bukuAlatLongitudinalsRelatedBySemesterIdScheduledForDeletion[]= clone $bukuAlatLongitudinalRelatedBySemesterId;
            $bukuAlatLongitudinalRelatedBySemesterId->setSemesterRelatedBySemesterId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related BukuAlatLongitudinalsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BukuAlatLongitudinal[] List of BukuAlatLongitudinal objects
     */
    public function getBukuAlatLongitudinalsRelatedBySemesterIdJoinBukuAlatRelatedByBukuAlatId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BukuAlatLongitudinalQuery::create(null, $criteria);
        $query->joinWith('BukuAlatRelatedByBukuAlatId', $join_behavior);

        return $this->getBukuAlatLongitudinalsRelatedBySemesterId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Semester is new, it will return
     * an empty collection; or if this Semester has previously
     * been saved, it will retrieve related BukuAlatLongitudinalsRelatedBySemesterId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Semester.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BukuAlatLongitudinal[] List of BukuAlatLongitudinal objects
     */
    public function getBukuAlatLongitudinalsRelatedBySemesterIdJoinBukuAlatRelatedByBukuAlatId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BukuAlatLongitudinalQuery::create(null, $criteria);
        $query->joinWith('BukuAlatRelatedByBukuAlatId', $join_behavior);

        return $this->getBukuAlatLongitudinalsRelatedBySemesterId($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->semester_id = null;
        $this->tahun_ajaran_id = null;
        $this->nama = null;
        $this->semester = null;
        $this->periode_aktif = null;
        $this->tanggal_mulai = null;
        $this->tanggal_selesai = null;
        $this->create_date = null;
        $this->last_update = null;
        $this->expired_date = null;
        $this->last_sync = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collSekolahLongitudinalsRelatedBySemesterId) {
                foreach ($this->collSekolahLongitudinalsRelatedBySemesterId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSekolahLongitudinalsRelatedBySemesterId) {
                foreach ($this->collSekolahLongitudinalsRelatedBySemesterId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPesertaDidikLongitudinalsRelatedBySemesterId) {
                foreach ($this->collPesertaDidikLongitudinalsRelatedBySemesterId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPesertaDidikLongitudinalsRelatedBySemesterId) {
                foreach ($this->collPesertaDidikLongitudinalsRelatedBySemesterId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSanitasisRelatedBySemesterId) {
                foreach ($this->collSanitasisRelatedBySemesterId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSanitasisRelatedBySemesterId) {
                foreach ($this->collSanitasisRelatedBySemesterId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSaranaLongitudinalsRelatedBySemesterId) {
                foreach ($this->collSaranaLongitudinalsRelatedBySemesterId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSaranaLongitudinalsRelatedBySemesterId) {
                foreach ($this->collSaranaLongitudinalsRelatedBySemesterId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->singleBatasWaktuRapor) {
                $this->singleBatasWaktuRapor->clearAllReferences($deep);
            }
            if ($this->collPembelajaransRelatedBySemesterId) {
                foreach ($this->collPembelajaransRelatedBySemesterId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPembelajaransRelatedBySemesterId) {
                foreach ($this->collPembelajaransRelatedBySemesterId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPrasaranaLongitudinalsRelatedBySemesterId) {
                foreach ($this->collPrasaranaLongitudinalsRelatedBySemesterId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPrasaranaLongitudinalsRelatedBySemesterId) {
                foreach ($this->collPrasaranaLongitudinalsRelatedBySemesterId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRombonganBelajarsRelatedBySemesterId) {
                foreach ($this->collRombonganBelajarsRelatedBySemesterId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRombonganBelajarsRelatedBySemesterId) {
                foreach ($this->collRombonganBelajarsRelatedBySemesterId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collBukuAlatLongitudinalsRelatedBySemesterId) {
                foreach ($this->collBukuAlatLongitudinalsRelatedBySemesterId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collBukuAlatLongitudinalsRelatedBySemesterId) {
                foreach ($this->collBukuAlatLongitudinalsRelatedBySemesterId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aTahunAjaran instanceof Persistent) {
              $this->aTahunAjaran->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collSekolahLongitudinalsRelatedBySemesterId instanceof PropelCollection) {
            $this->collSekolahLongitudinalsRelatedBySemesterId->clearIterator();
        }
        $this->collSekolahLongitudinalsRelatedBySemesterId = null;
        if ($this->collSekolahLongitudinalsRelatedBySemesterId instanceof PropelCollection) {
            $this->collSekolahLongitudinalsRelatedBySemesterId->clearIterator();
        }
        $this->collSekolahLongitudinalsRelatedBySemesterId = null;
        if ($this->collPesertaDidikLongitudinalsRelatedBySemesterId instanceof PropelCollection) {
            $this->collPesertaDidikLongitudinalsRelatedBySemesterId->clearIterator();
        }
        $this->collPesertaDidikLongitudinalsRelatedBySemesterId = null;
        if ($this->collPesertaDidikLongitudinalsRelatedBySemesterId instanceof PropelCollection) {
            $this->collPesertaDidikLongitudinalsRelatedBySemesterId->clearIterator();
        }
        $this->collPesertaDidikLongitudinalsRelatedBySemesterId = null;
        if ($this->collSanitasisRelatedBySemesterId instanceof PropelCollection) {
            $this->collSanitasisRelatedBySemesterId->clearIterator();
        }
        $this->collSanitasisRelatedBySemesterId = null;
        if ($this->collSanitasisRelatedBySemesterId instanceof PropelCollection) {
            $this->collSanitasisRelatedBySemesterId->clearIterator();
        }
        $this->collSanitasisRelatedBySemesterId = null;
        if ($this->collSaranaLongitudinalsRelatedBySemesterId instanceof PropelCollection) {
            $this->collSaranaLongitudinalsRelatedBySemesterId->clearIterator();
        }
        $this->collSaranaLongitudinalsRelatedBySemesterId = null;
        if ($this->collSaranaLongitudinalsRelatedBySemesterId instanceof PropelCollection) {
            $this->collSaranaLongitudinalsRelatedBySemesterId->clearIterator();
        }
        $this->collSaranaLongitudinalsRelatedBySemesterId = null;
        if ($this->singleBatasWaktuRapor instanceof PropelCollection) {
            $this->singleBatasWaktuRapor->clearIterator();
        }
        $this->singleBatasWaktuRapor = null;
        if ($this->collPembelajaransRelatedBySemesterId instanceof PropelCollection) {
            $this->collPembelajaransRelatedBySemesterId->clearIterator();
        }
        $this->collPembelajaransRelatedBySemesterId = null;
        if ($this->collPembelajaransRelatedBySemesterId instanceof PropelCollection) {
            $this->collPembelajaransRelatedBySemesterId->clearIterator();
        }
        $this->collPembelajaransRelatedBySemesterId = null;
        if ($this->collPrasaranaLongitudinalsRelatedBySemesterId instanceof PropelCollection) {
            $this->collPrasaranaLongitudinalsRelatedBySemesterId->clearIterator();
        }
        $this->collPrasaranaLongitudinalsRelatedBySemesterId = null;
        if ($this->collPrasaranaLongitudinalsRelatedBySemesterId instanceof PropelCollection) {
            $this->collPrasaranaLongitudinalsRelatedBySemesterId->clearIterator();
        }
        $this->collPrasaranaLongitudinalsRelatedBySemesterId = null;
        if ($this->collRombonganBelajarsRelatedBySemesterId instanceof PropelCollection) {
            $this->collRombonganBelajarsRelatedBySemesterId->clearIterator();
        }
        $this->collRombonganBelajarsRelatedBySemesterId = null;
        if ($this->collRombonganBelajarsRelatedBySemesterId instanceof PropelCollection) {
            $this->collRombonganBelajarsRelatedBySemesterId->clearIterator();
        }
        $this->collRombonganBelajarsRelatedBySemesterId = null;
        if ($this->collBukuAlatLongitudinalsRelatedBySemesterId instanceof PropelCollection) {
            $this->collBukuAlatLongitudinalsRelatedBySemesterId->clearIterator();
        }
        $this->collBukuAlatLongitudinalsRelatedBySemesterId = null;
        if ($this->collBukuAlatLongitudinalsRelatedBySemesterId instanceof PropelCollection) {
            $this->collBukuAlatLongitudinalsRelatedBySemesterId->clearIterator();
        }
        $this->collBukuAlatLongitudinalsRelatedBySemesterId = null;
        $this->aTahunAjaran = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(SemesterPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
