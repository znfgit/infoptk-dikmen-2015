<?php

namespace angulex\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelDateTime;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\Anak;
use angulex\Model\AnakPeer;
use angulex\Model\AnakQuery;
use angulex\Model\JenjangPendidikan;
use angulex\Model\JenjangPendidikanQuery;
use angulex\Model\Ptk;
use angulex\Model\PtkQuery;
use angulex\Model\StatusAnak;
use angulex\Model\StatusAnakQuery;
use angulex\Model\VldAnak;
use angulex\Model\VldAnakQuery;

/**
 * Base class that represents a row from the 'anak' table.
 *
 * 
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseAnak extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'angulex\\Model\\AnakPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        AnakPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the anak_id field.
     * @var        string
     */
    protected $anak_id;

    /**
     * The value for the ptk_id field.
     * @var        string
     */
    protected $ptk_id;

    /**
     * The value for the status_anak_id field.
     * @var        string
     */
    protected $status_anak_id;

    /**
     * The value for the jenjang_pendidikan_id field.
     * @var        string
     */
    protected $jenjang_pendidikan_id;

    /**
     * The value for the nisn field.
     * @var        string
     */
    protected $nisn;

    /**
     * The value for the nama field.
     * @var        string
     */
    protected $nama;

    /**
     * The value for the jenis_kelamin field.
     * @var        string
     */
    protected $jenis_kelamin;

    /**
     * The value for the tempat_lahir field.
     * @var        string
     */
    protected $tempat_lahir;

    /**
     * The value for the tanggal_lahir field.
     * @var        string
     */
    protected $tanggal_lahir;

    /**
     * The value for the tahun_masuk field.
     * @var        int
     */
    protected $tahun_masuk;

    /**
     * The value for the last_update field.
     * @var        string
     */
    protected $last_update;

    /**
     * The value for the soft_delete field.
     * @var        string
     */
    protected $soft_delete;

    /**
     * The value for the last_sync field.
     * @var        string
     */
    protected $last_sync;

    /**
     * The value for the updater_id field.
     * @var        string
     */
    protected $updater_id;

    /**
     * @var        Ptk
     */
    protected $aPtkRelatedByPtkId;

    /**
     * @var        Ptk
     */
    protected $aPtkRelatedByPtkId;

    /**
     * @var        JenjangPendidikan
     */
    protected $aJenjangPendidikanRelatedByJenjangPendidikanId;

    /**
     * @var        JenjangPendidikan
     */
    protected $aJenjangPendidikanRelatedByJenjangPendidikanId;

    /**
     * @var        StatusAnak
     */
    protected $aStatusAnakRelatedByStatusAnakId;

    /**
     * @var        StatusAnak
     */
    protected $aStatusAnakRelatedByStatusAnakId;

    /**
     * @var        PropelObjectCollection|VldAnak[] Collection to store aggregation of VldAnak objects.
     */
    protected $collVldAnaksRelatedByAnakId;
    protected $collVldAnaksRelatedByAnakIdPartial;

    /**
     * @var        PropelObjectCollection|VldAnak[] Collection to store aggregation of VldAnak objects.
     */
    protected $collVldAnaksRelatedByAnakId;
    protected $collVldAnaksRelatedByAnakIdPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldAnaksRelatedByAnakIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldAnaksRelatedByAnakIdScheduledForDeletion = null;

    /**
     * Get the [anak_id] column value.
     * 
     * @return string
     */
    public function getAnakId()
    {
        return $this->anak_id;
    }

    /**
     * Get the [ptk_id] column value.
     * 
     * @return string
     */
    public function getPtkId()
    {
        return $this->ptk_id;
    }

    /**
     * Get the [status_anak_id] column value.
     * 
     * @return string
     */
    public function getStatusAnakId()
    {
        return $this->status_anak_id;
    }

    /**
     * Get the [jenjang_pendidikan_id] column value.
     * 
     * @return string
     */
    public function getJenjangPendidikanId()
    {
        return $this->jenjang_pendidikan_id;
    }

    /**
     * Get the [nisn] column value.
     * 
     * @return string
     */
    public function getNisn()
    {
        return $this->nisn;
    }

    /**
     * Get the [nama] column value.
     * 
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Get the [jenis_kelamin] column value.
     * 
     * @return string
     */
    public function getJenisKelamin()
    {
        return $this->jenis_kelamin;
    }

    /**
     * Get the [tempat_lahir] column value.
     * 
     * @return string
     */
    public function getTempatLahir()
    {
        return $this->tempat_lahir;
    }

    /**
     * Get the [tanggal_lahir] column value.
     * 
     * @return string
     */
    public function getTanggalLahir()
    {
        return $this->tanggal_lahir;
    }

    /**
     * Get the [tahun_masuk] column value.
     * 
     * @return int
     */
    public function getTahunMasuk()
    {
        return $this->tahun_masuk;
    }

    /**
     * Get the [optionally formatted] temporal [last_update] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastUpdate($format = 'Y-m-d H:i:s')
    {
        if ($this->last_update === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_update);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_update, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [soft_delete] column value.
     * 
     * @return string
     */
    public function getSoftDelete()
    {
        return $this->soft_delete;
    }

    /**
     * Get the [optionally formatted] temporal [last_sync] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastSync($format = 'Y-m-d H:i:s')
    {
        if ($this->last_sync === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_sync);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_sync, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [updater_id] column value.
     * 
     * @return string
     */
    public function getUpdaterId()
    {
        return $this->updater_id;
    }

    /**
     * Set the value of [anak_id] column.
     * 
     * @param string $v new value
     * @return Anak The current object (for fluent API support)
     */
    public function setAnakId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->anak_id !== $v) {
            $this->anak_id = $v;
            $this->modifiedColumns[] = AnakPeer::ANAK_ID;
        }


        return $this;
    } // setAnakId()

    /**
     * Set the value of [ptk_id] column.
     * 
     * @param string $v new value
     * @return Anak The current object (for fluent API support)
     */
    public function setPtkId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->ptk_id !== $v) {
            $this->ptk_id = $v;
            $this->modifiedColumns[] = AnakPeer::PTK_ID;
        }

        if ($this->aPtkRelatedByPtkId !== null && $this->aPtkRelatedByPtkId->getPtkId() !== $v) {
            $this->aPtkRelatedByPtkId = null;
        }

        if ($this->aPtkRelatedByPtkId !== null && $this->aPtkRelatedByPtkId->getPtkId() !== $v) {
            $this->aPtkRelatedByPtkId = null;
        }


        return $this;
    } // setPtkId()

    /**
     * Set the value of [status_anak_id] column.
     * 
     * @param string $v new value
     * @return Anak The current object (for fluent API support)
     */
    public function setStatusAnakId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->status_anak_id !== $v) {
            $this->status_anak_id = $v;
            $this->modifiedColumns[] = AnakPeer::STATUS_ANAK_ID;
        }

        if ($this->aStatusAnakRelatedByStatusAnakId !== null && $this->aStatusAnakRelatedByStatusAnakId->getStatusAnakId() !== $v) {
            $this->aStatusAnakRelatedByStatusAnakId = null;
        }

        if ($this->aStatusAnakRelatedByStatusAnakId !== null && $this->aStatusAnakRelatedByStatusAnakId->getStatusAnakId() !== $v) {
            $this->aStatusAnakRelatedByStatusAnakId = null;
        }


        return $this;
    } // setStatusAnakId()

    /**
     * Set the value of [jenjang_pendidikan_id] column.
     * 
     * @param string $v new value
     * @return Anak The current object (for fluent API support)
     */
    public function setJenjangPendidikanId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->jenjang_pendidikan_id !== $v) {
            $this->jenjang_pendidikan_id = $v;
            $this->modifiedColumns[] = AnakPeer::JENJANG_PENDIDIKAN_ID;
        }

        if ($this->aJenjangPendidikanRelatedByJenjangPendidikanId !== null && $this->aJenjangPendidikanRelatedByJenjangPendidikanId->getJenjangPendidikanId() !== $v) {
            $this->aJenjangPendidikanRelatedByJenjangPendidikanId = null;
        }

        if ($this->aJenjangPendidikanRelatedByJenjangPendidikanId !== null && $this->aJenjangPendidikanRelatedByJenjangPendidikanId->getJenjangPendidikanId() !== $v) {
            $this->aJenjangPendidikanRelatedByJenjangPendidikanId = null;
        }


        return $this;
    } // setJenjangPendidikanId()

    /**
     * Set the value of [nisn] column.
     * 
     * @param string $v new value
     * @return Anak The current object (for fluent API support)
     */
    public function setNisn($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nisn !== $v) {
            $this->nisn = $v;
            $this->modifiedColumns[] = AnakPeer::NISN;
        }


        return $this;
    } // setNisn()

    /**
     * Set the value of [nama] column.
     * 
     * @param string $v new value
     * @return Anak The current object (for fluent API support)
     */
    public function setNama($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nama !== $v) {
            $this->nama = $v;
            $this->modifiedColumns[] = AnakPeer::NAMA;
        }


        return $this;
    } // setNama()

    /**
     * Set the value of [jenis_kelamin] column.
     * 
     * @param string $v new value
     * @return Anak The current object (for fluent API support)
     */
    public function setJenisKelamin($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->jenis_kelamin !== $v) {
            $this->jenis_kelamin = $v;
            $this->modifiedColumns[] = AnakPeer::JENIS_KELAMIN;
        }


        return $this;
    } // setJenisKelamin()

    /**
     * Set the value of [tempat_lahir] column.
     * 
     * @param string $v new value
     * @return Anak The current object (for fluent API support)
     */
    public function setTempatLahir($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tempat_lahir !== $v) {
            $this->tempat_lahir = $v;
            $this->modifiedColumns[] = AnakPeer::TEMPAT_LAHIR;
        }


        return $this;
    } // setTempatLahir()

    /**
     * Set the value of [tanggal_lahir] column.
     * 
     * @param string $v new value
     * @return Anak The current object (for fluent API support)
     */
    public function setTanggalLahir($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tanggal_lahir !== $v) {
            $this->tanggal_lahir = $v;
            $this->modifiedColumns[] = AnakPeer::TANGGAL_LAHIR;
        }


        return $this;
    } // setTanggalLahir()

    /**
     * Set the value of [tahun_masuk] column.
     * 
     * @param int $v new value
     * @return Anak The current object (for fluent API support)
     */
    public function setTahunMasuk($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->tahun_masuk !== $v) {
            $this->tahun_masuk = $v;
            $this->modifiedColumns[] = AnakPeer::TAHUN_MASUK;
        }


        return $this;
    } // setTahunMasuk()

    /**
     * Sets the value of [last_update] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Anak The current object (for fluent API support)
     */
    public function setLastUpdate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_update !== null || $dt !== null) {
            $currentDateAsString = ($this->last_update !== null && $tmpDt = new DateTime($this->last_update)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_update = $newDateAsString;
                $this->modifiedColumns[] = AnakPeer::LAST_UPDATE;
            }
        } // if either are not null


        return $this;
    } // setLastUpdate()

    /**
     * Set the value of [soft_delete] column.
     * 
     * @param string $v new value
     * @return Anak The current object (for fluent API support)
     */
    public function setSoftDelete($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->soft_delete !== $v) {
            $this->soft_delete = $v;
            $this->modifiedColumns[] = AnakPeer::SOFT_DELETE;
        }


        return $this;
    } // setSoftDelete()

    /**
     * Sets the value of [last_sync] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Anak The current object (for fluent API support)
     */
    public function setLastSync($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_sync !== null || $dt !== null) {
            $currentDateAsString = ($this->last_sync !== null && $tmpDt = new DateTime($this->last_sync)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_sync = $newDateAsString;
                $this->modifiedColumns[] = AnakPeer::LAST_SYNC;
            }
        } // if either are not null


        return $this;
    } // setLastSync()

    /**
     * Set the value of [updater_id] column.
     * 
     * @param string $v new value
     * @return Anak The current object (for fluent API support)
     */
    public function setUpdaterId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->updater_id !== $v) {
            $this->updater_id = $v;
            $this->modifiedColumns[] = AnakPeer::UPDATER_ID;
        }


        return $this;
    } // setUpdaterId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->anak_id = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
            $this->ptk_id = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->status_anak_id = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->jenjang_pendidikan_id = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->nisn = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->nama = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->jenis_kelamin = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->tempat_lahir = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->tanggal_lahir = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->tahun_masuk = ($row[$startcol + 9] !== null) ? (int) $row[$startcol + 9] : null;
            $this->last_update = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->soft_delete = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->last_sync = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->updater_id = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 14; // 14 = AnakPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Anak object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aPtkRelatedByPtkId !== null && $this->ptk_id !== $this->aPtkRelatedByPtkId->getPtkId()) {
            $this->aPtkRelatedByPtkId = null;
        }
        if ($this->aPtkRelatedByPtkId !== null && $this->ptk_id !== $this->aPtkRelatedByPtkId->getPtkId()) {
            $this->aPtkRelatedByPtkId = null;
        }
        if ($this->aStatusAnakRelatedByStatusAnakId !== null && $this->status_anak_id !== $this->aStatusAnakRelatedByStatusAnakId->getStatusAnakId()) {
            $this->aStatusAnakRelatedByStatusAnakId = null;
        }
        if ($this->aStatusAnakRelatedByStatusAnakId !== null && $this->status_anak_id !== $this->aStatusAnakRelatedByStatusAnakId->getStatusAnakId()) {
            $this->aStatusAnakRelatedByStatusAnakId = null;
        }
        if ($this->aJenjangPendidikanRelatedByJenjangPendidikanId !== null && $this->jenjang_pendidikan_id !== $this->aJenjangPendidikanRelatedByJenjangPendidikanId->getJenjangPendidikanId()) {
            $this->aJenjangPendidikanRelatedByJenjangPendidikanId = null;
        }
        if ($this->aJenjangPendidikanRelatedByJenjangPendidikanId !== null && $this->jenjang_pendidikan_id !== $this->aJenjangPendidikanRelatedByJenjangPendidikanId->getJenjangPendidikanId()) {
            $this->aJenjangPendidikanRelatedByJenjangPendidikanId = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(AnakPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = AnakPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aPtkRelatedByPtkId = null;
            $this->aPtkRelatedByPtkId = null;
            $this->aJenjangPendidikanRelatedByJenjangPendidikanId = null;
            $this->aJenjangPendidikanRelatedByJenjangPendidikanId = null;
            $this->aStatusAnakRelatedByStatusAnakId = null;
            $this->aStatusAnakRelatedByStatusAnakId = null;
            $this->collVldAnaksRelatedByAnakId = null;

            $this->collVldAnaksRelatedByAnakId = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(AnakPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = AnakQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(AnakPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                AnakPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPtkRelatedByPtkId !== null) {
                if ($this->aPtkRelatedByPtkId->isModified() || $this->aPtkRelatedByPtkId->isNew()) {
                    $affectedRows += $this->aPtkRelatedByPtkId->save($con);
                }
                $this->setPtkRelatedByPtkId($this->aPtkRelatedByPtkId);
            }

            if ($this->aPtkRelatedByPtkId !== null) {
                if ($this->aPtkRelatedByPtkId->isModified() || $this->aPtkRelatedByPtkId->isNew()) {
                    $affectedRows += $this->aPtkRelatedByPtkId->save($con);
                }
                $this->setPtkRelatedByPtkId($this->aPtkRelatedByPtkId);
            }

            if ($this->aJenjangPendidikanRelatedByJenjangPendidikanId !== null) {
                if ($this->aJenjangPendidikanRelatedByJenjangPendidikanId->isModified() || $this->aJenjangPendidikanRelatedByJenjangPendidikanId->isNew()) {
                    $affectedRows += $this->aJenjangPendidikanRelatedByJenjangPendidikanId->save($con);
                }
                $this->setJenjangPendidikanRelatedByJenjangPendidikanId($this->aJenjangPendidikanRelatedByJenjangPendidikanId);
            }

            if ($this->aJenjangPendidikanRelatedByJenjangPendidikanId !== null) {
                if ($this->aJenjangPendidikanRelatedByJenjangPendidikanId->isModified() || $this->aJenjangPendidikanRelatedByJenjangPendidikanId->isNew()) {
                    $affectedRows += $this->aJenjangPendidikanRelatedByJenjangPendidikanId->save($con);
                }
                $this->setJenjangPendidikanRelatedByJenjangPendidikanId($this->aJenjangPendidikanRelatedByJenjangPendidikanId);
            }

            if ($this->aStatusAnakRelatedByStatusAnakId !== null) {
                if ($this->aStatusAnakRelatedByStatusAnakId->isModified() || $this->aStatusAnakRelatedByStatusAnakId->isNew()) {
                    $affectedRows += $this->aStatusAnakRelatedByStatusAnakId->save($con);
                }
                $this->setStatusAnakRelatedByStatusAnakId($this->aStatusAnakRelatedByStatusAnakId);
            }

            if ($this->aStatusAnakRelatedByStatusAnakId !== null) {
                if ($this->aStatusAnakRelatedByStatusAnakId->isModified() || $this->aStatusAnakRelatedByStatusAnakId->isNew()) {
                    $affectedRows += $this->aStatusAnakRelatedByStatusAnakId->save($con);
                }
                $this->setStatusAnakRelatedByStatusAnakId($this->aStatusAnakRelatedByStatusAnakId);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->vldAnaksRelatedByAnakIdScheduledForDeletion !== null) {
                if (!$this->vldAnaksRelatedByAnakIdScheduledForDeletion->isEmpty()) {
                    VldAnakQuery::create()
                        ->filterByPrimaryKeys($this->vldAnaksRelatedByAnakIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldAnaksRelatedByAnakIdScheduledForDeletion = null;
                }
            }

            if ($this->collVldAnaksRelatedByAnakId !== null) {
                foreach ($this->collVldAnaksRelatedByAnakId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldAnaksRelatedByAnakIdScheduledForDeletion !== null) {
                if (!$this->vldAnaksRelatedByAnakIdScheduledForDeletion->isEmpty()) {
                    VldAnakQuery::create()
                        ->filterByPrimaryKeys($this->vldAnaksRelatedByAnakIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldAnaksRelatedByAnakIdScheduledForDeletion = null;
                }
            }

            if ($this->collVldAnaksRelatedByAnakId !== null) {
                foreach ($this->collVldAnaksRelatedByAnakId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $criteria = $this->buildCriteria();
        $pk = BasePeer::doInsert($criteria, $con);
        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPtkRelatedByPtkId !== null) {
                if (!$this->aPtkRelatedByPtkId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPtkRelatedByPtkId->getValidationFailures());
                }
            }

            if ($this->aPtkRelatedByPtkId !== null) {
                if (!$this->aPtkRelatedByPtkId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPtkRelatedByPtkId->getValidationFailures());
                }
            }

            if ($this->aJenjangPendidikanRelatedByJenjangPendidikanId !== null) {
                if (!$this->aJenjangPendidikanRelatedByJenjangPendidikanId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aJenjangPendidikanRelatedByJenjangPendidikanId->getValidationFailures());
                }
            }

            if ($this->aJenjangPendidikanRelatedByJenjangPendidikanId !== null) {
                if (!$this->aJenjangPendidikanRelatedByJenjangPendidikanId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aJenjangPendidikanRelatedByJenjangPendidikanId->getValidationFailures());
                }
            }

            if ($this->aStatusAnakRelatedByStatusAnakId !== null) {
                if (!$this->aStatusAnakRelatedByStatusAnakId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aStatusAnakRelatedByStatusAnakId->getValidationFailures());
                }
            }

            if ($this->aStatusAnakRelatedByStatusAnakId !== null) {
                if (!$this->aStatusAnakRelatedByStatusAnakId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aStatusAnakRelatedByStatusAnakId->getValidationFailures());
                }
            }


            if (($retval = AnakPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collVldAnaksRelatedByAnakId !== null) {
                    foreach ($this->collVldAnaksRelatedByAnakId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldAnaksRelatedByAnakId !== null) {
                    foreach ($this->collVldAnaksRelatedByAnakId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = AnakPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getAnakId();
                break;
            case 1:
                return $this->getPtkId();
                break;
            case 2:
                return $this->getStatusAnakId();
                break;
            case 3:
                return $this->getJenjangPendidikanId();
                break;
            case 4:
                return $this->getNisn();
                break;
            case 5:
                return $this->getNama();
                break;
            case 6:
                return $this->getJenisKelamin();
                break;
            case 7:
                return $this->getTempatLahir();
                break;
            case 8:
                return $this->getTanggalLahir();
                break;
            case 9:
                return $this->getTahunMasuk();
                break;
            case 10:
                return $this->getLastUpdate();
                break;
            case 11:
                return $this->getSoftDelete();
                break;
            case 12:
                return $this->getLastSync();
                break;
            case 13:
                return $this->getUpdaterId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Anak'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Anak'][$this->getPrimaryKey()] = true;
        $keys = AnakPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getAnakId(),
            $keys[1] => $this->getPtkId(),
            $keys[2] => $this->getStatusAnakId(),
            $keys[3] => $this->getJenjangPendidikanId(),
            $keys[4] => $this->getNisn(),
            $keys[5] => $this->getNama(),
            $keys[6] => $this->getJenisKelamin(),
            $keys[7] => $this->getTempatLahir(),
            $keys[8] => $this->getTanggalLahir(),
            $keys[9] => $this->getTahunMasuk(),
            $keys[10] => $this->getLastUpdate(),
            $keys[11] => $this->getSoftDelete(),
            $keys[12] => $this->getLastSync(),
            $keys[13] => $this->getUpdaterId(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aPtkRelatedByPtkId) {
                $result['PtkRelatedByPtkId'] = $this->aPtkRelatedByPtkId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPtkRelatedByPtkId) {
                $result['PtkRelatedByPtkId'] = $this->aPtkRelatedByPtkId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aJenjangPendidikanRelatedByJenjangPendidikanId) {
                $result['JenjangPendidikanRelatedByJenjangPendidikanId'] = $this->aJenjangPendidikanRelatedByJenjangPendidikanId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aJenjangPendidikanRelatedByJenjangPendidikanId) {
                $result['JenjangPendidikanRelatedByJenjangPendidikanId'] = $this->aJenjangPendidikanRelatedByJenjangPendidikanId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aStatusAnakRelatedByStatusAnakId) {
                $result['StatusAnakRelatedByStatusAnakId'] = $this->aStatusAnakRelatedByStatusAnakId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aStatusAnakRelatedByStatusAnakId) {
                $result['StatusAnakRelatedByStatusAnakId'] = $this->aStatusAnakRelatedByStatusAnakId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collVldAnaksRelatedByAnakId) {
                $result['VldAnaksRelatedByAnakId'] = $this->collVldAnaksRelatedByAnakId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldAnaksRelatedByAnakId) {
                $result['VldAnaksRelatedByAnakId'] = $this->collVldAnaksRelatedByAnakId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = AnakPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setAnakId($value);
                break;
            case 1:
                $this->setPtkId($value);
                break;
            case 2:
                $this->setStatusAnakId($value);
                break;
            case 3:
                $this->setJenjangPendidikanId($value);
                break;
            case 4:
                $this->setNisn($value);
                break;
            case 5:
                $this->setNama($value);
                break;
            case 6:
                $this->setJenisKelamin($value);
                break;
            case 7:
                $this->setTempatLahir($value);
                break;
            case 8:
                $this->setTanggalLahir($value);
                break;
            case 9:
                $this->setTahunMasuk($value);
                break;
            case 10:
                $this->setLastUpdate($value);
                break;
            case 11:
                $this->setSoftDelete($value);
                break;
            case 12:
                $this->setLastSync($value);
                break;
            case 13:
                $this->setUpdaterId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = AnakPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setAnakId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setPtkId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setStatusAnakId($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setJenjangPendidikanId($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setNisn($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setNama($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setJenisKelamin($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setTempatLahir($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setTanggalLahir($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setTahunMasuk($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setLastUpdate($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setSoftDelete($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setLastSync($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setUpdaterId($arr[$keys[13]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(AnakPeer::DATABASE_NAME);

        if ($this->isColumnModified(AnakPeer::ANAK_ID)) $criteria->add(AnakPeer::ANAK_ID, $this->anak_id);
        if ($this->isColumnModified(AnakPeer::PTK_ID)) $criteria->add(AnakPeer::PTK_ID, $this->ptk_id);
        if ($this->isColumnModified(AnakPeer::STATUS_ANAK_ID)) $criteria->add(AnakPeer::STATUS_ANAK_ID, $this->status_anak_id);
        if ($this->isColumnModified(AnakPeer::JENJANG_PENDIDIKAN_ID)) $criteria->add(AnakPeer::JENJANG_PENDIDIKAN_ID, $this->jenjang_pendidikan_id);
        if ($this->isColumnModified(AnakPeer::NISN)) $criteria->add(AnakPeer::NISN, $this->nisn);
        if ($this->isColumnModified(AnakPeer::NAMA)) $criteria->add(AnakPeer::NAMA, $this->nama);
        if ($this->isColumnModified(AnakPeer::JENIS_KELAMIN)) $criteria->add(AnakPeer::JENIS_KELAMIN, $this->jenis_kelamin);
        if ($this->isColumnModified(AnakPeer::TEMPAT_LAHIR)) $criteria->add(AnakPeer::TEMPAT_LAHIR, $this->tempat_lahir);
        if ($this->isColumnModified(AnakPeer::TANGGAL_LAHIR)) $criteria->add(AnakPeer::TANGGAL_LAHIR, $this->tanggal_lahir);
        if ($this->isColumnModified(AnakPeer::TAHUN_MASUK)) $criteria->add(AnakPeer::TAHUN_MASUK, $this->tahun_masuk);
        if ($this->isColumnModified(AnakPeer::LAST_UPDATE)) $criteria->add(AnakPeer::LAST_UPDATE, $this->last_update);
        if ($this->isColumnModified(AnakPeer::SOFT_DELETE)) $criteria->add(AnakPeer::SOFT_DELETE, $this->soft_delete);
        if ($this->isColumnModified(AnakPeer::LAST_SYNC)) $criteria->add(AnakPeer::LAST_SYNC, $this->last_sync);
        if ($this->isColumnModified(AnakPeer::UPDATER_ID)) $criteria->add(AnakPeer::UPDATER_ID, $this->updater_id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(AnakPeer::DATABASE_NAME);
        $criteria->add(AnakPeer::ANAK_ID, $this->anak_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getAnakId();
    }

    /**
     * Generic method to set the primary key (anak_id column).
     *
     * @param  string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setAnakId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getAnakId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Anak (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setPtkId($this->getPtkId());
        $copyObj->setStatusAnakId($this->getStatusAnakId());
        $copyObj->setJenjangPendidikanId($this->getJenjangPendidikanId());
        $copyObj->setNisn($this->getNisn());
        $copyObj->setNama($this->getNama());
        $copyObj->setJenisKelamin($this->getJenisKelamin());
        $copyObj->setTempatLahir($this->getTempatLahir());
        $copyObj->setTanggalLahir($this->getTanggalLahir());
        $copyObj->setTahunMasuk($this->getTahunMasuk());
        $copyObj->setLastUpdate($this->getLastUpdate());
        $copyObj->setSoftDelete($this->getSoftDelete());
        $copyObj->setLastSync($this->getLastSync());
        $copyObj->setUpdaterId($this->getUpdaterId());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getVldAnaksRelatedByAnakId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldAnakRelatedByAnakId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldAnaksRelatedByAnakId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldAnakRelatedByAnakId($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setAnakId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Anak Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return AnakPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new AnakPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a Ptk object.
     *
     * @param             Ptk $v
     * @return Anak The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPtkRelatedByPtkId(Ptk $v = null)
    {
        if ($v === null) {
            $this->setPtkId(NULL);
        } else {
            $this->setPtkId($v->getPtkId());
        }

        $this->aPtkRelatedByPtkId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Ptk object, it will not be re-added.
        if ($v !== null) {
            $v->addAnakRelatedByPtkId($this);
        }


        return $this;
    }


    /**
     * Get the associated Ptk object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Ptk The associated Ptk object.
     * @throws PropelException
     */
    public function getPtkRelatedByPtkId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPtkRelatedByPtkId === null && (($this->ptk_id !== "" && $this->ptk_id !== null)) && $doQuery) {
            $this->aPtkRelatedByPtkId = PtkQuery::create()->findPk($this->ptk_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPtkRelatedByPtkId->addAnaksRelatedByPtkId($this);
             */
        }

        return $this->aPtkRelatedByPtkId;
    }

    /**
     * Declares an association between this object and a Ptk object.
     *
     * @param             Ptk $v
     * @return Anak The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPtkRelatedByPtkId(Ptk $v = null)
    {
        if ($v === null) {
            $this->setPtkId(NULL);
        } else {
            $this->setPtkId($v->getPtkId());
        }

        $this->aPtkRelatedByPtkId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Ptk object, it will not be re-added.
        if ($v !== null) {
            $v->addAnakRelatedByPtkId($this);
        }


        return $this;
    }


    /**
     * Get the associated Ptk object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Ptk The associated Ptk object.
     * @throws PropelException
     */
    public function getPtkRelatedByPtkId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPtkRelatedByPtkId === null && (($this->ptk_id !== "" && $this->ptk_id !== null)) && $doQuery) {
            $this->aPtkRelatedByPtkId = PtkQuery::create()->findPk($this->ptk_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPtkRelatedByPtkId->addAnaksRelatedByPtkId($this);
             */
        }

        return $this->aPtkRelatedByPtkId;
    }

    /**
     * Declares an association between this object and a JenjangPendidikan object.
     *
     * @param             JenjangPendidikan $v
     * @return Anak The current object (for fluent API support)
     * @throws PropelException
     */
    public function setJenjangPendidikanRelatedByJenjangPendidikanId(JenjangPendidikan $v = null)
    {
        if ($v === null) {
            $this->setJenjangPendidikanId(NULL);
        } else {
            $this->setJenjangPendidikanId($v->getJenjangPendidikanId());
        }

        $this->aJenjangPendidikanRelatedByJenjangPendidikanId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the JenjangPendidikan object, it will not be re-added.
        if ($v !== null) {
            $v->addAnakRelatedByJenjangPendidikanId($this);
        }


        return $this;
    }


    /**
     * Get the associated JenjangPendidikan object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return JenjangPendidikan The associated JenjangPendidikan object.
     * @throws PropelException
     */
    public function getJenjangPendidikanRelatedByJenjangPendidikanId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aJenjangPendidikanRelatedByJenjangPendidikanId === null && (($this->jenjang_pendidikan_id !== "" && $this->jenjang_pendidikan_id !== null)) && $doQuery) {
            $this->aJenjangPendidikanRelatedByJenjangPendidikanId = JenjangPendidikanQuery::create()->findPk($this->jenjang_pendidikan_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aJenjangPendidikanRelatedByJenjangPendidikanId->addAnaksRelatedByJenjangPendidikanId($this);
             */
        }

        return $this->aJenjangPendidikanRelatedByJenjangPendidikanId;
    }

    /**
     * Declares an association between this object and a JenjangPendidikan object.
     *
     * @param             JenjangPendidikan $v
     * @return Anak The current object (for fluent API support)
     * @throws PropelException
     */
    public function setJenjangPendidikanRelatedByJenjangPendidikanId(JenjangPendidikan $v = null)
    {
        if ($v === null) {
            $this->setJenjangPendidikanId(NULL);
        } else {
            $this->setJenjangPendidikanId($v->getJenjangPendidikanId());
        }

        $this->aJenjangPendidikanRelatedByJenjangPendidikanId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the JenjangPendidikan object, it will not be re-added.
        if ($v !== null) {
            $v->addAnakRelatedByJenjangPendidikanId($this);
        }


        return $this;
    }


    /**
     * Get the associated JenjangPendidikan object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return JenjangPendidikan The associated JenjangPendidikan object.
     * @throws PropelException
     */
    public function getJenjangPendidikanRelatedByJenjangPendidikanId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aJenjangPendidikanRelatedByJenjangPendidikanId === null && (($this->jenjang_pendidikan_id !== "" && $this->jenjang_pendidikan_id !== null)) && $doQuery) {
            $this->aJenjangPendidikanRelatedByJenjangPendidikanId = JenjangPendidikanQuery::create()->findPk($this->jenjang_pendidikan_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aJenjangPendidikanRelatedByJenjangPendidikanId->addAnaksRelatedByJenjangPendidikanId($this);
             */
        }

        return $this->aJenjangPendidikanRelatedByJenjangPendidikanId;
    }

    /**
     * Declares an association between this object and a StatusAnak object.
     *
     * @param             StatusAnak $v
     * @return Anak The current object (for fluent API support)
     * @throws PropelException
     */
    public function setStatusAnakRelatedByStatusAnakId(StatusAnak $v = null)
    {
        if ($v === null) {
            $this->setStatusAnakId(NULL);
        } else {
            $this->setStatusAnakId($v->getStatusAnakId());
        }

        $this->aStatusAnakRelatedByStatusAnakId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the StatusAnak object, it will not be re-added.
        if ($v !== null) {
            $v->addAnakRelatedByStatusAnakId($this);
        }


        return $this;
    }


    /**
     * Get the associated StatusAnak object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return StatusAnak The associated StatusAnak object.
     * @throws PropelException
     */
    public function getStatusAnakRelatedByStatusAnakId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aStatusAnakRelatedByStatusAnakId === null && (($this->status_anak_id !== "" && $this->status_anak_id !== null)) && $doQuery) {
            $this->aStatusAnakRelatedByStatusAnakId = StatusAnakQuery::create()->findPk($this->status_anak_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aStatusAnakRelatedByStatusAnakId->addAnaksRelatedByStatusAnakId($this);
             */
        }

        return $this->aStatusAnakRelatedByStatusAnakId;
    }

    /**
     * Declares an association between this object and a StatusAnak object.
     *
     * @param             StatusAnak $v
     * @return Anak The current object (for fluent API support)
     * @throws PropelException
     */
    public function setStatusAnakRelatedByStatusAnakId(StatusAnak $v = null)
    {
        if ($v === null) {
            $this->setStatusAnakId(NULL);
        } else {
            $this->setStatusAnakId($v->getStatusAnakId());
        }

        $this->aStatusAnakRelatedByStatusAnakId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the StatusAnak object, it will not be re-added.
        if ($v !== null) {
            $v->addAnakRelatedByStatusAnakId($this);
        }


        return $this;
    }


    /**
     * Get the associated StatusAnak object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return StatusAnak The associated StatusAnak object.
     * @throws PropelException
     */
    public function getStatusAnakRelatedByStatusAnakId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aStatusAnakRelatedByStatusAnakId === null && (($this->status_anak_id !== "" && $this->status_anak_id !== null)) && $doQuery) {
            $this->aStatusAnakRelatedByStatusAnakId = StatusAnakQuery::create()->findPk($this->status_anak_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aStatusAnakRelatedByStatusAnakId->addAnaksRelatedByStatusAnakId($this);
             */
        }

        return $this->aStatusAnakRelatedByStatusAnakId;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('VldAnakRelatedByAnakId' == $relationName) {
            $this->initVldAnaksRelatedByAnakId();
        }
        if ('VldAnakRelatedByAnakId' == $relationName) {
            $this->initVldAnaksRelatedByAnakId();
        }
    }

    /**
     * Clears out the collVldAnaksRelatedByAnakId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Anak The current object (for fluent API support)
     * @see        addVldAnaksRelatedByAnakId()
     */
    public function clearVldAnaksRelatedByAnakId()
    {
        $this->collVldAnaksRelatedByAnakId = null; // important to set this to null since that means it is uninitialized
        $this->collVldAnaksRelatedByAnakIdPartial = null;

        return $this;
    }

    /**
     * reset is the collVldAnaksRelatedByAnakId collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldAnaksRelatedByAnakId($v = true)
    {
        $this->collVldAnaksRelatedByAnakIdPartial = $v;
    }

    /**
     * Initializes the collVldAnaksRelatedByAnakId collection.
     *
     * By default this just sets the collVldAnaksRelatedByAnakId collection to an empty array (like clearcollVldAnaksRelatedByAnakId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldAnaksRelatedByAnakId($overrideExisting = true)
    {
        if (null !== $this->collVldAnaksRelatedByAnakId && !$overrideExisting) {
            return;
        }
        $this->collVldAnaksRelatedByAnakId = new PropelObjectCollection();
        $this->collVldAnaksRelatedByAnakId->setModel('VldAnak');
    }

    /**
     * Gets an array of VldAnak objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Anak is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldAnak[] List of VldAnak objects
     * @throws PropelException
     */
    public function getVldAnaksRelatedByAnakId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldAnaksRelatedByAnakIdPartial && !$this->isNew();
        if (null === $this->collVldAnaksRelatedByAnakId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldAnaksRelatedByAnakId) {
                // return empty collection
                $this->initVldAnaksRelatedByAnakId();
            } else {
                $collVldAnaksRelatedByAnakId = VldAnakQuery::create(null, $criteria)
                    ->filterByAnakRelatedByAnakId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldAnaksRelatedByAnakIdPartial && count($collVldAnaksRelatedByAnakId)) {
                      $this->initVldAnaksRelatedByAnakId(false);

                      foreach($collVldAnaksRelatedByAnakId as $obj) {
                        if (false == $this->collVldAnaksRelatedByAnakId->contains($obj)) {
                          $this->collVldAnaksRelatedByAnakId->append($obj);
                        }
                      }

                      $this->collVldAnaksRelatedByAnakIdPartial = true;
                    }

                    $collVldAnaksRelatedByAnakId->getInternalIterator()->rewind();
                    return $collVldAnaksRelatedByAnakId;
                }

                if($partial && $this->collVldAnaksRelatedByAnakId) {
                    foreach($this->collVldAnaksRelatedByAnakId as $obj) {
                        if($obj->isNew()) {
                            $collVldAnaksRelatedByAnakId[] = $obj;
                        }
                    }
                }

                $this->collVldAnaksRelatedByAnakId = $collVldAnaksRelatedByAnakId;
                $this->collVldAnaksRelatedByAnakIdPartial = false;
            }
        }

        return $this->collVldAnaksRelatedByAnakId;
    }

    /**
     * Sets a collection of VldAnakRelatedByAnakId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldAnaksRelatedByAnakId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Anak The current object (for fluent API support)
     */
    public function setVldAnaksRelatedByAnakId(PropelCollection $vldAnaksRelatedByAnakId, PropelPDO $con = null)
    {
        $vldAnaksRelatedByAnakIdToDelete = $this->getVldAnaksRelatedByAnakId(new Criteria(), $con)->diff($vldAnaksRelatedByAnakId);

        $this->vldAnaksRelatedByAnakIdScheduledForDeletion = unserialize(serialize($vldAnaksRelatedByAnakIdToDelete));

        foreach ($vldAnaksRelatedByAnakIdToDelete as $vldAnakRelatedByAnakIdRemoved) {
            $vldAnakRelatedByAnakIdRemoved->setAnakRelatedByAnakId(null);
        }

        $this->collVldAnaksRelatedByAnakId = null;
        foreach ($vldAnaksRelatedByAnakId as $vldAnakRelatedByAnakId) {
            $this->addVldAnakRelatedByAnakId($vldAnakRelatedByAnakId);
        }

        $this->collVldAnaksRelatedByAnakId = $vldAnaksRelatedByAnakId;
        $this->collVldAnaksRelatedByAnakIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldAnak objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldAnak objects.
     * @throws PropelException
     */
    public function countVldAnaksRelatedByAnakId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldAnaksRelatedByAnakIdPartial && !$this->isNew();
        if (null === $this->collVldAnaksRelatedByAnakId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldAnaksRelatedByAnakId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldAnaksRelatedByAnakId());
            }
            $query = VldAnakQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByAnakRelatedByAnakId($this)
                ->count($con);
        }

        return count($this->collVldAnaksRelatedByAnakId);
    }

    /**
     * Method called to associate a VldAnak object to this object
     * through the VldAnak foreign key attribute.
     *
     * @param    VldAnak $l VldAnak
     * @return Anak The current object (for fluent API support)
     */
    public function addVldAnakRelatedByAnakId(VldAnak $l)
    {
        if ($this->collVldAnaksRelatedByAnakId === null) {
            $this->initVldAnaksRelatedByAnakId();
            $this->collVldAnaksRelatedByAnakIdPartial = true;
        }
        if (!in_array($l, $this->collVldAnaksRelatedByAnakId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldAnakRelatedByAnakId($l);
        }

        return $this;
    }

    /**
     * @param	VldAnakRelatedByAnakId $vldAnakRelatedByAnakId The vldAnakRelatedByAnakId object to add.
     */
    protected function doAddVldAnakRelatedByAnakId($vldAnakRelatedByAnakId)
    {
        $this->collVldAnaksRelatedByAnakId[]= $vldAnakRelatedByAnakId;
        $vldAnakRelatedByAnakId->setAnakRelatedByAnakId($this);
    }

    /**
     * @param	VldAnakRelatedByAnakId $vldAnakRelatedByAnakId The vldAnakRelatedByAnakId object to remove.
     * @return Anak The current object (for fluent API support)
     */
    public function removeVldAnakRelatedByAnakId($vldAnakRelatedByAnakId)
    {
        if ($this->getVldAnaksRelatedByAnakId()->contains($vldAnakRelatedByAnakId)) {
            $this->collVldAnaksRelatedByAnakId->remove($this->collVldAnaksRelatedByAnakId->search($vldAnakRelatedByAnakId));
            if (null === $this->vldAnaksRelatedByAnakIdScheduledForDeletion) {
                $this->vldAnaksRelatedByAnakIdScheduledForDeletion = clone $this->collVldAnaksRelatedByAnakId;
                $this->vldAnaksRelatedByAnakIdScheduledForDeletion->clear();
            }
            $this->vldAnaksRelatedByAnakIdScheduledForDeletion[]= clone $vldAnakRelatedByAnakId;
            $vldAnakRelatedByAnakId->setAnakRelatedByAnakId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Anak is new, it will return
     * an empty collection; or if this Anak has previously
     * been saved, it will retrieve related VldAnaksRelatedByAnakId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Anak.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldAnak[] List of VldAnak objects
     */
    public function getVldAnaksRelatedByAnakIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldAnakQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldAnaksRelatedByAnakId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Anak is new, it will return
     * an empty collection; or if this Anak has previously
     * been saved, it will retrieve related VldAnaksRelatedByAnakId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Anak.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldAnak[] List of VldAnak objects
     */
    public function getVldAnaksRelatedByAnakIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldAnakQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldAnaksRelatedByAnakId($query, $con);
    }

    /**
     * Clears out the collVldAnaksRelatedByAnakId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Anak The current object (for fluent API support)
     * @see        addVldAnaksRelatedByAnakId()
     */
    public function clearVldAnaksRelatedByAnakId()
    {
        $this->collVldAnaksRelatedByAnakId = null; // important to set this to null since that means it is uninitialized
        $this->collVldAnaksRelatedByAnakIdPartial = null;

        return $this;
    }

    /**
     * reset is the collVldAnaksRelatedByAnakId collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldAnaksRelatedByAnakId($v = true)
    {
        $this->collVldAnaksRelatedByAnakIdPartial = $v;
    }

    /**
     * Initializes the collVldAnaksRelatedByAnakId collection.
     *
     * By default this just sets the collVldAnaksRelatedByAnakId collection to an empty array (like clearcollVldAnaksRelatedByAnakId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldAnaksRelatedByAnakId($overrideExisting = true)
    {
        if (null !== $this->collVldAnaksRelatedByAnakId && !$overrideExisting) {
            return;
        }
        $this->collVldAnaksRelatedByAnakId = new PropelObjectCollection();
        $this->collVldAnaksRelatedByAnakId->setModel('VldAnak');
    }

    /**
     * Gets an array of VldAnak objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Anak is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldAnak[] List of VldAnak objects
     * @throws PropelException
     */
    public function getVldAnaksRelatedByAnakId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldAnaksRelatedByAnakIdPartial && !$this->isNew();
        if (null === $this->collVldAnaksRelatedByAnakId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldAnaksRelatedByAnakId) {
                // return empty collection
                $this->initVldAnaksRelatedByAnakId();
            } else {
                $collVldAnaksRelatedByAnakId = VldAnakQuery::create(null, $criteria)
                    ->filterByAnakRelatedByAnakId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldAnaksRelatedByAnakIdPartial && count($collVldAnaksRelatedByAnakId)) {
                      $this->initVldAnaksRelatedByAnakId(false);

                      foreach($collVldAnaksRelatedByAnakId as $obj) {
                        if (false == $this->collVldAnaksRelatedByAnakId->contains($obj)) {
                          $this->collVldAnaksRelatedByAnakId->append($obj);
                        }
                      }

                      $this->collVldAnaksRelatedByAnakIdPartial = true;
                    }

                    $collVldAnaksRelatedByAnakId->getInternalIterator()->rewind();
                    return $collVldAnaksRelatedByAnakId;
                }

                if($partial && $this->collVldAnaksRelatedByAnakId) {
                    foreach($this->collVldAnaksRelatedByAnakId as $obj) {
                        if($obj->isNew()) {
                            $collVldAnaksRelatedByAnakId[] = $obj;
                        }
                    }
                }

                $this->collVldAnaksRelatedByAnakId = $collVldAnaksRelatedByAnakId;
                $this->collVldAnaksRelatedByAnakIdPartial = false;
            }
        }

        return $this->collVldAnaksRelatedByAnakId;
    }

    /**
     * Sets a collection of VldAnakRelatedByAnakId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldAnaksRelatedByAnakId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Anak The current object (for fluent API support)
     */
    public function setVldAnaksRelatedByAnakId(PropelCollection $vldAnaksRelatedByAnakId, PropelPDO $con = null)
    {
        $vldAnaksRelatedByAnakIdToDelete = $this->getVldAnaksRelatedByAnakId(new Criteria(), $con)->diff($vldAnaksRelatedByAnakId);

        $this->vldAnaksRelatedByAnakIdScheduledForDeletion = unserialize(serialize($vldAnaksRelatedByAnakIdToDelete));

        foreach ($vldAnaksRelatedByAnakIdToDelete as $vldAnakRelatedByAnakIdRemoved) {
            $vldAnakRelatedByAnakIdRemoved->setAnakRelatedByAnakId(null);
        }

        $this->collVldAnaksRelatedByAnakId = null;
        foreach ($vldAnaksRelatedByAnakId as $vldAnakRelatedByAnakId) {
            $this->addVldAnakRelatedByAnakId($vldAnakRelatedByAnakId);
        }

        $this->collVldAnaksRelatedByAnakId = $vldAnaksRelatedByAnakId;
        $this->collVldAnaksRelatedByAnakIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldAnak objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldAnak objects.
     * @throws PropelException
     */
    public function countVldAnaksRelatedByAnakId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldAnaksRelatedByAnakIdPartial && !$this->isNew();
        if (null === $this->collVldAnaksRelatedByAnakId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldAnaksRelatedByAnakId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldAnaksRelatedByAnakId());
            }
            $query = VldAnakQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByAnakRelatedByAnakId($this)
                ->count($con);
        }

        return count($this->collVldAnaksRelatedByAnakId);
    }

    /**
     * Method called to associate a VldAnak object to this object
     * through the VldAnak foreign key attribute.
     *
     * @param    VldAnak $l VldAnak
     * @return Anak The current object (for fluent API support)
     */
    public function addVldAnakRelatedByAnakId(VldAnak $l)
    {
        if ($this->collVldAnaksRelatedByAnakId === null) {
            $this->initVldAnaksRelatedByAnakId();
            $this->collVldAnaksRelatedByAnakIdPartial = true;
        }
        if (!in_array($l, $this->collVldAnaksRelatedByAnakId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldAnakRelatedByAnakId($l);
        }

        return $this;
    }

    /**
     * @param	VldAnakRelatedByAnakId $vldAnakRelatedByAnakId The vldAnakRelatedByAnakId object to add.
     */
    protected function doAddVldAnakRelatedByAnakId($vldAnakRelatedByAnakId)
    {
        $this->collVldAnaksRelatedByAnakId[]= $vldAnakRelatedByAnakId;
        $vldAnakRelatedByAnakId->setAnakRelatedByAnakId($this);
    }

    /**
     * @param	VldAnakRelatedByAnakId $vldAnakRelatedByAnakId The vldAnakRelatedByAnakId object to remove.
     * @return Anak The current object (for fluent API support)
     */
    public function removeVldAnakRelatedByAnakId($vldAnakRelatedByAnakId)
    {
        if ($this->getVldAnaksRelatedByAnakId()->contains($vldAnakRelatedByAnakId)) {
            $this->collVldAnaksRelatedByAnakId->remove($this->collVldAnaksRelatedByAnakId->search($vldAnakRelatedByAnakId));
            if (null === $this->vldAnaksRelatedByAnakIdScheduledForDeletion) {
                $this->vldAnaksRelatedByAnakIdScheduledForDeletion = clone $this->collVldAnaksRelatedByAnakId;
                $this->vldAnaksRelatedByAnakIdScheduledForDeletion->clear();
            }
            $this->vldAnaksRelatedByAnakIdScheduledForDeletion[]= clone $vldAnakRelatedByAnakId;
            $vldAnakRelatedByAnakId->setAnakRelatedByAnakId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Anak is new, it will return
     * an empty collection; or if this Anak has previously
     * been saved, it will retrieve related VldAnaksRelatedByAnakId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Anak.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldAnak[] List of VldAnak objects
     */
    public function getVldAnaksRelatedByAnakIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldAnakQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldAnaksRelatedByAnakId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Anak is new, it will return
     * an empty collection; or if this Anak has previously
     * been saved, it will retrieve related VldAnaksRelatedByAnakId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Anak.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldAnak[] List of VldAnak objects
     */
    public function getVldAnaksRelatedByAnakIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldAnakQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldAnaksRelatedByAnakId($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->anak_id = null;
        $this->ptk_id = null;
        $this->status_anak_id = null;
        $this->jenjang_pendidikan_id = null;
        $this->nisn = null;
        $this->nama = null;
        $this->jenis_kelamin = null;
        $this->tempat_lahir = null;
        $this->tanggal_lahir = null;
        $this->tahun_masuk = null;
        $this->last_update = null;
        $this->soft_delete = null;
        $this->last_sync = null;
        $this->updater_id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collVldAnaksRelatedByAnakId) {
                foreach ($this->collVldAnaksRelatedByAnakId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldAnaksRelatedByAnakId) {
                foreach ($this->collVldAnaksRelatedByAnakId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aPtkRelatedByPtkId instanceof Persistent) {
              $this->aPtkRelatedByPtkId->clearAllReferences($deep);
            }
            if ($this->aPtkRelatedByPtkId instanceof Persistent) {
              $this->aPtkRelatedByPtkId->clearAllReferences($deep);
            }
            if ($this->aJenjangPendidikanRelatedByJenjangPendidikanId instanceof Persistent) {
              $this->aJenjangPendidikanRelatedByJenjangPendidikanId->clearAllReferences($deep);
            }
            if ($this->aJenjangPendidikanRelatedByJenjangPendidikanId instanceof Persistent) {
              $this->aJenjangPendidikanRelatedByJenjangPendidikanId->clearAllReferences($deep);
            }
            if ($this->aStatusAnakRelatedByStatusAnakId instanceof Persistent) {
              $this->aStatusAnakRelatedByStatusAnakId->clearAllReferences($deep);
            }
            if ($this->aStatusAnakRelatedByStatusAnakId instanceof Persistent) {
              $this->aStatusAnakRelatedByStatusAnakId->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collVldAnaksRelatedByAnakId instanceof PropelCollection) {
            $this->collVldAnaksRelatedByAnakId->clearIterator();
        }
        $this->collVldAnaksRelatedByAnakId = null;
        if ($this->collVldAnaksRelatedByAnakId instanceof PropelCollection) {
            $this->collVldAnaksRelatedByAnakId->clearIterator();
        }
        $this->collVldAnaksRelatedByAnakId = null;
        $this->aPtkRelatedByPtkId = null;
        $this->aPtkRelatedByPtkId = null;
        $this->aJenjangPendidikanRelatedByJenjangPendidikanId = null;
        $this->aJenjangPendidikanRelatedByJenjangPendidikanId = null;
        $this->aStatusAnakRelatedByStatusAnakId = null;
        $this->aStatusAnakRelatedByStatusAnakId = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(AnakPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
