<?php

namespace angulex\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\BukuPtk;
use angulex\Model\BukuPtkPeer;
use angulex\Model\BukuPtkQuery;
use angulex\Model\Ptk;
use angulex\Model\VldBukuPtk;

/**
 * Base class that represents a query for the 'buku_ptk' table.
 *
 * 
 *
 * @method BukuPtkQuery orderByBukuId($order = Criteria::ASC) Order by the buku_id column
 * @method BukuPtkQuery orderByPtkId($order = Criteria::ASC) Order by the ptk_id column
 * @method BukuPtkQuery orderByJudulBuku($order = Criteria::ASC) Order by the judul_buku column
 * @method BukuPtkQuery orderByTahun($order = Criteria::ASC) Order by the tahun column
 * @method BukuPtkQuery orderByPenerbit($order = Criteria::ASC) Order by the penerbit column
 * @method BukuPtkQuery orderByLastUpdate($order = Criteria::ASC) Order by the Last_update column
 * @method BukuPtkQuery orderBySoftDelete($order = Criteria::ASC) Order by the Soft_delete column
 * @method BukuPtkQuery orderByLastSync($order = Criteria::ASC) Order by the last_sync column
 * @method BukuPtkQuery orderByUpdaterId($order = Criteria::ASC) Order by the Updater_ID column
 *
 * @method BukuPtkQuery groupByBukuId() Group by the buku_id column
 * @method BukuPtkQuery groupByPtkId() Group by the ptk_id column
 * @method BukuPtkQuery groupByJudulBuku() Group by the judul_buku column
 * @method BukuPtkQuery groupByTahun() Group by the tahun column
 * @method BukuPtkQuery groupByPenerbit() Group by the penerbit column
 * @method BukuPtkQuery groupByLastUpdate() Group by the Last_update column
 * @method BukuPtkQuery groupBySoftDelete() Group by the Soft_delete column
 * @method BukuPtkQuery groupByLastSync() Group by the last_sync column
 * @method BukuPtkQuery groupByUpdaterId() Group by the Updater_ID column
 *
 * @method BukuPtkQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method BukuPtkQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method BukuPtkQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method BukuPtkQuery leftJoinPtkRelatedByPtkId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PtkRelatedByPtkId relation
 * @method BukuPtkQuery rightJoinPtkRelatedByPtkId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PtkRelatedByPtkId relation
 * @method BukuPtkQuery innerJoinPtkRelatedByPtkId($relationAlias = null) Adds a INNER JOIN clause to the query using the PtkRelatedByPtkId relation
 *
 * @method BukuPtkQuery leftJoinPtkRelatedByPtkId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PtkRelatedByPtkId relation
 * @method BukuPtkQuery rightJoinPtkRelatedByPtkId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PtkRelatedByPtkId relation
 * @method BukuPtkQuery innerJoinPtkRelatedByPtkId($relationAlias = null) Adds a INNER JOIN clause to the query using the PtkRelatedByPtkId relation
 *
 * @method BukuPtkQuery leftJoinVldBukuPtkRelatedByBukuId($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldBukuPtkRelatedByBukuId relation
 * @method BukuPtkQuery rightJoinVldBukuPtkRelatedByBukuId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldBukuPtkRelatedByBukuId relation
 * @method BukuPtkQuery innerJoinVldBukuPtkRelatedByBukuId($relationAlias = null) Adds a INNER JOIN clause to the query using the VldBukuPtkRelatedByBukuId relation
 *
 * @method BukuPtkQuery leftJoinVldBukuPtkRelatedByBukuId($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldBukuPtkRelatedByBukuId relation
 * @method BukuPtkQuery rightJoinVldBukuPtkRelatedByBukuId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldBukuPtkRelatedByBukuId relation
 * @method BukuPtkQuery innerJoinVldBukuPtkRelatedByBukuId($relationAlias = null) Adds a INNER JOIN clause to the query using the VldBukuPtkRelatedByBukuId relation
 *
 * @method BukuPtk findOne(PropelPDO $con = null) Return the first BukuPtk matching the query
 * @method BukuPtk findOneOrCreate(PropelPDO $con = null) Return the first BukuPtk matching the query, or a new BukuPtk object populated from the query conditions when no match is found
 *
 * @method BukuPtk findOneByPtkId(string $ptk_id) Return the first BukuPtk filtered by the ptk_id column
 * @method BukuPtk findOneByJudulBuku(string $judul_buku) Return the first BukuPtk filtered by the judul_buku column
 * @method BukuPtk findOneByTahun(string $tahun) Return the first BukuPtk filtered by the tahun column
 * @method BukuPtk findOneByPenerbit(string $penerbit) Return the first BukuPtk filtered by the penerbit column
 * @method BukuPtk findOneByLastUpdate(string $Last_update) Return the first BukuPtk filtered by the Last_update column
 * @method BukuPtk findOneBySoftDelete(string $Soft_delete) Return the first BukuPtk filtered by the Soft_delete column
 * @method BukuPtk findOneByLastSync(string $last_sync) Return the first BukuPtk filtered by the last_sync column
 * @method BukuPtk findOneByUpdaterId(string $Updater_ID) Return the first BukuPtk filtered by the Updater_ID column
 *
 * @method array findByBukuId(string $buku_id) Return BukuPtk objects filtered by the buku_id column
 * @method array findByPtkId(string $ptk_id) Return BukuPtk objects filtered by the ptk_id column
 * @method array findByJudulBuku(string $judul_buku) Return BukuPtk objects filtered by the judul_buku column
 * @method array findByTahun(string $tahun) Return BukuPtk objects filtered by the tahun column
 * @method array findByPenerbit(string $penerbit) Return BukuPtk objects filtered by the penerbit column
 * @method array findByLastUpdate(string $Last_update) Return BukuPtk objects filtered by the Last_update column
 * @method array findBySoftDelete(string $Soft_delete) Return BukuPtk objects filtered by the Soft_delete column
 * @method array findByLastSync(string $last_sync) Return BukuPtk objects filtered by the last_sync column
 * @method array findByUpdaterId(string $Updater_ID) Return BukuPtk objects filtered by the Updater_ID column
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseBukuPtkQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseBukuPtkQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Dapodikmen', $modelName = 'angulex\\Model\\BukuPtk', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new BukuPtkQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   BukuPtkQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return BukuPtkQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof BukuPtkQuery) {
            return $criteria;
        }
        $query = new BukuPtkQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   BukuPtk|BukuPtk[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = BukuPtkPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(BukuPtkPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 BukuPtk A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByBukuId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 BukuPtk A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [buku_id], [ptk_id], [judul_buku], [tahun], [penerbit], [Last_update], [Soft_delete], [last_sync], [Updater_ID] FROM [buku_ptk] WHERE [buku_id] = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new BukuPtk();
            $obj->hydrate($row);
            BukuPtkPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return BukuPtk|BukuPtk[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|BukuPtk[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return BukuPtkQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(BukuPtkPeer::BUKU_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return BukuPtkQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(BukuPtkPeer::BUKU_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the buku_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBukuId('fooValue');   // WHERE buku_id = 'fooValue'
     * $query->filterByBukuId('%fooValue%'); // WHERE buku_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $bukuId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BukuPtkQuery The current query, for fluid interface
     */
    public function filterByBukuId($bukuId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($bukuId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $bukuId)) {
                $bukuId = str_replace('*', '%', $bukuId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BukuPtkPeer::BUKU_ID, $bukuId, $comparison);
    }

    /**
     * Filter the query on the ptk_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPtkId('fooValue');   // WHERE ptk_id = 'fooValue'
     * $query->filterByPtkId('%fooValue%'); // WHERE ptk_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ptkId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BukuPtkQuery The current query, for fluid interface
     */
    public function filterByPtkId($ptkId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ptkId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ptkId)) {
                $ptkId = str_replace('*', '%', $ptkId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BukuPtkPeer::PTK_ID, $ptkId, $comparison);
    }

    /**
     * Filter the query on the judul_buku column
     *
     * Example usage:
     * <code>
     * $query->filterByJudulBuku('fooValue');   // WHERE judul_buku = 'fooValue'
     * $query->filterByJudulBuku('%fooValue%'); // WHERE judul_buku LIKE '%fooValue%'
     * </code>
     *
     * @param     string $judulBuku The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BukuPtkQuery The current query, for fluid interface
     */
    public function filterByJudulBuku($judulBuku = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($judulBuku)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $judulBuku)) {
                $judulBuku = str_replace('*', '%', $judulBuku);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BukuPtkPeer::JUDUL_BUKU, $judulBuku, $comparison);
    }

    /**
     * Filter the query on the tahun column
     *
     * Example usage:
     * <code>
     * $query->filterByTahun(1234); // WHERE tahun = 1234
     * $query->filterByTahun(array(12, 34)); // WHERE tahun IN (12, 34)
     * $query->filterByTahun(array('min' => 12)); // WHERE tahun >= 12
     * $query->filterByTahun(array('max' => 12)); // WHERE tahun <= 12
     * </code>
     *
     * @param     mixed $tahun The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BukuPtkQuery The current query, for fluid interface
     */
    public function filterByTahun($tahun = null, $comparison = null)
    {
        if (is_array($tahun)) {
            $useMinMax = false;
            if (isset($tahun['min'])) {
                $this->addUsingAlias(BukuPtkPeer::TAHUN, $tahun['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tahun['max'])) {
                $this->addUsingAlias(BukuPtkPeer::TAHUN, $tahun['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BukuPtkPeer::TAHUN, $tahun, $comparison);
    }

    /**
     * Filter the query on the penerbit column
     *
     * Example usage:
     * <code>
     * $query->filterByPenerbit('fooValue');   // WHERE penerbit = 'fooValue'
     * $query->filterByPenerbit('%fooValue%'); // WHERE penerbit LIKE '%fooValue%'
     * </code>
     *
     * @param     string $penerbit The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BukuPtkQuery The current query, for fluid interface
     */
    public function filterByPenerbit($penerbit = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($penerbit)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $penerbit)) {
                $penerbit = str_replace('*', '%', $penerbit);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BukuPtkPeer::PENERBIT, $penerbit, $comparison);
    }

    /**
     * Filter the query on the Last_update column
     *
     * Example usage:
     * <code>
     * $query->filterByLastUpdate('2011-03-14'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate('now'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate(array('max' => 'yesterday')); // WHERE Last_update > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastUpdate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BukuPtkQuery The current query, for fluid interface
     */
    public function filterByLastUpdate($lastUpdate = null, $comparison = null)
    {
        if (is_array($lastUpdate)) {
            $useMinMax = false;
            if (isset($lastUpdate['min'])) {
                $this->addUsingAlias(BukuPtkPeer::LAST_UPDATE, $lastUpdate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastUpdate['max'])) {
                $this->addUsingAlias(BukuPtkPeer::LAST_UPDATE, $lastUpdate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BukuPtkPeer::LAST_UPDATE, $lastUpdate, $comparison);
    }

    /**
     * Filter the query on the Soft_delete column
     *
     * Example usage:
     * <code>
     * $query->filterBySoftDelete(1234); // WHERE Soft_delete = 1234
     * $query->filterBySoftDelete(array(12, 34)); // WHERE Soft_delete IN (12, 34)
     * $query->filterBySoftDelete(array('min' => 12)); // WHERE Soft_delete >= 12
     * $query->filterBySoftDelete(array('max' => 12)); // WHERE Soft_delete <= 12
     * </code>
     *
     * @param     mixed $softDelete The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BukuPtkQuery The current query, for fluid interface
     */
    public function filterBySoftDelete($softDelete = null, $comparison = null)
    {
        if (is_array($softDelete)) {
            $useMinMax = false;
            if (isset($softDelete['min'])) {
                $this->addUsingAlias(BukuPtkPeer::SOFT_DELETE, $softDelete['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($softDelete['max'])) {
                $this->addUsingAlias(BukuPtkPeer::SOFT_DELETE, $softDelete['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BukuPtkPeer::SOFT_DELETE, $softDelete, $comparison);
    }

    /**
     * Filter the query on the last_sync column
     *
     * Example usage:
     * <code>
     * $query->filterByLastSync('2011-03-14'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync('now'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync(array('max' => 'yesterday')); // WHERE last_sync > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastSync The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BukuPtkQuery The current query, for fluid interface
     */
    public function filterByLastSync($lastSync = null, $comparison = null)
    {
        if (is_array($lastSync)) {
            $useMinMax = false;
            if (isset($lastSync['min'])) {
                $this->addUsingAlias(BukuPtkPeer::LAST_SYNC, $lastSync['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastSync['max'])) {
                $this->addUsingAlias(BukuPtkPeer::LAST_SYNC, $lastSync['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BukuPtkPeer::LAST_SYNC, $lastSync, $comparison);
    }

    /**
     * Filter the query on the Updater_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdaterId('fooValue');   // WHERE Updater_ID = 'fooValue'
     * $query->filterByUpdaterId('%fooValue%'); // WHERE Updater_ID LIKE '%fooValue%'
     * </code>
     *
     * @param     string $updaterId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BukuPtkQuery The current query, for fluid interface
     */
    public function filterByUpdaterId($updaterId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($updaterId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $updaterId)) {
                $updaterId = str_replace('*', '%', $updaterId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BukuPtkPeer::UPDATER_ID, $updaterId, $comparison);
    }

    /**
     * Filter the query by a related Ptk object
     *
     * @param   Ptk|PropelObjectCollection $ptk The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 BukuPtkQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPtkRelatedByPtkId($ptk, $comparison = null)
    {
        if ($ptk instanceof Ptk) {
            return $this
                ->addUsingAlias(BukuPtkPeer::PTK_ID, $ptk->getPtkId(), $comparison);
        } elseif ($ptk instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BukuPtkPeer::PTK_ID, $ptk->toKeyValue('PrimaryKey', 'PtkId'), $comparison);
        } else {
            throw new PropelException('filterByPtkRelatedByPtkId() only accepts arguments of type Ptk or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PtkRelatedByPtkId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return BukuPtkQuery The current query, for fluid interface
     */
    public function joinPtkRelatedByPtkId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PtkRelatedByPtkId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PtkRelatedByPtkId');
        }

        return $this;
    }

    /**
     * Use the PtkRelatedByPtkId relation Ptk object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PtkQuery A secondary query class using the current class as primary query
     */
    public function usePtkRelatedByPtkIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPtkRelatedByPtkId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PtkRelatedByPtkId', '\angulex\Model\PtkQuery');
    }

    /**
     * Filter the query by a related Ptk object
     *
     * @param   Ptk|PropelObjectCollection $ptk The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 BukuPtkQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPtkRelatedByPtkId($ptk, $comparison = null)
    {
        if ($ptk instanceof Ptk) {
            return $this
                ->addUsingAlias(BukuPtkPeer::PTK_ID, $ptk->getPtkId(), $comparison);
        } elseif ($ptk instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BukuPtkPeer::PTK_ID, $ptk->toKeyValue('PrimaryKey', 'PtkId'), $comparison);
        } else {
            throw new PropelException('filterByPtkRelatedByPtkId() only accepts arguments of type Ptk or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PtkRelatedByPtkId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return BukuPtkQuery The current query, for fluid interface
     */
    public function joinPtkRelatedByPtkId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PtkRelatedByPtkId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PtkRelatedByPtkId');
        }

        return $this;
    }

    /**
     * Use the PtkRelatedByPtkId relation Ptk object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PtkQuery A secondary query class using the current class as primary query
     */
    public function usePtkRelatedByPtkIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPtkRelatedByPtkId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PtkRelatedByPtkId', '\angulex\Model\PtkQuery');
    }

    /**
     * Filter the query by a related VldBukuPtk object
     *
     * @param   VldBukuPtk|PropelObjectCollection $vldBukuPtk  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 BukuPtkQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldBukuPtkRelatedByBukuId($vldBukuPtk, $comparison = null)
    {
        if ($vldBukuPtk instanceof VldBukuPtk) {
            return $this
                ->addUsingAlias(BukuPtkPeer::BUKU_ID, $vldBukuPtk->getBukuId(), $comparison);
        } elseif ($vldBukuPtk instanceof PropelObjectCollection) {
            return $this
                ->useVldBukuPtkRelatedByBukuIdQuery()
                ->filterByPrimaryKeys($vldBukuPtk->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldBukuPtkRelatedByBukuId() only accepts arguments of type VldBukuPtk or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldBukuPtkRelatedByBukuId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return BukuPtkQuery The current query, for fluid interface
     */
    public function joinVldBukuPtkRelatedByBukuId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldBukuPtkRelatedByBukuId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldBukuPtkRelatedByBukuId');
        }

        return $this;
    }

    /**
     * Use the VldBukuPtkRelatedByBukuId relation VldBukuPtk object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldBukuPtkQuery A secondary query class using the current class as primary query
     */
    public function useVldBukuPtkRelatedByBukuIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldBukuPtkRelatedByBukuId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldBukuPtkRelatedByBukuId', '\angulex\Model\VldBukuPtkQuery');
    }

    /**
     * Filter the query by a related VldBukuPtk object
     *
     * @param   VldBukuPtk|PropelObjectCollection $vldBukuPtk  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 BukuPtkQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldBukuPtkRelatedByBukuId($vldBukuPtk, $comparison = null)
    {
        if ($vldBukuPtk instanceof VldBukuPtk) {
            return $this
                ->addUsingAlias(BukuPtkPeer::BUKU_ID, $vldBukuPtk->getBukuId(), $comparison);
        } elseif ($vldBukuPtk instanceof PropelObjectCollection) {
            return $this
                ->useVldBukuPtkRelatedByBukuIdQuery()
                ->filterByPrimaryKeys($vldBukuPtk->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldBukuPtkRelatedByBukuId() only accepts arguments of type VldBukuPtk or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldBukuPtkRelatedByBukuId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return BukuPtkQuery The current query, for fluid interface
     */
    public function joinVldBukuPtkRelatedByBukuId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldBukuPtkRelatedByBukuId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldBukuPtkRelatedByBukuId');
        }

        return $this;
    }

    /**
     * Use the VldBukuPtkRelatedByBukuId relation VldBukuPtk object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldBukuPtkQuery A secondary query class using the current class as primary query
     */
    public function useVldBukuPtkRelatedByBukuIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldBukuPtkRelatedByBukuId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldBukuPtkRelatedByBukuId', '\angulex\Model\VldBukuPtkQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   BukuPtk $bukuPtk Object to remove from the list of results
     *
     * @return BukuPtkQuery The current query, for fluid interface
     */
    public function prune($bukuPtk = null)
    {
        if ($bukuPtk) {
            $this->addUsingAlias(BukuPtkPeer::BUKU_ID, $bukuPtk->getBukuId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
