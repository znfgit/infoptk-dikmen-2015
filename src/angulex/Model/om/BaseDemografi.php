<?php

namespace angulex\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelDateTime;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\Demografi;
use angulex\Model\DemografiPeer;
use angulex\Model\DemografiQuery;
use angulex\Model\MstWilayah;
use angulex\Model\MstWilayahQuery;
use angulex\Model\TahunAjaran;
use angulex\Model\TahunAjaranQuery;
use angulex\Model\VldDemografi;
use angulex\Model\VldDemografiQuery;

/**
 * Base class that represents a row from the 'demografi' table.
 *
 * 
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseDemografi extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'angulex\\Model\\DemografiPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        DemografiPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the demografi_id field.
     * @var        string
     */
    protected $demografi_id;

    /**
     * The value for the kode_wilayah field.
     * @var        string
     */
    protected $kode_wilayah;

    /**
     * The value for the tahun_ajaran_id field.
     * @var        string
     */
    protected $tahun_ajaran_id;

    /**
     * The value for the usia_5 field.
     * @var        string
     */
    protected $usia_5;

    /**
     * The value for the usia_7 field.
     * @var        string
     */
    protected $usia_7;

    /**
     * The value for the usia_13 field.
     * @var        string
     */
    protected $usia_13;

    /**
     * The value for the usia_16 field.
     * @var        string
     */
    protected $usia_16;

    /**
     * The value for the usia_18 field.
     * @var        string
     */
    protected $usia_18;

    /**
     * The value for the jumlah_penduduk field.
     * @var        string
     */
    protected $jumlah_penduduk;

    /**
     * The value for the last_update field.
     * @var        string
     */
    protected $last_update;

    /**
     * The value for the soft_delete field.
     * @var        string
     */
    protected $soft_delete;

    /**
     * The value for the last_sync field.
     * @var        string
     */
    protected $last_sync;

    /**
     * The value for the updater_id field.
     * @var        string
     */
    protected $updater_id;

    /**
     * @var        MstWilayah
     */
    protected $aMstWilayahRelatedByKodeWilayah;

    /**
     * @var        MstWilayah
     */
    protected $aMstWilayahRelatedByKodeWilayah;

    /**
     * @var        TahunAjaran
     */
    protected $aTahunAjaranRelatedByTahunAjaranId;

    /**
     * @var        TahunAjaran
     */
    protected $aTahunAjaranRelatedByTahunAjaranId;

    /**
     * @var        PropelObjectCollection|VldDemografi[] Collection to store aggregation of VldDemografi objects.
     */
    protected $collVldDemografisRelatedByDemografiId;
    protected $collVldDemografisRelatedByDemografiIdPartial;

    /**
     * @var        PropelObjectCollection|VldDemografi[] Collection to store aggregation of VldDemografi objects.
     */
    protected $collVldDemografisRelatedByDemografiId;
    protected $collVldDemografisRelatedByDemografiIdPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldDemografisRelatedByDemografiIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldDemografisRelatedByDemografiIdScheduledForDeletion = null;

    /**
     * Get the [demografi_id] column value.
     * 
     * @return string
     */
    public function getDemografiId()
    {
        return $this->demografi_id;
    }

    /**
     * Get the [kode_wilayah] column value.
     * 
     * @return string
     */
    public function getKodeWilayah()
    {
        return $this->kode_wilayah;
    }

    /**
     * Get the [tahun_ajaran_id] column value.
     * 
     * @return string
     */
    public function getTahunAjaranId()
    {
        return $this->tahun_ajaran_id;
    }

    /**
     * Get the [usia_5] column value.
     * 
     * @return string
     */
    public function getUsia5()
    {
        return $this->usia_5;
    }

    /**
     * Get the [usia_7] column value.
     * 
     * @return string
     */
    public function getUsia7()
    {
        return $this->usia_7;
    }

    /**
     * Get the [usia_13] column value.
     * 
     * @return string
     */
    public function getUsia13()
    {
        return $this->usia_13;
    }

    /**
     * Get the [usia_16] column value.
     * 
     * @return string
     */
    public function getUsia16()
    {
        return $this->usia_16;
    }

    /**
     * Get the [usia_18] column value.
     * 
     * @return string
     */
    public function getUsia18()
    {
        return $this->usia_18;
    }

    /**
     * Get the [jumlah_penduduk] column value.
     * 
     * @return string
     */
    public function getJumlahPenduduk()
    {
        return $this->jumlah_penduduk;
    }

    /**
     * Get the [optionally formatted] temporal [last_update] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastUpdate($format = 'Y-m-d H:i:s')
    {
        if ($this->last_update === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_update);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_update, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [soft_delete] column value.
     * 
     * @return string
     */
    public function getSoftDelete()
    {
        return $this->soft_delete;
    }

    /**
     * Get the [optionally formatted] temporal [last_sync] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastSync($format = 'Y-m-d H:i:s')
    {
        if ($this->last_sync === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_sync);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_sync, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [updater_id] column value.
     * 
     * @return string
     */
    public function getUpdaterId()
    {
        return $this->updater_id;
    }

    /**
     * Set the value of [demografi_id] column.
     * 
     * @param string $v new value
     * @return Demografi The current object (for fluent API support)
     */
    public function setDemografiId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->demografi_id !== $v) {
            $this->demografi_id = $v;
            $this->modifiedColumns[] = DemografiPeer::DEMOGRAFI_ID;
        }


        return $this;
    } // setDemografiId()

    /**
     * Set the value of [kode_wilayah] column.
     * 
     * @param string $v new value
     * @return Demografi The current object (for fluent API support)
     */
    public function setKodeWilayah($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->kode_wilayah !== $v) {
            $this->kode_wilayah = $v;
            $this->modifiedColumns[] = DemografiPeer::KODE_WILAYAH;
        }

        if ($this->aMstWilayahRelatedByKodeWilayah !== null && $this->aMstWilayahRelatedByKodeWilayah->getKodeWilayah() !== $v) {
            $this->aMstWilayahRelatedByKodeWilayah = null;
        }

        if ($this->aMstWilayahRelatedByKodeWilayah !== null && $this->aMstWilayahRelatedByKodeWilayah->getKodeWilayah() !== $v) {
            $this->aMstWilayahRelatedByKodeWilayah = null;
        }


        return $this;
    } // setKodeWilayah()

    /**
     * Set the value of [tahun_ajaran_id] column.
     * 
     * @param string $v new value
     * @return Demografi The current object (for fluent API support)
     */
    public function setTahunAjaranId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tahun_ajaran_id !== $v) {
            $this->tahun_ajaran_id = $v;
            $this->modifiedColumns[] = DemografiPeer::TAHUN_AJARAN_ID;
        }

        if ($this->aTahunAjaranRelatedByTahunAjaranId !== null && $this->aTahunAjaranRelatedByTahunAjaranId->getTahunAjaranId() !== $v) {
            $this->aTahunAjaranRelatedByTahunAjaranId = null;
        }

        if ($this->aTahunAjaranRelatedByTahunAjaranId !== null && $this->aTahunAjaranRelatedByTahunAjaranId->getTahunAjaranId() !== $v) {
            $this->aTahunAjaranRelatedByTahunAjaranId = null;
        }


        return $this;
    } // setTahunAjaranId()

    /**
     * Set the value of [usia_5] column.
     * 
     * @param string $v new value
     * @return Demografi The current object (for fluent API support)
     */
    public function setUsia5($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->usia_5 !== $v) {
            $this->usia_5 = $v;
            $this->modifiedColumns[] = DemografiPeer::USIA_5;
        }


        return $this;
    } // setUsia5()

    /**
     * Set the value of [usia_7] column.
     * 
     * @param string $v new value
     * @return Demografi The current object (for fluent API support)
     */
    public function setUsia7($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->usia_7 !== $v) {
            $this->usia_7 = $v;
            $this->modifiedColumns[] = DemografiPeer::USIA_7;
        }


        return $this;
    } // setUsia7()

    /**
     * Set the value of [usia_13] column.
     * 
     * @param string $v new value
     * @return Demografi The current object (for fluent API support)
     */
    public function setUsia13($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->usia_13 !== $v) {
            $this->usia_13 = $v;
            $this->modifiedColumns[] = DemografiPeer::USIA_13;
        }


        return $this;
    } // setUsia13()

    /**
     * Set the value of [usia_16] column.
     * 
     * @param string $v new value
     * @return Demografi The current object (for fluent API support)
     */
    public function setUsia16($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->usia_16 !== $v) {
            $this->usia_16 = $v;
            $this->modifiedColumns[] = DemografiPeer::USIA_16;
        }


        return $this;
    } // setUsia16()

    /**
     * Set the value of [usia_18] column.
     * 
     * @param string $v new value
     * @return Demografi The current object (for fluent API support)
     */
    public function setUsia18($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->usia_18 !== $v) {
            $this->usia_18 = $v;
            $this->modifiedColumns[] = DemografiPeer::USIA_18;
        }


        return $this;
    } // setUsia18()

    /**
     * Set the value of [jumlah_penduduk] column.
     * 
     * @param string $v new value
     * @return Demografi The current object (for fluent API support)
     */
    public function setJumlahPenduduk($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->jumlah_penduduk !== $v) {
            $this->jumlah_penduduk = $v;
            $this->modifiedColumns[] = DemografiPeer::JUMLAH_PENDUDUK;
        }


        return $this;
    } // setJumlahPenduduk()

    /**
     * Sets the value of [last_update] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Demografi The current object (for fluent API support)
     */
    public function setLastUpdate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_update !== null || $dt !== null) {
            $currentDateAsString = ($this->last_update !== null && $tmpDt = new DateTime($this->last_update)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_update = $newDateAsString;
                $this->modifiedColumns[] = DemografiPeer::LAST_UPDATE;
            }
        } // if either are not null


        return $this;
    } // setLastUpdate()

    /**
     * Set the value of [soft_delete] column.
     * 
     * @param string $v new value
     * @return Demografi The current object (for fluent API support)
     */
    public function setSoftDelete($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->soft_delete !== $v) {
            $this->soft_delete = $v;
            $this->modifiedColumns[] = DemografiPeer::SOFT_DELETE;
        }


        return $this;
    } // setSoftDelete()

    /**
     * Sets the value of [last_sync] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Demografi The current object (for fluent API support)
     */
    public function setLastSync($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_sync !== null || $dt !== null) {
            $currentDateAsString = ($this->last_sync !== null && $tmpDt = new DateTime($this->last_sync)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_sync = $newDateAsString;
                $this->modifiedColumns[] = DemografiPeer::LAST_SYNC;
            }
        } // if either are not null


        return $this;
    } // setLastSync()

    /**
     * Set the value of [updater_id] column.
     * 
     * @param string $v new value
     * @return Demografi The current object (for fluent API support)
     */
    public function setUpdaterId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->updater_id !== $v) {
            $this->updater_id = $v;
            $this->modifiedColumns[] = DemografiPeer::UPDATER_ID;
        }


        return $this;
    } // setUpdaterId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->demografi_id = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
            $this->kode_wilayah = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->tahun_ajaran_id = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->usia_5 = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->usia_7 = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->usia_13 = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->usia_16 = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->usia_18 = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->jumlah_penduduk = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->last_update = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->soft_delete = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->last_sync = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->updater_id = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 13; // 13 = DemografiPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Demografi object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aMstWilayahRelatedByKodeWilayah !== null && $this->kode_wilayah !== $this->aMstWilayahRelatedByKodeWilayah->getKodeWilayah()) {
            $this->aMstWilayahRelatedByKodeWilayah = null;
        }
        if ($this->aMstWilayahRelatedByKodeWilayah !== null && $this->kode_wilayah !== $this->aMstWilayahRelatedByKodeWilayah->getKodeWilayah()) {
            $this->aMstWilayahRelatedByKodeWilayah = null;
        }
        if ($this->aTahunAjaranRelatedByTahunAjaranId !== null && $this->tahun_ajaran_id !== $this->aTahunAjaranRelatedByTahunAjaranId->getTahunAjaranId()) {
            $this->aTahunAjaranRelatedByTahunAjaranId = null;
        }
        if ($this->aTahunAjaranRelatedByTahunAjaranId !== null && $this->tahun_ajaran_id !== $this->aTahunAjaranRelatedByTahunAjaranId->getTahunAjaranId()) {
            $this->aTahunAjaranRelatedByTahunAjaranId = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(DemografiPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = DemografiPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aMstWilayahRelatedByKodeWilayah = null;
            $this->aMstWilayahRelatedByKodeWilayah = null;
            $this->aTahunAjaranRelatedByTahunAjaranId = null;
            $this->aTahunAjaranRelatedByTahunAjaranId = null;
            $this->collVldDemografisRelatedByDemografiId = null;

            $this->collVldDemografisRelatedByDemografiId = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(DemografiPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = DemografiQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(DemografiPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                DemografiPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aMstWilayahRelatedByKodeWilayah !== null) {
                if ($this->aMstWilayahRelatedByKodeWilayah->isModified() || $this->aMstWilayahRelatedByKodeWilayah->isNew()) {
                    $affectedRows += $this->aMstWilayahRelatedByKodeWilayah->save($con);
                }
                $this->setMstWilayahRelatedByKodeWilayah($this->aMstWilayahRelatedByKodeWilayah);
            }

            if ($this->aMstWilayahRelatedByKodeWilayah !== null) {
                if ($this->aMstWilayahRelatedByKodeWilayah->isModified() || $this->aMstWilayahRelatedByKodeWilayah->isNew()) {
                    $affectedRows += $this->aMstWilayahRelatedByKodeWilayah->save($con);
                }
                $this->setMstWilayahRelatedByKodeWilayah($this->aMstWilayahRelatedByKodeWilayah);
            }

            if ($this->aTahunAjaranRelatedByTahunAjaranId !== null) {
                if ($this->aTahunAjaranRelatedByTahunAjaranId->isModified() || $this->aTahunAjaranRelatedByTahunAjaranId->isNew()) {
                    $affectedRows += $this->aTahunAjaranRelatedByTahunAjaranId->save($con);
                }
                $this->setTahunAjaranRelatedByTahunAjaranId($this->aTahunAjaranRelatedByTahunAjaranId);
            }

            if ($this->aTahunAjaranRelatedByTahunAjaranId !== null) {
                if ($this->aTahunAjaranRelatedByTahunAjaranId->isModified() || $this->aTahunAjaranRelatedByTahunAjaranId->isNew()) {
                    $affectedRows += $this->aTahunAjaranRelatedByTahunAjaranId->save($con);
                }
                $this->setTahunAjaranRelatedByTahunAjaranId($this->aTahunAjaranRelatedByTahunAjaranId);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->vldDemografisRelatedByDemografiIdScheduledForDeletion !== null) {
                if (!$this->vldDemografisRelatedByDemografiIdScheduledForDeletion->isEmpty()) {
                    VldDemografiQuery::create()
                        ->filterByPrimaryKeys($this->vldDemografisRelatedByDemografiIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldDemografisRelatedByDemografiIdScheduledForDeletion = null;
                }
            }

            if ($this->collVldDemografisRelatedByDemografiId !== null) {
                foreach ($this->collVldDemografisRelatedByDemografiId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldDemografisRelatedByDemografiIdScheduledForDeletion !== null) {
                if (!$this->vldDemografisRelatedByDemografiIdScheduledForDeletion->isEmpty()) {
                    VldDemografiQuery::create()
                        ->filterByPrimaryKeys($this->vldDemografisRelatedByDemografiIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldDemografisRelatedByDemografiIdScheduledForDeletion = null;
                }
            }

            if ($this->collVldDemografisRelatedByDemografiId !== null) {
                foreach ($this->collVldDemografisRelatedByDemografiId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $criteria = $this->buildCriteria();
        $pk = BasePeer::doInsert($criteria, $con);
        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aMstWilayahRelatedByKodeWilayah !== null) {
                if (!$this->aMstWilayahRelatedByKodeWilayah->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aMstWilayahRelatedByKodeWilayah->getValidationFailures());
                }
            }

            if ($this->aMstWilayahRelatedByKodeWilayah !== null) {
                if (!$this->aMstWilayahRelatedByKodeWilayah->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aMstWilayahRelatedByKodeWilayah->getValidationFailures());
                }
            }

            if ($this->aTahunAjaranRelatedByTahunAjaranId !== null) {
                if (!$this->aTahunAjaranRelatedByTahunAjaranId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aTahunAjaranRelatedByTahunAjaranId->getValidationFailures());
                }
            }

            if ($this->aTahunAjaranRelatedByTahunAjaranId !== null) {
                if (!$this->aTahunAjaranRelatedByTahunAjaranId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aTahunAjaranRelatedByTahunAjaranId->getValidationFailures());
                }
            }


            if (($retval = DemografiPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collVldDemografisRelatedByDemografiId !== null) {
                    foreach ($this->collVldDemografisRelatedByDemografiId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldDemografisRelatedByDemografiId !== null) {
                    foreach ($this->collVldDemografisRelatedByDemografiId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = DemografiPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getDemografiId();
                break;
            case 1:
                return $this->getKodeWilayah();
                break;
            case 2:
                return $this->getTahunAjaranId();
                break;
            case 3:
                return $this->getUsia5();
                break;
            case 4:
                return $this->getUsia7();
                break;
            case 5:
                return $this->getUsia13();
                break;
            case 6:
                return $this->getUsia16();
                break;
            case 7:
                return $this->getUsia18();
                break;
            case 8:
                return $this->getJumlahPenduduk();
                break;
            case 9:
                return $this->getLastUpdate();
                break;
            case 10:
                return $this->getSoftDelete();
                break;
            case 11:
                return $this->getLastSync();
                break;
            case 12:
                return $this->getUpdaterId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Demografi'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Demografi'][$this->getPrimaryKey()] = true;
        $keys = DemografiPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getDemografiId(),
            $keys[1] => $this->getKodeWilayah(),
            $keys[2] => $this->getTahunAjaranId(),
            $keys[3] => $this->getUsia5(),
            $keys[4] => $this->getUsia7(),
            $keys[5] => $this->getUsia13(),
            $keys[6] => $this->getUsia16(),
            $keys[7] => $this->getUsia18(),
            $keys[8] => $this->getJumlahPenduduk(),
            $keys[9] => $this->getLastUpdate(),
            $keys[10] => $this->getSoftDelete(),
            $keys[11] => $this->getLastSync(),
            $keys[12] => $this->getUpdaterId(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aMstWilayahRelatedByKodeWilayah) {
                $result['MstWilayahRelatedByKodeWilayah'] = $this->aMstWilayahRelatedByKodeWilayah->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aMstWilayahRelatedByKodeWilayah) {
                $result['MstWilayahRelatedByKodeWilayah'] = $this->aMstWilayahRelatedByKodeWilayah->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aTahunAjaranRelatedByTahunAjaranId) {
                $result['TahunAjaranRelatedByTahunAjaranId'] = $this->aTahunAjaranRelatedByTahunAjaranId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aTahunAjaranRelatedByTahunAjaranId) {
                $result['TahunAjaranRelatedByTahunAjaranId'] = $this->aTahunAjaranRelatedByTahunAjaranId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collVldDemografisRelatedByDemografiId) {
                $result['VldDemografisRelatedByDemografiId'] = $this->collVldDemografisRelatedByDemografiId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldDemografisRelatedByDemografiId) {
                $result['VldDemografisRelatedByDemografiId'] = $this->collVldDemografisRelatedByDemografiId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = DemografiPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setDemografiId($value);
                break;
            case 1:
                $this->setKodeWilayah($value);
                break;
            case 2:
                $this->setTahunAjaranId($value);
                break;
            case 3:
                $this->setUsia5($value);
                break;
            case 4:
                $this->setUsia7($value);
                break;
            case 5:
                $this->setUsia13($value);
                break;
            case 6:
                $this->setUsia16($value);
                break;
            case 7:
                $this->setUsia18($value);
                break;
            case 8:
                $this->setJumlahPenduduk($value);
                break;
            case 9:
                $this->setLastUpdate($value);
                break;
            case 10:
                $this->setSoftDelete($value);
                break;
            case 11:
                $this->setLastSync($value);
                break;
            case 12:
                $this->setUpdaterId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = DemografiPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setDemografiId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setKodeWilayah($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setTahunAjaranId($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setUsia5($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setUsia7($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setUsia13($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setUsia16($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setUsia18($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setJumlahPenduduk($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setLastUpdate($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setSoftDelete($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setLastSync($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setUpdaterId($arr[$keys[12]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(DemografiPeer::DATABASE_NAME);

        if ($this->isColumnModified(DemografiPeer::DEMOGRAFI_ID)) $criteria->add(DemografiPeer::DEMOGRAFI_ID, $this->demografi_id);
        if ($this->isColumnModified(DemografiPeer::KODE_WILAYAH)) $criteria->add(DemografiPeer::KODE_WILAYAH, $this->kode_wilayah);
        if ($this->isColumnModified(DemografiPeer::TAHUN_AJARAN_ID)) $criteria->add(DemografiPeer::TAHUN_AJARAN_ID, $this->tahun_ajaran_id);
        if ($this->isColumnModified(DemografiPeer::USIA_5)) $criteria->add(DemografiPeer::USIA_5, $this->usia_5);
        if ($this->isColumnModified(DemografiPeer::USIA_7)) $criteria->add(DemografiPeer::USIA_7, $this->usia_7);
        if ($this->isColumnModified(DemografiPeer::USIA_13)) $criteria->add(DemografiPeer::USIA_13, $this->usia_13);
        if ($this->isColumnModified(DemografiPeer::USIA_16)) $criteria->add(DemografiPeer::USIA_16, $this->usia_16);
        if ($this->isColumnModified(DemografiPeer::USIA_18)) $criteria->add(DemografiPeer::USIA_18, $this->usia_18);
        if ($this->isColumnModified(DemografiPeer::JUMLAH_PENDUDUK)) $criteria->add(DemografiPeer::JUMLAH_PENDUDUK, $this->jumlah_penduduk);
        if ($this->isColumnModified(DemografiPeer::LAST_UPDATE)) $criteria->add(DemografiPeer::LAST_UPDATE, $this->last_update);
        if ($this->isColumnModified(DemografiPeer::SOFT_DELETE)) $criteria->add(DemografiPeer::SOFT_DELETE, $this->soft_delete);
        if ($this->isColumnModified(DemografiPeer::LAST_SYNC)) $criteria->add(DemografiPeer::LAST_SYNC, $this->last_sync);
        if ($this->isColumnModified(DemografiPeer::UPDATER_ID)) $criteria->add(DemografiPeer::UPDATER_ID, $this->updater_id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(DemografiPeer::DATABASE_NAME);
        $criteria->add(DemografiPeer::DEMOGRAFI_ID, $this->demografi_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getDemografiId();
    }

    /**
     * Generic method to set the primary key (demografi_id column).
     *
     * @param  string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setDemografiId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getDemografiId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Demografi (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setKodeWilayah($this->getKodeWilayah());
        $copyObj->setTahunAjaranId($this->getTahunAjaranId());
        $copyObj->setUsia5($this->getUsia5());
        $copyObj->setUsia7($this->getUsia7());
        $copyObj->setUsia13($this->getUsia13());
        $copyObj->setUsia16($this->getUsia16());
        $copyObj->setUsia18($this->getUsia18());
        $copyObj->setJumlahPenduduk($this->getJumlahPenduduk());
        $copyObj->setLastUpdate($this->getLastUpdate());
        $copyObj->setSoftDelete($this->getSoftDelete());
        $copyObj->setLastSync($this->getLastSync());
        $copyObj->setUpdaterId($this->getUpdaterId());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getVldDemografisRelatedByDemografiId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldDemografiRelatedByDemografiId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldDemografisRelatedByDemografiId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldDemografiRelatedByDemografiId($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setDemografiId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Demografi Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return DemografiPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new DemografiPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a MstWilayah object.
     *
     * @param             MstWilayah $v
     * @return Demografi The current object (for fluent API support)
     * @throws PropelException
     */
    public function setMstWilayahRelatedByKodeWilayah(MstWilayah $v = null)
    {
        if ($v === null) {
            $this->setKodeWilayah(NULL);
        } else {
            $this->setKodeWilayah($v->getKodeWilayah());
        }

        $this->aMstWilayahRelatedByKodeWilayah = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the MstWilayah object, it will not be re-added.
        if ($v !== null) {
            $v->addDemografiRelatedByKodeWilayah($this);
        }


        return $this;
    }


    /**
     * Get the associated MstWilayah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return MstWilayah The associated MstWilayah object.
     * @throws PropelException
     */
    public function getMstWilayahRelatedByKodeWilayah(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aMstWilayahRelatedByKodeWilayah === null && (($this->kode_wilayah !== "" && $this->kode_wilayah !== null)) && $doQuery) {
            $this->aMstWilayahRelatedByKodeWilayah = MstWilayahQuery::create()->findPk($this->kode_wilayah, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aMstWilayahRelatedByKodeWilayah->addDemografisRelatedByKodeWilayah($this);
             */
        }

        return $this->aMstWilayahRelatedByKodeWilayah;
    }

    /**
     * Declares an association between this object and a MstWilayah object.
     *
     * @param             MstWilayah $v
     * @return Demografi The current object (for fluent API support)
     * @throws PropelException
     */
    public function setMstWilayahRelatedByKodeWilayah(MstWilayah $v = null)
    {
        if ($v === null) {
            $this->setKodeWilayah(NULL);
        } else {
            $this->setKodeWilayah($v->getKodeWilayah());
        }

        $this->aMstWilayahRelatedByKodeWilayah = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the MstWilayah object, it will not be re-added.
        if ($v !== null) {
            $v->addDemografiRelatedByKodeWilayah($this);
        }


        return $this;
    }


    /**
     * Get the associated MstWilayah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return MstWilayah The associated MstWilayah object.
     * @throws PropelException
     */
    public function getMstWilayahRelatedByKodeWilayah(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aMstWilayahRelatedByKodeWilayah === null && (($this->kode_wilayah !== "" && $this->kode_wilayah !== null)) && $doQuery) {
            $this->aMstWilayahRelatedByKodeWilayah = MstWilayahQuery::create()->findPk($this->kode_wilayah, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aMstWilayahRelatedByKodeWilayah->addDemografisRelatedByKodeWilayah($this);
             */
        }

        return $this->aMstWilayahRelatedByKodeWilayah;
    }

    /**
     * Declares an association between this object and a TahunAjaran object.
     *
     * @param             TahunAjaran $v
     * @return Demografi The current object (for fluent API support)
     * @throws PropelException
     */
    public function setTahunAjaranRelatedByTahunAjaranId(TahunAjaran $v = null)
    {
        if ($v === null) {
            $this->setTahunAjaranId(NULL);
        } else {
            $this->setTahunAjaranId($v->getTahunAjaranId());
        }

        $this->aTahunAjaranRelatedByTahunAjaranId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the TahunAjaran object, it will not be re-added.
        if ($v !== null) {
            $v->addDemografiRelatedByTahunAjaranId($this);
        }


        return $this;
    }


    /**
     * Get the associated TahunAjaran object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return TahunAjaran The associated TahunAjaran object.
     * @throws PropelException
     */
    public function getTahunAjaranRelatedByTahunAjaranId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aTahunAjaranRelatedByTahunAjaranId === null && (($this->tahun_ajaran_id !== "" && $this->tahun_ajaran_id !== null)) && $doQuery) {
            $this->aTahunAjaranRelatedByTahunAjaranId = TahunAjaranQuery::create()->findPk($this->tahun_ajaran_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aTahunAjaranRelatedByTahunAjaranId->addDemografisRelatedByTahunAjaranId($this);
             */
        }

        return $this->aTahunAjaranRelatedByTahunAjaranId;
    }

    /**
     * Declares an association between this object and a TahunAjaran object.
     *
     * @param             TahunAjaran $v
     * @return Demografi The current object (for fluent API support)
     * @throws PropelException
     */
    public function setTahunAjaranRelatedByTahunAjaranId(TahunAjaran $v = null)
    {
        if ($v === null) {
            $this->setTahunAjaranId(NULL);
        } else {
            $this->setTahunAjaranId($v->getTahunAjaranId());
        }

        $this->aTahunAjaranRelatedByTahunAjaranId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the TahunAjaran object, it will not be re-added.
        if ($v !== null) {
            $v->addDemografiRelatedByTahunAjaranId($this);
        }


        return $this;
    }


    /**
     * Get the associated TahunAjaran object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return TahunAjaran The associated TahunAjaran object.
     * @throws PropelException
     */
    public function getTahunAjaranRelatedByTahunAjaranId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aTahunAjaranRelatedByTahunAjaranId === null && (($this->tahun_ajaran_id !== "" && $this->tahun_ajaran_id !== null)) && $doQuery) {
            $this->aTahunAjaranRelatedByTahunAjaranId = TahunAjaranQuery::create()->findPk($this->tahun_ajaran_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aTahunAjaranRelatedByTahunAjaranId->addDemografisRelatedByTahunAjaranId($this);
             */
        }

        return $this->aTahunAjaranRelatedByTahunAjaranId;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('VldDemografiRelatedByDemografiId' == $relationName) {
            $this->initVldDemografisRelatedByDemografiId();
        }
        if ('VldDemografiRelatedByDemografiId' == $relationName) {
            $this->initVldDemografisRelatedByDemografiId();
        }
    }

    /**
     * Clears out the collVldDemografisRelatedByDemografiId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Demografi The current object (for fluent API support)
     * @see        addVldDemografisRelatedByDemografiId()
     */
    public function clearVldDemografisRelatedByDemografiId()
    {
        $this->collVldDemografisRelatedByDemografiId = null; // important to set this to null since that means it is uninitialized
        $this->collVldDemografisRelatedByDemografiIdPartial = null;

        return $this;
    }

    /**
     * reset is the collVldDemografisRelatedByDemografiId collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldDemografisRelatedByDemografiId($v = true)
    {
        $this->collVldDemografisRelatedByDemografiIdPartial = $v;
    }

    /**
     * Initializes the collVldDemografisRelatedByDemografiId collection.
     *
     * By default this just sets the collVldDemografisRelatedByDemografiId collection to an empty array (like clearcollVldDemografisRelatedByDemografiId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldDemografisRelatedByDemografiId($overrideExisting = true)
    {
        if (null !== $this->collVldDemografisRelatedByDemografiId && !$overrideExisting) {
            return;
        }
        $this->collVldDemografisRelatedByDemografiId = new PropelObjectCollection();
        $this->collVldDemografisRelatedByDemografiId->setModel('VldDemografi');
    }

    /**
     * Gets an array of VldDemografi objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Demografi is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldDemografi[] List of VldDemografi objects
     * @throws PropelException
     */
    public function getVldDemografisRelatedByDemografiId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldDemografisRelatedByDemografiIdPartial && !$this->isNew();
        if (null === $this->collVldDemografisRelatedByDemografiId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldDemografisRelatedByDemografiId) {
                // return empty collection
                $this->initVldDemografisRelatedByDemografiId();
            } else {
                $collVldDemografisRelatedByDemografiId = VldDemografiQuery::create(null, $criteria)
                    ->filterByDemografiRelatedByDemografiId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldDemografisRelatedByDemografiIdPartial && count($collVldDemografisRelatedByDemografiId)) {
                      $this->initVldDemografisRelatedByDemografiId(false);

                      foreach($collVldDemografisRelatedByDemografiId as $obj) {
                        if (false == $this->collVldDemografisRelatedByDemografiId->contains($obj)) {
                          $this->collVldDemografisRelatedByDemografiId->append($obj);
                        }
                      }

                      $this->collVldDemografisRelatedByDemografiIdPartial = true;
                    }

                    $collVldDemografisRelatedByDemografiId->getInternalIterator()->rewind();
                    return $collVldDemografisRelatedByDemografiId;
                }

                if($partial && $this->collVldDemografisRelatedByDemografiId) {
                    foreach($this->collVldDemografisRelatedByDemografiId as $obj) {
                        if($obj->isNew()) {
                            $collVldDemografisRelatedByDemografiId[] = $obj;
                        }
                    }
                }

                $this->collVldDemografisRelatedByDemografiId = $collVldDemografisRelatedByDemografiId;
                $this->collVldDemografisRelatedByDemografiIdPartial = false;
            }
        }

        return $this->collVldDemografisRelatedByDemografiId;
    }

    /**
     * Sets a collection of VldDemografiRelatedByDemografiId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldDemografisRelatedByDemografiId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Demografi The current object (for fluent API support)
     */
    public function setVldDemografisRelatedByDemografiId(PropelCollection $vldDemografisRelatedByDemografiId, PropelPDO $con = null)
    {
        $vldDemografisRelatedByDemografiIdToDelete = $this->getVldDemografisRelatedByDemografiId(new Criteria(), $con)->diff($vldDemografisRelatedByDemografiId);

        $this->vldDemografisRelatedByDemografiIdScheduledForDeletion = unserialize(serialize($vldDemografisRelatedByDemografiIdToDelete));

        foreach ($vldDemografisRelatedByDemografiIdToDelete as $vldDemografiRelatedByDemografiIdRemoved) {
            $vldDemografiRelatedByDemografiIdRemoved->setDemografiRelatedByDemografiId(null);
        }

        $this->collVldDemografisRelatedByDemografiId = null;
        foreach ($vldDemografisRelatedByDemografiId as $vldDemografiRelatedByDemografiId) {
            $this->addVldDemografiRelatedByDemografiId($vldDemografiRelatedByDemografiId);
        }

        $this->collVldDemografisRelatedByDemografiId = $vldDemografisRelatedByDemografiId;
        $this->collVldDemografisRelatedByDemografiIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldDemografi objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldDemografi objects.
     * @throws PropelException
     */
    public function countVldDemografisRelatedByDemografiId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldDemografisRelatedByDemografiIdPartial && !$this->isNew();
        if (null === $this->collVldDemografisRelatedByDemografiId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldDemografisRelatedByDemografiId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldDemografisRelatedByDemografiId());
            }
            $query = VldDemografiQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByDemografiRelatedByDemografiId($this)
                ->count($con);
        }

        return count($this->collVldDemografisRelatedByDemografiId);
    }

    /**
     * Method called to associate a VldDemografi object to this object
     * through the VldDemografi foreign key attribute.
     *
     * @param    VldDemografi $l VldDemografi
     * @return Demografi The current object (for fluent API support)
     */
    public function addVldDemografiRelatedByDemografiId(VldDemografi $l)
    {
        if ($this->collVldDemografisRelatedByDemografiId === null) {
            $this->initVldDemografisRelatedByDemografiId();
            $this->collVldDemografisRelatedByDemografiIdPartial = true;
        }
        if (!in_array($l, $this->collVldDemografisRelatedByDemografiId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldDemografiRelatedByDemografiId($l);
        }

        return $this;
    }

    /**
     * @param	VldDemografiRelatedByDemografiId $vldDemografiRelatedByDemografiId The vldDemografiRelatedByDemografiId object to add.
     */
    protected function doAddVldDemografiRelatedByDemografiId($vldDemografiRelatedByDemografiId)
    {
        $this->collVldDemografisRelatedByDemografiId[]= $vldDemografiRelatedByDemografiId;
        $vldDemografiRelatedByDemografiId->setDemografiRelatedByDemografiId($this);
    }

    /**
     * @param	VldDemografiRelatedByDemografiId $vldDemografiRelatedByDemografiId The vldDemografiRelatedByDemografiId object to remove.
     * @return Demografi The current object (for fluent API support)
     */
    public function removeVldDemografiRelatedByDemografiId($vldDemografiRelatedByDemografiId)
    {
        if ($this->getVldDemografisRelatedByDemografiId()->contains($vldDemografiRelatedByDemografiId)) {
            $this->collVldDemografisRelatedByDemografiId->remove($this->collVldDemografisRelatedByDemografiId->search($vldDemografiRelatedByDemografiId));
            if (null === $this->vldDemografisRelatedByDemografiIdScheduledForDeletion) {
                $this->vldDemografisRelatedByDemografiIdScheduledForDeletion = clone $this->collVldDemografisRelatedByDemografiId;
                $this->vldDemografisRelatedByDemografiIdScheduledForDeletion->clear();
            }
            $this->vldDemografisRelatedByDemografiIdScheduledForDeletion[]= clone $vldDemografiRelatedByDemografiId;
            $vldDemografiRelatedByDemografiId->setDemografiRelatedByDemografiId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Demografi is new, it will return
     * an empty collection; or if this Demografi has previously
     * been saved, it will retrieve related VldDemografisRelatedByDemografiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Demografi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldDemografi[] List of VldDemografi objects
     */
    public function getVldDemografisRelatedByDemografiIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldDemografiQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldDemografisRelatedByDemografiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Demografi is new, it will return
     * an empty collection; or if this Demografi has previously
     * been saved, it will retrieve related VldDemografisRelatedByDemografiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Demografi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldDemografi[] List of VldDemografi objects
     */
    public function getVldDemografisRelatedByDemografiIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldDemografiQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldDemografisRelatedByDemografiId($query, $con);
    }

    /**
     * Clears out the collVldDemografisRelatedByDemografiId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Demografi The current object (for fluent API support)
     * @see        addVldDemografisRelatedByDemografiId()
     */
    public function clearVldDemografisRelatedByDemografiId()
    {
        $this->collVldDemografisRelatedByDemografiId = null; // important to set this to null since that means it is uninitialized
        $this->collVldDemografisRelatedByDemografiIdPartial = null;

        return $this;
    }

    /**
     * reset is the collVldDemografisRelatedByDemografiId collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldDemografisRelatedByDemografiId($v = true)
    {
        $this->collVldDemografisRelatedByDemografiIdPartial = $v;
    }

    /**
     * Initializes the collVldDemografisRelatedByDemografiId collection.
     *
     * By default this just sets the collVldDemografisRelatedByDemografiId collection to an empty array (like clearcollVldDemografisRelatedByDemografiId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldDemografisRelatedByDemografiId($overrideExisting = true)
    {
        if (null !== $this->collVldDemografisRelatedByDemografiId && !$overrideExisting) {
            return;
        }
        $this->collVldDemografisRelatedByDemografiId = new PropelObjectCollection();
        $this->collVldDemografisRelatedByDemografiId->setModel('VldDemografi');
    }

    /**
     * Gets an array of VldDemografi objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Demografi is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldDemografi[] List of VldDemografi objects
     * @throws PropelException
     */
    public function getVldDemografisRelatedByDemografiId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldDemografisRelatedByDemografiIdPartial && !$this->isNew();
        if (null === $this->collVldDemografisRelatedByDemografiId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldDemografisRelatedByDemografiId) {
                // return empty collection
                $this->initVldDemografisRelatedByDemografiId();
            } else {
                $collVldDemografisRelatedByDemografiId = VldDemografiQuery::create(null, $criteria)
                    ->filterByDemografiRelatedByDemografiId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldDemografisRelatedByDemografiIdPartial && count($collVldDemografisRelatedByDemografiId)) {
                      $this->initVldDemografisRelatedByDemografiId(false);

                      foreach($collVldDemografisRelatedByDemografiId as $obj) {
                        if (false == $this->collVldDemografisRelatedByDemografiId->contains($obj)) {
                          $this->collVldDemografisRelatedByDemografiId->append($obj);
                        }
                      }

                      $this->collVldDemografisRelatedByDemografiIdPartial = true;
                    }

                    $collVldDemografisRelatedByDemografiId->getInternalIterator()->rewind();
                    return $collVldDemografisRelatedByDemografiId;
                }

                if($partial && $this->collVldDemografisRelatedByDemografiId) {
                    foreach($this->collVldDemografisRelatedByDemografiId as $obj) {
                        if($obj->isNew()) {
                            $collVldDemografisRelatedByDemografiId[] = $obj;
                        }
                    }
                }

                $this->collVldDemografisRelatedByDemografiId = $collVldDemografisRelatedByDemografiId;
                $this->collVldDemografisRelatedByDemografiIdPartial = false;
            }
        }

        return $this->collVldDemografisRelatedByDemografiId;
    }

    /**
     * Sets a collection of VldDemografiRelatedByDemografiId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldDemografisRelatedByDemografiId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Demografi The current object (for fluent API support)
     */
    public function setVldDemografisRelatedByDemografiId(PropelCollection $vldDemografisRelatedByDemografiId, PropelPDO $con = null)
    {
        $vldDemografisRelatedByDemografiIdToDelete = $this->getVldDemografisRelatedByDemografiId(new Criteria(), $con)->diff($vldDemografisRelatedByDemografiId);

        $this->vldDemografisRelatedByDemografiIdScheduledForDeletion = unserialize(serialize($vldDemografisRelatedByDemografiIdToDelete));

        foreach ($vldDemografisRelatedByDemografiIdToDelete as $vldDemografiRelatedByDemografiIdRemoved) {
            $vldDemografiRelatedByDemografiIdRemoved->setDemografiRelatedByDemografiId(null);
        }

        $this->collVldDemografisRelatedByDemografiId = null;
        foreach ($vldDemografisRelatedByDemografiId as $vldDemografiRelatedByDemografiId) {
            $this->addVldDemografiRelatedByDemografiId($vldDemografiRelatedByDemografiId);
        }

        $this->collVldDemografisRelatedByDemografiId = $vldDemografisRelatedByDemografiId;
        $this->collVldDemografisRelatedByDemografiIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldDemografi objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldDemografi objects.
     * @throws PropelException
     */
    public function countVldDemografisRelatedByDemografiId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldDemografisRelatedByDemografiIdPartial && !$this->isNew();
        if (null === $this->collVldDemografisRelatedByDemografiId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldDemografisRelatedByDemografiId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldDemografisRelatedByDemografiId());
            }
            $query = VldDemografiQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByDemografiRelatedByDemografiId($this)
                ->count($con);
        }

        return count($this->collVldDemografisRelatedByDemografiId);
    }

    /**
     * Method called to associate a VldDemografi object to this object
     * through the VldDemografi foreign key attribute.
     *
     * @param    VldDemografi $l VldDemografi
     * @return Demografi The current object (for fluent API support)
     */
    public function addVldDemografiRelatedByDemografiId(VldDemografi $l)
    {
        if ($this->collVldDemografisRelatedByDemografiId === null) {
            $this->initVldDemografisRelatedByDemografiId();
            $this->collVldDemografisRelatedByDemografiIdPartial = true;
        }
        if (!in_array($l, $this->collVldDemografisRelatedByDemografiId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldDemografiRelatedByDemografiId($l);
        }

        return $this;
    }

    /**
     * @param	VldDemografiRelatedByDemografiId $vldDemografiRelatedByDemografiId The vldDemografiRelatedByDemografiId object to add.
     */
    protected function doAddVldDemografiRelatedByDemografiId($vldDemografiRelatedByDemografiId)
    {
        $this->collVldDemografisRelatedByDemografiId[]= $vldDemografiRelatedByDemografiId;
        $vldDemografiRelatedByDemografiId->setDemografiRelatedByDemografiId($this);
    }

    /**
     * @param	VldDemografiRelatedByDemografiId $vldDemografiRelatedByDemografiId The vldDemografiRelatedByDemografiId object to remove.
     * @return Demografi The current object (for fluent API support)
     */
    public function removeVldDemografiRelatedByDemografiId($vldDemografiRelatedByDemografiId)
    {
        if ($this->getVldDemografisRelatedByDemografiId()->contains($vldDemografiRelatedByDemografiId)) {
            $this->collVldDemografisRelatedByDemografiId->remove($this->collVldDemografisRelatedByDemografiId->search($vldDemografiRelatedByDemografiId));
            if (null === $this->vldDemografisRelatedByDemografiIdScheduledForDeletion) {
                $this->vldDemografisRelatedByDemografiIdScheduledForDeletion = clone $this->collVldDemografisRelatedByDemografiId;
                $this->vldDemografisRelatedByDemografiIdScheduledForDeletion->clear();
            }
            $this->vldDemografisRelatedByDemografiIdScheduledForDeletion[]= clone $vldDemografiRelatedByDemografiId;
            $vldDemografiRelatedByDemografiId->setDemografiRelatedByDemografiId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Demografi is new, it will return
     * an empty collection; or if this Demografi has previously
     * been saved, it will retrieve related VldDemografisRelatedByDemografiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Demografi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldDemografi[] List of VldDemografi objects
     */
    public function getVldDemografisRelatedByDemografiIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldDemografiQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldDemografisRelatedByDemografiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Demografi is new, it will return
     * an empty collection; or if this Demografi has previously
     * been saved, it will retrieve related VldDemografisRelatedByDemografiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Demografi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldDemografi[] List of VldDemografi objects
     */
    public function getVldDemografisRelatedByDemografiIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldDemografiQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldDemografisRelatedByDemografiId($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->demografi_id = null;
        $this->kode_wilayah = null;
        $this->tahun_ajaran_id = null;
        $this->usia_5 = null;
        $this->usia_7 = null;
        $this->usia_13 = null;
        $this->usia_16 = null;
        $this->usia_18 = null;
        $this->jumlah_penduduk = null;
        $this->last_update = null;
        $this->soft_delete = null;
        $this->last_sync = null;
        $this->updater_id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collVldDemografisRelatedByDemografiId) {
                foreach ($this->collVldDemografisRelatedByDemografiId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldDemografisRelatedByDemografiId) {
                foreach ($this->collVldDemografisRelatedByDemografiId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aMstWilayahRelatedByKodeWilayah instanceof Persistent) {
              $this->aMstWilayahRelatedByKodeWilayah->clearAllReferences($deep);
            }
            if ($this->aMstWilayahRelatedByKodeWilayah instanceof Persistent) {
              $this->aMstWilayahRelatedByKodeWilayah->clearAllReferences($deep);
            }
            if ($this->aTahunAjaranRelatedByTahunAjaranId instanceof Persistent) {
              $this->aTahunAjaranRelatedByTahunAjaranId->clearAllReferences($deep);
            }
            if ($this->aTahunAjaranRelatedByTahunAjaranId instanceof Persistent) {
              $this->aTahunAjaranRelatedByTahunAjaranId->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collVldDemografisRelatedByDemografiId instanceof PropelCollection) {
            $this->collVldDemografisRelatedByDemografiId->clearIterator();
        }
        $this->collVldDemografisRelatedByDemografiId = null;
        if ($this->collVldDemografisRelatedByDemografiId instanceof PropelCollection) {
            $this->collVldDemografisRelatedByDemografiId->clearIterator();
        }
        $this->collVldDemografisRelatedByDemografiId = null;
        $this->aMstWilayahRelatedByKodeWilayah = null;
        $this->aMstWilayahRelatedByKodeWilayah = null;
        $this->aTahunAjaranRelatedByTahunAjaranId = null;
        $this->aTahunAjaranRelatedByTahunAjaranId = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(DemografiPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
