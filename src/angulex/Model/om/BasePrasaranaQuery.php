<?php

namespace angulex\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\BukuAlat;
use angulex\Model\JenisPrasarana;
use angulex\Model\Prasarana;
use angulex\Model\PrasaranaLongitudinal;
use angulex\Model\PrasaranaPeer;
use angulex\Model\PrasaranaQuery;
use angulex\Model\RombonganBelajar;
use angulex\Model\Sarana;
use angulex\Model\Sekolah;
use angulex\Model\StatusKepemilikanSarpras;
use angulex\Model\VldPrasarana;

/**
 * Base class that represents a query for the 'prasarana' table.
 *
 * 
 *
 * @method PrasaranaQuery orderByPrasaranaId($order = Criteria::ASC) Order by the prasarana_id column
 * @method PrasaranaQuery orderByJenisPrasaranaId($order = Criteria::ASC) Order by the jenis_prasarana_id column
 * @method PrasaranaQuery orderBySekolahId($order = Criteria::ASC) Order by the sekolah_id column
 * @method PrasaranaQuery orderByKepemilikanSarprasId($order = Criteria::ASC) Order by the kepemilikan_sarpras_id column
 * @method PrasaranaQuery orderByNama($order = Criteria::ASC) Order by the nama column
 * @method PrasaranaQuery orderByPanjang($order = Criteria::ASC) Order by the panjang column
 * @method PrasaranaQuery orderByLebar($order = Criteria::ASC) Order by the lebar column
 * @method PrasaranaQuery orderByLastUpdate($order = Criteria::ASC) Order by the Last_update column
 * @method PrasaranaQuery orderBySoftDelete($order = Criteria::ASC) Order by the Soft_delete column
 * @method PrasaranaQuery orderByLastSync($order = Criteria::ASC) Order by the last_sync column
 * @method PrasaranaQuery orderByUpdaterId($order = Criteria::ASC) Order by the Updater_ID column
 *
 * @method PrasaranaQuery groupByPrasaranaId() Group by the prasarana_id column
 * @method PrasaranaQuery groupByJenisPrasaranaId() Group by the jenis_prasarana_id column
 * @method PrasaranaQuery groupBySekolahId() Group by the sekolah_id column
 * @method PrasaranaQuery groupByKepemilikanSarprasId() Group by the kepemilikan_sarpras_id column
 * @method PrasaranaQuery groupByNama() Group by the nama column
 * @method PrasaranaQuery groupByPanjang() Group by the panjang column
 * @method PrasaranaQuery groupByLebar() Group by the lebar column
 * @method PrasaranaQuery groupByLastUpdate() Group by the Last_update column
 * @method PrasaranaQuery groupBySoftDelete() Group by the Soft_delete column
 * @method PrasaranaQuery groupByLastSync() Group by the last_sync column
 * @method PrasaranaQuery groupByUpdaterId() Group by the Updater_ID column
 *
 * @method PrasaranaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method PrasaranaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method PrasaranaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method PrasaranaQuery leftJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method PrasaranaQuery rightJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method PrasaranaQuery innerJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the SekolahRelatedBySekolahId relation
 *
 * @method PrasaranaQuery leftJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method PrasaranaQuery rightJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method PrasaranaQuery innerJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the SekolahRelatedBySekolahId relation
 *
 * @method PrasaranaQuery leftJoinJenisPrasaranaRelatedByJenisPrasaranaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the JenisPrasaranaRelatedByJenisPrasaranaId relation
 * @method PrasaranaQuery rightJoinJenisPrasaranaRelatedByJenisPrasaranaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JenisPrasaranaRelatedByJenisPrasaranaId relation
 * @method PrasaranaQuery innerJoinJenisPrasaranaRelatedByJenisPrasaranaId($relationAlias = null) Adds a INNER JOIN clause to the query using the JenisPrasaranaRelatedByJenisPrasaranaId relation
 *
 * @method PrasaranaQuery leftJoinJenisPrasaranaRelatedByJenisPrasaranaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the JenisPrasaranaRelatedByJenisPrasaranaId relation
 * @method PrasaranaQuery rightJoinJenisPrasaranaRelatedByJenisPrasaranaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JenisPrasaranaRelatedByJenisPrasaranaId relation
 * @method PrasaranaQuery innerJoinJenisPrasaranaRelatedByJenisPrasaranaId($relationAlias = null) Adds a INNER JOIN clause to the query using the JenisPrasaranaRelatedByJenisPrasaranaId relation
 *
 * @method PrasaranaQuery leftJoinStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($relationAlias = null) Adds a LEFT JOIN clause to the query using the StatusKepemilikanSarprasRelatedByKepemilikanSarprasId relation
 * @method PrasaranaQuery rightJoinStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the StatusKepemilikanSarprasRelatedByKepemilikanSarprasId relation
 * @method PrasaranaQuery innerJoinStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($relationAlias = null) Adds a INNER JOIN clause to the query using the StatusKepemilikanSarprasRelatedByKepemilikanSarprasId relation
 *
 * @method PrasaranaQuery leftJoinStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($relationAlias = null) Adds a LEFT JOIN clause to the query using the StatusKepemilikanSarprasRelatedByKepemilikanSarprasId relation
 * @method PrasaranaQuery rightJoinStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the StatusKepemilikanSarprasRelatedByKepemilikanSarprasId relation
 * @method PrasaranaQuery innerJoinStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($relationAlias = null) Adds a INNER JOIN clause to the query using the StatusKepemilikanSarprasRelatedByKepemilikanSarprasId relation
 *
 * @method PrasaranaQuery leftJoinBukuAlatRelatedByPrasaranaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the BukuAlatRelatedByPrasaranaId relation
 * @method PrasaranaQuery rightJoinBukuAlatRelatedByPrasaranaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BukuAlatRelatedByPrasaranaId relation
 * @method PrasaranaQuery innerJoinBukuAlatRelatedByPrasaranaId($relationAlias = null) Adds a INNER JOIN clause to the query using the BukuAlatRelatedByPrasaranaId relation
 *
 * @method PrasaranaQuery leftJoinBukuAlatRelatedByPrasaranaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the BukuAlatRelatedByPrasaranaId relation
 * @method PrasaranaQuery rightJoinBukuAlatRelatedByPrasaranaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BukuAlatRelatedByPrasaranaId relation
 * @method PrasaranaQuery innerJoinBukuAlatRelatedByPrasaranaId($relationAlias = null) Adds a INNER JOIN clause to the query using the BukuAlatRelatedByPrasaranaId relation
 *
 * @method PrasaranaQuery leftJoinVldPrasaranaRelatedByPrasaranaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldPrasaranaRelatedByPrasaranaId relation
 * @method PrasaranaQuery rightJoinVldPrasaranaRelatedByPrasaranaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldPrasaranaRelatedByPrasaranaId relation
 * @method PrasaranaQuery innerJoinVldPrasaranaRelatedByPrasaranaId($relationAlias = null) Adds a INNER JOIN clause to the query using the VldPrasaranaRelatedByPrasaranaId relation
 *
 * @method PrasaranaQuery leftJoinVldPrasaranaRelatedByPrasaranaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldPrasaranaRelatedByPrasaranaId relation
 * @method PrasaranaQuery rightJoinVldPrasaranaRelatedByPrasaranaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldPrasaranaRelatedByPrasaranaId relation
 * @method PrasaranaQuery innerJoinVldPrasaranaRelatedByPrasaranaId($relationAlias = null) Adds a INNER JOIN clause to the query using the VldPrasaranaRelatedByPrasaranaId relation
 *
 * @method PrasaranaQuery leftJoinSaranaRelatedByPrasaranaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SaranaRelatedByPrasaranaId relation
 * @method PrasaranaQuery rightJoinSaranaRelatedByPrasaranaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SaranaRelatedByPrasaranaId relation
 * @method PrasaranaQuery innerJoinSaranaRelatedByPrasaranaId($relationAlias = null) Adds a INNER JOIN clause to the query using the SaranaRelatedByPrasaranaId relation
 *
 * @method PrasaranaQuery leftJoinSaranaRelatedByPrasaranaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SaranaRelatedByPrasaranaId relation
 * @method PrasaranaQuery rightJoinSaranaRelatedByPrasaranaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SaranaRelatedByPrasaranaId relation
 * @method PrasaranaQuery innerJoinSaranaRelatedByPrasaranaId($relationAlias = null) Adds a INNER JOIN clause to the query using the SaranaRelatedByPrasaranaId relation
 *
 * @method PrasaranaQuery leftJoinPrasaranaLongitudinalRelatedByPrasaranaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PrasaranaLongitudinalRelatedByPrasaranaId relation
 * @method PrasaranaQuery rightJoinPrasaranaLongitudinalRelatedByPrasaranaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PrasaranaLongitudinalRelatedByPrasaranaId relation
 * @method PrasaranaQuery innerJoinPrasaranaLongitudinalRelatedByPrasaranaId($relationAlias = null) Adds a INNER JOIN clause to the query using the PrasaranaLongitudinalRelatedByPrasaranaId relation
 *
 * @method PrasaranaQuery leftJoinPrasaranaLongitudinalRelatedByPrasaranaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PrasaranaLongitudinalRelatedByPrasaranaId relation
 * @method PrasaranaQuery rightJoinPrasaranaLongitudinalRelatedByPrasaranaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PrasaranaLongitudinalRelatedByPrasaranaId relation
 * @method PrasaranaQuery innerJoinPrasaranaLongitudinalRelatedByPrasaranaId($relationAlias = null) Adds a INNER JOIN clause to the query using the PrasaranaLongitudinalRelatedByPrasaranaId relation
 *
 * @method PrasaranaQuery leftJoinRombonganBelajarRelatedByPrasaranaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the RombonganBelajarRelatedByPrasaranaId relation
 * @method PrasaranaQuery rightJoinRombonganBelajarRelatedByPrasaranaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RombonganBelajarRelatedByPrasaranaId relation
 * @method PrasaranaQuery innerJoinRombonganBelajarRelatedByPrasaranaId($relationAlias = null) Adds a INNER JOIN clause to the query using the RombonganBelajarRelatedByPrasaranaId relation
 *
 * @method PrasaranaQuery leftJoinRombonganBelajarRelatedByPrasaranaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the RombonganBelajarRelatedByPrasaranaId relation
 * @method PrasaranaQuery rightJoinRombonganBelajarRelatedByPrasaranaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RombonganBelajarRelatedByPrasaranaId relation
 * @method PrasaranaQuery innerJoinRombonganBelajarRelatedByPrasaranaId($relationAlias = null) Adds a INNER JOIN clause to the query using the RombonganBelajarRelatedByPrasaranaId relation
 *
 * @method Prasarana findOne(PropelPDO $con = null) Return the first Prasarana matching the query
 * @method Prasarana findOneOrCreate(PropelPDO $con = null) Return the first Prasarana matching the query, or a new Prasarana object populated from the query conditions when no match is found
 *
 * @method Prasarana findOneByJenisPrasaranaId(int $jenis_prasarana_id) Return the first Prasarana filtered by the jenis_prasarana_id column
 * @method Prasarana findOneBySekolahId(string $sekolah_id) Return the first Prasarana filtered by the sekolah_id column
 * @method Prasarana findOneByKepemilikanSarprasId(string $kepemilikan_sarpras_id) Return the first Prasarana filtered by the kepemilikan_sarpras_id column
 * @method Prasarana findOneByNama(string $nama) Return the first Prasarana filtered by the nama column
 * @method Prasarana findOneByPanjang(double $panjang) Return the first Prasarana filtered by the panjang column
 * @method Prasarana findOneByLebar(double $lebar) Return the first Prasarana filtered by the lebar column
 * @method Prasarana findOneByLastUpdate(string $Last_update) Return the first Prasarana filtered by the Last_update column
 * @method Prasarana findOneBySoftDelete(string $Soft_delete) Return the first Prasarana filtered by the Soft_delete column
 * @method Prasarana findOneByLastSync(string $last_sync) Return the first Prasarana filtered by the last_sync column
 * @method Prasarana findOneByUpdaterId(string $Updater_ID) Return the first Prasarana filtered by the Updater_ID column
 *
 * @method array findByPrasaranaId(string $prasarana_id) Return Prasarana objects filtered by the prasarana_id column
 * @method array findByJenisPrasaranaId(int $jenis_prasarana_id) Return Prasarana objects filtered by the jenis_prasarana_id column
 * @method array findBySekolahId(string $sekolah_id) Return Prasarana objects filtered by the sekolah_id column
 * @method array findByKepemilikanSarprasId(string $kepemilikan_sarpras_id) Return Prasarana objects filtered by the kepemilikan_sarpras_id column
 * @method array findByNama(string $nama) Return Prasarana objects filtered by the nama column
 * @method array findByPanjang(double $panjang) Return Prasarana objects filtered by the panjang column
 * @method array findByLebar(double $lebar) Return Prasarana objects filtered by the lebar column
 * @method array findByLastUpdate(string $Last_update) Return Prasarana objects filtered by the Last_update column
 * @method array findBySoftDelete(string $Soft_delete) Return Prasarana objects filtered by the Soft_delete column
 * @method array findByLastSync(string $last_sync) Return Prasarana objects filtered by the last_sync column
 * @method array findByUpdaterId(string $Updater_ID) Return Prasarana objects filtered by the Updater_ID column
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BasePrasaranaQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BasePrasaranaQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Dapodikmen', $modelName = 'angulex\\Model\\Prasarana', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new PrasaranaQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   PrasaranaQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return PrasaranaQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof PrasaranaQuery) {
            return $criteria;
        }
        $query = new PrasaranaQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Prasarana|Prasarana[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = PrasaranaPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(PrasaranaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Prasarana A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByPrasaranaId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Prasarana A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [prasarana_id], [jenis_prasarana_id], [sekolah_id], [kepemilikan_sarpras_id], [nama], [panjang], [lebar], [Last_update], [Soft_delete], [last_sync], [Updater_ID] FROM [prasarana] WHERE [prasarana_id] = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Prasarana();
            $obj->hydrate($row);
            PrasaranaPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Prasarana|Prasarana[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Prasarana[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PrasaranaPeer::PRASARANA_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PrasaranaPeer::PRASARANA_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the prasarana_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPrasaranaId('fooValue');   // WHERE prasarana_id = 'fooValue'
     * $query->filterByPrasaranaId('%fooValue%'); // WHERE prasarana_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $prasaranaId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByPrasaranaId($prasaranaId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($prasaranaId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $prasaranaId)) {
                $prasaranaId = str_replace('*', '%', $prasaranaId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::PRASARANA_ID, $prasaranaId, $comparison);
    }

    /**
     * Filter the query on the jenis_prasarana_id column
     *
     * Example usage:
     * <code>
     * $query->filterByJenisPrasaranaId(1234); // WHERE jenis_prasarana_id = 1234
     * $query->filterByJenisPrasaranaId(array(12, 34)); // WHERE jenis_prasarana_id IN (12, 34)
     * $query->filterByJenisPrasaranaId(array('min' => 12)); // WHERE jenis_prasarana_id >= 12
     * $query->filterByJenisPrasaranaId(array('max' => 12)); // WHERE jenis_prasarana_id <= 12
     * </code>
     *
     * @see       filterByJenisPrasaranaRelatedByJenisPrasaranaId()
     *
     * @see       filterByJenisPrasaranaRelatedByJenisPrasaranaId()
     *
     * @param     mixed $jenisPrasaranaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByJenisPrasaranaId($jenisPrasaranaId = null, $comparison = null)
    {
        if (is_array($jenisPrasaranaId)) {
            $useMinMax = false;
            if (isset($jenisPrasaranaId['min'])) {
                $this->addUsingAlias(PrasaranaPeer::JENIS_PRASARANA_ID, $jenisPrasaranaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jenisPrasaranaId['max'])) {
                $this->addUsingAlias(PrasaranaPeer::JENIS_PRASARANA_ID, $jenisPrasaranaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::JENIS_PRASARANA_ID, $jenisPrasaranaId, $comparison);
    }

    /**
     * Filter the query on the sekolah_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySekolahId('fooValue');   // WHERE sekolah_id = 'fooValue'
     * $query->filterBySekolahId('%fooValue%'); // WHERE sekolah_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sekolahId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterBySekolahId($sekolahId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sekolahId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $sekolahId)) {
                $sekolahId = str_replace('*', '%', $sekolahId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::SEKOLAH_ID, $sekolahId, $comparison);
    }

    /**
     * Filter the query on the kepemilikan_sarpras_id column
     *
     * Example usage:
     * <code>
     * $query->filterByKepemilikanSarprasId(1234); // WHERE kepemilikan_sarpras_id = 1234
     * $query->filterByKepemilikanSarprasId(array(12, 34)); // WHERE kepemilikan_sarpras_id IN (12, 34)
     * $query->filterByKepemilikanSarprasId(array('min' => 12)); // WHERE kepemilikan_sarpras_id >= 12
     * $query->filterByKepemilikanSarprasId(array('max' => 12)); // WHERE kepemilikan_sarpras_id <= 12
     * </code>
     *
     * @see       filterByStatusKepemilikanSarprasRelatedByKepemilikanSarprasId()
     *
     * @see       filterByStatusKepemilikanSarprasRelatedByKepemilikanSarprasId()
     *
     * @param     mixed $kepemilikanSarprasId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByKepemilikanSarprasId($kepemilikanSarprasId = null, $comparison = null)
    {
        if (is_array($kepemilikanSarprasId)) {
            $useMinMax = false;
            if (isset($kepemilikanSarprasId['min'])) {
                $this->addUsingAlias(PrasaranaPeer::KEPEMILIKAN_SARPRAS_ID, $kepemilikanSarprasId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($kepemilikanSarprasId['max'])) {
                $this->addUsingAlias(PrasaranaPeer::KEPEMILIKAN_SARPRAS_ID, $kepemilikanSarprasId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::KEPEMILIKAN_SARPRAS_ID, $kepemilikanSarprasId, $comparison);
    }

    /**
     * Filter the query on the nama column
     *
     * Example usage:
     * <code>
     * $query->filterByNama('fooValue');   // WHERE nama = 'fooValue'
     * $query->filterByNama('%fooValue%'); // WHERE nama LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nama The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByNama($nama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nama)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nama)) {
                $nama = str_replace('*', '%', $nama);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::NAMA, $nama, $comparison);
    }

    /**
     * Filter the query on the panjang column
     *
     * Example usage:
     * <code>
     * $query->filterByPanjang(1234); // WHERE panjang = 1234
     * $query->filterByPanjang(array(12, 34)); // WHERE panjang IN (12, 34)
     * $query->filterByPanjang(array('min' => 12)); // WHERE panjang >= 12
     * $query->filterByPanjang(array('max' => 12)); // WHERE panjang <= 12
     * </code>
     *
     * @param     mixed $panjang The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByPanjang($panjang = null, $comparison = null)
    {
        if (is_array($panjang)) {
            $useMinMax = false;
            if (isset($panjang['min'])) {
                $this->addUsingAlias(PrasaranaPeer::PANJANG, $panjang['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($panjang['max'])) {
                $this->addUsingAlias(PrasaranaPeer::PANJANG, $panjang['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::PANJANG, $panjang, $comparison);
    }

    /**
     * Filter the query on the lebar column
     *
     * Example usage:
     * <code>
     * $query->filterByLebar(1234); // WHERE lebar = 1234
     * $query->filterByLebar(array(12, 34)); // WHERE lebar IN (12, 34)
     * $query->filterByLebar(array('min' => 12)); // WHERE lebar >= 12
     * $query->filterByLebar(array('max' => 12)); // WHERE lebar <= 12
     * </code>
     *
     * @param     mixed $lebar The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByLebar($lebar = null, $comparison = null)
    {
        if (is_array($lebar)) {
            $useMinMax = false;
            if (isset($lebar['min'])) {
                $this->addUsingAlias(PrasaranaPeer::LEBAR, $lebar['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lebar['max'])) {
                $this->addUsingAlias(PrasaranaPeer::LEBAR, $lebar['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::LEBAR, $lebar, $comparison);
    }

    /**
     * Filter the query on the Last_update column
     *
     * Example usage:
     * <code>
     * $query->filterByLastUpdate('2011-03-14'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate('now'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate(array('max' => 'yesterday')); // WHERE Last_update > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastUpdate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByLastUpdate($lastUpdate = null, $comparison = null)
    {
        if (is_array($lastUpdate)) {
            $useMinMax = false;
            if (isset($lastUpdate['min'])) {
                $this->addUsingAlias(PrasaranaPeer::LAST_UPDATE, $lastUpdate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastUpdate['max'])) {
                $this->addUsingAlias(PrasaranaPeer::LAST_UPDATE, $lastUpdate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::LAST_UPDATE, $lastUpdate, $comparison);
    }

    /**
     * Filter the query on the Soft_delete column
     *
     * Example usage:
     * <code>
     * $query->filterBySoftDelete(1234); // WHERE Soft_delete = 1234
     * $query->filterBySoftDelete(array(12, 34)); // WHERE Soft_delete IN (12, 34)
     * $query->filterBySoftDelete(array('min' => 12)); // WHERE Soft_delete >= 12
     * $query->filterBySoftDelete(array('max' => 12)); // WHERE Soft_delete <= 12
     * </code>
     *
     * @param     mixed $softDelete The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterBySoftDelete($softDelete = null, $comparison = null)
    {
        if (is_array($softDelete)) {
            $useMinMax = false;
            if (isset($softDelete['min'])) {
                $this->addUsingAlias(PrasaranaPeer::SOFT_DELETE, $softDelete['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($softDelete['max'])) {
                $this->addUsingAlias(PrasaranaPeer::SOFT_DELETE, $softDelete['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::SOFT_DELETE, $softDelete, $comparison);
    }

    /**
     * Filter the query on the last_sync column
     *
     * Example usage:
     * <code>
     * $query->filterByLastSync('2011-03-14'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync('now'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync(array('max' => 'yesterday')); // WHERE last_sync > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastSync The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByLastSync($lastSync = null, $comparison = null)
    {
        if (is_array($lastSync)) {
            $useMinMax = false;
            if (isset($lastSync['min'])) {
                $this->addUsingAlias(PrasaranaPeer::LAST_SYNC, $lastSync['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastSync['max'])) {
                $this->addUsingAlias(PrasaranaPeer::LAST_SYNC, $lastSync['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::LAST_SYNC, $lastSync, $comparison);
    }

    /**
     * Filter the query on the Updater_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdaterId('fooValue');   // WHERE Updater_ID = 'fooValue'
     * $query->filterByUpdaterId('%fooValue%'); // WHERE Updater_ID LIKE '%fooValue%'
     * </code>
     *
     * @param     string $updaterId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByUpdaterId($updaterId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($updaterId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $updaterId)) {
                $updaterId = str_replace('*', '%', $updaterId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::UPDATER_ID, $updaterId, $comparison);
    }

    /**
     * Filter the query by a related Sekolah object
     *
     * @param   Sekolah|PropelObjectCollection $sekolah The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PrasaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySekolahRelatedBySekolahId($sekolah, $comparison = null)
    {
        if ($sekolah instanceof Sekolah) {
            return $this
                ->addUsingAlias(PrasaranaPeer::SEKOLAH_ID, $sekolah->getSekolahId(), $comparison);
        } elseif ($sekolah instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PrasaranaPeer::SEKOLAH_ID, $sekolah->toKeyValue('PrimaryKey', 'SekolahId'), $comparison);
        } else {
            throw new PropelException('filterBySekolahRelatedBySekolahId() only accepts arguments of type Sekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SekolahRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function joinSekolahRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SekolahRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SekolahRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the SekolahRelatedBySekolahId relation Sekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SekolahQuery A secondary query class using the current class as primary query
     */
    public function useSekolahRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSekolahRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SekolahRelatedBySekolahId', '\angulex\Model\SekolahQuery');
    }

    /**
     * Filter the query by a related Sekolah object
     *
     * @param   Sekolah|PropelObjectCollection $sekolah The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PrasaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySekolahRelatedBySekolahId($sekolah, $comparison = null)
    {
        if ($sekolah instanceof Sekolah) {
            return $this
                ->addUsingAlias(PrasaranaPeer::SEKOLAH_ID, $sekolah->getSekolahId(), $comparison);
        } elseif ($sekolah instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PrasaranaPeer::SEKOLAH_ID, $sekolah->toKeyValue('PrimaryKey', 'SekolahId'), $comparison);
        } else {
            throw new PropelException('filterBySekolahRelatedBySekolahId() only accepts arguments of type Sekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SekolahRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function joinSekolahRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SekolahRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SekolahRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the SekolahRelatedBySekolahId relation Sekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SekolahQuery A secondary query class using the current class as primary query
     */
    public function useSekolahRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSekolahRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SekolahRelatedBySekolahId', '\angulex\Model\SekolahQuery');
    }

    /**
     * Filter the query by a related JenisPrasarana object
     *
     * @param   JenisPrasarana|PropelObjectCollection $jenisPrasarana The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PrasaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJenisPrasaranaRelatedByJenisPrasaranaId($jenisPrasarana, $comparison = null)
    {
        if ($jenisPrasarana instanceof JenisPrasarana) {
            return $this
                ->addUsingAlias(PrasaranaPeer::JENIS_PRASARANA_ID, $jenisPrasarana->getJenisPrasaranaId(), $comparison);
        } elseif ($jenisPrasarana instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PrasaranaPeer::JENIS_PRASARANA_ID, $jenisPrasarana->toKeyValue('PrimaryKey', 'JenisPrasaranaId'), $comparison);
        } else {
            throw new PropelException('filterByJenisPrasaranaRelatedByJenisPrasaranaId() only accepts arguments of type JenisPrasarana or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the JenisPrasaranaRelatedByJenisPrasaranaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function joinJenisPrasaranaRelatedByJenisPrasaranaId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('JenisPrasaranaRelatedByJenisPrasaranaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'JenisPrasaranaRelatedByJenisPrasaranaId');
        }

        return $this;
    }

    /**
     * Use the JenisPrasaranaRelatedByJenisPrasaranaId relation JenisPrasarana object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\JenisPrasaranaQuery A secondary query class using the current class as primary query
     */
    public function useJenisPrasaranaRelatedByJenisPrasaranaIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinJenisPrasaranaRelatedByJenisPrasaranaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'JenisPrasaranaRelatedByJenisPrasaranaId', '\angulex\Model\JenisPrasaranaQuery');
    }

    /**
     * Filter the query by a related JenisPrasarana object
     *
     * @param   JenisPrasarana|PropelObjectCollection $jenisPrasarana The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PrasaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJenisPrasaranaRelatedByJenisPrasaranaId($jenisPrasarana, $comparison = null)
    {
        if ($jenisPrasarana instanceof JenisPrasarana) {
            return $this
                ->addUsingAlias(PrasaranaPeer::JENIS_PRASARANA_ID, $jenisPrasarana->getJenisPrasaranaId(), $comparison);
        } elseif ($jenisPrasarana instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PrasaranaPeer::JENIS_PRASARANA_ID, $jenisPrasarana->toKeyValue('PrimaryKey', 'JenisPrasaranaId'), $comparison);
        } else {
            throw new PropelException('filterByJenisPrasaranaRelatedByJenisPrasaranaId() only accepts arguments of type JenisPrasarana or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the JenisPrasaranaRelatedByJenisPrasaranaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function joinJenisPrasaranaRelatedByJenisPrasaranaId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('JenisPrasaranaRelatedByJenisPrasaranaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'JenisPrasaranaRelatedByJenisPrasaranaId');
        }

        return $this;
    }

    /**
     * Use the JenisPrasaranaRelatedByJenisPrasaranaId relation JenisPrasarana object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\JenisPrasaranaQuery A secondary query class using the current class as primary query
     */
    public function useJenisPrasaranaRelatedByJenisPrasaranaIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinJenisPrasaranaRelatedByJenisPrasaranaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'JenisPrasaranaRelatedByJenisPrasaranaId', '\angulex\Model\JenisPrasaranaQuery');
    }

    /**
     * Filter the query by a related StatusKepemilikanSarpras object
     *
     * @param   StatusKepemilikanSarpras|PropelObjectCollection $statusKepemilikanSarpras The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PrasaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($statusKepemilikanSarpras, $comparison = null)
    {
        if ($statusKepemilikanSarpras instanceof StatusKepemilikanSarpras) {
            return $this
                ->addUsingAlias(PrasaranaPeer::KEPEMILIKAN_SARPRAS_ID, $statusKepemilikanSarpras->getKepemilikanSarprasId(), $comparison);
        } elseif ($statusKepemilikanSarpras instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PrasaranaPeer::KEPEMILIKAN_SARPRAS_ID, $statusKepemilikanSarpras->toKeyValue('PrimaryKey', 'KepemilikanSarprasId'), $comparison);
        } else {
            throw new PropelException('filterByStatusKepemilikanSarprasRelatedByKepemilikanSarprasId() only accepts arguments of type StatusKepemilikanSarpras or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the StatusKepemilikanSarprasRelatedByKepemilikanSarprasId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function joinStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('StatusKepemilikanSarprasRelatedByKepemilikanSarprasId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'StatusKepemilikanSarprasRelatedByKepemilikanSarprasId');
        }

        return $this;
    }

    /**
     * Use the StatusKepemilikanSarprasRelatedByKepemilikanSarprasId relation StatusKepemilikanSarpras object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\StatusKepemilikanSarprasQuery A secondary query class using the current class as primary query
     */
    public function useStatusKepemilikanSarprasRelatedByKepemilikanSarprasIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'StatusKepemilikanSarprasRelatedByKepemilikanSarprasId', '\angulex\Model\StatusKepemilikanSarprasQuery');
    }

    /**
     * Filter the query by a related StatusKepemilikanSarpras object
     *
     * @param   StatusKepemilikanSarpras|PropelObjectCollection $statusKepemilikanSarpras The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PrasaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($statusKepemilikanSarpras, $comparison = null)
    {
        if ($statusKepemilikanSarpras instanceof StatusKepemilikanSarpras) {
            return $this
                ->addUsingAlias(PrasaranaPeer::KEPEMILIKAN_SARPRAS_ID, $statusKepemilikanSarpras->getKepemilikanSarprasId(), $comparison);
        } elseif ($statusKepemilikanSarpras instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PrasaranaPeer::KEPEMILIKAN_SARPRAS_ID, $statusKepemilikanSarpras->toKeyValue('PrimaryKey', 'KepemilikanSarprasId'), $comparison);
        } else {
            throw new PropelException('filterByStatusKepemilikanSarprasRelatedByKepemilikanSarprasId() only accepts arguments of type StatusKepemilikanSarpras or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the StatusKepemilikanSarprasRelatedByKepemilikanSarprasId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function joinStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('StatusKepemilikanSarprasRelatedByKepemilikanSarprasId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'StatusKepemilikanSarprasRelatedByKepemilikanSarprasId');
        }

        return $this;
    }

    /**
     * Use the StatusKepemilikanSarprasRelatedByKepemilikanSarprasId relation StatusKepemilikanSarpras object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\StatusKepemilikanSarprasQuery A secondary query class using the current class as primary query
     */
    public function useStatusKepemilikanSarprasRelatedByKepemilikanSarprasIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'StatusKepemilikanSarprasRelatedByKepemilikanSarprasId', '\angulex\Model\StatusKepemilikanSarprasQuery');
    }

    /**
     * Filter the query by a related BukuAlat object
     *
     * @param   BukuAlat|PropelObjectCollection $bukuAlat  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PrasaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByBukuAlatRelatedByPrasaranaId($bukuAlat, $comparison = null)
    {
        if ($bukuAlat instanceof BukuAlat) {
            return $this
                ->addUsingAlias(PrasaranaPeer::PRASARANA_ID, $bukuAlat->getPrasaranaId(), $comparison);
        } elseif ($bukuAlat instanceof PropelObjectCollection) {
            return $this
                ->useBukuAlatRelatedByPrasaranaIdQuery()
                ->filterByPrimaryKeys($bukuAlat->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBukuAlatRelatedByPrasaranaId() only accepts arguments of type BukuAlat or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BukuAlatRelatedByPrasaranaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function joinBukuAlatRelatedByPrasaranaId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BukuAlatRelatedByPrasaranaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BukuAlatRelatedByPrasaranaId');
        }

        return $this;
    }

    /**
     * Use the BukuAlatRelatedByPrasaranaId relation BukuAlat object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\BukuAlatQuery A secondary query class using the current class as primary query
     */
    public function useBukuAlatRelatedByPrasaranaIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinBukuAlatRelatedByPrasaranaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BukuAlatRelatedByPrasaranaId', '\angulex\Model\BukuAlatQuery');
    }

    /**
     * Filter the query by a related BukuAlat object
     *
     * @param   BukuAlat|PropelObjectCollection $bukuAlat  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PrasaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByBukuAlatRelatedByPrasaranaId($bukuAlat, $comparison = null)
    {
        if ($bukuAlat instanceof BukuAlat) {
            return $this
                ->addUsingAlias(PrasaranaPeer::PRASARANA_ID, $bukuAlat->getPrasaranaId(), $comparison);
        } elseif ($bukuAlat instanceof PropelObjectCollection) {
            return $this
                ->useBukuAlatRelatedByPrasaranaIdQuery()
                ->filterByPrimaryKeys($bukuAlat->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBukuAlatRelatedByPrasaranaId() only accepts arguments of type BukuAlat or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BukuAlatRelatedByPrasaranaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function joinBukuAlatRelatedByPrasaranaId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BukuAlatRelatedByPrasaranaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BukuAlatRelatedByPrasaranaId');
        }

        return $this;
    }

    /**
     * Use the BukuAlatRelatedByPrasaranaId relation BukuAlat object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\BukuAlatQuery A secondary query class using the current class as primary query
     */
    public function useBukuAlatRelatedByPrasaranaIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinBukuAlatRelatedByPrasaranaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BukuAlatRelatedByPrasaranaId', '\angulex\Model\BukuAlatQuery');
    }

    /**
     * Filter the query by a related VldPrasarana object
     *
     * @param   VldPrasarana|PropelObjectCollection $vldPrasarana  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PrasaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldPrasaranaRelatedByPrasaranaId($vldPrasarana, $comparison = null)
    {
        if ($vldPrasarana instanceof VldPrasarana) {
            return $this
                ->addUsingAlias(PrasaranaPeer::PRASARANA_ID, $vldPrasarana->getPrasaranaId(), $comparison);
        } elseif ($vldPrasarana instanceof PropelObjectCollection) {
            return $this
                ->useVldPrasaranaRelatedByPrasaranaIdQuery()
                ->filterByPrimaryKeys($vldPrasarana->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldPrasaranaRelatedByPrasaranaId() only accepts arguments of type VldPrasarana or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldPrasaranaRelatedByPrasaranaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function joinVldPrasaranaRelatedByPrasaranaId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldPrasaranaRelatedByPrasaranaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldPrasaranaRelatedByPrasaranaId');
        }

        return $this;
    }

    /**
     * Use the VldPrasaranaRelatedByPrasaranaId relation VldPrasarana object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldPrasaranaQuery A secondary query class using the current class as primary query
     */
    public function useVldPrasaranaRelatedByPrasaranaIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldPrasaranaRelatedByPrasaranaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldPrasaranaRelatedByPrasaranaId', '\angulex\Model\VldPrasaranaQuery');
    }

    /**
     * Filter the query by a related VldPrasarana object
     *
     * @param   VldPrasarana|PropelObjectCollection $vldPrasarana  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PrasaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldPrasaranaRelatedByPrasaranaId($vldPrasarana, $comparison = null)
    {
        if ($vldPrasarana instanceof VldPrasarana) {
            return $this
                ->addUsingAlias(PrasaranaPeer::PRASARANA_ID, $vldPrasarana->getPrasaranaId(), $comparison);
        } elseif ($vldPrasarana instanceof PropelObjectCollection) {
            return $this
                ->useVldPrasaranaRelatedByPrasaranaIdQuery()
                ->filterByPrimaryKeys($vldPrasarana->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldPrasaranaRelatedByPrasaranaId() only accepts arguments of type VldPrasarana or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldPrasaranaRelatedByPrasaranaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function joinVldPrasaranaRelatedByPrasaranaId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldPrasaranaRelatedByPrasaranaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldPrasaranaRelatedByPrasaranaId');
        }

        return $this;
    }

    /**
     * Use the VldPrasaranaRelatedByPrasaranaId relation VldPrasarana object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldPrasaranaQuery A secondary query class using the current class as primary query
     */
    public function useVldPrasaranaRelatedByPrasaranaIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldPrasaranaRelatedByPrasaranaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldPrasaranaRelatedByPrasaranaId', '\angulex\Model\VldPrasaranaQuery');
    }

    /**
     * Filter the query by a related Sarana object
     *
     * @param   Sarana|PropelObjectCollection $sarana  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PrasaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySaranaRelatedByPrasaranaId($sarana, $comparison = null)
    {
        if ($sarana instanceof Sarana) {
            return $this
                ->addUsingAlias(PrasaranaPeer::PRASARANA_ID, $sarana->getPrasaranaId(), $comparison);
        } elseif ($sarana instanceof PropelObjectCollection) {
            return $this
                ->useSaranaRelatedByPrasaranaIdQuery()
                ->filterByPrimaryKeys($sarana->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySaranaRelatedByPrasaranaId() only accepts arguments of type Sarana or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SaranaRelatedByPrasaranaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function joinSaranaRelatedByPrasaranaId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SaranaRelatedByPrasaranaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SaranaRelatedByPrasaranaId');
        }

        return $this;
    }

    /**
     * Use the SaranaRelatedByPrasaranaId relation Sarana object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SaranaQuery A secondary query class using the current class as primary query
     */
    public function useSaranaRelatedByPrasaranaIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSaranaRelatedByPrasaranaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SaranaRelatedByPrasaranaId', '\angulex\Model\SaranaQuery');
    }

    /**
     * Filter the query by a related Sarana object
     *
     * @param   Sarana|PropelObjectCollection $sarana  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PrasaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySaranaRelatedByPrasaranaId($sarana, $comparison = null)
    {
        if ($sarana instanceof Sarana) {
            return $this
                ->addUsingAlias(PrasaranaPeer::PRASARANA_ID, $sarana->getPrasaranaId(), $comparison);
        } elseif ($sarana instanceof PropelObjectCollection) {
            return $this
                ->useSaranaRelatedByPrasaranaIdQuery()
                ->filterByPrimaryKeys($sarana->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySaranaRelatedByPrasaranaId() only accepts arguments of type Sarana or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SaranaRelatedByPrasaranaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function joinSaranaRelatedByPrasaranaId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SaranaRelatedByPrasaranaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SaranaRelatedByPrasaranaId');
        }

        return $this;
    }

    /**
     * Use the SaranaRelatedByPrasaranaId relation Sarana object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SaranaQuery A secondary query class using the current class as primary query
     */
    public function useSaranaRelatedByPrasaranaIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSaranaRelatedByPrasaranaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SaranaRelatedByPrasaranaId', '\angulex\Model\SaranaQuery');
    }

    /**
     * Filter the query by a related PrasaranaLongitudinal object
     *
     * @param   PrasaranaLongitudinal|PropelObjectCollection $prasaranaLongitudinal  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PrasaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPrasaranaLongitudinalRelatedByPrasaranaId($prasaranaLongitudinal, $comparison = null)
    {
        if ($prasaranaLongitudinal instanceof PrasaranaLongitudinal) {
            return $this
                ->addUsingAlias(PrasaranaPeer::PRASARANA_ID, $prasaranaLongitudinal->getPrasaranaId(), $comparison);
        } elseif ($prasaranaLongitudinal instanceof PropelObjectCollection) {
            return $this
                ->usePrasaranaLongitudinalRelatedByPrasaranaIdQuery()
                ->filterByPrimaryKeys($prasaranaLongitudinal->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPrasaranaLongitudinalRelatedByPrasaranaId() only accepts arguments of type PrasaranaLongitudinal or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PrasaranaLongitudinalRelatedByPrasaranaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function joinPrasaranaLongitudinalRelatedByPrasaranaId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PrasaranaLongitudinalRelatedByPrasaranaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PrasaranaLongitudinalRelatedByPrasaranaId');
        }

        return $this;
    }

    /**
     * Use the PrasaranaLongitudinalRelatedByPrasaranaId relation PrasaranaLongitudinal object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PrasaranaLongitudinalQuery A secondary query class using the current class as primary query
     */
    public function usePrasaranaLongitudinalRelatedByPrasaranaIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPrasaranaLongitudinalRelatedByPrasaranaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PrasaranaLongitudinalRelatedByPrasaranaId', '\angulex\Model\PrasaranaLongitudinalQuery');
    }

    /**
     * Filter the query by a related PrasaranaLongitudinal object
     *
     * @param   PrasaranaLongitudinal|PropelObjectCollection $prasaranaLongitudinal  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PrasaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPrasaranaLongitudinalRelatedByPrasaranaId($prasaranaLongitudinal, $comparison = null)
    {
        if ($prasaranaLongitudinal instanceof PrasaranaLongitudinal) {
            return $this
                ->addUsingAlias(PrasaranaPeer::PRASARANA_ID, $prasaranaLongitudinal->getPrasaranaId(), $comparison);
        } elseif ($prasaranaLongitudinal instanceof PropelObjectCollection) {
            return $this
                ->usePrasaranaLongitudinalRelatedByPrasaranaIdQuery()
                ->filterByPrimaryKeys($prasaranaLongitudinal->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPrasaranaLongitudinalRelatedByPrasaranaId() only accepts arguments of type PrasaranaLongitudinal or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PrasaranaLongitudinalRelatedByPrasaranaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function joinPrasaranaLongitudinalRelatedByPrasaranaId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PrasaranaLongitudinalRelatedByPrasaranaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PrasaranaLongitudinalRelatedByPrasaranaId');
        }

        return $this;
    }

    /**
     * Use the PrasaranaLongitudinalRelatedByPrasaranaId relation PrasaranaLongitudinal object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PrasaranaLongitudinalQuery A secondary query class using the current class as primary query
     */
    public function usePrasaranaLongitudinalRelatedByPrasaranaIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPrasaranaLongitudinalRelatedByPrasaranaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PrasaranaLongitudinalRelatedByPrasaranaId', '\angulex\Model\PrasaranaLongitudinalQuery');
    }

    /**
     * Filter the query by a related RombonganBelajar object
     *
     * @param   RombonganBelajar|PropelObjectCollection $rombonganBelajar  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PrasaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRombonganBelajarRelatedByPrasaranaId($rombonganBelajar, $comparison = null)
    {
        if ($rombonganBelajar instanceof RombonganBelajar) {
            return $this
                ->addUsingAlias(PrasaranaPeer::PRASARANA_ID, $rombonganBelajar->getPrasaranaId(), $comparison);
        } elseif ($rombonganBelajar instanceof PropelObjectCollection) {
            return $this
                ->useRombonganBelajarRelatedByPrasaranaIdQuery()
                ->filterByPrimaryKeys($rombonganBelajar->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRombonganBelajarRelatedByPrasaranaId() only accepts arguments of type RombonganBelajar or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RombonganBelajarRelatedByPrasaranaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function joinRombonganBelajarRelatedByPrasaranaId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RombonganBelajarRelatedByPrasaranaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RombonganBelajarRelatedByPrasaranaId');
        }

        return $this;
    }

    /**
     * Use the RombonganBelajarRelatedByPrasaranaId relation RombonganBelajar object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\RombonganBelajarQuery A secondary query class using the current class as primary query
     */
    public function useRombonganBelajarRelatedByPrasaranaIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRombonganBelajarRelatedByPrasaranaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RombonganBelajarRelatedByPrasaranaId', '\angulex\Model\RombonganBelajarQuery');
    }

    /**
     * Filter the query by a related RombonganBelajar object
     *
     * @param   RombonganBelajar|PropelObjectCollection $rombonganBelajar  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PrasaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRombonganBelajarRelatedByPrasaranaId($rombonganBelajar, $comparison = null)
    {
        if ($rombonganBelajar instanceof RombonganBelajar) {
            return $this
                ->addUsingAlias(PrasaranaPeer::PRASARANA_ID, $rombonganBelajar->getPrasaranaId(), $comparison);
        } elseif ($rombonganBelajar instanceof PropelObjectCollection) {
            return $this
                ->useRombonganBelajarRelatedByPrasaranaIdQuery()
                ->filterByPrimaryKeys($rombonganBelajar->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRombonganBelajarRelatedByPrasaranaId() only accepts arguments of type RombonganBelajar or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RombonganBelajarRelatedByPrasaranaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function joinRombonganBelajarRelatedByPrasaranaId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RombonganBelajarRelatedByPrasaranaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RombonganBelajarRelatedByPrasaranaId');
        }

        return $this;
    }

    /**
     * Use the RombonganBelajarRelatedByPrasaranaId relation RombonganBelajar object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\RombonganBelajarQuery A secondary query class using the current class as primary query
     */
    public function useRombonganBelajarRelatedByPrasaranaIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRombonganBelajarRelatedByPrasaranaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RombonganBelajarRelatedByPrasaranaId', '\angulex\Model\RombonganBelajarQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Prasarana $prasarana Object to remove from the list of results
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function prune($prasarana = null)
    {
        if ($prasarana) {
            $this->addUsingAlias(PrasaranaPeer::PRASARANA_ID, $prasarana->getPrasaranaId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
