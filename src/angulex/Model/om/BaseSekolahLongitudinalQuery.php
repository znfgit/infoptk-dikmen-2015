<?php

namespace angulex\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\AksesInternet;
use angulex\Model\Sekolah;
use angulex\Model\SekolahLongitudinal;
use angulex\Model\SekolahLongitudinalPeer;
use angulex\Model\SekolahLongitudinalQuery;
use angulex\Model\Semester;
use angulex\Model\SertifikasiIso;
use angulex\Model\SumberListrik;
use angulex\Model\WaktuPenyelenggaraan;

/**
 * Base class that represents a query for the 'sekolah_longitudinal' table.
 *
 * 
 *
 * @method SekolahLongitudinalQuery orderBySekolahId($order = Criteria::ASC) Order by the sekolah_id column
 * @method SekolahLongitudinalQuery orderBySemesterId($order = Criteria::ASC) Order by the semester_id column
 * @method SekolahLongitudinalQuery orderByDayaListrik($order = Criteria::ASC) Order by the daya_listrik column
 * @method SekolahLongitudinalQuery orderByWilayahTerpencil($order = Criteria::ASC) Order by the wilayah_terpencil column
 * @method SekolahLongitudinalQuery orderByWilayahPerbatasan($order = Criteria::ASC) Order by the wilayah_perbatasan column
 * @method SekolahLongitudinalQuery orderByWilayahTransmigrasi($order = Criteria::ASC) Order by the wilayah_transmigrasi column
 * @method SekolahLongitudinalQuery orderByWilayahAdatTerpencil($order = Criteria::ASC) Order by the wilayah_adat_terpencil column
 * @method SekolahLongitudinalQuery orderByWilayahBencanaAlam($order = Criteria::ASC) Order by the wilayah_bencana_alam column
 * @method SekolahLongitudinalQuery orderByWilayahBencanaSosial($order = Criteria::ASC) Order by the wilayah_bencana_sosial column
 * @method SekolahLongitudinalQuery orderByPartisipasiBos($order = Criteria::ASC) Order by the partisipasi_bos column
 * @method SekolahLongitudinalQuery orderByWaktuPenyelenggaraanId($order = Criteria::ASC) Order by the waktu_penyelenggaraan_id column
 * @method SekolahLongitudinalQuery orderBySumberListrikId($order = Criteria::ASC) Order by the sumber_listrik_id column
 * @method SekolahLongitudinalQuery orderBySertifikasiIsoId($order = Criteria::ASC) Order by the sertifikasi_iso_id column
 * @method SekolahLongitudinalQuery orderByAksesInternetId($order = Criteria::ASC) Order by the akses_internet_id column
 * @method SekolahLongitudinalQuery orderByAksesInternet2Id($order = Criteria::ASC) Order by the akses_internet_2_id column
 * @method SekolahLongitudinalQuery orderByBlobId($order = Criteria::ASC) Order by the blob_id column
 * @method SekolahLongitudinalQuery orderByLastUpdate($order = Criteria::ASC) Order by the Last_update column
 * @method SekolahLongitudinalQuery orderBySoftDelete($order = Criteria::ASC) Order by the Soft_delete column
 * @method SekolahLongitudinalQuery orderByLastSync($order = Criteria::ASC) Order by the last_sync column
 * @method SekolahLongitudinalQuery orderByUpdaterId($order = Criteria::ASC) Order by the Updater_ID column
 *
 * @method SekolahLongitudinalQuery groupBySekolahId() Group by the sekolah_id column
 * @method SekolahLongitudinalQuery groupBySemesterId() Group by the semester_id column
 * @method SekolahLongitudinalQuery groupByDayaListrik() Group by the daya_listrik column
 * @method SekolahLongitudinalQuery groupByWilayahTerpencil() Group by the wilayah_terpencil column
 * @method SekolahLongitudinalQuery groupByWilayahPerbatasan() Group by the wilayah_perbatasan column
 * @method SekolahLongitudinalQuery groupByWilayahTransmigrasi() Group by the wilayah_transmigrasi column
 * @method SekolahLongitudinalQuery groupByWilayahAdatTerpencil() Group by the wilayah_adat_terpencil column
 * @method SekolahLongitudinalQuery groupByWilayahBencanaAlam() Group by the wilayah_bencana_alam column
 * @method SekolahLongitudinalQuery groupByWilayahBencanaSosial() Group by the wilayah_bencana_sosial column
 * @method SekolahLongitudinalQuery groupByPartisipasiBos() Group by the partisipasi_bos column
 * @method SekolahLongitudinalQuery groupByWaktuPenyelenggaraanId() Group by the waktu_penyelenggaraan_id column
 * @method SekolahLongitudinalQuery groupBySumberListrikId() Group by the sumber_listrik_id column
 * @method SekolahLongitudinalQuery groupBySertifikasiIsoId() Group by the sertifikasi_iso_id column
 * @method SekolahLongitudinalQuery groupByAksesInternetId() Group by the akses_internet_id column
 * @method SekolahLongitudinalQuery groupByAksesInternet2Id() Group by the akses_internet_2_id column
 * @method SekolahLongitudinalQuery groupByBlobId() Group by the blob_id column
 * @method SekolahLongitudinalQuery groupByLastUpdate() Group by the Last_update column
 * @method SekolahLongitudinalQuery groupBySoftDelete() Group by the Soft_delete column
 * @method SekolahLongitudinalQuery groupByLastSync() Group by the last_sync column
 * @method SekolahLongitudinalQuery groupByUpdaterId() Group by the Updater_ID column
 *
 * @method SekolahLongitudinalQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SekolahLongitudinalQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SekolahLongitudinalQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method SekolahLongitudinalQuery leftJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method SekolahLongitudinalQuery rightJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method SekolahLongitudinalQuery innerJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the SekolahRelatedBySekolahId relation
 *
 * @method SekolahLongitudinalQuery leftJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method SekolahLongitudinalQuery rightJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method SekolahLongitudinalQuery innerJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the SekolahRelatedBySekolahId relation
 *
 * @method SekolahLongitudinalQuery leftJoinAksesInternetRelatedByAksesInternetId($relationAlias = null) Adds a LEFT JOIN clause to the query using the AksesInternetRelatedByAksesInternetId relation
 * @method SekolahLongitudinalQuery rightJoinAksesInternetRelatedByAksesInternetId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the AksesInternetRelatedByAksesInternetId relation
 * @method SekolahLongitudinalQuery innerJoinAksesInternetRelatedByAksesInternetId($relationAlias = null) Adds a INNER JOIN clause to the query using the AksesInternetRelatedByAksesInternetId relation
 *
 * @method SekolahLongitudinalQuery leftJoinAksesInternetRelatedByAksesInternet2Id($relationAlias = null) Adds a LEFT JOIN clause to the query using the AksesInternetRelatedByAksesInternet2Id relation
 * @method SekolahLongitudinalQuery rightJoinAksesInternetRelatedByAksesInternet2Id($relationAlias = null) Adds a RIGHT JOIN clause to the query using the AksesInternetRelatedByAksesInternet2Id relation
 * @method SekolahLongitudinalQuery innerJoinAksesInternetRelatedByAksesInternet2Id($relationAlias = null) Adds a INNER JOIN clause to the query using the AksesInternetRelatedByAksesInternet2Id relation
 *
 * @method SekolahLongitudinalQuery leftJoinAksesInternetRelatedByAksesInternetId($relationAlias = null) Adds a LEFT JOIN clause to the query using the AksesInternetRelatedByAksesInternetId relation
 * @method SekolahLongitudinalQuery rightJoinAksesInternetRelatedByAksesInternetId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the AksesInternetRelatedByAksesInternetId relation
 * @method SekolahLongitudinalQuery innerJoinAksesInternetRelatedByAksesInternetId($relationAlias = null) Adds a INNER JOIN clause to the query using the AksesInternetRelatedByAksesInternetId relation
 *
 * @method SekolahLongitudinalQuery leftJoinAksesInternetRelatedByAksesInternet2Id($relationAlias = null) Adds a LEFT JOIN clause to the query using the AksesInternetRelatedByAksesInternet2Id relation
 * @method SekolahLongitudinalQuery rightJoinAksesInternetRelatedByAksesInternet2Id($relationAlias = null) Adds a RIGHT JOIN clause to the query using the AksesInternetRelatedByAksesInternet2Id relation
 * @method SekolahLongitudinalQuery innerJoinAksesInternetRelatedByAksesInternet2Id($relationAlias = null) Adds a INNER JOIN clause to the query using the AksesInternetRelatedByAksesInternet2Id relation
 *
 * @method SekolahLongitudinalQuery leftJoinSemesterRelatedBySemesterId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SemesterRelatedBySemesterId relation
 * @method SekolahLongitudinalQuery rightJoinSemesterRelatedBySemesterId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SemesterRelatedBySemesterId relation
 * @method SekolahLongitudinalQuery innerJoinSemesterRelatedBySemesterId($relationAlias = null) Adds a INNER JOIN clause to the query using the SemesterRelatedBySemesterId relation
 *
 * @method SekolahLongitudinalQuery leftJoinSemesterRelatedBySemesterId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SemesterRelatedBySemesterId relation
 * @method SekolahLongitudinalQuery rightJoinSemesterRelatedBySemesterId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SemesterRelatedBySemesterId relation
 * @method SekolahLongitudinalQuery innerJoinSemesterRelatedBySemesterId($relationAlias = null) Adds a INNER JOIN clause to the query using the SemesterRelatedBySemesterId relation
 *
 * @method SekolahLongitudinalQuery leftJoinSertifikasiIsoRelatedBySertifikasiIsoId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SertifikasiIsoRelatedBySertifikasiIsoId relation
 * @method SekolahLongitudinalQuery rightJoinSertifikasiIsoRelatedBySertifikasiIsoId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SertifikasiIsoRelatedBySertifikasiIsoId relation
 * @method SekolahLongitudinalQuery innerJoinSertifikasiIsoRelatedBySertifikasiIsoId($relationAlias = null) Adds a INNER JOIN clause to the query using the SertifikasiIsoRelatedBySertifikasiIsoId relation
 *
 * @method SekolahLongitudinalQuery leftJoinSertifikasiIsoRelatedBySertifikasiIsoId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SertifikasiIsoRelatedBySertifikasiIsoId relation
 * @method SekolahLongitudinalQuery rightJoinSertifikasiIsoRelatedBySertifikasiIsoId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SertifikasiIsoRelatedBySertifikasiIsoId relation
 * @method SekolahLongitudinalQuery innerJoinSertifikasiIsoRelatedBySertifikasiIsoId($relationAlias = null) Adds a INNER JOIN clause to the query using the SertifikasiIsoRelatedBySertifikasiIsoId relation
 *
 * @method SekolahLongitudinalQuery leftJoinSumberListrikRelatedBySumberListrikId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SumberListrikRelatedBySumberListrikId relation
 * @method SekolahLongitudinalQuery rightJoinSumberListrikRelatedBySumberListrikId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SumberListrikRelatedBySumberListrikId relation
 * @method SekolahLongitudinalQuery innerJoinSumberListrikRelatedBySumberListrikId($relationAlias = null) Adds a INNER JOIN clause to the query using the SumberListrikRelatedBySumberListrikId relation
 *
 * @method SekolahLongitudinalQuery leftJoinSumberListrikRelatedBySumberListrikId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SumberListrikRelatedBySumberListrikId relation
 * @method SekolahLongitudinalQuery rightJoinSumberListrikRelatedBySumberListrikId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SumberListrikRelatedBySumberListrikId relation
 * @method SekolahLongitudinalQuery innerJoinSumberListrikRelatedBySumberListrikId($relationAlias = null) Adds a INNER JOIN clause to the query using the SumberListrikRelatedBySumberListrikId relation
 *
 * @method SekolahLongitudinalQuery leftJoinWaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId($relationAlias = null) Adds a LEFT JOIN clause to the query using the WaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId relation
 * @method SekolahLongitudinalQuery rightJoinWaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the WaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId relation
 * @method SekolahLongitudinalQuery innerJoinWaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId($relationAlias = null) Adds a INNER JOIN clause to the query using the WaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId relation
 *
 * @method SekolahLongitudinalQuery leftJoinWaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId($relationAlias = null) Adds a LEFT JOIN clause to the query using the WaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId relation
 * @method SekolahLongitudinalQuery rightJoinWaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the WaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId relation
 * @method SekolahLongitudinalQuery innerJoinWaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId($relationAlias = null) Adds a INNER JOIN clause to the query using the WaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId relation
 *
 * @method SekolahLongitudinal findOne(PropelPDO $con = null) Return the first SekolahLongitudinal matching the query
 * @method SekolahLongitudinal findOneOrCreate(PropelPDO $con = null) Return the first SekolahLongitudinal matching the query, or a new SekolahLongitudinal object populated from the query conditions when no match is found
 *
 * @method SekolahLongitudinal findOneBySekolahId(string $sekolah_id) Return the first SekolahLongitudinal filtered by the sekolah_id column
 * @method SekolahLongitudinal findOneBySemesterId(string $semester_id) Return the first SekolahLongitudinal filtered by the semester_id column
 * @method SekolahLongitudinal findOneByDayaListrik(string $daya_listrik) Return the first SekolahLongitudinal filtered by the daya_listrik column
 * @method SekolahLongitudinal findOneByWilayahTerpencil(string $wilayah_terpencil) Return the first SekolahLongitudinal filtered by the wilayah_terpencil column
 * @method SekolahLongitudinal findOneByWilayahPerbatasan(string $wilayah_perbatasan) Return the first SekolahLongitudinal filtered by the wilayah_perbatasan column
 * @method SekolahLongitudinal findOneByWilayahTransmigrasi(string $wilayah_transmigrasi) Return the first SekolahLongitudinal filtered by the wilayah_transmigrasi column
 * @method SekolahLongitudinal findOneByWilayahAdatTerpencil(string $wilayah_adat_terpencil) Return the first SekolahLongitudinal filtered by the wilayah_adat_terpencil column
 * @method SekolahLongitudinal findOneByWilayahBencanaAlam(string $wilayah_bencana_alam) Return the first SekolahLongitudinal filtered by the wilayah_bencana_alam column
 * @method SekolahLongitudinal findOneByWilayahBencanaSosial(string $wilayah_bencana_sosial) Return the first SekolahLongitudinal filtered by the wilayah_bencana_sosial column
 * @method SekolahLongitudinal findOneByPartisipasiBos(string $partisipasi_bos) Return the first SekolahLongitudinal filtered by the partisipasi_bos column
 * @method SekolahLongitudinal findOneByWaktuPenyelenggaraanId(string $waktu_penyelenggaraan_id) Return the first SekolahLongitudinal filtered by the waktu_penyelenggaraan_id column
 * @method SekolahLongitudinal findOneBySumberListrikId(string $sumber_listrik_id) Return the first SekolahLongitudinal filtered by the sumber_listrik_id column
 * @method SekolahLongitudinal findOneBySertifikasiIsoId(int $sertifikasi_iso_id) Return the first SekolahLongitudinal filtered by the sertifikasi_iso_id column
 * @method SekolahLongitudinal findOneByAksesInternetId(int $akses_internet_id) Return the first SekolahLongitudinal filtered by the akses_internet_id column
 * @method SekolahLongitudinal findOneByAksesInternet2Id(int $akses_internet_2_id) Return the first SekolahLongitudinal filtered by the akses_internet_2_id column
 * @method SekolahLongitudinal findOneByBlobId(string $blob_id) Return the first SekolahLongitudinal filtered by the blob_id column
 * @method SekolahLongitudinal findOneByLastUpdate(string $Last_update) Return the first SekolahLongitudinal filtered by the Last_update column
 * @method SekolahLongitudinal findOneBySoftDelete(string $Soft_delete) Return the first SekolahLongitudinal filtered by the Soft_delete column
 * @method SekolahLongitudinal findOneByLastSync(string $last_sync) Return the first SekolahLongitudinal filtered by the last_sync column
 * @method SekolahLongitudinal findOneByUpdaterId(string $Updater_ID) Return the first SekolahLongitudinal filtered by the Updater_ID column
 *
 * @method array findBySekolahId(string $sekolah_id) Return SekolahLongitudinal objects filtered by the sekolah_id column
 * @method array findBySemesterId(string $semester_id) Return SekolahLongitudinal objects filtered by the semester_id column
 * @method array findByDayaListrik(string $daya_listrik) Return SekolahLongitudinal objects filtered by the daya_listrik column
 * @method array findByWilayahTerpencil(string $wilayah_terpencil) Return SekolahLongitudinal objects filtered by the wilayah_terpencil column
 * @method array findByWilayahPerbatasan(string $wilayah_perbatasan) Return SekolahLongitudinal objects filtered by the wilayah_perbatasan column
 * @method array findByWilayahTransmigrasi(string $wilayah_transmigrasi) Return SekolahLongitudinal objects filtered by the wilayah_transmigrasi column
 * @method array findByWilayahAdatTerpencil(string $wilayah_adat_terpencil) Return SekolahLongitudinal objects filtered by the wilayah_adat_terpencil column
 * @method array findByWilayahBencanaAlam(string $wilayah_bencana_alam) Return SekolahLongitudinal objects filtered by the wilayah_bencana_alam column
 * @method array findByWilayahBencanaSosial(string $wilayah_bencana_sosial) Return SekolahLongitudinal objects filtered by the wilayah_bencana_sosial column
 * @method array findByPartisipasiBos(string $partisipasi_bos) Return SekolahLongitudinal objects filtered by the partisipasi_bos column
 * @method array findByWaktuPenyelenggaraanId(string $waktu_penyelenggaraan_id) Return SekolahLongitudinal objects filtered by the waktu_penyelenggaraan_id column
 * @method array findBySumberListrikId(string $sumber_listrik_id) Return SekolahLongitudinal objects filtered by the sumber_listrik_id column
 * @method array findBySertifikasiIsoId(int $sertifikasi_iso_id) Return SekolahLongitudinal objects filtered by the sertifikasi_iso_id column
 * @method array findByAksesInternetId(int $akses_internet_id) Return SekolahLongitudinal objects filtered by the akses_internet_id column
 * @method array findByAksesInternet2Id(int $akses_internet_2_id) Return SekolahLongitudinal objects filtered by the akses_internet_2_id column
 * @method array findByBlobId(string $blob_id) Return SekolahLongitudinal objects filtered by the blob_id column
 * @method array findByLastUpdate(string $Last_update) Return SekolahLongitudinal objects filtered by the Last_update column
 * @method array findBySoftDelete(string $Soft_delete) Return SekolahLongitudinal objects filtered by the Soft_delete column
 * @method array findByLastSync(string $last_sync) Return SekolahLongitudinal objects filtered by the last_sync column
 * @method array findByUpdaterId(string $Updater_ID) Return SekolahLongitudinal objects filtered by the Updater_ID column
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseSekolahLongitudinalQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSekolahLongitudinalQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Dapodikmen', $modelName = 'angulex\\Model\\SekolahLongitudinal', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SekolahLongitudinalQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   SekolahLongitudinalQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SekolahLongitudinalQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SekolahLongitudinalQuery) {
            return $criteria;
        }
        $query = new SekolahLongitudinalQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34), $con);
     * </code>
     *
     * @param array $key Primary key to use for the query 
                         A Primary key composition: [$sekolah_id, $semester_id]
     * @param     PropelPDO $con an optional connection object
     *
     * @return   SekolahLongitudinal|SekolahLongitudinal[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SekolahLongitudinalPeer::getInstanceFromPool(serialize(array((string) $key[0], (string) $key[1]))))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SekolahLongitudinalPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 SekolahLongitudinal A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [sekolah_id], [semester_id], [daya_listrik], [wilayah_terpencil], [wilayah_perbatasan], [wilayah_transmigrasi], [wilayah_adat_terpencil], [wilayah_bencana_alam], [wilayah_bencana_sosial], [partisipasi_bos], [waktu_penyelenggaraan_id], [sumber_listrik_id], [sertifikasi_iso_id], [akses_internet_id], [akses_internet_2_id], [blob_id], [Last_update], [Soft_delete], [last_sync], [Updater_ID] FROM [sekolah_longitudinal] WHERE [sekolah_id] = :p0 AND [semester_id] = :p1';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_STR);			
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new SekolahLongitudinal();
            $obj->hydrate($row);
            SekolahLongitudinalPeer::addInstanceToPool($obj, serialize(array((string) $key[0], (string) $key[1])));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return SekolahLongitudinal|SekolahLongitudinal[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|SekolahLongitudinal[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SekolahLongitudinalQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(SekolahLongitudinalPeer::SEKOLAH_ID, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(SekolahLongitudinalPeer::SEMESTER_ID, $key[1], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SekolahLongitudinalQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(SekolahLongitudinalPeer::SEKOLAH_ID, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(SekolahLongitudinalPeer::SEMESTER_ID, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the sekolah_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySekolahId('fooValue');   // WHERE sekolah_id = 'fooValue'
     * $query->filterBySekolahId('%fooValue%'); // WHERE sekolah_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sekolahId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahLongitudinalQuery The current query, for fluid interface
     */
    public function filterBySekolahId($sekolahId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sekolahId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $sekolahId)) {
                $sekolahId = str_replace('*', '%', $sekolahId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SekolahLongitudinalPeer::SEKOLAH_ID, $sekolahId, $comparison);
    }

    /**
     * Filter the query on the semester_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySemesterId('fooValue');   // WHERE semester_id = 'fooValue'
     * $query->filterBySemesterId('%fooValue%'); // WHERE semester_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $semesterId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahLongitudinalQuery The current query, for fluid interface
     */
    public function filterBySemesterId($semesterId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($semesterId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $semesterId)) {
                $semesterId = str_replace('*', '%', $semesterId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SekolahLongitudinalPeer::SEMESTER_ID, $semesterId, $comparison);
    }

    /**
     * Filter the query on the daya_listrik column
     *
     * Example usage:
     * <code>
     * $query->filterByDayaListrik(1234); // WHERE daya_listrik = 1234
     * $query->filterByDayaListrik(array(12, 34)); // WHERE daya_listrik IN (12, 34)
     * $query->filterByDayaListrik(array('min' => 12)); // WHERE daya_listrik >= 12
     * $query->filterByDayaListrik(array('max' => 12)); // WHERE daya_listrik <= 12
     * </code>
     *
     * @param     mixed $dayaListrik The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahLongitudinalQuery The current query, for fluid interface
     */
    public function filterByDayaListrik($dayaListrik = null, $comparison = null)
    {
        if (is_array($dayaListrik)) {
            $useMinMax = false;
            if (isset($dayaListrik['min'])) {
                $this->addUsingAlias(SekolahLongitudinalPeer::DAYA_LISTRIK, $dayaListrik['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dayaListrik['max'])) {
                $this->addUsingAlias(SekolahLongitudinalPeer::DAYA_LISTRIK, $dayaListrik['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SekolahLongitudinalPeer::DAYA_LISTRIK, $dayaListrik, $comparison);
    }

    /**
     * Filter the query on the wilayah_terpencil column
     *
     * Example usage:
     * <code>
     * $query->filterByWilayahTerpencil(1234); // WHERE wilayah_terpencil = 1234
     * $query->filterByWilayahTerpencil(array(12, 34)); // WHERE wilayah_terpencil IN (12, 34)
     * $query->filterByWilayahTerpencil(array('min' => 12)); // WHERE wilayah_terpencil >= 12
     * $query->filterByWilayahTerpencil(array('max' => 12)); // WHERE wilayah_terpencil <= 12
     * </code>
     *
     * @param     mixed $wilayahTerpencil The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahLongitudinalQuery The current query, for fluid interface
     */
    public function filterByWilayahTerpencil($wilayahTerpencil = null, $comparison = null)
    {
        if (is_array($wilayahTerpencil)) {
            $useMinMax = false;
            if (isset($wilayahTerpencil['min'])) {
                $this->addUsingAlias(SekolahLongitudinalPeer::WILAYAH_TERPENCIL, $wilayahTerpencil['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($wilayahTerpencil['max'])) {
                $this->addUsingAlias(SekolahLongitudinalPeer::WILAYAH_TERPENCIL, $wilayahTerpencil['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SekolahLongitudinalPeer::WILAYAH_TERPENCIL, $wilayahTerpencil, $comparison);
    }

    /**
     * Filter the query on the wilayah_perbatasan column
     *
     * Example usage:
     * <code>
     * $query->filterByWilayahPerbatasan(1234); // WHERE wilayah_perbatasan = 1234
     * $query->filterByWilayahPerbatasan(array(12, 34)); // WHERE wilayah_perbatasan IN (12, 34)
     * $query->filterByWilayahPerbatasan(array('min' => 12)); // WHERE wilayah_perbatasan >= 12
     * $query->filterByWilayahPerbatasan(array('max' => 12)); // WHERE wilayah_perbatasan <= 12
     * </code>
     *
     * @param     mixed $wilayahPerbatasan The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahLongitudinalQuery The current query, for fluid interface
     */
    public function filterByWilayahPerbatasan($wilayahPerbatasan = null, $comparison = null)
    {
        if (is_array($wilayahPerbatasan)) {
            $useMinMax = false;
            if (isset($wilayahPerbatasan['min'])) {
                $this->addUsingAlias(SekolahLongitudinalPeer::WILAYAH_PERBATASAN, $wilayahPerbatasan['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($wilayahPerbatasan['max'])) {
                $this->addUsingAlias(SekolahLongitudinalPeer::WILAYAH_PERBATASAN, $wilayahPerbatasan['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SekolahLongitudinalPeer::WILAYAH_PERBATASAN, $wilayahPerbatasan, $comparison);
    }

    /**
     * Filter the query on the wilayah_transmigrasi column
     *
     * Example usage:
     * <code>
     * $query->filterByWilayahTransmigrasi(1234); // WHERE wilayah_transmigrasi = 1234
     * $query->filterByWilayahTransmigrasi(array(12, 34)); // WHERE wilayah_transmigrasi IN (12, 34)
     * $query->filterByWilayahTransmigrasi(array('min' => 12)); // WHERE wilayah_transmigrasi >= 12
     * $query->filterByWilayahTransmigrasi(array('max' => 12)); // WHERE wilayah_transmigrasi <= 12
     * </code>
     *
     * @param     mixed $wilayahTransmigrasi The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahLongitudinalQuery The current query, for fluid interface
     */
    public function filterByWilayahTransmigrasi($wilayahTransmigrasi = null, $comparison = null)
    {
        if (is_array($wilayahTransmigrasi)) {
            $useMinMax = false;
            if (isset($wilayahTransmigrasi['min'])) {
                $this->addUsingAlias(SekolahLongitudinalPeer::WILAYAH_TRANSMIGRASI, $wilayahTransmigrasi['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($wilayahTransmigrasi['max'])) {
                $this->addUsingAlias(SekolahLongitudinalPeer::WILAYAH_TRANSMIGRASI, $wilayahTransmigrasi['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SekolahLongitudinalPeer::WILAYAH_TRANSMIGRASI, $wilayahTransmigrasi, $comparison);
    }

    /**
     * Filter the query on the wilayah_adat_terpencil column
     *
     * Example usage:
     * <code>
     * $query->filterByWilayahAdatTerpencil(1234); // WHERE wilayah_adat_terpencil = 1234
     * $query->filterByWilayahAdatTerpencil(array(12, 34)); // WHERE wilayah_adat_terpencil IN (12, 34)
     * $query->filterByWilayahAdatTerpencil(array('min' => 12)); // WHERE wilayah_adat_terpencil >= 12
     * $query->filterByWilayahAdatTerpencil(array('max' => 12)); // WHERE wilayah_adat_terpencil <= 12
     * </code>
     *
     * @param     mixed $wilayahAdatTerpencil The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahLongitudinalQuery The current query, for fluid interface
     */
    public function filterByWilayahAdatTerpencil($wilayahAdatTerpencil = null, $comparison = null)
    {
        if (is_array($wilayahAdatTerpencil)) {
            $useMinMax = false;
            if (isset($wilayahAdatTerpencil['min'])) {
                $this->addUsingAlias(SekolahLongitudinalPeer::WILAYAH_ADAT_TERPENCIL, $wilayahAdatTerpencil['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($wilayahAdatTerpencil['max'])) {
                $this->addUsingAlias(SekolahLongitudinalPeer::WILAYAH_ADAT_TERPENCIL, $wilayahAdatTerpencil['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SekolahLongitudinalPeer::WILAYAH_ADAT_TERPENCIL, $wilayahAdatTerpencil, $comparison);
    }

    /**
     * Filter the query on the wilayah_bencana_alam column
     *
     * Example usage:
     * <code>
     * $query->filterByWilayahBencanaAlam(1234); // WHERE wilayah_bencana_alam = 1234
     * $query->filterByWilayahBencanaAlam(array(12, 34)); // WHERE wilayah_bencana_alam IN (12, 34)
     * $query->filterByWilayahBencanaAlam(array('min' => 12)); // WHERE wilayah_bencana_alam >= 12
     * $query->filterByWilayahBencanaAlam(array('max' => 12)); // WHERE wilayah_bencana_alam <= 12
     * </code>
     *
     * @param     mixed $wilayahBencanaAlam The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahLongitudinalQuery The current query, for fluid interface
     */
    public function filterByWilayahBencanaAlam($wilayahBencanaAlam = null, $comparison = null)
    {
        if (is_array($wilayahBencanaAlam)) {
            $useMinMax = false;
            if (isset($wilayahBencanaAlam['min'])) {
                $this->addUsingAlias(SekolahLongitudinalPeer::WILAYAH_BENCANA_ALAM, $wilayahBencanaAlam['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($wilayahBencanaAlam['max'])) {
                $this->addUsingAlias(SekolahLongitudinalPeer::WILAYAH_BENCANA_ALAM, $wilayahBencanaAlam['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SekolahLongitudinalPeer::WILAYAH_BENCANA_ALAM, $wilayahBencanaAlam, $comparison);
    }

    /**
     * Filter the query on the wilayah_bencana_sosial column
     *
     * Example usage:
     * <code>
     * $query->filterByWilayahBencanaSosial(1234); // WHERE wilayah_bencana_sosial = 1234
     * $query->filterByWilayahBencanaSosial(array(12, 34)); // WHERE wilayah_bencana_sosial IN (12, 34)
     * $query->filterByWilayahBencanaSosial(array('min' => 12)); // WHERE wilayah_bencana_sosial >= 12
     * $query->filterByWilayahBencanaSosial(array('max' => 12)); // WHERE wilayah_bencana_sosial <= 12
     * </code>
     *
     * @param     mixed $wilayahBencanaSosial The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahLongitudinalQuery The current query, for fluid interface
     */
    public function filterByWilayahBencanaSosial($wilayahBencanaSosial = null, $comparison = null)
    {
        if (is_array($wilayahBencanaSosial)) {
            $useMinMax = false;
            if (isset($wilayahBencanaSosial['min'])) {
                $this->addUsingAlias(SekolahLongitudinalPeer::WILAYAH_BENCANA_SOSIAL, $wilayahBencanaSosial['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($wilayahBencanaSosial['max'])) {
                $this->addUsingAlias(SekolahLongitudinalPeer::WILAYAH_BENCANA_SOSIAL, $wilayahBencanaSosial['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SekolahLongitudinalPeer::WILAYAH_BENCANA_SOSIAL, $wilayahBencanaSosial, $comparison);
    }

    /**
     * Filter the query on the partisipasi_bos column
     *
     * Example usage:
     * <code>
     * $query->filterByPartisipasiBos(1234); // WHERE partisipasi_bos = 1234
     * $query->filterByPartisipasiBos(array(12, 34)); // WHERE partisipasi_bos IN (12, 34)
     * $query->filterByPartisipasiBos(array('min' => 12)); // WHERE partisipasi_bos >= 12
     * $query->filterByPartisipasiBos(array('max' => 12)); // WHERE partisipasi_bos <= 12
     * </code>
     *
     * @param     mixed $partisipasiBos The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahLongitudinalQuery The current query, for fluid interface
     */
    public function filterByPartisipasiBos($partisipasiBos = null, $comparison = null)
    {
        if (is_array($partisipasiBos)) {
            $useMinMax = false;
            if (isset($partisipasiBos['min'])) {
                $this->addUsingAlias(SekolahLongitudinalPeer::PARTISIPASI_BOS, $partisipasiBos['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($partisipasiBos['max'])) {
                $this->addUsingAlias(SekolahLongitudinalPeer::PARTISIPASI_BOS, $partisipasiBos['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SekolahLongitudinalPeer::PARTISIPASI_BOS, $partisipasiBos, $comparison);
    }

    /**
     * Filter the query on the waktu_penyelenggaraan_id column
     *
     * Example usage:
     * <code>
     * $query->filterByWaktuPenyelenggaraanId(1234); // WHERE waktu_penyelenggaraan_id = 1234
     * $query->filterByWaktuPenyelenggaraanId(array(12, 34)); // WHERE waktu_penyelenggaraan_id IN (12, 34)
     * $query->filterByWaktuPenyelenggaraanId(array('min' => 12)); // WHERE waktu_penyelenggaraan_id >= 12
     * $query->filterByWaktuPenyelenggaraanId(array('max' => 12)); // WHERE waktu_penyelenggaraan_id <= 12
     * </code>
     *
     * @see       filterByWaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId()
     *
     * @see       filterByWaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId()
     *
     * @param     mixed $waktuPenyelenggaraanId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahLongitudinalQuery The current query, for fluid interface
     */
    public function filterByWaktuPenyelenggaraanId($waktuPenyelenggaraanId = null, $comparison = null)
    {
        if (is_array($waktuPenyelenggaraanId)) {
            $useMinMax = false;
            if (isset($waktuPenyelenggaraanId['min'])) {
                $this->addUsingAlias(SekolahLongitudinalPeer::WAKTU_PENYELENGGARAAN_ID, $waktuPenyelenggaraanId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($waktuPenyelenggaraanId['max'])) {
                $this->addUsingAlias(SekolahLongitudinalPeer::WAKTU_PENYELENGGARAAN_ID, $waktuPenyelenggaraanId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SekolahLongitudinalPeer::WAKTU_PENYELENGGARAAN_ID, $waktuPenyelenggaraanId, $comparison);
    }

    /**
     * Filter the query on the sumber_listrik_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySumberListrikId(1234); // WHERE sumber_listrik_id = 1234
     * $query->filterBySumberListrikId(array(12, 34)); // WHERE sumber_listrik_id IN (12, 34)
     * $query->filterBySumberListrikId(array('min' => 12)); // WHERE sumber_listrik_id >= 12
     * $query->filterBySumberListrikId(array('max' => 12)); // WHERE sumber_listrik_id <= 12
     * </code>
     *
     * @see       filterBySumberListrikRelatedBySumberListrikId()
     *
     * @see       filterBySumberListrikRelatedBySumberListrikId()
     *
     * @param     mixed $sumberListrikId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahLongitudinalQuery The current query, for fluid interface
     */
    public function filterBySumberListrikId($sumberListrikId = null, $comparison = null)
    {
        if (is_array($sumberListrikId)) {
            $useMinMax = false;
            if (isset($sumberListrikId['min'])) {
                $this->addUsingAlias(SekolahLongitudinalPeer::SUMBER_LISTRIK_ID, $sumberListrikId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sumberListrikId['max'])) {
                $this->addUsingAlias(SekolahLongitudinalPeer::SUMBER_LISTRIK_ID, $sumberListrikId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SekolahLongitudinalPeer::SUMBER_LISTRIK_ID, $sumberListrikId, $comparison);
    }

    /**
     * Filter the query on the sertifikasi_iso_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySertifikasiIsoId(1234); // WHERE sertifikasi_iso_id = 1234
     * $query->filterBySertifikasiIsoId(array(12, 34)); // WHERE sertifikasi_iso_id IN (12, 34)
     * $query->filterBySertifikasiIsoId(array('min' => 12)); // WHERE sertifikasi_iso_id >= 12
     * $query->filterBySertifikasiIsoId(array('max' => 12)); // WHERE sertifikasi_iso_id <= 12
     * </code>
     *
     * @see       filterBySertifikasiIsoRelatedBySertifikasiIsoId()
     *
     * @see       filterBySertifikasiIsoRelatedBySertifikasiIsoId()
     *
     * @param     mixed $sertifikasiIsoId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahLongitudinalQuery The current query, for fluid interface
     */
    public function filterBySertifikasiIsoId($sertifikasiIsoId = null, $comparison = null)
    {
        if (is_array($sertifikasiIsoId)) {
            $useMinMax = false;
            if (isset($sertifikasiIsoId['min'])) {
                $this->addUsingAlias(SekolahLongitudinalPeer::SERTIFIKASI_ISO_ID, $sertifikasiIsoId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sertifikasiIsoId['max'])) {
                $this->addUsingAlias(SekolahLongitudinalPeer::SERTIFIKASI_ISO_ID, $sertifikasiIsoId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SekolahLongitudinalPeer::SERTIFIKASI_ISO_ID, $sertifikasiIsoId, $comparison);
    }

    /**
     * Filter the query on the akses_internet_id column
     *
     * Example usage:
     * <code>
     * $query->filterByAksesInternetId(1234); // WHERE akses_internet_id = 1234
     * $query->filterByAksesInternetId(array(12, 34)); // WHERE akses_internet_id IN (12, 34)
     * $query->filterByAksesInternetId(array('min' => 12)); // WHERE akses_internet_id >= 12
     * $query->filterByAksesInternetId(array('max' => 12)); // WHERE akses_internet_id <= 12
     * </code>
     *
     * @see       filterByAksesInternetRelatedByAksesInternetId()
     *
     * @see       filterByAksesInternetRelatedByAksesInternetId()
     *
     * @param     mixed $aksesInternetId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahLongitudinalQuery The current query, for fluid interface
     */
    public function filterByAksesInternetId($aksesInternetId = null, $comparison = null)
    {
        if (is_array($aksesInternetId)) {
            $useMinMax = false;
            if (isset($aksesInternetId['min'])) {
                $this->addUsingAlias(SekolahLongitudinalPeer::AKSES_INTERNET_ID, $aksesInternetId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($aksesInternetId['max'])) {
                $this->addUsingAlias(SekolahLongitudinalPeer::AKSES_INTERNET_ID, $aksesInternetId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SekolahLongitudinalPeer::AKSES_INTERNET_ID, $aksesInternetId, $comparison);
    }

    /**
     * Filter the query on the akses_internet_2_id column
     *
     * Example usage:
     * <code>
     * $query->filterByAksesInternet2Id(1234); // WHERE akses_internet_2_id = 1234
     * $query->filterByAksesInternet2Id(array(12, 34)); // WHERE akses_internet_2_id IN (12, 34)
     * $query->filterByAksesInternet2Id(array('min' => 12)); // WHERE akses_internet_2_id >= 12
     * $query->filterByAksesInternet2Id(array('max' => 12)); // WHERE akses_internet_2_id <= 12
     * </code>
     *
     * @see       filterByAksesInternetRelatedByAksesInternet2Id()
     *
     * @see       filterByAksesInternetRelatedByAksesInternet2Id()
     *
     * @param     mixed $aksesInternet2Id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahLongitudinalQuery The current query, for fluid interface
     */
    public function filterByAksesInternet2Id($aksesInternet2Id = null, $comparison = null)
    {
        if (is_array($aksesInternet2Id)) {
            $useMinMax = false;
            if (isset($aksesInternet2Id['min'])) {
                $this->addUsingAlias(SekolahLongitudinalPeer::AKSES_INTERNET_2_ID, $aksesInternet2Id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($aksesInternet2Id['max'])) {
                $this->addUsingAlias(SekolahLongitudinalPeer::AKSES_INTERNET_2_ID, $aksesInternet2Id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SekolahLongitudinalPeer::AKSES_INTERNET_2_ID, $aksesInternet2Id, $comparison);
    }

    /**
     * Filter the query on the blob_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBlobId('fooValue');   // WHERE blob_id = 'fooValue'
     * $query->filterByBlobId('%fooValue%'); // WHERE blob_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $blobId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahLongitudinalQuery The current query, for fluid interface
     */
    public function filterByBlobId($blobId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($blobId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $blobId)) {
                $blobId = str_replace('*', '%', $blobId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SekolahLongitudinalPeer::BLOB_ID, $blobId, $comparison);
    }

    /**
     * Filter the query on the Last_update column
     *
     * Example usage:
     * <code>
     * $query->filterByLastUpdate('2011-03-14'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate('now'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate(array('max' => 'yesterday')); // WHERE Last_update > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastUpdate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahLongitudinalQuery The current query, for fluid interface
     */
    public function filterByLastUpdate($lastUpdate = null, $comparison = null)
    {
        if (is_array($lastUpdate)) {
            $useMinMax = false;
            if (isset($lastUpdate['min'])) {
                $this->addUsingAlias(SekolahLongitudinalPeer::LAST_UPDATE, $lastUpdate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastUpdate['max'])) {
                $this->addUsingAlias(SekolahLongitudinalPeer::LAST_UPDATE, $lastUpdate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SekolahLongitudinalPeer::LAST_UPDATE, $lastUpdate, $comparison);
    }

    /**
     * Filter the query on the Soft_delete column
     *
     * Example usage:
     * <code>
     * $query->filterBySoftDelete(1234); // WHERE Soft_delete = 1234
     * $query->filterBySoftDelete(array(12, 34)); // WHERE Soft_delete IN (12, 34)
     * $query->filterBySoftDelete(array('min' => 12)); // WHERE Soft_delete >= 12
     * $query->filterBySoftDelete(array('max' => 12)); // WHERE Soft_delete <= 12
     * </code>
     *
     * @param     mixed $softDelete The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahLongitudinalQuery The current query, for fluid interface
     */
    public function filterBySoftDelete($softDelete = null, $comparison = null)
    {
        if (is_array($softDelete)) {
            $useMinMax = false;
            if (isset($softDelete['min'])) {
                $this->addUsingAlias(SekolahLongitudinalPeer::SOFT_DELETE, $softDelete['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($softDelete['max'])) {
                $this->addUsingAlias(SekolahLongitudinalPeer::SOFT_DELETE, $softDelete['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SekolahLongitudinalPeer::SOFT_DELETE, $softDelete, $comparison);
    }

    /**
     * Filter the query on the last_sync column
     *
     * Example usage:
     * <code>
     * $query->filterByLastSync('2011-03-14'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync('now'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync(array('max' => 'yesterday')); // WHERE last_sync > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastSync The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahLongitudinalQuery The current query, for fluid interface
     */
    public function filterByLastSync($lastSync = null, $comparison = null)
    {
        if (is_array($lastSync)) {
            $useMinMax = false;
            if (isset($lastSync['min'])) {
                $this->addUsingAlias(SekolahLongitudinalPeer::LAST_SYNC, $lastSync['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastSync['max'])) {
                $this->addUsingAlias(SekolahLongitudinalPeer::LAST_SYNC, $lastSync['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SekolahLongitudinalPeer::LAST_SYNC, $lastSync, $comparison);
    }

    /**
     * Filter the query on the Updater_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdaterId('fooValue');   // WHERE Updater_ID = 'fooValue'
     * $query->filterByUpdaterId('%fooValue%'); // WHERE Updater_ID LIKE '%fooValue%'
     * </code>
     *
     * @param     string $updaterId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahLongitudinalQuery The current query, for fluid interface
     */
    public function filterByUpdaterId($updaterId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($updaterId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $updaterId)) {
                $updaterId = str_replace('*', '%', $updaterId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SekolahLongitudinalPeer::UPDATER_ID, $updaterId, $comparison);
    }

    /**
     * Filter the query by a related Sekolah object
     *
     * @param   Sekolah|PropelObjectCollection $sekolah The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahLongitudinalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySekolahRelatedBySekolahId($sekolah, $comparison = null)
    {
        if ($sekolah instanceof Sekolah) {
            return $this
                ->addUsingAlias(SekolahLongitudinalPeer::SEKOLAH_ID, $sekolah->getSekolahId(), $comparison);
        } elseif ($sekolah instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SekolahLongitudinalPeer::SEKOLAH_ID, $sekolah->toKeyValue('PrimaryKey', 'SekolahId'), $comparison);
        } else {
            throw new PropelException('filterBySekolahRelatedBySekolahId() only accepts arguments of type Sekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SekolahRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahLongitudinalQuery The current query, for fluid interface
     */
    public function joinSekolahRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SekolahRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SekolahRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the SekolahRelatedBySekolahId relation Sekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SekolahQuery A secondary query class using the current class as primary query
     */
    public function useSekolahRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSekolahRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SekolahRelatedBySekolahId', '\angulex\Model\SekolahQuery');
    }

    /**
     * Filter the query by a related Sekolah object
     *
     * @param   Sekolah|PropelObjectCollection $sekolah The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahLongitudinalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySekolahRelatedBySekolahId($sekolah, $comparison = null)
    {
        if ($sekolah instanceof Sekolah) {
            return $this
                ->addUsingAlias(SekolahLongitudinalPeer::SEKOLAH_ID, $sekolah->getSekolahId(), $comparison);
        } elseif ($sekolah instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SekolahLongitudinalPeer::SEKOLAH_ID, $sekolah->toKeyValue('PrimaryKey', 'SekolahId'), $comparison);
        } else {
            throw new PropelException('filterBySekolahRelatedBySekolahId() only accepts arguments of type Sekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SekolahRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahLongitudinalQuery The current query, for fluid interface
     */
    public function joinSekolahRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SekolahRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SekolahRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the SekolahRelatedBySekolahId relation Sekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SekolahQuery A secondary query class using the current class as primary query
     */
    public function useSekolahRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSekolahRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SekolahRelatedBySekolahId', '\angulex\Model\SekolahQuery');
    }

    /**
     * Filter the query by a related AksesInternet object
     *
     * @param   AksesInternet|PropelObjectCollection $aksesInternet The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahLongitudinalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByAksesInternetRelatedByAksesInternetId($aksesInternet, $comparison = null)
    {
        if ($aksesInternet instanceof AksesInternet) {
            return $this
                ->addUsingAlias(SekolahLongitudinalPeer::AKSES_INTERNET_ID, $aksesInternet->getAksesInternetId(), $comparison);
        } elseif ($aksesInternet instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SekolahLongitudinalPeer::AKSES_INTERNET_ID, $aksesInternet->toKeyValue('PrimaryKey', 'AksesInternetId'), $comparison);
        } else {
            throw new PropelException('filterByAksesInternetRelatedByAksesInternetId() only accepts arguments of type AksesInternet or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the AksesInternetRelatedByAksesInternetId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahLongitudinalQuery The current query, for fluid interface
     */
    public function joinAksesInternetRelatedByAksesInternetId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('AksesInternetRelatedByAksesInternetId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'AksesInternetRelatedByAksesInternetId');
        }

        return $this;
    }

    /**
     * Use the AksesInternetRelatedByAksesInternetId relation AksesInternet object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\AksesInternetQuery A secondary query class using the current class as primary query
     */
    public function useAksesInternetRelatedByAksesInternetIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinAksesInternetRelatedByAksesInternetId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'AksesInternetRelatedByAksesInternetId', '\angulex\Model\AksesInternetQuery');
    }

    /**
     * Filter the query by a related AksesInternet object
     *
     * @param   AksesInternet|PropelObjectCollection $aksesInternet The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahLongitudinalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByAksesInternetRelatedByAksesInternet2Id($aksesInternet, $comparison = null)
    {
        if ($aksesInternet instanceof AksesInternet) {
            return $this
                ->addUsingAlias(SekolahLongitudinalPeer::AKSES_INTERNET_2_ID, $aksesInternet->getAksesInternetId(), $comparison);
        } elseif ($aksesInternet instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SekolahLongitudinalPeer::AKSES_INTERNET_2_ID, $aksesInternet->toKeyValue('PrimaryKey', 'AksesInternetId'), $comparison);
        } else {
            throw new PropelException('filterByAksesInternetRelatedByAksesInternet2Id() only accepts arguments of type AksesInternet or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the AksesInternetRelatedByAksesInternet2Id relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahLongitudinalQuery The current query, for fluid interface
     */
    public function joinAksesInternetRelatedByAksesInternet2Id($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('AksesInternetRelatedByAksesInternet2Id');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'AksesInternetRelatedByAksesInternet2Id');
        }

        return $this;
    }

    /**
     * Use the AksesInternetRelatedByAksesInternet2Id relation AksesInternet object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\AksesInternetQuery A secondary query class using the current class as primary query
     */
    public function useAksesInternetRelatedByAksesInternet2IdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinAksesInternetRelatedByAksesInternet2Id($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'AksesInternetRelatedByAksesInternet2Id', '\angulex\Model\AksesInternetQuery');
    }

    /**
     * Filter the query by a related AksesInternet object
     *
     * @param   AksesInternet|PropelObjectCollection $aksesInternet The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahLongitudinalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByAksesInternetRelatedByAksesInternetId($aksesInternet, $comparison = null)
    {
        if ($aksesInternet instanceof AksesInternet) {
            return $this
                ->addUsingAlias(SekolahLongitudinalPeer::AKSES_INTERNET_ID, $aksesInternet->getAksesInternetId(), $comparison);
        } elseif ($aksesInternet instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SekolahLongitudinalPeer::AKSES_INTERNET_ID, $aksesInternet->toKeyValue('PrimaryKey', 'AksesInternetId'), $comparison);
        } else {
            throw new PropelException('filterByAksesInternetRelatedByAksesInternetId() only accepts arguments of type AksesInternet or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the AksesInternetRelatedByAksesInternetId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahLongitudinalQuery The current query, for fluid interface
     */
    public function joinAksesInternetRelatedByAksesInternetId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('AksesInternetRelatedByAksesInternetId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'AksesInternetRelatedByAksesInternetId');
        }

        return $this;
    }

    /**
     * Use the AksesInternetRelatedByAksesInternetId relation AksesInternet object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\AksesInternetQuery A secondary query class using the current class as primary query
     */
    public function useAksesInternetRelatedByAksesInternetIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinAksesInternetRelatedByAksesInternetId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'AksesInternetRelatedByAksesInternetId', '\angulex\Model\AksesInternetQuery');
    }

    /**
     * Filter the query by a related AksesInternet object
     *
     * @param   AksesInternet|PropelObjectCollection $aksesInternet The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahLongitudinalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByAksesInternetRelatedByAksesInternet2Id($aksesInternet, $comparison = null)
    {
        if ($aksesInternet instanceof AksesInternet) {
            return $this
                ->addUsingAlias(SekolahLongitudinalPeer::AKSES_INTERNET_2_ID, $aksesInternet->getAksesInternetId(), $comparison);
        } elseif ($aksesInternet instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SekolahLongitudinalPeer::AKSES_INTERNET_2_ID, $aksesInternet->toKeyValue('PrimaryKey', 'AksesInternetId'), $comparison);
        } else {
            throw new PropelException('filterByAksesInternetRelatedByAksesInternet2Id() only accepts arguments of type AksesInternet or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the AksesInternetRelatedByAksesInternet2Id relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahLongitudinalQuery The current query, for fluid interface
     */
    public function joinAksesInternetRelatedByAksesInternet2Id($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('AksesInternetRelatedByAksesInternet2Id');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'AksesInternetRelatedByAksesInternet2Id');
        }

        return $this;
    }

    /**
     * Use the AksesInternetRelatedByAksesInternet2Id relation AksesInternet object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\AksesInternetQuery A secondary query class using the current class as primary query
     */
    public function useAksesInternetRelatedByAksesInternet2IdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinAksesInternetRelatedByAksesInternet2Id($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'AksesInternetRelatedByAksesInternet2Id', '\angulex\Model\AksesInternetQuery');
    }

    /**
     * Filter the query by a related Semester object
     *
     * @param   Semester|PropelObjectCollection $semester The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahLongitudinalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySemesterRelatedBySemesterId($semester, $comparison = null)
    {
        if ($semester instanceof Semester) {
            return $this
                ->addUsingAlias(SekolahLongitudinalPeer::SEMESTER_ID, $semester->getSemesterId(), $comparison);
        } elseif ($semester instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SekolahLongitudinalPeer::SEMESTER_ID, $semester->toKeyValue('PrimaryKey', 'SemesterId'), $comparison);
        } else {
            throw new PropelException('filterBySemesterRelatedBySemesterId() only accepts arguments of type Semester or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SemesterRelatedBySemesterId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahLongitudinalQuery The current query, for fluid interface
     */
    public function joinSemesterRelatedBySemesterId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SemesterRelatedBySemesterId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SemesterRelatedBySemesterId');
        }

        return $this;
    }

    /**
     * Use the SemesterRelatedBySemesterId relation Semester object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SemesterQuery A secondary query class using the current class as primary query
     */
    public function useSemesterRelatedBySemesterIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSemesterRelatedBySemesterId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SemesterRelatedBySemesterId', '\angulex\Model\SemesterQuery');
    }

    /**
     * Filter the query by a related Semester object
     *
     * @param   Semester|PropelObjectCollection $semester The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahLongitudinalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySemesterRelatedBySemesterId($semester, $comparison = null)
    {
        if ($semester instanceof Semester) {
            return $this
                ->addUsingAlias(SekolahLongitudinalPeer::SEMESTER_ID, $semester->getSemesterId(), $comparison);
        } elseif ($semester instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SekolahLongitudinalPeer::SEMESTER_ID, $semester->toKeyValue('PrimaryKey', 'SemesterId'), $comparison);
        } else {
            throw new PropelException('filterBySemesterRelatedBySemesterId() only accepts arguments of type Semester or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SemesterRelatedBySemesterId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahLongitudinalQuery The current query, for fluid interface
     */
    public function joinSemesterRelatedBySemesterId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SemesterRelatedBySemesterId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SemesterRelatedBySemesterId');
        }

        return $this;
    }

    /**
     * Use the SemesterRelatedBySemesterId relation Semester object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SemesterQuery A secondary query class using the current class as primary query
     */
    public function useSemesterRelatedBySemesterIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSemesterRelatedBySemesterId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SemesterRelatedBySemesterId', '\angulex\Model\SemesterQuery');
    }

    /**
     * Filter the query by a related SertifikasiIso object
     *
     * @param   SertifikasiIso|PropelObjectCollection $sertifikasiIso The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahLongitudinalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySertifikasiIsoRelatedBySertifikasiIsoId($sertifikasiIso, $comparison = null)
    {
        if ($sertifikasiIso instanceof SertifikasiIso) {
            return $this
                ->addUsingAlias(SekolahLongitudinalPeer::SERTIFIKASI_ISO_ID, $sertifikasiIso->getSertifikasiIsoId(), $comparison);
        } elseif ($sertifikasiIso instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SekolahLongitudinalPeer::SERTIFIKASI_ISO_ID, $sertifikasiIso->toKeyValue('PrimaryKey', 'SertifikasiIsoId'), $comparison);
        } else {
            throw new PropelException('filterBySertifikasiIsoRelatedBySertifikasiIsoId() only accepts arguments of type SertifikasiIso or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SertifikasiIsoRelatedBySertifikasiIsoId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahLongitudinalQuery The current query, for fluid interface
     */
    public function joinSertifikasiIsoRelatedBySertifikasiIsoId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SertifikasiIsoRelatedBySertifikasiIsoId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SertifikasiIsoRelatedBySertifikasiIsoId');
        }

        return $this;
    }

    /**
     * Use the SertifikasiIsoRelatedBySertifikasiIsoId relation SertifikasiIso object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SertifikasiIsoQuery A secondary query class using the current class as primary query
     */
    public function useSertifikasiIsoRelatedBySertifikasiIsoIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSertifikasiIsoRelatedBySertifikasiIsoId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SertifikasiIsoRelatedBySertifikasiIsoId', '\angulex\Model\SertifikasiIsoQuery');
    }

    /**
     * Filter the query by a related SertifikasiIso object
     *
     * @param   SertifikasiIso|PropelObjectCollection $sertifikasiIso The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahLongitudinalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySertifikasiIsoRelatedBySertifikasiIsoId($sertifikasiIso, $comparison = null)
    {
        if ($sertifikasiIso instanceof SertifikasiIso) {
            return $this
                ->addUsingAlias(SekolahLongitudinalPeer::SERTIFIKASI_ISO_ID, $sertifikasiIso->getSertifikasiIsoId(), $comparison);
        } elseif ($sertifikasiIso instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SekolahLongitudinalPeer::SERTIFIKASI_ISO_ID, $sertifikasiIso->toKeyValue('PrimaryKey', 'SertifikasiIsoId'), $comparison);
        } else {
            throw new PropelException('filterBySertifikasiIsoRelatedBySertifikasiIsoId() only accepts arguments of type SertifikasiIso or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SertifikasiIsoRelatedBySertifikasiIsoId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahLongitudinalQuery The current query, for fluid interface
     */
    public function joinSertifikasiIsoRelatedBySertifikasiIsoId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SertifikasiIsoRelatedBySertifikasiIsoId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SertifikasiIsoRelatedBySertifikasiIsoId');
        }

        return $this;
    }

    /**
     * Use the SertifikasiIsoRelatedBySertifikasiIsoId relation SertifikasiIso object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SertifikasiIsoQuery A secondary query class using the current class as primary query
     */
    public function useSertifikasiIsoRelatedBySertifikasiIsoIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSertifikasiIsoRelatedBySertifikasiIsoId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SertifikasiIsoRelatedBySertifikasiIsoId', '\angulex\Model\SertifikasiIsoQuery');
    }

    /**
     * Filter the query by a related SumberListrik object
     *
     * @param   SumberListrik|PropelObjectCollection $sumberListrik The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahLongitudinalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySumberListrikRelatedBySumberListrikId($sumberListrik, $comparison = null)
    {
        if ($sumberListrik instanceof SumberListrik) {
            return $this
                ->addUsingAlias(SekolahLongitudinalPeer::SUMBER_LISTRIK_ID, $sumberListrik->getSumberListrikId(), $comparison);
        } elseif ($sumberListrik instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SekolahLongitudinalPeer::SUMBER_LISTRIK_ID, $sumberListrik->toKeyValue('PrimaryKey', 'SumberListrikId'), $comparison);
        } else {
            throw new PropelException('filterBySumberListrikRelatedBySumberListrikId() only accepts arguments of type SumberListrik or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SumberListrikRelatedBySumberListrikId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahLongitudinalQuery The current query, for fluid interface
     */
    public function joinSumberListrikRelatedBySumberListrikId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SumberListrikRelatedBySumberListrikId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SumberListrikRelatedBySumberListrikId');
        }

        return $this;
    }

    /**
     * Use the SumberListrikRelatedBySumberListrikId relation SumberListrik object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SumberListrikQuery A secondary query class using the current class as primary query
     */
    public function useSumberListrikRelatedBySumberListrikIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSumberListrikRelatedBySumberListrikId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SumberListrikRelatedBySumberListrikId', '\angulex\Model\SumberListrikQuery');
    }

    /**
     * Filter the query by a related SumberListrik object
     *
     * @param   SumberListrik|PropelObjectCollection $sumberListrik The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahLongitudinalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySumberListrikRelatedBySumberListrikId($sumberListrik, $comparison = null)
    {
        if ($sumberListrik instanceof SumberListrik) {
            return $this
                ->addUsingAlias(SekolahLongitudinalPeer::SUMBER_LISTRIK_ID, $sumberListrik->getSumberListrikId(), $comparison);
        } elseif ($sumberListrik instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SekolahLongitudinalPeer::SUMBER_LISTRIK_ID, $sumberListrik->toKeyValue('PrimaryKey', 'SumberListrikId'), $comparison);
        } else {
            throw new PropelException('filterBySumberListrikRelatedBySumberListrikId() only accepts arguments of type SumberListrik or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SumberListrikRelatedBySumberListrikId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahLongitudinalQuery The current query, for fluid interface
     */
    public function joinSumberListrikRelatedBySumberListrikId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SumberListrikRelatedBySumberListrikId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SumberListrikRelatedBySumberListrikId');
        }

        return $this;
    }

    /**
     * Use the SumberListrikRelatedBySumberListrikId relation SumberListrik object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SumberListrikQuery A secondary query class using the current class as primary query
     */
    public function useSumberListrikRelatedBySumberListrikIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSumberListrikRelatedBySumberListrikId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SumberListrikRelatedBySumberListrikId', '\angulex\Model\SumberListrikQuery');
    }

    /**
     * Filter the query by a related WaktuPenyelenggaraan object
     *
     * @param   WaktuPenyelenggaraan|PropelObjectCollection $waktuPenyelenggaraan The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahLongitudinalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByWaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId($waktuPenyelenggaraan, $comparison = null)
    {
        if ($waktuPenyelenggaraan instanceof WaktuPenyelenggaraan) {
            return $this
                ->addUsingAlias(SekolahLongitudinalPeer::WAKTU_PENYELENGGARAAN_ID, $waktuPenyelenggaraan->getWaktuPenyelenggaraanId(), $comparison);
        } elseif ($waktuPenyelenggaraan instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SekolahLongitudinalPeer::WAKTU_PENYELENGGARAAN_ID, $waktuPenyelenggaraan->toKeyValue('PrimaryKey', 'WaktuPenyelenggaraanId'), $comparison);
        } else {
            throw new PropelException('filterByWaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId() only accepts arguments of type WaktuPenyelenggaraan or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the WaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahLongitudinalQuery The current query, for fluid interface
     */
    public function joinWaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('WaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'WaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId');
        }

        return $this;
    }

    /**
     * Use the WaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId relation WaktuPenyelenggaraan object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\WaktuPenyelenggaraanQuery A secondary query class using the current class as primary query
     */
    public function useWaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinWaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'WaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId', '\angulex\Model\WaktuPenyelenggaraanQuery');
    }

    /**
     * Filter the query by a related WaktuPenyelenggaraan object
     *
     * @param   WaktuPenyelenggaraan|PropelObjectCollection $waktuPenyelenggaraan The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahLongitudinalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByWaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId($waktuPenyelenggaraan, $comparison = null)
    {
        if ($waktuPenyelenggaraan instanceof WaktuPenyelenggaraan) {
            return $this
                ->addUsingAlias(SekolahLongitudinalPeer::WAKTU_PENYELENGGARAAN_ID, $waktuPenyelenggaraan->getWaktuPenyelenggaraanId(), $comparison);
        } elseif ($waktuPenyelenggaraan instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SekolahLongitudinalPeer::WAKTU_PENYELENGGARAAN_ID, $waktuPenyelenggaraan->toKeyValue('PrimaryKey', 'WaktuPenyelenggaraanId'), $comparison);
        } else {
            throw new PropelException('filterByWaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId() only accepts arguments of type WaktuPenyelenggaraan or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the WaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahLongitudinalQuery The current query, for fluid interface
     */
    public function joinWaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('WaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'WaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId');
        }

        return $this;
    }

    /**
     * Use the WaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId relation WaktuPenyelenggaraan object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\WaktuPenyelenggaraanQuery A secondary query class using the current class as primary query
     */
    public function useWaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinWaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'WaktuPenyelenggaraanRelatedByWaktuPenyelenggaraanId', '\angulex\Model\WaktuPenyelenggaraanQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   SekolahLongitudinal $sekolahLongitudinal Object to remove from the list of results
     *
     * @return SekolahLongitudinalQuery The current query, for fluid interface
     */
    public function prune($sekolahLongitudinal = null)
    {
        if ($sekolahLongitudinal) {
            $this->addCond('pruneCond0', $this->getAliasedColName(SekolahLongitudinalPeer::SEKOLAH_ID), $sekolahLongitudinal->getSekolahId(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(SekolahLongitudinalPeer::SEMESTER_ID), $sekolahLongitudinal->getSemesterId(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

}
