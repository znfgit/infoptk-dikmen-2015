<?php

namespace angulex\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelDateTime;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\JenisSertifikasi;
use angulex\Model\JenisSertifikasiPeer;
use angulex\Model\JenisSertifikasiQuery;
use angulex\Model\KebutuhanKhusus;
use angulex\Model\KebutuhanKhususQuery;
use angulex\Model\RwySertifikasi;
use angulex\Model\RwySertifikasiQuery;

/**
 * Base class that represents a row from the 'ref.jenis_sertifikasi' table.
 *
 * 
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseJenisSertifikasi extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'angulex\\Model\\JenisSertifikasiPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        JenisSertifikasiPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id_jenis_sertifikasi field.
     * @var        string
     */
    protected $id_jenis_sertifikasi;

    /**
     * The value for the jenis_sertifikasi field.
     * @var        string
     */
    protected $jenis_sertifikasi;

    /**
     * The value for the prof_guru field.
     * @var        string
     */
    protected $prof_guru;

    /**
     * The value for the kepala_sekolah field.
     * @var        string
     */
    protected $kepala_sekolah;

    /**
     * The value for the laboran field.
     * @var        string
     */
    protected $laboran;

    /**
     * The value for the kebutuhan_khusus_id field.
     * @var        int
     */
    protected $kebutuhan_khusus_id;

    /**
     * The value for the create_date field.
     * @var        string
     */
    protected $create_date;

    /**
     * The value for the last_update field.
     * @var        string
     */
    protected $last_update;

    /**
     * The value for the expired_date field.
     * @var        string
     */
    protected $expired_date;

    /**
     * The value for the last_sync field.
     * @var        string
     */
    protected $last_sync;

    /**
     * @var        KebutuhanKhusus
     */
    protected $aKebutuhanKhusus;

    /**
     * @var        PropelObjectCollection|RwySertifikasi[] Collection to store aggregation of RwySertifikasi objects.
     */
    protected $collRwySertifikasisRelatedByIdJenisSertifikasi;
    protected $collRwySertifikasisRelatedByIdJenisSertifikasiPartial;

    /**
     * @var        PropelObjectCollection|RwySertifikasi[] Collection to store aggregation of RwySertifikasi objects.
     */
    protected $collRwySertifikasisRelatedByIdJenisSertifikasi;
    protected $collRwySertifikasisRelatedByIdJenisSertifikasiPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rwySertifikasisRelatedByIdJenisSertifikasiScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rwySertifikasisRelatedByIdJenisSertifikasiScheduledForDeletion = null;

    /**
     * Get the [id_jenis_sertifikasi] column value.
     * 
     * @return string
     */
    public function getIdJenisSertifikasi()
    {
        return $this->id_jenis_sertifikasi;
    }

    /**
     * Get the [jenis_sertifikasi] column value.
     * 
     * @return string
     */
    public function getJenisSertifikasi()
    {
        return $this->jenis_sertifikasi;
    }

    /**
     * Get the [prof_guru] column value.
     * 
     * @return string
     */
    public function getProfGuru()
    {
        return $this->prof_guru;
    }

    /**
     * Get the [kepala_sekolah] column value.
     * 
     * @return string
     */
    public function getKepalaSekolah()
    {
        return $this->kepala_sekolah;
    }

    /**
     * Get the [laboran] column value.
     * 
     * @return string
     */
    public function getLaboran()
    {
        return $this->laboran;
    }

    /**
     * Get the [kebutuhan_khusus_id] column value.
     * 
     * @return int
     */
    public function getKebutuhanKhususId()
    {
        return $this->kebutuhan_khusus_id;
    }

    /**
     * Get the [optionally formatted] temporal [create_date] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreateDate($format = 'Y-m-d H:i:s')
    {
        if ($this->create_date === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->create_date);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->create_date, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [last_update] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastUpdate($format = 'Y-m-d H:i:s')
    {
        if ($this->last_update === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_update);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_update, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [expired_date] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getExpiredDate($format = 'Y-m-d H:i:s')
    {
        if ($this->expired_date === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->expired_date);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->expired_date, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [last_sync] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastSync($format = 'Y-m-d H:i:s')
    {
        if ($this->last_sync === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_sync);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_sync, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Set the value of [id_jenis_sertifikasi] column.
     * 
     * @param string $v new value
     * @return JenisSertifikasi The current object (for fluent API support)
     */
    public function setIdJenisSertifikasi($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->id_jenis_sertifikasi !== $v) {
            $this->id_jenis_sertifikasi = $v;
            $this->modifiedColumns[] = JenisSertifikasiPeer::ID_JENIS_SERTIFIKASI;
        }


        return $this;
    } // setIdJenisSertifikasi()

    /**
     * Set the value of [jenis_sertifikasi] column.
     * 
     * @param string $v new value
     * @return JenisSertifikasi The current object (for fluent API support)
     */
    public function setJenisSertifikasi($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->jenis_sertifikasi !== $v) {
            $this->jenis_sertifikasi = $v;
            $this->modifiedColumns[] = JenisSertifikasiPeer::JENIS_SERTIFIKASI;
        }


        return $this;
    } // setJenisSertifikasi()

    /**
     * Set the value of [prof_guru] column.
     * 
     * @param string $v new value
     * @return JenisSertifikasi The current object (for fluent API support)
     */
    public function setProfGuru($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->prof_guru !== $v) {
            $this->prof_guru = $v;
            $this->modifiedColumns[] = JenisSertifikasiPeer::PROF_GURU;
        }


        return $this;
    } // setProfGuru()

    /**
     * Set the value of [kepala_sekolah] column.
     * 
     * @param string $v new value
     * @return JenisSertifikasi The current object (for fluent API support)
     */
    public function setKepalaSekolah($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->kepala_sekolah !== $v) {
            $this->kepala_sekolah = $v;
            $this->modifiedColumns[] = JenisSertifikasiPeer::KEPALA_SEKOLAH;
        }


        return $this;
    } // setKepalaSekolah()

    /**
     * Set the value of [laboran] column.
     * 
     * @param string $v new value
     * @return JenisSertifikasi The current object (for fluent API support)
     */
    public function setLaboran($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->laboran !== $v) {
            $this->laboran = $v;
            $this->modifiedColumns[] = JenisSertifikasiPeer::LABORAN;
        }


        return $this;
    } // setLaboran()

    /**
     * Set the value of [kebutuhan_khusus_id] column.
     * 
     * @param int $v new value
     * @return JenisSertifikasi The current object (for fluent API support)
     */
    public function setKebutuhanKhususId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->kebutuhan_khusus_id !== $v) {
            $this->kebutuhan_khusus_id = $v;
            $this->modifiedColumns[] = JenisSertifikasiPeer::KEBUTUHAN_KHUSUS_ID;
        }

        if ($this->aKebutuhanKhusus !== null && $this->aKebutuhanKhusus->getKebutuhanKhususId() !== $v) {
            $this->aKebutuhanKhusus = null;
        }


        return $this;
    } // setKebutuhanKhususId()

    /**
     * Sets the value of [create_date] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return JenisSertifikasi The current object (for fluent API support)
     */
    public function setCreateDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->create_date !== null || $dt !== null) {
            $currentDateAsString = ($this->create_date !== null && $tmpDt = new DateTime($this->create_date)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->create_date = $newDateAsString;
                $this->modifiedColumns[] = JenisSertifikasiPeer::CREATE_DATE;
            }
        } // if either are not null


        return $this;
    } // setCreateDate()

    /**
     * Sets the value of [last_update] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return JenisSertifikasi The current object (for fluent API support)
     */
    public function setLastUpdate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_update !== null || $dt !== null) {
            $currentDateAsString = ($this->last_update !== null && $tmpDt = new DateTime($this->last_update)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_update = $newDateAsString;
                $this->modifiedColumns[] = JenisSertifikasiPeer::LAST_UPDATE;
            }
        } // if either are not null


        return $this;
    } // setLastUpdate()

    /**
     * Sets the value of [expired_date] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return JenisSertifikasi The current object (for fluent API support)
     */
    public function setExpiredDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->expired_date !== null || $dt !== null) {
            $currentDateAsString = ($this->expired_date !== null && $tmpDt = new DateTime($this->expired_date)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->expired_date = $newDateAsString;
                $this->modifiedColumns[] = JenisSertifikasiPeer::EXPIRED_DATE;
            }
        } // if either are not null


        return $this;
    } // setExpiredDate()

    /**
     * Sets the value of [last_sync] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return JenisSertifikasi The current object (for fluent API support)
     */
    public function setLastSync($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_sync !== null || $dt !== null) {
            $currentDateAsString = ($this->last_sync !== null && $tmpDt = new DateTime($this->last_sync)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_sync = $newDateAsString;
                $this->modifiedColumns[] = JenisSertifikasiPeer::LAST_SYNC;
            }
        } // if either are not null


        return $this;
    } // setLastSync()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id_jenis_sertifikasi = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
            $this->jenis_sertifikasi = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->prof_guru = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->kepala_sekolah = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->laboran = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->kebutuhan_khusus_id = ($row[$startcol + 5] !== null) ? (int) $row[$startcol + 5] : null;
            $this->create_date = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->last_update = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->expired_date = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->last_sync = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 10; // 10 = JenisSertifikasiPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating JenisSertifikasi object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aKebutuhanKhusus !== null && $this->kebutuhan_khusus_id !== $this->aKebutuhanKhusus->getKebutuhanKhususId()) {
            $this->aKebutuhanKhusus = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(JenisSertifikasiPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = JenisSertifikasiPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aKebutuhanKhusus = null;
            $this->collRwySertifikasisRelatedByIdJenisSertifikasi = null;

            $this->collRwySertifikasisRelatedByIdJenisSertifikasi = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(JenisSertifikasiPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = JenisSertifikasiQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(JenisSertifikasiPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                JenisSertifikasiPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aKebutuhanKhusus !== null) {
                if ($this->aKebutuhanKhusus->isModified() || $this->aKebutuhanKhusus->isNew()) {
                    $affectedRows += $this->aKebutuhanKhusus->save($con);
                }
                $this->setKebutuhanKhusus($this->aKebutuhanKhusus);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->rwySertifikasisRelatedByIdJenisSertifikasiScheduledForDeletion !== null) {
                if (!$this->rwySertifikasisRelatedByIdJenisSertifikasiScheduledForDeletion->isEmpty()) {
                    RwySertifikasiQuery::create()
                        ->filterByPrimaryKeys($this->rwySertifikasisRelatedByIdJenisSertifikasiScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rwySertifikasisRelatedByIdJenisSertifikasiScheduledForDeletion = null;
                }
            }

            if ($this->collRwySertifikasisRelatedByIdJenisSertifikasi !== null) {
                foreach ($this->collRwySertifikasisRelatedByIdJenisSertifikasi as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rwySertifikasisRelatedByIdJenisSertifikasiScheduledForDeletion !== null) {
                if (!$this->rwySertifikasisRelatedByIdJenisSertifikasiScheduledForDeletion->isEmpty()) {
                    RwySertifikasiQuery::create()
                        ->filterByPrimaryKeys($this->rwySertifikasisRelatedByIdJenisSertifikasiScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rwySertifikasisRelatedByIdJenisSertifikasiScheduledForDeletion = null;
                }
            }

            if ($this->collRwySertifikasisRelatedByIdJenisSertifikasi !== null) {
                foreach ($this->collRwySertifikasisRelatedByIdJenisSertifikasi as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $criteria = $this->buildCriteria();
        $pk = BasePeer::doInsert($criteria, $con);
        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aKebutuhanKhusus !== null) {
                if (!$this->aKebutuhanKhusus->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aKebutuhanKhusus->getValidationFailures());
                }
            }


            if (($retval = JenisSertifikasiPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collRwySertifikasisRelatedByIdJenisSertifikasi !== null) {
                    foreach ($this->collRwySertifikasisRelatedByIdJenisSertifikasi as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRwySertifikasisRelatedByIdJenisSertifikasi !== null) {
                    foreach ($this->collRwySertifikasisRelatedByIdJenisSertifikasi as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = JenisSertifikasiPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdJenisSertifikasi();
                break;
            case 1:
                return $this->getJenisSertifikasi();
                break;
            case 2:
                return $this->getProfGuru();
                break;
            case 3:
                return $this->getKepalaSekolah();
                break;
            case 4:
                return $this->getLaboran();
                break;
            case 5:
                return $this->getKebutuhanKhususId();
                break;
            case 6:
                return $this->getCreateDate();
                break;
            case 7:
                return $this->getLastUpdate();
                break;
            case 8:
                return $this->getExpiredDate();
                break;
            case 9:
                return $this->getLastSync();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['JenisSertifikasi'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['JenisSertifikasi'][$this->getPrimaryKey()] = true;
        $keys = JenisSertifikasiPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdJenisSertifikasi(),
            $keys[1] => $this->getJenisSertifikasi(),
            $keys[2] => $this->getProfGuru(),
            $keys[3] => $this->getKepalaSekolah(),
            $keys[4] => $this->getLaboran(),
            $keys[5] => $this->getKebutuhanKhususId(),
            $keys[6] => $this->getCreateDate(),
            $keys[7] => $this->getLastUpdate(),
            $keys[8] => $this->getExpiredDate(),
            $keys[9] => $this->getLastSync(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aKebutuhanKhusus) {
                $result['KebutuhanKhusus'] = $this->aKebutuhanKhusus->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collRwySertifikasisRelatedByIdJenisSertifikasi) {
                $result['RwySertifikasisRelatedByIdJenisSertifikasi'] = $this->collRwySertifikasisRelatedByIdJenisSertifikasi->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRwySertifikasisRelatedByIdJenisSertifikasi) {
                $result['RwySertifikasisRelatedByIdJenisSertifikasi'] = $this->collRwySertifikasisRelatedByIdJenisSertifikasi->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = JenisSertifikasiPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdJenisSertifikasi($value);
                break;
            case 1:
                $this->setJenisSertifikasi($value);
                break;
            case 2:
                $this->setProfGuru($value);
                break;
            case 3:
                $this->setKepalaSekolah($value);
                break;
            case 4:
                $this->setLaboran($value);
                break;
            case 5:
                $this->setKebutuhanKhususId($value);
                break;
            case 6:
                $this->setCreateDate($value);
                break;
            case 7:
                $this->setLastUpdate($value);
                break;
            case 8:
                $this->setExpiredDate($value);
                break;
            case 9:
                $this->setLastSync($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = JenisSertifikasiPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setIdJenisSertifikasi($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setJenisSertifikasi($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setProfGuru($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setKepalaSekolah($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setLaboran($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setKebutuhanKhususId($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setCreateDate($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setLastUpdate($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setExpiredDate($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setLastSync($arr[$keys[9]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(JenisSertifikasiPeer::DATABASE_NAME);

        if ($this->isColumnModified(JenisSertifikasiPeer::ID_JENIS_SERTIFIKASI)) $criteria->add(JenisSertifikasiPeer::ID_JENIS_SERTIFIKASI, $this->id_jenis_sertifikasi);
        if ($this->isColumnModified(JenisSertifikasiPeer::JENIS_SERTIFIKASI)) $criteria->add(JenisSertifikasiPeer::JENIS_SERTIFIKASI, $this->jenis_sertifikasi);
        if ($this->isColumnModified(JenisSertifikasiPeer::PROF_GURU)) $criteria->add(JenisSertifikasiPeer::PROF_GURU, $this->prof_guru);
        if ($this->isColumnModified(JenisSertifikasiPeer::KEPALA_SEKOLAH)) $criteria->add(JenisSertifikasiPeer::KEPALA_SEKOLAH, $this->kepala_sekolah);
        if ($this->isColumnModified(JenisSertifikasiPeer::LABORAN)) $criteria->add(JenisSertifikasiPeer::LABORAN, $this->laboran);
        if ($this->isColumnModified(JenisSertifikasiPeer::KEBUTUHAN_KHUSUS_ID)) $criteria->add(JenisSertifikasiPeer::KEBUTUHAN_KHUSUS_ID, $this->kebutuhan_khusus_id);
        if ($this->isColumnModified(JenisSertifikasiPeer::CREATE_DATE)) $criteria->add(JenisSertifikasiPeer::CREATE_DATE, $this->create_date);
        if ($this->isColumnModified(JenisSertifikasiPeer::LAST_UPDATE)) $criteria->add(JenisSertifikasiPeer::LAST_UPDATE, $this->last_update);
        if ($this->isColumnModified(JenisSertifikasiPeer::EXPIRED_DATE)) $criteria->add(JenisSertifikasiPeer::EXPIRED_DATE, $this->expired_date);
        if ($this->isColumnModified(JenisSertifikasiPeer::LAST_SYNC)) $criteria->add(JenisSertifikasiPeer::LAST_SYNC, $this->last_sync);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(JenisSertifikasiPeer::DATABASE_NAME);
        $criteria->add(JenisSertifikasiPeer::ID_JENIS_SERTIFIKASI, $this->id_jenis_sertifikasi);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getIdJenisSertifikasi();
    }

    /**
     * Generic method to set the primary key (id_jenis_sertifikasi column).
     *
     * @param  string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdJenisSertifikasi($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getIdJenisSertifikasi();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of JenisSertifikasi (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setJenisSertifikasi($this->getJenisSertifikasi());
        $copyObj->setProfGuru($this->getProfGuru());
        $copyObj->setKepalaSekolah($this->getKepalaSekolah());
        $copyObj->setLaboran($this->getLaboran());
        $copyObj->setKebutuhanKhususId($this->getKebutuhanKhususId());
        $copyObj->setCreateDate($this->getCreateDate());
        $copyObj->setLastUpdate($this->getLastUpdate());
        $copyObj->setExpiredDate($this->getExpiredDate());
        $copyObj->setLastSync($this->getLastSync());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getRwySertifikasisRelatedByIdJenisSertifikasi() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRwySertifikasiRelatedByIdJenisSertifikasi($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRwySertifikasisRelatedByIdJenisSertifikasi() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRwySertifikasiRelatedByIdJenisSertifikasi($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdJenisSertifikasi(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return JenisSertifikasi Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return JenisSertifikasiPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new JenisSertifikasiPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a KebutuhanKhusus object.
     *
     * @param             KebutuhanKhusus $v
     * @return JenisSertifikasi The current object (for fluent API support)
     * @throws PropelException
     */
    public function setKebutuhanKhusus(KebutuhanKhusus $v = null)
    {
        if ($v === null) {
            $this->setKebutuhanKhususId(NULL);
        } else {
            $this->setKebutuhanKhususId($v->getKebutuhanKhususId());
        }

        $this->aKebutuhanKhusus = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the KebutuhanKhusus object, it will not be re-added.
        if ($v !== null) {
            $v->addJenisSertifikasi($this);
        }


        return $this;
    }


    /**
     * Get the associated KebutuhanKhusus object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return KebutuhanKhusus The associated KebutuhanKhusus object.
     * @throws PropelException
     */
    public function getKebutuhanKhusus(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aKebutuhanKhusus === null && ($this->kebutuhan_khusus_id !== null) && $doQuery) {
            $this->aKebutuhanKhusus = KebutuhanKhususQuery::create()->findPk($this->kebutuhan_khusus_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aKebutuhanKhusus->addJenisSertifikasis($this);
             */
        }

        return $this->aKebutuhanKhusus;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('RwySertifikasiRelatedByIdJenisSertifikasi' == $relationName) {
            $this->initRwySertifikasisRelatedByIdJenisSertifikasi();
        }
        if ('RwySertifikasiRelatedByIdJenisSertifikasi' == $relationName) {
            $this->initRwySertifikasisRelatedByIdJenisSertifikasi();
        }
    }

    /**
     * Clears out the collRwySertifikasisRelatedByIdJenisSertifikasi collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return JenisSertifikasi The current object (for fluent API support)
     * @see        addRwySertifikasisRelatedByIdJenisSertifikasi()
     */
    public function clearRwySertifikasisRelatedByIdJenisSertifikasi()
    {
        $this->collRwySertifikasisRelatedByIdJenisSertifikasi = null; // important to set this to null since that means it is uninitialized
        $this->collRwySertifikasisRelatedByIdJenisSertifikasiPartial = null;

        return $this;
    }

    /**
     * reset is the collRwySertifikasisRelatedByIdJenisSertifikasi collection loaded partially
     *
     * @return void
     */
    public function resetPartialRwySertifikasisRelatedByIdJenisSertifikasi($v = true)
    {
        $this->collRwySertifikasisRelatedByIdJenisSertifikasiPartial = $v;
    }

    /**
     * Initializes the collRwySertifikasisRelatedByIdJenisSertifikasi collection.
     *
     * By default this just sets the collRwySertifikasisRelatedByIdJenisSertifikasi collection to an empty array (like clearcollRwySertifikasisRelatedByIdJenisSertifikasi());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRwySertifikasisRelatedByIdJenisSertifikasi($overrideExisting = true)
    {
        if (null !== $this->collRwySertifikasisRelatedByIdJenisSertifikasi && !$overrideExisting) {
            return;
        }
        $this->collRwySertifikasisRelatedByIdJenisSertifikasi = new PropelObjectCollection();
        $this->collRwySertifikasisRelatedByIdJenisSertifikasi->setModel('RwySertifikasi');
    }

    /**
     * Gets an array of RwySertifikasi objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this JenisSertifikasi is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RwySertifikasi[] List of RwySertifikasi objects
     * @throws PropelException
     */
    public function getRwySertifikasisRelatedByIdJenisSertifikasi($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRwySertifikasisRelatedByIdJenisSertifikasiPartial && !$this->isNew();
        if (null === $this->collRwySertifikasisRelatedByIdJenisSertifikasi || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRwySertifikasisRelatedByIdJenisSertifikasi) {
                // return empty collection
                $this->initRwySertifikasisRelatedByIdJenisSertifikasi();
            } else {
                $collRwySertifikasisRelatedByIdJenisSertifikasi = RwySertifikasiQuery::create(null, $criteria)
                    ->filterByJenisSertifikasiRelatedByIdJenisSertifikasi($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRwySertifikasisRelatedByIdJenisSertifikasiPartial && count($collRwySertifikasisRelatedByIdJenisSertifikasi)) {
                      $this->initRwySertifikasisRelatedByIdJenisSertifikasi(false);

                      foreach($collRwySertifikasisRelatedByIdJenisSertifikasi as $obj) {
                        if (false == $this->collRwySertifikasisRelatedByIdJenisSertifikasi->contains($obj)) {
                          $this->collRwySertifikasisRelatedByIdJenisSertifikasi->append($obj);
                        }
                      }

                      $this->collRwySertifikasisRelatedByIdJenisSertifikasiPartial = true;
                    }

                    $collRwySertifikasisRelatedByIdJenisSertifikasi->getInternalIterator()->rewind();
                    return $collRwySertifikasisRelatedByIdJenisSertifikasi;
                }

                if($partial && $this->collRwySertifikasisRelatedByIdJenisSertifikasi) {
                    foreach($this->collRwySertifikasisRelatedByIdJenisSertifikasi as $obj) {
                        if($obj->isNew()) {
                            $collRwySertifikasisRelatedByIdJenisSertifikasi[] = $obj;
                        }
                    }
                }

                $this->collRwySertifikasisRelatedByIdJenisSertifikasi = $collRwySertifikasisRelatedByIdJenisSertifikasi;
                $this->collRwySertifikasisRelatedByIdJenisSertifikasiPartial = false;
            }
        }

        return $this->collRwySertifikasisRelatedByIdJenisSertifikasi;
    }

    /**
     * Sets a collection of RwySertifikasiRelatedByIdJenisSertifikasi objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rwySertifikasisRelatedByIdJenisSertifikasi A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return JenisSertifikasi The current object (for fluent API support)
     */
    public function setRwySertifikasisRelatedByIdJenisSertifikasi(PropelCollection $rwySertifikasisRelatedByIdJenisSertifikasi, PropelPDO $con = null)
    {
        $rwySertifikasisRelatedByIdJenisSertifikasiToDelete = $this->getRwySertifikasisRelatedByIdJenisSertifikasi(new Criteria(), $con)->diff($rwySertifikasisRelatedByIdJenisSertifikasi);

        $this->rwySertifikasisRelatedByIdJenisSertifikasiScheduledForDeletion = unserialize(serialize($rwySertifikasisRelatedByIdJenisSertifikasiToDelete));

        foreach ($rwySertifikasisRelatedByIdJenisSertifikasiToDelete as $rwySertifikasiRelatedByIdJenisSertifikasiRemoved) {
            $rwySertifikasiRelatedByIdJenisSertifikasiRemoved->setJenisSertifikasiRelatedByIdJenisSertifikasi(null);
        }

        $this->collRwySertifikasisRelatedByIdJenisSertifikasi = null;
        foreach ($rwySertifikasisRelatedByIdJenisSertifikasi as $rwySertifikasiRelatedByIdJenisSertifikasi) {
            $this->addRwySertifikasiRelatedByIdJenisSertifikasi($rwySertifikasiRelatedByIdJenisSertifikasi);
        }

        $this->collRwySertifikasisRelatedByIdJenisSertifikasi = $rwySertifikasisRelatedByIdJenisSertifikasi;
        $this->collRwySertifikasisRelatedByIdJenisSertifikasiPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RwySertifikasi objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RwySertifikasi objects.
     * @throws PropelException
     */
    public function countRwySertifikasisRelatedByIdJenisSertifikasi(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRwySertifikasisRelatedByIdJenisSertifikasiPartial && !$this->isNew();
        if (null === $this->collRwySertifikasisRelatedByIdJenisSertifikasi || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRwySertifikasisRelatedByIdJenisSertifikasi) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getRwySertifikasisRelatedByIdJenisSertifikasi());
            }
            $query = RwySertifikasiQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByJenisSertifikasiRelatedByIdJenisSertifikasi($this)
                ->count($con);
        }

        return count($this->collRwySertifikasisRelatedByIdJenisSertifikasi);
    }

    /**
     * Method called to associate a RwySertifikasi object to this object
     * through the RwySertifikasi foreign key attribute.
     *
     * @param    RwySertifikasi $l RwySertifikasi
     * @return JenisSertifikasi The current object (for fluent API support)
     */
    public function addRwySertifikasiRelatedByIdJenisSertifikasi(RwySertifikasi $l)
    {
        if ($this->collRwySertifikasisRelatedByIdJenisSertifikasi === null) {
            $this->initRwySertifikasisRelatedByIdJenisSertifikasi();
            $this->collRwySertifikasisRelatedByIdJenisSertifikasiPartial = true;
        }
        if (!in_array($l, $this->collRwySertifikasisRelatedByIdJenisSertifikasi->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRwySertifikasiRelatedByIdJenisSertifikasi($l);
        }

        return $this;
    }

    /**
     * @param	RwySertifikasiRelatedByIdJenisSertifikasi $rwySertifikasiRelatedByIdJenisSertifikasi The rwySertifikasiRelatedByIdJenisSertifikasi object to add.
     */
    protected function doAddRwySertifikasiRelatedByIdJenisSertifikasi($rwySertifikasiRelatedByIdJenisSertifikasi)
    {
        $this->collRwySertifikasisRelatedByIdJenisSertifikasi[]= $rwySertifikasiRelatedByIdJenisSertifikasi;
        $rwySertifikasiRelatedByIdJenisSertifikasi->setJenisSertifikasiRelatedByIdJenisSertifikasi($this);
    }

    /**
     * @param	RwySertifikasiRelatedByIdJenisSertifikasi $rwySertifikasiRelatedByIdJenisSertifikasi The rwySertifikasiRelatedByIdJenisSertifikasi object to remove.
     * @return JenisSertifikasi The current object (for fluent API support)
     */
    public function removeRwySertifikasiRelatedByIdJenisSertifikasi($rwySertifikasiRelatedByIdJenisSertifikasi)
    {
        if ($this->getRwySertifikasisRelatedByIdJenisSertifikasi()->contains($rwySertifikasiRelatedByIdJenisSertifikasi)) {
            $this->collRwySertifikasisRelatedByIdJenisSertifikasi->remove($this->collRwySertifikasisRelatedByIdJenisSertifikasi->search($rwySertifikasiRelatedByIdJenisSertifikasi));
            if (null === $this->rwySertifikasisRelatedByIdJenisSertifikasiScheduledForDeletion) {
                $this->rwySertifikasisRelatedByIdJenisSertifikasiScheduledForDeletion = clone $this->collRwySertifikasisRelatedByIdJenisSertifikasi;
                $this->rwySertifikasisRelatedByIdJenisSertifikasiScheduledForDeletion->clear();
            }
            $this->rwySertifikasisRelatedByIdJenisSertifikasiScheduledForDeletion[]= clone $rwySertifikasiRelatedByIdJenisSertifikasi;
            $rwySertifikasiRelatedByIdJenisSertifikasi->setJenisSertifikasiRelatedByIdJenisSertifikasi(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisSertifikasi is new, it will return
     * an empty collection; or if this JenisSertifikasi has previously
     * been saved, it will retrieve related RwySertifikasisRelatedByIdJenisSertifikasi from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisSertifikasi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwySertifikasi[] List of RwySertifikasi objects
     */
    public function getRwySertifikasisRelatedByIdJenisSertifikasiJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwySertifikasiQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getRwySertifikasisRelatedByIdJenisSertifikasi($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisSertifikasi is new, it will return
     * an empty collection; or if this JenisSertifikasi has previously
     * been saved, it will retrieve related RwySertifikasisRelatedByIdJenisSertifikasi from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisSertifikasi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwySertifikasi[] List of RwySertifikasi objects
     */
    public function getRwySertifikasisRelatedByIdJenisSertifikasiJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwySertifikasiQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getRwySertifikasisRelatedByIdJenisSertifikasi($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisSertifikasi is new, it will return
     * an empty collection; or if this JenisSertifikasi has previously
     * been saved, it will retrieve related RwySertifikasisRelatedByIdJenisSertifikasi from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisSertifikasi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwySertifikasi[] List of RwySertifikasi objects
     */
    public function getRwySertifikasisRelatedByIdJenisSertifikasiJoinBidangStudiRelatedByBidangStudiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwySertifikasiQuery::create(null, $criteria);
        $query->joinWith('BidangStudiRelatedByBidangStudiId', $join_behavior);

        return $this->getRwySertifikasisRelatedByIdJenisSertifikasi($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisSertifikasi is new, it will return
     * an empty collection; or if this JenisSertifikasi has previously
     * been saved, it will retrieve related RwySertifikasisRelatedByIdJenisSertifikasi from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisSertifikasi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwySertifikasi[] List of RwySertifikasi objects
     */
    public function getRwySertifikasisRelatedByIdJenisSertifikasiJoinBidangStudiRelatedByBidangStudiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwySertifikasiQuery::create(null, $criteria);
        $query->joinWith('BidangStudiRelatedByBidangStudiId', $join_behavior);

        return $this->getRwySertifikasisRelatedByIdJenisSertifikasi($query, $con);
    }

    /**
     * Clears out the collRwySertifikasisRelatedByIdJenisSertifikasi collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return JenisSertifikasi The current object (for fluent API support)
     * @see        addRwySertifikasisRelatedByIdJenisSertifikasi()
     */
    public function clearRwySertifikasisRelatedByIdJenisSertifikasi()
    {
        $this->collRwySertifikasisRelatedByIdJenisSertifikasi = null; // important to set this to null since that means it is uninitialized
        $this->collRwySertifikasisRelatedByIdJenisSertifikasiPartial = null;

        return $this;
    }

    /**
     * reset is the collRwySertifikasisRelatedByIdJenisSertifikasi collection loaded partially
     *
     * @return void
     */
    public function resetPartialRwySertifikasisRelatedByIdJenisSertifikasi($v = true)
    {
        $this->collRwySertifikasisRelatedByIdJenisSertifikasiPartial = $v;
    }

    /**
     * Initializes the collRwySertifikasisRelatedByIdJenisSertifikasi collection.
     *
     * By default this just sets the collRwySertifikasisRelatedByIdJenisSertifikasi collection to an empty array (like clearcollRwySertifikasisRelatedByIdJenisSertifikasi());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRwySertifikasisRelatedByIdJenisSertifikasi($overrideExisting = true)
    {
        if (null !== $this->collRwySertifikasisRelatedByIdJenisSertifikasi && !$overrideExisting) {
            return;
        }
        $this->collRwySertifikasisRelatedByIdJenisSertifikasi = new PropelObjectCollection();
        $this->collRwySertifikasisRelatedByIdJenisSertifikasi->setModel('RwySertifikasi');
    }

    /**
     * Gets an array of RwySertifikasi objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this JenisSertifikasi is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RwySertifikasi[] List of RwySertifikasi objects
     * @throws PropelException
     */
    public function getRwySertifikasisRelatedByIdJenisSertifikasi($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRwySertifikasisRelatedByIdJenisSertifikasiPartial && !$this->isNew();
        if (null === $this->collRwySertifikasisRelatedByIdJenisSertifikasi || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRwySertifikasisRelatedByIdJenisSertifikasi) {
                // return empty collection
                $this->initRwySertifikasisRelatedByIdJenisSertifikasi();
            } else {
                $collRwySertifikasisRelatedByIdJenisSertifikasi = RwySertifikasiQuery::create(null, $criteria)
                    ->filterByJenisSertifikasiRelatedByIdJenisSertifikasi($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRwySertifikasisRelatedByIdJenisSertifikasiPartial && count($collRwySertifikasisRelatedByIdJenisSertifikasi)) {
                      $this->initRwySertifikasisRelatedByIdJenisSertifikasi(false);

                      foreach($collRwySertifikasisRelatedByIdJenisSertifikasi as $obj) {
                        if (false == $this->collRwySertifikasisRelatedByIdJenisSertifikasi->contains($obj)) {
                          $this->collRwySertifikasisRelatedByIdJenisSertifikasi->append($obj);
                        }
                      }

                      $this->collRwySertifikasisRelatedByIdJenisSertifikasiPartial = true;
                    }

                    $collRwySertifikasisRelatedByIdJenisSertifikasi->getInternalIterator()->rewind();
                    return $collRwySertifikasisRelatedByIdJenisSertifikasi;
                }

                if($partial && $this->collRwySertifikasisRelatedByIdJenisSertifikasi) {
                    foreach($this->collRwySertifikasisRelatedByIdJenisSertifikasi as $obj) {
                        if($obj->isNew()) {
                            $collRwySertifikasisRelatedByIdJenisSertifikasi[] = $obj;
                        }
                    }
                }

                $this->collRwySertifikasisRelatedByIdJenisSertifikasi = $collRwySertifikasisRelatedByIdJenisSertifikasi;
                $this->collRwySertifikasisRelatedByIdJenisSertifikasiPartial = false;
            }
        }

        return $this->collRwySertifikasisRelatedByIdJenisSertifikasi;
    }

    /**
     * Sets a collection of RwySertifikasiRelatedByIdJenisSertifikasi objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rwySertifikasisRelatedByIdJenisSertifikasi A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return JenisSertifikasi The current object (for fluent API support)
     */
    public function setRwySertifikasisRelatedByIdJenisSertifikasi(PropelCollection $rwySertifikasisRelatedByIdJenisSertifikasi, PropelPDO $con = null)
    {
        $rwySertifikasisRelatedByIdJenisSertifikasiToDelete = $this->getRwySertifikasisRelatedByIdJenisSertifikasi(new Criteria(), $con)->diff($rwySertifikasisRelatedByIdJenisSertifikasi);

        $this->rwySertifikasisRelatedByIdJenisSertifikasiScheduledForDeletion = unserialize(serialize($rwySertifikasisRelatedByIdJenisSertifikasiToDelete));

        foreach ($rwySertifikasisRelatedByIdJenisSertifikasiToDelete as $rwySertifikasiRelatedByIdJenisSertifikasiRemoved) {
            $rwySertifikasiRelatedByIdJenisSertifikasiRemoved->setJenisSertifikasiRelatedByIdJenisSertifikasi(null);
        }

        $this->collRwySertifikasisRelatedByIdJenisSertifikasi = null;
        foreach ($rwySertifikasisRelatedByIdJenisSertifikasi as $rwySertifikasiRelatedByIdJenisSertifikasi) {
            $this->addRwySertifikasiRelatedByIdJenisSertifikasi($rwySertifikasiRelatedByIdJenisSertifikasi);
        }

        $this->collRwySertifikasisRelatedByIdJenisSertifikasi = $rwySertifikasisRelatedByIdJenisSertifikasi;
        $this->collRwySertifikasisRelatedByIdJenisSertifikasiPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RwySertifikasi objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RwySertifikasi objects.
     * @throws PropelException
     */
    public function countRwySertifikasisRelatedByIdJenisSertifikasi(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRwySertifikasisRelatedByIdJenisSertifikasiPartial && !$this->isNew();
        if (null === $this->collRwySertifikasisRelatedByIdJenisSertifikasi || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRwySertifikasisRelatedByIdJenisSertifikasi) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getRwySertifikasisRelatedByIdJenisSertifikasi());
            }
            $query = RwySertifikasiQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByJenisSertifikasiRelatedByIdJenisSertifikasi($this)
                ->count($con);
        }

        return count($this->collRwySertifikasisRelatedByIdJenisSertifikasi);
    }

    /**
     * Method called to associate a RwySertifikasi object to this object
     * through the RwySertifikasi foreign key attribute.
     *
     * @param    RwySertifikasi $l RwySertifikasi
     * @return JenisSertifikasi The current object (for fluent API support)
     */
    public function addRwySertifikasiRelatedByIdJenisSertifikasi(RwySertifikasi $l)
    {
        if ($this->collRwySertifikasisRelatedByIdJenisSertifikasi === null) {
            $this->initRwySertifikasisRelatedByIdJenisSertifikasi();
            $this->collRwySertifikasisRelatedByIdJenisSertifikasiPartial = true;
        }
        if (!in_array($l, $this->collRwySertifikasisRelatedByIdJenisSertifikasi->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRwySertifikasiRelatedByIdJenisSertifikasi($l);
        }

        return $this;
    }

    /**
     * @param	RwySertifikasiRelatedByIdJenisSertifikasi $rwySertifikasiRelatedByIdJenisSertifikasi The rwySertifikasiRelatedByIdJenisSertifikasi object to add.
     */
    protected function doAddRwySertifikasiRelatedByIdJenisSertifikasi($rwySertifikasiRelatedByIdJenisSertifikasi)
    {
        $this->collRwySertifikasisRelatedByIdJenisSertifikasi[]= $rwySertifikasiRelatedByIdJenisSertifikasi;
        $rwySertifikasiRelatedByIdJenisSertifikasi->setJenisSertifikasiRelatedByIdJenisSertifikasi($this);
    }

    /**
     * @param	RwySertifikasiRelatedByIdJenisSertifikasi $rwySertifikasiRelatedByIdJenisSertifikasi The rwySertifikasiRelatedByIdJenisSertifikasi object to remove.
     * @return JenisSertifikasi The current object (for fluent API support)
     */
    public function removeRwySertifikasiRelatedByIdJenisSertifikasi($rwySertifikasiRelatedByIdJenisSertifikasi)
    {
        if ($this->getRwySertifikasisRelatedByIdJenisSertifikasi()->contains($rwySertifikasiRelatedByIdJenisSertifikasi)) {
            $this->collRwySertifikasisRelatedByIdJenisSertifikasi->remove($this->collRwySertifikasisRelatedByIdJenisSertifikasi->search($rwySertifikasiRelatedByIdJenisSertifikasi));
            if (null === $this->rwySertifikasisRelatedByIdJenisSertifikasiScheduledForDeletion) {
                $this->rwySertifikasisRelatedByIdJenisSertifikasiScheduledForDeletion = clone $this->collRwySertifikasisRelatedByIdJenisSertifikasi;
                $this->rwySertifikasisRelatedByIdJenisSertifikasiScheduledForDeletion->clear();
            }
            $this->rwySertifikasisRelatedByIdJenisSertifikasiScheduledForDeletion[]= clone $rwySertifikasiRelatedByIdJenisSertifikasi;
            $rwySertifikasiRelatedByIdJenisSertifikasi->setJenisSertifikasiRelatedByIdJenisSertifikasi(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisSertifikasi is new, it will return
     * an empty collection; or if this JenisSertifikasi has previously
     * been saved, it will retrieve related RwySertifikasisRelatedByIdJenisSertifikasi from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisSertifikasi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwySertifikasi[] List of RwySertifikasi objects
     */
    public function getRwySertifikasisRelatedByIdJenisSertifikasiJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwySertifikasiQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getRwySertifikasisRelatedByIdJenisSertifikasi($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisSertifikasi is new, it will return
     * an empty collection; or if this JenisSertifikasi has previously
     * been saved, it will retrieve related RwySertifikasisRelatedByIdJenisSertifikasi from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisSertifikasi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwySertifikasi[] List of RwySertifikasi objects
     */
    public function getRwySertifikasisRelatedByIdJenisSertifikasiJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwySertifikasiQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getRwySertifikasisRelatedByIdJenisSertifikasi($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisSertifikasi is new, it will return
     * an empty collection; or if this JenisSertifikasi has previously
     * been saved, it will retrieve related RwySertifikasisRelatedByIdJenisSertifikasi from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisSertifikasi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwySertifikasi[] List of RwySertifikasi objects
     */
    public function getRwySertifikasisRelatedByIdJenisSertifikasiJoinBidangStudiRelatedByBidangStudiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwySertifikasiQuery::create(null, $criteria);
        $query->joinWith('BidangStudiRelatedByBidangStudiId', $join_behavior);

        return $this->getRwySertifikasisRelatedByIdJenisSertifikasi($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JenisSertifikasi is new, it will return
     * an empty collection; or if this JenisSertifikasi has previously
     * been saved, it will retrieve related RwySertifikasisRelatedByIdJenisSertifikasi from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JenisSertifikasi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwySertifikasi[] List of RwySertifikasi objects
     */
    public function getRwySertifikasisRelatedByIdJenisSertifikasiJoinBidangStudiRelatedByBidangStudiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwySertifikasiQuery::create(null, $criteria);
        $query->joinWith('BidangStudiRelatedByBidangStudiId', $join_behavior);

        return $this->getRwySertifikasisRelatedByIdJenisSertifikasi($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id_jenis_sertifikasi = null;
        $this->jenis_sertifikasi = null;
        $this->prof_guru = null;
        $this->kepala_sekolah = null;
        $this->laboran = null;
        $this->kebutuhan_khusus_id = null;
        $this->create_date = null;
        $this->last_update = null;
        $this->expired_date = null;
        $this->last_sync = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collRwySertifikasisRelatedByIdJenisSertifikasi) {
                foreach ($this->collRwySertifikasisRelatedByIdJenisSertifikasi as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRwySertifikasisRelatedByIdJenisSertifikasi) {
                foreach ($this->collRwySertifikasisRelatedByIdJenisSertifikasi as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aKebutuhanKhusus instanceof Persistent) {
              $this->aKebutuhanKhusus->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collRwySertifikasisRelatedByIdJenisSertifikasi instanceof PropelCollection) {
            $this->collRwySertifikasisRelatedByIdJenisSertifikasi->clearIterator();
        }
        $this->collRwySertifikasisRelatedByIdJenisSertifikasi = null;
        if ($this->collRwySertifikasisRelatedByIdJenisSertifikasi instanceof PropelCollection) {
            $this->collRwySertifikasisRelatedByIdJenisSertifikasi->clearIterator();
        }
        $this->collRwySertifikasisRelatedByIdJenisSertifikasi = null;
        $this->aKebutuhanKhusus = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(JenisSertifikasiPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
