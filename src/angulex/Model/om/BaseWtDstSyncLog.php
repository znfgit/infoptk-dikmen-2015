<?php

namespace angulex\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelDateTime;
use \PropelException;
use \PropelPDO;
use angulex\Model\MstWilayah;
use angulex\Model\MstWilayahQuery;
use angulex\Model\Sekolah;
use angulex\Model\SekolahQuery;
use angulex\Model\TableSync;
use angulex\Model\TableSyncQuery;
use angulex\Model\WtDstSyncLog;
use angulex\Model\WtDstSyncLogPeer;
use angulex\Model\WtDstSyncLogQuery;

/**
 * Base class that represents a row from the 'wt_dst_sync_log' table.
 *
 * 
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseWtDstSyncLog extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'angulex\\Model\\WtDstSyncLogPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        WtDstSyncLogPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the kode_wilayah field.
     * @var        string
     */
    protected $kode_wilayah;

    /**
     * The value for the sekolah_id field.
     * @var        string
     */
    protected $sekolah_id;

    /**
     * The value for the table_name field.
     * @var        string
     */
    protected $table_name;

    /**
     * The value for the begin_sync field.
     * @var        string
     */
    protected $begin_sync;

    /**
     * The value for the end_sync field.
     * @var        string
     */
    protected $end_sync;

    /**
     * The value for the sync_media field.
     * Note: this column has a database default value of: '(\'1\')'
     * @var        string
     */
    protected $sync_media;

    /**
     * The value for the is_success field.
     * @var        string
     */
    protected $is_success;

    /**
     * The value for the n_create field.
     * @var        int
     */
    protected $n_create;

    /**
     * The value for the n_update field.
     * @var        int
     */
    protected $n_update;

    /**
     * The value for the n_hapus field.
     * @var        int
     */
    protected $n_hapus;

    /**
     * The value for the n_konflik field.
     * @var        int
     */
    protected $n_konflik;

    /**
     * The value for the selisih_waktu_server field.
     * @var        string
     */
    protected $selisih_waktu_server;

    /**
     * The value for the alamat_ip field.
     * @var        string
     */
    protected $alamat_ip;

    /**
     * The value for the pengguna_id field.
     * @var        string
     */
    protected $pengguna_id;

    /**
     * @var        Sekolah
     */
    protected $aSekolahRelatedBySekolahId;

    /**
     * @var        Sekolah
     */
    protected $aSekolahRelatedBySekolahId;

    /**
     * @var        MstWilayah
     */
    protected $aMstWilayahRelatedByKodeWilayah;

    /**
     * @var        MstWilayah
     */
    protected $aMstWilayahRelatedByKodeWilayah;

    /**
     * @var        TableSync
     */
    protected $aTableSyncRelatedByTableName;

    /**
     * @var        TableSync
     */
    protected $aTableSyncRelatedByTableName;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->sync_media = '(\'1\')';
    }

    /**
     * Initializes internal state of BaseWtDstSyncLog object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [kode_wilayah] column value.
     * 
     * @return string
     */
    public function getKodeWilayah()
    {
        return $this->kode_wilayah;
    }

    /**
     * Get the [sekolah_id] column value.
     * 
     * @return string
     */
    public function getSekolahId()
    {
        return $this->sekolah_id;
    }

    /**
     * Get the [table_name] column value.
     * 
     * @return string
     */
    public function getTableName()
    {
        return $this->table_name;
    }

    /**
     * Get the [optionally formatted] temporal [begin_sync] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getBeginSync($format = 'Y-m-d H:i:s')
    {
        if ($this->begin_sync === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->begin_sync);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->begin_sync, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [end_sync] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getEndSync($format = 'Y-m-d H:i:s')
    {
        if ($this->end_sync === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->end_sync);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->end_sync, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [sync_media] column value.
     * 
     * @return string
     */
    public function getSyncMedia()
    {
        return $this->sync_media;
    }

    /**
     * Get the [is_success] column value.
     * 
     * @return string
     */
    public function getIsSuccess()
    {
        return $this->is_success;
    }

    /**
     * Get the [n_create] column value.
     * 
     * @return int
     */
    public function getNCreate()
    {
        return $this->n_create;
    }

    /**
     * Get the [n_update] column value.
     * 
     * @return int
     */
    public function getNUpdate()
    {
        return $this->n_update;
    }

    /**
     * Get the [n_hapus] column value.
     * 
     * @return int
     */
    public function getNHapus()
    {
        return $this->n_hapus;
    }

    /**
     * Get the [n_konflik] column value.
     * 
     * @return int
     */
    public function getNKonflik()
    {
        return $this->n_konflik;
    }

    /**
     * Get the [selisih_waktu_server] column value.
     * 
     * @return string
     */
    public function getSelisihWaktuServer()
    {
        return $this->selisih_waktu_server;
    }

    /**
     * Get the [alamat_ip] column value.
     * 
     * @return string
     */
    public function getAlamatIp()
    {
        return $this->alamat_ip;
    }

    /**
     * Get the [pengguna_id] column value.
     * 
     * @return string
     */
    public function getPenggunaId()
    {
        return $this->pengguna_id;
    }

    /**
     * Set the value of [kode_wilayah] column.
     * 
     * @param string $v new value
     * @return WtDstSyncLog The current object (for fluent API support)
     */
    public function setKodeWilayah($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->kode_wilayah !== $v) {
            $this->kode_wilayah = $v;
            $this->modifiedColumns[] = WtDstSyncLogPeer::KODE_WILAYAH;
        }

        if ($this->aMstWilayahRelatedByKodeWilayah !== null && $this->aMstWilayahRelatedByKodeWilayah->getKodeWilayah() !== $v) {
            $this->aMstWilayahRelatedByKodeWilayah = null;
        }

        if ($this->aMstWilayahRelatedByKodeWilayah !== null && $this->aMstWilayahRelatedByKodeWilayah->getKodeWilayah() !== $v) {
            $this->aMstWilayahRelatedByKodeWilayah = null;
        }


        return $this;
    } // setKodeWilayah()

    /**
     * Set the value of [sekolah_id] column.
     * 
     * @param string $v new value
     * @return WtDstSyncLog The current object (for fluent API support)
     */
    public function setSekolahId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->sekolah_id !== $v) {
            $this->sekolah_id = $v;
            $this->modifiedColumns[] = WtDstSyncLogPeer::SEKOLAH_ID;
        }

        if ($this->aSekolahRelatedBySekolahId !== null && $this->aSekolahRelatedBySekolahId->getSekolahId() !== $v) {
            $this->aSekolahRelatedBySekolahId = null;
        }

        if ($this->aSekolahRelatedBySekolahId !== null && $this->aSekolahRelatedBySekolahId->getSekolahId() !== $v) {
            $this->aSekolahRelatedBySekolahId = null;
        }


        return $this;
    } // setSekolahId()

    /**
     * Set the value of [table_name] column.
     * 
     * @param string $v new value
     * @return WtDstSyncLog The current object (for fluent API support)
     */
    public function setTableName($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->table_name !== $v) {
            $this->table_name = $v;
            $this->modifiedColumns[] = WtDstSyncLogPeer::TABLE_NAME;
        }

        if ($this->aTableSyncRelatedByTableName !== null && $this->aTableSyncRelatedByTableName->getTableName() !== $v) {
            $this->aTableSyncRelatedByTableName = null;
        }

        if ($this->aTableSyncRelatedByTableName !== null && $this->aTableSyncRelatedByTableName->getTableName() !== $v) {
            $this->aTableSyncRelatedByTableName = null;
        }


        return $this;
    } // setTableName()

    /**
     * Sets the value of [begin_sync] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return WtDstSyncLog The current object (for fluent API support)
     */
    public function setBeginSync($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->begin_sync !== null || $dt !== null) {
            $currentDateAsString = ($this->begin_sync !== null && $tmpDt = new DateTime($this->begin_sync)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->begin_sync = $newDateAsString;
                $this->modifiedColumns[] = WtDstSyncLogPeer::BEGIN_SYNC;
            }
        } // if either are not null


        return $this;
    } // setBeginSync()

    /**
     * Sets the value of [end_sync] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return WtDstSyncLog The current object (for fluent API support)
     */
    public function setEndSync($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->end_sync !== null || $dt !== null) {
            $currentDateAsString = ($this->end_sync !== null && $tmpDt = new DateTime($this->end_sync)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->end_sync = $newDateAsString;
                $this->modifiedColumns[] = WtDstSyncLogPeer::END_SYNC;
            }
        } // if either are not null


        return $this;
    } // setEndSync()

    /**
     * Set the value of [sync_media] column.
     * 
     * @param string $v new value
     * @return WtDstSyncLog The current object (for fluent API support)
     */
    public function setSyncMedia($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->sync_media !== $v) {
            $this->sync_media = $v;
            $this->modifiedColumns[] = WtDstSyncLogPeer::SYNC_MEDIA;
        }


        return $this;
    } // setSyncMedia()

    /**
     * Set the value of [is_success] column.
     * 
     * @param string $v new value
     * @return WtDstSyncLog The current object (for fluent API support)
     */
    public function setIsSuccess($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->is_success !== $v) {
            $this->is_success = $v;
            $this->modifiedColumns[] = WtDstSyncLogPeer::IS_SUCCESS;
        }


        return $this;
    } // setIsSuccess()

    /**
     * Set the value of [n_create] column.
     * 
     * @param int $v new value
     * @return WtDstSyncLog The current object (for fluent API support)
     */
    public function setNCreate($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->n_create !== $v) {
            $this->n_create = $v;
            $this->modifiedColumns[] = WtDstSyncLogPeer::N_CREATE;
        }


        return $this;
    } // setNCreate()

    /**
     * Set the value of [n_update] column.
     * 
     * @param int $v new value
     * @return WtDstSyncLog The current object (for fluent API support)
     */
    public function setNUpdate($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->n_update !== $v) {
            $this->n_update = $v;
            $this->modifiedColumns[] = WtDstSyncLogPeer::N_UPDATE;
        }


        return $this;
    } // setNUpdate()

    /**
     * Set the value of [n_hapus] column.
     * 
     * @param int $v new value
     * @return WtDstSyncLog The current object (for fluent API support)
     */
    public function setNHapus($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->n_hapus !== $v) {
            $this->n_hapus = $v;
            $this->modifiedColumns[] = WtDstSyncLogPeer::N_HAPUS;
        }


        return $this;
    } // setNHapus()

    /**
     * Set the value of [n_konflik] column.
     * 
     * @param int $v new value
     * @return WtDstSyncLog The current object (for fluent API support)
     */
    public function setNKonflik($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->n_konflik !== $v) {
            $this->n_konflik = $v;
            $this->modifiedColumns[] = WtDstSyncLogPeer::N_KONFLIK;
        }


        return $this;
    } // setNKonflik()

    /**
     * Set the value of [selisih_waktu_server] column.
     * 
     * @param string $v new value
     * @return WtDstSyncLog The current object (for fluent API support)
     */
    public function setSelisihWaktuServer($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->selisih_waktu_server !== $v) {
            $this->selisih_waktu_server = $v;
            $this->modifiedColumns[] = WtDstSyncLogPeer::SELISIH_WAKTU_SERVER;
        }


        return $this;
    } // setSelisihWaktuServer()

    /**
     * Set the value of [alamat_ip] column.
     * 
     * @param string $v new value
     * @return WtDstSyncLog The current object (for fluent API support)
     */
    public function setAlamatIp($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->alamat_ip !== $v) {
            $this->alamat_ip = $v;
            $this->modifiedColumns[] = WtDstSyncLogPeer::ALAMAT_IP;
        }


        return $this;
    } // setAlamatIp()

    /**
     * Set the value of [pengguna_id] column.
     * 
     * @param string $v new value
     * @return WtDstSyncLog The current object (for fluent API support)
     */
    public function setPenggunaId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->pengguna_id !== $v) {
            $this->pengguna_id = $v;
            $this->modifiedColumns[] = WtDstSyncLogPeer::PENGGUNA_ID;
        }


        return $this;
    } // setPenggunaId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->sync_media !== '(\'1\')') {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->kode_wilayah = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
            $this->sekolah_id = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->table_name = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->begin_sync = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->end_sync = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->sync_media = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->is_success = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->n_create = ($row[$startcol + 7] !== null) ? (int) $row[$startcol + 7] : null;
            $this->n_update = ($row[$startcol + 8] !== null) ? (int) $row[$startcol + 8] : null;
            $this->n_hapus = ($row[$startcol + 9] !== null) ? (int) $row[$startcol + 9] : null;
            $this->n_konflik = ($row[$startcol + 10] !== null) ? (int) $row[$startcol + 10] : null;
            $this->selisih_waktu_server = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->alamat_ip = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->pengguna_id = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 14; // 14 = WtDstSyncLogPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating WtDstSyncLog object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aMstWilayahRelatedByKodeWilayah !== null && $this->kode_wilayah !== $this->aMstWilayahRelatedByKodeWilayah->getKodeWilayah()) {
            $this->aMstWilayahRelatedByKodeWilayah = null;
        }
        if ($this->aMstWilayahRelatedByKodeWilayah !== null && $this->kode_wilayah !== $this->aMstWilayahRelatedByKodeWilayah->getKodeWilayah()) {
            $this->aMstWilayahRelatedByKodeWilayah = null;
        }
        if ($this->aSekolahRelatedBySekolahId !== null && $this->sekolah_id !== $this->aSekolahRelatedBySekolahId->getSekolahId()) {
            $this->aSekolahRelatedBySekolahId = null;
        }
        if ($this->aSekolahRelatedBySekolahId !== null && $this->sekolah_id !== $this->aSekolahRelatedBySekolahId->getSekolahId()) {
            $this->aSekolahRelatedBySekolahId = null;
        }
        if ($this->aTableSyncRelatedByTableName !== null && $this->table_name !== $this->aTableSyncRelatedByTableName->getTableName()) {
            $this->aTableSyncRelatedByTableName = null;
        }
        if ($this->aTableSyncRelatedByTableName !== null && $this->table_name !== $this->aTableSyncRelatedByTableName->getTableName()) {
            $this->aTableSyncRelatedByTableName = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(WtDstSyncLogPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = WtDstSyncLogPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aSekolahRelatedBySekolahId = null;
            $this->aSekolahRelatedBySekolahId = null;
            $this->aMstWilayahRelatedByKodeWilayah = null;
            $this->aMstWilayahRelatedByKodeWilayah = null;
            $this->aTableSyncRelatedByTableName = null;
            $this->aTableSyncRelatedByTableName = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(WtDstSyncLogPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = WtDstSyncLogQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(WtDstSyncLogPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                WtDstSyncLogPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if ($this->aSekolahRelatedBySekolahId->isModified() || $this->aSekolahRelatedBySekolahId->isNew()) {
                    $affectedRows += $this->aSekolahRelatedBySekolahId->save($con);
                }
                $this->setSekolahRelatedBySekolahId($this->aSekolahRelatedBySekolahId);
            }

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if ($this->aSekolahRelatedBySekolahId->isModified() || $this->aSekolahRelatedBySekolahId->isNew()) {
                    $affectedRows += $this->aSekolahRelatedBySekolahId->save($con);
                }
                $this->setSekolahRelatedBySekolahId($this->aSekolahRelatedBySekolahId);
            }

            if ($this->aMstWilayahRelatedByKodeWilayah !== null) {
                if ($this->aMstWilayahRelatedByKodeWilayah->isModified() || $this->aMstWilayahRelatedByKodeWilayah->isNew()) {
                    $affectedRows += $this->aMstWilayahRelatedByKodeWilayah->save($con);
                }
                $this->setMstWilayahRelatedByKodeWilayah($this->aMstWilayahRelatedByKodeWilayah);
            }

            if ($this->aMstWilayahRelatedByKodeWilayah !== null) {
                if ($this->aMstWilayahRelatedByKodeWilayah->isModified() || $this->aMstWilayahRelatedByKodeWilayah->isNew()) {
                    $affectedRows += $this->aMstWilayahRelatedByKodeWilayah->save($con);
                }
                $this->setMstWilayahRelatedByKodeWilayah($this->aMstWilayahRelatedByKodeWilayah);
            }

            if ($this->aTableSyncRelatedByTableName !== null) {
                if ($this->aTableSyncRelatedByTableName->isModified() || $this->aTableSyncRelatedByTableName->isNew()) {
                    $affectedRows += $this->aTableSyncRelatedByTableName->save($con);
                }
                $this->setTableSyncRelatedByTableName($this->aTableSyncRelatedByTableName);
            }

            if ($this->aTableSyncRelatedByTableName !== null) {
                if ($this->aTableSyncRelatedByTableName->isModified() || $this->aTableSyncRelatedByTableName->isNew()) {
                    $affectedRows += $this->aTableSyncRelatedByTableName->save($con);
                }
                $this->setTableSyncRelatedByTableName($this->aTableSyncRelatedByTableName);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $criteria = $this->buildCriteria();
        $pk = BasePeer::doInsert($criteria, $con);
        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if (!$this->aSekolahRelatedBySekolahId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSekolahRelatedBySekolahId->getValidationFailures());
                }
            }

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if (!$this->aSekolahRelatedBySekolahId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSekolahRelatedBySekolahId->getValidationFailures());
                }
            }

            if ($this->aMstWilayahRelatedByKodeWilayah !== null) {
                if (!$this->aMstWilayahRelatedByKodeWilayah->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aMstWilayahRelatedByKodeWilayah->getValidationFailures());
                }
            }

            if ($this->aMstWilayahRelatedByKodeWilayah !== null) {
                if (!$this->aMstWilayahRelatedByKodeWilayah->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aMstWilayahRelatedByKodeWilayah->getValidationFailures());
                }
            }

            if ($this->aTableSyncRelatedByTableName !== null) {
                if (!$this->aTableSyncRelatedByTableName->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aTableSyncRelatedByTableName->getValidationFailures());
                }
            }

            if ($this->aTableSyncRelatedByTableName !== null) {
                if (!$this->aTableSyncRelatedByTableName->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aTableSyncRelatedByTableName->getValidationFailures());
                }
            }


            if (($retval = WtDstSyncLogPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = WtDstSyncLogPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getKodeWilayah();
                break;
            case 1:
                return $this->getSekolahId();
                break;
            case 2:
                return $this->getTableName();
                break;
            case 3:
                return $this->getBeginSync();
                break;
            case 4:
                return $this->getEndSync();
                break;
            case 5:
                return $this->getSyncMedia();
                break;
            case 6:
                return $this->getIsSuccess();
                break;
            case 7:
                return $this->getNCreate();
                break;
            case 8:
                return $this->getNUpdate();
                break;
            case 9:
                return $this->getNHapus();
                break;
            case 10:
                return $this->getNKonflik();
                break;
            case 11:
                return $this->getSelisihWaktuServer();
                break;
            case 12:
                return $this->getAlamatIp();
                break;
            case 13:
                return $this->getPenggunaId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['WtDstSyncLog'][serialize($this->getPrimaryKey())])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['WtDstSyncLog'][serialize($this->getPrimaryKey())] = true;
        $keys = WtDstSyncLogPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getKodeWilayah(),
            $keys[1] => $this->getSekolahId(),
            $keys[2] => $this->getTableName(),
            $keys[3] => $this->getBeginSync(),
            $keys[4] => $this->getEndSync(),
            $keys[5] => $this->getSyncMedia(),
            $keys[6] => $this->getIsSuccess(),
            $keys[7] => $this->getNCreate(),
            $keys[8] => $this->getNUpdate(),
            $keys[9] => $this->getNHapus(),
            $keys[10] => $this->getNKonflik(),
            $keys[11] => $this->getSelisihWaktuServer(),
            $keys[12] => $this->getAlamatIp(),
            $keys[13] => $this->getPenggunaId(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aSekolahRelatedBySekolahId) {
                $result['SekolahRelatedBySekolahId'] = $this->aSekolahRelatedBySekolahId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSekolahRelatedBySekolahId) {
                $result['SekolahRelatedBySekolahId'] = $this->aSekolahRelatedBySekolahId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aMstWilayahRelatedByKodeWilayah) {
                $result['MstWilayahRelatedByKodeWilayah'] = $this->aMstWilayahRelatedByKodeWilayah->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aMstWilayahRelatedByKodeWilayah) {
                $result['MstWilayahRelatedByKodeWilayah'] = $this->aMstWilayahRelatedByKodeWilayah->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aTableSyncRelatedByTableName) {
                $result['TableSyncRelatedByTableName'] = $this->aTableSyncRelatedByTableName->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aTableSyncRelatedByTableName) {
                $result['TableSyncRelatedByTableName'] = $this->aTableSyncRelatedByTableName->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = WtDstSyncLogPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setKodeWilayah($value);
                break;
            case 1:
                $this->setSekolahId($value);
                break;
            case 2:
                $this->setTableName($value);
                break;
            case 3:
                $this->setBeginSync($value);
                break;
            case 4:
                $this->setEndSync($value);
                break;
            case 5:
                $this->setSyncMedia($value);
                break;
            case 6:
                $this->setIsSuccess($value);
                break;
            case 7:
                $this->setNCreate($value);
                break;
            case 8:
                $this->setNUpdate($value);
                break;
            case 9:
                $this->setNHapus($value);
                break;
            case 10:
                $this->setNKonflik($value);
                break;
            case 11:
                $this->setSelisihWaktuServer($value);
                break;
            case 12:
                $this->setAlamatIp($value);
                break;
            case 13:
                $this->setPenggunaId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = WtDstSyncLogPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setKodeWilayah($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setSekolahId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setTableName($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setBeginSync($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setEndSync($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setSyncMedia($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setIsSuccess($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setNCreate($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setNUpdate($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setNHapus($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setNKonflik($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setSelisihWaktuServer($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setAlamatIp($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setPenggunaId($arr[$keys[13]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(WtDstSyncLogPeer::DATABASE_NAME);

        if ($this->isColumnModified(WtDstSyncLogPeer::KODE_WILAYAH)) $criteria->add(WtDstSyncLogPeer::KODE_WILAYAH, $this->kode_wilayah);
        if ($this->isColumnModified(WtDstSyncLogPeer::SEKOLAH_ID)) $criteria->add(WtDstSyncLogPeer::SEKOLAH_ID, $this->sekolah_id);
        if ($this->isColumnModified(WtDstSyncLogPeer::TABLE_NAME)) $criteria->add(WtDstSyncLogPeer::TABLE_NAME, $this->table_name);
        if ($this->isColumnModified(WtDstSyncLogPeer::BEGIN_SYNC)) $criteria->add(WtDstSyncLogPeer::BEGIN_SYNC, $this->begin_sync);
        if ($this->isColumnModified(WtDstSyncLogPeer::END_SYNC)) $criteria->add(WtDstSyncLogPeer::END_SYNC, $this->end_sync);
        if ($this->isColumnModified(WtDstSyncLogPeer::SYNC_MEDIA)) $criteria->add(WtDstSyncLogPeer::SYNC_MEDIA, $this->sync_media);
        if ($this->isColumnModified(WtDstSyncLogPeer::IS_SUCCESS)) $criteria->add(WtDstSyncLogPeer::IS_SUCCESS, $this->is_success);
        if ($this->isColumnModified(WtDstSyncLogPeer::N_CREATE)) $criteria->add(WtDstSyncLogPeer::N_CREATE, $this->n_create);
        if ($this->isColumnModified(WtDstSyncLogPeer::N_UPDATE)) $criteria->add(WtDstSyncLogPeer::N_UPDATE, $this->n_update);
        if ($this->isColumnModified(WtDstSyncLogPeer::N_HAPUS)) $criteria->add(WtDstSyncLogPeer::N_HAPUS, $this->n_hapus);
        if ($this->isColumnModified(WtDstSyncLogPeer::N_KONFLIK)) $criteria->add(WtDstSyncLogPeer::N_KONFLIK, $this->n_konflik);
        if ($this->isColumnModified(WtDstSyncLogPeer::SELISIH_WAKTU_SERVER)) $criteria->add(WtDstSyncLogPeer::SELISIH_WAKTU_SERVER, $this->selisih_waktu_server);
        if ($this->isColumnModified(WtDstSyncLogPeer::ALAMAT_IP)) $criteria->add(WtDstSyncLogPeer::ALAMAT_IP, $this->alamat_ip);
        if ($this->isColumnModified(WtDstSyncLogPeer::PENGGUNA_ID)) $criteria->add(WtDstSyncLogPeer::PENGGUNA_ID, $this->pengguna_id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(WtDstSyncLogPeer::DATABASE_NAME);
        $criteria->add(WtDstSyncLogPeer::KODE_WILAYAH, $this->kode_wilayah);
        $criteria->add(WtDstSyncLogPeer::SEKOLAH_ID, $this->sekolah_id);
        $criteria->add(WtDstSyncLogPeer::TABLE_NAME, $this->table_name);

        return $criteria;
    }

    /**
     * Returns the composite primary key for this object.
     * The array elements will be in same order as specified in XML.
     * @return array
     */
    public function getPrimaryKey()
    {
        $pks = array();
        $pks[0] = $this->getKodeWilayah();
        $pks[1] = $this->getSekolahId();
        $pks[2] = $this->getTableName();

        return $pks;
    }

    /**
     * Set the [composite] primary key.
     *
     * @param array $keys The elements of the composite key (order must match the order in XML file).
     * @return void
     */
    public function setPrimaryKey($keys)
    {
        $this->setKodeWilayah($keys[0]);
        $this->setSekolahId($keys[1]);
        $this->setTableName($keys[2]);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return (null === $this->getKodeWilayah()) && (null === $this->getSekolahId()) && (null === $this->getTableName());
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of WtDstSyncLog (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setKodeWilayah($this->getKodeWilayah());
        $copyObj->setSekolahId($this->getSekolahId());
        $copyObj->setTableName($this->getTableName());
        $copyObj->setBeginSync($this->getBeginSync());
        $copyObj->setEndSync($this->getEndSync());
        $copyObj->setSyncMedia($this->getSyncMedia());
        $copyObj->setIsSuccess($this->getIsSuccess());
        $copyObj->setNCreate($this->getNCreate());
        $copyObj->setNUpdate($this->getNUpdate());
        $copyObj->setNHapus($this->getNHapus());
        $copyObj->setNKonflik($this->getNKonflik());
        $copyObj->setSelisihWaktuServer($this->getSelisihWaktuServer());
        $copyObj->setAlamatIp($this->getAlamatIp());
        $copyObj->setPenggunaId($this->getPenggunaId());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return WtDstSyncLog Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return WtDstSyncLogPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new WtDstSyncLogPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a Sekolah object.
     *
     * @param             Sekolah $v
     * @return WtDstSyncLog The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSekolahRelatedBySekolahId(Sekolah $v = null)
    {
        if ($v === null) {
            $this->setSekolahId(NULL);
        } else {
            $this->setSekolahId($v->getSekolahId());
        }

        $this->aSekolahRelatedBySekolahId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Sekolah object, it will not be re-added.
        if ($v !== null) {
            $v->addWtDstSyncLogRelatedBySekolahId($this);
        }


        return $this;
    }


    /**
     * Get the associated Sekolah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Sekolah The associated Sekolah object.
     * @throws PropelException
     */
    public function getSekolahRelatedBySekolahId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSekolahRelatedBySekolahId === null && (($this->sekolah_id !== "" && $this->sekolah_id !== null)) && $doQuery) {
            $this->aSekolahRelatedBySekolahId = SekolahQuery::create()->findPk($this->sekolah_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSekolahRelatedBySekolahId->addWtDstSyncLogsRelatedBySekolahId($this);
             */
        }

        return $this->aSekolahRelatedBySekolahId;
    }

    /**
     * Declares an association between this object and a Sekolah object.
     *
     * @param             Sekolah $v
     * @return WtDstSyncLog The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSekolahRelatedBySekolahId(Sekolah $v = null)
    {
        if ($v === null) {
            $this->setSekolahId(NULL);
        } else {
            $this->setSekolahId($v->getSekolahId());
        }

        $this->aSekolahRelatedBySekolahId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Sekolah object, it will not be re-added.
        if ($v !== null) {
            $v->addWtDstSyncLogRelatedBySekolahId($this);
        }


        return $this;
    }


    /**
     * Get the associated Sekolah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Sekolah The associated Sekolah object.
     * @throws PropelException
     */
    public function getSekolahRelatedBySekolahId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSekolahRelatedBySekolahId === null && (($this->sekolah_id !== "" && $this->sekolah_id !== null)) && $doQuery) {
            $this->aSekolahRelatedBySekolahId = SekolahQuery::create()->findPk($this->sekolah_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSekolahRelatedBySekolahId->addWtDstSyncLogsRelatedBySekolahId($this);
             */
        }

        return $this->aSekolahRelatedBySekolahId;
    }

    /**
     * Declares an association between this object and a MstWilayah object.
     *
     * @param             MstWilayah $v
     * @return WtDstSyncLog The current object (for fluent API support)
     * @throws PropelException
     */
    public function setMstWilayahRelatedByKodeWilayah(MstWilayah $v = null)
    {
        if ($v === null) {
            $this->setKodeWilayah(NULL);
        } else {
            $this->setKodeWilayah($v->getKodeWilayah());
        }

        $this->aMstWilayahRelatedByKodeWilayah = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the MstWilayah object, it will not be re-added.
        if ($v !== null) {
            $v->addWtDstSyncLogRelatedByKodeWilayah($this);
        }


        return $this;
    }


    /**
     * Get the associated MstWilayah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return MstWilayah The associated MstWilayah object.
     * @throws PropelException
     */
    public function getMstWilayahRelatedByKodeWilayah(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aMstWilayahRelatedByKodeWilayah === null && (($this->kode_wilayah !== "" && $this->kode_wilayah !== null)) && $doQuery) {
            $this->aMstWilayahRelatedByKodeWilayah = MstWilayahQuery::create()->findPk($this->kode_wilayah, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aMstWilayahRelatedByKodeWilayah->addWtDstSyncLogsRelatedByKodeWilayah($this);
             */
        }

        return $this->aMstWilayahRelatedByKodeWilayah;
    }

    /**
     * Declares an association between this object and a MstWilayah object.
     *
     * @param             MstWilayah $v
     * @return WtDstSyncLog The current object (for fluent API support)
     * @throws PropelException
     */
    public function setMstWilayahRelatedByKodeWilayah(MstWilayah $v = null)
    {
        if ($v === null) {
            $this->setKodeWilayah(NULL);
        } else {
            $this->setKodeWilayah($v->getKodeWilayah());
        }

        $this->aMstWilayahRelatedByKodeWilayah = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the MstWilayah object, it will not be re-added.
        if ($v !== null) {
            $v->addWtDstSyncLogRelatedByKodeWilayah($this);
        }


        return $this;
    }


    /**
     * Get the associated MstWilayah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return MstWilayah The associated MstWilayah object.
     * @throws PropelException
     */
    public function getMstWilayahRelatedByKodeWilayah(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aMstWilayahRelatedByKodeWilayah === null && (($this->kode_wilayah !== "" && $this->kode_wilayah !== null)) && $doQuery) {
            $this->aMstWilayahRelatedByKodeWilayah = MstWilayahQuery::create()->findPk($this->kode_wilayah, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aMstWilayahRelatedByKodeWilayah->addWtDstSyncLogsRelatedByKodeWilayah($this);
             */
        }

        return $this->aMstWilayahRelatedByKodeWilayah;
    }

    /**
     * Declares an association between this object and a TableSync object.
     *
     * @param             TableSync $v
     * @return WtDstSyncLog The current object (for fluent API support)
     * @throws PropelException
     */
    public function setTableSyncRelatedByTableName(TableSync $v = null)
    {
        if ($v === null) {
            $this->setTableName(NULL);
        } else {
            $this->setTableName($v->getTableName());
        }

        $this->aTableSyncRelatedByTableName = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the TableSync object, it will not be re-added.
        if ($v !== null) {
            $v->addWtDstSyncLogRelatedByTableName($this);
        }


        return $this;
    }


    /**
     * Get the associated TableSync object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return TableSync The associated TableSync object.
     * @throws PropelException
     */
    public function getTableSyncRelatedByTableName(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aTableSyncRelatedByTableName === null && (($this->table_name !== "" && $this->table_name !== null)) && $doQuery) {
            $this->aTableSyncRelatedByTableName = TableSyncQuery::create()->findPk($this->table_name, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aTableSyncRelatedByTableName->addWtDstSyncLogsRelatedByTableName($this);
             */
        }

        return $this->aTableSyncRelatedByTableName;
    }

    /**
     * Declares an association between this object and a TableSync object.
     *
     * @param             TableSync $v
     * @return WtDstSyncLog The current object (for fluent API support)
     * @throws PropelException
     */
    public function setTableSyncRelatedByTableName(TableSync $v = null)
    {
        if ($v === null) {
            $this->setTableName(NULL);
        } else {
            $this->setTableName($v->getTableName());
        }

        $this->aTableSyncRelatedByTableName = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the TableSync object, it will not be re-added.
        if ($v !== null) {
            $v->addWtDstSyncLogRelatedByTableName($this);
        }


        return $this;
    }


    /**
     * Get the associated TableSync object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return TableSync The associated TableSync object.
     * @throws PropelException
     */
    public function getTableSyncRelatedByTableName(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aTableSyncRelatedByTableName === null && (($this->table_name !== "" && $this->table_name !== null)) && $doQuery) {
            $this->aTableSyncRelatedByTableName = TableSyncQuery::create()->findPk($this->table_name, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aTableSyncRelatedByTableName->addWtDstSyncLogsRelatedByTableName($this);
             */
        }

        return $this->aTableSyncRelatedByTableName;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->kode_wilayah = null;
        $this->sekolah_id = null;
        $this->table_name = null;
        $this->begin_sync = null;
        $this->end_sync = null;
        $this->sync_media = null;
        $this->is_success = null;
        $this->n_create = null;
        $this->n_update = null;
        $this->n_hapus = null;
        $this->n_konflik = null;
        $this->selisih_waktu_server = null;
        $this->alamat_ip = null;
        $this->pengguna_id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->aSekolahRelatedBySekolahId instanceof Persistent) {
              $this->aSekolahRelatedBySekolahId->clearAllReferences($deep);
            }
            if ($this->aSekolahRelatedBySekolahId instanceof Persistent) {
              $this->aSekolahRelatedBySekolahId->clearAllReferences($deep);
            }
            if ($this->aMstWilayahRelatedByKodeWilayah instanceof Persistent) {
              $this->aMstWilayahRelatedByKodeWilayah->clearAllReferences($deep);
            }
            if ($this->aMstWilayahRelatedByKodeWilayah instanceof Persistent) {
              $this->aMstWilayahRelatedByKodeWilayah->clearAllReferences($deep);
            }
            if ($this->aTableSyncRelatedByTableName instanceof Persistent) {
              $this->aTableSyncRelatedByTableName->clearAllReferences($deep);
            }
            if ($this->aTableSyncRelatedByTableName instanceof Persistent) {
              $this->aTableSyncRelatedByTableName->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        $this->aSekolahRelatedBySekolahId = null;
        $this->aSekolahRelatedBySekolahId = null;
        $this->aMstWilayahRelatedByKodeWilayah = null;
        $this->aMstWilayahRelatedByKodeWilayah = null;
        $this->aTableSyncRelatedByTableName = null;
        $this->aTableSyncRelatedByTableName = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(WtDstSyncLogPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
