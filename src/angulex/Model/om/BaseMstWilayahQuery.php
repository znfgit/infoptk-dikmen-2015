<?php

namespace angulex\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\Demografi;
use angulex\Model\LembagaNonSekolah;
use angulex\Model\LevelWilayah;
use angulex\Model\MstWilayah;
use angulex\Model\MstWilayahPeer;
use angulex\Model\MstWilayahQuery;
use angulex\Model\Negara;
use angulex\Model\Pengguna;
use angulex\Model\PesertaDidik;
use angulex\Model\Ptk;
use angulex\Model\Sekolah;
use angulex\Model\TetanggaKabkota;
use angulex\Model\WPendingJob;
use angulex\Model\WdstSyncLog;
use angulex\Model\WsrcSyncLog;
use angulex\Model\WsyncSession;
use angulex\Model\WtDstSyncLog;
use angulex\Model\WtSrcSyncLog;
use angulex\Model\Yayasan;

/**
 * Base class that represents a query for the 'ref.mst_wilayah' table.
 *
 * 
 *
 * @method MstWilayahQuery orderByKodeWilayah($order = Criteria::ASC) Order by the kode_wilayah column
 * @method MstWilayahQuery orderByNama($order = Criteria::ASC) Order by the nama column
 * @method MstWilayahQuery orderByIdLevelWilayah($order = Criteria::ASC) Order by the id_level_wilayah column
 * @method MstWilayahQuery orderByMstKodeWilayah($order = Criteria::ASC) Order by the mst_kode_wilayah column
 * @method MstWilayahQuery orderByNegaraId($order = Criteria::ASC) Order by the negara_id column
 * @method MstWilayahQuery orderByAsalWilayah($order = Criteria::ASC) Order by the asal_wilayah column
 * @method MstWilayahQuery orderByKodeBps($order = Criteria::ASC) Order by the kode_bps column
 * @method MstWilayahQuery orderByKodeDagri($order = Criteria::ASC) Order by the kode_dagri column
 * @method MstWilayahQuery orderByKodeKeu($order = Criteria::ASC) Order by the kode_keu column
 * @method MstWilayahQuery orderByCreateDate($order = Criteria::ASC) Order by the create_date column
 * @method MstWilayahQuery orderByLastUpdate($order = Criteria::ASC) Order by the last_update column
 * @method MstWilayahQuery orderByExpiredDate($order = Criteria::ASC) Order by the expired_date column
 * @method MstWilayahQuery orderByLastSync($order = Criteria::ASC) Order by the last_sync column
 *
 * @method MstWilayahQuery groupByKodeWilayah() Group by the kode_wilayah column
 * @method MstWilayahQuery groupByNama() Group by the nama column
 * @method MstWilayahQuery groupByIdLevelWilayah() Group by the id_level_wilayah column
 * @method MstWilayahQuery groupByMstKodeWilayah() Group by the mst_kode_wilayah column
 * @method MstWilayahQuery groupByNegaraId() Group by the negara_id column
 * @method MstWilayahQuery groupByAsalWilayah() Group by the asal_wilayah column
 * @method MstWilayahQuery groupByKodeBps() Group by the kode_bps column
 * @method MstWilayahQuery groupByKodeDagri() Group by the kode_dagri column
 * @method MstWilayahQuery groupByKodeKeu() Group by the kode_keu column
 * @method MstWilayahQuery groupByCreateDate() Group by the create_date column
 * @method MstWilayahQuery groupByLastUpdate() Group by the last_update column
 * @method MstWilayahQuery groupByExpiredDate() Group by the expired_date column
 * @method MstWilayahQuery groupByLastSync() Group by the last_sync column
 *
 * @method MstWilayahQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method MstWilayahQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method MstWilayahQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method MstWilayahQuery leftJoinLevelWilayah($relationAlias = null) Adds a LEFT JOIN clause to the query using the LevelWilayah relation
 * @method MstWilayahQuery rightJoinLevelWilayah($relationAlias = null) Adds a RIGHT JOIN clause to the query using the LevelWilayah relation
 * @method MstWilayahQuery innerJoinLevelWilayah($relationAlias = null) Adds a INNER JOIN clause to the query using the LevelWilayah relation
 *
 * @method MstWilayahQuery leftJoinMstWilayahRelatedByMstKodeWilayah($relationAlias = null) Adds a LEFT JOIN clause to the query using the MstWilayahRelatedByMstKodeWilayah relation
 * @method MstWilayahQuery rightJoinMstWilayahRelatedByMstKodeWilayah($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MstWilayahRelatedByMstKodeWilayah relation
 * @method MstWilayahQuery innerJoinMstWilayahRelatedByMstKodeWilayah($relationAlias = null) Adds a INNER JOIN clause to the query using the MstWilayahRelatedByMstKodeWilayah relation
 *
 * @method MstWilayahQuery leftJoinNegara($relationAlias = null) Adds a LEFT JOIN clause to the query using the Negara relation
 * @method MstWilayahQuery rightJoinNegara($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Negara relation
 * @method MstWilayahQuery innerJoinNegara($relationAlias = null) Adds a INNER JOIN clause to the query using the Negara relation
 *
 * @method MstWilayahQuery leftJoinTetanggaKabkotaRelatedByKodeWilayah($relationAlias = null) Adds a LEFT JOIN clause to the query using the TetanggaKabkotaRelatedByKodeWilayah relation
 * @method MstWilayahQuery rightJoinTetanggaKabkotaRelatedByKodeWilayah($relationAlias = null) Adds a RIGHT JOIN clause to the query using the TetanggaKabkotaRelatedByKodeWilayah relation
 * @method MstWilayahQuery innerJoinTetanggaKabkotaRelatedByKodeWilayah($relationAlias = null) Adds a INNER JOIN clause to the query using the TetanggaKabkotaRelatedByKodeWilayah relation
 *
 * @method MstWilayahQuery leftJoinTetanggaKabkotaRelatedByMstKodeWilayah($relationAlias = null) Adds a LEFT JOIN clause to the query using the TetanggaKabkotaRelatedByMstKodeWilayah relation
 * @method MstWilayahQuery rightJoinTetanggaKabkotaRelatedByMstKodeWilayah($relationAlias = null) Adds a RIGHT JOIN clause to the query using the TetanggaKabkotaRelatedByMstKodeWilayah relation
 * @method MstWilayahQuery innerJoinTetanggaKabkotaRelatedByMstKodeWilayah($relationAlias = null) Adds a INNER JOIN clause to the query using the TetanggaKabkotaRelatedByMstKodeWilayah relation
 *
 * @method MstWilayahQuery leftJoinPesertaDidikRelatedByKodeWilayah($relationAlias = null) Adds a LEFT JOIN clause to the query using the PesertaDidikRelatedByKodeWilayah relation
 * @method MstWilayahQuery rightJoinPesertaDidikRelatedByKodeWilayah($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PesertaDidikRelatedByKodeWilayah relation
 * @method MstWilayahQuery innerJoinPesertaDidikRelatedByKodeWilayah($relationAlias = null) Adds a INNER JOIN clause to the query using the PesertaDidikRelatedByKodeWilayah relation
 *
 * @method MstWilayahQuery leftJoinPesertaDidikRelatedByKodeWilayah($relationAlias = null) Adds a LEFT JOIN clause to the query using the PesertaDidikRelatedByKodeWilayah relation
 * @method MstWilayahQuery rightJoinPesertaDidikRelatedByKodeWilayah($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PesertaDidikRelatedByKodeWilayah relation
 * @method MstWilayahQuery innerJoinPesertaDidikRelatedByKodeWilayah($relationAlias = null) Adds a INNER JOIN clause to the query using the PesertaDidikRelatedByKodeWilayah relation
 *
 * @method MstWilayahQuery leftJoinMstWilayahRelatedByKodeWilayah($relationAlias = null) Adds a LEFT JOIN clause to the query using the MstWilayahRelatedByKodeWilayah relation
 * @method MstWilayahQuery rightJoinMstWilayahRelatedByKodeWilayah($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MstWilayahRelatedByKodeWilayah relation
 * @method MstWilayahQuery innerJoinMstWilayahRelatedByKodeWilayah($relationAlias = null) Adds a INNER JOIN clause to the query using the MstWilayahRelatedByKodeWilayah relation
 *
 * @method MstWilayahQuery leftJoinPenggunaRelatedByKodeWilayah($relationAlias = null) Adds a LEFT JOIN clause to the query using the PenggunaRelatedByKodeWilayah relation
 * @method MstWilayahQuery rightJoinPenggunaRelatedByKodeWilayah($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PenggunaRelatedByKodeWilayah relation
 * @method MstWilayahQuery innerJoinPenggunaRelatedByKodeWilayah($relationAlias = null) Adds a INNER JOIN clause to the query using the PenggunaRelatedByKodeWilayah relation
 *
 * @method MstWilayahQuery leftJoinPenggunaRelatedByKodeWilayah($relationAlias = null) Adds a LEFT JOIN clause to the query using the PenggunaRelatedByKodeWilayah relation
 * @method MstWilayahQuery rightJoinPenggunaRelatedByKodeWilayah($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PenggunaRelatedByKodeWilayah relation
 * @method MstWilayahQuery innerJoinPenggunaRelatedByKodeWilayah($relationAlias = null) Adds a INNER JOIN clause to the query using the PenggunaRelatedByKodeWilayah relation
 *
 * @method MstWilayahQuery leftJoinSekolahRelatedByKodeWilayah($relationAlias = null) Adds a LEFT JOIN clause to the query using the SekolahRelatedByKodeWilayah relation
 * @method MstWilayahQuery rightJoinSekolahRelatedByKodeWilayah($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SekolahRelatedByKodeWilayah relation
 * @method MstWilayahQuery innerJoinSekolahRelatedByKodeWilayah($relationAlias = null) Adds a INNER JOIN clause to the query using the SekolahRelatedByKodeWilayah relation
 *
 * @method MstWilayahQuery leftJoinSekolahRelatedByKodeWilayah($relationAlias = null) Adds a LEFT JOIN clause to the query using the SekolahRelatedByKodeWilayah relation
 * @method MstWilayahQuery rightJoinSekolahRelatedByKodeWilayah($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SekolahRelatedByKodeWilayah relation
 * @method MstWilayahQuery innerJoinSekolahRelatedByKodeWilayah($relationAlias = null) Adds a INNER JOIN clause to the query using the SekolahRelatedByKodeWilayah relation
 *
 * @method MstWilayahQuery leftJoinYayasanRelatedByKodeWilayah($relationAlias = null) Adds a LEFT JOIN clause to the query using the YayasanRelatedByKodeWilayah relation
 * @method MstWilayahQuery rightJoinYayasanRelatedByKodeWilayah($relationAlias = null) Adds a RIGHT JOIN clause to the query using the YayasanRelatedByKodeWilayah relation
 * @method MstWilayahQuery innerJoinYayasanRelatedByKodeWilayah($relationAlias = null) Adds a INNER JOIN clause to the query using the YayasanRelatedByKodeWilayah relation
 *
 * @method MstWilayahQuery leftJoinYayasanRelatedByKodeWilayah($relationAlias = null) Adds a LEFT JOIN clause to the query using the YayasanRelatedByKodeWilayah relation
 * @method MstWilayahQuery rightJoinYayasanRelatedByKodeWilayah($relationAlias = null) Adds a RIGHT JOIN clause to the query using the YayasanRelatedByKodeWilayah relation
 * @method MstWilayahQuery innerJoinYayasanRelatedByKodeWilayah($relationAlias = null) Adds a INNER JOIN clause to the query using the YayasanRelatedByKodeWilayah relation
 *
 * @method MstWilayahQuery leftJoinPtkRelatedByKodeWilayah($relationAlias = null) Adds a LEFT JOIN clause to the query using the PtkRelatedByKodeWilayah relation
 * @method MstWilayahQuery rightJoinPtkRelatedByKodeWilayah($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PtkRelatedByKodeWilayah relation
 * @method MstWilayahQuery innerJoinPtkRelatedByKodeWilayah($relationAlias = null) Adds a INNER JOIN clause to the query using the PtkRelatedByKodeWilayah relation
 *
 * @method MstWilayahQuery leftJoinPtkRelatedByKodeWilayah($relationAlias = null) Adds a LEFT JOIN clause to the query using the PtkRelatedByKodeWilayah relation
 * @method MstWilayahQuery rightJoinPtkRelatedByKodeWilayah($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PtkRelatedByKodeWilayah relation
 * @method MstWilayahQuery innerJoinPtkRelatedByKodeWilayah($relationAlias = null) Adds a INNER JOIN clause to the query using the PtkRelatedByKodeWilayah relation
 *
 * @method MstWilayahQuery leftJoinWtSrcSyncLogRelatedByKodeWilayah($relationAlias = null) Adds a LEFT JOIN clause to the query using the WtSrcSyncLogRelatedByKodeWilayah relation
 * @method MstWilayahQuery rightJoinWtSrcSyncLogRelatedByKodeWilayah($relationAlias = null) Adds a RIGHT JOIN clause to the query using the WtSrcSyncLogRelatedByKodeWilayah relation
 * @method MstWilayahQuery innerJoinWtSrcSyncLogRelatedByKodeWilayah($relationAlias = null) Adds a INNER JOIN clause to the query using the WtSrcSyncLogRelatedByKodeWilayah relation
 *
 * @method MstWilayahQuery leftJoinWtSrcSyncLogRelatedByKodeWilayah($relationAlias = null) Adds a LEFT JOIN clause to the query using the WtSrcSyncLogRelatedByKodeWilayah relation
 * @method MstWilayahQuery rightJoinWtSrcSyncLogRelatedByKodeWilayah($relationAlias = null) Adds a RIGHT JOIN clause to the query using the WtSrcSyncLogRelatedByKodeWilayah relation
 * @method MstWilayahQuery innerJoinWtSrcSyncLogRelatedByKodeWilayah($relationAlias = null) Adds a INNER JOIN clause to the query using the WtSrcSyncLogRelatedByKodeWilayah relation
 *
 * @method MstWilayahQuery leftJoinWtDstSyncLogRelatedByKodeWilayah($relationAlias = null) Adds a LEFT JOIN clause to the query using the WtDstSyncLogRelatedByKodeWilayah relation
 * @method MstWilayahQuery rightJoinWtDstSyncLogRelatedByKodeWilayah($relationAlias = null) Adds a RIGHT JOIN clause to the query using the WtDstSyncLogRelatedByKodeWilayah relation
 * @method MstWilayahQuery innerJoinWtDstSyncLogRelatedByKodeWilayah($relationAlias = null) Adds a INNER JOIN clause to the query using the WtDstSyncLogRelatedByKodeWilayah relation
 *
 * @method MstWilayahQuery leftJoinWtDstSyncLogRelatedByKodeWilayah($relationAlias = null) Adds a LEFT JOIN clause to the query using the WtDstSyncLogRelatedByKodeWilayah relation
 * @method MstWilayahQuery rightJoinWtDstSyncLogRelatedByKodeWilayah($relationAlias = null) Adds a RIGHT JOIN clause to the query using the WtDstSyncLogRelatedByKodeWilayah relation
 * @method MstWilayahQuery innerJoinWtDstSyncLogRelatedByKodeWilayah($relationAlias = null) Adds a INNER JOIN clause to the query using the WtDstSyncLogRelatedByKodeWilayah relation
 *
 * @method MstWilayahQuery leftJoinWsyncSessionRelatedByKodeWilayah($relationAlias = null) Adds a LEFT JOIN clause to the query using the WsyncSessionRelatedByKodeWilayah relation
 * @method MstWilayahQuery rightJoinWsyncSessionRelatedByKodeWilayah($relationAlias = null) Adds a RIGHT JOIN clause to the query using the WsyncSessionRelatedByKodeWilayah relation
 * @method MstWilayahQuery innerJoinWsyncSessionRelatedByKodeWilayah($relationAlias = null) Adds a INNER JOIN clause to the query using the WsyncSessionRelatedByKodeWilayah relation
 *
 * @method MstWilayahQuery leftJoinWsyncSessionRelatedByKodeWilayah($relationAlias = null) Adds a LEFT JOIN clause to the query using the WsyncSessionRelatedByKodeWilayah relation
 * @method MstWilayahQuery rightJoinWsyncSessionRelatedByKodeWilayah($relationAlias = null) Adds a RIGHT JOIN clause to the query using the WsyncSessionRelatedByKodeWilayah relation
 * @method MstWilayahQuery innerJoinWsyncSessionRelatedByKodeWilayah($relationAlias = null) Adds a INNER JOIN clause to the query using the WsyncSessionRelatedByKodeWilayah relation
 *
 * @method MstWilayahQuery leftJoinWsrcSyncLogRelatedByKodeWilayah($relationAlias = null) Adds a LEFT JOIN clause to the query using the WsrcSyncLogRelatedByKodeWilayah relation
 * @method MstWilayahQuery rightJoinWsrcSyncLogRelatedByKodeWilayah($relationAlias = null) Adds a RIGHT JOIN clause to the query using the WsrcSyncLogRelatedByKodeWilayah relation
 * @method MstWilayahQuery innerJoinWsrcSyncLogRelatedByKodeWilayah($relationAlias = null) Adds a INNER JOIN clause to the query using the WsrcSyncLogRelatedByKodeWilayah relation
 *
 * @method MstWilayahQuery leftJoinWsrcSyncLogRelatedByKodeWilayah($relationAlias = null) Adds a LEFT JOIN clause to the query using the WsrcSyncLogRelatedByKodeWilayah relation
 * @method MstWilayahQuery rightJoinWsrcSyncLogRelatedByKodeWilayah($relationAlias = null) Adds a RIGHT JOIN clause to the query using the WsrcSyncLogRelatedByKodeWilayah relation
 * @method MstWilayahQuery innerJoinWsrcSyncLogRelatedByKodeWilayah($relationAlias = null) Adds a INNER JOIN clause to the query using the WsrcSyncLogRelatedByKodeWilayah relation
 *
 * @method MstWilayahQuery leftJoinWdstSyncLogRelatedByKodeWilayah($relationAlias = null) Adds a LEFT JOIN clause to the query using the WdstSyncLogRelatedByKodeWilayah relation
 * @method MstWilayahQuery rightJoinWdstSyncLogRelatedByKodeWilayah($relationAlias = null) Adds a RIGHT JOIN clause to the query using the WdstSyncLogRelatedByKodeWilayah relation
 * @method MstWilayahQuery innerJoinWdstSyncLogRelatedByKodeWilayah($relationAlias = null) Adds a INNER JOIN clause to the query using the WdstSyncLogRelatedByKodeWilayah relation
 *
 * @method MstWilayahQuery leftJoinWdstSyncLogRelatedByKodeWilayah($relationAlias = null) Adds a LEFT JOIN clause to the query using the WdstSyncLogRelatedByKodeWilayah relation
 * @method MstWilayahQuery rightJoinWdstSyncLogRelatedByKodeWilayah($relationAlias = null) Adds a RIGHT JOIN clause to the query using the WdstSyncLogRelatedByKodeWilayah relation
 * @method MstWilayahQuery innerJoinWdstSyncLogRelatedByKodeWilayah($relationAlias = null) Adds a INNER JOIN clause to the query using the WdstSyncLogRelatedByKodeWilayah relation
 *
 * @method MstWilayahQuery leftJoinWPendingJobRelatedByKodeWilayah($relationAlias = null) Adds a LEFT JOIN clause to the query using the WPendingJobRelatedByKodeWilayah relation
 * @method MstWilayahQuery rightJoinWPendingJobRelatedByKodeWilayah($relationAlias = null) Adds a RIGHT JOIN clause to the query using the WPendingJobRelatedByKodeWilayah relation
 * @method MstWilayahQuery innerJoinWPendingJobRelatedByKodeWilayah($relationAlias = null) Adds a INNER JOIN clause to the query using the WPendingJobRelatedByKodeWilayah relation
 *
 * @method MstWilayahQuery leftJoinWPendingJobRelatedByKodeWilayah($relationAlias = null) Adds a LEFT JOIN clause to the query using the WPendingJobRelatedByKodeWilayah relation
 * @method MstWilayahQuery rightJoinWPendingJobRelatedByKodeWilayah($relationAlias = null) Adds a RIGHT JOIN clause to the query using the WPendingJobRelatedByKodeWilayah relation
 * @method MstWilayahQuery innerJoinWPendingJobRelatedByKodeWilayah($relationAlias = null) Adds a INNER JOIN clause to the query using the WPendingJobRelatedByKodeWilayah relation
 *
 * @method MstWilayahQuery leftJoinLembagaNonSekolahRelatedByKodeWilayah($relationAlias = null) Adds a LEFT JOIN clause to the query using the LembagaNonSekolahRelatedByKodeWilayah relation
 * @method MstWilayahQuery rightJoinLembagaNonSekolahRelatedByKodeWilayah($relationAlias = null) Adds a RIGHT JOIN clause to the query using the LembagaNonSekolahRelatedByKodeWilayah relation
 * @method MstWilayahQuery innerJoinLembagaNonSekolahRelatedByKodeWilayah($relationAlias = null) Adds a INNER JOIN clause to the query using the LembagaNonSekolahRelatedByKodeWilayah relation
 *
 * @method MstWilayahQuery leftJoinLembagaNonSekolahRelatedByKodeWilayah($relationAlias = null) Adds a LEFT JOIN clause to the query using the LembagaNonSekolahRelatedByKodeWilayah relation
 * @method MstWilayahQuery rightJoinLembagaNonSekolahRelatedByKodeWilayah($relationAlias = null) Adds a RIGHT JOIN clause to the query using the LembagaNonSekolahRelatedByKodeWilayah relation
 * @method MstWilayahQuery innerJoinLembagaNonSekolahRelatedByKodeWilayah($relationAlias = null) Adds a INNER JOIN clause to the query using the LembagaNonSekolahRelatedByKodeWilayah relation
 *
 * @method MstWilayahQuery leftJoinDemografiRelatedByKodeWilayah($relationAlias = null) Adds a LEFT JOIN clause to the query using the DemografiRelatedByKodeWilayah relation
 * @method MstWilayahQuery rightJoinDemografiRelatedByKodeWilayah($relationAlias = null) Adds a RIGHT JOIN clause to the query using the DemografiRelatedByKodeWilayah relation
 * @method MstWilayahQuery innerJoinDemografiRelatedByKodeWilayah($relationAlias = null) Adds a INNER JOIN clause to the query using the DemografiRelatedByKodeWilayah relation
 *
 * @method MstWilayahQuery leftJoinDemografiRelatedByKodeWilayah($relationAlias = null) Adds a LEFT JOIN clause to the query using the DemografiRelatedByKodeWilayah relation
 * @method MstWilayahQuery rightJoinDemografiRelatedByKodeWilayah($relationAlias = null) Adds a RIGHT JOIN clause to the query using the DemografiRelatedByKodeWilayah relation
 * @method MstWilayahQuery innerJoinDemografiRelatedByKodeWilayah($relationAlias = null) Adds a INNER JOIN clause to the query using the DemografiRelatedByKodeWilayah relation
 *
 * @method MstWilayah findOne(PropelPDO $con = null) Return the first MstWilayah matching the query
 * @method MstWilayah findOneOrCreate(PropelPDO $con = null) Return the first MstWilayah matching the query, or a new MstWilayah object populated from the query conditions when no match is found
 *
 * @method MstWilayah findOneByNama(string $nama) Return the first MstWilayah filtered by the nama column
 * @method MstWilayah findOneByIdLevelWilayah(int $id_level_wilayah) Return the first MstWilayah filtered by the id_level_wilayah column
 * @method MstWilayah findOneByMstKodeWilayah(string $mst_kode_wilayah) Return the first MstWilayah filtered by the mst_kode_wilayah column
 * @method MstWilayah findOneByNegaraId(string $negara_id) Return the first MstWilayah filtered by the negara_id column
 * @method MstWilayah findOneByAsalWilayah(string $asal_wilayah) Return the first MstWilayah filtered by the asal_wilayah column
 * @method MstWilayah findOneByKodeBps(string $kode_bps) Return the first MstWilayah filtered by the kode_bps column
 * @method MstWilayah findOneByKodeDagri(string $kode_dagri) Return the first MstWilayah filtered by the kode_dagri column
 * @method MstWilayah findOneByKodeKeu(string $kode_keu) Return the first MstWilayah filtered by the kode_keu column
 * @method MstWilayah findOneByCreateDate(string $create_date) Return the first MstWilayah filtered by the create_date column
 * @method MstWilayah findOneByLastUpdate(string $last_update) Return the first MstWilayah filtered by the last_update column
 * @method MstWilayah findOneByExpiredDate(string $expired_date) Return the first MstWilayah filtered by the expired_date column
 * @method MstWilayah findOneByLastSync(string $last_sync) Return the first MstWilayah filtered by the last_sync column
 *
 * @method array findByKodeWilayah(string $kode_wilayah) Return MstWilayah objects filtered by the kode_wilayah column
 * @method array findByNama(string $nama) Return MstWilayah objects filtered by the nama column
 * @method array findByIdLevelWilayah(int $id_level_wilayah) Return MstWilayah objects filtered by the id_level_wilayah column
 * @method array findByMstKodeWilayah(string $mst_kode_wilayah) Return MstWilayah objects filtered by the mst_kode_wilayah column
 * @method array findByNegaraId(string $negara_id) Return MstWilayah objects filtered by the negara_id column
 * @method array findByAsalWilayah(string $asal_wilayah) Return MstWilayah objects filtered by the asal_wilayah column
 * @method array findByKodeBps(string $kode_bps) Return MstWilayah objects filtered by the kode_bps column
 * @method array findByKodeDagri(string $kode_dagri) Return MstWilayah objects filtered by the kode_dagri column
 * @method array findByKodeKeu(string $kode_keu) Return MstWilayah objects filtered by the kode_keu column
 * @method array findByCreateDate(string $create_date) Return MstWilayah objects filtered by the create_date column
 * @method array findByLastUpdate(string $last_update) Return MstWilayah objects filtered by the last_update column
 * @method array findByExpiredDate(string $expired_date) Return MstWilayah objects filtered by the expired_date column
 * @method array findByLastSync(string $last_sync) Return MstWilayah objects filtered by the last_sync column
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseMstWilayahQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseMstWilayahQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Dapodikmen', $modelName = 'angulex\\Model\\MstWilayah', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new MstWilayahQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   MstWilayahQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return MstWilayahQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof MstWilayahQuery) {
            return $criteria;
        }
        $query = new MstWilayahQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   MstWilayah|MstWilayah[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = MstWilayahPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(MstWilayahPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 MstWilayah A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByKodeWilayah($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 MstWilayah A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [kode_wilayah], [nama], [id_level_wilayah], [mst_kode_wilayah], [negara_id], [asal_wilayah], [kode_bps], [kode_dagri], [kode_keu], [create_date], [last_update], [expired_date], [last_sync] FROM [ref].[mst_wilayah] WHERE [kode_wilayah] = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new MstWilayah();
            $obj->hydrate($row);
            MstWilayahPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return MstWilayah|MstWilayah[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|MstWilayah[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(MstWilayahPeer::KODE_WILAYAH, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(MstWilayahPeer::KODE_WILAYAH, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the kode_wilayah column
     *
     * Example usage:
     * <code>
     * $query->filterByKodeWilayah('fooValue');   // WHERE kode_wilayah = 'fooValue'
     * $query->filterByKodeWilayah('%fooValue%'); // WHERE kode_wilayah LIKE '%fooValue%'
     * </code>
     *
     * @param     string $kodeWilayah The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function filterByKodeWilayah($kodeWilayah = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($kodeWilayah)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $kodeWilayah)) {
                $kodeWilayah = str_replace('*', '%', $kodeWilayah);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MstWilayahPeer::KODE_WILAYAH, $kodeWilayah, $comparison);
    }

    /**
     * Filter the query on the nama column
     *
     * Example usage:
     * <code>
     * $query->filterByNama('fooValue');   // WHERE nama = 'fooValue'
     * $query->filterByNama('%fooValue%'); // WHERE nama LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nama The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function filterByNama($nama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nama)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nama)) {
                $nama = str_replace('*', '%', $nama);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MstWilayahPeer::NAMA, $nama, $comparison);
    }

    /**
     * Filter the query on the id_level_wilayah column
     *
     * Example usage:
     * <code>
     * $query->filterByIdLevelWilayah(1234); // WHERE id_level_wilayah = 1234
     * $query->filterByIdLevelWilayah(array(12, 34)); // WHERE id_level_wilayah IN (12, 34)
     * $query->filterByIdLevelWilayah(array('min' => 12)); // WHERE id_level_wilayah >= 12
     * $query->filterByIdLevelWilayah(array('max' => 12)); // WHERE id_level_wilayah <= 12
     * </code>
     *
     * @see       filterByLevelWilayah()
     *
     * @param     mixed $idLevelWilayah The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function filterByIdLevelWilayah($idLevelWilayah = null, $comparison = null)
    {
        if (is_array($idLevelWilayah)) {
            $useMinMax = false;
            if (isset($idLevelWilayah['min'])) {
                $this->addUsingAlias(MstWilayahPeer::ID_LEVEL_WILAYAH, $idLevelWilayah['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idLevelWilayah['max'])) {
                $this->addUsingAlias(MstWilayahPeer::ID_LEVEL_WILAYAH, $idLevelWilayah['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MstWilayahPeer::ID_LEVEL_WILAYAH, $idLevelWilayah, $comparison);
    }

    /**
     * Filter the query on the mst_kode_wilayah column
     *
     * Example usage:
     * <code>
     * $query->filterByMstKodeWilayah('fooValue');   // WHERE mst_kode_wilayah = 'fooValue'
     * $query->filterByMstKodeWilayah('%fooValue%'); // WHERE mst_kode_wilayah LIKE '%fooValue%'
     * </code>
     *
     * @param     string $mstKodeWilayah The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function filterByMstKodeWilayah($mstKodeWilayah = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($mstKodeWilayah)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $mstKodeWilayah)) {
                $mstKodeWilayah = str_replace('*', '%', $mstKodeWilayah);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MstWilayahPeer::MST_KODE_WILAYAH, $mstKodeWilayah, $comparison);
    }

    /**
     * Filter the query on the negara_id column
     *
     * Example usage:
     * <code>
     * $query->filterByNegaraId('fooValue');   // WHERE negara_id = 'fooValue'
     * $query->filterByNegaraId('%fooValue%'); // WHERE negara_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $negaraId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function filterByNegaraId($negaraId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($negaraId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $negaraId)) {
                $negaraId = str_replace('*', '%', $negaraId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MstWilayahPeer::NEGARA_ID, $negaraId, $comparison);
    }

    /**
     * Filter the query on the asal_wilayah column
     *
     * Example usage:
     * <code>
     * $query->filterByAsalWilayah('fooValue');   // WHERE asal_wilayah = 'fooValue'
     * $query->filterByAsalWilayah('%fooValue%'); // WHERE asal_wilayah LIKE '%fooValue%'
     * </code>
     *
     * @param     string $asalWilayah The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function filterByAsalWilayah($asalWilayah = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($asalWilayah)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $asalWilayah)) {
                $asalWilayah = str_replace('*', '%', $asalWilayah);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MstWilayahPeer::ASAL_WILAYAH, $asalWilayah, $comparison);
    }

    /**
     * Filter the query on the kode_bps column
     *
     * Example usage:
     * <code>
     * $query->filterByKodeBps('fooValue');   // WHERE kode_bps = 'fooValue'
     * $query->filterByKodeBps('%fooValue%'); // WHERE kode_bps LIKE '%fooValue%'
     * </code>
     *
     * @param     string $kodeBps The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function filterByKodeBps($kodeBps = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($kodeBps)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $kodeBps)) {
                $kodeBps = str_replace('*', '%', $kodeBps);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MstWilayahPeer::KODE_BPS, $kodeBps, $comparison);
    }

    /**
     * Filter the query on the kode_dagri column
     *
     * Example usage:
     * <code>
     * $query->filterByKodeDagri('fooValue');   // WHERE kode_dagri = 'fooValue'
     * $query->filterByKodeDagri('%fooValue%'); // WHERE kode_dagri LIKE '%fooValue%'
     * </code>
     *
     * @param     string $kodeDagri The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function filterByKodeDagri($kodeDagri = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($kodeDagri)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $kodeDagri)) {
                $kodeDagri = str_replace('*', '%', $kodeDagri);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MstWilayahPeer::KODE_DAGRI, $kodeDagri, $comparison);
    }

    /**
     * Filter the query on the kode_keu column
     *
     * Example usage:
     * <code>
     * $query->filterByKodeKeu('fooValue');   // WHERE kode_keu = 'fooValue'
     * $query->filterByKodeKeu('%fooValue%'); // WHERE kode_keu LIKE '%fooValue%'
     * </code>
     *
     * @param     string $kodeKeu The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function filterByKodeKeu($kodeKeu = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($kodeKeu)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $kodeKeu)) {
                $kodeKeu = str_replace('*', '%', $kodeKeu);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MstWilayahPeer::KODE_KEU, $kodeKeu, $comparison);
    }

    /**
     * Filter the query on the create_date column
     *
     * Example usage:
     * <code>
     * $query->filterByCreateDate('2011-03-14'); // WHERE create_date = '2011-03-14'
     * $query->filterByCreateDate('now'); // WHERE create_date = '2011-03-14'
     * $query->filterByCreateDate(array('max' => 'yesterday')); // WHERE create_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $createDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function filterByCreateDate($createDate = null, $comparison = null)
    {
        if (is_array($createDate)) {
            $useMinMax = false;
            if (isset($createDate['min'])) {
                $this->addUsingAlias(MstWilayahPeer::CREATE_DATE, $createDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createDate['max'])) {
                $this->addUsingAlias(MstWilayahPeer::CREATE_DATE, $createDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MstWilayahPeer::CREATE_DATE, $createDate, $comparison);
    }

    /**
     * Filter the query on the last_update column
     *
     * Example usage:
     * <code>
     * $query->filterByLastUpdate('2011-03-14'); // WHERE last_update = '2011-03-14'
     * $query->filterByLastUpdate('now'); // WHERE last_update = '2011-03-14'
     * $query->filterByLastUpdate(array('max' => 'yesterday')); // WHERE last_update > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastUpdate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function filterByLastUpdate($lastUpdate = null, $comparison = null)
    {
        if (is_array($lastUpdate)) {
            $useMinMax = false;
            if (isset($lastUpdate['min'])) {
                $this->addUsingAlias(MstWilayahPeer::LAST_UPDATE, $lastUpdate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastUpdate['max'])) {
                $this->addUsingAlias(MstWilayahPeer::LAST_UPDATE, $lastUpdate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MstWilayahPeer::LAST_UPDATE, $lastUpdate, $comparison);
    }

    /**
     * Filter the query on the expired_date column
     *
     * Example usage:
     * <code>
     * $query->filterByExpiredDate('2011-03-14'); // WHERE expired_date = '2011-03-14'
     * $query->filterByExpiredDate('now'); // WHERE expired_date = '2011-03-14'
     * $query->filterByExpiredDate(array('max' => 'yesterday')); // WHERE expired_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $expiredDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function filterByExpiredDate($expiredDate = null, $comparison = null)
    {
        if (is_array($expiredDate)) {
            $useMinMax = false;
            if (isset($expiredDate['min'])) {
                $this->addUsingAlias(MstWilayahPeer::EXPIRED_DATE, $expiredDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($expiredDate['max'])) {
                $this->addUsingAlias(MstWilayahPeer::EXPIRED_DATE, $expiredDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MstWilayahPeer::EXPIRED_DATE, $expiredDate, $comparison);
    }

    /**
     * Filter the query on the last_sync column
     *
     * Example usage:
     * <code>
     * $query->filterByLastSync('2011-03-14'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync('now'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync(array('max' => 'yesterday')); // WHERE last_sync > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastSync The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function filterByLastSync($lastSync = null, $comparison = null)
    {
        if (is_array($lastSync)) {
            $useMinMax = false;
            if (isset($lastSync['min'])) {
                $this->addUsingAlias(MstWilayahPeer::LAST_SYNC, $lastSync['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastSync['max'])) {
                $this->addUsingAlias(MstWilayahPeer::LAST_SYNC, $lastSync['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MstWilayahPeer::LAST_SYNC, $lastSync, $comparison);
    }

    /**
     * Filter the query by a related LevelWilayah object
     *
     * @param   LevelWilayah|PropelObjectCollection $levelWilayah The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 MstWilayahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByLevelWilayah($levelWilayah, $comparison = null)
    {
        if ($levelWilayah instanceof LevelWilayah) {
            return $this
                ->addUsingAlias(MstWilayahPeer::ID_LEVEL_WILAYAH, $levelWilayah->getIdLevelWilayah(), $comparison);
        } elseif ($levelWilayah instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(MstWilayahPeer::ID_LEVEL_WILAYAH, $levelWilayah->toKeyValue('PrimaryKey', 'IdLevelWilayah'), $comparison);
        } else {
            throw new PropelException('filterByLevelWilayah() only accepts arguments of type LevelWilayah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the LevelWilayah relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function joinLevelWilayah($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('LevelWilayah');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'LevelWilayah');
        }

        return $this;
    }

    /**
     * Use the LevelWilayah relation LevelWilayah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\LevelWilayahQuery A secondary query class using the current class as primary query
     */
    public function useLevelWilayahQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinLevelWilayah($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'LevelWilayah', '\angulex\Model\LevelWilayahQuery');
    }

    /**
     * Filter the query by a related MstWilayah object
     *
     * @param   MstWilayah|PropelObjectCollection $mstWilayah The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 MstWilayahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByMstWilayahRelatedByMstKodeWilayah($mstWilayah, $comparison = null)
    {
        if ($mstWilayah instanceof MstWilayah) {
            return $this
                ->addUsingAlias(MstWilayahPeer::MST_KODE_WILAYAH, $mstWilayah->getKodeWilayah(), $comparison);
        } elseif ($mstWilayah instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(MstWilayahPeer::MST_KODE_WILAYAH, $mstWilayah->toKeyValue('PrimaryKey', 'KodeWilayah'), $comparison);
        } else {
            throw new PropelException('filterByMstWilayahRelatedByMstKodeWilayah() only accepts arguments of type MstWilayah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MstWilayahRelatedByMstKodeWilayah relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function joinMstWilayahRelatedByMstKodeWilayah($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MstWilayahRelatedByMstKodeWilayah');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MstWilayahRelatedByMstKodeWilayah');
        }

        return $this;
    }

    /**
     * Use the MstWilayahRelatedByMstKodeWilayah relation MstWilayah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\MstWilayahQuery A secondary query class using the current class as primary query
     */
    public function useMstWilayahRelatedByMstKodeWilayahQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinMstWilayahRelatedByMstKodeWilayah($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MstWilayahRelatedByMstKodeWilayah', '\angulex\Model\MstWilayahQuery');
    }

    /**
     * Filter the query by a related Negara object
     *
     * @param   Negara|PropelObjectCollection $negara The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 MstWilayahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByNegara($negara, $comparison = null)
    {
        if ($negara instanceof Negara) {
            return $this
                ->addUsingAlias(MstWilayahPeer::NEGARA_ID, $negara->getNegaraId(), $comparison);
        } elseif ($negara instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(MstWilayahPeer::NEGARA_ID, $negara->toKeyValue('PrimaryKey', 'NegaraId'), $comparison);
        } else {
            throw new PropelException('filterByNegara() only accepts arguments of type Negara or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Negara relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function joinNegara($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Negara');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Negara');
        }

        return $this;
    }

    /**
     * Use the Negara relation Negara object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\NegaraQuery A secondary query class using the current class as primary query
     */
    public function useNegaraQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinNegara($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Negara', '\angulex\Model\NegaraQuery');
    }

    /**
     * Filter the query by a related TetanggaKabkota object
     *
     * @param   TetanggaKabkota|PropelObjectCollection $tetanggaKabkota  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 MstWilayahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByTetanggaKabkotaRelatedByKodeWilayah($tetanggaKabkota, $comparison = null)
    {
        if ($tetanggaKabkota instanceof TetanggaKabkota) {
            return $this
                ->addUsingAlias(MstWilayahPeer::KODE_WILAYAH, $tetanggaKabkota->getKodeWilayah(), $comparison);
        } elseif ($tetanggaKabkota instanceof PropelObjectCollection) {
            return $this
                ->useTetanggaKabkotaRelatedByKodeWilayahQuery()
                ->filterByPrimaryKeys($tetanggaKabkota->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByTetanggaKabkotaRelatedByKodeWilayah() only accepts arguments of type TetanggaKabkota or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the TetanggaKabkotaRelatedByKodeWilayah relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function joinTetanggaKabkotaRelatedByKodeWilayah($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('TetanggaKabkotaRelatedByKodeWilayah');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'TetanggaKabkotaRelatedByKodeWilayah');
        }

        return $this;
    }

    /**
     * Use the TetanggaKabkotaRelatedByKodeWilayah relation TetanggaKabkota object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\TetanggaKabkotaQuery A secondary query class using the current class as primary query
     */
    public function useTetanggaKabkotaRelatedByKodeWilayahQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTetanggaKabkotaRelatedByKodeWilayah($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'TetanggaKabkotaRelatedByKodeWilayah', '\angulex\Model\TetanggaKabkotaQuery');
    }

    /**
     * Filter the query by a related TetanggaKabkota object
     *
     * @param   TetanggaKabkota|PropelObjectCollection $tetanggaKabkota  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 MstWilayahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByTetanggaKabkotaRelatedByMstKodeWilayah($tetanggaKabkota, $comparison = null)
    {
        if ($tetanggaKabkota instanceof TetanggaKabkota) {
            return $this
                ->addUsingAlias(MstWilayahPeer::KODE_WILAYAH, $tetanggaKabkota->getMstKodeWilayah(), $comparison);
        } elseif ($tetanggaKabkota instanceof PropelObjectCollection) {
            return $this
                ->useTetanggaKabkotaRelatedByMstKodeWilayahQuery()
                ->filterByPrimaryKeys($tetanggaKabkota->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByTetanggaKabkotaRelatedByMstKodeWilayah() only accepts arguments of type TetanggaKabkota or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the TetanggaKabkotaRelatedByMstKodeWilayah relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function joinTetanggaKabkotaRelatedByMstKodeWilayah($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('TetanggaKabkotaRelatedByMstKodeWilayah');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'TetanggaKabkotaRelatedByMstKodeWilayah');
        }

        return $this;
    }

    /**
     * Use the TetanggaKabkotaRelatedByMstKodeWilayah relation TetanggaKabkota object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\TetanggaKabkotaQuery A secondary query class using the current class as primary query
     */
    public function useTetanggaKabkotaRelatedByMstKodeWilayahQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTetanggaKabkotaRelatedByMstKodeWilayah($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'TetanggaKabkotaRelatedByMstKodeWilayah', '\angulex\Model\TetanggaKabkotaQuery');
    }

    /**
     * Filter the query by a related PesertaDidik object
     *
     * @param   PesertaDidik|PropelObjectCollection $pesertaDidik  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 MstWilayahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPesertaDidikRelatedByKodeWilayah($pesertaDidik, $comparison = null)
    {
        if ($pesertaDidik instanceof PesertaDidik) {
            return $this
                ->addUsingAlias(MstWilayahPeer::KODE_WILAYAH, $pesertaDidik->getKodeWilayah(), $comparison);
        } elseif ($pesertaDidik instanceof PropelObjectCollection) {
            return $this
                ->usePesertaDidikRelatedByKodeWilayahQuery()
                ->filterByPrimaryKeys($pesertaDidik->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPesertaDidikRelatedByKodeWilayah() only accepts arguments of type PesertaDidik or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PesertaDidikRelatedByKodeWilayah relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function joinPesertaDidikRelatedByKodeWilayah($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PesertaDidikRelatedByKodeWilayah');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PesertaDidikRelatedByKodeWilayah');
        }

        return $this;
    }

    /**
     * Use the PesertaDidikRelatedByKodeWilayah relation PesertaDidik object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PesertaDidikQuery A secondary query class using the current class as primary query
     */
    public function usePesertaDidikRelatedByKodeWilayahQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPesertaDidikRelatedByKodeWilayah($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PesertaDidikRelatedByKodeWilayah', '\angulex\Model\PesertaDidikQuery');
    }

    /**
     * Filter the query by a related PesertaDidik object
     *
     * @param   PesertaDidik|PropelObjectCollection $pesertaDidik  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 MstWilayahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPesertaDidikRelatedByKodeWilayah($pesertaDidik, $comparison = null)
    {
        if ($pesertaDidik instanceof PesertaDidik) {
            return $this
                ->addUsingAlias(MstWilayahPeer::KODE_WILAYAH, $pesertaDidik->getKodeWilayah(), $comparison);
        } elseif ($pesertaDidik instanceof PropelObjectCollection) {
            return $this
                ->usePesertaDidikRelatedByKodeWilayahQuery()
                ->filterByPrimaryKeys($pesertaDidik->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPesertaDidikRelatedByKodeWilayah() only accepts arguments of type PesertaDidik or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PesertaDidikRelatedByKodeWilayah relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function joinPesertaDidikRelatedByKodeWilayah($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PesertaDidikRelatedByKodeWilayah');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PesertaDidikRelatedByKodeWilayah');
        }

        return $this;
    }

    /**
     * Use the PesertaDidikRelatedByKodeWilayah relation PesertaDidik object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PesertaDidikQuery A secondary query class using the current class as primary query
     */
    public function usePesertaDidikRelatedByKodeWilayahQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPesertaDidikRelatedByKodeWilayah($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PesertaDidikRelatedByKodeWilayah', '\angulex\Model\PesertaDidikQuery');
    }

    /**
     * Filter the query by a related MstWilayah object
     *
     * @param   MstWilayah|PropelObjectCollection $mstWilayah  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 MstWilayahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByMstWilayahRelatedByKodeWilayah($mstWilayah, $comparison = null)
    {
        if ($mstWilayah instanceof MstWilayah) {
            return $this
                ->addUsingAlias(MstWilayahPeer::KODE_WILAYAH, $mstWilayah->getMstKodeWilayah(), $comparison);
        } elseif ($mstWilayah instanceof PropelObjectCollection) {
            return $this
                ->useMstWilayahRelatedByKodeWilayahQuery()
                ->filterByPrimaryKeys($mstWilayah->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByMstWilayahRelatedByKodeWilayah() only accepts arguments of type MstWilayah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MstWilayahRelatedByKodeWilayah relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function joinMstWilayahRelatedByKodeWilayah($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MstWilayahRelatedByKodeWilayah');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MstWilayahRelatedByKodeWilayah');
        }

        return $this;
    }

    /**
     * Use the MstWilayahRelatedByKodeWilayah relation MstWilayah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\MstWilayahQuery A secondary query class using the current class as primary query
     */
    public function useMstWilayahRelatedByKodeWilayahQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinMstWilayahRelatedByKodeWilayah($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MstWilayahRelatedByKodeWilayah', '\angulex\Model\MstWilayahQuery');
    }

    /**
     * Filter the query by a related Pengguna object
     *
     * @param   Pengguna|PropelObjectCollection $pengguna  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 MstWilayahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPenggunaRelatedByKodeWilayah($pengguna, $comparison = null)
    {
        if ($pengguna instanceof Pengguna) {
            return $this
                ->addUsingAlias(MstWilayahPeer::KODE_WILAYAH, $pengguna->getKodeWilayah(), $comparison);
        } elseif ($pengguna instanceof PropelObjectCollection) {
            return $this
                ->usePenggunaRelatedByKodeWilayahQuery()
                ->filterByPrimaryKeys($pengguna->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPenggunaRelatedByKodeWilayah() only accepts arguments of type Pengguna or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PenggunaRelatedByKodeWilayah relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function joinPenggunaRelatedByKodeWilayah($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PenggunaRelatedByKodeWilayah');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PenggunaRelatedByKodeWilayah');
        }

        return $this;
    }

    /**
     * Use the PenggunaRelatedByKodeWilayah relation Pengguna object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PenggunaQuery A secondary query class using the current class as primary query
     */
    public function usePenggunaRelatedByKodeWilayahQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPenggunaRelatedByKodeWilayah($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PenggunaRelatedByKodeWilayah', '\angulex\Model\PenggunaQuery');
    }

    /**
     * Filter the query by a related Pengguna object
     *
     * @param   Pengguna|PropelObjectCollection $pengguna  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 MstWilayahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPenggunaRelatedByKodeWilayah($pengguna, $comparison = null)
    {
        if ($pengguna instanceof Pengguna) {
            return $this
                ->addUsingAlias(MstWilayahPeer::KODE_WILAYAH, $pengguna->getKodeWilayah(), $comparison);
        } elseif ($pengguna instanceof PropelObjectCollection) {
            return $this
                ->usePenggunaRelatedByKodeWilayahQuery()
                ->filterByPrimaryKeys($pengguna->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPenggunaRelatedByKodeWilayah() only accepts arguments of type Pengguna or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PenggunaRelatedByKodeWilayah relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function joinPenggunaRelatedByKodeWilayah($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PenggunaRelatedByKodeWilayah');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PenggunaRelatedByKodeWilayah');
        }

        return $this;
    }

    /**
     * Use the PenggunaRelatedByKodeWilayah relation Pengguna object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PenggunaQuery A secondary query class using the current class as primary query
     */
    public function usePenggunaRelatedByKodeWilayahQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPenggunaRelatedByKodeWilayah($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PenggunaRelatedByKodeWilayah', '\angulex\Model\PenggunaQuery');
    }

    /**
     * Filter the query by a related Sekolah object
     *
     * @param   Sekolah|PropelObjectCollection $sekolah  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 MstWilayahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySekolahRelatedByKodeWilayah($sekolah, $comparison = null)
    {
        if ($sekolah instanceof Sekolah) {
            return $this
                ->addUsingAlias(MstWilayahPeer::KODE_WILAYAH, $sekolah->getKodeWilayah(), $comparison);
        } elseif ($sekolah instanceof PropelObjectCollection) {
            return $this
                ->useSekolahRelatedByKodeWilayahQuery()
                ->filterByPrimaryKeys($sekolah->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySekolahRelatedByKodeWilayah() only accepts arguments of type Sekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SekolahRelatedByKodeWilayah relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function joinSekolahRelatedByKodeWilayah($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SekolahRelatedByKodeWilayah');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SekolahRelatedByKodeWilayah');
        }

        return $this;
    }

    /**
     * Use the SekolahRelatedByKodeWilayah relation Sekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SekolahQuery A secondary query class using the current class as primary query
     */
    public function useSekolahRelatedByKodeWilayahQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSekolahRelatedByKodeWilayah($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SekolahRelatedByKodeWilayah', '\angulex\Model\SekolahQuery');
    }

    /**
     * Filter the query by a related Sekolah object
     *
     * @param   Sekolah|PropelObjectCollection $sekolah  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 MstWilayahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySekolahRelatedByKodeWilayah($sekolah, $comparison = null)
    {
        if ($sekolah instanceof Sekolah) {
            return $this
                ->addUsingAlias(MstWilayahPeer::KODE_WILAYAH, $sekolah->getKodeWilayah(), $comparison);
        } elseif ($sekolah instanceof PropelObjectCollection) {
            return $this
                ->useSekolahRelatedByKodeWilayahQuery()
                ->filterByPrimaryKeys($sekolah->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySekolahRelatedByKodeWilayah() only accepts arguments of type Sekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SekolahRelatedByKodeWilayah relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function joinSekolahRelatedByKodeWilayah($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SekolahRelatedByKodeWilayah');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SekolahRelatedByKodeWilayah');
        }

        return $this;
    }

    /**
     * Use the SekolahRelatedByKodeWilayah relation Sekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SekolahQuery A secondary query class using the current class as primary query
     */
    public function useSekolahRelatedByKodeWilayahQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSekolahRelatedByKodeWilayah($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SekolahRelatedByKodeWilayah', '\angulex\Model\SekolahQuery');
    }

    /**
     * Filter the query by a related Yayasan object
     *
     * @param   Yayasan|PropelObjectCollection $yayasan  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 MstWilayahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByYayasanRelatedByKodeWilayah($yayasan, $comparison = null)
    {
        if ($yayasan instanceof Yayasan) {
            return $this
                ->addUsingAlias(MstWilayahPeer::KODE_WILAYAH, $yayasan->getKodeWilayah(), $comparison);
        } elseif ($yayasan instanceof PropelObjectCollection) {
            return $this
                ->useYayasanRelatedByKodeWilayahQuery()
                ->filterByPrimaryKeys($yayasan->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByYayasanRelatedByKodeWilayah() only accepts arguments of type Yayasan or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the YayasanRelatedByKodeWilayah relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function joinYayasanRelatedByKodeWilayah($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('YayasanRelatedByKodeWilayah');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'YayasanRelatedByKodeWilayah');
        }

        return $this;
    }

    /**
     * Use the YayasanRelatedByKodeWilayah relation Yayasan object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\YayasanQuery A secondary query class using the current class as primary query
     */
    public function useYayasanRelatedByKodeWilayahQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinYayasanRelatedByKodeWilayah($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'YayasanRelatedByKodeWilayah', '\angulex\Model\YayasanQuery');
    }

    /**
     * Filter the query by a related Yayasan object
     *
     * @param   Yayasan|PropelObjectCollection $yayasan  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 MstWilayahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByYayasanRelatedByKodeWilayah($yayasan, $comparison = null)
    {
        if ($yayasan instanceof Yayasan) {
            return $this
                ->addUsingAlias(MstWilayahPeer::KODE_WILAYAH, $yayasan->getKodeWilayah(), $comparison);
        } elseif ($yayasan instanceof PropelObjectCollection) {
            return $this
                ->useYayasanRelatedByKodeWilayahQuery()
                ->filterByPrimaryKeys($yayasan->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByYayasanRelatedByKodeWilayah() only accepts arguments of type Yayasan or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the YayasanRelatedByKodeWilayah relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function joinYayasanRelatedByKodeWilayah($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('YayasanRelatedByKodeWilayah');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'YayasanRelatedByKodeWilayah');
        }

        return $this;
    }

    /**
     * Use the YayasanRelatedByKodeWilayah relation Yayasan object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\YayasanQuery A secondary query class using the current class as primary query
     */
    public function useYayasanRelatedByKodeWilayahQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinYayasanRelatedByKodeWilayah($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'YayasanRelatedByKodeWilayah', '\angulex\Model\YayasanQuery');
    }

    /**
     * Filter the query by a related Ptk object
     *
     * @param   Ptk|PropelObjectCollection $ptk  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 MstWilayahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPtkRelatedByKodeWilayah($ptk, $comparison = null)
    {
        if ($ptk instanceof Ptk) {
            return $this
                ->addUsingAlias(MstWilayahPeer::KODE_WILAYAH, $ptk->getKodeWilayah(), $comparison);
        } elseif ($ptk instanceof PropelObjectCollection) {
            return $this
                ->usePtkRelatedByKodeWilayahQuery()
                ->filterByPrimaryKeys($ptk->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPtkRelatedByKodeWilayah() only accepts arguments of type Ptk or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PtkRelatedByKodeWilayah relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function joinPtkRelatedByKodeWilayah($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PtkRelatedByKodeWilayah');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PtkRelatedByKodeWilayah');
        }

        return $this;
    }

    /**
     * Use the PtkRelatedByKodeWilayah relation Ptk object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PtkQuery A secondary query class using the current class as primary query
     */
    public function usePtkRelatedByKodeWilayahQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPtkRelatedByKodeWilayah($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PtkRelatedByKodeWilayah', '\angulex\Model\PtkQuery');
    }

    /**
     * Filter the query by a related Ptk object
     *
     * @param   Ptk|PropelObjectCollection $ptk  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 MstWilayahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPtkRelatedByKodeWilayah($ptk, $comparison = null)
    {
        if ($ptk instanceof Ptk) {
            return $this
                ->addUsingAlias(MstWilayahPeer::KODE_WILAYAH, $ptk->getKodeWilayah(), $comparison);
        } elseif ($ptk instanceof PropelObjectCollection) {
            return $this
                ->usePtkRelatedByKodeWilayahQuery()
                ->filterByPrimaryKeys($ptk->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPtkRelatedByKodeWilayah() only accepts arguments of type Ptk or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PtkRelatedByKodeWilayah relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function joinPtkRelatedByKodeWilayah($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PtkRelatedByKodeWilayah');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PtkRelatedByKodeWilayah');
        }

        return $this;
    }

    /**
     * Use the PtkRelatedByKodeWilayah relation Ptk object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PtkQuery A secondary query class using the current class as primary query
     */
    public function usePtkRelatedByKodeWilayahQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPtkRelatedByKodeWilayah($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PtkRelatedByKodeWilayah', '\angulex\Model\PtkQuery');
    }

    /**
     * Filter the query by a related WtSrcSyncLog object
     *
     * @param   WtSrcSyncLog|PropelObjectCollection $wtSrcSyncLog  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 MstWilayahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByWtSrcSyncLogRelatedByKodeWilayah($wtSrcSyncLog, $comparison = null)
    {
        if ($wtSrcSyncLog instanceof WtSrcSyncLog) {
            return $this
                ->addUsingAlias(MstWilayahPeer::KODE_WILAYAH, $wtSrcSyncLog->getKodeWilayah(), $comparison);
        } elseif ($wtSrcSyncLog instanceof PropelObjectCollection) {
            return $this
                ->useWtSrcSyncLogRelatedByKodeWilayahQuery()
                ->filterByPrimaryKeys($wtSrcSyncLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByWtSrcSyncLogRelatedByKodeWilayah() only accepts arguments of type WtSrcSyncLog or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the WtSrcSyncLogRelatedByKodeWilayah relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function joinWtSrcSyncLogRelatedByKodeWilayah($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('WtSrcSyncLogRelatedByKodeWilayah');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'WtSrcSyncLogRelatedByKodeWilayah');
        }

        return $this;
    }

    /**
     * Use the WtSrcSyncLogRelatedByKodeWilayah relation WtSrcSyncLog object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\WtSrcSyncLogQuery A secondary query class using the current class as primary query
     */
    public function useWtSrcSyncLogRelatedByKodeWilayahQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinWtSrcSyncLogRelatedByKodeWilayah($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'WtSrcSyncLogRelatedByKodeWilayah', '\angulex\Model\WtSrcSyncLogQuery');
    }

    /**
     * Filter the query by a related WtSrcSyncLog object
     *
     * @param   WtSrcSyncLog|PropelObjectCollection $wtSrcSyncLog  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 MstWilayahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByWtSrcSyncLogRelatedByKodeWilayah($wtSrcSyncLog, $comparison = null)
    {
        if ($wtSrcSyncLog instanceof WtSrcSyncLog) {
            return $this
                ->addUsingAlias(MstWilayahPeer::KODE_WILAYAH, $wtSrcSyncLog->getKodeWilayah(), $comparison);
        } elseif ($wtSrcSyncLog instanceof PropelObjectCollection) {
            return $this
                ->useWtSrcSyncLogRelatedByKodeWilayahQuery()
                ->filterByPrimaryKeys($wtSrcSyncLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByWtSrcSyncLogRelatedByKodeWilayah() only accepts arguments of type WtSrcSyncLog or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the WtSrcSyncLogRelatedByKodeWilayah relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function joinWtSrcSyncLogRelatedByKodeWilayah($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('WtSrcSyncLogRelatedByKodeWilayah');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'WtSrcSyncLogRelatedByKodeWilayah');
        }

        return $this;
    }

    /**
     * Use the WtSrcSyncLogRelatedByKodeWilayah relation WtSrcSyncLog object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\WtSrcSyncLogQuery A secondary query class using the current class as primary query
     */
    public function useWtSrcSyncLogRelatedByKodeWilayahQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinWtSrcSyncLogRelatedByKodeWilayah($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'WtSrcSyncLogRelatedByKodeWilayah', '\angulex\Model\WtSrcSyncLogQuery');
    }

    /**
     * Filter the query by a related WtDstSyncLog object
     *
     * @param   WtDstSyncLog|PropelObjectCollection $wtDstSyncLog  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 MstWilayahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByWtDstSyncLogRelatedByKodeWilayah($wtDstSyncLog, $comparison = null)
    {
        if ($wtDstSyncLog instanceof WtDstSyncLog) {
            return $this
                ->addUsingAlias(MstWilayahPeer::KODE_WILAYAH, $wtDstSyncLog->getKodeWilayah(), $comparison);
        } elseif ($wtDstSyncLog instanceof PropelObjectCollection) {
            return $this
                ->useWtDstSyncLogRelatedByKodeWilayahQuery()
                ->filterByPrimaryKeys($wtDstSyncLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByWtDstSyncLogRelatedByKodeWilayah() only accepts arguments of type WtDstSyncLog or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the WtDstSyncLogRelatedByKodeWilayah relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function joinWtDstSyncLogRelatedByKodeWilayah($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('WtDstSyncLogRelatedByKodeWilayah');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'WtDstSyncLogRelatedByKodeWilayah');
        }

        return $this;
    }

    /**
     * Use the WtDstSyncLogRelatedByKodeWilayah relation WtDstSyncLog object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\WtDstSyncLogQuery A secondary query class using the current class as primary query
     */
    public function useWtDstSyncLogRelatedByKodeWilayahQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinWtDstSyncLogRelatedByKodeWilayah($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'WtDstSyncLogRelatedByKodeWilayah', '\angulex\Model\WtDstSyncLogQuery');
    }

    /**
     * Filter the query by a related WtDstSyncLog object
     *
     * @param   WtDstSyncLog|PropelObjectCollection $wtDstSyncLog  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 MstWilayahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByWtDstSyncLogRelatedByKodeWilayah($wtDstSyncLog, $comparison = null)
    {
        if ($wtDstSyncLog instanceof WtDstSyncLog) {
            return $this
                ->addUsingAlias(MstWilayahPeer::KODE_WILAYAH, $wtDstSyncLog->getKodeWilayah(), $comparison);
        } elseif ($wtDstSyncLog instanceof PropelObjectCollection) {
            return $this
                ->useWtDstSyncLogRelatedByKodeWilayahQuery()
                ->filterByPrimaryKeys($wtDstSyncLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByWtDstSyncLogRelatedByKodeWilayah() only accepts arguments of type WtDstSyncLog or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the WtDstSyncLogRelatedByKodeWilayah relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function joinWtDstSyncLogRelatedByKodeWilayah($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('WtDstSyncLogRelatedByKodeWilayah');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'WtDstSyncLogRelatedByKodeWilayah');
        }

        return $this;
    }

    /**
     * Use the WtDstSyncLogRelatedByKodeWilayah relation WtDstSyncLog object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\WtDstSyncLogQuery A secondary query class using the current class as primary query
     */
    public function useWtDstSyncLogRelatedByKodeWilayahQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinWtDstSyncLogRelatedByKodeWilayah($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'WtDstSyncLogRelatedByKodeWilayah', '\angulex\Model\WtDstSyncLogQuery');
    }

    /**
     * Filter the query by a related WsyncSession object
     *
     * @param   WsyncSession|PropelObjectCollection $wsyncSession  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 MstWilayahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByWsyncSessionRelatedByKodeWilayah($wsyncSession, $comparison = null)
    {
        if ($wsyncSession instanceof WsyncSession) {
            return $this
                ->addUsingAlias(MstWilayahPeer::KODE_WILAYAH, $wsyncSession->getKodeWilayah(), $comparison);
        } elseif ($wsyncSession instanceof PropelObjectCollection) {
            return $this
                ->useWsyncSessionRelatedByKodeWilayahQuery()
                ->filterByPrimaryKeys($wsyncSession->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByWsyncSessionRelatedByKodeWilayah() only accepts arguments of type WsyncSession or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the WsyncSessionRelatedByKodeWilayah relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function joinWsyncSessionRelatedByKodeWilayah($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('WsyncSessionRelatedByKodeWilayah');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'WsyncSessionRelatedByKodeWilayah');
        }

        return $this;
    }

    /**
     * Use the WsyncSessionRelatedByKodeWilayah relation WsyncSession object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\WsyncSessionQuery A secondary query class using the current class as primary query
     */
    public function useWsyncSessionRelatedByKodeWilayahQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinWsyncSessionRelatedByKodeWilayah($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'WsyncSessionRelatedByKodeWilayah', '\angulex\Model\WsyncSessionQuery');
    }

    /**
     * Filter the query by a related WsyncSession object
     *
     * @param   WsyncSession|PropelObjectCollection $wsyncSession  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 MstWilayahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByWsyncSessionRelatedByKodeWilayah($wsyncSession, $comparison = null)
    {
        if ($wsyncSession instanceof WsyncSession) {
            return $this
                ->addUsingAlias(MstWilayahPeer::KODE_WILAYAH, $wsyncSession->getKodeWilayah(), $comparison);
        } elseif ($wsyncSession instanceof PropelObjectCollection) {
            return $this
                ->useWsyncSessionRelatedByKodeWilayahQuery()
                ->filterByPrimaryKeys($wsyncSession->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByWsyncSessionRelatedByKodeWilayah() only accepts arguments of type WsyncSession or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the WsyncSessionRelatedByKodeWilayah relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function joinWsyncSessionRelatedByKodeWilayah($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('WsyncSessionRelatedByKodeWilayah');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'WsyncSessionRelatedByKodeWilayah');
        }

        return $this;
    }

    /**
     * Use the WsyncSessionRelatedByKodeWilayah relation WsyncSession object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\WsyncSessionQuery A secondary query class using the current class as primary query
     */
    public function useWsyncSessionRelatedByKodeWilayahQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinWsyncSessionRelatedByKodeWilayah($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'WsyncSessionRelatedByKodeWilayah', '\angulex\Model\WsyncSessionQuery');
    }

    /**
     * Filter the query by a related WsrcSyncLog object
     *
     * @param   WsrcSyncLog|PropelObjectCollection $wsrcSyncLog  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 MstWilayahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByWsrcSyncLogRelatedByKodeWilayah($wsrcSyncLog, $comparison = null)
    {
        if ($wsrcSyncLog instanceof WsrcSyncLog) {
            return $this
                ->addUsingAlias(MstWilayahPeer::KODE_WILAYAH, $wsrcSyncLog->getKodeWilayah(), $comparison);
        } elseif ($wsrcSyncLog instanceof PropelObjectCollection) {
            return $this
                ->useWsrcSyncLogRelatedByKodeWilayahQuery()
                ->filterByPrimaryKeys($wsrcSyncLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByWsrcSyncLogRelatedByKodeWilayah() only accepts arguments of type WsrcSyncLog or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the WsrcSyncLogRelatedByKodeWilayah relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function joinWsrcSyncLogRelatedByKodeWilayah($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('WsrcSyncLogRelatedByKodeWilayah');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'WsrcSyncLogRelatedByKodeWilayah');
        }

        return $this;
    }

    /**
     * Use the WsrcSyncLogRelatedByKodeWilayah relation WsrcSyncLog object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\WsrcSyncLogQuery A secondary query class using the current class as primary query
     */
    public function useWsrcSyncLogRelatedByKodeWilayahQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinWsrcSyncLogRelatedByKodeWilayah($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'WsrcSyncLogRelatedByKodeWilayah', '\angulex\Model\WsrcSyncLogQuery');
    }

    /**
     * Filter the query by a related WsrcSyncLog object
     *
     * @param   WsrcSyncLog|PropelObjectCollection $wsrcSyncLog  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 MstWilayahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByWsrcSyncLogRelatedByKodeWilayah($wsrcSyncLog, $comparison = null)
    {
        if ($wsrcSyncLog instanceof WsrcSyncLog) {
            return $this
                ->addUsingAlias(MstWilayahPeer::KODE_WILAYAH, $wsrcSyncLog->getKodeWilayah(), $comparison);
        } elseif ($wsrcSyncLog instanceof PropelObjectCollection) {
            return $this
                ->useWsrcSyncLogRelatedByKodeWilayahQuery()
                ->filterByPrimaryKeys($wsrcSyncLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByWsrcSyncLogRelatedByKodeWilayah() only accepts arguments of type WsrcSyncLog or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the WsrcSyncLogRelatedByKodeWilayah relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function joinWsrcSyncLogRelatedByKodeWilayah($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('WsrcSyncLogRelatedByKodeWilayah');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'WsrcSyncLogRelatedByKodeWilayah');
        }

        return $this;
    }

    /**
     * Use the WsrcSyncLogRelatedByKodeWilayah relation WsrcSyncLog object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\WsrcSyncLogQuery A secondary query class using the current class as primary query
     */
    public function useWsrcSyncLogRelatedByKodeWilayahQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinWsrcSyncLogRelatedByKodeWilayah($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'WsrcSyncLogRelatedByKodeWilayah', '\angulex\Model\WsrcSyncLogQuery');
    }

    /**
     * Filter the query by a related WdstSyncLog object
     *
     * @param   WdstSyncLog|PropelObjectCollection $wdstSyncLog  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 MstWilayahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByWdstSyncLogRelatedByKodeWilayah($wdstSyncLog, $comparison = null)
    {
        if ($wdstSyncLog instanceof WdstSyncLog) {
            return $this
                ->addUsingAlias(MstWilayahPeer::KODE_WILAYAH, $wdstSyncLog->getKodeWilayah(), $comparison);
        } elseif ($wdstSyncLog instanceof PropelObjectCollection) {
            return $this
                ->useWdstSyncLogRelatedByKodeWilayahQuery()
                ->filterByPrimaryKeys($wdstSyncLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByWdstSyncLogRelatedByKodeWilayah() only accepts arguments of type WdstSyncLog or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the WdstSyncLogRelatedByKodeWilayah relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function joinWdstSyncLogRelatedByKodeWilayah($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('WdstSyncLogRelatedByKodeWilayah');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'WdstSyncLogRelatedByKodeWilayah');
        }

        return $this;
    }

    /**
     * Use the WdstSyncLogRelatedByKodeWilayah relation WdstSyncLog object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\WdstSyncLogQuery A secondary query class using the current class as primary query
     */
    public function useWdstSyncLogRelatedByKodeWilayahQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinWdstSyncLogRelatedByKodeWilayah($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'WdstSyncLogRelatedByKodeWilayah', '\angulex\Model\WdstSyncLogQuery');
    }

    /**
     * Filter the query by a related WdstSyncLog object
     *
     * @param   WdstSyncLog|PropelObjectCollection $wdstSyncLog  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 MstWilayahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByWdstSyncLogRelatedByKodeWilayah($wdstSyncLog, $comparison = null)
    {
        if ($wdstSyncLog instanceof WdstSyncLog) {
            return $this
                ->addUsingAlias(MstWilayahPeer::KODE_WILAYAH, $wdstSyncLog->getKodeWilayah(), $comparison);
        } elseif ($wdstSyncLog instanceof PropelObjectCollection) {
            return $this
                ->useWdstSyncLogRelatedByKodeWilayahQuery()
                ->filterByPrimaryKeys($wdstSyncLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByWdstSyncLogRelatedByKodeWilayah() only accepts arguments of type WdstSyncLog or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the WdstSyncLogRelatedByKodeWilayah relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function joinWdstSyncLogRelatedByKodeWilayah($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('WdstSyncLogRelatedByKodeWilayah');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'WdstSyncLogRelatedByKodeWilayah');
        }

        return $this;
    }

    /**
     * Use the WdstSyncLogRelatedByKodeWilayah relation WdstSyncLog object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\WdstSyncLogQuery A secondary query class using the current class as primary query
     */
    public function useWdstSyncLogRelatedByKodeWilayahQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinWdstSyncLogRelatedByKodeWilayah($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'WdstSyncLogRelatedByKodeWilayah', '\angulex\Model\WdstSyncLogQuery');
    }

    /**
     * Filter the query by a related WPendingJob object
     *
     * @param   WPendingJob|PropelObjectCollection $wPendingJob  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 MstWilayahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByWPendingJobRelatedByKodeWilayah($wPendingJob, $comparison = null)
    {
        if ($wPendingJob instanceof WPendingJob) {
            return $this
                ->addUsingAlias(MstWilayahPeer::KODE_WILAYAH, $wPendingJob->getKodeWilayah(), $comparison);
        } elseif ($wPendingJob instanceof PropelObjectCollection) {
            return $this
                ->useWPendingJobRelatedByKodeWilayahQuery()
                ->filterByPrimaryKeys($wPendingJob->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByWPendingJobRelatedByKodeWilayah() only accepts arguments of type WPendingJob or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the WPendingJobRelatedByKodeWilayah relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function joinWPendingJobRelatedByKodeWilayah($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('WPendingJobRelatedByKodeWilayah');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'WPendingJobRelatedByKodeWilayah');
        }

        return $this;
    }

    /**
     * Use the WPendingJobRelatedByKodeWilayah relation WPendingJob object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\WPendingJobQuery A secondary query class using the current class as primary query
     */
    public function useWPendingJobRelatedByKodeWilayahQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinWPendingJobRelatedByKodeWilayah($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'WPendingJobRelatedByKodeWilayah', '\angulex\Model\WPendingJobQuery');
    }

    /**
     * Filter the query by a related WPendingJob object
     *
     * @param   WPendingJob|PropelObjectCollection $wPendingJob  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 MstWilayahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByWPendingJobRelatedByKodeWilayah($wPendingJob, $comparison = null)
    {
        if ($wPendingJob instanceof WPendingJob) {
            return $this
                ->addUsingAlias(MstWilayahPeer::KODE_WILAYAH, $wPendingJob->getKodeWilayah(), $comparison);
        } elseif ($wPendingJob instanceof PropelObjectCollection) {
            return $this
                ->useWPendingJobRelatedByKodeWilayahQuery()
                ->filterByPrimaryKeys($wPendingJob->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByWPendingJobRelatedByKodeWilayah() only accepts arguments of type WPendingJob or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the WPendingJobRelatedByKodeWilayah relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function joinWPendingJobRelatedByKodeWilayah($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('WPendingJobRelatedByKodeWilayah');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'WPendingJobRelatedByKodeWilayah');
        }

        return $this;
    }

    /**
     * Use the WPendingJobRelatedByKodeWilayah relation WPendingJob object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\WPendingJobQuery A secondary query class using the current class as primary query
     */
    public function useWPendingJobRelatedByKodeWilayahQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinWPendingJobRelatedByKodeWilayah($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'WPendingJobRelatedByKodeWilayah', '\angulex\Model\WPendingJobQuery');
    }

    /**
     * Filter the query by a related LembagaNonSekolah object
     *
     * @param   LembagaNonSekolah|PropelObjectCollection $lembagaNonSekolah  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 MstWilayahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByLembagaNonSekolahRelatedByKodeWilayah($lembagaNonSekolah, $comparison = null)
    {
        if ($lembagaNonSekolah instanceof LembagaNonSekolah) {
            return $this
                ->addUsingAlias(MstWilayahPeer::KODE_WILAYAH, $lembagaNonSekolah->getKodeWilayah(), $comparison);
        } elseif ($lembagaNonSekolah instanceof PropelObjectCollection) {
            return $this
                ->useLembagaNonSekolahRelatedByKodeWilayahQuery()
                ->filterByPrimaryKeys($lembagaNonSekolah->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByLembagaNonSekolahRelatedByKodeWilayah() only accepts arguments of type LembagaNonSekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the LembagaNonSekolahRelatedByKodeWilayah relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function joinLembagaNonSekolahRelatedByKodeWilayah($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('LembagaNonSekolahRelatedByKodeWilayah');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'LembagaNonSekolahRelatedByKodeWilayah');
        }

        return $this;
    }

    /**
     * Use the LembagaNonSekolahRelatedByKodeWilayah relation LembagaNonSekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\LembagaNonSekolahQuery A secondary query class using the current class as primary query
     */
    public function useLembagaNonSekolahRelatedByKodeWilayahQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinLembagaNonSekolahRelatedByKodeWilayah($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'LembagaNonSekolahRelatedByKodeWilayah', '\angulex\Model\LembagaNonSekolahQuery');
    }

    /**
     * Filter the query by a related LembagaNonSekolah object
     *
     * @param   LembagaNonSekolah|PropelObjectCollection $lembagaNonSekolah  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 MstWilayahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByLembagaNonSekolahRelatedByKodeWilayah($lembagaNonSekolah, $comparison = null)
    {
        if ($lembagaNonSekolah instanceof LembagaNonSekolah) {
            return $this
                ->addUsingAlias(MstWilayahPeer::KODE_WILAYAH, $lembagaNonSekolah->getKodeWilayah(), $comparison);
        } elseif ($lembagaNonSekolah instanceof PropelObjectCollection) {
            return $this
                ->useLembagaNonSekolahRelatedByKodeWilayahQuery()
                ->filterByPrimaryKeys($lembagaNonSekolah->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByLembagaNonSekolahRelatedByKodeWilayah() only accepts arguments of type LembagaNonSekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the LembagaNonSekolahRelatedByKodeWilayah relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function joinLembagaNonSekolahRelatedByKodeWilayah($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('LembagaNonSekolahRelatedByKodeWilayah');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'LembagaNonSekolahRelatedByKodeWilayah');
        }

        return $this;
    }

    /**
     * Use the LembagaNonSekolahRelatedByKodeWilayah relation LembagaNonSekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\LembagaNonSekolahQuery A secondary query class using the current class as primary query
     */
    public function useLembagaNonSekolahRelatedByKodeWilayahQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinLembagaNonSekolahRelatedByKodeWilayah($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'LembagaNonSekolahRelatedByKodeWilayah', '\angulex\Model\LembagaNonSekolahQuery');
    }

    /**
     * Filter the query by a related Demografi object
     *
     * @param   Demografi|PropelObjectCollection $demografi  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 MstWilayahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByDemografiRelatedByKodeWilayah($demografi, $comparison = null)
    {
        if ($demografi instanceof Demografi) {
            return $this
                ->addUsingAlias(MstWilayahPeer::KODE_WILAYAH, $demografi->getKodeWilayah(), $comparison);
        } elseif ($demografi instanceof PropelObjectCollection) {
            return $this
                ->useDemografiRelatedByKodeWilayahQuery()
                ->filterByPrimaryKeys($demografi->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByDemografiRelatedByKodeWilayah() only accepts arguments of type Demografi or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the DemografiRelatedByKodeWilayah relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function joinDemografiRelatedByKodeWilayah($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('DemografiRelatedByKodeWilayah');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'DemografiRelatedByKodeWilayah');
        }

        return $this;
    }

    /**
     * Use the DemografiRelatedByKodeWilayah relation Demografi object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\DemografiQuery A secondary query class using the current class as primary query
     */
    public function useDemografiRelatedByKodeWilayahQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinDemografiRelatedByKodeWilayah($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'DemografiRelatedByKodeWilayah', '\angulex\Model\DemografiQuery');
    }

    /**
     * Filter the query by a related Demografi object
     *
     * @param   Demografi|PropelObjectCollection $demografi  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 MstWilayahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByDemografiRelatedByKodeWilayah($demografi, $comparison = null)
    {
        if ($demografi instanceof Demografi) {
            return $this
                ->addUsingAlias(MstWilayahPeer::KODE_WILAYAH, $demografi->getKodeWilayah(), $comparison);
        } elseif ($demografi instanceof PropelObjectCollection) {
            return $this
                ->useDemografiRelatedByKodeWilayahQuery()
                ->filterByPrimaryKeys($demografi->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByDemografiRelatedByKodeWilayah() only accepts arguments of type Demografi or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the DemografiRelatedByKodeWilayah relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function joinDemografiRelatedByKodeWilayah($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('DemografiRelatedByKodeWilayah');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'DemografiRelatedByKodeWilayah');
        }

        return $this;
    }

    /**
     * Use the DemografiRelatedByKodeWilayah relation Demografi object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\DemografiQuery A secondary query class using the current class as primary query
     */
    public function useDemografiRelatedByKodeWilayahQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinDemografiRelatedByKodeWilayah($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'DemografiRelatedByKodeWilayah', '\angulex\Model\DemografiQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   MstWilayah $mstWilayah Object to remove from the list of results
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function prune($mstWilayah = null)
    {
        if ($mstWilayah) {
            $this->addUsingAlias(MstWilayahPeer::KODE_WILAYAH, $mstWilayah->getKodeWilayah(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
