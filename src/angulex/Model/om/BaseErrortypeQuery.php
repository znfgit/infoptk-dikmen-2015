<?php

namespace angulex\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\Errortype;
use angulex\Model\ErrortypePeer;
use angulex\Model\ErrortypeQuery;
use angulex\Model\VldAnak;
use angulex\Model\VldBeaPd;
use angulex\Model\VldBeaPtk;
use angulex\Model\VldBukuPtk;
use angulex\Model\VldDemografi;
use angulex\Model\VldInpassing;
use angulex\Model\VldJurusanSp;
use angulex\Model\VldKaryaTulis;
use angulex\Model\VldKesejahteraan;
use angulex\Model\VldMou;
use angulex\Model\VldNilaiRapor;
use angulex\Model\VldNilaiTest;
use angulex\Model\VldNonsekolah;
use angulex\Model\VldPdLong;
use angulex\Model\VldPembelajaran;
use angulex\Model\VldPenghargaan;
use angulex\Model\VldPesertaDidik;
use angulex\Model\VldPrasarana;
use angulex\Model\VldPrestasi;
use angulex\Model\VldPtk;
use angulex\Model\VldRombel;
use angulex\Model\VldRwyFungsional;
use angulex\Model\VldRwyKepangkatan;
use angulex\Model\VldRwyPendFormal;
use angulex\Model\VldRwySertifikasi;
use angulex\Model\VldRwyStruktural;
use angulex\Model\VldSarana;
use angulex\Model\VldSekolah;
use angulex\Model\VldTugasTambahan;
use angulex\Model\VldTunjangan;
use angulex\Model\VldUn;
use angulex\Model\VldYayasan;

/**
 * Base class that represents a query for the 'ref.errortype' table.
 *
 * 
 *
 * @method ErrortypeQuery orderByIdtype($order = Criteria::ASC) Order by the idtype column
 * @method ErrortypeQuery orderByKategoriError($order = Criteria::ASC) Order by the kategori_error column
 * @method ErrortypeQuery orderByKeterangan($order = Criteria::ASC) Order by the keterangan column
 * @method ErrortypeQuery orderByCreateDate($order = Criteria::ASC) Order by the create_date column
 * @method ErrortypeQuery orderByLastUpdate($order = Criteria::ASC) Order by the last_update column
 * @method ErrortypeQuery orderByExpiredDate($order = Criteria::ASC) Order by the expired_date column
 * @method ErrortypeQuery orderByLastSync($order = Criteria::ASC) Order by the last_sync column
 *
 * @method ErrortypeQuery groupByIdtype() Group by the idtype column
 * @method ErrortypeQuery groupByKategoriError() Group by the kategori_error column
 * @method ErrortypeQuery groupByKeterangan() Group by the keterangan column
 * @method ErrortypeQuery groupByCreateDate() Group by the create_date column
 * @method ErrortypeQuery groupByLastUpdate() Group by the last_update column
 * @method ErrortypeQuery groupByExpiredDate() Group by the expired_date column
 * @method ErrortypeQuery groupByLastSync() Group by the last_sync column
 *
 * @method ErrortypeQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method ErrortypeQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method ErrortypeQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method ErrortypeQuery leftJoinVldRwyKepangkatanRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldRwyKepangkatanRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldRwyKepangkatanRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldRwyKepangkatanRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldRwyKepangkatanRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldRwyKepangkatanRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldRwyKepangkatanRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldRwyKepangkatanRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldRwyKepangkatanRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldRwyKepangkatanRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldRwyKepangkatanRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldRwyKepangkatanRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldRwyFungsionalRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldRwyFungsionalRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldRwyFungsionalRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldRwyFungsionalRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldRwyFungsionalRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldRwyFungsionalRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldRwyFungsionalRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldRwyFungsionalRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldRwyFungsionalRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldRwyFungsionalRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldRwyFungsionalRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldRwyFungsionalRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldRombelRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldRombelRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldRombelRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldRombelRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldRombelRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldRombelRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldRombelRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldRombelRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldRombelRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldRombelRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldRombelRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldRombelRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldPtkRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldPtkRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldPtkRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldPtkRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldPtkRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldPtkRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldPtkRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldPtkRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldPtkRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldPtkRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldPtkRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldPtkRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldPrestasiRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldPrestasiRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldPrestasiRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldPrestasiRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldPrestasiRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldPrestasiRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldPrestasiRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldPrestasiRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldPrestasiRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldPrestasiRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldPrestasiRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldPrestasiRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldPrasaranaRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldPrasaranaRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldPrasaranaRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldPrasaranaRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldPrasaranaRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldPrasaranaRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldPrasaranaRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldPrasaranaRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldPrasaranaRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldPrasaranaRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldPrasaranaRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldPrasaranaRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldPesertaDidikRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldPesertaDidikRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldPesertaDidikRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldPesertaDidikRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldPesertaDidikRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldPesertaDidikRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldPesertaDidikRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldPesertaDidikRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldPesertaDidikRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldPesertaDidikRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldPesertaDidikRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldPesertaDidikRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldPenghargaanRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldPenghargaanRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldPenghargaanRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldPenghargaanRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldPenghargaanRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldPenghargaanRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldPenghargaanRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldPenghargaanRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldPenghargaanRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldPenghargaanRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldPenghargaanRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldPenghargaanRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldPembelajaranRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldPembelajaranRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldPembelajaranRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldPembelajaranRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldPembelajaranRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldPembelajaranRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldPembelajaranRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldPembelajaranRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldPembelajaranRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldPembelajaranRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldPembelajaranRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldPembelajaranRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldPdLongRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldPdLongRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldPdLongRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldPdLongRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldPdLongRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldPdLongRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldPdLongRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldPdLongRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldPdLongRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldPdLongRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldPdLongRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldPdLongRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldNonsekolahRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldNonsekolahRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldNonsekolahRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldNonsekolahRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldNonsekolahRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldNonsekolahRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldNonsekolahRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldNonsekolahRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldNonsekolahRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldNonsekolahRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldNonsekolahRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldNonsekolahRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldNilaiTestRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldNilaiTestRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldNilaiTestRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldNilaiTestRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldNilaiTestRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldNilaiTestRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldNilaiTestRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldNilaiTestRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldNilaiTestRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldNilaiTestRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldNilaiTestRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldNilaiTestRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldNilaiRaporRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldNilaiRaporRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldNilaiRaporRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldNilaiRaporRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldNilaiRaporRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldNilaiRaporRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldNilaiRaporRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldNilaiRaporRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldNilaiRaporRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldNilaiRaporRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldNilaiRaporRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldNilaiRaporRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldMouRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldMouRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldMouRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldMouRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldMouRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldMouRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldMouRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldMouRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldMouRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldMouRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldMouRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldMouRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldKesejahteraanRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldKesejahteraanRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldKesejahteraanRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldKesejahteraanRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldKesejahteraanRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldKesejahteraanRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldKesejahteraanRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldKesejahteraanRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldKesejahteraanRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldKesejahteraanRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldKesejahteraanRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldKesejahteraanRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldKaryaTulisRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldKaryaTulisRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldKaryaTulisRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldKaryaTulisRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldKaryaTulisRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldKaryaTulisRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldKaryaTulisRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldKaryaTulisRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldKaryaTulisRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldKaryaTulisRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldKaryaTulisRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldKaryaTulisRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldJurusanSpRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldJurusanSpRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldJurusanSpRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldJurusanSpRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldJurusanSpRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldJurusanSpRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldJurusanSpRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldJurusanSpRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldJurusanSpRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldJurusanSpRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldJurusanSpRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldJurusanSpRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldInpassingRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldInpassingRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldInpassingRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldInpassingRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldInpassingRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldInpassingRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldInpassingRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldInpassingRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldInpassingRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldInpassingRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldInpassingRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldInpassingRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldDemografiRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldDemografiRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldDemografiRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldDemografiRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldDemografiRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldDemografiRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldDemografiRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldDemografiRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldDemografiRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldDemografiRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldDemografiRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldDemografiRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldBukuPtkRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldBukuPtkRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldBukuPtkRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldBukuPtkRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldBukuPtkRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldBukuPtkRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldBukuPtkRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldBukuPtkRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldBukuPtkRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldBukuPtkRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldBukuPtkRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldBukuPtkRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldBeaPtkRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldBeaPtkRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldBeaPtkRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldBeaPtkRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldBeaPtkRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldBeaPtkRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldBeaPtkRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldBeaPtkRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldBeaPtkRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldBeaPtkRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldBeaPtkRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldBeaPtkRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldBeaPdRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldBeaPdRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldBeaPdRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldBeaPdRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldBeaPdRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldBeaPdRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldBeaPdRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldBeaPdRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldBeaPdRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldBeaPdRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldBeaPdRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldBeaPdRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldAnakRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldAnakRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldAnakRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldAnakRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldAnakRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldAnakRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldAnakRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldAnakRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldAnakRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldAnakRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldAnakRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldAnakRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldYayasanRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldYayasanRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldYayasanRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldYayasanRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldYayasanRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldYayasanRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldYayasanRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldYayasanRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldYayasanRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldYayasanRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldYayasanRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldYayasanRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldUnRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldUnRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldUnRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldUnRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldUnRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldUnRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldUnRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldUnRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldUnRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldUnRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldUnRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldUnRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldTunjanganRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldTunjanganRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldTunjanganRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldTunjanganRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldTunjanganRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldTunjanganRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldTunjanganRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldTunjanganRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldTunjanganRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldTunjanganRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldTunjanganRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldTunjanganRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldTugasTambahanRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldTugasTambahanRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldTugasTambahanRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldTugasTambahanRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldTugasTambahanRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldTugasTambahanRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldTugasTambahanRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldTugasTambahanRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldTugasTambahanRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldTugasTambahanRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldTugasTambahanRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldTugasTambahanRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldSekolahRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldSekolahRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldSekolahRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldSekolahRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldSekolahRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldSekolahRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldSekolahRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldSekolahRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldSekolahRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldSekolahRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldSekolahRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldSekolahRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldSaranaRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldSaranaRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldSaranaRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldSaranaRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldSaranaRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldSaranaRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldSaranaRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldSaranaRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldSaranaRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldSaranaRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldSaranaRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldSaranaRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldRwyStrukturalRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldRwyStrukturalRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldRwyStrukturalRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldRwyStrukturalRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldRwyStrukturalRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldRwyStrukturalRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldRwyStrukturalRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldRwyStrukturalRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldRwyStrukturalRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldRwyStrukturalRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldRwyStrukturalRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldRwyStrukturalRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldRwySertifikasiRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldRwySertifikasiRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldRwySertifikasiRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldRwySertifikasiRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldRwySertifikasiRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldRwySertifikasiRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldRwySertifikasiRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldRwySertifikasiRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldRwySertifikasiRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldRwySertifikasiRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldRwySertifikasiRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldRwySertifikasiRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldRwyPendFormalRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldRwyPendFormalRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldRwyPendFormalRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldRwyPendFormalRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldRwyPendFormalRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldRwyPendFormalRelatedByIdtype relation
 *
 * @method ErrortypeQuery leftJoinVldRwyPendFormalRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldRwyPendFormalRelatedByIdtype relation
 * @method ErrortypeQuery rightJoinVldRwyPendFormalRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldRwyPendFormalRelatedByIdtype relation
 * @method ErrortypeQuery innerJoinVldRwyPendFormalRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the VldRwyPendFormalRelatedByIdtype relation
 *
 * @method Errortype findOne(PropelPDO $con = null) Return the first Errortype matching the query
 * @method Errortype findOneOrCreate(PropelPDO $con = null) Return the first Errortype matching the query, or a new Errortype object populated from the query conditions when no match is found
 *
 * @method Errortype findOneByKategoriError(int $kategori_error) Return the first Errortype filtered by the kategori_error column
 * @method Errortype findOneByKeterangan(string $keterangan) Return the first Errortype filtered by the keterangan column
 * @method Errortype findOneByCreateDate(string $create_date) Return the first Errortype filtered by the create_date column
 * @method Errortype findOneByLastUpdate(string $last_update) Return the first Errortype filtered by the last_update column
 * @method Errortype findOneByExpiredDate(string $expired_date) Return the first Errortype filtered by the expired_date column
 * @method Errortype findOneByLastSync(string $last_sync) Return the first Errortype filtered by the last_sync column
 *
 * @method array findByIdtype(int $idtype) Return Errortype objects filtered by the idtype column
 * @method array findByKategoriError(int $kategori_error) Return Errortype objects filtered by the kategori_error column
 * @method array findByKeterangan(string $keterangan) Return Errortype objects filtered by the keterangan column
 * @method array findByCreateDate(string $create_date) Return Errortype objects filtered by the create_date column
 * @method array findByLastUpdate(string $last_update) Return Errortype objects filtered by the last_update column
 * @method array findByExpiredDate(string $expired_date) Return Errortype objects filtered by the expired_date column
 * @method array findByLastSync(string $last_sync) Return Errortype objects filtered by the last_sync column
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseErrortypeQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseErrortypeQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Dapodikmen', $modelName = 'angulex\\Model\\Errortype', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ErrortypeQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   ErrortypeQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ErrortypeQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ErrortypeQuery) {
            return $criteria;
        }
        $query = new ErrortypeQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Errortype|Errortype[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ErrortypePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ErrortypePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Errortype A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByIdtype($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Errortype A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [idtype], [kategori_error], [keterangan], [create_date], [last_update], [expired_date], [last_sync] FROM [ref].[errortype] WHERE [idtype] = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Errortype();
            $obj->hydrate($row);
            ErrortypePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Errortype|Errortype[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Errortype[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ErrortypePeer::IDTYPE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ErrortypePeer::IDTYPE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idtype column
     *
     * Example usage:
     * <code>
     * $query->filterByIdtype(1234); // WHERE idtype = 1234
     * $query->filterByIdtype(array(12, 34)); // WHERE idtype IN (12, 34)
     * $query->filterByIdtype(array('min' => 12)); // WHERE idtype >= 12
     * $query->filterByIdtype(array('max' => 12)); // WHERE idtype <= 12
     * </code>
     *
     * @param     mixed $idtype The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function filterByIdtype($idtype = null, $comparison = null)
    {
        if (is_array($idtype)) {
            $useMinMax = false;
            if (isset($idtype['min'])) {
                $this->addUsingAlias(ErrortypePeer::IDTYPE, $idtype['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idtype['max'])) {
                $this->addUsingAlias(ErrortypePeer::IDTYPE, $idtype['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ErrortypePeer::IDTYPE, $idtype, $comparison);
    }

    /**
     * Filter the query on the kategori_error column
     *
     * Example usage:
     * <code>
     * $query->filterByKategoriError(1234); // WHERE kategori_error = 1234
     * $query->filterByKategoriError(array(12, 34)); // WHERE kategori_error IN (12, 34)
     * $query->filterByKategoriError(array('min' => 12)); // WHERE kategori_error >= 12
     * $query->filterByKategoriError(array('max' => 12)); // WHERE kategori_error <= 12
     * </code>
     *
     * @param     mixed $kategoriError The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function filterByKategoriError($kategoriError = null, $comparison = null)
    {
        if (is_array($kategoriError)) {
            $useMinMax = false;
            if (isset($kategoriError['min'])) {
                $this->addUsingAlias(ErrortypePeer::KATEGORI_ERROR, $kategoriError['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($kategoriError['max'])) {
                $this->addUsingAlias(ErrortypePeer::KATEGORI_ERROR, $kategoriError['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ErrortypePeer::KATEGORI_ERROR, $kategoriError, $comparison);
    }

    /**
     * Filter the query on the keterangan column
     *
     * Example usage:
     * <code>
     * $query->filterByKeterangan('fooValue');   // WHERE keterangan = 'fooValue'
     * $query->filterByKeterangan('%fooValue%'); // WHERE keterangan LIKE '%fooValue%'
     * </code>
     *
     * @param     string $keterangan The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function filterByKeterangan($keterangan = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($keterangan)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $keterangan)) {
                $keterangan = str_replace('*', '%', $keterangan);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ErrortypePeer::KETERANGAN, $keterangan, $comparison);
    }

    /**
     * Filter the query on the create_date column
     *
     * Example usage:
     * <code>
     * $query->filterByCreateDate('2011-03-14'); // WHERE create_date = '2011-03-14'
     * $query->filterByCreateDate('now'); // WHERE create_date = '2011-03-14'
     * $query->filterByCreateDate(array('max' => 'yesterday')); // WHERE create_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $createDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function filterByCreateDate($createDate = null, $comparison = null)
    {
        if (is_array($createDate)) {
            $useMinMax = false;
            if (isset($createDate['min'])) {
                $this->addUsingAlias(ErrortypePeer::CREATE_DATE, $createDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createDate['max'])) {
                $this->addUsingAlias(ErrortypePeer::CREATE_DATE, $createDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ErrortypePeer::CREATE_DATE, $createDate, $comparison);
    }

    /**
     * Filter the query on the last_update column
     *
     * Example usage:
     * <code>
     * $query->filterByLastUpdate('2011-03-14'); // WHERE last_update = '2011-03-14'
     * $query->filterByLastUpdate('now'); // WHERE last_update = '2011-03-14'
     * $query->filterByLastUpdate(array('max' => 'yesterday')); // WHERE last_update > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastUpdate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function filterByLastUpdate($lastUpdate = null, $comparison = null)
    {
        if (is_array($lastUpdate)) {
            $useMinMax = false;
            if (isset($lastUpdate['min'])) {
                $this->addUsingAlias(ErrortypePeer::LAST_UPDATE, $lastUpdate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastUpdate['max'])) {
                $this->addUsingAlias(ErrortypePeer::LAST_UPDATE, $lastUpdate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ErrortypePeer::LAST_UPDATE, $lastUpdate, $comparison);
    }

    /**
     * Filter the query on the expired_date column
     *
     * Example usage:
     * <code>
     * $query->filterByExpiredDate('2011-03-14'); // WHERE expired_date = '2011-03-14'
     * $query->filterByExpiredDate('now'); // WHERE expired_date = '2011-03-14'
     * $query->filterByExpiredDate(array('max' => 'yesterday')); // WHERE expired_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $expiredDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function filterByExpiredDate($expiredDate = null, $comparison = null)
    {
        if (is_array($expiredDate)) {
            $useMinMax = false;
            if (isset($expiredDate['min'])) {
                $this->addUsingAlias(ErrortypePeer::EXPIRED_DATE, $expiredDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($expiredDate['max'])) {
                $this->addUsingAlias(ErrortypePeer::EXPIRED_DATE, $expiredDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ErrortypePeer::EXPIRED_DATE, $expiredDate, $comparison);
    }

    /**
     * Filter the query on the last_sync column
     *
     * Example usage:
     * <code>
     * $query->filterByLastSync('2011-03-14'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync('now'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync(array('max' => 'yesterday')); // WHERE last_sync > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastSync The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function filterByLastSync($lastSync = null, $comparison = null)
    {
        if (is_array($lastSync)) {
            $useMinMax = false;
            if (isset($lastSync['min'])) {
                $this->addUsingAlias(ErrortypePeer::LAST_SYNC, $lastSync['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastSync['max'])) {
                $this->addUsingAlias(ErrortypePeer::LAST_SYNC, $lastSync['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ErrortypePeer::LAST_SYNC, $lastSync, $comparison);
    }

    /**
     * Filter the query by a related VldRwyKepangkatan object
     *
     * @param   VldRwyKepangkatan|PropelObjectCollection $vldRwyKepangkatan  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldRwyKepangkatanRelatedByIdtype($vldRwyKepangkatan, $comparison = null)
    {
        if ($vldRwyKepangkatan instanceof VldRwyKepangkatan) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldRwyKepangkatan->getIdtype(), $comparison);
        } elseif ($vldRwyKepangkatan instanceof PropelObjectCollection) {
            return $this
                ->useVldRwyKepangkatanRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldRwyKepangkatan->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldRwyKepangkatanRelatedByIdtype() only accepts arguments of type VldRwyKepangkatan or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldRwyKepangkatanRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldRwyKepangkatanRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldRwyKepangkatanRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldRwyKepangkatanRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldRwyKepangkatanRelatedByIdtype relation VldRwyKepangkatan object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldRwyKepangkatanQuery A secondary query class using the current class as primary query
     */
    public function useVldRwyKepangkatanRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldRwyKepangkatanRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldRwyKepangkatanRelatedByIdtype', '\angulex\Model\VldRwyKepangkatanQuery');
    }

    /**
     * Filter the query by a related VldRwyKepangkatan object
     *
     * @param   VldRwyKepangkatan|PropelObjectCollection $vldRwyKepangkatan  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldRwyKepangkatanRelatedByIdtype($vldRwyKepangkatan, $comparison = null)
    {
        if ($vldRwyKepangkatan instanceof VldRwyKepangkatan) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldRwyKepangkatan->getIdtype(), $comparison);
        } elseif ($vldRwyKepangkatan instanceof PropelObjectCollection) {
            return $this
                ->useVldRwyKepangkatanRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldRwyKepangkatan->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldRwyKepangkatanRelatedByIdtype() only accepts arguments of type VldRwyKepangkatan or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldRwyKepangkatanRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldRwyKepangkatanRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldRwyKepangkatanRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldRwyKepangkatanRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldRwyKepangkatanRelatedByIdtype relation VldRwyKepangkatan object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldRwyKepangkatanQuery A secondary query class using the current class as primary query
     */
    public function useVldRwyKepangkatanRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldRwyKepangkatanRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldRwyKepangkatanRelatedByIdtype', '\angulex\Model\VldRwyKepangkatanQuery');
    }

    /**
     * Filter the query by a related VldRwyFungsional object
     *
     * @param   VldRwyFungsional|PropelObjectCollection $vldRwyFungsional  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldRwyFungsionalRelatedByIdtype($vldRwyFungsional, $comparison = null)
    {
        if ($vldRwyFungsional instanceof VldRwyFungsional) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldRwyFungsional->getIdtype(), $comparison);
        } elseif ($vldRwyFungsional instanceof PropelObjectCollection) {
            return $this
                ->useVldRwyFungsionalRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldRwyFungsional->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldRwyFungsionalRelatedByIdtype() only accepts arguments of type VldRwyFungsional or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldRwyFungsionalRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldRwyFungsionalRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldRwyFungsionalRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldRwyFungsionalRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldRwyFungsionalRelatedByIdtype relation VldRwyFungsional object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldRwyFungsionalQuery A secondary query class using the current class as primary query
     */
    public function useVldRwyFungsionalRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldRwyFungsionalRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldRwyFungsionalRelatedByIdtype', '\angulex\Model\VldRwyFungsionalQuery');
    }

    /**
     * Filter the query by a related VldRwyFungsional object
     *
     * @param   VldRwyFungsional|PropelObjectCollection $vldRwyFungsional  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldRwyFungsionalRelatedByIdtype($vldRwyFungsional, $comparison = null)
    {
        if ($vldRwyFungsional instanceof VldRwyFungsional) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldRwyFungsional->getIdtype(), $comparison);
        } elseif ($vldRwyFungsional instanceof PropelObjectCollection) {
            return $this
                ->useVldRwyFungsionalRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldRwyFungsional->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldRwyFungsionalRelatedByIdtype() only accepts arguments of type VldRwyFungsional or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldRwyFungsionalRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldRwyFungsionalRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldRwyFungsionalRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldRwyFungsionalRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldRwyFungsionalRelatedByIdtype relation VldRwyFungsional object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldRwyFungsionalQuery A secondary query class using the current class as primary query
     */
    public function useVldRwyFungsionalRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldRwyFungsionalRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldRwyFungsionalRelatedByIdtype', '\angulex\Model\VldRwyFungsionalQuery');
    }

    /**
     * Filter the query by a related VldRombel object
     *
     * @param   VldRombel|PropelObjectCollection $vldRombel  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldRombelRelatedByIdtype($vldRombel, $comparison = null)
    {
        if ($vldRombel instanceof VldRombel) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldRombel->getIdtype(), $comparison);
        } elseif ($vldRombel instanceof PropelObjectCollection) {
            return $this
                ->useVldRombelRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldRombel->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldRombelRelatedByIdtype() only accepts arguments of type VldRombel or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldRombelRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldRombelRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldRombelRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldRombelRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldRombelRelatedByIdtype relation VldRombel object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldRombelQuery A secondary query class using the current class as primary query
     */
    public function useVldRombelRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldRombelRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldRombelRelatedByIdtype', '\angulex\Model\VldRombelQuery');
    }

    /**
     * Filter the query by a related VldRombel object
     *
     * @param   VldRombel|PropelObjectCollection $vldRombel  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldRombelRelatedByIdtype($vldRombel, $comparison = null)
    {
        if ($vldRombel instanceof VldRombel) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldRombel->getIdtype(), $comparison);
        } elseif ($vldRombel instanceof PropelObjectCollection) {
            return $this
                ->useVldRombelRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldRombel->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldRombelRelatedByIdtype() only accepts arguments of type VldRombel or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldRombelRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldRombelRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldRombelRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldRombelRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldRombelRelatedByIdtype relation VldRombel object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldRombelQuery A secondary query class using the current class as primary query
     */
    public function useVldRombelRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldRombelRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldRombelRelatedByIdtype', '\angulex\Model\VldRombelQuery');
    }

    /**
     * Filter the query by a related VldPtk object
     *
     * @param   VldPtk|PropelObjectCollection $vldPtk  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldPtkRelatedByIdtype($vldPtk, $comparison = null)
    {
        if ($vldPtk instanceof VldPtk) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldPtk->getIdtype(), $comparison);
        } elseif ($vldPtk instanceof PropelObjectCollection) {
            return $this
                ->useVldPtkRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldPtk->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldPtkRelatedByIdtype() only accepts arguments of type VldPtk or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldPtkRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldPtkRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldPtkRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldPtkRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldPtkRelatedByIdtype relation VldPtk object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldPtkQuery A secondary query class using the current class as primary query
     */
    public function useVldPtkRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldPtkRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldPtkRelatedByIdtype', '\angulex\Model\VldPtkQuery');
    }

    /**
     * Filter the query by a related VldPtk object
     *
     * @param   VldPtk|PropelObjectCollection $vldPtk  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldPtkRelatedByIdtype($vldPtk, $comparison = null)
    {
        if ($vldPtk instanceof VldPtk) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldPtk->getIdtype(), $comparison);
        } elseif ($vldPtk instanceof PropelObjectCollection) {
            return $this
                ->useVldPtkRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldPtk->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldPtkRelatedByIdtype() only accepts arguments of type VldPtk or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldPtkRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldPtkRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldPtkRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldPtkRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldPtkRelatedByIdtype relation VldPtk object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldPtkQuery A secondary query class using the current class as primary query
     */
    public function useVldPtkRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldPtkRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldPtkRelatedByIdtype', '\angulex\Model\VldPtkQuery');
    }

    /**
     * Filter the query by a related VldPrestasi object
     *
     * @param   VldPrestasi|PropelObjectCollection $vldPrestasi  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldPrestasiRelatedByIdtype($vldPrestasi, $comparison = null)
    {
        if ($vldPrestasi instanceof VldPrestasi) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldPrestasi->getIdtype(), $comparison);
        } elseif ($vldPrestasi instanceof PropelObjectCollection) {
            return $this
                ->useVldPrestasiRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldPrestasi->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldPrestasiRelatedByIdtype() only accepts arguments of type VldPrestasi or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldPrestasiRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldPrestasiRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldPrestasiRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldPrestasiRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldPrestasiRelatedByIdtype relation VldPrestasi object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldPrestasiQuery A secondary query class using the current class as primary query
     */
    public function useVldPrestasiRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldPrestasiRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldPrestasiRelatedByIdtype', '\angulex\Model\VldPrestasiQuery');
    }

    /**
     * Filter the query by a related VldPrestasi object
     *
     * @param   VldPrestasi|PropelObjectCollection $vldPrestasi  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldPrestasiRelatedByIdtype($vldPrestasi, $comparison = null)
    {
        if ($vldPrestasi instanceof VldPrestasi) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldPrestasi->getIdtype(), $comparison);
        } elseif ($vldPrestasi instanceof PropelObjectCollection) {
            return $this
                ->useVldPrestasiRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldPrestasi->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldPrestasiRelatedByIdtype() only accepts arguments of type VldPrestasi or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldPrestasiRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldPrestasiRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldPrestasiRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldPrestasiRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldPrestasiRelatedByIdtype relation VldPrestasi object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldPrestasiQuery A secondary query class using the current class as primary query
     */
    public function useVldPrestasiRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldPrestasiRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldPrestasiRelatedByIdtype', '\angulex\Model\VldPrestasiQuery');
    }

    /**
     * Filter the query by a related VldPrasarana object
     *
     * @param   VldPrasarana|PropelObjectCollection $vldPrasarana  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldPrasaranaRelatedByIdtype($vldPrasarana, $comparison = null)
    {
        if ($vldPrasarana instanceof VldPrasarana) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldPrasarana->getIdtype(), $comparison);
        } elseif ($vldPrasarana instanceof PropelObjectCollection) {
            return $this
                ->useVldPrasaranaRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldPrasarana->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldPrasaranaRelatedByIdtype() only accepts arguments of type VldPrasarana or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldPrasaranaRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldPrasaranaRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldPrasaranaRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldPrasaranaRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldPrasaranaRelatedByIdtype relation VldPrasarana object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldPrasaranaQuery A secondary query class using the current class as primary query
     */
    public function useVldPrasaranaRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldPrasaranaRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldPrasaranaRelatedByIdtype', '\angulex\Model\VldPrasaranaQuery');
    }

    /**
     * Filter the query by a related VldPrasarana object
     *
     * @param   VldPrasarana|PropelObjectCollection $vldPrasarana  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldPrasaranaRelatedByIdtype($vldPrasarana, $comparison = null)
    {
        if ($vldPrasarana instanceof VldPrasarana) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldPrasarana->getIdtype(), $comparison);
        } elseif ($vldPrasarana instanceof PropelObjectCollection) {
            return $this
                ->useVldPrasaranaRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldPrasarana->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldPrasaranaRelatedByIdtype() only accepts arguments of type VldPrasarana or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldPrasaranaRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldPrasaranaRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldPrasaranaRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldPrasaranaRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldPrasaranaRelatedByIdtype relation VldPrasarana object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldPrasaranaQuery A secondary query class using the current class as primary query
     */
    public function useVldPrasaranaRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldPrasaranaRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldPrasaranaRelatedByIdtype', '\angulex\Model\VldPrasaranaQuery');
    }

    /**
     * Filter the query by a related VldPesertaDidik object
     *
     * @param   VldPesertaDidik|PropelObjectCollection $vldPesertaDidik  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldPesertaDidikRelatedByIdtype($vldPesertaDidik, $comparison = null)
    {
        if ($vldPesertaDidik instanceof VldPesertaDidik) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldPesertaDidik->getIdtype(), $comparison);
        } elseif ($vldPesertaDidik instanceof PropelObjectCollection) {
            return $this
                ->useVldPesertaDidikRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldPesertaDidik->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldPesertaDidikRelatedByIdtype() only accepts arguments of type VldPesertaDidik or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldPesertaDidikRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldPesertaDidikRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldPesertaDidikRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldPesertaDidikRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldPesertaDidikRelatedByIdtype relation VldPesertaDidik object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldPesertaDidikQuery A secondary query class using the current class as primary query
     */
    public function useVldPesertaDidikRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldPesertaDidikRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldPesertaDidikRelatedByIdtype', '\angulex\Model\VldPesertaDidikQuery');
    }

    /**
     * Filter the query by a related VldPesertaDidik object
     *
     * @param   VldPesertaDidik|PropelObjectCollection $vldPesertaDidik  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldPesertaDidikRelatedByIdtype($vldPesertaDidik, $comparison = null)
    {
        if ($vldPesertaDidik instanceof VldPesertaDidik) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldPesertaDidik->getIdtype(), $comparison);
        } elseif ($vldPesertaDidik instanceof PropelObjectCollection) {
            return $this
                ->useVldPesertaDidikRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldPesertaDidik->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldPesertaDidikRelatedByIdtype() only accepts arguments of type VldPesertaDidik or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldPesertaDidikRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldPesertaDidikRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldPesertaDidikRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldPesertaDidikRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldPesertaDidikRelatedByIdtype relation VldPesertaDidik object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldPesertaDidikQuery A secondary query class using the current class as primary query
     */
    public function useVldPesertaDidikRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldPesertaDidikRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldPesertaDidikRelatedByIdtype', '\angulex\Model\VldPesertaDidikQuery');
    }

    /**
     * Filter the query by a related VldPenghargaan object
     *
     * @param   VldPenghargaan|PropelObjectCollection $vldPenghargaan  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldPenghargaanRelatedByIdtype($vldPenghargaan, $comparison = null)
    {
        if ($vldPenghargaan instanceof VldPenghargaan) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldPenghargaan->getIdtype(), $comparison);
        } elseif ($vldPenghargaan instanceof PropelObjectCollection) {
            return $this
                ->useVldPenghargaanRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldPenghargaan->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldPenghargaanRelatedByIdtype() only accepts arguments of type VldPenghargaan or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldPenghargaanRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldPenghargaanRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldPenghargaanRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldPenghargaanRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldPenghargaanRelatedByIdtype relation VldPenghargaan object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldPenghargaanQuery A secondary query class using the current class as primary query
     */
    public function useVldPenghargaanRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldPenghargaanRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldPenghargaanRelatedByIdtype', '\angulex\Model\VldPenghargaanQuery');
    }

    /**
     * Filter the query by a related VldPenghargaan object
     *
     * @param   VldPenghargaan|PropelObjectCollection $vldPenghargaan  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldPenghargaanRelatedByIdtype($vldPenghargaan, $comparison = null)
    {
        if ($vldPenghargaan instanceof VldPenghargaan) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldPenghargaan->getIdtype(), $comparison);
        } elseif ($vldPenghargaan instanceof PropelObjectCollection) {
            return $this
                ->useVldPenghargaanRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldPenghargaan->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldPenghargaanRelatedByIdtype() only accepts arguments of type VldPenghargaan or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldPenghargaanRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldPenghargaanRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldPenghargaanRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldPenghargaanRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldPenghargaanRelatedByIdtype relation VldPenghargaan object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldPenghargaanQuery A secondary query class using the current class as primary query
     */
    public function useVldPenghargaanRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldPenghargaanRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldPenghargaanRelatedByIdtype', '\angulex\Model\VldPenghargaanQuery');
    }

    /**
     * Filter the query by a related VldPembelajaran object
     *
     * @param   VldPembelajaran|PropelObjectCollection $vldPembelajaran  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldPembelajaranRelatedByIdtype($vldPembelajaran, $comparison = null)
    {
        if ($vldPembelajaran instanceof VldPembelajaran) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldPembelajaran->getIdtype(), $comparison);
        } elseif ($vldPembelajaran instanceof PropelObjectCollection) {
            return $this
                ->useVldPembelajaranRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldPembelajaran->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldPembelajaranRelatedByIdtype() only accepts arguments of type VldPembelajaran or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldPembelajaranRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldPembelajaranRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldPembelajaranRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldPembelajaranRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldPembelajaranRelatedByIdtype relation VldPembelajaran object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldPembelajaranQuery A secondary query class using the current class as primary query
     */
    public function useVldPembelajaranRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldPembelajaranRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldPembelajaranRelatedByIdtype', '\angulex\Model\VldPembelajaranQuery');
    }

    /**
     * Filter the query by a related VldPembelajaran object
     *
     * @param   VldPembelajaran|PropelObjectCollection $vldPembelajaran  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldPembelajaranRelatedByIdtype($vldPembelajaran, $comparison = null)
    {
        if ($vldPembelajaran instanceof VldPembelajaran) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldPembelajaran->getIdtype(), $comparison);
        } elseif ($vldPembelajaran instanceof PropelObjectCollection) {
            return $this
                ->useVldPembelajaranRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldPembelajaran->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldPembelajaranRelatedByIdtype() only accepts arguments of type VldPembelajaran or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldPembelajaranRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldPembelajaranRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldPembelajaranRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldPembelajaranRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldPembelajaranRelatedByIdtype relation VldPembelajaran object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldPembelajaranQuery A secondary query class using the current class as primary query
     */
    public function useVldPembelajaranRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldPembelajaranRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldPembelajaranRelatedByIdtype', '\angulex\Model\VldPembelajaranQuery');
    }

    /**
     * Filter the query by a related VldPdLong object
     *
     * @param   VldPdLong|PropelObjectCollection $vldPdLong  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldPdLongRelatedByIdtype($vldPdLong, $comparison = null)
    {
        if ($vldPdLong instanceof VldPdLong) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldPdLong->getIdtype(), $comparison);
        } elseif ($vldPdLong instanceof PropelObjectCollection) {
            return $this
                ->useVldPdLongRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldPdLong->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldPdLongRelatedByIdtype() only accepts arguments of type VldPdLong or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldPdLongRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldPdLongRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldPdLongRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldPdLongRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldPdLongRelatedByIdtype relation VldPdLong object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldPdLongQuery A secondary query class using the current class as primary query
     */
    public function useVldPdLongRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldPdLongRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldPdLongRelatedByIdtype', '\angulex\Model\VldPdLongQuery');
    }

    /**
     * Filter the query by a related VldPdLong object
     *
     * @param   VldPdLong|PropelObjectCollection $vldPdLong  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldPdLongRelatedByIdtype($vldPdLong, $comparison = null)
    {
        if ($vldPdLong instanceof VldPdLong) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldPdLong->getIdtype(), $comparison);
        } elseif ($vldPdLong instanceof PropelObjectCollection) {
            return $this
                ->useVldPdLongRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldPdLong->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldPdLongRelatedByIdtype() only accepts arguments of type VldPdLong or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldPdLongRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldPdLongRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldPdLongRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldPdLongRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldPdLongRelatedByIdtype relation VldPdLong object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldPdLongQuery A secondary query class using the current class as primary query
     */
    public function useVldPdLongRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldPdLongRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldPdLongRelatedByIdtype', '\angulex\Model\VldPdLongQuery');
    }

    /**
     * Filter the query by a related VldNonsekolah object
     *
     * @param   VldNonsekolah|PropelObjectCollection $vldNonsekolah  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldNonsekolahRelatedByIdtype($vldNonsekolah, $comparison = null)
    {
        if ($vldNonsekolah instanceof VldNonsekolah) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldNonsekolah->getIdtype(), $comparison);
        } elseif ($vldNonsekolah instanceof PropelObjectCollection) {
            return $this
                ->useVldNonsekolahRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldNonsekolah->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldNonsekolahRelatedByIdtype() only accepts arguments of type VldNonsekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldNonsekolahRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldNonsekolahRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldNonsekolahRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldNonsekolahRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldNonsekolahRelatedByIdtype relation VldNonsekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldNonsekolahQuery A secondary query class using the current class as primary query
     */
    public function useVldNonsekolahRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldNonsekolahRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldNonsekolahRelatedByIdtype', '\angulex\Model\VldNonsekolahQuery');
    }

    /**
     * Filter the query by a related VldNonsekolah object
     *
     * @param   VldNonsekolah|PropelObjectCollection $vldNonsekolah  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldNonsekolahRelatedByIdtype($vldNonsekolah, $comparison = null)
    {
        if ($vldNonsekolah instanceof VldNonsekolah) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldNonsekolah->getIdtype(), $comparison);
        } elseif ($vldNonsekolah instanceof PropelObjectCollection) {
            return $this
                ->useVldNonsekolahRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldNonsekolah->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldNonsekolahRelatedByIdtype() only accepts arguments of type VldNonsekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldNonsekolahRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldNonsekolahRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldNonsekolahRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldNonsekolahRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldNonsekolahRelatedByIdtype relation VldNonsekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldNonsekolahQuery A secondary query class using the current class as primary query
     */
    public function useVldNonsekolahRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldNonsekolahRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldNonsekolahRelatedByIdtype', '\angulex\Model\VldNonsekolahQuery');
    }

    /**
     * Filter the query by a related VldNilaiTest object
     *
     * @param   VldNilaiTest|PropelObjectCollection $vldNilaiTest  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldNilaiTestRelatedByIdtype($vldNilaiTest, $comparison = null)
    {
        if ($vldNilaiTest instanceof VldNilaiTest) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldNilaiTest->getIdtype(), $comparison);
        } elseif ($vldNilaiTest instanceof PropelObjectCollection) {
            return $this
                ->useVldNilaiTestRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldNilaiTest->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldNilaiTestRelatedByIdtype() only accepts arguments of type VldNilaiTest or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldNilaiTestRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldNilaiTestRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldNilaiTestRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldNilaiTestRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldNilaiTestRelatedByIdtype relation VldNilaiTest object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldNilaiTestQuery A secondary query class using the current class as primary query
     */
    public function useVldNilaiTestRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldNilaiTestRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldNilaiTestRelatedByIdtype', '\angulex\Model\VldNilaiTestQuery');
    }

    /**
     * Filter the query by a related VldNilaiTest object
     *
     * @param   VldNilaiTest|PropelObjectCollection $vldNilaiTest  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldNilaiTestRelatedByIdtype($vldNilaiTest, $comparison = null)
    {
        if ($vldNilaiTest instanceof VldNilaiTest) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldNilaiTest->getIdtype(), $comparison);
        } elseif ($vldNilaiTest instanceof PropelObjectCollection) {
            return $this
                ->useVldNilaiTestRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldNilaiTest->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldNilaiTestRelatedByIdtype() only accepts arguments of type VldNilaiTest or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldNilaiTestRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldNilaiTestRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldNilaiTestRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldNilaiTestRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldNilaiTestRelatedByIdtype relation VldNilaiTest object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldNilaiTestQuery A secondary query class using the current class as primary query
     */
    public function useVldNilaiTestRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldNilaiTestRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldNilaiTestRelatedByIdtype', '\angulex\Model\VldNilaiTestQuery');
    }

    /**
     * Filter the query by a related VldNilaiRapor object
     *
     * @param   VldNilaiRapor|PropelObjectCollection $vldNilaiRapor  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldNilaiRaporRelatedByIdtype($vldNilaiRapor, $comparison = null)
    {
        if ($vldNilaiRapor instanceof VldNilaiRapor) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldNilaiRapor->getIdtype(), $comparison);
        } elseif ($vldNilaiRapor instanceof PropelObjectCollection) {
            return $this
                ->useVldNilaiRaporRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldNilaiRapor->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldNilaiRaporRelatedByIdtype() only accepts arguments of type VldNilaiRapor or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldNilaiRaporRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldNilaiRaporRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldNilaiRaporRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldNilaiRaporRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldNilaiRaporRelatedByIdtype relation VldNilaiRapor object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldNilaiRaporQuery A secondary query class using the current class as primary query
     */
    public function useVldNilaiRaporRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldNilaiRaporRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldNilaiRaporRelatedByIdtype', '\angulex\Model\VldNilaiRaporQuery');
    }

    /**
     * Filter the query by a related VldNilaiRapor object
     *
     * @param   VldNilaiRapor|PropelObjectCollection $vldNilaiRapor  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldNilaiRaporRelatedByIdtype($vldNilaiRapor, $comparison = null)
    {
        if ($vldNilaiRapor instanceof VldNilaiRapor) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldNilaiRapor->getIdtype(), $comparison);
        } elseif ($vldNilaiRapor instanceof PropelObjectCollection) {
            return $this
                ->useVldNilaiRaporRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldNilaiRapor->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldNilaiRaporRelatedByIdtype() only accepts arguments of type VldNilaiRapor or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldNilaiRaporRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldNilaiRaporRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldNilaiRaporRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldNilaiRaporRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldNilaiRaporRelatedByIdtype relation VldNilaiRapor object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldNilaiRaporQuery A secondary query class using the current class as primary query
     */
    public function useVldNilaiRaporRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldNilaiRaporRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldNilaiRaporRelatedByIdtype', '\angulex\Model\VldNilaiRaporQuery');
    }

    /**
     * Filter the query by a related VldMou object
     *
     * @param   VldMou|PropelObjectCollection $vldMou  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldMouRelatedByIdtype($vldMou, $comparison = null)
    {
        if ($vldMou instanceof VldMou) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldMou->getIdtype(), $comparison);
        } elseif ($vldMou instanceof PropelObjectCollection) {
            return $this
                ->useVldMouRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldMou->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldMouRelatedByIdtype() only accepts arguments of type VldMou or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldMouRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldMouRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldMouRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldMouRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldMouRelatedByIdtype relation VldMou object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldMouQuery A secondary query class using the current class as primary query
     */
    public function useVldMouRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldMouRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldMouRelatedByIdtype', '\angulex\Model\VldMouQuery');
    }

    /**
     * Filter the query by a related VldMou object
     *
     * @param   VldMou|PropelObjectCollection $vldMou  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldMouRelatedByIdtype($vldMou, $comparison = null)
    {
        if ($vldMou instanceof VldMou) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldMou->getIdtype(), $comparison);
        } elseif ($vldMou instanceof PropelObjectCollection) {
            return $this
                ->useVldMouRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldMou->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldMouRelatedByIdtype() only accepts arguments of type VldMou or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldMouRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldMouRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldMouRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldMouRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldMouRelatedByIdtype relation VldMou object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldMouQuery A secondary query class using the current class as primary query
     */
    public function useVldMouRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldMouRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldMouRelatedByIdtype', '\angulex\Model\VldMouQuery');
    }

    /**
     * Filter the query by a related VldKesejahteraan object
     *
     * @param   VldKesejahteraan|PropelObjectCollection $vldKesejahteraan  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldKesejahteraanRelatedByIdtype($vldKesejahteraan, $comparison = null)
    {
        if ($vldKesejahteraan instanceof VldKesejahteraan) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldKesejahteraan->getIdtype(), $comparison);
        } elseif ($vldKesejahteraan instanceof PropelObjectCollection) {
            return $this
                ->useVldKesejahteraanRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldKesejahteraan->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldKesejahteraanRelatedByIdtype() only accepts arguments of type VldKesejahteraan or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldKesejahteraanRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldKesejahteraanRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldKesejahteraanRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldKesejahteraanRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldKesejahteraanRelatedByIdtype relation VldKesejahteraan object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldKesejahteraanQuery A secondary query class using the current class as primary query
     */
    public function useVldKesejahteraanRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldKesejahteraanRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldKesejahteraanRelatedByIdtype', '\angulex\Model\VldKesejahteraanQuery');
    }

    /**
     * Filter the query by a related VldKesejahteraan object
     *
     * @param   VldKesejahteraan|PropelObjectCollection $vldKesejahteraan  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldKesejahteraanRelatedByIdtype($vldKesejahteraan, $comparison = null)
    {
        if ($vldKesejahteraan instanceof VldKesejahteraan) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldKesejahteraan->getIdtype(), $comparison);
        } elseif ($vldKesejahteraan instanceof PropelObjectCollection) {
            return $this
                ->useVldKesejahteraanRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldKesejahteraan->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldKesejahteraanRelatedByIdtype() only accepts arguments of type VldKesejahteraan or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldKesejahteraanRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldKesejahteraanRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldKesejahteraanRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldKesejahteraanRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldKesejahteraanRelatedByIdtype relation VldKesejahteraan object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldKesejahteraanQuery A secondary query class using the current class as primary query
     */
    public function useVldKesejahteraanRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldKesejahteraanRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldKesejahteraanRelatedByIdtype', '\angulex\Model\VldKesejahteraanQuery');
    }

    /**
     * Filter the query by a related VldKaryaTulis object
     *
     * @param   VldKaryaTulis|PropelObjectCollection $vldKaryaTulis  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldKaryaTulisRelatedByIdtype($vldKaryaTulis, $comparison = null)
    {
        if ($vldKaryaTulis instanceof VldKaryaTulis) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldKaryaTulis->getIdtype(), $comparison);
        } elseif ($vldKaryaTulis instanceof PropelObjectCollection) {
            return $this
                ->useVldKaryaTulisRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldKaryaTulis->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldKaryaTulisRelatedByIdtype() only accepts arguments of type VldKaryaTulis or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldKaryaTulisRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldKaryaTulisRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldKaryaTulisRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldKaryaTulisRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldKaryaTulisRelatedByIdtype relation VldKaryaTulis object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldKaryaTulisQuery A secondary query class using the current class as primary query
     */
    public function useVldKaryaTulisRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldKaryaTulisRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldKaryaTulisRelatedByIdtype', '\angulex\Model\VldKaryaTulisQuery');
    }

    /**
     * Filter the query by a related VldKaryaTulis object
     *
     * @param   VldKaryaTulis|PropelObjectCollection $vldKaryaTulis  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldKaryaTulisRelatedByIdtype($vldKaryaTulis, $comparison = null)
    {
        if ($vldKaryaTulis instanceof VldKaryaTulis) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldKaryaTulis->getIdtype(), $comparison);
        } elseif ($vldKaryaTulis instanceof PropelObjectCollection) {
            return $this
                ->useVldKaryaTulisRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldKaryaTulis->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldKaryaTulisRelatedByIdtype() only accepts arguments of type VldKaryaTulis or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldKaryaTulisRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldKaryaTulisRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldKaryaTulisRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldKaryaTulisRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldKaryaTulisRelatedByIdtype relation VldKaryaTulis object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldKaryaTulisQuery A secondary query class using the current class as primary query
     */
    public function useVldKaryaTulisRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldKaryaTulisRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldKaryaTulisRelatedByIdtype', '\angulex\Model\VldKaryaTulisQuery');
    }

    /**
     * Filter the query by a related VldJurusanSp object
     *
     * @param   VldJurusanSp|PropelObjectCollection $vldJurusanSp  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldJurusanSpRelatedByIdtype($vldJurusanSp, $comparison = null)
    {
        if ($vldJurusanSp instanceof VldJurusanSp) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldJurusanSp->getIdtype(), $comparison);
        } elseif ($vldJurusanSp instanceof PropelObjectCollection) {
            return $this
                ->useVldJurusanSpRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldJurusanSp->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldJurusanSpRelatedByIdtype() only accepts arguments of type VldJurusanSp or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldJurusanSpRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldJurusanSpRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldJurusanSpRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldJurusanSpRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldJurusanSpRelatedByIdtype relation VldJurusanSp object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldJurusanSpQuery A secondary query class using the current class as primary query
     */
    public function useVldJurusanSpRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldJurusanSpRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldJurusanSpRelatedByIdtype', '\angulex\Model\VldJurusanSpQuery');
    }

    /**
     * Filter the query by a related VldJurusanSp object
     *
     * @param   VldJurusanSp|PropelObjectCollection $vldJurusanSp  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldJurusanSpRelatedByIdtype($vldJurusanSp, $comparison = null)
    {
        if ($vldJurusanSp instanceof VldJurusanSp) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldJurusanSp->getIdtype(), $comparison);
        } elseif ($vldJurusanSp instanceof PropelObjectCollection) {
            return $this
                ->useVldJurusanSpRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldJurusanSp->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldJurusanSpRelatedByIdtype() only accepts arguments of type VldJurusanSp or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldJurusanSpRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldJurusanSpRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldJurusanSpRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldJurusanSpRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldJurusanSpRelatedByIdtype relation VldJurusanSp object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldJurusanSpQuery A secondary query class using the current class as primary query
     */
    public function useVldJurusanSpRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldJurusanSpRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldJurusanSpRelatedByIdtype', '\angulex\Model\VldJurusanSpQuery');
    }

    /**
     * Filter the query by a related VldInpassing object
     *
     * @param   VldInpassing|PropelObjectCollection $vldInpassing  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldInpassingRelatedByIdtype($vldInpassing, $comparison = null)
    {
        if ($vldInpassing instanceof VldInpassing) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldInpassing->getIdtype(), $comparison);
        } elseif ($vldInpassing instanceof PropelObjectCollection) {
            return $this
                ->useVldInpassingRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldInpassing->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldInpassingRelatedByIdtype() only accepts arguments of type VldInpassing or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldInpassingRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldInpassingRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldInpassingRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldInpassingRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldInpassingRelatedByIdtype relation VldInpassing object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldInpassingQuery A secondary query class using the current class as primary query
     */
    public function useVldInpassingRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldInpassingRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldInpassingRelatedByIdtype', '\angulex\Model\VldInpassingQuery');
    }

    /**
     * Filter the query by a related VldInpassing object
     *
     * @param   VldInpassing|PropelObjectCollection $vldInpassing  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldInpassingRelatedByIdtype($vldInpassing, $comparison = null)
    {
        if ($vldInpassing instanceof VldInpassing) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldInpassing->getIdtype(), $comparison);
        } elseif ($vldInpassing instanceof PropelObjectCollection) {
            return $this
                ->useVldInpassingRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldInpassing->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldInpassingRelatedByIdtype() only accepts arguments of type VldInpassing or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldInpassingRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldInpassingRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldInpassingRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldInpassingRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldInpassingRelatedByIdtype relation VldInpassing object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldInpassingQuery A secondary query class using the current class as primary query
     */
    public function useVldInpassingRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldInpassingRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldInpassingRelatedByIdtype', '\angulex\Model\VldInpassingQuery');
    }

    /**
     * Filter the query by a related VldDemografi object
     *
     * @param   VldDemografi|PropelObjectCollection $vldDemografi  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldDemografiRelatedByIdtype($vldDemografi, $comparison = null)
    {
        if ($vldDemografi instanceof VldDemografi) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldDemografi->getIdtype(), $comparison);
        } elseif ($vldDemografi instanceof PropelObjectCollection) {
            return $this
                ->useVldDemografiRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldDemografi->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldDemografiRelatedByIdtype() only accepts arguments of type VldDemografi or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldDemografiRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldDemografiRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldDemografiRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldDemografiRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldDemografiRelatedByIdtype relation VldDemografi object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldDemografiQuery A secondary query class using the current class as primary query
     */
    public function useVldDemografiRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldDemografiRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldDemografiRelatedByIdtype', '\angulex\Model\VldDemografiQuery');
    }

    /**
     * Filter the query by a related VldDemografi object
     *
     * @param   VldDemografi|PropelObjectCollection $vldDemografi  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldDemografiRelatedByIdtype($vldDemografi, $comparison = null)
    {
        if ($vldDemografi instanceof VldDemografi) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldDemografi->getIdtype(), $comparison);
        } elseif ($vldDemografi instanceof PropelObjectCollection) {
            return $this
                ->useVldDemografiRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldDemografi->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldDemografiRelatedByIdtype() only accepts arguments of type VldDemografi or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldDemografiRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldDemografiRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldDemografiRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldDemografiRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldDemografiRelatedByIdtype relation VldDemografi object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldDemografiQuery A secondary query class using the current class as primary query
     */
    public function useVldDemografiRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldDemografiRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldDemografiRelatedByIdtype', '\angulex\Model\VldDemografiQuery');
    }

    /**
     * Filter the query by a related VldBukuPtk object
     *
     * @param   VldBukuPtk|PropelObjectCollection $vldBukuPtk  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldBukuPtkRelatedByIdtype($vldBukuPtk, $comparison = null)
    {
        if ($vldBukuPtk instanceof VldBukuPtk) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldBukuPtk->getIdtype(), $comparison);
        } elseif ($vldBukuPtk instanceof PropelObjectCollection) {
            return $this
                ->useVldBukuPtkRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldBukuPtk->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldBukuPtkRelatedByIdtype() only accepts arguments of type VldBukuPtk or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldBukuPtkRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldBukuPtkRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldBukuPtkRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldBukuPtkRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldBukuPtkRelatedByIdtype relation VldBukuPtk object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldBukuPtkQuery A secondary query class using the current class as primary query
     */
    public function useVldBukuPtkRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldBukuPtkRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldBukuPtkRelatedByIdtype', '\angulex\Model\VldBukuPtkQuery');
    }

    /**
     * Filter the query by a related VldBukuPtk object
     *
     * @param   VldBukuPtk|PropelObjectCollection $vldBukuPtk  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldBukuPtkRelatedByIdtype($vldBukuPtk, $comparison = null)
    {
        if ($vldBukuPtk instanceof VldBukuPtk) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldBukuPtk->getIdtype(), $comparison);
        } elseif ($vldBukuPtk instanceof PropelObjectCollection) {
            return $this
                ->useVldBukuPtkRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldBukuPtk->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldBukuPtkRelatedByIdtype() only accepts arguments of type VldBukuPtk or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldBukuPtkRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldBukuPtkRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldBukuPtkRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldBukuPtkRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldBukuPtkRelatedByIdtype relation VldBukuPtk object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldBukuPtkQuery A secondary query class using the current class as primary query
     */
    public function useVldBukuPtkRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldBukuPtkRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldBukuPtkRelatedByIdtype', '\angulex\Model\VldBukuPtkQuery');
    }

    /**
     * Filter the query by a related VldBeaPtk object
     *
     * @param   VldBeaPtk|PropelObjectCollection $vldBeaPtk  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldBeaPtkRelatedByIdtype($vldBeaPtk, $comparison = null)
    {
        if ($vldBeaPtk instanceof VldBeaPtk) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldBeaPtk->getIdtype(), $comparison);
        } elseif ($vldBeaPtk instanceof PropelObjectCollection) {
            return $this
                ->useVldBeaPtkRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldBeaPtk->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldBeaPtkRelatedByIdtype() only accepts arguments of type VldBeaPtk or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldBeaPtkRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldBeaPtkRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldBeaPtkRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldBeaPtkRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldBeaPtkRelatedByIdtype relation VldBeaPtk object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldBeaPtkQuery A secondary query class using the current class as primary query
     */
    public function useVldBeaPtkRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldBeaPtkRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldBeaPtkRelatedByIdtype', '\angulex\Model\VldBeaPtkQuery');
    }

    /**
     * Filter the query by a related VldBeaPtk object
     *
     * @param   VldBeaPtk|PropelObjectCollection $vldBeaPtk  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldBeaPtkRelatedByIdtype($vldBeaPtk, $comparison = null)
    {
        if ($vldBeaPtk instanceof VldBeaPtk) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldBeaPtk->getIdtype(), $comparison);
        } elseif ($vldBeaPtk instanceof PropelObjectCollection) {
            return $this
                ->useVldBeaPtkRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldBeaPtk->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldBeaPtkRelatedByIdtype() only accepts arguments of type VldBeaPtk or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldBeaPtkRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldBeaPtkRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldBeaPtkRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldBeaPtkRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldBeaPtkRelatedByIdtype relation VldBeaPtk object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldBeaPtkQuery A secondary query class using the current class as primary query
     */
    public function useVldBeaPtkRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldBeaPtkRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldBeaPtkRelatedByIdtype', '\angulex\Model\VldBeaPtkQuery');
    }

    /**
     * Filter the query by a related VldBeaPd object
     *
     * @param   VldBeaPd|PropelObjectCollection $vldBeaPd  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldBeaPdRelatedByIdtype($vldBeaPd, $comparison = null)
    {
        if ($vldBeaPd instanceof VldBeaPd) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldBeaPd->getIdtype(), $comparison);
        } elseif ($vldBeaPd instanceof PropelObjectCollection) {
            return $this
                ->useVldBeaPdRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldBeaPd->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldBeaPdRelatedByIdtype() only accepts arguments of type VldBeaPd or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldBeaPdRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldBeaPdRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldBeaPdRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldBeaPdRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldBeaPdRelatedByIdtype relation VldBeaPd object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldBeaPdQuery A secondary query class using the current class as primary query
     */
    public function useVldBeaPdRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldBeaPdRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldBeaPdRelatedByIdtype', '\angulex\Model\VldBeaPdQuery');
    }

    /**
     * Filter the query by a related VldBeaPd object
     *
     * @param   VldBeaPd|PropelObjectCollection $vldBeaPd  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldBeaPdRelatedByIdtype($vldBeaPd, $comparison = null)
    {
        if ($vldBeaPd instanceof VldBeaPd) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldBeaPd->getIdtype(), $comparison);
        } elseif ($vldBeaPd instanceof PropelObjectCollection) {
            return $this
                ->useVldBeaPdRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldBeaPd->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldBeaPdRelatedByIdtype() only accepts arguments of type VldBeaPd or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldBeaPdRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldBeaPdRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldBeaPdRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldBeaPdRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldBeaPdRelatedByIdtype relation VldBeaPd object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldBeaPdQuery A secondary query class using the current class as primary query
     */
    public function useVldBeaPdRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldBeaPdRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldBeaPdRelatedByIdtype', '\angulex\Model\VldBeaPdQuery');
    }

    /**
     * Filter the query by a related VldAnak object
     *
     * @param   VldAnak|PropelObjectCollection $vldAnak  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldAnakRelatedByIdtype($vldAnak, $comparison = null)
    {
        if ($vldAnak instanceof VldAnak) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldAnak->getIdtype(), $comparison);
        } elseif ($vldAnak instanceof PropelObjectCollection) {
            return $this
                ->useVldAnakRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldAnak->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldAnakRelatedByIdtype() only accepts arguments of type VldAnak or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldAnakRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldAnakRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldAnakRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldAnakRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldAnakRelatedByIdtype relation VldAnak object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldAnakQuery A secondary query class using the current class as primary query
     */
    public function useVldAnakRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldAnakRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldAnakRelatedByIdtype', '\angulex\Model\VldAnakQuery');
    }

    /**
     * Filter the query by a related VldAnak object
     *
     * @param   VldAnak|PropelObjectCollection $vldAnak  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldAnakRelatedByIdtype($vldAnak, $comparison = null)
    {
        if ($vldAnak instanceof VldAnak) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldAnak->getIdtype(), $comparison);
        } elseif ($vldAnak instanceof PropelObjectCollection) {
            return $this
                ->useVldAnakRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldAnak->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldAnakRelatedByIdtype() only accepts arguments of type VldAnak or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldAnakRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldAnakRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldAnakRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldAnakRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldAnakRelatedByIdtype relation VldAnak object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldAnakQuery A secondary query class using the current class as primary query
     */
    public function useVldAnakRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldAnakRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldAnakRelatedByIdtype', '\angulex\Model\VldAnakQuery');
    }

    /**
     * Filter the query by a related VldYayasan object
     *
     * @param   VldYayasan|PropelObjectCollection $vldYayasan  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldYayasanRelatedByIdtype($vldYayasan, $comparison = null)
    {
        if ($vldYayasan instanceof VldYayasan) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldYayasan->getIdtype(), $comparison);
        } elseif ($vldYayasan instanceof PropelObjectCollection) {
            return $this
                ->useVldYayasanRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldYayasan->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldYayasanRelatedByIdtype() only accepts arguments of type VldYayasan or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldYayasanRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldYayasanRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldYayasanRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldYayasanRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldYayasanRelatedByIdtype relation VldYayasan object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldYayasanQuery A secondary query class using the current class as primary query
     */
    public function useVldYayasanRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldYayasanRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldYayasanRelatedByIdtype', '\angulex\Model\VldYayasanQuery');
    }

    /**
     * Filter the query by a related VldYayasan object
     *
     * @param   VldYayasan|PropelObjectCollection $vldYayasan  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldYayasanRelatedByIdtype($vldYayasan, $comparison = null)
    {
        if ($vldYayasan instanceof VldYayasan) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldYayasan->getIdtype(), $comparison);
        } elseif ($vldYayasan instanceof PropelObjectCollection) {
            return $this
                ->useVldYayasanRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldYayasan->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldYayasanRelatedByIdtype() only accepts arguments of type VldYayasan or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldYayasanRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldYayasanRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldYayasanRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldYayasanRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldYayasanRelatedByIdtype relation VldYayasan object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldYayasanQuery A secondary query class using the current class as primary query
     */
    public function useVldYayasanRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldYayasanRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldYayasanRelatedByIdtype', '\angulex\Model\VldYayasanQuery');
    }

    /**
     * Filter the query by a related VldUn object
     *
     * @param   VldUn|PropelObjectCollection $vldUn  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldUnRelatedByIdtype($vldUn, $comparison = null)
    {
        if ($vldUn instanceof VldUn) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldUn->getIdtype(), $comparison);
        } elseif ($vldUn instanceof PropelObjectCollection) {
            return $this
                ->useVldUnRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldUn->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldUnRelatedByIdtype() only accepts arguments of type VldUn or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldUnRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldUnRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldUnRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldUnRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldUnRelatedByIdtype relation VldUn object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldUnQuery A secondary query class using the current class as primary query
     */
    public function useVldUnRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldUnRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldUnRelatedByIdtype', '\angulex\Model\VldUnQuery');
    }

    /**
     * Filter the query by a related VldUn object
     *
     * @param   VldUn|PropelObjectCollection $vldUn  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldUnRelatedByIdtype($vldUn, $comparison = null)
    {
        if ($vldUn instanceof VldUn) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldUn->getIdtype(), $comparison);
        } elseif ($vldUn instanceof PropelObjectCollection) {
            return $this
                ->useVldUnRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldUn->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldUnRelatedByIdtype() only accepts arguments of type VldUn or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldUnRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldUnRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldUnRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldUnRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldUnRelatedByIdtype relation VldUn object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldUnQuery A secondary query class using the current class as primary query
     */
    public function useVldUnRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldUnRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldUnRelatedByIdtype', '\angulex\Model\VldUnQuery');
    }

    /**
     * Filter the query by a related VldTunjangan object
     *
     * @param   VldTunjangan|PropelObjectCollection $vldTunjangan  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldTunjanganRelatedByIdtype($vldTunjangan, $comparison = null)
    {
        if ($vldTunjangan instanceof VldTunjangan) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldTunjangan->getIdtype(), $comparison);
        } elseif ($vldTunjangan instanceof PropelObjectCollection) {
            return $this
                ->useVldTunjanganRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldTunjangan->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldTunjanganRelatedByIdtype() only accepts arguments of type VldTunjangan or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldTunjanganRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldTunjanganRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldTunjanganRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldTunjanganRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldTunjanganRelatedByIdtype relation VldTunjangan object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldTunjanganQuery A secondary query class using the current class as primary query
     */
    public function useVldTunjanganRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldTunjanganRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldTunjanganRelatedByIdtype', '\angulex\Model\VldTunjanganQuery');
    }

    /**
     * Filter the query by a related VldTunjangan object
     *
     * @param   VldTunjangan|PropelObjectCollection $vldTunjangan  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldTunjanganRelatedByIdtype($vldTunjangan, $comparison = null)
    {
        if ($vldTunjangan instanceof VldTunjangan) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldTunjangan->getIdtype(), $comparison);
        } elseif ($vldTunjangan instanceof PropelObjectCollection) {
            return $this
                ->useVldTunjanganRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldTunjangan->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldTunjanganRelatedByIdtype() only accepts arguments of type VldTunjangan or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldTunjanganRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldTunjanganRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldTunjanganRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldTunjanganRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldTunjanganRelatedByIdtype relation VldTunjangan object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldTunjanganQuery A secondary query class using the current class as primary query
     */
    public function useVldTunjanganRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldTunjanganRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldTunjanganRelatedByIdtype', '\angulex\Model\VldTunjanganQuery');
    }

    /**
     * Filter the query by a related VldTugasTambahan object
     *
     * @param   VldTugasTambahan|PropelObjectCollection $vldTugasTambahan  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldTugasTambahanRelatedByIdtype($vldTugasTambahan, $comparison = null)
    {
        if ($vldTugasTambahan instanceof VldTugasTambahan) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldTugasTambahan->getIdtype(), $comparison);
        } elseif ($vldTugasTambahan instanceof PropelObjectCollection) {
            return $this
                ->useVldTugasTambahanRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldTugasTambahan->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldTugasTambahanRelatedByIdtype() only accepts arguments of type VldTugasTambahan or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldTugasTambahanRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldTugasTambahanRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldTugasTambahanRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldTugasTambahanRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldTugasTambahanRelatedByIdtype relation VldTugasTambahan object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldTugasTambahanQuery A secondary query class using the current class as primary query
     */
    public function useVldTugasTambahanRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldTugasTambahanRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldTugasTambahanRelatedByIdtype', '\angulex\Model\VldTugasTambahanQuery');
    }

    /**
     * Filter the query by a related VldTugasTambahan object
     *
     * @param   VldTugasTambahan|PropelObjectCollection $vldTugasTambahan  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldTugasTambahanRelatedByIdtype($vldTugasTambahan, $comparison = null)
    {
        if ($vldTugasTambahan instanceof VldTugasTambahan) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldTugasTambahan->getIdtype(), $comparison);
        } elseif ($vldTugasTambahan instanceof PropelObjectCollection) {
            return $this
                ->useVldTugasTambahanRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldTugasTambahan->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldTugasTambahanRelatedByIdtype() only accepts arguments of type VldTugasTambahan or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldTugasTambahanRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldTugasTambahanRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldTugasTambahanRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldTugasTambahanRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldTugasTambahanRelatedByIdtype relation VldTugasTambahan object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldTugasTambahanQuery A secondary query class using the current class as primary query
     */
    public function useVldTugasTambahanRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldTugasTambahanRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldTugasTambahanRelatedByIdtype', '\angulex\Model\VldTugasTambahanQuery');
    }

    /**
     * Filter the query by a related VldSekolah object
     *
     * @param   VldSekolah|PropelObjectCollection $vldSekolah  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldSekolahRelatedByIdtype($vldSekolah, $comparison = null)
    {
        if ($vldSekolah instanceof VldSekolah) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldSekolah->getIdtype(), $comparison);
        } elseif ($vldSekolah instanceof PropelObjectCollection) {
            return $this
                ->useVldSekolahRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldSekolah->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldSekolahRelatedByIdtype() only accepts arguments of type VldSekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldSekolahRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldSekolahRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldSekolahRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldSekolahRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldSekolahRelatedByIdtype relation VldSekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldSekolahQuery A secondary query class using the current class as primary query
     */
    public function useVldSekolahRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldSekolahRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldSekolahRelatedByIdtype', '\angulex\Model\VldSekolahQuery');
    }

    /**
     * Filter the query by a related VldSekolah object
     *
     * @param   VldSekolah|PropelObjectCollection $vldSekolah  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldSekolahRelatedByIdtype($vldSekolah, $comparison = null)
    {
        if ($vldSekolah instanceof VldSekolah) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldSekolah->getIdtype(), $comparison);
        } elseif ($vldSekolah instanceof PropelObjectCollection) {
            return $this
                ->useVldSekolahRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldSekolah->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldSekolahRelatedByIdtype() only accepts arguments of type VldSekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldSekolahRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldSekolahRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldSekolahRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldSekolahRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldSekolahRelatedByIdtype relation VldSekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldSekolahQuery A secondary query class using the current class as primary query
     */
    public function useVldSekolahRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldSekolahRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldSekolahRelatedByIdtype', '\angulex\Model\VldSekolahQuery');
    }

    /**
     * Filter the query by a related VldSarana object
     *
     * @param   VldSarana|PropelObjectCollection $vldSarana  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldSaranaRelatedByIdtype($vldSarana, $comparison = null)
    {
        if ($vldSarana instanceof VldSarana) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldSarana->getIdtype(), $comparison);
        } elseif ($vldSarana instanceof PropelObjectCollection) {
            return $this
                ->useVldSaranaRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldSarana->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldSaranaRelatedByIdtype() only accepts arguments of type VldSarana or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldSaranaRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldSaranaRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldSaranaRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldSaranaRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldSaranaRelatedByIdtype relation VldSarana object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldSaranaQuery A secondary query class using the current class as primary query
     */
    public function useVldSaranaRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldSaranaRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldSaranaRelatedByIdtype', '\angulex\Model\VldSaranaQuery');
    }

    /**
     * Filter the query by a related VldSarana object
     *
     * @param   VldSarana|PropelObjectCollection $vldSarana  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldSaranaRelatedByIdtype($vldSarana, $comparison = null)
    {
        if ($vldSarana instanceof VldSarana) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldSarana->getIdtype(), $comparison);
        } elseif ($vldSarana instanceof PropelObjectCollection) {
            return $this
                ->useVldSaranaRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldSarana->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldSaranaRelatedByIdtype() only accepts arguments of type VldSarana or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldSaranaRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldSaranaRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldSaranaRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldSaranaRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldSaranaRelatedByIdtype relation VldSarana object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldSaranaQuery A secondary query class using the current class as primary query
     */
    public function useVldSaranaRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldSaranaRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldSaranaRelatedByIdtype', '\angulex\Model\VldSaranaQuery');
    }

    /**
     * Filter the query by a related VldRwyStruktural object
     *
     * @param   VldRwyStruktural|PropelObjectCollection $vldRwyStruktural  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldRwyStrukturalRelatedByIdtype($vldRwyStruktural, $comparison = null)
    {
        if ($vldRwyStruktural instanceof VldRwyStruktural) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldRwyStruktural->getIdtype(), $comparison);
        } elseif ($vldRwyStruktural instanceof PropelObjectCollection) {
            return $this
                ->useVldRwyStrukturalRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldRwyStruktural->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldRwyStrukturalRelatedByIdtype() only accepts arguments of type VldRwyStruktural or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldRwyStrukturalRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldRwyStrukturalRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldRwyStrukturalRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldRwyStrukturalRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldRwyStrukturalRelatedByIdtype relation VldRwyStruktural object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldRwyStrukturalQuery A secondary query class using the current class as primary query
     */
    public function useVldRwyStrukturalRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldRwyStrukturalRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldRwyStrukturalRelatedByIdtype', '\angulex\Model\VldRwyStrukturalQuery');
    }

    /**
     * Filter the query by a related VldRwyStruktural object
     *
     * @param   VldRwyStruktural|PropelObjectCollection $vldRwyStruktural  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldRwyStrukturalRelatedByIdtype($vldRwyStruktural, $comparison = null)
    {
        if ($vldRwyStruktural instanceof VldRwyStruktural) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldRwyStruktural->getIdtype(), $comparison);
        } elseif ($vldRwyStruktural instanceof PropelObjectCollection) {
            return $this
                ->useVldRwyStrukturalRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldRwyStruktural->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldRwyStrukturalRelatedByIdtype() only accepts arguments of type VldRwyStruktural or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldRwyStrukturalRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldRwyStrukturalRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldRwyStrukturalRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldRwyStrukturalRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldRwyStrukturalRelatedByIdtype relation VldRwyStruktural object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldRwyStrukturalQuery A secondary query class using the current class as primary query
     */
    public function useVldRwyStrukturalRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldRwyStrukturalRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldRwyStrukturalRelatedByIdtype', '\angulex\Model\VldRwyStrukturalQuery');
    }

    /**
     * Filter the query by a related VldRwySertifikasi object
     *
     * @param   VldRwySertifikasi|PropelObjectCollection $vldRwySertifikasi  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldRwySertifikasiRelatedByIdtype($vldRwySertifikasi, $comparison = null)
    {
        if ($vldRwySertifikasi instanceof VldRwySertifikasi) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldRwySertifikasi->getIdtype(), $comparison);
        } elseif ($vldRwySertifikasi instanceof PropelObjectCollection) {
            return $this
                ->useVldRwySertifikasiRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldRwySertifikasi->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldRwySertifikasiRelatedByIdtype() only accepts arguments of type VldRwySertifikasi or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldRwySertifikasiRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldRwySertifikasiRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldRwySertifikasiRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldRwySertifikasiRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldRwySertifikasiRelatedByIdtype relation VldRwySertifikasi object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldRwySertifikasiQuery A secondary query class using the current class as primary query
     */
    public function useVldRwySertifikasiRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldRwySertifikasiRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldRwySertifikasiRelatedByIdtype', '\angulex\Model\VldRwySertifikasiQuery');
    }

    /**
     * Filter the query by a related VldRwySertifikasi object
     *
     * @param   VldRwySertifikasi|PropelObjectCollection $vldRwySertifikasi  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldRwySertifikasiRelatedByIdtype($vldRwySertifikasi, $comparison = null)
    {
        if ($vldRwySertifikasi instanceof VldRwySertifikasi) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldRwySertifikasi->getIdtype(), $comparison);
        } elseif ($vldRwySertifikasi instanceof PropelObjectCollection) {
            return $this
                ->useVldRwySertifikasiRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldRwySertifikasi->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldRwySertifikasiRelatedByIdtype() only accepts arguments of type VldRwySertifikasi or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldRwySertifikasiRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldRwySertifikasiRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldRwySertifikasiRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldRwySertifikasiRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldRwySertifikasiRelatedByIdtype relation VldRwySertifikasi object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldRwySertifikasiQuery A secondary query class using the current class as primary query
     */
    public function useVldRwySertifikasiRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldRwySertifikasiRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldRwySertifikasiRelatedByIdtype', '\angulex\Model\VldRwySertifikasiQuery');
    }

    /**
     * Filter the query by a related VldRwyPendFormal object
     *
     * @param   VldRwyPendFormal|PropelObjectCollection $vldRwyPendFormal  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldRwyPendFormalRelatedByIdtype($vldRwyPendFormal, $comparison = null)
    {
        if ($vldRwyPendFormal instanceof VldRwyPendFormal) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldRwyPendFormal->getIdtype(), $comparison);
        } elseif ($vldRwyPendFormal instanceof PropelObjectCollection) {
            return $this
                ->useVldRwyPendFormalRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldRwyPendFormal->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldRwyPendFormalRelatedByIdtype() only accepts arguments of type VldRwyPendFormal or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldRwyPendFormalRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldRwyPendFormalRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldRwyPendFormalRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldRwyPendFormalRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldRwyPendFormalRelatedByIdtype relation VldRwyPendFormal object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldRwyPendFormalQuery A secondary query class using the current class as primary query
     */
    public function useVldRwyPendFormalRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldRwyPendFormalRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldRwyPendFormalRelatedByIdtype', '\angulex\Model\VldRwyPendFormalQuery');
    }

    /**
     * Filter the query by a related VldRwyPendFormal object
     *
     * @param   VldRwyPendFormal|PropelObjectCollection $vldRwyPendFormal  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ErrortypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldRwyPendFormalRelatedByIdtype($vldRwyPendFormal, $comparison = null)
    {
        if ($vldRwyPendFormal instanceof VldRwyPendFormal) {
            return $this
                ->addUsingAlias(ErrortypePeer::IDTYPE, $vldRwyPendFormal->getIdtype(), $comparison);
        } elseif ($vldRwyPendFormal instanceof PropelObjectCollection) {
            return $this
                ->useVldRwyPendFormalRelatedByIdtypeQuery()
                ->filterByPrimaryKeys($vldRwyPendFormal->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldRwyPendFormalRelatedByIdtype() only accepts arguments of type VldRwyPendFormal or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldRwyPendFormalRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function joinVldRwyPendFormalRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldRwyPendFormalRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldRwyPendFormalRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the VldRwyPendFormalRelatedByIdtype relation VldRwyPendFormal object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldRwyPendFormalQuery A secondary query class using the current class as primary query
     */
    public function useVldRwyPendFormalRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldRwyPendFormalRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldRwyPendFormalRelatedByIdtype', '\angulex\Model\VldRwyPendFormalQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Errortype $errortype Object to remove from the list of results
     *
     * @return ErrortypeQuery The current query, for fluid interface
     */
    public function prune($errortype = null)
    {
        if ($errortype) {
            $this->addUsingAlias(ErrortypePeer::IDTYPE, $errortype->getIdtype(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
