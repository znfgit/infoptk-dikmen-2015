<?php

namespace angulex\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelDateTime;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\Inpassing;
use angulex\Model\InpassingPeer;
use angulex\Model\InpassingQuery;
use angulex\Model\PangkatGolongan;
use angulex\Model\PangkatGolonganQuery;
use angulex\Model\Ptk;
use angulex\Model\PtkQuery;
use angulex\Model\VldInpassing;
use angulex\Model\VldInpassingQuery;

/**
 * Base class that represents a row from the 'inpassing' table.
 *
 * 
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseInpassing extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'angulex\\Model\\InpassingPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        InpassingPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the inpassing_id field.
     * @var        string
     */
    protected $inpassing_id;

    /**
     * The value for the pangkat_golongan_id field.
     * @var        string
     */
    protected $pangkat_golongan_id;

    /**
     * The value for the ptk_id field.
     * @var        string
     */
    protected $ptk_id;

    /**
     * The value for the no_sk_inpassing field.
     * @var        string
     */
    protected $no_sk_inpassing;

    /**
     * The value for the tmt_inpassing field.
     * @var        string
     */
    protected $tmt_inpassing;

    /**
     * The value for the angka_kredit field.
     * Note: this column has a database default value of: '((0))'
     * @var        string
     */
    protected $angka_kredit;

    /**
     * The value for the masa_kerja_tahun field.
     * Note: this column has a database default value of: '((0))'
     * @var        string
     */
    protected $masa_kerja_tahun;

    /**
     * The value for the masa_kerja_bulan field.
     * Note: this column has a database default value of: '((0))'
     * @var        string
     */
    protected $masa_kerja_bulan;

    /**
     * The value for the last_update field.
     * @var        string
     */
    protected $last_update;

    /**
     * The value for the soft_delete field.
     * @var        string
     */
    protected $soft_delete;

    /**
     * The value for the last_sync field.
     * @var        string
     */
    protected $last_sync;

    /**
     * The value for the updater_id field.
     * @var        string
     */
    protected $updater_id;

    /**
     * @var        Ptk
     */
    protected $aPtkRelatedByPtkId;

    /**
     * @var        Ptk
     */
    protected $aPtkRelatedByPtkId;

    /**
     * @var        PangkatGolongan
     */
    protected $aPangkatGolonganRelatedByPangkatGolonganId;

    /**
     * @var        PangkatGolongan
     */
    protected $aPangkatGolonganRelatedByPangkatGolonganId;

    /**
     * @var        PropelObjectCollection|VldInpassing[] Collection to store aggregation of VldInpassing objects.
     */
    protected $collVldInpassingsRelatedByInpassingId;
    protected $collVldInpassingsRelatedByInpassingIdPartial;

    /**
     * @var        PropelObjectCollection|VldInpassing[] Collection to store aggregation of VldInpassing objects.
     */
    protected $collVldInpassingsRelatedByInpassingId;
    protected $collVldInpassingsRelatedByInpassingIdPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldInpassingsRelatedByInpassingIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldInpassingsRelatedByInpassingIdScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->angka_kredit = '((0))';
        $this->masa_kerja_tahun = '((0))';
        $this->masa_kerja_bulan = '((0))';
    }

    /**
     * Initializes internal state of BaseInpassing object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [inpassing_id] column value.
     * 
     * @return string
     */
    public function getInpassingId()
    {
        return $this->inpassing_id;
    }

    /**
     * Get the [pangkat_golongan_id] column value.
     * 
     * @return string
     */
    public function getPangkatGolonganId()
    {
        return $this->pangkat_golongan_id;
    }

    /**
     * Get the [ptk_id] column value.
     * 
     * @return string
     */
    public function getPtkId()
    {
        return $this->ptk_id;
    }

    /**
     * Get the [no_sk_inpassing] column value.
     * 
     * @return string
     */
    public function getNoSkInpassing()
    {
        return $this->no_sk_inpassing;
    }

    /**
     * Get the [tmt_inpassing] column value.
     * 
     * @return string
     */
    public function getTmtInpassing()
    {
        return $this->tmt_inpassing;
    }

    /**
     * Get the [angka_kredit] column value.
     * 
     * @return string
     */
    public function getAngkaKredit()
    {
        return $this->angka_kredit;
    }

    /**
     * Get the [masa_kerja_tahun] column value.
     * 
     * @return string
     */
    public function getMasaKerjaTahun()
    {
        return $this->masa_kerja_tahun;
    }

    /**
     * Get the [masa_kerja_bulan] column value.
     * 
     * @return string
     */
    public function getMasaKerjaBulan()
    {
        return $this->masa_kerja_bulan;
    }

    /**
     * Get the [optionally formatted] temporal [last_update] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastUpdate($format = 'Y-m-d H:i:s')
    {
        if ($this->last_update === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_update);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_update, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [soft_delete] column value.
     * 
     * @return string
     */
    public function getSoftDelete()
    {
        return $this->soft_delete;
    }

    /**
     * Get the [optionally formatted] temporal [last_sync] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastSync($format = 'Y-m-d H:i:s')
    {
        if ($this->last_sync === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_sync);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_sync, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [updater_id] column value.
     * 
     * @return string
     */
    public function getUpdaterId()
    {
        return $this->updater_id;
    }

    /**
     * Set the value of [inpassing_id] column.
     * 
     * @param string $v new value
     * @return Inpassing The current object (for fluent API support)
     */
    public function setInpassingId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->inpassing_id !== $v) {
            $this->inpassing_id = $v;
            $this->modifiedColumns[] = InpassingPeer::INPASSING_ID;
        }


        return $this;
    } // setInpassingId()

    /**
     * Set the value of [pangkat_golongan_id] column.
     * 
     * @param string $v new value
     * @return Inpassing The current object (for fluent API support)
     */
    public function setPangkatGolonganId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->pangkat_golongan_id !== $v) {
            $this->pangkat_golongan_id = $v;
            $this->modifiedColumns[] = InpassingPeer::PANGKAT_GOLONGAN_ID;
        }

        if ($this->aPangkatGolonganRelatedByPangkatGolonganId !== null && $this->aPangkatGolonganRelatedByPangkatGolonganId->getPangkatGolonganId() !== $v) {
            $this->aPangkatGolonganRelatedByPangkatGolonganId = null;
        }

        if ($this->aPangkatGolonganRelatedByPangkatGolonganId !== null && $this->aPangkatGolonganRelatedByPangkatGolonganId->getPangkatGolonganId() !== $v) {
            $this->aPangkatGolonganRelatedByPangkatGolonganId = null;
        }


        return $this;
    } // setPangkatGolonganId()

    /**
     * Set the value of [ptk_id] column.
     * 
     * @param string $v new value
     * @return Inpassing The current object (for fluent API support)
     */
    public function setPtkId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->ptk_id !== $v) {
            $this->ptk_id = $v;
            $this->modifiedColumns[] = InpassingPeer::PTK_ID;
        }

        if ($this->aPtkRelatedByPtkId !== null && $this->aPtkRelatedByPtkId->getPtkId() !== $v) {
            $this->aPtkRelatedByPtkId = null;
        }

        if ($this->aPtkRelatedByPtkId !== null && $this->aPtkRelatedByPtkId->getPtkId() !== $v) {
            $this->aPtkRelatedByPtkId = null;
        }


        return $this;
    } // setPtkId()

    /**
     * Set the value of [no_sk_inpassing] column.
     * 
     * @param string $v new value
     * @return Inpassing The current object (for fluent API support)
     */
    public function setNoSkInpassing($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->no_sk_inpassing !== $v) {
            $this->no_sk_inpassing = $v;
            $this->modifiedColumns[] = InpassingPeer::NO_SK_INPASSING;
        }


        return $this;
    } // setNoSkInpassing()

    /**
     * Set the value of [tmt_inpassing] column.
     * 
     * @param string $v new value
     * @return Inpassing The current object (for fluent API support)
     */
    public function setTmtInpassing($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tmt_inpassing !== $v) {
            $this->tmt_inpassing = $v;
            $this->modifiedColumns[] = InpassingPeer::TMT_INPASSING;
        }


        return $this;
    } // setTmtInpassing()

    /**
     * Set the value of [angka_kredit] column.
     * 
     * @param string $v new value
     * @return Inpassing The current object (for fluent API support)
     */
    public function setAngkaKredit($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->angka_kredit !== $v) {
            $this->angka_kredit = $v;
            $this->modifiedColumns[] = InpassingPeer::ANGKA_KREDIT;
        }


        return $this;
    } // setAngkaKredit()

    /**
     * Set the value of [masa_kerja_tahun] column.
     * 
     * @param string $v new value
     * @return Inpassing The current object (for fluent API support)
     */
    public function setMasaKerjaTahun($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->masa_kerja_tahun !== $v) {
            $this->masa_kerja_tahun = $v;
            $this->modifiedColumns[] = InpassingPeer::MASA_KERJA_TAHUN;
        }


        return $this;
    } // setMasaKerjaTahun()

    /**
     * Set the value of [masa_kerja_bulan] column.
     * 
     * @param string $v new value
     * @return Inpassing The current object (for fluent API support)
     */
    public function setMasaKerjaBulan($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->masa_kerja_bulan !== $v) {
            $this->masa_kerja_bulan = $v;
            $this->modifiedColumns[] = InpassingPeer::MASA_KERJA_BULAN;
        }


        return $this;
    } // setMasaKerjaBulan()

    /**
     * Sets the value of [last_update] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Inpassing The current object (for fluent API support)
     */
    public function setLastUpdate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_update !== null || $dt !== null) {
            $currentDateAsString = ($this->last_update !== null && $tmpDt = new DateTime($this->last_update)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_update = $newDateAsString;
                $this->modifiedColumns[] = InpassingPeer::LAST_UPDATE;
            }
        } // if either are not null


        return $this;
    } // setLastUpdate()

    /**
     * Set the value of [soft_delete] column.
     * 
     * @param string $v new value
     * @return Inpassing The current object (for fluent API support)
     */
    public function setSoftDelete($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->soft_delete !== $v) {
            $this->soft_delete = $v;
            $this->modifiedColumns[] = InpassingPeer::SOFT_DELETE;
        }


        return $this;
    } // setSoftDelete()

    /**
     * Sets the value of [last_sync] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Inpassing The current object (for fluent API support)
     */
    public function setLastSync($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_sync !== null || $dt !== null) {
            $currentDateAsString = ($this->last_sync !== null && $tmpDt = new DateTime($this->last_sync)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_sync = $newDateAsString;
                $this->modifiedColumns[] = InpassingPeer::LAST_SYNC;
            }
        } // if either are not null


        return $this;
    } // setLastSync()

    /**
     * Set the value of [updater_id] column.
     * 
     * @param string $v new value
     * @return Inpassing The current object (for fluent API support)
     */
    public function setUpdaterId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->updater_id !== $v) {
            $this->updater_id = $v;
            $this->modifiedColumns[] = InpassingPeer::UPDATER_ID;
        }


        return $this;
    } // setUpdaterId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->angka_kredit !== '((0))') {
                return false;
            }

            if ($this->masa_kerja_tahun !== '((0))') {
                return false;
            }

            if ($this->masa_kerja_bulan !== '((0))') {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->inpassing_id = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
            $this->pangkat_golongan_id = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->ptk_id = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->no_sk_inpassing = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->tmt_inpassing = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->angka_kredit = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->masa_kerja_tahun = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->masa_kerja_bulan = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->last_update = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->soft_delete = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->last_sync = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->updater_id = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 12; // 12 = InpassingPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Inpassing object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aPangkatGolonganRelatedByPangkatGolonganId !== null && $this->pangkat_golongan_id !== $this->aPangkatGolonganRelatedByPangkatGolonganId->getPangkatGolonganId()) {
            $this->aPangkatGolonganRelatedByPangkatGolonganId = null;
        }
        if ($this->aPangkatGolonganRelatedByPangkatGolonganId !== null && $this->pangkat_golongan_id !== $this->aPangkatGolonganRelatedByPangkatGolonganId->getPangkatGolonganId()) {
            $this->aPangkatGolonganRelatedByPangkatGolonganId = null;
        }
        if ($this->aPtkRelatedByPtkId !== null && $this->ptk_id !== $this->aPtkRelatedByPtkId->getPtkId()) {
            $this->aPtkRelatedByPtkId = null;
        }
        if ($this->aPtkRelatedByPtkId !== null && $this->ptk_id !== $this->aPtkRelatedByPtkId->getPtkId()) {
            $this->aPtkRelatedByPtkId = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(InpassingPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = InpassingPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aPtkRelatedByPtkId = null;
            $this->aPtkRelatedByPtkId = null;
            $this->aPangkatGolonganRelatedByPangkatGolonganId = null;
            $this->aPangkatGolonganRelatedByPangkatGolonganId = null;
            $this->collVldInpassingsRelatedByInpassingId = null;

            $this->collVldInpassingsRelatedByInpassingId = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(InpassingPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = InpassingQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(InpassingPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                InpassingPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPtkRelatedByPtkId !== null) {
                if ($this->aPtkRelatedByPtkId->isModified() || $this->aPtkRelatedByPtkId->isNew()) {
                    $affectedRows += $this->aPtkRelatedByPtkId->save($con);
                }
                $this->setPtkRelatedByPtkId($this->aPtkRelatedByPtkId);
            }

            if ($this->aPtkRelatedByPtkId !== null) {
                if ($this->aPtkRelatedByPtkId->isModified() || $this->aPtkRelatedByPtkId->isNew()) {
                    $affectedRows += $this->aPtkRelatedByPtkId->save($con);
                }
                $this->setPtkRelatedByPtkId($this->aPtkRelatedByPtkId);
            }

            if ($this->aPangkatGolonganRelatedByPangkatGolonganId !== null) {
                if ($this->aPangkatGolonganRelatedByPangkatGolonganId->isModified() || $this->aPangkatGolonganRelatedByPangkatGolonganId->isNew()) {
                    $affectedRows += $this->aPangkatGolonganRelatedByPangkatGolonganId->save($con);
                }
                $this->setPangkatGolonganRelatedByPangkatGolonganId($this->aPangkatGolonganRelatedByPangkatGolonganId);
            }

            if ($this->aPangkatGolonganRelatedByPangkatGolonganId !== null) {
                if ($this->aPangkatGolonganRelatedByPangkatGolonganId->isModified() || $this->aPangkatGolonganRelatedByPangkatGolonganId->isNew()) {
                    $affectedRows += $this->aPangkatGolonganRelatedByPangkatGolonganId->save($con);
                }
                $this->setPangkatGolonganRelatedByPangkatGolonganId($this->aPangkatGolonganRelatedByPangkatGolonganId);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->vldInpassingsRelatedByInpassingIdScheduledForDeletion !== null) {
                if (!$this->vldInpassingsRelatedByInpassingIdScheduledForDeletion->isEmpty()) {
                    VldInpassingQuery::create()
                        ->filterByPrimaryKeys($this->vldInpassingsRelatedByInpassingIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldInpassingsRelatedByInpassingIdScheduledForDeletion = null;
                }
            }

            if ($this->collVldInpassingsRelatedByInpassingId !== null) {
                foreach ($this->collVldInpassingsRelatedByInpassingId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldInpassingsRelatedByInpassingIdScheduledForDeletion !== null) {
                if (!$this->vldInpassingsRelatedByInpassingIdScheduledForDeletion->isEmpty()) {
                    VldInpassingQuery::create()
                        ->filterByPrimaryKeys($this->vldInpassingsRelatedByInpassingIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldInpassingsRelatedByInpassingIdScheduledForDeletion = null;
                }
            }

            if ($this->collVldInpassingsRelatedByInpassingId !== null) {
                foreach ($this->collVldInpassingsRelatedByInpassingId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $criteria = $this->buildCriteria();
        $pk = BasePeer::doInsert($criteria, $con);
        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPtkRelatedByPtkId !== null) {
                if (!$this->aPtkRelatedByPtkId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPtkRelatedByPtkId->getValidationFailures());
                }
            }

            if ($this->aPtkRelatedByPtkId !== null) {
                if (!$this->aPtkRelatedByPtkId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPtkRelatedByPtkId->getValidationFailures());
                }
            }

            if ($this->aPangkatGolonganRelatedByPangkatGolonganId !== null) {
                if (!$this->aPangkatGolonganRelatedByPangkatGolonganId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPangkatGolonganRelatedByPangkatGolonganId->getValidationFailures());
                }
            }

            if ($this->aPangkatGolonganRelatedByPangkatGolonganId !== null) {
                if (!$this->aPangkatGolonganRelatedByPangkatGolonganId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPangkatGolonganRelatedByPangkatGolonganId->getValidationFailures());
                }
            }


            if (($retval = InpassingPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collVldInpassingsRelatedByInpassingId !== null) {
                    foreach ($this->collVldInpassingsRelatedByInpassingId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldInpassingsRelatedByInpassingId !== null) {
                    foreach ($this->collVldInpassingsRelatedByInpassingId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = InpassingPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getInpassingId();
                break;
            case 1:
                return $this->getPangkatGolonganId();
                break;
            case 2:
                return $this->getPtkId();
                break;
            case 3:
                return $this->getNoSkInpassing();
                break;
            case 4:
                return $this->getTmtInpassing();
                break;
            case 5:
                return $this->getAngkaKredit();
                break;
            case 6:
                return $this->getMasaKerjaTahun();
                break;
            case 7:
                return $this->getMasaKerjaBulan();
                break;
            case 8:
                return $this->getLastUpdate();
                break;
            case 9:
                return $this->getSoftDelete();
                break;
            case 10:
                return $this->getLastSync();
                break;
            case 11:
                return $this->getUpdaterId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Inpassing'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Inpassing'][$this->getPrimaryKey()] = true;
        $keys = InpassingPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getInpassingId(),
            $keys[1] => $this->getPangkatGolonganId(),
            $keys[2] => $this->getPtkId(),
            $keys[3] => $this->getNoSkInpassing(),
            $keys[4] => $this->getTmtInpassing(),
            $keys[5] => $this->getAngkaKredit(),
            $keys[6] => $this->getMasaKerjaTahun(),
            $keys[7] => $this->getMasaKerjaBulan(),
            $keys[8] => $this->getLastUpdate(),
            $keys[9] => $this->getSoftDelete(),
            $keys[10] => $this->getLastSync(),
            $keys[11] => $this->getUpdaterId(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aPtkRelatedByPtkId) {
                $result['PtkRelatedByPtkId'] = $this->aPtkRelatedByPtkId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPtkRelatedByPtkId) {
                $result['PtkRelatedByPtkId'] = $this->aPtkRelatedByPtkId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPangkatGolonganRelatedByPangkatGolonganId) {
                $result['PangkatGolonganRelatedByPangkatGolonganId'] = $this->aPangkatGolonganRelatedByPangkatGolonganId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPangkatGolonganRelatedByPangkatGolonganId) {
                $result['PangkatGolonganRelatedByPangkatGolonganId'] = $this->aPangkatGolonganRelatedByPangkatGolonganId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collVldInpassingsRelatedByInpassingId) {
                $result['VldInpassingsRelatedByInpassingId'] = $this->collVldInpassingsRelatedByInpassingId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldInpassingsRelatedByInpassingId) {
                $result['VldInpassingsRelatedByInpassingId'] = $this->collVldInpassingsRelatedByInpassingId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = InpassingPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setInpassingId($value);
                break;
            case 1:
                $this->setPangkatGolonganId($value);
                break;
            case 2:
                $this->setPtkId($value);
                break;
            case 3:
                $this->setNoSkInpassing($value);
                break;
            case 4:
                $this->setTmtInpassing($value);
                break;
            case 5:
                $this->setAngkaKredit($value);
                break;
            case 6:
                $this->setMasaKerjaTahun($value);
                break;
            case 7:
                $this->setMasaKerjaBulan($value);
                break;
            case 8:
                $this->setLastUpdate($value);
                break;
            case 9:
                $this->setSoftDelete($value);
                break;
            case 10:
                $this->setLastSync($value);
                break;
            case 11:
                $this->setUpdaterId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = InpassingPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setInpassingId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setPangkatGolonganId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setPtkId($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setNoSkInpassing($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setTmtInpassing($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setAngkaKredit($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setMasaKerjaTahun($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setMasaKerjaBulan($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setLastUpdate($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setSoftDelete($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setLastSync($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setUpdaterId($arr[$keys[11]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(InpassingPeer::DATABASE_NAME);

        if ($this->isColumnModified(InpassingPeer::INPASSING_ID)) $criteria->add(InpassingPeer::INPASSING_ID, $this->inpassing_id);
        if ($this->isColumnModified(InpassingPeer::PANGKAT_GOLONGAN_ID)) $criteria->add(InpassingPeer::PANGKAT_GOLONGAN_ID, $this->pangkat_golongan_id);
        if ($this->isColumnModified(InpassingPeer::PTK_ID)) $criteria->add(InpassingPeer::PTK_ID, $this->ptk_id);
        if ($this->isColumnModified(InpassingPeer::NO_SK_INPASSING)) $criteria->add(InpassingPeer::NO_SK_INPASSING, $this->no_sk_inpassing);
        if ($this->isColumnModified(InpassingPeer::TMT_INPASSING)) $criteria->add(InpassingPeer::TMT_INPASSING, $this->tmt_inpassing);
        if ($this->isColumnModified(InpassingPeer::ANGKA_KREDIT)) $criteria->add(InpassingPeer::ANGKA_KREDIT, $this->angka_kredit);
        if ($this->isColumnModified(InpassingPeer::MASA_KERJA_TAHUN)) $criteria->add(InpassingPeer::MASA_KERJA_TAHUN, $this->masa_kerja_tahun);
        if ($this->isColumnModified(InpassingPeer::MASA_KERJA_BULAN)) $criteria->add(InpassingPeer::MASA_KERJA_BULAN, $this->masa_kerja_bulan);
        if ($this->isColumnModified(InpassingPeer::LAST_UPDATE)) $criteria->add(InpassingPeer::LAST_UPDATE, $this->last_update);
        if ($this->isColumnModified(InpassingPeer::SOFT_DELETE)) $criteria->add(InpassingPeer::SOFT_DELETE, $this->soft_delete);
        if ($this->isColumnModified(InpassingPeer::LAST_SYNC)) $criteria->add(InpassingPeer::LAST_SYNC, $this->last_sync);
        if ($this->isColumnModified(InpassingPeer::UPDATER_ID)) $criteria->add(InpassingPeer::UPDATER_ID, $this->updater_id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(InpassingPeer::DATABASE_NAME);
        $criteria->add(InpassingPeer::INPASSING_ID, $this->inpassing_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getInpassingId();
    }

    /**
     * Generic method to set the primary key (inpassing_id column).
     *
     * @param  string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setInpassingId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getInpassingId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Inpassing (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setPangkatGolonganId($this->getPangkatGolonganId());
        $copyObj->setPtkId($this->getPtkId());
        $copyObj->setNoSkInpassing($this->getNoSkInpassing());
        $copyObj->setTmtInpassing($this->getTmtInpassing());
        $copyObj->setAngkaKredit($this->getAngkaKredit());
        $copyObj->setMasaKerjaTahun($this->getMasaKerjaTahun());
        $copyObj->setMasaKerjaBulan($this->getMasaKerjaBulan());
        $copyObj->setLastUpdate($this->getLastUpdate());
        $copyObj->setSoftDelete($this->getSoftDelete());
        $copyObj->setLastSync($this->getLastSync());
        $copyObj->setUpdaterId($this->getUpdaterId());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getVldInpassingsRelatedByInpassingId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldInpassingRelatedByInpassingId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldInpassingsRelatedByInpassingId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldInpassingRelatedByInpassingId($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setInpassingId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Inpassing Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return InpassingPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new InpassingPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a Ptk object.
     *
     * @param             Ptk $v
     * @return Inpassing The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPtkRelatedByPtkId(Ptk $v = null)
    {
        if ($v === null) {
            $this->setPtkId(NULL);
        } else {
            $this->setPtkId($v->getPtkId());
        }

        $this->aPtkRelatedByPtkId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Ptk object, it will not be re-added.
        if ($v !== null) {
            $v->addInpassingRelatedByPtkId($this);
        }


        return $this;
    }


    /**
     * Get the associated Ptk object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Ptk The associated Ptk object.
     * @throws PropelException
     */
    public function getPtkRelatedByPtkId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPtkRelatedByPtkId === null && (($this->ptk_id !== "" && $this->ptk_id !== null)) && $doQuery) {
            $this->aPtkRelatedByPtkId = PtkQuery::create()->findPk($this->ptk_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPtkRelatedByPtkId->addInpassingsRelatedByPtkId($this);
             */
        }

        return $this->aPtkRelatedByPtkId;
    }

    /**
     * Declares an association between this object and a Ptk object.
     *
     * @param             Ptk $v
     * @return Inpassing The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPtkRelatedByPtkId(Ptk $v = null)
    {
        if ($v === null) {
            $this->setPtkId(NULL);
        } else {
            $this->setPtkId($v->getPtkId());
        }

        $this->aPtkRelatedByPtkId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Ptk object, it will not be re-added.
        if ($v !== null) {
            $v->addInpassingRelatedByPtkId($this);
        }


        return $this;
    }


    /**
     * Get the associated Ptk object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Ptk The associated Ptk object.
     * @throws PropelException
     */
    public function getPtkRelatedByPtkId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPtkRelatedByPtkId === null && (($this->ptk_id !== "" && $this->ptk_id !== null)) && $doQuery) {
            $this->aPtkRelatedByPtkId = PtkQuery::create()->findPk($this->ptk_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPtkRelatedByPtkId->addInpassingsRelatedByPtkId($this);
             */
        }

        return $this->aPtkRelatedByPtkId;
    }

    /**
     * Declares an association between this object and a PangkatGolongan object.
     *
     * @param             PangkatGolongan $v
     * @return Inpassing The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPangkatGolonganRelatedByPangkatGolonganId(PangkatGolongan $v = null)
    {
        if ($v === null) {
            $this->setPangkatGolonganId(NULL);
        } else {
            $this->setPangkatGolonganId($v->getPangkatGolonganId());
        }

        $this->aPangkatGolonganRelatedByPangkatGolonganId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the PangkatGolongan object, it will not be re-added.
        if ($v !== null) {
            $v->addInpassingRelatedByPangkatGolonganId($this);
        }


        return $this;
    }


    /**
     * Get the associated PangkatGolongan object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return PangkatGolongan The associated PangkatGolongan object.
     * @throws PropelException
     */
    public function getPangkatGolonganRelatedByPangkatGolonganId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPangkatGolonganRelatedByPangkatGolonganId === null && (($this->pangkat_golongan_id !== "" && $this->pangkat_golongan_id !== null)) && $doQuery) {
            $this->aPangkatGolonganRelatedByPangkatGolonganId = PangkatGolonganQuery::create()->findPk($this->pangkat_golongan_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPangkatGolonganRelatedByPangkatGolonganId->addInpassingsRelatedByPangkatGolonganId($this);
             */
        }

        return $this->aPangkatGolonganRelatedByPangkatGolonganId;
    }

    /**
     * Declares an association between this object and a PangkatGolongan object.
     *
     * @param             PangkatGolongan $v
     * @return Inpassing The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPangkatGolonganRelatedByPangkatGolonganId(PangkatGolongan $v = null)
    {
        if ($v === null) {
            $this->setPangkatGolonganId(NULL);
        } else {
            $this->setPangkatGolonganId($v->getPangkatGolonganId());
        }

        $this->aPangkatGolonganRelatedByPangkatGolonganId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the PangkatGolongan object, it will not be re-added.
        if ($v !== null) {
            $v->addInpassingRelatedByPangkatGolonganId($this);
        }


        return $this;
    }


    /**
     * Get the associated PangkatGolongan object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return PangkatGolongan The associated PangkatGolongan object.
     * @throws PropelException
     */
    public function getPangkatGolonganRelatedByPangkatGolonganId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPangkatGolonganRelatedByPangkatGolonganId === null && (($this->pangkat_golongan_id !== "" && $this->pangkat_golongan_id !== null)) && $doQuery) {
            $this->aPangkatGolonganRelatedByPangkatGolonganId = PangkatGolonganQuery::create()->findPk($this->pangkat_golongan_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPangkatGolonganRelatedByPangkatGolonganId->addInpassingsRelatedByPangkatGolonganId($this);
             */
        }

        return $this->aPangkatGolonganRelatedByPangkatGolonganId;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('VldInpassingRelatedByInpassingId' == $relationName) {
            $this->initVldInpassingsRelatedByInpassingId();
        }
        if ('VldInpassingRelatedByInpassingId' == $relationName) {
            $this->initVldInpassingsRelatedByInpassingId();
        }
    }

    /**
     * Clears out the collVldInpassingsRelatedByInpassingId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Inpassing The current object (for fluent API support)
     * @see        addVldInpassingsRelatedByInpassingId()
     */
    public function clearVldInpassingsRelatedByInpassingId()
    {
        $this->collVldInpassingsRelatedByInpassingId = null; // important to set this to null since that means it is uninitialized
        $this->collVldInpassingsRelatedByInpassingIdPartial = null;

        return $this;
    }

    /**
     * reset is the collVldInpassingsRelatedByInpassingId collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldInpassingsRelatedByInpassingId($v = true)
    {
        $this->collVldInpassingsRelatedByInpassingIdPartial = $v;
    }

    /**
     * Initializes the collVldInpassingsRelatedByInpassingId collection.
     *
     * By default this just sets the collVldInpassingsRelatedByInpassingId collection to an empty array (like clearcollVldInpassingsRelatedByInpassingId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldInpassingsRelatedByInpassingId($overrideExisting = true)
    {
        if (null !== $this->collVldInpassingsRelatedByInpassingId && !$overrideExisting) {
            return;
        }
        $this->collVldInpassingsRelatedByInpassingId = new PropelObjectCollection();
        $this->collVldInpassingsRelatedByInpassingId->setModel('VldInpassing');
    }

    /**
     * Gets an array of VldInpassing objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Inpassing is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldInpassing[] List of VldInpassing objects
     * @throws PropelException
     */
    public function getVldInpassingsRelatedByInpassingId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldInpassingsRelatedByInpassingIdPartial && !$this->isNew();
        if (null === $this->collVldInpassingsRelatedByInpassingId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldInpassingsRelatedByInpassingId) {
                // return empty collection
                $this->initVldInpassingsRelatedByInpassingId();
            } else {
                $collVldInpassingsRelatedByInpassingId = VldInpassingQuery::create(null, $criteria)
                    ->filterByInpassingRelatedByInpassingId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldInpassingsRelatedByInpassingIdPartial && count($collVldInpassingsRelatedByInpassingId)) {
                      $this->initVldInpassingsRelatedByInpassingId(false);

                      foreach($collVldInpassingsRelatedByInpassingId as $obj) {
                        if (false == $this->collVldInpassingsRelatedByInpassingId->contains($obj)) {
                          $this->collVldInpassingsRelatedByInpassingId->append($obj);
                        }
                      }

                      $this->collVldInpassingsRelatedByInpassingIdPartial = true;
                    }

                    $collVldInpassingsRelatedByInpassingId->getInternalIterator()->rewind();
                    return $collVldInpassingsRelatedByInpassingId;
                }

                if($partial && $this->collVldInpassingsRelatedByInpassingId) {
                    foreach($this->collVldInpassingsRelatedByInpassingId as $obj) {
                        if($obj->isNew()) {
                            $collVldInpassingsRelatedByInpassingId[] = $obj;
                        }
                    }
                }

                $this->collVldInpassingsRelatedByInpassingId = $collVldInpassingsRelatedByInpassingId;
                $this->collVldInpassingsRelatedByInpassingIdPartial = false;
            }
        }

        return $this->collVldInpassingsRelatedByInpassingId;
    }

    /**
     * Sets a collection of VldInpassingRelatedByInpassingId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldInpassingsRelatedByInpassingId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Inpassing The current object (for fluent API support)
     */
    public function setVldInpassingsRelatedByInpassingId(PropelCollection $vldInpassingsRelatedByInpassingId, PropelPDO $con = null)
    {
        $vldInpassingsRelatedByInpassingIdToDelete = $this->getVldInpassingsRelatedByInpassingId(new Criteria(), $con)->diff($vldInpassingsRelatedByInpassingId);

        $this->vldInpassingsRelatedByInpassingIdScheduledForDeletion = unserialize(serialize($vldInpassingsRelatedByInpassingIdToDelete));

        foreach ($vldInpassingsRelatedByInpassingIdToDelete as $vldInpassingRelatedByInpassingIdRemoved) {
            $vldInpassingRelatedByInpassingIdRemoved->setInpassingRelatedByInpassingId(null);
        }

        $this->collVldInpassingsRelatedByInpassingId = null;
        foreach ($vldInpassingsRelatedByInpassingId as $vldInpassingRelatedByInpassingId) {
            $this->addVldInpassingRelatedByInpassingId($vldInpassingRelatedByInpassingId);
        }

        $this->collVldInpassingsRelatedByInpassingId = $vldInpassingsRelatedByInpassingId;
        $this->collVldInpassingsRelatedByInpassingIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldInpassing objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldInpassing objects.
     * @throws PropelException
     */
    public function countVldInpassingsRelatedByInpassingId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldInpassingsRelatedByInpassingIdPartial && !$this->isNew();
        if (null === $this->collVldInpassingsRelatedByInpassingId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldInpassingsRelatedByInpassingId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldInpassingsRelatedByInpassingId());
            }
            $query = VldInpassingQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByInpassingRelatedByInpassingId($this)
                ->count($con);
        }

        return count($this->collVldInpassingsRelatedByInpassingId);
    }

    /**
     * Method called to associate a VldInpassing object to this object
     * through the VldInpassing foreign key attribute.
     *
     * @param    VldInpassing $l VldInpassing
     * @return Inpassing The current object (for fluent API support)
     */
    public function addVldInpassingRelatedByInpassingId(VldInpassing $l)
    {
        if ($this->collVldInpassingsRelatedByInpassingId === null) {
            $this->initVldInpassingsRelatedByInpassingId();
            $this->collVldInpassingsRelatedByInpassingIdPartial = true;
        }
        if (!in_array($l, $this->collVldInpassingsRelatedByInpassingId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldInpassingRelatedByInpassingId($l);
        }

        return $this;
    }

    /**
     * @param	VldInpassingRelatedByInpassingId $vldInpassingRelatedByInpassingId The vldInpassingRelatedByInpassingId object to add.
     */
    protected function doAddVldInpassingRelatedByInpassingId($vldInpassingRelatedByInpassingId)
    {
        $this->collVldInpassingsRelatedByInpassingId[]= $vldInpassingRelatedByInpassingId;
        $vldInpassingRelatedByInpassingId->setInpassingRelatedByInpassingId($this);
    }

    /**
     * @param	VldInpassingRelatedByInpassingId $vldInpassingRelatedByInpassingId The vldInpassingRelatedByInpassingId object to remove.
     * @return Inpassing The current object (for fluent API support)
     */
    public function removeVldInpassingRelatedByInpassingId($vldInpassingRelatedByInpassingId)
    {
        if ($this->getVldInpassingsRelatedByInpassingId()->contains($vldInpassingRelatedByInpassingId)) {
            $this->collVldInpassingsRelatedByInpassingId->remove($this->collVldInpassingsRelatedByInpassingId->search($vldInpassingRelatedByInpassingId));
            if (null === $this->vldInpassingsRelatedByInpassingIdScheduledForDeletion) {
                $this->vldInpassingsRelatedByInpassingIdScheduledForDeletion = clone $this->collVldInpassingsRelatedByInpassingId;
                $this->vldInpassingsRelatedByInpassingIdScheduledForDeletion->clear();
            }
            $this->vldInpassingsRelatedByInpassingIdScheduledForDeletion[]= clone $vldInpassingRelatedByInpassingId;
            $vldInpassingRelatedByInpassingId->setInpassingRelatedByInpassingId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Inpassing is new, it will return
     * an empty collection; or if this Inpassing has previously
     * been saved, it will retrieve related VldInpassingsRelatedByInpassingId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Inpassing.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldInpassing[] List of VldInpassing objects
     */
    public function getVldInpassingsRelatedByInpassingIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldInpassingQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldInpassingsRelatedByInpassingId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Inpassing is new, it will return
     * an empty collection; or if this Inpassing has previously
     * been saved, it will retrieve related VldInpassingsRelatedByInpassingId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Inpassing.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldInpassing[] List of VldInpassing objects
     */
    public function getVldInpassingsRelatedByInpassingIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldInpassingQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldInpassingsRelatedByInpassingId($query, $con);
    }

    /**
     * Clears out the collVldInpassingsRelatedByInpassingId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Inpassing The current object (for fluent API support)
     * @see        addVldInpassingsRelatedByInpassingId()
     */
    public function clearVldInpassingsRelatedByInpassingId()
    {
        $this->collVldInpassingsRelatedByInpassingId = null; // important to set this to null since that means it is uninitialized
        $this->collVldInpassingsRelatedByInpassingIdPartial = null;

        return $this;
    }

    /**
     * reset is the collVldInpassingsRelatedByInpassingId collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldInpassingsRelatedByInpassingId($v = true)
    {
        $this->collVldInpassingsRelatedByInpassingIdPartial = $v;
    }

    /**
     * Initializes the collVldInpassingsRelatedByInpassingId collection.
     *
     * By default this just sets the collVldInpassingsRelatedByInpassingId collection to an empty array (like clearcollVldInpassingsRelatedByInpassingId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldInpassingsRelatedByInpassingId($overrideExisting = true)
    {
        if (null !== $this->collVldInpassingsRelatedByInpassingId && !$overrideExisting) {
            return;
        }
        $this->collVldInpassingsRelatedByInpassingId = new PropelObjectCollection();
        $this->collVldInpassingsRelatedByInpassingId->setModel('VldInpassing');
    }

    /**
     * Gets an array of VldInpassing objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Inpassing is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldInpassing[] List of VldInpassing objects
     * @throws PropelException
     */
    public function getVldInpassingsRelatedByInpassingId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldInpassingsRelatedByInpassingIdPartial && !$this->isNew();
        if (null === $this->collVldInpassingsRelatedByInpassingId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldInpassingsRelatedByInpassingId) {
                // return empty collection
                $this->initVldInpassingsRelatedByInpassingId();
            } else {
                $collVldInpassingsRelatedByInpassingId = VldInpassingQuery::create(null, $criteria)
                    ->filterByInpassingRelatedByInpassingId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldInpassingsRelatedByInpassingIdPartial && count($collVldInpassingsRelatedByInpassingId)) {
                      $this->initVldInpassingsRelatedByInpassingId(false);

                      foreach($collVldInpassingsRelatedByInpassingId as $obj) {
                        if (false == $this->collVldInpassingsRelatedByInpassingId->contains($obj)) {
                          $this->collVldInpassingsRelatedByInpassingId->append($obj);
                        }
                      }

                      $this->collVldInpassingsRelatedByInpassingIdPartial = true;
                    }

                    $collVldInpassingsRelatedByInpassingId->getInternalIterator()->rewind();
                    return $collVldInpassingsRelatedByInpassingId;
                }

                if($partial && $this->collVldInpassingsRelatedByInpassingId) {
                    foreach($this->collVldInpassingsRelatedByInpassingId as $obj) {
                        if($obj->isNew()) {
                            $collVldInpassingsRelatedByInpassingId[] = $obj;
                        }
                    }
                }

                $this->collVldInpassingsRelatedByInpassingId = $collVldInpassingsRelatedByInpassingId;
                $this->collVldInpassingsRelatedByInpassingIdPartial = false;
            }
        }

        return $this->collVldInpassingsRelatedByInpassingId;
    }

    /**
     * Sets a collection of VldInpassingRelatedByInpassingId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldInpassingsRelatedByInpassingId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Inpassing The current object (for fluent API support)
     */
    public function setVldInpassingsRelatedByInpassingId(PropelCollection $vldInpassingsRelatedByInpassingId, PropelPDO $con = null)
    {
        $vldInpassingsRelatedByInpassingIdToDelete = $this->getVldInpassingsRelatedByInpassingId(new Criteria(), $con)->diff($vldInpassingsRelatedByInpassingId);

        $this->vldInpassingsRelatedByInpassingIdScheduledForDeletion = unserialize(serialize($vldInpassingsRelatedByInpassingIdToDelete));

        foreach ($vldInpassingsRelatedByInpassingIdToDelete as $vldInpassingRelatedByInpassingIdRemoved) {
            $vldInpassingRelatedByInpassingIdRemoved->setInpassingRelatedByInpassingId(null);
        }

        $this->collVldInpassingsRelatedByInpassingId = null;
        foreach ($vldInpassingsRelatedByInpassingId as $vldInpassingRelatedByInpassingId) {
            $this->addVldInpassingRelatedByInpassingId($vldInpassingRelatedByInpassingId);
        }

        $this->collVldInpassingsRelatedByInpassingId = $vldInpassingsRelatedByInpassingId;
        $this->collVldInpassingsRelatedByInpassingIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldInpassing objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldInpassing objects.
     * @throws PropelException
     */
    public function countVldInpassingsRelatedByInpassingId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldInpassingsRelatedByInpassingIdPartial && !$this->isNew();
        if (null === $this->collVldInpassingsRelatedByInpassingId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldInpassingsRelatedByInpassingId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldInpassingsRelatedByInpassingId());
            }
            $query = VldInpassingQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByInpassingRelatedByInpassingId($this)
                ->count($con);
        }

        return count($this->collVldInpassingsRelatedByInpassingId);
    }

    /**
     * Method called to associate a VldInpassing object to this object
     * through the VldInpassing foreign key attribute.
     *
     * @param    VldInpassing $l VldInpassing
     * @return Inpassing The current object (for fluent API support)
     */
    public function addVldInpassingRelatedByInpassingId(VldInpassing $l)
    {
        if ($this->collVldInpassingsRelatedByInpassingId === null) {
            $this->initVldInpassingsRelatedByInpassingId();
            $this->collVldInpassingsRelatedByInpassingIdPartial = true;
        }
        if (!in_array($l, $this->collVldInpassingsRelatedByInpassingId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldInpassingRelatedByInpassingId($l);
        }

        return $this;
    }

    /**
     * @param	VldInpassingRelatedByInpassingId $vldInpassingRelatedByInpassingId The vldInpassingRelatedByInpassingId object to add.
     */
    protected function doAddVldInpassingRelatedByInpassingId($vldInpassingRelatedByInpassingId)
    {
        $this->collVldInpassingsRelatedByInpassingId[]= $vldInpassingRelatedByInpassingId;
        $vldInpassingRelatedByInpassingId->setInpassingRelatedByInpassingId($this);
    }

    /**
     * @param	VldInpassingRelatedByInpassingId $vldInpassingRelatedByInpassingId The vldInpassingRelatedByInpassingId object to remove.
     * @return Inpassing The current object (for fluent API support)
     */
    public function removeVldInpassingRelatedByInpassingId($vldInpassingRelatedByInpassingId)
    {
        if ($this->getVldInpassingsRelatedByInpassingId()->contains($vldInpassingRelatedByInpassingId)) {
            $this->collVldInpassingsRelatedByInpassingId->remove($this->collVldInpassingsRelatedByInpassingId->search($vldInpassingRelatedByInpassingId));
            if (null === $this->vldInpassingsRelatedByInpassingIdScheduledForDeletion) {
                $this->vldInpassingsRelatedByInpassingIdScheduledForDeletion = clone $this->collVldInpassingsRelatedByInpassingId;
                $this->vldInpassingsRelatedByInpassingIdScheduledForDeletion->clear();
            }
            $this->vldInpassingsRelatedByInpassingIdScheduledForDeletion[]= clone $vldInpassingRelatedByInpassingId;
            $vldInpassingRelatedByInpassingId->setInpassingRelatedByInpassingId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Inpassing is new, it will return
     * an empty collection; or if this Inpassing has previously
     * been saved, it will retrieve related VldInpassingsRelatedByInpassingId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Inpassing.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldInpassing[] List of VldInpassing objects
     */
    public function getVldInpassingsRelatedByInpassingIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldInpassingQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldInpassingsRelatedByInpassingId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Inpassing is new, it will return
     * an empty collection; or if this Inpassing has previously
     * been saved, it will retrieve related VldInpassingsRelatedByInpassingId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Inpassing.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldInpassing[] List of VldInpassing objects
     */
    public function getVldInpassingsRelatedByInpassingIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldInpassingQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldInpassingsRelatedByInpassingId($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->inpassing_id = null;
        $this->pangkat_golongan_id = null;
        $this->ptk_id = null;
        $this->no_sk_inpassing = null;
        $this->tmt_inpassing = null;
        $this->angka_kredit = null;
        $this->masa_kerja_tahun = null;
        $this->masa_kerja_bulan = null;
        $this->last_update = null;
        $this->soft_delete = null;
        $this->last_sync = null;
        $this->updater_id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collVldInpassingsRelatedByInpassingId) {
                foreach ($this->collVldInpassingsRelatedByInpassingId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldInpassingsRelatedByInpassingId) {
                foreach ($this->collVldInpassingsRelatedByInpassingId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aPtkRelatedByPtkId instanceof Persistent) {
              $this->aPtkRelatedByPtkId->clearAllReferences($deep);
            }
            if ($this->aPtkRelatedByPtkId instanceof Persistent) {
              $this->aPtkRelatedByPtkId->clearAllReferences($deep);
            }
            if ($this->aPangkatGolonganRelatedByPangkatGolonganId instanceof Persistent) {
              $this->aPangkatGolonganRelatedByPangkatGolonganId->clearAllReferences($deep);
            }
            if ($this->aPangkatGolonganRelatedByPangkatGolonganId instanceof Persistent) {
              $this->aPangkatGolonganRelatedByPangkatGolonganId->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collVldInpassingsRelatedByInpassingId instanceof PropelCollection) {
            $this->collVldInpassingsRelatedByInpassingId->clearIterator();
        }
        $this->collVldInpassingsRelatedByInpassingId = null;
        if ($this->collVldInpassingsRelatedByInpassingId instanceof PropelCollection) {
            $this->collVldInpassingsRelatedByInpassingId->clearIterator();
        }
        $this->collVldInpassingsRelatedByInpassingId = null;
        $this->aPtkRelatedByPtkId = null;
        $this->aPtkRelatedByPtkId = null;
        $this->aPangkatGolonganRelatedByPangkatGolonganId = null;
        $this->aPangkatGolonganRelatedByPangkatGolonganId = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(InpassingPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
