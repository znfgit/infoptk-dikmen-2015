<?php

namespace angulex\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelDateTime;
use \PropelException;
use \PropelPDO;
use angulex\Model\Pengguna;
use angulex\Model\PenggunaQuery;
use angulex\Model\SasaranSurvey;
use angulex\Model\SasaranSurveyPeer;
use angulex\Model\SasaranSurveyQuery;
use angulex\Model\Sekolah;
use angulex\Model\SekolahQuery;

/**
 * Base class that represents a row from the 'sasaran_survey' table.
 *
 * 
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseSasaranSurvey extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'angulex\\Model\\SasaranSurveyPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        SasaranSurveyPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the pengguna_id field.
     * @var        string
     */
    protected $pengguna_id;

    /**
     * The value for the sekolah_id field.
     * @var        string
     */
    protected $sekolah_id;

    /**
     * The value for the nomor_urut field.
     * @var        int
     */
    protected $nomor_urut;

    /**
     * The value for the tanggal_rencana field.
     * @var        string
     */
    protected $tanggal_rencana;

    /**
     * The value for the tanggal_pelaksanaan field.
     * @var        string
     */
    protected $tanggal_pelaksanaan;

    /**
     * The value for the waktu_berangkat field.
     * @var        string
     */
    protected $waktu_berangkat;

    /**
     * The value for the waktu_sampai field.
     * @var        string
     */
    protected $waktu_sampai;

    /**
     * The value for the waktu_mulai_survey field.
     * @var        string
     */
    protected $waktu_mulai_survey;

    /**
     * The value for the waktu_selesai field.
     * @var        string
     */
    protected $waktu_selesai;

    /**
     * The value for the waktu_isi_form_cetak field.
     * @var        int
     */
    protected $waktu_isi_form_cetak;

    /**
     * The value for the waktu_isi_form_elektronik field.
     * @var        int
     */
    protected $waktu_isi_form_elektronik;

    /**
     * @var        Pengguna
     */
    protected $aPenggunaRelatedByPenggunaId;

    /**
     * @var        Pengguna
     */
    protected $aPenggunaRelatedByPenggunaId;

    /**
     * @var        Sekolah
     */
    protected $aSekolahRelatedBySekolahId;

    /**
     * @var        Sekolah
     */
    protected $aSekolahRelatedBySekolahId;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Get the [pengguna_id] column value.
     * 
     * @return string
     */
    public function getPenggunaId()
    {
        return $this->pengguna_id;
    }

    /**
     * Get the [sekolah_id] column value.
     * 
     * @return string
     */
    public function getSekolahId()
    {
        return $this->sekolah_id;
    }

    /**
     * Get the [nomor_urut] column value.
     * 
     * @return int
     */
    public function getNomorUrut()
    {
        return $this->nomor_urut;
    }

    /**
     * Get the [tanggal_rencana] column value.
     * 
     * @return string
     */
    public function getTanggalRencana()
    {
        return $this->tanggal_rencana;
    }

    /**
     * Get the [tanggal_pelaksanaan] column value.
     * 
     * @return string
     */
    public function getTanggalPelaksanaan()
    {
        return $this->tanggal_pelaksanaan;
    }

    /**
     * Get the [optionally formatted] temporal [waktu_berangkat] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getWaktuBerangkat($format = 'Y-m-d H:i:s')
    {
        if ($this->waktu_berangkat === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->waktu_berangkat);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->waktu_berangkat, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [waktu_sampai] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getWaktuSampai($format = 'Y-m-d H:i:s')
    {
        if ($this->waktu_sampai === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->waktu_sampai);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->waktu_sampai, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [waktu_mulai_survey] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getWaktuMulaiSurvey($format = 'Y-m-d H:i:s')
    {
        if ($this->waktu_mulai_survey === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->waktu_mulai_survey);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->waktu_mulai_survey, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [waktu_selesai] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getWaktuSelesai($format = 'Y-m-d H:i:s')
    {
        if ($this->waktu_selesai === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->waktu_selesai);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->waktu_selesai, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [waktu_isi_form_cetak] column value.
     * 
     * @return int
     */
    public function getWaktuIsiFormCetak()
    {
        return $this->waktu_isi_form_cetak;
    }

    /**
     * Get the [waktu_isi_form_elektronik] column value.
     * 
     * @return int
     */
    public function getWaktuIsiFormElektronik()
    {
        return $this->waktu_isi_form_elektronik;
    }

    /**
     * Set the value of [pengguna_id] column.
     * 
     * @param string $v new value
     * @return SasaranSurvey The current object (for fluent API support)
     */
    public function setPenggunaId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->pengguna_id !== $v) {
            $this->pengguna_id = $v;
            $this->modifiedColumns[] = SasaranSurveyPeer::PENGGUNA_ID;
        }

        if ($this->aPenggunaRelatedByPenggunaId !== null && $this->aPenggunaRelatedByPenggunaId->getPenggunaId() !== $v) {
            $this->aPenggunaRelatedByPenggunaId = null;
        }

        if ($this->aPenggunaRelatedByPenggunaId !== null && $this->aPenggunaRelatedByPenggunaId->getPenggunaId() !== $v) {
            $this->aPenggunaRelatedByPenggunaId = null;
        }


        return $this;
    } // setPenggunaId()

    /**
     * Set the value of [sekolah_id] column.
     * 
     * @param string $v new value
     * @return SasaranSurvey The current object (for fluent API support)
     */
    public function setSekolahId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->sekolah_id !== $v) {
            $this->sekolah_id = $v;
            $this->modifiedColumns[] = SasaranSurveyPeer::SEKOLAH_ID;
        }

        if ($this->aSekolahRelatedBySekolahId !== null && $this->aSekolahRelatedBySekolahId->getSekolahId() !== $v) {
            $this->aSekolahRelatedBySekolahId = null;
        }

        if ($this->aSekolahRelatedBySekolahId !== null && $this->aSekolahRelatedBySekolahId->getSekolahId() !== $v) {
            $this->aSekolahRelatedBySekolahId = null;
        }


        return $this;
    } // setSekolahId()

    /**
     * Set the value of [nomor_urut] column.
     * 
     * @param int $v new value
     * @return SasaranSurvey The current object (for fluent API support)
     */
    public function setNomorUrut($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->nomor_urut !== $v) {
            $this->nomor_urut = $v;
            $this->modifiedColumns[] = SasaranSurveyPeer::NOMOR_URUT;
        }


        return $this;
    } // setNomorUrut()

    /**
     * Set the value of [tanggal_rencana] column.
     * 
     * @param string $v new value
     * @return SasaranSurvey The current object (for fluent API support)
     */
    public function setTanggalRencana($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tanggal_rencana !== $v) {
            $this->tanggal_rencana = $v;
            $this->modifiedColumns[] = SasaranSurveyPeer::TANGGAL_RENCANA;
        }


        return $this;
    } // setTanggalRencana()

    /**
     * Set the value of [tanggal_pelaksanaan] column.
     * 
     * @param string $v new value
     * @return SasaranSurvey The current object (for fluent API support)
     */
    public function setTanggalPelaksanaan($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tanggal_pelaksanaan !== $v) {
            $this->tanggal_pelaksanaan = $v;
            $this->modifiedColumns[] = SasaranSurveyPeer::TANGGAL_PELAKSANAAN;
        }


        return $this;
    } // setTanggalPelaksanaan()

    /**
     * Sets the value of [waktu_berangkat] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return SasaranSurvey The current object (for fluent API support)
     */
    public function setWaktuBerangkat($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->waktu_berangkat !== null || $dt !== null) {
            $currentDateAsString = ($this->waktu_berangkat !== null && $tmpDt = new DateTime($this->waktu_berangkat)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->waktu_berangkat = $newDateAsString;
                $this->modifiedColumns[] = SasaranSurveyPeer::WAKTU_BERANGKAT;
            }
        } // if either are not null


        return $this;
    } // setWaktuBerangkat()

    /**
     * Sets the value of [waktu_sampai] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return SasaranSurvey The current object (for fluent API support)
     */
    public function setWaktuSampai($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->waktu_sampai !== null || $dt !== null) {
            $currentDateAsString = ($this->waktu_sampai !== null && $tmpDt = new DateTime($this->waktu_sampai)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->waktu_sampai = $newDateAsString;
                $this->modifiedColumns[] = SasaranSurveyPeer::WAKTU_SAMPAI;
            }
        } // if either are not null


        return $this;
    } // setWaktuSampai()

    /**
     * Sets the value of [waktu_mulai_survey] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return SasaranSurvey The current object (for fluent API support)
     */
    public function setWaktuMulaiSurvey($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->waktu_mulai_survey !== null || $dt !== null) {
            $currentDateAsString = ($this->waktu_mulai_survey !== null && $tmpDt = new DateTime($this->waktu_mulai_survey)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->waktu_mulai_survey = $newDateAsString;
                $this->modifiedColumns[] = SasaranSurveyPeer::WAKTU_MULAI_SURVEY;
            }
        } // if either are not null


        return $this;
    } // setWaktuMulaiSurvey()

    /**
     * Sets the value of [waktu_selesai] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return SasaranSurvey The current object (for fluent API support)
     */
    public function setWaktuSelesai($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->waktu_selesai !== null || $dt !== null) {
            $currentDateAsString = ($this->waktu_selesai !== null && $tmpDt = new DateTime($this->waktu_selesai)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->waktu_selesai = $newDateAsString;
                $this->modifiedColumns[] = SasaranSurveyPeer::WAKTU_SELESAI;
            }
        } // if either are not null


        return $this;
    } // setWaktuSelesai()

    /**
     * Set the value of [waktu_isi_form_cetak] column.
     * 
     * @param int $v new value
     * @return SasaranSurvey The current object (for fluent API support)
     */
    public function setWaktuIsiFormCetak($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->waktu_isi_form_cetak !== $v) {
            $this->waktu_isi_form_cetak = $v;
            $this->modifiedColumns[] = SasaranSurveyPeer::WAKTU_ISI_FORM_CETAK;
        }


        return $this;
    } // setWaktuIsiFormCetak()

    /**
     * Set the value of [waktu_isi_form_elektronik] column.
     * 
     * @param int $v new value
     * @return SasaranSurvey The current object (for fluent API support)
     */
    public function setWaktuIsiFormElektronik($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->waktu_isi_form_elektronik !== $v) {
            $this->waktu_isi_form_elektronik = $v;
            $this->modifiedColumns[] = SasaranSurveyPeer::WAKTU_ISI_FORM_ELEKTRONIK;
        }


        return $this;
    } // setWaktuIsiFormElektronik()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->pengguna_id = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
            $this->sekolah_id = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->nomor_urut = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->tanggal_rencana = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->tanggal_pelaksanaan = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->waktu_berangkat = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->waktu_sampai = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->waktu_mulai_survey = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->waktu_selesai = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->waktu_isi_form_cetak = ($row[$startcol + 9] !== null) ? (int) $row[$startcol + 9] : null;
            $this->waktu_isi_form_elektronik = ($row[$startcol + 10] !== null) ? (int) $row[$startcol + 10] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 11; // 11 = SasaranSurveyPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating SasaranSurvey object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aPenggunaRelatedByPenggunaId !== null && $this->pengguna_id !== $this->aPenggunaRelatedByPenggunaId->getPenggunaId()) {
            $this->aPenggunaRelatedByPenggunaId = null;
        }
        if ($this->aPenggunaRelatedByPenggunaId !== null && $this->pengguna_id !== $this->aPenggunaRelatedByPenggunaId->getPenggunaId()) {
            $this->aPenggunaRelatedByPenggunaId = null;
        }
        if ($this->aSekolahRelatedBySekolahId !== null && $this->sekolah_id !== $this->aSekolahRelatedBySekolahId->getSekolahId()) {
            $this->aSekolahRelatedBySekolahId = null;
        }
        if ($this->aSekolahRelatedBySekolahId !== null && $this->sekolah_id !== $this->aSekolahRelatedBySekolahId->getSekolahId()) {
            $this->aSekolahRelatedBySekolahId = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SasaranSurveyPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = SasaranSurveyPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aPenggunaRelatedByPenggunaId = null;
            $this->aPenggunaRelatedByPenggunaId = null;
            $this->aSekolahRelatedBySekolahId = null;
            $this->aSekolahRelatedBySekolahId = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SasaranSurveyPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = SasaranSurveyQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SasaranSurveyPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                SasaranSurveyPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPenggunaRelatedByPenggunaId !== null) {
                if ($this->aPenggunaRelatedByPenggunaId->isModified() || $this->aPenggunaRelatedByPenggunaId->isNew()) {
                    $affectedRows += $this->aPenggunaRelatedByPenggunaId->save($con);
                }
                $this->setPenggunaRelatedByPenggunaId($this->aPenggunaRelatedByPenggunaId);
            }

            if ($this->aPenggunaRelatedByPenggunaId !== null) {
                if ($this->aPenggunaRelatedByPenggunaId->isModified() || $this->aPenggunaRelatedByPenggunaId->isNew()) {
                    $affectedRows += $this->aPenggunaRelatedByPenggunaId->save($con);
                }
                $this->setPenggunaRelatedByPenggunaId($this->aPenggunaRelatedByPenggunaId);
            }

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if ($this->aSekolahRelatedBySekolahId->isModified() || $this->aSekolahRelatedBySekolahId->isNew()) {
                    $affectedRows += $this->aSekolahRelatedBySekolahId->save($con);
                }
                $this->setSekolahRelatedBySekolahId($this->aSekolahRelatedBySekolahId);
            }

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if ($this->aSekolahRelatedBySekolahId->isModified() || $this->aSekolahRelatedBySekolahId->isNew()) {
                    $affectedRows += $this->aSekolahRelatedBySekolahId->save($con);
                }
                $this->setSekolahRelatedBySekolahId($this->aSekolahRelatedBySekolahId);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $criteria = $this->buildCriteria();
        $pk = BasePeer::doInsert($criteria, $con);
        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPenggunaRelatedByPenggunaId !== null) {
                if (!$this->aPenggunaRelatedByPenggunaId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPenggunaRelatedByPenggunaId->getValidationFailures());
                }
            }

            if ($this->aPenggunaRelatedByPenggunaId !== null) {
                if (!$this->aPenggunaRelatedByPenggunaId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPenggunaRelatedByPenggunaId->getValidationFailures());
                }
            }

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if (!$this->aSekolahRelatedBySekolahId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSekolahRelatedBySekolahId->getValidationFailures());
                }
            }

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if (!$this->aSekolahRelatedBySekolahId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSekolahRelatedBySekolahId->getValidationFailures());
                }
            }


            if (($retval = SasaranSurveyPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SasaranSurveyPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getPenggunaId();
                break;
            case 1:
                return $this->getSekolahId();
                break;
            case 2:
                return $this->getNomorUrut();
                break;
            case 3:
                return $this->getTanggalRencana();
                break;
            case 4:
                return $this->getTanggalPelaksanaan();
                break;
            case 5:
                return $this->getWaktuBerangkat();
                break;
            case 6:
                return $this->getWaktuSampai();
                break;
            case 7:
                return $this->getWaktuMulaiSurvey();
                break;
            case 8:
                return $this->getWaktuSelesai();
                break;
            case 9:
                return $this->getWaktuIsiFormCetak();
                break;
            case 10:
                return $this->getWaktuIsiFormElektronik();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['SasaranSurvey'][serialize($this->getPrimaryKey())])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['SasaranSurvey'][serialize($this->getPrimaryKey())] = true;
        $keys = SasaranSurveyPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getPenggunaId(),
            $keys[1] => $this->getSekolahId(),
            $keys[2] => $this->getNomorUrut(),
            $keys[3] => $this->getTanggalRencana(),
            $keys[4] => $this->getTanggalPelaksanaan(),
            $keys[5] => $this->getWaktuBerangkat(),
            $keys[6] => $this->getWaktuSampai(),
            $keys[7] => $this->getWaktuMulaiSurvey(),
            $keys[8] => $this->getWaktuSelesai(),
            $keys[9] => $this->getWaktuIsiFormCetak(),
            $keys[10] => $this->getWaktuIsiFormElektronik(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aPenggunaRelatedByPenggunaId) {
                $result['PenggunaRelatedByPenggunaId'] = $this->aPenggunaRelatedByPenggunaId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPenggunaRelatedByPenggunaId) {
                $result['PenggunaRelatedByPenggunaId'] = $this->aPenggunaRelatedByPenggunaId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSekolahRelatedBySekolahId) {
                $result['SekolahRelatedBySekolahId'] = $this->aSekolahRelatedBySekolahId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSekolahRelatedBySekolahId) {
                $result['SekolahRelatedBySekolahId'] = $this->aSekolahRelatedBySekolahId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SasaranSurveyPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setPenggunaId($value);
                break;
            case 1:
                $this->setSekolahId($value);
                break;
            case 2:
                $this->setNomorUrut($value);
                break;
            case 3:
                $this->setTanggalRencana($value);
                break;
            case 4:
                $this->setTanggalPelaksanaan($value);
                break;
            case 5:
                $this->setWaktuBerangkat($value);
                break;
            case 6:
                $this->setWaktuSampai($value);
                break;
            case 7:
                $this->setWaktuMulaiSurvey($value);
                break;
            case 8:
                $this->setWaktuSelesai($value);
                break;
            case 9:
                $this->setWaktuIsiFormCetak($value);
                break;
            case 10:
                $this->setWaktuIsiFormElektronik($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = SasaranSurveyPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setPenggunaId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setSekolahId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setNomorUrut($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setTanggalRencana($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setTanggalPelaksanaan($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setWaktuBerangkat($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setWaktuSampai($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setWaktuMulaiSurvey($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setWaktuSelesai($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setWaktuIsiFormCetak($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setWaktuIsiFormElektronik($arr[$keys[10]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(SasaranSurveyPeer::DATABASE_NAME);

        if ($this->isColumnModified(SasaranSurveyPeer::PENGGUNA_ID)) $criteria->add(SasaranSurveyPeer::PENGGUNA_ID, $this->pengguna_id);
        if ($this->isColumnModified(SasaranSurveyPeer::SEKOLAH_ID)) $criteria->add(SasaranSurveyPeer::SEKOLAH_ID, $this->sekolah_id);
        if ($this->isColumnModified(SasaranSurveyPeer::NOMOR_URUT)) $criteria->add(SasaranSurveyPeer::NOMOR_URUT, $this->nomor_urut);
        if ($this->isColumnModified(SasaranSurveyPeer::TANGGAL_RENCANA)) $criteria->add(SasaranSurveyPeer::TANGGAL_RENCANA, $this->tanggal_rencana);
        if ($this->isColumnModified(SasaranSurveyPeer::TANGGAL_PELAKSANAAN)) $criteria->add(SasaranSurveyPeer::TANGGAL_PELAKSANAAN, $this->tanggal_pelaksanaan);
        if ($this->isColumnModified(SasaranSurveyPeer::WAKTU_BERANGKAT)) $criteria->add(SasaranSurveyPeer::WAKTU_BERANGKAT, $this->waktu_berangkat);
        if ($this->isColumnModified(SasaranSurveyPeer::WAKTU_SAMPAI)) $criteria->add(SasaranSurveyPeer::WAKTU_SAMPAI, $this->waktu_sampai);
        if ($this->isColumnModified(SasaranSurveyPeer::WAKTU_MULAI_SURVEY)) $criteria->add(SasaranSurveyPeer::WAKTU_MULAI_SURVEY, $this->waktu_mulai_survey);
        if ($this->isColumnModified(SasaranSurveyPeer::WAKTU_SELESAI)) $criteria->add(SasaranSurveyPeer::WAKTU_SELESAI, $this->waktu_selesai);
        if ($this->isColumnModified(SasaranSurveyPeer::WAKTU_ISI_FORM_CETAK)) $criteria->add(SasaranSurveyPeer::WAKTU_ISI_FORM_CETAK, $this->waktu_isi_form_cetak);
        if ($this->isColumnModified(SasaranSurveyPeer::WAKTU_ISI_FORM_ELEKTRONIK)) $criteria->add(SasaranSurveyPeer::WAKTU_ISI_FORM_ELEKTRONIK, $this->waktu_isi_form_elektronik);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(SasaranSurveyPeer::DATABASE_NAME);
        $criteria->add(SasaranSurveyPeer::PENGGUNA_ID, $this->pengguna_id);
        $criteria->add(SasaranSurveyPeer::SEKOLAH_ID, $this->sekolah_id);

        return $criteria;
    }

    /**
     * Returns the composite primary key for this object.
     * The array elements will be in same order as specified in XML.
     * @return array
     */
    public function getPrimaryKey()
    {
        $pks = array();
        $pks[0] = $this->getPenggunaId();
        $pks[1] = $this->getSekolahId();

        return $pks;
    }

    /**
     * Set the [composite] primary key.
     *
     * @param array $keys The elements of the composite key (order must match the order in XML file).
     * @return void
     */
    public function setPrimaryKey($keys)
    {
        $this->setPenggunaId($keys[0]);
        $this->setSekolahId($keys[1]);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return (null === $this->getPenggunaId()) && (null === $this->getSekolahId());
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of SasaranSurvey (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setPenggunaId($this->getPenggunaId());
        $copyObj->setSekolahId($this->getSekolahId());
        $copyObj->setNomorUrut($this->getNomorUrut());
        $copyObj->setTanggalRencana($this->getTanggalRencana());
        $copyObj->setTanggalPelaksanaan($this->getTanggalPelaksanaan());
        $copyObj->setWaktuBerangkat($this->getWaktuBerangkat());
        $copyObj->setWaktuSampai($this->getWaktuSampai());
        $copyObj->setWaktuMulaiSurvey($this->getWaktuMulaiSurvey());
        $copyObj->setWaktuSelesai($this->getWaktuSelesai());
        $copyObj->setWaktuIsiFormCetak($this->getWaktuIsiFormCetak());
        $copyObj->setWaktuIsiFormElektronik($this->getWaktuIsiFormElektronik());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return SasaranSurvey Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return SasaranSurveyPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new SasaranSurveyPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a Pengguna object.
     *
     * @param             Pengguna $v
     * @return SasaranSurvey The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPenggunaRelatedByPenggunaId(Pengguna $v = null)
    {
        if ($v === null) {
            $this->setPenggunaId(NULL);
        } else {
            $this->setPenggunaId($v->getPenggunaId());
        }

        $this->aPenggunaRelatedByPenggunaId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Pengguna object, it will not be re-added.
        if ($v !== null) {
            $v->addSasaranSurveyRelatedByPenggunaId($this);
        }


        return $this;
    }


    /**
     * Get the associated Pengguna object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Pengguna The associated Pengguna object.
     * @throws PropelException
     */
    public function getPenggunaRelatedByPenggunaId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPenggunaRelatedByPenggunaId === null && (($this->pengguna_id !== "" && $this->pengguna_id !== null)) && $doQuery) {
            $this->aPenggunaRelatedByPenggunaId = PenggunaQuery::create()->findPk($this->pengguna_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPenggunaRelatedByPenggunaId->addSasaranSurveysRelatedByPenggunaId($this);
             */
        }

        return $this->aPenggunaRelatedByPenggunaId;
    }

    /**
     * Declares an association between this object and a Pengguna object.
     *
     * @param             Pengguna $v
     * @return SasaranSurvey The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPenggunaRelatedByPenggunaId(Pengguna $v = null)
    {
        if ($v === null) {
            $this->setPenggunaId(NULL);
        } else {
            $this->setPenggunaId($v->getPenggunaId());
        }

        $this->aPenggunaRelatedByPenggunaId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Pengguna object, it will not be re-added.
        if ($v !== null) {
            $v->addSasaranSurveyRelatedByPenggunaId($this);
        }


        return $this;
    }


    /**
     * Get the associated Pengguna object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Pengguna The associated Pengguna object.
     * @throws PropelException
     */
    public function getPenggunaRelatedByPenggunaId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPenggunaRelatedByPenggunaId === null && (($this->pengguna_id !== "" && $this->pengguna_id !== null)) && $doQuery) {
            $this->aPenggunaRelatedByPenggunaId = PenggunaQuery::create()->findPk($this->pengguna_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPenggunaRelatedByPenggunaId->addSasaranSurveysRelatedByPenggunaId($this);
             */
        }

        return $this->aPenggunaRelatedByPenggunaId;
    }

    /**
     * Declares an association between this object and a Sekolah object.
     *
     * @param             Sekolah $v
     * @return SasaranSurvey The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSekolahRelatedBySekolahId(Sekolah $v = null)
    {
        if ($v === null) {
            $this->setSekolahId(NULL);
        } else {
            $this->setSekolahId($v->getSekolahId());
        }

        $this->aSekolahRelatedBySekolahId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Sekolah object, it will not be re-added.
        if ($v !== null) {
            $v->addSasaranSurveyRelatedBySekolahId($this);
        }


        return $this;
    }


    /**
     * Get the associated Sekolah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Sekolah The associated Sekolah object.
     * @throws PropelException
     */
    public function getSekolahRelatedBySekolahId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSekolahRelatedBySekolahId === null && (($this->sekolah_id !== "" && $this->sekolah_id !== null)) && $doQuery) {
            $this->aSekolahRelatedBySekolahId = SekolahQuery::create()->findPk($this->sekolah_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSekolahRelatedBySekolahId->addSasaranSurveysRelatedBySekolahId($this);
             */
        }

        return $this->aSekolahRelatedBySekolahId;
    }

    /**
     * Declares an association between this object and a Sekolah object.
     *
     * @param             Sekolah $v
     * @return SasaranSurvey The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSekolahRelatedBySekolahId(Sekolah $v = null)
    {
        if ($v === null) {
            $this->setSekolahId(NULL);
        } else {
            $this->setSekolahId($v->getSekolahId());
        }

        $this->aSekolahRelatedBySekolahId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Sekolah object, it will not be re-added.
        if ($v !== null) {
            $v->addSasaranSurveyRelatedBySekolahId($this);
        }


        return $this;
    }


    /**
     * Get the associated Sekolah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Sekolah The associated Sekolah object.
     * @throws PropelException
     */
    public function getSekolahRelatedBySekolahId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSekolahRelatedBySekolahId === null && (($this->sekolah_id !== "" && $this->sekolah_id !== null)) && $doQuery) {
            $this->aSekolahRelatedBySekolahId = SekolahQuery::create()->findPk($this->sekolah_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSekolahRelatedBySekolahId->addSasaranSurveysRelatedBySekolahId($this);
             */
        }

        return $this->aSekolahRelatedBySekolahId;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->pengguna_id = null;
        $this->sekolah_id = null;
        $this->nomor_urut = null;
        $this->tanggal_rencana = null;
        $this->tanggal_pelaksanaan = null;
        $this->waktu_berangkat = null;
        $this->waktu_sampai = null;
        $this->waktu_mulai_survey = null;
        $this->waktu_selesai = null;
        $this->waktu_isi_form_cetak = null;
        $this->waktu_isi_form_elektronik = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->aPenggunaRelatedByPenggunaId instanceof Persistent) {
              $this->aPenggunaRelatedByPenggunaId->clearAllReferences($deep);
            }
            if ($this->aPenggunaRelatedByPenggunaId instanceof Persistent) {
              $this->aPenggunaRelatedByPenggunaId->clearAllReferences($deep);
            }
            if ($this->aSekolahRelatedBySekolahId instanceof Persistent) {
              $this->aSekolahRelatedBySekolahId->clearAllReferences($deep);
            }
            if ($this->aSekolahRelatedBySekolahId instanceof Persistent) {
              $this->aSekolahRelatedBySekolahId->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        $this->aPenggunaRelatedByPenggunaId = null;
        $this->aPenggunaRelatedByPenggunaId = null;
        $this->aSekolahRelatedBySekolahId = null;
        $this->aSekolahRelatedBySekolahId = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(SasaranSurveyPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
