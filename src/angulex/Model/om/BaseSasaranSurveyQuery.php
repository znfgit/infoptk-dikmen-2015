<?php

namespace angulex\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\Pengguna;
use angulex\Model\SasaranSurvey;
use angulex\Model\SasaranSurveyPeer;
use angulex\Model\SasaranSurveyQuery;
use angulex\Model\Sekolah;

/**
 * Base class that represents a query for the 'sasaran_survey' table.
 *
 * 
 *
 * @method SasaranSurveyQuery orderByPenggunaId($order = Criteria::ASC) Order by the pengguna_id column
 * @method SasaranSurveyQuery orderBySekolahId($order = Criteria::ASC) Order by the sekolah_id column
 * @method SasaranSurveyQuery orderByNomorUrut($order = Criteria::ASC) Order by the nomor_urut column
 * @method SasaranSurveyQuery orderByTanggalRencana($order = Criteria::ASC) Order by the tanggal_rencana column
 * @method SasaranSurveyQuery orderByTanggalPelaksanaan($order = Criteria::ASC) Order by the tanggal_pelaksanaan column
 * @method SasaranSurveyQuery orderByWaktuBerangkat($order = Criteria::ASC) Order by the waktu_berangkat column
 * @method SasaranSurveyQuery orderByWaktuSampai($order = Criteria::ASC) Order by the waktu_sampai column
 * @method SasaranSurveyQuery orderByWaktuMulaiSurvey($order = Criteria::ASC) Order by the waktu_mulai_survey column
 * @method SasaranSurveyQuery orderByWaktuSelesai($order = Criteria::ASC) Order by the waktu_selesai column
 * @method SasaranSurveyQuery orderByWaktuIsiFormCetak($order = Criteria::ASC) Order by the waktu_isi_form_cetak column
 * @method SasaranSurveyQuery orderByWaktuIsiFormElektronik($order = Criteria::ASC) Order by the waktu_isi_form_elektronik column
 *
 * @method SasaranSurveyQuery groupByPenggunaId() Group by the pengguna_id column
 * @method SasaranSurveyQuery groupBySekolahId() Group by the sekolah_id column
 * @method SasaranSurveyQuery groupByNomorUrut() Group by the nomor_urut column
 * @method SasaranSurveyQuery groupByTanggalRencana() Group by the tanggal_rencana column
 * @method SasaranSurveyQuery groupByTanggalPelaksanaan() Group by the tanggal_pelaksanaan column
 * @method SasaranSurveyQuery groupByWaktuBerangkat() Group by the waktu_berangkat column
 * @method SasaranSurveyQuery groupByWaktuSampai() Group by the waktu_sampai column
 * @method SasaranSurveyQuery groupByWaktuMulaiSurvey() Group by the waktu_mulai_survey column
 * @method SasaranSurveyQuery groupByWaktuSelesai() Group by the waktu_selesai column
 * @method SasaranSurveyQuery groupByWaktuIsiFormCetak() Group by the waktu_isi_form_cetak column
 * @method SasaranSurveyQuery groupByWaktuIsiFormElektronik() Group by the waktu_isi_form_elektronik column
 *
 * @method SasaranSurveyQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SasaranSurveyQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SasaranSurveyQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method SasaranSurveyQuery leftJoinPenggunaRelatedByPenggunaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PenggunaRelatedByPenggunaId relation
 * @method SasaranSurveyQuery rightJoinPenggunaRelatedByPenggunaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PenggunaRelatedByPenggunaId relation
 * @method SasaranSurveyQuery innerJoinPenggunaRelatedByPenggunaId($relationAlias = null) Adds a INNER JOIN clause to the query using the PenggunaRelatedByPenggunaId relation
 *
 * @method SasaranSurveyQuery leftJoinPenggunaRelatedByPenggunaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PenggunaRelatedByPenggunaId relation
 * @method SasaranSurveyQuery rightJoinPenggunaRelatedByPenggunaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PenggunaRelatedByPenggunaId relation
 * @method SasaranSurveyQuery innerJoinPenggunaRelatedByPenggunaId($relationAlias = null) Adds a INNER JOIN clause to the query using the PenggunaRelatedByPenggunaId relation
 *
 * @method SasaranSurveyQuery leftJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method SasaranSurveyQuery rightJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method SasaranSurveyQuery innerJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the SekolahRelatedBySekolahId relation
 *
 * @method SasaranSurveyQuery leftJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method SasaranSurveyQuery rightJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method SasaranSurveyQuery innerJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the SekolahRelatedBySekolahId relation
 *
 * @method SasaranSurvey findOne(PropelPDO $con = null) Return the first SasaranSurvey matching the query
 * @method SasaranSurvey findOneOrCreate(PropelPDO $con = null) Return the first SasaranSurvey matching the query, or a new SasaranSurvey object populated from the query conditions when no match is found
 *
 * @method SasaranSurvey findOneByPenggunaId(string $pengguna_id) Return the first SasaranSurvey filtered by the pengguna_id column
 * @method SasaranSurvey findOneBySekolahId(string $sekolah_id) Return the first SasaranSurvey filtered by the sekolah_id column
 * @method SasaranSurvey findOneByNomorUrut(int $nomor_urut) Return the first SasaranSurvey filtered by the nomor_urut column
 * @method SasaranSurvey findOneByTanggalRencana(string $tanggal_rencana) Return the first SasaranSurvey filtered by the tanggal_rencana column
 * @method SasaranSurvey findOneByTanggalPelaksanaan(string $tanggal_pelaksanaan) Return the first SasaranSurvey filtered by the tanggal_pelaksanaan column
 * @method SasaranSurvey findOneByWaktuBerangkat(string $waktu_berangkat) Return the first SasaranSurvey filtered by the waktu_berangkat column
 * @method SasaranSurvey findOneByWaktuSampai(string $waktu_sampai) Return the first SasaranSurvey filtered by the waktu_sampai column
 * @method SasaranSurvey findOneByWaktuMulaiSurvey(string $waktu_mulai_survey) Return the first SasaranSurvey filtered by the waktu_mulai_survey column
 * @method SasaranSurvey findOneByWaktuSelesai(string $waktu_selesai) Return the first SasaranSurvey filtered by the waktu_selesai column
 * @method SasaranSurvey findOneByWaktuIsiFormCetak(int $waktu_isi_form_cetak) Return the first SasaranSurvey filtered by the waktu_isi_form_cetak column
 * @method SasaranSurvey findOneByWaktuIsiFormElektronik(int $waktu_isi_form_elektronik) Return the first SasaranSurvey filtered by the waktu_isi_form_elektronik column
 *
 * @method array findByPenggunaId(string $pengguna_id) Return SasaranSurvey objects filtered by the pengguna_id column
 * @method array findBySekolahId(string $sekolah_id) Return SasaranSurvey objects filtered by the sekolah_id column
 * @method array findByNomorUrut(int $nomor_urut) Return SasaranSurvey objects filtered by the nomor_urut column
 * @method array findByTanggalRencana(string $tanggal_rencana) Return SasaranSurvey objects filtered by the tanggal_rencana column
 * @method array findByTanggalPelaksanaan(string $tanggal_pelaksanaan) Return SasaranSurvey objects filtered by the tanggal_pelaksanaan column
 * @method array findByWaktuBerangkat(string $waktu_berangkat) Return SasaranSurvey objects filtered by the waktu_berangkat column
 * @method array findByWaktuSampai(string $waktu_sampai) Return SasaranSurvey objects filtered by the waktu_sampai column
 * @method array findByWaktuMulaiSurvey(string $waktu_mulai_survey) Return SasaranSurvey objects filtered by the waktu_mulai_survey column
 * @method array findByWaktuSelesai(string $waktu_selesai) Return SasaranSurvey objects filtered by the waktu_selesai column
 * @method array findByWaktuIsiFormCetak(int $waktu_isi_form_cetak) Return SasaranSurvey objects filtered by the waktu_isi_form_cetak column
 * @method array findByWaktuIsiFormElektronik(int $waktu_isi_form_elektronik) Return SasaranSurvey objects filtered by the waktu_isi_form_elektronik column
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseSasaranSurveyQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSasaranSurveyQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Dapodikmen', $modelName = 'angulex\\Model\\SasaranSurvey', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SasaranSurveyQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   SasaranSurveyQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SasaranSurveyQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SasaranSurveyQuery) {
            return $criteria;
        }
        $query = new SasaranSurveyQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34), $con);
     * </code>
     *
     * @param array $key Primary key to use for the query 
                         A Primary key composition: [$pengguna_id, $sekolah_id]
     * @param     PropelPDO $con an optional connection object
     *
     * @return   SasaranSurvey|SasaranSurvey[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SasaranSurveyPeer::getInstanceFromPool(serialize(array((string) $key[0], (string) $key[1]))))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SasaranSurveyPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 SasaranSurvey A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [pengguna_id], [sekolah_id], [nomor_urut], [tanggal_rencana], [tanggal_pelaksanaan], [waktu_berangkat], [waktu_sampai], [waktu_mulai_survey], [waktu_selesai], [waktu_isi_form_cetak], [waktu_isi_form_elektronik] FROM [sasaran_survey] WHERE [pengguna_id] = :p0 AND [sekolah_id] = :p1';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_STR);			
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new SasaranSurvey();
            $obj->hydrate($row);
            SasaranSurveyPeer::addInstanceToPool($obj, serialize(array((string) $key[0], (string) $key[1])));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return SasaranSurvey|SasaranSurvey[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|SasaranSurvey[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SasaranSurveyQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(SasaranSurveyPeer::PENGGUNA_ID, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(SasaranSurveyPeer::SEKOLAH_ID, $key[1], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SasaranSurveyQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(SasaranSurveyPeer::PENGGUNA_ID, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(SasaranSurveyPeer::SEKOLAH_ID, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the pengguna_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPenggunaId('fooValue');   // WHERE pengguna_id = 'fooValue'
     * $query->filterByPenggunaId('%fooValue%'); // WHERE pengguna_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $penggunaId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SasaranSurveyQuery The current query, for fluid interface
     */
    public function filterByPenggunaId($penggunaId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($penggunaId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $penggunaId)) {
                $penggunaId = str_replace('*', '%', $penggunaId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SasaranSurveyPeer::PENGGUNA_ID, $penggunaId, $comparison);
    }

    /**
     * Filter the query on the sekolah_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySekolahId('fooValue');   // WHERE sekolah_id = 'fooValue'
     * $query->filterBySekolahId('%fooValue%'); // WHERE sekolah_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sekolahId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SasaranSurveyQuery The current query, for fluid interface
     */
    public function filterBySekolahId($sekolahId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sekolahId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $sekolahId)) {
                $sekolahId = str_replace('*', '%', $sekolahId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SasaranSurveyPeer::SEKOLAH_ID, $sekolahId, $comparison);
    }

    /**
     * Filter the query on the nomor_urut column
     *
     * Example usage:
     * <code>
     * $query->filterByNomorUrut(1234); // WHERE nomor_urut = 1234
     * $query->filterByNomorUrut(array(12, 34)); // WHERE nomor_urut IN (12, 34)
     * $query->filterByNomorUrut(array('min' => 12)); // WHERE nomor_urut >= 12
     * $query->filterByNomorUrut(array('max' => 12)); // WHERE nomor_urut <= 12
     * </code>
     *
     * @param     mixed $nomorUrut The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SasaranSurveyQuery The current query, for fluid interface
     */
    public function filterByNomorUrut($nomorUrut = null, $comparison = null)
    {
        if (is_array($nomorUrut)) {
            $useMinMax = false;
            if (isset($nomorUrut['min'])) {
                $this->addUsingAlias(SasaranSurveyPeer::NOMOR_URUT, $nomorUrut['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($nomorUrut['max'])) {
                $this->addUsingAlias(SasaranSurveyPeer::NOMOR_URUT, $nomorUrut['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SasaranSurveyPeer::NOMOR_URUT, $nomorUrut, $comparison);
    }

    /**
     * Filter the query on the tanggal_rencana column
     *
     * Example usage:
     * <code>
     * $query->filterByTanggalRencana('fooValue');   // WHERE tanggal_rencana = 'fooValue'
     * $query->filterByTanggalRencana('%fooValue%'); // WHERE tanggal_rencana LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tanggalRencana The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SasaranSurveyQuery The current query, for fluid interface
     */
    public function filterByTanggalRencana($tanggalRencana = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tanggalRencana)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tanggalRencana)) {
                $tanggalRencana = str_replace('*', '%', $tanggalRencana);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SasaranSurveyPeer::TANGGAL_RENCANA, $tanggalRencana, $comparison);
    }

    /**
     * Filter the query on the tanggal_pelaksanaan column
     *
     * Example usage:
     * <code>
     * $query->filterByTanggalPelaksanaan('fooValue');   // WHERE tanggal_pelaksanaan = 'fooValue'
     * $query->filterByTanggalPelaksanaan('%fooValue%'); // WHERE tanggal_pelaksanaan LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tanggalPelaksanaan The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SasaranSurveyQuery The current query, for fluid interface
     */
    public function filterByTanggalPelaksanaan($tanggalPelaksanaan = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tanggalPelaksanaan)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tanggalPelaksanaan)) {
                $tanggalPelaksanaan = str_replace('*', '%', $tanggalPelaksanaan);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SasaranSurveyPeer::TANGGAL_PELAKSANAAN, $tanggalPelaksanaan, $comparison);
    }

    /**
     * Filter the query on the waktu_berangkat column
     *
     * Example usage:
     * <code>
     * $query->filterByWaktuBerangkat('2011-03-14'); // WHERE waktu_berangkat = '2011-03-14'
     * $query->filterByWaktuBerangkat('now'); // WHERE waktu_berangkat = '2011-03-14'
     * $query->filterByWaktuBerangkat(array('max' => 'yesterday')); // WHERE waktu_berangkat > '2011-03-13'
     * </code>
     *
     * @param     mixed $waktuBerangkat The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SasaranSurveyQuery The current query, for fluid interface
     */
    public function filterByWaktuBerangkat($waktuBerangkat = null, $comparison = null)
    {
        if (is_array($waktuBerangkat)) {
            $useMinMax = false;
            if (isset($waktuBerangkat['min'])) {
                $this->addUsingAlias(SasaranSurveyPeer::WAKTU_BERANGKAT, $waktuBerangkat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($waktuBerangkat['max'])) {
                $this->addUsingAlias(SasaranSurveyPeer::WAKTU_BERANGKAT, $waktuBerangkat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SasaranSurveyPeer::WAKTU_BERANGKAT, $waktuBerangkat, $comparison);
    }

    /**
     * Filter the query on the waktu_sampai column
     *
     * Example usage:
     * <code>
     * $query->filterByWaktuSampai('2011-03-14'); // WHERE waktu_sampai = '2011-03-14'
     * $query->filterByWaktuSampai('now'); // WHERE waktu_sampai = '2011-03-14'
     * $query->filterByWaktuSampai(array('max' => 'yesterday')); // WHERE waktu_sampai > '2011-03-13'
     * </code>
     *
     * @param     mixed $waktuSampai The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SasaranSurveyQuery The current query, for fluid interface
     */
    public function filterByWaktuSampai($waktuSampai = null, $comparison = null)
    {
        if (is_array($waktuSampai)) {
            $useMinMax = false;
            if (isset($waktuSampai['min'])) {
                $this->addUsingAlias(SasaranSurveyPeer::WAKTU_SAMPAI, $waktuSampai['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($waktuSampai['max'])) {
                $this->addUsingAlias(SasaranSurveyPeer::WAKTU_SAMPAI, $waktuSampai['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SasaranSurveyPeer::WAKTU_SAMPAI, $waktuSampai, $comparison);
    }

    /**
     * Filter the query on the waktu_mulai_survey column
     *
     * Example usage:
     * <code>
     * $query->filterByWaktuMulaiSurvey('2011-03-14'); // WHERE waktu_mulai_survey = '2011-03-14'
     * $query->filterByWaktuMulaiSurvey('now'); // WHERE waktu_mulai_survey = '2011-03-14'
     * $query->filterByWaktuMulaiSurvey(array('max' => 'yesterday')); // WHERE waktu_mulai_survey > '2011-03-13'
     * </code>
     *
     * @param     mixed $waktuMulaiSurvey The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SasaranSurveyQuery The current query, for fluid interface
     */
    public function filterByWaktuMulaiSurvey($waktuMulaiSurvey = null, $comparison = null)
    {
        if (is_array($waktuMulaiSurvey)) {
            $useMinMax = false;
            if (isset($waktuMulaiSurvey['min'])) {
                $this->addUsingAlias(SasaranSurveyPeer::WAKTU_MULAI_SURVEY, $waktuMulaiSurvey['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($waktuMulaiSurvey['max'])) {
                $this->addUsingAlias(SasaranSurveyPeer::WAKTU_MULAI_SURVEY, $waktuMulaiSurvey['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SasaranSurveyPeer::WAKTU_MULAI_SURVEY, $waktuMulaiSurvey, $comparison);
    }

    /**
     * Filter the query on the waktu_selesai column
     *
     * Example usage:
     * <code>
     * $query->filterByWaktuSelesai('2011-03-14'); // WHERE waktu_selesai = '2011-03-14'
     * $query->filterByWaktuSelesai('now'); // WHERE waktu_selesai = '2011-03-14'
     * $query->filterByWaktuSelesai(array('max' => 'yesterday')); // WHERE waktu_selesai > '2011-03-13'
     * </code>
     *
     * @param     mixed $waktuSelesai The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SasaranSurveyQuery The current query, for fluid interface
     */
    public function filterByWaktuSelesai($waktuSelesai = null, $comparison = null)
    {
        if (is_array($waktuSelesai)) {
            $useMinMax = false;
            if (isset($waktuSelesai['min'])) {
                $this->addUsingAlias(SasaranSurveyPeer::WAKTU_SELESAI, $waktuSelesai['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($waktuSelesai['max'])) {
                $this->addUsingAlias(SasaranSurveyPeer::WAKTU_SELESAI, $waktuSelesai['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SasaranSurveyPeer::WAKTU_SELESAI, $waktuSelesai, $comparison);
    }

    /**
     * Filter the query on the waktu_isi_form_cetak column
     *
     * Example usage:
     * <code>
     * $query->filterByWaktuIsiFormCetak(1234); // WHERE waktu_isi_form_cetak = 1234
     * $query->filterByWaktuIsiFormCetak(array(12, 34)); // WHERE waktu_isi_form_cetak IN (12, 34)
     * $query->filterByWaktuIsiFormCetak(array('min' => 12)); // WHERE waktu_isi_form_cetak >= 12
     * $query->filterByWaktuIsiFormCetak(array('max' => 12)); // WHERE waktu_isi_form_cetak <= 12
     * </code>
     *
     * @param     mixed $waktuIsiFormCetak The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SasaranSurveyQuery The current query, for fluid interface
     */
    public function filterByWaktuIsiFormCetak($waktuIsiFormCetak = null, $comparison = null)
    {
        if (is_array($waktuIsiFormCetak)) {
            $useMinMax = false;
            if (isset($waktuIsiFormCetak['min'])) {
                $this->addUsingAlias(SasaranSurveyPeer::WAKTU_ISI_FORM_CETAK, $waktuIsiFormCetak['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($waktuIsiFormCetak['max'])) {
                $this->addUsingAlias(SasaranSurveyPeer::WAKTU_ISI_FORM_CETAK, $waktuIsiFormCetak['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SasaranSurveyPeer::WAKTU_ISI_FORM_CETAK, $waktuIsiFormCetak, $comparison);
    }

    /**
     * Filter the query on the waktu_isi_form_elektronik column
     *
     * Example usage:
     * <code>
     * $query->filterByWaktuIsiFormElektronik(1234); // WHERE waktu_isi_form_elektronik = 1234
     * $query->filterByWaktuIsiFormElektronik(array(12, 34)); // WHERE waktu_isi_form_elektronik IN (12, 34)
     * $query->filterByWaktuIsiFormElektronik(array('min' => 12)); // WHERE waktu_isi_form_elektronik >= 12
     * $query->filterByWaktuIsiFormElektronik(array('max' => 12)); // WHERE waktu_isi_form_elektronik <= 12
     * </code>
     *
     * @param     mixed $waktuIsiFormElektronik The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SasaranSurveyQuery The current query, for fluid interface
     */
    public function filterByWaktuIsiFormElektronik($waktuIsiFormElektronik = null, $comparison = null)
    {
        if (is_array($waktuIsiFormElektronik)) {
            $useMinMax = false;
            if (isset($waktuIsiFormElektronik['min'])) {
                $this->addUsingAlias(SasaranSurveyPeer::WAKTU_ISI_FORM_ELEKTRONIK, $waktuIsiFormElektronik['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($waktuIsiFormElektronik['max'])) {
                $this->addUsingAlias(SasaranSurveyPeer::WAKTU_ISI_FORM_ELEKTRONIK, $waktuIsiFormElektronik['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SasaranSurveyPeer::WAKTU_ISI_FORM_ELEKTRONIK, $waktuIsiFormElektronik, $comparison);
    }

    /**
     * Filter the query by a related Pengguna object
     *
     * @param   Pengguna|PropelObjectCollection $pengguna The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SasaranSurveyQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPenggunaRelatedByPenggunaId($pengguna, $comparison = null)
    {
        if ($pengguna instanceof Pengguna) {
            return $this
                ->addUsingAlias(SasaranSurveyPeer::PENGGUNA_ID, $pengguna->getPenggunaId(), $comparison);
        } elseif ($pengguna instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SasaranSurveyPeer::PENGGUNA_ID, $pengguna->toKeyValue('PrimaryKey', 'PenggunaId'), $comparison);
        } else {
            throw new PropelException('filterByPenggunaRelatedByPenggunaId() only accepts arguments of type Pengguna or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PenggunaRelatedByPenggunaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SasaranSurveyQuery The current query, for fluid interface
     */
    public function joinPenggunaRelatedByPenggunaId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PenggunaRelatedByPenggunaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PenggunaRelatedByPenggunaId');
        }

        return $this;
    }

    /**
     * Use the PenggunaRelatedByPenggunaId relation Pengguna object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PenggunaQuery A secondary query class using the current class as primary query
     */
    public function usePenggunaRelatedByPenggunaIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPenggunaRelatedByPenggunaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PenggunaRelatedByPenggunaId', '\angulex\Model\PenggunaQuery');
    }

    /**
     * Filter the query by a related Pengguna object
     *
     * @param   Pengguna|PropelObjectCollection $pengguna The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SasaranSurveyQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPenggunaRelatedByPenggunaId($pengguna, $comparison = null)
    {
        if ($pengguna instanceof Pengguna) {
            return $this
                ->addUsingAlias(SasaranSurveyPeer::PENGGUNA_ID, $pengguna->getPenggunaId(), $comparison);
        } elseif ($pengguna instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SasaranSurveyPeer::PENGGUNA_ID, $pengguna->toKeyValue('PrimaryKey', 'PenggunaId'), $comparison);
        } else {
            throw new PropelException('filterByPenggunaRelatedByPenggunaId() only accepts arguments of type Pengguna or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PenggunaRelatedByPenggunaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SasaranSurveyQuery The current query, for fluid interface
     */
    public function joinPenggunaRelatedByPenggunaId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PenggunaRelatedByPenggunaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PenggunaRelatedByPenggunaId');
        }

        return $this;
    }

    /**
     * Use the PenggunaRelatedByPenggunaId relation Pengguna object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PenggunaQuery A secondary query class using the current class as primary query
     */
    public function usePenggunaRelatedByPenggunaIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPenggunaRelatedByPenggunaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PenggunaRelatedByPenggunaId', '\angulex\Model\PenggunaQuery');
    }

    /**
     * Filter the query by a related Sekolah object
     *
     * @param   Sekolah|PropelObjectCollection $sekolah The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SasaranSurveyQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySekolahRelatedBySekolahId($sekolah, $comparison = null)
    {
        if ($sekolah instanceof Sekolah) {
            return $this
                ->addUsingAlias(SasaranSurveyPeer::SEKOLAH_ID, $sekolah->getSekolahId(), $comparison);
        } elseif ($sekolah instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SasaranSurveyPeer::SEKOLAH_ID, $sekolah->toKeyValue('PrimaryKey', 'SekolahId'), $comparison);
        } else {
            throw new PropelException('filterBySekolahRelatedBySekolahId() only accepts arguments of type Sekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SekolahRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SasaranSurveyQuery The current query, for fluid interface
     */
    public function joinSekolahRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SekolahRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SekolahRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the SekolahRelatedBySekolahId relation Sekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SekolahQuery A secondary query class using the current class as primary query
     */
    public function useSekolahRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSekolahRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SekolahRelatedBySekolahId', '\angulex\Model\SekolahQuery');
    }

    /**
     * Filter the query by a related Sekolah object
     *
     * @param   Sekolah|PropelObjectCollection $sekolah The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SasaranSurveyQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySekolahRelatedBySekolahId($sekolah, $comparison = null)
    {
        if ($sekolah instanceof Sekolah) {
            return $this
                ->addUsingAlias(SasaranSurveyPeer::SEKOLAH_ID, $sekolah->getSekolahId(), $comparison);
        } elseif ($sekolah instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SasaranSurveyPeer::SEKOLAH_ID, $sekolah->toKeyValue('PrimaryKey', 'SekolahId'), $comparison);
        } else {
            throw new PropelException('filterBySekolahRelatedBySekolahId() only accepts arguments of type Sekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SekolahRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SasaranSurveyQuery The current query, for fluid interface
     */
    public function joinSekolahRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SekolahRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SekolahRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the SekolahRelatedBySekolahId relation Sekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SekolahQuery A secondary query class using the current class as primary query
     */
    public function useSekolahRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSekolahRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SekolahRelatedBySekolahId', '\angulex\Model\SekolahQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   SasaranSurvey $sasaranSurvey Object to remove from the list of results
     *
     * @return SasaranSurveyQuery The current query, for fluid interface
     */
    public function prune($sasaranSurvey = null)
    {
        if ($sasaranSurvey) {
            $this->addCond('pruneCond0', $this->getAliasedColName(SasaranSurveyPeer::PENGGUNA_ID), $sasaranSurvey->getPenggunaId(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(SasaranSurveyPeer::SEKOLAH_ID), $sasaranSurvey->getSekolahId(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

}
