<?php

namespace angulex\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\PangkatGolongan;
use angulex\Model\Ptk;
use angulex\Model\RwyKepangkatan;
use angulex\Model\RwyKepangkatanPeer;
use angulex\Model\RwyKepangkatanQuery;
use angulex\Model\VldRwyKepangkatan;

/**
 * Base class that represents a query for the 'rwy_kepangkatan' table.
 *
 * 
 *
 * @method RwyKepangkatanQuery orderByRiwayatKepangkatanId($order = Criteria::ASC) Order by the riwayat_kepangkatan_id column
 * @method RwyKepangkatanQuery orderByPtkId($order = Criteria::ASC) Order by the ptk_id column
 * @method RwyKepangkatanQuery orderByPangkatGolonganId($order = Criteria::ASC) Order by the pangkat_golongan_id column
 * @method RwyKepangkatanQuery orderByNomorSk($order = Criteria::ASC) Order by the nomor_sk column
 * @method RwyKepangkatanQuery orderByTanggalSk($order = Criteria::ASC) Order by the tanggal_sk column
 * @method RwyKepangkatanQuery orderByTmtPangkat($order = Criteria::ASC) Order by the tmt_pangkat column
 * @method RwyKepangkatanQuery orderByMasaKerjaGolTahun($order = Criteria::ASC) Order by the masa_kerja_gol_tahun column
 * @method RwyKepangkatanQuery orderByMasaKerjaGolBulan($order = Criteria::ASC) Order by the masa_kerja_gol_bulan column
 * @method RwyKepangkatanQuery orderByLastUpdate($order = Criteria::ASC) Order by the Last_update column
 * @method RwyKepangkatanQuery orderBySoftDelete($order = Criteria::ASC) Order by the Soft_delete column
 * @method RwyKepangkatanQuery orderByLastSync($order = Criteria::ASC) Order by the last_sync column
 * @method RwyKepangkatanQuery orderByUpdaterId($order = Criteria::ASC) Order by the Updater_ID column
 *
 * @method RwyKepangkatanQuery groupByRiwayatKepangkatanId() Group by the riwayat_kepangkatan_id column
 * @method RwyKepangkatanQuery groupByPtkId() Group by the ptk_id column
 * @method RwyKepangkatanQuery groupByPangkatGolonganId() Group by the pangkat_golongan_id column
 * @method RwyKepangkatanQuery groupByNomorSk() Group by the nomor_sk column
 * @method RwyKepangkatanQuery groupByTanggalSk() Group by the tanggal_sk column
 * @method RwyKepangkatanQuery groupByTmtPangkat() Group by the tmt_pangkat column
 * @method RwyKepangkatanQuery groupByMasaKerjaGolTahun() Group by the masa_kerja_gol_tahun column
 * @method RwyKepangkatanQuery groupByMasaKerjaGolBulan() Group by the masa_kerja_gol_bulan column
 * @method RwyKepangkatanQuery groupByLastUpdate() Group by the Last_update column
 * @method RwyKepangkatanQuery groupBySoftDelete() Group by the Soft_delete column
 * @method RwyKepangkatanQuery groupByLastSync() Group by the last_sync column
 * @method RwyKepangkatanQuery groupByUpdaterId() Group by the Updater_ID column
 *
 * @method RwyKepangkatanQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method RwyKepangkatanQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method RwyKepangkatanQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method RwyKepangkatanQuery leftJoinPtkRelatedByPtkId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PtkRelatedByPtkId relation
 * @method RwyKepangkatanQuery rightJoinPtkRelatedByPtkId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PtkRelatedByPtkId relation
 * @method RwyKepangkatanQuery innerJoinPtkRelatedByPtkId($relationAlias = null) Adds a INNER JOIN clause to the query using the PtkRelatedByPtkId relation
 *
 * @method RwyKepangkatanQuery leftJoinPtkRelatedByPtkId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PtkRelatedByPtkId relation
 * @method RwyKepangkatanQuery rightJoinPtkRelatedByPtkId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PtkRelatedByPtkId relation
 * @method RwyKepangkatanQuery innerJoinPtkRelatedByPtkId($relationAlias = null) Adds a INNER JOIN clause to the query using the PtkRelatedByPtkId relation
 *
 * @method RwyKepangkatanQuery leftJoinPangkatGolonganRelatedByPangkatGolonganId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PangkatGolonganRelatedByPangkatGolonganId relation
 * @method RwyKepangkatanQuery rightJoinPangkatGolonganRelatedByPangkatGolonganId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PangkatGolonganRelatedByPangkatGolonganId relation
 * @method RwyKepangkatanQuery innerJoinPangkatGolonganRelatedByPangkatGolonganId($relationAlias = null) Adds a INNER JOIN clause to the query using the PangkatGolonganRelatedByPangkatGolonganId relation
 *
 * @method RwyKepangkatanQuery leftJoinPangkatGolonganRelatedByPangkatGolonganId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PangkatGolonganRelatedByPangkatGolonganId relation
 * @method RwyKepangkatanQuery rightJoinPangkatGolonganRelatedByPangkatGolonganId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PangkatGolonganRelatedByPangkatGolonganId relation
 * @method RwyKepangkatanQuery innerJoinPangkatGolonganRelatedByPangkatGolonganId($relationAlias = null) Adds a INNER JOIN clause to the query using the PangkatGolonganRelatedByPangkatGolonganId relation
 *
 * @method RwyKepangkatanQuery leftJoinVldRwyKepangkatanRelatedByRiwayatKepangkatanId($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldRwyKepangkatanRelatedByRiwayatKepangkatanId relation
 * @method RwyKepangkatanQuery rightJoinVldRwyKepangkatanRelatedByRiwayatKepangkatanId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldRwyKepangkatanRelatedByRiwayatKepangkatanId relation
 * @method RwyKepangkatanQuery innerJoinVldRwyKepangkatanRelatedByRiwayatKepangkatanId($relationAlias = null) Adds a INNER JOIN clause to the query using the VldRwyKepangkatanRelatedByRiwayatKepangkatanId relation
 *
 * @method RwyKepangkatanQuery leftJoinVldRwyKepangkatanRelatedByRiwayatKepangkatanId($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldRwyKepangkatanRelatedByRiwayatKepangkatanId relation
 * @method RwyKepangkatanQuery rightJoinVldRwyKepangkatanRelatedByRiwayatKepangkatanId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldRwyKepangkatanRelatedByRiwayatKepangkatanId relation
 * @method RwyKepangkatanQuery innerJoinVldRwyKepangkatanRelatedByRiwayatKepangkatanId($relationAlias = null) Adds a INNER JOIN clause to the query using the VldRwyKepangkatanRelatedByRiwayatKepangkatanId relation
 *
 * @method RwyKepangkatan findOne(PropelPDO $con = null) Return the first RwyKepangkatan matching the query
 * @method RwyKepangkatan findOneOrCreate(PropelPDO $con = null) Return the first RwyKepangkatan matching the query, or a new RwyKepangkatan object populated from the query conditions when no match is found
 *
 * @method RwyKepangkatan findOneByPtkId(string $ptk_id) Return the first RwyKepangkatan filtered by the ptk_id column
 * @method RwyKepangkatan findOneByPangkatGolonganId(string $pangkat_golongan_id) Return the first RwyKepangkatan filtered by the pangkat_golongan_id column
 * @method RwyKepangkatan findOneByNomorSk(string $nomor_sk) Return the first RwyKepangkatan filtered by the nomor_sk column
 * @method RwyKepangkatan findOneByTanggalSk(string $tanggal_sk) Return the first RwyKepangkatan filtered by the tanggal_sk column
 * @method RwyKepangkatan findOneByTmtPangkat(string $tmt_pangkat) Return the first RwyKepangkatan filtered by the tmt_pangkat column
 * @method RwyKepangkatan findOneByMasaKerjaGolTahun(string $masa_kerja_gol_tahun) Return the first RwyKepangkatan filtered by the masa_kerja_gol_tahun column
 * @method RwyKepangkatan findOneByMasaKerjaGolBulan(string $masa_kerja_gol_bulan) Return the first RwyKepangkatan filtered by the masa_kerja_gol_bulan column
 * @method RwyKepangkatan findOneByLastUpdate(string $Last_update) Return the first RwyKepangkatan filtered by the Last_update column
 * @method RwyKepangkatan findOneBySoftDelete(string $Soft_delete) Return the first RwyKepangkatan filtered by the Soft_delete column
 * @method RwyKepangkatan findOneByLastSync(string $last_sync) Return the first RwyKepangkatan filtered by the last_sync column
 * @method RwyKepangkatan findOneByUpdaterId(string $Updater_ID) Return the first RwyKepangkatan filtered by the Updater_ID column
 *
 * @method array findByRiwayatKepangkatanId(string $riwayat_kepangkatan_id) Return RwyKepangkatan objects filtered by the riwayat_kepangkatan_id column
 * @method array findByPtkId(string $ptk_id) Return RwyKepangkatan objects filtered by the ptk_id column
 * @method array findByPangkatGolonganId(string $pangkat_golongan_id) Return RwyKepangkatan objects filtered by the pangkat_golongan_id column
 * @method array findByNomorSk(string $nomor_sk) Return RwyKepangkatan objects filtered by the nomor_sk column
 * @method array findByTanggalSk(string $tanggal_sk) Return RwyKepangkatan objects filtered by the tanggal_sk column
 * @method array findByTmtPangkat(string $tmt_pangkat) Return RwyKepangkatan objects filtered by the tmt_pangkat column
 * @method array findByMasaKerjaGolTahun(string $masa_kerja_gol_tahun) Return RwyKepangkatan objects filtered by the masa_kerja_gol_tahun column
 * @method array findByMasaKerjaGolBulan(string $masa_kerja_gol_bulan) Return RwyKepangkatan objects filtered by the masa_kerja_gol_bulan column
 * @method array findByLastUpdate(string $Last_update) Return RwyKepangkatan objects filtered by the Last_update column
 * @method array findBySoftDelete(string $Soft_delete) Return RwyKepangkatan objects filtered by the Soft_delete column
 * @method array findByLastSync(string $last_sync) Return RwyKepangkatan objects filtered by the last_sync column
 * @method array findByUpdaterId(string $Updater_ID) Return RwyKepangkatan objects filtered by the Updater_ID column
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseRwyKepangkatanQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseRwyKepangkatanQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Dapodikmen', $modelName = 'angulex\\Model\\RwyKepangkatan', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new RwyKepangkatanQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   RwyKepangkatanQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return RwyKepangkatanQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof RwyKepangkatanQuery) {
            return $criteria;
        }
        $query = new RwyKepangkatanQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   RwyKepangkatan|RwyKepangkatan[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = RwyKepangkatanPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(RwyKepangkatanPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 RwyKepangkatan A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByRiwayatKepangkatanId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 RwyKepangkatan A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [riwayat_kepangkatan_id], [ptk_id], [pangkat_golongan_id], [nomor_sk], [tanggal_sk], [tmt_pangkat], [masa_kerja_gol_tahun], [masa_kerja_gol_bulan], [Last_update], [Soft_delete], [last_sync], [Updater_ID] FROM [rwy_kepangkatan] WHERE [riwayat_kepangkatan_id] = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new RwyKepangkatan();
            $obj->hydrate($row);
            RwyKepangkatanPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return RwyKepangkatan|RwyKepangkatan[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|RwyKepangkatan[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return RwyKepangkatanQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(RwyKepangkatanPeer::RIWAYAT_KEPANGKATAN_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return RwyKepangkatanQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(RwyKepangkatanPeer::RIWAYAT_KEPANGKATAN_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the riwayat_kepangkatan_id column
     *
     * Example usage:
     * <code>
     * $query->filterByRiwayatKepangkatanId('fooValue');   // WHERE riwayat_kepangkatan_id = 'fooValue'
     * $query->filterByRiwayatKepangkatanId('%fooValue%'); // WHERE riwayat_kepangkatan_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $riwayatKepangkatanId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyKepangkatanQuery The current query, for fluid interface
     */
    public function filterByRiwayatKepangkatanId($riwayatKepangkatanId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($riwayatKepangkatanId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $riwayatKepangkatanId)) {
                $riwayatKepangkatanId = str_replace('*', '%', $riwayatKepangkatanId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RwyKepangkatanPeer::RIWAYAT_KEPANGKATAN_ID, $riwayatKepangkatanId, $comparison);
    }

    /**
     * Filter the query on the ptk_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPtkId('fooValue');   // WHERE ptk_id = 'fooValue'
     * $query->filterByPtkId('%fooValue%'); // WHERE ptk_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ptkId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyKepangkatanQuery The current query, for fluid interface
     */
    public function filterByPtkId($ptkId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ptkId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ptkId)) {
                $ptkId = str_replace('*', '%', $ptkId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RwyKepangkatanPeer::PTK_ID, $ptkId, $comparison);
    }

    /**
     * Filter the query on the pangkat_golongan_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPangkatGolonganId(1234); // WHERE pangkat_golongan_id = 1234
     * $query->filterByPangkatGolonganId(array(12, 34)); // WHERE pangkat_golongan_id IN (12, 34)
     * $query->filterByPangkatGolonganId(array('min' => 12)); // WHERE pangkat_golongan_id >= 12
     * $query->filterByPangkatGolonganId(array('max' => 12)); // WHERE pangkat_golongan_id <= 12
     * </code>
     *
     * @see       filterByPangkatGolonganRelatedByPangkatGolonganId()
     *
     * @see       filterByPangkatGolonganRelatedByPangkatGolonganId()
     *
     * @param     mixed $pangkatGolonganId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyKepangkatanQuery The current query, for fluid interface
     */
    public function filterByPangkatGolonganId($pangkatGolonganId = null, $comparison = null)
    {
        if (is_array($pangkatGolonganId)) {
            $useMinMax = false;
            if (isset($pangkatGolonganId['min'])) {
                $this->addUsingAlias(RwyKepangkatanPeer::PANGKAT_GOLONGAN_ID, $pangkatGolonganId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pangkatGolonganId['max'])) {
                $this->addUsingAlias(RwyKepangkatanPeer::PANGKAT_GOLONGAN_ID, $pangkatGolonganId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RwyKepangkatanPeer::PANGKAT_GOLONGAN_ID, $pangkatGolonganId, $comparison);
    }

    /**
     * Filter the query on the nomor_sk column
     *
     * Example usage:
     * <code>
     * $query->filterByNomorSk('fooValue');   // WHERE nomor_sk = 'fooValue'
     * $query->filterByNomorSk('%fooValue%'); // WHERE nomor_sk LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomorSk The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyKepangkatanQuery The current query, for fluid interface
     */
    public function filterByNomorSk($nomorSk = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomorSk)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nomorSk)) {
                $nomorSk = str_replace('*', '%', $nomorSk);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RwyKepangkatanPeer::NOMOR_SK, $nomorSk, $comparison);
    }

    /**
     * Filter the query on the tanggal_sk column
     *
     * Example usage:
     * <code>
     * $query->filterByTanggalSk('fooValue');   // WHERE tanggal_sk = 'fooValue'
     * $query->filterByTanggalSk('%fooValue%'); // WHERE tanggal_sk LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tanggalSk The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyKepangkatanQuery The current query, for fluid interface
     */
    public function filterByTanggalSk($tanggalSk = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tanggalSk)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tanggalSk)) {
                $tanggalSk = str_replace('*', '%', $tanggalSk);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RwyKepangkatanPeer::TANGGAL_SK, $tanggalSk, $comparison);
    }

    /**
     * Filter the query on the tmt_pangkat column
     *
     * Example usage:
     * <code>
     * $query->filterByTmtPangkat('fooValue');   // WHERE tmt_pangkat = 'fooValue'
     * $query->filterByTmtPangkat('%fooValue%'); // WHERE tmt_pangkat LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tmtPangkat The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyKepangkatanQuery The current query, for fluid interface
     */
    public function filterByTmtPangkat($tmtPangkat = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tmtPangkat)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tmtPangkat)) {
                $tmtPangkat = str_replace('*', '%', $tmtPangkat);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RwyKepangkatanPeer::TMT_PANGKAT, $tmtPangkat, $comparison);
    }

    /**
     * Filter the query on the masa_kerja_gol_tahun column
     *
     * Example usage:
     * <code>
     * $query->filterByMasaKerjaGolTahun(1234); // WHERE masa_kerja_gol_tahun = 1234
     * $query->filterByMasaKerjaGolTahun(array(12, 34)); // WHERE masa_kerja_gol_tahun IN (12, 34)
     * $query->filterByMasaKerjaGolTahun(array('min' => 12)); // WHERE masa_kerja_gol_tahun >= 12
     * $query->filterByMasaKerjaGolTahun(array('max' => 12)); // WHERE masa_kerja_gol_tahun <= 12
     * </code>
     *
     * @param     mixed $masaKerjaGolTahun The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyKepangkatanQuery The current query, for fluid interface
     */
    public function filterByMasaKerjaGolTahun($masaKerjaGolTahun = null, $comparison = null)
    {
        if (is_array($masaKerjaGolTahun)) {
            $useMinMax = false;
            if (isset($masaKerjaGolTahun['min'])) {
                $this->addUsingAlias(RwyKepangkatanPeer::MASA_KERJA_GOL_TAHUN, $masaKerjaGolTahun['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($masaKerjaGolTahun['max'])) {
                $this->addUsingAlias(RwyKepangkatanPeer::MASA_KERJA_GOL_TAHUN, $masaKerjaGolTahun['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RwyKepangkatanPeer::MASA_KERJA_GOL_TAHUN, $masaKerjaGolTahun, $comparison);
    }

    /**
     * Filter the query on the masa_kerja_gol_bulan column
     *
     * Example usage:
     * <code>
     * $query->filterByMasaKerjaGolBulan(1234); // WHERE masa_kerja_gol_bulan = 1234
     * $query->filterByMasaKerjaGolBulan(array(12, 34)); // WHERE masa_kerja_gol_bulan IN (12, 34)
     * $query->filterByMasaKerjaGolBulan(array('min' => 12)); // WHERE masa_kerja_gol_bulan >= 12
     * $query->filterByMasaKerjaGolBulan(array('max' => 12)); // WHERE masa_kerja_gol_bulan <= 12
     * </code>
     *
     * @param     mixed $masaKerjaGolBulan The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyKepangkatanQuery The current query, for fluid interface
     */
    public function filterByMasaKerjaGolBulan($masaKerjaGolBulan = null, $comparison = null)
    {
        if (is_array($masaKerjaGolBulan)) {
            $useMinMax = false;
            if (isset($masaKerjaGolBulan['min'])) {
                $this->addUsingAlias(RwyKepangkatanPeer::MASA_KERJA_GOL_BULAN, $masaKerjaGolBulan['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($masaKerjaGolBulan['max'])) {
                $this->addUsingAlias(RwyKepangkatanPeer::MASA_KERJA_GOL_BULAN, $masaKerjaGolBulan['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RwyKepangkatanPeer::MASA_KERJA_GOL_BULAN, $masaKerjaGolBulan, $comparison);
    }

    /**
     * Filter the query on the Last_update column
     *
     * Example usage:
     * <code>
     * $query->filterByLastUpdate('2011-03-14'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate('now'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate(array('max' => 'yesterday')); // WHERE Last_update > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastUpdate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyKepangkatanQuery The current query, for fluid interface
     */
    public function filterByLastUpdate($lastUpdate = null, $comparison = null)
    {
        if (is_array($lastUpdate)) {
            $useMinMax = false;
            if (isset($lastUpdate['min'])) {
                $this->addUsingAlias(RwyKepangkatanPeer::LAST_UPDATE, $lastUpdate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastUpdate['max'])) {
                $this->addUsingAlias(RwyKepangkatanPeer::LAST_UPDATE, $lastUpdate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RwyKepangkatanPeer::LAST_UPDATE, $lastUpdate, $comparison);
    }

    /**
     * Filter the query on the Soft_delete column
     *
     * Example usage:
     * <code>
     * $query->filterBySoftDelete(1234); // WHERE Soft_delete = 1234
     * $query->filterBySoftDelete(array(12, 34)); // WHERE Soft_delete IN (12, 34)
     * $query->filterBySoftDelete(array('min' => 12)); // WHERE Soft_delete >= 12
     * $query->filterBySoftDelete(array('max' => 12)); // WHERE Soft_delete <= 12
     * </code>
     *
     * @param     mixed $softDelete The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyKepangkatanQuery The current query, for fluid interface
     */
    public function filterBySoftDelete($softDelete = null, $comparison = null)
    {
        if (is_array($softDelete)) {
            $useMinMax = false;
            if (isset($softDelete['min'])) {
                $this->addUsingAlias(RwyKepangkatanPeer::SOFT_DELETE, $softDelete['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($softDelete['max'])) {
                $this->addUsingAlias(RwyKepangkatanPeer::SOFT_DELETE, $softDelete['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RwyKepangkatanPeer::SOFT_DELETE, $softDelete, $comparison);
    }

    /**
     * Filter the query on the last_sync column
     *
     * Example usage:
     * <code>
     * $query->filterByLastSync('2011-03-14'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync('now'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync(array('max' => 'yesterday')); // WHERE last_sync > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastSync The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyKepangkatanQuery The current query, for fluid interface
     */
    public function filterByLastSync($lastSync = null, $comparison = null)
    {
        if (is_array($lastSync)) {
            $useMinMax = false;
            if (isset($lastSync['min'])) {
                $this->addUsingAlias(RwyKepangkatanPeer::LAST_SYNC, $lastSync['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastSync['max'])) {
                $this->addUsingAlias(RwyKepangkatanPeer::LAST_SYNC, $lastSync['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RwyKepangkatanPeer::LAST_SYNC, $lastSync, $comparison);
    }

    /**
     * Filter the query on the Updater_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdaterId('fooValue');   // WHERE Updater_ID = 'fooValue'
     * $query->filterByUpdaterId('%fooValue%'); // WHERE Updater_ID LIKE '%fooValue%'
     * </code>
     *
     * @param     string $updaterId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyKepangkatanQuery The current query, for fluid interface
     */
    public function filterByUpdaterId($updaterId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($updaterId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $updaterId)) {
                $updaterId = str_replace('*', '%', $updaterId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RwyKepangkatanPeer::UPDATER_ID, $updaterId, $comparison);
    }

    /**
     * Filter the query by a related Ptk object
     *
     * @param   Ptk|PropelObjectCollection $ptk The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RwyKepangkatanQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPtkRelatedByPtkId($ptk, $comparison = null)
    {
        if ($ptk instanceof Ptk) {
            return $this
                ->addUsingAlias(RwyKepangkatanPeer::PTK_ID, $ptk->getPtkId(), $comparison);
        } elseif ($ptk instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RwyKepangkatanPeer::PTK_ID, $ptk->toKeyValue('PrimaryKey', 'PtkId'), $comparison);
        } else {
            throw new PropelException('filterByPtkRelatedByPtkId() only accepts arguments of type Ptk or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PtkRelatedByPtkId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RwyKepangkatanQuery The current query, for fluid interface
     */
    public function joinPtkRelatedByPtkId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PtkRelatedByPtkId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PtkRelatedByPtkId');
        }

        return $this;
    }

    /**
     * Use the PtkRelatedByPtkId relation Ptk object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PtkQuery A secondary query class using the current class as primary query
     */
    public function usePtkRelatedByPtkIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPtkRelatedByPtkId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PtkRelatedByPtkId', '\angulex\Model\PtkQuery');
    }

    /**
     * Filter the query by a related Ptk object
     *
     * @param   Ptk|PropelObjectCollection $ptk The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RwyKepangkatanQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPtkRelatedByPtkId($ptk, $comparison = null)
    {
        if ($ptk instanceof Ptk) {
            return $this
                ->addUsingAlias(RwyKepangkatanPeer::PTK_ID, $ptk->getPtkId(), $comparison);
        } elseif ($ptk instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RwyKepangkatanPeer::PTK_ID, $ptk->toKeyValue('PrimaryKey', 'PtkId'), $comparison);
        } else {
            throw new PropelException('filterByPtkRelatedByPtkId() only accepts arguments of type Ptk or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PtkRelatedByPtkId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RwyKepangkatanQuery The current query, for fluid interface
     */
    public function joinPtkRelatedByPtkId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PtkRelatedByPtkId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PtkRelatedByPtkId');
        }

        return $this;
    }

    /**
     * Use the PtkRelatedByPtkId relation Ptk object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PtkQuery A secondary query class using the current class as primary query
     */
    public function usePtkRelatedByPtkIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPtkRelatedByPtkId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PtkRelatedByPtkId', '\angulex\Model\PtkQuery');
    }

    /**
     * Filter the query by a related PangkatGolongan object
     *
     * @param   PangkatGolongan|PropelObjectCollection $pangkatGolongan The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RwyKepangkatanQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPangkatGolonganRelatedByPangkatGolonganId($pangkatGolongan, $comparison = null)
    {
        if ($pangkatGolongan instanceof PangkatGolongan) {
            return $this
                ->addUsingAlias(RwyKepangkatanPeer::PANGKAT_GOLONGAN_ID, $pangkatGolongan->getPangkatGolonganId(), $comparison);
        } elseif ($pangkatGolongan instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RwyKepangkatanPeer::PANGKAT_GOLONGAN_ID, $pangkatGolongan->toKeyValue('PrimaryKey', 'PangkatGolonganId'), $comparison);
        } else {
            throw new PropelException('filterByPangkatGolonganRelatedByPangkatGolonganId() only accepts arguments of type PangkatGolongan or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PangkatGolonganRelatedByPangkatGolonganId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RwyKepangkatanQuery The current query, for fluid interface
     */
    public function joinPangkatGolonganRelatedByPangkatGolonganId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PangkatGolonganRelatedByPangkatGolonganId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PangkatGolonganRelatedByPangkatGolonganId');
        }

        return $this;
    }

    /**
     * Use the PangkatGolonganRelatedByPangkatGolonganId relation PangkatGolongan object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PangkatGolonganQuery A secondary query class using the current class as primary query
     */
    public function usePangkatGolonganRelatedByPangkatGolonganIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPangkatGolonganRelatedByPangkatGolonganId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PangkatGolonganRelatedByPangkatGolonganId', '\angulex\Model\PangkatGolonganQuery');
    }

    /**
     * Filter the query by a related PangkatGolongan object
     *
     * @param   PangkatGolongan|PropelObjectCollection $pangkatGolongan The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RwyKepangkatanQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPangkatGolonganRelatedByPangkatGolonganId($pangkatGolongan, $comparison = null)
    {
        if ($pangkatGolongan instanceof PangkatGolongan) {
            return $this
                ->addUsingAlias(RwyKepangkatanPeer::PANGKAT_GOLONGAN_ID, $pangkatGolongan->getPangkatGolonganId(), $comparison);
        } elseif ($pangkatGolongan instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RwyKepangkatanPeer::PANGKAT_GOLONGAN_ID, $pangkatGolongan->toKeyValue('PrimaryKey', 'PangkatGolonganId'), $comparison);
        } else {
            throw new PropelException('filterByPangkatGolonganRelatedByPangkatGolonganId() only accepts arguments of type PangkatGolongan or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PangkatGolonganRelatedByPangkatGolonganId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RwyKepangkatanQuery The current query, for fluid interface
     */
    public function joinPangkatGolonganRelatedByPangkatGolonganId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PangkatGolonganRelatedByPangkatGolonganId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PangkatGolonganRelatedByPangkatGolonganId');
        }

        return $this;
    }

    /**
     * Use the PangkatGolonganRelatedByPangkatGolonganId relation PangkatGolongan object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PangkatGolonganQuery A secondary query class using the current class as primary query
     */
    public function usePangkatGolonganRelatedByPangkatGolonganIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPangkatGolonganRelatedByPangkatGolonganId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PangkatGolonganRelatedByPangkatGolonganId', '\angulex\Model\PangkatGolonganQuery');
    }

    /**
     * Filter the query by a related VldRwyKepangkatan object
     *
     * @param   VldRwyKepangkatan|PropelObjectCollection $vldRwyKepangkatan  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RwyKepangkatanQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldRwyKepangkatanRelatedByRiwayatKepangkatanId($vldRwyKepangkatan, $comparison = null)
    {
        if ($vldRwyKepangkatan instanceof VldRwyKepangkatan) {
            return $this
                ->addUsingAlias(RwyKepangkatanPeer::RIWAYAT_KEPANGKATAN_ID, $vldRwyKepangkatan->getRiwayatKepangkatanId(), $comparison);
        } elseif ($vldRwyKepangkatan instanceof PropelObjectCollection) {
            return $this
                ->useVldRwyKepangkatanRelatedByRiwayatKepangkatanIdQuery()
                ->filterByPrimaryKeys($vldRwyKepangkatan->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldRwyKepangkatanRelatedByRiwayatKepangkatanId() only accepts arguments of type VldRwyKepangkatan or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldRwyKepangkatanRelatedByRiwayatKepangkatanId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RwyKepangkatanQuery The current query, for fluid interface
     */
    public function joinVldRwyKepangkatanRelatedByRiwayatKepangkatanId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldRwyKepangkatanRelatedByRiwayatKepangkatanId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldRwyKepangkatanRelatedByRiwayatKepangkatanId');
        }

        return $this;
    }

    /**
     * Use the VldRwyKepangkatanRelatedByRiwayatKepangkatanId relation VldRwyKepangkatan object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldRwyKepangkatanQuery A secondary query class using the current class as primary query
     */
    public function useVldRwyKepangkatanRelatedByRiwayatKepangkatanIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldRwyKepangkatanRelatedByRiwayatKepangkatanId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldRwyKepangkatanRelatedByRiwayatKepangkatanId', '\angulex\Model\VldRwyKepangkatanQuery');
    }

    /**
     * Filter the query by a related VldRwyKepangkatan object
     *
     * @param   VldRwyKepangkatan|PropelObjectCollection $vldRwyKepangkatan  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RwyKepangkatanQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldRwyKepangkatanRelatedByRiwayatKepangkatanId($vldRwyKepangkatan, $comparison = null)
    {
        if ($vldRwyKepangkatan instanceof VldRwyKepangkatan) {
            return $this
                ->addUsingAlias(RwyKepangkatanPeer::RIWAYAT_KEPANGKATAN_ID, $vldRwyKepangkatan->getRiwayatKepangkatanId(), $comparison);
        } elseif ($vldRwyKepangkatan instanceof PropelObjectCollection) {
            return $this
                ->useVldRwyKepangkatanRelatedByRiwayatKepangkatanIdQuery()
                ->filterByPrimaryKeys($vldRwyKepangkatan->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldRwyKepangkatanRelatedByRiwayatKepangkatanId() only accepts arguments of type VldRwyKepangkatan or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldRwyKepangkatanRelatedByRiwayatKepangkatanId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RwyKepangkatanQuery The current query, for fluid interface
     */
    public function joinVldRwyKepangkatanRelatedByRiwayatKepangkatanId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldRwyKepangkatanRelatedByRiwayatKepangkatanId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldRwyKepangkatanRelatedByRiwayatKepangkatanId');
        }

        return $this;
    }

    /**
     * Use the VldRwyKepangkatanRelatedByRiwayatKepangkatanId relation VldRwyKepangkatan object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldRwyKepangkatanQuery A secondary query class using the current class as primary query
     */
    public function useVldRwyKepangkatanRelatedByRiwayatKepangkatanIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldRwyKepangkatanRelatedByRiwayatKepangkatanId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldRwyKepangkatanRelatedByRiwayatKepangkatanId', '\angulex\Model\VldRwyKepangkatanQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   RwyKepangkatan $rwyKepangkatan Object to remove from the list of results
     *
     * @return RwyKepangkatanQuery The current query, for fluid interface
     */
    public function prune($rwyKepangkatan = null)
    {
        if ($rwyKepangkatan) {
            $this->addUsingAlias(RwyKepangkatanPeer::RIWAYAT_KEPANGKATAN_ID, $rwyKepangkatan->getRiwayatKepangkatanId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
