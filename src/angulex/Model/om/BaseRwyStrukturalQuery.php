<?php

namespace angulex\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\JabatanTugasPtk;
use angulex\Model\Ptk;
use angulex\Model\RwyStruktural;
use angulex\Model\RwyStrukturalPeer;
use angulex\Model\RwyStrukturalQuery;
use angulex\Model\VldRwyStruktural;

/**
 * Base class that represents a query for the 'rwy_struktural' table.
 *
 * 
 *
 * @method RwyStrukturalQuery orderByRiwayatStrukturalId($order = Criteria::ASC) Order by the riwayat_struktural_id column
 * @method RwyStrukturalQuery orderByPtkId($order = Criteria::ASC) Order by the ptk_id column
 * @method RwyStrukturalQuery orderByJabatanPtkId($order = Criteria::ASC) Order by the jabatan_ptk_id column
 * @method RwyStrukturalQuery orderBySkStruktural($order = Criteria::ASC) Order by the sk_struktural column
 * @method RwyStrukturalQuery orderByTmtJabatan($order = Criteria::ASC) Order by the tmt_jabatan column
 * @method RwyStrukturalQuery orderByLastUpdate($order = Criteria::ASC) Order by the Last_update column
 * @method RwyStrukturalQuery orderBySoftDelete($order = Criteria::ASC) Order by the Soft_delete column
 * @method RwyStrukturalQuery orderByLastSync($order = Criteria::ASC) Order by the last_sync column
 * @method RwyStrukturalQuery orderByUpdaterId($order = Criteria::ASC) Order by the Updater_ID column
 *
 * @method RwyStrukturalQuery groupByRiwayatStrukturalId() Group by the riwayat_struktural_id column
 * @method RwyStrukturalQuery groupByPtkId() Group by the ptk_id column
 * @method RwyStrukturalQuery groupByJabatanPtkId() Group by the jabatan_ptk_id column
 * @method RwyStrukturalQuery groupBySkStruktural() Group by the sk_struktural column
 * @method RwyStrukturalQuery groupByTmtJabatan() Group by the tmt_jabatan column
 * @method RwyStrukturalQuery groupByLastUpdate() Group by the Last_update column
 * @method RwyStrukturalQuery groupBySoftDelete() Group by the Soft_delete column
 * @method RwyStrukturalQuery groupByLastSync() Group by the last_sync column
 * @method RwyStrukturalQuery groupByUpdaterId() Group by the Updater_ID column
 *
 * @method RwyStrukturalQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method RwyStrukturalQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method RwyStrukturalQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method RwyStrukturalQuery leftJoinPtkRelatedByPtkId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PtkRelatedByPtkId relation
 * @method RwyStrukturalQuery rightJoinPtkRelatedByPtkId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PtkRelatedByPtkId relation
 * @method RwyStrukturalQuery innerJoinPtkRelatedByPtkId($relationAlias = null) Adds a INNER JOIN clause to the query using the PtkRelatedByPtkId relation
 *
 * @method RwyStrukturalQuery leftJoinPtkRelatedByPtkId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PtkRelatedByPtkId relation
 * @method RwyStrukturalQuery rightJoinPtkRelatedByPtkId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PtkRelatedByPtkId relation
 * @method RwyStrukturalQuery innerJoinPtkRelatedByPtkId($relationAlias = null) Adds a INNER JOIN clause to the query using the PtkRelatedByPtkId relation
 *
 * @method RwyStrukturalQuery leftJoinJabatanTugasPtkRelatedByJabatanPtkId($relationAlias = null) Adds a LEFT JOIN clause to the query using the JabatanTugasPtkRelatedByJabatanPtkId relation
 * @method RwyStrukturalQuery rightJoinJabatanTugasPtkRelatedByJabatanPtkId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JabatanTugasPtkRelatedByJabatanPtkId relation
 * @method RwyStrukturalQuery innerJoinJabatanTugasPtkRelatedByJabatanPtkId($relationAlias = null) Adds a INNER JOIN clause to the query using the JabatanTugasPtkRelatedByJabatanPtkId relation
 *
 * @method RwyStrukturalQuery leftJoinJabatanTugasPtkRelatedByJabatanPtkId($relationAlias = null) Adds a LEFT JOIN clause to the query using the JabatanTugasPtkRelatedByJabatanPtkId relation
 * @method RwyStrukturalQuery rightJoinJabatanTugasPtkRelatedByJabatanPtkId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JabatanTugasPtkRelatedByJabatanPtkId relation
 * @method RwyStrukturalQuery innerJoinJabatanTugasPtkRelatedByJabatanPtkId($relationAlias = null) Adds a INNER JOIN clause to the query using the JabatanTugasPtkRelatedByJabatanPtkId relation
 *
 * @method RwyStrukturalQuery leftJoinVldRwyStrukturalRelatedByRiwayatStrukturalId($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldRwyStrukturalRelatedByRiwayatStrukturalId relation
 * @method RwyStrukturalQuery rightJoinVldRwyStrukturalRelatedByRiwayatStrukturalId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldRwyStrukturalRelatedByRiwayatStrukturalId relation
 * @method RwyStrukturalQuery innerJoinVldRwyStrukturalRelatedByRiwayatStrukturalId($relationAlias = null) Adds a INNER JOIN clause to the query using the VldRwyStrukturalRelatedByRiwayatStrukturalId relation
 *
 * @method RwyStrukturalQuery leftJoinVldRwyStrukturalRelatedByRiwayatStrukturalId($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldRwyStrukturalRelatedByRiwayatStrukturalId relation
 * @method RwyStrukturalQuery rightJoinVldRwyStrukturalRelatedByRiwayatStrukturalId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldRwyStrukturalRelatedByRiwayatStrukturalId relation
 * @method RwyStrukturalQuery innerJoinVldRwyStrukturalRelatedByRiwayatStrukturalId($relationAlias = null) Adds a INNER JOIN clause to the query using the VldRwyStrukturalRelatedByRiwayatStrukturalId relation
 *
 * @method RwyStruktural findOne(PropelPDO $con = null) Return the first RwyStruktural matching the query
 * @method RwyStruktural findOneOrCreate(PropelPDO $con = null) Return the first RwyStruktural matching the query, or a new RwyStruktural object populated from the query conditions when no match is found
 *
 * @method RwyStruktural findOneByPtkId(string $ptk_id) Return the first RwyStruktural filtered by the ptk_id column
 * @method RwyStruktural findOneByJabatanPtkId(string $jabatan_ptk_id) Return the first RwyStruktural filtered by the jabatan_ptk_id column
 * @method RwyStruktural findOneBySkStruktural(string $sk_struktural) Return the first RwyStruktural filtered by the sk_struktural column
 * @method RwyStruktural findOneByTmtJabatan(string $tmt_jabatan) Return the first RwyStruktural filtered by the tmt_jabatan column
 * @method RwyStruktural findOneByLastUpdate(string $Last_update) Return the first RwyStruktural filtered by the Last_update column
 * @method RwyStruktural findOneBySoftDelete(string $Soft_delete) Return the first RwyStruktural filtered by the Soft_delete column
 * @method RwyStruktural findOneByLastSync(string $last_sync) Return the first RwyStruktural filtered by the last_sync column
 * @method RwyStruktural findOneByUpdaterId(string $Updater_ID) Return the first RwyStruktural filtered by the Updater_ID column
 *
 * @method array findByRiwayatStrukturalId(string $riwayat_struktural_id) Return RwyStruktural objects filtered by the riwayat_struktural_id column
 * @method array findByPtkId(string $ptk_id) Return RwyStruktural objects filtered by the ptk_id column
 * @method array findByJabatanPtkId(string $jabatan_ptk_id) Return RwyStruktural objects filtered by the jabatan_ptk_id column
 * @method array findBySkStruktural(string $sk_struktural) Return RwyStruktural objects filtered by the sk_struktural column
 * @method array findByTmtJabatan(string $tmt_jabatan) Return RwyStruktural objects filtered by the tmt_jabatan column
 * @method array findByLastUpdate(string $Last_update) Return RwyStruktural objects filtered by the Last_update column
 * @method array findBySoftDelete(string $Soft_delete) Return RwyStruktural objects filtered by the Soft_delete column
 * @method array findByLastSync(string $last_sync) Return RwyStruktural objects filtered by the last_sync column
 * @method array findByUpdaterId(string $Updater_ID) Return RwyStruktural objects filtered by the Updater_ID column
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseRwyStrukturalQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseRwyStrukturalQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Dapodikmen', $modelName = 'angulex\\Model\\RwyStruktural', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new RwyStrukturalQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   RwyStrukturalQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return RwyStrukturalQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof RwyStrukturalQuery) {
            return $criteria;
        }
        $query = new RwyStrukturalQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   RwyStruktural|RwyStruktural[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = RwyStrukturalPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(RwyStrukturalPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 RwyStruktural A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByRiwayatStrukturalId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 RwyStruktural A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [riwayat_struktural_id], [ptk_id], [jabatan_ptk_id], [sk_struktural], [tmt_jabatan], [Last_update], [Soft_delete], [last_sync], [Updater_ID] FROM [rwy_struktural] WHERE [riwayat_struktural_id] = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new RwyStruktural();
            $obj->hydrate($row);
            RwyStrukturalPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return RwyStruktural|RwyStruktural[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|RwyStruktural[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return RwyStrukturalQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(RwyStrukturalPeer::RIWAYAT_STRUKTURAL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return RwyStrukturalQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(RwyStrukturalPeer::RIWAYAT_STRUKTURAL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the riwayat_struktural_id column
     *
     * Example usage:
     * <code>
     * $query->filterByRiwayatStrukturalId('fooValue');   // WHERE riwayat_struktural_id = 'fooValue'
     * $query->filterByRiwayatStrukturalId('%fooValue%'); // WHERE riwayat_struktural_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $riwayatStrukturalId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyStrukturalQuery The current query, for fluid interface
     */
    public function filterByRiwayatStrukturalId($riwayatStrukturalId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($riwayatStrukturalId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $riwayatStrukturalId)) {
                $riwayatStrukturalId = str_replace('*', '%', $riwayatStrukturalId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RwyStrukturalPeer::RIWAYAT_STRUKTURAL_ID, $riwayatStrukturalId, $comparison);
    }

    /**
     * Filter the query on the ptk_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPtkId('fooValue');   // WHERE ptk_id = 'fooValue'
     * $query->filterByPtkId('%fooValue%'); // WHERE ptk_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ptkId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyStrukturalQuery The current query, for fluid interface
     */
    public function filterByPtkId($ptkId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ptkId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ptkId)) {
                $ptkId = str_replace('*', '%', $ptkId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RwyStrukturalPeer::PTK_ID, $ptkId, $comparison);
    }

    /**
     * Filter the query on the jabatan_ptk_id column
     *
     * Example usage:
     * <code>
     * $query->filterByJabatanPtkId(1234); // WHERE jabatan_ptk_id = 1234
     * $query->filterByJabatanPtkId(array(12, 34)); // WHERE jabatan_ptk_id IN (12, 34)
     * $query->filterByJabatanPtkId(array('min' => 12)); // WHERE jabatan_ptk_id >= 12
     * $query->filterByJabatanPtkId(array('max' => 12)); // WHERE jabatan_ptk_id <= 12
     * </code>
     *
     * @see       filterByJabatanTugasPtkRelatedByJabatanPtkId()
     *
     * @see       filterByJabatanTugasPtkRelatedByJabatanPtkId()
     *
     * @param     mixed $jabatanPtkId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyStrukturalQuery The current query, for fluid interface
     */
    public function filterByJabatanPtkId($jabatanPtkId = null, $comparison = null)
    {
        if (is_array($jabatanPtkId)) {
            $useMinMax = false;
            if (isset($jabatanPtkId['min'])) {
                $this->addUsingAlias(RwyStrukturalPeer::JABATAN_PTK_ID, $jabatanPtkId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jabatanPtkId['max'])) {
                $this->addUsingAlias(RwyStrukturalPeer::JABATAN_PTK_ID, $jabatanPtkId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RwyStrukturalPeer::JABATAN_PTK_ID, $jabatanPtkId, $comparison);
    }

    /**
     * Filter the query on the sk_struktural column
     *
     * Example usage:
     * <code>
     * $query->filterBySkStruktural('fooValue');   // WHERE sk_struktural = 'fooValue'
     * $query->filterBySkStruktural('%fooValue%'); // WHERE sk_struktural LIKE '%fooValue%'
     * </code>
     *
     * @param     string $skStruktural The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyStrukturalQuery The current query, for fluid interface
     */
    public function filterBySkStruktural($skStruktural = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($skStruktural)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $skStruktural)) {
                $skStruktural = str_replace('*', '%', $skStruktural);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RwyStrukturalPeer::SK_STRUKTURAL, $skStruktural, $comparison);
    }

    /**
     * Filter the query on the tmt_jabatan column
     *
     * Example usage:
     * <code>
     * $query->filterByTmtJabatan('fooValue');   // WHERE tmt_jabatan = 'fooValue'
     * $query->filterByTmtJabatan('%fooValue%'); // WHERE tmt_jabatan LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tmtJabatan The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyStrukturalQuery The current query, for fluid interface
     */
    public function filterByTmtJabatan($tmtJabatan = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tmtJabatan)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tmtJabatan)) {
                $tmtJabatan = str_replace('*', '%', $tmtJabatan);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RwyStrukturalPeer::TMT_JABATAN, $tmtJabatan, $comparison);
    }

    /**
     * Filter the query on the Last_update column
     *
     * Example usage:
     * <code>
     * $query->filterByLastUpdate('2011-03-14'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate('now'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate(array('max' => 'yesterday')); // WHERE Last_update > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastUpdate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyStrukturalQuery The current query, for fluid interface
     */
    public function filterByLastUpdate($lastUpdate = null, $comparison = null)
    {
        if (is_array($lastUpdate)) {
            $useMinMax = false;
            if (isset($lastUpdate['min'])) {
                $this->addUsingAlias(RwyStrukturalPeer::LAST_UPDATE, $lastUpdate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastUpdate['max'])) {
                $this->addUsingAlias(RwyStrukturalPeer::LAST_UPDATE, $lastUpdate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RwyStrukturalPeer::LAST_UPDATE, $lastUpdate, $comparison);
    }

    /**
     * Filter the query on the Soft_delete column
     *
     * Example usage:
     * <code>
     * $query->filterBySoftDelete(1234); // WHERE Soft_delete = 1234
     * $query->filterBySoftDelete(array(12, 34)); // WHERE Soft_delete IN (12, 34)
     * $query->filterBySoftDelete(array('min' => 12)); // WHERE Soft_delete >= 12
     * $query->filterBySoftDelete(array('max' => 12)); // WHERE Soft_delete <= 12
     * </code>
     *
     * @param     mixed $softDelete The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyStrukturalQuery The current query, for fluid interface
     */
    public function filterBySoftDelete($softDelete = null, $comparison = null)
    {
        if (is_array($softDelete)) {
            $useMinMax = false;
            if (isset($softDelete['min'])) {
                $this->addUsingAlias(RwyStrukturalPeer::SOFT_DELETE, $softDelete['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($softDelete['max'])) {
                $this->addUsingAlias(RwyStrukturalPeer::SOFT_DELETE, $softDelete['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RwyStrukturalPeer::SOFT_DELETE, $softDelete, $comparison);
    }

    /**
     * Filter the query on the last_sync column
     *
     * Example usage:
     * <code>
     * $query->filterByLastSync('2011-03-14'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync('now'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync(array('max' => 'yesterday')); // WHERE last_sync > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastSync The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyStrukturalQuery The current query, for fluid interface
     */
    public function filterByLastSync($lastSync = null, $comparison = null)
    {
        if (is_array($lastSync)) {
            $useMinMax = false;
            if (isset($lastSync['min'])) {
                $this->addUsingAlias(RwyStrukturalPeer::LAST_SYNC, $lastSync['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastSync['max'])) {
                $this->addUsingAlias(RwyStrukturalPeer::LAST_SYNC, $lastSync['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RwyStrukturalPeer::LAST_SYNC, $lastSync, $comparison);
    }

    /**
     * Filter the query on the Updater_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdaterId('fooValue');   // WHERE Updater_ID = 'fooValue'
     * $query->filterByUpdaterId('%fooValue%'); // WHERE Updater_ID LIKE '%fooValue%'
     * </code>
     *
     * @param     string $updaterId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyStrukturalQuery The current query, for fluid interface
     */
    public function filterByUpdaterId($updaterId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($updaterId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $updaterId)) {
                $updaterId = str_replace('*', '%', $updaterId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RwyStrukturalPeer::UPDATER_ID, $updaterId, $comparison);
    }

    /**
     * Filter the query by a related Ptk object
     *
     * @param   Ptk|PropelObjectCollection $ptk The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RwyStrukturalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPtkRelatedByPtkId($ptk, $comparison = null)
    {
        if ($ptk instanceof Ptk) {
            return $this
                ->addUsingAlias(RwyStrukturalPeer::PTK_ID, $ptk->getPtkId(), $comparison);
        } elseif ($ptk instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RwyStrukturalPeer::PTK_ID, $ptk->toKeyValue('PrimaryKey', 'PtkId'), $comparison);
        } else {
            throw new PropelException('filterByPtkRelatedByPtkId() only accepts arguments of type Ptk or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PtkRelatedByPtkId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RwyStrukturalQuery The current query, for fluid interface
     */
    public function joinPtkRelatedByPtkId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PtkRelatedByPtkId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PtkRelatedByPtkId');
        }

        return $this;
    }

    /**
     * Use the PtkRelatedByPtkId relation Ptk object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PtkQuery A secondary query class using the current class as primary query
     */
    public function usePtkRelatedByPtkIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPtkRelatedByPtkId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PtkRelatedByPtkId', '\angulex\Model\PtkQuery');
    }

    /**
     * Filter the query by a related Ptk object
     *
     * @param   Ptk|PropelObjectCollection $ptk The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RwyStrukturalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPtkRelatedByPtkId($ptk, $comparison = null)
    {
        if ($ptk instanceof Ptk) {
            return $this
                ->addUsingAlias(RwyStrukturalPeer::PTK_ID, $ptk->getPtkId(), $comparison);
        } elseif ($ptk instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RwyStrukturalPeer::PTK_ID, $ptk->toKeyValue('PrimaryKey', 'PtkId'), $comparison);
        } else {
            throw new PropelException('filterByPtkRelatedByPtkId() only accepts arguments of type Ptk or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PtkRelatedByPtkId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RwyStrukturalQuery The current query, for fluid interface
     */
    public function joinPtkRelatedByPtkId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PtkRelatedByPtkId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PtkRelatedByPtkId');
        }

        return $this;
    }

    /**
     * Use the PtkRelatedByPtkId relation Ptk object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PtkQuery A secondary query class using the current class as primary query
     */
    public function usePtkRelatedByPtkIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPtkRelatedByPtkId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PtkRelatedByPtkId', '\angulex\Model\PtkQuery');
    }

    /**
     * Filter the query by a related JabatanTugasPtk object
     *
     * @param   JabatanTugasPtk|PropelObjectCollection $jabatanTugasPtk The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RwyStrukturalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJabatanTugasPtkRelatedByJabatanPtkId($jabatanTugasPtk, $comparison = null)
    {
        if ($jabatanTugasPtk instanceof JabatanTugasPtk) {
            return $this
                ->addUsingAlias(RwyStrukturalPeer::JABATAN_PTK_ID, $jabatanTugasPtk->getJabatanPtkId(), $comparison);
        } elseif ($jabatanTugasPtk instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RwyStrukturalPeer::JABATAN_PTK_ID, $jabatanTugasPtk->toKeyValue('PrimaryKey', 'JabatanPtkId'), $comparison);
        } else {
            throw new PropelException('filterByJabatanTugasPtkRelatedByJabatanPtkId() only accepts arguments of type JabatanTugasPtk or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the JabatanTugasPtkRelatedByJabatanPtkId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RwyStrukturalQuery The current query, for fluid interface
     */
    public function joinJabatanTugasPtkRelatedByJabatanPtkId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('JabatanTugasPtkRelatedByJabatanPtkId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'JabatanTugasPtkRelatedByJabatanPtkId');
        }

        return $this;
    }

    /**
     * Use the JabatanTugasPtkRelatedByJabatanPtkId relation JabatanTugasPtk object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\JabatanTugasPtkQuery A secondary query class using the current class as primary query
     */
    public function useJabatanTugasPtkRelatedByJabatanPtkIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinJabatanTugasPtkRelatedByJabatanPtkId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'JabatanTugasPtkRelatedByJabatanPtkId', '\angulex\Model\JabatanTugasPtkQuery');
    }

    /**
     * Filter the query by a related JabatanTugasPtk object
     *
     * @param   JabatanTugasPtk|PropelObjectCollection $jabatanTugasPtk The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RwyStrukturalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJabatanTugasPtkRelatedByJabatanPtkId($jabatanTugasPtk, $comparison = null)
    {
        if ($jabatanTugasPtk instanceof JabatanTugasPtk) {
            return $this
                ->addUsingAlias(RwyStrukturalPeer::JABATAN_PTK_ID, $jabatanTugasPtk->getJabatanPtkId(), $comparison);
        } elseif ($jabatanTugasPtk instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RwyStrukturalPeer::JABATAN_PTK_ID, $jabatanTugasPtk->toKeyValue('PrimaryKey', 'JabatanPtkId'), $comparison);
        } else {
            throw new PropelException('filterByJabatanTugasPtkRelatedByJabatanPtkId() only accepts arguments of type JabatanTugasPtk or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the JabatanTugasPtkRelatedByJabatanPtkId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RwyStrukturalQuery The current query, for fluid interface
     */
    public function joinJabatanTugasPtkRelatedByJabatanPtkId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('JabatanTugasPtkRelatedByJabatanPtkId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'JabatanTugasPtkRelatedByJabatanPtkId');
        }

        return $this;
    }

    /**
     * Use the JabatanTugasPtkRelatedByJabatanPtkId relation JabatanTugasPtk object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\JabatanTugasPtkQuery A secondary query class using the current class as primary query
     */
    public function useJabatanTugasPtkRelatedByJabatanPtkIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinJabatanTugasPtkRelatedByJabatanPtkId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'JabatanTugasPtkRelatedByJabatanPtkId', '\angulex\Model\JabatanTugasPtkQuery');
    }

    /**
     * Filter the query by a related VldRwyStruktural object
     *
     * @param   VldRwyStruktural|PropelObjectCollection $vldRwyStruktural  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RwyStrukturalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldRwyStrukturalRelatedByRiwayatStrukturalId($vldRwyStruktural, $comparison = null)
    {
        if ($vldRwyStruktural instanceof VldRwyStruktural) {
            return $this
                ->addUsingAlias(RwyStrukturalPeer::RIWAYAT_STRUKTURAL_ID, $vldRwyStruktural->getRiwayatStrukturalId(), $comparison);
        } elseif ($vldRwyStruktural instanceof PropelObjectCollection) {
            return $this
                ->useVldRwyStrukturalRelatedByRiwayatStrukturalIdQuery()
                ->filterByPrimaryKeys($vldRwyStruktural->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldRwyStrukturalRelatedByRiwayatStrukturalId() only accepts arguments of type VldRwyStruktural or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldRwyStrukturalRelatedByRiwayatStrukturalId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RwyStrukturalQuery The current query, for fluid interface
     */
    public function joinVldRwyStrukturalRelatedByRiwayatStrukturalId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldRwyStrukturalRelatedByRiwayatStrukturalId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldRwyStrukturalRelatedByRiwayatStrukturalId');
        }

        return $this;
    }

    /**
     * Use the VldRwyStrukturalRelatedByRiwayatStrukturalId relation VldRwyStruktural object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldRwyStrukturalQuery A secondary query class using the current class as primary query
     */
    public function useVldRwyStrukturalRelatedByRiwayatStrukturalIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldRwyStrukturalRelatedByRiwayatStrukturalId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldRwyStrukturalRelatedByRiwayatStrukturalId', '\angulex\Model\VldRwyStrukturalQuery');
    }

    /**
     * Filter the query by a related VldRwyStruktural object
     *
     * @param   VldRwyStruktural|PropelObjectCollection $vldRwyStruktural  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RwyStrukturalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldRwyStrukturalRelatedByRiwayatStrukturalId($vldRwyStruktural, $comparison = null)
    {
        if ($vldRwyStruktural instanceof VldRwyStruktural) {
            return $this
                ->addUsingAlias(RwyStrukturalPeer::RIWAYAT_STRUKTURAL_ID, $vldRwyStruktural->getRiwayatStrukturalId(), $comparison);
        } elseif ($vldRwyStruktural instanceof PropelObjectCollection) {
            return $this
                ->useVldRwyStrukturalRelatedByRiwayatStrukturalIdQuery()
                ->filterByPrimaryKeys($vldRwyStruktural->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldRwyStrukturalRelatedByRiwayatStrukturalId() only accepts arguments of type VldRwyStruktural or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldRwyStrukturalRelatedByRiwayatStrukturalId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RwyStrukturalQuery The current query, for fluid interface
     */
    public function joinVldRwyStrukturalRelatedByRiwayatStrukturalId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldRwyStrukturalRelatedByRiwayatStrukturalId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldRwyStrukturalRelatedByRiwayatStrukturalId');
        }

        return $this;
    }

    /**
     * Use the VldRwyStrukturalRelatedByRiwayatStrukturalId relation VldRwyStruktural object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldRwyStrukturalQuery A secondary query class using the current class as primary query
     */
    public function useVldRwyStrukturalRelatedByRiwayatStrukturalIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldRwyStrukturalRelatedByRiwayatStrukturalId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldRwyStrukturalRelatedByRiwayatStrukturalId', '\angulex\Model\VldRwyStrukturalQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   RwyStruktural $rwyStruktural Object to remove from the list of results
     *
     * @return RwyStrukturalQuery The current query, for fluid interface
     */
    public function prune($rwyStruktural = null)
    {
        if ($rwyStruktural) {
            $this->addUsingAlias(RwyStrukturalPeer::RIWAYAT_STRUKTURAL_ID, $rwyStruktural->getRiwayatStrukturalId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
