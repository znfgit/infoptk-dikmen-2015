<?php

namespace angulex\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelDateTime;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\BidangStudi;
use angulex\Model\BidangStudiPeer;
use angulex\Model\BidangStudiQuery;
use angulex\Model\MapBidangMataPelajaran;
use angulex\Model\MapBidangMataPelajaranQuery;
use angulex\Model\PengawasTerdaftar;
use angulex\Model\PengawasTerdaftarQuery;
use angulex\Model\Ptk;
use angulex\Model\PtkQuery;
use angulex\Model\RwyPendFormal;
use angulex\Model\RwyPendFormalQuery;
use angulex\Model\RwySertifikasi;
use angulex\Model\RwySertifikasiQuery;

/**
 * Base class that represents a row from the 'ref.bidang_studi' table.
 *
 * 
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseBidangStudi extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'angulex\\Model\\BidangStudiPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        BidangStudiPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the bidang_studi_id field.
     * @var        int
     */
    protected $bidang_studi_id;

    /**
     * The value for the kelompok_bidang_studi_id field.
     * @var        int
     */
    protected $kelompok_bidang_studi_id;

    /**
     * The value for the kode field.
     * @var        string
     */
    protected $kode;

    /**
     * The value for the bidang_studi field.
     * @var        string
     */
    protected $bidang_studi;

    /**
     * The value for the kelompok field.
     * @var        string
     */
    protected $kelompok;

    /**
     * The value for the jenjang_paud field.
     * @var        string
     */
    protected $jenjang_paud;

    /**
     * The value for the jenjang_tk field.
     * @var        string
     */
    protected $jenjang_tk;

    /**
     * The value for the jenjang_sd field.
     * @var        string
     */
    protected $jenjang_sd;

    /**
     * The value for the jenjang_smp field.
     * @var        string
     */
    protected $jenjang_smp;

    /**
     * The value for the jenjang_sma field.
     * @var        string
     */
    protected $jenjang_sma;

    /**
     * The value for the jenjang_tinggi field.
     * @var        string
     */
    protected $jenjang_tinggi;

    /**
     * The value for the create_date field.
     * @var        string
     */
    protected $create_date;

    /**
     * The value for the last_update field.
     * @var        string
     */
    protected $last_update;

    /**
     * The value for the expired_date field.
     * @var        string
     */
    protected $expired_date;

    /**
     * The value for the last_sync field.
     * @var        string
     */
    protected $last_sync;

    /**
     * @var        BidangStudi
     */
    protected $aBidangStudiRelatedByKelompokBidangStudiId;

    /**
     * @var        PropelObjectCollection|MapBidangMataPelajaran[] Collection to store aggregation of MapBidangMataPelajaran objects.
     */
    protected $collMapBidangMataPelajarans;
    protected $collMapBidangMataPelajaransPartial;

    /**
     * @var        PropelObjectCollection|BidangStudi[] Collection to store aggregation of BidangStudi objects.
     */
    protected $collBidangStudisRelatedByBidangStudiId;
    protected $collBidangStudisRelatedByBidangStudiIdPartial;

    /**
     * @var        PropelObjectCollection|Ptk[] Collection to store aggregation of Ptk objects.
     */
    protected $collPtksRelatedByPengawasBidangStudiId;
    protected $collPtksRelatedByPengawasBidangStudiIdPartial;

    /**
     * @var        PropelObjectCollection|Ptk[] Collection to store aggregation of Ptk objects.
     */
    protected $collPtksRelatedByPengawasBidangStudiId;
    protected $collPtksRelatedByPengawasBidangStudiIdPartial;

    /**
     * @var        PropelObjectCollection|PengawasTerdaftar[] Collection to store aggregation of PengawasTerdaftar objects.
     */
    protected $collPengawasTerdaftarsRelatedByBidangStudiId;
    protected $collPengawasTerdaftarsRelatedByBidangStudiIdPartial;

    /**
     * @var        PropelObjectCollection|PengawasTerdaftar[] Collection to store aggregation of PengawasTerdaftar objects.
     */
    protected $collPengawasTerdaftarsRelatedByBidangStudiId;
    protected $collPengawasTerdaftarsRelatedByBidangStudiIdPartial;

    /**
     * @var        PropelObjectCollection|RwyPendFormal[] Collection to store aggregation of RwyPendFormal objects.
     */
    protected $collRwyPendFormalsRelatedByBidangStudiId;
    protected $collRwyPendFormalsRelatedByBidangStudiIdPartial;

    /**
     * @var        PropelObjectCollection|RwyPendFormal[] Collection to store aggregation of RwyPendFormal objects.
     */
    protected $collRwyPendFormalsRelatedByBidangStudiId;
    protected $collRwyPendFormalsRelatedByBidangStudiIdPartial;

    /**
     * @var        PropelObjectCollection|RwySertifikasi[] Collection to store aggregation of RwySertifikasi objects.
     */
    protected $collRwySertifikasisRelatedByBidangStudiId;
    protected $collRwySertifikasisRelatedByBidangStudiIdPartial;

    /**
     * @var        PropelObjectCollection|RwySertifikasi[] Collection to store aggregation of RwySertifikasi objects.
     */
    protected $collRwySertifikasisRelatedByBidangStudiId;
    protected $collRwySertifikasisRelatedByBidangStudiIdPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $mapBidangMataPelajaransScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $bidangStudisRelatedByBidangStudiIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $ptksRelatedByPengawasBidangStudiIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $ptksRelatedByPengawasBidangStudiIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $pengawasTerdaftarsRelatedByBidangStudiIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $pengawasTerdaftarsRelatedByBidangStudiIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rwyPendFormalsRelatedByBidangStudiIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rwyPendFormalsRelatedByBidangStudiIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rwySertifikasisRelatedByBidangStudiIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rwySertifikasisRelatedByBidangStudiIdScheduledForDeletion = null;

    /**
     * Get the [bidang_studi_id] column value.
     * 
     * @return int
     */
    public function getBidangStudiId()
    {
        return $this->bidang_studi_id;
    }

    /**
     * Get the [kelompok_bidang_studi_id] column value.
     * 
     * @return int
     */
    public function getKelompokBidangStudiId()
    {
        return $this->kelompok_bidang_studi_id;
    }

    /**
     * Get the [kode] column value.
     * 
     * @return string
     */
    public function getKode()
    {
        return $this->kode;
    }

    /**
     * Get the [bidang_studi] column value.
     * 
     * @return string
     */
    public function getBidangStudi()
    {
        return $this->bidang_studi;
    }

    /**
     * Get the [kelompok] column value.
     * 
     * @return string
     */
    public function getKelompok()
    {
        return $this->kelompok;
    }

    /**
     * Get the [jenjang_paud] column value.
     * 
     * @return string
     */
    public function getJenjangPaud()
    {
        return $this->jenjang_paud;
    }

    /**
     * Get the [jenjang_tk] column value.
     * 
     * @return string
     */
    public function getJenjangTk()
    {
        return $this->jenjang_tk;
    }

    /**
     * Get the [jenjang_sd] column value.
     * 
     * @return string
     */
    public function getJenjangSd()
    {
        return $this->jenjang_sd;
    }

    /**
     * Get the [jenjang_smp] column value.
     * 
     * @return string
     */
    public function getJenjangSmp()
    {
        return $this->jenjang_smp;
    }

    /**
     * Get the [jenjang_sma] column value.
     * 
     * @return string
     */
    public function getJenjangSma()
    {
        return $this->jenjang_sma;
    }

    /**
     * Get the [jenjang_tinggi] column value.
     * 
     * @return string
     */
    public function getJenjangTinggi()
    {
        return $this->jenjang_tinggi;
    }

    /**
     * Get the [optionally formatted] temporal [create_date] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreateDate($format = 'Y-m-d H:i:s')
    {
        if ($this->create_date === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->create_date);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->create_date, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [last_update] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastUpdate($format = 'Y-m-d H:i:s')
    {
        if ($this->last_update === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_update);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_update, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [expired_date] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getExpiredDate($format = 'Y-m-d H:i:s')
    {
        if ($this->expired_date === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->expired_date);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->expired_date, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [last_sync] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastSync($format = 'Y-m-d H:i:s')
    {
        if ($this->last_sync === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_sync);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_sync, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Set the value of [bidang_studi_id] column.
     * 
     * @param int $v new value
     * @return BidangStudi The current object (for fluent API support)
     */
    public function setBidangStudiId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->bidang_studi_id !== $v) {
            $this->bidang_studi_id = $v;
            $this->modifiedColumns[] = BidangStudiPeer::BIDANG_STUDI_ID;
        }


        return $this;
    } // setBidangStudiId()

    /**
     * Set the value of [kelompok_bidang_studi_id] column.
     * 
     * @param int $v new value
     * @return BidangStudi The current object (for fluent API support)
     */
    public function setKelompokBidangStudiId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->kelompok_bidang_studi_id !== $v) {
            $this->kelompok_bidang_studi_id = $v;
            $this->modifiedColumns[] = BidangStudiPeer::KELOMPOK_BIDANG_STUDI_ID;
        }

        if ($this->aBidangStudiRelatedByKelompokBidangStudiId !== null && $this->aBidangStudiRelatedByKelompokBidangStudiId->getBidangStudiId() !== $v) {
            $this->aBidangStudiRelatedByKelompokBidangStudiId = null;
        }


        return $this;
    } // setKelompokBidangStudiId()

    /**
     * Set the value of [kode] column.
     * 
     * @param string $v new value
     * @return BidangStudi The current object (for fluent API support)
     */
    public function setKode($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->kode !== $v) {
            $this->kode = $v;
            $this->modifiedColumns[] = BidangStudiPeer::KODE;
        }


        return $this;
    } // setKode()

    /**
     * Set the value of [bidang_studi] column.
     * 
     * @param string $v new value
     * @return BidangStudi The current object (for fluent API support)
     */
    public function setBidangStudi($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->bidang_studi !== $v) {
            $this->bidang_studi = $v;
            $this->modifiedColumns[] = BidangStudiPeer::BIDANG_STUDI;
        }


        return $this;
    } // setBidangStudi()

    /**
     * Set the value of [kelompok] column.
     * 
     * @param string $v new value
     * @return BidangStudi The current object (for fluent API support)
     */
    public function setKelompok($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->kelompok !== $v) {
            $this->kelompok = $v;
            $this->modifiedColumns[] = BidangStudiPeer::KELOMPOK;
        }


        return $this;
    } // setKelompok()

    /**
     * Set the value of [jenjang_paud] column.
     * 
     * @param string $v new value
     * @return BidangStudi The current object (for fluent API support)
     */
    public function setJenjangPaud($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->jenjang_paud !== $v) {
            $this->jenjang_paud = $v;
            $this->modifiedColumns[] = BidangStudiPeer::JENJANG_PAUD;
        }


        return $this;
    } // setJenjangPaud()

    /**
     * Set the value of [jenjang_tk] column.
     * 
     * @param string $v new value
     * @return BidangStudi The current object (for fluent API support)
     */
    public function setJenjangTk($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->jenjang_tk !== $v) {
            $this->jenjang_tk = $v;
            $this->modifiedColumns[] = BidangStudiPeer::JENJANG_TK;
        }


        return $this;
    } // setJenjangTk()

    /**
     * Set the value of [jenjang_sd] column.
     * 
     * @param string $v new value
     * @return BidangStudi The current object (for fluent API support)
     */
    public function setJenjangSd($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->jenjang_sd !== $v) {
            $this->jenjang_sd = $v;
            $this->modifiedColumns[] = BidangStudiPeer::JENJANG_SD;
        }


        return $this;
    } // setJenjangSd()

    /**
     * Set the value of [jenjang_smp] column.
     * 
     * @param string $v new value
     * @return BidangStudi The current object (for fluent API support)
     */
    public function setJenjangSmp($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->jenjang_smp !== $v) {
            $this->jenjang_smp = $v;
            $this->modifiedColumns[] = BidangStudiPeer::JENJANG_SMP;
        }


        return $this;
    } // setJenjangSmp()

    /**
     * Set the value of [jenjang_sma] column.
     * 
     * @param string $v new value
     * @return BidangStudi The current object (for fluent API support)
     */
    public function setJenjangSma($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->jenjang_sma !== $v) {
            $this->jenjang_sma = $v;
            $this->modifiedColumns[] = BidangStudiPeer::JENJANG_SMA;
        }


        return $this;
    } // setJenjangSma()

    /**
     * Set the value of [jenjang_tinggi] column.
     * 
     * @param string $v new value
     * @return BidangStudi The current object (for fluent API support)
     */
    public function setJenjangTinggi($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->jenjang_tinggi !== $v) {
            $this->jenjang_tinggi = $v;
            $this->modifiedColumns[] = BidangStudiPeer::JENJANG_TINGGI;
        }


        return $this;
    } // setJenjangTinggi()

    /**
     * Sets the value of [create_date] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return BidangStudi The current object (for fluent API support)
     */
    public function setCreateDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->create_date !== null || $dt !== null) {
            $currentDateAsString = ($this->create_date !== null && $tmpDt = new DateTime($this->create_date)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->create_date = $newDateAsString;
                $this->modifiedColumns[] = BidangStudiPeer::CREATE_DATE;
            }
        } // if either are not null


        return $this;
    } // setCreateDate()

    /**
     * Sets the value of [last_update] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return BidangStudi The current object (for fluent API support)
     */
    public function setLastUpdate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_update !== null || $dt !== null) {
            $currentDateAsString = ($this->last_update !== null && $tmpDt = new DateTime($this->last_update)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_update = $newDateAsString;
                $this->modifiedColumns[] = BidangStudiPeer::LAST_UPDATE;
            }
        } // if either are not null


        return $this;
    } // setLastUpdate()

    /**
     * Sets the value of [expired_date] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return BidangStudi The current object (for fluent API support)
     */
    public function setExpiredDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->expired_date !== null || $dt !== null) {
            $currentDateAsString = ($this->expired_date !== null && $tmpDt = new DateTime($this->expired_date)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->expired_date = $newDateAsString;
                $this->modifiedColumns[] = BidangStudiPeer::EXPIRED_DATE;
            }
        } // if either are not null


        return $this;
    } // setExpiredDate()

    /**
     * Sets the value of [last_sync] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return BidangStudi The current object (for fluent API support)
     */
    public function setLastSync($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_sync !== null || $dt !== null) {
            $currentDateAsString = ($this->last_sync !== null && $tmpDt = new DateTime($this->last_sync)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_sync = $newDateAsString;
                $this->modifiedColumns[] = BidangStudiPeer::LAST_SYNC;
            }
        } // if either are not null


        return $this;
    } // setLastSync()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->bidang_studi_id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->kelompok_bidang_studi_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->kode = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->bidang_studi = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->kelompok = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->jenjang_paud = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->jenjang_tk = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->jenjang_sd = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->jenjang_smp = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->jenjang_sma = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->jenjang_tinggi = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->create_date = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->last_update = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->expired_date = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->last_sync = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 15; // 15 = BidangStudiPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating BidangStudi object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aBidangStudiRelatedByKelompokBidangStudiId !== null && $this->kelompok_bidang_studi_id !== $this->aBidangStudiRelatedByKelompokBidangStudiId->getBidangStudiId()) {
            $this->aBidangStudiRelatedByKelompokBidangStudiId = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(BidangStudiPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = BidangStudiPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aBidangStudiRelatedByKelompokBidangStudiId = null;
            $this->collMapBidangMataPelajarans = null;

            $this->collBidangStudisRelatedByBidangStudiId = null;

            $this->collPtksRelatedByPengawasBidangStudiId = null;

            $this->collPtksRelatedByPengawasBidangStudiId = null;

            $this->collPengawasTerdaftarsRelatedByBidangStudiId = null;

            $this->collPengawasTerdaftarsRelatedByBidangStudiId = null;

            $this->collRwyPendFormalsRelatedByBidangStudiId = null;

            $this->collRwyPendFormalsRelatedByBidangStudiId = null;

            $this->collRwySertifikasisRelatedByBidangStudiId = null;

            $this->collRwySertifikasisRelatedByBidangStudiId = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(BidangStudiPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = BidangStudiQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(BidangStudiPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                BidangStudiPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aBidangStudiRelatedByKelompokBidangStudiId !== null) {
                if ($this->aBidangStudiRelatedByKelompokBidangStudiId->isModified() || $this->aBidangStudiRelatedByKelompokBidangStudiId->isNew()) {
                    $affectedRows += $this->aBidangStudiRelatedByKelompokBidangStudiId->save($con);
                }
                $this->setBidangStudiRelatedByKelompokBidangStudiId($this->aBidangStudiRelatedByKelompokBidangStudiId);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->mapBidangMataPelajaransScheduledForDeletion !== null) {
                if (!$this->mapBidangMataPelajaransScheduledForDeletion->isEmpty()) {
                    MapBidangMataPelajaranQuery::create()
                        ->filterByPrimaryKeys($this->mapBidangMataPelajaransScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->mapBidangMataPelajaransScheduledForDeletion = null;
                }
            }

            if ($this->collMapBidangMataPelajarans !== null) {
                foreach ($this->collMapBidangMataPelajarans as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->bidangStudisRelatedByBidangStudiIdScheduledForDeletion !== null) {
                if (!$this->bidangStudisRelatedByBidangStudiIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->bidangStudisRelatedByBidangStudiIdScheduledForDeletion as $bidangStudiRelatedByBidangStudiId) {
                        // need to save related object because we set the relation to null
                        $bidangStudiRelatedByBidangStudiId->save($con);
                    }
                    $this->bidangStudisRelatedByBidangStudiIdScheduledForDeletion = null;
                }
            }

            if ($this->collBidangStudisRelatedByBidangStudiId !== null) {
                foreach ($this->collBidangStudisRelatedByBidangStudiId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->ptksRelatedByPengawasBidangStudiIdScheduledForDeletion !== null) {
                if (!$this->ptksRelatedByPengawasBidangStudiIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->ptksRelatedByPengawasBidangStudiIdScheduledForDeletion as $ptkRelatedByPengawasBidangStudiId) {
                        // need to save related object because we set the relation to null
                        $ptkRelatedByPengawasBidangStudiId->save($con);
                    }
                    $this->ptksRelatedByPengawasBidangStudiIdScheduledForDeletion = null;
                }
            }

            if ($this->collPtksRelatedByPengawasBidangStudiId !== null) {
                foreach ($this->collPtksRelatedByPengawasBidangStudiId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->ptksRelatedByPengawasBidangStudiIdScheduledForDeletion !== null) {
                if (!$this->ptksRelatedByPengawasBidangStudiIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->ptksRelatedByPengawasBidangStudiIdScheduledForDeletion as $ptkRelatedByPengawasBidangStudiId) {
                        // need to save related object because we set the relation to null
                        $ptkRelatedByPengawasBidangStudiId->save($con);
                    }
                    $this->ptksRelatedByPengawasBidangStudiIdScheduledForDeletion = null;
                }
            }

            if ($this->collPtksRelatedByPengawasBidangStudiId !== null) {
                foreach ($this->collPtksRelatedByPengawasBidangStudiId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->pengawasTerdaftarsRelatedByBidangStudiIdScheduledForDeletion !== null) {
                if (!$this->pengawasTerdaftarsRelatedByBidangStudiIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->pengawasTerdaftarsRelatedByBidangStudiIdScheduledForDeletion as $pengawasTerdaftarRelatedByBidangStudiId) {
                        // need to save related object because we set the relation to null
                        $pengawasTerdaftarRelatedByBidangStudiId->save($con);
                    }
                    $this->pengawasTerdaftarsRelatedByBidangStudiIdScheduledForDeletion = null;
                }
            }

            if ($this->collPengawasTerdaftarsRelatedByBidangStudiId !== null) {
                foreach ($this->collPengawasTerdaftarsRelatedByBidangStudiId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->pengawasTerdaftarsRelatedByBidangStudiIdScheduledForDeletion !== null) {
                if (!$this->pengawasTerdaftarsRelatedByBidangStudiIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->pengawasTerdaftarsRelatedByBidangStudiIdScheduledForDeletion as $pengawasTerdaftarRelatedByBidangStudiId) {
                        // need to save related object because we set the relation to null
                        $pengawasTerdaftarRelatedByBidangStudiId->save($con);
                    }
                    $this->pengawasTerdaftarsRelatedByBidangStudiIdScheduledForDeletion = null;
                }
            }

            if ($this->collPengawasTerdaftarsRelatedByBidangStudiId !== null) {
                foreach ($this->collPengawasTerdaftarsRelatedByBidangStudiId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rwyPendFormalsRelatedByBidangStudiIdScheduledForDeletion !== null) {
                if (!$this->rwyPendFormalsRelatedByBidangStudiIdScheduledForDeletion->isEmpty()) {
                    RwyPendFormalQuery::create()
                        ->filterByPrimaryKeys($this->rwyPendFormalsRelatedByBidangStudiIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rwyPendFormalsRelatedByBidangStudiIdScheduledForDeletion = null;
                }
            }

            if ($this->collRwyPendFormalsRelatedByBidangStudiId !== null) {
                foreach ($this->collRwyPendFormalsRelatedByBidangStudiId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rwyPendFormalsRelatedByBidangStudiIdScheduledForDeletion !== null) {
                if (!$this->rwyPendFormalsRelatedByBidangStudiIdScheduledForDeletion->isEmpty()) {
                    RwyPendFormalQuery::create()
                        ->filterByPrimaryKeys($this->rwyPendFormalsRelatedByBidangStudiIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rwyPendFormalsRelatedByBidangStudiIdScheduledForDeletion = null;
                }
            }

            if ($this->collRwyPendFormalsRelatedByBidangStudiId !== null) {
                foreach ($this->collRwyPendFormalsRelatedByBidangStudiId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rwySertifikasisRelatedByBidangStudiIdScheduledForDeletion !== null) {
                if (!$this->rwySertifikasisRelatedByBidangStudiIdScheduledForDeletion->isEmpty()) {
                    RwySertifikasiQuery::create()
                        ->filterByPrimaryKeys($this->rwySertifikasisRelatedByBidangStudiIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rwySertifikasisRelatedByBidangStudiIdScheduledForDeletion = null;
                }
            }

            if ($this->collRwySertifikasisRelatedByBidangStudiId !== null) {
                foreach ($this->collRwySertifikasisRelatedByBidangStudiId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rwySertifikasisRelatedByBidangStudiIdScheduledForDeletion !== null) {
                if (!$this->rwySertifikasisRelatedByBidangStudiIdScheduledForDeletion->isEmpty()) {
                    RwySertifikasiQuery::create()
                        ->filterByPrimaryKeys($this->rwySertifikasisRelatedByBidangStudiIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rwySertifikasisRelatedByBidangStudiIdScheduledForDeletion = null;
                }
            }

            if ($this->collRwySertifikasisRelatedByBidangStudiId !== null) {
                foreach ($this->collRwySertifikasisRelatedByBidangStudiId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $criteria = $this->buildCriteria();
        $pk = BasePeer::doInsert($criteria, $con);
        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aBidangStudiRelatedByKelompokBidangStudiId !== null) {
                if (!$this->aBidangStudiRelatedByKelompokBidangStudiId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aBidangStudiRelatedByKelompokBidangStudiId->getValidationFailures());
                }
            }


            if (($retval = BidangStudiPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collMapBidangMataPelajarans !== null) {
                    foreach ($this->collMapBidangMataPelajarans as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collBidangStudisRelatedByBidangStudiId !== null) {
                    foreach ($this->collBidangStudisRelatedByBidangStudiId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPtksRelatedByPengawasBidangStudiId !== null) {
                    foreach ($this->collPtksRelatedByPengawasBidangStudiId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPtksRelatedByPengawasBidangStudiId !== null) {
                    foreach ($this->collPtksRelatedByPengawasBidangStudiId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPengawasTerdaftarsRelatedByBidangStudiId !== null) {
                    foreach ($this->collPengawasTerdaftarsRelatedByBidangStudiId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPengawasTerdaftarsRelatedByBidangStudiId !== null) {
                    foreach ($this->collPengawasTerdaftarsRelatedByBidangStudiId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRwyPendFormalsRelatedByBidangStudiId !== null) {
                    foreach ($this->collRwyPendFormalsRelatedByBidangStudiId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRwyPendFormalsRelatedByBidangStudiId !== null) {
                    foreach ($this->collRwyPendFormalsRelatedByBidangStudiId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRwySertifikasisRelatedByBidangStudiId !== null) {
                    foreach ($this->collRwySertifikasisRelatedByBidangStudiId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRwySertifikasisRelatedByBidangStudiId !== null) {
                    foreach ($this->collRwySertifikasisRelatedByBidangStudiId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = BidangStudiPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getBidangStudiId();
                break;
            case 1:
                return $this->getKelompokBidangStudiId();
                break;
            case 2:
                return $this->getKode();
                break;
            case 3:
                return $this->getBidangStudi();
                break;
            case 4:
                return $this->getKelompok();
                break;
            case 5:
                return $this->getJenjangPaud();
                break;
            case 6:
                return $this->getJenjangTk();
                break;
            case 7:
                return $this->getJenjangSd();
                break;
            case 8:
                return $this->getJenjangSmp();
                break;
            case 9:
                return $this->getJenjangSma();
                break;
            case 10:
                return $this->getJenjangTinggi();
                break;
            case 11:
                return $this->getCreateDate();
                break;
            case 12:
                return $this->getLastUpdate();
                break;
            case 13:
                return $this->getExpiredDate();
                break;
            case 14:
                return $this->getLastSync();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['BidangStudi'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['BidangStudi'][$this->getPrimaryKey()] = true;
        $keys = BidangStudiPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getBidangStudiId(),
            $keys[1] => $this->getKelompokBidangStudiId(),
            $keys[2] => $this->getKode(),
            $keys[3] => $this->getBidangStudi(),
            $keys[4] => $this->getKelompok(),
            $keys[5] => $this->getJenjangPaud(),
            $keys[6] => $this->getJenjangTk(),
            $keys[7] => $this->getJenjangSd(),
            $keys[8] => $this->getJenjangSmp(),
            $keys[9] => $this->getJenjangSma(),
            $keys[10] => $this->getJenjangTinggi(),
            $keys[11] => $this->getCreateDate(),
            $keys[12] => $this->getLastUpdate(),
            $keys[13] => $this->getExpiredDate(),
            $keys[14] => $this->getLastSync(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aBidangStudiRelatedByKelompokBidangStudiId) {
                $result['BidangStudiRelatedByKelompokBidangStudiId'] = $this->aBidangStudiRelatedByKelompokBidangStudiId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collMapBidangMataPelajarans) {
                $result['MapBidangMataPelajarans'] = $this->collMapBidangMataPelajarans->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collBidangStudisRelatedByBidangStudiId) {
                $result['BidangStudisRelatedByBidangStudiId'] = $this->collBidangStudisRelatedByBidangStudiId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPtksRelatedByPengawasBidangStudiId) {
                $result['PtksRelatedByPengawasBidangStudiId'] = $this->collPtksRelatedByPengawasBidangStudiId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPtksRelatedByPengawasBidangStudiId) {
                $result['PtksRelatedByPengawasBidangStudiId'] = $this->collPtksRelatedByPengawasBidangStudiId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPengawasTerdaftarsRelatedByBidangStudiId) {
                $result['PengawasTerdaftarsRelatedByBidangStudiId'] = $this->collPengawasTerdaftarsRelatedByBidangStudiId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPengawasTerdaftarsRelatedByBidangStudiId) {
                $result['PengawasTerdaftarsRelatedByBidangStudiId'] = $this->collPengawasTerdaftarsRelatedByBidangStudiId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRwyPendFormalsRelatedByBidangStudiId) {
                $result['RwyPendFormalsRelatedByBidangStudiId'] = $this->collRwyPendFormalsRelatedByBidangStudiId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRwyPendFormalsRelatedByBidangStudiId) {
                $result['RwyPendFormalsRelatedByBidangStudiId'] = $this->collRwyPendFormalsRelatedByBidangStudiId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRwySertifikasisRelatedByBidangStudiId) {
                $result['RwySertifikasisRelatedByBidangStudiId'] = $this->collRwySertifikasisRelatedByBidangStudiId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRwySertifikasisRelatedByBidangStudiId) {
                $result['RwySertifikasisRelatedByBidangStudiId'] = $this->collRwySertifikasisRelatedByBidangStudiId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = BidangStudiPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setBidangStudiId($value);
                break;
            case 1:
                $this->setKelompokBidangStudiId($value);
                break;
            case 2:
                $this->setKode($value);
                break;
            case 3:
                $this->setBidangStudi($value);
                break;
            case 4:
                $this->setKelompok($value);
                break;
            case 5:
                $this->setJenjangPaud($value);
                break;
            case 6:
                $this->setJenjangTk($value);
                break;
            case 7:
                $this->setJenjangSd($value);
                break;
            case 8:
                $this->setJenjangSmp($value);
                break;
            case 9:
                $this->setJenjangSma($value);
                break;
            case 10:
                $this->setJenjangTinggi($value);
                break;
            case 11:
                $this->setCreateDate($value);
                break;
            case 12:
                $this->setLastUpdate($value);
                break;
            case 13:
                $this->setExpiredDate($value);
                break;
            case 14:
                $this->setLastSync($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = BidangStudiPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setBidangStudiId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setKelompokBidangStudiId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setKode($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setBidangStudi($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setKelompok($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setJenjangPaud($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setJenjangTk($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setJenjangSd($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setJenjangSmp($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setJenjangSma($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setJenjangTinggi($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setCreateDate($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setLastUpdate($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setExpiredDate($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setLastSync($arr[$keys[14]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(BidangStudiPeer::DATABASE_NAME);

        if ($this->isColumnModified(BidangStudiPeer::BIDANG_STUDI_ID)) $criteria->add(BidangStudiPeer::BIDANG_STUDI_ID, $this->bidang_studi_id);
        if ($this->isColumnModified(BidangStudiPeer::KELOMPOK_BIDANG_STUDI_ID)) $criteria->add(BidangStudiPeer::KELOMPOK_BIDANG_STUDI_ID, $this->kelompok_bidang_studi_id);
        if ($this->isColumnModified(BidangStudiPeer::KODE)) $criteria->add(BidangStudiPeer::KODE, $this->kode);
        if ($this->isColumnModified(BidangStudiPeer::BIDANG_STUDI)) $criteria->add(BidangStudiPeer::BIDANG_STUDI, $this->bidang_studi);
        if ($this->isColumnModified(BidangStudiPeer::KELOMPOK)) $criteria->add(BidangStudiPeer::KELOMPOK, $this->kelompok);
        if ($this->isColumnModified(BidangStudiPeer::JENJANG_PAUD)) $criteria->add(BidangStudiPeer::JENJANG_PAUD, $this->jenjang_paud);
        if ($this->isColumnModified(BidangStudiPeer::JENJANG_TK)) $criteria->add(BidangStudiPeer::JENJANG_TK, $this->jenjang_tk);
        if ($this->isColumnModified(BidangStudiPeer::JENJANG_SD)) $criteria->add(BidangStudiPeer::JENJANG_SD, $this->jenjang_sd);
        if ($this->isColumnModified(BidangStudiPeer::JENJANG_SMP)) $criteria->add(BidangStudiPeer::JENJANG_SMP, $this->jenjang_smp);
        if ($this->isColumnModified(BidangStudiPeer::JENJANG_SMA)) $criteria->add(BidangStudiPeer::JENJANG_SMA, $this->jenjang_sma);
        if ($this->isColumnModified(BidangStudiPeer::JENJANG_TINGGI)) $criteria->add(BidangStudiPeer::JENJANG_TINGGI, $this->jenjang_tinggi);
        if ($this->isColumnModified(BidangStudiPeer::CREATE_DATE)) $criteria->add(BidangStudiPeer::CREATE_DATE, $this->create_date);
        if ($this->isColumnModified(BidangStudiPeer::LAST_UPDATE)) $criteria->add(BidangStudiPeer::LAST_UPDATE, $this->last_update);
        if ($this->isColumnModified(BidangStudiPeer::EXPIRED_DATE)) $criteria->add(BidangStudiPeer::EXPIRED_DATE, $this->expired_date);
        if ($this->isColumnModified(BidangStudiPeer::LAST_SYNC)) $criteria->add(BidangStudiPeer::LAST_SYNC, $this->last_sync);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(BidangStudiPeer::DATABASE_NAME);
        $criteria->add(BidangStudiPeer::BIDANG_STUDI_ID, $this->bidang_studi_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getBidangStudiId();
    }

    /**
     * Generic method to set the primary key (bidang_studi_id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setBidangStudiId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getBidangStudiId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of BidangStudi (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setKelompokBidangStudiId($this->getKelompokBidangStudiId());
        $copyObj->setKode($this->getKode());
        $copyObj->setBidangStudi($this->getBidangStudi());
        $copyObj->setKelompok($this->getKelompok());
        $copyObj->setJenjangPaud($this->getJenjangPaud());
        $copyObj->setJenjangTk($this->getJenjangTk());
        $copyObj->setJenjangSd($this->getJenjangSd());
        $copyObj->setJenjangSmp($this->getJenjangSmp());
        $copyObj->setJenjangSma($this->getJenjangSma());
        $copyObj->setJenjangTinggi($this->getJenjangTinggi());
        $copyObj->setCreateDate($this->getCreateDate());
        $copyObj->setLastUpdate($this->getLastUpdate());
        $copyObj->setExpiredDate($this->getExpiredDate());
        $copyObj->setLastSync($this->getLastSync());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getMapBidangMataPelajarans() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addMapBidangMataPelajaran($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getBidangStudisRelatedByBidangStudiId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBidangStudiRelatedByBidangStudiId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPtksRelatedByPengawasBidangStudiId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPtkRelatedByPengawasBidangStudiId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPtksRelatedByPengawasBidangStudiId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPtkRelatedByPengawasBidangStudiId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPengawasTerdaftarsRelatedByBidangStudiId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPengawasTerdaftarRelatedByBidangStudiId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPengawasTerdaftarsRelatedByBidangStudiId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPengawasTerdaftarRelatedByBidangStudiId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRwyPendFormalsRelatedByBidangStudiId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRwyPendFormalRelatedByBidangStudiId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRwyPendFormalsRelatedByBidangStudiId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRwyPendFormalRelatedByBidangStudiId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRwySertifikasisRelatedByBidangStudiId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRwySertifikasiRelatedByBidangStudiId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRwySertifikasisRelatedByBidangStudiId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRwySertifikasiRelatedByBidangStudiId($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setBidangStudiId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return BidangStudi Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return BidangStudiPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new BidangStudiPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a BidangStudi object.
     *
     * @param             BidangStudi $v
     * @return BidangStudi The current object (for fluent API support)
     * @throws PropelException
     */
    public function setBidangStudiRelatedByKelompokBidangStudiId(BidangStudi $v = null)
    {
        if ($v === null) {
            $this->setKelompokBidangStudiId(NULL);
        } else {
            $this->setKelompokBidangStudiId($v->getBidangStudiId());
        }

        $this->aBidangStudiRelatedByKelompokBidangStudiId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the BidangStudi object, it will not be re-added.
        if ($v !== null) {
            $v->addBidangStudiRelatedByBidangStudiId($this);
        }


        return $this;
    }


    /**
     * Get the associated BidangStudi object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return BidangStudi The associated BidangStudi object.
     * @throws PropelException
     */
    public function getBidangStudiRelatedByKelompokBidangStudiId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aBidangStudiRelatedByKelompokBidangStudiId === null && ($this->kelompok_bidang_studi_id !== null) && $doQuery) {
            $this->aBidangStudiRelatedByKelompokBidangStudiId = BidangStudiQuery::create()->findPk($this->kelompok_bidang_studi_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aBidangStudiRelatedByKelompokBidangStudiId->addBidangStudisRelatedByBidangStudiId($this);
             */
        }

        return $this->aBidangStudiRelatedByKelompokBidangStudiId;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('MapBidangMataPelajaran' == $relationName) {
            $this->initMapBidangMataPelajarans();
        }
        if ('BidangStudiRelatedByBidangStudiId' == $relationName) {
            $this->initBidangStudisRelatedByBidangStudiId();
        }
        if ('PtkRelatedByPengawasBidangStudiId' == $relationName) {
            $this->initPtksRelatedByPengawasBidangStudiId();
        }
        if ('PtkRelatedByPengawasBidangStudiId' == $relationName) {
            $this->initPtksRelatedByPengawasBidangStudiId();
        }
        if ('PengawasTerdaftarRelatedByBidangStudiId' == $relationName) {
            $this->initPengawasTerdaftarsRelatedByBidangStudiId();
        }
        if ('PengawasTerdaftarRelatedByBidangStudiId' == $relationName) {
            $this->initPengawasTerdaftarsRelatedByBidangStudiId();
        }
        if ('RwyPendFormalRelatedByBidangStudiId' == $relationName) {
            $this->initRwyPendFormalsRelatedByBidangStudiId();
        }
        if ('RwyPendFormalRelatedByBidangStudiId' == $relationName) {
            $this->initRwyPendFormalsRelatedByBidangStudiId();
        }
        if ('RwySertifikasiRelatedByBidangStudiId' == $relationName) {
            $this->initRwySertifikasisRelatedByBidangStudiId();
        }
        if ('RwySertifikasiRelatedByBidangStudiId' == $relationName) {
            $this->initRwySertifikasisRelatedByBidangStudiId();
        }
    }

    /**
     * Clears out the collMapBidangMataPelajarans collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return BidangStudi The current object (for fluent API support)
     * @see        addMapBidangMataPelajarans()
     */
    public function clearMapBidangMataPelajarans()
    {
        $this->collMapBidangMataPelajarans = null; // important to set this to null since that means it is uninitialized
        $this->collMapBidangMataPelajaransPartial = null;

        return $this;
    }

    /**
     * reset is the collMapBidangMataPelajarans collection loaded partially
     *
     * @return void
     */
    public function resetPartialMapBidangMataPelajarans($v = true)
    {
        $this->collMapBidangMataPelajaransPartial = $v;
    }

    /**
     * Initializes the collMapBidangMataPelajarans collection.
     *
     * By default this just sets the collMapBidangMataPelajarans collection to an empty array (like clearcollMapBidangMataPelajarans());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initMapBidangMataPelajarans($overrideExisting = true)
    {
        if (null !== $this->collMapBidangMataPelajarans && !$overrideExisting) {
            return;
        }
        $this->collMapBidangMataPelajarans = new PropelObjectCollection();
        $this->collMapBidangMataPelajarans->setModel('MapBidangMataPelajaran');
    }

    /**
     * Gets an array of MapBidangMataPelajaran objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this BidangStudi is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|MapBidangMataPelajaran[] List of MapBidangMataPelajaran objects
     * @throws PropelException
     */
    public function getMapBidangMataPelajarans($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collMapBidangMataPelajaransPartial && !$this->isNew();
        if (null === $this->collMapBidangMataPelajarans || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collMapBidangMataPelajarans) {
                // return empty collection
                $this->initMapBidangMataPelajarans();
            } else {
                $collMapBidangMataPelajarans = MapBidangMataPelajaranQuery::create(null, $criteria)
                    ->filterByBidangStudi($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collMapBidangMataPelajaransPartial && count($collMapBidangMataPelajarans)) {
                      $this->initMapBidangMataPelajarans(false);

                      foreach($collMapBidangMataPelajarans as $obj) {
                        if (false == $this->collMapBidangMataPelajarans->contains($obj)) {
                          $this->collMapBidangMataPelajarans->append($obj);
                        }
                      }

                      $this->collMapBidangMataPelajaransPartial = true;
                    }

                    $collMapBidangMataPelajarans->getInternalIterator()->rewind();
                    return $collMapBidangMataPelajarans;
                }

                if($partial && $this->collMapBidangMataPelajarans) {
                    foreach($this->collMapBidangMataPelajarans as $obj) {
                        if($obj->isNew()) {
                            $collMapBidangMataPelajarans[] = $obj;
                        }
                    }
                }

                $this->collMapBidangMataPelajarans = $collMapBidangMataPelajarans;
                $this->collMapBidangMataPelajaransPartial = false;
            }
        }

        return $this->collMapBidangMataPelajarans;
    }

    /**
     * Sets a collection of MapBidangMataPelajaran objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $mapBidangMataPelajarans A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return BidangStudi The current object (for fluent API support)
     */
    public function setMapBidangMataPelajarans(PropelCollection $mapBidangMataPelajarans, PropelPDO $con = null)
    {
        $mapBidangMataPelajaransToDelete = $this->getMapBidangMataPelajarans(new Criteria(), $con)->diff($mapBidangMataPelajarans);

        $this->mapBidangMataPelajaransScheduledForDeletion = unserialize(serialize($mapBidangMataPelajaransToDelete));

        foreach ($mapBidangMataPelajaransToDelete as $mapBidangMataPelajaranRemoved) {
            $mapBidangMataPelajaranRemoved->setBidangStudi(null);
        }

        $this->collMapBidangMataPelajarans = null;
        foreach ($mapBidangMataPelajarans as $mapBidangMataPelajaran) {
            $this->addMapBidangMataPelajaran($mapBidangMataPelajaran);
        }

        $this->collMapBidangMataPelajarans = $mapBidangMataPelajarans;
        $this->collMapBidangMataPelajaransPartial = false;

        return $this;
    }

    /**
     * Returns the number of related MapBidangMataPelajaran objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related MapBidangMataPelajaran objects.
     * @throws PropelException
     */
    public function countMapBidangMataPelajarans(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collMapBidangMataPelajaransPartial && !$this->isNew();
        if (null === $this->collMapBidangMataPelajarans || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collMapBidangMataPelajarans) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getMapBidangMataPelajarans());
            }
            $query = MapBidangMataPelajaranQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByBidangStudi($this)
                ->count($con);
        }

        return count($this->collMapBidangMataPelajarans);
    }

    /**
     * Method called to associate a MapBidangMataPelajaran object to this object
     * through the MapBidangMataPelajaran foreign key attribute.
     *
     * @param    MapBidangMataPelajaran $l MapBidangMataPelajaran
     * @return BidangStudi The current object (for fluent API support)
     */
    public function addMapBidangMataPelajaran(MapBidangMataPelajaran $l)
    {
        if ($this->collMapBidangMataPelajarans === null) {
            $this->initMapBidangMataPelajarans();
            $this->collMapBidangMataPelajaransPartial = true;
        }
        if (!in_array($l, $this->collMapBidangMataPelajarans->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddMapBidangMataPelajaran($l);
        }

        return $this;
    }

    /**
     * @param	MapBidangMataPelajaran $mapBidangMataPelajaran The mapBidangMataPelajaran object to add.
     */
    protected function doAddMapBidangMataPelajaran($mapBidangMataPelajaran)
    {
        $this->collMapBidangMataPelajarans[]= $mapBidangMataPelajaran;
        $mapBidangMataPelajaran->setBidangStudi($this);
    }

    /**
     * @param	MapBidangMataPelajaran $mapBidangMataPelajaran The mapBidangMataPelajaran object to remove.
     * @return BidangStudi The current object (for fluent API support)
     */
    public function removeMapBidangMataPelajaran($mapBidangMataPelajaran)
    {
        if ($this->getMapBidangMataPelajarans()->contains($mapBidangMataPelajaran)) {
            $this->collMapBidangMataPelajarans->remove($this->collMapBidangMataPelajarans->search($mapBidangMataPelajaran));
            if (null === $this->mapBidangMataPelajaransScheduledForDeletion) {
                $this->mapBidangMataPelajaransScheduledForDeletion = clone $this->collMapBidangMataPelajarans;
                $this->mapBidangMataPelajaransScheduledForDeletion->clear();
            }
            $this->mapBidangMataPelajaransScheduledForDeletion[]= clone $mapBidangMataPelajaran;
            $mapBidangMataPelajaran->setBidangStudi(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related MapBidangMataPelajarans from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|MapBidangMataPelajaran[] List of MapBidangMataPelajaran objects
     */
    public function getMapBidangMataPelajaransJoinMataPelajaran($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = MapBidangMataPelajaranQuery::create(null, $criteria);
        $query->joinWith('MataPelajaran', $join_behavior);

        return $this->getMapBidangMataPelajarans($query, $con);
    }

    /**
     * Clears out the collBidangStudisRelatedByBidangStudiId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return BidangStudi The current object (for fluent API support)
     * @see        addBidangStudisRelatedByBidangStudiId()
     */
    public function clearBidangStudisRelatedByBidangStudiId()
    {
        $this->collBidangStudisRelatedByBidangStudiId = null; // important to set this to null since that means it is uninitialized
        $this->collBidangStudisRelatedByBidangStudiIdPartial = null;

        return $this;
    }

    /**
     * reset is the collBidangStudisRelatedByBidangStudiId collection loaded partially
     *
     * @return void
     */
    public function resetPartialBidangStudisRelatedByBidangStudiId($v = true)
    {
        $this->collBidangStudisRelatedByBidangStudiIdPartial = $v;
    }

    /**
     * Initializes the collBidangStudisRelatedByBidangStudiId collection.
     *
     * By default this just sets the collBidangStudisRelatedByBidangStudiId collection to an empty array (like clearcollBidangStudisRelatedByBidangStudiId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBidangStudisRelatedByBidangStudiId($overrideExisting = true)
    {
        if (null !== $this->collBidangStudisRelatedByBidangStudiId && !$overrideExisting) {
            return;
        }
        $this->collBidangStudisRelatedByBidangStudiId = new PropelObjectCollection();
        $this->collBidangStudisRelatedByBidangStudiId->setModel('BidangStudi');
    }

    /**
     * Gets an array of BidangStudi objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this BidangStudi is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|BidangStudi[] List of BidangStudi objects
     * @throws PropelException
     */
    public function getBidangStudisRelatedByBidangStudiId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collBidangStudisRelatedByBidangStudiIdPartial && !$this->isNew();
        if (null === $this->collBidangStudisRelatedByBidangStudiId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBidangStudisRelatedByBidangStudiId) {
                // return empty collection
                $this->initBidangStudisRelatedByBidangStudiId();
            } else {
                $collBidangStudisRelatedByBidangStudiId = BidangStudiQuery::create(null, $criteria)
                    ->filterByBidangStudiRelatedByKelompokBidangStudiId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collBidangStudisRelatedByBidangStudiIdPartial && count($collBidangStudisRelatedByBidangStudiId)) {
                      $this->initBidangStudisRelatedByBidangStudiId(false);

                      foreach($collBidangStudisRelatedByBidangStudiId as $obj) {
                        if (false == $this->collBidangStudisRelatedByBidangStudiId->contains($obj)) {
                          $this->collBidangStudisRelatedByBidangStudiId->append($obj);
                        }
                      }

                      $this->collBidangStudisRelatedByBidangStudiIdPartial = true;
                    }

                    $collBidangStudisRelatedByBidangStudiId->getInternalIterator()->rewind();
                    return $collBidangStudisRelatedByBidangStudiId;
                }

                if($partial && $this->collBidangStudisRelatedByBidangStudiId) {
                    foreach($this->collBidangStudisRelatedByBidangStudiId as $obj) {
                        if($obj->isNew()) {
                            $collBidangStudisRelatedByBidangStudiId[] = $obj;
                        }
                    }
                }

                $this->collBidangStudisRelatedByBidangStudiId = $collBidangStudisRelatedByBidangStudiId;
                $this->collBidangStudisRelatedByBidangStudiIdPartial = false;
            }
        }

        return $this->collBidangStudisRelatedByBidangStudiId;
    }

    /**
     * Sets a collection of BidangStudiRelatedByBidangStudiId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $bidangStudisRelatedByBidangStudiId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return BidangStudi The current object (for fluent API support)
     */
    public function setBidangStudisRelatedByBidangStudiId(PropelCollection $bidangStudisRelatedByBidangStudiId, PropelPDO $con = null)
    {
        $bidangStudisRelatedByBidangStudiIdToDelete = $this->getBidangStudisRelatedByBidangStudiId(new Criteria(), $con)->diff($bidangStudisRelatedByBidangStudiId);

        $this->bidangStudisRelatedByBidangStudiIdScheduledForDeletion = unserialize(serialize($bidangStudisRelatedByBidangStudiIdToDelete));

        foreach ($bidangStudisRelatedByBidangStudiIdToDelete as $bidangStudiRelatedByBidangStudiIdRemoved) {
            $bidangStudiRelatedByBidangStudiIdRemoved->setBidangStudiRelatedByKelompokBidangStudiId(null);
        }

        $this->collBidangStudisRelatedByBidangStudiId = null;
        foreach ($bidangStudisRelatedByBidangStudiId as $bidangStudiRelatedByBidangStudiId) {
            $this->addBidangStudiRelatedByBidangStudiId($bidangStudiRelatedByBidangStudiId);
        }

        $this->collBidangStudisRelatedByBidangStudiId = $bidangStudisRelatedByBidangStudiId;
        $this->collBidangStudisRelatedByBidangStudiIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BidangStudi objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related BidangStudi objects.
     * @throws PropelException
     */
    public function countBidangStudisRelatedByBidangStudiId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collBidangStudisRelatedByBidangStudiIdPartial && !$this->isNew();
        if (null === $this->collBidangStudisRelatedByBidangStudiId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBidangStudisRelatedByBidangStudiId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getBidangStudisRelatedByBidangStudiId());
            }
            $query = BidangStudiQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByBidangStudiRelatedByKelompokBidangStudiId($this)
                ->count($con);
        }

        return count($this->collBidangStudisRelatedByBidangStudiId);
    }

    /**
     * Method called to associate a BidangStudi object to this object
     * through the BidangStudi foreign key attribute.
     *
     * @param    BidangStudi $l BidangStudi
     * @return BidangStudi The current object (for fluent API support)
     */
    public function addBidangStudiRelatedByBidangStudiId(BidangStudi $l)
    {
        if ($this->collBidangStudisRelatedByBidangStudiId === null) {
            $this->initBidangStudisRelatedByBidangStudiId();
            $this->collBidangStudisRelatedByBidangStudiIdPartial = true;
        }
        if (!in_array($l, $this->collBidangStudisRelatedByBidangStudiId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddBidangStudiRelatedByBidangStudiId($l);
        }

        return $this;
    }

    /**
     * @param	BidangStudiRelatedByBidangStudiId $bidangStudiRelatedByBidangStudiId The bidangStudiRelatedByBidangStudiId object to add.
     */
    protected function doAddBidangStudiRelatedByBidangStudiId($bidangStudiRelatedByBidangStudiId)
    {
        $this->collBidangStudisRelatedByBidangStudiId[]= $bidangStudiRelatedByBidangStudiId;
        $bidangStudiRelatedByBidangStudiId->setBidangStudiRelatedByKelompokBidangStudiId($this);
    }

    /**
     * @param	BidangStudiRelatedByBidangStudiId $bidangStudiRelatedByBidangStudiId The bidangStudiRelatedByBidangStudiId object to remove.
     * @return BidangStudi The current object (for fluent API support)
     */
    public function removeBidangStudiRelatedByBidangStudiId($bidangStudiRelatedByBidangStudiId)
    {
        if ($this->getBidangStudisRelatedByBidangStudiId()->contains($bidangStudiRelatedByBidangStudiId)) {
            $this->collBidangStudisRelatedByBidangStudiId->remove($this->collBidangStudisRelatedByBidangStudiId->search($bidangStudiRelatedByBidangStudiId));
            if (null === $this->bidangStudisRelatedByBidangStudiIdScheduledForDeletion) {
                $this->bidangStudisRelatedByBidangStudiIdScheduledForDeletion = clone $this->collBidangStudisRelatedByBidangStudiId;
                $this->bidangStudisRelatedByBidangStudiIdScheduledForDeletion->clear();
            }
            $this->bidangStudisRelatedByBidangStudiIdScheduledForDeletion[]= $bidangStudiRelatedByBidangStudiId;
            $bidangStudiRelatedByBidangStudiId->setBidangStudiRelatedByKelompokBidangStudiId(null);
        }

        return $this;
    }

    /**
     * Clears out the collPtksRelatedByPengawasBidangStudiId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return BidangStudi The current object (for fluent API support)
     * @see        addPtksRelatedByPengawasBidangStudiId()
     */
    public function clearPtksRelatedByPengawasBidangStudiId()
    {
        $this->collPtksRelatedByPengawasBidangStudiId = null; // important to set this to null since that means it is uninitialized
        $this->collPtksRelatedByPengawasBidangStudiIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPtksRelatedByPengawasBidangStudiId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPtksRelatedByPengawasBidangStudiId($v = true)
    {
        $this->collPtksRelatedByPengawasBidangStudiIdPartial = $v;
    }

    /**
     * Initializes the collPtksRelatedByPengawasBidangStudiId collection.
     *
     * By default this just sets the collPtksRelatedByPengawasBidangStudiId collection to an empty array (like clearcollPtksRelatedByPengawasBidangStudiId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPtksRelatedByPengawasBidangStudiId($overrideExisting = true)
    {
        if (null !== $this->collPtksRelatedByPengawasBidangStudiId && !$overrideExisting) {
            return;
        }
        $this->collPtksRelatedByPengawasBidangStudiId = new PropelObjectCollection();
        $this->collPtksRelatedByPengawasBidangStudiId->setModel('Ptk');
    }

    /**
     * Gets an array of Ptk objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this BidangStudi is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     * @throws PropelException
     */
    public function getPtksRelatedByPengawasBidangStudiId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPtksRelatedByPengawasBidangStudiIdPartial && !$this->isNew();
        if (null === $this->collPtksRelatedByPengawasBidangStudiId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPtksRelatedByPengawasBidangStudiId) {
                // return empty collection
                $this->initPtksRelatedByPengawasBidangStudiId();
            } else {
                $collPtksRelatedByPengawasBidangStudiId = PtkQuery::create(null, $criteria)
                    ->filterByBidangStudiRelatedByPengawasBidangStudiId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPtksRelatedByPengawasBidangStudiIdPartial && count($collPtksRelatedByPengawasBidangStudiId)) {
                      $this->initPtksRelatedByPengawasBidangStudiId(false);

                      foreach($collPtksRelatedByPengawasBidangStudiId as $obj) {
                        if (false == $this->collPtksRelatedByPengawasBidangStudiId->contains($obj)) {
                          $this->collPtksRelatedByPengawasBidangStudiId->append($obj);
                        }
                      }

                      $this->collPtksRelatedByPengawasBidangStudiIdPartial = true;
                    }

                    $collPtksRelatedByPengawasBidangStudiId->getInternalIterator()->rewind();
                    return $collPtksRelatedByPengawasBidangStudiId;
                }

                if($partial && $this->collPtksRelatedByPengawasBidangStudiId) {
                    foreach($this->collPtksRelatedByPengawasBidangStudiId as $obj) {
                        if($obj->isNew()) {
                            $collPtksRelatedByPengawasBidangStudiId[] = $obj;
                        }
                    }
                }

                $this->collPtksRelatedByPengawasBidangStudiId = $collPtksRelatedByPengawasBidangStudiId;
                $this->collPtksRelatedByPengawasBidangStudiIdPartial = false;
            }
        }

        return $this->collPtksRelatedByPengawasBidangStudiId;
    }

    /**
     * Sets a collection of PtkRelatedByPengawasBidangStudiId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $ptksRelatedByPengawasBidangStudiId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return BidangStudi The current object (for fluent API support)
     */
    public function setPtksRelatedByPengawasBidangStudiId(PropelCollection $ptksRelatedByPengawasBidangStudiId, PropelPDO $con = null)
    {
        $ptksRelatedByPengawasBidangStudiIdToDelete = $this->getPtksRelatedByPengawasBidangStudiId(new Criteria(), $con)->diff($ptksRelatedByPengawasBidangStudiId);

        $this->ptksRelatedByPengawasBidangStudiIdScheduledForDeletion = unserialize(serialize($ptksRelatedByPengawasBidangStudiIdToDelete));

        foreach ($ptksRelatedByPengawasBidangStudiIdToDelete as $ptkRelatedByPengawasBidangStudiIdRemoved) {
            $ptkRelatedByPengawasBidangStudiIdRemoved->setBidangStudiRelatedByPengawasBidangStudiId(null);
        }

        $this->collPtksRelatedByPengawasBidangStudiId = null;
        foreach ($ptksRelatedByPengawasBidangStudiId as $ptkRelatedByPengawasBidangStudiId) {
            $this->addPtkRelatedByPengawasBidangStudiId($ptkRelatedByPengawasBidangStudiId);
        }

        $this->collPtksRelatedByPengawasBidangStudiId = $ptksRelatedByPengawasBidangStudiId;
        $this->collPtksRelatedByPengawasBidangStudiIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Ptk objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Ptk objects.
     * @throws PropelException
     */
    public function countPtksRelatedByPengawasBidangStudiId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPtksRelatedByPengawasBidangStudiIdPartial && !$this->isNew();
        if (null === $this->collPtksRelatedByPengawasBidangStudiId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPtksRelatedByPengawasBidangStudiId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPtksRelatedByPengawasBidangStudiId());
            }
            $query = PtkQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByBidangStudiRelatedByPengawasBidangStudiId($this)
                ->count($con);
        }

        return count($this->collPtksRelatedByPengawasBidangStudiId);
    }

    /**
     * Method called to associate a Ptk object to this object
     * through the Ptk foreign key attribute.
     *
     * @param    Ptk $l Ptk
     * @return BidangStudi The current object (for fluent API support)
     */
    public function addPtkRelatedByPengawasBidangStudiId(Ptk $l)
    {
        if ($this->collPtksRelatedByPengawasBidangStudiId === null) {
            $this->initPtksRelatedByPengawasBidangStudiId();
            $this->collPtksRelatedByPengawasBidangStudiIdPartial = true;
        }
        if (!in_array($l, $this->collPtksRelatedByPengawasBidangStudiId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPtkRelatedByPengawasBidangStudiId($l);
        }

        return $this;
    }

    /**
     * @param	PtkRelatedByPengawasBidangStudiId $ptkRelatedByPengawasBidangStudiId The ptkRelatedByPengawasBidangStudiId object to add.
     */
    protected function doAddPtkRelatedByPengawasBidangStudiId($ptkRelatedByPengawasBidangStudiId)
    {
        $this->collPtksRelatedByPengawasBidangStudiId[]= $ptkRelatedByPengawasBidangStudiId;
        $ptkRelatedByPengawasBidangStudiId->setBidangStudiRelatedByPengawasBidangStudiId($this);
    }

    /**
     * @param	PtkRelatedByPengawasBidangStudiId $ptkRelatedByPengawasBidangStudiId The ptkRelatedByPengawasBidangStudiId object to remove.
     * @return BidangStudi The current object (for fluent API support)
     */
    public function removePtkRelatedByPengawasBidangStudiId($ptkRelatedByPengawasBidangStudiId)
    {
        if ($this->getPtksRelatedByPengawasBidangStudiId()->contains($ptkRelatedByPengawasBidangStudiId)) {
            $this->collPtksRelatedByPengawasBidangStudiId->remove($this->collPtksRelatedByPengawasBidangStudiId->search($ptkRelatedByPengawasBidangStudiId));
            if (null === $this->ptksRelatedByPengawasBidangStudiIdScheduledForDeletion) {
                $this->ptksRelatedByPengawasBidangStudiIdScheduledForDeletion = clone $this->collPtksRelatedByPengawasBidangStudiId;
                $this->ptksRelatedByPengawasBidangStudiIdScheduledForDeletion->clear();
            }
            $this->ptksRelatedByPengawasBidangStudiIdScheduledForDeletion[]= $ptkRelatedByPengawasBidangStudiId;
            $ptkRelatedByPengawasBidangStudiId->setBidangStudiRelatedByPengawasBidangStudiId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinSekolahRelatedByEntrySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedByEntrySekolahId', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinSekolahRelatedByEntrySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedByEntrySekolahId', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinAgamaRelatedByAgamaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('AgamaRelatedByAgamaId', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinAgamaRelatedByAgamaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('AgamaRelatedByAgamaId', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinJenisPtkRelatedByJenisPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('JenisPtkRelatedByJenisPtkId', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinJenisPtkRelatedByJenisPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('JenisPtkRelatedByJenisPtkId', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('KeahlianLaboratoriumRelatedByKeahlianLaboratoriumId', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('KeahlianLaboratoriumRelatedByKeahlianLaboratoriumId', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinKebutuhanKhususRelatedByMampuHandleKk($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByMampuHandleKk', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinKebutuhanKhususRelatedByMampuHandleKk($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByMampuHandleKk', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinLembagaPengangkatRelatedByLembagaPengangkatId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('LembagaPengangkatRelatedByLembagaPengangkatId', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinLembagaPengangkatRelatedByLembagaPengangkatId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('LembagaPengangkatRelatedByLembagaPengangkatId', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinMstWilayahRelatedByKodeWilayah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('MstWilayahRelatedByKodeWilayah', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinMstWilayahRelatedByKodeWilayah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('MstWilayahRelatedByKodeWilayah', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinNegaraRelatedByKewarganegaraan($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('NegaraRelatedByKewarganegaraan', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinNegaraRelatedByKewarganegaraan($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('NegaraRelatedByKewarganegaraan', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinPangkatGolonganRelatedByPangkatGolonganId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('PangkatGolonganRelatedByPangkatGolonganId', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinPangkatGolonganRelatedByPangkatGolonganId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('PangkatGolonganRelatedByPangkatGolonganId', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinPekerjaanRelatedByPekerjaanSuamiIstri($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('PekerjaanRelatedByPekerjaanSuamiIstri', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinPekerjaanRelatedByPekerjaanSuamiIstri($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('PekerjaanRelatedByPekerjaanSuamiIstri', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinStatusKepegawaianRelatedByStatusKepegawaianId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('StatusKepegawaianRelatedByStatusKepegawaianId', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinStatusKepegawaianRelatedByStatusKepegawaianId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('StatusKepegawaianRelatedByStatusKepegawaianId', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinStatusKeaktifanPegawaiRelatedByStatusKeaktifanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('StatusKeaktifanPegawaiRelatedByStatusKeaktifanId', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinStatusKeaktifanPegawaiRelatedByStatusKeaktifanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('StatusKeaktifanPegawaiRelatedByStatusKeaktifanId', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinSumberGajiRelatedBySumberGajiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('SumberGajiRelatedBySumberGajiId', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinSumberGajiRelatedBySumberGajiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('SumberGajiRelatedBySumberGajiId', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }

    /**
     * Clears out the collPtksRelatedByPengawasBidangStudiId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return BidangStudi The current object (for fluent API support)
     * @see        addPtksRelatedByPengawasBidangStudiId()
     */
    public function clearPtksRelatedByPengawasBidangStudiId()
    {
        $this->collPtksRelatedByPengawasBidangStudiId = null; // important to set this to null since that means it is uninitialized
        $this->collPtksRelatedByPengawasBidangStudiIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPtksRelatedByPengawasBidangStudiId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPtksRelatedByPengawasBidangStudiId($v = true)
    {
        $this->collPtksRelatedByPengawasBidangStudiIdPartial = $v;
    }

    /**
     * Initializes the collPtksRelatedByPengawasBidangStudiId collection.
     *
     * By default this just sets the collPtksRelatedByPengawasBidangStudiId collection to an empty array (like clearcollPtksRelatedByPengawasBidangStudiId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPtksRelatedByPengawasBidangStudiId($overrideExisting = true)
    {
        if (null !== $this->collPtksRelatedByPengawasBidangStudiId && !$overrideExisting) {
            return;
        }
        $this->collPtksRelatedByPengawasBidangStudiId = new PropelObjectCollection();
        $this->collPtksRelatedByPengawasBidangStudiId->setModel('Ptk');
    }

    /**
     * Gets an array of Ptk objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this BidangStudi is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     * @throws PropelException
     */
    public function getPtksRelatedByPengawasBidangStudiId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPtksRelatedByPengawasBidangStudiIdPartial && !$this->isNew();
        if (null === $this->collPtksRelatedByPengawasBidangStudiId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPtksRelatedByPengawasBidangStudiId) {
                // return empty collection
                $this->initPtksRelatedByPengawasBidangStudiId();
            } else {
                $collPtksRelatedByPengawasBidangStudiId = PtkQuery::create(null, $criteria)
                    ->filterByBidangStudiRelatedByPengawasBidangStudiId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPtksRelatedByPengawasBidangStudiIdPartial && count($collPtksRelatedByPengawasBidangStudiId)) {
                      $this->initPtksRelatedByPengawasBidangStudiId(false);

                      foreach($collPtksRelatedByPengawasBidangStudiId as $obj) {
                        if (false == $this->collPtksRelatedByPengawasBidangStudiId->contains($obj)) {
                          $this->collPtksRelatedByPengawasBidangStudiId->append($obj);
                        }
                      }

                      $this->collPtksRelatedByPengawasBidangStudiIdPartial = true;
                    }

                    $collPtksRelatedByPengawasBidangStudiId->getInternalIterator()->rewind();
                    return $collPtksRelatedByPengawasBidangStudiId;
                }

                if($partial && $this->collPtksRelatedByPengawasBidangStudiId) {
                    foreach($this->collPtksRelatedByPengawasBidangStudiId as $obj) {
                        if($obj->isNew()) {
                            $collPtksRelatedByPengawasBidangStudiId[] = $obj;
                        }
                    }
                }

                $this->collPtksRelatedByPengawasBidangStudiId = $collPtksRelatedByPengawasBidangStudiId;
                $this->collPtksRelatedByPengawasBidangStudiIdPartial = false;
            }
        }

        return $this->collPtksRelatedByPengawasBidangStudiId;
    }

    /**
     * Sets a collection of PtkRelatedByPengawasBidangStudiId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $ptksRelatedByPengawasBidangStudiId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return BidangStudi The current object (for fluent API support)
     */
    public function setPtksRelatedByPengawasBidangStudiId(PropelCollection $ptksRelatedByPengawasBidangStudiId, PropelPDO $con = null)
    {
        $ptksRelatedByPengawasBidangStudiIdToDelete = $this->getPtksRelatedByPengawasBidangStudiId(new Criteria(), $con)->diff($ptksRelatedByPengawasBidangStudiId);

        $this->ptksRelatedByPengawasBidangStudiIdScheduledForDeletion = unserialize(serialize($ptksRelatedByPengawasBidangStudiIdToDelete));

        foreach ($ptksRelatedByPengawasBidangStudiIdToDelete as $ptkRelatedByPengawasBidangStudiIdRemoved) {
            $ptkRelatedByPengawasBidangStudiIdRemoved->setBidangStudiRelatedByPengawasBidangStudiId(null);
        }

        $this->collPtksRelatedByPengawasBidangStudiId = null;
        foreach ($ptksRelatedByPengawasBidangStudiId as $ptkRelatedByPengawasBidangStudiId) {
            $this->addPtkRelatedByPengawasBidangStudiId($ptkRelatedByPengawasBidangStudiId);
        }

        $this->collPtksRelatedByPengawasBidangStudiId = $ptksRelatedByPengawasBidangStudiId;
        $this->collPtksRelatedByPengawasBidangStudiIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Ptk objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Ptk objects.
     * @throws PropelException
     */
    public function countPtksRelatedByPengawasBidangStudiId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPtksRelatedByPengawasBidangStudiIdPartial && !$this->isNew();
        if (null === $this->collPtksRelatedByPengawasBidangStudiId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPtksRelatedByPengawasBidangStudiId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPtksRelatedByPengawasBidangStudiId());
            }
            $query = PtkQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByBidangStudiRelatedByPengawasBidangStudiId($this)
                ->count($con);
        }

        return count($this->collPtksRelatedByPengawasBidangStudiId);
    }

    /**
     * Method called to associate a Ptk object to this object
     * through the Ptk foreign key attribute.
     *
     * @param    Ptk $l Ptk
     * @return BidangStudi The current object (for fluent API support)
     */
    public function addPtkRelatedByPengawasBidangStudiId(Ptk $l)
    {
        if ($this->collPtksRelatedByPengawasBidangStudiId === null) {
            $this->initPtksRelatedByPengawasBidangStudiId();
            $this->collPtksRelatedByPengawasBidangStudiIdPartial = true;
        }
        if (!in_array($l, $this->collPtksRelatedByPengawasBidangStudiId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPtkRelatedByPengawasBidangStudiId($l);
        }

        return $this;
    }

    /**
     * @param	PtkRelatedByPengawasBidangStudiId $ptkRelatedByPengawasBidangStudiId The ptkRelatedByPengawasBidangStudiId object to add.
     */
    protected function doAddPtkRelatedByPengawasBidangStudiId($ptkRelatedByPengawasBidangStudiId)
    {
        $this->collPtksRelatedByPengawasBidangStudiId[]= $ptkRelatedByPengawasBidangStudiId;
        $ptkRelatedByPengawasBidangStudiId->setBidangStudiRelatedByPengawasBidangStudiId($this);
    }

    /**
     * @param	PtkRelatedByPengawasBidangStudiId $ptkRelatedByPengawasBidangStudiId The ptkRelatedByPengawasBidangStudiId object to remove.
     * @return BidangStudi The current object (for fluent API support)
     */
    public function removePtkRelatedByPengawasBidangStudiId($ptkRelatedByPengawasBidangStudiId)
    {
        if ($this->getPtksRelatedByPengawasBidangStudiId()->contains($ptkRelatedByPengawasBidangStudiId)) {
            $this->collPtksRelatedByPengawasBidangStudiId->remove($this->collPtksRelatedByPengawasBidangStudiId->search($ptkRelatedByPengawasBidangStudiId));
            if (null === $this->ptksRelatedByPengawasBidangStudiIdScheduledForDeletion) {
                $this->ptksRelatedByPengawasBidangStudiIdScheduledForDeletion = clone $this->collPtksRelatedByPengawasBidangStudiId;
                $this->ptksRelatedByPengawasBidangStudiIdScheduledForDeletion->clear();
            }
            $this->ptksRelatedByPengawasBidangStudiIdScheduledForDeletion[]= $ptkRelatedByPengawasBidangStudiId;
            $ptkRelatedByPengawasBidangStudiId->setBidangStudiRelatedByPengawasBidangStudiId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinSekolahRelatedByEntrySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedByEntrySekolahId', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinSekolahRelatedByEntrySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedByEntrySekolahId', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinAgamaRelatedByAgamaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('AgamaRelatedByAgamaId', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinAgamaRelatedByAgamaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('AgamaRelatedByAgamaId', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinJenisPtkRelatedByJenisPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('JenisPtkRelatedByJenisPtkId', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinJenisPtkRelatedByJenisPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('JenisPtkRelatedByJenisPtkId', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('KeahlianLaboratoriumRelatedByKeahlianLaboratoriumId', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('KeahlianLaboratoriumRelatedByKeahlianLaboratoriumId', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinKebutuhanKhususRelatedByMampuHandleKk($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByMampuHandleKk', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinKebutuhanKhususRelatedByMampuHandleKk($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByMampuHandleKk', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinLembagaPengangkatRelatedByLembagaPengangkatId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('LembagaPengangkatRelatedByLembagaPengangkatId', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinLembagaPengangkatRelatedByLembagaPengangkatId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('LembagaPengangkatRelatedByLembagaPengangkatId', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinMstWilayahRelatedByKodeWilayah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('MstWilayahRelatedByKodeWilayah', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinMstWilayahRelatedByKodeWilayah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('MstWilayahRelatedByKodeWilayah', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinNegaraRelatedByKewarganegaraan($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('NegaraRelatedByKewarganegaraan', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinNegaraRelatedByKewarganegaraan($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('NegaraRelatedByKewarganegaraan', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinPangkatGolonganRelatedByPangkatGolonganId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('PangkatGolonganRelatedByPangkatGolonganId', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinPangkatGolonganRelatedByPangkatGolonganId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('PangkatGolonganRelatedByPangkatGolonganId', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinPekerjaanRelatedByPekerjaanSuamiIstri($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('PekerjaanRelatedByPekerjaanSuamiIstri', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinPekerjaanRelatedByPekerjaanSuamiIstri($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('PekerjaanRelatedByPekerjaanSuamiIstri', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinStatusKepegawaianRelatedByStatusKepegawaianId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('StatusKepegawaianRelatedByStatusKepegawaianId', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinStatusKepegawaianRelatedByStatusKepegawaianId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('StatusKepegawaianRelatedByStatusKepegawaianId', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinStatusKeaktifanPegawaiRelatedByStatusKeaktifanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('StatusKeaktifanPegawaiRelatedByStatusKeaktifanId', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinStatusKeaktifanPegawaiRelatedByStatusKeaktifanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('StatusKeaktifanPegawaiRelatedByStatusKeaktifanId', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinSumberGajiRelatedBySumberGajiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('SumberGajiRelatedBySumberGajiId', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PtksRelatedByPengawasBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPengawasBidangStudiIdJoinSumberGajiRelatedBySumberGajiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('SumberGajiRelatedBySumberGajiId', $join_behavior);

        return $this->getPtksRelatedByPengawasBidangStudiId($query, $con);
    }

    /**
     * Clears out the collPengawasTerdaftarsRelatedByBidangStudiId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return BidangStudi The current object (for fluent API support)
     * @see        addPengawasTerdaftarsRelatedByBidangStudiId()
     */
    public function clearPengawasTerdaftarsRelatedByBidangStudiId()
    {
        $this->collPengawasTerdaftarsRelatedByBidangStudiId = null; // important to set this to null since that means it is uninitialized
        $this->collPengawasTerdaftarsRelatedByBidangStudiIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPengawasTerdaftarsRelatedByBidangStudiId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPengawasTerdaftarsRelatedByBidangStudiId($v = true)
    {
        $this->collPengawasTerdaftarsRelatedByBidangStudiIdPartial = $v;
    }

    /**
     * Initializes the collPengawasTerdaftarsRelatedByBidangStudiId collection.
     *
     * By default this just sets the collPengawasTerdaftarsRelatedByBidangStudiId collection to an empty array (like clearcollPengawasTerdaftarsRelatedByBidangStudiId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPengawasTerdaftarsRelatedByBidangStudiId($overrideExisting = true)
    {
        if (null !== $this->collPengawasTerdaftarsRelatedByBidangStudiId && !$overrideExisting) {
            return;
        }
        $this->collPengawasTerdaftarsRelatedByBidangStudiId = new PropelObjectCollection();
        $this->collPengawasTerdaftarsRelatedByBidangStudiId->setModel('PengawasTerdaftar');
    }

    /**
     * Gets an array of PengawasTerdaftar objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this BidangStudi is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     * @throws PropelException
     */
    public function getPengawasTerdaftarsRelatedByBidangStudiId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPengawasTerdaftarsRelatedByBidangStudiIdPartial && !$this->isNew();
        if (null === $this->collPengawasTerdaftarsRelatedByBidangStudiId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPengawasTerdaftarsRelatedByBidangStudiId) {
                // return empty collection
                $this->initPengawasTerdaftarsRelatedByBidangStudiId();
            } else {
                $collPengawasTerdaftarsRelatedByBidangStudiId = PengawasTerdaftarQuery::create(null, $criteria)
                    ->filterByBidangStudiRelatedByBidangStudiId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPengawasTerdaftarsRelatedByBidangStudiIdPartial && count($collPengawasTerdaftarsRelatedByBidangStudiId)) {
                      $this->initPengawasTerdaftarsRelatedByBidangStudiId(false);

                      foreach($collPengawasTerdaftarsRelatedByBidangStudiId as $obj) {
                        if (false == $this->collPengawasTerdaftarsRelatedByBidangStudiId->contains($obj)) {
                          $this->collPengawasTerdaftarsRelatedByBidangStudiId->append($obj);
                        }
                      }

                      $this->collPengawasTerdaftarsRelatedByBidangStudiIdPartial = true;
                    }

                    $collPengawasTerdaftarsRelatedByBidangStudiId->getInternalIterator()->rewind();
                    return $collPengawasTerdaftarsRelatedByBidangStudiId;
                }

                if($partial && $this->collPengawasTerdaftarsRelatedByBidangStudiId) {
                    foreach($this->collPengawasTerdaftarsRelatedByBidangStudiId as $obj) {
                        if($obj->isNew()) {
                            $collPengawasTerdaftarsRelatedByBidangStudiId[] = $obj;
                        }
                    }
                }

                $this->collPengawasTerdaftarsRelatedByBidangStudiId = $collPengawasTerdaftarsRelatedByBidangStudiId;
                $this->collPengawasTerdaftarsRelatedByBidangStudiIdPartial = false;
            }
        }

        return $this->collPengawasTerdaftarsRelatedByBidangStudiId;
    }

    /**
     * Sets a collection of PengawasTerdaftarRelatedByBidangStudiId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $pengawasTerdaftarsRelatedByBidangStudiId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return BidangStudi The current object (for fluent API support)
     */
    public function setPengawasTerdaftarsRelatedByBidangStudiId(PropelCollection $pengawasTerdaftarsRelatedByBidangStudiId, PropelPDO $con = null)
    {
        $pengawasTerdaftarsRelatedByBidangStudiIdToDelete = $this->getPengawasTerdaftarsRelatedByBidangStudiId(new Criteria(), $con)->diff($pengawasTerdaftarsRelatedByBidangStudiId);

        $this->pengawasTerdaftarsRelatedByBidangStudiIdScheduledForDeletion = unserialize(serialize($pengawasTerdaftarsRelatedByBidangStudiIdToDelete));

        foreach ($pengawasTerdaftarsRelatedByBidangStudiIdToDelete as $pengawasTerdaftarRelatedByBidangStudiIdRemoved) {
            $pengawasTerdaftarRelatedByBidangStudiIdRemoved->setBidangStudiRelatedByBidangStudiId(null);
        }

        $this->collPengawasTerdaftarsRelatedByBidangStudiId = null;
        foreach ($pengawasTerdaftarsRelatedByBidangStudiId as $pengawasTerdaftarRelatedByBidangStudiId) {
            $this->addPengawasTerdaftarRelatedByBidangStudiId($pengawasTerdaftarRelatedByBidangStudiId);
        }

        $this->collPengawasTerdaftarsRelatedByBidangStudiId = $pengawasTerdaftarsRelatedByBidangStudiId;
        $this->collPengawasTerdaftarsRelatedByBidangStudiIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PengawasTerdaftar objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related PengawasTerdaftar objects.
     * @throws PropelException
     */
    public function countPengawasTerdaftarsRelatedByBidangStudiId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPengawasTerdaftarsRelatedByBidangStudiIdPartial && !$this->isNew();
        if (null === $this->collPengawasTerdaftarsRelatedByBidangStudiId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPengawasTerdaftarsRelatedByBidangStudiId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPengawasTerdaftarsRelatedByBidangStudiId());
            }
            $query = PengawasTerdaftarQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByBidangStudiRelatedByBidangStudiId($this)
                ->count($con);
        }

        return count($this->collPengawasTerdaftarsRelatedByBidangStudiId);
    }

    /**
     * Method called to associate a PengawasTerdaftar object to this object
     * through the PengawasTerdaftar foreign key attribute.
     *
     * @param    PengawasTerdaftar $l PengawasTerdaftar
     * @return BidangStudi The current object (for fluent API support)
     */
    public function addPengawasTerdaftarRelatedByBidangStudiId(PengawasTerdaftar $l)
    {
        if ($this->collPengawasTerdaftarsRelatedByBidangStudiId === null) {
            $this->initPengawasTerdaftarsRelatedByBidangStudiId();
            $this->collPengawasTerdaftarsRelatedByBidangStudiIdPartial = true;
        }
        if (!in_array($l, $this->collPengawasTerdaftarsRelatedByBidangStudiId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPengawasTerdaftarRelatedByBidangStudiId($l);
        }

        return $this;
    }

    /**
     * @param	PengawasTerdaftarRelatedByBidangStudiId $pengawasTerdaftarRelatedByBidangStudiId The pengawasTerdaftarRelatedByBidangStudiId object to add.
     */
    protected function doAddPengawasTerdaftarRelatedByBidangStudiId($pengawasTerdaftarRelatedByBidangStudiId)
    {
        $this->collPengawasTerdaftarsRelatedByBidangStudiId[]= $pengawasTerdaftarRelatedByBidangStudiId;
        $pengawasTerdaftarRelatedByBidangStudiId->setBidangStudiRelatedByBidangStudiId($this);
    }

    /**
     * @param	PengawasTerdaftarRelatedByBidangStudiId $pengawasTerdaftarRelatedByBidangStudiId The pengawasTerdaftarRelatedByBidangStudiId object to remove.
     * @return BidangStudi The current object (for fluent API support)
     */
    public function removePengawasTerdaftarRelatedByBidangStudiId($pengawasTerdaftarRelatedByBidangStudiId)
    {
        if ($this->getPengawasTerdaftarsRelatedByBidangStudiId()->contains($pengawasTerdaftarRelatedByBidangStudiId)) {
            $this->collPengawasTerdaftarsRelatedByBidangStudiId->remove($this->collPengawasTerdaftarsRelatedByBidangStudiId->search($pengawasTerdaftarRelatedByBidangStudiId));
            if (null === $this->pengawasTerdaftarsRelatedByBidangStudiIdScheduledForDeletion) {
                $this->pengawasTerdaftarsRelatedByBidangStudiIdScheduledForDeletion = clone $this->collPengawasTerdaftarsRelatedByBidangStudiId;
                $this->pengawasTerdaftarsRelatedByBidangStudiIdScheduledForDeletion->clear();
            }
            $this->pengawasTerdaftarsRelatedByBidangStudiIdScheduledForDeletion[]= $pengawasTerdaftarRelatedByBidangStudiId;
            $pengawasTerdaftarRelatedByBidangStudiId->setBidangStudiRelatedByBidangStudiId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByBidangStudiIdJoinLembagaNonSekolahRelatedByLembagaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('LembagaNonSekolahRelatedByLembagaId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByBidangStudiIdJoinLembagaNonSekolahRelatedByLembagaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('LembagaNonSekolahRelatedByLembagaId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByBidangStudiIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByBidangStudiIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByBidangStudiIdJoinJenisKeluarRelatedByJenisKeluarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('JenisKeluarRelatedByJenisKeluarId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByBidangStudiIdJoinJenisKeluarRelatedByJenisKeluarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('JenisKeluarRelatedByJenisKeluarId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByBidangStudiIdJoinJenjangKepengawasanRelatedByJenjangKepengawasanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('JenjangKepengawasanRelatedByJenjangKepengawasanId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByBidangStudiIdJoinJenjangKepengawasanRelatedByJenjangKepengawasanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('JenjangKepengawasanRelatedByJenjangKepengawasanId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByBidangStudiIdJoinMataPelajaranRelatedByMataPelajaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('MataPelajaranRelatedByMataPelajaranId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByBidangStudiIdJoinMataPelajaranRelatedByMataPelajaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('MataPelajaranRelatedByMataPelajaranId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByBidangStudiIdJoinTahunAjaranRelatedByTahunAjaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('TahunAjaranRelatedByTahunAjaranId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByBidangStudiIdJoinTahunAjaranRelatedByTahunAjaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('TahunAjaranRelatedByTahunAjaranId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByBidangStudiId($query, $con);
    }

    /**
     * Clears out the collPengawasTerdaftarsRelatedByBidangStudiId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return BidangStudi The current object (for fluent API support)
     * @see        addPengawasTerdaftarsRelatedByBidangStudiId()
     */
    public function clearPengawasTerdaftarsRelatedByBidangStudiId()
    {
        $this->collPengawasTerdaftarsRelatedByBidangStudiId = null; // important to set this to null since that means it is uninitialized
        $this->collPengawasTerdaftarsRelatedByBidangStudiIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPengawasTerdaftarsRelatedByBidangStudiId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPengawasTerdaftarsRelatedByBidangStudiId($v = true)
    {
        $this->collPengawasTerdaftarsRelatedByBidangStudiIdPartial = $v;
    }

    /**
     * Initializes the collPengawasTerdaftarsRelatedByBidangStudiId collection.
     *
     * By default this just sets the collPengawasTerdaftarsRelatedByBidangStudiId collection to an empty array (like clearcollPengawasTerdaftarsRelatedByBidangStudiId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPengawasTerdaftarsRelatedByBidangStudiId($overrideExisting = true)
    {
        if (null !== $this->collPengawasTerdaftarsRelatedByBidangStudiId && !$overrideExisting) {
            return;
        }
        $this->collPengawasTerdaftarsRelatedByBidangStudiId = new PropelObjectCollection();
        $this->collPengawasTerdaftarsRelatedByBidangStudiId->setModel('PengawasTerdaftar');
    }

    /**
     * Gets an array of PengawasTerdaftar objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this BidangStudi is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     * @throws PropelException
     */
    public function getPengawasTerdaftarsRelatedByBidangStudiId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPengawasTerdaftarsRelatedByBidangStudiIdPartial && !$this->isNew();
        if (null === $this->collPengawasTerdaftarsRelatedByBidangStudiId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPengawasTerdaftarsRelatedByBidangStudiId) {
                // return empty collection
                $this->initPengawasTerdaftarsRelatedByBidangStudiId();
            } else {
                $collPengawasTerdaftarsRelatedByBidangStudiId = PengawasTerdaftarQuery::create(null, $criteria)
                    ->filterByBidangStudiRelatedByBidangStudiId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPengawasTerdaftarsRelatedByBidangStudiIdPartial && count($collPengawasTerdaftarsRelatedByBidangStudiId)) {
                      $this->initPengawasTerdaftarsRelatedByBidangStudiId(false);

                      foreach($collPengawasTerdaftarsRelatedByBidangStudiId as $obj) {
                        if (false == $this->collPengawasTerdaftarsRelatedByBidangStudiId->contains($obj)) {
                          $this->collPengawasTerdaftarsRelatedByBidangStudiId->append($obj);
                        }
                      }

                      $this->collPengawasTerdaftarsRelatedByBidangStudiIdPartial = true;
                    }

                    $collPengawasTerdaftarsRelatedByBidangStudiId->getInternalIterator()->rewind();
                    return $collPengawasTerdaftarsRelatedByBidangStudiId;
                }

                if($partial && $this->collPengawasTerdaftarsRelatedByBidangStudiId) {
                    foreach($this->collPengawasTerdaftarsRelatedByBidangStudiId as $obj) {
                        if($obj->isNew()) {
                            $collPengawasTerdaftarsRelatedByBidangStudiId[] = $obj;
                        }
                    }
                }

                $this->collPengawasTerdaftarsRelatedByBidangStudiId = $collPengawasTerdaftarsRelatedByBidangStudiId;
                $this->collPengawasTerdaftarsRelatedByBidangStudiIdPartial = false;
            }
        }

        return $this->collPengawasTerdaftarsRelatedByBidangStudiId;
    }

    /**
     * Sets a collection of PengawasTerdaftarRelatedByBidangStudiId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $pengawasTerdaftarsRelatedByBidangStudiId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return BidangStudi The current object (for fluent API support)
     */
    public function setPengawasTerdaftarsRelatedByBidangStudiId(PropelCollection $pengawasTerdaftarsRelatedByBidangStudiId, PropelPDO $con = null)
    {
        $pengawasTerdaftarsRelatedByBidangStudiIdToDelete = $this->getPengawasTerdaftarsRelatedByBidangStudiId(new Criteria(), $con)->diff($pengawasTerdaftarsRelatedByBidangStudiId);

        $this->pengawasTerdaftarsRelatedByBidangStudiIdScheduledForDeletion = unserialize(serialize($pengawasTerdaftarsRelatedByBidangStudiIdToDelete));

        foreach ($pengawasTerdaftarsRelatedByBidangStudiIdToDelete as $pengawasTerdaftarRelatedByBidangStudiIdRemoved) {
            $pengawasTerdaftarRelatedByBidangStudiIdRemoved->setBidangStudiRelatedByBidangStudiId(null);
        }

        $this->collPengawasTerdaftarsRelatedByBidangStudiId = null;
        foreach ($pengawasTerdaftarsRelatedByBidangStudiId as $pengawasTerdaftarRelatedByBidangStudiId) {
            $this->addPengawasTerdaftarRelatedByBidangStudiId($pengawasTerdaftarRelatedByBidangStudiId);
        }

        $this->collPengawasTerdaftarsRelatedByBidangStudiId = $pengawasTerdaftarsRelatedByBidangStudiId;
        $this->collPengawasTerdaftarsRelatedByBidangStudiIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PengawasTerdaftar objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related PengawasTerdaftar objects.
     * @throws PropelException
     */
    public function countPengawasTerdaftarsRelatedByBidangStudiId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPengawasTerdaftarsRelatedByBidangStudiIdPartial && !$this->isNew();
        if (null === $this->collPengawasTerdaftarsRelatedByBidangStudiId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPengawasTerdaftarsRelatedByBidangStudiId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPengawasTerdaftarsRelatedByBidangStudiId());
            }
            $query = PengawasTerdaftarQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByBidangStudiRelatedByBidangStudiId($this)
                ->count($con);
        }

        return count($this->collPengawasTerdaftarsRelatedByBidangStudiId);
    }

    /**
     * Method called to associate a PengawasTerdaftar object to this object
     * through the PengawasTerdaftar foreign key attribute.
     *
     * @param    PengawasTerdaftar $l PengawasTerdaftar
     * @return BidangStudi The current object (for fluent API support)
     */
    public function addPengawasTerdaftarRelatedByBidangStudiId(PengawasTerdaftar $l)
    {
        if ($this->collPengawasTerdaftarsRelatedByBidangStudiId === null) {
            $this->initPengawasTerdaftarsRelatedByBidangStudiId();
            $this->collPengawasTerdaftarsRelatedByBidangStudiIdPartial = true;
        }
        if (!in_array($l, $this->collPengawasTerdaftarsRelatedByBidangStudiId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPengawasTerdaftarRelatedByBidangStudiId($l);
        }

        return $this;
    }

    /**
     * @param	PengawasTerdaftarRelatedByBidangStudiId $pengawasTerdaftarRelatedByBidangStudiId The pengawasTerdaftarRelatedByBidangStudiId object to add.
     */
    protected function doAddPengawasTerdaftarRelatedByBidangStudiId($pengawasTerdaftarRelatedByBidangStudiId)
    {
        $this->collPengawasTerdaftarsRelatedByBidangStudiId[]= $pengawasTerdaftarRelatedByBidangStudiId;
        $pengawasTerdaftarRelatedByBidangStudiId->setBidangStudiRelatedByBidangStudiId($this);
    }

    /**
     * @param	PengawasTerdaftarRelatedByBidangStudiId $pengawasTerdaftarRelatedByBidangStudiId The pengawasTerdaftarRelatedByBidangStudiId object to remove.
     * @return BidangStudi The current object (for fluent API support)
     */
    public function removePengawasTerdaftarRelatedByBidangStudiId($pengawasTerdaftarRelatedByBidangStudiId)
    {
        if ($this->getPengawasTerdaftarsRelatedByBidangStudiId()->contains($pengawasTerdaftarRelatedByBidangStudiId)) {
            $this->collPengawasTerdaftarsRelatedByBidangStudiId->remove($this->collPengawasTerdaftarsRelatedByBidangStudiId->search($pengawasTerdaftarRelatedByBidangStudiId));
            if (null === $this->pengawasTerdaftarsRelatedByBidangStudiIdScheduledForDeletion) {
                $this->pengawasTerdaftarsRelatedByBidangStudiIdScheduledForDeletion = clone $this->collPengawasTerdaftarsRelatedByBidangStudiId;
                $this->pengawasTerdaftarsRelatedByBidangStudiIdScheduledForDeletion->clear();
            }
            $this->pengawasTerdaftarsRelatedByBidangStudiIdScheduledForDeletion[]= $pengawasTerdaftarRelatedByBidangStudiId;
            $pengawasTerdaftarRelatedByBidangStudiId->setBidangStudiRelatedByBidangStudiId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByBidangStudiIdJoinLembagaNonSekolahRelatedByLembagaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('LembagaNonSekolahRelatedByLembagaId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByBidangStudiIdJoinLembagaNonSekolahRelatedByLembagaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('LembagaNonSekolahRelatedByLembagaId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByBidangStudiIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByBidangStudiIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByBidangStudiIdJoinJenisKeluarRelatedByJenisKeluarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('JenisKeluarRelatedByJenisKeluarId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByBidangStudiIdJoinJenisKeluarRelatedByJenisKeluarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('JenisKeluarRelatedByJenisKeluarId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByBidangStudiIdJoinJenjangKepengawasanRelatedByJenjangKepengawasanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('JenjangKepengawasanRelatedByJenjangKepengawasanId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByBidangStudiIdJoinJenjangKepengawasanRelatedByJenjangKepengawasanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('JenjangKepengawasanRelatedByJenjangKepengawasanId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByBidangStudiIdJoinMataPelajaranRelatedByMataPelajaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('MataPelajaranRelatedByMataPelajaranId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByBidangStudiIdJoinMataPelajaranRelatedByMataPelajaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('MataPelajaranRelatedByMataPelajaranId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByBidangStudiIdJoinTahunAjaranRelatedByTahunAjaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('TahunAjaranRelatedByTahunAjaranId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByBidangStudiIdJoinTahunAjaranRelatedByTahunAjaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('TahunAjaranRelatedByTahunAjaranId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByBidangStudiId($query, $con);
    }

    /**
     * Clears out the collRwyPendFormalsRelatedByBidangStudiId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return BidangStudi The current object (for fluent API support)
     * @see        addRwyPendFormalsRelatedByBidangStudiId()
     */
    public function clearRwyPendFormalsRelatedByBidangStudiId()
    {
        $this->collRwyPendFormalsRelatedByBidangStudiId = null; // important to set this to null since that means it is uninitialized
        $this->collRwyPendFormalsRelatedByBidangStudiIdPartial = null;

        return $this;
    }

    /**
     * reset is the collRwyPendFormalsRelatedByBidangStudiId collection loaded partially
     *
     * @return void
     */
    public function resetPartialRwyPendFormalsRelatedByBidangStudiId($v = true)
    {
        $this->collRwyPendFormalsRelatedByBidangStudiIdPartial = $v;
    }

    /**
     * Initializes the collRwyPendFormalsRelatedByBidangStudiId collection.
     *
     * By default this just sets the collRwyPendFormalsRelatedByBidangStudiId collection to an empty array (like clearcollRwyPendFormalsRelatedByBidangStudiId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRwyPendFormalsRelatedByBidangStudiId($overrideExisting = true)
    {
        if (null !== $this->collRwyPendFormalsRelatedByBidangStudiId && !$overrideExisting) {
            return;
        }
        $this->collRwyPendFormalsRelatedByBidangStudiId = new PropelObjectCollection();
        $this->collRwyPendFormalsRelatedByBidangStudiId->setModel('RwyPendFormal');
    }

    /**
     * Gets an array of RwyPendFormal objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this BidangStudi is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RwyPendFormal[] List of RwyPendFormal objects
     * @throws PropelException
     */
    public function getRwyPendFormalsRelatedByBidangStudiId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRwyPendFormalsRelatedByBidangStudiIdPartial && !$this->isNew();
        if (null === $this->collRwyPendFormalsRelatedByBidangStudiId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRwyPendFormalsRelatedByBidangStudiId) {
                // return empty collection
                $this->initRwyPendFormalsRelatedByBidangStudiId();
            } else {
                $collRwyPendFormalsRelatedByBidangStudiId = RwyPendFormalQuery::create(null, $criteria)
                    ->filterByBidangStudiRelatedByBidangStudiId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRwyPendFormalsRelatedByBidangStudiIdPartial && count($collRwyPendFormalsRelatedByBidangStudiId)) {
                      $this->initRwyPendFormalsRelatedByBidangStudiId(false);

                      foreach($collRwyPendFormalsRelatedByBidangStudiId as $obj) {
                        if (false == $this->collRwyPendFormalsRelatedByBidangStudiId->contains($obj)) {
                          $this->collRwyPendFormalsRelatedByBidangStudiId->append($obj);
                        }
                      }

                      $this->collRwyPendFormalsRelatedByBidangStudiIdPartial = true;
                    }

                    $collRwyPendFormalsRelatedByBidangStudiId->getInternalIterator()->rewind();
                    return $collRwyPendFormalsRelatedByBidangStudiId;
                }

                if($partial && $this->collRwyPendFormalsRelatedByBidangStudiId) {
                    foreach($this->collRwyPendFormalsRelatedByBidangStudiId as $obj) {
                        if($obj->isNew()) {
                            $collRwyPendFormalsRelatedByBidangStudiId[] = $obj;
                        }
                    }
                }

                $this->collRwyPendFormalsRelatedByBidangStudiId = $collRwyPendFormalsRelatedByBidangStudiId;
                $this->collRwyPendFormalsRelatedByBidangStudiIdPartial = false;
            }
        }

        return $this->collRwyPendFormalsRelatedByBidangStudiId;
    }

    /**
     * Sets a collection of RwyPendFormalRelatedByBidangStudiId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rwyPendFormalsRelatedByBidangStudiId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return BidangStudi The current object (for fluent API support)
     */
    public function setRwyPendFormalsRelatedByBidangStudiId(PropelCollection $rwyPendFormalsRelatedByBidangStudiId, PropelPDO $con = null)
    {
        $rwyPendFormalsRelatedByBidangStudiIdToDelete = $this->getRwyPendFormalsRelatedByBidangStudiId(new Criteria(), $con)->diff($rwyPendFormalsRelatedByBidangStudiId);

        $this->rwyPendFormalsRelatedByBidangStudiIdScheduledForDeletion = unserialize(serialize($rwyPendFormalsRelatedByBidangStudiIdToDelete));

        foreach ($rwyPendFormalsRelatedByBidangStudiIdToDelete as $rwyPendFormalRelatedByBidangStudiIdRemoved) {
            $rwyPendFormalRelatedByBidangStudiIdRemoved->setBidangStudiRelatedByBidangStudiId(null);
        }

        $this->collRwyPendFormalsRelatedByBidangStudiId = null;
        foreach ($rwyPendFormalsRelatedByBidangStudiId as $rwyPendFormalRelatedByBidangStudiId) {
            $this->addRwyPendFormalRelatedByBidangStudiId($rwyPendFormalRelatedByBidangStudiId);
        }

        $this->collRwyPendFormalsRelatedByBidangStudiId = $rwyPendFormalsRelatedByBidangStudiId;
        $this->collRwyPendFormalsRelatedByBidangStudiIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RwyPendFormal objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RwyPendFormal objects.
     * @throws PropelException
     */
    public function countRwyPendFormalsRelatedByBidangStudiId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRwyPendFormalsRelatedByBidangStudiIdPartial && !$this->isNew();
        if (null === $this->collRwyPendFormalsRelatedByBidangStudiId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRwyPendFormalsRelatedByBidangStudiId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getRwyPendFormalsRelatedByBidangStudiId());
            }
            $query = RwyPendFormalQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByBidangStudiRelatedByBidangStudiId($this)
                ->count($con);
        }

        return count($this->collRwyPendFormalsRelatedByBidangStudiId);
    }

    /**
     * Method called to associate a RwyPendFormal object to this object
     * through the RwyPendFormal foreign key attribute.
     *
     * @param    RwyPendFormal $l RwyPendFormal
     * @return BidangStudi The current object (for fluent API support)
     */
    public function addRwyPendFormalRelatedByBidangStudiId(RwyPendFormal $l)
    {
        if ($this->collRwyPendFormalsRelatedByBidangStudiId === null) {
            $this->initRwyPendFormalsRelatedByBidangStudiId();
            $this->collRwyPendFormalsRelatedByBidangStudiIdPartial = true;
        }
        if (!in_array($l, $this->collRwyPendFormalsRelatedByBidangStudiId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRwyPendFormalRelatedByBidangStudiId($l);
        }

        return $this;
    }

    /**
     * @param	RwyPendFormalRelatedByBidangStudiId $rwyPendFormalRelatedByBidangStudiId The rwyPendFormalRelatedByBidangStudiId object to add.
     */
    protected function doAddRwyPendFormalRelatedByBidangStudiId($rwyPendFormalRelatedByBidangStudiId)
    {
        $this->collRwyPendFormalsRelatedByBidangStudiId[]= $rwyPendFormalRelatedByBidangStudiId;
        $rwyPendFormalRelatedByBidangStudiId->setBidangStudiRelatedByBidangStudiId($this);
    }

    /**
     * @param	RwyPendFormalRelatedByBidangStudiId $rwyPendFormalRelatedByBidangStudiId The rwyPendFormalRelatedByBidangStudiId object to remove.
     * @return BidangStudi The current object (for fluent API support)
     */
    public function removeRwyPendFormalRelatedByBidangStudiId($rwyPendFormalRelatedByBidangStudiId)
    {
        if ($this->getRwyPendFormalsRelatedByBidangStudiId()->contains($rwyPendFormalRelatedByBidangStudiId)) {
            $this->collRwyPendFormalsRelatedByBidangStudiId->remove($this->collRwyPendFormalsRelatedByBidangStudiId->search($rwyPendFormalRelatedByBidangStudiId));
            if (null === $this->rwyPendFormalsRelatedByBidangStudiIdScheduledForDeletion) {
                $this->rwyPendFormalsRelatedByBidangStudiIdScheduledForDeletion = clone $this->collRwyPendFormalsRelatedByBidangStudiId;
                $this->rwyPendFormalsRelatedByBidangStudiIdScheduledForDeletion->clear();
            }
            $this->rwyPendFormalsRelatedByBidangStudiIdScheduledForDeletion[]= clone $rwyPendFormalRelatedByBidangStudiId;
            $rwyPendFormalRelatedByBidangStudiId->setBidangStudiRelatedByBidangStudiId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related RwyPendFormalsRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwyPendFormal[] List of RwyPendFormal objects
     */
    public function getRwyPendFormalsRelatedByBidangStudiIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwyPendFormalQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getRwyPendFormalsRelatedByBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related RwyPendFormalsRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwyPendFormal[] List of RwyPendFormal objects
     */
    public function getRwyPendFormalsRelatedByBidangStudiIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwyPendFormalQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getRwyPendFormalsRelatedByBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related RwyPendFormalsRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwyPendFormal[] List of RwyPendFormal objects
     */
    public function getRwyPendFormalsRelatedByBidangStudiIdJoinGelarAkademikRelatedByGelarAkademikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwyPendFormalQuery::create(null, $criteria);
        $query->joinWith('GelarAkademikRelatedByGelarAkademikId', $join_behavior);

        return $this->getRwyPendFormalsRelatedByBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related RwyPendFormalsRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwyPendFormal[] List of RwyPendFormal objects
     */
    public function getRwyPendFormalsRelatedByBidangStudiIdJoinGelarAkademikRelatedByGelarAkademikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwyPendFormalQuery::create(null, $criteria);
        $query->joinWith('GelarAkademikRelatedByGelarAkademikId', $join_behavior);

        return $this->getRwyPendFormalsRelatedByBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related RwyPendFormalsRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwyPendFormal[] List of RwyPendFormal objects
     */
    public function getRwyPendFormalsRelatedByBidangStudiIdJoinJenjangPendidikanRelatedByJenjangPendidikanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwyPendFormalQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanId', $join_behavior);

        return $this->getRwyPendFormalsRelatedByBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related RwyPendFormalsRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwyPendFormal[] List of RwyPendFormal objects
     */
    public function getRwyPendFormalsRelatedByBidangStudiIdJoinJenjangPendidikanRelatedByJenjangPendidikanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwyPendFormalQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanId', $join_behavior);

        return $this->getRwyPendFormalsRelatedByBidangStudiId($query, $con);
    }

    /**
     * Clears out the collRwyPendFormalsRelatedByBidangStudiId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return BidangStudi The current object (for fluent API support)
     * @see        addRwyPendFormalsRelatedByBidangStudiId()
     */
    public function clearRwyPendFormalsRelatedByBidangStudiId()
    {
        $this->collRwyPendFormalsRelatedByBidangStudiId = null; // important to set this to null since that means it is uninitialized
        $this->collRwyPendFormalsRelatedByBidangStudiIdPartial = null;

        return $this;
    }

    /**
     * reset is the collRwyPendFormalsRelatedByBidangStudiId collection loaded partially
     *
     * @return void
     */
    public function resetPartialRwyPendFormalsRelatedByBidangStudiId($v = true)
    {
        $this->collRwyPendFormalsRelatedByBidangStudiIdPartial = $v;
    }

    /**
     * Initializes the collRwyPendFormalsRelatedByBidangStudiId collection.
     *
     * By default this just sets the collRwyPendFormalsRelatedByBidangStudiId collection to an empty array (like clearcollRwyPendFormalsRelatedByBidangStudiId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRwyPendFormalsRelatedByBidangStudiId($overrideExisting = true)
    {
        if (null !== $this->collRwyPendFormalsRelatedByBidangStudiId && !$overrideExisting) {
            return;
        }
        $this->collRwyPendFormalsRelatedByBidangStudiId = new PropelObjectCollection();
        $this->collRwyPendFormalsRelatedByBidangStudiId->setModel('RwyPendFormal');
    }

    /**
     * Gets an array of RwyPendFormal objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this BidangStudi is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RwyPendFormal[] List of RwyPendFormal objects
     * @throws PropelException
     */
    public function getRwyPendFormalsRelatedByBidangStudiId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRwyPendFormalsRelatedByBidangStudiIdPartial && !$this->isNew();
        if (null === $this->collRwyPendFormalsRelatedByBidangStudiId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRwyPendFormalsRelatedByBidangStudiId) {
                // return empty collection
                $this->initRwyPendFormalsRelatedByBidangStudiId();
            } else {
                $collRwyPendFormalsRelatedByBidangStudiId = RwyPendFormalQuery::create(null, $criteria)
                    ->filterByBidangStudiRelatedByBidangStudiId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRwyPendFormalsRelatedByBidangStudiIdPartial && count($collRwyPendFormalsRelatedByBidangStudiId)) {
                      $this->initRwyPendFormalsRelatedByBidangStudiId(false);

                      foreach($collRwyPendFormalsRelatedByBidangStudiId as $obj) {
                        if (false == $this->collRwyPendFormalsRelatedByBidangStudiId->contains($obj)) {
                          $this->collRwyPendFormalsRelatedByBidangStudiId->append($obj);
                        }
                      }

                      $this->collRwyPendFormalsRelatedByBidangStudiIdPartial = true;
                    }

                    $collRwyPendFormalsRelatedByBidangStudiId->getInternalIterator()->rewind();
                    return $collRwyPendFormalsRelatedByBidangStudiId;
                }

                if($partial && $this->collRwyPendFormalsRelatedByBidangStudiId) {
                    foreach($this->collRwyPendFormalsRelatedByBidangStudiId as $obj) {
                        if($obj->isNew()) {
                            $collRwyPendFormalsRelatedByBidangStudiId[] = $obj;
                        }
                    }
                }

                $this->collRwyPendFormalsRelatedByBidangStudiId = $collRwyPendFormalsRelatedByBidangStudiId;
                $this->collRwyPendFormalsRelatedByBidangStudiIdPartial = false;
            }
        }

        return $this->collRwyPendFormalsRelatedByBidangStudiId;
    }

    /**
     * Sets a collection of RwyPendFormalRelatedByBidangStudiId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rwyPendFormalsRelatedByBidangStudiId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return BidangStudi The current object (for fluent API support)
     */
    public function setRwyPendFormalsRelatedByBidangStudiId(PropelCollection $rwyPendFormalsRelatedByBidangStudiId, PropelPDO $con = null)
    {
        $rwyPendFormalsRelatedByBidangStudiIdToDelete = $this->getRwyPendFormalsRelatedByBidangStudiId(new Criteria(), $con)->diff($rwyPendFormalsRelatedByBidangStudiId);

        $this->rwyPendFormalsRelatedByBidangStudiIdScheduledForDeletion = unserialize(serialize($rwyPendFormalsRelatedByBidangStudiIdToDelete));

        foreach ($rwyPendFormalsRelatedByBidangStudiIdToDelete as $rwyPendFormalRelatedByBidangStudiIdRemoved) {
            $rwyPendFormalRelatedByBidangStudiIdRemoved->setBidangStudiRelatedByBidangStudiId(null);
        }

        $this->collRwyPendFormalsRelatedByBidangStudiId = null;
        foreach ($rwyPendFormalsRelatedByBidangStudiId as $rwyPendFormalRelatedByBidangStudiId) {
            $this->addRwyPendFormalRelatedByBidangStudiId($rwyPendFormalRelatedByBidangStudiId);
        }

        $this->collRwyPendFormalsRelatedByBidangStudiId = $rwyPendFormalsRelatedByBidangStudiId;
        $this->collRwyPendFormalsRelatedByBidangStudiIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RwyPendFormal objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RwyPendFormal objects.
     * @throws PropelException
     */
    public function countRwyPendFormalsRelatedByBidangStudiId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRwyPendFormalsRelatedByBidangStudiIdPartial && !$this->isNew();
        if (null === $this->collRwyPendFormalsRelatedByBidangStudiId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRwyPendFormalsRelatedByBidangStudiId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getRwyPendFormalsRelatedByBidangStudiId());
            }
            $query = RwyPendFormalQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByBidangStudiRelatedByBidangStudiId($this)
                ->count($con);
        }

        return count($this->collRwyPendFormalsRelatedByBidangStudiId);
    }

    /**
     * Method called to associate a RwyPendFormal object to this object
     * through the RwyPendFormal foreign key attribute.
     *
     * @param    RwyPendFormal $l RwyPendFormal
     * @return BidangStudi The current object (for fluent API support)
     */
    public function addRwyPendFormalRelatedByBidangStudiId(RwyPendFormal $l)
    {
        if ($this->collRwyPendFormalsRelatedByBidangStudiId === null) {
            $this->initRwyPendFormalsRelatedByBidangStudiId();
            $this->collRwyPendFormalsRelatedByBidangStudiIdPartial = true;
        }
        if (!in_array($l, $this->collRwyPendFormalsRelatedByBidangStudiId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRwyPendFormalRelatedByBidangStudiId($l);
        }

        return $this;
    }

    /**
     * @param	RwyPendFormalRelatedByBidangStudiId $rwyPendFormalRelatedByBidangStudiId The rwyPendFormalRelatedByBidangStudiId object to add.
     */
    protected function doAddRwyPendFormalRelatedByBidangStudiId($rwyPendFormalRelatedByBidangStudiId)
    {
        $this->collRwyPendFormalsRelatedByBidangStudiId[]= $rwyPendFormalRelatedByBidangStudiId;
        $rwyPendFormalRelatedByBidangStudiId->setBidangStudiRelatedByBidangStudiId($this);
    }

    /**
     * @param	RwyPendFormalRelatedByBidangStudiId $rwyPendFormalRelatedByBidangStudiId The rwyPendFormalRelatedByBidangStudiId object to remove.
     * @return BidangStudi The current object (for fluent API support)
     */
    public function removeRwyPendFormalRelatedByBidangStudiId($rwyPendFormalRelatedByBidangStudiId)
    {
        if ($this->getRwyPendFormalsRelatedByBidangStudiId()->contains($rwyPendFormalRelatedByBidangStudiId)) {
            $this->collRwyPendFormalsRelatedByBidangStudiId->remove($this->collRwyPendFormalsRelatedByBidangStudiId->search($rwyPendFormalRelatedByBidangStudiId));
            if (null === $this->rwyPendFormalsRelatedByBidangStudiIdScheduledForDeletion) {
                $this->rwyPendFormalsRelatedByBidangStudiIdScheduledForDeletion = clone $this->collRwyPendFormalsRelatedByBidangStudiId;
                $this->rwyPendFormalsRelatedByBidangStudiIdScheduledForDeletion->clear();
            }
            $this->rwyPendFormalsRelatedByBidangStudiIdScheduledForDeletion[]= clone $rwyPendFormalRelatedByBidangStudiId;
            $rwyPendFormalRelatedByBidangStudiId->setBidangStudiRelatedByBidangStudiId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related RwyPendFormalsRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwyPendFormal[] List of RwyPendFormal objects
     */
    public function getRwyPendFormalsRelatedByBidangStudiIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwyPendFormalQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getRwyPendFormalsRelatedByBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related RwyPendFormalsRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwyPendFormal[] List of RwyPendFormal objects
     */
    public function getRwyPendFormalsRelatedByBidangStudiIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwyPendFormalQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getRwyPendFormalsRelatedByBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related RwyPendFormalsRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwyPendFormal[] List of RwyPendFormal objects
     */
    public function getRwyPendFormalsRelatedByBidangStudiIdJoinGelarAkademikRelatedByGelarAkademikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwyPendFormalQuery::create(null, $criteria);
        $query->joinWith('GelarAkademikRelatedByGelarAkademikId', $join_behavior);

        return $this->getRwyPendFormalsRelatedByBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related RwyPendFormalsRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwyPendFormal[] List of RwyPendFormal objects
     */
    public function getRwyPendFormalsRelatedByBidangStudiIdJoinGelarAkademikRelatedByGelarAkademikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwyPendFormalQuery::create(null, $criteria);
        $query->joinWith('GelarAkademikRelatedByGelarAkademikId', $join_behavior);

        return $this->getRwyPendFormalsRelatedByBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related RwyPendFormalsRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwyPendFormal[] List of RwyPendFormal objects
     */
    public function getRwyPendFormalsRelatedByBidangStudiIdJoinJenjangPendidikanRelatedByJenjangPendidikanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwyPendFormalQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanId', $join_behavior);

        return $this->getRwyPendFormalsRelatedByBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related RwyPendFormalsRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwyPendFormal[] List of RwyPendFormal objects
     */
    public function getRwyPendFormalsRelatedByBidangStudiIdJoinJenjangPendidikanRelatedByJenjangPendidikanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwyPendFormalQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanId', $join_behavior);

        return $this->getRwyPendFormalsRelatedByBidangStudiId($query, $con);
    }

    /**
     * Clears out the collRwySertifikasisRelatedByBidangStudiId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return BidangStudi The current object (for fluent API support)
     * @see        addRwySertifikasisRelatedByBidangStudiId()
     */
    public function clearRwySertifikasisRelatedByBidangStudiId()
    {
        $this->collRwySertifikasisRelatedByBidangStudiId = null; // important to set this to null since that means it is uninitialized
        $this->collRwySertifikasisRelatedByBidangStudiIdPartial = null;

        return $this;
    }

    /**
     * reset is the collRwySertifikasisRelatedByBidangStudiId collection loaded partially
     *
     * @return void
     */
    public function resetPartialRwySertifikasisRelatedByBidangStudiId($v = true)
    {
        $this->collRwySertifikasisRelatedByBidangStudiIdPartial = $v;
    }

    /**
     * Initializes the collRwySertifikasisRelatedByBidangStudiId collection.
     *
     * By default this just sets the collRwySertifikasisRelatedByBidangStudiId collection to an empty array (like clearcollRwySertifikasisRelatedByBidangStudiId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRwySertifikasisRelatedByBidangStudiId($overrideExisting = true)
    {
        if (null !== $this->collRwySertifikasisRelatedByBidangStudiId && !$overrideExisting) {
            return;
        }
        $this->collRwySertifikasisRelatedByBidangStudiId = new PropelObjectCollection();
        $this->collRwySertifikasisRelatedByBidangStudiId->setModel('RwySertifikasi');
    }

    /**
     * Gets an array of RwySertifikasi objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this BidangStudi is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RwySertifikasi[] List of RwySertifikasi objects
     * @throws PropelException
     */
    public function getRwySertifikasisRelatedByBidangStudiId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRwySertifikasisRelatedByBidangStudiIdPartial && !$this->isNew();
        if (null === $this->collRwySertifikasisRelatedByBidangStudiId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRwySertifikasisRelatedByBidangStudiId) {
                // return empty collection
                $this->initRwySertifikasisRelatedByBidangStudiId();
            } else {
                $collRwySertifikasisRelatedByBidangStudiId = RwySertifikasiQuery::create(null, $criteria)
                    ->filterByBidangStudiRelatedByBidangStudiId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRwySertifikasisRelatedByBidangStudiIdPartial && count($collRwySertifikasisRelatedByBidangStudiId)) {
                      $this->initRwySertifikasisRelatedByBidangStudiId(false);

                      foreach($collRwySertifikasisRelatedByBidangStudiId as $obj) {
                        if (false == $this->collRwySertifikasisRelatedByBidangStudiId->contains($obj)) {
                          $this->collRwySertifikasisRelatedByBidangStudiId->append($obj);
                        }
                      }

                      $this->collRwySertifikasisRelatedByBidangStudiIdPartial = true;
                    }

                    $collRwySertifikasisRelatedByBidangStudiId->getInternalIterator()->rewind();
                    return $collRwySertifikasisRelatedByBidangStudiId;
                }

                if($partial && $this->collRwySertifikasisRelatedByBidangStudiId) {
                    foreach($this->collRwySertifikasisRelatedByBidangStudiId as $obj) {
                        if($obj->isNew()) {
                            $collRwySertifikasisRelatedByBidangStudiId[] = $obj;
                        }
                    }
                }

                $this->collRwySertifikasisRelatedByBidangStudiId = $collRwySertifikasisRelatedByBidangStudiId;
                $this->collRwySertifikasisRelatedByBidangStudiIdPartial = false;
            }
        }

        return $this->collRwySertifikasisRelatedByBidangStudiId;
    }

    /**
     * Sets a collection of RwySertifikasiRelatedByBidangStudiId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rwySertifikasisRelatedByBidangStudiId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return BidangStudi The current object (for fluent API support)
     */
    public function setRwySertifikasisRelatedByBidangStudiId(PropelCollection $rwySertifikasisRelatedByBidangStudiId, PropelPDO $con = null)
    {
        $rwySertifikasisRelatedByBidangStudiIdToDelete = $this->getRwySertifikasisRelatedByBidangStudiId(new Criteria(), $con)->diff($rwySertifikasisRelatedByBidangStudiId);

        $this->rwySertifikasisRelatedByBidangStudiIdScheduledForDeletion = unserialize(serialize($rwySertifikasisRelatedByBidangStudiIdToDelete));

        foreach ($rwySertifikasisRelatedByBidangStudiIdToDelete as $rwySertifikasiRelatedByBidangStudiIdRemoved) {
            $rwySertifikasiRelatedByBidangStudiIdRemoved->setBidangStudiRelatedByBidangStudiId(null);
        }

        $this->collRwySertifikasisRelatedByBidangStudiId = null;
        foreach ($rwySertifikasisRelatedByBidangStudiId as $rwySertifikasiRelatedByBidangStudiId) {
            $this->addRwySertifikasiRelatedByBidangStudiId($rwySertifikasiRelatedByBidangStudiId);
        }

        $this->collRwySertifikasisRelatedByBidangStudiId = $rwySertifikasisRelatedByBidangStudiId;
        $this->collRwySertifikasisRelatedByBidangStudiIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RwySertifikasi objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RwySertifikasi objects.
     * @throws PropelException
     */
    public function countRwySertifikasisRelatedByBidangStudiId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRwySertifikasisRelatedByBidangStudiIdPartial && !$this->isNew();
        if (null === $this->collRwySertifikasisRelatedByBidangStudiId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRwySertifikasisRelatedByBidangStudiId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getRwySertifikasisRelatedByBidangStudiId());
            }
            $query = RwySertifikasiQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByBidangStudiRelatedByBidangStudiId($this)
                ->count($con);
        }

        return count($this->collRwySertifikasisRelatedByBidangStudiId);
    }

    /**
     * Method called to associate a RwySertifikasi object to this object
     * through the RwySertifikasi foreign key attribute.
     *
     * @param    RwySertifikasi $l RwySertifikasi
     * @return BidangStudi The current object (for fluent API support)
     */
    public function addRwySertifikasiRelatedByBidangStudiId(RwySertifikasi $l)
    {
        if ($this->collRwySertifikasisRelatedByBidangStudiId === null) {
            $this->initRwySertifikasisRelatedByBidangStudiId();
            $this->collRwySertifikasisRelatedByBidangStudiIdPartial = true;
        }
        if (!in_array($l, $this->collRwySertifikasisRelatedByBidangStudiId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRwySertifikasiRelatedByBidangStudiId($l);
        }

        return $this;
    }

    /**
     * @param	RwySertifikasiRelatedByBidangStudiId $rwySertifikasiRelatedByBidangStudiId The rwySertifikasiRelatedByBidangStudiId object to add.
     */
    protected function doAddRwySertifikasiRelatedByBidangStudiId($rwySertifikasiRelatedByBidangStudiId)
    {
        $this->collRwySertifikasisRelatedByBidangStudiId[]= $rwySertifikasiRelatedByBidangStudiId;
        $rwySertifikasiRelatedByBidangStudiId->setBidangStudiRelatedByBidangStudiId($this);
    }

    /**
     * @param	RwySertifikasiRelatedByBidangStudiId $rwySertifikasiRelatedByBidangStudiId The rwySertifikasiRelatedByBidangStudiId object to remove.
     * @return BidangStudi The current object (for fluent API support)
     */
    public function removeRwySertifikasiRelatedByBidangStudiId($rwySertifikasiRelatedByBidangStudiId)
    {
        if ($this->getRwySertifikasisRelatedByBidangStudiId()->contains($rwySertifikasiRelatedByBidangStudiId)) {
            $this->collRwySertifikasisRelatedByBidangStudiId->remove($this->collRwySertifikasisRelatedByBidangStudiId->search($rwySertifikasiRelatedByBidangStudiId));
            if (null === $this->rwySertifikasisRelatedByBidangStudiIdScheduledForDeletion) {
                $this->rwySertifikasisRelatedByBidangStudiIdScheduledForDeletion = clone $this->collRwySertifikasisRelatedByBidangStudiId;
                $this->rwySertifikasisRelatedByBidangStudiIdScheduledForDeletion->clear();
            }
            $this->rwySertifikasisRelatedByBidangStudiIdScheduledForDeletion[]= clone $rwySertifikasiRelatedByBidangStudiId;
            $rwySertifikasiRelatedByBidangStudiId->setBidangStudiRelatedByBidangStudiId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related RwySertifikasisRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwySertifikasi[] List of RwySertifikasi objects
     */
    public function getRwySertifikasisRelatedByBidangStudiIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwySertifikasiQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getRwySertifikasisRelatedByBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related RwySertifikasisRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwySertifikasi[] List of RwySertifikasi objects
     */
    public function getRwySertifikasisRelatedByBidangStudiIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwySertifikasiQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getRwySertifikasisRelatedByBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related RwySertifikasisRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwySertifikasi[] List of RwySertifikasi objects
     */
    public function getRwySertifikasisRelatedByBidangStudiIdJoinJenisSertifikasiRelatedByIdJenisSertifikasi($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwySertifikasiQuery::create(null, $criteria);
        $query->joinWith('JenisSertifikasiRelatedByIdJenisSertifikasi', $join_behavior);

        return $this->getRwySertifikasisRelatedByBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related RwySertifikasisRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwySertifikasi[] List of RwySertifikasi objects
     */
    public function getRwySertifikasisRelatedByBidangStudiIdJoinJenisSertifikasiRelatedByIdJenisSertifikasi($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwySertifikasiQuery::create(null, $criteria);
        $query->joinWith('JenisSertifikasiRelatedByIdJenisSertifikasi', $join_behavior);

        return $this->getRwySertifikasisRelatedByBidangStudiId($query, $con);
    }

    /**
     * Clears out the collRwySertifikasisRelatedByBidangStudiId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return BidangStudi The current object (for fluent API support)
     * @see        addRwySertifikasisRelatedByBidangStudiId()
     */
    public function clearRwySertifikasisRelatedByBidangStudiId()
    {
        $this->collRwySertifikasisRelatedByBidangStudiId = null; // important to set this to null since that means it is uninitialized
        $this->collRwySertifikasisRelatedByBidangStudiIdPartial = null;

        return $this;
    }

    /**
     * reset is the collRwySertifikasisRelatedByBidangStudiId collection loaded partially
     *
     * @return void
     */
    public function resetPartialRwySertifikasisRelatedByBidangStudiId($v = true)
    {
        $this->collRwySertifikasisRelatedByBidangStudiIdPartial = $v;
    }

    /**
     * Initializes the collRwySertifikasisRelatedByBidangStudiId collection.
     *
     * By default this just sets the collRwySertifikasisRelatedByBidangStudiId collection to an empty array (like clearcollRwySertifikasisRelatedByBidangStudiId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRwySertifikasisRelatedByBidangStudiId($overrideExisting = true)
    {
        if (null !== $this->collRwySertifikasisRelatedByBidangStudiId && !$overrideExisting) {
            return;
        }
        $this->collRwySertifikasisRelatedByBidangStudiId = new PropelObjectCollection();
        $this->collRwySertifikasisRelatedByBidangStudiId->setModel('RwySertifikasi');
    }

    /**
     * Gets an array of RwySertifikasi objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this BidangStudi is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RwySertifikasi[] List of RwySertifikasi objects
     * @throws PropelException
     */
    public function getRwySertifikasisRelatedByBidangStudiId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRwySertifikasisRelatedByBidangStudiIdPartial && !$this->isNew();
        if (null === $this->collRwySertifikasisRelatedByBidangStudiId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRwySertifikasisRelatedByBidangStudiId) {
                // return empty collection
                $this->initRwySertifikasisRelatedByBidangStudiId();
            } else {
                $collRwySertifikasisRelatedByBidangStudiId = RwySertifikasiQuery::create(null, $criteria)
                    ->filterByBidangStudiRelatedByBidangStudiId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRwySertifikasisRelatedByBidangStudiIdPartial && count($collRwySertifikasisRelatedByBidangStudiId)) {
                      $this->initRwySertifikasisRelatedByBidangStudiId(false);

                      foreach($collRwySertifikasisRelatedByBidangStudiId as $obj) {
                        if (false == $this->collRwySertifikasisRelatedByBidangStudiId->contains($obj)) {
                          $this->collRwySertifikasisRelatedByBidangStudiId->append($obj);
                        }
                      }

                      $this->collRwySertifikasisRelatedByBidangStudiIdPartial = true;
                    }

                    $collRwySertifikasisRelatedByBidangStudiId->getInternalIterator()->rewind();
                    return $collRwySertifikasisRelatedByBidangStudiId;
                }

                if($partial && $this->collRwySertifikasisRelatedByBidangStudiId) {
                    foreach($this->collRwySertifikasisRelatedByBidangStudiId as $obj) {
                        if($obj->isNew()) {
                            $collRwySertifikasisRelatedByBidangStudiId[] = $obj;
                        }
                    }
                }

                $this->collRwySertifikasisRelatedByBidangStudiId = $collRwySertifikasisRelatedByBidangStudiId;
                $this->collRwySertifikasisRelatedByBidangStudiIdPartial = false;
            }
        }

        return $this->collRwySertifikasisRelatedByBidangStudiId;
    }

    /**
     * Sets a collection of RwySertifikasiRelatedByBidangStudiId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rwySertifikasisRelatedByBidangStudiId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return BidangStudi The current object (for fluent API support)
     */
    public function setRwySertifikasisRelatedByBidangStudiId(PropelCollection $rwySertifikasisRelatedByBidangStudiId, PropelPDO $con = null)
    {
        $rwySertifikasisRelatedByBidangStudiIdToDelete = $this->getRwySertifikasisRelatedByBidangStudiId(new Criteria(), $con)->diff($rwySertifikasisRelatedByBidangStudiId);

        $this->rwySertifikasisRelatedByBidangStudiIdScheduledForDeletion = unserialize(serialize($rwySertifikasisRelatedByBidangStudiIdToDelete));

        foreach ($rwySertifikasisRelatedByBidangStudiIdToDelete as $rwySertifikasiRelatedByBidangStudiIdRemoved) {
            $rwySertifikasiRelatedByBidangStudiIdRemoved->setBidangStudiRelatedByBidangStudiId(null);
        }

        $this->collRwySertifikasisRelatedByBidangStudiId = null;
        foreach ($rwySertifikasisRelatedByBidangStudiId as $rwySertifikasiRelatedByBidangStudiId) {
            $this->addRwySertifikasiRelatedByBidangStudiId($rwySertifikasiRelatedByBidangStudiId);
        }

        $this->collRwySertifikasisRelatedByBidangStudiId = $rwySertifikasisRelatedByBidangStudiId;
        $this->collRwySertifikasisRelatedByBidangStudiIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RwySertifikasi objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RwySertifikasi objects.
     * @throws PropelException
     */
    public function countRwySertifikasisRelatedByBidangStudiId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRwySertifikasisRelatedByBidangStudiIdPartial && !$this->isNew();
        if (null === $this->collRwySertifikasisRelatedByBidangStudiId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRwySertifikasisRelatedByBidangStudiId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getRwySertifikasisRelatedByBidangStudiId());
            }
            $query = RwySertifikasiQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByBidangStudiRelatedByBidangStudiId($this)
                ->count($con);
        }

        return count($this->collRwySertifikasisRelatedByBidangStudiId);
    }

    /**
     * Method called to associate a RwySertifikasi object to this object
     * through the RwySertifikasi foreign key attribute.
     *
     * @param    RwySertifikasi $l RwySertifikasi
     * @return BidangStudi The current object (for fluent API support)
     */
    public function addRwySertifikasiRelatedByBidangStudiId(RwySertifikasi $l)
    {
        if ($this->collRwySertifikasisRelatedByBidangStudiId === null) {
            $this->initRwySertifikasisRelatedByBidangStudiId();
            $this->collRwySertifikasisRelatedByBidangStudiIdPartial = true;
        }
        if (!in_array($l, $this->collRwySertifikasisRelatedByBidangStudiId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRwySertifikasiRelatedByBidangStudiId($l);
        }

        return $this;
    }

    /**
     * @param	RwySertifikasiRelatedByBidangStudiId $rwySertifikasiRelatedByBidangStudiId The rwySertifikasiRelatedByBidangStudiId object to add.
     */
    protected function doAddRwySertifikasiRelatedByBidangStudiId($rwySertifikasiRelatedByBidangStudiId)
    {
        $this->collRwySertifikasisRelatedByBidangStudiId[]= $rwySertifikasiRelatedByBidangStudiId;
        $rwySertifikasiRelatedByBidangStudiId->setBidangStudiRelatedByBidangStudiId($this);
    }

    /**
     * @param	RwySertifikasiRelatedByBidangStudiId $rwySertifikasiRelatedByBidangStudiId The rwySertifikasiRelatedByBidangStudiId object to remove.
     * @return BidangStudi The current object (for fluent API support)
     */
    public function removeRwySertifikasiRelatedByBidangStudiId($rwySertifikasiRelatedByBidangStudiId)
    {
        if ($this->getRwySertifikasisRelatedByBidangStudiId()->contains($rwySertifikasiRelatedByBidangStudiId)) {
            $this->collRwySertifikasisRelatedByBidangStudiId->remove($this->collRwySertifikasisRelatedByBidangStudiId->search($rwySertifikasiRelatedByBidangStudiId));
            if (null === $this->rwySertifikasisRelatedByBidangStudiIdScheduledForDeletion) {
                $this->rwySertifikasisRelatedByBidangStudiIdScheduledForDeletion = clone $this->collRwySertifikasisRelatedByBidangStudiId;
                $this->rwySertifikasisRelatedByBidangStudiIdScheduledForDeletion->clear();
            }
            $this->rwySertifikasisRelatedByBidangStudiIdScheduledForDeletion[]= clone $rwySertifikasiRelatedByBidangStudiId;
            $rwySertifikasiRelatedByBidangStudiId->setBidangStudiRelatedByBidangStudiId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related RwySertifikasisRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwySertifikasi[] List of RwySertifikasi objects
     */
    public function getRwySertifikasisRelatedByBidangStudiIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwySertifikasiQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getRwySertifikasisRelatedByBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related RwySertifikasisRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwySertifikasi[] List of RwySertifikasi objects
     */
    public function getRwySertifikasisRelatedByBidangStudiIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwySertifikasiQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getRwySertifikasisRelatedByBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related RwySertifikasisRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwySertifikasi[] List of RwySertifikasi objects
     */
    public function getRwySertifikasisRelatedByBidangStudiIdJoinJenisSertifikasiRelatedByIdJenisSertifikasi($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwySertifikasiQuery::create(null, $criteria);
        $query->joinWith('JenisSertifikasiRelatedByIdJenisSertifikasi', $join_behavior);

        return $this->getRwySertifikasisRelatedByBidangStudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BidangStudi is new, it will return
     * an empty collection; or if this BidangStudi has previously
     * been saved, it will retrieve related RwySertifikasisRelatedByBidangStudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BidangStudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwySertifikasi[] List of RwySertifikasi objects
     */
    public function getRwySertifikasisRelatedByBidangStudiIdJoinJenisSertifikasiRelatedByIdJenisSertifikasi($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwySertifikasiQuery::create(null, $criteria);
        $query->joinWith('JenisSertifikasiRelatedByIdJenisSertifikasi', $join_behavior);

        return $this->getRwySertifikasisRelatedByBidangStudiId($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->bidang_studi_id = null;
        $this->kelompok_bidang_studi_id = null;
        $this->kode = null;
        $this->bidang_studi = null;
        $this->kelompok = null;
        $this->jenjang_paud = null;
        $this->jenjang_tk = null;
        $this->jenjang_sd = null;
        $this->jenjang_smp = null;
        $this->jenjang_sma = null;
        $this->jenjang_tinggi = null;
        $this->create_date = null;
        $this->last_update = null;
        $this->expired_date = null;
        $this->last_sync = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collMapBidangMataPelajarans) {
                foreach ($this->collMapBidangMataPelajarans as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collBidangStudisRelatedByBidangStudiId) {
                foreach ($this->collBidangStudisRelatedByBidangStudiId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPtksRelatedByPengawasBidangStudiId) {
                foreach ($this->collPtksRelatedByPengawasBidangStudiId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPtksRelatedByPengawasBidangStudiId) {
                foreach ($this->collPtksRelatedByPengawasBidangStudiId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPengawasTerdaftarsRelatedByBidangStudiId) {
                foreach ($this->collPengawasTerdaftarsRelatedByBidangStudiId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPengawasTerdaftarsRelatedByBidangStudiId) {
                foreach ($this->collPengawasTerdaftarsRelatedByBidangStudiId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRwyPendFormalsRelatedByBidangStudiId) {
                foreach ($this->collRwyPendFormalsRelatedByBidangStudiId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRwyPendFormalsRelatedByBidangStudiId) {
                foreach ($this->collRwyPendFormalsRelatedByBidangStudiId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRwySertifikasisRelatedByBidangStudiId) {
                foreach ($this->collRwySertifikasisRelatedByBidangStudiId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRwySertifikasisRelatedByBidangStudiId) {
                foreach ($this->collRwySertifikasisRelatedByBidangStudiId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aBidangStudiRelatedByKelompokBidangStudiId instanceof Persistent) {
              $this->aBidangStudiRelatedByKelompokBidangStudiId->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collMapBidangMataPelajarans instanceof PropelCollection) {
            $this->collMapBidangMataPelajarans->clearIterator();
        }
        $this->collMapBidangMataPelajarans = null;
        if ($this->collBidangStudisRelatedByBidangStudiId instanceof PropelCollection) {
            $this->collBidangStudisRelatedByBidangStudiId->clearIterator();
        }
        $this->collBidangStudisRelatedByBidangStudiId = null;
        if ($this->collPtksRelatedByPengawasBidangStudiId instanceof PropelCollection) {
            $this->collPtksRelatedByPengawasBidangStudiId->clearIterator();
        }
        $this->collPtksRelatedByPengawasBidangStudiId = null;
        if ($this->collPtksRelatedByPengawasBidangStudiId instanceof PropelCollection) {
            $this->collPtksRelatedByPengawasBidangStudiId->clearIterator();
        }
        $this->collPtksRelatedByPengawasBidangStudiId = null;
        if ($this->collPengawasTerdaftarsRelatedByBidangStudiId instanceof PropelCollection) {
            $this->collPengawasTerdaftarsRelatedByBidangStudiId->clearIterator();
        }
        $this->collPengawasTerdaftarsRelatedByBidangStudiId = null;
        if ($this->collPengawasTerdaftarsRelatedByBidangStudiId instanceof PropelCollection) {
            $this->collPengawasTerdaftarsRelatedByBidangStudiId->clearIterator();
        }
        $this->collPengawasTerdaftarsRelatedByBidangStudiId = null;
        if ($this->collRwyPendFormalsRelatedByBidangStudiId instanceof PropelCollection) {
            $this->collRwyPendFormalsRelatedByBidangStudiId->clearIterator();
        }
        $this->collRwyPendFormalsRelatedByBidangStudiId = null;
        if ($this->collRwyPendFormalsRelatedByBidangStudiId instanceof PropelCollection) {
            $this->collRwyPendFormalsRelatedByBidangStudiId->clearIterator();
        }
        $this->collRwyPendFormalsRelatedByBidangStudiId = null;
        if ($this->collRwySertifikasisRelatedByBidangStudiId instanceof PropelCollection) {
            $this->collRwySertifikasisRelatedByBidangStudiId->clearIterator();
        }
        $this->collRwySertifikasisRelatedByBidangStudiId = null;
        if ($this->collRwySertifikasisRelatedByBidangStudiId instanceof PropelCollection) {
            $this->collRwySertifikasisRelatedByBidangStudiId->clearIterator();
        }
        $this->collRwySertifikasisRelatedByBidangStudiId = null;
        $this->aBidangStudiRelatedByKelompokBidangStudiId = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(BidangStudiPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
