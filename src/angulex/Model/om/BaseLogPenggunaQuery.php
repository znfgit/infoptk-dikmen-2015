<?php

namespace angulex\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\LogPengguna;
use angulex\Model\LogPenggunaPeer;
use angulex\Model\LogPenggunaQuery;
use angulex\Model\Pengguna;

/**
 * Base class that represents a query for the 'log_pengguna' table.
 *
 * 
 *
 * @method LogPenggunaQuery orderByWaktuLogin($order = Criteria::ASC) Order by the waktu_login column
 * @method LogPenggunaQuery orderByPenggunaId($order = Criteria::ASC) Order by the pengguna_id column
 * @method LogPenggunaQuery orderByAlamatIp($order = Criteria::ASC) Order by the alamat_ip column
 * @method LogPenggunaQuery orderByKeterangan($order = Criteria::ASC) Order by the keterangan column
 * @method LogPenggunaQuery orderByWaktuLogout($order = Criteria::ASC) Order by the waktu_logout column
 *
 * @method LogPenggunaQuery groupByWaktuLogin() Group by the waktu_login column
 * @method LogPenggunaQuery groupByPenggunaId() Group by the pengguna_id column
 * @method LogPenggunaQuery groupByAlamatIp() Group by the alamat_ip column
 * @method LogPenggunaQuery groupByKeterangan() Group by the keterangan column
 * @method LogPenggunaQuery groupByWaktuLogout() Group by the waktu_logout column
 *
 * @method LogPenggunaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method LogPenggunaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method LogPenggunaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method LogPenggunaQuery leftJoinPenggunaRelatedByPenggunaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PenggunaRelatedByPenggunaId relation
 * @method LogPenggunaQuery rightJoinPenggunaRelatedByPenggunaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PenggunaRelatedByPenggunaId relation
 * @method LogPenggunaQuery innerJoinPenggunaRelatedByPenggunaId($relationAlias = null) Adds a INNER JOIN clause to the query using the PenggunaRelatedByPenggunaId relation
 *
 * @method LogPenggunaQuery leftJoinPenggunaRelatedByPenggunaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PenggunaRelatedByPenggunaId relation
 * @method LogPenggunaQuery rightJoinPenggunaRelatedByPenggunaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PenggunaRelatedByPenggunaId relation
 * @method LogPenggunaQuery innerJoinPenggunaRelatedByPenggunaId($relationAlias = null) Adds a INNER JOIN clause to the query using the PenggunaRelatedByPenggunaId relation
 *
 * @method LogPengguna findOne(PropelPDO $con = null) Return the first LogPengguna matching the query
 * @method LogPengguna findOneOrCreate(PropelPDO $con = null) Return the first LogPengguna matching the query, or a new LogPengguna object populated from the query conditions when no match is found
 *
 * @method LogPengguna findOneByPenggunaId(string $pengguna_id) Return the first LogPengguna filtered by the pengguna_id column
 * @method LogPengguna findOneByAlamatIp(string $alamat_ip) Return the first LogPengguna filtered by the alamat_ip column
 * @method LogPengguna findOneByKeterangan(string $keterangan) Return the first LogPengguna filtered by the keterangan column
 * @method LogPengguna findOneByWaktuLogout(string $waktu_logout) Return the first LogPengguna filtered by the waktu_logout column
 *
 * @method array findByWaktuLogin(string $waktu_login) Return LogPengguna objects filtered by the waktu_login column
 * @method array findByPenggunaId(string $pengguna_id) Return LogPengguna objects filtered by the pengguna_id column
 * @method array findByAlamatIp(string $alamat_ip) Return LogPengguna objects filtered by the alamat_ip column
 * @method array findByKeterangan(string $keterangan) Return LogPengguna objects filtered by the keterangan column
 * @method array findByWaktuLogout(string $waktu_logout) Return LogPengguna objects filtered by the waktu_logout column
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseLogPenggunaQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseLogPenggunaQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Dapodikmen', $modelName = 'angulex\\Model\\LogPengguna', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new LogPenggunaQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   LogPenggunaQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return LogPenggunaQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof LogPenggunaQuery) {
            return $criteria;
        }
        $query = new LogPenggunaQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   LogPengguna|LogPengguna[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = LogPenggunaPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(LogPenggunaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 LogPengguna A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByWaktuLogin($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 LogPengguna A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [waktu_login], [pengguna_id], [alamat_ip], [keterangan], [waktu_logout] FROM [log_pengguna] WHERE [waktu_login] = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new LogPengguna();
            $obj->hydrate($row);
            LogPenggunaPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return LogPengguna|LogPengguna[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|LogPengguna[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return LogPenggunaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(LogPenggunaPeer::WAKTU_LOGIN, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return LogPenggunaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(LogPenggunaPeer::WAKTU_LOGIN, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the waktu_login column
     *
     * Example usage:
     * <code>
     * $query->filterByWaktuLogin('2011-03-14'); // WHERE waktu_login = '2011-03-14'
     * $query->filterByWaktuLogin('now'); // WHERE waktu_login = '2011-03-14'
     * $query->filterByWaktuLogin(array('max' => 'yesterday')); // WHERE waktu_login > '2011-03-13'
     * </code>
     *
     * @param     mixed $waktuLogin The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogPenggunaQuery The current query, for fluid interface
     */
    public function filterByWaktuLogin($waktuLogin = null, $comparison = null)
    {
        if (is_array($waktuLogin)) {
            $useMinMax = false;
            if (isset($waktuLogin['min'])) {
                $this->addUsingAlias(LogPenggunaPeer::WAKTU_LOGIN, $waktuLogin['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($waktuLogin['max'])) {
                $this->addUsingAlias(LogPenggunaPeer::WAKTU_LOGIN, $waktuLogin['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LogPenggunaPeer::WAKTU_LOGIN, $waktuLogin, $comparison);
    }

    /**
     * Filter the query on the pengguna_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPenggunaId('fooValue');   // WHERE pengguna_id = 'fooValue'
     * $query->filterByPenggunaId('%fooValue%'); // WHERE pengguna_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $penggunaId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogPenggunaQuery The current query, for fluid interface
     */
    public function filterByPenggunaId($penggunaId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($penggunaId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $penggunaId)) {
                $penggunaId = str_replace('*', '%', $penggunaId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LogPenggunaPeer::PENGGUNA_ID, $penggunaId, $comparison);
    }

    /**
     * Filter the query on the alamat_ip column
     *
     * Example usage:
     * <code>
     * $query->filterByAlamatIp('fooValue');   // WHERE alamat_ip = 'fooValue'
     * $query->filterByAlamatIp('%fooValue%'); // WHERE alamat_ip LIKE '%fooValue%'
     * </code>
     *
     * @param     string $alamatIp The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogPenggunaQuery The current query, for fluid interface
     */
    public function filterByAlamatIp($alamatIp = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($alamatIp)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $alamatIp)) {
                $alamatIp = str_replace('*', '%', $alamatIp);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LogPenggunaPeer::ALAMAT_IP, $alamatIp, $comparison);
    }

    /**
     * Filter the query on the keterangan column
     *
     * Example usage:
     * <code>
     * $query->filterByKeterangan('fooValue');   // WHERE keterangan = 'fooValue'
     * $query->filterByKeterangan('%fooValue%'); // WHERE keterangan LIKE '%fooValue%'
     * </code>
     *
     * @param     string $keterangan The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogPenggunaQuery The current query, for fluid interface
     */
    public function filterByKeterangan($keterangan = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($keterangan)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $keterangan)) {
                $keterangan = str_replace('*', '%', $keterangan);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LogPenggunaPeer::KETERANGAN, $keterangan, $comparison);
    }

    /**
     * Filter the query on the waktu_logout column
     *
     * Example usage:
     * <code>
     * $query->filterByWaktuLogout('2011-03-14'); // WHERE waktu_logout = '2011-03-14'
     * $query->filterByWaktuLogout('now'); // WHERE waktu_logout = '2011-03-14'
     * $query->filterByWaktuLogout(array('max' => 'yesterday')); // WHERE waktu_logout > '2011-03-13'
     * </code>
     *
     * @param     mixed $waktuLogout The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogPenggunaQuery The current query, for fluid interface
     */
    public function filterByWaktuLogout($waktuLogout = null, $comparison = null)
    {
        if (is_array($waktuLogout)) {
            $useMinMax = false;
            if (isset($waktuLogout['min'])) {
                $this->addUsingAlias(LogPenggunaPeer::WAKTU_LOGOUT, $waktuLogout['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($waktuLogout['max'])) {
                $this->addUsingAlias(LogPenggunaPeer::WAKTU_LOGOUT, $waktuLogout['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LogPenggunaPeer::WAKTU_LOGOUT, $waktuLogout, $comparison);
    }

    /**
     * Filter the query by a related Pengguna object
     *
     * @param   Pengguna|PropelObjectCollection $pengguna The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 LogPenggunaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPenggunaRelatedByPenggunaId($pengguna, $comparison = null)
    {
        if ($pengguna instanceof Pengguna) {
            return $this
                ->addUsingAlias(LogPenggunaPeer::PENGGUNA_ID, $pengguna->getPenggunaId(), $comparison);
        } elseif ($pengguna instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(LogPenggunaPeer::PENGGUNA_ID, $pengguna->toKeyValue('PrimaryKey', 'PenggunaId'), $comparison);
        } else {
            throw new PropelException('filterByPenggunaRelatedByPenggunaId() only accepts arguments of type Pengguna or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PenggunaRelatedByPenggunaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return LogPenggunaQuery The current query, for fluid interface
     */
    public function joinPenggunaRelatedByPenggunaId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PenggunaRelatedByPenggunaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PenggunaRelatedByPenggunaId');
        }

        return $this;
    }

    /**
     * Use the PenggunaRelatedByPenggunaId relation Pengguna object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PenggunaQuery A secondary query class using the current class as primary query
     */
    public function usePenggunaRelatedByPenggunaIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPenggunaRelatedByPenggunaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PenggunaRelatedByPenggunaId', '\angulex\Model\PenggunaQuery');
    }

    /**
     * Filter the query by a related Pengguna object
     *
     * @param   Pengguna|PropelObjectCollection $pengguna The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 LogPenggunaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPenggunaRelatedByPenggunaId($pengguna, $comparison = null)
    {
        if ($pengguna instanceof Pengguna) {
            return $this
                ->addUsingAlias(LogPenggunaPeer::PENGGUNA_ID, $pengguna->getPenggunaId(), $comparison);
        } elseif ($pengguna instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(LogPenggunaPeer::PENGGUNA_ID, $pengguna->toKeyValue('PrimaryKey', 'PenggunaId'), $comparison);
        } else {
            throw new PropelException('filterByPenggunaRelatedByPenggunaId() only accepts arguments of type Pengguna or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PenggunaRelatedByPenggunaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return LogPenggunaQuery The current query, for fluid interface
     */
    public function joinPenggunaRelatedByPenggunaId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PenggunaRelatedByPenggunaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PenggunaRelatedByPenggunaId');
        }

        return $this;
    }

    /**
     * Use the PenggunaRelatedByPenggunaId relation Pengguna object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PenggunaQuery A secondary query class using the current class as primary query
     */
    public function usePenggunaRelatedByPenggunaIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPenggunaRelatedByPenggunaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PenggunaRelatedByPenggunaId', '\angulex\Model\PenggunaQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   LogPengguna $logPengguna Object to remove from the list of results
     *
     * @return LogPenggunaQuery The current query, for fluid interface
     */
    public function prune($logPengguna = null)
    {
        if ($logPengguna) {
            $this->addUsingAlias(LogPenggunaPeer::WAKTU_LOGIN, $logPengguna->getWaktuLogin(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
