<?php

namespace angulex\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\Sanitasi;
use angulex\Model\SanitasiPeer;
use angulex\Model\SanitasiQuery;
use angulex\Model\Sekolah;
use angulex\Model\Semester;
use angulex\Model\SumberAir;

/**
 * Base class that represents a query for the 'sanitasi' table.
 *
 * 
 *
 * @method SanitasiQuery orderBySekolahId($order = Criteria::ASC) Order by the sekolah_id column
 * @method SanitasiQuery orderBySemesterId($order = Criteria::ASC) Order by the semester_id column
 * @method SanitasiQuery orderBySumberAirId($order = Criteria::ASC) Order by the sumber_air_id column
 * @method SanitasiQuery orderByKetersediaanAir($order = Criteria::ASC) Order by the ketersediaan_air column
 * @method SanitasiQuery orderByKecukupanAir($order = Criteria::ASC) Order by the kecukupan_air column
 * @method SanitasiQuery orderByMinumSiswa($order = Criteria::ASC) Order by the minum_siswa column
 * @method SanitasiQuery orderByMemprosesAir($order = Criteria::ASC) Order by the memproses_air column
 * @method SanitasiQuery orderBySiswaBawaAir($order = Criteria::ASC) Order by the siswa_bawa_air column
 * @method SanitasiQuery orderByToiletSiswaLaki($order = Criteria::ASC) Order by the toilet_siswa_laki column
 * @method SanitasiQuery orderByToiletSiswaPerempuan($order = Criteria::ASC) Order by the toilet_siswa_perempuan column
 * @method SanitasiQuery orderByToiletSiswaKk($order = Criteria::ASC) Order by the toilet_siswa_kk column
 * @method SanitasiQuery orderByToiletSiswaKecil($order = Criteria::ASC) Order by the toilet_siswa_kecil column
 * @method SanitasiQuery orderByTempatCuciTangan($order = Criteria::ASC) Order by the tempat_cuci_tangan column
 * @method SanitasiQuery orderByLastUpdate($order = Criteria::ASC) Order by the Last_update column
 * @method SanitasiQuery orderBySoftDelete($order = Criteria::ASC) Order by the Soft_delete column
 * @method SanitasiQuery orderByLastSync($order = Criteria::ASC) Order by the last_sync column
 * @method SanitasiQuery orderByUpdaterId($order = Criteria::ASC) Order by the Updater_ID column
 *
 * @method SanitasiQuery groupBySekolahId() Group by the sekolah_id column
 * @method SanitasiQuery groupBySemesterId() Group by the semester_id column
 * @method SanitasiQuery groupBySumberAirId() Group by the sumber_air_id column
 * @method SanitasiQuery groupByKetersediaanAir() Group by the ketersediaan_air column
 * @method SanitasiQuery groupByKecukupanAir() Group by the kecukupan_air column
 * @method SanitasiQuery groupByMinumSiswa() Group by the minum_siswa column
 * @method SanitasiQuery groupByMemprosesAir() Group by the memproses_air column
 * @method SanitasiQuery groupBySiswaBawaAir() Group by the siswa_bawa_air column
 * @method SanitasiQuery groupByToiletSiswaLaki() Group by the toilet_siswa_laki column
 * @method SanitasiQuery groupByToiletSiswaPerempuan() Group by the toilet_siswa_perempuan column
 * @method SanitasiQuery groupByToiletSiswaKk() Group by the toilet_siswa_kk column
 * @method SanitasiQuery groupByToiletSiswaKecil() Group by the toilet_siswa_kecil column
 * @method SanitasiQuery groupByTempatCuciTangan() Group by the tempat_cuci_tangan column
 * @method SanitasiQuery groupByLastUpdate() Group by the Last_update column
 * @method SanitasiQuery groupBySoftDelete() Group by the Soft_delete column
 * @method SanitasiQuery groupByLastSync() Group by the last_sync column
 * @method SanitasiQuery groupByUpdaterId() Group by the Updater_ID column
 *
 * @method SanitasiQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SanitasiQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SanitasiQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method SanitasiQuery leftJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method SanitasiQuery rightJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method SanitasiQuery innerJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the SekolahRelatedBySekolahId relation
 *
 * @method SanitasiQuery leftJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method SanitasiQuery rightJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method SanitasiQuery innerJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the SekolahRelatedBySekolahId relation
 *
 * @method SanitasiQuery leftJoinSemesterRelatedBySemesterId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SemesterRelatedBySemesterId relation
 * @method SanitasiQuery rightJoinSemesterRelatedBySemesterId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SemesterRelatedBySemesterId relation
 * @method SanitasiQuery innerJoinSemesterRelatedBySemesterId($relationAlias = null) Adds a INNER JOIN clause to the query using the SemesterRelatedBySemesterId relation
 *
 * @method SanitasiQuery leftJoinSemesterRelatedBySemesterId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SemesterRelatedBySemesterId relation
 * @method SanitasiQuery rightJoinSemesterRelatedBySemesterId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SemesterRelatedBySemesterId relation
 * @method SanitasiQuery innerJoinSemesterRelatedBySemesterId($relationAlias = null) Adds a INNER JOIN clause to the query using the SemesterRelatedBySemesterId relation
 *
 * @method SanitasiQuery leftJoinSumberAirRelatedBySumberAirId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SumberAirRelatedBySumberAirId relation
 * @method SanitasiQuery rightJoinSumberAirRelatedBySumberAirId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SumberAirRelatedBySumberAirId relation
 * @method SanitasiQuery innerJoinSumberAirRelatedBySumberAirId($relationAlias = null) Adds a INNER JOIN clause to the query using the SumberAirRelatedBySumberAirId relation
 *
 * @method SanitasiQuery leftJoinSumberAirRelatedBySumberAirId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SumberAirRelatedBySumberAirId relation
 * @method SanitasiQuery rightJoinSumberAirRelatedBySumberAirId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SumberAirRelatedBySumberAirId relation
 * @method SanitasiQuery innerJoinSumberAirRelatedBySumberAirId($relationAlias = null) Adds a INNER JOIN clause to the query using the SumberAirRelatedBySumberAirId relation
 *
 * @method Sanitasi findOne(PropelPDO $con = null) Return the first Sanitasi matching the query
 * @method Sanitasi findOneOrCreate(PropelPDO $con = null) Return the first Sanitasi matching the query, or a new Sanitasi object populated from the query conditions when no match is found
 *
 * @method Sanitasi findOneBySekolahId(string $sekolah_id) Return the first Sanitasi filtered by the sekolah_id column
 * @method Sanitasi findOneBySemesterId(string $semester_id) Return the first Sanitasi filtered by the semester_id column
 * @method Sanitasi findOneBySumberAirId(string $sumber_air_id) Return the first Sanitasi filtered by the sumber_air_id column
 * @method Sanitasi findOneByKetersediaanAir(string $ketersediaan_air) Return the first Sanitasi filtered by the ketersediaan_air column
 * @method Sanitasi findOneByKecukupanAir(string $kecukupan_air) Return the first Sanitasi filtered by the kecukupan_air column
 * @method Sanitasi findOneByMinumSiswa(string $minum_siswa) Return the first Sanitasi filtered by the minum_siswa column
 * @method Sanitasi findOneByMemprosesAir(string $memproses_air) Return the first Sanitasi filtered by the memproses_air column
 * @method Sanitasi findOneBySiswaBawaAir(string $siswa_bawa_air) Return the first Sanitasi filtered by the siswa_bawa_air column
 * @method Sanitasi findOneByToiletSiswaLaki(string $toilet_siswa_laki) Return the first Sanitasi filtered by the toilet_siswa_laki column
 * @method Sanitasi findOneByToiletSiswaPerempuan(string $toilet_siswa_perempuan) Return the first Sanitasi filtered by the toilet_siswa_perempuan column
 * @method Sanitasi findOneByToiletSiswaKk(string $toilet_siswa_kk) Return the first Sanitasi filtered by the toilet_siswa_kk column
 * @method Sanitasi findOneByToiletSiswaKecil(string $toilet_siswa_kecil) Return the first Sanitasi filtered by the toilet_siswa_kecil column
 * @method Sanitasi findOneByTempatCuciTangan(string $tempat_cuci_tangan) Return the first Sanitasi filtered by the tempat_cuci_tangan column
 * @method Sanitasi findOneByLastUpdate(string $Last_update) Return the first Sanitasi filtered by the Last_update column
 * @method Sanitasi findOneBySoftDelete(string $Soft_delete) Return the first Sanitasi filtered by the Soft_delete column
 * @method Sanitasi findOneByLastSync(string $last_sync) Return the first Sanitasi filtered by the last_sync column
 * @method Sanitasi findOneByUpdaterId(string $Updater_ID) Return the first Sanitasi filtered by the Updater_ID column
 *
 * @method array findBySekolahId(string $sekolah_id) Return Sanitasi objects filtered by the sekolah_id column
 * @method array findBySemesterId(string $semester_id) Return Sanitasi objects filtered by the semester_id column
 * @method array findBySumberAirId(string $sumber_air_id) Return Sanitasi objects filtered by the sumber_air_id column
 * @method array findByKetersediaanAir(string $ketersediaan_air) Return Sanitasi objects filtered by the ketersediaan_air column
 * @method array findByKecukupanAir(string $kecukupan_air) Return Sanitasi objects filtered by the kecukupan_air column
 * @method array findByMinumSiswa(string $minum_siswa) Return Sanitasi objects filtered by the minum_siswa column
 * @method array findByMemprosesAir(string $memproses_air) Return Sanitasi objects filtered by the memproses_air column
 * @method array findBySiswaBawaAir(string $siswa_bawa_air) Return Sanitasi objects filtered by the siswa_bawa_air column
 * @method array findByToiletSiswaLaki(string $toilet_siswa_laki) Return Sanitasi objects filtered by the toilet_siswa_laki column
 * @method array findByToiletSiswaPerempuan(string $toilet_siswa_perempuan) Return Sanitasi objects filtered by the toilet_siswa_perempuan column
 * @method array findByToiletSiswaKk(string $toilet_siswa_kk) Return Sanitasi objects filtered by the toilet_siswa_kk column
 * @method array findByToiletSiswaKecil(string $toilet_siswa_kecil) Return Sanitasi objects filtered by the toilet_siswa_kecil column
 * @method array findByTempatCuciTangan(string $tempat_cuci_tangan) Return Sanitasi objects filtered by the tempat_cuci_tangan column
 * @method array findByLastUpdate(string $Last_update) Return Sanitasi objects filtered by the Last_update column
 * @method array findBySoftDelete(string $Soft_delete) Return Sanitasi objects filtered by the Soft_delete column
 * @method array findByLastSync(string $last_sync) Return Sanitasi objects filtered by the last_sync column
 * @method array findByUpdaterId(string $Updater_ID) Return Sanitasi objects filtered by the Updater_ID column
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseSanitasiQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSanitasiQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Dapodikmen', $modelName = 'angulex\\Model\\Sanitasi', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SanitasiQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   SanitasiQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SanitasiQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SanitasiQuery) {
            return $criteria;
        }
        $query = new SanitasiQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34), $con);
     * </code>
     *
     * @param array $key Primary key to use for the query 
                         A Primary key composition: [$sekolah_id, $semester_id]
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Sanitasi|Sanitasi[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SanitasiPeer::getInstanceFromPool(serialize(array((string) $key[0], (string) $key[1]))))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SanitasiPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Sanitasi A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [sekolah_id], [semester_id], [sumber_air_id], [ketersediaan_air], [kecukupan_air], [minum_siswa], [memproses_air], [siswa_bawa_air], [toilet_siswa_laki], [toilet_siswa_perempuan], [toilet_siswa_kk], [toilet_siswa_kecil], [tempat_cuci_tangan], [Last_update], [Soft_delete], [last_sync], [Updater_ID] FROM [sanitasi] WHERE [sekolah_id] = :p0 AND [semester_id] = :p1';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_STR);			
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Sanitasi();
            $obj->hydrate($row);
            SanitasiPeer::addInstanceToPool($obj, serialize(array((string) $key[0], (string) $key[1])));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Sanitasi|Sanitasi[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Sanitasi[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SanitasiQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(SanitasiPeer::SEKOLAH_ID, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(SanitasiPeer::SEMESTER_ID, $key[1], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SanitasiQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(SanitasiPeer::SEKOLAH_ID, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(SanitasiPeer::SEMESTER_ID, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the sekolah_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySekolahId('fooValue');   // WHERE sekolah_id = 'fooValue'
     * $query->filterBySekolahId('%fooValue%'); // WHERE sekolah_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sekolahId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SanitasiQuery The current query, for fluid interface
     */
    public function filterBySekolahId($sekolahId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sekolahId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $sekolahId)) {
                $sekolahId = str_replace('*', '%', $sekolahId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SanitasiPeer::SEKOLAH_ID, $sekolahId, $comparison);
    }

    /**
     * Filter the query on the semester_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySemesterId('fooValue');   // WHERE semester_id = 'fooValue'
     * $query->filterBySemesterId('%fooValue%'); // WHERE semester_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $semesterId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SanitasiQuery The current query, for fluid interface
     */
    public function filterBySemesterId($semesterId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($semesterId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $semesterId)) {
                $semesterId = str_replace('*', '%', $semesterId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SanitasiPeer::SEMESTER_ID, $semesterId, $comparison);
    }

    /**
     * Filter the query on the sumber_air_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySumberAirId(1234); // WHERE sumber_air_id = 1234
     * $query->filterBySumberAirId(array(12, 34)); // WHERE sumber_air_id IN (12, 34)
     * $query->filterBySumberAirId(array('min' => 12)); // WHERE sumber_air_id >= 12
     * $query->filterBySumberAirId(array('max' => 12)); // WHERE sumber_air_id <= 12
     * </code>
     *
     * @see       filterBySumberAirRelatedBySumberAirId()
     *
     * @see       filterBySumberAirRelatedBySumberAirId()
     *
     * @param     mixed $sumberAirId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SanitasiQuery The current query, for fluid interface
     */
    public function filterBySumberAirId($sumberAirId = null, $comparison = null)
    {
        if (is_array($sumberAirId)) {
            $useMinMax = false;
            if (isset($sumberAirId['min'])) {
                $this->addUsingAlias(SanitasiPeer::SUMBER_AIR_ID, $sumberAirId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sumberAirId['max'])) {
                $this->addUsingAlias(SanitasiPeer::SUMBER_AIR_ID, $sumberAirId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SanitasiPeer::SUMBER_AIR_ID, $sumberAirId, $comparison);
    }

    /**
     * Filter the query on the ketersediaan_air column
     *
     * Example usage:
     * <code>
     * $query->filterByKetersediaanAir(1234); // WHERE ketersediaan_air = 1234
     * $query->filterByKetersediaanAir(array(12, 34)); // WHERE ketersediaan_air IN (12, 34)
     * $query->filterByKetersediaanAir(array('min' => 12)); // WHERE ketersediaan_air >= 12
     * $query->filterByKetersediaanAir(array('max' => 12)); // WHERE ketersediaan_air <= 12
     * </code>
     *
     * @param     mixed $ketersediaanAir The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SanitasiQuery The current query, for fluid interface
     */
    public function filterByKetersediaanAir($ketersediaanAir = null, $comparison = null)
    {
        if (is_array($ketersediaanAir)) {
            $useMinMax = false;
            if (isset($ketersediaanAir['min'])) {
                $this->addUsingAlias(SanitasiPeer::KETERSEDIAAN_AIR, $ketersediaanAir['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($ketersediaanAir['max'])) {
                $this->addUsingAlias(SanitasiPeer::KETERSEDIAAN_AIR, $ketersediaanAir['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SanitasiPeer::KETERSEDIAAN_AIR, $ketersediaanAir, $comparison);
    }

    /**
     * Filter the query on the kecukupan_air column
     *
     * Example usage:
     * <code>
     * $query->filterByKecukupanAir(1234); // WHERE kecukupan_air = 1234
     * $query->filterByKecukupanAir(array(12, 34)); // WHERE kecukupan_air IN (12, 34)
     * $query->filterByKecukupanAir(array('min' => 12)); // WHERE kecukupan_air >= 12
     * $query->filterByKecukupanAir(array('max' => 12)); // WHERE kecukupan_air <= 12
     * </code>
     *
     * @param     mixed $kecukupanAir The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SanitasiQuery The current query, for fluid interface
     */
    public function filterByKecukupanAir($kecukupanAir = null, $comparison = null)
    {
        if (is_array($kecukupanAir)) {
            $useMinMax = false;
            if (isset($kecukupanAir['min'])) {
                $this->addUsingAlias(SanitasiPeer::KECUKUPAN_AIR, $kecukupanAir['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($kecukupanAir['max'])) {
                $this->addUsingAlias(SanitasiPeer::KECUKUPAN_AIR, $kecukupanAir['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SanitasiPeer::KECUKUPAN_AIR, $kecukupanAir, $comparison);
    }

    /**
     * Filter the query on the minum_siswa column
     *
     * Example usage:
     * <code>
     * $query->filterByMinumSiswa(1234); // WHERE minum_siswa = 1234
     * $query->filterByMinumSiswa(array(12, 34)); // WHERE minum_siswa IN (12, 34)
     * $query->filterByMinumSiswa(array('min' => 12)); // WHERE minum_siswa >= 12
     * $query->filterByMinumSiswa(array('max' => 12)); // WHERE minum_siswa <= 12
     * </code>
     *
     * @param     mixed $minumSiswa The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SanitasiQuery The current query, for fluid interface
     */
    public function filterByMinumSiswa($minumSiswa = null, $comparison = null)
    {
        if (is_array($minumSiswa)) {
            $useMinMax = false;
            if (isset($minumSiswa['min'])) {
                $this->addUsingAlias(SanitasiPeer::MINUM_SISWA, $minumSiswa['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($minumSiswa['max'])) {
                $this->addUsingAlias(SanitasiPeer::MINUM_SISWA, $minumSiswa['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SanitasiPeer::MINUM_SISWA, $minumSiswa, $comparison);
    }

    /**
     * Filter the query on the memproses_air column
     *
     * Example usage:
     * <code>
     * $query->filterByMemprosesAir(1234); // WHERE memproses_air = 1234
     * $query->filterByMemprosesAir(array(12, 34)); // WHERE memproses_air IN (12, 34)
     * $query->filterByMemprosesAir(array('min' => 12)); // WHERE memproses_air >= 12
     * $query->filterByMemprosesAir(array('max' => 12)); // WHERE memproses_air <= 12
     * </code>
     *
     * @param     mixed $memprosesAir The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SanitasiQuery The current query, for fluid interface
     */
    public function filterByMemprosesAir($memprosesAir = null, $comparison = null)
    {
        if (is_array($memprosesAir)) {
            $useMinMax = false;
            if (isset($memprosesAir['min'])) {
                $this->addUsingAlias(SanitasiPeer::MEMPROSES_AIR, $memprosesAir['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($memprosesAir['max'])) {
                $this->addUsingAlias(SanitasiPeer::MEMPROSES_AIR, $memprosesAir['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SanitasiPeer::MEMPROSES_AIR, $memprosesAir, $comparison);
    }

    /**
     * Filter the query on the siswa_bawa_air column
     *
     * Example usage:
     * <code>
     * $query->filterBySiswaBawaAir(1234); // WHERE siswa_bawa_air = 1234
     * $query->filterBySiswaBawaAir(array(12, 34)); // WHERE siswa_bawa_air IN (12, 34)
     * $query->filterBySiswaBawaAir(array('min' => 12)); // WHERE siswa_bawa_air >= 12
     * $query->filterBySiswaBawaAir(array('max' => 12)); // WHERE siswa_bawa_air <= 12
     * </code>
     *
     * @param     mixed $siswaBawaAir The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SanitasiQuery The current query, for fluid interface
     */
    public function filterBySiswaBawaAir($siswaBawaAir = null, $comparison = null)
    {
        if (is_array($siswaBawaAir)) {
            $useMinMax = false;
            if (isset($siswaBawaAir['min'])) {
                $this->addUsingAlias(SanitasiPeer::SISWA_BAWA_AIR, $siswaBawaAir['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($siswaBawaAir['max'])) {
                $this->addUsingAlias(SanitasiPeer::SISWA_BAWA_AIR, $siswaBawaAir['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SanitasiPeer::SISWA_BAWA_AIR, $siswaBawaAir, $comparison);
    }

    /**
     * Filter the query on the toilet_siswa_laki column
     *
     * Example usage:
     * <code>
     * $query->filterByToiletSiswaLaki(1234); // WHERE toilet_siswa_laki = 1234
     * $query->filterByToiletSiswaLaki(array(12, 34)); // WHERE toilet_siswa_laki IN (12, 34)
     * $query->filterByToiletSiswaLaki(array('min' => 12)); // WHERE toilet_siswa_laki >= 12
     * $query->filterByToiletSiswaLaki(array('max' => 12)); // WHERE toilet_siswa_laki <= 12
     * </code>
     *
     * @param     mixed $toiletSiswaLaki The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SanitasiQuery The current query, for fluid interface
     */
    public function filterByToiletSiswaLaki($toiletSiswaLaki = null, $comparison = null)
    {
        if (is_array($toiletSiswaLaki)) {
            $useMinMax = false;
            if (isset($toiletSiswaLaki['min'])) {
                $this->addUsingAlias(SanitasiPeer::TOILET_SISWA_LAKI, $toiletSiswaLaki['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($toiletSiswaLaki['max'])) {
                $this->addUsingAlias(SanitasiPeer::TOILET_SISWA_LAKI, $toiletSiswaLaki['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SanitasiPeer::TOILET_SISWA_LAKI, $toiletSiswaLaki, $comparison);
    }

    /**
     * Filter the query on the toilet_siswa_perempuan column
     *
     * Example usage:
     * <code>
     * $query->filterByToiletSiswaPerempuan(1234); // WHERE toilet_siswa_perempuan = 1234
     * $query->filterByToiletSiswaPerempuan(array(12, 34)); // WHERE toilet_siswa_perempuan IN (12, 34)
     * $query->filterByToiletSiswaPerempuan(array('min' => 12)); // WHERE toilet_siswa_perempuan >= 12
     * $query->filterByToiletSiswaPerempuan(array('max' => 12)); // WHERE toilet_siswa_perempuan <= 12
     * </code>
     *
     * @param     mixed $toiletSiswaPerempuan The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SanitasiQuery The current query, for fluid interface
     */
    public function filterByToiletSiswaPerempuan($toiletSiswaPerempuan = null, $comparison = null)
    {
        if (is_array($toiletSiswaPerempuan)) {
            $useMinMax = false;
            if (isset($toiletSiswaPerempuan['min'])) {
                $this->addUsingAlias(SanitasiPeer::TOILET_SISWA_PEREMPUAN, $toiletSiswaPerempuan['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($toiletSiswaPerempuan['max'])) {
                $this->addUsingAlias(SanitasiPeer::TOILET_SISWA_PEREMPUAN, $toiletSiswaPerempuan['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SanitasiPeer::TOILET_SISWA_PEREMPUAN, $toiletSiswaPerempuan, $comparison);
    }

    /**
     * Filter the query on the toilet_siswa_kk column
     *
     * Example usage:
     * <code>
     * $query->filterByToiletSiswaKk(1234); // WHERE toilet_siswa_kk = 1234
     * $query->filterByToiletSiswaKk(array(12, 34)); // WHERE toilet_siswa_kk IN (12, 34)
     * $query->filterByToiletSiswaKk(array('min' => 12)); // WHERE toilet_siswa_kk >= 12
     * $query->filterByToiletSiswaKk(array('max' => 12)); // WHERE toilet_siswa_kk <= 12
     * </code>
     *
     * @param     mixed $toiletSiswaKk The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SanitasiQuery The current query, for fluid interface
     */
    public function filterByToiletSiswaKk($toiletSiswaKk = null, $comparison = null)
    {
        if (is_array($toiletSiswaKk)) {
            $useMinMax = false;
            if (isset($toiletSiswaKk['min'])) {
                $this->addUsingAlias(SanitasiPeer::TOILET_SISWA_KK, $toiletSiswaKk['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($toiletSiswaKk['max'])) {
                $this->addUsingAlias(SanitasiPeer::TOILET_SISWA_KK, $toiletSiswaKk['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SanitasiPeer::TOILET_SISWA_KK, $toiletSiswaKk, $comparison);
    }

    /**
     * Filter the query on the toilet_siswa_kecil column
     *
     * Example usage:
     * <code>
     * $query->filterByToiletSiswaKecil(1234); // WHERE toilet_siswa_kecil = 1234
     * $query->filterByToiletSiswaKecil(array(12, 34)); // WHERE toilet_siswa_kecil IN (12, 34)
     * $query->filterByToiletSiswaKecil(array('min' => 12)); // WHERE toilet_siswa_kecil >= 12
     * $query->filterByToiletSiswaKecil(array('max' => 12)); // WHERE toilet_siswa_kecil <= 12
     * </code>
     *
     * @param     mixed $toiletSiswaKecil The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SanitasiQuery The current query, for fluid interface
     */
    public function filterByToiletSiswaKecil($toiletSiswaKecil = null, $comparison = null)
    {
        if (is_array($toiletSiswaKecil)) {
            $useMinMax = false;
            if (isset($toiletSiswaKecil['min'])) {
                $this->addUsingAlias(SanitasiPeer::TOILET_SISWA_KECIL, $toiletSiswaKecil['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($toiletSiswaKecil['max'])) {
                $this->addUsingAlias(SanitasiPeer::TOILET_SISWA_KECIL, $toiletSiswaKecil['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SanitasiPeer::TOILET_SISWA_KECIL, $toiletSiswaKecil, $comparison);
    }

    /**
     * Filter the query on the tempat_cuci_tangan column
     *
     * Example usage:
     * <code>
     * $query->filterByTempatCuciTangan(1234); // WHERE tempat_cuci_tangan = 1234
     * $query->filterByTempatCuciTangan(array(12, 34)); // WHERE tempat_cuci_tangan IN (12, 34)
     * $query->filterByTempatCuciTangan(array('min' => 12)); // WHERE tempat_cuci_tangan >= 12
     * $query->filterByTempatCuciTangan(array('max' => 12)); // WHERE tempat_cuci_tangan <= 12
     * </code>
     *
     * @param     mixed $tempatCuciTangan The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SanitasiQuery The current query, for fluid interface
     */
    public function filterByTempatCuciTangan($tempatCuciTangan = null, $comparison = null)
    {
        if (is_array($tempatCuciTangan)) {
            $useMinMax = false;
            if (isset($tempatCuciTangan['min'])) {
                $this->addUsingAlias(SanitasiPeer::TEMPAT_CUCI_TANGAN, $tempatCuciTangan['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tempatCuciTangan['max'])) {
                $this->addUsingAlias(SanitasiPeer::TEMPAT_CUCI_TANGAN, $tempatCuciTangan['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SanitasiPeer::TEMPAT_CUCI_TANGAN, $tempatCuciTangan, $comparison);
    }

    /**
     * Filter the query on the Last_update column
     *
     * Example usage:
     * <code>
     * $query->filterByLastUpdate('2011-03-14'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate('now'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate(array('max' => 'yesterday')); // WHERE Last_update > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastUpdate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SanitasiQuery The current query, for fluid interface
     */
    public function filterByLastUpdate($lastUpdate = null, $comparison = null)
    {
        if (is_array($lastUpdate)) {
            $useMinMax = false;
            if (isset($lastUpdate['min'])) {
                $this->addUsingAlias(SanitasiPeer::LAST_UPDATE, $lastUpdate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastUpdate['max'])) {
                $this->addUsingAlias(SanitasiPeer::LAST_UPDATE, $lastUpdate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SanitasiPeer::LAST_UPDATE, $lastUpdate, $comparison);
    }

    /**
     * Filter the query on the Soft_delete column
     *
     * Example usage:
     * <code>
     * $query->filterBySoftDelete(1234); // WHERE Soft_delete = 1234
     * $query->filterBySoftDelete(array(12, 34)); // WHERE Soft_delete IN (12, 34)
     * $query->filterBySoftDelete(array('min' => 12)); // WHERE Soft_delete >= 12
     * $query->filterBySoftDelete(array('max' => 12)); // WHERE Soft_delete <= 12
     * </code>
     *
     * @param     mixed $softDelete The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SanitasiQuery The current query, for fluid interface
     */
    public function filterBySoftDelete($softDelete = null, $comparison = null)
    {
        if (is_array($softDelete)) {
            $useMinMax = false;
            if (isset($softDelete['min'])) {
                $this->addUsingAlias(SanitasiPeer::SOFT_DELETE, $softDelete['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($softDelete['max'])) {
                $this->addUsingAlias(SanitasiPeer::SOFT_DELETE, $softDelete['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SanitasiPeer::SOFT_DELETE, $softDelete, $comparison);
    }

    /**
     * Filter the query on the last_sync column
     *
     * Example usage:
     * <code>
     * $query->filterByLastSync('2011-03-14'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync('now'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync(array('max' => 'yesterday')); // WHERE last_sync > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastSync The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SanitasiQuery The current query, for fluid interface
     */
    public function filterByLastSync($lastSync = null, $comparison = null)
    {
        if (is_array($lastSync)) {
            $useMinMax = false;
            if (isset($lastSync['min'])) {
                $this->addUsingAlias(SanitasiPeer::LAST_SYNC, $lastSync['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastSync['max'])) {
                $this->addUsingAlias(SanitasiPeer::LAST_SYNC, $lastSync['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SanitasiPeer::LAST_SYNC, $lastSync, $comparison);
    }

    /**
     * Filter the query on the Updater_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdaterId('fooValue');   // WHERE Updater_ID = 'fooValue'
     * $query->filterByUpdaterId('%fooValue%'); // WHERE Updater_ID LIKE '%fooValue%'
     * </code>
     *
     * @param     string $updaterId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SanitasiQuery The current query, for fluid interface
     */
    public function filterByUpdaterId($updaterId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($updaterId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $updaterId)) {
                $updaterId = str_replace('*', '%', $updaterId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SanitasiPeer::UPDATER_ID, $updaterId, $comparison);
    }

    /**
     * Filter the query by a related Sekolah object
     *
     * @param   Sekolah|PropelObjectCollection $sekolah The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SanitasiQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySekolahRelatedBySekolahId($sekolah, $comparison = null)
    {
        if ($sekolah instanceof Sekolah) {
            return $this
                ->addUsingAlias(SanitasiPeer::SEKOLAH_ID, $sekolah->getSekolahId(), $comparison);
        } elseif ($sekolah instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SanitasiPeer::SEKOLAH_ID, $sekolah->toKeyValue('PrimaryKey', 'SekolahId'), $comparison);
        } else {
            throw new PropelException('filterBySekolahRelatedBySekolahId() only accepts arguments of type Sekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SekolahRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SanitasiQuery The current query, for fluid interface
     */
    public function joinSekolahRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SekolahRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SekolahRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the SekolahRelatedBySekolahId relation Sekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SekolahQuery A secondary query class using the current class as primary query
     */
    public function useSekolahRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSekolahRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SekolahRelatedBySekolahId', '\angulex\Model\SekolahQuery');
    }

    /**
     * Filter the query by a related Sekolah object
     *
     * @param   Sekolah|PropelObjectCollection $sekolah The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SanitasiQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySekolahRelatedBySekolahId($sekolah, $comparison = null)
    {
        if ($sekolah instanceof Sekolah) {
            return $this
                ->addUsingAlias(SanitasiPeer::SEKOLAH_ID, $sekolah->getSekolahId(), $comparison);
        } elseif ($sekolah instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SanitasiPeer::SEKOLAH_ID, $sekolah->toKeyValue('PrimaryKey', 'SekolahId'), $comparison);
        } else {
            throw new PropelException('filterBySekolahRelatedBySekolahId() only accepts arguments of type Sekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SekolahRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SanitasiQuery The current query, for fluid interface
     */
    public function joinSekolahRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SekolahRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SekolahRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the SekolahRelatedBySekolahId relation Sekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SekolahQuery A secondary query class using the current class as primary query
     */
    public function useSekolahRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSekolahRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SekolahRelatedBySekolahId', '\angulex\Model\SekolahQuery');
    }

    /**
     * Filter the query by a related Semester object
     *
     * @param   Semester|PropelObjectCollection $semester The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SanitasiQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySemesterRelatedBySemesterId($semester, $comparison = null)
    {
        if ($semester instanceof Semester) {
            return $this
                ->addUsingAlias(SanitasiPeer::SEMESTER_ID, $semester->getSemesterId(), $comparison);
        } elseif ($semester instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SanitasiPeer::SEMESTER_ID, $semester->toKeyValue('PrimaryKey', 'SemesterId'), $comparison);
        } else {
            throw new PropelException('filterBySemesterRelatedBySemesterId() only accepts arguments of type Semester or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SemesterRelatedBySemesterId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SanitasiQuery The current query, for fluid interface
     */
    public function joinSemesterRelatedBySemesterId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SemesterRelatedBySemesterId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SemesterRelatedBySemesterId');
        }

        return $this;
    }

    /**
     * Use the SemesterRelatedBySemesterId relation Semester object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SemesterQuery A secondary query class using the current class as primary query
     */
    public function useSemesterRelatedBySemesterIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSemesterRelatedBySemesterId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SemesterRelatedBySemesterId', '\angulex\Model\SemesterQuery');
    }

    /**
     * Filter the query by a related Semester object
     *
     * @param   Semester|PropelObjectCollection $semester The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SanitasiQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySemesterRelatedBySemesterId($semester, $comparison = null)
    {
        if ($semester instanceof Semester) {
            return $this
                ->addUsingAlias(SanitasiPeer::SEMESTER_ID, $semester->getSemesterId(), $comparison);
        } elseif ($semester instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SanitasiPeer::SEMESTER_ID, $semester->toKeyValue('PrimaryKey', 'SemesterId'), $comparison);
        } else {
            throw new PropelException('filterBySemesterRelatedBySemesterId() only accepts arguments of type Semester or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SemesterRelatedBySemesterId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SanitasiQuery The current query, for fluid interface
     */
    public function joinSemesterRelatedBySemesterId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SemesterRelatedBySemesterId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SemesterRelatedBySemesterId');
        }

        return $this;
    }

    /**
     * Use the SemesterRelatedBySemesterId relation Semester object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SemesterQuery A secondary query class using the current class as primary query
     */
    public function useSemesterRelatedBySemesterIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSemesterRelatedBySemesterId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SemesterRelatedBySemesterId', '\angulex\Model\SemesterQuery');
    }

    /**
     * Filter the query by a related SumberAir object
     *
     * @param   SumberAir|PropelObjectCollection $sumberAir The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SanitasiQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySumberAirRelatedBySumberAirId($sumberAir, $comparison = null)
    {
        if ($sumberAir instanceof SumberAir) {
            return $this
                ->addUsingAlias(SanitasiPeer::SUMBER_AIR_ID, $sumberAir->getSumberAirId(), $comparison);
        } elseif ($sumberAir instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SanitasiPeer::SUMBER_AIR_ID, $sumberAir->toKeyValue('PrimaryKey', 'SumberAirId'), $comparison);
        } else {
            throw new PropelException('filterBySumberAirRelatedBySumberAirId() only accepts arguments of type SumberAir or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SumberAirRelatedBySumberAirId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SanitasiQuery The current query, for fluid interface
     */
    public function joinSumberAirRelatedBySumberAirId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SumberAirRelatedBySumberAirId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SumberAirRelatedBySumberAirId');
        }

        return $this;
    }

    /**
     * Use the SumberAirRelatedBySumberAirId relation SumberAir object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SumberAirQuery A secondary query class using the current class as primary query
     */
    public function useSumberAirRelatedBySumberAirIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSumberAirRelatedBySumberAirId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SumberAirRelatedBySumberAirId', '\angulex\Model\SumberAirQuery');
    }

    /**
     * Filter the query by a related SumberAir object
     *
     * @param   SumberAir|PropelObjectCollection $sumberAir The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SanitasiQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySumberAirRelatedBySumberAirId($sumberAir, $comparison = null)
    {
        if ($sumberAir instanceof SumberAir) {
            return $this
                ->addUsingAlias(SanitasiPeer::SUMBER_AIR_ID, $sumberAir->getSumberAirId(), $comparison);
        } elseif ($sumberAir instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SanitasiPeer::SUMBER_AIR_ID, $sumberAir->toKeyValue('PrimaryKey', 'SumberAirId'), $comparison);
        } else {
            throw new PropelException('filterBySumberAirRelatedBySumberAirId() only accepts arguments of type SumberAir or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SumberAirRelatedBySumberAirId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SanitasiQuery The current query, for fluid interface
     */
    public function joinSumberAirRelatedBySumberAirId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SumberAirRelatedBySumberAirId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SumberAirRelatedBySumberAirId');
        }

        return $this;
    }

    /**
     * Use the SumberAirRelatedBySumberAirId relation SumberAir object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SumberAirQuery A secondary query class using the current class as primary query
     */
    public function useSumberAirRelatedBySumberAirIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSumberAirRelatedBySumberAirId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SumberAirRelatedBySumberAirId', '\angulex\Model\SumberAirQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Sanitasi $sanitasi Object to remove from the list of results
     *
     * @return SanitasiQuery The current query, for fluid interface
     */
    public function prune($sanitasi = null)
    {
        if ($sanitasi) {
            $this->addCond('pruneCond0', $this->getAliasedColName(SanitasiPeer::SEKOLAH_ID), $sanitasi->getSekolahId(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(SanitasiPeer::SEMESTER_ID), $sanitasi->getSemesterId(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

}
