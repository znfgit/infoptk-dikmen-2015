<?php

namespace angulex\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelDateTime;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\MataPelajaran;
use angulex\Model\MataPelajaranQuery;
use angulex\Model\Pembelajaran;
use angulex\Model\PembelajaranPeer;
use angulex\Model\PembelajaranQuery;
use angulex\Model\PtkTerdaftar;
use angulex\Model\PtkTerdaftarQuery;
use angulex\Model\RombonganBelajar;
use angulex\Model\RombonganBelajarQuery;
use angulex\Model\Semester;
use angulex\Model\SemesterQuery;
use angulex\Model\VldPembelajaran;
use angulex\Model\VldPembelajaranQuery;

/**
 * Base class that represents a row from the 'pembelajaran' table.
 *
 * 
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BasePembelajaran extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'angulex\\Model\\PembelajaranPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        PembelajaranPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the pembelajaran_id field.
     * @var        string
     */
    protected $pembelajaran_id;

    /**
     * The value for the rombongan_belajar_id field.
     * @var        string
     */
    protected $rombongan_belajar_id;

    /**
     * The value for the semester_id field.
     * @var        string
     */
    protected $semester_id;

    /**
     * The value for the mata_pelajaran_id field.
     * @var        int
     */
    protected $mata_pelajaran_id;

    /**
     * The value for the ptk_terdaftar_id field.
     * @var        string
     */
    protected $ptk_terdaftar_id;

    /**
     * The value for the sk_mengajar field.
     * @var        string
     */
    protected $sk_mengajar;

    /**
     * The value for the tanggal_sk_mengajar field.
     * @var        string
     */
    protected $tanggal_sk_mengajar;

    /**
     * The value for the jam_mengajar_per_minggu field.
     * @var        string
     */
    protected $jam_mengajar_per_minggu;

    /**
     * The value for the status_di_kurikulum field.
     * @var        string
     */
    protected $status_di_kurikulum;

    /**
     * The value for the nama_mata_pelajaran field.
     * @var        string
     */
    protected $nama_mata_pelajaran;

    /**
     * The value for the last_update field.
     * @var        string
     */
    protected $last_update;

    /**
     * The value for the soft_delete field.
     * @var        string
     */
    protected $soft_delete;

    /**
     * The value for the last_sync field.
     * @var        string
     */
    protected $last_sync;

    /**
     * The value for the updater_id field.
     * @var        string
     */
    protected $updater_id;

    /**
     * @var        PtkTerdaftar
     */
    protected $aPtkTerdaftarRelatedByPtkTerdaftarId;

    /**
     * @var        PtkTerdaftar
     */
    protected $aPtkTerdaftarRelatedByPtkTerdaftarId;

    /**
     * @var        RombonganBelajar
     */
    protected $aRombonganBelajarRelatedByRombonganBelajarId;

    /**
     * @var        RombonganBelajar
     */
    protected $aRombonganBelajarRelatedByRombonganBelajarId;

    /**
     * @var        MataPelajaran
     */
    protected $aMataPelajaranRelatedByMataPelajaranId;

    /**
     * @var        MataPelajaran
     */
    protected $aMataPelajaranRelatedByMataPelajaranId;

    /**
     * @var        Semester
     */
    protected $aSemesterRelatedBySemesterId;

    /**
     * @var        Semester
     */
    protected $aSemesterRelatedBySemesterId;

    /**
     * @var        PropelObjectCollection|VldPembelajaran[] Collection to store aggregation of VldPembelajaran objects.
     */
    protected $collVldPembelajaransRelatedByPembelajaranId;
    protected $collVldPembelajaransRelatedByPembelajaranIdPartial;

    /**
     * @var        PropelObjectCollection|VldPembelajaran[] Collection to store aggregation of VldPembelajaran objects.
     */
    protected $collVldPembelajaransRelatedByPembelajaranId;
    protected $collVldPembelajaransRelatedByPembelajaranIdPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldPembelajaransRelatedByPembelajaranIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldPembelajaransRelatedByPembelajaranIdScheduledForDeletion = null;

    /**
     * Get the [pembelajaran_id] column value.
     * 
     * @return string
     */
    public function getPembelajaranId()
    {
        return $this->pembelajaran_id;
    }

    /**
     * Get the [rombongan_belajar_id] column value.
     * 
     * @return string
     */
    public function getRombonganBelajarId()
    {
        return $this->rombongan_belajar_id;
    }

    /**
     * Get the [semester_id] column value.
     * 
     * @return string
     */
    public function getSemesterId()
    {
        return $this->semester_id;
    }

    /**
     * Get the [mata_pelajaran_id] column value.
     * 
     * @return int
     */
    public function getMataPelajaranId()
    {
        return $this->mata_pelajaran_id;
    }

    /**
     * Get the [ptk_terdaftar_id] column value.
     * 
     * @return string
     */
    public function getPtkTerdaftarId()
    {
        return $this->ptk_terdaftar_id;
    }

    /**
     * Get the [sk_mengajar] column value.
     * 
     * @return string
     */
    public function getSkMengajar()
    {
        return $this->sk_mengajar;
    }

    /**
     * Get the [tanggal_sk_mengajar] column value.
     * 
     * @return string
     */
    public function getTanggalSkMengajar()
    {
        return $this->tanggal_sk_mengajar;
    }

    /**
     * Get the [jam_mengajar_per_minggu] column value.
     * 
     * @return string
     */
    public function getJamMengajarPerMinggu()
    {
        return $this->jam_mengajar_per_minggu;
    }

    /**
     * Get the [status_di_kurikulum] column value.
     * 
     * @return string
     */
    public function getStatusDiKurikulum()
    {
        return $this->status_di_kurikulum;
    }

    /**
     * Get the [nama_mata_pelajaran] column value.
     * 
     * @return string
     */
    public function getNamaMataPelajaran()
    {
        return $this->nama_mata_pelajaran;
    }

    /**
     * Get the [optionally formatted] temporal [last_update] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastUpdate($format = 'Y-m-d H:i:s')
    {
        if ($this->last_update === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_update);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_update, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [soft_delete] column value.
     * 
     * @return string
     */
    public function getSoftDelete()
    {
        return $this->soft_delete;
    }

    /**
     * Get the [optionally formatted] temporal [last_sync] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastSync($format = 'Y-m-d H:i:s')
    {
        if ($this->last_sync === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_sync);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_sync, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [updater_id] column value.
     * 
     * @return string
     */
    public function getUpdaterId()
    {
        return $this->updater_id;
    }

    /**
     * Set the value of [pembelajaran_id] column.
     * 
     * @param string $v new value
     * @return Pembelajaran The current object (for fluent API support)
     */
    public function setPembelajaranId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->pembelajaran_id !== $v) {
            $this->pembelajaran_id = $v;
            $this->modifiedColumns[] = PembelajaranPeer::PEMBELAJARAN_ID;
        }


        return $this;
    } // setPembelajaranId()

    /**
     * Set the value of [rombongan_belajar_id] column.
     * 
     * @param string $v new value
     * @return Pembelajaran The current object (for fluent API support)
     */
    public function setRombonganBelajarId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->rombongan_belajar_id !== $v) {
            $this->rombongan_belajar_id = $v;
            $this->modifiedColumns[] = PembelajaranPeer::ROMBONGAN_BELAJAR_ID;
        }

        if ($this->aRombonganBelajarRelatedByRombonganBelajarId !== null && $this->aRombonganBelajarRelatedByRombonganBelajarId->getRombonganBelajarId() !== $v) {
            $this->aRombonganBelajarRelatedByRombonganBelajarId = null;
        }

        if ($this->aRombonganBelajarRelatedByRombonganBelajarId !== null && $this->aRombonganBelajarRelatedByRombonganBelajarId->getRombonganBelajarId() !== $v) {
            $this->aRombonganBelajarRelatedByRombonganBelajarId = null;
        }


        return $this;
    } // setRombonganBelajarId()

    /**
     * Set the value of [semester_id] column.
     * 
     * @param string $v new value
     * @return Pembelajaran The current object (for fluent API support)
     */
    public function setSemesterId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->semester_id !== $v) {
            $this->semester_id = $v;
            $this->modifiedColumns[] = PembelajaranPeer::SEMESTER_ID;
        }

        if ($this->aSemesterRelatedBySemesterId !== null && $this->aSemesterRelatedBySemesterId->getSemesterId() !== $v) {
            $this->aSemesterRelatedBySemesterId = null;
        }

        if ($this->aSemesterRelatedBySemesterId !== null && $this->aSemesterRelatedBySemesterId->getSemesterId() !== $v) {
            $this->aSemesterRelatedBySemesterId = null;
        }


        return $this;
    } // setSemesterId()

    /**
     * Set the value of [mata_pelajaran_id] column.
     * 
     * @param int $v new value
     * @return Pembelajaran The current object (for fluent API support)
     */
    public function setMataPelajaranId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->mata_pelajaran_id !== $v) {
            $this->mata_pelajaran_id = $v;
            $this->modifiedColumns[] = PembelajaranPeer::MATA_PELAJARAN_ID;
        }

        if ($this->aMataPelajaranRelatedByMataPelajaranId !== null && $this->aMataPelajaranRelatedByMataPelajaranId->getMataPelajaranId() !== $v) {
            $this->aMataPelajaranRelatedByMataPelajaranId = null;
        }

        if ($this->aMataPelajaranRelatedByMataPelajaranId !== null && $this->aMataPelajaranRelatedByMataPelajaranId->getMataPelajaranId() !== $v) {
            $this->aMataPelajaranRelatedByMataPelajaranId = null;
        }


        return $this;
    } // setMataPelajaranId()

    /**
     * Set the value of [ptk_terdaftar_id] column.
     * 
     * @param string $v new value
     * @return Pembelajaran The current object (for fluent API support)
     */
    public function setPtkTerdaftarId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->ptk_terdaftar_id !== $v) {
            $this->ptk_terdaftar_id = $v;
            $this->modifiedColumns[] = PembelajaranPeer::PTK_TERDAFTAR_ID;
        }

        if ($this->aPtkTerdaftarRelatedByPtkTerdaftarId !== null && $this->aPtkTerdaftarRelatedByPtkTerdaftarId->getPtkTerdaftarId() !== $v) {
            $this->aPtkTerdaftarRelatedByPtkTerdaftarId = null;
        }

        if ($this->aPtkTerdaftarRelatedByPtkTerdaftarId !== null && $this->aPtkTerdaftarRelatedByPtkTerdaftarId->getPtkTerdaftarId() !== $v) {
            $this->aPtkTerdaftarRelatedByPtkTerdaftarId = null;
        }


        return $this;
    } // setPtkTerdaftarId()

    /**
     * Set the value of [sk_mengajar] column.
     * 
     * @param string $v new value
     * @return Pembelajaran The current object (for fluent API support)
     */
    public function setSkMengajar($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->sk_mengajar !== $v) {
            $this->sk_mengajar = $v;
            $this->modifiedColumns[] = PembelajaranPeer::SK_MENGAJAR;
        }


        return $this;
    } // setSkMengajar()

    /**
     * Set the value of [tanggal_sk_mengajar] column.
     * 
     * @param string $v new value
     * @return Pembelajaran The current object (for fluent API support)
     */
    public function setTanggalSkMengajar($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tanggal_sk_mengajar !== $v) {
            $this->tanggal_sk_mengajar = $v;
            $this->modifiedColumns[] = PembelajaranPeer::TANGGAL_SK_MENGAJAR;
        }


        return $this;
    } // setTanggalSkMengajar()

    /**
     * Set the value of [jam_mengajar_per_minggu] column.
     * 
     * @param string $v new value
     * @return Pembelajaran The current object (for fluent API support)
     */
    public function setJamMengajarPerMinggu($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->jam_mengajar_per_minggu !== $v) {
            $this->jam_mengajar_per_minggu = $v;
            $this->modifiedColumns[] = PembelajaranPeer::JAM_MENGAJAR_PER_MINGGU;
        }


        return $this;
    } // setJamMengajarPerMinggu()

    /**
     * Set the value of [status_di_kurikulum] column.
     * 
     * @param string $v new value
     * @return Pembelajaran The current object (for fluent API support)
     */
    public function setStatusDiKurikulum($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->status_di_kurikulum !== $v) {
            $this->status_di_kurikulum = $v;
            $this->modifiedColumns[] = PembelajaranPeer::STATUS_DI_KURIKULUM;
        }


        return $this;
    } // setStatusDiKurikulum()

    /**
     * Set the value of [nama_mata_pelajaran] column.
     * 
     * @param string $v new value
     * @return Pembelajaran The current object (for fluent API support)
     */
    public function setNamaMataPelajaran($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nama_mata_pelajaran !== $v) {
            $this->nama_mata_pelajaran = $v;
            $this->modifiedColumns[] = PembelajaranPeer::NAMA_MATA_PELAJARAN;
        }


        return $this;
    } // setNamaMataPelajaran()

    /**
     * Sets the value of [last_update] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Pembelajaran The current object (for fluent API support)
     */
    public function setLastUpdate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_update !== null || $dt !== null) {
            $currentDateAsString = ($this->last_update !== null && $tmpDt = new DateTime($this->last_update)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_update = $newDateAsString;
                $this->modifiedColumns[] = PembelajaranPeer::LAST_UPDATE;
            }
        } // if either are not null


        return $this;
    } // setLastUpdate()

    /**
     * Set the value of [soft_delete] column.
     * 
     * @param string $v new value
     * @return Pembelajaran The current object (for fluent API support)
     */
    public function setSoftDelete($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->soft_delete !== $v) {
            $this->soft_delete = $v;
            $this->modifiedColumns[] = PembelajaranPeer::SOFT_DELETE;
        }


        return $this;
    } // setSoftDelete()

    /**
     * Sets the value of [last_sync] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Pembelajaran The current object (for fluent API support)
     */
    public function setLastSync($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_sync !== null || $dt !== null) {
            $currentDateAsString = ($this->last_sync !== null && $tmpDt = new DateTime($this->last_sync)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_sync = $newDateAsString;
                $this->modifiedColumns[] = PembelajaranPeer::LAST_SYNC;
            }
        } // if either are not null


        return $this;
    } // setLastSync()

    /**
     * Set the value of [updater_id] column.
     * 
     * @param string $v new value
     * @return Pembelajaran The current object (for fluent API support)
     */
    public function setUpdaterId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->updater_id !== $v) {
            $this->updater_id = $v;
            $this->modifiedColumns[] = PembelajaranPeer::UPDATER_ID;
        }


        return $this;
    } // setUpdaterId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->pembelajaran_id = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
            $this->rombongan_belajar_id = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->semester_id = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->mata_pelajaran_id = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
            $this->ptk_terdaftar_id = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->sk_mengajar = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->tanggal_sk_mengajar = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->jam_mengajar_per_minggu = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->status_di_kurikulum = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->nama_mata_pelajaran = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->last_update = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->soft_delete = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->last_sync = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->updater_id = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 14; // 14 = PembelajaranPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Pembelajaran object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aRombonganBelajarRelatedByRombonganBelajarId !== null && $this->rombongan_belajar_id !== $this->aRombonganBelajarRelatedByRombonganBelajarId->getRombonganBelajarId()) {
            $this->aRombonganBelajarRelatedByRombonganBelajarId = null;
        }
        if ($this->aRombonganBelajarRelatedByRombonganBelajarId !== null && $this->rombongan_belajar_id !== $this->aRombonganBelajarRelatedByRombonganBelajarId->getRombonganBelajarId()) {
            $this->aRombonganBelajarRelatedByRombonganBelajarId = null;
        }
        if ($this->aSemesterRelatedBySemesterId !== null && $this->semester_id !== $this->aSemesterRelatedBySemesterId->getSemesterId()) {
            $this->aSemesterRelatedBySemesterId = null;
        }
        if ($this->aSemesterRelatedBySemesterId !== null && $this->semester_id !== $this->aSemesterRelatedBySemesterId->getSemesterId()) {
            $this->aSemesterRelatedBySemesterId = null;
        }
        if ($this->aMataPelajaranRelatedByMataPelajaranId !== null && $this->mata_pelajaran_id !== $this->aMataPelajaranRelatedByMataPelajaranId->getMataPelajaranId()) {
            $this->aMataPelajaranRelatedByMataPelajaranId = null;
        }
        if ($this->aMataPelajaranRelatedByMataPelajaranId !== null && $this->mata_pelajaran_id !== $this->aMataPelajaranRelatedByMataPelajaranId->getMataPelajaranId()) {
            $this->aMataPelajaranRelatedByMataPelajaranId = null;
        }
        if ($this->aPtkTerdaftarRelatedByPtkTerdaftarId !== null && $this->ptk_terdaftar_id !== $this->aPtkTerdaftarRelatedByPtkTerdaftarId->getPtkTerdaftarId()) {
            $this->aPtkTerdaftarRelatedByPtkTerdaftarId = null;
        }
        if ($this->aPtkTerdaftarRelatedByPtkTerdaftarId !== null && $this->ptk_terdaftar_id !== $this->aPtkTerdaftarRelatedByPtkTerdaftarId->getPtkTerdaftarId()) {
            $this->aPtkTerdaftarRelatedByPtkTerdaftarId = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PembelajaranPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = PembelajaranPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aPtkTerdaftarRelatedByPtkTerdaftarId = null;
            $this->aPtkTerdaftarRelatedByPtkTerdaftarId = null;
            $this->aRombonganBelajarRelatedByRombonganBelajarId = null;
            $this->aRombonganBelajarRelatedByRombonganBelajarId = null;
            $this->aMataPelajaranRelatedByMataPelajaranId = null;
            $this->aMataPelajaranRelatedByMataPelajaranId = null;
            $this->aSemesterRelatedBySemesterId = null;
            $this->aSemesterRelatedBySemesterId = null;
            $this->collVldPembelajaransRelatedByPembelajaranId = null;

            $this->collVldPembelajaransRelatedByPembelajaranId = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PembelajaranPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = PembelajaranQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PembelajaranPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                PembelajaranPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPtkTerdaftarRelatedByPtkTerdaftarId !== null) {
                if ($this->aPtkTerdaftarRelatedByPtkTerdaftarId->isModified() || $this->aPtkTerdaftarRelatedByPtkTerdaftarId->isNew()) {
                    $affectedRows += $this->aPtkTerdaftarRelatedByPtkTerdaftarId->save($con);
                }
                $this->setPtkTerdaftarRelatedByPtkTerdaftarId($this->aPtkTerdaftarRelatedByPtkTerdaftarId);
            }

            if ($this->aPtkTerdaftarRelatedByPtkTerdaftarId !== null) {
                if ($this->aPtkTerdaftarRelatedByPtkTerdaftarId->isModified() || $this->aPtkTerdaftarRelatedByPtkTerdaftarId->isNew()) {
                    $affectedRows += $this->aPtkTerdaftarRelatedByPtkTerdaftarId->save($con);
                }
                $this->setPtkTerdaftarRelatedByPtkTerdaftarId($this->aPtkTerdaftarRelatedByPtkTerdaftarId);
            }

            if ($this->aRombonganBelajarRelatedByRombonganBelajarId !== null) {
                if ($this->aRombonganBelajarRelatedByRombonganBelajarId->isModified() || $this->aRombonganBelajarRelatedByRombonganBelajarId->isNew()) {
                    $affectedRows += $this->aRombonganBelajarRelatedByRombonganBelajarId->save($con);
                }
                $this->setRombonganBelajarRelatedByRombonganBelajarId($this->aRombonganBelajarRelatedByRombonganBelajarId);
            }

            if ($this->aRombonganBelajarRelatedByRombonganBelajarId !== null) {
                if ($this->aRombonganBelajarRelatedByRombonganBelajarId->isModified() || $this->aRombonganBelajarRelatedByRombonganBelajarId->isNew()) {
                    $affectedRows += $this->aRombonganBelajarRelatedByRombonganBelajarId->save($con);
                }
                $this->setRombonganBelajarRelatedByRombonganBelajarId($this->aRombonganBelajarRelatedByRombonganBelajarId);
            }

            if ($this->aMataPelajaranRelatedByMataPelajaranId !== null) {
                if ($this->aMataPelajaranRelatedByMataPelajaranId->isModified() || $this->aMataPelajaranRelatedByMataPelajaranId->isNew()) {
                    $affectedRows += $this->aMataPelajaranRelatedByMataPelajaranId->save($con);
                }
                $this->setMataPelajaranRelatedByMataPelajaranId($this->aMataPelajaranRelatedByMataPelajaranId);
            }

            if ($this->aMataPelajaranRelatedByMataPelajaranId !== null) {
                if ($this->aMataPelajaranRelatedByMataPelajaranId->isModified() || $this->aMataPelajaranRelatedByMataPelajaranId->isNew()) {
                    $affectedRows += $this->aMataPelajaranRelatedByMataPelajaranId->save($con);
                }
                $this->setMataPelajaranRelatedByMataPelajaranId($this->aMataPelajaranRelatedByMataPelajaranId);
            }

            if ($this->aSemesterRelatedBySemesterId !== null) {
                if ($this->aSemesterRelatedBySemesterId->isModified() || $this->aSemesterRelatedBySemesterId->isNew()) {
                    $affectedRows += $this->aSemesterRelatedBySemesterId->save($con);
                }
                $this->setSemesterRelatedBySemesterId($this->aSemesterRelatedBySemesterId);
            }

            if ($this->aSemesterRelatedBySemesterId !== null) {
                if ($this->aSemesterRelatedBySemesterId->isModified() || $this->aSemesterRelatedBySemesterId->isNew()) {
                    $affectedRows += $this->aSemesterRelatedBySemesterId->save($con);
                }
                $this->setSemesterRelatedBySemesterId($this->aSemesterRelatedBySemesterId);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->vldPembelajaransRelatedByPembelajaranIdScheduledForDeletion !== null) {
                if (!$this->vldPembelajaransRelatedByPembelajaranIdScheduledForDeletion->isEmpty()) {
                    VldPembelajaranQuery::create()
                        ->filterByPrimaryKeys($this->vldPembelajaransRelatedByPembelajaranIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldPembelajaransRelatedByPembelajaranIdScheduledForDeletion = null;
                }
            }

            if ($this->collVldPembelajaransRelatedByPembelajaranId !== null) {
                foreach ($this->collVldPembelajaransRelatedByPembelajaranId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldPembelajaransRelatedByPembelajaranIdScheduledForDeletion !== null) {
                if (!$this->vldPembelajaransRelatedByPembelajaranIdScheduledForDeletion->isEmpty()) {
                    VldPembelajaranQuery::create()
                        ->filterByPrimaryKeys($this->vldPembelajaransRelatedByPembelajaranIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldPembelajaransRelatedByPembelajaranIdScheduledForDeletion = null;
                }
            }

            if ($this->collVldPembelajaransRelatedByPembelajaranId !== null) {
                foreach ($this->collVldPembelajaransRelatedByPembelajaranId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $criteria = $this->buildCriteria();
        $pk = BasePeer::doInsert($criteria, $con);
        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPtkTerdaftarRelatedByPtkTerdaftarId !== null) {
                if (!$this->aPtkTerdaftarRelatedByPtkTerdaftarId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPtkTerdaftarRelatedByPtkTerdaftarId->getValidationFailures());
                }
            }

            if ($this->aPtkTerdaftarRelatedByPtkTerdaftarId !== null) {
                if (!$this->aPtkTerdaftarRelatedByPtkTerdaftarId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPtkTerdaftarRelatedByPtkTerdaftarId->getValidationFailures());
                }
            }

            if ($this->aRombonganBelajarRelatedByRombonganBelajarId !== null) {
                if (!$this->aRombonganBelajarRelatedByRombonganBelajarId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aRombonganBelajarRelatedByRombonganBelajarId->getValidationFailures());
                }
            }

            if ($this->aRombonganBelajarRelatedByRombonganBelajarId !== null) {
                if (!$this->aRombonganBelajarRelatedByRombonganBelajarId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aRombonganBelajarRelatedByRombonganBelajarId->getValidationFailures());
                }
            }

            if ($this->aMataPelajaranRelatedByMataPelajaranId !== null) {
                if (!$this->aMataPelajaranRelatedByMataPelajaranId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aMataPelajaranRelatedByMataPelajaranId->getValidationFailures());
                }
            }

            if ($this->aMataPelajaranRelatedByMataPelajaranId !== null) {
                if (!$this->aMataPelajaranRelatedByMataPelajaranId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aMataPelajaranRelatedByMataPelajaranId->getValidationFailures());
                }
            }

            if ($this->aSemesterRelatedBySemesterId !== null) {
                if (!$this->aSemesterRelatedBySemesterId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSemesterRelatedBySemesterId->getValidationFailures());
                }
            }

            if ($this->aSemesterRelatedBySemesterId !== null) {
                if (!$this->aSemesterRelatedBySemesterId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSemesterRelatedBySemesterId->getValidationFailures());
                }
            }


            if (($retval = PembelajaranPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collVldPembelajaransRelatedByPembelajaranId !== null) {
                    foreach ($this->collVldPembelajaransRelatedByPembelajaranId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldPembelajaransRelatedByPembelajaranId !== null) {
                    foreach ($this->collVldPembelajaransRelatedByPembelajaranId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = PembelajaranPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getPembelajaranId();
                break;
            case 1:
                return $this->getRombonganBelajarId();
                break;
            case 2:
                return $this->getSemesterId();
                break;
            case 3:
                return $this->getMataPelajaranId();
                break;
            case 4:
                return $this->getPtkTerdaftarId();
                break;
            case 5:
                return $this->getSkMengajar();
                break;
            case 6:
                return $this->getTanggalSkMengajar();
                break;
            case 7:
                return $this->getJamMengajarPerMinggu();
                break;
            case 8:
                return $this->getStatusDiKurikulum();
                break;
            case 9:
                return $this->getNamaMataPelajaran();
                break;
            case 10:
                return $this->getLastUpdate();
                break;
            case 11:
                return $this->getSoftDelete();
                break;
            case 12:
                return $this->getLastSync();
                break;
            case 13:
                return $this->getUpdaterId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Pembelajaran'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Pembelajaran'][$this->getPrimaryKey()] = true;
        $keys = PembelajaranPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getPembelajaranId(),
            $keys[1] => $this->getRombonganBelajarId(),
            $keys[2] => $this->getSemesterId(),
            $keys[3] => $this->getMataPelajaranId(),
            $keys[4] => $this->getPtkTerdaftarId(),
            $keys[5] => $this->getSkMengajar(),
            $keys[6] => $this->getTanggalSkMengajar(),
            $keys[7] => $this->getJamMengajarPerMinggu(),
            $keys[8] => $this->getStatusDiKurikulum(),
            $keys[9] => $this->getNamaMataPelajaran(),
            $keys[10] => $this->getLastUpdate(),
            $keys[11] => $this->getSoftDelete(),
            $keys[12] => $this->getLastSync(),
            $keys[13] => $this->getUpdaterId(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aPtkTerdaftarRelatedByPtkTerdaftarId) {
                $result['PtkTerdaftarRelatedByPtkTerdaftarId'] = $this->aPtkTerdaftarRelatedByPtkTerdaftarId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPtkTerdaftarRelatedByPtkTerdaftarId) {
                $result['PtkTerdaftarRelatedByPtkTerdaftarId'] = $this->aPtkTerdaftarRelatedByPtkTerdaftarId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aRombonganBelajarRelatedByRombonganBelajarId) {
                $result['RombonganBelajarRelatedByRombonganBelajarId'] = $this->aRombonganBelajarRelatedByRombonganBelajarId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aRombonganBelajarRelatedByRombonganBelajarId) {
                $result['RombonganBelajarRelatedByRombonganBelajarId'] = $this->aRombonganBelajarRelatedByRombonganBelajarId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aMataPelajaranRelatedByMataPelajaranId) {
                $result['MataPelajaranRelatedByMataPelajaranId'] = $this->aMataPelajaranRelatedByMataPelajaranId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aMataPelajaranRelatedByMataPelajaranId) {
                $result['MataPelajaranRelatedByMataPelajaranId'] = $this->aMataPelajaranRelatedByMataPelajaranId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSemesterRelatedBySemesterId) {
                $result['SemesterRelatedBySemesterId'] = $this->aSemesterRelatedBySemesterId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSemesterRelatedBySemesterId) {
                $result['SemesterRelatedBySemesterId'] = $this->aSemesterRelatedBySemesterId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collVldPembelajaransRelatedByPembelajaranId) {
                $result['VldPembelajaransRelatedByPembelajaranId'] = $this->collVldPembelajaransRelatedByPembelajaranId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldPembelajaransRelatedByPembelajaranId) {
                $result['VldPembelajaransRelatedByPembelajaranId'] = $this->collVldPembelajaransRelatedByPembelajaranId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = PembelajaranPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setPembelajaranId($value);
                break;
            case 1:
                $this->setRombonganBelajarId($value);
                break;
            case 2:
                $this->setSemesterId($value);
                break;
            case 3:
                $this->setMataPelajaranId($value);
                break;
            case 4:
                $this->setPtkTerdaftarId($value);
                break;
            case 5:
                $this->setSkMengajar($value);
                break;
            case 6:
                $this->setTanggalSkMengajar($value);
                break;
            case 7:
                $this->setJamMengajarPerMinggu($value);
                break;
            case 8:
                $this->setStatusDiKurikulum($value);
                break;
            case 9:
                $this->setNamaMataPelajaran($value);
                break;
            case 10:
                $this->setLastUpdate($value);
                break;
            case 11:
                $this->setSoftDelete($value);
                break;
            case 12:
                $this->setLastSync($value);
                break;
            case 13:
                $this->setUpdaterId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = PembelajaranPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setPembelajaranId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setRombonganBelajarId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setSemesterId($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setMataPelajaranId($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setPtkTerdaftarId($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setSkMengajar($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setTanggalSkMengajar($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setJamMengajarPerMinggu($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setStatusDiKurikulum($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setNamaMataPelajaran($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setLastUpdate($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setSoftDelete($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setLastSync($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setUpdaterId($arr[$keys[13]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(PembelajaranPeer::DATABASE_NAME);

        if ($this->isColumnModified(PembelajaranPeer::PEMBELAJARAN_ID)) $criteria->add(PembelajaranPeer::PEMBELAJARAN_ID, $this->pembelajaran_id);
        if ($this->isColumnModified(PembelajaranPeer::ROMBONGAN_BELAJAR_ID)) $criteria->add(PembelajaranPeer::ROMBONGAN_BELAJAR_ID, $this->rombongan_belajar_id);
        if ($this->isColumnModified(PembelajaranPeer::SEMESTER_ID)) $criteria->add(PembelajaranPeer::SEMESTER_ID, $this->semester_id);
        if ($this->isColumnModified(PembelajaranPeer::MATA_PELAJARAN_ID)) $criteria->add(PembelajaranPeer::MATA_PELAJARAN_ID, $this->mata_pelajaran_id);
        if ($this->isColumnModified(PembelajaranPeer::PTK_TERDAFTAR_ID)) $criteria->add(PembelajaranPeer::PTK_TERDAFTAR_ID, $this->ptk_terdaftar_id);
        if ($this->isColumnModified(PembelajaranPeer::SK_MENGAJAR)) $criteria->add(PembelajaranPeer::SK_MENGAJAR, $this->sk_mengajar);
        if ($this->isColumnModified(PembelajaranPeer::TANGGAL_SK_MENGAJAR)) $criteria->add(PembelajaranPeer::TANGGAL_SK_MENGAJAR, $this->tanggal_sk_mengajar);
        if ($this->isColumnModified(PembelajaranPeer::JAM_MENGAJAR_PER_MINGGU)) $criteria->add(PembelajaranPeer::JAM_MENGAJAR_PER_MINGGU, $this->jam_mengajar_per_minggu);
        if ($this->isColumnModified(PembelajaranPeer::STATUS_DI_KURIKULUM)) $criteria->add(PembelajaranPeer::STATUS_DI_KURIKULUM, $this->status_di_kurikulum);
        if ($this->isColumnModified(PembelajaranPeer::NAMA_MATA_PELAJARAN)) $criteria->add(PembelajaranPeer::NAMA_MATA_PELAJARAN, $this->nama_mata_pelajaran);
        if ($this->isColumnModified(PembelajaranPeer::LAST_UPDATE)) $criteria->add(PembelajaranPeer::LAST_UPDATE, $this->last_update);
        if ($this->isColumnModified(PembelajaranPeer::SOFT_DELETE)) $criteria->add(PembelajaranPeer::SOFT_DELETE, $this->soft_delete);
        if ($this->isColumnModified(PembelajaranPeer::LAST_SYNC)) $criteria->add(PembelajaranPeer::LAST_SYNC, $this->last_sync);
        if ($this->isColumnModified(PembelajaranPeer::UPDATER_ID)) $criteria->add(PembelajaranPeer::UPDATER_ID, $this->updater_id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(PembelajaranPeer::DATABASE_NAME);
        $criteria->add(PembelajaranPeer::PEMBELAJARAN_ID, $this->pembelajaran_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getPembelajaranId();
    }

    /**
     * Generic method to set the primary key (pembelajaran_id column).
     *
     * @param  string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setPembelajaranId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getPembelajaranId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Pembelajaran (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setRombonganBelajarId($this->getRombonganBelajarId());
        $copyObj->setSemesterId($this->getSemesterId());
        $copyObj->setMataPelajaranId($this->getMataPelajaranId());
        $copyObj->setPtkTerdaftarId($this->getPtkTerdaftarId());
        $copyObj->setSkMengajar($this->getSkMengajar());
        $copyObj->setTanggalSkMengajar($this->getTanggalSkMengajar());
        $copyObj->setJamMengajarPerMinggu($this->getJamMengajarPerMinggu());
        $copyObj->setStatusDiKurikulum($this->getStatusDiKurikulum());
        $copyObj->setNamaMataPelajaran($this->getNamaMataPelajaran());
        $copyObj->setLastUpdate($this->getLastUpdate());
        $copyObj->setSoftDelete($this->getSoftDelete());
        $copyObj->setLastSync($this->getLastSync());
        $copyObj->setUpdaterId($this->getUpdaterId());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getVldPembelajaransRelatedByPembelajaranId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldPembelajaranRelatedByPembelajaranId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldPembelajaransRelatedByPembelajaranId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldPembelajaranRelatedByPembelajaranId($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setPembelajaranId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Pembelajaran Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return PembelajaranPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new PembelajaranPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a PtkTerdaftar object.
     *
     * @param             PtkTerdaftar $v
     * @return Pembelajaran The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPtkTerdaftarRelatedByPtkTerdaftarId(PtkTerdaftar $v = null)
    {
        if ($v === null) {
            $this->setPtkTerdaftarId(NULL);
        } else {
            $this->setPtkTerdaftarId($v->getPtkTerdaftarId());
        }

        $this->aPtkTerdaftarRelatedByPtkTerdaftarId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the PtkTerdaftar object, it will not be re-added.
        if ($v !== null) {
            $v->addPembelajaranRelatedByPtkTerdaftarId($this);
        }


        return $this;
    }


    /**
     * Get the associated PtkTerdaftar object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return PtkTerdaftar The associated PtkTerdaftar object.
     * @throws PropelException
     */
    public function getPtkTerdaftarRelatedByPtkTerdaftarId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPtkTerdaftarRelatedByPtkTerdaftarId === null && (($this->ptk_terdaftar_id !== "" && $this->ptk_terdaftar_id !== null)) && $doQuery) {
            $this->aPtkTerdaftarRelatedByPtkTerdaftarId = PtkTerdaftarQuery::create()->findPk($this->ptk_terdaftar_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPtkTerdaftarRelatedByPtkTerdaftarId->addPembelajaransRelatedByPtkTerdaftarId($this);
             */
        }

        return $this->aPtkTerdaftarRelatedByPtkTerdaftarId;
    }

    /**
     * Declares an association between this object and a PtkTerdaftar object.
     *
     * @param             PtkTerdaftar $v
     * @return Pembelajaran The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPtkTerdaftarRelatedByPtkTerdaftarId(PtkTerdaftar $v = null)
    {
        if ($v === null) {
            $this->setPtkTerdaftarId(NULL);
        } else {
            $this->setPtkTerdaftarId($v->getPtkTerdaftarId());
        }

        $this->aPtkTerdaftarRelatedByPtkTerdaftarId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the PtkTerdaftar object, it will not be re-added.
        if ($v !== null) {
            $v->addPembelajaranRelatedByPtkTerdaftarId($this);
        }


        return $this;
    }


    /**
     * Get the associated PtkTerdaftar object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return PtkTerdaftar The associated PtkTerdaftar object.
     * @throws PropelException
     */
    public function getPtkTerdaftarRelatedByPtkTerdaftarId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPtkTerdaftarRelatedByPtkTerdaftarId === null && (($this->ptk_terdaftar_id !== "" && $this->ptk_terdaftar_id !== null)) && $doQuery) {
            $this->aPtkTerdaftarRelatedByPtkTerdaftarId = PtkTerdaftarQuery::create()->findPk($this->ptk_terdaftar_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPtkTerdaftarRelatedByPtkTerdaftarId->addPembelajaransRelatedByPtkTerdaftarId($this);
             */
        }

        return $this->aPtkTerdaftarRelatedByPtkTerdaftarId;
    }

    /**
     * Declares an association between this object and a RombonganBelajar object.
     *
     * @param             RombonganBelajar $v
     * @return Pembelajaran The current object (for fluent API support)
     * @throws PropelException
     */
    public function setRombonganBelajarRelatedByRombonganBelajarId(RombonganBelajar $v = null)
    {
        if ($v === null) {
            $this->setRombonganBelajarId(NULL);
        } else {
            $this->setRombonganBelajarId($v->getRombonganBelajarId());
        }

        $this->aRombonganBelajarRelatedByRombonganBelajarId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the RombonganBelajar object, it will not be re-added.
        if ($v !== null) {
            $v->addPembelajaranRelatedByRombonganBelajarId($this);
        }


        return $this;
    }


    /**
     * Get the associated RombonganBelajar object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return RombonganBelajar The associated RombonganBelajar object.
     * @throws PropelException
     */
    public function getRombonganBelajarRelatedByRombonganBelajarId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aRombonganBelajarRelatedByRombonganBelajarId === null && (($this->rombongan_belajar_id !== "" && $this->rombongan_belajar_id !== null)) && $doQuery) {
            $this->aRombonganBelajarRelatedByRombonganBelajarId = RombonganBelajarQuery::create()->findPk($this->rombongan_belajar_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aRombonganBelajarRelatedByRombonganBelajarId->addPembelajaransRelatedByRombonganBelajarId($this);
             */
        }

        return $this->aRombonganBelajarRelatedByRombonganBelajarId;
    }

    /**
     * Declares an association between this object and a RombonganBelajar object.
     *
     * @param             RombonganBelajar $v
     * @return Pembelajaran The current object (for fluent API support)
     * @throws PropelException
     */
    public function setRombonganBelajarRelatedByRombonganBelajarId(RombonganBelajar $v = null)
    {
        if ($v === null) {
            $this->setRombonganBelajarId(NULL);
        } else {
            $this->setRombonganBelajarId($v->getRombonganBelajarId());
        }

        $this->aRombonganBelajarRelatedByRombonganBelajarId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the RombonganBelajar object, it will not be re-added.
        if ($v !== null) {
            $v->addPembelajaranRelatedByRombonganBelajarId($this);
        }


        return $this;
    }


    /**
     * Get the associated RombonganBelajar object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return RombonganBelajar The associated RombonganBelajar object.
     * @throws PropelException
     */
    public function getRombonganBelajarRelatedByRombonganBelajarId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aRombonganBelajarRelatedByRombonganBelajarId === null && (($this->rombongan_belajar_id !== "" && $this->rombongan_belajar_id !== null)) && $doQuery) {
            $this->aRombonganBelajarRelatedByRombonganBelajarId = RombonganBelajarQuery::create()->findPk($this->rombongan_belajar_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aRombonganBelajarRelatedByRombonganBelajarId->addPembelajaransRelatedByRombonganBelajarId($this);
             */
        }

        return $this->aRombonganBelajarRelatedByRombonganBelajarId;
    }

    /**
     * Declares an association between this object and a MataPelajaran object.
     *
     * @param             MataPelajaran $v
     * @return Pembelajaran The current object (for fluent API support)
     * @throws PropelException
     */
    public function setMataPelajaranRelatedByMataPelajaranId(MataPelajaran $v = null)
    {
        if ($v === null) {
            $this->setMataPelajaranId(NULL);
        } else {
            $this->setMataPelajaranId($v->getMataPelajaranId());
        }

        $this->aMataPelajaranRelatedByMataPelajaranId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the MataPelajaran object, it will not be re-added.
        if ($v !== null) {
            $v->addPembelajaranRelatedByMataPelajaranId($this);
        }


        return $this;
    }


    /**
     * Get the associated MataPelajaran object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return MataPelajaran The associated MataPelajaran object.
     * @throws PropelException
     */
    public function getMataPelajaranRelatedByMataPelajaranId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aMataPelajaranRelatedByMataPelajaranId === null && ($this->mata_pelajaran_id !== null) && $doQuery) {
            $this->aMataPelajaranRelatedByMataPelajaranId = MataPelajaranQuery::create()->findPk($this->mata_pelajaran_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aMataPelajaranRelatedByMataPelajaranId->addPembelajaransRelatedByMataPelajaranId($this);
             */
        }

        return $this->aMataPelajaranRelatedByMataPelajaranId;
    }

    /**
     * Declares an association between this object and a MataPelajaran object.
     *
     * @param             MataPelajaran $v
     * @return Pembelajaran The current object (for fluent API support)
     * @throws PropelException
     */
    public function setMataPelajaranRelatedByMataPelajaranId(MataPelajaran $v = null)
    {
        if ($v === null) {
            $this->setMataPelajaranId(NULL);
        } else {
            $this->setMataPelajaranId($v->getMataPelajaranId());
        }

        $this->aMataPelajaranRelatedByMataPelajaranId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the MataPelajaran object, it will not be re-added.
        if ($v !== null) {
            $v->addPembelajaranRelatedByMataPelajaranId($this);
        }


        return $this;
    }


    /**
     * Get the associated MataPelajaran object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return MataPelajaran The associated MataPelajaran object.
     * @throws PropelException
     */
    public function getMataPelajaranRelatedByMataPelajaranId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aMataPelajaranRelatedByMataPelajaranId === null && ($this->mata_pelajaran_id !== null) && $doQuery) {
            $this->aMataPelajaranRelatedByMataPelajaranId = MataPelajaranQuery::create()->findPk($this->mata_pelajaran_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aMataPelajaranRelatedByMataPelajaranId->addPembelajaransRelatedByMataPelajaranId($this);
             */
        }

        return $this->aMataPelajaranRelatedByMataPelajaranId;
    }

    /**
     * Declares an association between this object and a Semester object.
     *
     * @param             Semester $v
     * @return Pembelajaran The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSemesterRelatedBySemesterId(Semester $v = null)
    {
        if ($v === null) {
            $this->setSemesterId(NULL);
        } else {
            $this->setSemesterId($v->getSemesterId());
        }

        $this->aSemesterRelatedBySemesterId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Semester object, it will not be re-added.
        if ($v !== null) {
            $v->addPembelajaranRelatedBySemesterId($this);
        }


        return $this;
    }


    /**
     * Get the associated Semester object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Semester The associated Semester object.
     * @throws PropelException
     */
    public function getSemesterRelatedBySemesterId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSemesterRelatedBySemesterId === null && (($this->semester_id !== "" && $this->semester_id !== null)) && $doQuery) {
            $this->aSemesterRelatedBySemesterId = SemesterQuery::create()->findPk($this->semester_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSemesterRelatedBySemesterId->addPembelajaransRelatedBySemesterId($this);
             */
        }

        return $this->aSemesterRelatedBySemesterId;
    }

    /**
     * Declares an association between this object and a Semester object.
     *
     * @param             Semester $v
     * @return Pembelajaran The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSemesterRelatedBySemesterId(Semester $v = null)
    {
        if ($v === null) {
            $this->setSemesterId(NULL);
        } else {
            $this->setSemesterId($v->getSemesterId());
        }

        $this->aSemesterRelatedBySemesterId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Semester object, it will not be re-added.
        if ($v !== null) {
            $v->addPembelajaranRelatedBySemesterId($this);
        }


        return $this;
    }


    /**
     * Get the associated Semester object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Semester The associated Semester object.
     * @throws PropelException
     */
    public function getSemesterRelatedBySemesterId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSemesterRelatedBySemesterId === null && (($this->semester_id !== "" && $this->semester_id !== null)) && $doQuery) {
            $this->aSemesterRelatedBySemesterId = SemesterQuery::create()->findPk($this->semester_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSemesterRelatedBySemesterId->addPembelajaransRelatedBySemesterId($this);
             */
        }

        return $this->aSemesterRelatedBySemesterId;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('VldPembelajaranRelatedByPembelajaranId' == $relationName) {
            $this->initVldPembelajaransRelatedByPembelajaranId();
        }
        if ('VldPembelajaranRelatedByPembelajaranId' == $relationName) {
            $this->initVldPembelajaransRelatedByPembelajaranId();
        }
    }

    /**
     * Clears out the collVldPembelajaransRelatedByPembelajaranId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Pembelajaran The current object (for fluent API support)
     * @see        addVldPembelajaransRelatedByPembelajaranId()
     */
    public function clearVldPembelajaransRelatedByPembelajaranId()
    {
        $this->collVldPembelajaransRelatedByPembelajaranId = null; // important to set this to null since that means it is uninitialized
        $this->collVldPembelajaransRelatedByPembelajaranIdPartial = null;

        return $this;
    }

    /**
     * reset is the collVldPembelajaransRelatedByPembelajaranId collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldPembelajaransRelatedByPembelajaranId($v = true)
    {
        $this->collVldPembelajaransRelatedByPembelajaranIdPartial = $v;
    }

    /**
     * Initializes the collVldPembelajaransRelatedByPembelajaranId collection.
     *
     * By default this just sets the collVldPembelajaransRelatedByPembelajaranId collection to an empty array (like clearcollVldPembelajaransRelatedByPembelajaranId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldPembelajaransRelatedByPembelajaranId($overrideExisting = true)
    {
        if (null !== $this->collVldPembelajaransRelatedByPembelajaranId && !$overrideExisting) {
            return;
        }
        $this->collVldPembelajaransRelatedByPembelajaranId = new PropelObjectCollection();
        $this->collVldPembelajaransRelatedByPembelajaranId->setModel('VldPembelajaran');
    }

    /**
     * Gets an array of VldPembelajaran objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Pembelajaran is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldPembelajaran[] List of VldPembelajaran objects
     * @throws PropelException
     */
    public function getVldPembelajaransRelatedByPembelajaranId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldPembelajaransRelatedByPembelajaranIdPartial && !$this->isNew();
        if (null === $this->collVldPembelajaransRelatedByPembelajaranId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldPembelajaransRelatedByPembelajaranId) {
                // return empty collection
                $this->initVldPembelajaransRelatedByPembelajaranId();
            } else {
                $collVldPembelajaransRelatedByPembelajaranId = VldPembelajaranQuery::create(null, $criteria)
                    ->filterByPembelajaranRelatedByPembelajaranId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldPembelajaransRelatedByPembelajaranIdPartial && count($collVldPembelajaransRelatedByPembelajaranId)) {
                      $this->initVldPembelajaransRelatedByPembelajaranId(false);

                      foreach($collVldPembelajaransRelatedByPembelajaranId as $obj) {
                        if (false == $this->collVldPembelajaransRelatedByPembelajaranId->contains($obj)) {
                          $this->collVldPembelajaransRelatedByPembelajaranId->append($obj);
                        }
                      }

                      $this->collVldPembelajaransRelatedByPembelajaranIdPartial = true;
                    }

                    $collVldPembelajaransRelatedByPembelajaranId->getInternalIterator()->rewind();
                    return $collVldPembelajaransRelatedByPembelajaranId;
                }

                if($partial && $this->collVldPembelajaransRelatedByPembelajaranId) {
                    foreach($this->collVldPembelajaransRelatedByPembelajaranId as $obj) {
                        if($obj->isNew()) {
                            $collVldPembelajaransRelatedByPembelajaranId[] = $obj;
                        }
                    }
                }

                $this->collVldPembelajaransRelatedByPembelajaranId = $collVldPembelajaransRelatedByPembelajaranId;
                $this->collVldPembelajaransRelatedByPembelajaranIdPartial = false;
            }
        }

        return $this->collVldPembelajaransRelatedByPembelajaranId;
    }

    /**
     * Sets a collection of VldPembelajaranRelatedByPembelajaranId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldPembelajaransRelatedByPembelajaranId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Pembelajaran The current object (for fluent API support)
     */
    public function setVldPembelajaransRelatedByPembelajaranId(PropelCollection $vldPembelajaransRelatedByPembelajaranId, PropelPDO $con = null)
    {
        $vldPembelajaransRelatedByPembelajaranIdToDelete = $this->getVldPembelajaransRelatedByPembelajaranId(new Criteria(), $con)->diff($vldPembelajaransRelatedByPembelajaranId);

        $this->vldPembelajaransRelatedByPembelajaranIdScheduledForDeletion = unserialize(serialize($vldPembelajaransRelatedByPembelajaranIdToDelete));

        foreach ($vldPembelajaransRelatedByPembelajaranIdToDelete as $vldPembelajaranRelatedByPembelajaranIdRemoved) {
            $vldPembelajaranRelatedByPembelajaranIdRemoved->setPembelajaranRelatedByPembelajaranId(null);
        }

        $this->collVldPembelajaransRelatedByPembelajaranId = null;
        foreach ($vldPembelajaransRelatedByPembelajaranId as $vldPembelajaranRelatedByPembelajaranId) {
            $this->addVldPembelajaranRelatedByPembelajaranId($vldPembelajaranRelatedByPembelajaranId);
        }

        $this->collVldPembelajaransRelatedByPembelajaranId = $vldPembelajaransRelatedByPembelajaranId;
        $this->collVldPembelajaransRelatedByPembelajaranIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldPembelajaran objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldPembelajaran objects.
     * @throws PropelException
     */
    public function countVldPembelajaransRelatedByPembelajaranId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldPembelajaransRelatedByPembelajaranIdPartial && !$this->isNew();
        if (null === $this->collVldPembelajaransRelatedByPembelajaranId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldPembelajaransRelatedByPembelajaranId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldPembelajaransRelatedByPembelajaranId());
            }
            $query = VldPembelajaranQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPembelajaranRelatedByPembelajaranId($this)
                ->count($con);
        }

        return count($this->collVldPembelajaransRelatedByPembelajaranId);
    }

    /**
     * Method called to associate a VldPembelajaran object to this object
     * through the VldPembelajaran foreign key attribute.
     *
     * @param    VldPembelajaran $l VldPembelajaran
     * @return Pembelajaran The current object (for fluent API support)
     */
    public function addVldPembelajaranRelatedByPembelajaranId(VldPembelajaran $l)
    {
        if ($this->collVldPembelajaransRelatedByPembelajaranId === null) {
            $this->initVldPembelajaransRelatedByPembelajaranId();
            $this->collVldPembelajaransRelatedByPembelajaranIdPartial = true;
        }
        if (!in_array($l, $this->collVldPembelajaransRelatedByPembelajaranId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldPembelajaranRelatedByPembelajaranId($l);
        }

        return $this;
    }

    /**
     * @param	VldPembelajaranRelatedByPembelajaranId $vldPembelajaranRelatedByPembelajaranId The vldPembelajaranRelatedByPembelajaranId object to add.
     */
    protected function doAddVldPembelajaranRelatedByPembelajaranId($vldPembelajaranRelatedByPembelajaranId)
    {
        $this->collVldPembelajaransRelatedByPembelajaranId[]= $vldPembelajaranRelatedByPembelajaranId;
        $vldPembelajaranRelatedByPembelajaranId->setPembelajaranRelatedByPembelajaranId($this);
    }

    /**
     * @param	VldPembelajaranRelatedByPembelajaranId $vldPembelajaranRelatedByPembelajaranId The vldPembelajaranRelatedByPembelajaranId object to remove.
     * @return Pembelajaran The current object (for fluent API support)
     */
    public function removeVldPembelajaranRelatedByPembelajaranId($vldPembelajaranRelatedByPembelajaranId)
    {
        if ($this->getVldPembelajaransRelatedByPembelajaranId()->contains($vldPembelajaranRelatedByPembelajaranId)) {
            $this->collVldPembelajaransRelatedByPembelajaranId->remove($this->collVldPembelajaransRelatedByPembelajaranId->search($vldPembelajaranRelatedByPembelajaranId));
            if (null === $this->vldPembelajaransRelatedByPembelajaranIdScheduledForDeletion) {
                $this->vldPembelajaransRelatedByPembelajaranIdScheduledForDeletion = clone $this->collVldPembelajaransRelatedByPembelajaranId;
                $this->vldPembelajaransRelatedByPembelajaranIdScheduledForDeletion->clear();
            }
            $this->vldPembelajaransRelatedByPembelajaranIdScheduledForDeletion[]= clone $vldPembelajaranRelatedByPembelajaranId;
            $vldPembelajaranRelatedByPembelajaranId->setPembelajaranRelatedByPembelajaranId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pembelajaran is new, it will return
     * an empty collection; or if this Pembelajaran has previously
     * been saved, it will retrieve related VldPembelajaransRelatedByPembelajaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pembelajaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPembelajaran[] List of VldPembelajaran objects
     */
    public function getVldPembelajaransRelatedByPembelajaranIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPembelajaranQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldPembelajaransRelatedByPembelajaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pembelajaran is new, it will return
     * an empty collection; or if this Pembelajaran has previously
     * been saved, it will retrieve related VldPembelajaransRelatedByPembelajaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pembelajaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPembelajaran[] List of VldPembelajaran objects
     */
    public function getVldPembelajaransRelatedByPembelajaranIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPembelajaranQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldPembelajaransRelatedByPembelajaranId($query, $con);
    }

    /**
     * Clears out the collVldPembelajaransRelatedByPembelajaranId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Pembelajaran The current object (for fluent API support)
     * @see        addVldPembelajaransRelatedByPembelajaranId()
     */
    public function clearVldPembelajaransRelatedByPembelajaranId()
    {
        $this->collVldPembelajaransRelatedByPembelajaranId = null; // important to set this to null since that means it is uninitialized
        $this->collVldPembelajaransRelatedByPembelajaranIdPartial = null;

        return $this;
    }

    /**
     * reset is the collVldPembelajaransRelatedByPembelajaranId collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldPembelajaransRelatedByPembelajaranId($v = true)
    {
        $this->collVldPembelajaransRelatedByPembelajaranIdPartial = $v;
    }

    /**
     * Initializes the collVldPembelajaransRelatedByPembelajaranId collection.
     *
     * By default this just sets the collVldPembelajaransRelatedByPembelajaranId collection to an empty array (like clearcollVldPembelajaransRelatedByPembelajaranId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldPembelajaransRelatedByPembelajaranId($overrideExisting = true)
    {
        if (null !== $this->collVldPembelajaransRelatedByPembelajaranId && !$overrideExisting) {
            return;
        }
        $this->collVldPembelajaransRelatedByPembelajaranId = new PropelObjectCollection();
        $this->collVldPembelajaransRelatedByPembelajaranId->setModel('VldPembelajaran');
    }

    /**
     * Gets an array of VldPembelajaran objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Pembelajaran is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldPembelajaran[] List of VldPembelajaran objects
     * @throws PropelException
     */
    public function getVldPembelajaransRelatedByPembelajaranId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldPembelajaransRelatedByPembelajaranIdPartial && !$this->isNew();
        if (null === $this->collVldPembelajaransRelatedByPembelajaranId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldPembelajaransRelatedByPembelajaranId) {
                // return empty collection
                $this->initVldPembelajaransRelatedByPembelajaranId();
            } else {
                $collVldPembelajaransRelatedByPembelajaranId = VldPembelajaranQuery::create(null, $criteria)
                    ->filterByPembelajaranRelatedByPembelajaranId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldPembelajaransRelatedByPembelajaranIdPartial && count($collVldPembelajaransRelatedByPembelajaranId)) {
                      $this->initVldPembelajaransRelatedByPembelajaranId(false);

                      foreach($collVldPembelajaransRelatedByPembelajaranId as $obj) {
                        if (false == $this->collVldPembelajaransRelatedByPembelajaranId->contains($obj)) {
                          $this->collVldPembelajaransRelatedByPembelajaranId->append($obj);
                        }
                      }

                      $this->collVldPembelajaransRelatedByPembelajaranIdPartial = true;
                    }

                    $collVldPembelajaransRelatedByPembelajaranId->getInternalIterator()->rewind();
                    return $collVldPembelajaransRelatedByPembelajaranId;
                }

                if($partial && $this->collVldPembelajaransRelatedByPembelajaranId) {
                    foreach($this->collVldPembelajaransRelatedByPembelajaranId as $obj) {
                        if($obj->isNew()) {
                            $collVldPembelajaransRelatedByPembelajaranId[] = $obj;
                        }
                    }
                }

                $this->collVldPembelajaransRelatedByPembelajaranId = $collVldPembelajaransRelatedByPembelajaranId;
                $this->collVldPembelajaransRelatedByPembelajaranIdPartial = false;
            }
        }

        return $this->collVldPembelajaransRelatedByPembelajaranId;
    }

    /**
     * Sets a collection of VldPembelajaranRelatedByPembelajaranId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldPembelajaransRelatedByPembelajaranId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Pembelajaran The current object (for fluent API support)
     */
    public function setVldPembelajaransRelatedByPembelajaranId(PropelCollection $vldPembelajaransRelatedByPembelajaranId, PropelPDO $con = null)
    {
        $vldPembelajaransRelatedByPembelajaranIdToDelete = $this->getVldPembelajaransRelatedByPembelajaranId(new Criteria(), $con)->diff($vldPembelajaransRelatedByPembelajaranId);

        $this->vldPembelajaransRelatedByPembelajaranIdScheduledForDeletion = unserialize(serialize($vldPembelajaransRelatedByPembelajaranIdToDelete));

        foreach ($vldPembelajaransRelatedByPembelajaranIdToDelete as $vldPembelajaranRelatedByPembelajaranIdRemoved) {
            $vldPembelajaranRelatedByPembelajaranIdRemoved->setPembelajaranRelatedByPembelajaranId(null);
        }

        $this->collVldPembelajaransRelatedByPembelajaranId = null;
        foreach ($vldPembelajaransRelatedByPembelajaranId as $vldPembelajaranRelatedByPembelajaranId) {
            $this->addVldPembelajaranRelatedByPembelajaranId($vldPembelajaranRelatedByPembelajaranId);
        }

        $this->collVldPembelajaransRelatedByPembelajaranId = $vldPembelajaransRelatedByPembelajaranId;
        $this->collVldPembelajaransRelatedByPembelajaranIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldPembelajaran objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldPembelajaran objects.
     * @throws PropelException
     */
    public function countVldPembelajaransRelatedByPembelajaranId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldPembelajaransRelatedByPembelajaranIdPartial && !$this->isNew();
        if (null === $this->collVldPembelajaransRelatedByPembelajaranId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldPembelajaransRelatedByPembelajaranId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldPembelajaransRelatedByPembelajaranId());
            }
            $query = VldPembelajaranQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPembelajaranRelatedByPembelajaranId($this)
                ->count($con);
        }

        return count($this->collVldPembelajaransRelatedByPembelajaranId);
    }

    /**
     * Method called to associate a VldPembelajaran object to this object
     * through the VldPembelajaran foreign key attribute.
     *
     * @param    VldPembelajaran $l VldPembelajaran
     * @return Pembelajaran The current object (for fluent API support)
     */
    public function addVldPembelajaranRelatedByPembelajaranId(VldPembelajaran $l)
    {
        if ($this->collVldPembelajaransRelatedByPembelajaranId === null) {
            $this->initVldPembelajaransRelatedByPembelajaranId();
            $this->collVldPembelajaransRelatedByPembelajaranIdPartial = true;
        }
        if (!in_array($l, $this->collVldPembelajaransRelatedByPembelajaranId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldPembelajaranRelatedByPembelajaranId($l);
        }

        return $this;
    }

    /**
     * @param	VldPembelajaranRelatedByPembelajaranId $vldPembelajaranRelatedByPembelajaranId The vldPembelajaranRelatedByPembelajaranId object to add.
     */
    protected function doAddVldPembelajaranRelatedByPembelajaranId($vldPembelajaranRelatedByPembelajaranId)
    {
        $this->collVldPembelajaransRelatedByPembelajaranId[]= $vldPembelajaranRelatedByPembelajaranId;
        $vldPembelajaranRelatedByPembelajaranId->setPembelajaranRelatedByPembelajaranId($this);
    }

    /**
     * @param	VldPembelajaranRelatedByPembelajaranId $vldPembelajaranRelatedByPembelajaranId The vldPembelajaranRelatedByPembelajaranId object to remove.
     * @return Pembelajaran The current object (for fluent API support)
     */
    public function removeVldPembelajaranRelatedByPembelajaranId($vldPembelajaranRelatedByPembelajaranId)
    {
        if ($this->getVldPembelajaransRelatedByPembelajaranId()->contains($vldPembelajaranRelatedByPembelajaranId)) {
            $this->collVldPembelajaransRelatedByPembelajaranId->remove($this->collVldPembelajaransRelatedByPembelajaranId->search($vldPembelajaranRelatedByPembelajaranId));
            if (null === $this->vldPembelajaransRelatedByPembelajaranIdScheduledForDeletion) {
                $this->vldPembelajaransRelatedByPembelajaranIdScheduledForDeletion = clone $this->collVldPembelajaransRelatedByPembelajaranId;
                $this->vldPembelajaransRelatedByPembelajaranIdScheduledForDeletion->clear();
            }
            $this->vldPembelajaransRelatedByPembelajaranIdScheduledForDeletion[]= clone $vldPembelajaranRelatedByPembelajaranId;
            $vldPembelajaranRelatedByPembelajaranId->setPembelajaranRelatedByPembelajaranId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pembelajaran is new, it will return
     * an empty collection; or if this Pembelajaran has previously
     * been saved, it will retrieve related VldPembelajaransRelatedByPembelajaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pembelajaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPembelajaran[] List of VldPembelajaran objects
     */
    public function getVldPembelajaransRelatedByPembelajaranIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPembelajaranQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldPembelajaransRelatedByPembelajaranId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pembelajaran is new, it will return
     * an empty collection; or if this Pembelajaran has previously
     * been saved, it will retrieve related VldPembelajaransRelatedByPembelajaranId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pembelajaran.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPembelajaran[] List of VldPembelajaran objects
     */
    public function getVldPembelajaransRelatedByPembelajaranIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPembelajaranQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldPembelajaransRelatedByPembelajaranId($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->pembelajaran_id = null;
        $this->rombongan_belajar_id = null;
        $this->semester_id = null;
        $this->mata_pelajaran_id = null;
        $this->ptk_terdaftar_id = null;
        $this->sk_mengajar = null;
        $this->tanggal_sk_mengajar = null;
        $this->jam_mengajar_per_minggu = null;
        $this->status_di_kurikulum = null;
        $this->nama_mata_pelajaran = null;
        $this->last_update = null;
        $this->soft_delete = null;
        $this->last_sync = null;
        $this->updater_id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collVldPembelajaransRelatedByPembelajaranId) {
                foreach ($this->collVldPembelajaransRelatedByPembelajaranId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldPembelajaransRelatedByPembelajaranId) {
                foreach ($this->collVldPembelajaransRelatedByPembelajaranId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aPtkTerdaftarRelatedByPtkTerdaftarId instanceof Persistent) {
              $this->aPtkTerdaftarRelatedByPtkTerdaftarId->clearAllReferences($deep);
            }
            if ($this->aPtkTerdaftarRelatedByPtkTerdaftarId instanceof Persistent) {
              $this->aPtkTerdaftarRelatedByPtkTerdaftarId->clearAllReferences($deep);
            }
            if ($this->aRombonganBelajarRelatedByRombonganBelajarId instanceof Persistent) {
              $this->aRombonganBelajarRelatedByRombonganBelajarId->clearAllReferences($deep);
            }
            if ($this->aRombonganBelajarRelatedByRombonganBelajarId instanceof Persistent) {
              $this->aRombonganBelajarRelatedByRombonganBelajarId->clearAllReferences($deep);
            }
            if ($this->aMataPelajaranRelatedByMataPelajaranId instanceof Persistent) {
              $this->aMataPelajaranRelatedByMataPelajaranId->clearAllReferences($deep);
            }
            if ($this->aMataPelajaranRelatedByMataPelajaranId instanceof Persistent) {
              $this->aMataPelajaranRelatedByMataPelajaranId->clearAllReferences($deep);
            }
            if ($this->aSemesterRelatedBySemesterId instanceof Persistent) {
              $this->aSemesterRelatedBySemesterId->clearAllReferences($deep);
            }
            if ($this->aSemesterRelatedBySemesterId instanceof Persistent) {
              $this->aSemesterRelatedBySemesterId->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collVldPembelajaransRelatedByPembelajaranId instanceof PropelCollection) {
            $this->collVldPembelajaransRelatedByPembelajaranId->clearIterator();
        }
        $this->collVldPembelajaransRelatedByPembelajaranId = null;
        if ($this->collVldPembelajaransRelatedByPembelajaranId instanceof PropelCollection) {
            $this->collVldPembelajaransRelatedByPembelajaranId->clearIterator();
        }
        $this->collVldPembelajaransRelatedByPembelajaranId = null;
        $this->aPtkTerdaftarRelatedByPtkTerdaftarId = null;
        $this->aPtkTerdaftarRelatedByPtkTerdaftarId = null;
        $this->aRombonganBelajarRelatedByRombonganBelajarId = null;
        $this->aRombonganBelajarRelatedByRombonganBelajarId = null;
        $this->aMataPelajaranRelatedByMataPelajaranId = null;
        $this->aMataPelajaranRelatedByMataPelajaranId = null;
        $this->aSemesterRelatedBySemesterId = null;
        $this->aSemesterRelatedBySemesterId = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(PembelajaranPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
