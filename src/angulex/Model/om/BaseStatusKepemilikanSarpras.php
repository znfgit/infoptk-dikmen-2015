<?php

namespace angulex\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelDateTime;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\Prasarana;
use angulex\Model\PrasaranaQuery;
use angulex\Model\Sarana;
use angulex\Model\SaranaQuery;
use angulex\Model\StatusKepemilikanSarpras;
use angulex\Model\StatusKepemilikanSarprasPeer;
use angulex\Model\StatusKepemilikanSarprasQuery;

/**
 * Base class that represents a row from the 'ref.status_kepemilikan_sarpras' table.
 *
 * 
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseStatusKepemilikanSarpras extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'angulex\\Model\\StatusKepemilikanSarprasPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        StatusKepemilikanSarprasPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the kepemilikan_sarpras_id field.
     * @var        string
     */
    protected $kepemilikan_sarpras_id;

    /**
     * The value for the nama field.
     * @var        string
     */
    protected $nama;

    /**
     * The value for the create_date field.
     * @var        string
     */
    protected $create_date;

    /**
     * The value for the last_update field.
     * @var        string
     */
    protected $last_update;

    /**
     * The value for the expired_date field.
     * @var        string
     */
    protected $expired_date;

    /**
     * The value for the last_sync field.
     * @var        string
     */
    protected $last_sync;

    /**
     * @var        PropelObjectCollection|Sarana[] Collection to store aggregation of Sarana objects.
     */
    protected $collSaranasRelatedByKepemilikanSarprasId;
    protected $collSaranasRelatedByKepemilikanSarprasIdPartial;

    /**
     * @var        PropelObjectCollection|Sarana[] Collection to store aggregation of Sarana objects.
     */
    protected $collSaranasRelatedByKepemilikanSarprasId;
    protected $collSaranasRelatedByKepemilikanSarprasIdPartial;

    /**
     * @var        PropelObjectCollection|Prasarana[] Collection to store aggregation of Prasarana objects.
     */
    protected $collPrasaranasRelatedByKepemilikanSarprasId;
    protected $collPrasaranasRelatedByKepemilikanSarprasIdPartial;

    /**
     * @var        PropelObjectCollection|Prasarana[] Collection to store aggregation of Prasarana objects.
     */
    protected $collPrasaranasRelatedByKepemilikanSarprasId;
    protected $collPrasaranasRelatedByKepemilikanSarprasIdPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $saranasRelatedByKepemilikanSarprasIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $saranasRelatedByKepemilikanSarprasIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $prasaranasRelatedByKepemilikanSarprasIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $prasaranasRelatedByKepemilikanSarprasIdScheduledForDeletion = null;

    /**
     * Get the [kepemilikan_sarpras_id] column value.
     * 
     * @return string
     */
    public function getKepemilikanSarprasId()
    {
        return $this->kepemilikan_sarpras_id;
    }

    /**
     * Get the [nama] column value.
     * 
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Get the [optionally formatted] temporal [create_date] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreateDate($format = 'Y-m-d H:i:s')
    {
        if ($this->create_date === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->create_date);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->create_date, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [last_update] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastUpdate($format = 'Y-m-d H:i:s')
    {
        if ($this->last_update === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_update);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_update, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [expired_date] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getExpiredDate($format = 'Y-m-d H:i:s')
    {
        if ($this->expired_date === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->expired_date);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->expired_date, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [last_sync] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastSync($format = 'Y-m-d H:i:s')
    {
        if ($this->last_sync === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_sync);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_sync, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Set the value of [kepemilikan_sarpras_id] column.
     * 
     * @param string $v new value
     * @return StatusKepemilikanSarpras The current object (for fluent API support)
     */
    public function setKepemilikanSarprasId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->kepemilikan_sarpras_id !== $v) {
            $this->kepemilikan_sarpras_id = $v;
            $this->modifiedColumns[] = StatusKepemilikanSarprasPeer::KEPEMILIKAN_SARPRAS_ID;
        }


        return $this;
    } // setKepemilikanSarprasId()

    /**
     * Set the value of [nama] column.
     * 
     * @param string $v new value
     * @return StatusKepemilikanSarpras The current object (for fluent API support)
     */
    public function setNama($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nama !== $v) {
            $this->nama = $v;
            $this->modifiedColumns[] = StatusKepemilikanSarprasPeer::NAMA;
        }


        return $this;
    } // setNama()

    /**
     * Sets the value of [create_date] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return StatusKepemilikanSarpras The current object (for fluent API support)
     */
    public function setCreateDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->create_date !== null || $dt !== null) {
            $currentDateAsString = ($this->create_date !== null && $tmpDt = new DateTime($this->create_date)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->create_date = $newDateAsString;
                $this->modifiedColumns[] = StatusKepemilikanSarprasPeer::CREATE_DATE;
            }
        } // if either are not null


        return $this;
    } // setCreateDate()

    /**
     * Sets the value of [last_update] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return StatusKepemilikanSarpras The current object (for fluent API support)
     */
    public function setLastUpdate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_update !== null || $dt !== null) {
            $currentDateAsString = ($this->last_update !== null && $tmpDt = new DateTime($this->last_update)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_update = $newDateAsString;
                $this->modifiedColumns[] = StatusKepemilikanSarprasPeer::LAST_UPDATE;
            }
        } // if either are not null


        return $this;
    } // setLastUpdate()

    /**
     * Sets the value of [expired_date] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return StatusKepemilikanSarpras The current object (for fluent API support)
     */
    public function setExpiredDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->expired_date !== null || $dt !== null) {
            $currentDateAsString = ($this->expired_date !== null && $tmpDt = new DateTime($this->expired_date)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->expired_date = $newDateAsString;
                $this->modifiedColumns[] = StatusKepemilikanSarprasPeer::EXPIRED_DATE;
            }
        } // if either are not null


        return $this;
    } // setExpiredDate()

    /**
     * Sets the value of [last_sync] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return StatusKepemilikanSarpras The current object (for fluent API support)
     */
    public function setLastSync($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_sync !== null || $dt !== null) {
            $currentDateAsString = ($this->last_sync !== null && $tmpDt = new DateTime($this->last_sync)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_sync = $newDateAsString;
                $this->modifiedColumns[] = StatusKepemilikanSarprasPeer::LAST_SYNC;
            }
        } // if either are not null


        return $this;
    } // setLastSync()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->kepemilikan_sarpras_id = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
            $this->nama = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->create_date = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->last_update = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->expired_date = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->last_sync = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 6; // 6 = StatusKepemilikanSarprasPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating StatusKepemilikanSarpras object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(StatusKepemilikanSarprasPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = StatusKepemilikanSarprasPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collSaranasRelatedByKepemilikanSarprasId = null;

            $this->collSaranasRelatedByKepemilikanSarprasId = null;

            $this->collPrasaranasRelatedByKepemilikanSarprasId = null;

            $this->collPrasaranasRelatedByKepemilikanSarprasId = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(StatusKepemilikanSarprasPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = StatusKepemilikanSarprasQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(StatusKepemilikanSarprasPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                StatusKepemilikanSarprasPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->saranasRelatedByKepemilikanSarprasIdScheduledForDeletion !== null) {
                if (!$this->saranasRelatedByKepemilikanSarprasIdScheduledForDeletion->isEmpty()) {
                    SaranaQuery::create()
                        ->filterByPrimaryKeys($this->saranasRelatedByKepemilikanSarprasIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->saranasRelatedByKepemilikanSarprasIdScheduledForDeletion = null;
                }
            }

            if ($this->collSaranasRelatedByKepemilikanSarprasId !== null) {
                foreach ($this->collSaranasRelatedByKepemilikanSarprasId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->saranasRelatedByKepemilikanSarprasIdScheduledForDeletion !== null) {
                if (!$this->saranasRelatedByKepemilikanSarprasIdScheduledForDeletion->isEmpty()) {
                    SaranaQuery::create()
                        ->filterByPrimaryKeys($this->saranasRelatedByKepemilikanSarprasIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->saranasRelatedByKepemilikanSarprasIdScheduledForDeletion = null;
                }
            }

            if ($this->collSaranasRelatedByKepemilikanSarprasId !== null) {
                foreach ($this->collSaranasRelatedByKepemilikanSarprasId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->prasaranasRelatedByKepemilikanSarprasIdScheduledForDeletion !== null) {
                if (!$this->prasaranasRelatedByKepemilikanSarprasIdScheduledForDeletion->isEmpty()) {
                    PrasaranaQuery::create()
                        ->filterByPrimaryKeys($this->prasaranasRelatedByKepemilikanSarprasIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->prasaranasRelatedByKepemilikanSarprasIdScheduledForDeletion = null;
                }
            }

            if ($this->collPrasaranasRelatedByKepemilikanSarprasId !== null) {
                foreach ($this->collPrasaranasRelatedByKepemilikanSarprasId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->prasaranasRelatedByKepemilikanSarprasIdScheduledForDeletion !== null) {
                if (!$this->prasaranasRelatedByKepemilikanSarprasIdScheduledForDeletion->isEmpty()) {
                    PrasaranaQuery::create()
                        ->filterByPrimaryKeys($this->prasaranasRelatedByKepemilikanSarprasIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->prasaranasRelatedByKepemilikanSarprasIdScheduledForDeletion = null;
                }
            }

            if ($this->collPrasaranasRelatedByKepemilikanSarprasId !== null) {
                foreach ($this->collPrasaranasRelatedByKepemilikanSarprasId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $criteria = $this->buildCriteria();
        $pk = BasePeer::doInsert($criteria, $con);
        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = StatusKepemilikanSarprasPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collSaranasRelatedByKepemilikanSarprasId !== null) {
                    foreach ($this->collSaranasRelatedByKepemilikanSarprasId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSaranasRelatedByKepemilikanSarprasId !== null) {
                    foreach ($this->collSaranasRelatedByKepemilikanSarprasId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPrasaranasRelatedByKepemilikanSarprasId !== null) {
                    foreach ($this->collPrasaranasRelatedByKepemilikanSarprasId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPrasaranasRelatedByKepemilikanSarprasId !== null) {
                    foreach ($this->collPrasaranasRelatedByKepemilikanSarprasId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = StatusKepemilikanSarprasPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getKepemilikanSarprasId();
                break;
            case 1:
                return $this->getNama();
                break;
            case 2:
                return $this->getCreateDate();
                break;
            case 3:
                return $this->getLastUpdate();
                break;
            case 4:
                return $this->getExpiredDate();
                break;
            case 5:
                return $this->getLastSync();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['StatusKepemilikanSarpras'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['StatusKepemilikanSarpras'][$this->getPrimaryKey()] = true;
        $keys = StatusKepemilikanSarprasPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getKepemilikanSarprasId(),
            $keys[1] => $this->getNama(),
            $keys[2] => $this->getCreateDate(),
            $keys[3] => $this->getLastUpdate(),
            $keys[4] => $this->getExpiredDate(),
            $keys[5] => $this->getLastSync(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->collSaranasRelatedByKepemilikanSarprasId) {
                $result['SaranasRelatedByKepemilikanSarprasId'] = $this->collSaranasRelatedByKepemilikanSarprasId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSaranasRelatedByKepemilikanSarprasId) {
                $result['SaranasRelatedByKepemilikanSarprasId'] = $this->collSaranasRelatedByKepemilikanSarprasId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPrasaranasRelatedByKepemilikanSarprasId) {
                $result['PrasaranasRelatedByKepemilikanSarprasId'] = $this->collPrasaranasRelatedByKepemilikanSarprasId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPrasaranasRelatedByKepemilikanSarprasId) {
                $result['PrasaranasRelatedByKepemilikanSarprasId'] = $this->collPrasaranasRelatedByKepemilikanSarprasId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = StatusKepemilikanSarprasPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setKepemilikanSarprasId($value);
                break;
            case 1:
                $this->setNama($value);
                break;
            case 2:
                $this->setCreateDate($value);
                break;
            case 3:
                $this->setLastUpdate($value);
                break;
            case 4:
                $this->setExpiredDate($value);
                break;
            case 5:
                $this->setLastSync($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = StatusKepemilikanSarprasPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setKepemilikanSarprasId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setNama($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setCreateDate($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setLastUpdate($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setExpiredDate($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setLastSync($arr[$keys[5]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(StatusKepemilikanSarprasPeer::DATABASE_NAME);

        if ($this->isColumnModified(StatusKepemilikanSarprasPeer::KEPEMILIKAN_SARPRAS_ID)) $criteria->add(StatusKepemilikanSarprasPeer::KEPEMILIKAN_SARPRAS_ID, $this->kepemilikan_sarpras_id);
        if ($this->isColumnModified(StatusKepemilikanSarprasPeer::NAMA)) $criteria->add(StatusKepemilikanSarprasPeer::NAMA, $this->nama);
        if ($this->isColumnModified(StatusKepemilikanSarprasPeer::CREATE_DATE)) $criteria->add(StatusKepemilikanSarprasPeer::CREATE_DATE, $this->create_date);
        if ($this->isColumnModified(StatusKepemilikanSarprasPeer::LAST_UPDATE)) $criteria->add(StatusKepemilikanSarprasPeer::LAST_UPDATE, $this->last_update);
        if ($this->isColumnModified(StatusKepemilikanSarprasPeer::EXPIRED_DATE)) $criteria->add(StatusKepemilikanSarprasPeer::EXPIRED_DATE, $this->expired_date);
        if ($this->isColumnModified(StatusKepemilikanSarprasPeer::LAST_SYNC)) $criteria->add(StatusKepemilikanSarprasPeer::LAST_SYNC, $this->last_sync);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(StatusKepemilikanSarprasPeer::DATABASE_NAME);
        $criteria->add(StatusKepemilikanSarprasPeer::KEPEMILIKAN_SARPRAS_ID, $this->kepemilikan_sarpras_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getKepemilikanSarprasId();
    }

    /**
     * Generic method to set the primary key (kepemilikan_sarpras_id column).
     *
     * @param  string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setKepemilikanSarprasId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getKepemilikanSarprasId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of StatusKepemilikanSarpras (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setNama($this->getNama());
        $copyObj->setCreateDate($this->getCreateDate());
        $copyObj->setLastUpdate($this->getLastUpdate());
        $copyObj->setExpiredDate($this->getExpiredDate());
        $copyObj->setLastSync($this->getLastSync());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getSaranasRelatedByKepemilikanSarprasId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSaranaRelatedByKepemilikanSarprasId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSaranasRelatedByKepemilikanSarprasId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSaranaRelatedByKepemilikanSarprasId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPrasaranasRelatedByKepemilikanSarprasId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPrasaranaRelatedByKepemilikanSarprasId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPrasaranasRelatedByKepemilikanSarprasId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPrasaranaRelatedByKepemilikanSarprasId($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setKepemilikanSarprasId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return StatusKepemilikanSarpras Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return StatusKepemilikanSarprasPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new StatusKepemilikanSarprasPeer();
        }

        return self::$peer;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('SaranaRelatedByKepemilikanSarprasId' == $relationName) {
            $this->initSaranasRelatedByKepemilikanSarprasId();
        }
        if ('SaranaRelatedByKepemilikanSarprasId' == $relationName) {
            $this->initSaranasRelatedByKepemilikanSarprasId();
        }
        if ('PrasaranaRelatedByKepemilikanSarprasId' == $relationName) {
            $this->initPrasaranasRelatedByKepemilikanSarprasId();
        }
        if ('PrasaranaRelatedByKepemilikanSarprasId' == $relationName) {
            $this->initPrasaranasRelatedByKepemilikanSarprasId();
        }
    }

    /**
     * Clears out the collSaranasRelatedByKepemilikanSarprasId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return StatusKepemilikanSarpras The current object (for fluent API support)
     * @see        addSaranasRelatedByKepemilikanSarprasId()
     */
    public function clearSaranasRelatedByKepemilikanSarprasId()
    {
        $this->collSaranasRelatedByKepemilikanSarprasId = null; // important to set this to null since that means it is uninitialized
        $this->collSaranasRelatedByKepemilikanSarprasIdPartial = null;

        return $this;
    }

    /**
     * reset is the collSaranasRelatedByKepemilikanSarprasId collection loaded partially
     *
     * @return void
     */
    public function resetPartialSaranasRelatedByKepemilikanSarprasId($v = true)
    {
        $this->collSaranasRelatedByKepemilikanSarprasIdPartial = $v;
    }

    /**
     * Initializes the collSaranasRelatedByKepemilikanSarprasId collection.
     *
     * By default this just sets the collSaranasRelatedByKepemilikanSarprasId collection to an empty array (like clearcollSaranasRelatedByKepemilikanSarprasId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSaranasRelatedByKepemilikanSarprasId($overrideExisting = true)
    {
        if (null !== $this->collSaranasRelatedByKepemilikanSarprasId && !$overrideExisting) {
            return;
        }
        $this->collSaranasRelatedByKepemilikanSarprasId = new PropelObjectCollection();
        $this->collSaranasRelatedByKepemilikanSarprasId->setModel('Sarana');
    }

    /**
     * Gets an array of Sarana objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this StatusKepemilikanSarpras is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Sarana[] List of Sarana objects
     * @throws PropelException
     */
    public function getSaranasRelatedByKepemilikanSarprasId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSaranasRelatedByKepemilikanSarprasIdPartial && !$this->isNew();
        if (null === $this->collSaranasRelatedByKepemilikanSarprasId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSaranasRelatedByKepemilikanSarprasId) {
                // return empty collection
                $this->initSaranasRelatedByKepemilikanSarprasId();
            } else {
                $collSaranasRelatedByKepemilikanSarprasId = SaranaQuery::create(null, $criteria)
                    ->filterByStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSaranasRelatedByKepemilikanSarprasIdPartial && count($collSaranasRelatedByKepemilikanSarprasId)) {
                      $this->initSaranasRelatedByKepemilikanSarprasId(false);

                      foreach($collSaranasRelatedByKepemilikanSarprasId as $obj) {
                        if (false == $this->collSaranasRelatedByKepemilikanSarprasId->contains($obj)) {
                          $this->collSaranasRelatedByKepemilikanSarprasId->append($obj);
                        }
                      }

                      $this->collSaranasRelatedByKepemilikanSarprasIdPartial = true;
                    }

                    $collSaranasRelatedByKepemilikanSarprasId->getInternalIterator()->rewind();
                    return $collSaranasRelatedByKepemilikanSarprasId;
                }

                if($partial && $this->collSaranasRelatedByKepemilikanSarprasId) {
                    foreach($this->collSaranasRelatedByKepemilikanSarprasId as $obj) {
                        if($obj->isNew()) {
                            $collSaranasRelatedByKepemilikanSarprasId[] = $obj;
                        }
                    }
                }

                $this->collSaranasRelatedByKepemilikanSarprasId = $collSaranasRelatedByKepemilikanSarprasId;
                $this->collSaranasRelatedByKepemilikanSarprasIdPartial = false;
            }
        }

        return $this->collSaranasRelatedByKepemilikanSarprasId;
    }

    /**
     * Sets a collection of SaranaRelatedByKepemilikanSarprasId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $saranasRelatedByKepemilikanSarprasId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return StatusKepemilikanSarpras The current object (for fluent API support)
     */
    public function setSaranasRelatedByKepemilikanSarprasId(PropelCollection $saranasRelatedByKepemilikanSarprasId, PropelPDO $con = null)
    {
        $saranasRelatedByKepemilikanSarprasIdToDelete = $this->getSaranasRelatedByKepemilikanSarprasId(new Criteria(), $con)->diff($saranasRelatedByKepemilikanSarprasId);

        $this->saranasRelatedByKepemilikanSarprasIdScheduledForDeletion = unserialize(serialize($saranasRelatedByKepemilikanSarprasIdToDelete));

        foreach ($saranasRelatedByKepemilikanSarprasIdToDelete as $saranaRelatedByKepemilikanSarprasIdRemoved) {
            $saranaRelatedByKepemilikanSarprasIdRemoved->setStatusKepemilikanSarprasRelatedByKepemilikanSarprasId(null);
        }

        $this->collSaranasRelatedByKepemilikanSarprasId = null;
        foreach ($saranasRelatedByKepemilikanSarprasId as $saranaRelatedByKepemilikanSarprasId) {
            $this->addSaranaRelatedByKepemilikanSarprasId($saranaRelatedByKepemilikanSarprasId);
        }

        $this->collSaranasRelatedByKepemilikanSarprasId = $saranasRelatedByKepemilikanSarprasId;
        $this->collSaranasRelatedByKepemilikanSarprasIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Sarana objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Sarana objects.
     * @throws PropelException
     */
    public function countSaranasRelatedByKepemilikanSarprasId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSaranasRelatedByKepemilikanSarprasIdPartial && !$this->isNew();
        if (null === $this->collSaranasRelatedByKepemilikanSarprasId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSaranasRelatedByKepemilikanSarprasId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSaranasRelatedByKepemilikanSarprasId());
            }
            $query = SaranaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($this)
                ->count($con);
        }

        return count($this->collSaranasRelatedByKepemilikanSarprasId);
    }

    /**
     * Method called to associate a Sarana object to this object
     * through the Sarana foreign key attribute.
     *
     * @param    Sarana $l Sarana
     * @return StatusKepemilikanSarpras The current object (for fluent API support)
     */
    public function addSaranaRelatedByKepemilikanSarprasId(Sarana $l)
    {
        if ($this->collSaranasRelatedByKepemilikanSarprasId === null) {
            $this->initSaranasRelatedByKepemilikanSarprasId();
            $this->collSaranasRelatedByKepemilikanSarprasIdPartial = true;
        }
        if (!in_array($l, $this->collSaranasRelatedByKepemilikanSarprasId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSaranaRelatedByKepemilikanSarprasId($l);
        }

        return $this;
    }

    /**
     * @param	SaranaRelatedByKepemilikanSarprasId $saranaRelatedByKepemilikanSarprasId The saranaRelatedByKepemilikanSarprasId object to add.
     */
    protected function doAddSaranaRelatedByKepemilikanSarprasId($saranaRelatedByKepemilikanSarprasId)
    {
        $this->collSaranasRelatedByKepemilikanSarprasId[]= $saranaRelatedByKepemilikanSarprasId;
        $saranaRelatedByKepemilikanSarprasId->setStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($this);
    }

    /**
     * @param	SaranaRelatedByKepemilikanSarprasId $saranaRelatedByKepemilikanSarprasId The saranaRelatedByKepemilikanSarprasId object to remove.
     * @return StatusKepemilikanSarpras The current object (for fluent API support)
     */
    public function removeSaranaRelatedByKepemilikanSarprasId($saranaRelatedByKepemilikanSarprasId)
    {
        if ($this->getSaranasRelatedByKepemilikanSarprasId()->contains($saranaRelatedByKepemilikanSarprasId)) {
            $this->collSaranasRelatedByKepemilikanSarprasId->remove($this->collSaranasRelatedByKepemilikanSarprasId->search($saranaRelatedByKepemilikanSarprasId));
            if (null === $this->saranasRelatedByKepemilikanSarprasIdScheduledForDeletion) {
                $this->saranasRelatedByKepemilikanSarprasIdScheduledForDeletion = clone $this->collSaranasRelatedByKepemilikanSarprasId;
                $this->saranasRelatedByKepemilikanSarprasIdScheduledForDeletion->clear();
            }
            $this->saranasRelatedByKepemilikanSarprasIdScheduledForDeletion[]= clone $saranaRelatedByKepemilikanSarprasId;
            $saranaRelatedByKepemilikanSarprasId->setStatusKepemilikanSarprasRelatedByKepemilikanSarprasId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this StatusKepemilikanSarpras is new, it will return
     * an empty collection; or if this StatusKepemilikanSarpras has previously
     * been saved, it will retrieve related SaranasRelatedByKepemilikanSarprasId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in StatusKepemilikanSarpras.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sarana[] List of Sarana objects
     */
    public function getSaranasRelatedByKepemilikanSarprasIdJoinPrasaranaRelatedByPrasaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SaranaQuery::create(null, $criteria);
        $query->joinWith('PrasaranaRelatedByPrasaranaId', $join_behavior);

        return $this->getSaranasRelatedByKepemilikanSarprasId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this StatusKepemilikanSarpras is new, it will return
     * an empty collection; or if this StatusKepemilikanSarpras has previously
     * been saved, it will retrieve related SaranasRelatedByKepemilikanSarprasId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in StatusKepemilikanSarpras.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sarana[] List of Sarana objects
     */
    public function getSaranasRelatedByKepemilikanSarprasIdJoinPrasaranaRelatedByPrasaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SaranaQuery::create(null, $criteria);
        $query->joinWith('PrasaranaRelatedByPrasaranaId', $join_behavior);

        return $this->getSaranasRelatedByKepemilikanSarprasId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this StatusKepemilikanSarpras is new, it will return
     * an empty collection; or if this StatusKepemilikanSarpras has previously
     * been saved, it will retrieve related SaranasRelatedByKepemilikanSarprasId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in StatusKepemilikanSarpras.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sarana[] List of Sarana objects
     */
    public function getSaranasRelatedByKepemilikanSarprasIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SaranaQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getSaranasRelatedByKepemilikanSarprasId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this StatusKepemilikanSarpras is new, it will return
     * an empty collection; or if this StatusKepemilikanSarpras has previously
     * been saved, it will retrieve related SaranasRelatedByKepemilikanSarprasId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in StatusKepemilikanSarpras.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sarana[] List of Sarana objects
     */
    public function getSaranasRelatedByKepemilikanSarprasIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SaranaQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getSaranasRelatedByKepemilikanSarprasId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this StatusKepemilikanSarpras is new, it will return
     * an empty collection; or if this StatusKepemilikanSarpras has previously
     * been saved, it will retrieve related SaranasRelatedByKepemilikanSarprasId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in StatusKepemilikanSarpras.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sarana[] List of Sarana objects
     */
    public function getSaranasRelatedByKepemilikanSarprasIdJoinJenisSaranaRelatedByJenisSaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SaranaQuery::create(null, $criteria);
        $query->joinWith('JenisSaranaRelatedByJenisSaranaId', $join_behavior);

        return $this->getSaranasRelatedByKepemilikanSarprasId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this StatusKepemilikanSarpras is new, it will return
     * an empty collection; or if this StatusKepemilikanSarpras has previously
     * been saved, it will retrieve related SaranasRelatedByKepemilikanSarprasId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in StatusKepemilikanSarpras.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sarana[] List of Sarana objects
     */
    public function getSaranasRelatedByKepemilikanSarprasIdJoinJenisSaranaRelatedByJenisSaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SaranaQuery::create(null, $criteria);
        $query->joinWith('JenisSaranaRelatedByJenisSaranaId', $join_behavior);

        return $this->getSaranasRelatedByKepemilikanSarprasId($query, $con);
    }

    /**
     * Clears out the collSaranasRelatedByKepemilikanSarprasId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return StatusKepemilikanSarpras The current object (for fluent API support)
     * @see        addSaranasRelatedByKepemilikanSarprasId()
     */
    public function clearSaranasRelatedByKepemilikanSarprasId()
    {
        $this->collSaranasRelatedByKepemilikanSarprasId = null; // important to set this to null since that means it is uninitialized
        $this->collSaranasRelatedByKepemilikanSarprasIdPartial = null;

        return $this;
    }

    /**
     * reset is the collSaranasRelatedByKepemilikanSarprasId collection loaded partially
     *
     * @return void
     */
    public function resetPartialSaranasRelatedByKepemilikanSarprasId($v = true)
    {
        $this->collSaranasRelatedByKepemilikanSarprasIdPartial = $v;
    }

    /**
     * Initializes the collSaranasRelatedByKepemilikanSarprasId collection.
     *
     * By default this just sets the collSaranasRelatedByKepemilikanSarprasId collection to an empty array (like clearcollSaranasRelatedByKepemilikanSarprasId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSaranasRelatedByKepemilikanSarprasId($overrideExisting = true)
    {
        if (null !== $this->collSaranasRelatedByKepemilikanSarprasId && !$overrideExisting) {
            return;
        }
        $this->collSaranasRelatedByKepemilikanSarprasId = new PropelObjectCollection();
        $this->collSaranasRelatedByKepemilikanSarprasId->setModel('Sarana');
    }

    /**
     * Gets an array of Sarana objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this StatusKepemilikanSarpras is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Sarana[] List of Sarana objects
     * @throws PropelException
     */
    public function getSaranasRelatedByKepemilikanSarprasId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSaranasRelatedByKepemilikanSarprasIdPartial && !$this->isNew();
        if (null === $this->collSaranasRelatedByKepemilikanSarprasId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSaranasRelatedByKepemilikanSarprasId) {
                // return empty collection
                $this->initSaranasRelatedByKepemilikanSarprasId();
            } else {
                $collSaranasRelatedByKepemilikanSarprasId = SaranaQuery::create(null, $criteria)
                    ->filterByStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSaranasRelatedByKepemilikanSarprasIdPartial && count($collSaranasRelatedByKepemilikanSarprasId)) {
                      $this->initSaranasRelatedByKepemilikanSarprasId(false);

                      foreach($collSaranasRelatedByKepemilikanSarprasId as $obj) {
                        if (false == $this->collSaranasRelatedByKepemilikanSarprasId->contains($obj)) {
                          $this->collSaranasRelatedByKepemilikanSarprasId->append($obj);
                        }
                      }

                      $this->collSaranasRelatedByKepemilikanSarprasIdPartial = true;
                    }

                    $collSaranasRelatedByKepemilikanSarprasId->getInternalIterator()->rewind();
                    return $collSaranasRelatedByKepemilikanSarprasId;
                }

                if($partial && $this->collSaranasRelatedByKepemilikanSarprasId) {
                    foreach($this->collSaranasRelatedByKepemilikanSarprasId as $obj) {
                        if($obj->isNew()) {
                            $collSaranasRelatedByKepemilikanSarprasId[] = $obj;
                        }
                    }
                }

                $this->collSaranasRelatedByKepemilikanSarprasId = $collSaranasRelatedByKepemilikanSarprasId;
                $this->collSaranasRelatedByKepemilikanSarprasIdPartial = false;
            }
        }

        return $this->collSaranasRelatedByKepemilikanSarprasId;
    }

    /**
     * Sets a collection of SaranaRelatedByKepemilikanSarprasId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $saranasRelatedByKepemilikanSarprasId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return StatusKepemilikanSarpras The current object (for fluent API support)
     */
    public function setSaranasRelatedByKepemilikanSarprasId(PropelCollection $saranasRelatedByKepemilikanSarprasId, PropelPDO $con = null)
    {
        $saranasRelatedByKepemilikanSarprasIdToDelete = $this->getSaranasRelatedByKepemilikanSarprasId(new Criteria(), $con)->diff($saranasRelatedByKepemilikanSarprasId);

        $this->saranasRelatedByKepemilikanSarprasIdScheduledForDeletion = unserialize(serialize($saranasRelatedByKepemilikanSarprasIdToDelete));

        foreach ($saranasRelatedByKepemilikanSarprasIdToDelete as $saranaRelatedByKepemilikanSarprasIdRemoved) {
            $saranaRelatedByKepemilikanSarprasIdRemoved->setStatusKepemilikanSarprasRelatedByKepemilikanSarprasId(null);
        }

        $this->collSaranasRelatedByKepemilikanSarprasId = null;
        foreach ($saranasRelatedByKepemilikanSarprasId as $saranaRelatedByKepemilikanSarprasId) {
            $this->addSaranaRelatedByKepemilikanSarprasId($saranaRelatedByKepemilikanSarprasId);
        }

        $this->collSaranasRelatedByKepemilikanSarprasId = $saranasRelatedByKepemilikanSarprasId;
        $this->collSaranasRelatedByKepemilikanSarprasIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Sarana objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Sarana objects.
     * @throws PropelException
     */
    public function countSaranasRelatedByKepemilikanSarprasId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSaranasRelatedByKepemilikanSarprasIdPartial && !$this->isNew();
        if (null === $this->collSaranasRelatedByKepemilikanSarprasId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSaranasRelatedByKepemilikanSarprasId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSaranasRelatedByKepemilikanSarprasId());
            }
            $query = SaranaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($this)
                ->count($con);
        }

        return count($this->collSaranasRelatedByKepemilikanSarprasId);
    }

    /**
     * Method called to associate a Sarana object to this object
     * through the Sarana foreign key attribute.
     *
     * @param    Sarana $l Sarana
     * @return StatusKepemilikanSarpras The current object (for fluent API support)
     */
    public function addSaranaRelatedByKepemilikanSarprasId(Sarana $l)
    {
        if ($this->collSaranasRelatedByKepemilikanSarprasId === null) {
            $this->initSaranasRelatedByKepemilikanSarprasId();
            $this->collSaranasRelatedByKepemilikanSarprasIdPartial = true;
        }
        if (!in_array($l, $this->collSaranasRelatedByKepemilikanSarprasId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSaranaRelatedByKepemilikanSarprasId($l);
        }

        return $this;
    }

    /**
     * @param	SaranaRelatedByKepemilikanSarprasId $saranaRelatedByKepemilikanSarprasId The saranaRelatedByKepemilikanSarprasId object to add.
     */
    protected function doAddSaranaRelatedByKepemilikanSarprasId($saranaRelatedByKepemilikanSarprasId)
    {
        $this->collSaranasRelatedByKepemilikanSarprasId[]= $saranaRelatedByKepemilikanSarprasId;
        $saranaRelatedByKepemilikanSarprasId->setStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($this);
    }

    /**
     * @param	SaranaRelatedByKepemilikanSarprasId $saranaRelatedByKepemilikanSarprasId The saranaRelatedByKepemilikanSarprasId object to remove.
     * @return StatusKepemilikanSarpras The current object (for fluent API support)
     */
    public function removeSaranaRelatedByKepemilikanSarprasId($saranaRelatedByKepemilikanSarprasId)
    {
        if ($this->getSaranasRelatedByKepemilikanSarprasId()->contains($saranaRelatedByKepemilikanSarprasId)) {
            $this->collSaranasRelatedByKepemilikanSarprasId->remove($this->collSaranasRelatedByKepemilikanSarprasId->search($saranaRelatedByKepemilikanSarprasId));
            if (null === $this->saranasRelatedByKepemilikanSarprasIdScheduledForDeletion) {
                $this->saranasRelatedByKepemilikanSarprasIdScheduledForDeletion = clone $this->collSaranasRelatedByKepemilikanSarprasId;
                $this->saranasRelatedByKepemilikanSarprasIdScheduledForDeletion->clear();
            }
            $this->saranasRelatedByKepemilikanSarprasIdScheduledForDeletion[]= clone $saranaRelatedByKepemilikanSarprasId;
            $saranaRelatedByKepemilikanSarprasId->setStatusKepemilikanSarprasRelatedByKepemilikanSarprasId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this StatusKepemilikanSarpras is new, it will return
     * an empty collection; or if this StatusKepemilikanSarpras has previously
     * been saved, it will retrieve related SaranasRelatedByKepemilikanSarprasId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in StatusKepemilikanSarpras.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sarana[] List of Sarana objects
     */
    public function getSaranasRelatedByKepemilikanSarprasIdJoinPrasaranaRelatedByPrasaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SaranaQuery::create(null, $criteria);
        $query->joinWith('PrasaranaRelatedByPrasaranaId', $join_behavior);

        return $this->getSaranasRelatedByKepemilikanSarprasId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this StatusKepemilikanSarpras is new, it will return
     * an empty collection; or if this StatusKepemilikanSarpras has previously
     * been saved, it will retrieve related SaranasRelatedByKepemilikanSarprasId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in StatusKepemilikanSarpras.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sarana[] List of Sarana objects
     */
    public function getSaranasRelatedByKepemilikanSarprasIdJoinPrasaranaRelatedByPrasaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SaranaQuery::create(null, $criteria);
        $query->joinWith('PrasaranaRelatedByPrasaranaId', $join_behavior);

        return $this->getSaranasRelatedByKepemilikanSarprasId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this StatusKepemilikanSarpras is new, it will return
     * an empty collection; or if this StatusKepemilikanSarpras has previously
     * been saved, it will retrieve related SaranasRelatedByKepemilikanSarprasId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in StatusKepemilikanSarpras.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sarana[] List of Sarana objects
     */
    public function getSaranasRelatedByKepemilikanSarprasIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SaranaQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getSaranasRelatedByKepemilikanSarprasId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this StatusKepemilikanSarpras is new, it will return
     * an empty collection; or if this StatusKepemilikanSarpras has previously
     * been saved, it will retrieve related SaranasRelatedByKepemilikanSarprasId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in StatusKepemilikanSarpras.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sarana[] List of Sarana objects
     */
    public function getSaranasRelatedByKepemilikanSarprasIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SaranaQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getSaranasRelatedByKepemilikanSarprasId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this StatusKepemilikanSarpras is new, it will return
     * an empty collection; or if this StatusKepemilikanSarpras has previously
     * been saved, it will retrieve related SaranasRelatedByKepemilikanSarprasId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in StatusKepemilikanSarpras.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sarana[] List of Sarana objects
     */
    public function getSaranasRelatedByKepemilikanSarprasIdJoinJenisSaranaRelatedByJenisSaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SaranaQuery::create(null, $criteria);
        $query->joinWith('JenisSaranaRelatedByJenisSaranaId', $join_behavior);

        return $this->getSaranasRelatedByKepemilikanSarprasId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this StatusKepemilikanSarpras is new, it will return
     * an empty collection; or if this StatusKepemilikanSarpras has previously
     * been saved, it will retrieve related SaranasRelatedByKepemilikanSarprasId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in StatusKepemilikanSarpras.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sarana[] List of Sarana objects
     */
    public function getSaranasRelatedByKepemilikanSarprasIdJoinJenisSaranaRelatedByJenisSaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SaranaQuery::create(null, $criteria);
        $query->joinWith('JenisSaranaRelatedByJenisSaranaId', $join_behavior);

        return $this->getSaranasRelatedByKepemilikanSarprasId($query, $con);
    }

    /**
     * Clears out the collPrasaranasRelatedByKepemilikanSarprasId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return StatusKepemilikanSarpras The current object (for fluent API support)
     * @see        addPrasaranasRelatedByKepemilikanSarprasId()
     */
    public function clearPrasaranasRelatedByKepemilikanSarprasId()
    {
        $this->collPrasaranasRelatedByKepemilikanSarprasId = null; // important to set this to null since that means it is uninitialized
        $this->collPrasaranasRelatedByKepemilikanSarprasIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPrasaranasRelatedByKepemilikanSarprasId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPrasaranasRelatedByKepemilikanSarprasId($v = true)
    {
        $this->collPrasaranasRelatedByKepemilikanSarprasIdPartial = $v;
    }

    /**
     * Initializes the collPrasaranasRelatedByKepemilikanSarprasId collection.
     *
     * By default this just sets the collPrasaranasRelatedByKepemilikanSarprasId collection to an empty array (like clearcollPrasaranasRelatedByKepemilikanSarprasId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPrasaranasRelatedByKepemilikanSarprasId($overrideExisting = true)
    {
        if (null !== $this->collPrasaranasRelatedByKepemilikanSarprasId && !$overrideExisting) {
            return;
        }
        $this->collPrasaranasRelatedByKepemilikanSarprasId = new PropelObjectCollection();
        $this->collPrasaranasRelatedByKepemilikanSarprasId->setModel('Prasarana');
    }

    /**
     * Gets an array of Prasarana objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this StatusKepemilikanSarpras is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Prasarana[] List of Prasarana objects
     * @throws PropelException
     */
    public function getPrasaranasRelatedByKepemilikanSarprasId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPrasaranasRelatedByKepemilikanSarprasIdPartial && !$this->isNew();
        if (null === $this->collPrasaranasRelatedByKepemilikanSarprasId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPrasaranasRelatedByKepemilikanSarprasId) {
                // return empty collection
                $this->initPrasaranasRelatedByKepemilikanSarprasId();
            } else {
                $collPrasaranasRelatedByKepemilikanSarprasId = PrasaranaQuery::create(null, $criteria)
                    ->filterByStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPrasaranasRelatedByKepemilikanSarprasIdPartial && count($collPrasaranasRelatedByKepemilikanSarprasId)) {
                      $this->initPrasaranasRelatedByKepemilikanSarprasId(false);

                      foreach($collPrasaranasRelatedByKepemilikanSarprasId as $obj) {
                        if (false == $this->collPrasaranasRelatedByKepemilikanSarprasId->contains($obj)) {
                          $this->collPrasaranasRelatedByKepemilikanSarprasId->append($obj);
                        }
                      }

                      $this->collPrasaranasRelatedByKepemilikanSarprasIdPartial = true;
                    }

                    $collPrasaranasRelatedByKepemilikanSarprasId->getInternalIterator()->rewind();
                    return $collPrasaranasRelatedByKepemilikanSarprasId;
                }

                if($partial && $this->collPrasaranasRelatedByKepemilikanSarprasId) {
                    foreach($this->collPrasaranasRelatedByKepemilikanSarprasId as $obj) {
                        if($obj->isNew()) {
                            $collPrasaranasRelatedByKepemilikanSarprasId[] = $obj;
                        }
                    }
                }

                $this->collPrasaranasRelatedByKepemilikanSarprasId = $collPrasaranasRelatedByKepemilikanSarprasId;
                $this->collPrasaranasRelatedByKepemilikanSarprasIdPartial = false;
            }
        }

        return $this->collPrasaranasRelatedByKepemilikanSarprasId;
    }

    /**
     * Sets a collection of PrasaranaRelatedByKepemilikanSarprasId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $prasaranasRelatedByKepemilikanSarprasId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return StatusKepemilikanSarpras The current object (for fluent API support)
     */
    public function setPrasaranasRelatedByKepemilikanSarprasId(PropelCollection $prasaranasRelatedByKepemilikanSarprasId, PropelPDO $con = null)
    {
        $prasaranasRelatedByKepemilikanSarprasIdToDelete = $this->getPrasaranasRelatedByKepemilikanSarprasId(new Criteria(), $con)->diff($prasaranasRelatedByKepemilikanSarprasId);

        $this->prasaranasRelatedByKepemilikanSarprasIdScheduledForDeletion = unserialize(serialize($prasaranasRelatedByKepemilikanSarprasIdToDelete));

        foreach ($prasaranasRelatedByKepemilikanSarprasIdToDelete as $prasaranaRelatedByKepemilikanSarprasIdRemoved) {
            $prasaranaRelatedByKepemilikanSarprasIdRemoved->setStatusKepemilikanSarprasRelatedByKepemilikanSarprasId(null);
        }

        $this->collPrasaranasRelatedByKepemilikanSarprasId = null;
        foreach ($prasaranasRelatedByKepemilikanSarprasId as $prasaranaRelatedByKepemilikanSarprasId) {
            $this->addPrasaranaRelatedByKepemilikanSarprasId($prasaranaRelatedByKepemilikanSarprasId);
        }

        $this->collPrasaranasRelatedByKepemilikanSarprasId = $prasaranasRelatedByKepemilikanSarprasId;
        $this->collPrasaranasRelatedByKepemilikanSarprasIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Prasarana objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Prasarana objects.
     * @throws PropelException
     */
    public function countPrasaranasRelatedByKepemilikanSarprasId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPrasaranasRelatedByKepemilikanSarprasIdPartial && !$this->isNew();
        if (null === $this->collPrasaranasRelatedByKepemilikanSarprasId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPrasaranasRelatedByKepemilikanSarprasId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPrasaranasRelatedByKepemilikanSarprasId());
            }
            $query = PrasaranaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($this)
                ->count($con);
        }

        return count($this->collPrasaranasRelatedByKepemilikanSarprasId);
    }

    /**
     * Method called to associate a Prasarana object to this object
     * through the Prasarana foreign key attribute.
     *
     * @param    Prasarana $l Prasarana
     * @return StatusKepemilikanSarpras The current object (for fluent API support)
     */
    public function addPrasaranaRelatedByKepemilikanSarprasId(Prasarana $l)
    {
        if ($this->collPrasaranasRelatedByKepemilikanSarprasId === null) {
            $this->initPrasaranasRelatedByKepemilikanSarprasId();
            $this->collPrasaranasRelatedByKepemilikanSarprasIdPartial = true;
        }
        if (!in_array($l, $this->collPrasaranasRelatedByKepemilikanSarprasId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPrasaranaRelatedByKepemilikanSarprasId($l);
        }

        return $this;
    }

    /**
     * @param	PrasaranaRelatedByKepemilikanSarprasId $prasaranaRelatedByKepemilikanSarprasId The prasaranaRelatedByKepemilikanSarprasId object to add.
     */
    protected function doAddPrasaranaRelatedByKepemilikanSarprasId($prasaranaRelatedByKepemilikanSarprasId)
    {
        $this->collPrasaranasRelatedByKepemilikanSarprasId[]= $prasaranaRelatedByKepemilikanSarprasId;
        $prasaranaRelatedByKepemilikanSarprasId->setStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($this);
    }

    /**
     * @param	PrasaranaRelatedByKepemilikanSarprasId $prasaranaRelatedByKepemilikanSarprasId The prasaranaRelatedByKepemilikanSarprasId object to remove.
     * @return StatusKepemilikanSarpras The current object (for fluent API support)
     */
    public function removePrasaranaRelatedByKepemilikanSarprasId($prasaranaRelatedByKepemilikanSarprasId)
    {
        if ($this->getPrasaranasRelatedByKepemilikanSarprasId()->contains($prasaranaRelatedByKepemilikanSarprasId)) {
            $this->collPrasaranasRelatedByKepemilikanSarprasId->remove($this->collPrasaranasRelatedByKepemilikanSarprasId->search($prasaranaRelatedByKepemilikanSarprasId));
            if (null === $this->prasaranasRelatedByKepemilikanSarprasIdScheduledForDeletion) {
                $this->prasaranasRelatedByKepemilikanSarprasIdScheduledForDeletion = clone $this->collPrasaranasRelatedByKepemilikanSarprasId;
                $this->prasaranasRelatedByKepemilikanSarprasIdScheduledForDeletion->clear();
            }
            $this->prasaranasRelatedByKepemilikanSarprasIdScheduledForDeletion[]= clone $prasaranaRelatedByKepemilikanSarprasId;
            $prasaranaRelatedByKepemilikanSarprasId->setStatusKepemilikanSarprasRelatedByKepemilikanSarprasId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this StatusKepemilikanSarpras is new, it will return
     * an empty collection; or if this StatusKepemilikanSarpras has previously
     * been saved, it will retrieve related PrasaranasRelatedByKepemilikanSarprasId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in StatusKepemilikanSarpras.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Prasarana[] List of Prasarana objects
     */
    public function getPrasaranasRelatedByKepemilikanSarprasIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PrasaranaQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPrasaranasRelatedByKepemilikanSarprasId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this StatusKepemilikanSarpras is new, it will return
     * an empty collection; or if this StatusKepemilikanSarpras has previously
     * been saved, it will retrieve related PrasaranasRelatedByKepemilikanSarprasId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in StatusKepemilikanSarpras.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Prasarana[] List of Prasarana objects
     */
    public function getPrasaranasRelatedByKepemilikanSarprasIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PrasaranaQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPrasaranasRelatedByKepemilikanSarprasId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this StatusKepemilikanSarpras is new, it will return
     * an empty collection; or if this StatusKepemilikanSarpras has previously
     * been saved, it will retrieve related PrasaranasRelatedByKepemilikanSarprasId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in StatusKepemilikanSarpras.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Prasarana[] List of Prasarana objects
     */
    public function getPrasaranasRelatedByKepemilikanSarprasIdJoinJenisPrasaranaRelatedByJenisPrasaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PrasaranaQuery::create(null, $criteria);
        $query->joinWith('JenisPrasaranaRelatedByJenisPrasaranaId', $join_behavior);

        return $this->getPrasaranasRelatedByKepemilikanSarprasId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this StatusKepemilikanSarpras is new, it will return
     * an empty collection; or if this StatusKepemilikanSarpras has previously
     * been saved, it will retrieve related PrasaranasRelatedByKepemilikanSarprasId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in StatusKepemilikanSarpras.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Prasarana[] List of Prasarana objects
     */
    public function getPrasaranasRelatedByKepemilikanSarprasIdJoinJenisPrasaranaRelatedByJenisPrasaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PrasaranaQuery::create(null, $criteria);
        $query->joinWith('JenisPrasaranaRelatedByJenisPrasaranaId', $join_behavior);

        return $this->getPrasaranasRelatedByKepemilikanSarprasId($query, $con);
    }

    /**
     * Clears out the collPrasaranasRelatedByKepemilikanSarprasId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return StatusKepemilikanSarpras The current object (for fluent API support)
     * @see        addPrasaranasRelatedByKepemilikanSarprasId()
     */
    public function clearPrasaranasRelatedByKepemilikanSarprasId()
    {
        $this->collPrasaranasRelatedByKepemilikanSarprasId = null; // important to set this to null since that means it is uninitialized
        $this->collPrasaranasRelatedByKepemilikanSarprasIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPrasaranasRelatedByKepemilikanSarprasId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPrasaranasRelatedByKepemilikanSarprasId($v = true)
    {
        $this->collPrasaranasRelatedByKepemilikanSarprasIdPartial = $v;
    }

    /**
     * Initializes the collPrasaranasRelatedByKepemilikanSarprasId collection.
     *
     * By default this just sets the collPrasaranasRelatedByKepemilikanSarprasId collection to an empty array (like clearcollPrasaranasRelatedByKepemilikanSarprasId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPrasaranasRelatedByKepemilikanSarprasId($overrideExisting = true)
    {
        if (null !== $this->collPrasaranasRelatedByKepemilikanSarprasId && !$overrideExisting) {
            return;
        }
        $this->collPrasaranasRelatedByKepemilikanSarprasId = new PropelObjectCollection();
        $this->collPrasaranasRelatedByKepemilikanSarprasId->setModel('Prasarana');
    }

    /**
     * Gets an array of Prasarana objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this StatusKepemilikanSarpras is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Prasarana[] List of Prasarana objects
     * @throws PropelException
     */
    public function getPrasaranasRelatedByKepemilikanSarprasId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPrasaranasRelatedByKepemilikanSarprasIdPartial && !$this->isNew();
        if (null === $this->collPrasaranasRelatedByKepemilikanSarprasId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPrasaranasRelatedByKepemilikanSarprasId) {
                // return empty collection
                $this->initPrasaranasRelatedByKepemilikanSarprasId();
            } else {
                $collPrasaranasRelatedByKepemilikanSarprasId = PrasaranaQuery::create(null, $criteria)
                    ->filterByStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPrasaranasRelatedByKepemilikanSarprasIdPartial && count($collPrasaranasRelatedByKepemilikanSarprasId)) {
                      $this->initPrasaranasRelatedByKepemilikanSarprasId(false);

                      foreach($collPrasaranasRelatedByKepemilikanSarprasId as $obj) {
                        if (false == $this->collPrasaranasRelatedByKepemilikanSarprasId->contains($obj)) {
                          $this->collPrasaranasRelatedByKepemilikanSarprasId->append($obj);
                        }
                      }

                      $this->collPrasaranasRelatedByKepemilikanSarprasIdPartial = true;
                    }

                    $collPrasaranasRelatedByKepemilikanSarprasId->getInternalIterator()->rewind();
                    return $collPrasaranasRelatedByKepemilikanSarprasId;
                }

                if($partial && $this->collPrasaranasRelatedByKepemilikanSarprasId) {
                    foreach($this->collPrasaranasRelatedByKepemilikanSarprasId as $obj) {
                        if($obj->isNew()) {
                            $collPrasaranasRelatedByKepemilikanSarprasId[] = $obj;
                        }
                    }
                }

                $this->collPrasaranasRelatedByKepemilikanSarprasId = $collPrasaranasRelatedByKepemilikanSarprasId;
                $this->collPrasaranasRelatedByKepemilikanSarprasIdPartial = false;
            }
        }

        return $this->collPrasaranasRelatedByKepemilikanSarprasId;
    }

    /**
     * Sets a collection of PrasaranaRelatedByKepemilikanSarprasId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $prasaranasRelatedByKepemilikanSarprasId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return StatusKepemilikanSarpras The current object (for fluent API support)
     */
    public function setPrasaranasRelatedByKepemilikanSarprasId(PropelCollection $prasaranasRelatedByKepemilikanSarprasId, PropelPDO $con = null)
    {
        $prasaranasRelatedByKepemilikanSarprasIdToDelete = $this->getPrasaranasRelatedByKepemilikanSarprasId(new Criteria(), $con)->diff($prasaranasRelatedByKepemilikanSarprasId);

        $this->prasaranasRelatedByKepemilikanSarprasIdScheduledForDeletion = unserialize(serialize($prasaranasRelatedByKepemilikanSarprasIdToDelete));

        foreach ($prasaranasRelatedByKepemilikanSarprasIdToDelete as $prasaranaRelatedByKepemilikanSarprasIdRemoved) {
            $prasaranaRelatedByKepemilikanSarprasIdRemoved->setStatusKepemilikanSarprasRelatedByKepemilikanSarprasId(null);
        }

        $this->collPrasaranasRelatedByKepemilikanSarprasId = null;
        foreach ($prasaranasRelatedByKepemilikanSarprasId as $prasaranaRelatedByKepemilikanSarprasId) {
            $this->addPrasaranaRelatedByKepemilikanSarprasId($prasaranaRelatedByKepemilikanSarprasId);
        }

        $this->collPrasaranasRelatedByKepemilikanSarprasId = $prasaranasRelatedByKepemilikanSarprasId;
        $this->collPrasaranasRelatedByKepemilikanSarprasIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Prasarana objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Prasarana objects.
     * @throws PropelException
     */
    public function countPrasaranasRelatedByKepemilikanSarprasId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPrasaranasRelatedByKepemilikanSarprasIdPartial && !$this->isNew();
        if (null === $this->collPrasaranasRelatedByKepemilikanSarprasId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPrasaranasRelatedByKepemilikanSarprasId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPrasaranasRelatedByKepemilikanSarprasId());
            }
            $query = PrasaranaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($this)
                ->count($con);
        }

        return count($this->collPrasaranasRelatedByKepemilikanSarprasId);
    }

    /**
     * Method called to associate a Prasarana object to this object
     * through the Prasarana foreign key attribute.
     *
     * @param    Prasarana $l Prasarana
     * @return StatusKepemilikanSarpras The current object (for fluent API support)
     */
    public function addPrasaranaRelatedByKepemilikanSarprasId(Prasarana $l)
    {
        if ($this->collPrasaranasRelatedByKepemilikanSarprasId === null) {
            $this->initPrasaranasRelatedByKepemilikanSarprasId();
            $this->collPrasaranasRelatedByKepemilikanSarprasIdPartial = true;
        }
        if (!in_array($l, $this->collPrasaranasRelatedByKepemilikanSarprasId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPrasaranaRelatedByKepemilikanSarprasId($l);
        }

        return $this;
    }

    /**
     * @param	PrasaranaRelatedByKepemilikanSarprasId $prasaranaRelatedByKepemilikanSarprasId The prasaranaRelatedByKepemilikanSarprasId object to add.
     */
    protected function doAddPrasaranaRelatedByKepemilikanSarprasId($prasaranaRelatedByKepemilikanSarprasId)
    {
        $this->collPrasaranasRelatedByKepemilikanSarprasId[]= $prasaranaRelatedByKepemilikanSarprasId;
        $prasaranaRelatedByKepemilikanSarprasId->setStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($this);
    }

    /**
     * @param	PrasaranaRelatedByKepemilikanSarprasId $prasaranaRelatedByKepemilikanSarprasId The prasaranaRelatedByKepemilikanSarprasId object to remove.
     * @return StatusKepemilikanSarpras The current object (for fluent API support)
     */
    public function removePrasaranaRelatedByKepemilikanSarprasId($prasaranaRelatedByKepemilikanSarprasId)
    {
        if ($this->getPrasaranasRelatedByKepemilikanSarprasId()->contains($prasaranaRelatedByKepemilikanSarprasId)) {
            $this->collPrasaranasRelatedByKepemilikanSarprasId->remove($this->collPrasaranasRelatedByKepemilikanSarprasId->search($prasaranaRelatedByKepemilikanSarprasId));
            if (null === $this->prasaranasRelatedByKepemilikanSarprasIdScheduledForDeletion) {
                $this->prasaranasRelatedByKepemilikanSarprasIdScheduledForDeletion = clone $this->collPrasaranasRelatedByKepemilikanSarprasId;
                $this->prasaranasRelatedByKepemilikanSarprasIdScheduledForDeletion->clear();
            }
            $this->prasaranasRelatedByKepemilikanSarprasIdScheduledForDeletion[]= clone $prasaranaRelatedByKepemilikanSarprasId;
            $prasaranaRelatedByKepemilikanSarprasId->setStatusKepemilikanSarprasRelatedByKepemilikanSarprasId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this StatusKepemilikanSarpras is new, it will return
     * an empty collection; or if this StatusKepemilikanSarpras has previously
     * been saved, it will retrieve related PrasaranasRelatedByKepemilikanSarprasId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in StatusKepemilikanSarpras.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Prasarana[] List of Prasarana objects
     */
    public function getPrasaranasRelatedByKepemilikanSarprasIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PrasaranaQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPrasaranasRelatedByKepemilikanSarprasId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this StatusKepemilikanSarpras is new, it will return
     * an empty collection; or if this StatusKepemilikanSarpras has previously
     * been saved, it will retrieve related PrasaranasRelatedByKepemilikanSarprasId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in StatusKepemilikanSarpras.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Prasarana[] List of Prasarana objects
     */
    public function getPrasaranasRelatedByKepemilikanSarprasIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PrasaranaQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPrasaranasRelatedByKepemilikanSarprasId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this StatusKepemilikanSarpras is new, it will return
     * an empty collection; or if this StatusKepemilikanSarpras has previously
     * been saved, it will retrieve related PrasaranasRelatedByKepemilikanSarprasId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in StatusKepemilikanSarpras.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Prasarana[] List of Prasarana objects
     */
    public function getPrasaranasRelatedByKepemilikanSarprasIdJoinJenisPrasaranaRelatedByJenisPrasaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PrasaranaQuery::create(null, $criteria);
        $query->joinWith('JenisPrasaranaRelatedByJenisPrasaranaId', $join_behavior);

        return $this->getPrasaranasRelatedByKepemilikanSarprasId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this StatusKepemilikanSarpras is new, it will return
     * an empty collection; or if this StatusKepemilikanSarpras has previously
     * been saved, it will retrieve related PrasaranasRelatedByKepemilikanSarprasId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in StatusKepemilikanSarpras.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Prasarana[] List of Prasarana objects
     */
    public function getPrasaranasRelatedByKepemilikanSarprasIdJoinJenisPrasaranaRelatedByJenisPrasaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PrasaranaQuery::create(null, $criteria);
        $query->joinWith('JenisPrasaranaRelatedByJenisPrasaranaId', $join_behavior);

        return $this->getPrasaranasRelatedByKepemilikanSarprasId($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->kepemilikan_sarpras_id = null;
        $this->nama = null;
        $this->create_date = null;
        $this->last_update = null;
        $this->expired_date = null;
        $this->last_sync = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collSaranasRelatedByKepemilikanSarprasId) {
                foreach ($this->collSaranasRelatedByKepemilikanSarprasId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSaranasRelatedByKepemilikanSarprasId) {
                foreach ($this->collSaranasRelatedByKepemilikanSarprasId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPrasaranasRelatedByKepemilikanSarprasId) {
                foreach ($this->collPrasaranasRelatedByKepemilikanSarprasId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPrasaranasRelatedByKepemilikanSarprasId) {
                foreach ($this->collPrasaranasRelatedByKepemilikanSarprasId as $o) {
                    $o->clearAllReferences($deep);
                }
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collSaranasRelatedByKepemilikanSarprasId instanceof PropelCollection) {
            $this->collSaranasRelatedByKepemilikanSarprasId->clearIterator();
        }
        $this->collSaranasRelatedByKepemilikanSarprasId = null;
        if ($this->collSaranasRelatedByKepemilikanSarprasId instanceof PropelCollection) {
            $this->collSaranasRelatedByKepemilikanSarprasId->clearIterator();
        }
        $this->collSaranasRelatedByKepemilikanSarprasId = null;
        if ($this->collPrasaranasRelatedByKepemilikanSarprasId instanceof PropelCollection) {
            $this->collPrasaranasRelatedByKepemilikanSarprasId->clearIterator();
        }
        $this->collPrasaranasRelatedByKepemilikanSarprasId = null;
        if ($this->collPrasaranasRelatedByKepemilikanSarprasId instanceof PropelCollection) {
            $this->collPrasaranasRelatedByKepemilikanSarprasId->clearIterator();
        }
        $this->collPrasaranasRelatedByKepemilikanSarprasId = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(StatusKepemilikanSarprasPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
