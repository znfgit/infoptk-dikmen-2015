<?php

namespace angulex\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelDateTime;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\LembagaNonSekolah;
use angulex\Model\LembagaNonSekolahQuery;
use angulex\Model\LogPengguna;
use angulex\Model\LogPenggunaQuery;
use angulex\Model\MstWilayah;
use angulex\Model\MstWilayahQuery;
use angulex\Model\Pengguna;
use angulex\Model\PenggunaPeer;
use angulex\Model\PenggunaQuery;
use angulex\Model\Peran;
use angulex\Model\PeranQuery;
use angulex\Model\SasaranSurvey;
use angulex\Model\SasaranSurveyQuery;
use angulex\Model\Sekolah;
use angulex\Model\SekolahQuery;

/**
 * Base class that represents a row from the 'pengguna' table.
 *
 * 
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BasePengguna extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'angulex\\Model\\PenggunaPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        PenggunaPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the pengguna_id field.
     * @var        string
     */
    protected $pengguna_id;

    /**
     * The value for the sekolah_id field.
     * @var        string
     */
    protected $sekolah_id;

    /**
     * The value for the lembaga_id field.
     * @var        string
     */
    protected $lembaga_id;

    /**
     * The value for the yayasan_id field.
     * @var        string
     */
    protected $yayasan_id;

    /**
     * The value for the la_id field.
     * @var        string
     */
    protected $la_id;

    /**
     * The value for the peran_id field.
     * @var        int
     */
    protected $peran_id;

    /**
     * The value for the username field.
     * @var        string
     */
    protected $username;

    /**
     * The value for the password field.
     * @var        string
     */
    protected $password;

    /**
     * The value for the nama field.
     * @var        string
     */
    protected $nama;

    /**
     * The value for the nip_nim field.
     * @var        string
     */
    protected $nip_nim;

    /**
     * The value for the jabatan_lembaga field.
     * @var        string
     */
    protected $jabatan_lembaga;

    /**
     * The value for the ym field.
     * @var        string
     */
    protected $ym;

    /**
     * The value for the skype field.
     * @var        string
     */
    protected $skype;

    /**
     * The value for the alamat field.
     * @var        string
     */
    protected $alamat;

    /**
     * The value for the kode_wilayah field.
     * @var        string
     */
    protected $kode_wilayah;

    /**
     * The value for the no_telepon field.
     * @var        string
     */
    protected $no_telepon;

    /**
     * The value for the no_hp field.
     * @var        string
     */
    protected $no_hp;

    /**
     * The value for the aktif field.
     * Note: this column has a database default value of: '((1))'
     * @var        string
     */
    protected $aktif;

    /**
     * The value for the last_update field.
     * @var        string
     */
    protected $last_update;

    /**
     * The value for the soft_delete field.
     * @var        string
     */
    protected $soft_delete;

    /**
     * The value for the last_sync field.
     * @var        string
     */
    protected $last_sync;

    /**
     * The value for the updater_id field.
     * @var        string
     */
    protected $updater_id;

    /**
     * @var        LembagaNonSekolah
     */
    protected $aLembagaNonSekolahRelatedByLembagaId;

    /**
     * @var        LembagaNonSekolah
     */
    protected $aLembagaNonSekolahRelatedByLembagaId;

    /**
     * @var        Sekolah
     */
    protected $aSekolahRelatedBySekolahId;

    /**
     * @var        Sekolah
     */
    protected $aSekolahRelatedBySekolahId;

    /**
     * @var        MstWilayah
     */
    protected $aMstWilayahRelatedByKodeWilayah;

    /**
     * @var        MstWilayah
     */
    protected $aMstWilayahRelatedByKodeWilayah;

    /**
     * @var        Peran
     */
    protected $aPeranRelatedByPeranId;

    /**
     * @var        Peran
     */
    protected $aPeranRelatedByPeranId;

    /**
     * @var        PropelObjectCollection|LogPengguna[] Collection to store aggregation of LogPengguna objects.
     */
    protected $collLogPenggunasRelatedByPenggunaId;
    protected $collLogPenggunasRelatedByPenggunaIdPartial;

    /**
     * @var        PropelObjectCollection|LogPengguna[] Collection to store aggregation of LogPengguna objects.
     */
    protected $collLogPenggunasRelatedByPenggunaId;
    protected $collLogPenggunasRelatedByPenggunaIdPartial;

    /**
     * @var        PropelObjectCollection|SasaranSurvey[] Collection to store aggregation of SasaranSurvey objects.
     */
    protected $collSasaranSurveysRelatedByPenggunaId;
    protected $collSasaranSurveysRelatedByPenggunaIdPartial;

    /**
     * @var        PropelObjectCollection|SasaranSurvey[] Collection to store aggregation of SasaranSurvey objects.
     */
    protected $collSasaranSurveysRelatedByPenggunaId;
    protected $collSasaranSurveysRelatedByPenggunaIdPartial;

    /**
     * @var        PropelObjectCollection|LembagaNonSekolah[] Collection to store aggregation of LembagaNonSekolah objects.
     */
    protected $collLembagaNonSekolahsRelatedByPenggunaId;
    protected $collLembagaNonSekolahsRelatedByPenggunaIdPartial;

    /**
     * @var        PropelObjectCollection|LembagaNonSekolah[] Collection to store aggregation of LembagaNonSekolah objects.
     */
    protected $collLembagaNonSekolahsRelatedByPenggunaId;
    protected $collLembagaNonSekolahsRelatedByPenggunaIdPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $logPenggunasRelatedByPenggunaIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $logPenggunasRelatedByPenggunaIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $sasaranSurveysRelatedByPenggunaIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $sasaranSurveysRelatedByPenggunaIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $lembagaNonSekolahsRelatedByPenggunaIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $lembagaNonSekolahsRelatedByPenggunaIdScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->aktif = '((1))';
    }

    /**
     * Initializes internal state of BasePengguna object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [pengguna_id] column value.
     * 
     * @return string
     */
    public function getPenggunaId()
    {
        return $this->pengguna_id;
    }

    /**
     * Get the [sekolah_id] column value.
     * 
     * @return string
     */
    public function getSekolahId()
    {
        return $this->sekolah_id;
    }

    /**
     * Get the [lembaga_id] column value.
     * 
     * @return string
     */
    public function getLembagaId()
    {
        return $this->lembaga_id;
    }

    /**
     * Get the [yayasan_id] column value.
     * 
     * @return string
     */
    public function getYayasanId()
    {
        return $this->yayasan_id;
    }

    /**
     * Get the [la_id] column value.
     * 
     * @return string
     */
    public function getLaId()
    {
        return $this->la_id;
    }

    /**
     * Get the [peran_id] column value.
     * 
     * @return int
     */
    public function getPeranId()
    {
        return $this->peran_id;
    }

    /**
     * Get the [username] column value.
     * 
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Get the [password] column value.
     * 
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Get the [nama] column value.
     * 
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Get the [nip_nim] column value.
     * 
     * @return string
     */
    public function getNipNim()
    {
        return $this->nip_nim;
    }

    /**
     * Get the [jabatan_lembaga] column value.
     * 
     * @return string
     */
    public function getJabatanLembaga()
    {
        return $this->jabatan_lembaga;
    }

    /**
     * Get the [ym] column value.
     * 
     * @return string
     */
    public function getYm()
    {
        return $this->ym;
    }

    /**
     * Get the [skype] column value.
     * 
     * @return string
     */
    public function getSkype()
    {
        return $this->skype;
    }

    /**
     * Get the [alamat] column value.
     * 
     * @return string
     */
    public function getAlamat()
    {
        return $this->alamat;
    }

    /**
     * Get the [kode_wilayah] column value.
     * 
     * @return string
     */
    public function getKodeWilayah()
    {
        return $this->kode_wilayah;
    }

    /**
     * Get the [no_telepon] column value.
     * 
     * @return string
     */
    public function getNoTelepon()
    {
        return $this->no_telepon;
    }

    /**
     * Get the [no_hp] column value.
     * 
     * @return string
     */
    public function getNoHp()
    {
        return $this->no_hp;
    }

    /**
     * Get the [aktif] column value.
     * 
     * @return string
     */
    public function getAktif()
    {
        return $this->aktif;
    }

    /**
     * Get the [optionally formatted] temporal [last_update] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastUpdate($format = 'Y-m-d H:i:s')
    {
        if ($this->last_update === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_update);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_update, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [soft_delete] column value.
     * 
     * @return string
     */
    public function getSoftDelete()
    {
        return $this->soft_delete;
    }

    /**
     * Get the [optionally formatted] temporal [last_sync] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastSync($format = 'Y-m-d H:i:s')
    {
        if ($this->last_sync === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_sync);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_sync, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [updater_id] column value.
     * 
     * @return string
     */
    public function getUpdaterId()
    {
        return $this->updater_id;
    }

    /**
     * Set the value of [pengguna_id] column.
     * 
     * @param string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setPenggunaId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->pengguna_id !== $v) {
            $this->pengguna_id = $v;
            $this->modifiedColumns[] = PenggunaPeer::PENGGUNA_ID;
        }


        return $this;
    } // setPenggunaId()

    /**
     * Set the value of [sekolah_id] column.
     * 
     * @param string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setSekolahId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->sekolah_id !== $v) {
            $this->sekolah_id = $v;
            $this->modifiedColumns[] = PenggunaPeer::SEKOLAH_ID;
        }

        if ($this->aSekolahRelatedBySekolahId !== null && $this->aSekolahRelatedBySekolahId->getSekolahId() !== $v) {
            $this->aSekolahRelatedBySekolahId = null;
        }

        if ($this->aSekolahRelatedBySekolahId !== null && $this->aSekolahRelatedBySekolahId->getSekolahId() !== $v) {
            $this->aSekolahRelatedBySekolahId = null;
        }


        return $this;
    } // setSekolahId()

    /**
     * Set the value of [lembaga_id] column.
     * 
     * @param string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setLembagaId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->lembaga_id !== $v) {
            $this->lembaga_id = $v;
            $this->modifiedColumns[] = PenggunaPeer::LEMBAGA_ID;
        }

        if ($this->aLembagaNonSekolahRelatedByLembagaId !== null && $this->aLembagaNonSekolahRelatedByLembagaId->getLembagaId() !== $v) {
            $this->aLembagaNonSekolahRelatedByLembagaId = null;
        }

        if ($this->aLembagaNonSekolahRelatedByLembagaId !== null && $this->aLembagaNonSekolahRelatedByLembagaId->getLembagaId() !== $v) {
            $this->aLembagaNonSekolahRelatedByLembagaId = null;
        }


        return $this;
    } // setLembagaId()

    /**
     * Set the value of [yayasan_id] column.
     * 
     * @param string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setYayasanId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->yayasan_id !== $v) {
            $this->yayasan_id = $v;
            $this->modifiedColumns[] = PenggunaPeer::YAYASAN_ID;
        }


        return $this;
    } // setYayasanId()

    /**
     * Set the value of [la_id] column.
     * 
     * @param string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setLaId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->la_id !== $v) {
            $this->la_id = $v;
            $this->modifiedColumns[] = PenggunaPeer::LA_ID;
        }


        return $this;
    } // setLaId()

    /**
     * Set the value of [peran_id] column.
     * 
     * @param int $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setPeranId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->peran_id !== $v) {
            $this->peran_id = $v;
            $this->modifiedColumns[] = PenggunaPeer::PERAN_ID;
        }

        if ($this->aPeranRelatedByPeranId !== null && $this->aPeranRelatedByPeranId->getPeranId() !== $v) {
            $this->aPeranRelatedByPeranId = null;
        }

        if ($this->aPeranRelatedByPeranId !== null && $this->aPeranRelatedByPeranId->getPeranId() !== $v) {
            $this->aPeranRelatedByPeranId = null;
        }


        return $this;
    } // setPeranId()

    /**
     * Set the value of [username] column.
     * 
     * @param string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setUsername($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->username !== $v) {
            $this->username = $v;
            $this->modifiedColumns[] = PenggunaPeer::USERNAME;
        }


        return $this;
    } // setUsername()

    /**
     * Set the value of [password] column.
     * 
     * @param string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setPassword($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->password !== $v) {
            $this->password = $v;
            $this->modifiedColumns[] = PenggunaPeer::PASSWORD;
        }


        return $this;
    } // setPassword()

    /**
     * Set the value of [nama] column.
     * 
     * @param string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setNama($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nama !== $v) {
            $this->nama = $v;
            $this->modifiedColumns[] = PenggunaPeer::NAMA;
        }


        return $this;
    } // setNama()

    /**
     * Set the value of [nip_nim] column.
     * 
     * @param string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setNipNim($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nip_nim !== $v) {
            $this->nip_nim = $v;
            $this->modifiedColumns[] = PenggunaPeer::NIP_NIM;
        }


        return $this;
    } // setNipNim()

    /**
     * Set the value of [jabatan_lembaga] column.
     * 
     * @param string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setJabatanLembaga($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->jabatan_lembaga !== $v) {
            $this->jabatan_lembaga = $v;
            $this->modifiedColumns[] = PenggunaPeer::JABATAN_LEMBAGA;
        }


        return $this;
    } // setJabatanLembaga()

    /**
     * Set the value of [ym] column.
     * 
     * @param string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setYm($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->ym !== $v) {
            $this->ym = $v;
            $this->modifiedColumns[] = PenggunaPeer::YM;
        }


        return $this;
    } // setYm()

    /**
     * Set the value of [skype] column.
     * 
     * @param string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setSkype($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->skype !== $v) {
            $this->skype = $v;
            $this->modifiedColumns[] = PenggunaPeer::SKYPE;
        }


        return $this;
    } // setSkype()

    /**
     * Set the value of [alamat] column.
     * 
     * @param string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setAlamat($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->alamat !== $v) {
            $this->alamat = $v;
            $this->modifiedColumns[] = PenggunaPeer::ALAMAT;
        }


        return $this;
    } // setAlamat()

    /**
     * Set the value of [kode_wilayah] column.
     * 
     * @param string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setKodeWilayah($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->kode_wilayah !== $v) {
            $this->kode_wilayah = $v;
            $this->modifiedColumns[] = PenggunaPeer::KODE_WILAYAH;
        }

        if ($this->aMstWilayahRelatedByKodeWilayah !== null && $this->aMstWilayahRelatedByKodeWilayah->getKodeWilayah() !== $v) {
            $this->aMstWilayahRelatedByKodeWilayah = null;
        }

        if ($this->aMstWilayahRelatedByKodeWilayah !== null && $this->aMstWilayahRelatedByKodeWilayah->getKodeWilayah() !== $v) {
            $this->aMstWilayahRelatedByKodeWilayah = null;
        }


        return $this;
    } // setKodeWilayah()

    /**
     * Set the value of [no_telepon] column.
     * 
     * @param string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setNoTelepon($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->no_telepon !== $v) {
            $this->no_telepon = $v;
            $this->modifiedColumns[] = PenggunaPeer::NO_TELEPON;
        }


        return $this;
    } // setNoTelepon()

    /**
     * Set the value of [no_hp] column.
     * 
     * @param string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setNoHp($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->no_hp !== $v) {
            $this->no_hp = $v;
            $this->modifiedColumns[] = PenggunaPeer::NO_HP;
        }


        return $this;
    } // setNoHp()

    /**
     * Set the value of [aktif] column.
     * 
     * @param string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setAktif($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->aktif !== $v) {
            $this->aktif = $v;
            $this->modifiedColumns[] = PenggunaPeer::AKTIF;
        }


        return $this;
    } // setAktif()

    /**
     * Sets the value of [last_update] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Pengguna The current object (for fluent API support)
     */
    public function setLastUpdate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_update !== null || $dt !== null) {
            $currentDateAsString = ($this->last_update !== null && $tmpDt = new DateTime($this->last_update)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_update = $newDateAsString;
                $this->modifiedColumns[] = PenggunaPeer::LAST_UPDATE;
            }
        } // if either are not null


        return $this;
    } // setLastUpdate()

    /**
     * Set the value of [soft_delete] column.
     * 
     * @param string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setSoftDelete($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->soft_delete !== $v) {
            $this->soft_delete = $v;
            $this->modifiedColumns[] = PenggunaPeer::SOFT_DELETE;
        }


        return $this;
    } // setSoftDelete()

    /**
     * Sets the value of [last_sync] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Pengguna The current object (for fluent API support)
     */
    public function setLastSync($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_sync !== null || $dt !== null) {
            $currentDateAsString = ($this->last_sync !== null && $tmpDt = new DateTime($this->last_sync)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_sync = $newDateAsString;
                $this->modifiedColumns[] = PenggunaPeer::LAST_SYNC;
            }
        } // if either are not null


        return $this;
    } // setLastSync()

    /**
     * Set the value of [updater_id] column.
     * 
     * @param string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setUpdaterId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->updater_id !== $v) {
            $this->updater_id = $v;
            $this->modifiedColumns[] = PenggunaPeer::UPDATER_ID;
        }


        return $this;
    } // setUpdaterId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->aktif !== '((1))') {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->pengguna_id = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
            $this->sekolah_id = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->lembaga_id = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->yayasan_id = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->la_id = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->peran_id = ($row[$startcol + 5] !== null) ? (int) $row[$startcol + 5] : null;
            $this->username = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->password = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->nama = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->nip_nim = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->jabatan_lembaga = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->ym = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->skype = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->alamat = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->kode_wilayah = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
            $this->no_telepon = ($row[$startcol + 15] !== null) ? (string) $row[$startcol + 15] : null;
            $this->no_hp = ($row[$startcol + 16] !== null) ? (string) $row[$startcol + 16] : null;
            $this->aktif = ($row[$startcol + 17] !== null) ? (string) $row[$startcol + 17] : null;
            $this->last_update = ($row[$startcol + 18] !== null) ? (string) $row[$startcol + 18] : null;
            $this->soft_delete = ($row[$startcol + 19] !== null) ? (string) $row[$startcol + 19] : null;
            $this->last_sync = ($row[$startcol + 20] !== null) ? (string) $row[$startcol + 20] : null;
            $this->updater_id = ($row[$startcol + 21] !== null) ? (string) $row[$startcol + 21] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 22; // 22 = PenggunaPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Pengguna object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aSekolahRelatedBySekolahId !== null && $this->sekolah_id !== $this->aSekolahRelatedBySekolahId->getSekolahId()) {
            $this->aSekolahRelatedBySekolahId = null;
        }
        if ($this->aSekolahRelatedBySekolahId !== null && $this->sekolah_id !== $this->aSekolahRelatedBySekolahId->getSekolahId()) {
            $this->aSekolahRelatedBySekolahId = null;
        }
        if ($this->aLembagaNonSekolahRelatedByLembagaId !== null && $this->lembaga_id !== $this->aLembagaNonSekolahRelatedByLembagaId->getLembagaId()) {
            $this->aLembagaNonSekolahRelatedByLembagaId = null;
        }
        if ($this->aLembagaNonSekolahRelatedByLembagaId !== null && $this->lembaga_id !== $this->aLembagaNonSekolahRelatedByLembagaId->getLembagaId()) {
            $this->aLembagaNonSekolahRelatedByLembagaId = null;
        }
        if ($this->aPeranRelatedByPeranId !== null && $this->peran_id !== $this->aPeranRelatedByPeranId->getPeranId()) {
            $this->aPeranRelatedByPeranId = null;
        }
        if ($this->aPeranRelatedByPeranId !== null && $this->peran_id !== $this->aPeranRelatedByPeranId->getPeranId()) {
            $this->aPeranRelatedByPeranId = null;
        }
        if ($this->aMstWilayahRelatedByKodeWilayah !== null && $this->kode_wilayah !== $this->aMstWilayahRelatedByKodeWilayah->getKodeWilayah()) {
            $this->aMstWilayahRelatedByKodeWilayah = null;
        }
        if ($this->aMstWilayahRelatedByKodeWilayah !== null && $this->kode_wilayah !== $this->aMstWilayahRelatedByKodeWilayah->getKodeWilayah()) {
            $this->aMstWilayahRelatedByKodeWilayah = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PenggunaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = PenggunaPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aLembagaNonSekolahRelatedByLembagaId = null;
            $this->aLembagaNonSekolahRelatedByLembagaId = null;
            $this->aSekolahRelatedBySekolahId = null;
            $this->aSekolahRelatedBySekolahId = null;
            $this->aMstWilayahRelatedByKodeWilayah = null;
            $this->aMstWilayahRelatedByKodeWilayah = null;
            $this->aPeranRelatedByPeranId = null;
            $this->aPeranRelatedByPeranId = null;
            $this->collLogPenggunasRelatedByPenggunaId = null;

            $this->collLogPenggunasRelatedByPenggunaId = null;

            $this->collSasaranSurveysRelatedByPenggunaId = null;

            $this->collSasaranSurveysRelatedByPenggunaId = null;

            $this->collLembagaNonSekolahsRelatedByPenggunaId = null;

            $this->collLembagaNonSekolahsRelatedByPenggunaId = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PenggunaPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = PenggunaQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PenggunaPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                PenggunaPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aLembagaNonSekolahRelatedByLembagaId !== null) {
                if ($this->aLembagaNonSekolahRelatedByLembagaId->isModified() || $this->aLembagaNonSekolahRelatedByLembagaId->isNew()) {
                    $affectedRows += $this->aLembagaNonSekolahRelatedByLembagaId->save($con);
                }
                $this->setLembagaNonSekolahRelatedByLembagaId($this->aLembagaNonSekolahRelatedByLembagaId);
            }

            if ($this->aLembagaNonSekolahRelatedByLembagaId !== null) {
                if ($this->aLembagaNonSekolahRelatedByLembagaId->isModified() || $this->aLembagaNonSekolahRelatedByLembagaId->isNew()) {
                    $affectedRows += $this->aLembagaNonSekolahRelatedByLembagaId->save($con);
                }
                $this->setLembagaNonSekolahRelatedByLembagaId($this->aLembagaNonSekolahRelatedByLembagaId);
            }

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if ($this->aSekolahRelatedBySekolahId->isModified() || $this->aSekolahRelatedBySekolahId->isNew()) {
                    $affectedRows += $this->aSekolahRelatedBySekolahId->save($con);
                }
                $this->setSekolahRelatedBySekolahId($this->aSekolahRelatedBySekolahId);
            }

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if ($this->aSekolahRelatedBySekolahId->isModified() || $this->aSekolahRelatedBySekolahId->isNew()) {
                    $affectedRows += $this->aSekolahRelatedBySekolahId->save($con);
                }
                $this->setSekolahRelatedBySekolahId($this->aSekolahRelatedBySekolahId);
            }

            if ($this->aMstWilayahRelatedByKodeWilayah !== null) {
                if ($this->aMstWilayahRelatedByKodeWilayah->isModified() || $this->aMstWilayahRelatedByKodeWilayah->isNew()) {
                    $affectedRows += $this->aMstWilayahRelatedByKodeWilayah->save($con);
                }
                $this->setMstWilayahRelatedByKodeWilayah($this->aMstWilayahRelatedByKodeWilayah);
            }

            if ($this->aMstWilayahRelatedByKodeWilayah !== null) {
                if ($this->aMstWilayahRelatedByKodeWilayah->isModified() || $this->aMstWilayahRelatedByKodeWilayah->isNew()) {
                    $affectedRows += $this->aMstWilayahRelatedByKodeWilayah->save($con);
                }
                $this->setMstWilayahRelatedByKodeWilayah($this->aMstWilayahRelatedByKodeWilayah);
            }

            if ($this->aPeranRelatedByPeranId !== null) {
                if ($this->aPeranRelatedByPeranId->isModified() || $this->aPeranRelatedByPeranId->isNew()) {
                    $affectedRows += $this->aPeranRelatedByPeranId->save($con);
                }
                $this->setPeranRelatedByPeranId($this->aPeranRelatedByPeranId);
            }

            if ($this->aPeranRelatedByPeranId !== null) {
                if ($this->aPeranRelatedByPeranId->isModified() || $this->aPeranRelatedByPeranId->isNew()) {
                    $affectedRows += $this->aPeranRelatedByPeranId->save($con);
                }
                $this->setPeranRelatedByPeranId($this->aPeranRelatedByPeranId);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->logPenggunasRelatedByPenggunaIdScheduledForDeletion !== null) {
                if (!$this->logPenggunasRelatedByPenggunaIdScheduledForDeletion->isEmpty()) {
                    LogPenggunaQuery::create()
                        ->filterByPrimaryKeys($this->logPenggunasRelatedByPenggunaIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->logPenggunasRelatedByPenggunaIdScheduledForDeletion = null;
                }
            }

            if ($this->collLogPenggunasRelatedByPenggunaId !== null) {
                foreach ($this->collLogPenggunasRelatedByPenggunaId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->logPenggunasRelatedByPenggunaIdScheduledForDeletion !== null) {
                if (!$this->logPenggunasRelatedByPenggunaIdScheduledForDeletion->isEmpty()) {
                    LogPenggunaQuery::create()
                        ->filterByPrimaryKeys($this->logPenggunasRelatedByPenggunaIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->logPenggunasRelatedByPenggunaIdScheduledForDeletion = null;
                }
            }

            if ($this->collLogPenggunasRelatedByPenggunaId !== null) {
                foreach ($this->collLogPenggunasRelatedByPenggunaId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->sasaranSurveysRelatedByPenggunaIdScheduledForDeletion !== null) {
                if (!$this->sasaranSurveysRelatedByPenggunaIdScheduledForDeletion->isEmpty()) {
                    SasaranSurveyQuery::create()
                        ->filterByPrimaryKeys($this->sasaranSurveysRelatedByPenggunaIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->sasaranSurveysRelatedByPenggunaIdScheduledForDeletion = null;
                }
            }

            if ($this->collSasaranSurveysRelatedByPenggunaId !== null) {
                foreach ($this->collSasaranSurveysRelatedByPenggunaId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->sasaranSurveysRelatedByPenggunaIdScheduledForDeletion !== null) {
                if (!$this->sasaranSurveysRelatedByPenggunaIdScheduledForDeletion->isEmpty()) {
                    SasaranSurveyQuery::create()
                        ->filterByPrimaryKeys($this->sasaranSurveysRelatedByPenggunaIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->sasaranSurveysRelatedByPenggunaIdScheduledForDeletion = null;
                }
            }

            if ($this->collSasaranSurveysRelatedByPenggunaId !== null) {
                foreach ($this->collSasaranSurveysRelatedByPenggunaId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->lembagaNonSekolahsRelatedByPenggunaIdScheduledForDeletion !== null) {
                if (!$this->lembagaNonSekolahsRelatedByPenggunaIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->lembagaNonSekolahsRelatedByPenggunaIdScheduledForDeletion as $lembagaNonSekolahRelatedByPenggunaId) {
                        // need to save related object because we set the relation to null
                        $lembagaNonSekolahRelatedByPenggunaId->save($con);
                    }
                    $this->lembagaNonSekolahsRelatedByPenggunaIdScheduledForDeletion = null;
                }
            }

            if ($this->collLembagaNonSekolahsRelatedByPenggunaId !== null) {
                foreach ($this->collLembagaNonSekolahsRelatedByPenggunaId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->lembagaNonSekolahsRelatedByPenggunaIdScheduledForDeletion !== null) {
                if (!$this->lembagaNonSekolahsRelatedByPenggunaIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->lembagaNonSekolahsRelatedByPenggunaIdScheduledForDeletion as $lembagaNonSekolahRelatedByPenggunaId) {
                        // need to save related object because we set the relation to null
                        $lembagaNonSekolahRelatedByPenggunaId->save($con);
                    }
                    $this->lembagaNonSekolahsRelatedByPenggunaIdScheduledForDeletion = null;
                }
            }

            if ($this->collLembagaNonSekolahsRelatedByPenggunaId !== null) {
                foreach ($this->collLembagaNonSekolahsRelatedByPenggunaId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $criteria = $this->buildCriteria();
        $pk = BasePeer::doInsert($criteria, $con);
        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aLembagaNonSekolahRelatedByLembagaId !== null) {
                if (!$this->aLembagaNonSekolahRelatedByLembagaId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aLembagaNonSekolahRelatedByLembagaId->getValidationFailures());
                }
            }

            if ($this->aLembagaNonSekolahRelatedByLembagaId !== null) {
                if (!$this->aLembagaNonSekolahRelatedByLembagaId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aLembagaNonSekolahRelatedByLembagaId->getValidationFailures());
                }
            }

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if (!$this->aSekolahRelatedBySekolahId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSekolahRelatedBySekolahId->getValidationFailures());
                }
            }

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if (!$this->aSekolahRelatedBySekolahId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSekolahRelatedBySekolahId->getValidationFailures());
                }
            }

            if ($this->aMstWilayahRelatedByKodeWilayah !== null) {
                if (!$this->aMstWilayahRelatedByKodeWilayah->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aMstWilayahRelatedByKodeWilayah->getValidationFailures());
                }
            }

            if ($this->aMstWilayahRelatedByKodeWilayah !== null) {
                if (!$this->aMstWilayahRelatedByKodeWilayah->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aMstWilayahRelatedByKodeWilayah->getValidationFailures());
                }
            }

            if ($this->aPeranRelatedByPeranId !== null) {
                if (!$this->aPeranRelatedByPeranId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPeranRelatedByPeranId->getValidationFailures());
                }
            }

            if ($this->aPeranRelatedByPeranId !== null) {
                if (!$this->aPeranRelatedByPeranId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPeranRelatedByPeranId->getValidationFailures());
                }
            }


            if (($retval = PenggunaPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collLogPenggunasRelatedByPenggunaId !== null) {
                    foreach ($this->collLogPenggunasRelatedByPenggunaId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collLogPenggunasRelatedByPenggunaId !== null) {
                    foreach ($this->collLogPenggunasRelatedByPenggunaId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSasaranSurveysRelatedByPenggunaId !== null) {
                    foreach ($this->collSasaranSurveysRelatedByPenggunaId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSasaranSurveysRelatedByPenggunaId !== null) {
                    foreach ($this->collSasaranSurveysRelatedByPenggunaId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collLembagaNonSekolahsRelatedByPenggunaId !== null) {
                    foreach ($this->collLembagaNonSekolahsRelatedByPenggunaId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collLembagaNonSekolahsRelatedByPenggunaId !== null) {
                    foreach ($this->collLembagaNonSekolahsRelatedByPenggunaId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = PenggunaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getPenggunaId();
                break;
            case 1:
                return $this->getSekolahId();
                break;
            case 2:
                return $this->getLembagaId();
                break;
            case 3:
                return $this->getYayasanId();
                break;
            case 4:
                return $this->getLaId();
                break;
            case 5:
                return $this->getPeranId();
                break;
            case 6:
                return $this->getUsername();
                break;
            case 7:
                return $this->getPassword();
                break;
            case 8:
                return $this->getNama();
                break;
            case 9:
                return $this->getNipNim();
                break;
            case 10:
                return $this->getJabatanLembaga();
                break;
            case 11:
                return $this->getYm();
                break;
            case 12:
                return $this->getSkype();
                break;
            case 13:
                return $this->getAlamat();
                break;
            case 14:
                return $this->getKodeWilayah();
                break;
            case 15:
                return $this->getNoTelepon();
                break;
            case 16:
                return $this->getNoHp();
                break;
            case 17:
                return $this->getAktif();
                break;
            case 18:
                return $this->getLastUpdate();
                break;
            case 19:
                return $this->getSoftDelete();
                break;
            case 20:
                return $this->getLastSync();
                break;
            case 21:
                return $this->getUpdaterId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Pengguna'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Pengguna'][$this->getPrimaryKey()] = true;
        $keys = PenggunaPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getPenggunaId(),
            $keys[1] => $this->getSekolahId(),
            $keys[2] => $this->getLembagaId(),
            $keys[3] => $this->getYayasanId(),
            $keys[4] => $this->getLaId(),
            $keys[5] => $this->getPeranId(),
            $keys[6] => $this->getUsername(),
            $keys[7] => $this->getPassword(),
            $keys[8] => $this->getNama(),
            $keys[9] => $this->getNipNim(),
            $keys[10] => $this->getJabatanLembaga(),
            $keys[11] => $this->getYm(),
            $keys[12] => $this->getSkype(),
            $keys[13] => $this->getAlamat(),
            $keys[14] => $this->getKodeWilayah(),
            $keys[15] => $this->getNoTelepon(),
            $keys[16] => $this->getNoHp(),
            $keys[17] => $this->getAktif(),
            $keys[18] => $this->getLastUpdate(),
            $keys[19] => $this->getSoftDelete(),
            $keys[20] => $this->getLastSync(),
            $keys[21] => $this->getUpdaterId(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aLembagaNonSekolahRelatedByLembagaId) {
                $result['LembagaNonSekolahRelatedByLembagaId'] = $this->aLembagaNonSekolahRelatedByLembagaId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aLembagaNonSekolahRelatedByLembagaId) {
                $result['LembagaNonSekolahRelatedByLembagaId'] = $this->aLembagaNonSekolahRelatedByLembagaId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSekolahRelatedBySekolahId) {
                $result['SekolahRelatedBySekolahId'] = $this->aSekolahRelatedBySekolahId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSekolahRelatedBySekolahId) {
                $result['SekolahRelatedBySekolahId'] = $this->aSekolahRelatedBySekolahId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aMstWilayahRelatedByKodeWilayah) {
                $result['MstWilayahRelatedByKodeWilayah'] = $this->aMstWilayahRelatedByKodeWilayah->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aMstWilayahRelatedByKodeWilayah) {
                $result['MstWilayahRelatedByKodeWilayah'] = $this->aMstWilayahRelatedByKodeWilayah->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPeranRelatedByPeranId) {
                $result['PeranRelatedByPeranId'] = $this->aPeranRelatedByPeranId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPeranRelatedByPeranId) {
                $result['PeranRelatedByPeranId'] = $this->aPeranRelatedByPeranId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collLogPenggunasRelatedByPenggunaId) {
                $result['LogPenggunasRelatedByPenggunaId'] = $this->collLogPenggunasRelatedByPenggunaId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collLogPenggunasRelatedByPenggunaId) {
                $result['LogPenggunasRelatedByPenggunaId'] = $this->collLogPenggunasRelatedByPenggunaId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSasaranSurveysRelatedByPenggunaId) {
                $result['SasaranSurveysRelatedByPenggunaId'] = $this->collSasaranSurveysRelatedByPenggunaId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSasaranSurveysRelatedByPenggunaId) {
                $result['SasaranSurveysRelatedByPenggunaId'] = $this->collSasaranSurveysRelatedByPenggunaId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collLembagaNonSekolahsRelatedByPenggunaId) {
                $result['LembagaNonSekolahsRelatedByPenggunaId'] = $this->collLembagaNonSekolahsRelatedByPenggunaId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collLembagaNonSekolahsRelatedByPenggunaId) {
                $result['LembagaNonSekolahsRelatedByPenggunaId'] = $this->collLembagaNonSekolahsRelatedByPenggunaId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = PenggunaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setPenggunaId($value);
                break;
            case 1:
                $this->setSekolahId($value);
                break;
            case 2:
                $this->setLembagaId($value);
                break;
            case 3:
                $this->setYayasanId($value);
                break;
            case 4:
                $this->setLaId($value);
                break;
            case 5:
                $this->setPeranId($value);
                break;
            case 6:
                $this->setUsername($value);
                break;
            case 7:
                $this->setPassword($value);
                break;
            case 8:
                $this->setNama($value);
                break;
            case 9:
                $this->setNipNim($value);
                break;
            case 10:
                $this->setJabatanLembaga($value);
                break;
            case 11:
                $this->setYm($value);
                break;
            case 12:
                $this->setSkype($value);
                break;
            case 13:
                $this->setAlamat($value);
                break;
            case 14:
                $this->setKodeWilayah($value);
                break;
            case 15:
                $this->setNoTelepon($value);
                break;
            case 16:
                $this->setNoHp($value);
                break;
            case 17:
                $this->setAktif($value);
                break;
            case 18:
                $this->setLastUpdate($value);
                break;
            case 19:
                $this->setSoftDelete($value);
                break;
            case 20:
                $this->setLastSync($value);
                break;
            case 21:
                $this->setUpdaterId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = PenggunaPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setPenggunaId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setSekolahId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setLembagaId($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setYayasanId($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setLaId($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setPeranId($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setUsername($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setPassword($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setNama($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setNipNim($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setJabatanLembaga($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setYm($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setSkype($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setAlamat($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setKodeWilayah($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setNoTelepon($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setNoHp($arr[$keys[16]]);
        if (array_key_exists($keys[17], $arr)) $this->setAktif($arr[$keys[17]]);
        if (array_key_exists($keys[18], $arr)) $this->setLastUpdate($arr[$keys[18]]);
        if (array_key_exists($keys[19], $arr)) $this->setSoftDelete($arr[$keys[19]]);
        if (array_key_exists($keys[20], $arr)) $this->setLastSync($arr[$keys[20]]);
        if (array_key_exists($keys[21], $arr)) $this->setUpdaterId($arr[$keys[21]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(PenggunaPeer::DATABASE_NAME);

        if ($this->isColumnModified(PenggunaPeer::PENGGUNA_ID)) $criteria->add(PenggunaPeer::PENGGUNA_ID, $this->pengguna_id);
        if ($this->isColumnModified(PenggunaPeer::SEKOLAH_ID)) $criteria->add(PenggunaPeer::SEKOLAH_ID, $this->sekolah_id);
        if ($this->isColumnModified(PenggunaPeer::LEMBAGA_ID)) $criteria->add(PenggunaPeer::LEMBAGA_ID, $this->lembaga_id);
        if ($this->isColumnModified(PenggunaPeer::YAYASAN_ID)) $criteria->add(PenggunaPeer::YAYASAN_ID, $this->yayasan_id);
        if ($this->isColumnModified(PenggunaPeer::LA_ID)) $criteria->add(PenggunaPeer::LA_ID, $this->la_id);
        if ($this->isColumnModified(PenggunaPeer::PERAN_ID)) $criteria->add(PenggunaPeer::PERAN_ID, $this->peran_id);
        if ($this->isColumnModified(PenggunaPeer::USERNAME)) $criteria->add(PenggunaPeer::USERNAME, $this->username);
        if ($this->isColumnModified(PenggunaPeer::PASSWORD)) $criteria->add(PenggunaPeer::PASSWORD, $this->password);
        if ($this->isColumnModified(PenggunaPeer::NAMA)) $criteria->add(PenggunaPeer::NAMA, $this->nama);
        if ($this->isColumnModified(PenggunaPeer::NIP_NIM)) $criteria->add(PenggunaPeer::NIP_NIM, $this->nip_nim);
        if ($this->isColumnModified(PenggunaPeer::JABATAN_LEMBAGA)) $criteria->add(PenggunaPeer::JABATAN_LEMBAGA, $this->jabatan_lembaga);
        if ($this->isColumnModified(PenggunaPeer::YM)) $criteria->add(PenggunaPeer::YM, $this->ym);
        if ($this->isColumnModified(PenggunaPeer::SKYPE)) $criteria->add(PenggunaPeer::SKYPE, $this->skype);
        if ($this->isColumnModified(PenggunaPeer::ALAMAT)) $criteria->add(PenggunaPeer::ALAMAT, $this->alamat);
        if ($this->isColumnModified(PenggunaPeer::KODE_WILAYAH)) $criteria->add(PenggunaPeer::KODE_WILAYAH, $this->kode_wilayah);
        if ($this->isColumnModified(PenggunaPeer::NO_TELEPON)) $criteria->add(PenggunaPeer::NO_TELEPON, $this->no_telepon);
        if ($this->isColumnModified(PenggunaPeer::NO_HP)) $criteria->add(PenggunaPeer::NO_HP, $this->no_hp);
        if ($this->isColumnModified(PenggunaPeer::AKTIF)) $criteria->add(PenggunaPeer::AKTIF, $this->aktif);
        if ($this->isColumnModified(PenggunaPeer::LAST_UPDATE)) $criteria->add(PenggunaPeer::LAST_UPDATE, $this->last_update);
        if ($this->isColumnModified(PenggunaPeer::SOFT_DELETE)) $criteria->add(PenggunaPeer::SOFT_DELETE, $this->soft_delete);
        if ($this->isColumnModified(PenggunaPeer::LAST_SYNC)) $criteria->add(PenggunaPeer::LAST_SYNC, $this->last_sync);
        if ($this->isColumnModified(PenggunaPeer::UPDATER_ID)) $criteria->add(PenggunaPeer::UPDATER_ID, $this->updater_id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(PenggunaPeer::DATABASE_NAME);
        $criteria->add(PenggunaPeer::PENGGUNA_ID, $this->pengguna_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getPenggunaId();
    }

    /**
     * Generic method to set the primary key (pengguna_id column).
     *
     * @param  string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setPenggunaId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getPenggunaId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Pengguna (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setSekolahId($this->getSekolahId());
        $copyObj->setLembagaId($this->getLembagaId());
        $copyObj->setYayasanId($this->getYayasanId());
        $copyObj->setLaId($this->getLaId());
        $copyObj->setPeranId($this->getPeranId());
        $copyObj->setUsername($this->getUsername());
        $copyObj->setPassword($this->getPassword());
        $copyObj->setNama($this->getNama());
        $copyObj->setNipNim($this->getNipNim());
        $copyObj->setJabatanLembaga($this->getJabatanLembaga());
        $copyObj->setYm($this->getYm());
        $copyObj->setSkype($this->getSkype());
        $copyObj->setAlamat($this->getAlamat());
        $copyObj->setKodeWilayah($this->getKodeWilayah());
        $copyObj->setNoTelepon($this->getNoTelepon());
        $copyObj->setNoHp($this->getNoHp());
        $copyObj->setAktif($this->getAktif());
        $copyObj->setLastUpdate($this->getLastUpdate());
        $copyObj->setSoftDelete($this->getSoftDelete());
        $copyObj->setLastSync($this->getLastSync());
        $copyObj->setUpdaterId($this->getUpdaterId());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getLogPenggunasRelatedByPenggunaId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addLogPenggunaRelatedByPenggunaId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getLogPenggunasRelatedByPenggunaId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addLogPenggunaRelatedByPenggunaId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSasaranSurveysRelatedByPenggunaId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSasaranSurveyRelatedByPenggunaId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSasaranSurveysRelatedByPenggunaId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSasaranSurveyRelatedByPenggunaId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getLembagaNonSekolahsRelatedByPenggunaId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addLembagaNonSekolahRelatedByPenggunaId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getLembagaNonSekolahsRelatedByPenggunaId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addLembagaNonSekolahRelatedByPenggunaId($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setPenggunaId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Pengguna Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return PenggunaPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new PenggunaPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a LembagaNonSekolah object.
     *
     * @param             LembagaNonSekolah $v
     * @return Pengguna The current object (for fluent API support)
     * @throws PropelException
     */
    public function setLembagaNonSekolahRelatedByLembagaId(LembagaNonSekolah $v = null)
    {
        if ($v === null) {
            $this->setLembagaId(NULL);
        } else {
            $this->setLembagaId($v->getLembagaId());
        }

        $this->aLembagaNonSekolahRelatedByLembagaId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the LembagaNonSekolah object, it will not be re-added.
        if ($v !== null) {
            $v->addPenggunaRelatedByLembagaId($this);
        }


        return $this;
    }


    /**
     * Get the associated LembagaNonSekolah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return LembagaNonSekolah The associated LembagaNonSekolah object.
     * @throws PropelException
     */
    public function getLembagaNonSekolahRelatedByLembagaId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aLembagaNonSekolahRelatedByLembagaId === null && (($this->lembaga_id !== "" && $this->lembaga_id !== null)) && $doQuery) {
            $this->aLembagaNonSekolahRelatedByLembagaId = LembagaNonSekolahQuery::create()->findPk($this->lembaga_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aLembagaNonSekolahRelatedByLembagaId->addPenggunasRelatedByLembagaId($this);
             */
        }

        return $this->aLembagaNonSekolahRelatedByLembagaId;
    }

    /**
     * Declares an association between this object and a LembagaNonSekolah object.
     *
     * @param             LembagaNonSekolah $v
     * @return Pengguna The current object (for fluent API support)
     * @throws PropelException
     */
    public function setLembagaNonSekolahRelatedByLembagaId(LembagaNonSekolah $v = null)
    {
        if ($v === null) {
            $this->setLembagaId(NULL);
        } else {
            $this->setLembagaId($v->getLembagaId());
        }

        $this->aLembagaNonSekolahRelatedByLembagaId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the LembagaNonSekolah object, it will not be re-added.
        if ($v !== null) {
            $v->addPenggunaRelatedByLembagaId($this);
        }


        return $this;
    }


    /**
     * Get the associated LembagaNonSekolah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return LembagaNonSekolah The associated LembagaNonSekolah object.
     * @throws PropelException
     */
    public function getLembagaNonSekolahRelatedByLembagaId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aLembagaNonSekolahRelatedByLembagaId === null && (($this->lembaga_id !== "" && $this->lembaga_id !== null)) && $doQuery) {
            $this->aLembagaNonSekolahRelatedByLembagaId = LembagaNonSekolahQuery::create()->findPk($this->lembaga_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aLembagaNonSekolahRelatedByLembagaId->addPenggunasRelatedByLembagaId($this);
             */
        }

        return $this->aLembagaNonSekolahRelatedByLembagaId;
    }

    /**
     * Declares an association between this object and a Sekolah object.
     *
     * @param             Sekolah $v
     * @return Pengguna The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSekolahRelatedBySekolahId(Sekolah $v = null)
    {
        if ($v === null) {
            $this->setSekolahId(NULL);
        } else {
            $this->setSekolahId($v->getSekolahId());
        }

        $this->aSekolahRelatedBySekolahId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Sekolah object, it will not be re-added.
        if ($v !== null) {
            $v->addPenggunaRelatedBySekolahId($this);
        }


        return $this;
    }


    /**
     * Get the associated Sekolah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Sekolah The associated Sekolah object.
     * @throws PropelException
     */
    public function getSekolahRelatedBySekolahId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSekolahRelatedBySekolahId === null && (($this->sekolah_id !== "" && $this->sekolah_id !== null)) && $doQuery) {
            $this->aSekolahRelatedBySekolahId = SekolahQuery::create()->findPk($this->sekolah_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSekolahRelatedBySekolahId->addPenggunasRelatedBySekolahId($this);
             */
        }

        return $this->aSekolahRelatedBySekolahId;
    }

    /**
     * Declares an association between this object and a Sekolah object.
     *
     * @param             Sekolah $v
     * @return Pengguna The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSekolahRelatedBySekolahId(Sekolah $v = null)
    {
        if ($v === null) {
            $this->setSekolahId(NULL);
        } else {
            $this->setSekolahId($v->getSekolahId());
        }

        $this->aSekolahRelatedBySekolahId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Sekolah object, it will not be re-added.
        if ($v !== null) {
            $v->addPenggunaRelatedBySekolahId($this);
        }


        return $this;
    }


    /**
     * Get the associated Sekolah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Sekolah The associated Sekolah object.
     * @throws PropelException
     */
    public function getSekolahRelatedBySekolahId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSekolahRelatedBySekolahId === null && (($this->sekolah_id !== "" && $this->sekolah_id !== null)) && $doQuery) {
            $this->aSekolahRelatedBySekolahId = SekolahQuery::create()->findPk($this->sekolah_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSekolahRelatedBySekolahId->addPenggunasRelatedBySekolahId($this);
             */
        }

        return $this->aSekolahRelatedBySekolahId;
    }

    /**
     * Declares an association between this object and a MstWilayah object.
     *
     * @param             MstWilayah $v
     * @return Pengguna The current object (for fluent API support)
     * @throws PropelException
     */
    public function setMstWilayahRelatedByKodeWilayah(MstWilayah $v = null)
    {
        if ($v === null) {
            $this->setKodeWilayah(NULL);
        } else {
            $this->setKodeWilayah($v->getKodeWilayah());
        }

        $this->aMstWilayahRelatedByKodeWilayah = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the MstWilayah object, it will not be re-added.
        if ($v !== null) {
            $v->addPenggunaRelatedByKodeWilayah($this);
        }


        return $this;
    }


    /**
     * Get the associated MstWilayah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return MstWilayah The associated MstWilayah object.
     * @throws PropelException
     */
    public function getMstWilayahRelatedByKodeWilayah(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aMstWilayahRelatedByKodeWilayah === null && (($this->kode_wilayah !== "" && $this->kode_wilayah !== null)) && $doQuery) {
            $this->aMstWilayahRelatedByKodeWilayah = MstWilayahQuery::create()->findPk($this->kode_wilayah, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aMstWilayahRelatedByKodeWilayah->addPenggunasRelatedByKodeWilayah($this);
             */
        }

        return $this->aMstWilayahRelatedByKodeWilayah;
    }

    /**
     * Declares an association between this object and a MstWilayah object.
     *
     * @param             MstWilayah $v
     * @return Pengguna The current object (for fluent API support)
     * @throws PropelException
     */
    public function setMstWilayahRelatedByKodeWilayah(MstWilayah $v = null)
    {
        if ($v === null) {
            $this->setKodeWilayah(NULL);
        } else {
            $this->setKodeWilayah($v->getKodeWilayah());
        }

        $this->aMstWilayahRelatedByKodeWilayah = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the MstWilayah object, it will not be re-added.
        if ($v !== null) {
            $v->addPenggunaRelatedByKodeWilayah($this);
        }


        return $this;
    }


    /**
     * Get the associated MstWilayah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return MstWilayah The associated MstWilayah object.
     * @throws PropelException
     */
    public function getMstWilayahRelatedByKodeWilayah(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aMstWilayahRelatedByKodeWilayah === null && (($this->kode_wilayah !== "" && $this->kode_wilayah !== null)) && $doQuery) {
            $this->aMstWilayahRelatedByKodeWilayah = MstWilayahQuery::create()->findPk($this->kode_wilayah, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aMstWilayahRelatedByKodeWilayah->addPenggunasRelatedByKodeWilayah($this);
             */
        }

        return $this->aMstWilayahRelatedByKodeWilayah;
    }

    /**
     * Declares an association between this object and a Peran object.
     *
     * @param             Peran $v
     * @return Pengguna The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPeranRelatedByPeranId(Peran $v = null)
    {
        if ($v === null) {
            $this->setPeranId(NULL);
        } else {
            $this->setPeranId($v->getPeranId());
        }

        $this->aPeranRelatedByPeranId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Peran object, it will not be re-added.
        if ($v !== null) {
            $v->addPenggunaRelatedByPeranId($this);
        }


        return $this;
    }


    /**
     * Get the associated Peran object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Peran The associated Peran object.
     * @throws PropelException
     */
    public function getPeranRelatedByPeranId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPeranRelatedByPeranId === null && ($this->peran_id !== null) && $doQuery) {
            $this->aPeranRelatedByPeranId = PeranQuery::create()->findPk($this->peran_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPeranRelatedByPeranId->addPenggunasRelatedByPeranId($this);
             */
        }

        return $this->aPeranRelatedByPeranId;
    }

    /**
     * Declares an association between this object and a Peran object.
     *
     * @param             Peran $v
     * @return Pengguna The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPeranRelatedByPeranId(Peran $v = null)
    {
        if ($v === null) {
            $this->setPeranId(NULL);
        } else {
            $this->setPeranId($v->getPeranId());
        }

        $this->aPeranRelatedByPeranId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Peran object, it will not be re-added.
        if ($v !== null) {
            $v->addPenggunaRelatedByPeranId($this);
        }


        return $this;
    }


    /**
     * Get the associated Peran object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Peran The associated Peran object.
     * @throws PropelException
     */
    public function getPeranRelatedByPeranId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPeranRelatedByPeranId === null && ($this->peran_id !== null) && $doQuery) {
            $this->aPeranRelatedByPeranId = PeranQuery::create()->findPk($this->peran_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPeranRelatedByPeranId->addPenggunasRelatedByPeranId($this);
             */
        }

        return $this->aPeranRelatedByPeranId;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('LogPenggunaRelatedByPenggunaId' == $relationName) {
            $this->initLogPenggunasRelatedByPenggunaId();
        }
        if ('LogPenggunaRelatedByPenggunaId' == $relationName) {
            $this->initLogPenggunasRelatedByPenggunaId();
        }
        if ('SasaranSurveyRelatedByPenggunaId' == $relationName) {
            $this->initSasaranSurveysRelatedByPenggunaId();
        }
        if ('SasaranSurveyRelatedByPenggunaId' == $relationName) {
            $this->initSasaranSurveysRelatedByPenggunaId();
        }
        if ('LembagaNonSekolahRelatedByPenggunaId' == $relationName) {
            $this->initLembagaNonSekolahsRelatedByPenggunaId();
        }
        if ('LembagaNonSekolahRelatedByPenggunaId' == $relationName) {
            $this->initLembagaNonSekolahsRelatedByPenggunaId();
        }
    }

    /**
     * Clears out the collLogPenggunasRelatedByPenggunaId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Pengguna The current object (for fluent API support)
     * @see        addLogPenggunasRelatedByPenggunaId()
     */
    public function clearLogPenggunasRelatedByPenggunaId()
    {
        $this->collLogPenggunasRelatedByPenggunaId = null; // important to set this to null since that means it is uninitialized
        $this->collLogPenggunasRelatedByPenggunaIdPartial = null;

        return $this;
    }

    /**
     * reset is the collLogPenggunasRelatedByPenggunaId collection loaded partially
     *
     * @return void
     */
    public function resetPartialLogPenggunasRelatedByPenggunaId($v = true)
    {
        $this->collLogPenggunasRelatedByPenggunaIdPartial = $v;
    }

    /**
     * Initializes the collLogPenggunasRelatedByPenggunaId collection.
     *
     * By default this just sets the collLogPenggunasRelatedByPenggunaId collection to an empty array (like clearcollLogPenggunasRelatedByPenggunaId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initLogPenggunasRelatedByPenggunaId($overrideExisting = true)
    {
        if (null !== $this->collLogPenggunasRelatedByPenggunaId && !$overrideExisting) {
            return;
        }
        $this->collLogPenggunasRelatedByPenggunaId = new PropelObjectCollection();
        $this->collLogPenggunasRelatedByPenggunaId->setModel('LogPengguna');
    }

    /**
     * Gets an array of LogPengguna objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Pengguna is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|LogPengguna[] List of LogPengguna objects
     * @throws PropelException
     */
    public function getLogPenggunasRelatedByPenggunaId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collLogPenggunasRelatedByPenggunaIdPartial && !$this->isNew();
        if (null === $this->collLogPenggunasRelatedByPenggunaId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collLogPenggunasRelatedByPenggunaId) {
                // return empty collection
                $this->initLogPenggunasRelatedByPenggunaId();
            } else {
                $collLogPenggunasRelatedByPenggunaId = LogPenggunaQuery::create(null, $criteria)
                    ->filterByPenggunaRelatedByPenggunaId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collLogPenggunasRelatedByPenggunaIdPartial && count($collLogPenggunasRelatedByPenggunaId)) {
                      $this->initLogPenggunasRelatedByPenggunaId(false);

                      foreach($collLogPenggunasRelatedByPenggunaId as $obj) {
                        if (false == $this->collLogPenggunasRelatedByPenggunaId->contains($obj)) {
                          $this->collLogPenggunasRelatedByPenggunaId->append($obj);
                        }
                      }

                      $this->collLogPenggunasRelatedByPenggunaIdPartial = true;
                    }

                    $collLogPenggunasRelatedByPenggunaId->getInternalIterator()->rewind();
                    return $collLogPenggunasRelatedByPenggunaId;
                }

                if($partial && $this->collLogPenggunasRelatedByPenggunaId) {
                    foreach($this->collLogPenggunasRelatedByPenggunaId as $obj) {
                        if($obj->isNew()) {
                            $collLogPenggunasRelatedByPenggunaId[] = $obj;
                        }
                    }
                }

                $this->collLogPenggunasRelatedByPenggunaId = $collLogPenggunasRelatedByPenggunaId;
                $this->collLogPenggunasRelatedByPenggunaIdPartial = false;
            }
        }

        return $this->collLogPenggunasRelatedByPenggunaId;
    }

    /**
     * Sets a collection of LogPenggunaRelatedByPenggunaId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $logPenggunasRelatedByPenggunaId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Pengguna The current object (for fluent API support)
     */
    public function setLogPenggunasRelatedByPenggunaId(PropelCollection $logPenggunasRelatedByPenggunaId, PropelPDO $con = null)
    {
        $logPenggunasRelatedByPenggunaIdToDelete = $this->getLogPenggunasRelatedByPenggunaId(new Criteria(), $con)->diff($logPenggunasRelatedByPenggunaId);

        $this->logPenggunasRelatedByPenggunaIdScheduledForDeletion = unserialize(serialize($logPenggunasRelatedByPenggunaIdToDelete));

        foreach ($logPenggunasRelatedByPenggunaIdToDelete as $logPenggunaRelatedByPenggunaIdRemoved) {
            $logPenggunaRelatedByPenggunaIdRemoved->setPenggunaRelatedByPenggunaId(null);
        }

        $this->collLogPenggunasRelatedByPenggunaId = null;
        foreach ($logPenggunasRelatedByPenggunaId as $logPenggunaRelatedByPenggunaId) {
            $this->addLogPenggunaRelatedByPenggunaId($logPenggunaRelatedByPenggunaId);
        }

        $this->collLogPenggunasRelatedByPenggunaId = $logPenggunasRelatedByPenggunaId;
        $this->collLogPenggunasRelatedByPenggunaIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related LogPengguna objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related LogPengguna objects.
     * @throws PropelException
     */
    public function countLogPenggunasRelatedByPenggunaId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collLogPenggunasRelatedByPenggunaIdPartial && !$this->isNew();
        if (null === $this->collLogPenggunasRelatedByPenggunaId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collLogPenggunasRelatedByPenggunaId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getLogPenggunasRelatedByPenggunaId());
            }
            $query = LogPenggunaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPenggunaRelatedByPenggunaId($this)
                ->count($con);
        }

        return count($this->collLogPenggunasRelatedByPenggunaId);
    }

    /**
     * Method called to associate a LogPengguna object to this object
     * through the LogPengguna foreign key attribute.
     *
     * @param    LogPengguna $l LogPengguna
     * @return Pengguna The current object (for fluent API support)
     */
    public function addLogPenggunaRelatedByPenggunaId(LogPengguna $l)
    {
        if ($this->collLogPenggunasRelatedByPenggunaId === null) {
            $this->initLogPenggunasRelatedByPenggunaId();
            $this->collLogPenggunasRelatedByPenggunaIdPartial = true;
        }
        if (!in_array($l, $this->collLogPenggunasRelatedByPenggunaId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddLogPenggunaRelatedByPenggunaId($l);
        }

        return $this;
    }

    /**
     * @param	LogPenggunaRelatedByPenggunaId $logPenggunaRelatedByPenggunaId The logPenggunaRelatedByPenggunaId object to add.
     */
    protected function doAddLogPenggunaRelatedByPenggunaId($logPenggunaRelatedByPenggunaId)
    {
        $this->collLogPenggunasRelatedByPenggunaId[]= $logPenggunaRelatedByPenggunaId;
        $logPenggunaRelatedByPenggunaId->setPenggunaRelatedByPenggunaId($this);
    }

    /**
     * @param	LogPenggunaRelatedByPenggunaId $logPenggunaRelatedByPenggunaId The logPenggunaRelatedByPenggunaId object to remove.
     * @return Pengguna The current object (for fluent API support)
     */
    public function removeLogPenggunaRelatedByPenggunaId($logPenggunaRelatedByPenggunaId)
    {
        if ($this->getLogPenggunasRelatedByPenggunaId()->contains($logPenggunaRelatedByPenggunaId)) {
            $this->collLogPenggunasRelatedByPenggunaId->remove($this->collLogPenggunasRelatedByPenggunaId->search($logPenggunaRelatedByPenggunaId));
            if (null === $this->logPenggunasRelatedByPenggunaIdScheduledForDeletion) {
                $this->logPenggunasRelatedByPenggunaIdScheduledForDeletion = clone $this->collLogPenggunasRelatedByPenggunaId;
                $this->logPenggunasRelatedByPenggunaIdScheduledForDeletion->clear();
            }
            $this->logPenggunasRelatedByPenggunaIdScheduledForDeletion[]= clone $logPenggunaRelatedByPenggunaId;
            $logPenggunaRelatedByPenggunaId->setPenggunaRelatedByPenggunaId(null);
        }

        return $this;
    }

    /**
     * Clears out the collLogPenggunasRelatedByPenggunaId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Pengguna The current object (for fluent API support)
     * @see        addLogPenggunasRelatedByPenggunaId()
     */
    public function clearLogPenggunasRelatedByPenggunaId()
    {
        $this->collLogPenggunasRelatedByPenggunaId = null; // important to set this to null since that means it is uninitialized
        $this->collLogPenggunasRelatedByPenggunaIdPartial = null;

        return $this;
    }

    /**
     * reset is the collLogPenggunasRelatedByPenggunaId collection loaded partially
     *
     * @return void
     */
    public function resetPartialLogPenggunasRelatedByPenggunaId($v = true)
    {
        $this->collLogPenggunasRelatedByPenggunaIdPartial = $v;
    }

    /**
     * Initializes the collLogPenggunasRelatedByPenggunaId collection.
     *
     * By default this just sets the collLogPenggunasRelatedByPenggunaId collection to an empty array (like clearcollLogPenggunasRelatedByPenggunaId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initLogPenggunasRelatedByPenggunaId($overrideExisting = true)
    {
        if (null !== $this->collLogPenggunasRelatedByPenggunaId && !$overrideExisting) {
            return;
        }
        $this->collLogPenggunasRelatedByPenggunaId = new PropelObjectCollection();
        $this->collLogPenggunasRelatedByPenggunaId->setModel('LogPengguna');
    }

    /**
     * Gets an array of LogPengguna objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Pengguna is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|LogPengguna[] List of LogPengguna objects
     * @throws PropelException
     */
    public function getLogPenggunasRelatedByPenggunaId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collLogPenggunasRelatedByPenggunaIdPartial && !$this->isNew();
        if (null === $this->collLogPenggunasRelatedByPenggunaId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collLogPenggunasRelatedByPenggunaId) {
                // return empty collection
                $this->initLogPenggunasRelatedByPenggunaId();
            } else {
                $collLogPenggunasRelatedByPenggunaId = LogPenggunaQuery::create(null, $criteria)
                    ->filterByPenggunaRelatedByPenggunaId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collLogPenggunasRelatedByPenggunaIdPartial && count($collLogPenggunasRelatedByPenggunaId)) {
                      $this->initLogPenggunasRelatedByPenggunaId(false);

                      foreach($collLogPenggunasRelatedByPenggunaId as $obj) {
                        if (false == $this->collLogPenggunasRelatedByPenggunaId->contains($obj)) {
                          $this->collLogPenggunasRelatedByPenggunaId->append($obj);
                        }
                      }

                      $this->collLogPenggunasRelatedByPenggunaIdPartial = true;
                    }

                    $collLogPenggunasRelatedByPenggunaId->getInternalIterator()->rewind();
                    return $collLogPenggunasRelatedByPenggunaId;
                }

                if($partial && $this->collLogPenggunasRelatedByPenggunaId) {
                    foreach($this->collLogPenggunasRelatedByPenggunaId as $obj) {
                        if($obj->isNew()) {
                            $collLogPenggunasRelatedByPenggunaId[] = $obj;
                        }
                    }
                }

                $this->collLogPenggunasRelatedByPenggunaId = $collLogPenggunasRelatedByPenggunaId;
                $this->collLogPenggunasRelatedByPenggunaIdPartial = false;
            }
        }

        return $this->collLogPenggunasRelatedByPenggunaId;
    }

    /**
     * Sets a collection of LogPenggunaRelatedByPenggunaId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $logPenggunasRelatedByPenggunaId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Pengguna The current object (for fluent API support)
     */
    public function setLogPenggunasRelatedByPenggunaId(PropelCollection $logPenggunasRelatedByPenggunaId, PropelPDO $con = null)
    {
        $logPenggunasRelatedByPenggunaIdToDelete = $this->getLogPenggunasRelatedByPenggunaId(new Criteria(), $con)->diff($logPenggunasRelatedByPenggunaId);

        $this->logPenggunasRelatedByPenggunaIdScheduledForDeletion = unserialize(serialize($logPenggunasRelatedByPenggunaIdToDelete));

        foreach ($logPenggunasRelatedByPenggunaIdToDelete as $logPenggunaRelatedByPenggunaIdRemoved) {
            $logPenggunaRelatedByPenggunaIdRemoved->setPenggunaRelatedByPenggunaId(null);
        }

        $this->collLogPenggunasRelatedByPenggunaId = null;
        foreach ($logPenggunasRelatedByPenggunaId as $logPenggunaRelatedByPenggunaId) {
            $this->addLogPenggunaRelatedByPenggunaId($logPenggunaRelatedByPenggunaId);
        }

        $this->collLogPenggunasRelatedByPenggunaId = $logPenggunasRelatedByPenggunaId;
        $this->collLogPenggunasRelatedByPenggunaIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related LogPengguna objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related LogPengguna objects.
     * @throws PropelException
     */
    public function countLogPenggunasRelatedByPenggunaId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collLogPenggunasRelatedByPenggunaIdPartial && !$this->isNew();
        if (null === $this->collLogPenggunasRelatedByPenggunaId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collLogPenggunasRelatedByPenggunaId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getLogPenggunasRelatedByPenggunaId());
            }
            $query = LogPenggunaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPenggunaRelatedByPenggunaId($this)
                ->count($con);
        }

        return count($this->collLogPenggunasRelatedByPenggunaId);
    }

    /**
     * Method called to associate a LogPengguna object to this object
     * through the LogPengguna foreign key attribute.
     *
     * @param    LogPengguna $l LogPengguna
     * @return Pengguna The current object (for fluent API support)
     */
    public function addLogPenggunaRelatedByPenggunaId(LogPengguna $l)
    {
        if ($this->collLogPenggunasRelatedByPenggunaId === null) {
            $this->initLogPenggunasRelatedByPenggunaId();
            $this->collLogPenggunasRelatedByPenggunaIdPartial = true;
        }
        if (!in_array($l, $this->collLogPenggunasRelatedByPenggunaId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddLogPenggunaRelatedByPenggunaId($l);
        }

        return $this;
    }

    /**
     * @param	LogPenggunaRelatedByPenggunaId $logPenggunaRelatedByPenggunaId The logPenggunaRelatedByPenggunaId object to add.
     */
    protected function doAddLogPenggunaRelatedByPenggunaId($logPenggunaRelatedByPenggunaId)
    {
        $this->collLogPenggunasRelatedByPenggunaId[]= $logPenggunaRelatedByPenggunaId;
        $logPenggunaRelatedByPenggunaId->setPenggunaRelatedByPenggunaId($this);
    }

    /**
     * @param	LogPenggunaRelatedByPenggunaId $logPenggunaRelatedByPenggunaId The logPenggunaRelatedByPenggunaId object to remove.
     * @return Pengguna The current object (for fluent API support)
     */
    public function removeLogPenggunaRelatedByPenggunaId($logPenggunaRelatedByPenggunaId)
    {
        if ($this->getLogPenggunasRelatedByPenggunaId()->contains($logPenggunaRelatedByPenggunaId)) {
            $this->collLogPenggunasRelatedByPenggunaId->remove($this->collLogPenggunasRelatedByPenggunaId->search($logPenggunaRelatedByPenggunaId));
            if (null === $this->logPenggunasRelatedByPenggunaIdScheduledForDeletion) {
                $this->logPenggunasRelatedByPenggunaIdScheduledForDeletion = clone $this->collLogPenggunasRelatedByPenggunaId;
                $this->logPenggunasRelatedByPenggunaIdScheduledForDeletion->clear();
            }
            $this->logPenggunasRelatedByPenggunaIdScheduledForDeletion[]= clone $logPenggunaRelatedByPenggunaId;
            $logPenggunaRelatedByPenggunaId->setPenggunaRelatedByPenggunaId(null);
        }

        return $this;
    }

    /**
     * Clears out the collSasaranSurveysRelatedByPenggunaId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Pengguna The current object (for fluent API support)
     * @see        addSasaranSurveysRelatedByPenggunaId()
     */
    public function clearSasaranSurveysRelatedByPenggunaId()
    {
        $this->collSasaranSurveysRelatedByPenggunaId = null; // important to set this to null since that means it is uninitialized
        $this->collSasaranSurveysRelatedByPenggunaIdPartial = null;

        return $this;
    }

    /**
     * reset is the collSasaranSurveysRelatedByPenggunaId collection loaded partially
     *
     * @return void
     */
    public function resetPartialSasaranSurveysRelatedByPenggunaId($v = true)
    {
        $this->collSasaranSurveysRelatedByPenggunaIdPartial = $v;
    }

    /**
     * Initializes the collSasaranSurveysRelatedByPenggunaId collection.
     *
     * By default this just sets the collSasaranSurveysRelatedByPenggunaId collection to an empty array (like clearcollSasaranSurveysRelatedByPenggunaId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSasaranSurveysRelatedByPenggunaId($overrideExisting = true)
    {
        if (null !== $this->collSasaranSurveysRelatedByPenggunaId && !$overrideExisting) {
            return;
        }
        $this->collSasaranSurveysRelatedByPenggunaId = new PropelObjectCollection();
        $this->collSasaranSurveysRelatedByPenggunaId->setModel('SasaranSurvey');
    }

    /**
     * Gets an array of SasaranSurvey objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Pengguna is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SasaranSurvey[] List of SasaranSurvey objects
     * @throws PropelException
     */
    public function getSasaranSurveysRelatedByPenggunaId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSasaranSurveysRelatedByPenggunaIdPartial && !$this->isNew();
        if (null === $this->collSasaranSurveysRelatedByPenggunaId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSasaranSurveysRelatedByPenggunaId) {
                // return empty collection
                $this->initSasaranSurveysRelatedByPenggunaId();
            } else {
                $collSasaranSurveysRelatedByPenggunaId = SasaranSurveyQuery::create(null, $criteria)
                    ->filterByPenggunaRelatedByPenggunaId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSasaranSurveysRelatedByPenggunaIdPartial && count($collSasaranSurveysRelatedByPenggunaId)) {
                      $this->initSasaranSurveysRelatedByPenggunaId(false);

                      foreach($collSasaranSurveysRelatedByPenggunaId as $obj) {
                        if (false == $this->collSasaranSurveysRelatedByPenggunaId->contains($obj)) {
                          $this->collSasaranSurveysRelatedByPenggunaId->append($obj);
                        }
                      }

                      $this->collSasaranSurveysRelatedByPenggunaIdPartial = true;
                    }

                    $collSasaranSurveysRelatedByPenggunaId->getInternalIterator()->rewind();
                    return $collSasaranSurveysRelatedByPenggunaId;
                }

                if($partial && $this->collSasaranSurveysRelatedByPenggunaId) {
                    foreach($this->collSasaranSurveysRelatedByPenggunaId as $obj) {
                        if($obj->isNew()) {
                            $collSasaranSurveysRelatedByPenggunaId[] = $obj;
                        }
                    }
                }

                $this->collSasaranSurveysRelatedByPenggunaId = $collSasaranSurveysRelatedByPenggunaId;
                $this->collSasaranSurveysRelatedByPenggunaIdPartial = false;
            }
        }

        return $this->collSasaranSurveysRelatedByPenggunaId;
    }

    /**
     * Sets a collection of SasaranSurveyRelatedByPenggunaId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $sasaranSurveysRelatedByPenggunaId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Pengguna The current object (for fluent API support)
     */
    public function setSasaranSurveysRelatedByPenggunaId(PropelCollection $sasaranSurveysRelatedByPenggunaId, PropelPDO $con = null)
    {
        $sasaranSurveysRelatedByPenggunaIdToDelete = $this->getSasaranSurveysRelatedByPenggunaId(new Criteria(), $con)->diff($sasaranSurveysRelatedByPenggunaId);

        $this->sasaranSurveysRelatedByPenggunaIdScheduledForDeletion = unserialize(serialize($sasaranSurveysRelatedByPenggunaIdToDelete));

        foreach ($sasaranSurveysRelatedByPenggunaIdToDelete as $sasaranSurveyRelatedByPenggunaIdRemoved) {
            $sasaranSurveyRelatedByPenggunaIdRemoved->setPenggunaRelatedByPenggunaId(null);
        }

        $this->collSasaranSurveysRelatedByPenggunaId = null;
        foreach ($sasaranSurveysRelatedByPenggunaId as $sasaranSurveyRelatedByPenggunaId) {
            $this->addSasaranSurveyRelatedByPenggunaId($sasaranSurveyRelatedByPenggunaId);
        }

        $this->collSasaranSurveysRelatedByPenggunaId = $sasaranSurveysRelatedByPenggunaId;
        $this->collSasaranSurveysRelatedByPenggunaIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SasaranSurvey objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SasaranSurvey objects.
     * @throws PropelException
     */
    public function countSasaranSurveysRelatedByPenggunaId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSasaranSurveysRelatedByPenggunaIdPartial && !$this->isNew();
        if (null === $this->collSasaranSurveysRelatedByPenggunaId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSasaranSurveysRelatedByPenggunaId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSasaranSurveysRelatedByPenggunaId());
            }
            $query = SasaranSurveyQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPenggunaRelatedByPenggunaId($this)
                ->count($con);
        }

        return count($this->collSasaranSurveysRelatedByPenggunaId);
    }

    /**
     * Method called to associate a SasaranSurvey object to this object
     * through the SasaranSurvey foreign key attribute.
     *
     * @param    SasaranSurvey $l SasaranSurvey
     * @return Pengguna The current object (for fluent API support)
     */
    public function addSasaranSurveyRelatedByPenggunaId(SasaranSurvey $l)
    {
        if ($this->collSasaranSurveysRelatedByPenggunaId === null) {
            $this->initSasaranSurveysRelatedByPenggunaId();
            $this->collSasaranSurveysRelatedByPenggunaIdPartial = true;
        }
        if (!in_array($l, $this->collSasaranSurveysRelatedByPenggunaId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSasaranSurveyRelatedByPenggunaId($l);
        }

        return $this;
    }

    /**
     * @param	SasaranSurveyRelatedByPenggunaId $sasaranSurveyRelatedByPenggunaId The sasaranSurveyRelatedByPenggunaId object to add.
     */
    protected function doAddSasaranSurveyRelatedByPenggunaId($sasaranSurveyRelatedByPenggunaId)
    {
        $this->collSasaranSurveysRelatedByPenggunaId[]= $sasaranSurveyRelatedByPenggunaId;
        $sasaranSurveyRelatedByPenggunaId->setPenggunaRelatedByPenggunaId($this);
    }

    /**
     * @param	SasaranSurveyRelatedByPenggunaId $sasaranSurveyRelatedByPenggunaId The sasaranSurveyRelatedByPenggunaId object to remove.
     * @return Pengguna The current object (for fluent API support)
     */
    public function removeSasaranSurveyRelatedByPenggunaId($sasaranSurveyRelatedByPenggunaId)
    {
        if ($this->getSasaranSurveysRelatedByPenggunaId()->contains($sasaranSurveyRelatedByPenggunaId)) {
            $this->collSasaranSurveysRelatedByPenggunaId->remove($this->collSasaranSurveysRelatedByPenggunaId->search($sasaranSurveyRelatedByPenggunaId));
            if (null === $this->sasaranSurveysRelatedByPenggunaIdScheduledForDeletion) {
                $this->sasaranSurveysRelatedByPenggunaIdScheduledForDeletion = clone $this->collSasaranSurveysRelatedByPenggunaId;
                $this->sasaranSurveysRelatedByPenggunaIdScheduledForDeletion->clear();
            }
            $this->sasaranSurveysRelatedByPenggunaIdScheduledForDeletion[]= clone $sasaranSurveyRelatedByPenggunaId;
            $sasaranSurveyRelatedByPenggunaId->setPenggunaRelatedByPenggunaId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pengguna is new, it will return
     * an empty collection; or if this Pengguna has previously
     * been saved, it will retrieve related SasaranSurveysRelatedByPenggunaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pengguna.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SasaranSurvey[] List of SasaranSurvey objects
     */
    public function getSasaranSurveysRelatedByPenggunaIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SasaranSurveyQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getSasaranSurveysRelatedByPenggunaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pengguna is new, it will return
     * an empty collection; or if this Pengguna has previously
     * been saved, it will retrieve related SasaranSurveysRelatedByPenggunaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pengguna.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SasaranSurvey[] List of SasaranSurvey objects
     */
    public function getSasaranSurveysRelatedByPenggunaIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SasaranSurveyQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getSasaranSurveysRelatedByPenggunaId($query, $con);
    }

    /**
     * Clears out the collSasaranSurveysRelatedByPenggunaId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Pengguna The current object (for fluent API support)
     * @see        addSasaranSurveysRelatedByPenggunaId()
     */
    public function clearSasaranSurveysRelatedByPenggunaId()
    {
        $this->collSasaranSurveysRelatedByPenggunaId = null; // important to set this to null since that means it is uninitialized
        $this->collSasaranSurveysRelatedByPenggunaIdPartial = null;

        return $this;
    }

    /**
     * reset is the collSasaranSurveysRelatedByPenggunaId collection loaded partially
     *
     * @return void
     */
    public function resetPartialSasaranSurveysRelatedByPenggunaId($v = true)
    {
        $this->collSasaranSurveysRelatedByPenggunaIdPartial = $v;
    }

    /**
     * Initializes the collSasaranSurveysRelatedByPenggunaId collection.
     *
     * By default this just sets the collSasaranSurveysRelatedByPenggunaId collection to an empty array (like clearcollSasaranSurveysRelatedByPenggunaId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSasaranSurveysRelatedByPenggunaId($overrideExisting = true)
    {
        if (null !== $this->collSasaranSurveysRelatedByPenggunaId && !$overrideExisting) {
            return;
        }
        $this->collSasaranSurveysRelatedByPenggunaId = new PropelObjectCollection();
        $this->collSasaranSurveysRelatedByPenggunaId->setModel('SasaranSurvey');
    }

    /**
     * Gets an array of SasaranSurvey objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Pengguna is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SasaranSurvey[] List of SasaranSurvey objects
     * @throws PropelException
     */
    public function getSasaranSurveysRelatedByPenggunaId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSasaranSurveysRelatedByPenggunaIdPartial && !$this->isNew();
        if (null === $this->collSasaranSurveysRelatedByPenggunaId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSasaranSurveysRelatedByPenggunaId) {
                // return empty collection
                $this->initSasaranSurveysRelatedByPenggunaId();
            } else {
                $collSasaranSurveysRelatedByPenggunaId = SasaranSurveyQuery::create(null, $criteria)
                    ->filterByPenggunaRelatedByPenggunaId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSasaranSurveysRelatedByPenggunaIdPartial && count($collSasaranSurveysRelatedByPenggunaId)) {
                      $this->initSasaranSurveysRelatedByPenggunaId(false);

                      foreach($collSasaranSurveysRelatedByPenggunaId as $obj) {
                        if (false == $this->collSasaranSurveysRelatedByPenggunaId->contains($obj)) {
                          $this->collSasaranSurveysRelatedByPenggunaId->append($obj);
                        }
                      }

                      $this->collSasaranSurveysRelatedByPenggunaIdPartial = true;
                    }

                    $collSasaranSurveysRelatedByPenggunaId->getInternalIterator()->rewind();
                    return $collSasaranSurveysRelatedByPenggunaId;
                }

                if($partial && $this->collSasaranSurveysRelatedByPenggunaId) {
                    foreach($this->collSasaranSurveysRelatedByPenggunaId as $obj) {
                        if($obj->isNew()) {
                            $collSasaranSurveysRelatedByPenggunaId[] = $obj;
                        }
                    }
                }

                $this->collSasaranSurveysRelatedByPenggunaId = $collSasaranSurveysRelatedByPenggunaId;
                $this->collSasaranSurveysRelatedByPenggunaIdPartial = false;
            }
        }

        return $this->collSasaranSurveysRelatedByPenggunaId;
    }

    /**
     * Sets a collection of SasaranSurveyRelatedByPenggunaId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $sasaranSurveysRelatedByPenggunaId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Pengguna The current object (for fluent API support)
     */
    public function setSasaranSurveysRelatedByPenggunaId(PropelCollection $sasaranSurveysRelatedByPenggunaId, PropelPDO $con = null)
    {
        $sasaranSurveysRelatedByPenggunaIdToDelete = $this->getSasaranSurveysRelatedByPenggunaId(new Criteria(), $con)->diff($sasaranSurveysRelatedByPenggunaId);

        $this->sasaranSurveysRelatedByPenggunaIdScheduledForDeletion = unserialize(serialize($sasaranSurveysRelatedByPenggunaIdToDelete));

        foreach ($sasaranSurveysRelatedByPenggunaIdToDelete as $sasaranSurveyRelatedByPenggunaIdRemoved) {
            $sasaranSurveyRelatedByPenggunaIdRemoved->setPenggunaRelatedByPenggunaId(null);
        }

        $this->collSasaranSurveysRelatedByPenggunaId = null;
        foreach ($sasaranSurveysRelatedByPenggunaId as $sasaranSurveyRelatedByPenggunaId) {
            $this->addSasaranSurveyRelatedByPenggunaId($sasaranSurveyRelatedByPenggunaId);
        }

        $this->collSasaranSurveysRelatedByPenggunaId = $sasaranSurveysRelatedByPenggunaId;
        $this->collSasaranSurveysRelatedByPenggunaIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SasaranSurvey objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SasaranSurvey objects.
     * @throws PropelException
     */
    public function countSasaranSurveysRelatedByPenggunaId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSasaranSurveysRelatedByPenggunaIdPartial && !$this->isNew();
        if (null === $this->collSasaranSurveysRelatedByPenggunaId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSasaranSurveysRelatedByPenggunaId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSasaranSurveysRelatedByPenggunaId());
            }
            $query = SasaranSurveyQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPenggunaRelatedByPenggunaId($this)
                ->count($con);
        }

        return count($this->collSasaranSurveysRelatedByPenggunaId);
    }

    /**
     * Method called to associate a SasaranSurvey object to this object
     * through the SasaranSurvey foreign key attribute.
     *
     * @param    SasaranSurvey $l SasaranSurvey
     * @return Pengguna The current object (for fluent API support)
     */
    public function addSasaranSurveyRelatedByPenggunaId(SasaranSurvey $l)
    {
        if ($this->collSasaranSurveysRelatedByPenggunaId === null) {
            $this->initSasaranSurveysRelatedByPenggunaId();
            $this->collSasaranSurveysRelatedByPenggunaIdPartial = true;
        }
        if (!in_array($l, $this->collSasaranSurveysRelatedByPenggunaId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSasaranSurveyRelatedByPenggunaId($l);
        }

        return $this;
    }

    /**
     * @param	SasaranSurveyRelatedByPenggunaId $sasaranSurveyRelatedByPenggunaId The sasaranSurveyRelatedByPenggunaId object to add.
     */
    protected function doAddSasaranSurveyRelatedByPenggunaId($sasaranSurveyRelatedByPenggunaId)
    {
        $this->collSasaranSurveysRelatedByPenggunaId[]= $sasaranSurveyRelatedByPenggunaId;
        $sasaranSurveyRelatedByPenggunaId->setPenggunaRelatedByPenggunaId($this);
    }

    /**
     * @param	SasaranSurveyRelatedByPenggunaId $sasaranSurveyRelatedByPenggunaId The sasaranSurveyRelatedByPenggunaId object to remove.
     * @return Pengguna The current object (for fluent API support)
     */
    public function removeSasaranSurveyRelatedByPenggunaId($sasaranSurveyRelatedByPenggunaId)
    {
        if ($this->getSasaranSurveysRelatedByPenggunaId()->contains($sasaranSurveyRelatedByPenggunaId)) {
            $this->collSasaranSurveysRelatedByPenggunaId->remove($this->collSasaranSurveysRelatedByPenggunaId->search($sasaranSurveyRelatedByPenggunaId));
            if (null === $this->sasaranSurveysRelatedByPenggunaIdScheduledForDeletion) {
                $this->sasaranSurveysRelatedByPenggunaIdScheduledForDeletion = clone $this->collSasaranSurveysRelatedByPenggunaId;
                $this->sasaranSurveysRelatedByPenggunaIdScheduledForDeletion->clear();
            }
            $this->sasaranSurveysRelatedByPenggunaIdScheduledForDeletion[]= clone $sasaranSurveyRelatedByPenggunaId;
            $sasaranSurveyRelatedByPenggunaId->setPenggunaRelatedByPenggunaId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pengguna is new, it will return
     * an empty collection; or if this Pengguna has previously
     * been saved, it will retrieve related SasaranSurveysRelatedByPenggunaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pengguna.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SasaranSurvey[] List of SasaranSurvey objects
     */
    public function getSasaranSurveysRelatedByPenggunaIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SasaranSurveyQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getSasaranSurveysRelatedByPenggunaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pengguna is new, it will return
     * an empty collection; or if this Pengguna has previously
     * been saved, it will retrieve related SasaranSurveysRelatedByPenggunaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pengguna.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SasaranSurvey[] List of SasaranSurvey objects
     */
    public function getSasaranSurveysRelatedByPenggunaIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SasaranSurveyQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getSasaranSurveysRelatedByPenggunaId($query, $con);
    }

    /**
     * Clears out the collLembagaNonSekolahsRelatedByPenggunaId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Pengguna The current object (for fluent API support)
     * @see        addLembagaNonSekolahsRelatedByPenggunaId()
     */
    public function clearLembagaNonSekolahsRelatedByPenggunaId()
    {
        $this->collLembagaNonSekolahsRelatedByPenggunaId = null; // important to set this to null since that means it is uninitialized
        $this->collLembagaNonSekolahsRelatedByPenggunaIdPartial = null;

        return $this;
    }

    /**
     * reset is the collLembagaNonSekolahsRelatedByPenggunaId collection loaded partially
     *
     * @return void
     */
    public function resetPartialLembagaNonSekolahsRelatedByPenggunaId($v = true)
    {
        $this->collLembagaNonSekolahsRelatedByPenggunaIdPartial = $v;
    }

    /**
     * Initializes the collLembagaNonSekolahsRelatedByPenggunaId collection.
     *
     * By default this just sets the collLembagaNonSekolahsRelatedByPenggunaId collection to an empty array (like clearcollLembagaNonSekolahsRelatedByPenggunaId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initLembagaNonSekolahsRelatedByPenggunaId($overrideExisting = true)
    {
        if (null !== $this->collLembagaNonSekolahsRelatedByPenggunaId && !$overrideExisting) {
            return;
        }
        $this->collLembagaNonSekolahsRelatedByPenggunaId = new PropelObjectCollection();
        $this->collLembagaNonSekolahsRelatedByPenggunaId->setModel('LembagaNonSekolah');
    }

    /**
     * Gets an array of LembagaNonSekolah objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Pengguna is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|LembagaNonSekolah[] List of LembagaNonSekolah objects
     * @throws PropelException
     */
    public function getLembagaNonSekolahsRelatedByPenggunaId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collLembagaNonSekolahsRelatedByPenggunaIdPartial && !$this->isNew();
        if (null === $this->collLembagaNonSekolahsRelatedByPenggunaId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collLembagaNonSekolahsRelatedByPenggunaId) {
                // return empty collection
                $this->initLembagaNonSekolahsRelatedByPenggunaId();
            } else {
                $collLembagaNonSekolahsRelatedByPenggunaId = LembagaNonSekolahQuery::create(null, $criteria)
                    ->filterByPenggunaRelatedByPenggunaId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collLembagaNonSekolahsRelatedByPenggunaIdPartial && count($collLembagaNonSekolahsRelatedByPenggunaId)) {
                      $this->initLembagaNonSekolahsRelatedByPenggunaId(false);

                      foreach($collLembagaNonSekolahsRelatedByPenggunaId as $obj) {
                        if (false == $this->collLembagaNonSekolahsRelatedByPenggunaId->contains($obj)) {
                          $this->collLembagaNonSekolahsRelatedByPenggunaId->append($obj);
                        }
                      }

                      $this->collLembagaNonSekolahsRelatedByPenggunaIdPartial = true;
                    }

                    $collLembagaNonSekolahsRelatedByPenggunaId->getInternalIterator()->rewind();
                    return $collLembagaNonSekolahsRelatedByPenggunaId;
                }

                if($partial && $this->collLembagaNonSekolahsRelatedByPenggunaId) {
                    foreach($this->collLembagaNonSekolahsRelatedByPenggunaId as $obj) {
                        if($obj->isNew()) {
                            $collLembagaNonSekolahsRelatedByPenggunaId[] = $obj;
                        }
                    }
                }

                $this->collLembagaNonSekolahsRelatedByPenggunaId = $collLembagaNonSekolahsRelatedByPenggunaId;
                $this->collLembagaNonSekolahsRelatedByPenggunaIdPartial = false;
            }
        }

        return $this->collLembagaNonSekolahsRelatedByPenggunaId;
    }

    /**
     * Sets a collection of LembagaNonSekolahRelatedByPenggunaId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $lembagaNonSekolahsRelatedByPenggunaId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Pengguna The current object (for fluent API support)
     */
    public function setLembagaNonSekolahsRelatedByPenggunaId(PropelCollection $lembagaNonSekolahsRelatedByPenggunaId, PropelPDO $con = null)
    {
        $lembagaNonSekolahsRelatedByPenggunaIdToDelete = $this->getLembagaNonSekolahsRelatedByPenggunaId(new Criteria(), $con)->diff($lembagaNonSekolahsRelatedByPenggunaId);

        $this->lembagaNonSekolahsRelatedByPenggunaIdScheduledForDeletion = unserialize(serialize($lembagaNonSekolahsRelatedByPenggunaIdToDelete));

        foreach ($lembagaNonSekolahsRelatedByPenggunaIdToDelete as $lembagaNonSekolahRelatedByPenggunaIdRemoved) {
            $lembagaNonSekolahRelatedByPenggunaIdRemoved->setPenggunaRelatedByPenggunaId(null);
        }

        $this->collLembagaNonSekolahsRelatedByPenggunaId = null;
        foreach ($lembagaNonSekolahsRelatedByPenggunaId as $lembagaNonSekolahRelatedByPenggunaId) {
            $this->addLembagaNonSekolahRelatedByPenggunaId($lembagaNonSekolahRelatedByPenggunaId);
        }

        $this->collLembagaNonSekolahsRelatedByPenggunaId = $lembagaNonSekolahsRelatedByPenggunaId;
        $this->collLembagaNonSekolahsRelatedByPenggunaIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related LembagaNonSekolah objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related LembagaNonSekolah objects.
     * @throws PropelException
     */
    public function countLembagaNonSekolahsRelatedByPenggunaId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collLembagaNonSekolahsRelatedByPenggunaIdPartial && !$this->isNew();
        if (null === $this->collLembagaNonSekolahsRelatedByPenggunaId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collLembagaNonSekolahsRelatedByPenggunaId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getLembagaNonSekolahsRelatedByPenggunaId());
            }
            $query = LembagaNonSekolahQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPenggunaRelatedByPenggunaId($this)
                ->count($con);
        }

        return count($this->collLembagaNonSekolahsRelatedByPenggunaId);
    }

    /**
     * Method called to associate a LembagaNonSekolah object to this object
     * through the LembagaNonSekolah foreign key attribute.
     *
     * @param    LembagaNonSekolah $l LembagaNonSekolah
     * @return Pengguna The current object (for fluent API support)
     */
    public function addLembagaNonSekolahRelatedByPenggunaId(LembagaNonSekolah $l)
    {
        if ($this->collLembagaNonSekolahsRelatedByPenggunaId === null) {
            $this->initLembagaNonSekolahsRelatedByPenggunaId();
            $this->collLembagaNonSekolahsRelatedByPenggunaIdPartial = true;
        }
        if (!in_array($l, $this->collLembagaNonSekolahsRelatedByPenggunaId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddLembagaNonSekolahRelatedByPenggunaId($l);
        }

        return $this;
    }

    /**
     * @param	LembagaNonSekolahRelatedByPenggunaId $lembagaNonSekolahRelatedByPenggunaId The lembagaNonSekolahRelatedByPenggunaId object to add.
     */
    protected function doAddLembagaNonSekolahRelatedByPenggunaId($lembagaNonSekolahRelatedByPenggunaId)
    {
        $this->collLembagaNonSekolahsRelatedByPenggunaId[]= $lembagaNonSekolahRelatedByPenggunaId;
        $lembagaNonSekolahRelatedByPenggunaId->setPenggunaRelatedByPenggunaId($this);
    }

    /**
     * @param	LembagaNonSekolahRelatedByPenggunaId $lembagaNonSekolahRelatedByPenggunaId The lembagaNonSekolahRelatedByPenggunaId object to remove.
     * @return Pengguna The current object (for fluent API support)
     */
    public function removeLembagaNonSekolahRelatedByPenggunaId($lembagaNonSekolahRelatedByPenggunaId)
    {
        if ($this->getLembagaNonSekolahsRelatedByPenggunaId()->contains($lembagaNonSekolahRelatedByPenggunaId)) {
            $this->collLembagaNonSekolahsRelatedByPenggunaId->remove($this->collLembagaNonSekolahsRelatedByPenggunaId->search($lembagaNonSekolahRelatedByPenggunaId));
            if (null === $this->lembagaNonSekolahsRelatedByPenggunaIdScheduledForDeletion) {
                $this->lembagaNonSekolahsRelatedByPenggunaIdScheduledForDeletion = clone $this->collLembagaNonSekolahsRelatedByPenggunaId;
                $this->lembagaNonSekolahsRelatedByPenggunaIdScheduledForDeletion->clear();
            }
            $this->lembagaNonSekolahsRelatedByPenggunaIdScheduledForDeletion[]= $lembagaNonSekolahRelatedByPenggunaId;
            $lembagaNonSekolahRelatedByPenggunaId->setPenggunaRelatedByPenggunaId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pengguna is new, it will return
     * an empty collection; or if this Pengguna has previously
     * been saved, it will retrieve related LembagaNonSekolahsRelatedByPenggunaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pengguna.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|LembagaNonSekolah[] List of LembagaNonSekolah objects
     */
    public function getLembagaNonSekolahsRelatedByPenggunaIdJoinJenisLembagaRelatedByJenisLembagaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = LembagaNonSekolahQuery::create(null, $criteria);
        $query->joinWith('JenisLembagaRelatedByJenisLembagaId', $join_behavior);

        return $this->getLembagaNonSekolahsRelatedByPenggunaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pengguna is new, it will return
     * an empty collection; or if this Pengguna has previously
     * been saved, it will retrieve related LembagaNonSekolahsRelatedByPenggunaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pengguna.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|LembagaNonSekolah[] List of LembagaNonSekolah objects
     */
    public function getLembagaNonSekolahsRelatedByPenggunaIdJoinJenisLembagaRelatedByJenisLembagaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = LembagaNonSekolahQuery::create(null, $criteria);
        $query->joinWith('JenisLembagaRelatedByJenisLembagaId', $join_behavior);

        return $this->getLembagaNonSekolahsRelatedByPenggunaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pengguna is new, it will return
     * an empty collection; or if this Pengguna has previously
     * been saved, it will retrieve related LembagaNonSekolahsRelatedByPenggunaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pengguna.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|LembagaNonSekolah[] List of LembagaNonSekolah objects
     */
    public function getLembagaNonSekolahsRelatedByPenggunaIdJoinMstWilayahRelatedByKodeWilayah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = LembagaNonSekolahQuery::create(null, $criteria);
        $query->joinWith('MstWilayahRelatedByKodeWilayah', $join_behavior);

        return $this->getLembagaNonSekolahsRelatedByPenggunaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pengguna is new, it will return
     * an empty collection; or if this Pengguna has previously
     * been saved, it will retrieve related LembagaNonSekolahsRelatedByPenggunaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pengguna.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|LembagaNonSekolah[] List of LembagaNonSekolah objects
     */
    public function getLembagaNonSekolahsRelatedByPenggunaIdJoinMstWilayahRelatedByKodeWilayah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = LembagaNonSekolahQuery::create(null, $criteria);
        $query->joinWith('MstWilayahRelatedByKodeWilayah', $join_behavior);

        return $this->getLembagaNonSekolahsRelatedByPenggunaId($query, $con);
    }

    /**
     * Clears out the collLembagaNonSekolahsRelatedByPenggunaId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Pengguna The current object (for fluent API support)
     * @see        addLembagaNonSekolahsRelatedByPenggunaId()
     */
    public function clearLembagaNonSekolahsRelatedByPenggunaId()
    {
        $this->collLembagaNonSekolahsRelatedByPenggunaId = null; // important to set this to null since that means it is uninitialized
        $this->collLembagaNonSekolahsRelatedByPenggunaIdPartial = null;

        return $this;
    }

    /**
     * reset is the collLembagaNonSekolahsRelatedByPenggunaId collection loaded partially
     *
     * @return void
     */
    public function resetPartialLembagaNonSekolahsRelatedByPenggunaId($v = true)
    {
        $this->collLembagaNonSekolahsRelatedByPenggunaIdPartial = $v;
    }

    /**
     * Initializes the collLembagaNonSekolahsRelatedByPenggunaId collection.
     *
     * By default this just sets the collLembagaNonSekolahsRelatedByPenggunaId collection to an empty array (like clearcollLembagaNonSekolahsRelatedByPenggunaId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initLembagaNonSekolahsRelatedByPenggunaId($overrideExisting = true)
    {
        if (null !== $this->collLembagaNonSekolahsRelatedByPenggunaId && !$overrideExisting) {
            return;
        }
        $this->collLembagaNonSekolahsRelatedByPenggunaId = new PropelObjectCollection();
        $this->collLembagaNonSekolahsRelatedByPenggunaId->setModel('LembagaNonSekolah');
    }

    /**
     * Gets an array of LembagaNonSekolah objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Pengguna is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|LembagaNonSekolah[] List of LembagaNonSekolah objects
     * @throws PropelException
     */
    public function getLembagaNonSekolahsRelatedByPenggunaId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collLembagaNonSekolahsRelatedByPenggunaIdPartial && !$this->isNew();
        if (null === $this->collLembagaNonSekolahsRelatedByPenggunaId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collLembagaNonSekolahsRelatedByPenggunaId) {
                // return empty collection
                $this->initLembagaNonSekolahsRelatedByPenggunaId();
            } else {
                $collLembagaNonSekolahsRelatedByPenggunaId = LembagaNonSekolahQuery::create(null, $criteria)
                    ->filterByPenggunaRelatedByPenggunaId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collLembagaNonSekolahsRelatedByPenggunaIdPartial && count($collLembagaNonSekolahsRelatedByPenggunaId)) {
                      $this->initLembagaNonSekolahsRelatedByPenggunaId(false);

                      foreach($collLembagaNonSekolahsRelatedByPenggunaId as $obj) {
                        if (false == $this->collLembagaNonSekolahsRelatedByPenggunaId->contains($obj)) {
                          $this->collLembagaNonSekolahsRelatedByPenggunaId->append($obj);
                        }
                      }

                      $this->collLembagaNonSekolahsRelatedByPenggunaIdPartial = true;
                    }

                    $collLembagaNonSekolahsRelatedByPenggunaId->getInternalIterator()->rewind();
                    return $collLembagaNonSekolahsRelatedByPenggunaId;
                }

                if($partial && $this->collLembagaNonSekolahsRelatedByPenggunaId) {
                    foreach($this->collLembagaNonSekolahsRelatedByPenggunaId as $obj) {
                        if($obj->isNew()) {
                            $collLembagaNonSekolahsRelatedByPenggunaId[] = $obj;
                        }
                    }
                }

                $this->collLembagaNonSekolahsRelatedByPenggunaId = $collLembagaNonSekolahsRelatedByPenggunaId;
                $this->collLembagaNonSekolahsRelatedByPenggunaIdPartial = false;
            }
        }

        return $this->collLembagaNonSekolahsRelatedByPenggunaId;
    }

    /**
     * Sets a collection of LembagaNonSekolahRelatedByPenggunaId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $lembagaNonSekolahsRelatedByPenggunaId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Pengguna The current object (for fluent API support)
     */
    public function setLembagaNonSekolahsRelatedByPenggunaId(PropelCollection $lembagaNonSekolahsRelatedByPenggunaId, PropelPDO $con = null)
    {
        $lembagaNonSekolahsRelatedByPenggunaIdToDelete = $this->getLembagaNonSekolahsRelatedByPenggunaId(new Criteria(), $con)->diff($lembagaNonSekolahsRelatedByPenggunaId);

        $this->lembagaNonSekolahsRelatedByPenggunaIdScheduledForDeletion = unserialize(serialize($lembagaNonSekolahsRelatedByPenggunaIdToDelete));

        foreach ($lembagaNonSekolahsRelatedByPenggunaIdToDelete as $lembagaNonSekolahRelatedByPenggunaIdRemoved) {
            $lembagaNonSekolahRelatedByPenggunaIdRemoved->setPenggunaRelatedByPenggunaId(null);
        }

        $this->collLembagaNonSekolahsRelatedByPenggunaId = null;
        foreach ($lembagaNonSekolahsRelatedByPenggunaId as $lembagaNonSekolahRelatedByPenggunaId) {
            $this->addLembagaNonSekolahRelatedByPenggunaId($lembagaNonSekolahRelatedByPenggunaId);
        }

        $this->collLembagaNonSekolahsRelatedByPenggunaId = $lembagaNonSekolahsRelatedByPenggunaId;
        $this->collLembagaNonSekolahsRelatedByPenggunaIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related LembagaNonSekolah objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related LembagaNonSekolah objects.
     * @throws PropelException
     */
    public function countLembagaNonSekolahsRelatedByPenggunaId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collLembagaNonSekolahsRelatedByPenggunaIdPartial && !$this->isNew();
        if (null === $this->collLembagaNonSekolahsRelatedByPenggunaId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collLembagaNonSekolahsRelatedByPenggunaId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getLembagaNonSekolahsRelatedByPenggunaId());
            }
            $query = LembagaNonSekolahQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPenggunaRelatedByPenggunaId($this)
                ->count($con);
        }

        return count($this->collLembagaNonSekolahsRelatedByPenggunaId);
    }

    /**
     * Method called to associate a LembagaNonSekolah object to this object
     * through the LembagaNonSekolah foreign key attribute.
     *
     * @param    LembagaNonSekolah $l LembagaNonSekolah
     * @return Pengguna The current object (for fluent API support)
     */
    public function addLembagaNonSekolahRelatedByPenggunaId(LembagaNonSekolah $l)
    {
        if ($this->collLembagaNonSekolahsRelatedByPenggunaId === null) {
            $this->initLembagaNonSekolahsRelatedByPenggunaId();
            $this->collLembagaNonSekolahsRelatedByPenggunaIdPartial = true;
        }
        if (!in_array($l, $this->collLembagaNonSekolahsRelatedByPenggunaId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddLembagaNonSekolahRelatedByPenggunaId($l);
        }

        return $this;
    }

    /**
     * @param	LembagaNonSekolahRelatedByPenggunaId $lembagaNonSekolahRelatedByPenggunaId The lembagaNonSekolahRelatedByPenggunaId object to add.
     */
    protected function doAddLembagaNonSekolahRelatedByPenggunaId($lembagaNonSekolahRelatedByPenggunaId)
    {
        $this->collLembagaNonSekolahsRelatedByPenggunaId[]= $lembagaNonSekolahRelatedByPenggunaId;
        $lembagaNonSekolahRelatedByPenggunaId->setPenggunaRelatedByPenggunaId($this);
    }

    /**
     * @param	LembagaNonSekolahRelatedByPenggunaId $lembagaNonSekolahRelatedByPenggunaId The lembagaNonSekolahRelatedByPenggunaId object to remove.
     * @return Pengguna The current object (for fluent API support)
     */
    public function removeLembagaNonSekolahRelatedByPenggunaId($lembagaNonSekolahRelatedByPenggunaId)
    {
        if ($this->getLembagaNonSekolahsRelatedByPenggunaId()->contains($lembagaNonSekolahRelatedByPenggunaId)) {
            $this->collLembagaNonSekolahsRelatedByPenggunaId->remove($this->collLembagaNonSekolahsRelatedByPenggunaId->search($lembagaNonSekolahRelatedByPenggunaId));
            if (null === $this->lembagaNonSekolahsRelatedByPenggunaIdScheduledForDeletion) {
                $this->lembagaNonSekolahsRelatedByPenggunaIdScheduledForDeletion = clone $this->collLembagaNonSekolahsRelatedByPenggunaId;
                $this->lembagaNonSekolahsRelatedByPenggunaIdScheduledForDeletion->clear();
            }
            $this->lembagaNonSekolahsRelatedByPenggunaIdScheduledForDeletion[]= $lembagaNonSekolahRelatedByPenggunaId;
            $lembagaNonSekolahRelatedByPenggunaId->setPenggunaRelatedByPenggunaId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pengguna is new, it will return
     * an empty collection; or if this Pengguna has previously
     * been saved, it will retrieve related LembagaNonSekolahsRelatedByPenggunaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pengguna.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|LembagaNonSekolah[] List of LembagaNonSekolah objects
     */
    public function getLembagaNonSekolahsRelatedByPenggunaIdJoinJenisLembagaRelatedByJenisLembagaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = LembagaNonSekolahQuery::create(null, $criteria);
        $query->joinWith('JenisLembagaRelatedByJenisLembagaId', $join_behavior);

        return $this->getLembagaNonSekolahsRelatedByPenggunaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pengguna is new, it will return
     * an empty collection; or if this Pengguna has previously
     * been saved, it will retrieve related LembagaNonSekolahsRelatedByPenggunaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pengguna.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|LembagaNonSekolah[] List of LembagaNonSekolah objects
     */
    public function getLembagaNonSekolahsRelatedByPenggunaIdJoinJenisLembagaRelatedByJenisLembagaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = LembagaNonSekolahQuery::create(null, $criteria);
        $query->joinWith('JenisLembagaRelatedByJenisLembagaId', $join_behavior);

        return $this->getLembagaNonSekolahsRelatedByPenggunaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pengguna is new, it will return
     * an empty collection; or if this Pengguna has previously
     * been saved, it will retrieve related LembagaNonSekolahsRelatedByPenggunaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pengguna.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|LembagaNonSekolah[] List of LembagaNonSekolah objects
     */
    public function getLembagaNonSekolahsRelatedByPenggunaIdJoinMstWilayahRelatedByKodeWilayah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = LembagaNonSekolahQuery::create(null, $criteria);
        $query->joinWith('MstWilayahRelatedByKodeWilayah', $join_behavior);

        return $this->getLembagaNonSekolahsRelatedByPenggunaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pengguna is new, it will return
     * an empty collection; or if this Pengguna has previously
     * been saved, it will retrieve related LembagaNonSekolahsRelatedByPenggunaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pengguna.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|LembagaNonSekolah[] List of LembagaNonSekolah objects
     */
    public function getLembagaNonSekolahsRelatedByPenggunaIdJoinMstWilayahRelatedByKodeWilayah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = LembagaNonSekolahQuery::create(null, $criteria);
        $query->joinWith('MstWilayahRelatedByKodeWilayah', $join_behavior);

        return $this->getLembagaNonSekolahsRelatedByPenggunaId($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->pengguna_id = null;
        $this->sekolah_id = null;
        $this->lembaga_id = null;
        $this->yayasan_id = null;
        $this->la_id = null;
        $this->peran_id = null;
        $this->username = null;
        $this->password = null;
        $this->nama = null;
        $this->nip_nim = null;
        $this->jabatan_lembaga = null;
        $this->ym = null;
        $this->skype = null;
        $this->alamat = null;
        $this->kode_wilayah = null;
        $this->no_telepon = null;
        $this->no_hp = null;
        $this->aktif = null;
        $this->last_update = null;
        $this->soft_delete = null;
        $this->last_sync = null;
        $this->updater_id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collLogPenggunasRelatedByPenggunaId) {
                foreach ($this->collLogPenggunasRelatedByPenggunaId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collLogPenggunasRelatedByPenggunaId) {
                foreach ($this->collLogPenggunasRelatedByPenggunaId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSasaranSurveysRelatedByPenggunaId) {
                foreach ($this->collSasaranSurveysRelatedByPenggunaId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSasaranSurveysRelatedByPenggunaId) {
                foreach ($this->collSasaranSurveysRelatedByPenggunaId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collLembagaNonSekolahsRelatedByPenggunaId) {
                foreach ($this->collLembagaNonSekolahsRelatedByPenggunaId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collLembagaNonSekolahsRelatedByPenggunaId) {
                foreach ($this->collLembagaNonSekolahsRelatedByPenggunaId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aLembagaNonSekolahRelatedByLembagaId instanceof Persistent) {
              $this->aLembagaNonSekolahRelatedByLembagaId->clearAllReferences($deep);
            }
            if ($this->aLembagaNonSekolahRelatedByLembagaId instanceof Persistent) {
              $this->aLembagaNonSekolahRelatedByLembagaId->clearAllReferences($deep);
            }
            if ($this->aSekolahRelatedBySekolahId instanceof Persistent) {
              $this->aSekolahRelatedBySekolahId->clearAllReferences($deep);
            }
            if ($this->aSekolahRelatedBySekolahId instanceof Persistent) {
              $this->aSekolahRelatedBySekolahId->clearAllReferences($deep);
            }
            if ($this->aMstWilayahRelatedByKodeWilayah instanceof Persistent) {
              $this->aMstWilayahRelatedByKodeWilayah->clearAllReferences($deep);
            }
            if ($this->aMstWilayahRelatedByKodeWilayah instanceof Persistent) {
              $this->aMstWilayahRelatedByKodeWilayah->clearAllReferences($deep);
            }
            if ($this->aPeranRelatedByPeranId instanceof Persistent) {
              $this->aPeranRelatedByPeranId->clearAllReferences($deep);
            }
            if ($this->aPeranRelatedByPeranId instanceof Persistent) {
              $this->aPeranRelatedByPeranId->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collLogPenggunasRelatedByPenggunaId instanceof PropelCollection) {
            $this->collLogPenggunasRelatedByPenggunaId->clearIterator();
        }
        $this->collLogPenggunasRelatedByPenggunaId = null;
        if ($this->collLogPenggunasRelatedByPenggunaId instanceof PropelCollection) {
            $this->collLogPenggunasRelatedByPenggunaId->clearIterator();
        }
        $this->collLogPenggunasRelatedByPenggunaId = null;
        if ($this->collSasaranSurveysRelatedByPenggunaId instanceof PropelCollection) {
            $this->collSasaranSurveysRelatedByPenggunaId->clearIterator();
        }
        $this->collSasaranSurveysRelatedByPenggunaId = null;
        if ($this->collSasaranSurveysRelatedByPenggunaId instanceof PropelCollection) {
            $this->collSasaranSurveysRelatedByPenggunaId->clearIterator();
        }
        $this->collSasaranSurveysRelatedByPenggunaId = null;
        if ($this->collLembagaNonSekolahsRelatedByPenggunaId instanceof PropelCollection) {
            $this->collLembagaNonSekolahsRelatedByPenggunaId->clearIterator();
        }
        $this->collLembagaNonSekolahsRelatedByPenggunaId = null;
        if ($this->collLembagaNonSekolahsRelatedByPenggunaId instanceof PropelCollection) {
            $this->collLembagaNonSekolahsRelatedByPenggunaId->clearIterator();
        }
        $this->collLembagaNonSekolahsRelatedByPenggunaId = null;
        $this->aLembagaNonSekolahRelatedByLembagaId = null;
        $this->aLembagaNonSekolahRelatedByLembagaId = null;
        $this->aSekolahRelatedBySekolahId = null;
        $this->aSekolahRelatedBySekolahId = null;
        $this->aMstWilayahRelatedByKodeWilayah = null;
        $this->aMstWilayahRelatedByKodeWilayah = null;
        $this->aPeranRelatedByPeranId = null;
        $this->aPeranRelatedByPeranId = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(PenggunaPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
