<?php

namespace angulex\Model\om;

use \BasePeer;
use \Criteria;
use \PDO;
use \PDOStatement;
use \Propel;
use \PropelException;
use \PropelPDO;
use angulex\Model\MstWilayahPeer;
use angulex\Model\SekolahPeer;
use angulex\Model\TableSyncPeer;
use angulex\Model\WtDstSyncLog;
use angulex\Model\WtDstSyncLogPeer;
use angulex\Model\map\WtDstSyncLogTableMap;

/**
 * Base static class for performing query and update operations on the 'wt_dst_sync_log' table.
 *
 * 
 *
 * @package propel.generator.angulex.Model.om
 */
abstract class BaseWtDstSyncLogPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'Dapodikmen';

    /** the table name for this class */
    const TABLE_NAME = 'wt_dst_sync_log';

    /** the related Propel class for this table */
    const OM_CLASS = 'angulex\\Model\\WtDstSyncLog';

    /** the related TableMap class for this table */
    const TM_CLASS = 'WtDstSyncLogTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 14;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 14;

    /** the column name for the kode_wilayah field */
    const KODE_WILAYAH = 'wt_dst_sync_log.kode_wilayah';

    /** the column name for the sekolah_id field */
    const SEKOLAH_ID = 'wt_dst_sync_log.sekolah_id';

    /** the column name for the table_name field */
    const TABLE_NAME = 'wt_dst_sync_log.table_name';

    /** the column name for the begin_sync field */
    const BEGIN_SYNC = 'wt_dst_sync_log.begin_sync';

    /** the column name for the end_sync field */
    const END_SYNC = 'wt_dst_sync_log.end_sync';

    /** the column name for the sync_media field */
    const SYNC_MEDIA = 'wt_dst_sync_log.sync_media';

    /** the column name for the is_success field */
    const IS_SUCCESS = 'wt_dst_sync_log.is_success';

    /** the column name for the n_create field */
    const N_CREATE = 'wt_dst_sync_log.n_create';

    /** the column name for the n_update field */
    const N_UPDATE = 'wt_dst_sync_log.n_update';

    /** the column name for the n_hapus field */
    const N_HAPUS = 'wt_dst_sync_log.n_hapus';

    /** the column name for the n_konflik field */
    const N_KONFLIK = 'wt_dst_sync_log.n_konflik';

    /** the column name for the selisih_waktu_server field */
    const SELISIH_WAKTU_SERVER = 'wt_dst_sync_log.selisih_waktu_server';

    /** the column name for the alamat_ip field */
    const ALAMAT_IP = 'wt_dst_sync_log.alamat_ip';

    /** the column name for the pengguna_id field */
    const PENGGUNA_ID = 'wt_dst_sync_log.pengguna_id';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identiy map to hold any loaded instances of WtDstSyncLog objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array WtDstSyncLog[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. WtDstSyncLogPeer::$fieldNames[WtDstSyncLogPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('KodeWilayah', 'SekolahId', 'TableName', 'BeginSync', 'EndSync', 'SyncMedia', 'IsSuccess', 'NCreate', 'NUpdate', 'NHapus', 'NKonflik', 'SelisihWaktuServer', 'AlamatIp', 'PenggunaId', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('kodeWilayah', 'sekolahId', 'tableName', 'beginSync', 'endSync', 'syncMedia', 'isSuccess', 'nCreate', 'nUpdate', 'nHapus', 'nKonflik', 'selisihWaktuServer', 'alamatIp', 'penggunaId', ),
        BasePeer::TYPE_COLNAME => array (WtDstSyncLogPeer::KODE_WILAYAH, WtDstSyncLogPeer::SEKOLAH_ID, WtDstSyncLogPeer::TABLE_NAME, WtDstSyncLogPeer::BEGIN_SYNC, WtDstSyncLogPeer::END_SYNC, WtDstSyncLogPeer::SYNC_MEDIA, WtDstSyncLogPeer::IS_SUCCESS, WtDstSyncLogPeer::N_CREATE, WtDstSyncLogPeer::N_UPDATE, WtDstSyncLogPeer::N_HAPUS, WtDstSyncLogPeer::N_KONFLIK, WtDstSyncLogPeer::SELISIH_WAKTU_SERVER, WtDstSyncLogPeer::ALAMAT_IP, WtDstSyncLogPeer::PENGGUNA_ID, ),
        BasePeer::TYPE_RAW_COLNAME => array ('KODE_WILAYAH', 'SEKOLAH_ID', 'TABLE_NAME', 'BEGIN_SYNC', 'END_SYNC', 'SYNC_MEDIA', 'IS_SUCCESS', 'N_CREATE', 'N_UPDATE', 'N_HAPUS', 'N_KONFLIK', 'SELISIH_WAKTU_SERVER', 'ALAMAT_IP', 'PENGGUNA_ID', ),
        BasePeer::TYPE_FIELDNAME => array ('kode_wilayah', 'sekolah_id', 'table_name', 'begin_sync', 'end_sync', 'sync_media', 'is_success', 'n_create', 'n_update', 'n_hapus', 'n_konflik', 'selisih_waktu_server', 'alamat_ip', 'pengguna_id', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. WtDstSyncLogPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('KodeWilayah' => 0, 'SekolahId' => 1, 'TableName' => 2, 'BeginSync' => 3, 'EndSync' => 4, 'SyncMedia' => 5, 'IsSuccess' => 6, 'NCreate' => 7, 'NUpdate' => 8, 'NHapus' => 9, 'NKonflik' => 10, 'SelisihWaktuServer' => 11, 'AlamatIp' => 12, 'PenggunaId' => 13, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('kodeWilayah' => 0, 'sekolahId' => 1, 'tableName' => 2, 'beginSync' => 3, 'endSync' => 4, 'syncMedia' => 5, 'isSuccess' => 6, 'nCreate' => 7, 'nUpdate' => 8, 'nHapus' => 9, 'nKonflik' => 10, 'selisihWaktuServer' => 11, 'alamatIp' => 12, 'penggunaId' => 13, ),
        BasePeer::TYPE_COLNAME => array (WtDstSyncLogPeer::KODE_WILAYAH => 0, WtDstSyncLogPeer::SEKOLAH_ID => 1, WtDstSyncLogPeer::TABLE_NAME => 2, WtDstSyncLogPeer::BEGIN_SYNC => 3, WtDstSyncLogPeer::END_SYNC => 4, WtDstSyncLogPeer::SYNC_MEDIA => 5, WtDstSyncLogPeer::IS_SUCCESS => 6, WtDstSyncLogPeer::N_CREATE => 7, WtDstSyncLogPeer::N_UPDATE => 8, WtDstSyncLogPeer::N_HAPUS => 9, WtDstSyncLogPeer::N_KONFLIK => 10, WtDstSyncLogPeer::SELISIH_WAKTU_SERVER => 11, WtDstSyncLogPeer::ALAMAT_IP => 12, WtDstSyncLogPeer::PENGGUNA_ID => 13, ),
        BasePeer::TYPE_RAW_COLNAME => array ('KODE_WILAYAH' => 0, 'SEKOLAH_ID' => 1, 'TABLE_NAME' => 2, 'BEGIN_SYNC' => 3, 'END_SYNC' => 4, 'SYNC_MEDIA' => 5, 'IS_SUCCESS' => 6, 'N_CREATE' => 7, 'N_UPDATE' => 8, 'N_HAPUS' => 9, 'N_KONFLIK' => 10, 'SELISIH_WAKTU_SERVER' => 11, 'ALAMAT_IP' => 12, 'PENGGUNA_ID' => 13, ),
        BasePeer::TYPE_FIELDNAME => array ('kode_wilayah' => 0, 'sekolah_id' => 1, 'table_name' => 2, 'begin_sync' => 3, 'end_sync' => 4, 'sync_media' => 5, 'is_success' => 6, 'n_create' => 7, 'n_update' => 8, 'n_hapus' => 9, 'n_konflik' => 10, 'selisih_waktu_server' => 11, 'alamat_ip' => 12, 'pengguna_id' => 13, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = WtDstSyncLogPeer::getFieldNames($toType);
        $key = isset(WtDstSyncLogPeer::$fieldKeys[$fromType][$name]) ? WtDstSyncLogPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(WtDstSyncLogPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, WtDstSyncLogPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return WtDstSyncLogPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. WtDstSyncLogPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(WtDstSyncLogPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(WtDstSyncLogPeer::KODE_WILAYAH);
            $criteria->addSelectColumn(WtDstSyncLogPeer::SEKOLAH_ID);
            $criteria->addSelectColumn(WtDstSyncLogPeer::TABLE_NAME);
            $criteria->addSelectColumn(WtDstSyncLogPeer::BEGIN_SYNC);
            $criteria->addSelectColumn(WtDstSyncLogPeer::END_SYNC);
            $criteria->addSelectColumn(WtDstSyncLogPeer::SYNC_MEDIA);
            $criteria->addSelectColumn(WtDstSyncLogPeer::IS_SUCCESS);
            $criteria->addSelectColumn(WtDstSyncLogPeer::N_CREATE);
            $criteria->addSelectColumn(WtDstSyncLogPeer::N_UPDATE);
            $criteria->addSelectColumn(WtDstSyncLogPeer::N_HAPUS);
            $criteria->addSelectColumn(WtDstSyncLogPeer::N_KONFLIK);
            $criteria->addSelectColumn(WtDstSyncLogPeer::SELISIH_WAKTU_SERVER);
            $criteria->addSelectColumn(WtDstSyncLogPeer::ALAMAT_IP);
            $criteria->addSelectColumn(WtDstSyncLogPeer::PENGGUNA_ID);
        } else {
            $criteria->addSelectColumn($alias . '.kode_wilayah');
            $criteria->addSelectColumn($alias . '.sekolah_id');
            $criteria->addSelectColumn($alias . '.table_name');
            $criteria->addSelectColumn($alias . '.begin_sync');
            $criteria->addSelectColumn($alias . '.end_sync');
            $criteria->addSelectColumn($alias . '.sync_media');
            $criteria->addSelectColumn($alias . '.is_success');
            $criteria->addSelectColumn($alias . '.n_create');
            $criteria->addSelectColumn($alias . '.n_update');
            $criteria->addSelectColumn($alias . '.n_hapus');
            $criteria->addSelectColumn($alias . '.n_konflik');
            $criteria->addSelectColumn($alias . '.selisih_waktu_server');
            $criteria->addSelectColumn($alias . '.alamat_ip');
            $criteria->addSelectColumn($alias . '.pengguna_id');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(WtDstSyncLogPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            WtDstSyncLogPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(WtDstSyncLogPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(WtDstSyncLogPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 WtDstSyncLog
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = WtDstSyncLogPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return WtDstSyncLogPeer::populateObjects(WtDstSyncLogPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(WtDstSyncLogPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            WtDstSyncLogPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(WtDstSyncLogPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      WtDstSyncLog $obj A WtDstSyncLog object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = serialize(array((string) $obj->getKodeWilayah(), (string) $obj->getSekolahId(), (string) $obj->getTableName()));
            } // if key === null
            WtDstSyncLogPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A WtDstSyncLog object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof WtDstSyncLog) {
                $key = serialize(array((string) $value->getKodeWilayah(), (string) $value->getSekolahId(), (string) $value->getTableName()));
            } elseif (is_array($value) && count($value) === 3) {
                // assume we've been passed a primary key
                $key = serialize(array((string) $value[0], (string) $value[1], (string) $value[2]));
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or WtDstSyncLog object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(WtDstSyncLogPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   WtDstSyncLog Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(WtDstSyncLogPeer::$instances[$key])) {
                return WtDstSyncLogPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }
    
    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references)
      {
        foreach (WtDstSyncLogPeer::$instances as $instance)
        {
          $instance->clearAllReferences(true);
        }
      }
        WtDstSyncLogPeer::$instances = array();
    }
    
    /**
     * Method to invalidate the instance pool of all tables related to wt_dst_sync_log
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null && $row[$startcol + 1] === null && $row[$startcol + 2] === null) {
            return null;
        }

        return serialize(array((string) $row[$startcol], (string) $row[$startcol + 1], (string) $row[$startcol + 2]));
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return array((string) $row[$startcol], (string) $row[$startcol + 1], (string) $row[$startcol + 2]);
    }
    
    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();
    
        // set the class once to avoid overhead in the loop
        $cls = WtDstSyncLogPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = WtDstSyncLogPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = WtDstSyncLogPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                WtDstSyncLogPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (WtDstSyncLog object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = WtDstSyncLogPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = WtDstSyncLogPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + WtDstSyncLogPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = WtDstSyncLogPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            WtDstSyncLogPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related SekolahRelatedBySekolahId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinSekolahRelatedBySekolahId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(WtDstSyncLogPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            WtDstSyncLogPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(WtDstSyncLogPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(WtDstSyncLogPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(WtDstSyncLogPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SekolahRelatedBySekolahId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinSekolahRelatedBySekolahId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(WtDstSyncLogPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            WtDstSyncLogPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(WtDstSyncLogPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(WtDstSyncLogPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(WtDstSyncLogPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related MstWilayahRelatedByKodeWilayah table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinMstWilayahRelatedByKodeWilayah(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(WtDstSyncLogPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            WtDstSyncLogPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(WtDstSyncLogPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(WtDstSyncLogPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(WtDstSyncLogPeer::KODE_WILAYAH, MstWilayahPeer::KODE_WILAYAH, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related MstWilayahRelatedByKodeWilayah table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinMstWilayahRelatedByKodeWilayah(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(WtDstSyncLogPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            WtDstSyncLogPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(WtDstSyncLogPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(WtDstSyncLogPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(WtDstSyncLogPeer::KODE_WILAYAH, MstWilayahPeer::KODE_WILAYAH, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related TableSyncRelatedByTableName table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinTableSyncRelatedByTableName(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(WtDstSyncLogPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            WtDstSyncLogPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(WtDstSyncLogPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(WtDstSyncLogPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(WtDstSyncLogPeer::TABLE_NAME, TableSyncPeer::TABLE_NAME, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related TableSyncRelatedByTableName table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinTableSyncRelatedByTableName(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(WtDstSyncLogPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            WtDstSyncLogPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(WtDstSyncLogPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(WtDstSyncLogPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(WtDstSyncLogPeer::TABLE_NAME, TableSyncPeer::TABLE_NAME, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of WtDstSyncLog objects pre-filled with their Sekolah objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of WtDstSyncLog objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinSekolahRelatedBySekolahId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(WtDstSyncLogPeer::DATABASE_NAME);
        }

        WtDstSyncLogPeer::addSelectColumns($criteria);
        $startcol = WtDstSyncLogPeer::NUM_HYDRATE_COLUMNS;
        SekolahPeer::addSelectColumns($criteria);

        $criteria->addJoin(WtDstSyncLogPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = WtDstSyncLogPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = WtDstSyncLogPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = WtDstSyncLogPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                WtDstSyncLogPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = SekolahPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = SekolahPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    SekolahPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (WtDstSyncLog) to $obj2 (Sekolah)
                $obj2->addWtDstSyncLogRelatedBySekolahId($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of WtDstSyncLog objects pre-filled with their Sekolah objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of WtDstSyncLog objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinSekolahRelatedBySekolahId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(WtDstSyncLogPeer::DATABASE_NAME);
        }

        WtDstSyncLogPeer::addSelectColumns($criteria);
        $startcol = WtDstSyncLogPeer::NUM_HYDRATE_COLUMNS;
        SekolahPeer::addSelectColumns($criteria);

        $criteria->addJoin(WtDstSyncLogPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = WtDstSyncLogPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = WtDstSyncLogPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = WtDstSyncLogPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                WtDstSyncLogPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = SekolahPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = SekolahPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    SekolahPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (WtDstSyncLog) to $obj2 (Sekolah)
                $obj2->addWtDstSyncLogRelatedBySekolahId($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of WtDstSyncLog objects pre-filled with their MstWilayah objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of WtDstSyncLog objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinMstWilayahRelatedByKodeWilayah(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(WtDstSyncLogPeer::DATABASE_NAME);
        }

        WtDstSyncLogPeer::addSelectColumns($criteria);
        $startcol = WtDstSyncLogPeer::NUM_HYDRATE_COLUMNS;
        MstWilayahPeer::addSelectColumns($criteria);

        $criteria->addJoin(WtDstSyncLogPeer::KODE_WILAYAH, MstWilayahPeer::KODE_WILAYAH, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = WtDstSyncLogPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = WtDstSyncLogPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = WtDstSyncLogPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                WtDstSyncLogPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = MstWilayahPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = MstWilayahPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = MstWilayahPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    MstWilayahPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (WtDstSyncLog) to $obj2 (MstWilayah)
                $obj2->addWtDstSyncLogRelatedByKodeWilayah($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of WtDstSyncLog objects pre-filled with their MstWilayah objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of WtDstSyncLog objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinMstWilayahRelatedByKodeWilayah(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(WtDstSyncLogPeer::DATABASE_NAME);
        }

        WtDstSyncLogPeer::addSelectColumns($criteria);
        $startcol = WtDstSyncLogPeer::NUM_HYDRATE_COLUMNS;
        MstWilayahPeer::addSelectColumns($criteria);

        $criteria->addJoin(WtDstSyncLogPeer::KODE_WILAYAH, MstWilayahPeer::KODE_WILAYAH, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = WtDstSyncLogPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = WtDstSyncLogPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = WtDstSyncLogPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                WtDstSyncLogPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = MstWilayahPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = MstWilayahPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = MstWilayahPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    MstWilayahPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (WtDstSyncLog) to $obj2 (MstWilayah)
                $obj2->addWtDstSyncLogRelatedByKodeWilayah($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of WtDstSyncLog objects pre-filled with their TableSync objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of WtDstSyncLog objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinTableSyncRelatedByTableName(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(WtDstSyncLogPeer::DATABASE_NAME);
        }

        WtDstSyncLogPeer::addSelectColumns($criteria);
        $startcol = WtDstSyncLogPeer::NUM_HYDRATE_COLUMNS;
        TableSyncPeer::addSelectColumns($criteria);

        $criteria->addJoin(WtDstSyncLogPeer::TABLE_NAME, TableSyncPeer::TABLE_NAME, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = WtDstSyncLogPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = WtDstSyncLogPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = WtDstSyncLogPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                WtDstSyncLogPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = TableSyncPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = TableSyncPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = TableSyncPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    TableSyncPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (WtDstSyncLog) to $obj2 (TableSync)
                $obj2->addWtDstSyncLogRelatedByTableName($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of WtDstSyncLog objects pre-filled with their TableSync objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of WtDstSyncLog objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinTableSyncRelatedByTableName(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(WtDstSyncLogPeer::DATABASE_NAME);
        }

        WtDstSyncLogPeer::addSelectColumns($criteria);
        $startcol = WtDstSyncLogPeer::NUM_HYDRATE_COLUMNS;
        TableSyncPeer::addSelectColumns($criteria);

        $criteria->addJoin(WtDstSyncLogPeer::TABLE_NAME, TableSyncPeer::TABLE_NAME, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = WtDstSyncLogPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = WtDstSyncLogPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = WtDstSyncLogPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                WtDstSyncLogPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = TableSyncPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = TableSyncPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = TableSyncPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    TableSyncPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (WtDstSyncLog) to $obj2 (TableSync)
                $obj2->addWtDstSyncLogRelatedByTableName($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(WtDstSyncLogPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            WtDstSyncLogPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(WtDstSyncLogPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(WtDstSyncLogPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(WtDstSyncLogPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::KODE_WILAYAH, MstWilayahPeer::KODE_WILAYAH, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::KODE_WILAYAH, MstWilayahPeer::KODE_WILAYAH, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::TABLE_NAME, TableSyncPeer::TABLE_NAME, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::TABLE_NAME, TableSyncPeer::TABLE_NAME, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of WtDstSyncLog objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of WtDstSyncLog objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(WtDstSyncLogPeer::DATABASE_NAME);
        }

        WtDstSyncLogPeer::addSelectColumns($criteria);
        $startcol2 = WtDstSyncLogPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        MstWilayahPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + MstWilayahPeer::NUM_HYDRATE_COLUMNS;

        MstWilayahPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + MstWilayahPeer::NUM_HYDRATE_COLUMNS;

        TableSyncPeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + TableSyncPeer::NUM_HYDRATE_COLUMNS;

        TableSyncPeer::addSelectColumns($criteria);
        $startcol8 = $startcol7 + TableSyncPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(WtDstSyncLogPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::KODE_WILAYAH, MstWilayahPeer::KODE_WILAYAH, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::KODE_WILAYAH, MstWilayahPeer::KODE_WILAYAH, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::TABLE_NAME, TableSyncPeer::TABLE_NAME, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::TABLE_NAME, TableSyncPeer::TABLE_NAME, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = WtDstSyncLogPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = WtDstSyncLogPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = WtDstSyncLogPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                WtDstSyncLogPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined Sekolah rows

            $key2 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = SekolahPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = SekolahPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SekolahPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (WtDstSyncLog) to the collection in $obj2 (Sekolah)
                $obj2->addWtDstSyncLogRelatedBySekolahId($obj1);
            } // if joined row not null

            // Add objects for joined Sekolah rows

            $key3 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol3);
            if ($key3 !== null) {
                $obj3 = SekolahPeer::getInstanceFromPool($key3);
                if (!$obj3) {

                    $cls = SekolahPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    SekolahPeer::addInstanceToPool($obj3, $key3);
                } // if obj3 loaded

                // Add the $obj1 (WtDstSyncLog) to the collection in $obj3 (Sekolah)
                $obj3->addWtDstSyncLogRelatedBySekolahId($obj1);
            } // if joined row not null

            // Add objects for joined MstWilayah rows

            $key4 = MstWilayahPeer::getPrimaryKeyHashFromRow($row, $startcol4);
            if ($key4 !== null) {
                $obj4 = MstWilayahPeer::getInstanceFromPool($key4);
                if (!$obj4) {

                    $cls = MstWilayahPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    MstWilayahPeer::addInstanceToPool($obj4, $key4);
                } // if obj4 loaded

                // Add the $obj1 (WtDstSyncLog) to the collection in $obj4 (MstWilayah)
                $obj4->addWtDstSyncLogRelatedByKodeWilayah($obj1);
            } // if joined row not null

            // Add objects for joined MstWilayah rows

            $key5 = MstWilayahPeer::getPrimaryKeyHashFromRow($row, $startcol5);
            if ($key5 !== null) {
                $obj5 = MstWilayahPeer::getInstanceFromPool($key5);
                if (!$obj5) {

                    $cls = MstWilayahPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    MstWilayahPeer::addInstanceToPool($obj5, $key5);
                } // if obj5 loaded

                // Add the $obj1 (WtDstSyncLog) to the collection in $obj5 (MstWilayah)
                $obj5->addWtDstSyncLogRelatedByKodeWilayah($obj1);
            } // if joined row not null

            // Add objects for joined TableSync rows

            $key6 = TableSyncPeer::getPrimaryKeyHashFromRow($row, $startcol6);
            if ($key6 !== null) {
                $obj6 = TableSyncPeer::getInstanceFromPool($key6);
                if (!$obj6) {

                    $cls = TableSyncPeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    TableSyncPeer::addInstanceToPool($obj6, $key6);
                } // if obj6 loaded

                // Add the $obj1 (WtDstSyncLog) to the collection in $obj6 (TableSync)
                $obj6->addWtDstSyncLogRelatedByTableName($obj1);
            } // if joined row not null

            // Add objects for joined TableSync rows

            $key7 = TableSyncPeer::getPrimaryKeyHashFromRow($row, $startcol7);
            if ($key7 !== null) {
                $obj7 = TableSyncPeer::getInstanceFromPool($key7);
                if (!$obj7) {

                    $cls = TableSyncPeer::getOMClass();

                    $obj7 = new $cls();
                    $obj7->hydrate($row, $startcol7);
                    TableSyncPeer::addInstanceToPool($obj7, $key7);
                } // if obj7 loaded

                // Add the $obj1 (WtDstSyncLog) to the collection in $obj7 (TableSync)
                $obj7->addWtDstSyncLogRelatedByTableName($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SekolahRelatedBySekolahId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptSekolahRelatedBySekolahId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(WtDstSyncLogPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            WtDstSyncLogPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(WtDstSyncLogPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(WtDstSyncLogPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addJoin(WtDstSyncLogPeer::KODE_WILAYAH, MstWilayahPeer::KODE_WILAYAH, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::KODE_WILAYAH, MstWilayahPeer::KODE_WILAYAH, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::TABLE_NAME, TableSyncPeer::TABLE_NAME, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::TABLE_NAME, TableSyncPeer::TABLE_NAME, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SekolahRelatedBySekolahId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptSekolahRelatedBySekolahId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(WtDstSyncLogPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            WtDstSyncLogPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(WtDstSyncLogPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(WtDstSyncLogPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addJoin(WtDstSyncLogPeer::KODE_WILAYAH, MstWilayahPeer::KODE_WILAYAH, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::KODE_WILAYAH, MstWilayahPeer::KODE_WILAYAH, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::TABLE_NAME, TableSyncPeer::TABLE_NAME, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::TABLE_NAME, TableSyncPeer::TABLE_NAME, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related MstWilayahRelatedByKodeWilayah table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptMstWilayahRelatedByKodeWilayah(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(WtDstSyncLogPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            WtDstSyncLogPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(WtDstSyncLogPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(WtDstSyncLogPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addJoin(WtDstSyncLogPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::TABLE_NAME, TableSyncPeer::TABLE_NAME, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::TABLE_NAME, TableSyncPeer::TABLE_NAME, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related MstWilayahRelatedByKodeWilayah table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptMstWilayahRelatedByKodeWilayah(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(WtDstSyncLogPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            WtDstSyncLogPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(WtDstSyncLogPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(WtDstSyncLogPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addJoin(WtDstSyncLogPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::TABLE_NAME, TableSyncPeer::TABLE_NAME, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::TABLE_NAME, TableSyncPeer::TABLE_NAME, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related TableSyncRelatedByTableName table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptTableSyncRelatedByTableName(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(WtDstSyncLogPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            WtDstSyncLogPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(WtDstSyncLogPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(WtDstSyncLogPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addJoin(WtDstSyncLogPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::KODE_WILAYAH, MstWilayahPeer::KODE_WILAYAH, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::KODE_WILAYAH, MstWilayahPeer::KODE_WILAYAH, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related TableSyncRelatedByTableName table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptTableSyncRelatedByTableName(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(WtDstSyncLogPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            WtDstSyncLogPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(WtDstSyncLogPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(WtDstSyncLogPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addJoin(WtDstSyncLogPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::KODE_WILAYAH, MstWilayahPeer::KODE_WILAYAH, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::KODE_WILAYAH, MstWilayahPeer::KODE_WILAYAH, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of WtDstSyncLog objects pre-filled with all related objects except SekolahRelatedBySekolahId.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of WtDstSyncLog objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptSekolahRelatedBySekolahId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(WtDstSyncLogPeer::DATABASE_NAME);
        }

        WtDstSyncLogPeer::addSelectColumns($criteria);
        $startcol2 = WtDstSyncLogPeer::NUM_HYDRATE_COLUMNS;

        MstWilayahPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + MstWilayahPeer::NUM_HYDRATE_COLUMNS;

        MstWilayahPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + MstWilayahPeer::NUM_HYDRATE_COLUMNS;

        TableSyncPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + TableSyncPeer::NUM_HYDRATE_COLUMNS;

        TableSyncPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + TableSyncPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(WtDstSyncLogPeer::KODE_WILAYAH, MstWilayahPeer::KODE_WILAYAH, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::KODE_WILAYAH, MstWilayahPeer::KODE_WILAYAH, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::TABLE_NAME, TableSyncPeer::TABLE_NAME, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::TABLE_NAME, TableSyncPeer::TABLE_NAME, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = WtDstSyncLogPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = WtDstSyncLogPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = WtDstSyncLogPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                WtDstSyncLogPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined MstWilayah rows

                $key2 = MstWilayahPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = MstWilayahPeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = MstWilayahPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    MstWilayahPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (WtDstSyncLog) to the collection in $obj2 (MstWilayah)
                $obj2->addWtDstSyncLogRelatedByKodeWilayah($obj1);

            } // if joined row is not null

                // Add objects for joined MstWilayah rows

                $key3 = MstWilayahPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = MstWilayahPeer::getInstanceFromPool($key3);
                    if (!$obj3) {
    
                        $cls = MstWilayahPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    MstWilayahPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (WtDstSyncLog) to the collection in $obj3 (MstWilayah)
                $obj3->addWtDstSyncLogRelatedByKodeWilayah($obj1);

            } // if joined row is not null

                // Add objects for joined TableSync rows

                $key4 = TableSyncPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = TableSyncPeer::getInstanceFromPool($key4);
                    if (!$obj4) {
    
                        $cls = TableSyncPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    TableSyncPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (WtDstSyncLog) to the collection in $obj4 (TableSync)
                $obj4->addWtDstSyncLogRelatedByTableName($obj1);

            } // if joined row is not null

                // Add objects for joined TableSync rows

                $key5 = TableSyncPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = TableSyncPeer::getInstanceFromPool($key5);
                    if (!$obj5) {
    
                        $cls = TableSyncPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    TableSyncPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (WtDstSyncLog) to the collection in $obj5 (TableSync)
                $obj5->addWtDstSyncLogRelatedByTableName($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of WtDstSyncLog objects pre-filled with all related objects except SekolahRelatedBySekolahId.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of WtDstSyncLog objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptSekolahRelatedBySekolahId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(WtDstSyncLogPeer::DATABASE_NAME);
        }

        WtDstSyncLogPeer::addSelectColumns($criteria);
        $startcol2 = WtDstSyncLogPeer::NUM_HYDRATE_COLUMNS;

        MstWilayahPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + MstWilayahPeer::NUM_HYDRATE_COLUMNS;

        MstWilayahPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + MstWilayahPeer::NUM_HYDRATE_COLUMNS;

        TableSyncPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + TableSyncPeer::NUM_HYDRATE_COLUMNS;

        TableSyncPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + TableSyncPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(WtDstSyncLogPeer::KODE_WILAYAH, MstWilayahPeer::KODE_WILAYAH, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::KODE_WILAYAH, MstWilayahPeer::KODE_WILAYAH, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::TABLE_NAME, TableSyncPeer::TABLE_NAME, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::TABLE_NAME, TableSyncPeer::TABLE_NAME, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = WtDstSyncLogPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = WtDstSyncLogPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = WtDstSyncLogPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                WtDstSyncLogPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined MstWilayah rows

                $key2 = MstWilayahPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = MstWilayahPeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = MstWilayahPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    MstWilayahPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (WtDstSyncLog) to the collection in $obj2 (MstWilayah)
                $obj2->addWtDstSyncLogRelatedByKodeWilayah($obj1);

            } // if joined row is not null

                // Add objects for joined MstWilayah rows

                $key3 = MstWilayahPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = MstWilayahPeer::getInstanceFromPool($key3);
                    if (!$obj3) {
    
                        $cls = MstWilayahPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    MstWilayahPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (WtDstSyncLog) to the collection in $obj3 (MstWilayah)
                $obj3->addWtDstSyncLogRelatedByKodeWilayah($obj1);

            } // if joined row is not null

                // Add objects for joined TableSync rows

                $key4 = TableSyncPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = TableSyncPeer::getInstanceFromPool($key4);
                    if (!$obj4) {
    
                        $cls = TableSyncPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    TableSyncPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (WtDstSyncLog) to the collection in $obj4 (TableSync)
                $obj4->addWtDstSyncLogRelatedByTableName($obj1);

            } // if joined row is not null

                // Add objects for joined TableSync rows

                $key5 = TableSyncPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = TableSyncPeer::getInstanceFromPool($key5);
                    if (!$obj5) {
    
                        $cls = TableSyncPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    TableSyncPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (WtDstSyncLog) to the collection in $obj5 (TableSync)
                $obj5->addWtDstSyncLogRelatedByTableName($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of WtDstSyncLog objects pre-filled with all related objects except MstWilayahRelatedByKodeWilayah.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of WtDstSyncLog objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptMstWilayahRelatedByKodeWilayah(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(WtDstSyncLogPeer::DATABASE_NAME);
        }

        WtDstSyncLogPeer::addSelectColumns($criteria);
        $startcol2 = WtDstSyncLogPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        TableSyncPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + TableSyncPeer::NUM_HYDRATE_COLUMNS;

        TableSyncPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + TableSyncPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(WtDstSyncLogPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::TABLE_NAME, TableSyncPeer::TABLE_NAME, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::TABLE_NAME, TableSyncPeer::TABLE_NAME, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = WtDstSyncLogPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = WtDstSyncLogPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = WtDstSyncLogPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                WtDstSyncLogPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Sekolah rows

                $key2 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = SekolahPeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = SekolahPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SekolahPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (WtDstSyncLog) to the collection in $obj2 (Sekolah)
                $obj2->addWtDstSyncLogRelatedBySekolahId($obj1);

            } // if joined row is not null

                // Add objects for joined Sekolah rows

                $key3 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = SekolahPeer::getInstanceFromPool($key3);
                    if (!$obj3) {
    
                        $cls = SekolahPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    SekolahPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (WtDstSyncLog) to the collection in $obj3 (Sekolah)
                $obj3->addWtDstSyncLogRelatedBySekolahId($obj1);

            } // if joined row is not null

                // Add objects for joined TableSync rows

                $key4 = TableSyncPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = TableSyncPeer::getInstanceFromPool($key4);
                    if (!$obj4) {
    
                        $cls = TableSyncPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    TableSyncPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (WtDstSyncLog) to the collection in $obj4 (TableSync)
                $obj4->addWtDstSyncLogRelatedByTableName($obj1);

            } // if joined row is not null

                // Add objects for joined TableSync rows

                $key5 = TableSyncPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = TableSyncPeer::getInstanceFromPool($key5);
                    if (!$obj5) {
    
                        $cls = TableSyncPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    TableSyncPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (WtDstSyncLog) to the collection in $obj5 (TableSync)
                $obj5->addWtDstSyncLogRelatedByTableName($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of WtDstSyncLog objects pre-filled with all related objects except MstWilayahRelatedByKodeWilayah.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of WtDstSyncLog objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptMstWilayahRelatedByKodeWilayah(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(WtDstSyncLogPeer::DATABASE_NAME);
        }

        WtDstSyncLogPeer::addSelectColumns($criteria);
        $startcol2 = WtDstSyncLogPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        TableSyncPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + TableSyncPeer::NUM_HYDRATE_COLUMNS;

        TableSyncPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + TableSyncPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(WtDstSyncLogPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::TABLE_NAME, TableSyncPeer::TABLE_NAME, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::TABLE_NAME, TableSyncPeer::TABLE_NAME, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = WtDstSyncLogPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = WtDstSyncLogPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = WtDstSyncLogPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                WtDstSyncLogPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Sekolah rows

                $key2 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = SekolahPeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = SekolahPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SekolahPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (WtDstSyncLog) to the collection in $obj2 (Sekolah)
                $obj2->addWtDstSyncLogRelatedBySekolahId($obj1);

            } // if joined row is not null

                // Add objects for joined Sekolah rows

                $key3 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = SekolahPeer::getInstanceFromPool($key3);
                    if (!$obj3) {
    
                        $cls = SekolahPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    SekolahPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (WtDstSyncLog) to the collection in $obj3 (Sekolah)
                $obj3->addWtDstSyncLogRelatedBySekolahId($obj1);

            } // if joined row is not null

                // Add objects for joined TableSync rows

                $key4 = TableSyncPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = TableSyncPeer::getInstanceFromPool($key4);
                    if (!$obj4) {
    
                        $cls = TableSyncPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    TableSyncPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (WtDstSyncLog) to the collection in $obj4 (TableSync)
                $obj4->addWtDstSyncLogRelatedByTableName($obj1);

            } // if joined row is not null

                // Add objects for joined TableSync rows

                $key5 = TableSyncPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = TableSyncPeer::getInstanceFromPool($key5);
                    if (!$obj5) {
    
                        $cls = TableSyncPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    TableSyncPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (WtDstSyncLog) to the collection in $obj5 (TableSync)
                $obj5->addWtDstSyncLogRelatedByTableName($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of WtDstSyncLog objects pre-filled with all related objects except TableSyncRelatedByTableName.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of WtDstSyncLog objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptTableSyncRelatedByTableName(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(WtDstSyncLogPeer::DATABASE_NAME);
        }

        WtDstSyncLogPeer::addSelectColumns($criteria);
        $startcol2 = WtDstSyncLogPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        MstWilayahPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + MstWilayahPeer::NUM_HYDRATE_COLUMNS;

        MstWilayahPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + MstWilayahPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(WtDstSyncLogPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::KODE_WILAYAH, MstWilayahPeer::KODE_WILAYAH, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::KODE_WILAYAH, MstWilayahPeer::KODE_WILAYAH, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = WtDstSyncLogPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = WtDstSyncLogPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = WtDstSyncLogPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                WtDstSyncLogPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Sekolah rows

                $key2 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = SekolahPeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = SekolahPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SekolahPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (WtDstSyncLog) to the collection in $obj2 (Sekolah)
                $obj2->addWtDstSyncLogRelatedBySekolahId($obj1);

            } // if joined row is not null

                // Add objects for joined Sekolah rows

                $key3 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = SekolahPeer::getInstanceFromPool($key3);
                    if (!$obj3) {
    
                        $cls = SekolahPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    SekolahPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (WtDstSyncLog) to the collection in $obj3 (Sekolah)
                $obj3->addWtDstSyncLogRelatedBySekolahId($obj1);

            } // if joined row is not null

                // Add objects for joined MstWilayah rows

                $key4 = MstWilayahPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = MstWilayahPeer::getInstanceFromPool($key4);
                    if (!$obj4) {
    
                        $cls = MstWilayahPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    MstWilayahPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (WtDstSyncLog) to the collection in $obj4 (MstWilayah)
                $obj4->addWtDstSyncLogRelatedByKodeWilayah($obj1);

            } // if joined row is not null

                // Add objects for joined MstWilayah rows

                $key5 = MstWilayahPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = MstWilayahPeer::getInstanceFromPool($key5);
                    if (!$obj5) {
    
                        $cls = MstWilayahPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    MstWilayahPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (WtDstSyncLog) to the collection in $obj5 (MstWilayah)
                $obj5->addWtDstSyncLogRelatedByKodeWilayah($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of WtDstSyncLog objects pre-filled with all related objects except TableSyncRelatedByTableName.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of WtDstSyncLog objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptTableSyncRelatedByTableName(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(WtDstSyncLogPeer::DATABASE_NAME);
        }

        WtDstSyncLogPeer::addSelectColumns($criteria);
        $startcol2 = WtDstSyncLogPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        MstWilayahPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + MstWilayahPeer::NUM_HYDRATE_COLUMNS;

        MstWilayahPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + MstWilayahPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(WtDstSyncLogPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::KODE_WILAYAH, MstWilayahPeer::KODE_WILAYAH, $join_behavior);

        $criteria->addJoin(WtDstSyncLogPeer::KODE_WILAYAH, MstWilayahPeer::KODE_WILAYAH, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = WtDstSyncLogPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = WtDstSyncLogPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = WtDstSyncLogPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                WtDstSyncLogPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Sekolah rows

                $key2 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = SekolahPeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = SekolahPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SekolahPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (WtDstSyncLog) to the collection in $obj2 (Sekolah)
                $obj2->addWtDstSyncLogRelatedBySekolahId($obj1);

            } // if joined row is not null

                // Add objects for joined Sekolah rows

                $key3 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = SekolahPeer::getInstanceFromPool($key3);
                    if (!$obj3) {
    
                        $cls = SekolahPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    SekolahPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (WtDstSyncLog) to the collection in $obj3 (Sekolah)
                $obj3->addWtDstSyncLogRelatedBySekolahId($obj1);

            } // if joined row is not null

                // Add objects for joined MstWilayah rows

                $key4 = MstWilayahPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = MstWilayahPeer::getInstanceFromPool($key4);
                    if (!$obj4) {
    
                        $cls = MstWilayahPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    MstWilayahPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (WtDstSyncLog) to the collection in $obj4 (MstWilayah)
                $obj4->addWtDstSyncLogRelatedByKodeWilayah($obj1);

            } // if joined row is not null

                // Add objects for joined MstWilayah rows

                $key5 = MstWilayahPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = MstWilayahPeer::getInstanceFromPool($key5);
                    if (!$obj5) {
    
                        $cls = MstWilayahPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    MstWilayahPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (WtDstSyncLog) to the collection in $obj5 (MstWilayah)
                $obj5->addWtDstSyncLogRelatedByKodeWilayah($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(WtDstSyncLogPeer::DATABASE_NAME)->getTable(WtDstSyncLogPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseWtDstSyncLogPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseWtDstSyncLogPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new WtDstSyncLogTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass()
    {
        return WtDstSyncLogPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a WtDstSyncLog or Criteria object.
     *
     * @param      mixed $values Criteria or WtDstSyncLog object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(WtDstSyncLogPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from WtDstSyncLog object
        }


        // Set the correct dbName
        $criteria->setDbName(WtDstSyncLogPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a WtDstSyncLog or Criteria object.
     *
     * @param      mixed $values Criteria or WtDstSyncLog object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(WtDstSyncLogPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(WtDstSyncLogPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(WtDstSyncLogPeer::KODE_WILAYAH);
            $value = $criteria->remove(WtDstSyncLogPeer::KODE_WILAYAH);
            if ($value) {
                $selectCriteria->add(WtDstSyncLogPeer::KODE_WILAYAH, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(WtDstSyncLogPeer::TABLE_NAME);
            }

            $comparison = $criteria->getComparison(WtDstSyncLogPeer::SEKOLAH_ID);
            $value = $criteria->remove(WtDstSyncLogPeer::SEKOLAH_ID);
            if ($value) {
                $selectCriteria->add(WtDstSyncLogPeer::SEKOLAH_ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(WtDstSyncLogPeer::TABLE_NAME);
            }

            $comparison = $criteria->getComparison(WtDstSyncLogPeer::TABLE_NAME);
            $value = $criteria->remove(WtDstSyncLogPeer::TABLE_NAME);
            if ($value) {
                $selectCriteria->add(WtDstSyncLogPeer::TABLE_NAME, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(WtDstSyncLogPeer::TABLE_NAME);
            }

        } else { // $values is WtDstSyncLog object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(WtDstSyncLogPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the wt_dst_sync_log table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(WtDstSyncLogPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(WtDstSyncLogPeer::TABLE_NAME, $con, WtDstSyncLogPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            WtDstSyncLogPeer::clearInstancePool();
            WtDstSyncLogPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a WtDstSyncLog or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or WtDstSyncLog object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(WtDstSyncLogPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            WtDstSyncLogPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof WtDstSyncLog) { // it's a model object
            // invalidate the cache for this single object
            WtDstSyncLogPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(WtDstSyncLogPeer::DATABASE_NAME);
            // primary key is composite; we therefore, expect
            // the primary key passed to be an array of pkey values
            if (count($values) == count($values, COUNT_RECURSIVE)) {
                // array is not multi-dimensional
                $values = array($values);
            }
            foreach ($values as $value) {
                $criterion = $criteria->getNewCriterion(WtDstSyncLogPeer::KODE_WILAYAH, $value[0]);
                $criterion->addAnd($criteria->getNewCriterion(WtDstSyncLogPeer::SEKOLAH_ID, $value[1]));
                $criterion->addAnd($criteria->getNewCriterion(WtDstSyncLogPeer::TABLE_NAME, $value[2]));
                $criteria->addOr($criterion);
                // we can invalidate the cache for this single PK
                WtDstSyncLogPeer::removeInstanceFromPool($value);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(WtDstSyncLogPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            
            $affectedRows += BasePeer::doDelete($criteria, $con);
            WtDstSyncLogPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given WtDstSyncLog object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      WtDstSyncLog $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(WtDstSyncLogPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(WtDstSyncLogPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(WtDstSyncLogPeer::DATABASE_NAME, WtDstSyncLogPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve object using using composite pkey values.
     * @param   string $kode_wilayah
     * @param   string $sekolah_id
     * @param   string $table_name
     * @param      PropelPDO $con
     * @return   WtDstSyncLog
     */
    public static function retrieveByPK($kode_wilayah, $sekolah_id, $table_name, PropelPDO $con = null) {
        $_instancePoolKey = serialize(array((string) $kode_wilayah, (string) $sekolah_id, (string) $table_name));
         if (null !== ($obj = WtDstSyncLogPeer::getInstanceFromPool($_instancePoolKey))) {
             return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(WtDstSyncLogPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $criteria = new Criteria(WtDstSyncLogPeer::DATABASE_NAME);
        $criteria->add(WtDstSyncLogPeer::KODE_WILAYAH, $kode_wilayah);
        $criteria->add(WtDstSyncLogPeer::SEKOLAH_ID, $sekolah_id);
        $criteria->add(WtDstSyncLogPeer::TABLE_NAME, $table_name);
        $v = WtDstSyncLogPeer::doSelect($criteria, $con);

        return !empty($v) ? $v[0] : null;
    }
} // BaseWtDstSyncLogPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseWtDstSyncLogPeer::buildTableMap();

