<?php

namespace angulex\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\BidangUsaha;
use angulex\Model\Dudi;
use angulex\Model\DudiPeer;
use angulex\Model\DudiQuery;
use angulex\Model\Mou;

/**
 * Base class that represents a query for the 'dudi' table.
 *
 * 
 *
 * @method DudiQuery orderByDudiId($order = Criteria::ASC) Order by the dudi_id column
 * @method DudiQuery orderByBidangUsahaId($order = Criteria::ASC) Order by the bidang_usaha_id column
 * @method DudiQuery orderByNamaDudi($order = Criteria::ASC) Order by the nama_dudi column
 * @method DudiQuery orderByNpwp($order = Criteria::ASC) Order by the npwp column
 * @method DudiQuery orderByTelpKantor($order = Criteria::ASC) Order by the telp_kantor column
 * @method DudiQuery orderByFax($order = Criteria::ASC) Order by the fax column
 * @method DudiQuery orderByContactPerson($order = Criteria::ASC) Order by the contact_person column
 * @method DudiQuery orderByTelpCp($order = Criteria::ASC) Order by the telp_cp column
 * @method DudiQuery orderByJabatanCp($order = Criteria::ASC) Order by the jabatan_cp column
 * @method DudiQuery orderByLastUpdate($order = Criteria::ASC) Order by the Last_update column
 * @method DudiQuery orderBySoftDelete($order = Criteria::ASC) Order by the Soft_delete column
 * @method DudiQuery orderByLastSync($order = Criteria::ASC) Order by the last_sync column
 * @method DudiQuery orderByUpdaterId($order = Criteria::ASC) Order by the Updater_ID column
 *
 * @method DudiQuery groupByDudiId() Group by the dudi_id column
 * @method DudiQuery groupByBidangUsahaId() Group by the bidang_usaha_id column
 * @method DudiQuery groupByNamaDudi() Group by the nama_dudi column
 * @method DudiQuery groupByNpwp() Group by the npwp column
 * @method DudiQuery groupByTelpKantor() Group by the telp_kantor column
 * @method DudiQuery groupByFax() Group by the fax column
 * @method DudiQuery groupByContactPerson() Group by the contact_person column
 * @method DudiQuery groupByTelpCp() Group by the telp_cp column
 * @method DudiQuery groupByJabatanCp() Group by the jabatan_cp column
 * @method DudiQuery groupByLastUpdate() Group by the Last_update column
 * @method DudiQuery groupBySoftDelete() Group by the Soft_delete column
 * @method DudiQuery groupByLastSync() Group by the last_sync column
 * @method DudiQuery groupByUpdaterId() Group by the Updater_ID column
 *
 * @method DudiQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method DudiQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method DudiQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method DudiQuery leftJoinBidangUsahaRelatedByBidangUsahaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the BidangUsahaRelatedByBidangUsahaId relation
 * @method DudiQuery rightJoinBidangUsahaRelatedByBidangUsahaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BidangUsahaRelatedByBidangUsahaId relation
 * @method DudiQuery innerJoinBidangUsahaRelatedByBidangUsahaId($relationAlias = null) Adds a INNER JOIN clause to the query using the BidangUsahaRelatedByBidangUsahaId relation
 *
 * @method DudiQuery leftJoinBidangUsahaRelatedByBidangUsahaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the BidangUsahaRelatedByBidangUsahaId relation
 * @method DudiQuery rightJoinBidangUsahaRelatedByBidangUsahaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BidangUsahaRelatedByBidangUsahaId relation
 * @method DudiQuery innerJoinBidangUsahaRelatedByBidangUsahaId($relationAlias = null) Adds a INNER JOIN clause to the query using the BidangUsahaRelatedByBidangUsahaId relation
 *
 * @method DudiQuery leftJoinMouRelatedByDudiId($relationAlias = null) Adds a LEFT JOIN clause to the query using the MouRelatedByDudiId relation
 * @method DudiQuery rightJoinMouRelatedByDudiId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MouRelatedByDudiId relation
 * @method DudiQuery innerJoinMouRelatedByDudiId($relationAlias = null) Adds a INNER JOIN clause to the query using the MouRelatedByDudiId relation
 *
 * @method DudiQuery leftJoinMouRelatedByDudiId($relationAlias = null) Adds a LEFT JOIN clause to the query using the MouRelatedByDudiId relation
 * @method DudiQuery rightJoinMouRelatedByDudiId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MouRelatedByDudiId relation
 * @method DudiQuery innerJoinMouRelatedByDudiId($relationAlias = null) Adds a INNER JOIN clause to the query using the MouRelatedByDudiId relation
 *
 * @method Dudi findOne(PropelPDO $con = null) Return the first Dudi matching the query
 * @method Dudi findOneOrCreate(PropelPDO $con = null) Return the first Dudi matching the query, or a new Dudi object populated from the query conditions when no match is found
 *
 * @method Dudi findOneByBidangUsahaId(string $bidang_usaha_id) Return the first Dudi filtered by the bidang_usaha_id column
 * @method Dudi findOneByNamaDudi(string $nama_dudi) Return the first Dudi filtered by the nama_dudi column
 * @method Dudi findOneByNpwp(string $npwp) Return the first Dudi filtered by the npwp column
 * @method Dudi findOneByTelpKantor(string $telp_kantor) Return the first Dudi filtered by the telp_kantor column
 * @method Dudi findOneByFax(string $fax) Return the first Dudi filtered by the fax column
 * @method Dudi findOneByContactPerson(string $contact_person) Return the first Dudi filtered by the contact_person column
 * @method Dudi findOneByTelpCp(string $telp_cp) Return the first Dudi filtered by the telp_cp column
 * @method Dudi findOneByJabatanCp(string $jabatan_cp) Return the first Dudi filtered by the jabatan_cp column
 * @method Dudi findOneByLastUpdate(string $Last_update) Return the first Dudi filtered by the Last_update column
 * @method Dudi findOneBySoftDelete(string $Soft_delete) Return the first Dudi filtered by the Soft_delete column
 * @method Dudi findOneByLastSync(string $last_sync) Return the first Dudi filtered by the last_sync column
 * @method Dudi findOneByUpdaterId(string $Updater_ID) Return the first Dudi filtered by the Updater_ID column
 *
 * @method array findByDudiId(string $dudi_id) Return Dudi objects filtered by the dudi_id column
 * @method array findByBidangUsahaId(string $bidang_usaha_id) Return Dudi objects filtered by the bidang_usaha_id column
 * @method array findByNamaDudi(string $nama_dudi) Return Dudi objects filtered by the nama_dudi column
 * @method array findByNpwp(string $npwp) Return Dudi objects filtered by the npwp column
 * @method array findByTelpKantor(string $telp_kantor) Return Dudi objects filtered by the telp_kantor column
 * @method array findByFax(string $fax) Return Dudi objects filtered by the fax column
 * @method array findByContactPerson(string $contact_person) Return Dudi objects filtered by the contact_person column
 * @method array findByTelpCp(string $telp_cp) Return Dudi objects filtered by the telp_cp column
 * @method array findByJabatanCp(string $jabatan_cp) Return Dudi objects filtered by the jabatan_cp column
 * @method array findByLastUpdate(string $Last_update) Return Dudi objects filtered by the Last_update column
 * @method array findBySoftDelete(string $Soft_delete) Return Dudi objects filtered by the Soft_delete column
 * @method array findByLastSync(string $last_sync) Return Dudi objects filtered by the last_sync column
 * @method array findByUpdaterId(string $Updater_ID) Return Dudi objects filtered by the Updater_ID column
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseDudiQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseDudiQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Dapodikmen', $modelName = 'angulex\\Model\\Dudi', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new DudiQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   DudiQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return DudiQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof DudiQuery) {
            return $criteria;
        }
        $query = new DudiQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Dudi|Dudi[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = DudiPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(DudiPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Dudi A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByDudiId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Dudi A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [dudi_id], [bidang_usaha_id], [nama_dudi], [npwp], [telp_kantor], [fax], [contact_person], [telp_cp], [jabatan_cp], [Last_update], [Soft_delete], [last_sync], [Updater_ID] FROM [dudi] WHERE [dudi_id] = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Dudi();
            $obj->hydrate($row);
            DudiPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Dudi|Dudi[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Dudi[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return DudiQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(DudiPeer::DUDI_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return DudiQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(DudiPeer::DUDI_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the dudi_id column
     *
     * Example usage:
     * <code>
     * $query->filterByDudiId('fooValue');   // WHERE dudi_id = 'fooValue'
     * $query->filterByDudiId('%fooValue%'); // WHERE dudi_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $dudiId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DudiQuery The current query, for fluid interface
     */
    public function filterByDudiId($dudiId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($dudiId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $dudiId)) {
                $dudiId = str_replace('*', '%', $dudiId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(DudiPeer::DUDI_ID, $dudiId, $comparison);
    }

    /**
     * Filter the query on the bidang_usaha_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBidangUsahaId('fooValue');   // WHERE bidang_usaha_id = 'fooValue'
     * $query->filterByBidangUsahaId('%fooValue%'); // WHERE bidang_usaha_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $bidangUsahaId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DudiQuery The current query, for fluid interface
     */
    public function filterByBidangUsahaId($bidangUsahaId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($bidangUsahaId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $bidangUsahaId)) {
                $bidangUsahaId = str_replace('*', '%', $bidangUsahaId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(DudiPeer::BIDANG_USAHA_ID, $bidangUsahaId, $comparison);
    }

    /**
     * Filter the query on the nama_dudi column
     *
     * Example usage:
     * <code>
     * $query->filterByNamaDudi('fooValue');   // WHERE nama_dudi = 'fooValue'
     * $query->filterByNamaDudi('%fooValue%'); // WHERE nama_dudi LIKE '%fooValue%'
     * </code>
     *
     * @param     string $namaDudi The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DudiQuery The current query, for fluid interface
     */
    public function filterByNamaDudi($namaDudi = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($namaDudi)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $namaDudi)) {
                $namaDudi = str_replace('*', '%', $namaDudi);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(DudiPeer::NAMA_DUDI, $namaDudi, $comparison);
    }

    /**
     * Filter the query on the npwp column
     *
     * Example usage:
     * <code>
     * $query->filterByNpwp('fooValue');   // WHERE npwp = 'fooValue'
     * $query->filterByNpwp('%fooValue%'); // WHERE npwp LIKE '%fooValue%'
     * </code>
     *
     * @param     string $npwp The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DudiQuery The current query, for fluid interface
     */
    public function filterByNpwp($npwp = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($npwp)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $npwp)) {
                $npwp = str_replace('*', '%', $npwp);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(DudiPeer::NPWP, $npwp, $comparison);
    }

    /**
     * Filter the query on the telp_kantor column
     *
     * Example usage:
     * <code>
     * $query->filterByTelpKantor('fooValue');   // WHERE telp_kantor = 'fooValue'
     * $query->filterByTelpKantor('%fooValue%'); // WHERE telp_kantor LIKE '%fooValue%'
     * </code>
     *
     * @param     string $telpKantor The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DudiQuery The current query, for fluid interface
     */
    public function filterByTelpKantor($telpKantor = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($telpKantor)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $telpKantor)) {
                $telpKantor = str_replace('*', '%', $telpKantor);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(DudiPeer::TELP_KANTOR, $telpKantor, $comparison);
    }

    /**
     * Filter the query on the fax column
     *
     * Example usage:
     * <code>
     * $query->filterByFax('fooValue');   // WHERE fax = 'fooValue'
     * $query->filterByFax('%fooValue%'); // WHERE fax LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fax The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DudiQuery The current query, for fluid interface
     */
    public function filterByFax($fax = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fax)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $fax)) {
                $fax = str_replace('*', '%', $fax);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(DudiPeer::FAX, $fax, $comparison);
    }

    /**
     * Filter the query on the contact_person column
     *
     * Example usage:
     * <code>
     * $query->filterByContactPerson('fooValue');   // WHERE contact_person = 'fooValue'
     * $query->filterByContactPerson('%fooValue%'); // WHERE contact_person LIKE '%fooValue%'
     * </code>
     *
     * @param     string $contactPerson The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DudiQuery The current query, for fluid interface
     */
    public function filterByContactPerson($contactPerson = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($contactPerson)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $contactPerson)) {
                $contactPerson = str_replace('*', '%', $contactPerson);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(DudiPeer::CONTACT_PERSON, $contactPerson, $comparison);
    }

    /**
     * Filter the query on the telp_cp column
     *
     * Example usage:
     * <code>
     * $query->filterByTelpCp('fooValue');   // WHERE telp_cp = 'fooValue'
     * $query->filterByTelpCp('%fooValue%'); // WHERE telp_cp LIKE '%fooValue%'
     * </code>
     *
     * @param     string $telpCp The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DudiQuery The current query, for fluid interface
     */
    public function filterByTelpCp($telpCp = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($telpCp)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $telpCp)) {
                $telpCp = str_replace('*', '%', $telpCp);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(DudiPeer::TELP_CP, $telpCp, $comparison);
    }

    /**
     * Filter the query on the jabatan_cp column
     *
     * Example usage:
     * <code>
     * $query->filterByJabatanCp('fooValue');   // WHERE jabatan_cp = 'fooValue'
     * $query->filterByJabatanCp('%fooValue%'); // WHERE jabatan_cp LIKE '%fooValue%'
     * </code>
     *
     * @param     string $jabatanCp The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DudiQuery The current query, for fluid interface
     */
    public function filterByJabatanCp($jabatanCp = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($jabatanCp)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $jabatanCp)) {
                $jabatanCp = str_replace('*', '%', $jabatanCp);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(DudiPeer::JABATAN_CP, $jabatanCp, $comparison);
    }

    /**
     * Filter the query on the Last_update column
     *
     * Example usage:
     * <code>
     * $query->filterByLastUpdate('2011-03-14'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate('now'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate(array('max' => 'yesterday')); // WHERE Last_update > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastUpdate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DudiQuery The current query, for fluid interface
     */
    public function filterByLastUpdate($lastUpdate = null, $comparison = null)
    {
        if (is_array($lastUpdate)) {
            $useMinMax = false;
            if (isset($lastUpdate['min'])) {
                $this->addUsingAlias(DudiPeer::LAST_UPDATE, $lastUpdate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastUpdate['max'])) {
                $this->addUsingAlias(DudiPeer::LAST_UPDATE, $lastUpdate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DudiPeer::LAST_UPDATE, $lastUpdate, $comparison);
    }

    /**
     * Filter the query on the Soft_delete column
     *
     * Example usage:
     * <code>
     * $query->filterBySoftDelete(1234); // WHERE Soft_delete = 1234
     * $query->filterBySoftDelete(array(12, 34)); // WHERE Soft_delete IN (12, 34)
     * $query->filterBySoftDelete(array('min' => 12)); // WHERE Soft_delete >= 12
     * $query->filterBySoftDelete(array('max' => 12)); // WHERE Soft_delete <= 12
     * </code>
     *
     * @param     mixed $softDelete The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DudiQuery The current query, for fluid interface
     */
    public function filterBySoftDelete($softDelete = null, $comparison = null)
    {
        if (is_array($softDelete)) {
            $useMinMax = false;
            if (isset($softDelete['min'])) {
                $this->addUsingAlias(DudiPeer::SOFT_DELETE, $softDelete['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($softDelete['max'])) {
                $this->addUsingAlias(DudiPeer::SOFT_DELETE, $softDelete['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DudiPeer::SOFT_DELETE, $softDelete, $comparison);
    }

    /**
     * Filter the query on the last_sync column
     *
     * Example usage:
     * <code>
     * $query->filterByLastSync('2011-03-14'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync('now'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync(array('max' => 'yesterday')); // WHERE last_sync > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastSync The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DudiQuery The current query, for fluid interface
     */
    public function filterByLastSync($lastSync = null, $comparison = null)
    {
        if (is_array($lastSync)) {
            $useMinMax = false;
            if (isset($lastSync['min'])) {
                $this->addUsingAlias(DudiPeer::LAST_SYNC, $lastSync['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastSync['max'])) {
                $this->addUsingAlias(DudiPeer::LAST_SYNC, $lastSync['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DudiPeer::LAST_SYNC, $lastSync, $comparison);
    }

    /**
     * Filter the query on the Updater_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdaterId('fooValue');   // WHERE Updater_ID = 'fooValue'
     * $query->filterByUpdaterId('%fooValue%'); // WHERE Updater_ID LIKE '%fooValue%'
     * </code>
     *
     * @param     string $updaterId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DudiQuery The current query, for fluid interface
     */
    public function filterByUpdaterId($updaterId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($updaterId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $updaterId)) {
                $updaterId = str_replace('*', '%', $updaterId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(DudiPeer::UPDATER_ID, $updaterId, $comparison);
    }

    /**
     * Filter the query by a related BidangUsaha object
     *
     * @param   BidangUsaha|PropelObjectCollection $bidangUsaha The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 DudiQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByBidangUsahaRelatedByBidangUsahaId($bidangUsaha, $comparison = null)
    {
        if ($bidangUsaha instanceof BidangUsaha) {
            return $this
                ->addUsingAlias(DudiPeer::BIDANG_USAHA_ID, $bidangUsaha->getBidangUsahaId(), $comparison);
        } elseif ($bidangUsaha instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(DudiPeer::BIDANG_USAHA_ID, $bidangUsaha->toKeyValue('PrimaryKey', 'BidangUsahaId'), $comparison);
        } else {
            throw new PropelException('filterByBidangUsahaRelatedByBidangUsahaId() only accepts arguments of type BidangUsaha or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BidangUsahaRelatedByBidangUsahaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return DudiQuery The current query, for fluid interface
     */
    public function joinBidangUsahaRelatedByBidangUsahaId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BidangUsahaRelatedByBidangUsahaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BidangUsahaRelatedByBidangUsahaId');
        }

        return $this;
    }

    /**
     * Use the BidangUsahaRelatedByBidangUsahaId relation BidangUsaha object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\BidangUsahaQuery A secondary query class using the current class as primary query
     */
    public function useBidangUsahaRelatedByBidangUsahaIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBidangUsahaRelatedByBidangUsahaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BidangUsahaRelatedByBidangUsahaId', '\angulex\Model\BidangUsahaQuery');
    }

    /**
     * Filter the query by a related BidangUsaha object
     *
     * @param   BidangUsaha|PropelObjectCollection $bidangUsaha The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 DudiQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByBidangUsahaRelatedByBidangUsahaId($bidangUsaha, $comparison = null)
    {
        if ($bidangUsaha instanceof BidangUsaha) {
            return $this
                ->addUsingAlias(DudiPeer::BIDANG_USAHA_ID, $bidangUsaha->getBidangUsahaId(), $comparison);
        } elseif ($bidangUsaha instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(DudiPeer::BIDANG_USAHA_ID, $bidangUsaha->toKeyValue('PrimaryKey', 'BidangUsahaId'), $comparison);
        } else {
            throw new PropelException('filterByBidangUsahaRelatedByBidangUsahaId() only accepts arguments of type BidangUsaha or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BidangUsahaRelatedByBidangUsahaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return DudiQuery The current query, for fluid interface
     */
    public function joinBidangUsahaRelatedByBidangUsahaId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BidangUsahaRelatedByBidangUsahaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BidangUsahaRelatedByBidangUsahaId');
        }

        return $this;
    }

    /**
     * Use the BidangUsahaRelatedByBidangUsahaId relation BidangUsaha object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\BidangUsahaQuery A secondary query class using the current class as primary query
     */
    public function useBidangUsahaRelatedByBidangUsahaIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBidangUsahaRelatedByBidangUsahaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BidangUsahaRelatedByBidangUsahaId', '\angulex\Model\BidangUsahaQuery');
    }

    /**
     * Filter the query by a related Mou object
     *
     * @param   Mou|PropelObjectCollection $mou  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 DudiQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByMouRelatedByDudiId($mou, $comparison = null)
    {
        if ($mou instanceof Mou) {
            return $this
                ->addUsingAlias(DudiPeer::DUDI_ID, $mou->getDudiId(), $comparison);
        } elseif ($mou instanceof PropelObjectCollection) {
            return $this
                ->useMouRelatedByDudiIdQuery()
                ->filterByPrimaryKeys($mou->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByMouRelatedByDudiId() only accepts arguments of type Mou or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MouRelatedByDudiId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return DudiQuery The current query, for fluid interface
     */
    public function joinMouRelatedByDudiId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MouRelatedByDudiId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MouRelatedByDudiId');
        }

        return $this;
    }

    /**
     * Use the MouRelatedByDudiId relation Mou object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\MouQuery A secondary query class using the current class as primary query
     */
    public function useMouRelatedByDudiIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinMouRelatedByDudiId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MouRelatedByDudiId', '\angulex\Model\MouQuery');
    }

    /**
     * Filter the query by a related Mou object
     *
     * @param   Mou|PropelObjectCollection $mou  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 DudiQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByMouRelatedByDudiId($mou, $comparison = null)
    {
        if ($mou instanceof Mou) {
            return $this
                ->addUsingAlias(DudiPeer::DUDI_ID, $mou->getDudiId(), $comparison);
        } elseif ($mou instanceof PropelObjectCollection) {
            return $this
                ->useMouRelatedByDudiIdQuery()
                ->filterByPrimaryKeys($mou->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByMouRelatedByDudiId() only accepts arguments of type Mou or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MouRelatedByDudiId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return DudiQuery The current query, for fluid interface
     */
    public function joinMouRelatedByDudiId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MouRelatedByDudiId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MouRelatedByDudiId');
        }

        return $this;
    }

    /**
     * Use the MouRelatedByDudiId relation Mou object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\MouQuery A secondary query class using the current class as primary query
     */
    public function useMouRelatedByDudiIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinMouRelatedByDudiId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MouRelatedByDudiId', '\angulex\Model\MouQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Dudi $dudi Object to remove from the list of results
     *
     * @return DudiQuery The current query, for fluid interface
     */
    public function prune($dudi = null)
    {
        if ($dudi) {
            $this->addUsingAlias(DudiPeer::DUDI_ID, $dudi->getDudiId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
