<?php

namespace angulex\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelDateTime;
use \PropelException;
use \PropelPDO;
use angulex\Model\LogPengguna;
use angulex\Model\LogPenggunaPeer;
use angulex\Model\LogPenggunaQuery;
use angulex\Model\Pengguna;
use angulex\Model\PenggunaQuery;

/**
 * Base class that represents a row from the 'log_pengguna' table.
 *
 * 
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseLogPengguna extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'angulex\\Model\\LogPenggunaPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        LogPenggunaPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the waktu_login field.
     * @var        string
     */
    protected $waktu_login;

    /**
     * The value for the pengguna_id field.
     * @var        string
     */
    protected $pengguna_id;

    /**
     * The value for the alamat_ip field.
     * @var        string
     */
    protected $alamat_ip;

    /**
     * The value for the keterangan field.
     * @var        string
     */
    protected $keterangan;

    /**
     * The value for the waktu_logout field.
     * @var        string
     */
    protected $waktu_logout;

    /**
     * @var        Pengguna
     */
    protected $aPenggunaRelatedByPenggunaId;

    /**
     * @var        Pengguna
     */
    protected $aPenggunaRelatedByPenggunaId;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Get the [optionally formatted] temporal [waktu_login] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getWaktuLogin($format = 'Y-m-d H:i:s')
    {
        if ($this->waktu_login === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->waktu_login);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->waktu_login, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [pengguna_id] column value.
     * 
     * @return string
     */
    public function getPenggunaId()
    {
        return $this->pengguna_id;
    }

    /**
     * Get the [alamat_ip] column value.
     * 
     * @return string
     */
    public function getAlamatIp()
    {
        return $this->alamat_ip;
    }

    /**
     * Get the [keterangan] column value.
     * 
     * @return string
     */
    public function getKeterangan()
    {
        return $this->keterangan;
    }

    /**
     * Get the [optionally formatted] temporal [waktu_logout] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getWaktuLogout($format = 'Y-m-d H:i:s')
    {
        if ($this->waktu_logout === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->waktu_logout);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->waktu_logout, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Sets the value of [waktu_login] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return LogPengguna The current object (for fluent API support)
     */
    public function setWaktuLogin($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->waktu_login !== null || $dt !== null) {
            $currentDateAsString = ($this->waktu_login !== null && $tmpDt = new DateTime($this->waktu_login)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->waktu_login = $newDateAsString;
                $this->modifiedColumns[] = LogPenggunaPeer::WAKTU_LOGIN;
            }
        } // if either are not null


        return $this;
    } // setWaktuLogin()

    /**
     * Set the value of [pengguna_id] column.
     * 
     * @param string $v new value
     * @return LogPengguna The current object (for fluent API support)
     */
    public function setPenggunaId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->pengguna_id !== $v) {
            $this->pengguna_id = $v;
            $this->modifiedColumns[] = LogPenggunaPeer::PENGGUNA_ID;
        }

        if ($this->aPenggunaRelatedByPenggunaId !== null && $this->aPenggunaRelatedByPenggunaId->getPenggunaId() !== $v) {
            $this->aPenggunaRelatedByPenggunaId = null;
        }

        if ($this->aPenggunaRelatedByPenggunaId !== null && $this->aPenggunaRelatedByPenggunaId->getPenggunaId() !== $v) {
            $this->aPenggunaRelatedByPenggunaId = null;
        }


        return $this;
    } // setPenggunaId()

    /**
     * Set the value of [alamat_ip] column.
     * 
     * @param string $v new value
     * @return LogPengguna The current object (for fluent API support)
     */
    public function setAlamatIp($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->alamat_ip !== $v) {
            $this->alamat_ip = $v;
            $this->modifiedColumns[] = LogPenggunaPeer::ALAMAT_IP;
        }


        return $this;
    } // setAlamatIp()

    /**
     * Set the value of [keterangan] column.
     * 
     * @param string $v new value
     * @return LogPengguna The current object (for fluent API support)
     */
    public function setKeterangan($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->keterangan !== $v) {
            $this->keterangan = $v;
            $this->modifiedColumns[] = LogPenggunaPeer::KETERANGAN;
        }


        return $this;
    } // setKeterangan()

    /**
     * Sets the value of [waktu_logout] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return LogPengguna The current object (for fluent API support)
     */
    public function setWaktuLogout($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->waktu_logout !== null || $dt !== null) {
            $currentDateAsString = ($this->waktu_logout !== null && $tmpDt = new DateTime($this->waktu_logout)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->waktu_logout = $newDateAsString;
                $this->modifiedColumns[] = LogPenggunaPeer::WAKTU_LOGOUT;
            }
        } // if either are not null


        return $this;
    } // setWaktuLogout()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->waktu_login = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
            $this->pengguna_id = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->alamat_ip = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->keterangan = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->waktu_logout = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 5; // 5 = LogPenggunaPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating LogPengguna object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aPenggunaRelatedByPenggunaId !== null && $this->pengguna_id !== $this->aPenggunaRelatedByPenggunaId->getPenggunaId()) {
            $this->aPenggunaRelatedByPenggunaId = null;
        }
        if ($this->aPenggunaRelatedByPenggunaId !== null && $this->pengguna_id !== $this->aPenggunaRelatedByPenggunaId->getPenggunaId()) {
            $this->aPenggunaRelatedByPenggunaId = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(LogPenggunaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = LogPenggunaPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aPenggunaRelatedByPenggunaId = null;
            $this->aPenggunaRelatedByPenggunaId = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(LogPenggunaPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = LogPenggunaQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(LogPenggunaPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                LogPenggunaPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPenggunaRelatedByPenggunaId !== null) {
                if ($this->aPenggunaRelatedByPenggunaId->isModified() || $this->aPenggunaRelatedByPenggunaId->isNew()) {
                    $affectedRows += $this->aPenggunaRelatedByPenggunaId->save($con);
                }
                $this->setPenggunaRelatedByPenggunaId($this->aPenggunaRelatedByPenggunaId);
            }

            if ($this->aPenggunaRelatedByPenggunaId !== null) {
                if ($this->aPenggunaRelatedByPenggunaId->isModified() || $this->aPenggunaRelatedByPenggunaId->isNew()) {
                    $affectedRows += $this->aPenggunaRelatedByPenggunaId->save($con);
                }
                $this->setPenggunaRelatedByPenggunaId($this->aPenggunaRelatedByPenggunaId);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $criteria = $this->buildCriteria();
        $pk = BasePeer::doInsert($criteria, $con);
        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPenggunaRelatedByPenggunaId !== null) {
                if (!$this->aPenggunaRelatedByPenggunaId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPenggunaRelatedByPenggunaId->getValidationFailures());
                }
            }

            if ($this->aPenggunaRelatedByPenggunaId !== null) {
                if (!$this->aPenggunaRelatedByPenggunaId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPenggunaRelatedByPenggunaId->getValidationFailures());
                }
            }


            if (($retval = LogPenggunaPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = LogPenggunaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getWaktuLogin();
                break;
            case 1:
                return $this->getPenggunaId();
                break;
            case 2:
                return $this->getAlamatIp();
                break;
            case 3:
                return $this->getKeterangan();
                break;
            case 4:
                return $this->getWaktuLogout();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['LogPengguna'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['LogPengguna'][$this->getPrimaryKey()] = true;
        $keys = LogPenggunaPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getWaktuLogin(),
            $keys[1] => $this->getPenggunaId(),
            $keys[2] => $this->getAlamatIp(),
            $keys[3] => $this->getKeterangan(),
            $keys[4] => $this->getWaktuLogout(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aPenggunaRelatedByPenggunaId) {
                $result['PenggunaRelatedByPenggunaId'] = $this->aPenggunaRelatedByPenggunaId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPenggunaRelatedByPenggunaId) {
                $result['PenggunaRelatedByPenggunaId'] = $this->aPenggunaRelatedByPenggunaId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = LogPenggunaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setWaktuLogin($value);
                break;
            case 1:
                $this->setPenggunaId($value);
                break;
            case 2:
                $this->setAlamatIp($value);
                break;
            case 3:
                $this->setKeterangan($value);
                break;
            case 4:
                $this->setWaktuLogout($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = LogPenggunaPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setWaktuLogin($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setPenggunaId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setAlamatIp($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setKeterangan($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setWaktuLogout($arr[$keys[4]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(LogPenggunaPeer::DATABASE_NAME);

        if ($this->isColumnModified(LogPenggunaPeer::WAKTU_LOGIN)) $criteria->add(LogPenggunaPeer::WAKTU_LOGIN, $this->waktu_login);
        if ($this->isColumnModified(LogPenggunaPeer::PENGGUNA_ID)) $criteria->add(LogPenggunaPeer::PENGGUNA_ID, $this->pengguna_id);
        if ($this->isColumnModified(LogPenggunaPeer::ALAMAT_IP)) $criteria->add(LogPenggunaPeer::ALAMAT_IP, $this->alamat_ip);
        if ($this->isColumnModified(LogPenggunaPeer::KETERANGAN)) $criteria->add(LogPenggunaPeer::KETERANGAN, $this->keterangan);
        if ($this->isColumnModified(LogPenggunaPeer::WAKTU_LOGOUT)) $criteria->add(LogPenggunaPeer::WAKTU_LOGOUT, $this->waktu_logout);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(LogPenggunaPeer::DATABASE_NAME);
        $criteria->add(LogPenggunaPeer::WAKTU_LOGIN, $this->waktu_login);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getWaktuLogin();
    }

    /**
     * Generic method to set the primary key (waktu_login column).
     *
     * @param  string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setWaktuLogin($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getWaktuLogin();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of LogPengguna (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setPenggunaId($this->getPenggunaId());
        $copyObj->setAlamatIp($this->getAlamatIp());
        $copyObj->setKeterangan($this->getKeterangan());
        $copyObj->setWaktuLogout($this->getWaktuLogout());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setWaktuLogin(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return LogPengguna Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return LogPenggunaPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new LogPenggunaPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a Pengguna object.
     *
     * @param             Pengguna $v
     * @return LogPengguna The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPenggunaRelatedByPenggunaId(Pengguna $v = null)
    {
        if ($v === null) {
            $this->setPenggunaId(NULL);
        } else {
            $this->setPenggunaId($v->getPenggunaId());
        }

        $this->aPenggunaRelatedByPenggunaId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Pengguna object, it will not be re-added.
        if ($v !== null) {
            $v->addLogPenggunaRelatedByPenggunaId($this);
        }


        return $this;
    }


    /**
     * Get the associated Pengguna object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Pengguna The associated Pengguna object.
     * @throws PropelException
     */
    public function getPenggunaRelatedByPenggunaId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPenggunaRelatedByPenggunaId === null && (($this->pengguna_id !== "" && $this->pengguna_id !== null)) && $doQuery) {
            $this->aPenggunaRelatedByPenggunaId = PenggunaQuery::create()->findPk($this->pengguna_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPenggunaRelatedByPenggunaId->addLogPenggunasRelatedByPenggunaId($this);
             */
        }

        return $this->aPenggunaRelatedByPenggunaId;
    }

    /**
     * Declares an association between this object and a Pengguna object.
     *
     * @param             Pengguna $v
     * @return LogPengguna The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPenggunaRelatedByPenggunaId(Pengguna $v = null)
    {
        if ($v === null) {
            $this->setPenggunaId(NULL);
        } else {
            $this->setPenggunaId($v->getPenggunaId());
        }

        $this->aPenggunaRelatedByPenggunaId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Pengguna object, it will not be re-added.
        if ($v !== null) {
            $v->addLogPenggunaRelatedByPenggunaId($this);
        }


        return $this;
    }


    /**
     * Get the associated Pengguna object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Pengguna The associated Pengguna object.
     * @throws PropelException
     */
    public function getPenggunaRelatedByPenggunaId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPenggunaRelatedByPenggunaId === null && (($this->pengguna_id !== "" && $this->pengguna_id !== null)) && $doQuery) {
            $this->aPenggunaRelatedByPenggunaId = PenggunaQuery::create()->findPk($this->pengguna_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPenggunaRelatedByPenggunaId->addLogPenggunasRelatedByPenggunaId($this);
             */
        }

        return $this->aPenggunaRelatedByPenggunaId;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->waktu_login = null;
        $this->pengguna_id = null;
        $this->alamat_ip = null;
        $this->keterangan = null;
        $this->waktu_logout = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->aPenggunaRelatedByPenggunaId instanceof Persistent) {
              $this->aPenggunaRelatedByPenggunaId->clearAllReferences($deep);
            }
            if ($this->aPenggunaRelatedByPenggunaId instanceof Persistent) {
              $this->aPenggunaRelatedByPenggunaId->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        $this->aPenggunaRelatedByPenggunaId = null;
        $this->aPenggunaRelatedByPenggunaId = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(LogPenggunaPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
