<?php

namespace angulex\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelDateTime;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\Dudi;
use angulex\Model\DudiQuery;
use angulex\Model\JurusanKerjasama;
use angulex\Model\JurusanKerjasamaQuery;
use angulex\Model\Mou;
use angulex\Model\MouPeer;
use angulex\Model\MouQuery;
use angulex\Model\Sekolah;
use angulex\Model\SekolahQuery;
use angulex\Model\UnitUsahaKerjasama;
use angulex\Model\UnitUsahaKerjasamaQuery;
use angulex\Model\VldMou;
use angulex\Model\VldMouQuery;

/**
 * Base class that represents a row from the 'mou' table.
 *
 * 
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseMou extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'angulex\\Model\\MouPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        MouPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the mou_id field.
     * @var        string
     */
    protected $mou_id;

    /**
     * The value for the dudi_id field.
     * @var        string
     */
    protected $dudi_id;

    /**
     * The value for the sekolah_id field.
     * @var        string
     */
    protected $sekolah_id;

    /**
     * The value for the nomor_mou field.
     * @var        string
     */
    protected $nomor_mou;

    /**
     * The value for the judul_mou field.
     * @var        string
     */
    protected $judul_mou;

    /**
     * The value for the tanggal_mulai field.
     * @var        string
     */
    protected $tanggal_mulai;

    /**
     * The value for the tanggal_selesai field.
     * @var        string
     */
    protected $tanggal_selesai;

    /**
     * The value for the nama_dudi field.
     * @var        string
     */
    protected $nama_dudi;

    /**
     * The value for the npwp_dudi field.
     * @var        string
     */
    protected $npwp_dudi;

    /**
     * The value for the nama_bidang_usaha field.
     * @var        string
     */
    protected $nama_bidang_usaha;

    /**
     * The value for the telp_kantor field.
     * @var        string
     */
    protected $telp_kantor;

    /**
     * The value for the fax field.
     * @var        string
     */
    protected $fax;

    /**
     * The value for the contact_person field.
     * @var        string
     */
    protected $contact_person;

    /**
     * The value for the telp_cp field.
     * @var        string
     */
    protected $telp_cp;

    /**
     * The value for the jabatan_cp field.
     * @var        string
     */
    protected $jabatan_cp;

    /**
     * The value for the last_update field.
     * @var        string
     */
    protected $last_update;

    /**
     * The value for the soft_delete field.
     * @var        string
     */
    protected $soft_delete;

    /**
     * The value for the last_sync field.
     * @var        string
     */
    protected $last_sync;

    /**
     * The value for the updater_id field.
     * @var        string
     */
    protected $updater_id;

    /**
     * @var        Dudi
     */
    protected $aDudiRelatedByDudiId;

    /**
     * @var        Dudi
     */
    protected $aDudiRelatedByDudiId;

    /**
     * @var        Sekolah
     */
    protected $aSekolahRelatedBySekolahId;

    /**
     * @var        Sekolah
     */
    protected $aSekolahRelatedBySekolahId;

    /**
     * @var        PropelObjectCollection|VldMou[] Collection to store aggregation of VldMou objects.
     */
    protected $collVldMousRelatedByMouId;
    protected $collVldMousRelatedByMouIdPartial;

    /**
     * @var        PropelObjectCollection|VldMou[] Collection to store aggregation of VldMou objects.
     */
    protected $collVldMousRelatedByMouId;
    protected $collVldMousRelatedByMouIdPartial;

    /**
     * @var        PropelObjectCollection|JurusanKerjasama[] Collection to store aggregation of JurusanKerjasama objects.
     */
    protected $collJurusanKerjasamasRelatedByMouId;
    protected $collJurusanKerjasamasRelatedByMouIdPartial;

    /**
     * @var        PropelObjectCollection|JurusanKerjasama[] Collection to store aggregation of JurusanKerjasama objects.
     */
    protected $collJurusanKerjasamasRelatedByMouId;
    protected $collJurusanKerjasamasRelatedByMouIdPartial;

    /**
     * @var        PropelObjectCollection|UnitUsahaKerjasama[] Collection to store aggregation of UnitUsahaKerjasama objects.
     */
    protected $collUnitUsahaKerjasamasRelatedByMouId;
    protected $collUnitUsahaKerjasamasRelatedByMouIdPartial;

    /**
     * @var        PropelObjectCollection|UnitUsahaKerjasama[] Collection to store aggregation of UnitUsahaKerjasama objects.
     */
    protected $collUnitUsahaKerjasamasRelatedByMouId;
    protected $collUnitUsahaKerjasamasRelatedByMouIdPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldMousRelatedByMouIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldMousRelatedByMouIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $jurusanKerjasamasRelatedByMouIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $jurusanKerjasamasRelatedByMouIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $unitUsahaKerjasamasRelatedByMouIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $unitUsahaKerjasamasRelatedByMouIdScheduledForDeletion = null;

    /**
     * Get the [mou_id] column value.
     * 
     * @return string
     */
    public function getMouId()
    {
        return $this->mou_id;
    }

    /**
     * Get the [dudi_id] column value.
     * 
     * @return string
     */
    public function getDudiId()
    {
        return $this->dudi_id;
    }

    /**
     * Get the [sekolah_id] column value.
     * 
     * @return string
     */
    public function getSekolahId()
    {
        return $this->sekolah_id;
    }

    /**
     * Get the [nomor_mou] column value.
     * 
     * @return string
     */
    public function getNomorMou()
    {
        return $this->nomor_mou;
    }

    /**
     * Get the [judul_mou] column value.
     * 
     * @return string
     */
    public function getJudulMou()
    {
        return $this->judul_mou;
    }

    /**
     * Get the [tanggal_mulai] column value.
     * 
     * @return string
     */
    public function getTanggalMulai()
    {
        return $this->tanggal_mulai;
    }

    /**
     * Get the [tanggal_selesai] column value.
     * 
     * @return string
     */
    public function getTanggalSelesai()
    {
        return $this->tanggal_selesai;
    }

    /**
     * Get the [nama_dudi] column value.
     * 
     * @return string
     */
    public function getNamaDudi()
    {
        return $this->nama_dudi;
    }

    /**
     * Get the [npwp_dudi] column value.
     * 
     * @return string
     */
    public function getNpwpDudi()
    {
        return $this->npwp_dudi;
    }

    /**
     * Get the [nama_bidang_usaha] column value.
     * 
     * @return string
     */
    public function getNamaBidangUsaha()
    {
        return $this->nama_bidang_usaha;
    }

    /**
     * Get the [telp_kantor] column value.
     * 
     * @return string
     */
    public function getTelpKantor()
    {
        return $this->telp_kantor;
    }

    /**
     * Get the [fax] column value.
     * 
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Get the [contact_person] column value.
     * 
     * @return string
     */
    public function getContactPerson()
    {
        return $this->contact_person;
    }

    /**
     * Get the [telp_cp] column value.
     * 
     * @return string
     */
    public function getTelpCp()
    {
        return $this->telp_cp;
    }

    /**
     * Get the [jabatan_cp] column value.
     * 
     * @return string
     */
    public function getJabatanCp()
    {
        return $this->jabatan_cp;
    }

    /**
     * Get the [optionally formatted] temporal [last_update] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastUpdate($format = 'Y-m-d H:i:s')
    {
        if ($this->last_update === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_update);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_update, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [soft_delete] column value.
     * 
     * @return string
     */
    public function getSoftDelete()
    {
        return $this->soft_delete;
    }

    /**
     * Get the [optionally formatted] temporal [last_sync] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastSync($format = 'Y-m-d H:i:s')
    {
        if ($this->last_sync === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_sync);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_sync, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [updater_id] column value.
     * 
     * @return string
     */
    public function getUpdaterId()
    {
        return $this->updater_id;
    }

    /**
     * Set the value of [mou_id] column.
     * 
     * @param string $v new value
     * @return Mou The current object (for fluent API support)
     */
    public function setMouId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->mou_id !== $v) {
            $this->mou_id = $v;
            $this->modifiedColumns[] = MouPeer::MOU_ID;
        }


        return $this;
    } // setMouId()

    /**
     * Set the value of [dudi_id] column.
     * 
     * @param string $v new value
     * @return Mou The current object (for fluent API support)
     */
    public function setDudiId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->dudi_id !== $v) {
            $this->dudi_id = $v;
            $this->modifiedColumns[] = MouPeer::DUDI_ID;
        }

        if ($this->aDudiRelatedByDudiId !== null && $this->aDudiRelatedByDudiId->getDudiId() !== $v) {
            $this->aDudiRelatedByDudiId = null;
        }

        if ($this->aDudiRelatedByDudiId !== null && $this->aDudiRelatedByDudiId->getDudiId() !== $v) {
            $this->aDudiRelatedByDudiId = null;
        }


        return $this;
    } // setDudiId()

    /**
     * Set the value of [sekolah_id] column.
     * 
     * @param string $v new value
     * @return Mou The current object (for fluent API support)
     */
    public function setSekolahId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->sekolah_id !== $v) {
            $this->sekolah_id = $v;
            $this->modifiedColumns[] = MouPeer::SEKOLAH_ID;
        }

        if ($this->aSekolahRelatedBySekolahId !== null && $this->aSekolahRelatedBySekolahId->getSekolahId() !== $v) {
            $this->aSekolahRelatedBySekolahId = null;
        }

        if ($this->aSekolahRelatedBySekolahId !== null && $this->aSekolahRelatedBySekolahId->getSekolahId() !== $v) {
            $this->aSekolahRelatedBySekolahId = null;
        }


        return $this;
    } // setSekolahId()

    /**
     * Set the value of [nomor_mou] column.
     * 
     * @param string $v new value
     * @return Mou The current object (for fluent API support)
     */
    public function setNomorMou($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nomor_mou !== $v) {
            $this->nomor_mou = $v;
            $this->modifiedColumns[] = MouPeer::NOMOR_MOU;
        }


        return $this;
    } // setNomorMou()

    /**
     * Set the value of [judul_mou] column.
     * 
     * @param string $v new value
     * @return Mou The current object (for fluent API support)
     */
    public function setJudulMou($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->judul_mou !== $v) {
            $this->judul_mou = $v;
            $this->modifiedColumns[] = MouPeer::JUDUL_MOU;
        }


        return $this;
    } // setJudulMou()

    /**
     * Set the value of [tanggal_mulai] column.
     * 
     * @param string $v new value
     * @return Mou The current object (for fluent API support)
     */
    public function setTanggalMulai($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tanggal_mulai !== $v) {
            $this->tanggal_mulai = $v;
            $this->modifiedColumns[] = MouPeer::TANGGAL_MULAI;
        }


        return $this;
    } // setTanggalMulai()

    /**
     * Set the value of [tanggal_selesai] column.
     * 
     * @param string $v new value
     * @return Mou The current object (for fluent API support)
     */
    public function setTanggalSelesai($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tanggal_selesai !== $v) {
            $this->tanggal_selesai = $v;
            $this->modifiedColumns[] = MouPeer::TANGGAL_SELESAI;
        }


        return $this;
    } // setTanggalSelesai()

    /**
     * Set the value of [nama_dudi] column.
     * 
     * @param string $v new value
     * @return Mou The current object (for fluent API support)
     */
    public function setNamaDudi($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nama_dudi !== $v) {
            $this->nama_dudi = $v;
            $this->modifiedColumns[] = MouPeer::NAMA_DUDI;
        }


        return $this;
    } // setNamaDudi()

    /**
     * Set the value of [npwp_dudi] column.
     * 
     * @param string $v new value
     * @return Mou The current object (for fluent API support)
     */
    public function setNpwpDudi($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->npwp_dudi !== $v) {
            $this->npwp_dudi = $v;
            $this->modifiedColumns[] = MouPeer::NPWP_DUDI;
        }


        return $this;
    } // setNpwpDudi()

    /**
     * Set the value of [nama_bidang_usaha] column.
     * 
     * @param string $v new value
     * @return Mou The current object (for fluent API support)
     */
    public function setNamaBidangUsaha($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nama_bidang_usaha !== $v) {
            $this->nama_bidang_usaha = $v;
            $this->modifiedColumns[] = MouPeer::NAMA_BIDANG_USAHA;
        }


        return $this;
    } // setNamaBidangUsaha()

    /**
     * Set the value of [telp_kantor] column.
     * 
     * @param string $v new value
     * @return Mou The current object (for fluent API support)
     */
    public function setTelpKantor($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->telp_kantor !== $v) {
            $this->telp_kantor = $v;
            $this->modifiedColumns[] = MouPeer::TELP_KANTOR;
        }


        return $this;
    } // setTelpKantor()

    /**
     * Set the value of [fax] column.
     * 
     * @param string $v new value
     * @return Mou The current object (for fluent API support)
     */
    public function setFax($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->fax !== $v) {
            $this->fax = $v;
            $this->modifiedColumns[] = MouPeer::FAX;
        }


        return $this;
    } // setFax()

    /**
     * Set the value of [contact_person] column.
     * 
     * @param string $v new value
     * @return Mou The current object (for fluent API support)
     */
    public function setContactPerson($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->contact_person !== $v) {
            $this->contact_person = $v;
            $this->modifiedColumns[] = MouPeer::CONTACT_PERSON;
        }


        return $this;
    } // setContactPerson()

    /**
     * Set the value of [telp_cp] column.
     * 
     * @param string $v new value
     * @return Mou The current object (for fluent API support)
     */
    public function setTelpCp($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->telp_cp !== $v) {
            $this->telp_cp = $v;
            $this->modifiedColumns[] = MouPeer::TELP_CP;
        }


        return $this;
    } // setTelpCp()

    /**
     * Set the value of [jabatan_cp] column.
     * 
     * @param string $v new value
     * @return Mou The current object (for fluent API support)
     */
    public function setJabatanCp($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->jabatan_cp !== $v) {
            $this->jabatan_cp = $v;
            $this->modifiedColumns[] = MouPeer::JABATAN_CP;
        }


        return $this;
    } // setJabatanCp()

    /**
     * Sets the value of [last_update] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Mou The current object (for fluent API support)
     */
    public function setLastUpdate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_update !== null || $dt !== null) {
            $currentDateAsString = ($this->last_update !== null && $tmpDt = new DateTime($this->last_update)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_update = $newDateAsString;
                $this->modifiedColumns[] = MouPeer::LAST_UPDATE;
            }
        } // if either are not null


        return $this;
    } // setLastUpdate()

    /**
     * Set the value of [soft_delete] column.
     * 
     * @param string $v new value
     * @return Mou The current object (for fluent API support)
     */
    public function setSoftDelete($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->soft_delete !== $v) {
            $this->soft_delete = $v;
            $this->modifiedColumns[] = MouPeer::SOFT_DELETE;
        }


        return $this;
    } // setSoftDelete()

    /**
     * Sets the value of [last_sync] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Mou The current object (for fluent API support)
     */
    public function setLastSync($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_sync !== null || $dt !== null) {
            $currentDateAsString = ($this->last_sync !== null && $tmpDt = new DateTime($this->last_sync)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_sync = $newDateAsString;
                $this->modifiedColumns[] = MouPeer::LAST_SYNC;
            }
        } // if either are not null


        return $this;
    } // setLastSync()

    /**
     * Set the value of [updater_id] column.
     * 
     * @param string $v new value
     * @return Mou The current object (for fluent API support)
     */
    public function setUpdaterId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->updater_id !== $v) {
            $this->updater_id = $v;
            $this->modifiedColumns[] = MouPeer::UPDATER_ID;
        }


        return $this;
    } // setUpdaterId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->mou_id = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
            $this->dudi_id = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->sekolah_id = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->nomor_mou = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->judul_mou = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->tanggal_mulai = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->tanggal_selesai = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->nama_dudi = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->npwp_dudi = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->nama_bidang_usaha = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->telp_kantor = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->fax = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->contact_person = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->telp_cp = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->jabatan_cp = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
            $this->last_update = ($row[$startcol + 15] !== null) ? (string) $row[$startcol + 15] : null;
            $this->soft_delete = ($row[$startcol + 16] !== null) ? (string) $row[$startcol + 16] : null;
            $this->last_sync = ($row[$startcol + 17] !== null) ? (string) $row[$startcol + 17] : null;
            $this->updater_id = ($row[$startcol + 18] !== null) ? (string) $row[$startcol + 18] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 19; // 19 = MouPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Mou object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aDudiRelatedByDudiId !== null && $this->dudi_id !== $this->aDudiRelatedByDudiId->getDudiId()) {
            $this->aDudiRelatedByDudiId = null;
        }
        if ($this->aDudiRelatedByDudiId !== null && $this->dudi_id !== $this->aDudiRelatedByDudiId->getDudiId()) {
            $this->aDudiRelatedByDudiId = null;
        }
        if ($this->aSekolahRelatedBySekolahId !== null && $this->sekolah_id !== $this->aSekolahRelatedBySekolahId->getSekolahId()) {
            $this->aSekolahRelatedBySekolahId = null;
        }
        if ($this->aSekolahRelatedBySekolahId !== null && $this->sekolah_id !== $this->aSekolahRelatedBySekolahId->getSekolahId()) {
            $this->aSekolahRelatedBySekolahId = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(MouPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = MouPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aDudiRelatedByDudiId = null;
            $this->aDudiRelatedByDudiId = null;
            $this->aSekolahRelatedBySekolahId = null;
            $this->aSekolahRelatedBySekolahId = null;
            $this->collVldMousRelatedByMouId = null;

            $this->collVldMousRelatedByMouId = null;

            $this->collJurusanKerjasamasRelatedByMouId = null;

            $this->collJurusanKerjasamasRelatedByMouId = null;

            $this->collUnitUsahaKerjasamasRelatedByMouId = null;

            $this->collUnitUsahaKerjasamasRelatedByMouId = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(MouPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = MouQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(MouPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                MouPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aDudiRelatedByDudiId !== null) {
                if ($this->aDudiRelatedByDudiId->isModified() || $this->aDudiRelatedByDudiId->isNew()) {
                    $affectedRows += $this->aDudiRelatedByDudiId->save($con);
                }
                $this->setDudiRelatedByDudiId($this->aDudiRelatedByDudiId);
            }

            if ($this->aDudiRelatedByDudiId !== null) {
                if ($this->aDudiRelatedByDudiId->isModified() || $this->aDudiRelatedByDudiId->isNew()) {
                    $affectedRows += $this->aDudiRelatedByDudiId->save($con);
                }
                $this->setDudiRelatedByDudiId($this->aDudiRelatedByDudiId);
            }

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if ($this->aSekolahRelatedBySekolahId->isModified() || $this->aSekolahRelatedBySekolahId->isNew()) {
                    $affectedRows += $this->aSekolahRelatedBySekolahId->save($con);
                }
                $this->setSekolahRelatedBySekolahId($this->aSekolahRelatedBySekolahId);
            }

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if ($this->aSekolahRelatedBySekolahId->isModified() || $this->aSekolahRelatedBySekolahId->isNew()) {
                    $affectedRows += $this->aSekolahRelatedBySekolahId->save($con);
                }
                $this->setSekolahRelatedBySekolahId($this->aSekolahRelatedBySekolahId);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->vldMousRelatedByMouIdScheduledForDeletion !== null) {
                if (!$this->vldMousRelatedByMouIdScheduledForDeletion->isEmpty()) {
                    VldMouQuery::create()
                        ->filterByPrimaryKeys($this->vldMousRelatedByMouIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldMousRelatedByMouIdScheduledForDeletion = null;
                }
            }

            if ($this->collVldMousRelatedByMouId !== null) {
                foreach ($this->collVldMousRelatedByMouId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldMousRelatedByMouIdScheduledForDeletion !== null) {
                if (!$this->vldMousRelatedByMouIdScheduledForDeletion->isEmpty()) {
                    VldMouQuery::create()
                        ->filterByPrimaryKeys($this->vldMousRelatedByMouIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldMousRelatedByMouIdScheduledForDeletion = null;
                }
            }

            if ($this->collVldMousRelatedByMouId !== null) {
                foreach ($this->collVldMousRelatedByMouId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->jurusanKerjasamasRelatedByMouIdScheduledForDeletion !== null) {
                if (!$this->jurusanKerjasamasRelatedByMouIdScheduledForDeletion->isEmpty()) {
                    JurusanKerjasamaQuery::create()
                        ->filterByPrimaryKeys($this->jurusanKerjasamasRelatedByMouIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->jurusanKerjasamasRelatedByMouIdScheduledForDeletion = null;
                }
            }

            if ($this->collJurusanKerjasamasRelatedByMouId !== null) {
                foreach ($this->collJurusanKerjasamasRelatedByMouId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->jurusanKerjasamasRelatedByMouIdScheduledForDeletion !== null) {
                if (!$this->jurusanKerjasamasRelatedByMouIdScheduledForDeletion->isEmpty()) {
                    JurusanKerjasamaQuery::create()
                        ->filterByPrimaryKeys($this->jurusanKerjasamasRelatedByMouIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->jurusanKerjasamasRelatedByMouIdScheduledForDeletion = null;
                }
            }

            if ($this->collJurusanKerjasamasRelatedByMouId !== null) {
                foreach ($this->collJurusanKerjasamasRelatedByMouId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->unitUsahaKerjasamasRelatedByMouIdScheduledForDeletion !== null) {
                if (!$this->unitUsahaKerjasamasRelatedByMouIdScheduledForDeletion->isEmpty()) {
                    UnitUsahaKerjasamaQuery::create()
                        ->filterByPrimaryKeys($this->unitUsahaKerjasamasRelatedByMouIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->unitUsahaKerjasamasRelatedByMouIdScheduledForDeletion = null;
                }
            }

            if ($this->collUnitUsahaKerjasamasRelatedByMouId !== null) {
                foreach ($this->collUnitUsahaKerjasamasRelatedByMouId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->unitUsahaKerjasamasRelatedByMouIdScheduledForDeletion !== null) {
                if (!$this->unitUsahaKerjasamasRelatedByMouIdScheduledForDeletion->isEmpty()) {
                    UnitUsahaKerjasamaQuery::create()
                        ->filterByPrimaryKeys($this->unitUsahaKerjasamasRelatedByMouIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->unitUsahaKerjasamasRelatedByMouIdScheduledForDeletion = null;
                }
            }

            if ($this->collUnitUsahaKerjasamasRelatedByMouId !== null) {
                foreach ($this->collUnitUsahaKerjasamasRelatedByMouId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $criteria = $this->buildCriteria();
        $pk = BasePeer::doInsert($criteria, $con);
        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aDudiRelatedByDudiId !== null) {
                if (!$this->aDudiRelatedByDudiId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aDudiRelatedByDudiId->getValidationFailures());
                }
            }

            if ($this->aDudiRelatedByDudiId !== null) {
                if (!$this->aDudiRelatedByDudiId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aDudiRelatedByDudiId->getValidationFailures());
                }
            }

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if (!$this->aSekolahRelatedBySekolahId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSekolahRelatedBySekolahId->getValidationFailures());
                }
            }

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if (!$this->aSekolahRelatedBySekolahId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSekolahRelatedBySekolahId->getValidationFailures());
                }
            }


            if (($retval = MouPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collVldMousRelatedByMouId !== null) {
                    foreach ($this->collVldMousRelatedByMouId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldMousRelatedByMouId !== null) {
                    foreach ($this->collVldMousRelatedByMouId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collJurusanKerjasamasRelatedByMouId !== null) {
                    foreach ($this->collJurusanKerjasamasRelatedByMouId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collJurusanKerjasamasRelatedByMouId !== null) {
                    foreach ($this->collJurusanKerjasamasRelatedByMouId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collUnitUsahaKerjasamasRelatedByMouId !== null) {
                    foreach ($this->collUnitUsahaKerjasamasRelatedByMouId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collUnitUsahaKerjasamasRelatedByMouId !== null) {
                    foreach ($this->collUnitUsahaKerjasamasRelatedByMouId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = MouPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getMouId();
                break;
            case 1:
                return $this->getDudiId();
                break;
            case 2:
                return $this->getSekolahId();
                break;
            case 3:
                return $this->getNomorMou();
                break;
            case 4:
                return $this->getJudulMou();
                break;
            case 5:
                return $this->getTanggalMulai();
                break;
            case 6:
                return $this->getTanggalSelesai();
                break;
            case 7:
                return $this->getNamaDudi();
                break;
            case 8:
                return $this->getNpwpDudi();
                break;
            case 9:
                return $this->getNamaBidangUsaha();
                break;
            case 10:
                return $this->getTelpKantor();
                break;
            case 11:
                return $this->getFax();
                break;
            case 12:
                return $this->getContactPerson();
                break;
            case 13:
                return $this->getTelpCp();
                break;
            case 14:
                return $this->getJabatanCp();
                break;
            case 15:
                return $this->getLastUpdate();
                break;
            case 16:
                return $this->getSoftDelete();
                break;
            case 17:
                return $this->getLastSync();
                break;
            case 18:
                return $this->getUpdaterId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Mou'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Mou'][$this->getPrimaryKey()] = true;
        $keys = MouPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getMouId(),
            $keys[1] => $this->getDudiId(),
            $keys[2] => $this->getSekolahId(),
            $keys[3] => $this->getNomorMou(),
            $keys[4] => $this->getJudulMou(),
            $keys[5] => $this->getTanggalMulai(),
            $keys[6] => $this->getTanggalSelesai(),
            $keys[7] => $this->getNamaDudi(),
            $keys[8] => $this->getNpwpDudi(),
            $keys[9] => $this->getNamaBidangUsaha(),
            $keys[10] => $this->getTelpKantor(),
            $keys[11] => $this->getFax(),
            $keys[12] => $this->getContactPerson(),
            $keys[13] => $this->getTelpCp(),
            $keys[14] => $this->getJabatanCp(),
            $keys[15] => $this->getLastUpdate(),
            $keys[16] => $this->getSoftDelete(),
            $keys[17] => $this->getLastSync(),
            $keys[18] => $this->getUpdaterId(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aDudiRelatedByDudiId) {
                $result['DudiRelatedByDudiId'] = $this->aDudiRelatedByDudiId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aDudiRelatedByDudiId) {
                $result['DudiRelatedByDudiId'] = $this->aDudiRelatedByDudiId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSekolahRelatedBySekolahId) {
                $result['SekolahRelatedBySekolahId'] = $this->aSekolahRelatedBySekolahId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSekolahRelatedBySekolahId) {
                $result['SekolahRelatedBySekolahId'] = $this->aSekolahRelatedBySekolahId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collVldMousRelatedByMouId) {
                $result['VldMousRelatedByMouId'] = $this->collVldMousRelatedByMouId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldMousRelatedByMouId) {
                $result['VldMousRelatedByMouId'] = $this->collVldMousRelatedByMouId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collJurusanKerjasamasRelatedByMouId) {
                $result['JurusanKerjasamasRelatedByMouId'] = $this->collJurusanKerjasamasRelatedByMouId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collJurusanKerjasamasRelatedByMouId) {
                $result['JurusanKerjasamasRelatedByMouId'] = $this->collJurusanKerjasamasRelatedByMouId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUnitUsahaKerjasamasRelatedByMouId) {
                $result['UnitUsahaKerjasamasRelatedByMouId'] = $this->collUnitUsahaKerjasamasRelatedByMouId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUnitUsahaKerjasamasRelatedByMouId) {
                $result['UnitUsahaKerjasamasRelatedByMouId'] = $this->collUnitUsahaKerjasamasRelatedByMouId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = MouPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setMouId($value);
                break;
            case 1:
                $this->setDudiId($value);
                break;
            case 2:
                $this->setSekolahId($value);
                break;
            case 3:
                $this->setNomorMou($value);
                break;
            case 4:
                $this->setJudulMou($value);
                break;
            case 5:
                $this->setTanggalMulai($value);
                break;
            case 6:
                $this->setTanggalSelesai($value);
                break;
            case 7:
                $this->setNamaDudi($value);
                break;
            case 8:
                $this->setNpwpDudi($value);
                break;
            case 9:
                $this->setNamaBidangUsaha($value);
                break;
            case 10:
                $this->setTelpKantor($value);
                break;
            case 11:
                $this->setFax($value);
                break;
            case 12:
                $this->setContactPerson($value);
                break;
            case 13:
                $this->setTelpCp($value);
                break;
            case 14:
                $this->setJabatanCp($value);
                break;
            case 15:
                $this->setLastUpdate($value);
                break;
            case 16:
                $this->setSoftDelete($value);
                break;
            case 17:
                $this->setLastSync($value);
                break;
            case 18:
                $this->setUpdaterId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = MouPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setMouId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setDudiId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setSekolahId($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setNomorMou($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setJudulMou($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setTanggalMulai($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setTanggalSelesai($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setNamaDudi($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setNpwpDudi($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setNamaBidangUsaha($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setTelpKantor($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setFax($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setContactPerson($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setTelpCp($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setJabatanCp($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setLastUpdate($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setSoftDelete($arr[$keys[16]]);
        if (array_key_exists($keys[17], $arr)) $this->setLastSync($arr[$keys[17]]);
        if (array_key_exists($keys[18], $arr)) $this->setUpdaterId($arr[$keys[18]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(MouPeer::DATABASE_NAME);

        if ($this->isColumnModified(MouPeer::MOU_ID)) $criteria->add(MouPeer::MOU_ID, $this->mou_id);
        if ($this->isColumnModified(MouPeer::DUDI_ID)) $criteria->add(MouPeer::DUDI_ID, $this->dudi_id);
        if ($this->isColumnModified(MouPeer::SEKOLAH_ID)) $criteria->add(MouPeer::SEKOLAH_ID, $this->sekolah_id);
        if ($this->isColumnModified(MouPeer::NOMOR_MOU)) $criteria->add(MouPeer::NOMOR_MOU, $this->nomor_mou);
        if ($this->isColumnModified(MouPeer::JUDUL_MOU)) $criteria->add(MouPeer::JUDUL_MOU, $this->judul_mou);
        if ($this->isColumnModified(MouPeer::TANGGAL_MULAI)) $criteria->add(MouPeer::TANGGAL_MULAI, $this->tanggal_mulai);
        if ($this->isColumnModified(MouPeer::TANGGAL_SELESAI)) $criteria->add(MouPeer::TANGGAL_SELESAI, $this->tanggal_selesai);
        if ($this->isColumnModified(MouPeer::NAMA_DUDI)) $criteria->add(MouPeer::NAMA_DUDI, $this->nama_dudi);
        if ($this->isColumnModified(MouPeer::NPWP_DUDI)) $criteria->add(MouPeer::NPWP_DUDI, $this->npwp_dudi);
        if ($this->isColumnModified(MouPeer::NAMA_BIDANG_USAHA)) $criteria->add(MouPeer::NAMA_BIDANG_USAHA, $this->nama_bidang_usaha);
        if ($this->isColumnModified(MouPeer::TELP_KANTOR)) $criteria->add(MouPeer::TELP_KANTOR, $this->telp_kantor);
        if ($this->isColumnModified(MouPeer::FAX)) $criteria->add(MouPeer::FAX, $this->fax);
        if ($this->isColumnModified(MouPeer::CONTACT_PERSON)) $criteria->add(MouPeer::CONTACT_PERSON, $this->contact_person);
        if ($this->isColumnModified(MouPeer::TELP_CP)) $criteria->add(MouPeer::TELP_CP, $this->telp_cp);
        if ($this->isColumnModified(MouPeer::JABATAN_CP)) $criteria->add(MouPeer::JABATAN_CP, $this->jabatan_cp);
        if ($this->isColumnModified(MouPeer::LAST_UPDATE)) $criteria->add(MouPeer::LAST_UPDATE, $this->last_update);
        if ($this->isColumnModified(MouPeer::SOFT_DELETE)) $criteria->add(MouPeer::SOFT_DELETE, $this->soft_delete);
        if ($this->isColumnModified(MouPeer::LAST_SYNC)) $criteria->add(MouPeer::LAST_SYNC, $this->last_sync);
        if ($this->isColumnModified(MouPeer::UPDATER_ID)) $criteria->add(MouPeer::UPDATER_ID, $this->updater_id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(MouPeer::DATABASE_NAME);
        $criteria->add(MouPeer::MOU_ID, $this->mou_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getMouId();
    }

    /**
     * Generic method to set the primary key (mou_id column).
     *
     * @param  string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setMouId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getMouId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Mou (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setDudiId($this->getDudiId());
        $copyObj->setSekolahId($this->getSekolahId());
        $copyObj->setNomorMou($this->getNomorMou());
        $copyObj->setJudulMou($this->getJudulMou());
        $copyObj->setTanggalMulai($this->getTanggalMulai());
        $copyObj->setTanggalSelesai($this->getTanggalSelesai());
        $copyObj->setNamaDudi($this->getNamaDudi());
        $copyObj->setNpwpDudi($this->getNpwpDudi());
        $copyObj->setNamaBidangUsaha($this->getNamaBidangUsaha());
        $copyObj->setTelpKantor($this->getTelpKantor());
        $copyObj->setFax($this->getFax());
        $copyObj->setContactPerson($this->getContactPerson());
        $copyObj->setTelpCp($this->getTelpCp());
        $copyObj->setJabatanCp($this->getJabatanCp());
        $copyObj->setLastUpdate($this->getLastUpdate());
        $copyObj->setSoftDelete($this->getSoftDelete());
        $copyObj->setLastSync($this->getLastSync());
        $copyObj->setUpdaterId($this->getUpdaterId());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getVldMousRelatedByMouId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldMouRelatedByMouId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldMousRelatedByMouId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldMouRelatedByMouId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getJurusanKerjasamasRelatedByMouId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addJurusanKerjasamaRelatedByMouId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getJurusanKerjasamasRelatedByMouId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addJurusanKerjasamaRelatedByMouId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUnitUsahaKerjasamasRelatedByMouId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUnitUsahaKerjasamaRelatedByMouId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUnitUsahaKerjasamasRelatedByMouId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUnitUsahaKerjasamaRelatedByMouId($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setMouId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Mou Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return MouPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new MouPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a Dudi object.
     *
     * @param             Dudi $v
     * @return Mou The current object (for fluent API support)
     * @throws PropelException
     */
    public function setDudiRelatedByDudiId(Dudi $v = null)
    {
        if ($v === null) {
            $this->setDudiId(NULL);
        } else {
            $this->setDudiId($v->getDudiId());
        }

        $this->aDudiRelatedByDudiId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Dudi object, it will not be re-added.
        if ($v !== null) {
            $v->addMouRelatedByDudiId($this);
        }


        return $this;
    }


    /**
     * Get the associated Dudi object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Dudi The associated Dudi object.
     * @throws PropelException
     */
    public function getDudiRelatedByDudiId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aDudiRelatedByDudiId === null && (($this->dudi_id !== "" && $this->dudi_id !== null)) && $doQuery) {
            $this->aDudiRelatedByDudiId = DudiQuery::create()->findPk($this->dudi_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aDudiRelatedByDudiId->addMousRelatedByDudiId($this);
             */
        }

        return $this->aDudiRelatedByDudiId;
    }

    /**
     * Declares an association between this object and a Dudi object.
     *
     * @param             Dudi $v
     * @return Mou The current object (for fluent API support)
     * @throws PropelException
     */
    public function setDudiRelatedByDudiId(Dudi $v = null)
    {
        if ($v === null) {
            $this->setDudiId(NULL);
        } else {
            $this->setDudiId($v->getDudiId());
        }

        $this->aDudiRelatedByDudiId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Dudi object, it will not be re-added.
        if ($v !== null) {
            $v->addMouRelatedByDudiId($this);
        }


        return $this;
    }


    /**
     * Get the associated Dudi object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Dudi The associated Dudi object.
     * @throws PropelException
     */
    public function getDudiRelatedByDudiId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aDudiRelatedByDudiId === null && (($this->dudi_id !== "" && $this->dudi_id !== null)) && $doQuery) {
            $this->aDudiRelatedByDudiId = DudiQuery::create()->findPk($this->dudi_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aDudiRelatedByDudiId->addMousRelatedByDudiId($this);
             */
        }

        return $this->aDudiRelatedByDudiId;
    }

    /**
     * Declares an association between this object and a Sekolah object.
     *
     * @param             Sekolah $v
     * @return Mou The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSekolahRelatedBySekolahId(Sekolah $v = null)
    {
        if ($v === null) {
            $this->setSekolahId(NULL);
        } else {
            $this->setSekolahId($v->getSekolahId());
        }

        $this->aSekolahRelatedBySekolahId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Sekolah object, it will not be re-added.
        if ($v !== null) {
            $v->addMouRelatedBySekolahId($this);
        }


        return $this;
    }


    /**
     * Get the associated Sekolah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Sekolah The associated Sekolah object.
     * @throws PropelException
     */
    public function getSekolahRelatedBySekolahId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSekolahRelatedBySekolahId === null && (($this->sekolah_id !== "" && $this->sekolah_id !== null)) && $doQuery) {
            $this->aSekolahRelatedBySekolahId = SekolahQuery::create()->findPk($this->sekolah_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSekolahRelatedBySekolahId->addMousRelatedBySekolahId($this);
             */
        }

        return $this->aSekolahRelatedBySekolahId;
    }

    /**
     * Declares an association between this object and a Sekolah object.
     *
     * @param             Sekolah $v
     * @return Mou The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSekolahRelatedBySekolahId(Sekolah $v = null)
    {
        if ($v === null) {
            $this->setSekolahId(NULL);
        } else {
            $this->setSekolahId($v->getSekolahId());
        }

        $this->aSekolahRelatedBySekolahId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Sekolah object, it will not be re-added.
        if ($v !== null) {
            $v->addMouRelatedBySekolahId($this);
        }


        return $this;
    }


    /**
     * Get the associated Sekolah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Sekolah The associated Sekolah object.
     * @throws PropelException
     */
    public function getSekolahRelatedBySekolahId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSekolahRelatedBySekolahId === null && (($this->sekolah_id !== "" && $this->sekolah_id !== null)) && $doQuery) {
            $this->aSekolahRelatedBySekolahId = SekolahQuery::create()->findPk($this->sekolah_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSekolahRelatedBySekolahId->addMousRelatedBySekolahId($this);
             */
        }

        return $this->aSekolahRelatedBySekolahId;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('VldMouRelatedByMouId' == $relationName) {
            $this->initVldMousRelatedByMouId();
        }
        if ('VldMouRelatedByMouId' == $relationName) {
            $this->initVldMousRelatedByMouId();
        }
        if ('JurusanKerjasamaRelatedByMouId' == $relationName) {
            $this->initJurusanKerjasamasRelatedByMouId();
        }
        if ('JurusanKerjasamaRelatedByMouId' == $relationName) {
            $this->initJurusanKerjasamasRelatedByMouId();
        }
        if ('UnitUsahaKerjasamaRelatedByMouId' == $relationName) {
            $this->initUnitUsahaKerjasamasRelatedByMouId();
        }
        if ('UnitUsahaKerjasamaRelatedByMouId' == $relationName) {
            $this->initUnitUsahaKerjasamasRelatedByMouId();
        }
    }

    /**
     * Clears out the collVldMousRelatedByMouId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Mou The current object (for fluent API support)
     * @see        addVldMousRelatedByMouId()
     */
    public function clearVldMousRelatedByMouId()
    {
        $this->collVldMousRelatedByMouId = null; // important to set this to null since that means it is uninitialized
        $this->collVldMousRelatedByMouIdPartial = null;

        return $this;
    }

    /**
     * reset is the collVldMousRelatedByMouId collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldMousRelatedByMouId($v = true)
    {
        $this->collVldMousRelatedByMouIdPartial = $v;
    }

    /**
     * Initializes the collVldMousRelatedByMouId collection.
     *
     * By default this just sets the collVldMousRelatedByMouId collection to an empty array (like clearcollVldMousRelatedByMouId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldMousRelatedByMouId($overrideExisting = true)
    {
        if (null !== $this->collVldMousRelatedByMouId && !$overrideExisting) {
            return;
        }
        $this->collVldMousRelatedByMouId = new PropelObjectCollection();
        $this->collVldMousRelatedByMouId->setModel('VldMou');
    }

    /**
     * Gets an array of VldMou objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Mou is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldMou[] List of VldMou objects
     * @throws PropelException
     */
    public function getVldMousRelatedByMouId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldMousRelatedByMouIdPartial && !$this->isNew();
        if (null === $this->collVldMousRelatedByMouId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldMousRelatedByMouId) {
                // return empty collection
                $this->initVldMousRelatedByMouId();
            } else {
                $collVldMousRelatedByMouId = VldMouQuery::create(null, $criteria)
                    ->filterByMouRelatedByMouId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldMousRelatedByMouIdPartial && count($collVldMousRelatedByMouId)) {
                      $this->initVldMousRelatedByMouId(false);

                      foreach($collVldMousRelatedByMouId as $obj) {
                        if (false == $this->collVldMousRelatedByMouId->contains($obj)) {
                          $this->collVldMousRelatedByMouId->append($obj);
                        }
                      }

                      $this->collVldMousRelatedByMouIdPartial = true;
                    }

                    $collVldMousRelatedByMouId->getInternalIterator()->rewind();
                    return $collVldMousRelatedByMouId;
                }

                if($partial && $this->collVldMousRelatedByMouId) {
                    foreach($this->collVldMousRelatedByMouId as $obj) {
                        if($obj->isNew()) {
                            $collVldMousRelatedByMouId[] = $obj;
                        }
                    }
                }

                $this->collVldMousRelatedByMouId = $collVldMousRelatedByMouId;
                $this->collVldMousRelatedByMouIdPartial = false;
            }
        }

        return $this->collVldMousRelatedByMouId;
    }

    /**
     * Sets a collection of VldMouRelatedByMouId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldMousRelatedByMouId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Mou The current object (for fluent API support)
     */
    public function setVldMousRelatedByMouId(PropelCollection $vldMousRelatedByMouId, PropelPDO $con = null)
    {
        $vldMousRelatedByMouIdToDelete = $this->getVldMousRelatedByMouId(new Criteria(), $con)->diff($vldMousRelatedByMouId);

        $this->vldMousRelatedByMouIdScheduledForDeletion = unserialize(serialize($vldMousRelatedByMouIdToDelete));

        foreach ($vldMousRelatedByMouIdToDelete as $vldMouRelatedByMouIdRemoved) {
            $vldMouRelatedByMouIdRemoved->setMouRelatedByMouId(null);
        }

        $this->collVldMousRelatedByMouId = null;
        foreach ($vldMousRelatedByMouId as $vldMouRelatedByMouId) {
            $this->addVldMouRelatedByMouId($vldMouRelatedByMouId);
        }

        $this->collVldMousRelatedByMouId = $vldMousRelatedByMouId;
        $this->collVldMousRelatedByMouIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldMou objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldMou objects.
     * @throws PropelException
     */
    public function countVldMousRelatedByMouId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldMousRelatedByMouIdPartial && !$this->isNew();
        if (null === $this->collVldMousRelatedByMouId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldMousRelatedByMouId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldMousRelatedByMouId());
            }
            $query = VldMouQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMouRelatedByMouId($this)
                ->count($con);
        }

        return count($this->collVldMousRelatedByMouId);
    }

    /**
     * Method called to associate a VldMou object to this object
     * through the VldMou foreign key attribute.
     *
     * @param    VldMou $l VldMou
     * @return Mou The current object (for fluent API support)
     */
    public function addVldMouRelatedByMouId(VldMou $l)
    {
        if ($this->collVldMousRelatedByMouId === null) {
            $this->initVldMousRelatedByMouId();
            $this->collVldMousRelatedByMouIdPartial = true;
        }
        if (!in_array($l, $this->collVldMousRelatedByMouId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldMouRelatedByMouId($l);
        }

        return $this;
    }

    /**
     * @param	VldMouRelatedByMouId $vldMouRelatedByMouId The vldMouRelatedByMouId object to add.
     */
    protected function doAddVldMouRelatedByMouId($vldMouRelatedByMouId)
    {
        $this->collVldMousRelatedByMouId[]= $vldMouRelatedByMouId;
        $vldMouRelatedByMouId->setMouRelatedByMouId($this);
    }

    /**
     * @param	VldMouRelatedByMouId $vldMouRelatedByMouId The vldMouRelatedByMouId object to remove.
     * @return Mou The current object (for fluent API support)
     */
    public function removeVldMouRelatedByMouId($vldMouRelatedByMouId)
    {
        if ($this->getVldMousRelatedByMouId()->contains($vldMouRelatedByMouId)) {
            $this->collVldMousRelatedByMouId->remove($this->collVldMousRelatedByMouId->search($vldMouRelatedByMouId));
            if (null === $this->vldMousRelatedByMouIdScheduledForDeletion) {
                $this->vldMousRelatedByMouIdScheduledForDeletion = clone $this->collVldMousRelatedByMouId;
                $this->vldMousRelatedByMouIdScheduledForDeletion->clear();
            }
            $this->vldMousRelatedByMouIdScheduledForDeletion[]= clone $vldMouRelatedByMouId;
            $vldMouRelatedByMouId->setMouRelatedByMouId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Mou is new, it will return
     * an empty collection; or if this Mou has previously
     * been saved, it will retrieve related VldMousRelatedByMouId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Mou.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldMou[] List of VldMou objects
     */
    public function getVldMousRelatedByMouIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldMouQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldMousRelatedByMouId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Mou is new, it will return
     * an empty collection; or if this Mou has previously
     * been saved, it will retrieve related VldMousRelatedByMouId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Mou.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldMou[] List of VldMou objects
     */
    public function getVldMousRelatedByMouIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldMouQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldMousRelatedByMouId($query, $con);
    }

    /**
     * Clears out the collVldMousRelatedByMouId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Mou The current object (for fluent API support)
     * @see        addVldMousRelatedByMouId()
     */
    public function clearVldMousRelatedByMouId()
    {
        $this->collVldMousRelatedByMouId = null; // important to set this to null since that means it is uninitialized
        $this->collVldMousRelatedByMouIdPartial = null;

        return $this;
    }

    /**
     * reset is the collVldMousRelatedByMouId collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldMousRelatedByMouId($v = true)
    {
        $this->collVldMousRelatedByMouIdPartial = $v;
    }

    /**
     * Initializes the collVldMousRelatedByMouId collection.
     *
     * By default this just sets the collVldMousRelatedByMouId collection to an empty array (like clearcollVldMousRelatedByMouId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldMousRelatedByMouId($overrideExisting = true)
    {
        if (null !== $this->collVldMousRelatedByMouId && !$overrideExisting) {
            return;
        }
        $this->collVldMousRelatedByMouId = new PropelObjectCollection();
        $this->collVldMousRelatedByMouId->setModel('VldMou');
    }

    /**
     * Gets an array of VldMou objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Mou is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldMou[] List of VldMou objects
     * @throws PropelException
     */
    public function getVldMousRelatedByMouId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldMousRelatedByMouIdPartial && !$this->isNew();
        if (null === $this->collVldMousRelatedByMouId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldMousRelatedByMouId) {
                // return empty collection
                $this->initVldMousRelatedByMouId();
            } else {
                $collVldMousRelatedByMouId = VldMouQuery::create(null, $criteria)
                    ->filterByMouRelatedByMouId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldMousRelatedByMouIdPartial && count($collVldMousRelatedByMouId)) {
                      $this->initVldMousRelatedByMouId(false);

                      foreach($collVldMousRelatedByMouId as $obj) {
                        if (false == $this->collVldMousRelatedByMouId->contains($obj)) {
                          $this->collVldMousRelatedByMouId->append($obj);
                        }
                      }

                      $this->collVldMousRelatedByMouIdPartial = true;
                    }

                    $collVldMousRelatedByMouId->getInternalIterator()->rewind();
                    return $collVldMousRelatedByMouId;
                }

                if($partial && $this->collVldMousRelatedByMouId) {
                    foreach($this->collVldMousRelatedByMouId as $obj) {
                        if($obj->isNew()) {
                            $collVldMousRelatedByMouId[] = $obj;
                        }
                    }
                }

                $this->collVldMousRelatedByMouId = $collVldMousRelatedByMouId;
                $this->collVldMousRelatedByMouIdPartial = false;
            }
        }

        return $this->collVldMousRelatedByMouId;
    }

    /**
     * Sets a collection of VldMouRelatedByMouId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldMousRelatedByMouId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Mou The current object (for fluent API support)
     */
    public function setVldMousRelatedByMouId(PropelCollection $vldMousRelatedByMouId, PropelPDO $con = null)
    {
        $vldMousRelatedByMouIdToDelete = $this->getVldMousRelatedByMouId(new Criteria(), $con)->diff($vldMousRelatedByMouId);

        $this->vldMousRelatedByMouIdScheduledForDeletion = unserialize(serialize($vldMousRelatedByMouIdToDelete));

        foreach ($vldMousRelatedByMouIdToDelete as $vldMouRelatedByMouIdRemoved) {
            $vldMouRelatedByMouIdRemoved->setMouRelatedByMouId(null);
        }

        $this->collVldMousRelatedByMouId = null;
        foreach ($vldMousRelatedByMouId as $vldMouRelatedByMouId) {
            $this->addVldMouRelatedByMouId($vldMouRelatedByMouId);
        }

        $this->collVldMousRelatedByMouId = $vldMousRelatedByMouId;
        $this->collVldMousRelatedByMouIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldMou objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldMou objects.
     * @throws PropelException
     */
    public function countVldMousRelatedByMouId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldMousRelatedByMouIdPartial && !$this->isNew();
        if (null === $this->collVldMousRelatedByMouId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldMousRelatedByMouId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldMousRelatedByMouId());
            }
            $query = VldMouQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMouRelatedByMouId($this)
                ->count($con);
        }

        return count($this->collVldMousRelatedByMouId);
    }

    /**
     * Method called to associate a VldMou object to this object
     * through the VldMou foreign key attribute.
     *
     * @param    VldMou $l VldMou
     * @return Mou The current object (for fluent API support)
     */
    public function addVldMouRelatedByMouId(VldMou $l)
    {
        if ($this->collVldMousRelatedByMouId === null) {
            $this->initVldMousRelatedByMouId();
            $this->collVldMousRelatedByMouIdPartial = true;
        }
        if (!in_array($l, $this->collVldMousRelatedByMouId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldMouRelatedByMouId($l);
        }

        return $this;
    }

    /**
     * @param	VldMouRelatedByMouId $vldMouRelatedByMouId The vldMouRelatedByMouId object to add.
     */
    protected function doAddVldMouRelatedByMouId($vldMouRelatedByMouId)
    {
        $this->collVldMousRelatedByMouId[]= $vldMouRelatedByMouId;
        $vldMouRelatedByMouId->setMouRelatedByMouId($this);
    }

    /**
     * @param	VldMouRelatedByMouId $vldMouRelatedByMouId The vldMouRelatedByMouId object to remove.
     * @return Mou The current object (for fluent API support)
     */
    public function removeVldMouRelatedByMouId($vldMouRelatedByMouId)
    {
        if ($this->getVldMousRelatedByMouId()->contains($vldMouRelatedByMouId)) {
            $this->collVldMousRelatedByMouId->remove($this->collVldMousRelatedByMouId->search($vldMouRelatedByMouId));
            if (null === $this->vldMousRelatedByMouIdScheduledForDeletion) {
                $this->vldMousRelatedByMouIdScheduledForDeletion = clone $this->collVldMousRelatedByMouId;
                $this->vldMousRelatedByMouIdScheduledForDeletion->clear();
            }
            $this->vldMousRelatedByMouIdScheduledForDeletion[]= clone $vldMouRelatedByMouId;
            $vldMouRelatedByMouId->setMouRelatedByMouId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Mou is new, it will return
     * an empty collection; or if this Mou has previously
     * been saved, it will retrieve related VldMousRelatedByMouId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Mou.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldMou[] List of VldMou objects
     */
    public function getVldMousRelatedByMouIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldMouQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldMousRelatedByMouId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Mou is new, it will return
     * an empty collection; or if this Mou has previously
     * been saved, it will retrieve related VldMousRelatedByMouId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Mou.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldMou[] List of VldMou objects
     */
    public function getVldMousRelatedByMouIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldMouQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldMousRelatedByMouId($query, $con);
    }

    /**
     * Clears out the collJurusanKerjasamasRelatedByMouId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Mou The current object (for fluent API support)
     * @see        addJurusanKerjasamasRelatedByMouId()
     */
    public function clearJurusanKerjasamasRelatedByMouId()
    {
        $this->collJurusanKerjasamasRelatedByMouId = null; // important to set this to null since that means it is uninitialized
        $this->collJurusanKerjasamasRelatedByMouIdPartial = null;

        return $this;
    }

    /**
     * reset is the collJurusanKerjasamasRelatedByMouId collection loaded partially
     *
     * @return void
     */
    public function resetPartialJurusanKerjasamasRelatedByMouId($v = true)
    {
        $this->collJurusanKerjasamasRelatedByMouIdPartial = $v;
    }

    /**
     * Initializes the collJurusanKerjasamasRelatedByMouId collection.
     *
     * By default this just sets the collJurusanKerjasamasRelatedByMouId collection to an empty array (like clearcollJurusanKerjasamasRelatedByMouId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initJurusanKerjasamasRelatedByMouId($overrideExisting = true)
    {
        if (null !== $this->collJurusanKerjasamasRelatedByMouId && !$overrideExisting) {
            return;
        }
        $this->collJurusanKerjasamasRelatedByMouId = new PropelObjectCollection();
        $this->collJurusanKerjasamasRelatedByMouId->setModel('JurusanKerjasama');
    }

    /**
     * Gets an array of JurusanKerjasama objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Mou is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|JurusanKerjasama[] List of JurusanKerjasama objects
     * @throws PropelException
     */
    public function getJurusanKerjasamasRelatedByMouId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collJurusanKerjasamasRelatedByMouIdPartial && !$this->isNew();
        if (null === $this->collJurusanKerjasamasRelatedByMouId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collJurusanKerjasamasRelatedByMouId) {
                // return empty collection
                $this->initJurusanKerjasamasRelatedByMouId();
            } else {
                $collJurusanKerjasamasRelatedByMouId = JurusanKerjasamaQuery::create(null, $criteria)
                    ->filterByMouRelatedByMouId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collJurusanKerjasamasRelatedByMouIdPartial && count($collJurusanKerjasamasRelatedByMouId)) {
                      $this->initJurusanKerjasamasRelatedByMouId(false);

                      foreach($collJurusanKerjasamasRelatedByMouId as $obj) {
                        if (false == $this->collJurusanKerjasamasRelatedByMouId->contains($obj)) {
                          $this->collJurusanKerjasamasRelatedByMouId->append($obj);
                        }
                      }

                      $this->collJurusanKerjasamasRelatedByMouIdPartial = true;
                    }

                    $collJurusanKerjasamasRelatedByMouId->getInternalIterator()->rewind();
                    return $collJurusanKerjasamasRelatedByMouId;
                }

                if($partial && $this->collJurusanKerjasamasRelatedByMouId) {
                    foreach($this->collJurusanKerjasamasRelatedByMouId as $obj) {
                        if($obj->isNew()) {
                            $collJurusanKerjasamasRelatedByMouId[] = $obj;
                        }
                    }
                }

                $this->collJurusanKerjasamasRelatedByMouId = $collJurusanKerjasamasRelatedByMouId;
                $this->collJurusanKerjasamasRelatedByMouIdPartial = false;
            }
        }

        return $this->collJurusanKerjasamasRelatedByMouId;
    }

    /**
     * Sets a collection of JurusanKerjasamaRelatedByMouId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $jurusanKerjasamasRelatedByMouId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Mou The current object (for fluent API support)
     */
    public function setJurusanKerjasamasRelatedByMouId(PropelCollection $jurusanKerjasamasRelatedByMouId, PropelPDO $con = null)
    {
        $jurusanKerjasamasRelatedByMouIdToDelete = $this->getJurusanKerjasamasRelatedByMouId(new Criteria(), $con)->diff($jurusanKerjasamasRelatedByMouId);

        $this->jurusanKerjasamasRelatedByMouIdScheduledForDeletion = unserialize(serialize($jurusanKerjasamasRelatedByMouIdToDelete));

        foreach ($jurusanKerjasamasRelatedByMouIdToDelete as $jurusanKerjasamaRelatedByMouIdRemoved) {
            $jurusanKerjasamaRelatedByMouIdRemoved->setMouRelatedByMouId(null);
        }

        $this->collJurusanKerjasamasRelatedByMouId = null;
        foreach ($jurusanKerjasamasRelatedByMouId as $jurusanKerjasamaRelatedByMouId) {
            $this->addJurusanKerjasamaRelatedByMouId($jurusanKerjasamaRelatedByMouId);
        }

        $this->collJurusanKerjasamasRelatedByMouId = $jurusanKerjasamasRelatedByMouId;
        $this->collJurusanKerjasamasRelatedByMouIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related JurusanKerjasama objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related JurusanKerjasama objects.
     * @throws PropelException
     */
    public function countJurusanKerjasamasRelatedByMouId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collJurusanKerjasamasRelatedByMouIdPartial && !$this->isNew();
        if (null === $this->collJurusanKerjasamasRelatedByMouId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collJurusanKerjasamasRelatedByMouId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getJurusanKerjasamasRelatedByMouId());
            }
            $query = JurusanKerjasamaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMouRelatedByMouId($this)
                ->count($con);
        }

        return count($this->collJurusanKerjasamasRelatedByMouId);
    }

    /**
     * Method called to associate a JurusanKerjasama object to this object
     * through the JurusanKerjasama foreign key attribute.
     *
     * @param    JurusanKerjasama $l JurusanKerjasama
     * @return Mou The current object (for fluent API support)
     */
    public function addJurusanKerjasamaRelatedByMouId(JurusanKerjasama $l)
    {
        if ($this->collJurusanKerjasamasRelatedByMouId === null) {
            $this->initJurusanKerjasamasRelatedByMouId();
            $this->collJurusanKerjasamasRelatedByMouIdPartial = true;
        }
        if (!in_array($l, $this->collJurusanKerjasamasRelatedByMouId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddJurusanKerjasamaRelatedByMouId($l);
        }

        return $this;
    }

    /**
     * @param	JurusanKerjasamaRelatedByMouId $jurusanKerjasamaRelatedByMouId The jurusanKerjasamaRelatedByMouId object to add.
     */
    protected function doAddJurusanKerjasamaRelatedByMouId($jurusanKerjasamaRelatedByMouId)
    {
        $this->collJurusanKerjasamasRelatedByMouId[]= $jurusanKerjasamaRelatedByMouId;
        $jurusanKerjasamaRelatedByMouId->setMouRelatedByMouId($this);
    }

    /**
     * @param	JurusanKerjasamaRelatedByMouId $jurusanKerjasamaRelatedByMouId The jurusanKerjasamaRelatedByMouId object to remove.
     * @return Mou The current object (for fluent API support)
     */
    public function removeJurusanKerjasamaRelatedByMouId($jurusanKerjasamaRelatedByMouId)
    {
        if ($this->getJurusanKerjasamasRelatedByMouId()->contains($jurusanKerjasamaRelatedByMouId)) {
            $this->collJurusanKerjasamasRelatedByMouId->remove($this->collJurusanKerjasamasRelatedByMouId->search($jurusanKerjasamaRelatedByMouId));
            if (null === $this->jurusanKerjasamasRelatedByMouIdScheduledForDeletion) {
                $this->jurusanKerjasamasRelatedByMouIdScheduledForDeletion = clone $this->collJurusanKerjasamasRelatedByMouId;
                $this->jurusanKerjasamasRelatedByMouIdScheduledForDeletion->clear();
            }
            $this->jurusanKerjasamasRelatedByMouIdScheduledForDeletion[]= clone $jurusanKerjasamaRelatedByMouId;
            $jurusanKerjasamaRelatedByMouId->setMouRelatedByMouId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Mou is new, it will return
     * an empty collection; or if this Mou has previously
     * been saved, it will retrieve related JurusanKerjasamasRelatedByMouId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Mou.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|JurusanKerjasama[] List of JurusanKerjasama objects
     */
    public function getJurusanKerjasamasRelatedByMouIdJoinJurusanSpRelatedByJurusanSpId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = JurusanKerjasamaQuery::create(null, $criteria);
        $query->joinWith('JurusanSpRelatedByJurusanSpId', $join_behavior);

        return $this->getJurusanKerjasamasRelatedByMouId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Mou is new, it will return
     * an empty collection; or if this Mou has previously
     * been saved, it will retrieve related JurusanKerjasamasRelatedByMouId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Mou.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|JurusanKerjasama[] List of JurusanKerjasama objects
     */
    public function getJurusanKerjasamasRelatedByMouIdJoinJurusanSpRelatedByJurusanSpId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = JurusanKerjasamaQuery::create(null, $criteria);
        $query->joinWith('JurusanSpRelatedByJurusanSpId', $join_behavior);

        return $this->getJurusanKerjasamasRelatedByMouId($query, $con);
    }

    /**
     * Clears out the collJurusanKerjasamasRelatedByMouId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Mou The current object (for fluent API support)
     * @see        addJurusanKerjasamasRelatedByMouId()
     */
    public function clearJurusanKerjasamasRelatedByMouId()
    {
        $this->collJurusanKerjasamasRelatedByMouId = null; // important to set this to null since that means it is uninitialized
        $this->collJurusanKerjasamasRelatedByMouIdPartial = null;

        return $this;
    }

    /**
     * reset is the collJurusanKerjasamasRelatedByMouId collection loaded partially
     *
     * @return void
     */
    public function resetPartialJurusanKerjasamasRelatedByMouId($v = true)
    {
        $this->collJurusanKerjasamasRelatedByMouIdPartial = $v;
    }

    /**
     * Initializes the collJurusanKerjasamasRelatedByMouId collection.
     *
     * By default this just sets the collJurusanKerjasamasRelatedByMouId collection to an empty array (like clearcollJurusanKerjasamasRelatedByMouId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initJurusanKerjasamasRelatedByMouId($overrideExisting = true)
    {
        if (null !== $this->collJurusanKerjasamasRelatedByMouId && !$overrideExisting) {
            return;
        }
        $this->collJurusanKerjasamasRelatedByMouId = new PropelObjectCollection();
        $this->collJurusanKerjasamasRelatedByMouId->setModel('JurusanKerjasama');
    }

    /**
     * Gets an array of JurusanKerjasama objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Mou is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|JurusanKerjasama[] List of JurusanKerjasama objects
     * @throws PropelException
     */
    public function getJurusanKerjasamasRelatedByMouId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collJurusanKerjasamasRelatedByMouIdPartial && !$this->isNew();
        if (null === $this->collJurusanKerjasamasRelatedByMouId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collJurusanKerjasamasRelatedByMouId) {
                // return empty collection
                $this->initJurusanKerjasamasRelatedByMouId();
            } else {
                $collJurusanKerjasamasRelatedByMouId = JurusanKerjasamaQuery::create(null, $criteria)
                    ->filterByMouRelatedByMouId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collJurusanKerjasamasRelatedByMouIdPartial && count($collJurusanKerjasamasRelatedByMouId)) {
                      $this->initJurusanKerjasamasRelatedByMouId(false);

                      foreach($collJurusanKerjasamasRelatedByMouId as $obj) {
                        if (false == $this->collJurusanKerjasamasRelatedByMouId->contains($obj)) {
                          $this->collJurusanKerjasamasRelatedByMouId->append($obj);
                        }
                      }

                      $this->collJurusanKerjasamasRelatedByMouIdPartial = true;
                    }

                    $collJurusanKerjasamasRelatedByMouId->getInternalIterator()->rewind();
                    return $collJurusanKerjasamasRelatedByMouId;
                }

                if($partial && $this->collJurusanKerjasamasRelatedByMouId) {
                    foreach($this->collJurusanKerjasamasRelatedByMouId as $obj) {
                        if($obj->isNew()) {
                            $collJurusanKerjasamasRelatedByMouId[] = $obj;
                        }
                    }
                }

                $this->collJurusanKerjasamasRelatedByMouId = $collJurusanKerjasamasRelatedByMouId;
                $this->collJurusanKerjasamasRelatedByMouIdPartial = false;
            }
        }

        return $this->collJurusanKerjasamasRelatedByMouId;
    }

    /**
     * Sets a collection of JurusanKerjasamaRelatedByMouId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $jurusanKerjasamasRelatedByMouId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Mou The current object (for fluent API support)
     */
    public function setJurusanKerjasamasRelatedByMouId(PropelCollection $jurusanKerjasamasRelatedByMouId, PropelPDO $con = null)
    {
        $jurusanKerjasamasRelatedByMouIdToDelete = $this->getJurusanKerjasamasRelatedByMouId(new Criteria(), $con)->diff($jurusanKerjasamasRelatedByMouId);

        $this->jurusanKerjasamasRelatedByMouIdScheduledForDeletion = unserialize(serialize($jurusanKerjasamasRelatedByMouIdToDelete));

        foreach ($jurusanKerjasamasRelatedByMouIdToDelete as $jurusanKerjasamaRelatedByMouIdRemoved) {
            $jurusanKerjasamaRelatedByMouIdRemoved->setMouRelatedByMouId(null);
        }

        $this->collJurusanKerjasamasRelatedByMouId = null;
        foreach ($jurusanKerjasamasRelatedByMouId as $jurusanKerjasamaRelatedByMouId) {
            $this->addJurusanKerjasamaRelatedByMouId($jurusanKerjasamaRelatedByMouId);
        }

        $this->collJurusanKerjasamasRelatedByMouId = $jurusanKerjasamasRelatedByMouId;
        $this->collJurusanKerjasamasRelatedByMouIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related JurusanKerjasama objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related JurusanKerjasama objects.
     * @throws PropelException
     */
    public function countJurusanKerjasamasRelatedByMouId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collJurusanKerjasamasRelatedByMouIdPartial && !$this->isNew();
        if (null === $this->collJurusanKerjasamasRelatedByMouId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collJurusanKerjasamasRelatedByMouId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getJurusanKerjasamasRelatedByMouId());
            }
            $query = JurusanKerjasamaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMouRelatedByMouId($this)
                ->count($con);
        }

        return count($this->collJurusanKerjasamasRelatedByMouId);
    }

    /**
     * Method called to associate a JurusanKerjasama object to this object
     * through the JurusanKerjasama foreign key attribute.
     *
     * @param    JurusanKerjasama $l JurusanKerjasama
     * @return Mou The current object (for fluent API support)
     */
    public function addJurusanKerjasamaRelatedByMouId(JurusanKerjasama $l)
    {
        if ($this->collJurusanKerjasamasRelatedByMouId === null) {
            $this->initJurusanKerjasamasRelatedByMouId();
            $this->collJurusanKerjasamasRelatedByMouIdPartial = true;
        }
        if (!in_array($l, $this->collJurusanKerjasamasRelatedByMouId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddJurusanKerjasamaRelatedByMouId($l);
        }

        return $this;
    }

    /**
     * @param	JurusanKerjasamaRelatedByMouId $jurusanKerjasamaRelatedByMouId The jurusanKerjasamaRelatedByMouId object to add.
     */
    protected function doAddJurusanKerjasamaRelatedByMouId($jurusanKerjasamaRelatedByMouId)
    {
        $this->collJurusanKerjasamasRelatedByMouId[]= $jurusanKerjasamaRelatedByMouId;
        $jurusanKerjasamaRelatedByMouId->setMouRelatedByMouId($this);
    }

    /**
     * @param	JurusanKerjasamaRelatedByMouId $jurusanKerjasamaRelatedByMouId The jurusanKerjasamaRelatedByMouId object to remove.
     * @return Mou The current object (for fluent API support)
     */
    public function removeJurusanKerjasamaRelatedByMouId($jurusanKerjasamaRelatedByMouId)
    {
        if ($this->getJurusanKerjasamasRelatedByMouId()->contains($jurusanKerjasamaRelatedByMouId)) {
            $this->collJurusanKerjasamasRelatedByMouId->remove($this->collJurusanKerjasamasRelatedByMouId->search($jurusanKerjasamaRelatedByMouId));
            if (null === $this->jurusanKerjasamasRelatedByMouIdScheduledForDeletion) {
                $this->jurusanKerjasamasRelatedByMouIdScheduledForDeletion = clone $this->collJurusanKerjasamasRelatedByMouId;
                $this->jurusanKerjasamasRelatedByMouIdScheduledForDeletion->clear();
            }
            $this->jurusanKerjasamasRelatedByMouIdScheduledForDeletion[]= clone $jurusanKerjasamaRelatedByMouId;
            $jurusanKerjasamaRelatedByMouId->setMouRelatedByMouId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Mou is new, it will return
     * an empty collection; or if this Mou has previously
     * been saved, it will retrieve related JurusanKerjasamasRelatedByMouId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Mou.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|JurusanKerjasama[] List of JurusanKerjasama objects
     */
    public function getJurusanKerjasamasRelatedByMouIdJoinJurusanSpRelatedByJurusanSpId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = JurusanKerjasamaQuery::create(null, $criteria);
        $query->joinWith('JurusanSpRelatedByJurusanSpId', $join_behavior);

        return $this->getJurusanKerjasamasRelatedByMouId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Mou is new, it will return
     * an empty collection; or if this Mou has previously
     * been saved, it will retrieve related JurusanKerjasamasRelatedByMouId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Mou.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|JurusanKerjasama[] List of JurusanKerjasama objects
     */
    public function getJurusanKerjasamasRelatedByMouIdJoinJurusanSpRelatedByJurusanSpId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = JurusanKerjasamaQuery::create(null, $criteria);
        $query->joinWith('JurusanSpRelatedByJurusanSpId', $join_behavior);

        return $this->getJurusanKerjasamasRelatedByMouId($query, $con);
    }

    /**
     * Clears out the collUnitUsahaKerjasamasRelatedByMouId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Mou The current object (for fluent API support)
     * @see        addUnitUsahaKerjasamasRelatedByMouId()
     */
    public function clearUnitUsahaKerjasamasRelatedByMouId()
    {
        $this->collUnitUsahaKerjasamasRelatedByMouId = null; // important to set this to null since that means it is uninitialized
        $this->collUnitUsahaKerjasamasRelatedByMouIdPartial = null;

        return $this;
    }

    /**
     * reset is the collUnitUsahaKerjasamasRelatedByMouId collection loaded partially
     *
     * @return void
     */
    public function resetPartialUnitUsahaKerjasamasRelatedByMouId($v = true)
    {
        $this->collUnitUsahaKerjasamasRelatedByMouIdPartial = $v;
    }

    /**
     * Initializes the collUnitUsahaKerjasamasRelatedByMouId collection.
     *
     * By default this just sets the collUnitUsahaKerjasamasRelatedByMouId collection to an empty array (like clearcollUnitUsahaKerjasamasRelatedByMouId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUnitUsahaKerjasamasRelatedByMouId($overrideExisting = true)
    {
        if (null !== $this->collUnitUsahaKerjasamasRelatedByMouId && !$overrideExisting) {
            return;
        }
        $this->collUnitUsahaKerjasamasRelatedByMouId = new PropelObjectCollection();
        $this->collUnitUsahaKerjasamasRelatedByMouId->setModel('UnitUsahaKerjasama');
    }

    /**
     * Gets an array of UnitUsahaKerjasama objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Mou is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|UnitUsahaKerjasama[] List of UnitUsahaKerjasama objects
     * @throws PropelException
     */
    public function getUnitUsahaKerjasamasRelatedByMouId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collUnitUsahaKerjasamasRelatedByMouIdPartial && !$this->isNew();
        if (null === $this->collUnitUsahaKerjasamasRelatedByMouId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collUnitUsahaKerjasamasRelatedByMouId) {
                // return empty collection
                $this->initUnitUsahaKerjasamasRelatedByMouId();
            } else {
                $collUnitUsahaKerjasamasRelatedByMouId = UnitUsahaKerjasamaQuery::create(null, $criteria)
                    ->filterByMouRelatedByMouId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collUnitUsahaKerjasamasRelatedByMouIdPartial && count($collUnitUsahaKerjasamasRelatedByMouId)) {
                      $this->initUnitUsahaKerjasamasRelatedByMouId(false);

                      foreach($collUnitUsahaKerjasamasRelatedByMouId as $obj) {
                        if (false == $this->collUnitUsahaKerjasamasRelatedByMouId->contains($obj)) {
                          $this->collUnitUsahaKerjasamasRelatedByMouId->append($obj);
                        }
                      }

                      $this->collUnitUsahaKerjasamasRelatedByMouIdPartial = true;
                    }

                    $collUnitUsahaKerjasamasRelatedByMouId->getInternalIterator()->rewind();
                    return $collUnitUsahaKerjasamasRelatedByMouId;
                }

                if($partial && $this->collUnitUsahaKerjasamasRelatedByMouId) {
                    foreach($this->collUnitUsahaKerjasamasRelatedByMouId as $obj) {
                        if($obj->isNew()) {
                            $collUnitUsahaKerjasamasRelatedByMouId[] = $obj;
                        }
                    }
                }

                $this->collUnitUsahaKerjasamasRelatedByMouId = $collUnitUsahaKerjasamasRelatedByMouId;
                $this->collUnitUsahaKerjasamasRelatedByMouIdPartial = false;
            }
        }

        return $this->collUnitUsahaKerjasamasRelatedByMouId;
    }

    /**
     * Sets a collection of UnitUsahaKerjasamaRelatedByMouId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $unitUsahaKerjasamasRelatedByMouId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Mou The current object (for fluent API support)
     */
    public function setUnitUsahaKerjasamasRelatedByMouId(PropelCollection $unitUsahaKerjasamasRelatedByMouId, PropelPDO $con = null)
    {
        $unitUsahaKerjasamasRelatedByMouIdToDelete = $this->getUnitUsahaKerjasamasRelatedByMouId(new Criteria(), $con)->diff($unitUsahaKerjasamasRelatedByMouId);

        $this->unitUsahaKerjasamasRelatedByMouIdScheduledForDeletion = unserialize(serialize($unitUsahaKerjasamasRelatedByMouIdToDelete));

        foreach ($unitUsahaKerjasamasRelatedByMouIdToDelete as $unitUsahaKerjasamaRelatedByMouIdRemoved) {
            $unitUsahaKerjasamaRelatedByMouIdRemoved->setMouRelatedByMouId(null);
        }

        $this->collUnitUsahaKerjasamasRelatedByMouId = null;
        foreach ($unitUsahaKerjasamasRelatedByMouId as $unitUsahaKerjasamaRelatedByMouId) {
            $this->addUnitUsahaKerjasamaRelatedByMouId($unitUsahaKerjasamaRelatedByMouId);
        }

        $this->collUnitUsahaKerjasamasRelatedByMouId = $unitUsahaKerjasamasRelatedByMouId;
        $this->collUnitUsahaKerjasamasRelatedByMouIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related UnitUsahaKerjasama objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related UnitUsahaKerjasama objects.
     * @throws PropelException
     */
    public function countUnitUsahaKerjasamasRelatedByMouId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collUnitUsahaKerjasamasRelatedByMouIdPartial && !$this->isNew();
        if (null === $this->collUnitUsahaKerjasamasRelatedByMouId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUnitUsahaKerjasamasRelatedByMouId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getUnitUsahaKerjasamasRelatedByMouId());
            }
            $query = UnitUsahaKerjasamaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMouRelatedByMouId($this)
                ->count($con);
        }

        return count($this->collUnitUsahaKerjasamasRelatedByMouId);
    }

    /**
     * Method called to associate a UnitUsahaKerjasama object to this object
     * through the UnitUsahaKerjasama foreign key attribute.
     *
     * @param    UnitUsahaKerjasama $l UnitUsahaKerjasama
     * @return Mou The current object (for fluent API support)
     */
    public function addUnitUsahaKerjasamaRelatedByMouId(UnitUsahaKerjasama $l)
    {
        if ($this->collUnitUsahaKerjasamasRelatedByMouId === null) {
            $this->initUnitUsahaKerjasamasRelatedByMouId();
            $this->collUnitUsahaKerjasamasRelatedByMouIdPartial = true;
        }
        if (!in_array($l, $this->collUnitUsahaKerjasamasRelatedByMouId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddUnitUsahaKerjasamaRelatedByMouId($l);
        }

        return $this;
    }

    /**
     * @param	UnitUsahaKerjasamaRelatedByMouId $unitUsahaKerjasamaRelatedByMouId The unitUsahaKerjasamaRelatedByMouId object to add.
     */
    protected function doAddUnitUsahaKerjasamaRelatedByMouId($unitUsahaKerjasamaRelatedByMouId)
    {
        $this->collUnitUsahaKerjasamasRelatedByMouId[]= $unitUsahaKerjasamaRelatedByMouId;
        $unitUsahaKerjasamaRelatedByMouId->setMouRelatedByMouId($this);
    }

    /**
     * @param	UnitUsahaKerjasamaRelatedByMouId $unitUsahaKerjasamaRelatedByMouId The unitUsahaKerjasamaRelatedByMouId object to remove.
     * @return Mou The current object (for fluent API support)
     */
    public function removeUnitUsahaKerjasamaRelatedByMouId($unitUsahaKerjasamaRelatedByMouId)
    {
        if ($this->getUnitUsahaKerjasamasRelatedByMouId()->contains($unitUsahaKerjasamaRelatedByMouId)) {
            $this->collUnitUsahaKerjasamasRelatedByMouId->remove($this->collUnitUsahaKerjasamasRelatedByMouId->search($unitUsahaKerjasamaRelatedByMouId));
            if (null === $this->unitUsahaKerjasamasRelatedByMouIdScheduledForDeletion) {
                $this->unitUsahaKerjasamasRelatedByMouIdScheduledForDeletion = clone $this->collUnitUsahaKerjasamasRelatedByMouId;
                $this->unitUsahaKerjasamasRelatedByMouIdScheduledForDeletion->clear();
            }
            $this->unitUsahaKerjasamasRelatedByMouIdScheduledForDeletion[]= clone $unitUsahaKerjasamaRelatedByMouId;
            $unitUsahaKerjasamaRelatedByMouId->setMouRelatedByMouId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Mou is new, it will return
     * an empty collection; or if this Mou has previously
     * been saved, it will retrieve related UnitUsahaKerjasamasRelatedByMouId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Mou.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|UnitUsahaKerjasama[] List of UnitUsahaKerjasama objects
     */
    public function getUnitUsahaKerjasamasRelatedByMouIdJoinUnitUsahaRelatedByUnitUsahaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = UnitUsahaKerjasamaQuery::create(null, $criteria);
        $query->joinWith('UnitUsahaRelatedByUnitUsahaId', $join_behavior);

        return $this->getUnitUsahaKerjasamasRelatedByMouId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Mou is new, it will return
     * an empty collection; or if this Mou has previously
     * been saved, it will retrieve related UnitUsahaKerjasamasRelatedByMouId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Mou.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|UnitUsahaKerjasama[] List of UnitUsahaKerjasama objects
     */
    public function getUnitUsahaKerjasamasRelatedByMouIdJoinUnitUsahaRelatedByUnitUsahaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = UnitUsahaKerjasamaQuery::create(null, $criteria);
        $query->joinWith('UnitUsahaRelatedByUnitUsahaId', $join_behavior);

        return $this->getUnitUsahaKerjasamasRelatedByMouId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Mou is new, it will return
     * an empty collection; or if this Mou has previously
     * been saved, it will retrieve related UnitUsahaKerjasamasRelatedByMouId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Mou.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|UnitUsahaKerjasama[] List of UnitUsahaKerjasama objects
     */
    public function getUnitUsahaKerjasamasRelatedByMouIdJoinSumberDanaRelatedBySumberDanaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = UnitUsahaKerjasamaQuery::create(null, $criteria);
        $query->joinWith('SumberDanaRelatedBySumberDanaId', $join_behavior);

        return $this->getUnitUsahaKerjasamasRelatedByMouId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Mou is new, it will return
     * an empty collection; or if this Mou has previously
     * been saved, it will retrieve related UnitUsahaKerjasamasRelatedByMouId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Mou.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|UnitUsahaKerjasama[] List of UnitUsahaKerjasama objects
     */
    public function getUnitUsahaKerjasamasRelatedByMouIdJoinSumberDanaRelatedBySumberDanaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = UnitUsahaKerjasamaQuery::create(null, $criteria);
        $query->joinWith('SumberDanaRelatedBySumberDanaId', $join_behavior);

        return $this->getUnitUsahaKerjasamasRelatedByMouId($query, $con);
    }

    /**
     * Clears out the collUnitUsahaKerjasamasRelatedByMouId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Mou The current object (for fluent API support)
     * @see        addUnitUsahaKerjasamasRelatedByMouId()
     */
    public function clearUnitUsahaKerjasamasRelatedByMouId()
    {
        $this->collUnitUsahaKerjasamasRelatedByMouId = null; // important to set this to null since that means it is uninitialized
        $this->collUnitUsahaKerjasamasRelatedByMouIdPartial = null;

        return $this;
    }

    /**
     * reset is the collUnitUsahaKerjasamasRelatedByMouId collection loaded partially
     *
     * @return void
     */
    public function resetPartialUnitUsahaKerjasamasRelatedByMouId($v = true)
    {
        $this->collUnitUsahaKerjasamasRelatedByMouIdPartial = $v;
    }

    /**
     * Initializes the collUnitUsahaKerjasamasRelatedByMouId collection.
     *
     * By default this just sets the collUnitUsahaKerjasamasRelatedByMouId collection to an empty array (like clearcollUnitUsahaKerjasamasRelatedByMouId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUnitUsahaKerjasamasRelatedByMouId($overrideExisting = true)
    {
        if (null !== $this->collUnitUsahaKerjasamasRelatedByMouId && !$overrideExisting) {
            return;
        }
        $this->collUnitUsahaKerjasamasRelatedByMouId = new PropelObjectCollection();
        $this->collUnitUsahaKerjasamasRelatedByMouId->setModel('UnitUsahaKerjasama');
    }

    /**
     * Gets an array of UnitUsahaKerjasama objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Mou is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|UnitUsahaKerjasama[] List of UnitUsahaKerjasama objects
     * @throws PropelException
     */
    public function getUnitUsahaKerjasamasRelatedByMouId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collUnitUsahaKerjasamasRelatedByMouIdPartial && !$this->isNew();
        if (null === $this->collUnitUsahaKerjasamasRelatedByMouId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collUnitUsahaKerjasamasRelatedByMouId) {
                // return empty collection
                $this->initUnitUsahaKerjasamasRelatedByMouId();
            } else {
                $collUnitUsahaKerjasamasRelatedByMouId = UnitUsahaKerjasamaQuery::create(null, $criteria)
                    ->filterByMouRelatedByMouId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collUnitUsahaKerjasamasRelatedByMouIdPartial && count($collUnitUsahaKerjasamasRelatedByMouId)) {
                      $this->initUnitUsahaKerjasamasRelatedByMouId(false);

                      foreach($collUnitUsahaKerjasamasRelatedByMouId as $obj) {
                        if (false == $this->collUnitUsahaKerjasamasRelatedByMouId->contains($obj)) {
                          $this->collUnitUsahaKerjasamasRelatedByMouId->append($obj);
                        }
                      }

                      $this->collUnitUsahaKerjasamasRelatedByMouIdPartial = true;
                    }

                    $collUnitUsahaKerjasamasRelatedByMouId->getInternalIterator()->rewind();
                    return $collUnitUsahaKerjasamasRelatedByMouId;
                }

                if($partial && $this->collUnitUsahaKerjasamasRelatedByMouId) {
                    foreach($this->collUnitUsahaKerjasamasRelatedByMouId as $obj) {
                        if($obj->isNew()) {
                            $collUnitUsahaKerjasamasRelatedByMouId[] = $obj;
                        }
                    }
                }

                $this->collUnitUsahaKerjasamasRelatedByMouId = $collUnitUsahaKerjasamasRelatedByMouId;
                $this->collUnitUsahaKerjasamasRelatedByMouIdPartial = false;
            }
        }

        return $this->collUnitUsahaKerjasamasRelatedByMouId;
    }

    /**
     * Sets a collection of UnitUsahaKerjasamaRelatedByMouId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $unitUsahaKerjasamasRelatedByMouId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Mou The current object (for fluent API support)
     */
    public function setUnitUsahaKerjasamasRelatedByMouId(PropelCollection $unitUsahaKerjasamasRelatedByMouId, PropelPDO $con = null)
    {
        $unitUsahaKerjasamasRelatedByMouIdToDelete = $this->getUnitUsahaKerjasamasRelatedByMouId(new Criteria(), $con)->diff($unitUsahaKerjasamasRelatedByMouId);

        $this->unitUsahaKerjasamasRelatedByMouIdScheduledForDeletion = unserialize(serialize($unitUsahaKerjasamasRelatedByMouIdToDelete));

        foreach ($unitUsahaKerjasamasRelatedByMouIdToDelete as $unitUsahaKerjasamaRelatedByMouIdRemoved) {
            $unitUsahaKerjasamaRelatedByMouIdRemoved->setMouRelatedByMouId(null);
        }

        $this->collUnitUsahaKerjasamasRelatedByMouId = null;
        foreach ($unitUsahaKerjasamasRelatedByMouId as $unitUsahaKerjasamaRelatedByMouId) {
            $this->addUnitUsahaKerjasamaRelatedByMouId($unitUsahaKerjasamaRelatedByMouId);
        }

        $this->collUnitUsahaKerjasamasRelatedByMouId = $unitUsahaKerjasamasRelatedByMouId;
        $this->collUnitUsahaKerjasamasRelatedByMouIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related UnitUsahaKerjasama objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related UnitUsahaKerjasama objects.
     * @throws PropelException
     */
    public function countUnitUsahaKerjasamasRelatedByMouId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collUnitUsahaKerjasamasRelatedByMouIdPartial && !$this->isNew();
        if (null === $this->collUnitUsahaKerjasamasRelatedByMouId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUnitUsahaKerjasamasRelatedByMouId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getUnitUsahaKerjasamasRelatedByMouId());
            }
            $query = UnitUsahaKerjasamaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMouRelatedByMouId($this)
                ->count($con);
        }

        return count($this->collUnitUsahaKerjasamasRelatedByMouId);
    }

    /**
     * Method called to associate a UnitUsahaKerjasama object to this object
     * through the UnitUsahaKerjasama foreign key attribute.
     *
     * @param    UnitUsahaKerjasama $l UnitUsahaKerjasama
     * @return Mou The current object (for fluent API support)
     */
    public function addUnitUsahaKerjasamaRelatedByMouId(UnitUsahaKerjasama $l)
    {
        if ($this->collUnitUsahaKerjasamasRelatedByMouId === null) {
            $this->initUnitUsahaKerjasamasRelatedByMouId();
            $this->collUnitUsahaKerjasamasRelatedByMouIdPartial = true;
        }
        if (!in_array($l, $this->collUnitUsahaKerjasamasRelatedByMouId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddUnitUsahaKerjasamaRelatedByMouId($l);
        }

        return $this;
    }

    /**
     * @param	UnitUsahaKerjasamaRelatedByMouId $unitUsahaKerjasamaRelatedByMouId The unitUsahaKerjasamaRelatedByMouId object to add.
     */
    protected function doAddUnitUsahaKerjasamaRelatedByMouId($unitUsahaKerjasamaRelatedByMouId)
    {
        $this->collUnitUsahaKerjasamasRelatedByMouId[]= $unitUsahaKerjasamaRelatedByMouId;
        $unitUsahaKerjasamaRelatedByMouId->setMouRelatedByMouId($this);
    }

    /**
     * @param	UnitUsahaKerjasamaRelatedByMouId $unitUsahaKerjasamaRelatedByMouId The unitUsahaKerjasamaRelatedByMouId object to remove.
     * @return Mou The current object (for fluent API support)
     */
    public function removeUnitUsahaKerjasamaRelatedByMouId($unitUsahaKerjasamaRelatedByMouId)
    {
        if ($this->getUnitUsahaKerjasamasRelatedByMouId()->contains($unitUsahaKerjasamaRelatedByMouId)) {
            $this->collUnitUsahaKerjasamasRelatedByMouId->remove($this->collUnitUsahaKerjasamasRelatedByMouId->search($unitUsahaKerjasamaRelatedByMouId));
            if (null === $this->unitUsahaKerjasamasRelatedByMouIdScheduledForDeletion) {
                $this->unitUsahaKerjasamasRelatedByMouIdScheduledForDeletion = clone $this->collUnitUsahaKerjasamasRelatedByMouId;
                $this->unitUsahaKerjasamasRelatedByMouIdScheduledForDeletion->clear();
            }
            $this->unitUsahaKerjasamasRelatedByMouIdScheduledForDeletion[]= clone $unitUsahaKerjasamaRelatedByMouId;
            $unitUsahaKerjasamaRelatedByMouId->setMouRelatedByMouId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Mou is new, it will return
     * an empty collection; or if this Mou has previously
     * been saved, it will retrieve related UnitUsahaKerjasamasRelatedByMouId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Mou.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|UnitUsahaKerjasama[] List of UnitUsahaKerjasama objects
     */
    public function getUnitUsahaKerjasamasRelatedByMouIdJoinUnitUsahaRelatedByUnitUsahaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = UnitUsahaKerjasamaQuery::create(null, $criteria);
        $query->joinWith('UnitUsahaRelatedByUnitUsahaId', $join_behavior);

        return $this->getUnitUsahaKerjasamasRelatedByMouId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Mou is new, it will return
     * an empty collection; or if this Mou has previously
     * been saved, it will retrieve related UnitUsahaKerjasamasRelatedByMouId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Mou.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|UnitUsahaKerjasama[] List of UnitUsahaKerjasama objects
     */
    public function getUnitUsahaKerjasamasRelatedByMouIdJoinUnitUsahaRelatedByUnitUsahaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = UnitUsahaKerjasamaQuery::create(null, $criteria);
        $query->joinWith('UnitUsahaRelatedByUnitUsahaId', $join_behavior);

        return $this->getUnitUsahaKerjasamasRelatedByMouId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Mou is new, it will return
     * an empty collection; or if this Mou has previously
     * been saved, it will retrieve related UnitUsahaKerjasamasRelatedByMouId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Mou.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|UnitUsahaKerjasama[] List of UnitUsahaKerjasama objects
     */
    public function getUnitUsahaKerjasamasRelatedByMouIdJoinSumberDanaRelatedBySumberDanaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = UnitUsahaKerjasamaQuery::create(null, $criteria);
        $query->joinWith('SumberDanaRelatedBySumberDanaId', $join_behavior);

        return $this->getUnitUsahaKerjasamasRelatedByMouId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Mou is new, it will return
     * an empty collection; or if this Mou has previously
     * been saved, it will retrieve related UnitUsahaKerjasamasRelatedByMouId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Mou.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|UnitUsahaKerjasama[] List of UnitUsahaKerjasama objects
     */
    public function getUnitUsahaKerjasamasRelatedByMouIdJoinSumberDanaRelatedBySumberDanaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = UnitUsahaKerjasamaQuery::create(null, $criteria);
        $query->joinWith('SumberDanaRelatedBySumberDanaId', $join_behavior);

        return $this->getUnitUsahaKerjasamasRelatedByMouId($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->mou_id = null;
        $this->dudi_id = null;
        $this->sekolah_id = null;
        $this->nomor_mou = null;
        $this->judul_mou = null;
        $this->tanggal_mulai = null;
        $this->tanggal_selesai = null;
        $this->nama_dudi = null;
        $this->npwp_dudi = null;
        $this->nama_bidang_usaha = null;
        $this->telp_kantor = null;
        $this->fax = null;
        $this->contact_person = null;
        $this->telp_cp = null;
        $this->jabatan_cp = null;
        $this->last_update = null;
        $this->soft_delete = null;
        $this->last_sync = null;
        $this->updater_id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collVldMousRelatedByMouId) {
                foreach ($this->collVldMousRelatedByMouId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldMousRelatedByMouId) {
                foreach ($this->collVldMousRelatedByMouId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collJurusanKerjasamasRelatedByMouId) {
                foreach ($this->collJurusanKerjasamasRelatedByMouId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collJurusanKerjasamasRelatedByMouId) {
                foreach ($this->collJurusanKerjasamasRelatedByMouId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUnitUsahaKerjasamasRelatedByMouId) {
                foreach ($this->collUnitUsahaKerjasamasRelatedByMouId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUnitUsahaKerjasamasRelatedByMouId) {
                foreach ($this->collUnitUsahaKerjasamasRelatedByMouId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aDudiRelatedByDudiId instanceof Persistent) {
              $this->aDudiRelatedByDudiId->clearAllReferences($deep);
            }
            if ($this->aDudiRelatedByDudiId instanceof Persistent) {
              $this->aDudiRelatedByDudiId->clearAllReferences($deep);
            }
            if ($this->aSekolahRelatedBySekolahId instanceof Persistent) {
              $this->aSekolahRelatedBySekolahId->clearAllReferences($deep);
            }
            if ($this->aSekolahRelatedBySekolahId instanceof Persistent) {
              $this->aSekolahRelatedBySekolahId->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collVldMousRelatedByMouId instanceof PropelCollection) {
            $this->collVldMousRelatedByMouId->clearIterator();
        }
        $this->collVldMousRelatedByMouId = null;
        if ($this->collVldMousRelatedByMouId instanceof PropelCollection) {
            $this->collVldMousRelatedByMouId->clearIterator();
        }
        $this->collVldMousRelatedByMouId = null;
        if ($this->collJurusanKerjasamasRelatedByMouId instanceof PropelCollection) {
            $this->collJurusanKerjasamasRelatedByMouId->clearIterator();
        }
        $this->collJurusanKerjasamasRelatedByMouId = null;
        if ($this->collJurusanKerjasamasRelatedByMouId instanceof PropelCollection) {
            $this->collJurusanKerjasamasRelatedByMouId->clearIterator();
        }
        $this->collJurusanKerjasamasRelatedByMouId = null;
        if ($this->collUnitUsahaKerjasamasRelatedByMouId instanceof PropelCollection) {
            $this->collUnitUsahaKerjasamasRelatedByMouId->clearIterator();
        }
        $this->collUnitUsahaKerjasamasRelatedByMouId = null;
        if ($this->collUnitUsahaKerjasamasRelatedByMouId instanceof PropelCollection) {
            $this->collUnitUsahaKerjasamasRelatedByMouId->clearIterator();
        }
        $this->collUnitUsahaKerjasamasRelatedByMouId = null;
        $this->aDudiRelatedByDudiId = null;
        $this->aDudiRelatedByDudiId = null;
        $this->aSekolahRelatedBySekolahId = null;
        $this->aSekolahRelatedBySekolahId = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(MouPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
