<?php

namespace angulex\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\JenisSarana;
use angulex\Model\Prasarana;
use angulex\Model\Sarana;
use angulex\Model\SaranaLongitudinal;
use angulex\Model\SaranaPeer;
use angulex\Model\SaranaQuery;
use angulex\Model\Sekolah;
use angulex\Model\StatusKepemilikanSarpras;
use angulex\Model\VldSarana;

/**
 * Base class that represents a query for the 'sarana' table.
 *
 * 
 *
 * @method SaranaQuery orderBySaranaId($order = Criteria::ASC) Order by the sarana_id column
 * @method SaranaQuery orderBySekolahId($order = Criteria::ASC) Order by the sekolah_id column
 * @method SaranaQuery orderByJenisSaranaId($order = Criteria::ASC) Order by the jenis_sarana_id column
 * @method SaranaQuery orderByPrasaranaId($order = Criteria::ASC) Order by the prasarana_id column
 * @method SaranaQuery orderByKepemilikanSarprasId($order = Criteria::ASC) Order by the kepemilikan_sarpras_id column
 * @method SaranaQuery orderByNama($order = Criteria::ASC) Order by the nama column
 * @method SaranaQuery orderByLastUpdate($order = Criteria::ASC) Order by the Last_update column
 * @method SaranaQuery orderBySoftDelete($order = Criteria::ASC) Order by the Soft_delete column
 * @method SaranaQuery orderByLastSync($order = Criteria::ASC) Order by the last_sync column
 * @method SaranaQuery orderByUpdaterId($order = Criteria::ASC) Order by the Updater_ID column
 *
 * @method SaranaQuery groupBySaranaId() Group by the sarana_id column
 * @method SaranaQuery groupBySekolahId() Group by the sekolah_id column
 * @method SaranaQuery groupByJenisSaranaId() Group by the jenis_sarana_id column
 * @method SaranaQuery groupByPrasaranaId() Group by the prasarana_id column
 * @method SaranaQuery groupByKepemilikanSarprasId() Group by the kepemilikan_sarpras_id column
 * @method SaranaQuery groupByNama() Group by the nama column
 * @method SaranaQuery groupByLastUpdate() Group by the Last_update column
 * @method SaranaQuery groupBySoftDelete() Group by the Soft_delete column
 * @method SaranaQuery groupByLastSync() Group by the last_sync column
 * @method SaranaQuery groupByUpdaterId() Group by the Updater_ID column
 *
 * @method SaranaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SaranaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SaranaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method SaranaQuery leftJoinPrasaranaRelatedByPrasaranaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PrasaranaRelatedByPrasaranaId relation
 * @method SaranaQuery rightJoinPrasaranaRelatedByPrasaranaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PrasaranaRelatedByPrasaranaId relation
 * @method SaranaQuery innerJoinPrasaranaRelatedByPrasaranaId($relationAlias = null) Adds a INNER JOIN clause to the query using the PrasaranaRelatedByPrasaranaId relation
 *
 * @method SaranaQuery leftJoinPrasaranaRelatedByPrasaranaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PrasaranaRelatedByPrasaranaId relation
 * @method SaranaQuery rightJoinPrasaranaRelatedByPrasaranaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PrasaranaRelatedByPrasaranaId relation
 * @method SaranaQuery innerJoinPrasaranaRelatedByPrasaranaId($relationAlias = null) Adds a INNER JOIN clause to the query using the PrasaranaRelatedByPrasaranaId relation
 *
 * @method SaranaQuery leftJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method SaranaQuery rightJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method SaranaQuery innerJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the SekolahRelatedBySekolahId relation
 *
 * @method SaranaQuery leftJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method SaranaQuery rightJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method SaranaQuery innerJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the SekolahRelatedBySekolahId relation
 *
 * @method SaranaQuery leftJoinJenisSaranaRelatedByJenisSaranaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the JenisSaranaRelatedByJenisSaranaId relation
 * @method SaranaQuery rightJoinJenisSaranaRelatedByJenisSaranaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JenisSaranaRelatedByJenisSaranaId relation
 * @method SaranaQuery innerJoinJenisSaranaRelatedByJenisSaranaId($relationAlias = null) Adds a INNER JOIN clause to the query using the JenisSaranaRelatedByJenisSaranaId relation
 *
 * @method SaranaQuery leftJoinJenisSaranaRelatedByJenisSaranaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the JenisSaranaRelatedByJenisSaranaId relation
 * @method SaranaQuery rightJoinJenisSaranaRelatedByJenisSaranaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JenisSaranaRelatedByJenisSaranaId relation
 * @method SaranaQuery innerJoinJenisSaranaRelatedByJenisSaranaId($relationAlias = null) Adds a INNER JOIN clause to the query using the JenisSaranaRelatedByJenisSaranaId relation
 *
 * @method SaranaQuery leftJoinStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($relationAlias = null) Adds a LEFT JOIN clause to the query using the StatusKepemilikanSarprasRelatedByKepemilikanSarprasId relation
 * @method SaranaQuery rightJoinStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the StatusKepemilikanSarprasRelatedByKepemilikanSarprasId relation
 * @method SaranaQuery innerJoinStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($relationAlias = null) Adds a INNER JOIN clause to the query using the StatusKepemilikanSarprasRelatedByKepemilikanSarprasId relation
 *
 * @method SaranaQuery leftJoinStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($relationAlias = null) Adds a LEFT JOIN clause to the query using the StatusKepemilikanSarprasRelatedByKepemilikanSarprasId relation
 * @method SaranaQuery rightJoinStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the StatusKepemilikanSarprasRelatedByKepemilikanSarprasId relation
 * @method SaranaQuery innerJoinStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($relationAlias = null) Adds a INNER JOIN clause to the query using the StatusKepemilikanSarprasRelatedByKepemilikanSarprasId relation
 *
 * @method SaranaQuery leftJoinSaranaLongitudinalRelatedBySaranaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SaranaLongitudinalRelatedBySaranaId relation
 * @method SaranaQuery rightJoinSaranaLongitudinalRelatedBySaranaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SaranaLongitudinalRelatedBySaranaId relation
 * @method SaranaQuery innerJoinSaranaLongitudinalRelatedBySaranaId($relationAlias = null) Adds a INNER JOIN clause to the query using the SaranaLongitudinalRelatedBySaranaId relation
 *
 * @method SaranaQuery leftJoinSaranaLongitudinalRelatedBySaranaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SaranaLongitudinalRelatedBySaranaId relation
 * @method SaranaQuery rightJoinSaranaLongitudinalRelatedBySaranaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SaranaLongitudinalRelatedBySaranaId relation
 * @method SaranaQuery innerJoinSaranaLongitudinalRelatedBySaranaId($relationAlias = null) Adds a INNER JOIN clause to the query using the SaranaLongitudinalRelatedBySaranaId relation
 *
 * @method SaranaQuery leftJoinVldSaranaRelatedBySaranaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldSaranaRelatedBySaranaId relation
 * @method SaranaQuery rightJoinVldSaranaRelatedBySaranaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldSaranaRelatedBySaranaId relation
 * @method SaranaQuery innerJoinVldSaranaRelatedBySaranaId($relationAlias = null) Adds a INNER JOIN clause to the query using the VldSaranaRelatedBySaranaId relation
 *
 * @method SaranaQuery leftJoinVldSaranaRelatedBySaranaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldSaranaRelatedBySaranaId relation
 * @method SaranaQuery rightJoinVldSaranaRelatedBySaranaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldSaranaRelatedBySaranaId relation
 * @method SaranaQuery innerJoinVldSaranaRelatedBySaranaId($relationAlias = null) Adds a INNER JOIN clause to the query using the VldSaranaRelatedBySaranaId relation
 *
 * @method Sarana findOne(PropelPDO $con = null) Return the first Sarana matching the query
 * @method Sarana findOneOrCreate(PropelPDO $con = null) Return the first Sarana matching the query, or a new Sarana object populated from the query conditions when no match is found
 *
 * @method Sarana findOneBySekolahId(string $sekolah_id) Return the first Sarana filtered by the sekolah_id column
 * @method Sarana findOneByJenisSaranaId(int $jenis_sarana_id) Return the first Sarana filtered by the jenis_sarana_id column
 * @method Sarana findOneByPrasaranaId(string $prasarana_id) Return the first Sarana filtered by the prasarana_id column
 * @method Sarana findOneByKepemilikanSarprasId(string $kepemilikan_sarpras_id) Return the first Sarana filtered by the kepemilikan_sarpras_id column
 * @method Sarana findOneByNama(string $nama) Return the first Sarana filtered by the nama column
 * @method Sarana findOneByLastUpdate(string $Last_update) Return the first Sarana filtered by the Last_update column
 * @method Sarana findOneBySoftDelete(string $Soft_delete) Return the first Sarana filtered by the Soft_delete column
 * @method Sarana findOneByLastSync(string $last_sync) Return the first Sarana filtered by the last_sync column
 * @method Sarana findOneByUpdaterId(string $Updater_ID) Return the first Sarana filtered by the Updater_ID column
 *
 * @method array findBySaranaId(string $sarana_id) Return Sarana objects filtered by the sarana_id column
 * @method array findBySekolahId(string $sekolah_id) Return Sarana objects filtered by the sekolah_id column
 * @method array findByJenisSaranaId(int $jenis_sarana_id) Return Sarana objects filtered by the jenis_sarana_id column
 * @method array findByPrasaranaId(string $prasarana_id) Return Sarana objects filtered by the prasarana_id column
 * @method array findByKepemilikanSarprasId(string $kepemilikan_sarpras_id) Return Sarana objects filtered by the kepemilikan_sarpras_id column
 * @method array findByNama(string $nama) Return Sarana objects filtered by the nama column
 * @method array findByLastUpdate(string $Last_update) Return Sarana objects filtered by the Last_update column
 * @method array findBySoftDelete(string $Soft_delete) Return Sarana objects filtered by the Soft_delete column
 * @method array findByLastSync(string $last_sync) Return Sarana objects filtered by the last_sync column
 * @method array findByUpdaterId(string $Updater_ID) Return Sarana objects filtered by the Updater_ID column
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseSaranaQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSaranaQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Dapodikmen', $modelName = 'angulex\\Model\\Sarana', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SaranaQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   SaranaQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SaranaQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SaranaQuery) {
            return $criteria;
        }
        $query = new SaranaQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Sarana|Sarana[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SaranaPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SaranaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Sarana A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneBySaranaId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Sarana A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [sarana_id], [sekolah_id], [jenis_sarana_id], [prasarana_id], [kepemilikan_sarpras_id], [nama], [Last_update], [Soft_delete], [last_sync], [Updater_ID] FROM [sarana] WHERE [sarana_id] = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Sarana();
            $obj->hydrate($row);
            SaranaPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Sarana|Sarana[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Sarana[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SaranaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SaranaPeer::SARANA_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SaranaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SaranaPeer::SARANA_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the sarana_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySaranaId('fooValue');   // WHERE sarana_id = 'fooValue'
     * $query->filterBySaranaId('%fooValue%'); // WHERE sarana_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $saranaId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SaranaQuery The current query, for fluid interface
     */
    public function filterBySaranaId($saranaId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($saranaId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $saranaId)) {
                $saranaId = str_replace('*', '%', $saranaId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SaranaPeer::SARANA_ID, $saranaId, $comparison);
    }

    /**
     * Filter the query on the sekolah_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySekolahId('fooValue');   // WHERE sekolah_id = 'fooValue'
     * $query->filterBySekolahId('%fooValue%'); // WHERE sekolah_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sekolahId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SaranaQuery The current query, for fluid interface
     */
    public function filterBySekolahId($sekolahId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sekolahId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $sekolahId)) {
                $sekolahId = str_replace('*', '%', $sekolahId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SaranaPeer::SEKOLAH_ID, $sekolahId, $comparison);
    }

    /**
     * Filter the query on the jenis_sarana_id column
     *
     * Example usage:
     * <code>
     * $query->filterByJenisSaranaId(1234); // WHERE jenis_sarana_id = 1234
     * $query->filterByJenisSaranaId(array(12, 34)); // WHERE jenis_sarana_id IN (12, 34)
     * $query->filterByJenisSaranaId(array('min' => 12)); // WHERE jenis_sarana_id >= 12
     * $query->filterByJenisSaranaId(array('max' => 12)); // WHERE jenis_sarana_id <= 12
     * </code>
     *
     * @see       filterByJenisSaranaRelatedByJenisSaranaId()
     *
     * @see       filterByJenisSaranaRelatedByJenisSaranaId()
     *
     * @param     mixed $jenisSaranaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SaranaQuery The current query, for fluid interface
     */
    public function filterByJenisSaranaId($jenisSaranaId = null, $comparison = null)
    {
        if (is_array($jenisSaranaId)) {
            $useMinMax = false;
            if (isset($jenisSaranaId['min'])) {
                $this->addUsingAlias(SaranaPeer::JENIS_SARANA_ID, $jenisSaranaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jenisSaranaId['max'])) {
                $this->addUsingAlias(SaranaPeer::JENIS_SARANA_ID, $jenisSaranaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaranaPeer::JENIS_SARANA_ID, $jenisSaranaId, $comparison);
    }

    /**
     * Filter the query on the prasarana_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPrasaranaId('fooValue');   // WHERE prasarana_id = 'fooValue'
     * $query->filterByPrasaranaId('%fooValue%'); // WHERE prasarana_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $prasaranaId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SaranaQuery The current query, for fluid interface
     */
    public function filterByPrasaranaId($prasaranaId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($prasaranaId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $prasaranaId)) {
                $prasaranaId = str_replace('*', '%', $prasaranaId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SaranaPeer::PRASARANA_ID, $prasaranaId, $comparison);
    }

    /**
     * Filter the query on the kepemilikan_sarpras_id column
     *
     * Example usage:
     * <code>
     * $query->filterByKepemilikanSarprasId(1234); // WHERE kepemilikan_sarpras_id = 1234
     * $query->filterByKepemilikanSarprasId(array(12, 34)); // WHERE kepemilikan_sarpras_id IN (12, 34)
     * $query->filterByKepemilikanSarprasId(array('min' => 12)); // WHERE kepemilikan_sarpras_id >= 12
     * $query->filterByKepemilikanSarprasId(array('max' => 12)); // WHERE kepemilikan_sarpras_id <= 12
     * </code>
     *
     * @see       filterByStatusKepemilikanSarprasRelatedByKepemilikanSarprasId()
     *
     * @see       filterByStatusKepemilikanSarprasRelatedByKepemilikanSarprasId()
     *
     * @param     mixed $kepemilikanSarprasId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SaranaQuery The current query, for fluid interface
     */
    public function filterByKepemilikanSarprasId($kepemilikanSarprasId = null, $comparison = null)
    {
        if (is_array($kepemilikanSarprasId)) {
            $useMinMax = false;
            if (isset($kepemilikanSarprasId['min'])) {
                $this->addUsingAlias(SaranaPeer::KEPEMILIKAN_SARPRAS_ID, $kepemilikanSarprasId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($kepemilikanSarprasId['max'])) {
                $this->addUsingAlias(SaranaPeer::KEPEMILIKAN_SARPRAS_ID, $kepemilikanSarprasId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaranaPeer::KEPEMILIKAN_SARPRAS_ID, $kepemilikanSarprasId, $comparison);
    }

    /**
     * Filter the query on the nama column
     *
     * Example usage:
     * <code>
     * $query->filterByNama('fooValue');   // WHERE nama = 'fooValue'
     * $query->filterByNama('%fooValue%'); // WHERE nama LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nama The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SaranaQuery The current query, for fluid interface
     */
    public function filterByNama($nama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nama)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nama)) {
                $nama = str_replace('*', '%', $nama);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SaranaPeer::NAMA, $nama, $comparison);
    }

    /**
     * Filter the query on the Last_update column
     *
     * Example usage:
     * <code>
     * $query->filterByLastUpdate('2011-03-14'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate('now'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate(array('max' => 'yesterday')); // WHERE Last_update > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastUpdate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SaranaQuery The current query, for fluid interface
     */
    public function filterByLastUpdate($lastUpdate = null, $comparison = null)
    {
        if (is_array($lastUpdate)) {
            $useMinMax = false;
            if (isset($lastUpdate['min'])) {
                $this->addUsingAlias(SaranaPeer::LAST_UPDATE, $lastUpdate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastUpdate['max'])) {
                $this->addUsingAlias(SaranaPeer::LAST_UPDATE, $lastUpdate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaranaPeer::LAST_UPDATE, $lastUpdate, $comparison);
    }

    /**
     * Filter the query on the Soft_delete column
     *
     * Example usage:
     * <code>
     * $query->filterBySoftDelete(1234); // WHERE Soft_delete = 1234
     * $query->filterBySoftDelete(array(12, 34)); // WHERE Soft_delete IN (12, 34)
     * $query->filterBySoftDelete(array('min' => 12)); // WHERE Soft_delete >= 12
     * $query->filterBySoftDelete(array('max' => 12)); // WHERE Soft_delete <= 12
     * </code>
     *
     * @param     mixed $softDelete The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SaranaQuery The current query, for fluid interface
     */
    public function filterBySoftDelete($softDelete = null, $comparison = null)
    {
        if (is_array($softDelete)) {
            $useMinMax = false;
            if (isset($softDelete['min'])) {
                $this->addUsingAlias(SaranaPeer::SOFT_DELETE, $softDelete['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($softDelete['max'])) {
                $this->addUsingAlias(SaranaPeer::SOFT_DELETE, $softDelete['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaranaPeer::SOFT_DELETE, $softDelete, $comparison);
    }

    /**
     * Filter the query on the last_sync column
     *
     * Example usage:
     * <code>
     * $query->filterByLastSync('2011-03-14'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync('now'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync(array('max' => 'yesterday')); // WHERE last_sync > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastSync The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SaranaQuery The current query, for fluid interface
     */
    public function filterByLastSync($lastSync = null, $comparison = null)
    {
        if (is_array($lastSync)) {
            $useMinMax = false;
            if (isset($lastSync['min'])) {
                $this->addUsingAlias(SaranaPeer::LAST_SYNC, $lastSync['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastSync['max'])) {
                $this->addUsingAlias(SaranaPeer::LAST_SYNC, $lastSync['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaranaPeer::LAST_SYNC, $lastSync, $comparison);
    }

    /**
     * Filter the query on the Updater_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdaterId('fooValue');   // WHERE Updater_ID = 'fooValue'
     * $query->filterByUpdaterId('%fooValue%'); // WHERE Updater_ID LIKE '%fooValue%'
     * </code>
     *
     * @param     string $updaterId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SaranaQuery The current query, for fluid interface
     */
    public function filterByUpdaterId($updaterId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($updaterId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $updaterId)) {
                $updaterId = str_replace('*', '%', $updaterId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SaranaPeer::UPDATER_ID, $updaterId, $comparison);
    }

    /**
     * Filter the query by a related Prasarana object
     *
     * @param   Prasarana|PropelObjectCollection $prasarana The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPrasaranaRelatedByPrasaranaId($prasarana, $comparison = null)
    {
        if ($prasarana instanceof Prasarana) {
            return $this
                ->addUsingAlias(SaranaPeer::PRASARANA_ID, $prasarana->getPrasaranaId(), $comparison);
        } elseif ($prasarana instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SaranaPeer::PRASARANA_ID, $prasarana->toKeyValue('PrimaryKey', 'PrasaranaId'), $comparison);
        } else {
            throw new PropelException('filterByPrasaranaRelatedByPrasaranaId() only accepts arguments of type Prasarana or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PrasaranaRelatedByPrasaranaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SaranaQuery The current query, for fluid interface
     */
    public function joinPrasaranaRelatedByPrasaranaId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PrasaranaRelatedByPrasaranaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PrasaranaRelatedByPrasaranaId');
        }

        return $this;
    }

    /**
     * Use the PrasaranaRelatedByPrasaranaId relation Prasarana object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PrasaranaQuery A secondary query class using the current class as primary query
     */
    public function usePrasaranaRelatedByPrasaranaIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPrasaranaRelatedByPrasaranaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PrasaranaRelatedByPrasaranaId', '\angulex\Model\PrasaranaQuery');
    }

    /**
     * Filter the query by a related Prasarana object
     *
     * @param   Prasarana|PropelObjectCollection $prasarana The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPrasaranaRelatedByPrasaranaId($prasarana, $comparison = null)
    {
        if ($prasarana instanceof Prasarana) {
            return $this
                ->addUsingAlias(SaranaPeer::PRASARANA_ID, $prasarana->getPrasaranaId(), $comparison);
        } elseif ($prasarana instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SaranaPeer::PRASARANA_ID, $prasarana->toKeyValue('PrimaryKey', 'PrasaranaId'), $comparison);
        } else {
            throw new PropelException('filterByPrasaranaRelatedByPrasaranaId() only accepts arguments of type Prasarana or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PrasaranaRelatedByPrasaranaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SaranaQuery The current query, for fluid interface
     */
    public function joinPrasaranaRelatedByPrasaranaId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PrasaranaRelatedByPrasaranaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PrasaranaRelatedByPrasaranaId');
        }

        return $this;
    }

    /**
     * Use the PrasaranaRelatedByPrasaranaId relation Prasarana object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PrasaranaQuery A secondary query class using the current class as primary query
     */
    public function usePrasaranaRelatedByPrasaranaIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPrasaranaRelatedByPrasaranaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PrasaranaRelatedByPrasaranaId', '\angulex\Model\PrasaranaQuery');
    }

    /**
     * Filter the query by a related Sekolah object
     *
     * @param   Sekolah|PropelObjectCollection $sekolah The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySekolahRelatedBySekolahId($sekolah, $comparison = null)
    {
        if ($sekolah instanceof Sekolah) {
            return $this
                ->addUsingAlias(SaranaPeer::SEKOLAH_ID, $sekolah->getSekolahId(), $comparison);
        } elseif ($sekolah instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SaranaPeer::SEKOLAH_ID, $sekolah->toKeyValue('PrimaryKey', 'SekolahId'), $comparison);
        } else {
            throw new PropelException('filterBySekolahRelatedBySekolahId() only accepts arguments of type Sekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SekolahRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SaranaQuery The current query, for fluid interface
     */
    public function joinSekolahRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SekolahRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SekolahRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the SekolahRelatedBySekolahId relation Sekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SekolahQuery A secondary query class using the current class as primary query
     */
    public function useSekolahRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSekolahRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SekolahRelatedBySekolahId', '\angulex\Model\SekolahQuery');
    }

    /**
     * Filter the query by a related Sekolah object
     *
     * @param   Sekolah|PropelObjectCollection $sekolah The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySekolahRelatedBySekolahId($sekolah, $comparison = null)
    {
        if ($sekolah instanceof Sekolah) {
            return $this
                ->addUsingAlias(SaranaPeer::SEKOLAH_ID, $sekolah->getSekolahId(), $comparison);
        } elseif ($sekolah instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SaranaPeer::SEKOLAH_ID, $sekolah->toKeyValue('PrimaryKey', 'SekolahId'), $comparison);
        } else {
            throw new PropelException('filterBySekolahRelatedBySekolahId() only accepts arguments of type Sekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SekolahRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SaranaQuery The current query, for fluid interface
     */
    public function joinSekolahRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SekolahRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SekolahRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the SekolahRelatedBySekolahId relation Sekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SekolahQuery A secondary query class using the current class as primary query
     */
    public function useSekolahRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSekolahRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SekolahRelatedBySekolahId', '\angulex\Model\SekolahQuery');
    }

    /**
     * Filter the query by a related JenisSarana object
     *
     * @param   JenisSarana|PropelObjectCollection $jenisSarana The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJenisSaranaRelatedByJenisSaranaId($jenisSarana, $comparison = null)
    {
        if ($jenisSarana instanceof JenisSarana) {
            return $this
                ->addUsingAlias(SaranaPeer::JENIS_SARANA_ID, $jenisSarana->getJenisSaranaId(), $comparison);
        } elseif ($jenisSarana instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SaranaPeer::JENIS_SARANA_ID, $jenisSarana->toKeyValue('PrimaryKey', 'JenisSaranaId'), $comparison);
        } else {
            throw new PropelException('filterByJenisSaranaRelatedByJenisSaranaId() only accepts arguments of type JenisSarana or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the JenisSaranaRelatedByJenisSaranaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SaranaQuery The current query, for fluid interface
     */
    public function joinJenisSaranaRelatedByJenisSaranaId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('JenisSaranaRelatedByJenisSaranaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'JenisSaranaRelatedByJenisSaranaId');
        }

        return $this;
    }

    /**
     * Use the JenisSaranaRelatedByJenisSaranaId relation JenisSarana object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\JenisSaranaQuery A secondary query class using the current class as primary query
     */
    public function useJenisSaranaRelatedByJenisSaranaIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinJenisSaranaRelatedByJenisSaranaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'JenisSaranaRelatedByJenisSaranaId', '\angulex\Model\JenisSaranaQuery');
    }

    /**
     * Filter the query by a related JenisSarana object
     *
     * @param   JenisSarana|PropelObjectCollection $jenisSarana The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJenisSaranaRelatedByJenisSaranaId($jenisSarana, $comparison = null)
    {
        if ($jenisSarana instanceof JenisSarana) {
            return $this
                ->addUsingAlias(SaranaPeer::JENIS_SARANA_ID, $jenisSarana->getJenisSaranaId(), $comparison);
        } elseif ($jenisSarana instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SaranaPeer::JENIS_SARANA_ID, $jenisSarana->toKeyValue('PrimaryKey', 'JenisSaranaId'), $comparison);
        } else {
            throw new PropelException('filterByJenisSaranaRelatedByJenisSaranaId() only accepts arguments of type JenisSarana or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the JenisSaranaRelatedByJenisSaranaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SaranaQuery The current query, for fluid interface
     */
    public function joinJenisSaranaRelatedByJenisSaranaId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('JenisSaranaRelatedByJenisSaranaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'JenisSaranaRelatedByJenisSaranaId');
        }

        return $this;
    }

    /**
     * Use the JenisSaranaRelatedByJenisSaranaId relation JenisSarana object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\JenisSaranaQuery A secondary query class using the current class as primary query
     */
    public function useJenisSaranaRelatedByJenisSaranaIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinJenisSaranaRelatedByJenisSaranaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'JenisSaranaRelatedByJenisSaranaId', '\angulex\Model\JenisSaranaQuery');
    }

    /**
     * Filter the query by a related StatusKepemilikanSarpras object
     *
     * @param   StatusKepemilikanSarpras|PropelObjectCollection $statusKepemilikanSarpras The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($statusKepemilikanSarpras, $comparison = null)
    {
        if ($statusKepemilikanSarpras instanceof StatusKepemilikanSarpras) {
            return $this
                ->addUsingAlias(SaranaPeer::KEPEMILIKAN_SARPRAS_ID, $statusKepemilikanSarpras->getKepemilikanSarprasId(), $comparison);
        } elseif ($statusKepemilikanSarpras instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SaranaPeer::KEPEMILIKAN_SARPRAS_ID, $statusKepemilikanSarpras->toKeyValue('PrimaryKey', 'KepemilikanSarprasId'), $comparison);
        } else {
            throw new PropelException('filterByStatusKepemilikanSarprasRelatedByKepemilikanSarprasId() only accepts arguments of type StatusKepemilikanSarpras or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the StatusKepemilikanSarprasRelatedByKepemilikanSarprasId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SaranaQuery The current query, for fluid interface
     */
    public function joinStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('StatusKepemilikanSarprasRelatedByKepemilikanSarprasId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'StatusKepemilikanSarprasRelatedByKepemilikanSarprasId');
        }

        return $this;
    }

    /**
     * Use the StatusKepemilikanSarprasRelatedByKepemilikanSarprasId relation StatusKepemilikanSarpras object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\StatusKepemilikanSarprasQuery A secondary query class using the current class as primary query
     */
    public function useStatusKepemilikanSarprasRelatedByKepemilikanSarprasIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'StatusKepemilikanSarprasRelatedByKepemilikanSarprasId', '\angulex\Model\StatusKepemilikanSarprasQuery');
    }

    /**
     * Filter the query by a related StatusKepemilikanSarpras object
     *
     * @param   StatusKepemilikanSarpras|PropelObjectCollection $statusKepemilikanSarpras The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($statusKepemilikanSarpras, $comparison = null)
    {
        if ($statusKepemilikanSarpras instanceof StatusKepemilikanSarpras) {
            return $this
                ->addUsingAlias(SaranaPeer::KEPEMILIKAN_SARPRAS_ID, $statusKepemilikanSarpras->getKepemilikanSarprasId(), $comparison);
        } elseif ($statusKepemilikanSarpras instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SaranaPeer::KEPEMILIKAN_SARPRAS_ID, $statusKepemilikanSarpras->toKeyValue('PrimaryKey', 'KepemilikanSarprasId'), $comparison);
        } else {
            throw new PropelException('filterByStatusKepemilikanSarprasRelatedByKepemilikanSarprasId() only accepts arguments of type StatusKepemilikanSarpras or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the StatusKepemilikanSarprasRelatedByKepemilikanSarprasId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SaranaQuery The current query, for fluid interface
     */
    public function joinStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('StatusKepemilikanSarprasRelatedByKepemilikanSarprasId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'StatusKepemilikanSarprasRelatedByKepemilikanSarprasId');
        }

        return $this;
    }

    /**
     * Use the StatusKepemilikanSarprasRelatedByKepemilikanSarprasId relation StatusKepemilikanSarpras object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\StatusKepemilikanSarprasQuery A secondary query class using the current class as primary query
     */
    public function useStatusKepemilikanSarprasRelatedByKepemilikanSarprasIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinStatusKepemilikanSarprasRelatedByKepemilikanSarprasId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'StatusKepemilikanSarprasRelatedByKepemilikanSarprasId', '\angulex\Model\StatusKepemilikanSarprasQuery');
    }

    /**
     * Filter the query by a related SaranaLongitudinal object
     *
     * @param   SaranaLongitudinal|PropelObjectCollection $saranaLongitudinal  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySaranaLongitudinalRelatedBySaranaId($saranaLongitudinal, $comparison = null)
    {
        if ($saranaLongitudinal instanceof SaranaLongitudinal) {
            return $this
                ->addUsingAlias(SaranaPeer::SARANA_ID, $saranaLongitudinal->getSaranaId(), $comparison);
        } elseif ($saranaLongitudinal instanceof PropelObjectCollection) {
            return $this
                ->useSaranaLongitudinalRelatedBySaranaIdQuery()
                ->filterByPrimaryKeys($saranaLongitudinal->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySaranaLongitudinalRelatedBySaranaId() only accepts arguments of type SaranaLongitudinal or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SaranaLongitudinalRelatedBySaranaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SaranaQuery The current query, for fluid interface
     */
    public function joinSaranaLongitudinalRelatedBySaranaId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SaranaLongitudinalRelatedBySaranaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SaranaLongitudinalRelatedBySaranaId');
        }

        return $this;
    }

    /**
     * Use the SaranaLongitudinalRelatedBySaranaId relation SaranaLongitudinal object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SaranaLongitudinalQuery A secondary query class using the current class as primary query
     */
    public function useSaranaLongitudinalRelatedBySaranaIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSaranaLongitudinalRelatedBySaranaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SaranaLongitudinalRelatedBySaranaId', '\angulex\Model\SaranaLongitudinalQuery');
    }

    /**
     * Filter the query by a related SaranaLongitudinal object
     *
     * @param   SaranaLongitudinal|PropelObjectCollection $saranaLongitudinal  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySaranaLongitudinalRelatedBySaranaId($saranaLongitudinal, $comparison = null)
    {
        if ($saranaLongitudinal instanceof SaranaLongitudinal) {
            return $this
                ->addUsingAlias(SaranaPeer::SARANA_ID, $saranaLongitudinal->getSaranaId(), $comparison);
        } elseif ($saranaLongitudinal instanceof PropelObjectCollection) {
            return $this
                ->useSaranaLongitudinalRelatedBySaranaIdQuery()
                ->filterByPrimaryKeys($saranaLongitudinal->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySaranaLongitudinalRelatedBySaranaId() only accepts arguments of type SaranaLongitudinal or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SaranaLongitudinalRelatedBySaranaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SaranaQuery The current query, for fluid interface
     */
    public function joinSaranaLongitudinalRelatedBySaranaId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SaranaLongitudinalRelatedBySaranaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SaranaLongitudinalRelatedBySaranaId');
        }

        return $this;
    }

    /**
     * Use the SaranaLongitudinalRelatedBySaranaId relation SaranaLongitudinal object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SaranaLongitudinalQuery A secondary query class using the current class as primary query
     */
    public function useSaranaLongitudinalRelatedBySaranaIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSaranaLongitudinalRelatedBySaranaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SaranaLongitudinalRelatedBySaranaId', '\angulex\Model\SaranaLongitudinalQuery');
    }

    /**
     * Filter the query by a related VldSarana object
     *
     * @param   VldSarana|PropelObjectCollection $vldSarana  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldSaranaRelatedBySaranaId($vldSarana, $comparison = null)
    {
        if ($vldSarana instanceof VldSarana) {
            return $this
                ->addUsingAlias(SaranaPeer::SARANA_ID, $vldSarana->getSaranaId(), $comparison);
        } elseif ($vldSarana instanceof PropelObjectCollection) {
            return $this
                ->useVldSaranaRelatedBySaranaIdQuery()
                ->filterByPrimaryKeys($vldSarana->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldSaranaRelatedBySaranaId() only accepts arguments of type VldSarana or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldSaranaRelatedBySaranaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SaranaQuery The current query, for fluid interface
     */
    public function joinVldSaranaRelatedBySaranaId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldSaranaRelatedBySaranaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldSaranaRelatedBySaranaId');
        }

        return $this;
    }

    /**
     * Use the VldSaranaRelatedBySaranaId relation VldSarana object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldSaranaQuery A secondary query class using the current class as primary query
     */
    public function useVldSaranaRelatedBySaranaIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldSaranaRelatedBySaranaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldSaranaRelatedBySaranaId', '\angulex\Model\VldSaranaQuery');
    }

    /**
     * Filter the query by a related VldSarana object
     *
     * @param   VldSarana|PropelObjectCollection $vldSarana  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldSaranaRelatedBySaranaId($vldSarana, $comparison = null)
    {
        if ($vldSarana instanceof VldSarana) {
            return $this
                ->addUsingAlias(SaranaPeer::SARANA_ID, $vldSarana->getSaranaId(), $comparison);
        } elseif ($vldSarana instanceof PropelObjectCollection) {
            return $this
                ->useVldSaranaRelatedBySaranaIdQuery()
                ->filterByPrimaryKeys($vldSarana->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldSaranaRelatedBySaranaId() only accepts arguments of type VldSarana or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldSaranaRelatedBySaranaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SaranaQuery The current query, for fluid interface
     */
    public function joinVldSaranaRelatedBySaranaId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldSaranaRelatedBySaranaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldSaranaRelatedBySaranaId');
        }

        return $this;
    }

    /**
     * Use the VldSaranaRelatedBySaranaId relation VldSarana object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldSaranaQuery A secondary query class using the current class as primary query
     */
    public function useVldSaranaRelatedBySaranaIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldSaranaRelatedBySaranaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldSaranaRelatedBySaranaId', '\angulex\Model\VldSaranaQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Sarana $sarana Object to remove from the list of results
     *
     * @return SaranaQuery The current query, for fluid interface
     */
    public function prune($sarana = null)
    {
        if ($sarana) {
            $this->addUsingAlias(SaranaPeer::SARANA_ID, $sarana->getSaranaId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
