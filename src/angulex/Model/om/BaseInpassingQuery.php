<?php

namespace angulex\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\Inpassing;
use angulex\Model\InpassingPeer;
use angulex\Model\InpassingQuery;
use angulex\Model\PangkatGolongan;
use angulex\Model\Ptk;
use angulex\Model\VldInpassing;

/**
 * Base class that represents a query for the 'inpassing' table.
 *
 * 
 *
 * @method InpassingQuery orderByInpassingId($order = Criteria::ASC) Order by the inpassing_id column
 * @method InpassingQuery orderByPangkatGolonganId($order = Criteria::ASC) Order by the pangkat_golongan_id column
 * @method InpassingQuery orderByPtkId($order = Criteria::ASC) Order by the ptk_id column
 * @method InpassingQuery orderByNoSkInpassing($order = Criteria::ASC) Order by the no_sk_inpassing column
 * @method InpassingQuery orderByTmtInpassing($order = Criteria::ASC) Order by the tmt_inpassing column
 * @method InpassingQuery orderByAngkaKredit($order = Criteria::ASC) Order by the angka_kredit column
 * @method InpassingQuery orderByMasaKerjaTahun($order = Criteria::ASC) Order by the masa_kerja_tahun column
 * @method InpassingQuery orderByMasaKerjaBulan($order = Criteria::ASC) Order by the masa_kerja_bulan column
 * @method InpassingQuery orderByLastUpdate($order = Criteria::ASC) Order by the Last_update column
 * @method InpassingQuery orderBySoftDelete($order = Criteria::ASC) Order by the Soft_delete column
 * @method InpassingQuery orderByLastSync($order = Criteria::ASC) Order by the last_sync column
 * @method InpassingQuery orderByUpdaterId($order = Criteria::ASC) Order by the Updater_ID column
 *
 * @method InpassingQuery groupByInpassingId() Group by the inpassing_id column
 * @method InpassingQuery groupByPangkatGolonganId() Group by the pangkat_golongan_id column
 * @method InpassingQuery groupByPtkId() Group by the ptk_id column
 * @method InpassingQuery groupByNoSkInpassing() Group by the no_sk_inpassing column
 * @method InpassingQuery groupByTmtInpassing() Group by the tmt_inpassing column
 * @method InpassingQuery groupByAngkaKredit() Group by the angka_kredit column
 * @method InpassingQuery groupByMasaKerjaTahun() Group by the masa_kerja_tahun column
 * @method InpassingQuery groupByMasaKerjaBulan() Group by the masa_kerja_bulan column
 * @method InpassingQuery groupByLastUpdate() Group by the Last_update column
 * @method InpassingQuery groupBySoftDelete() Group by the Soft_delete column
 * @method InpassingQuery groupByLastSync() Group by the last_sync column
 * @method InpassingQuery groupByUpdaterId() Group by the Updater_ID column
 *
 * @method InpassingQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method InpassingQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method InpassingQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method InpassingQuery leftJoinPtkRelatedByPtkId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PtkRelatedByPtkId relation
 * @method InpassingQuery rightJoinPtkRelatedByPtkId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PtkRelatedByPtkId relation
 * @method InpassingQuery innerJoinPtkRelatedByPtkId($relationAlias = null) Adds a INNER JOIN clause to the query using the PtkRelatedByPtkId relation
 *
 * @method InpassingQuery leftJoinPtkRelatedByPtkId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PtkRelatedByPtkId relation
 * @method InpassingQuery rightJoinPtkRelatedByPtkId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PtkRelatedByPtkId relation
 * @method InpassingQuery innerJoinPtkRelatedByPtkId($relationAlias = null) Adds a INNER JOIN clause to the query using the PtkRelatedByPtkId relation
 *
 * @method InpassingQuery leftJoinPangkatGolonganRelatedByPangkatGolonganId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PangkatGolonganRelatedByPangkatGolonganId relation
 * @method InpassingQuery rightJoinPangkatGolonganRelatedByPangkatGolonganId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PangkatGolonganRelatedByPangkatGolonganId relation
 * @method InpassingQuery innerJoinPangkatGolonganRelatedByPangkatGolonganId($relationAlias = null) Adds a INNER JOIN clause to the query using the PangkatGolonganRelatedByPangkatGolonganId relation
 *
 * @method InpassingQuery leftJoinPangkatGolonganRelatedByPangkatGolonganId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PangkatGolonganRelatedByPangkatGolonganId relation
 * @method InpassingQuery rightJoinPangkatGolonganRelatedByPangkatGolonganId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PangkatGolonganRelatedByPangkatGolonganId relation
 * @method InpassingQuery innerJoinPangkatGolonganRelatedByPangkatGolonganId($relationAlias = null) Adds a INNER JOIN clause to the query using the PangkatGolonganRelatedByPangkatGolonganId relation
 *
 * @method InpassingQuery leftJoinVldInpassingRelatedByInpassingId($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldInpassingRelatedByInpassingId relation
 * @method InpassingQuery rightJoinVldInpassingRelatedByInpassingId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldInpassingRelatedByInpassingId relation
 * @method InpassingQuery innerJoinVldInpassingRelatedByInpassingId($relationAlias = null) Adds a INNER JOIN clause to the query using the VldInpassingRelatedByInpassingId relation
 *
 * @method InpassingQuery leftJoinVldInpassingRelatedByInpassingId($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldInpassingRelatedByInpassingId relation
 * @method InpassingQuery rightJoinVldInpassingRelatedByInpassingId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldInpassingRelatedByInpassingId relation
 * @method InpassingQuery innerJoinVldInpassingRelatedByInpassingId($relationAlias = null) Adds a INNER JOIN clause to the query using the VldInpassingRelatedByInpassingId relation
 *
 * @method Inpassing findOne(PropelPDO $con = null) Return the first Inpassing matching the query
 * @method Inpassing findOneOrCreate(PropelPDO $con = null) Return the first Inpassing matching the query, or a new Inpassing object populated from the query conditions when no match is found
 *
 * @method Inpassing findOneByPangkatGolonganId(string $pangkat_golongan_id) Return the first Inpassing filtered by the pangkat_golongan_id column
 * @method Inpassing findOneByPtkId(string $ptk_id) Return the first Inpassing filtered by the ptk_id column
 * @method Inpassing findOneByNoSkInpassing(string $no_sk_inpassing) Return the first Inpassing filtered by the no_sk_inpassing column
 * @method Inpassing findOneByTmtInpassing(string $tmt_inpassing) Return the first Inpassing filtered by the tmt_inpassing column
 * @method Inpassing findOneByAngkaKredit(string $angka_kredit) Return the first Inpassing filtered by the angka_kredit column
 * @method Inpassing findOneByMasaKerjaTahun(string $masa_kerja_tahun) Return the first Inpassing filtered by the masa_kerja_tahun column
 * @method Inpassing findOneByMasaKerjaBulan(string $masa_kerja_bulan) Return the first Inpassing filtered by the masa_kerja_bulan column
 * @method Inpassing findOneByLastUpdate(string $Last_update) Return the first Inpassing filtered by the Last_update column
 * @method Inpassing findOneBySoftDelete(string $Soft_delete) Return the first Inpassing filtered by the Soft_delete column
 * @method Inpassing findOneByLastSync(string $last_sync) Return the first Inpassing filtered by the last_sync column
 * @method Inpassing findOneByUpdaterId(string $Updater_ID) Return the first Inpassing filtered by the Updater_ID column
 *
 * @method array findByInpassingId(string $inpassing_id) Return Inpassing objects filtered by the inpassing_id column
 * @method array findByPangkatGolonganId(string $pangkat_golongan_id) Return Inpassing objects filtered by the pangkat_golongan_id column
 * @method array findByPtkId(string $ptk_id) Return Inpassing objects filtered by the ptk_id column
 * @method array findByNoSkInpassing(string $no_sk_inpassing) Return Inpassing objects filtered by the no_sk_inpassing column
 * @method array findByTmtInpassing(string $tmt_inpassing) Return Inpassing objects filtered by the tmt_inpassing column
 * @method array findByAngkaKredit(string $angka_kredit) Return Inpassing objects filtered by the angka_kredit column
 * @method array findByMasaKerjaTahun(string $masa_kerja_tahun) Return Inpassing objects filtered by the masa_kerja_tahun column
 * @method array findByMasaKerjaBulan(string $masa_kerja_bulan) Return Inpassing objects filtered by the masa_kerja_bulan column
 * @method array findByLastUpdate(string $Last_update) Return Inpassing objects filtered by the Last_update column
 * @method array findBySoftDelete(string $Soft_delete) Return Inpassing objects filtered by the Soft_delete column
 * @method array findByLastSync(string $last_sync) Return Inpassing objects filtered by the last_sync column
 * @method array findByUpdaterId(string $Updater_ID) Return Inpassing objects filtered by the Updater_ID column
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseInpassingQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseInpassingQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Dapodikmen', $modelName = 'angulex\\Model\\Inpassing', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new InpassingQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   InpassingQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return InpassingQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof InpassingQuery) {
            return $criteria;
        }
        $query = new InpassingQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Inpassing|Inpassing[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = InpassingPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(InpassingPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Inpassing A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByInpassingId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Inpassing A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [inpassing_id], [pangkat_golongan_id], [ptk_id], [no_sk_inpassing], [tmt_inpassing], [angka_kredit], [masa_kerja_tahun], [masa_kerja_bulan], [Last_update], [Soft_delete], [last_sync], [Updater_ID] FROM [inpassing] WHERE [inpassing_id] = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Inpassing();
            $obj->hydrate($row);
            InpassingPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Inpassing|Inpassing[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Inpassing[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return InpassingQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(InpassingPeer::INPASSING_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return InpassingQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(InpassingPeer::INPASSING_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the inpassing_id column
     *
     * Example usage:
     * <code>
     * $query->filterByInpassingId('fooValue');   // WHERE inpassing_id = 'fooValue'
     * $query->filterByInpassingId('%fooValue%'); // WHERE inpassing_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $inpassingId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InpassingQuery The current query, for fluid interface
     */
    public function filterByInpassingId($inpassingId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($inpassingId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $inpassingId)) {
                $inpassingId = str_replace('*', '%', $inpassingId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(InpassingPeer::INPASSING_ID, $inpassingId, $comparison);
    }

    /**
     * Filter the query on the pangkat_golongan_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPangkatGolonganId(1234); // WHERE pangkat_golongan_id = 1234
     * $query->filterByPangkatGolonganId(array(12, 34)); // WHERE pangkat_golongan_id IN (12, 34)
     * $query->filterByPangkatGolonganId(array('min' => 12)); // WHERE pangkat_golongan_id >= 12
     * $query->filterByPangkatGolonganId(array('max' => 12)); // WHERE pangkat_golongan_id <= 12
     * </code>
     *
     * @see       filterByPangkatGolonganRelatedByPangkatGolonganId()
     *
     * @see       filterByPangkatGolonganRelatedByPangkatGolonganId()
     *
     * @param     mixed $pangkatGolonganId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InpassingQuery The current query, for fluid interface
     */
    public function filterByPangkatGolonganId($pangkatGolonganId = null, $comparison = null)
    {
        if (is_array($pangkatGolonganId)) {
            $useMinMax = false;
            if (isset($pangkatGolonganId['min'])) {
                $this->addUsingAlias(InpassingPeer::PANGKAT_GOLONGAN_ID, $pangkatGolonganId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pangkatGolonganId['max'])) {
                $this->addUsingAlias(InpassingPeer::PANGKAT_GOLONGAN_ID, $pangkatGolonganId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InpassingPeer::PANGKAT_GOLONGAN_ID, $pangkatGolonganId, $comparison);
    }

    /**
     * Filter the query on the ptk_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPtkId('fooValue');   // WHERE ptk_id = 'fooValue'
     * $query->filterByPtkId('%fooValue%'); // WHERE ptk_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ptkId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InpassingQuery The current query, for fluid interface
     */
    public function filterByPtkId($ptkId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ptkId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ptkId)) {
                $ptkId = str_replace('*', '%', $ptkId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(InpassingPeer::PTK_ID, $ptkId, $comparison);
    }

    /**
     * Filter the query on the no_sk_inpassing column
     *
     * Example usage:
     * <code>
     * $query->filterByNoSkInpassing('fooValue');   // WHERE no_sk_inpassing = 'fooValue'
     * $query->filterByNoSkInpassing('%fooValue%'); // WHERE no_sk_inpassing LIKE '%fooValue%'
     * </code>
     *
     * @param     string $noSkInpassing The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InpassingQuery The current query, for fluid interface
     */
    public function filterByNoSkInpassing($noSkInpassing = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($noSkInpassing)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $noSkInpassing)) {
                $noSkInpassing = str_replace('*', '%', $noSkInpassing);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(InpassingPeer::NO_SK_INPASSING, $noSkInpassing, $comparison);
    }

    /**
     * Filter the query on the tmt_inpassing column
     *
     * Example usage:
     * <code>
     * $query->filterByTmtInpassing('fooValue');   // WHERE tmt_inpassing = 'fooValue'
     * $query->filterByTmtInpassing('%fooValue%'); // WHERE tmt_inpassing LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tmtInpassing The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InpassingQuery The current query, for fluid interface
     */
    public function filterByTmtInpassing($tmtInpassing = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tmtInpassing)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tmtInpassing)) {
                $tmtInpassing = str_replace('*', '%', $tmtInpassing);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(InpassingPeer::TMT_INPASSING, $tmtInpassing, $comparison);
    }

    /**
     * Filter the query on the angka_kredit column
     *
     * Example usage:
     * <code>
     * $query->filterByAngkaKredit(1234); // WHERE angka_kredit = 1234
     * $query->filterByAngkaKredit(array(12, 34)); // WHERE angka_kredit IN (12, 34)
     * $query->filterByAngkaKredit(array('min' => 12)); // WHERE angka_kredit >= 12
     * $query->filterByAngkaKredit(array('max' => 12)); // WHERE angka_kredit <= 12
     * </code>
     *
     * @param     mixed $angkaKredit The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InpassingQuery The current query, for fluid interface
     */
    public function filterByAngkaKredit($angkaKredit = null, $comparison = null)
    {
        if (is_array($angkaKredit)) {
            $useMinMax = false;
            if (isset($angkaKredit['min'])) {
                $this->addUsingAlias(InpassingPeer::ANGKA_KREDIT, $angkaKredit['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($angkaKredit['max'])) {
                $this->addUsingAlias(InpassingPeer::ANGKA_KREDIT, $angkaKredit['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InpassingPeer::ANGKA_KREDIT, $angkaKredit, $comparison);
    }

    /**
     * Filter the query on the masa_kerja_tahun column
     *
     * Example usage:
     * <code>
     * $query->filterByMasaKerjaTahun(1234); // WHERE masa_kerja_tahun = 1234
     * $query->filterByMasaKerjaTahun(array(12, 34)); // WHERE masa_kerja_tahun IN (12, 34)
     * $query->filterByMasaKerjaTahun(array('min' => 12)); // WHERE masa_kerja_tahun >= 12
     * $query->filterByMasaKerjaTahun(array('max' => 12)); // WHERE masa_kerja_tahun <= 12
     * </code>
     *
     * @param     mixed $masaKerjaTahun The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InpassingQuery The current query, for fluid interface
     */
    public function filterByMasaKerjaTahun($masaKerjaTahun = null, $comparison = null)
    {
        if (is_array($masaKerjaTahun)) {
            $useMinMax = false;
            if (isset($masaKerjaTahun['min'])) {
                $this->addUsingAlias(InpassingPeer::MASA_KERJA_TAHUN, $masaKerjaTahun['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($masaKerjaTahun['max'])) {
                $this->addUsingAlias(InpassingPeer::MASA_KERJA_TAHUN, $masaKerjaTahun['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InpassingPeer::MASA_KERJA_TAHUN, $masaKerjaTahun, $comparison);
    }

    /**
     * Filter the query on the masa_kerja_bulan column
     *
     * Example usage:
     * <code>
     * $query->filterByMasaKerjaBulan(1234); // WHERE masa_kerja_bulan = 1234
     * $query->filterByMasaKerjaBulan(array(12, 34)); // WHERE masa_kerja_bulan IN (12, 34)
     * $query->filterByMasaKerjaBulan(array('min' => 12)); // WHERE masa_kerja_bulan >= 12
     * $query->filterByMasaKerjaBulan(array('max' => 12)); // WHERE masa_kerja_bulan <= 12
     * </code>
     *
     * @param     mixed $masaKerjaBulan The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InpassingQuery The current query, for fluid interface
     */
    public function filterByMasaKerjaBulan($masaKerjaBulan = null, $comparison = null)
    {
        if (is_array($masaKerjaBulan)) {
            $useMinMax = false;
            if (isset($masaKerjaBulan['min'])) {
                $this->addUsingAlias(InpassingPeer::MASA_KERJA_BULAN, $masaKerjaBulan['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($masaKerjaBulan['max'])) {
                $this->addUsingAlias(InpassingPeer::MASA_KERJA_BULAN, $masaKerjaBulan['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InpassingPeer::MASA_KERJA_BULAN, $masaKerjaBulan, $comparison);
    }

    /**
     * Filter the query on the Last_update column
     *
     * Example usage:
     * <code>
     * $query->filterByLastUpdate('2011-03-14'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate('now'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate(array('max' => 'yesterday')); // WHERE Last_update > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastUpdate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InpassingQuery The current query, for fluid interface
     */
    public function filterByLastUpdate($lastUpdate = null, $comparison = null)
    {
        if (is_array($lastUpdate)) {
            $useMinMax = false;
            if (isset($lastUpdate['min'])) {
                $this->addUsingAlias(InpassingPeer::LAST_UPDATE, $lastUpdate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastUpdate['max'])) {
                $this->addUsingAlias(InpassingPeer::LAST_UPDATE, $lastUpdate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InpassingPeer::LAST_UPDATE, $lastUpdate, $comparison);
    }

    /**
     * Filter the query on the Soft_delete column
     *
     * Example usage:
     * <code>
     * $query->filterBySoftDelete(1234); // WHERE Soft_delete = 1234
     * $query->filterBySoftDelete(array(12, 34)); // WHERE Soft_delete IN (12, 34)
     * $query->filterBySoftDelete(array('min' => 12)); // WHERE Soft_delete >= 12
     * $query->filterBySoftDelete(array('max' => 12)); // WHERE Soft_delete <= 12
     * </code>
     *
     * @param     mixed $softDelete The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InpassingQuery The current query, for fluid interface
     */
    public function filterBySoftDelete($softDelete = null, $comparison = null)
    {
        if (is_array($softDelete)) {
            $useMinMax = false;
            if (isset($softDelete['min'])) {
                $this->addUsingAlias(InpassingPeer::SOFT_DELETE, $softDelete['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($softDelete['max'])) {
                $this->addUsingAlias(InpassingPeer::SOFT_DELETE, $softDelete['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InpassingPeer::SOFT_DELETE, $softDelete, $comparison);
    }

    /**
     * Filter the query on the last_sync column
     *
     * Example usage:
     * <code>
     * $query->filterByLastSync('2011-03-14'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync('now'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync(array('max' => 'yesterday')); // WHERE last_sync > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastSync The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InpassingQuery The current query, for fluid interface
     */
    public function filterByLastSync($lastSync = null, $comparison = null)
    {
        if (is_array($lastSync)) {
            $useMinMax = false;
            if (isset($lastSync['min'])) {
                $this->addUsingAlias(InpassingPeer::LAST_SYNC, $lastSync['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastSync['max'])) {
                $this->addUsingAlias(InpassingPeer::LAST_SYNC, $lastSync['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InpassingPeer::LAST_SYNC, $lastSync, $comparison);
    }

    /**
     * Filter the query on the Updater_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdaterId('fooValue');   // WHERE Updater_ID = 'fooValue'
     * $query->filterByUpdaterId('%fooValue%'); // WHERE Updater_ID LIKE '%fooValue%'
     * </code>
     *
     * @param     string $updaterId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InpassingQuery The current query, for fluid interface
     */
    public function filterByUpdaterId($updaterId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($updaterId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $updaterId)) {
                $updaterId = str_replace('*', '%', $updaterId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(InpassingPeer::UPDATER_ID, $updaterId, $comparison);
    }

    /**
     * Filter the query by a related Ptk object
     *
     * @param   Ptk|PropelObjectCollection $ptk The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 InpassingQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPtkRelatedByPtkId($ptk, $comparison = null)
    {
        if ($ptk instanceof Ptk) {
            return $this
                ->addUsingAlias(InpassingPeer::PTK_ID, $ptk->getPtkId(), $comparison);
        } elseif ($ptk instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(InpassingPeer::PTK_ID, $ptk->toKeyValue('PrimaryKey', 'PtkId'), $comparison);
        } else {
            throw new PropelException('filterByPtkRelatedByPtkId() only accepts arguments of type Ptk or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PtkRelatedByPtkId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return InpassingQuery The current query, for fluid interface
     */
    public function joinPtkRelatedByPtkId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PtkRelatedByPtkId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PtkRelatedByPtkId');
        }

        return $this;
    }

    /**
     * Use the PtkRelatedByPtkId relation Ptk object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PtkQuery A secondary query class using the current class as primary query
     */
    public function usePtkRelatedByPtkIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPtkRelatedByPtkId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PtkRelatedByPtkId', '\angulex\Model\PtkQuery');
    }

    /**
     * Filter the query by a related Ptk object
     *
     * @param   Ptk|PropelObjectCollection $ptk The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 InpassingQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPtkRelatedByPtkId($ptk, $comparison = null)
    {
        if ($ptk instanceof Ptk) {
            return $this
                ->addUsingAlias(InpassingPeer::PTK_ID, $ptk->getPtkId(), $comparison);
        } elseif ($ptk instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(InpassingPeer::PTK_ID, $ptk->toKeyValue('PrimaryKey', 'PtkId'), $comparison);
        } else {
            throw new PropelException('filterByPtkRelatedByPtkId() only accepts arguments of type Ptk or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PtkRelatedByPtkId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return InpassingQuery The current query, for fluid interface
     */
    public function joinPtkRelatedByPtkId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PtkRelatedByPtkId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PtkRelatedByPtkId');
        }

        return $this;
    }

    /**
     * Use the PtkRelatedByPtkId relation Ptk object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PtkQuery A secondary query class using the current class as primary query
     */
    public function usePtkRelatedByPtkIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPtkRelatedByPtkId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PtkRelatedByPtkId', '\angulex\Model\PtkQuery');
    }

    /**
     * Filter the query by a related PangkatGolongan object
     *
     * @param   PangkatGolongan|PropelObjectCollection $pangkatGolongan The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 InpassingQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPangkatGolonganRelatedByPangkatGolonganId($pangkatGolongan, $comparison = null)
    {
        if ($pangkatGolongan instanceof PangkatGolongan) {
            return $this
                ->addUsingAlias(InpassingPeer::PANGKAT_GOLONGAN_ID, $pangkatGolongan->getPangkatGolonganId(), $comparison);
        } elseif ($pangkatGolongan instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(InpassingPeer::PANGKAT_GOLONGAN_ID, $pangkatGolongan->toKeyValue('PrimaryKey', 'PangkatGolonganId'), $comparison);
        } else {
            throw new PropelException('filterByPangkatGolonganRelatedByPangkatGolonganId() only accepts arguments of type PangkatGolongan or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PangkatGolonganRelatedByPangkatGolonganId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return InpassingQuery The current query, for fluid interface
     */
    public function joinPangkatGolonganRelatedByPangkatGolonganId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PangkatGolonganRelatedByPangkatGolonganId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PangkatGolonganRelatedByPangkatGolonganId');
        }

        return $this;
    }

    /**
     * Use the PangkatGolonganRelatedByPangkatGolonganId relation PangkatGolongan object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PangkatGolonganQuery A secondary query class using the current class as primary query
     */
    public function usePangkatGolonganRelatedByPangkatGolonganIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPangkatGolonganRelatedByPangkatGolonganId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PangkatGolonganRelatedByPangkatGolonganId', '\angulex\Model\PangkatGolonganQuery');
    }

    /**
     * Filter the query by a related PangkatGolongan object
     *
     * @param   PangkatGolongan|PropelObjectCollection $pangkatGolongan The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 InpassingQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPangkatGolonganRelatedByPangkatGolonganId($pangkatGolongan, $comparison = null)
    {
        if ($pangkatGolongan instanceof PangkatGolongan) {
            return $this
                ->addUsingAlias(InpassingPeer::PANGKAT_GOLONGAN_ID, $pangkatGolongan->getPangkatGolonganId(), $comparison);
        } elseif ($pangkatGolongan instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(InpassingPeer::PANGKAT_GOLONGAN_ID, $pangkatGolongan->toKeyValue('PrimaryKey', 'PangkatGolonganId'), $comparison);
        } else {
            throw new PropelException('filterByPangkatGolonganRelatedByPangkatGolonganId() only accepts arguments of type PangkatGolongan or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PangkatGolonganRelatedByPangkatGolonganId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return InpassingQuery The current query, for fluid interface
     */
    public function joinPangkatGolonganRelatedByPangkatGolonganId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PangkatGolonganRelatedByPangkatGolonganId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PangkatGolonganRelatedByPangkatGolonganId');
        }

        return $this;
    }

    /**
     * Use the PangkatGolonganRelatedByPangkatGolonganId relation PangkatGolongan object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PangkatGolonganQuery A secondary query class using the current class as primary query
     */
    public function usePangkatGolonganRelatedByPangkatGolonganIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPangkatGolonganRelatedByPangkatGolonganId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PangkatGolonganRelatedByPangkatGolonganId', '\angulex\Model\PangkatGolonganQuery');
    }

    /**
     * Filter the query by a related VldInpassing object
     *
     * @param   VldInpassing|PropelObjectCollection $vldInpassing  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 InpassingQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldInpassingRelatedByInpassingId($vldInpassing, $comparison = null)
    {
        if ($vldInpassing instanceof VldInpassing) {
            return $this
                ->addUsingAlias(InpassingPeer::INPASSING_ID, $vldInpassing->getInpassingId(), $comparison);
        } elseif ($vldInpassing instanceof PropelObjectCollection) {
            return $this
                ->useVldInpassingRelatedByInpassingIdQuery()
                ->filterByPrimaryKeys($vldInpassing->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldInpassingRelatedByInpassingId() only accepts arguments of type VldInpassing or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldInpassingRelatedByInpassingId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return InpassingQuery The current query, for fluid interface
     */
    public function joinVldInpassingRelatedByInpassingId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldInpassingRelatedByInpassingId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldInpassingRelatedByInpassingId');
        }

        return $this;
    }

    /**
     * Use the VldInpassingRelatedByInpassingId relation VldInpassing object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldInpassingQuery A secondary query class using the current class as primary query
     */
    public function useVldInpassingRelatedByInpassingIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldInpassingRelatedByInpassingId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldInpassingRelatedByInpassingId', '\angulex\Model\VldInpassingQuery');
    }

    /**
     * Filter the query by a related VldInpassing object
     *
     * @param   VldInpassing|PropelObjectCollection $vldInpassing  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 InpassingQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldInpassingRelatedByInpassingId($vldInpassing, $comparison = null)
    {
        if ($vldInpassing instanceof VldInpassing) {
            return $this
                ->addUsingAlias(InpassingPeer::INPASSING_ID, $vldInpassing->getInpassingId(), $comparison);
        } elseif ($vldInpassing instanceof PropelObjectCollection) {
            return $this
                ->useVldInpassingRelatedByInpassingIdQuery()
                ->filterByPrimaryKeys($vldInpassing->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldInpassingRelatedByInpassingId() only accepts arguments of type VldInpassing or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldInpassingRelatedByInpassingId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return InpassingQuery The current query, for fluid interface
     */
    public function joinVldInpassingRelatedByInpassingId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldInpassingRelatedByInpassingId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldInpassingRelatedByInpassingId');
        }

        return $this;
    }

    /**
     * Use the VldInpassingRelatedByInpassingId relation VldInpassing object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldInpassingQuery A secondary query class using the current class as primary query
     */
    public function useVldInpassingRelatedByInpassingIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldInpassingRelatedByInpassingId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldInpassingRelatedByInpassingId', '\angulex\Model\VldInpassingQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Inpassing $inpassing Object to remove from the list of results
     *
     * @return InpassingQuery The current query, for fluid interface
     */
    public function prune($inpassing = null)
    {
        if ($inpassing) {
            $this->addUsingAlias(InpassingPeer::INPASSING_ID, $inpassing->getInpassingId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
