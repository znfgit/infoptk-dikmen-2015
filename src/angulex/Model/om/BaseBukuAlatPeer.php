<?php

namespace angulex\Model\om;

use \BasePeer;
use \Criteria;
use \PDO;
use \PDOStatement;
use \Propel;
use \PropelException;
use \PropelPDO;
use angulex\Model\BukuAlat;
use angulex\Model\BukuAlatPeer;
use angulex\Model\JenisBukuAlatPeer;
use angulex\Model\MataPelajaranPeer;
use angulex\Model\PrasaranaPeer;
use angulex\Model\SekolahPeer;
use angulex\Model\TingkatPendidikanPeer;
use angulex\Model\map\BukuAlatTableMap;

/**
 * Base static class for performing query and update operations on the 'buku_alat' table.
 *
 * 
 *
 * @package propel.generator.angulex.Model.om
 */
abstract class BaseBukuAlatPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'Dapodikmen';

    /** the table name for this class */
    const TABLE_NAME = 'buku_alat';

    /** the related Propel class for this table */
    const OM_CLASS = 'angulex\\Model\\BukuAlat';

    /** the related TableMap class for this table */
    const TM_CLASS = 'BukuAlatTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 11;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 11;

    /** the column name for the buku_alat_id field */
    const BUKU_ALAT_ID = 'buku_alat.buku_alat_id';

    /** the column name for the mata_pelajaran_id field */
    const MATA_PELAJARAN_ID = 'buku_alat.mata_pelajaran_id';

    /** the column name for the prasarana_id field */
    const PRASARANA_ID = 'buku_alat.prasarana_id';

    /** the column name for the sekolah_id field */
    const SEKOLAH_ID = 'buku_alat.sekolah_id';

    /** the column name for the tingkat_pendidikan_id field */
    const TINGKAT_PENDIDIKAN_ID = 'buku_alat.tingkat_pendidikan_id';

    /** the column name for the jenis_buku_alat_id field */
    const JENIS_BUKU_ALAT_ID = 'buku_alat.jenis_buku_alat_id';

    /** the column name for the buku_alat field */
    const BUKU_ALAT = 'buku_alat.buku_alat';

    /** the column name for the Last_update field */
    const LAST_UPDATE = 'buku_alat.Last_update';

    /** the column name for the Soft_delete field */
    const SOFT_DELETE = 'buku_alat.Soft_delete';

    /** the column name for the last_sync field */
    const LAST_SYNC = 'buku_alat.last_sync';

    /** the column name for the Updater_ID field */
    const UPDATER_ID = 'buku_alat.Updater_ID';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identiy map to hold any loaded instances of BukuAlat objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array BukuAlat[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. BukuAlatPeer::$fieldNames[BukuAlatPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('BukuAlatId', 'MataPelajaranId', 'PrasaranaId', 'SekolahId', 'TingkatPendidikanId', 'JenisBukuAlatId', 'BukuAlat', 'LastUpdate', 'SoftDelete', 'LastSync', 'UpdaterId', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('bukuAlatId', 'mataPelajaranId', 'prasaranaId', 'sekolahId', 'tingkatPendidikanId', 'jenisBukuAlatId', 'bukuAlat', 'lastUpdate', 'softDelete', 'lastSync', 'updaterId', ),
        BasePeer::TYPE_COLNAME => array (BukuAlatPeer::BUKU_ALAT_ID, BukuAlatPeer::MATA_PELAJARAN_ID, BukuAlatPeer::PRASARANA_ID, BukuAlatPeer::SEKOLAH_ID, BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, BukuAlatPeer::JENIS_BUKU_ALAT_ID, BukuAlatPeer::BUKU_ALAT, BukuAlatPeer::LAST_UPDATE, BukuAlatPeer::SOFT_DELETE, BukuAlatPeer::LAST_SYNC, BukuAlatPeer::UPDATER_ID, ),
        BasePeer::TYPE_RAW_COLNAME => array ('BUKU_ALAT_ID', 'MATA_PELAJARAN_ID', 'PRASARANA_ID', 'SEKOLAH_ID', 'TINGKAT_PENDIDIKAN_ID', 'JENIS_BUKU_ALAT_ID', 'BUKU_ALAT', 'LAST_UPDATE', 'SOFT_DELETE', 'LAST_SYNC', 'UPDATER_ID', ),
        BasePeer::TYPE_FIELDNAME => array ('buku_alat_id', 'mata_pelajaran_id', 'prasarana_id', 'sekolah_id', 'tingkat_pendidikan_id', 'jenis_buku_alat_id', 'buku_alat', 'Last_update', 'Soft_delete', 'last_sync', 'Updater_ID', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. BukuAlatPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('BukuAlatId' => 0, 'MataPelajaranId' => 1, 'PrasaranaId' => 2, 'SekolahId' => 3, 'TingkatPendidikanId' => 4, 'JenisBukuAlatId' => 5, 'BukuAlat' => 6, 'LastUpdate' => 7, 'SoftDelete' => 8, 'LastSync' => 9, 'UpdaterId' => 10, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('bukuAlatId' => 0, 'mataPelajaranId' => 1, 'prasaranaId' => 2, 'sekolahId' => 3, 'tingkatPendidikanId' => 4, 'jenisBukuAlatId' => 5, 'bukuAlat' => 6, 'lastUpdate' => 7, 'softDelete' => 8, 'lastSync' => 9, 'updaterId' => 10, ),
        BasePeer::TYPE_COLNAME => array (BukuAlatPeer::BUKU_ALAT_ID => 0, BukuAlatPeer::MATA_PELAJARAN_ID => 1, BukuAlatPeer::PRASARANA_ID => 2, BukuAlatPeer::SEKOLAH_ID => 3, BukuAlatPeer::TINGKAT_PENDIDIKAN_ID => 4, BukuAlatPeer::JENIS_BUKU_ALAT_ID => 5, BukuAlatPeer::BUKU_ALAT => 6, BukuAlatPeer::LAST_UPDATE => 7, BukuAlatPeer::SOFT_DELETE => 8, BukuAlatPeer::LAST_SYNC => 9, BukuAlatPeer::UPDATER_ID => 10, ),
        BasePeer::TYPE_RAW_COLNAME => array ('BUKU_ALAT_ID' => 0, 'MATA_PELAJARAN_ID' => 1, 'PRASARANA_ID' => 2, 'SEKOLAH_ID' => 3, 'TINGKAT_PENDIDIKAN_ID' => 4, 'JENIS_BUKU_ALAT_ID' => 5, 'BUKU_ALAT' => 6, 'LAST_UPDATE' => 7, 'SOFT_DELETE' => 8, 'LAST_SYNC' => 9, 'UPDATER_ID' => 10, ),
        BasePeer::TYPE_FIELDNAME => array ('buku_alat_id' => 0, 'mata_pelajaran_id' => 1, 'prasarana_id' => 2, 'sekolah_id' => 3, 'tingkat_pendidikan_id' => 4, 'jenis_buku_alat_id' => 5, 'buku_alat' => 6, 'Last_update' => 7, 'Soft_delete' => 8, 'last_sync' => 9, 'Updater_ID' => 10, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = BukuAlatPeer::getFieldNames($toType);
        $key = isset(BukuAlatPeer::$fieldKeys[$fromType][$name]) ? BukuAlatPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(BukuAlatPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, BukuAlatPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return BukuAlatPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. BukuAlatPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(BukuAlatPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(BukuAlatPeer::BUKU_ALAT_ID);
            $criteria->addSelectColumn(BukuAlatPeer::MATA_PELAJARAN_ID);
            $criteria->addSelectColumn(BukuAlatPeer::PRASARANA_ID);
            $criteria->addSelectColumn(BukuAlatPeer::SEKOLAH_ID);
            $criteria->addSelectColumn(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID);
            $criteria->addSelectColumn(BukuAlatPeer::JENIS_BUKU_ALAT_ID);
            $criteria->addSelectColumn(BukuAlatPeer::BUKU_ALAT);
            $criteria->addSelectColumn(BukuAlatPeer::LAST_UPDATE);
            $criteria->addSelectColumn(BukuAlatPeer::SOFT_DELETE);
            $criteria->addSelectColumn(BukuAlatPeer::LAST_SYNC);
            $criteria->addSelectColumn(BukuAlatPeer::UPDATER_ID);
        } else {
            $criteria->addSelectColumn($alias . '.buku_alat_id');
            $criteria->addSelectColumn($alias . '.mata_pelajaran_id');
            $criteria->addSelectColumn($alias . '.prasarana_id');
            $criteria->addSelectColumn($alias . '.sekolah_id');
            $criteria->addSelectColumn($alias . '.tingkat_pendidikan_id');
            $criteria->addSelectColumn($alias . '.jenis_buku_alat_id');
            $criteria->addSelectColumn($alias . '.buku_alat');
            $criteria->addSelectColumn($alias . '.Last_update');
            $criteria->addSelectColumn($alias . '.Soft_delete');
            $criteria->addSelectColumn($alias . '.last_sync');
            $criteria->addSelectColumn($alias . '.Updater_ID');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(BukuAlatPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            BukuAlatPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(BukuAlatPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(BukuAlatPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 BukuAlat
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = BukuAlatPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return BukuAlatPeer::populateObjects(BukuAlatPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(BukuAlatPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            BukuAlatPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      BukuAlat $obj A BukuAlat object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getBukuAlatId();
            } // if key === null
            BukuAlatPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A BukuAlat object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof BukuAlat) {
                $key = (string) $value->getBukuAlatId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or BukuAlat object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(BukuAlatPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   BukuAlat Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(BukuAlatPeer::$instances[$key])) {
                return BukuAlatPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }
    
    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references)
      {
        foreach (BukuAlatPeer::$instances as $instance)
        {
          $instance->clearAllReferences(true);
        }
      }
        BukuAlatPeer::$instances = array();
    }
    
    /**
     * Method to invalidate the instance pool of all tables related to buku_alat
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (string) $row[$startcol];
    }
    
    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();
    
        // set the class once to avoid overhead in the loop
        $cls = BukuAlatPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = BukuAlatPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = BukuAlatPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                BukuAlatPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (BukuAlat object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = BukuAlatPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = BukuAlatPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + BukuAlatPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = BukuAlatPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            BukuAlatPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related PrasaranaRelatedByPrasaranaId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinPrasaranaRelatedByPrasaranaId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(BukuAlatPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            BukuAlatPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(BukuAlatPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(BukuAlatPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related PrasaranaRelatedByPrasaranaId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinPrasaranaRelatedByPrasaranaId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(BukuAlatPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            BukuAlatPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(BukuAlatPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(BukuAlatPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SekolahRelatedBySekolahId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinSekolahRelatedBySekolahId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(BukuAlatPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            BukuAlatPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(BukuAlatPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(BukuAlatPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SekolahRelatedBySekolahId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinSekolahRelatedBySekolahId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(BukuAlatPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            BukuAlatPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(BukuAlatPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(BukuAlatPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related JenisBukuAlatRelatedByJenisBukuAlatId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinJenisBukuAlatRelatedByJenisBukuAlatId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(BukuAlatPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            BukuAlatPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(BukuAlatPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(BukuAlatPeer::JENIS_BUKU_ALAT_ID, JenisBukuAlatPeer::JENIS_BUKU_ALAT_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related JenisBukuAlatRelatedByJenisBukuAlatId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinJenisBukuAlatRelatedByJenisBukuAlatId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(BukuAlatPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            BukuAlatPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(BukuAlatPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(BukuAlatPeer::JENIS_BUKU_ALAT_ID, JenisBukuAlatPeer::JENIS_BUKU_ALAT_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related MataPelajaranRelatedByMataPelajaranId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinMataPelajaranRelatedByMataPelajaranId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(BukuAlatPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            BukuAlatPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(BukuAlatPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(BukuAlatPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related MataPelajaranRelatedByMataPelajaranId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinMataPelajaranRelatedByMataPelajaranId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(BukuAlatPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            BukuAlatPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(BukuAlatPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(BukuAlatPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related TingkatPendidikanRelatedByTingkatPendidikanId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinTingkatPendidikanRelatedByTingkatPendidikanId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(BukuAlatPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            BukuAlatPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(BukuAlatPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, TingkatPendidikanPeer::TINGKAT_PENDIDIKAN_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related TingkatPendidikanRelatedByTingkatPendidikanId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinTingkatPendidikanRelatedByTingkatPendidikanId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(BukuAlatPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            BukuAlatPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(BukuAlatPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, TingkatPendidikanPeer::TINGKAT_PENDIDIKAN_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of BukuAlat objects pre-filled with their Prasarana objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of BukuAlat objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinPrasaranaRelatedByPrasaranaId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);
        }

        BukuAlatPeer::addSelectColumns($criteria);
        $startcol = BukuAlatPeer::NUM_HYDRATE_COLUMNS;
        PrasaranaPeer::addSelectColumns($criteria);

        $criteria->addJoin(BukuAlatPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = BukuAlatPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = BukuAlatPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = BukuAlatPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                BukuAlatPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = PrasaranaPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = PrasaranaPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = PrasaranaPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    PrasaranaPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (BukuAlat) to $obj2 (Prasarana)
                $obj2->addBukuAlatRelatedByPrasaranaId($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of BukuAlat objects pre-filled with their Prasarana objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of BukuAlat objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinPrasaranaRelatedByPrasaranaId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);
        }

        BukuAlatPeer::addSelectColumns($criteria);
        $startcol = BukuAlatPeer::NUM_HYDRATE_COLUMNS;
        PrasaranaPeer::addSelectColumns($criteria);

        $criteria->addJoin(BukuAlatPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = BukuAlatPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = BukuAlatPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = BukuAlatPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                BukuAlatPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = PrasaranaPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = PrasaranaPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = PrasaranaPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    PrasaranaPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (BukuAlat) to $obj2 (Prasarana)
                $obj2->addBukuAlatRelatedByPrasaranaId($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of BukuAlat objects pre-filled with their Sekolah objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of BukuAlat objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinSekolahRelatedBySekolahId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);
        }

        BukuAlatPeer::addSelectColumns($criteria);
        $startcol = BukuAlatPeer::NUM_HYDRATE_COLUMNS;
        SekolahPeer::addSelectColumns($criteria);

        $criteria->addJoin(BukuAlatPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = BukuAlatPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = BukuAlatPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = BukuAlatPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                BukuAlatPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = SekolahPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = SekolahPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    SekolahPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (BukuAlat) to $obj2 (Sekolah)
                $obj2->addBukuAlatRelatedBySekolahId($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of BukuAlat objects pre-filled with their Sekolah objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of BukuAlat objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinSekolahRelatedBySekolahId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);
        }

        BukuAlatPeer::addSelectColumns($criteria);
        $startcol = BukuAlatPeer::NUM_HYDRATE_COLUMNS;
        SekolahPeer::addSelectColumns($criteria);

        $criteria->addJoin(BukuAlatPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = BukuAlatPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = BukuAlatPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = BukuAlatPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                BukuAlatPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = SekolahPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = SekolahPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    SekolahPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (BukuAlat) to $obj2 (Sekolah)
                $obj2->addBukuAlatRelatedBySekolahId($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of BukuAlat objects pre-filled with their JenisBukuAlat objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of BukuAlat objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinJenisBukuAlatRelatedByJenisBukuAlatId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);
        }

        BukuAlatPeer::addSelectColumns($criteria);
        $startcol = BukuAlatPeer::NUM_HYDRATE_COLUMNS;
        JenisBukuAlatPeer::addSelectColumns($criteria);

        $criteria->addJoin(BukuAlatPeer::JENIS_BUKU_ALAT_ID, JenisBukuAlatPeer::JENIS_BUKU_ALAT_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = BukuAlatPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = BukuAlatPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = BukuAlatPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                BukuAlatPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = JenisBukuAlatPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = JenisBukuAlatPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = JenisBukuAlatPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    JenisBukuAlatPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (BukuAlat) to $obj2 (JenisBukuAlat)
                $obj2->addBukuAlatRelatedByJenisBukuAlatId($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of BukuAlat objects pre-filled with their JenisBukuAlat objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of BukuAlat objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinJenisBukuAlatRelatedByJenisBukuAlatId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);
        }

        BukuAlatPeer::addSelectColumns($criteria);
        $startcol = BukuAlatPeer::NUM_HYDRATE_COLUMNS;
        JenisBukuAlatPeer::addSelectColumns($criteria);

        $criteria->addJoin(BukuAlatPeer::JENIS_BUKU_ALAT_ID, JenisBukuAlatPeer::JENIS_BUKU_ALAT_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = BukuAlatPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = BukuAlatPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = BukuAlatPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                BukuAlatPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = JenisBukuAlatPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = JenisBukuAlatPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = JenisBukuAlatPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    JenisBukuAlatPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (BukuAlat) to $obj2 (JenisBukuAlat)
                $obj2->addBukuAlatRelatedByJenisBukuAlatId($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of BukuAlat objects pre-filled with their MataPelajaran objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of BukuAlat objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinMataPelajaranRelatedByMataPelajaranId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);
        }

        BukuAlatPeer::addSelectColumns($criteria);
        $startcol = BukuAlatPeer::NUM_HYDRATE_COLUMNS;
        MataPelajaranPeer::addSelectColumns($criteria);

        $criteria->addJoin(BukuAlatPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = BukuAlatPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = BukuAlatPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = BukuAlatPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                BukuAlatPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = MataPelajaranPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = MataPelajaranPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = MataPelajaranPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    MataPelajaranPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (BukuAlat) to $obj2 (MataPelajaran)
                $obj2->addBukuAlatRelatedByMataPelajaranId($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of BukuAlat objects pre-filled with their MataPelajaran objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of BukuAlat objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinMataPelajaranRelatedByMataPelajaranId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);
        }

        BukuAlatPeer::addSelectColumns($criteria);
        $startcol = BukuAlatPeer::NUM_HYDRATE_COLUMNS;
        MataPelajaranPeer::addSelectColumns($criteria);

        $criteria->addJoin(BukuAlatPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = BukuAlatPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = BukuAlatPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = BukuAlatPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                BukuAlatPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = MataPelajaranPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = MataPelajaranPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = MataPelajaranPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    MataPelajaranPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (BukuAlat) to $obj2 (MataPelajaran)
                $obj2->addBukuAlatRelatedByMataPelajaranId($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of BukuAlat objects pre-filled with their TingkatPendidikan objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of BukuAlat objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinTingkatPendidikanRelatedByTingkatPendidikanId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);
        }

        BukuAlatPeer::addSelectColumns($criteria);
        $startcol = BukuAlatPeer::NUM_HYDRATE_COLUMNS;
        TingkatPendidikanPeer::addSelectColumns($criteria);

        $criteria->addJoin(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, TingkatPendidikanPeer::TINGKAT_PENDIDIKAN_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = BukuAlatPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = BukuAlatPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = BukuAlatPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                BukuAlatPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = TingkatPendidikanPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = TingkatPendidikanPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = TingkatPendidikanPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    TingkatPendidikanPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (BukuAlat) to $obj2 (TingkatPendidikan)
                $obj2->addBukuAlatRelatedByTingkatPendidikanId($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of BukuAlat objects pre-filled with their TingkatPendidikan objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of BukuAlat objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinTingkatPendidikanRelatedByTingkatPendidikanId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);
        }

        BukuAlatPeer::addSelectColumns($criteria);
        $startcol = BukuAlatPeer::NUM_HYDRATE_COLUMNS;
        TingkatPendidikanPeer::addSelectColumns($criteria);

        $criteria->addJoin(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, TingkatPendidikanPeer::TINGKAT_PENDIDIKAN_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = BukuAlatPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = BukuAlatPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = BukuAlatPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                BukuAlatPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = TingkatPendidikanPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = TingkatPendidikanPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = TingkatPendidikanPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    TingkatPendidikanPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (BukuAlat) to $obj2 (TingkatPendidikan)
                $obj2->addBukuAlatRelatedByTingkatPendidikanId($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(BukuAlatPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            BukuAlatPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(BukuAlatPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(BukuAlatPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::JENIS_BUKU_ALAT_ID, JenisBukuAlatPeer::JENIS_BUKU_ALAT_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::JENIS_BUKU_ALAT_ID, JenisBukuAlatPeer::JENIS_BUKU_ALAT_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, TingkatPendidikanPeer::TINGKAT_PENDIDIKAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, TingkatPendidikanPeer::TINGKAT_PENDIDIKAN_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of BukuAlat objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of BukuAlat objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);
        }

        BukuAlatPeer::addSelectColumns($criteria);
        $startcol2 = BukuAlatPeer::NUM_HYDRATE_COLUMNS;

        PrasaranaPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + PrasaranaPeer::NUM_HYDRATE_COLUMNS;

        PrasaranaPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + PrasaranaPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        JenisBukuAlatPeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + JenisBukuAlatPeer::NUM_HYDRATE_COLUMNS;

        JenisBukuAlatPeer::addSelectColumns($criteria);
        $startcol8 = $startcol7 + JenisBukuAlatPeer::NUM_HYDRATE_COLUMNS;

        MataPelajaranPeer::addSelectColumns($criteria);
        $startcol9 = $startcol8 + MataPelajaranPeer::NUM_HYDRATE_COLUMNS;

        MataPelajaranPeer::addSelectColumns($criteria);
        $startcol10 = $startcol9 + MataPelajaranPeer::NUM_HYDRATE_COLUMNS;

        TingkatPendidikanPeer::addSelectColumns($criteria);
        $startcol11 = $startcol10 + TingkatPendidikanPeer::NUM_HYDRATE_COLUMNS;

        TingkatPendidikanPeer::addSelectColumns($criteria);
        $startcol12 = $startcol11 + TingkatPendidikanPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(BukuAlatPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::JENIS_BUKU_ALAT_ID, JenisBukuAlatPeer::JENIS_BUKU_ALAT_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::JENIS_BUKU_ALAT_ID, JenisBukuAlatPeer::JENIS_BUKU_ALAT_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, TingkatPendidikanPeer::TINGKAT_PENDIDIKAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, TingkatPendidikanPeer::TINGKAT_PENDIDIKAN_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = BukuAlatPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = BukuAlatPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = BukuAlatPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                BukuAlatPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined Prasarana rows

            $key2 = PrasaranaPeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = PrasaranaPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = PrasaranaPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    PrasaranaPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj2 (Prasarana)
                $obj2->addBukuAlatRelatedByPrasaranaId($obj1);
            } // if joined row not null

            // Add objects for joined Prasarana rows

            $key3 = PrasaranaPeer::getPrimaryKeyHashFromRow($row, $startcol3);
            if ($key3 !== null) {
                $obj3 = PrasaranaPeer::getInstanceFromPool($key3);
                if (!$obj3) {

                    $cls = PrasaranaPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    PrasaranaPeer::addInstanceToPool($obj3, $key3);
                } // if obj3 loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj3 (Prasarana)
                $obj3->addBukuAlatRelatedByPrasaranaId($obj1);
            } // if joined row not null

            // Add objects for joined Sekolah rows

            $key4 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol4);
            if ($key4 !== null) {
                $obj4 = SekolahPeer::getInstanceFromPool($key4);
                if (!$obj4) {

                    $cls = SekolahPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    SekolahPeer::addInstanceToPool($obj4, $key4);
                } // if obj4 loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj4 (Sekolah)
                $obj4->addBukuAlatRelatedBySekolahId($obj1);
            } // if joined row not null

            // Add objects for joined Sekolah rows

            $key5 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol5);
            if ($key5 !== null) {
                $obj5 = SekolahPeer::getInstanceFromPool($key5);
                if (!$obj5) {

                    $cls = SekolahPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    SekolahPeer::addInstanceToPool($obj5, $key5);
                } // if obj5 loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj5 (Sekolah)
                $obj5->addBukuAlatRelatedBySekolahId($obj1);
            } // if joined row not null

            // Add objects for joined JenisBukuAlat rows

            $key6 = JenisBukuAlatPeer::getPrimaryKeyHashFromRow($row, $startcol6);
            if ($key6 !== null) {
                $obj6 = JenisBukuAlatPeer::getInstanceFromPool($key6);
                if (!$obj6) {

                    $cls = JenisBukuAlatPeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    JenisBukuAlatPeer::addInstanceToPool($obj6, $key6);
                } // if obj6 loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj6 (JenisBukuAlat)
                $obj6->addBukuAlatRelatedByJenisBukuAlatId($obj1);
            } // if joined row not null

            // Add objects for joined JenisBukuAlat rows

            $key7 = JenisBukuAlatPeer::getPrimaryKeyHashFromRow($row, $startcol7);
            if ($key7 !== null) {
                $obj7 = JenisBukuAlatPeer::getInstanceFromPool($key7);
                if (!$obj7) {

                    $cls = JenisBukuAlatPeer::getOMClass();

                    $obj7 = new $cls();
                    $obj7->hydrate($row, $startcol7);
                    JenisBukuAlatPeer::addInstanceToPool($obj7, $key7);
                } // if obj7 loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj7 (JenisBukuAlat)
                $obj7->addBukuAlatRelatedByJenisBukuAlatId($obj1);
            } // if joined row not null

            // Add objects for joined MataPelajaran rows

            $key8 = MataPelajaranPeer::getPrimaryKeyHashFromRow($row, $startcol8);
            if ($key8 !== null) {
                $obj8 = MataPelajaranPeer::getInstanceFromPool($key8);
                if (!$obj8) {

                    $cls = MataPelajaranPeer::getOMClass();

                    $obj8 = new $cls();
                    $obj8->hydrate($row, $startcol8);
                    MataPelajaranPeer::addInstanceToPool($obj8, $key8);
                } // if obj8 loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj8 (MataPelajaran)
                $obj8->addBukuAlatRelatedByMataPelajaranId($obj1);
            } // if joined row not null

            // Add objects for joined MataPelajaran rows

            $key9 = MataPelajaranPeer::getPrimaryKeyHashFromRow($row, $startcol9);
            if ($key9 !== null) {
                $obj9 = MataPelajaranPeer::getInstanceFromPool($key9);
                if (!$obj9) {

                    $cls = MataPelajaranPeer::getOMClass();

                    $obj9 = new $cls();
                    $obj9->hydrate($row, $startcol9);
                    MataPelajaranPeer::addInstanceToPool($obj9, $key9);
                } // if obj9 loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj9 (MataPelajaran)
                $obj9->addBukuAlatRelatedByMataPelajaranId($obj1);
            } // if joined row not null

            // Add objects for joined TingkatPendidikan rows

            $key10 = TingkatPendidikanPeer::getPrimaryKeyHashFromRow($row, $startcol10);
            if ($key10 !== null) {
                $obj10 = TingkatPendidikanPeer::getInstanceFromPool($key10);
                if (!$obj10) {

                    $cls = TingkatPendidikanPeer::getOMClass();

                    $obj10 = new $cls();
                    $obj10->hydrate($row, $startcol10);
                    TingkatPendidikanPeer::addInstanceToPool($obj10, $key10);
                } // if obj10 loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj10 (TingkatPendidikan)
                $obj10->addBukuAlatRelatedByTingkatPendidikanId($obj1);
            } // if joined row not null

            // Add objects for joined TingkatPendidikan rows

            $key11 = TingkatPendidikanPeer::getPrimaryKeyHashFromRow($row, $startcol11);
            if ($key11 !== null) {
                $obj11 = TingkatPendidikanPeer::getInstanceFromPool($key11);
                if (!$obj11) {

                    $cls = TingkatPendidikanPeer::getOMClass();

                    $obj11 = new $cls();
                    $obj11->hydrate($row, $startcol11);
                    TingkatPendidikanPeer::addInstanceToPool($obj11, $key11);
                } // if obj11 loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj11 (TingkatPendidikan)
                $obj11->addBukuAlatRelatedByTingkatPendidikanId($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining the related PrasaranaRelatedByPrasaranaId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptPrasaranaRelatedByPrasaranaId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(BukuAlatPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            BukuAlatPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(BukuAlatPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addJoin(BukuAlatPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::JENIS_BUKU_ALAT_ID, JenisBukuAlatPeer::JENIS_BUKU_ALAT_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::JENIS_BUKU_ALAT_ID, JenisBukuAlatPeer::JENIS_BUKU_ALAT_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, TingkatPendidikanPeer::TINGKAT_PENDIDIKAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, TingkatPendidikanPeer::TINGKAT_PENDIDIKAN_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related PrasaranaRelatedByPrasaranaId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptPrasaranaRelatedByPrasaranaId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(BukuAlatPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            BukuAlatPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(BukuAlatPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addJoin(BukuAlatPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::JENIS_BUKU_ALAT_ID, JenisBukuAlatPeer::JENIS_BUKU_ALAT_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::JENIS_BUKU_ALAT_ID, JenisBukuAlatPeer::JENIS_BUKU_ALAT_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, TingkatPendidikanPeer::TINGKAT_PENDIDIKAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, TingkatPendidikanPeer::TINGKAT_PENDIDIKAN_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SekolahRelatedBySekolahId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptSekolahRelatedBySekolahId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(BukuAlatPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            BukuAlatPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(BukuAlatPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addJoin(BukuAlatPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::JENIS_BUKU_ALAT_ID, JenisBukuAlatPeer::JENIS_BUKU_ALAT_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::JENIS_BUKU_ALAT_ID, JenisBukuAlatPeer::JENIS_BUKU_ALAT_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, TingkatPendidikanPeer::TINGKAT_PENDIDIKAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, TingkatPendidikanPeer::TINGKAT_PENDIDIKAN_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SekolahRelatedBySekolahId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptSekolahRelatedBySekolahId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(BukuAlatPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            BukuAlatPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(BukuAlatPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addJoin(BukuAlatPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::JENIS_BUKU_ALAT_ID, JenisBukuAlatPeer::JENIS_BUKU_ALAT_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::JENIS_BUKU_ALAT_ID, JenisBukuAlatPeer::JENIS_BUKU_ALAT_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, TingkatPendidikanPeer::TINGKAT_PENDIDIKAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, TingkatPendidikanPeer::TINGKAT_PENDIDIKAN_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related JenisBukuAlatRelatedByJenisBukuAlatId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptJenisBukuAlatRelatedByJenisBukuAlatId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(BukuAlatPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            BukuAlatPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(BukuAlatPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addJoin(BukuAlatPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, TingkatPendidikanPeer::TINGKAT_PENDIDIKAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, TingkatPendidikanPeer::TINGKAT_PENDIDIKAN_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related JenisBukuAlatRelatedByJenisBukuAlatId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptJenisBukuAlatRelatedByJenisBukuAlatId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(BukuAlatPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            BukuAlatPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(BukuAlatPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addJoin(BukuAlatPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, TingkatPendidikanPeer::TINGKAT_PENDIDIKAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, TingkatPendidikanPeer::TINGKAT_PENDIDIKAN_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related MataPelajaranRelatedByMataPelajaranId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptMataPelajaranRelatedByMataPelajaranId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(BukuAlatPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            BukuAlatPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(BukuAlatPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addJoin(BukuAlatPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::JENIS_BUKU_ALAT_ID, JenisBukuAlatPeer::JENIS_BUKU_ALAT_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::JENIS_BUKU_ALAT_ID, JenisBukuAlatPeer::JENIS_BUKU_ALAT_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, TingkatPendidikanPeer::TINGKAT_PENDIDIKAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, TingkatPendidikanPeer::TINGKAT_PENDIDIKAN_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related MataPelajaranRelatedByMataPelajaranId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptMataPelajaranRelatedByMataPelajaranId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(BukuAlatPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            BukuAlatPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(BukuAlatPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addJoin(BukuAlatPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::JENIS_BUKU_ALAT_ID, JenisBukuAlatPeer::JENIS_BUKU_ALAT_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::JENIS_BUKU_ALAT_ID, JenisBukuAlatPeer::JENIS_BUKU_ALAT_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, TingkatPendidikanPeer::TINGKAT_PENDIDIKAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, TingkatPendidikanPeer::TINGKAT_PENDIDIKAN_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related TingkatPendidikanRelatedByTingkatPendidikanId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptTingkatPendidikanRelatedByTingkatPendidikanId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(BukuAlatPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            BukuAlatPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(BukuAlatPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addJoin(BukuAlatPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::JENIS_BUKU_ALAT_ID, JenisBukuAlatPeer::JENIS_BUKU_ALAT_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::JENIS_BUKU_ALAT_ID, JenisBukuAlatPeer::JENIS_BUKU_ALAT_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related TingkatPendidikanRelatedByTingkatPendidikanId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptTingkatPendidikanRelatedByTingkatPendidikanId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(BukuAlatPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            BukuAlatPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(BukuAlatPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addJoin(BukuAlatPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::JENIS_BUKU_ALAT_ID, JenisBukuAlatPeer::JENIS_BUKU_ALAT_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::JENIS_BUKU_ALAT_ID, JenisBukuAlatPeer::JENIS_BUKU_ALAT_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of BukuAlat objects pre-filled with all related objects except PrasaranaRelatedByPrasaranaId.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of BukuAlat objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptPrasaranaRelatedByPrasaranaId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);
        }

        BukuAlatPeer::addSelectColumns($criteria);
        $startcol2 = BukuAlatPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        JenisBukuAlatPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + JenisBukuAlatPeer::NUM_HYDRATE_COLUMNS;

        JenisBukuAlatPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + JenisBukuAlatPeer::NUM_HYDRATE_COLUMNS;

        MataPelajaranPeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + MataPelajaranPeer::NUM_HYDRATE_COLUMNS;

        MataPelajaranPeer::addSelectColumns($criteria);
        $startcol8 = $startcol7 + MataPelajaranPeer::NUM_HYDRATE_COLUMNS;

        TingkatPendidikanPeer::addSelectColumns($criteria);
        $startcol9 = $startcol8 + TingkatPendidikanPeer::NUM_HYDRATE_COLUMNS;

        TingkatPendidikanPeer::addSelectColumns($criteria);
        $startcol10 = $startcol9 + TingkatPendidikanPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(BukuAlatPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::JENIS_BUKU_ALAT_ID, JenisBukuAlatPeer::JENIS_BUKU_ALAT_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::JENIS_BUKU_ALAT_ID, JenisBukuAlatPeer::JENIS_BUKU_ALAT_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, TingkatPendidikanPeer::TINGKAT_PENDIDIKAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, TingkatPendidikanPeer::TINGKAT_PENDIDIKAN_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = BukuAlatPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = BukuAlatPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = BukuAlatPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                BukuAlatPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Sekolah rows

                $key2 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = SekolahPeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = SekolahPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SekolahPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj2 (Sekolah)
                $obj2->addBukuAlatRelatedBySekolahId($obj1);

            } // if joined row is not null

                // Add objects for joined Sekolah rows

                $key3 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = SekolahPeer::getInstanceFromPool($key3);
                    if (!$obj3) {
    
                        $cls = SekolahPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    SekolahPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj3 (Sekolah)
                $obj3->addBukuAlatRelatedBySekolahId($obj1);

            } // if joined row is not null

                // Add objects for joined JenisBukuAlat rows

                $key4 = JenisBukuAlatPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = JenisBukuAlatPeer::getInstanceFromPool($key4);
                    if (!$obj4) {
    
                        $cls = JenisBukuAlatPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    JenisBukuAlatPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj4 (JenisBukuAlat)
                $obj4->addBukuAlatRelatedByJenisBukuAlatId($obj1);

            } // if joined row is not null

                // Add objects for joined JenisBukuAlat rows

                $key5 = JenisBukuAlatPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = JenisBukuAlatPeer::getInstanceFromPool($key5);
                    if (!$obj5) {
    
                        $cls = JenisBukuAlatPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    JenisBukuAlatPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj5 (JenisBukuAlat)
                $obj5->addBukuAlatRelatedByJenisBukuAlatId($obj1);

            } // if joined row is not null

                // Add objects for joined MataPelajaran rows

                $key6 = MataPelajaranPeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = MataPelajaranPeer::getInstanceFromPool($key6);
                    if (!$obj6) {
    
                        $cls = MataPelajaranPeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    MataPelajaranPeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj6 (MataPelajaran)
                $obj6->addBukuAlatRelatedByMataPelajaranId($obj1);

            } // if joined row is not null

                // Add objects for joined MataPelajaran rows

                $key7 = MataPelajaranPeer::getPrimaryKeyHashFromRow($row, $startcol7);
                if ($key7 !== null) {
                    $obj7 = MataPelajaranPeer::getInstanceFromPool($key7);
                    if (!$obj7) {
    
                        $cls = MataPelajaranPeer::getOMClass();

                    $obj7 = new $cls();
                    $obj7->hydrate($row, $startcol7);
                    MataPelajaranPeer::addInstanceToPool($obj7, $key7);
                } // if $obj7 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj7 (MataPelajaran)
                $obj7->addBukuAlatRelatedByMataPelajaranId($obj1);

            } // if joined row is not null

                // Add objects for joined TingkatPendidikan rows

                $key8 = TingkatPendidikanPeer::getPrimaryKeyHashFromRow($row, $startcol8);
                if ($key8 !== null) {
                    $obj8 = TingkatPendidikanPeer::getInstanceFromPool($key8);
                    if (!$obj8) {
    
                        $cls = TingkatPendidikanPeer::getOMClass();

                    $obj8 = new $cls();
                    $obj8->hydrate($row, $startcol8);
                    TingkatPendidikanPeer::addInstanceToPool($obj8, $key8);
                } // if $obj8 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj8 (TingkatPendidikan)
                $obj8->addBukuAlatRelatedByTingkatPendidikanId($obj1);

            } // if joined row is not null

                // Add objects for joined TingkatPendidikan rows

                $key9 = TingkatPendidikanPeer::getPrimaryKeyHashFromRow($row, $startcol9);
                if ($key9 !== null) {
                    $obj9 = TingkatPendidikanPeer::getInstanceFromPool($key9);
                    if (!$obj9) {
    
                        $cls = TingkatPendidikanPeer::getOMClass();

                    $obj9 = new $cls();
                    $obj9->hydrate($row, $startcol9);
                    TingkatPendidikanPeer::addInstanceToPool($obj9, $key9);
                } // if $obj9 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj9 (TingkatPendidikan)
                $obj9->addBukuAlatRelatedByTingkatPendidikanId($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of BukuAlat objects pre-filled with all related objects except PrasaranaRelatedByPrasaranaId.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of BukuAlat objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptPrasaranaRelatedByPrasaranaId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);
        }

        BukuAlatPeer::addSelectColumns($criteria);
        $startcol2 = BukuAlatPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        JenisBukuAlatPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + JenisBukuAlatPeer::NUM_HYDRATE_COLUMNS;

        JenisBukuAlatPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + JenisBukuAlatPeer::NUM_HYDRATE_COLUMNS;

        MataPelajaranPeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + MataPelajaranPeer::NUM_HYDRATE_COLUMNS;

        MataPelajaranPeer::addSelectColumns($criteria);
        $startcol8 = $startcol7 + MataPelajaranPeer::NUM_HYDRATE_COLUMNS;

        TingkatPendidikanPeer::addSelectColumns($criteria);
        $startcol9 = $startcol8 + TingkatPendidikanPeer::NUM_HYDRATE_COLUMNS;

        TingkatPendidikanPeer::addSelectColumns($criteria);
        $startcol10 = $startcol9 + TingkatPendidikanPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(BukuAlatPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::JENIS_BUKU_ALAT_ID, JenisBukuAlatPeer::JENIS_BUKU_ALAT_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::JENIS_BUKU_ALAT_ID, JenisBukuAlatPeer::JENIS_BUKU_ALAT_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, TingkatPendidikanPeer::TINGKAT_PENDIDIKAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, TingkatPendidikanPeer::TINGKAT_PENDIDIKAN_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = BukuAlatPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = BukuAlatPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = BukuAlatPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                BukuAlatPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Sekolah rows

                $key2 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = SekolahPeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = SekolahPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SekolahPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj2 (Sekolah)
                $obj2->addBukuAlatRelatedBySekolahId($obj1);

            } // if joined row is not null

                // Add objects for joined Sekolah rows

                $key3 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = SekolahPeer::getInstanceFromPool($key3);
                    if (!$obj3) {
    
                        $cls = SekolahPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    SekolahPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj3 (Sekolah)
                $obj3->addBukuAlatRelatedBySekolahId($obj1);

            } // if joined row is not null

                // Add objects for joined JenisBukuAlat rows

                $key4 = JenisBukuAlatPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = JenisBukuAlatPeer::getInstanceFromPool($key4);
                    if (!$obj4) {
    
                        $cls = JenisBukuAlatPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    JenisBukuAlatPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj4 (JenisBukuAlat)
                $obj4->addBukuAlatRelatedByJenisBukuAlatId($obj1);

            } // if joined row is not null

                // Add objects for joined JenisBukuAlat rows

                $key5 = JenisBukuAlatPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = JenisBukuAlatPeer::getInstanceFromPool($key5);
                    if (!$obj5) {
    
                        $cls = JenisBukuAlatPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    JenisBukuAlatPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj5 (JenisBukuAlat)
                $obj5->addBukuAlatRelatedByJenisBukuAlatId($obj1);

            } // if joined row is not null

                // Add objects for joined MataPelajaran rows

                $key6 = MataPelajaranPeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = MataPelajaranPeer::getInstanceFromPool($key6);
                    if (!$obj6) {
    
                        $cls = MataPelajaranPeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    MataPelajaranPeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj6 (MataPelajaran)
                $obj6->addBukuAlatRelatedByMataPelajaranId($obj1);

            } // if joined row is not null

                // Add objects for joined MataPelajaran rows

                $key7 = MataPelajaranPeer::getPrimaryKeyHashFromRow($row, $startcol7);
                if ($key7 !== null) {
                    $obj7 = MataPelajaranPeer::getInstanceFromPool($key7);
                    if (!$obj7) {
    
                        $cls = MataPelajaranPeer::getOMClass();

                    $obj7 = new $cls();
                    $obj7->hydrate($row, $startcol7);
                    MataPelajaranPeer::addInstanceToPool($obj7, $key7);
                } // if $obj7 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj7 (MataPelajaran)
                $obj7->addBukuAlatRelatedByMataPelajaranId($obj1);

            } // if joined row is not null

                // Add objects for joined TingkatPendidikan rows

                $key8 = TingkatPendidikanPeer::getPrimaryKeyHashFromRow($row, $startcol8);
                if ($key8 !== null) {
                    $obj8 = TingkatPendidikanPeer::getInstanceFromPool($key8);
                    if (!$obj8) {
    
                        $cls = TingkatPendidikanPeer::getOMClass();

                    $obj8 = new $cls();
                    $obj8->hydrate($row, $startcol8);
                    TingkatPendidikanPeer::addInstanceToPool($obj8, $key8);
                } // if $obj8 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj8 (TingkatPendidikan)
                $obj8->addBukuAlatRelatedByTingkatPendidikanId($obj1);

            } // if joined row is not null

                // Add objects for joined TingkatPendidikan rows

                $key9 = TingkatPendidikanPeer::getPrimaryKeyHashFromRow($row, $startcol9);
                if ($key9 !== null) {
                    $obj9 = TingkatPendidikanPeer::getInstanceFromPool($key9);
                    if (!$obj9) {
    
                        $cls = TingkatPendidikanPeer::getOMClass();

                    $obj9 = new $cls();
                    $obj9->hydrate($row, $startcol9);
                    TingkatPendidikanPeer::addInstanceToPool($obj9, $key9);
                } // if $obj9 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj9 (TingkatPendidikan)
                $obj9->addBukuAlatRelatedByTingkatPendidikanId($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of BukuAlat objects pre-filled with all related objects except SekolahRelatedBySekolahId.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of BukuAlat objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptSekolahRelatedBySekolahId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);
        }

        BukuAlatPeer::addSelectColumns($criteria);
        $startcol2 = BukuAlatPeer::NUM_HYDRATE_COLUMNS;

        PrasaranaPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + PrasaranaPeer::NUM_HYDRATE_COLUMNS;

        PrasaranaPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + PrasaranaPeer::NUM_HYDRATE_COLUMNS;

        JenisBukuAlatPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + JenisBukuAlatPeer::NUM_HYDRATE_COLUMNS;

        JenisBukuAlatPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + JenisBukuAlatPeer::NUM_HYDRATE_COLUMNS;

        MataPelajaranPeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + MataPelajaranPeer::NUM_HYDRATE_COLUMNS;

        MataPelajaranPeer::addSelectColumns($criteria);
        $startcol8 = $startcol7 + MataPelajaranPeer::NUM_HYDRATE_COLUMNS;

        TingkatPendidikanPeer::addSelectColumns($criteria);
        $startcol9 = $startcol8 + TingkatPendidikanPeer::NUM_HYDRATE_COLUMNS;

        TingkatPendidikanPeer::addSelectColumns($criteria);
        $startcol10 = $startcol9 + TingkatPendidikanPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(BukuAlatPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::JENIS_BUKU_ALAT_ID, JenisBukuAlatPeer::JENIS_BUKU_ALAT_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::JENIS_BUKU_ALAT_ID, JenisBukuAlatPeer::JENIS_BUKU_ALAT_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, TingkatPendidikanPeer::TINGKAT_PENDIDIKAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, TingkatPendidikanPeer::TINGKAT_PENDIDIKAN_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = BukuAlatPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = BukuAlatPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = BukuAlatPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                BukuAlatPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Prasarana rows

                $key2 = PrasaranaPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = PrasaranaPeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = PrasaranaPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    PrasaranaPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj2 (Prasarana)
                $obj2->addBukuAlatRelatedByPrasaranaId($obj1);

            } // if joined row is not null

                // Add objects for joined Prasarana rows

                $key3 = PrasaranaPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = PrasaranaPeer::getInstanceFromPool($key3);
                    if (!$obj3) {
    
                        $cls = PrasaranaPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    PrasaranaPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj3 (Prasarana)
                $obj3->addBukuAlatRelatedByPrasaranaId($obj1);

            } // if joined row is not null

                // Add objects for joined JenisBukuAlat rows

                $key4 = JenisBukuAlatPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = JenisBukuAlatPeer::getInstanceFromPool($key4);
                    if (!$obj4) {
    
                        $cls = JenisBukuAlatPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    JenisBukuAlatPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj4 (JenisBukuAlat)
                $obj4->addBukuAlatRelatedByJenisBukuAlatId($obj1);

            } // if joined row is not null

                // Add objects for joined JenisBukuAlat rows

                $key5 = JenisBukuAlatPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = JenisBukuAlatPeer::getInstanceFromPool($key5);
                    if (!$obj5) {
    
                        $cls = JenisBukuAlatPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    JenisBukuAlatPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj5 (JenisBukuAlat)
                $obj5->addBukuAlatRelatedByJenisBukuAlatId($obj1);

            } // if joined row is not null

                // Add objects for joined MataPelajaran rows

                $key6 = MataPelajaranPeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = MataPelajaranPeer::getInstanceFromPool($key6);
                    if (!$obj6) {
    
                        $cls = MataPelajaranPeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    MataPelajaranPeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj6 (MataPelajaran)
                $obj6->addBukuAlatRelatedByMataPelajaranId($obj1);

            } // if joined row is not null

                // Add objects for joined MataPelajaran rows

                $key7 = MataPelajaranPeer::getPrimaryKeyHashFromRow($row, $startcol7);
                if ($key7 !== null) {
                    $obj7 = MataPelajaranPeer::getInstanceFromPool($key7);
                    if (!$obj7) {
    
                        $cls = MataPelajaranPeer::getOMClass();

                    $obj7 = new $cls();
                    $obj7->hydrate($row, $startcol7);
                    MataPelajaranPeer::addInstanceToPool($obj7, $key7);
                } // if $obj7 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj7 (MataPelajaran)
                $obj7->addBukuAlatRelatedByMataPelajaranId($obj1);

            } // if joined row is not null

                // Add objects for joined TingkatPendidikan rows

                $key8 = TingkatPendidikanPeer::getPrimaryKeyHashFromRow($row, $startcol8);
                if ($key8 !== null) {
                    $obj8 = TingkatPendidikanPeer::getInstanceFromPool($key8);
                    if (!$obj8) {
    
                        $cls = TingkatPendidikanPeer::getOMClass();

                    $obj8 = new $cls();
                    $obj8->hydrate($row, $startcol8);
                    TingkatPendidikanPeer::addInstanceToPool($obj8, $key8);
                } // if $obj8 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj8 (TingkatPendidikan)
                $obj8->addBukuAlatRelatedByTingkatPendidikanId($obj1);

            } // if joined row is not null

                // Add objects for joined TingkatPendidikan rows

                $key9 = TingkatPendidikanPeer::getPrimaryKeyHashFromRow($row, $startcol9);
                if ($key9 !== null) {
                    $obj9 = TingkatPendidikanPeer::getInstanceFromPool($key9);
                    if (!$obj9) {
    
                        $cls = TingkatPendidikanPeer::getOMClass();

                    $obj9 = new $cls();
                    $obj9->hydrate($row, $startcol9);
                    TingkatPendidikanPeer::addInstanceToPool($obj9, $key9);
                } // if $obj9 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj9 (TingkatPendidikan)
                $obj9->addBukuAlatRelatedByTingkatPendidikanId($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of BukuAlat objects pre-filled with all related objects except SekolahRelatedBySekolahId.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of BukuAlat objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptSekolahRelatedBySekolahId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);
        }

        BukuAlatPeer::addSelectColumns($criteria);
        $startcol2 = BukuAlatPeer::NUM_HYDRATE_COLUMNS;

        PrasaranaPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + PrasaranaPeer::NUM_HYDRATE_COLUMNS;

        PrasaranaPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + PrasaranaPeer::NUM_HYDRATE_COLUMNS;

        JenisBukuAlatPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + JenisBukuAlatPeer::NUM_HYDRATE_COLUMNS;

        JenisBukuAlatPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + JenisBukuAlatPeer::NUM_HYDRATE_COLUMNS;

        MataPelajaranPeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + MataPelajaranPeer::NUM_HYDRATE_COLUMNS;

        MataPelajaranPeer::addSelectColumns($criteria);
        $startcol8 = $startcol7 + MataPelajaranPeer::NUM_HYDRATE_COLUMNS;

        TingkatPendidikanPeer::addSelectColumns($criteria);
        $startcol9 = $startcol8 + TingkatPendidikanPeer::NUM_HYDRATE_COLUMNS;

        TingkatPendidikanPeer::addSelectColumns($criteria);
        $startcol10 = $startcol9 + TingkatPendidikanPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(BukuAlatPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::JENIS_BUKU_ALAT_ID, JenisBukuAlatPeer::JENIS_BUKU_ALAT_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::JENIS_BUKU_ALAT_ID, JenisBukuAlatPeer::JENIS_BUKU_ALAT_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, TingkatPendidikanPeer::TINGKAT_PENDIDIKAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, TingkatPendidikanPeer::TINGKAT_PENDIDIKAN_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = BukuAlatPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = BukuAlatPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = BukuAlatPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                BukuAlatPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Prasarana rows

                $key2 = PrasaranaPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = PrasaranaPeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = PrasaranaPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    PrasaranaPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj2 (Prasarana)
                $obj2->addBukuAlatRelatedByPrasaranaId($obj1);

            } // if joined row is not null

                // Add objects for joined Prasarana rows

                $key3 = PrasaranaPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = PrasaranaPeer::getInstanceFromPool($key3);
                    if (!$obj3) {
    
                        $cls = PrasaranaPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    PrasaranaPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj3 (Prasarana)
                $obj3->addBukuAlatRelatedByPrasaranaId($obj1);

            } // if joined row is not null

                // Add objects for joined JenisBukuAlat rows

                $key4 = JenisBukuAlatPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = JenisBukuAlatPeer::getInstanceFromPool($key4);
                    if (!$obj4) {
    
                        $cls = JenisBukuAlatPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    JenisBukuAlatPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj4 (JenisBukuAlat)
                $obj4->addBukuAlatRelatedByJenisBukuAlatId($obj1);

            } // if joined row is not null

                // Add objects for joined JenisBukuAlat rows

                $key5 = JenisBukuAlatPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = JenisBukuAlatPeer::getInstanceFromPool($key5);
                    if (!$obj5) {
    
                        $cls = JenisBukuAlatPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    JenisBukuAlatPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj5 (JenisBukuAlat)
                $obj5->addBukuAlatRelatedByJenisBukuAlatId($obj1);

            } // if joined row is not null

                // Add objects for joined MataPelajaran rows

                $key6 = MataPelajaranPeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = MataPelajaranPeer::getInstanceFromPool($key6);
                    if (!$obj6) {
    
                        $cls = MataPelajaranPeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    MataPelajaranPeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj6 (MataPelajaran)
                $obj6->addBukuAlatRelatedByMataPelajaranId($obj1);

            } // if joined row is not null

                // Add objects for joined MataPelajaran rows

                $key7 = MataPelajaranPeer::getPrimaryKeyHashFromRow($row, $startcol7);
                if ($key7 !== null) {
                    $obj7 = MataPelajaranPeer::getInstanceFromPool($key7);
                    if (!$obj7) {
    
                        $cls = MataPelajaranPeer::getOMClass();

                    $obj7 = new $cls();
                    $obj7->hydrate($row, $startcol7);
                    MataPelajaranPeer::addInstanceToPool($obj7, $key7);
                } // if $obj7 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj7 (MataPelajaran)
                $obj7->addBukuAlatRelatedByMataPelajaranId($obj1);

            } // if joined row is not null

                // Add objects for joined TingkatPendidikan rows

                $key8 = TingkatPendidikanPeer::getPrimaryKeyHashFromRow($row, $startcol8);
                if ($key8 !== null) {
                    $obj8 = TingkatPendidikanPeer::getInstanceFromPool($key8);
                    if (!$obj8) {
    
                        $cls = TingkatPendidikanPeer::getOMClass();

                    $obj8 = new $cls();
                    $obj8->hydrate($row, $startcol8);
                    TingkatPendidikanPeer::addInstanceToPool($obj8, $key8);
                } // if $obj8 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj8 (TingkatPendidikan)
                $obj8->addBukuAlatRelatedByTingkatPendidikanId($obj1);

            } // if joined row is not null

                // Add objects for joined TingkatPendidikan rows

                $key9 = TingkatPendidikanPeer::getPrimaryKeyHashFromRow($row, $startcol9);
                if ($key9 !== null) {
                    $obj9 = TingkatPendidikanPeer::getInstanceFromPool($key9);
                    if (!$obj9) {
    
                        $cls = TingkatPendidikanPeer::getOMClass();

                    $obj9 = new $cls();
                    $obj9->hydrate($row, $startcol9);
                    TingkatPendidikanPeer::addInstanceToPool($obj9, $key9);
                } // if $obj9 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj9 (TingkatPendidikan)
                $obj9->addBukuAlatRelatedByTingkatPendidikanId($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of BukuAlat objects pre-filled with all related objects except JenisBukuAlatRelatedByJenisBukuAlatId.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of BukuAlat objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptJenisBukuAlatRelatedByJenisBukuAlatId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);
        }

        BukuAlatPeer::addSelectColumns($criteria);
        $startcol2 = BukuAlatPeer::NUM_HYDRATE_COLUMNS;

        PrasaranaPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + PrasaranaPeer::NUM_HYDRATE_COLUMNS;

        PrasaranaPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + PrasaranaPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        MataPelajaranPeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + MataPelajaranPeer::NUM_HYDRATE_COLUMNS;

        MataPelajaranPeer::addSelectColumns($criteria);
        $startcol8 = $startcol7 + MataPelajaranPeer::NUM_HYDRATE_COLUMNS;

        TingkatPendidikanPeer::addSelectColumns($criteria);
        $startcol9 = $startcol8 + TingkatPendidikanPeer::NUM_HYDRATE_COLUMNS;

        TingkatPendidikanPeer::addSelectColumns($criteria);
        $startcol10 = $startcol9 + TingkatPendidikanPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(BukuAlatPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, TingkatPendidikanPeer::TINGKAT_PENDIDIKAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, TingkatPendidikanPeer::TINGKAT_PENDIDIKAN_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = BukuAlatPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = BukuAlatPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = BukuAlatPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                BukuAlatPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Prasarana rows

                $key2 = PrasaranaPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = PrasaranaPeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = PrasaranaPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    PrasaranaPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj2 (Prasarana)
                $obj2->addBukuAlatRelatedByPrasaranaId($obj1);

            } // if joined row is not null

                // Add objects for joined Prasarana rows

                $key3 = PrasaranaPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = PrasaranaPeer::getInstanceFromPool($key3);
                    if (!$obj3) {
    
                        $cls = PrasaranaPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    PrasaranaPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj3 (Prasarana)
                $obj3->addBukuAlatRelatedByPrasaranaId($obj1);

            } // if joined row is not null

                // Add objects for joined Sekolah rows

                $key4 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = SekolahPeer::getInstanceFromPool($key4);
                    if (!$obj4) {
    
                        $cls = SekolahPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    SekolahPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj4 (Sekolah)
                $obj4->addBukuAlatRelatedBySekolahId($obj1);

            } // if joined row is not null

                // Add objects for joined Sekolah rows

                $key5 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = SekolahPeer::getInstanceFromPool($key5);
                    if (!$obj5) {
    
                        $cls = SekolahPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    SekolahPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj5 (Sekolah)
                $obj5->addBukuAlatRelatedBySekolahId($obj1);

            } // if joined row is not null

                // Add objects for joined MataPelajaran rows

                $key6 = MataPelajaranPeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = MataPelajaranPeer::getInstanceFromPool($key6);
                    if (!$obj6) {
    
                        $cls = MataPelajaranPeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    MataPelajaranPeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj6 (MataPelajaran)
                $obj6->addBukuAlatRelatedByMataPelajaranId($obj1);

            } // if joined row is not null

                // Add objects for joined MataPelajaran rows

                $key7 = MataPelajaranPeer::getPrimaryKeyHashFromRow($row, $startcol7);
                if ($key7 !== null) {
                    $obj7 = MataPelajaranPeer::getInstanceFromPool($key7);
                    if (!$obj7) {
    
                        $cls = MataPelajaranPeer::getOMClass();

                    $obj7 = new $cls();
                    $obj7->hydrate($row, $startcol7);
                    MataPelajaranPeer::addInstanceToPool($obj7, $key7);
                } // if $obj7 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj7 (MataPelajaran)
                $obj7->addBukuAlatRelatedByMataPelajaranId($obj1);

            } // if joined row is not null

                // Add objects for joined TingkatPendidikan rows

                $key8 = TingkatPendidikanPeer::getPrimaryKeyHashFromRow($row, $startcol8);
                if ($key8 !== null) {
                    $obj8 = TingkatPendidikanPeer::getInstanceFromPool($key8);
                    if (!$obj8) {
    
                        $cls = TingkatPendidikanPeer::getOMClass();

                    $obj8 = new $cls();
                    $obj8->hydrate($row, $startcol8);
                    TingkatPendidikanPeer::addInstanceToPool($obj8, $key8);
                } // if $obj8 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj8 (TingkatPendidikan)
                $obj8->addBukuAlatRelatedByTingkatPendidikanId($obj1);

            } // if joined row is not null

                // Add objects for joined TingkatPendidikan rows

                $key9 = TingkatPendidikanPeer::getPrimaryKeyHashFromRow($row, $startcol9);
                if ($key9 !== null) {
                    $obj9 = TingkatPendidikanPeer::getInstanceFromPool($key9);
                    if (!$obj9) {
    
                        $cls = TingkatPendidikanPeer::getOMClass();

                    $obj9 = new $cls();
                    $obj9->hydrate($row, $startcol9);
                    TingkatPendidikanPeer::addInstanceToPool($obj9, $key9);
                } // if $obj9 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj9 (TingkatPendidikan)
                $obj9->addBukuAlatRelatedByTingkatPendidikanId($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of BukuAlat objects pre-filled with all related objects except JenisBukuAlatRelatedByJenisBukuAlatId.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of BukuAlat objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptJenisBukuAlatRelatedByJenisBukuAlatId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);
        }

        BukuAlatPeer::addSelectColumns($criteria);
        $startcol2 = BukuAlatPeer::NUM_HYDRATE_COLUMNS;

        PrasaranaPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + PrasaranaPeer::NUM_HYDRATE_COLUMNS;

        PrasaranaPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + PrasaranaPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        MataPelajaranPeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + MataPelajaranPeer::NUM_HYDRATE_COLUMNS;

        MataPelajaranPeer::addSelectColumns($criteria);
        $startcol8 = $startcol7 + MataPelajaranPeer::NUM_HYDRATE_COLUMNS;

        TingkatPendidikanPeer::addSelectColumns($criteria);
        $startcol9 = $startcol8 + TingkatPendidikanPeer::NUM_HYDRATE_COLUMNS;

        TingkatPendidikanPeer::addSelectColumns($criteria);
        $startcol10 = $startcol9 + TingkatPendidikanPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(BukuAlatPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, TingkatPendidikanPeer::TINGKAT_PENDIDIKAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, TingkatPendidikanPeer::TINGKAT_PENDIDIKAN_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = BukuAlatPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = BukuAlatPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = BukuAlatPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                BukuAlatPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Prasarana rows

                $key2 = PrasaranaPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = PrasaranaPeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = PrasaranaPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    PrasaranaPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj2 (Prasarana)
                $obj2->addBukuAlatRelatedByPrasaranaId($obj1);

            } // if joined row is not null

                // Add objects for joined Prasarana rows

                $key3 = PrasaranaPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = PrasaranaPeer::getInstanceFromPool($key3);
                    if (!$obj3) {
    
                        $cls = PrasaranaPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    PrasaranaPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj3 (Prasarana)
                $obj3->addBukuAlatRelatedByPrasaranaId($obj1);

            } // if joined row is not null

                // Add objects for joined Sekolah rows

                $key4 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = SekolahPeer::getInstanceFromPool($key4);
                    if (!$obj4) {
    
                        $cls = SekolahPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    SekolahPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj4 (Sekolah)
                $obj4->addBukuAlatRelatedBySekolahId($obj1);

            } // if joined row is not null

                // Add objects for joined Sekolah rows

                $key5 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = SekolahPeer::getInstanceFromPool($key5);
                    if (!$obj5) {
    
                        $cls = SekolahPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    SekolahPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj5 (Sekolah)
                $obj5->addBukuAlatRelatedBySekolahId($obj1);

            } // if joined row is not null

                // Add objects for joined MataPelajaran rows

                $key6 = MataPelajaranPeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = MataPelajaranPeer::getInstanceFromPool($key6);
                    if (!$obj6) {
    
                        $cls = MataPelajaranPeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    MataPelajaranPeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj6 (MataPelajaran)
                $obj6->addBukuAlatRelatedByMataPelajaranId($obj1);

            } // if joined row is not null

                // Add objects for joined MataPelajaran rows

                $key7 = MataPelajaranPeer::getPrimaryKeyHashFromRow($row, $startcol7);
                if ($key7 !== null) {
                    $obj7 = MataPelajaranPeer::getInstanceFromPool($key7);
                    if (!$obj7) {
    
                        $cls = MataPelajaranPeer::getOMClass();

                    $obj7 = new $cls();
                    $obj7->hydrate($row, $startcol7);
                    MataPelajaranPeer::addInstanceToPool($obj7, $key7);
                } // if $obj7 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj7 (MataPelajaran)
                $obj7->addBukuAlatRelatedByMataPelajaranId($obj1);

            } // if joined row is not null

                // Add objects for joined TingkatPendidikan rows

                $key8 = TingkatPendidikanPeer::getPrimaryKeyHashFromRow($row, $startcol8);
                if ($key8 !== null) {
                    $obj8 = TingkatPendidikanPeer::getInstanceFromPool($key8);
                    if (!$obj8) {
    
                        $cls = TingkatPendidikanPeer::getOMClass();

                    $obj8 = new $cls();
                    $obj8->hydrate($row, $startcol8);
                    TingkatPendidikanPeer::addInstanceToPool($obj8, $key8);
                } // if $obj8 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj8 (TingkatPendidikan)
                $obj8->addBukuAlatRelatedByTingkatPendidikanId($obj1);

            } // if joined row is not null

                // Add objects for joined TingkatPendidikan rows

                $key9 = TingkatPendidikanPeer::getPrimaryKeyHashFromRow($row, $startcol9);
                if ($key9 !== null) {
                    $obj9 = TingkatPendidikanPeer::getInstanceFromPool($key9);
                    if (!$obj9) {
    
                        $cls = TingkatPendidikanPeer::getOMClass();

                    $obj9 = new $cls();
                    $obj9->hydrate($row, $startcol9);
                    TingkatPendidikanPeer::addInstanceToPool($obj9, $key9);
                } // if $obj9 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj9 (TingkatPendidikan)
                $obj9->addBukuAlatRelatedByTingkatPendidikanId($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of BukuAlat objects pre-filled with all related objects except MataPelajaranRelatedByMataPelajaranId.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of BukuAlat objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptMataPelajaranRelatedByMataPelajaranId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);
        }

        BukuAlatPeer::addSelectColumns($criteria);
        $startcol2 = BukuAlatPeer::NUM_HYDRATE_COLUMNS;

        PrasaranaPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + PrasaranaPeer::NUM_HYDRATE_COLUMNS;

        PrasaranaPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + PrasaranaPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        JenisBukuAlatPeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + JenisBukuAlatPeer::NUM_HYDRATE_COLUMNS;

        JenisBukuAlatPeer::addSelectColumns($criteria);
        $startcol8 = $startcol7 + JenisBukuAlatPeer::NUM_HYDRATE_COLUMNS;

        TingkatPendidikanPeer::addSelectColumns($criteria);
        $startcol9 = $startcol8 + TingkatPendidikanPeer::NUM_HYDRATE_COLUMNS;

        TingkatPendidikanPeer::addSelectColumns($criteria);
        $startcol10 = $startcol9 + TingkatPendidikanPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(BukuAlatPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::JENIS_BUKU_ALAT_ID, JenisBukuAlatPeer::JENIS_BUKU_ALAT_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::JENIS_BUKU_ALAT_ID, JenisBukuAlatPeer::JENIS_BUKU_ALAT_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, TingkatPendidikanPeer::TINGKAT_PENDIDIKAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, TingkatPendidikanPeer::TINGKAT_PENDIDIKAN_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = BukuAlatPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = BukuAlatPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = BukuAlatPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                BukuAlatPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Prasarana rows

                $key2 = PrasaranaPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = PrasaranaPeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = PrasaranaPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    PrasaranaPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj2 (Prasarana)
                $obj2->addBukuAlatRelatedByPrasaranaId($obj1);

            } // if joined row is not null

                // Add objects for joined Prasarana rows

                $key3 = PrasaranaPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = PrasaranaPeer::getInstanceFromPool($key3);
                    if (!$obj3) {
    
                        $cls = PrasaranaPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    PrasaranaPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj3 (Prasarana)
                $obj3->addBukuAlatRelatedByPrasaranaId($obj1);

            } // if joined row is not null

                // Add objects for joined Sekolah rows

                $key4 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = SekolahPeer::getInstanceFromPool($key4);
                    if (!$obj4) {
    
                        $cls = SekolahPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    SekolahPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj4 (Sekolah)
                $obj4->addBukuAlatRelatedBySekolahId($obj1);

            } // if joined row is not null

                // Add objects for joined Sekolah rows

                $key5 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = SekolahPeer::getInstanceFromPool($key5);
                    if (!$obj5) {
    
                        $cls = SekolahPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    SekolahPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj5 (Sekolah)
                $obj5->addBukuAlatRelatedBySekolahId($obj1);

            } // if joined row is not null

                // Add objects for joined JenisBukuAlat rows

                $key6 = JenisBukuAlatPeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = JenisBukuAlatPeer::getInstanceFromPool($key6);
                    if (!$obj6) {
    
                        $cls = JenisBukuAlatPeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    JenisBukuAlatPeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj6 (JenisBukuAlat)
                $obj6->addBukuAlatRelatedByJenisBukuAlatId($obj1);

            } // if joined row is not null

                // Add objects for joined JenisBukuAlat rows

                $key7 = JenisBukuAlatPeer::getPrimaryKeyHashFromRow($row, $startcol7);
                if ($key7 !== null) {
                    $obj7 = JenisBukuAlatPeer::getInstanceFromPool($key7);
                    if (!$obj7) {
    
                        $cls = JenisBukuAlatPeer::getOMClass();

                    $obj7 = new $cls();
                    $obj7->hydrate($row, $startcol7);
                    JenisBukuAlatPeer::addInstanceToPool($obj7, $key7);
                } // if $obj7 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj7 (JenisBukuAlat)
                $obj7->addBukuAlatRelatedByJenisBukuAlatId($obj1);

            } // if joined row is not null

                // Add objects for joined TingkatPendidikan rows

                $key8 = TingkatPendidikanPeer::getPrimaryKeyHashFromRow($row, $startcol8);
                if ($key8 !== null) {
                    $obj8 = TingkatPendidikanPeer::getInstanceFromPool($key8);
                    if (!$obj8) {
    
                        $cls = TingkatPendidikanPeer::getOMClass();

                    $obj8 = new $cls();
                    $obj8->hydrate($row, $startcol8);
                    TingkatPendidikanPeer::addInstanceToPool($obj8, $key8);
                } // if $obj8 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj8 (TingkatPendidikan)
                $obj8->addBukuAlatRelatedByTingkatPendidikanId($obj1);

            } // if joined row is not null

                // Add objects for joined TingkatPendidikan rows

                $key9 = TingkatPendidikanPeer::getPrimaryKeyHashFromRow($row, $startcol9);
                if ($key9 !== null) {
                    $obj9 = TingkatPendidikanPeer::getInstanceFromPool($key9);
                    if (!$obj9) {
    
                        $cls = TingkatPendidikanPeer::getOMClass();

                    $obj9 = new $cls();
                    $obj9->hydrate($row, $startcol9);
                    TingkatPendidikanPeer::addInstanceToPool($obj9, $key9);
                } // if $obj9 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj9 (TingkatPendidikan)
                $obj9->addBukuAlatRelatedByTingkatPendidikanId($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of BukuAlat objects pre-filled with all related objects except MataPelajaranRelatedByMataPelajaranId.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of BukuAlat objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptMataPelajaranRelatedByMataPelajaranId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);
        }

        BukuAlatPeer::addSelectColumns($criteria);
        $startcol2 = BukuAlatPeer::NUM_HYDRATE_COLUMNS;

        PrasaranaPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + PrasaranaPeer::NUM_HYDRATE_COLUMNS;

        PrasaranaPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + PrasaranaPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        JenisBukuAlatPeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + JenisBukuAlatPeer::NUM_HYDRATE_COLUMNS;

        JenisBukuAlatPeer::addSelectColumns($criteria);
        $startcol8 = $startcol7 + JenisBukuAlatPeer::NUM_HYDRATE_COLUMNS;

        TingkatPendidikanPeer::addSelectColumns($criteria);
        $startcol9 = $startcol8 + TingkatPendidikanPeer::NUM_HYDRATE_COLUMNS;

        TingkatPendidikanPeer::addSelectColumns($criteria);
        $startcol10 = $startcol9 + TingkatPendidikanPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(BukuAlatPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::JENIS_BUKU_ALAT_ID, JenisBukuAlatPeer::JENIS_BUKU_ALAT_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::JENIS_BUKU_ALAT_ID, JenisBukuAlatPeer::JENIS_BUKU_ALAT_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, TingkatPendidikanPeer::TINGKAT_PENDIDIKAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, TingkatPendidikanPeer::TINGKAT_PENDIDIKAN_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = BukuAlatPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = BukuAlatPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = BukuAlatPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                BukuAlatPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Prasarana rows

                $key2 = PrasaranaPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = PrasaranaPeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = PrasaranaPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    PrasaranaPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj2 (Prasarana)
                $obj2->addBukuAlatRelatedByPrasaranaId($obj1);

            } // if joined row is not null

                // Add objects for joined Prasarana rows

                $key3 = PrasaranaPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = PrasaranaPeer::getInstanceFromPool($key3);
                    if (!$obj3) {
    
                        $cls = PrasaranaPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    PrasaranaPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj3 (Prasarana)
                $obj3->addBukuAlatRelatedByPrasaranaId($obj1);

            } // if joined row is not null

                // Add objects for joined Sekolah rows

                $key4 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = SekolahPeer::getInstanceFromPool($key4);
                    if (!$obj4) {
    
                        $cls = SekolahPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    SekolahPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj4 (Sekolah)
                $obj4->addBukuAlatRelatedBySekolahId($obj1);

            } // if joined row is not null

                // Add objects for joined Sekolah rows

                $key5 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = SekolahPeer::getInstanceFromPool($key5);
                    if (!$obj5) {
    
                        $cls = SekolahPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    SekolahPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj5 (Sekolah)
                $obj5->addBukuAlatRelatedBySekolahId($obj1);

            } // if joined row is not null

                // Add objects for joined JenisBukuAlat rows

                $key6 = JenisBukuAlatPeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = JenisBukuAlatPeer::getInstanceFromPool($key6);
                    if (!$obj6) {
    
                        $cls = JenisBukuAlatPeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    JenisBukuAlatPeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj6 (JenisBukuAlat)
                $obj6->addBukuAlatRelatedByJenisBukuAlatId($obj1);

            } // if joined row is not null

                // Add objects for joined JenisBukuAlat rows

                $key7 = JenisBukuAlatPeer::getPrimaryKeyHashFromRow($row, $startcol7);
                if ($key7 !== null) {
                    $obj7 = JenisBukuAlatPeer::getInstanceFromPool($key7);
                    if (!$obj7) {
    
                        $cls = JenisBukuAlatPeer::getOMClass();

                    $obj7 = new $cls();
                    $obj7->hydrate($row, $startcol7);
                    JenisBukuAlatPeer::addInstanceToPool($obj7, $key7);
                } // if $obj7 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj7 (JenisBukuAlat)
                $obj7->addBukuAlatRelatedByJenisBukuAlatId($obj1);

            } // if joined row is not null

                // Add objects for joined TingkatPendidikan rows

                $key8 = TingkatPendidikanPeer::getPrimaryKeyHashFromRow($row, $startcol8);
                if ($key8 !== null) {
                    $obj8 = TingkatPendidikanPeer::getInstanceFromPool($key8);
                    if (!$obj8) {
    
                        $cls = TingkatPendidikanPeer::getOMClass();

                    $obj8 = new $cls();
                    $obj8->hydrate($row, $startcol8);
                    TingkatPendidikanPeer::addInstanceToPool($obj8, $key8);
                } // if $obj8 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj8 (TingkatPendidikan)
                $obj8->addBukuAlatRelatedByTingkatPendidikanId($obj1);

            } // if joined row is not null

                // Add objects for joined TingkatPendidikan rows

                $key9 = TingkatPendidikanPeer::getPrimaryKeyHashFromRow($row, $startcol9);
                if ($key9 !== null) {
                    $obj9 = TingkatPendidikanPeer::getInstanceFromPool($key9);
                    if (!$obj9) {
    
                        $cls = TingkatPendidikanPeer::getOMClass();

                    $obj9 = new $cls();
                    $obj9->hydrate($row, $startcol9);
                    TingkatPendidikanPeer::addInstanceToPool($obj9, $key9);
                } // if $obj9 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj9 (TingkatPendidikan)
                $obj9->addBukuAlatRelatedByTingkatPendidikanId($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of BukuAlat objects pre-filled with all related objects except TingkatPendidikanRelatedByTingkatPendidikanId.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of BukuAlat objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptTingkatPendidikanRelatedByTingkatPendidikanId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);
        }

        BukuAlatPeer::addSelectColumns($criteria);
        $startcol2 = BukuAlatPeer::NUM_HYDRATE_COLUMNS;

        PrasaranaPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + PrasaranaPeer::NUM_HYDRATE_COLUMNS;

        PrasaranaPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + PrasaranaPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        JenisBukuAlatPeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + JenisBukuAlatPeer::NUM_HYDRATE_COLUMNS;

        JenisBukuAlatPeer::addSelectColumns($criteria);
        $startcol8 = $startcol7 + JenisBukuAlatPeer::NUM_HYDRATE_COLUMNS;

        MataPelajaranPeer::addSelectColumns($criteria);
        $startcol9 = $startcol8 + MataPelajaranPeer::NUM_HYDRATE_COLUMNS;

        MataPelajaranPeer::addSelectColumns($criteria);
        $startcol10 = $startcol9 + MataPelajaranPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(BukuAlatPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::JENIS_BUKU_ALAT_ID, JenisBukuAlatPeer::JENIS_BUKU_ALAT_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::JENIS_BUKU_ALAT_ID, JenisBukuAlatPeer::JENIS_BUKU_ALAT_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = BukuAlatPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = BukuAlatPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = BukuAlatPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                BukuAlatPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Prasarana rows

                $key2 = PrasaranaPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = PrasaranaPeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = PrasaranaPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    PrasaranaPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj2 (Prasarana)
                $obj2->addBukuAlatRelatedByPrasaranaId($obj1);

            } // if joined row is not null

                // Add objects for joined Prasarana rows

                $key3 = PrasaranaPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = PrasaranaPeer::getInstanceFromPool($key3);
                    if (!$obj3) {
    
                        $cls = PrasaranaPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    PrasaranaPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj3 (Prasarana)
                $obj3->addBukuAlatRelatedByPrasaranaId($obj1);

            } // if joined row is not null

                // Add objects for joined Sekolah rows

                $key4 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = SekolahPeer::getInstanceFromPool($key4);
                    if (!$obj4) {
    
                        $cls = SekolahPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    SekolahPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj4 (Sekolah)
                $obj4->addBukuAlatRelatedBySekolahId($obj1);

            } // if joined row is not null

                // Add objects for joined Sekolah rows

                $key5 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = SekolahPeer::getInstanceFromPool($key5);
                    if (!$obj5) {
    
                        $cls = SekolahPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    SekolahPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj5 (Sekolah)
                $obj5->addBukuAlatRelatedBySekolahId($obj1);

            } // if joined row is not null

                // Add objects for joined JenisBukuAlat rows

                $key6 = JenisBukuAlatPeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = JenisBukuAlatPeer::getInstanceFromPool($key6);
                    if (!$obj6) {
    
                        $cls = JenisBukuAlatPeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    JenisBukuAlatPeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj6 (JenisBukuAlat)
                $obj6->addBukuAlatRelatedByJenisBukuAlatId($obj1);

            } // if joined row is not null

                // Add objects for joined JenisBukuAlat rows

                $key7 = JenisBukuAlatPeer::getPrimaryKeyHashFromRow($row, $startcol7);
                if ($key7 !== null) {
                    $obj7 = JenisBukuAlatPeer::getInstanceFromPool($key7);
                    if (!$obj7) {
    
                        $cls = JenisBukuAlatPeer::getOMClass();

                    $obj7 = new $cls();
                    $obj7->hydrate($row, $startcol7);
                    JenisBukuAlatPeer::addInstanceToPool($obj7, $key7);
                } // if $obj7 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj7 (JenisBukuAlat)
                $obj7->addBukuAlatRelatedByJenisBukuAlatId($obj1);

            } // if joined row is not null

                // Add objects for joined MataPelajaran rows

                $key8 = MataPelajaranPeer::getPrimaryKeyHashFromRow($row, $startcol8);
                if ($key8 !== null) {
                    $obj8 = MataPelajaranPeer::getInstanceFromPool($key8);
                    if (!$obj8) {
    
                        $cls = MataPelajaranPeer::getOMClass();

                    $obj8 = new $cls();
                    $obj8->hydrate($row, $startcol8);
                    MataPelajaranPeer::addInstanceToPool($obj8, $key8);
                } // if $obj8 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj8 (MataPelajaran)
                $obj8->addBukuAlatRelatedByMataPelajaranId($obj1);

            } // if joined row is not null

                // Add objects for joined MataPelajaran rows

                $key9 = MataPelajaranPeer::getPrimaryKeyHashFromRow($row, $startcol9);
                if ($key9 !== null) {
                    $obj9 = MataPelajaranPeer::getInstanceFromPool($key9);
                    if (!$obj9) {
    
                        $cls = MataPelajaranPeer::getOMClass();

                    $obj9 = new $cls();
                    $obj9->hydrate($row, $startcol9);
                    MataPelajaranPeer::addInstanceToPool($obj9, $key9);
                } // if $obj9 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj9 (MataPelajaran)
                $obj9->addBukuAlatRelatedByMataPelajaranId($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of BukuAlat objects pre-filled with all related objects except TingkatPendidikanRelatedByTingkatPendidikanId.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of BukuAlat objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptTingkatPendidikanRelatedByTingkatPendidikanId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);
        }

        BukuAlatPeer::addSelectColumns($criteria);
        $startcol2 = BukuAlatPeer::NUM_HYDRATE_COLUMNS;

        PrasaranaPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + PrasaranaPeer::NUM_HYDRATE_COLUMNS;

        PrasaranaPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + PrasaranaPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        JenisBukuAlatPeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + JenisBukuAlatPeer::NUM_HYDRATE_COLUMNS;

        JenisBukuAlatPeer::addSelectColumns($criteria);
        $startcol8 = $startcol7 + JenisBukuAlatPeer::NUM_HYDRATE_COLUMNS;

        MataPelajaranPeer::addSelectColumns($criteria);
        $startcol9 = $startcol8 + MataPelajaranPeer::NUM_HYDRATE_COLUMNS;

        MataPelajaranPeer::addSelectColumns($criteria);
        $startcol10 = $startcol9 + MataPelajaranPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(BukuAlatPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::JENIS_BUKU_ALAT_ID, JenisBukuAlatPeer::JENIS_BUKU_ALAT_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::JENIS_BUKU_ALAT_ID, JenisBukuAlatPeer::JENIS_BUKU_ALAT_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

        $criteria->addJoin(BukuAlatPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = BukuAlatPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = BukuAlatPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = BukuAlatPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                BukuAlatPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Prasarana rows

                $key2 = PrasaranaPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = PrasaranaPeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = PrasaranaPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    PrasaranaPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj2 (Prasarana)
                $obj2->addBukuAlatRelatedByPrasaranaId($obj1);

            } // if joined row is not null

                // Add objects for joined Prasarana rows

                $key3 = PrasaranaPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = PrasaranaPeer::getInstanceFromPool($key3);
                    if (!$obj3) {
    
                        $cls = PrasaranaPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    PrasaranaPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj3 (Prasarana)
                $obj3->addBukuAlatRelatedByPrasaranaId($obj1);

            } // if joined row is not null

                // Add objects for joined Sekolah rows

                $key4 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = SekolahPeer::getInstanceFromPool($key4);
                    if (!$obj4) {
    
                        $cls = SekolahPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    SekolahPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj4 (Sekolah)
                $obj4->addBukuAlatRelatedBySekolahId($obj1);

            } // if joined row is not null

                // Add objects for joined Sekolah rows

                $key5 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = SekolahPeer::getInstanceFromPool($key5);
                    if (!$obj5) {
    
                        $cls = SekolahPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    SekolahPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj5 (Sekolah)
                $obj5->addBukuAlatRelatedBySekolahId($obj1);

            } // if joined row is not null

                // Add objects for joined JenisBukuAlat rows

                $key6 = JenisBukuAlatPeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = JenisBukuAlatPeer::getInstanceFromPool($key6);
                    if (!$obj6) {
    
                        $cls = JenisBukuAlatPeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    JenisBukuAlatPeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj6 (JenisBukuAlat)
                $obj6->addBukuAlatRelatedByJenisBukuAlatId($obj1);

            } // if joined row is not null

                // Add objects for joined JenisBukuAlat rows

                $key7 = JenisBukuAlatPeer::getPrimaryKeyHashFromRow($row, $startcol7);
                if ($key7 !== null) {
                    $obj7 = JenisBukuAlatPeer::getInstanceFromPool($key7);
                    if (!$obj7) {
    
                        $cls = JenisBukuAlatPeer::getOMClass();

                    $obj7 = new $cls();
                    $obj7->hydrate($row, $startcol7);
                    JenisBukuAlatPeer::addInstanceToPool($obj7, $key7);
                } // if $obj7 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj7 (JenisBukuAlat)
                $obj7->addBukuAlatRelatedByJenisBukuAlatId($obj1);

            } // if joined row is not null

                // Add objects for joined MataPelajaran rows

                $key8 = MataPelajaranPeer::getPrimaryKeyHashFromRow($row, $startcol8);
                if ($key8 !== null) {
                    $obj8 = MataPelajaranPeer::getInstanceFromPool($key8);
                    if (!$obj8) {
    
                        $cls = MataPelajaranPeer::getOMClass();

                    $obj8 = new $cls();
                    $obj8->hydrate($row, $startcol8);
                    MataPelajaranPeer::addInstanceToPool($obj8, $key8);
                } // if $obj8 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj8 (MataPelajaran)
                $obj8->addBukuAlatRelatedByMataPelajaranId($obj1);

            } // if joined row is not null

                // Add objects for joined MataPelajaran rows

                $key9 = MataPelajaranPeer::getPrimaryKeyHashFromRow($row, $startcol9);
                if ($key9 !== null) {
                    $obj9 = MataPelajaranPeer::getInstanceFromPool($key9);
                    if (!$obj9) {
    
                        $cls = MataPelajaranPeer::getOMClass();

                    $obj9 = new $cls();
                    $obj9->hydrate($row, $startcol9);
                    MataPelajaranPeer::addInstanceToPool($obj9, $key9);
                } // if $obj9 already loaded

                // Add the $obj1 (BukuAlat) to the collection in $obj9 (MataPelajaran)
                $obj9->addBukuAlatRelatedByMataPelajaranId($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(BukuAlatPeer::DATABASE_NAME)->getTable(BukuAlatPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseBukuAlatPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseBukuAlatPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new BukuAlatTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass()
    {
        return BukuAlatPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a BukuAlat or Criteria object.
     *
     * @param      mixed $values Criteria or BukuAlat object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(BukuAlatPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from BukuAlat object
        }


        // Set the correct dbName
        $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a BukuAlat or Criteria object.
     *
     * @param      mixed $values Criteria or BukuAlat object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(BukuAlatPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(BukuAlatPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(BukuAlatPeer::BUKU_ALAT_ID);
            $value = $criteria->remove(BukuAlatPeer::BUKU_ALAT_ID);
            if ($value) {
                $selectCriteria->add(BukuAlatPeer::BUKU_ALAT_ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(BukuAlatPeer::TABLE_NAME);
            }

        } else { // $values is BukuAlat object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the buku_alat table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(BukuAlatPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(BukuAlatPeer::TABLE_NAME, $con, BukuAlatPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            BukuAlatPeer::clearInstancePool();
            BukuAlatPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a BukuAlat or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or BukuAlat object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(BukuAlatPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            BukuAlatPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof BukuAlat) { // it's a model object
            // invalidate the cache for this single object
            BukuAlatPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(BukuAlatPeer::DATABASE_NAME);
            $criteria->add(BukuAlatPeer::BUKU_ALAT_ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                BukuAlatPeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(BukuAlatPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            
            $affectedRows += BasePeer::doDelete($criteria, $con);
            BukuAlatPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given BukuAlat object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      BukuAlat $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(BukuAlatPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(BukuAlatPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(BukuAlatPeer::DATABASE_NAME, BukuAlatPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      string $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return BukuAlat
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = BukuAlatPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(BukuAlatPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(BukuAlatPeer::DATABASE_NAME);
        $criteria->add(BukuAlatPeer::BUKU_ALAT_ID, $pk);

        $v = BukuAlatPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return BukuAlat[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(BukuAlatPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(BukuAlatPeer::DATABASE_NAME);
            $criteria->add(BukuAlatPeer::BUKU_ALAT_ID, $pks, Criteria::IN);
            $objs = BukuAlatPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseBukuAlatPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseBukuAlatPeer::buildTableMap();

