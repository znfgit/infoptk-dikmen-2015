<?php

namespace angulex\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelDateTime;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\Inpassing;
use angulex\Model\InpassingQuery;
use angulex\Model\PangkatGolongan;
use angulex\Model\PangkatGolonganPeer;
use angulex\Model\PangkatGolonganQuery;
use angulex\Model\Ptk;
use angulex\Model\PtkQuery;
use angulex\Model\RiwayatGajiBerkala;
use angulex\Model\RiwayatGajiBerkalaQuery;
use angulex\Model\RwyKepangkatan;
use angulex\Model\RwyKepangkatanQuery;

/**
 * Base class that represents a row from the 'ref.pangkat_golongan' table.
 *
 * 
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BasePangkatGolongan extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'angulex\\Model\\PangkatGolonganPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        PangkatGolonganPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the pangkat_golongan_id field.
     * @var        string
     */
    protected $pangkat_golongan_id;

    /**
     * The value for the kode field.
     * @var        string
     */
    protected $kode;

    /**
     * The value for the nama field.
     * @var        string
     */
    protected $nama;

    /**
     * The value for the create_date field.
     * @var        string
     */
    protected $create_date;

    /**
     * The value for the last_update field.
     * @var        string
     */
    protected $last_update;

    /**
     * The value for the expired_date field.
     * @var        string
     */
    protected $expired_date;

    /**
     * The value for the last_sync field.
     * @var        string
     */
    protected $last_sync;

    /**
     * @var        PropelObjectCollection|Inpassing[] Collection to store aggregation of Inpassing objects.
     */
    protected $collInpassingsRelatedByPangkatGolonganId;
    protected $collInpassingsRelatedByPangkatGolonganIdPartial;

    /**
     * @var        PropelObjectCollection|Inpassing[] Collection to store aggregation of Inpassing objects.
     */
    protected $collInpassingsRelatedByPangkatGolonganId;
    protected $collInpassingsRelatedByPangkatGolonganIdPartial;

    /**
     * @var        PropelObjectCollection|RwyKepangkatan[] Collection to store aggregation of RwyKepangkatan objects.
     */
    protected $collRwyKepangkatansRelatedByPangkatGolonganId;
    protected $collRwyKepangkatansRelatedByPangkatGolonganIdPartial;

    /**
     * @var        PropelObjectCollection|RwyKepangkatan[] Collection to store aggregation of RwyKepangkatan objects.
     */
    protected $collRwyKepangkatansRelatedByPangkatGolonganId;
    protected $collRwyKepangkatansRelatedByPangkatGolonganIdPartial;

    /**
     * @var        PropelObjectCollection|Ptk[] Collection to store aggregation of Ptk objects.
     */
    protected $collPtksRelatedByPangkatGolonganId;
    protected $collPtksRelatedByPangkatGolonganIdPartial;

    /**
     * @var        PropelObjectCollection|Ptk[] Collection to store aggregation of Ptk objects.
     */
    protected $collPtksRelatedByPangkatGolonganId;
    protected $collPtksRelatedByPangkatGolonganIdPartial;

    /**
     * @var        PropelObjectCollection|RiwayatGajiBerkala[] Collection to store aggregation of RiwayatGajiBerkala objects.
     */
    protected $collRiwayatGajiBerkalasRelatedByPangkatGolonganId;
    protected $collRiwayatGajiBerkalasRelatedByPangkatGolonganIdPartial;

    /**
     * @var        PropelObjectCollection|RiwayatGajiBerkala[] Collection to store aggregation of RiwayatGajiBerkala objects.
     */
    protected $collRiwayatGajiBerkalasRelatedByPangkatGolonganId;
    protected $collRiwayatGajiBerkalasRelatedByPangkatGolonganIdPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $inpassingsRelatedByPangkatGolonganIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $inpassingsRelatedByPangkatGolonganIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rwyKepangkatansRelatedByPangkatGolonganIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rwyKepangkatansRelatedByPangkatGolonganIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $ptksRelatedByPangkatGolonganIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $ptksRelatedByPangkatGolonganIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $riwayatGajiBerkalasRelatedByPangkatGolonganIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $riwayatGajiBerkalasRelatedByPangkatGolonganIdScheduledForDeletion = null;

    /**
     * Get the [pangkat_golongan_id] column value.
     * 
     * @return string
     */
    public function getPangkatGolonganId()
    {
        return $this->pangkat_golongan_id;
    }

    /**
     * Get the [kode] column value.
     * 
     * @return string
     */
    public function getKode()
    {
        return $this->kode;
    }

    /**
     * Get the [nama] column value.
     * 
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Get the [optionally formatted] temporal [create_date] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreateDate($format = 'Y-m-d H:i:s')
    {
        if ($this->create_date === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->create_date);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->create_date, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [last_update] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastUpdate($format = 'Y-m-d H:i:s')
    {
        if ($this->last_update === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_update);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_update, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [expired_date] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getExpiredDate($format = 'Y-m-d H:i:s')
    {
        if ($this->expired_date === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->expired_date);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->expired_date, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [last_sync] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastSync($format = 'Y-m-d H:i:s')
    {
        if ($this->last_sync === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_sync);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_sync, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Set the value of [pangkat_golongan_id] column.
     * 
     * @param string $v new value
     * @return PangkatGolongan The current object (for fluent API support)
     */
    public function setPangkatGolonganId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->pangkat_golongan_id !== $v) {
            $this->pangkat_golongan_id = $v;
            $this->modifiedColumns[] = PangkatGolonganPeer::PANGKAT_GOLONGAN_ID;
        }


        return $this;
    } // setPangkatGolonganId()

    /**
     * Set the value of [kode] column.
     * 
     * @param string $v new value
     * @return PangkatGolongan The current object (for fluent API support)
     */
    public function setKode($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->kode !== $v) {
            $this->kode = $v;
            $this->modifiedColumns[] = PangkatGolonganPeer::KODE;
        }


        return $this;
    } // setKode()

    /**
     * Set the value of [nama] column.
     * 
     * @param string $v new value
     * @return PangkatGolongan The current object (for fluent API support)
     */
    public function setNama($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nama !== $v) {
            $this->nama = $v;
            $this->modifiedColumns[] = PangkatGolonganPeer::NAMA;
        }


        return $this;
    } // setNama()

    /**
     * Sets the value of [create_date] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return PangkatGolongan The current object (for fluent API support)
     */
    public function setCreateDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->create_date !== null || $dt !== null) {
            $currentDateAsString = ($this->create_date !== null && $tmpDt = new DateTime($this->create_date)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->create_date = $newDateAsString;
                $this->modifiedColumns[] = PangkatGolonganPeer::CREATE_DATE;
            }
        } // if either are not null


        return $this;
    } // setCreateDate()

    /**
     * Sets the value of [last_update] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return PangkatGolongan The current object (for fluent API support)
     */
    public function setLastUpdate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_update !== null || $dt !== null) {
            $currentDateAsString = ($this->last_update !== null && $tmpDt = new DateTime($this->last_update)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_update = $newDateAsString;
                $this->modifiedColumns[] = PangkatGolonganPeer::LAST_UPDATE;
            }
        } // if either are not null


        return $this;
    } // setLastUpdate()

    /**
     * Sets the value of [expired_date] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return PangkatGolongan The current object (for fluent API support)
     */
    public function setExpiredDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->expired_date !== null || $dt !== null) {
            $currentDateAsString = ($this->expired_date !== null && $tmpDt = new DateTime($this->expired_date)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->expired_date = $newDateAsString;
                $this->modifiedColumns[] = PangkatGolonganPeer::EXPIRED_DATE;
            }
        } // if either are not null


        return $this;
    } // setExpiredDate()

    /**
     * Sets the value of [last_sync] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return PangkatGolongan The current object (for fluent API support)
     */
    public function setLastSync($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_sync !== null || $dt !== null) {
            $currentDateAsString = ($this->last_sync !== null && $tmpDt = new DateTime($this->last_sync)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_sync = $newDateAsString;
                $this->modifiedColumns[] = PangkatGolonganPeer::LAST_SYNC;
            }
        } // if either are not null


        return $this;
    } // setLastSync()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->pangkat_golongan_id = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
            $this->kode = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->nama = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->create_date = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->last_update = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->expired_date = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->last_sync = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 7; // 7 = PangkatGolonganPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating PangkatGolongan object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PangkatGolonganPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = PangkatGolonganPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collInpassingsRelatedByPangkatGolonganId = null;

            $this->collInpassingsRelatedByPangkatGolonganId = null;

            $this->collRwyKepangkatansRelatedByPangkatGolonganId = null;

            $this->collRwyKepangkatansRelatedByPangkatGolonganId = null;

            $this->collPtksRelatedByPangkatGolonganId = null;

            $this->collPtksRelatedByPangkatGolonganId = null;

            $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId = null;

            $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PangkatGolonganPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = PangkatGolonganQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PangkatGolonganPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                PangkatGolonganPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->inpassingsRelatedByPangkatGolonganIdScheduledForDeletion !== null) {
                if (!$this->inpassingsRelatedByPangkatGolonganIdScheduledForDeletion->isEmpty()) {
                    InpassingQuery::create()
                        ->filterByPrimaryKeys($this->inpassingsRelatedByPangkatGolonganIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->inpassingsRelatedByPangkatGolonganIdScheduledForDeletion = null;
                }
            }

            if ($this->collInpassingsRelatedByPangkatGolonganId !== null) {
                foreach ($this->collInpassingsRelatedByPangkatGolonganId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->inpassingsRelatedByPangkatGolonganIdScheduledForDeletion !== null) {
                if (!$this->inpassingsRelatedByPangkatGolonganIdScheduledForDeletion->isEmpty()) {
                    InpassingQuery::create()
                        ->filterByPrimaryKeys($this->inpassingsRelatedByPangkatGolonganIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->inpassingsRelatedByPangkatGolonganIdScheduledForDeletion = null;
                }
            }

            if ($this->collInpassingsRelatedByPangkatGolonganId !== null) {
                foreach ($this->collInpassingsRelatedByPangkatGolonganId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rwyKepangkatansRelatedByPangkatGolonganIdScheduledForDeletion !== null) {
                if (!$this->rwyKepangkatansRelatedByPangkatGolonganIdScheduledForDeletion->isEmpty()) {
                    RwyKepangkatanQuery::create()
                        ->filterByPrimaryKeys($this->rwyKepangkatansRelatedByPangkatGolonganIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rwyKepangkatansRelatedByPangkatGolonganIdScheduledForDeletion = null;
                }
            }

            if ($this->collRwyKepangkatansRelatedByPangkatGolonganId !== null) {
                foreach ($this->collRwyKepangkatansRelatedByPangkatGolonganId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rwyKepangkatansRelatedByPangkatGolonganIdScheduledForDeletion !== null) {
                if (!$this->rwyKepangkatansRelatedByPangkatGolonganIdScheduledForDeletion->isEmpty()) {
                    RwyKepangkatanQuery::create()
                        ->filterByPrimaryKeys($this->rwyKepangkatansRelatedByPangkatGolonganIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rwyKepangkatansRelatedByPangkatGolonganIdScheduledForDeletion = null;
                }
            }

            if ($this->collRwyKepangkatansRelatedByPangkatGolonganId !== null) {
                foreach ($this->collRwyKepangkatansRelatedByPangkatGolonganId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->ptksRelatedByPangkatGolonganIdScheduledForDeletion !== null) {
                if (!$this->ptksRelatedByPangkatGolonganIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->ptksRelatedByPangkatGolonganIdScheduledForDeletion as $ptkRelatedByPangkatGolonganId) {
                        // need to save related object because we set the relation to null
                        $ptkRelatedByPangkatGolonganId->save($con);
                    }
                    $this->ptksRelatedByPangkatGolonganIdScheduledForDeletion = null;
                }
            }

            if ($this->collPtksRelatedByPangkatGolonganId !== null) {
                foreach ($this->collPtksRelatedByPangkatGolonganId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->ptksRelatedByPangkatGolonganIdScheduledForDeletion !== null) {
                if (!$this->ptksRelatedByPangkatGolonganIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->ptksRelatedByPangkatGolonganIdScheduledForDeletion as $ptkRelatedByPangkatGolonganId) {
                        // need to save related object because we set the relation to null
                        $ptkRelatedByPangkatGolonganId->save($con);
                    }
                    $this->ptksRelatedByPangkatGolonganIdScheduledForDeletion = null;
                }
            }

            if ($this->collPtksRelatedByPangkatGolonganId !== null) {
                foreach ($this->collPtksRelatedByPangkatGolonganId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->riwayatGajiBerkalasRelatedByPangkatGolonganIdScheduledForDeletion !== null) {
                if (!$this->riwayatGajiBerkalasRelatedByPangkatGolonganIdScheduledForDeletion->isEmpty()) {
                    RiwayatGajiBerkalaQuery::create()
                        ->filterByPrimaryKeys($this->riwayatGajiBerkalasRelatedByPangkatGolonganIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->riwayatGajiBerkalasRelatedByPangkatGolonganIdScheduledForDeletion = null;
                }
            }

            if ($this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId !== null) {
                foreach ($this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->riwayatGajiBerkalasRelatedByPangkatGolonganIdScheduledForDeletion !== null) {
                if (!$this->riwayatGajiBerkalasRelatedByPangkatGolonganIdScheduledForDeletion->isEmpty()) {
                    RiwayatGajiBerkalaQuery::create()
                        ->filterByPrimaryKeys($this->riwayatGajiBerkalasRelatedByPangkatGolonganIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->riwayatGajiBerkalasRelatedByPangkatGolonganIdScheduledForDeletion = null;
                }
            }

            if ($this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId !== null) {
                foreach ($this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $criteria = $this->buildCriteria();
        $pk = BasePeer::doInsert($criteria, $con);
        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = PangkatGolonganPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collInpassingsRelatedByPangkatGolonganId !== null) {
                    foreach ($this->collInpassingsRelatedByPangkatGolonganId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collInpassingsRelatedByPangkatGolonganId !== null) {
                    foreach ($this->collInpassingsRelatedByPangkatGolonganId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRwyKepangkatansRelatedByPangkatGolonganId !== null) {
                    foreach ($this->collRwyKepangkatansRelatedByPangkatGolonganId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRwyKepangkatansRelatedByPangkatGolonganId !== null) {
                    foreach ($this->collRwyKepangkatansRelatedByPangkatGolonganId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPtksRelatedByPangkatGolonganId !== null) {
                    foreach ($this->collPtksRelatedByPangkatGolonganId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPtksRelatedByPangkatGolonganId !== null) {
                    foreach ($this->collPtksRelatedByPangkatGolonganId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId !== null) {
                    foreach ($this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId !== null) {
                    foreach ($this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = PangkatGolonganPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getPangkatGolonganId();
                break;
            case 1:
                return $this->getKode();
                break;
            case 2:
                return $this->getNama();
                break;
            case 3:
                return $this->getCreateDate();
                break;
            case 4:
                return $this->getLastUpdate();
                break;
            case 5:
                return $this->getExpiredDate();
                break;
            case 6:
                return $this->getLastSync();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['PangkatGolongan'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['PangkatGolongan'][$this->getPrimaryKey()] = true;
        $keys = PangkatGolonganPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getPangkatGolonganId(),
            $keys[1] => $this->getKode(),
            $keys[2] => $this->getNama(),
            $keys[3] => $this->getCreateDate(),
            $keys[4] => $this->getLastUpdate(),
            $keys[5] => $this->getExpiredDate(),
            $keys[6] => $this->getLastSync(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->collInpassingsRelatedByPangkatGolonganId) {
                $result['InpassingsRelatedByPangkatGolonganId'] = $this->collInpassingsRelatedByPangkatGolonganId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collInpassingsRelatedByPangkatGolonganId) {
                $result['InpassingsRelatedByPangkatGolonganId'] = $this->collInpassingsRelatedByPangkatGolonganId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRwyKepangkatansRelatedByPangkatGolonganId) {
                $result['RwyKepangkatansRelatedByPangkatGolonganId'] = $this->collRwyKepangkatansRelatedByPangkatGolonganId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRwyKepangkatansRelatedByPangkatGolonganId) {
                $result['RwyKepangkatansRelatedByPangkatGolonganId'] = $this->collRwyKepangkatansRelatedByPangkatGolonganId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPtksRelatedByPangkatGolonganId) {
                $result['PtksRelatedByPangkatGolonganId'] = $this->collPtksRelatedByPangkatGolonganId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPtksRelatedByPangkatGolonganId) {
                $result['PtksRelatedByPangkatGolonganId'] = $this->collPtksRelatedByPangkatGolonganId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId) {
                $result['RiwayatGajiBerkalasRelatedByPangkatGolonganId'] = $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId) {
                $result['RiwayatGajiBerkalasRelatedByPangkatGolonganId'] = $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = PangkatGolonganPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setPangkatGolonganId($value);
                break;
            case 1:
                $this->setKode($value);
                break;
            case 2:
                $this->setNama($value);
                break;
            case 3:
                $this->setCreateDate($value);
                break;
            case 4:
                $this->setLastUpdate($value);
                break;
            case 5:
                $this->setExpiredDate($value);
                break;
            case 6:
                $this->setLastSync($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = PangkatGolonganPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setPangkatGolonganId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setKode($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setNama($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setCreateDate($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setLastUpdate($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setExpiredDate($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setLastSync($arr[$keys[6]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(PangkatGolonganPeer::DATABASE_NAME);

        if ($this->isColumnModified(PangkatGolonganPeer::PANGKAT_GOLONGAN_ID)) $criteria->add(PangkatGolonganPeer::PANGKAT_GOLONGAN_ID, $this->pangkat_golongan_id);
        if ($this->isColumnModified(PangkatGolonganPeer::KODE)) $criteria->add(PangkatGolonganPeer::KODE, $this->kode);
        if ($this->isColumnModified(PangkatGolonganPeer::NAMA)) $criteria->add(PangkatGolonganPeer::NAMA, $this->nama);
        if ($this->isColumnModified(PangkatGolonganPeer::CREATE_DATE)) $criteria->add(PangkatGolonganPeer::CREATE_DATE, $this->create_date);
        if ($this->isColumnModified(PangkatGolonganPeer::LAST_UPDATE)) $criteria->add(PangkatGolonganPeer::LAST_UPDATE, $this->last_update);
        if ($this->isColumnModified(PangkatGolonganPeer::EXPIRED_DATE)) $criteria->add(PangkatGolonganPeer::EXPIRED_DATE, $this->expired_date);
        if ($this->isColumnModified(PangkatGolonganPeer::LAST_SYNC)) $criteria->add(PangkatGolonganPeer::LAST_SYNC, $this->last_sync);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(PangkatGolonganPeer::DATABASE_NAME);
        $criteria->add(PangkatGolonganPeer::PANGKAT_GOLONGAN_ID, $this->pangkat_golongan_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getPangkatGolonganId();
    }

    /**
     * Generic method to set the primary key (pangkat_golongan_id column).
     *
     * @param  string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setPangkatGolonganId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getPangkatGolonganId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of PangkatGolongan (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setKode($this->getKode());
        $copyObj->setNama($this->getNama());
        $copyObj->setCreateDate($this->getCreateDate());
        $copyObj->setLastUpdate($this->getLastUpdate());
        $copyObj->setExpiredDate($this->getExpiredDate());
        $copyObj->setLastSync($this->getLastSync());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getInpassingsRelatedByPangkatGolonganId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addInpassingRelatedByPangkatGolonganId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getInpassingsRelatedByPangkatGolonganId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addInpassingRelatedByPangkatGolonganId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRwyKepangkatansRelatedByPangkatGolonganId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRwyKepangkatanRelatedByPangkatGolonganId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRwyKepangkatansRelatedByPangkatGolonganId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRwyKepangkatanRelatedByPangkatGolonganId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPtksRelatedByPangkatGolonganId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPtkRelatedByPangkatGolonganId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPtksRelatedByPangkatGolonganId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPtkRelatedByPangkatGolonganId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRiwayatGajiBerkalasRelatedByPangkatGolonganId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRiwayatGajiBerkalaRelatedByPangkatGolonganId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRiwayatGajiBerkalasRelatedByPangkatGolonganId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRiwayatGajiBerkalaRelatedByPangkatGolonganId($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setPangkatGolonganId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return PangkatGolongan Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return PangkatGolonganPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new PangkatGolonganPeer();
        }

        return self::$peer;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('InpassingRelatedByPangkatGolonganId' == $relationName) {
            $this->initInpassingsRelatedByPangkatGolonganId();
        }
        if ('InpassingRelatedByPangkatGolonganId' == $relationName) {
            $this->initInpassingsRelatedByPangkatGolonganId();
        }
        if ('RwyKepangkatanRelatedByPangkatGolonganId' == $relationName) {
            $this->initRwyKepangkatansRelatedByPangkatGolonganId();
        }
        if ('RwyKepangkatanRelatedByPangkatGolonganId' == $relationName) {
            $this->initRwyKepangkatansRelatedByPangkatGolonganId();
        }
        if ('PtkRelatedByPangkatGolonganId' == $relationName) {
            $this->initPtksRelatedByPangkatGolonganId();
        }
        if ('PtkRelatedByPangkatGolonganId' == $relationName) {
            $this->initPtksRelatedByPangkatGolonganId();
        }
        if ('RiwayatGajiBerkalaRelatedByPangkatGolonganId' == $relationName) {
            $this->initRiwayatGajiBerkalasRelatedByPangkatGolonganId();
        }
        if ('RiwayatGajiBerkalaRelatedByPangkatGolonganId' == $relationName) {
            $this->initRiwayatGajiBerkalasRelatedByPangkatGolonganId();
        }
    }

    /**
     * Clears out the collInpassingsRelatedByPangkatGolonganId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return PangkatGolongan The current object (for fluent API support)
     * @see        addInpassingsRelatedByPangkatGolonganId()
     */
    public function clearInpassingsRelatedByPangkatGolonganId()
    {
        $this->collInpassingsRelatedByPangkatGolonganId = null; // important to set this to null since that means it is uninitialized
        $this->collInpassingsRelatedByPangkatGolonganIdPartial = null;

        return $this;
    }

    /**
     * reset is the collInpassingsRelatedByPangkatGolonganId collection loaded partially
     *
     * @return void
     */
    public function resetPartialInpassingsRelatedByPangkatGolonganId($v = true)
    {
        $this->collInpassingsRelatedByPangkatGolonganIdPartial = $v;
    }

    /**
     * Initializes the collInpassingsRelatedByPangkatGolonganId collection.
     *
     * By default this just sets the collInpassingsRelatedByPangkatGolonganId collection to an empty array (like clearcollInpassingsRelatedByPangkatGolonganId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initInpassingsRelatedByPangkatGolonganId($overrideExisting = true)
    {
        if (null !== $this->collInpassingsRelatedByPangkatGolonganId && !$overrideExisting) {
            return;
        }
        $this->collInpassingsRelatedByPangkatGolonganId = new PropelObjectCollection();
        $this->collInpassingsRelatedByPangkatGolonganId->setModel('Inpassing');
    }

    /**
     * Gets an array of Inpassing objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this PangkatGolongan is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Inpassing[] List of Inpassing objects
     * @throws PropelException
     */
    public function getInpassingsRelatedByPangkatGolonganId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collInpassingsRelatedByPangkatGolonganIdPartial && !$this->isNew();
        if (null === $this->collInpassingsRelatedByPangkatGolonganId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collInpassingsRelatedByPangkatGolonganId) {
                // return empty collection
                $this->initInpassingsRelatedByPangkatGolonganId();
            } else {
                $collInpassingsRelatedByPangkatGolonganId = InpassingQuery::create(null, $criteria)
                    ->filterByPangkatGolonganRelatedByPangkatGolonganId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collInpassingsRelatedByPangkatGolonganIdPartial && count($collInpassingsRelatedByPangkatGolonganId)) {
                      $this->initInpassingsRelatedByPangkatGolonganId(false);

                      foreach($collInpassingsRelatedByPangkatGolonganId as $obj) {
                        if (false == $this->collInpassingsRelatedByPangkatGolonganId->contains($obj)) {
                          $this->collInpassingsRelatedByPangkatGolonganId->append($obj);
                        }
                      }

                      $this->collInpassingsRelatedByPangkatGolonganIdPartial = true;
                    }

                    $collInpassingsRelatedByPangkatGolonganId->getInternalIterator()->rewind();
                    return $collInpassingsRelatedByPangkatGolonganId;
                }

                if($partial && $this->collInpassingsRelatedByPangkatGolonganId) {
                    foreach($this->collInpassingsRelatedByPangkatGolonganId as $obj) {
                        if($obj->isNew()) {
                            $collInpassingsRelatedByPangkatGolonganId[] = $obj;
                        }
                    }
                }

                $this->collInpassingsRelatedByPangkatGolonganId = $collInpassingsRelatedByPangkatGolonganId;
                $this->collInpassingsRelatedByPangkatGolonganIdPartial = false;
            }
        }

        return $this->collInpassingsRelatedByPangkatGolonganId;
    }

    /**
     * Sets a collection of InpassingRelatedByPangkatGolonganId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $inpassingsRelatedByPangkatGolonganId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return PangkatGolongan The current object (for fluent API support)
     */
    public function setInpassingsRelatedByPangkatGolonganId(PropelCollection $inpassingsRelatedByPangkatGolonganId, PropelPDO $con = null)
    {
        $inpassingsRelatedByPangkatGolonganIdToDelete = $this->getInpassingsRelatedByPangkatGolonganId(new Criteria(), $con)->diff($inpassingsRelatedByPangkatGolonganId);

        $this->inpassingsRelatedByPangkatGolonganIdScheduledForDeletion = unserialize(serialize($inpassingsRelatedByPangkatGolonganIdToDelete));

        foreach ($inpassingsRelatedByPangkatGolonganIdToDelete as $inpassingRelatedByPangkatGolonganIdRemoved) {
            $inpassingRelatedByPangkatGolonganIdRemoved->setPangkatGolonganRelatedByPangkatGolonganId(null);
        }

        $this->collInpassingsRelatedByPangkatGolonganId = null;
        foreach ($inpassingsRelatedByPangkatGolonganId as $inpassingRelatedByPangkatGolonganId) {
            $this->addInpassingRelatedByPangkatGolonganId($inpassingRelatedByPangkatGolonganId);
        }

        $this->collInpassingsRelatedByPangkatGolonganId = $inpassingsRelatedByPangkatGolonganId;
        $this->collInpassingsRelatedByPangkatGolonganIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Inpassing objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Inpassing objects.
     * @throws PropelException
     */
    public function countInpassingsRelatedByPangkatGolonganId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collInpassingsRelatedByPangkatGolonganIdPartial && !$this->isNew();
        if (null === $this->collInpassingsRelatedByPangkatGolonganId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collInpassingsRelatedByPangkatGolonganId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getInpassingsRelatedByPangkatGolonganId());
            }
            $query = InpassingQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPangkatGolonganRelatedByPangkatGolonganId($this)
                ->count($con);
        }

        return count($this->collInpassingsRelatedByPangkatGolonganId);
    }

    /**
     * Method called to associate a Inpassing object to this object
     * through the Inpassing foreign key attribute.
     *
     * @param    Inpassing $l Inpassing
     * @return PangkatGolongan The current object (for fluent API support)
     */
    public function addInpassingRelatedByPangkatGolonganId(Inpassing $l)
    {
        if ($this->collInpassingsRelatedByPangkatGolonganId === null) {
            $this->initInpassingsRelatedByPangkatGolonganId();
            $this->collInpassingsRelatedByPangkatGolonganIdPartial = true;
        }
        if (!in_array($l, $this->collInpassingsRelatedByPangkatGolonganId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddInpassingRelatedByPangkatGolonganId($l);
        }

        return $this;
    }

    /**
     * @param	InpassingRelatedByPangkatGolonganId $inpassingRelatedByPangkatGolonganId The inpassingRelatedByPangkatGolonganId object to add.
     */
    protected function doAddInpassingRelatedByPangkatGolonganId($inpassingRelatedByPangkatGolonganId)
    {
        $this->collInpassingsRelatedByPangkatGolonganId[]= $inpassingRelatedByPangkatGolonganId;
        $inpassingRelatedByPangkatGolonganId->setPangkatGolonganRelatedByPangkatGolonganId($this);
    }

    /**
     * @param	InpassingRelatedByPangkatGolonganId $inpassingRelatedByPangkatGolonganId The inpassingRelatedByPangkatGolonganId object to remove.
     * @return PangkatGolongan The current object (for fluent API support)
     */
    public function removeInpassingRelatedByPangkatGolonganId($inpassingRelatedByPangkatGolonganId)
    {
        if ($this->getInpassingsRelatedByPangkatGolonganId()->contains($inpassingRelatedByPangkatGolonganId)) {
            $this->collInpassingsRelatedByPangkatGolonganId->remove($this->collInpassingsRelatedByPangkatGolonganId->search($inpassingRelatedByPangkatGolonganId));
            if (null === $this->inpassingsRelatedByPangkatGolonganIdScheduledForDeletion) {
                $this->inpassingsRelatedByPangkatGolonganIdScheduledForDeletion = clone $this->collInpassingsRelatedByPangkatGolonganId;
                $this->inpassingsRelatedByPangkatGolonganIdScheduledForDeletion->clear();
            }
            $this->inpassingsRelatedByPangkatGolonganIdScheduledForDeletion[]= clone $inpassingRelatedByPangkatGolonganId;
            $inpassingRelatedByPangkatGolonganId->setPangkatGolonganRelatedByPangkatGolonganId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related InpassingsRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Inpassing[] List of Inpassing objects
     */
    public function getInpassingsRelatedByPangkatGolonganIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = InpassingQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getInpassingsRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related InpassingsRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Inpassing[] List of Inpassing objects
     */
    public function getInpassingsRelatedByPangkatGolonganIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = InpassingQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getInpassingsRelatedByPangkatGolonganId($query, $con);
    }

    /**
     * Clears out the collInpassingsRelatedByPangkatGolonganId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return PangkatGolongan The current object (for fluent API support)
     * @see        addInpassingsRelatedByPangkatGolonganId()
     */
    public function clearInpassingsRelatedByPangkatGolonganId()
    {
        $this->collInpassingsRelatedByPangkatGolonganId = null; // important to set this to null since that means it is uninitialized
        $this->collInpassingsRelatedByPangkatGolonganIdPartial = null;

        return $this;
    }

    /**
     * reset is the collInpassingsRelatedByPangkatGolonganId collection loaded partially
     *
     * @return void
     */
    public function resetPartialInpassingsRelatedByPangkatGolonganId($v = true)
    {
        $this->collInpassingsRelatedByPangkatGolonganIdPartial = $v;
    }

    /**
     * Initializes the collInpassingsRelatedByPangkatGolonganId collection.
     *
     * By default this just sets the collInpassingsRelatedByPangkatGolonganId collection to an empty array (like clearcollInpassingsRelatedByPangkatGolonganId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initInpassingsRelatedByPangkatGolonganId($overrideExisting = true)
    {
        if (null !== $this->collInpassingsRelatedByPangkatGolonganId && !$overrideExisting) {
            return;
        }
        $this->collInpassingsRelatedByPangkatGolonganId = new PropelObjectCollection();
        $this->collInpassingsRelatedByPangkatGolonganId->setModel('Inpassing');
    }

    /**
     * Gets an array of Inpassing objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this PangkatGolongan is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Inpassing[] List of Inpassing objects
     * @throws PropelException
     */
    public function getInpassingsRelatedByPangkatGolonganId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collInpassingsRelatedByPangkatGolonganIdPartial && !$this->isNew();
        if (null === $this->collInpassingsRelatedByPangkatGolonganId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collInpassingsRelatedByPangkatGolonganId) {
                // return empty collection
                $this->initInpassingsRelatedByPangkatGolonganId();
            } else {
                $collInpassingsRelatedByPangkatGolonganId = InpassingQuery::create(null, $criteria)
                    ->filterByPangkatGolonganRelatedByPangkatGolonganId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collInpassingsRelatedByPangkatGolonganIdPartial && count($collInpassingsRelatedByPangkatGolonganId)) {
                      $this->initInpassingsRelatedByPangkatGolonganId(false);

                      foreach($collInpassingsRelatedByPangkatGolonganId as $obj) {
                        if (false == $this->collInpassingsRelatedByPangkatGolonganId->contains($obj)) {
                          $this->collInpassingsRelatedByPangkatGolonganId->append($obj);
                        }
                      }

                      $this->collInpassingsRelatedByPangkatGolonganIdPartial = true;
                    }

                    $collInpassingsRelatedByPangkatGolonganId->getInternalIterator()->rewind();
                    return $collInpassingsRelatedByPangkatGolonganId;
                }

                if($partial && $this->collInpassingsRelatedByPangkatGolonganId) {
                    foreach($this->collInpassingsRelatedByPangkatGolonganId as $obj) {
                        if($obj->isNew()) {
                            $collInpassingsRelatedByPangkatGolonganId[] = $obj;
                        }
                    }
                }

                $this->collInpassingsRelatedByPangkatGolonganId = $collInpassingsRelatedByPangkatGolonganId;
                $this->collInpassingsRelatedByPangkatGolonganIdPartial = false;
            }
        }

        return $this->collInpassingsRelatedByPangkatGolonganId;
    }

    /**
     * Sets a collection of InpassingRelatedByPangkatGolonganId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $inpassingsRelatedByPangkatGolonganId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return PangkatGolongan The current object (for fluent API support)
     */
    public function setInpassingsRelatedByPangkatGolonganId(PropelCollection $inpassingsRelatedByPangkatGolonganId, PropelPDO $con = null)
    {
        $inpassingsRelatedByPangkatGolonganIdToDelete = $this->getInpassingsRelatedByPangkatGolonganId(new Criteria(), $con)->diff($inpassingsRelatedByPangkatGolonganId);

        $this->inpassingsRelatedByPangkatGolonganIdScheduledForDeletion = unserialize(serialize($inpassingsRelatedByPangkatGolonganIdToDelete));

        foreach ($inpassingsRelatedByPangkatGolonganIdToDelete as $inpassingRelatedByPangkatGolonganIdRemoved) {
            $inpassingRelatedByPangkatGolonganIdRemoved->setPangkatGolonganRelatedByPangkatGolonganId(null);
        }

        $this->collInpassingsRelatedByPangkatGolonganId = null;
        foreach ($inpassingsRelatedByPangkatGolonganId as $inpassingRelatedByPangkatGolonganId) {
            $this->addInpassingRelatedByPangkatGolonganId($inpassingRelatedByPangkatGolonganId);
        }

        $this->collInpassingsRelatedByPangkatGolonganId = $inpassingsRelatedByPangkatGolonganId;
        $this->collInpassingsRelatedByPangkatGolonganIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Inpassing objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Inpassing objects.
     * @throws PropelException
     */
    public function countInpassingsRelatedByPangkatGolonganId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collInpassingsRelatedByPangkatGolonganIdPartial && !$this->isNew();
        if (null === $this->collInpassingsRelatedByPangkatGolonganId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collInpassingsRelatedByPangkatGolonganId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getInpassingsRelatedByPangkatGolonganId());
            }
            $query = InpassingQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPangkatGolonganRelatedByPangkatGolonganId($this)
                ->count($con);
        }

        return count($this->collInpassingsRelatedByPangkatGolonganId);
    }

    /**
     * Method called to associate a Inpassing object to this object
     * through the Inpassing foreign key attribute.
     *
     * @param    Inpassing $l Inpassing
     * @return PangkatGolongan The current object (for fluent API support)
     */
    public function addInpassingRelatedByPangkatGolonganId(Inpassing $l)
    {
        if ($this->collInpassingsRelatedByPangkatGolonganId === null) {
            $this->initInpassingsRelatedByPangkatGolonganId();
            $this->collInpassingsRelatedByPangkatGolonganIdPartial = true;
        }
        if (!in_array($l, $this->collInpassingsRelatedByPangkatGolonganId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddInpassingRelatedByPangkatGolonganId($l);
        }

        return $this;
    }

    /**
     * @param	InpassingRelatedByPangkatGolonganId $inpassingRelatedByPangkatGolonganId The inpassingRelatedByPangkatGolonganId object to add.
     */
    protected function doAddInpassingRelatedByPangkatGolonganId($inpassingRelatedByPangkatGolonganId)
    {
        $this->collInpassingsRelatedByPangkatGolonganId[]= $inpassingRelatedByPangkatGolonganId;
        $inpassingRelatedByPangkatGolonganId->setPangkatGolonganRelatedByPangkatGolonganId($this);
    }

    /**
     * @param	InpassingRelatedByPangkatGolonganId $inpassingRelatedByPangkatGolonganId The inpassingRelatedByPangkatGolonganId object to remove.
     * @return PangkatGolongan The current object (for fluent API support)
     */
    public function removeInpassingRelatedByPangkatGolonganId($inpassingRelatedByPangkatGolonganId)
    {
        if ($this->getInpassingsRelatedByPangkatGolonganId()->contains($inpassingRelatedByPangkatGolonganId)) {
            $this->collInpassingsRelatedByPangkatGolonganId->remove($this->collInpassingsRelatedByPangkatGolonganId->search($inpassingRelatedByPangkatGolonganId));
            if (null === $this->inpassingsRelatedByPangkatGolonganIdScheduledForDeletion) {
                $this->inpassingsRelatedByPangkatGolonganIdScheduledForDeletion = clone $this->collInpassingsRelatedByPangkatGolonganId;
                $this->inpassingsRelatedByPangkatGolonganIdScheduledForDeletion->clear();
            }
            $this->inpassingsRelatedByPangkatGolonganIdScheduledForDeletion[]= clone $inpassingRelatedByPangkatGolonganId;
            $inpassingRelatedByPangkatGolonganId->setPangkatGolonganRelatedByPangkatGolonganId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related InpassingsRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Inpassing[] List of Inpassing objects
     */
    public function getInpassingsRelatedByPangkatGolonganIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = InpassingQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getInpassingsRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related InpassingsRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Inpassing[] List of Inpassing objects
     */
    public function getInpassingsRelatedByPangkatGolonganIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = InpassingQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getInpassingsRelatedByPangkatGolonganId($query, $con);
    }

    /**
     * Clears out the collRwyKepangkatansRelatedByPangkatGolonganId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return PangkatGolongan The current object (for fluent API support)
     * @see        addRwyKepangkatansRelatedByPangkatGolonganId()
     */
    public function clearRwyKepangkatansRelatedByPangkatGolonganId()
    {
        $this->collRwyKepangkatansRelatedByPangkatGolonganId = null; // important to set this to null since that means it is uninitialized
        $this->collRwyKepangkatansRelatedByPangkatGolonganIdPartial = null;

        return $this;
    }

    /**
     * reset is the collRwyKepangkatansRelatedByPangkatGolonganId collection loaded partially
     *
     * @return void
     */
    public function resetPartialRwyKepangkatansRelatedByPangkatGolonganId($v = true)
    {
        $this->collRwyKepangkatansRelatedByPangkatGolonganIdPartial = $v;
    }

    /**
     * Initializes the collRwyKepangkatansRelatedByPangkatGolonganId collection.
     *
     * By default this just sets the collRwyKepangkatansRelatedByPangkatGolonganId collection to an empty array (like clearcollRwyKepangkatansRelatedByPangkatGolonganId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRwyKepangkatansRelatedByPangkatGolonganId($overrideExisting = true)
    {
        if (null !== $this->collRwyKepangkatansRelatedByPangkatGolonganId && !$overrideExisting) {
            return;
        }
        $this->collRwyKepangkatansRelatedByPangkatGolonganId = new PropelObjectCollection();
        $this->collRwyKepangkatansRelatedByPangkatGolonganId->setModel('RwyKepangkatan');
    }

    /**
     * Gets an array of RwyKepangkatan objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this PangkatGolongan is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RwyKepangkatan[] List of RwyKepangkatan objects
     * @throws PropelException
     */
    public function getRwyKepangkatansRelatedByPangkatGolonganId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRwyKepangkatansRelatedByPangkatGolonganIdPartial && !$this->isNew();
        if (null === $this->collRwyKepangkatansRelatedByPangkatGolonganId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRwyKepangkatansRelatedByPangkatGolonganId) {
                // return empty collection
                $this->initRwyKepangkatansRelatedByPangkatGolonganId();
            } else {
                $collRwyKepangkatansRelatedByPangkatGolonganId = RwyKepangkatanQuery::create(null, $criteria)
                    ->filterByPangkatGolonganRelatedByPangkatGolonganId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRwyKepangkatansRelatedByPangkatGolonganIdPartial && count($collRwyKepangkatansRelatedByPangkatGolonganId)) {
                      $this->initRwyKepangkatansRelatedByPangkatGolonganId(false);

                      foreach($collRwyKepangkatansRelatedByPangkatGolonganId as $obj) {
                        if (false == $this->collRwyKepangkatansRelatedByPangkatGolonganId->contains($obj)) {
                          $this->collRwyKepangkatansRelatedByPangkatGolonganId->append($obj);
                        }
                      }

                      $this->collRwyKepangkatansRelatedByPangkatGolonganIdPartial = true;
                    }

                    $collRwyKepangkatansRelatedByPangkatGolonganId->getInternalIterator()->rewind();
                    return $collRwyKepangkatansRelatedByPangkatGolonganId;
                }

                if($partial && $this->collRwyKepangkatansRelatedByPangkatGolonganId) {
                    foreach($this->collRwyKepangkatansRelatedByPangkatGolonganId as $obj) {
                        if($obj->isNew()) {
                            $collRwyKepangkatansRelatedByPangkatGolonganId[] = $obj;
                        }
                    }
                }

                $this->collRwyKepangkatansRelatedByPangkatGolonganId = $collRwyKepangkatansRelatedByPangkatGolonganId;
                $this->collRwyKepangkatansRelatedByPangkatGolonganIdPartial = false;
            }
        }

        return $this->collRwyKepangkatansRelatedByPangkatGolonganId;
    }

    /**
     * Sets a collection of RwyKepangkatanRelatedByPangkatGolonganId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rwyKepangkatansRelatedByPangkatGolonganId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return PangkatGolongan The current object (for fluent API support)
     */
    public function setRwyKepangkatansRelatedByPangkatGolonganId(PropelCollection $rwyKepangkatansRelatedByPangkatGolonganId, PropelPDO $con = null)
    {
        $rwyKepangkatansRelatedByPangkatGolonganIdToDelete = $this->getRwyKepangkatansRelatedByPangkatGolonganId(new Criteria(), $con)->diff($rwyKepangkatansRelatedByPangkatGolonganId);

        $this->rwyKepangkatansRelatedByPangkatGolonganIdScheduledForDeletion = unserialize(serialize($rwyKepangkatansRelatedByPangkatGolonganIdToDelete));

        foreach ($rwyKepangkatansRelatedByPangkatGolonganIdToDelete as $rwyKepangkatanRelatedByPangkatGolonganIdRemoved) {
            $rwyKepangkatanRelatedByPangkatGolonganIdRemoved->setPangkatGolonganRelatedByPangkatGolonganId(null);
        }

        $this->collRwyKepangkatansRelatedByPangkatGolonganId = null;
        foreach ($rwyKepangkatansRelatedByPangkatGolonganId as $rwyKepangkatanRelatedByPangkatGolonganId) {
            $this->addRwyKepangkatanRelatedByPangkatGolonganId($rwyKepangkatanRelatedByPangkatGolonganId);
        }

        $this->collRwyKepangkatansRelatedByPangkatGolonganId = $rwyKepangkatansRelatedByPangkatGolonganId;
        $this->collRwyKepangkatansRelatedByPangkatGolonganIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RwyKepangkatan objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RwyKepangkatan objects.
     * @throws PropelException
     */
    public function countRwyKepangkatansRelatedByPangkatGolonganId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRwyKepangkatansRelatedByPangkatGolonganIdPartial && !$this->isNew();
        if (null === $this->collRwyKepangkatansRelatedByPangkatGolonganId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRwyKepangkatansRelatedByPangkatGolonganId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getRwyKepangkatansRelatedByPangkatGolonganId());
            }
            $query = RwyKepangkatanQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPangkatGolonganRelatedByPangkatGolonganId($this)
                ->count($con);
        }

        return count($this->collRwyKepangkatansRelatedByPangkatGolonganId);
    }

    /**
     * Method called to associate a RwyKepangkatan object to this object
     * through the RwyKepangkatan foreign key attribute.
     *
     * @param    RwyKepangkatan $l RwyKepangkatan
     * @return PangkatGolongan The current object (for fluent API support)
     */
    public function addRwyKepangkatanRelatedByPangkatGolonganId(RwyKepangkatan $l)
    {
        if ($this->collRwyKepangkatansRelatedByPangkatGolonganId === null) {
            $this->initRwyKepangkatansRelatedByPangkatGolonganId();
            $this->collRwyKepangkatansRelatedByPangkatGolonganIdPartial = true;
        }
        if (!in_array($l, $this->collRwyKepangkatansRelatedByPangkatGolonganId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRwyKepangkatanRelatedByPangkatGolonganId($l);
        }

        return $this;
    }

    /**
     * @param	RwyKepangkatanRelatedByPangkatGolonganId $rwyKepangkatanRelatedByPangkatGolonganId The rwyKepangkatanRelatedByPangkatGolonganId object to add.
     */
    protected function doAddRwyKepangkatanRelatedByPangkatGolonganId($rwyKepangkatanRelatedByPangkatGolonganId)
    {
        $this->collRwyKepangkatansRelatedByPangkatGolonganId[]= $rwyKepangkatanRelatedByPangkatGolonganId;
        $rwyKepangkatanRelatedByPangkatGolonganId->setPangkatGolonganRelatedByPangkatGolonganId($this);
    }

    /**
     * @param	RwyKepangkatanRelatedByPangkatGolonganId $rwyKepangkatanRelatedByPangkatGolonganId The rwyKepangkatanRelatedByPangkatGolonganId object to remove.
     * @return PangkatGolongan The current object (for fluent API support)
     */
    public function removeRwyKepangkatanRelatedByPangkatGolonganId($rwyKepangkatanRelatedByPangkatGolonganId)
    {
        if ($this->getRwyKepangkatansRelatedByPangkatGolonganId()->contains($rwyKepangkatanRelatedByPangkatGolonganId)) {
            $this->collRwyKepangkatansRelatedByPangkatGolonganId->remove($this->collRwyKepangkatansRelatedByPangkatGolonganId->search($rwyKepangkatanRelatedByPangkatGolonganId));
            if (null === $this->rwyKepangkatansRelatedByPangkatGolonganIdScheduledForDeletion) {
                $this->rwyKepangkatansRelatedByPangkatGolonganIdScheduledForDeletion = clone $this->collRwyKepangkatansRelatedByPangkatGolonganId;
                $this->rwyKepangkatansRelatedByPangkatGolonganIdScheduledForDeletion->clear();
            }
            $this->rwyKepangkatansRelatedByPangkatGolonganIdScheduledForDeletion[]= clone $rwyKepangkatanRelatedByPangkatGolonganId;
            $rwyKepangkatanRelatedByPangkatGolonganId->setPangkatGolonganRelatedByPangkatGolonganId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related RwyKepangkatansRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwyKepangkatan[] List of RwyKepangkatan objects
     */
    public function getRwyKepangkatansRelatedByPangkatGolonganIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwyKepangkatanQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getRwyKepangkatansRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related RwyKepangkatansRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwyKepangkatan[] List of RwyKepangkatan objects
     */
    public function getRwyKepangkatansRelatedByPangkatGolonganIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwyKepangkatanQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getRwyKepangkatansRelatedByPangkatGolonganId($query, $con);
    }

    /**
     * Clears out the collRwyKepangkatansRelatedByPangkatGolonganId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return PangkatGolongan The current object (for fluent API support)
     * @see        addRwyKepangkatansRelatedByPangkatGolonganId()
     */
    public function clearRwyKepangkatansRelatedByPangkatGolonganId()
    {
        $this->collRwyKepangkatansRelatedByPangkatGolonganId = null; // important to set this to null since that means it is uninitialized
        $this->collRwyKepangkatansRelatedByPangkatGolonganIdPartial = null;

        return $this;
    }

    /**
     * reset is the collRwyKepangkatansRelatedByPangkatGolonganId collection loaded partially
     *
     * @return void
     */
    public function resetPartialRwyKepangkatansRelatedByPangkatGolonganId($v = true)
    {
        $this->collRwyKepangkatansRelatedByPangkatGolonganIdPartial = $v;
    }

    /**
     * Initializes the collRwyKepangkatansRelatedByPangkatGolonganId collection.
     *
     * By default this just sets the collRwyKepangkatansRelatedByPangkatGolonganId collection to an empty array (like clearcollRwyKepangkatansRelatedByPangkatGolonganId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRwyKepangkatansRelatedByPangkatGolonganId($overrideExisting = true)
    {
        if (null !== $this->collRwyKepangkatansRelatedByPangkatGolonganId && !$overrideExisting) {
            return;
        }
        $this->collRwyKepangkatansRelatedByPangkatGolonganId = new PropelObjectCollection();
        $this->collRwyKepangkatansRelatedByPangkatGolonganId->setModel('RwyKepangkatan');
    }

    /**
     * Gets an array of RwyKepangkatan objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this PangkatGolongan is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RwyKepangkatan[] List of RwyKepangkatan objects
     * @throws PropelException
     */
    public function getRwyKepangkatansRelatedByPangkatGolonganId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRwyKepangkatansRelatedByPangkatGolonganIdPartial && !$this->isNew();
        if (null === $this->collRwyKepangkatansRelatedByPangkatGolonganId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRwyKepangkatansRelatedByPangkatGolonganId) {
                // return empty collection
                $this->initRwyKepangkatansRelatedByPangkatGolonganId();
            } else {
                $collRwyKepangkatansRelatedByPangkatGolonganId = RwyKepangkatanQuery::create(null, $criteria)
                    ->filterByPangkatGolonganRelatedByPangkatGolonganId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRwyKepangkatansRelatedByPangkatGolonganIdPartial && count($collRwyKepangkatansRelatedByPangkatGolonganId)) {
                      $this->initRwyKepangkatansRelatedByPangkatGolonganId(false);

                      foreach($collRwyKepangkatansRelatedByPangkatGolonganId as $obj) {
                        if (false == $this->collRwyKepangkatansRelatedByPangkatGolonganId->contains($obj)) {
                          $this->collRwyKepangkatansRelatedByPangkatGolonganId->append($obj);
                        }
                      }

                      $this->collRwyKepangkatansRelatedByPangkatGolonganIdPartial = true;
                    }

                    $collRwyKepangkatansRelatedByPangkatGolonganId->getInternalIterator()->rewind();
                    return $collRwyKepangkatansRelatedByPangkatGolonganId;
                }

                if($partial && $this->collRwyKepangkatansRelatedByPangkatGolonganId) {
                    foreach($this->collRwyKepangkatansRelatedByPangkatGolonganId as $obj) {
                        if($obj->isNew()) {
                            $collRwyKepangkatansRelatedByPangkatGolonganId[] = $obj;
                        }
                    }
                }

                $this->collRwyKepangkatansRelatedByPangkatGolonganId = $collRwyKepangkatansRelatedByPangkatGolonganId;
                $this->collRwyKepangkatansRelatedByPangkatGolonganIdPartial = false;
            }
        }

        return $this->collRwyKepangkatansRelatedByPangkatGolonganId;
    }

    /**
     * Sets a collection of RwyKepangkatanRelatedByPangkatGolonganId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rwyKepangkatansRelatedByPangkatGolonganId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return PangkatGolongan The current object (for fluent API support)
     */
    public function setRwyKepangkatansRelatedByPangkatGolonganId(PropelCollection $rwyKepangkatansRelatedByPangkatGolonganId, PropelPDO $con = null)
    {
        $rwyKepangkatansRelatedByPangkatGolonganIdToDelete = $this->getRwyKepangkatansRelatedByPangkatGolonganId(new Criteria(), $con)->diff($rwyKepangkatansRelatedByPangkatGolonganId);

        $this->rwyKepangkatansRelatedByPangkatGolonganIdScheduledForDeletion = unserialize(serialize($rwyKepangkatansRelatedByPangkatGolonganIdToDelete));

        foreach ($rwyKepangkatansRelatedByPangkatGolonganIdToDelete as $rwyKepangkatanRelatedByPangkatGolonganIdRemoved) {
            $rwyKepangkatanRelatedByPangkatGolonganIdRemoved->setPangkatGolonganRelatedByPangkatGolonganId(null);
        }

        $this->collRwyKepangkatansRelatedByPangkatGolonganId = null;
        foreach ($rwyKepangkatansRelatedByPangkatGolonganId as $rwyKepangkatanRelatedByPangkatGolonganId) {
            $this->addRwyKepangkatanRelatedByPangkatGolonganId($rwyKepangkatanRelatedByPangkatGolonganId);
        }

        $this->collRwyKepangkatansRelatedByPangkatGolonganId = $rwyKepangkatansRelatedByPangkatGolonganId;
        $this->collRwyKepangkatansRelatedByPangkatGolonganIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RwyKepangkatan objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RwyKepangkatan objects.
     * @throws PropelException
     */
    public function countRwyKepangkatansRelatedByPangkatGolonganId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRwyKepangkatansRelatedByPangkatGolonganIdPartial && !$this->isNew();
        if (null === $this->collRwyKepangkatansRelatedByPangkatGolonganId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRwyKepangkatansRelatedByPangkatGolonganId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getRwyKepangkatansRelatedByPangkatGolonganId());
            }
            $query = RwyKepangkatanQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPangkatGolonganRelatedByPangkatGolonganId($this)
                ->count($con);
        }

        return count($this->collRwyKepangkatansRelatedByPangkatGolonganId);
    }

    /**
     * Method called to associate a RwyKepangkatan object to this object
     * through the RwyKepangkatan foreign key attribute.
     *
     * @param    RwyKepangkatan $l RwyKepangkatan
     * @return PangkatGolongan The current object (for fluent API support)
     */
    public function addRwyKepangkatanRelatedByPangkatGolonganId(RwyKepangkatan $l)
    {
        if ($this->collRwyKepangkatansRelatedByPangkatGolonganId === null) {
            $this->initRwyKepangkatansRelatedByPangkatGolonganId();
            $this->collRwyKepangkatansRelatedByPangkatGolonganIdPartial = true;
        }
        if (!in_array($l, $this->collRwyKepangkatansRelatedByPangkatGolonganId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRwyKepangkatanRelatedByPangkatGolonganId($l);
        }

        return $this;
    }

    /**
     * @param	RwyKepangkatanRelatedByPangkatGolonganId $rwyKepangkatanRelatedByPangkatGolonganId The rwyKepangkatanRelatedByPangkatGolonganId object to add.
     */
    protected function doAddRwyKepangkatanRelatedByPangkatGolonganId($rwyKepangkatanRelatedByPangkatGolonganId)
    {
        $this->collRwyKepangkatansRelatedByPangkatGolonganId[]= $rwyKepangkatanRelatedByPangkatGolonganId;
        $rwyKepangkatanRelatedByPangkatGolonganId->setPangkatGolonganRelatedByPangkatGolonganId($this);
    }

    /**
     * @param	RwyKepangkatanRelatedByPangkatGolonganId $rwyKepangkatanRelatedByPangkatGolonganId The rwyKepangkatanRelatedByPangkatGolonganId object to remove.
     * @return PangkatGolongan The current object (for fluent API support)
     */
    public function removeRwyKepangkatanRelatedByPangkatGolonganId($rwyKepangkatanRelatedByPangkatGolonganId)
    {
        if ($this->getRwyKepangkatansRelatedByPangkatGolonganId()->contains($rwyKepangkatanRelatedByPangkatGolonganId)) {
            $this->collRwyKepangkatansRelatedByPangkatGolonganId->remove($this->collRwyKepangkatansRelatedByPangkatGolonganId->search($rwyKepangkatanRelatedByPangkatGolonganId));
            if (null === $this->rwyKepangkatansRelatedByPangkatGolonganIdScheduledForDeletion) {
                $this->rwyKepangkatansRelatedByPangkatGolonganIdScheduledForDeletion = clone $this->collRwyKepangkatansRelatedByPangkatGolonganId;
                $this->rwyKepangkatansRelatedByPangkatGolonganIdScheduledForDeletion->clear();
            }
            $this->rwyKepangkatansRelatedByPangkatGolonganIdScheduledForDeletion[]= clone $rwyKepangkatanRelatedByPangkatGolonganId;
            $rwyKepangkatanRelatedByPangkatGolonganId->setPangkatGolonganRelatedByPangkatGolonganId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related RwyKepangkatansRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwyKepangkatan[] List of RwyKepangkatan objects
     */
    public function getRwyKepangkatansRelatedByPangkatGolonganIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwyKepangkatanQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getRwyKepangkatansRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related RwyKepangkatansRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwyKepangkatan[] List of RwyKepangkatan objects
     */
    public function getRwyKepangkatansRelatedByPangkatGolonganIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwyKepangkatanQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getRwyKepangkatansRelatedByPangkatGolonganId($query, $con);
    }

    /**
     * Clears out the collPtksRelatedByPangkatGolonganId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return PangkatGolongan The current object (for fluent API support)
     * @see        addPtksRelatedByPangkatGolonganId()
     */
    public function clearPtksRelatedByPangkatGolonganId()
    {
        $this->collPtksRelatedByPangkatGolonganId = null; // important to set this to null since that means it is uninitialized
        $this->collPtksRelatedByPangkatGolonganIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPtksRelatedByPangkatGolonganId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPtksRelatedByPangkatGolonganId($v = true)
    {
        $this->collPtksRelatedByPangkatGolonganIdPartial = $v;
    }

    /**
     * Initializes the collPtksRelatedByPangkatGolonganId collection.
     *
     * By default this just sets the collPtksRelatedByPangkatGolonganId collection to an empty array (like clearcollPtksRelatedByPangkatGolonganId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPtksRelatedByPangkatGolonganId($overrideExisting = true)
    {
        if (null !== $this->collPtksRelatedByPangkatGolonganId && !$overrideExisting) {
            return;
        }
        $this->collPtksRelatedByPangkatGolonganId = new PropelObjectCollection();
        $this->collPtksRelatedByPangkatGolonganId->setModel('Ptk');
    }

    /**
     * Gets an array of Ptk objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this PangkatGolongan is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     * @throws PropelException
     */
    public function getPtksRelatedByPangkatGolonganId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPtksRelatedByPangkatGolonganIdPartial && !$this->isNew();
        if (null === $this->collPtksRelatedByPangkatGolonganId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPtksRelatedByPangkatGolonganId) {
                // return empty collection
                $this->initPtksRelatedByPangkatGolonganId();
            } else {
                $collPtksRelatedByPangkatGolonganId = PtkQuery::create(null, $criteria)
                    ->filterByPangkatGolonganRelatedByPangkatGolonganId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPtksRelatedByPangkatGolonganIdPartial && count($collPtksRelatedByPangkatGolonganId)) {
                      $this->initPtksRelatedByPangkatGolonganId(false);

                      foreach($collPtksRelatedByPangkatGolonganId as $obj) {
                        if (false == $this->collPtksRelatedByPangkatGolonganId->contains($obj)) {
                          $this->collPtksRelatedByPangkatGolonganId->append($obj);
                        }
                      }

                      $this->collPtksRelatedByPangkatGolonganIdPartial = true;
                    }

                    $collPtksRelatedByPangkatGolonganId->getInternalIterator()->rewind();
                    return $collPtksRelatedByPangkatGolonganId;
                }

                if($partial && $this->collPtksRelatedByPangkatGolonganId) {
                    foreach($this->collPtksRelatedByPangkatGolonganId as $obj) {
                        if($obj->isNew()) {
                            $collPtksRelatedByPangkatGolonganId[] = $obj;
                        }
                    }
                }

                $this->collPtksRelatedByPangkatGolonganId = $collPtksRelatedByPangkatGolonganId;
                $this->collPtksRelatedByPangkatGolonganIdPartial = false;
            }
        }

        return $this->collPtksRelatedByPangkatGolonganId;
    }

    /**
     * Sets a collection of PtkRelatedByPangkatGolonganId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $ptksRelatedByPangkatGolonganId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return PangkatGolongan The current object (for fluent API support)
     */
    public function setPtksRelatedByPangkatGolonganId(PropelCollection $ptksRelatedByPangkatGolonganId, PropelPDO $con = null)
    {
        $ptksRelatedByPangkatGolonganIdToDelete = $this->getPtksRelatedByPangkatGolonganId(new Criteria(), $con)->diff($ptksRelatedByPangkatGolonganId);

        $this->ptksRelatedByPangkatGolonganIdScheduledForDeletion = unserialize(serialize($ptksRelatedByPangkatGolonganIdToDelete));

        foreach ($ptksRelatedByPangkatGolonganIdToDelete as $ptkRelatedByPangkatGolonganIdRemoved) {
            $ptkRelatedByPangkatGolonganIdRemoved->setPangkatGolonganRelatedByPangkatGolonganId(null);
        }

        $this->collPtksRelatedByPangkatGolonganId = null;
        foreach ($ptksRelatedByPangkatGolonganId as $ptkRelatedByPangkatGolonganId) {
            $this->addPtkRelatedByPangkatGolonganId($ptkRelatedByPangkatGolonganId);
        }

        $this->collPtksRelatedByPangkatGolonganId = $ptksRelatedByPangkatGolonganId;
        $this->collPtksRelatedByPangkatGolonganIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Ptk objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Ptk objects.
     * @throws PropelException
     */
    public function countPtksRelatedByPangkatGolonganId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPtksRelatedByPangkatGolonganIdPartial && !$this->isNew();
        if (null === $this->collPtksRelatedByPangkatGolonganId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPtksRelatedByPangkatGolonganId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPtksRelatedByPangkatGolonganId());
            }
            $query = PtkQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPangkatGolonganRelatedByPangkatGolonganId($this)
                ->count($con);
        }

        return count($this->collPtksRelatedByPangkatGolonganId);
    }

    /**
     * Method called to associate a Ptk object to this object
     * through the Ptk foreign key attribute.
     *
     * @param    Ptk $l Ptk
     * @return PangkatGolongan The current object (for fluent API support)
     */
    public function addPtkRelatedByPangkatGolonganId(Ptk $l)
    {
        if ($this->collPtksRelatedByPangkatGolonganId === null) {
            $this->initPtksRelatedByPangkatGolonganId();
            $this->collPtksRelatedByPangkatGolonganIdPartial = true;
        }
        if (!in_array($l, $this->collPtksRelatedByPangkatGolonganId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPtkRelatedByPangkatGolonganId($l);
        }

        return $this;
    }

    /**
     * @param	PtkRelatedByPangkatGolonganId $ptkRelatedByPangkatGolonganId The ptkRelatedByPangkatGolonganId object to add.
     */
    protected function doAddPtkRelatedByPangkatGolonganId($ptkRelatedByPangkatGolonganId)
    {
        $this->collPtksRelatedByPangkatGolonganId[]= $ptkRelatedByPangkatGolonganId;
        $ptkRelatedByPangkatGolonganId->setPangkatGolonganRelatedByPangkatGolonganId($this);
    }

    /**
     * @param	PtkRelatedByPangkatGolonganId $ptkRelatedByPangkatGolonganId The ptkRelatedByPangkatGolonganId object to remove.
     * @return PangkatGolongan The current object (for fluent API support)
     */
    public function removePtkRelatedByPangkatGolonganId($ptkRelatedByPangkatGolonganId)
    {
        if ($this->getPtksRelatedByPangkatGolonganId()->contains($ptkRelatedByPangkatGolonganId)) {
            $this->collPtksRelatedByPangkatGolonganId->remove($this->collPtksRelatedByPangkatGolonganId->search($ptkRelatedByPangkatGolonganId));
            if (null === $this->ptksRelatedByPangkatGolonganIdScheduledForDeletion) {
                $this->ptksRelatedByPangkatGolonganIdScheduledForDeletion = clone $this->collPtksRelatedByPangkatGolonganId;
                $this->ptksRelatedByPangkatGolonganIdScheduledForDeletion->clear();
            }
            $this->ptksRelatedByPangkatGolonganIdScheduledForDeletion[]= $ptkRelatedByPangkatGolonganId;
            $ptkRelatedByPangkatGolonganId->setPangkatGolonganRelatedByPangkatGolonganId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinSekolahRelatedByEntrySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedByEntrySekolahId', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinSekolahRelatedByEntrySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedByEntrySekolahId', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinAgamaRelatedByAgamaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('AgamaRelatedByAgamaId', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinAgamaRelatedByAgamaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('AgamaRelatedByAgamaId', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinBidangStudiRelatedByPengawasBidangStudiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('BidangStudiRelatedByPengawasBidangStudiId', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinBidangStudiRelatedByPengawasBidangStudiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('BidangStudiRelatedByPengawasBidangStudiId', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinJenisPtkRelatedByJenisPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('JenisPtkRelatedByJenisPtkId', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinJenisPtkRelatedByJenisPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('JenisPtkRelatedByJenisPtkId', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('KeahlianLaboratoriumRelatedByKeahlianLaboratoriumId', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('KeahlianLaboratoriumRelatedByKeahlianLaboratoriumId', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinKebutuhanKhususRelatedByMampuHandleKk($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByMampuHandleKk', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinKebutuhanKhususRelatedByMampuHandleKk($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByMampuHandleKk', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinLembagaPengangkatRelatedByLembagaPengangkatId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('LembagaPengangkatRelatedByLembagaPengangkatId', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinLembagaPengangkatRelatedByLembagaPengangkatId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('LembagaPengangkatRelatedByLembagaPengangkatId', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinMstWilayahRelatedByKodeWilayah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('MstWilayahRelatedByKodeWilayah', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinMstWilayahRelatedByKodeWilayah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('MstWilayahRelatedByKodeWilayah', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinNegaraRelatedByKewarganegaraan($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('NegaraRelatedByKewarganegaraan', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinNegaraRelatedByKewarganegaraan($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('NegaraRelatedByKewarganegaraan', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinPekerjaanRelatedByPekerjaanSuamiIstri($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('PekerjaanRelatedByPekerjaanSuamiIstri', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinPekerjaanRelatedByPekerjaanSuamiIstri($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('PekerjaanRelatedByPekerjaanSuamiIstri', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinStatusKepegawaianRelatedByStatusKepegawaianId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('StatusKepegawaianRelatedByStatusKepegawaianId', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinStatusKepegawaianRelatedByStatusKepegawaianId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('StatusKepegawaianRelatedByStatusKepegawaianId', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinStatusKeaktifanPegawaiRelatedByStatusKeaktifanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('StatusKeaktifanPegawaiRelatedByStatusKeaktifanId', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinStatusKeaktifanPegawaiRelatedByStatusKeaktifanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('StatusKeaktifanPegawaiRelatedByStatusKeaktifanId', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinSumberGajiRelatedBySumberGajiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('SumberGajiRelatedBySumberGajiId', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinSumberGajiRelatedBySumberGajiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('SumberGajiRelatedBySumberGajiId', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }

    /**
     * Clears out the collPtksRelatedByPangkatGolonganId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return PangkatGolongan The current object (for fluent API support)
     * @see        addPtksRelatedByPangkatGolonganId()
     */
    public function clearPtksRelatedByPangkatGolonganId()
    {
        $this->collPtksRelatedByPangkatGolonganId = null; // important to set this to null since that means it is uninitialized
        $this->collPtksRelatedByPangkatGolonganIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPtksRelatedByPangkatGolonganId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPtksRelatedByPangkatGolonganId($v = true)
    {
        $this->collPtksRelatedByPangkatGolonganIdPartial = $v;
    }

    /**
     * Initializes the collPtksRelatedByPangkatGolonganId collection.
     *
     * By default this just sets the collPtksRelatedByPangkatGolonganId collection to an empty array (like clearcollPtksRelatedByPangkatGolonganId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPtksRelatedByPangkatGolonganId($overrideExisting = true)
    {
        if (null !== $this->collPtksRelatedByPangkatGolonganId && !$overrideExisting) {
            return;
        }
        $this->collPtksRelatedByPangkatGolonganId = new PropelObjectCollection();
        $this->collPtksRelatedByPangkatGolonganId->setModel('Ptk');
    }

    /**
     * Gets an array of Ptk objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this PangkatGolongan is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     * @throws PropelException
     */
    public function getPtksRelatedByPangkatGolonganId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPtksRelatedByPangkatGolonganIdPartial && !$this->isNew();
        if (null === $this->collPtksRelatedByPangkatGolonganId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPtksRelatedByPangkatGolonganId) {
                // return empty collection
                $this->initPtksRelatedByPangkatGolonganId();
            } else {
                $collPtksRelatedByPangkatGolonganId = PtkQuery::create(null, $criteria)
                    ->filterByPangkatGolonganRelatedByPangkatGolonganId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPtksRelatedByPangkatGolonganIdPartial && count($collPtksRelatedByPangkatGolonganId)) {
                      $this->initPtksRelatedByPangkatGolonganId(false);

                      foreach($collPtksRelatedByPangkatGolonganId as $obj) {
                        if (false == $this->collPtksRelatedByPangkatGolonganId->contains($obj)) {
                          $this->collPtksRelatedByPangkatGolonganId->append($obj);
                        }
                      }

                      $this->collPtksRelatedByPangkatGolonganIdPartial = true;
                    }

                    $collPtksRelatedByPangkatGolonganId->getInternalIterator()->rewind();
                    return $collPtksRelatedByPangkatGolonganId;
                }

                if($partial && $this->collPtksRelatedByPangkatGolonganId) {
                    foreach($this->collPtksRelatedByPangkatGolonganId as $obj) {
                        if($obj->isNew()) {
                            $collPtksRelatedByPangkatGolonganId[] = $obj;
                        }
                    }
                }

                $this->collPtksRelatedByPangkatGolonganId = $collPtksRelatedByPangkatGolonganId;
                $this->collPtksRelatedByPangkatGolonganIdPartial = false;
            }
        }

        return $this->collPtksRelatedByPangkatGolonganId;
    }

    /**
     * Sets a collection of PtkRelatedByPangkatGolonganId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $ptksRelatedByPangkatGolonganId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return PangkatGolongan The current object (for fluent API support)
     */
    public function setPtksRelatedByPangkatGolonganId(PropelCollection $ptksRelatedByPangkatGolonganId, PropelPDO $con = null)
    {
        $ptksRelatedByPangkatGolonganIdToDelete = $this->getPtksRelatedByPangkatGolonganId(new Criteria(), $con)->diff($ptksRelatedByPangkatGolonganId);

        $this->ptksRelatedByPangkatGolonganIdScheduledForDeletion = unserialize(serialize($ptksRelatedByPangkatGolonganIdToDelete));

        foreach ($ptksRelatedByPangkatGolonganIdToDelete as $ptkRelatedByPangkatGolonganIdRemoved) {
            $ptkRelatedByPangkatGolonganIdRemoved->setPangkatGolonganRelatedByPangkatGolonganId(null);
        }

        $this->collPtksRelatedByPangkatGolonganId = null;
        foreach ($ptksRelatedByPangkatGolonganId as $ptkRelatedByPangkatGolonganId) {
            $this->addPtkRelatedByPangkatGolonganId($ptkRelatedByPangkatGolonganId);
        }

        $this->collPtksRelatedByPangkatGolonganId = $ptksRelatedByPangkatGolonganId;
        $this->collPtksRelatedByPangkatGolonganIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Ptk objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Ptk objects.
     * @throws PropelException
     */
    public function countPtksRelatedByPangkatGolonganId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPtksRelatedByPangkatGolonganIdPartial && !$this->isNew();
        if (null === $this->collPtksRelatedByPangkatGolonganId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPtksRelatedByPangkatGolonganId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPtksRelatedByPangkatGolonganId());
            }
            $query = PtkQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPangkatGolonganRelatedByPangkatGolonganId($this)
                ->count($con);
        }

        return count($this->collPtksRelatedByPangkatGolonganId);
    }

    /**
     * Method called to associate a Ptk object to this object
     * through the Ptk foreign key attribute.
     *
     * @param    Ptk $l Ptk
     * @return PangkatGolongan The current object (for fluent API support)
     */
    public function addPtkRelatedByPangkatGolonganId(Ptk $l)
    {
        if ($this->collPtksRelatedByPangkatGolonganId === null) {
            $this->initPtksRelatedByPangkatGolonganId();
            $this->collPtksRelatedByPangkatGolonganIdPartial = true;
        }
        if (!in_array($l, $this->collPtksRelatedByPangkatGolonganId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPtkRelatedByPangkatGolonganId($l);
        }

        return $this;
    }

    /**
     * @param	PtkRelatedByPangkatGolonganId $ptkRelatedByPangkatGolonganId The ptkRelatedByPangkatGolonganId object to add.
     */
    protected function doAddPtkRelatedByPangkatGolonganId($ptkRelatedByPangkatGolonganId)
    {
        $this->collPtksRelatedByPangkatGolonganId[]= $ptkRelatedByPangkatGolonganId;
        $ptkRelatedByPangkatGolonganId->setPangkatGolonganRelatedByPangkatGolonganId($this);
    }

    /**
     * @param	PtkRelatedByPangkatGolonganId $ptkRelatedByPangkatGolonganId The ptkRelatedByPangkatGolonganId object to remove.
     * @return PangkatGolongan The current object (for fluent API support)
     */
    public function removePtkRelatedByPangkatGolonganId($ptkRelatedByPangkatGolonganId)
    {
        if ($this->getPtksRelatedByPangkatGolonganId()->contains($ptkRelatedByPangkatGolonganId)) {
            $this->collPtksRelatedByPangkatGolonganId->remove($this->collPtksRelatedByPangkatGolonganId->search($ptkRelatedByPangkatGolonganId));
            if (null === $this->ptksRelatedByPangkatGolonganIdScheduledForDeletion) {
                $this->ptksRelatedByPangkatGolonganIdScheduledForDeletion = clone $this->collPtksRelatedByPangkatGolonganId;
                $this->ptksRelatedByPangkatGolonganIdScheduledForDeletion->clear();
            }
            $this->ptksRelatedByPangkatGolonganIdScheduledForDeletion[]= $ptkRelatedByPangkatGolonganId;
            $ptkRelatedByPangkatGolonganId->setPangkatGolonganRelatedByPangkatGolonganId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinSekolahRelatedByEntrySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedByEntrySekolahId', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinSekolahRelatedByEntrySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedByEntrySekolahId', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinAgamaRelatedByAgamaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('AgamaRelatedByAgamaId', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinAgamaRelatedByAgamaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('AgamaRelatedByAgamaId', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinBidangStudiRelatedByPengawasBidangStudiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('BidangStudiRelatedByPengawasBidangStudiId', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinBidangStudiRelatedByPengawasBidangStudiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('BidangStudiRelatedByPengawasBidangStudiId', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinJenisPtkRelatedByJenisPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('JenisPtkRelatedByJenisPtkId', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinJenisPtkRelatedByJenisPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('JenisPtkRelatedByJenisPtkId', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('KeahlianLaboratoriumRelatedByKeahlianLaboratoriumId', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('KeahlianLaboratoriumRelatedByKeahlianLaboratoriumId', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinKebutuhanKhususRelatedByMampuHandleKk($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByMampuHandleKk', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinKebutuhanKhususRelatedByMampuHandleKk($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByMampuHandleKk', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinLembagaPengangkatRelatedByLembagaPengangkatId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('LembagaPengangkatRelatedByLembagaPengangkatId', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinLembagaPengangkatRelatedByLembagaPengangkatId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('LembagaPengangkatRelatedByLembagaPengangkatId', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinMstWilayahRelatedByKodeWilayah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('MstWilayahRelatedByKodeWilayah', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinMstWilayahRelatedByKodeWilayah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('MstWilayahRelatedByKodeWilayah', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinNegaraRelatedByKewarganegaraan($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('NegaraRelatedByKewarganegaraan', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinNegaraRelatedByKewarganegaraan($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('NegaraRelatedByKewarganegaraan', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinPekerjaanRelatedByPekerjaanSuamiIstri($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('PekerjaanRelatedByPekerjaanSuamiIstri', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinPekerjaanRelatedByPekerjaanSuamiIstri($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('PekerjaanRelatedByPekerjaanSuamiIstri', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinStatusKepegawaianRelatedByStatusKepegawaianId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('StatusKepegawaianRelatedByStatusKepegawaianId', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinStatusKepegawaianRelatedByStatusKepegawaianId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('StatusKepegawaianRelatedByStatusKepegawaianId', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinStatusKeaktifanPegawaiRelatedByStatusKeaktifanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('StatusKeaktifanPegawaiRelatedByStatusKeaktifanId', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinStatusKeaktifanPegawaiRelatedByStatusKeaktifanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('StatusKeaktifanPegawaiRelatedByStatusKeaktifanId', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinSumberGajiRelatedBySumberGajiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('SumberGajiRelatedBySumberGajiId', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related PtksRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPangkatGolonganIdJoinSumberGajiRelatedBySumberGajiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('SumberGajiRelatedBySumberGajiId', $join_behavior);

        return $this->getPtksRelatedByPangkatGolonganId($query, $con);
    }

    /**
     * Clears out the collRiwayatGajiBerkalasRelatedByPangkatGolonganId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return PangkatGolongan The current object (for fluent API support)
     * @see        addRiwayatGajiBerkalasRelatedByPangkatGolonganId()
     */
    public function clearRiwayatGajiBerkalasRelatedByPangkatGolonganId()
    {
        $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId = null; // important to set this to null since that means it is uninitialized
        $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganIdPartial = null;

        return $this;
    }

    /**
     * reset is the collRiwayatGajiBerkalasRelatedByPangkatGolonganId collection loaded partially
     *
     * @return void
     */
    public function resetPartialRiwayatGajiBerkalasRelatedByPangkatGolonganId($v = true)
    {
        $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganIdPartial = $v;
    }

    /**
     * Initializes the collRiwayatGajiBerkalasRelatedByPangkatGolonganId collection.
     *
     * By default this just sets the collRiwayatGajiBerkalasRelatedByPangkatGolonganId collection to an empty array (like clearcollRiwayatGajiBerkalasRelatedByPangkatGolonganId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRiwayatGajiBerkalasRelatedByPangkatGolonganId($overrideExisting = true)
    {
        if (null !== $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId && !$overrideExisting) {
            return;
        }
        $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId = new PropelObjectCollection();
        $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId->setModel('RiwayatGajiBerkala');
    }

    /**
     * Gets an array of RiwayatGajiBerkala objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this PangkatGolongan is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RiwayatGajiBerkala[] List of RiwayatGajiBerkala objects
     * @throws PropelException
     */
    public function getRiwayatGajiBerkalasRelatedByPangkatGolonganId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganIdPartial && !$this->isNew();
        if (null === $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId) {
                // return empty collection
                $this->initRiwayatGajiBerkalasRelatedByPangkatGolonganId();
            } else {
                $collRiwayatGajiBerkalasRelatedByPangkatGolonganId = RiwayatGajiBerkalaQuery::create(null, $criteria)
                    ->filterByPangkatGolonganRelatedByPangkatGolonganId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganIdPartial && count($collRiwayatGajiBerkalasRelatedByPangkatGolonganId)) {
                      $this->initRiwayatGajiBerkalasRelatedByPangkatGolonganId(false);

                      foreach($collRiwayatGajiBerkalasRelatedByPangkatGolonganId as $obj) {
                        if (false == $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId->contains($obj)) {
                          $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId->append($obj);
                        }
                      }

                      $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganIdPartial = true;
                    }

                    $collRiwayatGajiBerkalasRelatedByPangkatGolonganId->getInternalIterator()->rewind();
                    return $collRiwayatGajiBerkalasRelatedByPangkatGolonganId;
                }

                if($partial && $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId) {
                    foreach($this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId as $obj) {
                        if($obj->isNew()) {
                            $collRiwayatGajiBerkalasRelatedByPangkatGolonganId[] = $obj;
                        }
                    }
                }

                $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId = $collRiwayatGajiBerkalasRelatedByPangkatGolonganId;
                $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganIdPartial = false;
            }
        }

        return $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId;
    }

    /**
     * Sets a collection of RiwayatGajiBerkalaRelatedByPangkatGolonganId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $riwayatGajiBerkalasRelatedByPangkatGolonganId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return PangkatGolongan The current object (for fluent API support)
     */
    public function setRiwayatGajiBerkalasRelatedByPangkatGolonganId(PropelCollection $riwayatGajiBerkalasRelatedByPangkatGolonganId, PropelPDO $con = null)
    {
        $riwayatGajiBerkalasRelatedByPangkatGolonganIdToDelete = $this->getRiwayatGajiBerkalasRelatedByPangkatGolonganId(new Criteria(), $con)->diff($riwayatGajiBerkalasRelatedByPangkatGolonganId);

        $this->riwayatGajiBerkalasRelatedByPangkatGolonganIdScheduledForDeletion = unserialize(serialize($riwayatGajiBerkalasRelatedByPangkatGolonganIdToDelete));

        foreach ($riwayatGajiBerkalasRelatedByPangkatGolonganIdToDelete as $riwayatGajiBerkalaRelatedByPangkatGolonganIdRemoved) {
            $riwayatGajiBerkalaRelatedByPangkatGolonganIdRemoved->setPangkatGolonganRelatedByPangkatGolonganId(null);
        }

        $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId = null;
        foreach ($riwayatGajiBerkalasRelatedByPangkatGolonganId as $riwayatGajiBerkalaRelatedByPangkatGolonganId) {
            $this->addRiwayatGajiBerkalaRelatedByPangkatGolonganId($riwayatGajiBerkalaRelatedByPangkatGolonganId);
        }

        $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId = $riwayatGajiBerkalasRelatedByPangkatGolonganId;
        $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RiwayatGajiBerkala objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RiwayatGajiBerkala objects.
     * @throws PropelException
     */
    public function countRiwayatGajiBerkalasRelatedByPangkatGolonganId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganIdPartial && !$this->isNew();
        if (null === $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getRiwayatGajiBerkalasRelatedByPangkatGolonganId());
            }
            $query = RiwayatGajiBerkalaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPangkatGolonganRelatedByPangkatGolonganId($this)
                ->count($con);
        }

        return count($this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId);
    }

    /**
     * Method called to associate a RiwayatGajiBerkala object to this object
     * through the RiwayatGajiBerkala foreign key attribute.
     *
     * @param    RiwayatGajiBerkala $l RiwayatGajiBerkala
     * @return PangkatGolongan The current object (for fluent API support)
     */
    public function addRiwayatGajiBerkalaRelatedByPangkatGolonganId(RiwayatGajiBerkala $l)
    {
        if ($this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId === null) {
            $this->initRiwayatGajiBerkalasRelatedByPangkatGolonganId();
            $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganIdPartial = true;
        }
        if (!in_array($l, $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRiwayatGajiBerkalaRelatedByPangkatGolonganId($l);
        }

        return $this;
    }

    /**
     * @param	RiwayatGajiBerkalaRelatedByPangkatGolonganId $riwayatGajiBerkalaRelatedByPangkatGolonganId The riwayatGajiBerkalaRelatedByPangkatGolonganId object to add.
     */
    protected function doAddRiwayatGajiBerkalaRelatedByPangkatGolonganId($riwayatGajiBerkalaRelatedByPangkatGolonganId)
    {
        $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId[]= $riwayatGajiBerkalaRelatedByPangkatGolonganId;
        $riwayatGajiBerkalaRelatedByPangkatGolonganId->setPangkatGolonganRelatedByPangkatGolonganId($this);
    }

    /**
     * @param	RiwayatGajiBerkalaRelatedByPangkatGolonganId $riwayatGajiBerkalaRelatedByPangkatGolonganId The riwayatGajiBerkalaRelatedByPangkatGolonganId object to remove.
     * @return PangkatGolongan The current object (for fluent API support)
     */
    public function removeRiwayatGajiBerkalaRelatedByPangkatGolonganId($riwayatGajiBerkalaRelatedByPangkatGolonganId)
    {
        if ($this->getRiwayatGajiBerkalasRelatedByPangkatGolonganId()->contains($riwayatGajiBerkalaRelatedByPangkatGolonganId)) {
            $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId->remove($this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId->search($riwayatGajiBerkalaRelatedByPangkatGolonganId));
            if (null === $this->riwayatGajiBerkalasRelatedByPangkatGolonganIdScheduledForDeletion) {
                $this->riwayatGajiBerkalasRelatedByPangkatGolonganIdScheduledForDeletion = clone $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId;
                $this->riwayatGajiBerkalasRelatedByPangkatGolonganIdScheduledForDeletion->clear();
            }
            $this->riwayatGajiBerkalasRelatedByPangkatGolonganIdScheduledForDeletion[]= clone $riwayatGajiBerkalaRelatedByPangkatGolonganId;
            $riwayatGajiBerkalaRelatedByPangkatGolonganId->setPangkatGolonganRelatedByPangkatGolonganId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related RiwayatGajiBerkalasRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RiwayatGajiBerkala[] List of RiwayatGajiBerkala objects
     */
    public function getRiwayatGajiBerkalasRelatedByPangkatGolonganIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RiwayatGajiBerkalaQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getRiwayatGajiBerkalasRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related RiwayatGajiBerkalasRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RiwayatGajiBerkala[] List of RiwayatGajiBerkala objects
     */
    public function getRiwayatGajiBerkalasRelatedByPangkatGolonganIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RiwayatGajiBerkalaQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getRiwayatGajiBerkalasRelatedByPangkatGolonganId($query, $con);
    }

    /**
     * Clears out the collRiwayatGajiBerkalasRelatedByPangkatGolonganId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return PangkatGolongan The current object (for fluent API support)
     * @see        addRiwayatGajiBerkalasRelatedByPangkatGolonganId()
     */
    public function clearRiwayatGajiBerkalasRelatedByPangkatGolonganId()
    {
        $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId = null; // important to set this to null since that means it is uninitialized
        $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganIdPartial = null;

        return $this;
    }

    /**
     * reset is the collRiwayatGajiBerkalasRelatedByPangkatGolonganId collection loaded partially
     *
     * @return void
     */
    public function resetPartialRiwayatGajiBerkalasRelatedByPangkatGolonganId($v = true)
    {
        $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganIdPartial = $v;
    }

    /**
     * Initializes the collRiwayatGajiBerkalasRelatedByPangkatGolonganId collection.
     *
     * By default this just sets the collRiwayatGajiBerkalasRelatedByPangkatGolonganId collection to an empty array (like clearcollRiwayatGajiBerkalasRelatedByPangkatGolonganId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRiwayatGajiBerkalasRelatedByPangkatGolonganId($overrideExisting = true)
    {
        if (null !== $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId && !$overrideExisting) {
            return;
        }
        $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId = new PropelObjectCollection();
        $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId->setModel('RiwayatGajiBerkala');
    }

    /**
     * Gets an array of RiwayatGajiBerkala objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this PangkatGolongan is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RiwayatGajiBerkala[] List of RiwayatGajiBerkala objects
     * @throws PropelException
     */
    public function getRiwayatGajiBerkalasRelatedByPangkatGolonganId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganIdPartial && !$this->isNew();
        if (null === $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId) {
                // return empty collection
                $this->initRiwayatGajiBerkalasRelatedByPangkatGolonganId();
            } else {
                $collRiwayatGajiBerkalasRelatedByPangkatGolonganId = RiwayatGajiBerkalaQuery::create(null, $criteria)
                    ->filterByPangkatGolonganRelatedByPangkatGolonganId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganIdPartial && count($collRiwayatGajiBerkalasRelatedByPangkatGolonganId)) {
                      $this->initRiwayatGajiBerkalasRelatedByPangkatGolonganId(false);

                      foreach($collRiwayatGajiBerkalasRelatedByPangkatGolonganId as $obj) {
                        if (false == $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId->contains($obj)) {
                          $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId->append($obj);
                        }
                      }

                      $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganIdPartial = true;
                    }

                    $collRiwayatGajiBerkalasRelatedByPangkatGolonganId->getInternalIterator()->rewind();
                    return $collRiwayatGajiBerkalasRelatedByPangkatGolonganId;
                }

                if($partial && $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId) {
                    foreach($this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId as $obj) {
                        if($obj->isNew()) {
                            $collRiwayatGajiBerkalasRelatedByPangkatGolonganId[] = $obj;
                        }
                    }
                }

                $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId = $collRiwayatGajiBerkalasRelatedByPangkatGolonganId;
                $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganIdPartial = false;
            }
        }

        return $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId;
    }

    /**
     * Sets a collection of RiwayatGajiBerkalaRelatedByPangkatGolonganId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $riwayatGajiBerkalasRelatedByPangkatGolonganId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return PangkatGolongan The current object (for fluent API support)
     */
    public function setRiwayatGajiBerkalasRelatedByPangkatGolonganId(PropelCollection $riwayatGajiBerkalasRelatedByPangkatGolonganId, PropelPDO $con = null)
    {
        $riwayatGajiBerkalasRelatedByPangkatGolonganIdToDelete = $this->getRiwayatGajiBerkalasRelatedByPangkatGolonganId(new Criteria(), $con)->diff($riwayatGajiBerkalasRelatedByPangkatGolonganId);

        $this->riwayatGajiBerkalasRelatedByPangkatGolonganIdScheduledForDeletion = unserialize(serialize($riwayatGajiBerkalasRelatedByPangkatGolonganIdToDelete));

        foreach ($riwayatGajiBerkalasRelatedByPangkatGolonganIdToDelete as $riwayatGajiBerkalaRelatedByPangkatGolonganIdRemoved) {
            $riwayatGajiBerkalaRelatedByPangkatGolonganIdRemoved->setPangkatGolonganRelatedByPangkatGolonganId(null);
        }

        $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId = null;
        foreach ($riwayatGajiBerkalasRelatedByPangkatGolonganId as $riwayatGajiBerkalaRelatedByPangkatGolonganId) {
            $this->addRiwayatGajiBerkalaRelatedByPangkatGolonganId($riwayatGajiBerkalaRelatedByPangkatGolonganId);
        }

        $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId = $riwayatGajiBerkalasRelatedByPangkatGolonganId;
        $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RiwayatGajiBerkala objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RiwayatGajiBerkala objects.
     * @throws PropelException
     */
    public function countRiwayatGajiBerkalasRelatedByPangkatGolonganId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganIdPartial && !$this->isNew();
        if (null === $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getRiwayatGajiBerkalasRelatedByPangkatGolonganId());
            }
            $query = RiwayatGajiBerkalaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPangkatGolonganRelatedByPangkatGolonganId($this)
                ->count($con);
        }

        return count($this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId);
    }

    /**
     * Method called to associate a RiwayatGajiBerkala object to this object
     * through the RiwayatGajiBerkala foreign key attribute.
     *
     * @param    RiwayatGajiBerkala $l RiwayatGajiBerkala
     * @return PangkatGolongan The current object (for fluent API support)
     */
    public function addRiwayatGajiBerkalaRelatedByPangkatGolonganId(RiwayatGajiBerkala $l)
    {
        if ($this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId === null) {
            $this->initRiwayatGajiBerkalasRelatedByPangkatGolonganId();
            $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganIdPartial = true;
        }
        if (!in_array($l, $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRiwayatGajiBerkalaRelatedByPangkatGolonganId($l);
        }

        return $this;
    }

    /**
     * @param	RiwayatGajiBerkalaRelatedByPangkatGolonganId $riwayatGajiBerkalaRelatedByPangkatGolonganId The riwayatGajiBerkalaRelatedByPangkatGolonganId object to add.
     */
    protected function doAddRiwayatGajiBerkalaRelatedByPangkatGolonganId($riwayatGajiBerkalaRelatedByPangkatGolonganId)
    {
        $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId[]= $riwayatGajiBerkalaRelatedByPangkatGolonganId;
        $riwayatGajiBerkalaRelatedByPangkatGolonganId->setPangkatGolonganRelatedByPangkatGolonganId($this);
    }

    /**
     * @param	RiwayatGajiBerkalaRelatedByPangkatGolonganId $riwayatGajiBerkalaRelatedByPangkatGolonganId The riwayatGajiBerkalaRelatedByPangkatGolonganId object to remove.
     * @return PangkatGolongan The current object (for fluent API support)
     */
    public function removeRiwayatGajiBerkalaRelatedByPangkatGolonganId($riwayatGajiBerkalaRelatedByPangkatGolonganId)
    {
        if ($this->getRiwayatGajiBerkalasRelatedByPangkatGolonganId()->contains($riwayatGajiBerkalaRelatedByPangkatGolonganId)) {
            $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId->remove($this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId->search($riwayatGajiBerkalaRelatedByPangkatGolonganId));
            if (null === $this->riwayatGajiBerkalasRelatedByPangkatGolonganIdScheduledForDeletion) {
                $this->riwayatGajiBerkalasRelatedByPangkatGolonganIdScheduledForDeletion = clone $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId;
                $this->riwayatGajiBerkalasRelatedByPangkatGolonganIdScheduledForDeletion->clear();
            }
            $this->riwayatGajiBerkalasRelatedByPangkatGolonganIdScheduledForDeletion[]= clone $riwayatGajiBerkalaRelatedByPangkatGolonganId;
            $riwayatGajiBerkalaRelatedByPangkatGolonganId->setPangkatGolonganRelatedByPangkatGolonganId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related RiwayatGajiBerkalasRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RiwayatGajiBerkala[] List of RiwayatGajiBerkala objects
     */
    public function getRiwayatGajiBerkalasRelatedByPangkatGolonganIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RiwayatGajiBerkalaQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getRiwayatGajiBerkalasRelatedByPangkatGolonganId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PangkatGolongan is new, it will return
     * an empty collection; or if this PangkatGolongan has previously
     * been saved, it will retrieve related RiwayatGajiBerkalasRelatedByPangkatGolonganId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PangkatGolongan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RiwayatGajiBerkala[] List of RiwayatGajiBerkala objects
     */
    public function getRiwayatGajiBerkalasRelatedByPangkatGolonganIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RiwayatGajiBerkalaQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getRiwayatGajiBerkalasRelatedByPangkatGolonganId($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->pangkat_golongan_id = null;
        $this->kode = null;
        $this->nama = null;
        $this->create_date = null;
        $this->last_update = null;
        $this->expired_date = null;
        $this->last_sync = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collInpassingsRelatedByPangkatGolonganId) {
                foreach ($this->collInpassingsRelatedByPangkatGolonganId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collInpassingsRelatedByPangkatGolonganId) {
                foreach ($this->collInpassingsRelatedByPangkatGolonganId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRwyKepangkatansRelatedByPangkatGolonganId) {
                foreach ($this->collRwyKepangkatansRelatedByPangkatGolonganId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRwyKepangkatansRelatedByPangkatGolonganId) {
                foreach ($this->collRwyKepangkatansRelatedByPangkatGolonganId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPtksRelatedByPangkatGolonganId) {
                foreach ($this->collPtksRelatedByPangkatGolonganId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPtksRelatedByPangkatGolonganId) {
                foreach ($this->collPtksRelatedByPangkatGolonganId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId) {
                foreach ($this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId) {
                foreach ($this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId as $o) {
                    $o->clearAllReferences($deep);
                }
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collInpassingsRelatedByPangkatGolonganId instanceof PropelCollection) {
            $this->collInpassingsRelatedByPangkatGolonganId->clearIterator();
        }
        $this->collInpassingsRelatedByPangkatGolonganId = null;
        if ($this->collInpassingsRelatedByPangkatGolonganId instanceof PropelCollection) {
            $this->collInpassingsRelatedByPangkatGolonganId->clearIterator();
        }
        $this->collInpassingsRelatedByPangkatGolonganId = null;
        if ($this->collRwyKepangkatansRelatedByPangkatGolonganId instanceof PropelCollection) {
            $this->collRwyKepangkatansRelatedByPangkatGolonganId->clearIterator();
        }
        $this->collRwyKepangkatansRelatedByPangkatGolonganId = null;
        if ($this->collRwyKepangkatansRelatedByPangkatGolonganId instanceof PropelCollection) {
            $this->collRwyKepangkatansRelatedByPangkatGolonganId->clearIterator();
        }
        $this->collRwyKepangkatansRelatedByPangkatGolonganId = null;
        if ($this->collPtksRelatedByPangkatGolonganId instanceof PropelCollection) {
            $this->collPtksRelatedByPangkatGolonganId->clearIterator();
        }
        $this->collPtksRelatedByPangkatGolonganId = null;
        if ($this->collPtksRelatedByPangkatGolonganId instanceof PropelCollection) {
            $this->collPtksRelatedByPangkatGolonganId->clearIterator();
        }
        $this->collPtksRelatedByPangkatGolonganId = null;
        if ($this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId instanceof PropelCollection) {
            $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId->clearIterator();
        }
        $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId = null;
        if ($this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId instanceof PropelCollection) {
            $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId->clearIterator();
        }
        $this->collRiwayatGajiBerkalasRelatedByPangkatGolonganId = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(PangkatGolonganPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
