<?php

namespace angulex\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\AnggotaGugus;
use angulex\Model\GugusSekolah;
use angulex\Model\GugusSekolahPeer;
use angulex\Model\GugusSekolahQuery;
use angulex\Model\JenisGugus;
use angulex\Model\Sekolah;

/**
 * Base class that represents a query for the 'gugus_sekolah' table.
 *
 * 
 *
 * @method GugusSekolahQuery orderByGugusId($order = Criteria::ASC) Order by the gugus_id column
 * @method GugusSekolahQuery orderBySekolahId($order = Criteria::ASC) Order by the sekolah_id column
 * @method GugusSekolahQuery orderByJenisGugusId($order = Criteria::ASC) Order by the jenis_gugus_id column
 * @method GugusSekolahQuery orderByNama($order = Criteria::ASC) Order by the nama column
 * @method GugusSekolahQuery orderByLastUpdate($order = Criteria::ASC) Order by the Last_update column
 * @method GugusSekolahQuery orderBySoftDelete($order = Criteria::ASC) Order by the Soft_delete column
 * @method GugusSekolahQuery orderByLastSync($order = Criteria::ASC) Order by the last_sync column
 * @method GugusSekolahQuery orderByUpdaterId($order = Criteria::ASC) Order by the Updater_ID column
 *
 * @method GugusSekolahQuery groupByGugusId() Group by the gugus_id column
 * @method GugusSekolahQuery groupBySekolahId() Group by the sekolah_id column
 * @method GugusSekolahQuery groupByJenisGugusId() Group by the jenis_gugus_id column
 * @method GugusSekolahQuery groupByNama() Group by the nama column
 * @method GugusSekolahQuery groupByLastUpdate() Group by the Last_update column
 * @method GugusSekolahQuery groupBySoftDelete() Group by the Soft_delete column
 * @method GugusSekolahQuery groupByLastSync() Group by the last_sync column
 * @method GugusSekolahQuery groupByUpdaterId() Group by the Updater_ID column
 *
 * @method GugusSekolahQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method GugusSekolahQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method GugusSekolahQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method GugusSekolahQuery leftJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method GugusSekolahQuery rightJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method GugusSekolahQuery innerJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the SekolahRelatedBySekolahId relation
 *
 * @method GugusSekolahQuery leftJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method GugusSekolahQuery rightJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SekolahRelatedBySekolahId relation
 * @method GugusSekolahQuery innerJoinSekolahRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the SekolahRelatedBySekolahId relation
 *
 * @method GugusSekolahQuery leftJoinJenisGugusRelatedByJenisGugusId($relationAlias = null) Adds a LEFT JOIN clause to the query using the JenisGugusRelatedByJenisGugusId relation
 * @method GugusSekolahQuery rightJoinJenisGugusRelatedByJenisGugusId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JenisGugusRelatedByJenisGugusId relation
 * @method GugusSekolahQuery innerJoinJenisGugusRelatedByJenisGugusId($relationAlias = null) Adds a INNER JOIN clause to the query using the JenisGugusRelatedByJenisGugusId relation
 *
 * @method GugusSekolahQuery leftJoinJenisGugusRelatedByJenisGugusId($relationAlias = null) Adds a LEFT JOIN clause to the query using the JenisGugusRelatedByJenisGugusId relation
 * @method GugusSekolahQuery rightJoinJenisGugusRelatedByJenisGugusId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JenisGugusRelatedByJenisGugusId relation
 * @method GugusSekolahQuery innerJoinJenisGugusRelatedByJenisGugusId($relationAlias = null) Adds a INNER JOIN clause to the query using the JenisGugusRelatedByJenisGugusId relation
 *
 * @method GugusSekolahQuery leftJoinAnggotaGugusRelatedByGugusId($relationAlias = null) Adds a LEFT JOIN clause to the query using the AnggotaGugusRelatedByGugusId relation
 * @method GugusSekolahQuery rightJoinAnggotaGugusRelatedByGugusId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the AnggotaGugusRelatedByGugusId relation
 * @method GugusSekolahQuery innerJoinAnggotaGugusRelatedByGugusId($relationAlias = null) Adds a INNER JOIN clause to the query using the AnggotaGugusRelatedByGugusId relation
 *
 * @method GugusSekolahQuery leftJoinAnggotaGugusRelatedByGugusId($relationAlias = null) Adds a LEFT JOIN clause to the query using the AnggotaGugusRelatedByGugusId relation
 * @method GugusSekolahQuery rightJoinAnggotaGugusRelatedByGugusId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the AnggotaGugusRelatedByGugusId relation
 * @method GugusSekolahQuery innerJoinAnggotaGugusRelatedByGugusId($relationAlias = null) Adds a INNER JOIN clause to the query using the AnggotaGugusRelatedByGugusId relation
 *
 * @method GugusSekolah findOne(PropelPDO $con = null) Return the first GugusSekolah matching the query
 * @method GugusSekolah findOneOrCreate(PropelPDO $con = null) Return the first GugusSekolah matching the query, or a new GugusSekolah object populated from the query conditions when no match is found
 *
 * @method GugusSekolah findOneBySekolahId(string $sekolah_id) Return the first GugusSekolah filtered by the sekolah_id column
 * @method GugusSekolah findOneByJenisGugusId(string $jenis_gugus_id) Return the first GugusSekolah filtered by the jenis_gugus_id column
 * @method GugusSekolah findOneByNama(string $nama) Return the first GugusSekolah filtered by the nama column
 * @method GugusSekolah findOneByLastUpdate(string $Last_update) Return the first GugusSekolah filtered by the Last_update column
 * @method GugusSekolah findOneBySoftDelete(string $Soft_delete) Return the first GugusSekolah filtered by the Soft_delete column
 * @method GugusSekolah findOneByLastSync(string $last_sync) Return the first GugusSekolah filtered by the last_sync column
 * @method GugusSekolah findOneByUpdaterId(string $Updater_ID) Return the first GugusSekolah filtered by the Updater_ID column
 *
 * @method array findByGugusId(string $gugus_id) Return GugusSekolah objects filtered by the gugus_id column
 * @method array findBySekolahId(string $sekolah_id) Return GugusSekolah objects filtered by the sekolah_id column
 * @method array findByJenisGugusId(string $jenis_gugus_id) Return GugusSekolah objects filtered by the jenis_gugus_id column
 * @method array findByNama(string $nama) Return GugusSekolah objects filtered by the nama column
 * @method array findByLastUpdate(string $Last_update) Return GugusSekolah objects filtered by the Last_update column
 * @method array findBySoftDelete(string $Soft_delete) Return GugusSekolah objects filtered by the Soft_delete column
 * @method array findByLastSync(string $last_sync) Return GugusSekolah objects filtered by the last_sync column
 * @method array findByUpdaterId(string $Updater_ID) Return GugusSekolah objects filtered by the Updater_ID column
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseGugusSekolahQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseGugusSekolahQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Dapodikmen', $modelName = 'angulex\\Model\\GugusSekolah', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new GugusSekolahQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   GugusSekolahQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return GugusSekolahQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof GugusSekolahQuery) {
            return $criteria;
        }
        $query = new GugusSekolahQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   GugusSekolah|GugusSekolah[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = GugusSekolahPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(GugusSekolahPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 GugusSekolah A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByGugusId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 GugusSekolah A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [gugus_id], [sekolah_id], [jenis_gugus_id], [nama], [Last_update], [Soft_delete], [last_sync], [Updater_ID] FROM [gugus_sekolah] WHERE [gugus_id] = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new GugusSekolah();
            $obj->hydrate($row);
            GugusSekolahPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return GugusSekolah|GugusSekolah[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|GugusSekolah[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return GugusSekolahQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(GugusSekolahPeer::GUGUS_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return GugusSekolahQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(GugusSekolahPeer::GUGUS_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the gugus_id column
     *
     * Example usage:
     * <code>
     * $query->filterByGugusId('fooValue');   // WHERE gugus_id = 'fooValue'
     * $query->filterByGugusId('%fooValue%'); // WHERE gugus_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $gugusId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return GugusSekolahQuery The current query, for fluid interface
     */
    public function filterByGugusId($gugusId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($gugusId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $gugusId)) {
                $gugusId = str_replace('*', '%', $gugusId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(GugusSekolahPeer::GUGUS_ID, $gugusId, $comparison);
    }

    /**
     * Filter the query on the sekolah_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySekolahId('fooValue');   // WHERE sekolah_id = 'fooValue'
     * $query->filterBySekolahId('%fooValue%'); // WHERE sekolah_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sekolahId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return GugusSekolahQuery The current query, for fluid interface
     */
    public function filterBySekolahId($sekolahId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sekolahId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $sekolahId)) {
                $sekolahId = str_replace('*', '%', $sekolahId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(GugusSekolahPeer::SEKOLAH_ID, $sekolahId, $comparison);
    }

    /**
     * Filter the query on the jenis_gugus_id column
     *
     * Example usage:
     * <code>
     * $query->filterByJenisGugusId(1234); // WHERE jenis_gugus_id = 1234
     * $query->filterByJenisGugusId(array(12, 34)); // WHERE jenis_gugus_id IN (12, 34)
     * $query->filterByJenisGugusId(array('min' => 12)); // WHERE jenis_gugus_id >= 12
     * $query->filterByJenisGugusId(array('max' => 12)); // WHERE jenis_gugus_id <= 12
     * </code>
     *
     * @see       filterByJenisGugusRelatedByJenisGugusId()
     *
     * @see       filterByJenisGugusRelatedByJenisGugusId()
     *
     * @param     mixed $jenisGugusId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return GugusSekolahQuery The current query, for fluid interface
     */
    public function filterByJenisGugusId($jenisGugusId = null, $comparison = null)
    {
        if (is_array($jenisGugusId)) {
            $useMinMax = false;
            if (isset($jenisGugusId['min'])) {
                $this->addUsingAlias(GugusSekolahPeer::JENIS_GUGUS_ID, $jenisGugusId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jenisGugusId['max'])) {
                $this->addUsingAlias(GugusSekolahPeer::JENIS_GUGUS_ID, $jenisGugusId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GugusSekolahPeer::JENIS_GUGUS_ID, $jenisGugusId, $comparison);
    }

    /**
     * Filter the query on the nama column
     *
     * Example usage:
     * <code>
     * $query->filterByNama('fooValue');   // WHERE nama = 'fooValue'
     * $query->filterByNama('%fooValue%'); // WHERE nama LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nama The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return GugusSekolahQuery The current query, for fluid interface
     */
    public function filterByNama($nama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nama)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nama)) {
                $nama = str_replace('*', '%', $nama);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(GugusSekolahPeer::NAMA, $nama, $comparison);
    }

    /**
     * Filter the query on the Last_update column
     *
     * Example usage:
     * <code>
     * $query->filterByLastUpdate('2011-03-14'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate('now'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate(array('max' => 'yesterday')); // WHERE Last_update > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastUpdate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return GugusSekolahQuery The current query, for fluid interface
     */
    public function filterByLastUpdate($lastUpdate = null, $comparison = null)
    {
        if (is_array($lastUpdate)) {
            $useMinMax = false;
            if (isset($lastUpdate['min'])) {
                $this->addUsingAlias(GugusSekolahPeer::LAST_UPDATE, $lastUpdate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastUpdate['max'])) {
                $this->addUsingAlias(GugusSekolahPeer::LAST_UPDATE, $lastUpdate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GugusSekolahPeer::LAST_UPDATE, $lastUpdate, $comparison);
    }

    /**
     * Filter the query on the Soft_delete column
     *
     * Example usage:
     * <code>
     * $query->filterBySoftDelete(1234); // WHERE Soft_delete = 1234
     * $query->filterBySoftDelete(array(12, 34)); // WHERE Soft_delete IN (12, 34)
     * $query->filterBySoftDelete(array('min' => 12)); // WHERE Soft_delete >= 12
     * $query->filterBySoftDelete(array('max' => 12)); // WHERE Soft_delete <= 12
     * </code>
     *
     * @param     mixed $softDelete The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return GugusSekolahQuery The current query, for fluid interface
     */
    public function filterBySoftDelete($softDelete = null, $comparison = null)
    {
        if (is_array($softDelete)) {
            $useMinMax = false;
            if (isset($softDelete['min'])) {
                $this->addUsingAlias(GugusSekolahPeer::SOFT_DELETE, $softDelete['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($softDelete['max'])) {
                $this->addUsingAlias(GugusSekolahPeer::SOFT_DELETE, $softDelete['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GugusSekolahPeer::SOFT_DELETE, $softDelete, $comparison);
    }

    /**
     * Filter the query on the last_sync column
     *
     * Example usage:
     * <code>
     * $query->filterByLastSync('2011-03-14'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync('now'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync(array('max' => 'yesterday')); // WHERE last_sync > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastSync The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return GugusSekolahQuery The current query, for fluid interface
     */
    public function filterByLastSync($lastSync = null, $comparison = null)
    {
        if (is_array($lastSync)) {
            $useMinMax = false;
            if (isset($lastSync['min'])) {
                $this->addUsingAlias(GugusSekolahPeer::LAST_SYNC, $lastSync['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastSync['max'])) {
                $this->addUsingAlias(GugusSekolahPeer::LAST_SYNC, $lastSync['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GugusSekolahPeer::LAST_SYNC, $lastSync, $comparison);
    }

    /**
     * Filter the query on the Updater_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdaterId('fooValue');   // WHERE Updater_ID = 'fooValue'
     * $query->filterByUpdaterId('%fooValue%'); // WHERE Updater_ID LIKE '%fooValue%'
     * </code>
     *
     * @param     string $updaterId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return GugusSekolahQuery The current query, for fluid interface
     */
    public function filterByUpdaterId($updaterId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($updaterId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $updaterId)) {
                $updaterId = str_replace('*', '%', $updaterId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(GugusSekolahPeer::UPDATER_ID, $updaterId, $comparison);
    }

    /**
     * Filter the query by a related Sekolah object
     *
     * @param   Sekolah|PropelObjectCollection $sekolah The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 GugusSekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySekolahRelatedBySekolahId($sekolah, $comparison = null)
    {
        if ($sekolah instanceof Sekolah) {
            return $this
                ->addUsingAlias(GugusSekolahPeer::SEKOLAH_ID, $sekolah->getSekolahId(), $comparison);
        } elseif ($sekolah instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(GugusSekolahPeer::SEKOLAH_ID, $sekolah->toKeyValue('PrimaryKey', 'SekolahId'), $comparison);
        } else {
            throw new PropelException('filterBySekolahRelatedBySekolahId() only accepts arguments of type Sekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SekolahRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return GugusSekolahQuery The current query, for fluid interface
     */
    public function joinSekolahRelatedBySekolahId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SekolahRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SekolahRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the SekolahRelatedBySekolahId relation Sekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SekolahQuery A secondary query class using the current class as primary query
     */
    public function useSekolahRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSekolahRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SekolahRelatedBySekolahId', '\angulex\Model\SekolahQuery');
    }

    /**
     * Filter the query by a related Sekolah object
     *
     * @param   Sekolah|PropelObjectCollection $sekolah The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 GugusSekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySekolahRelatedBySekolahId($sekolah, $comparison = null)
    {
        if ($sekolah instanceof Sekolah) {
            return $this
                ->addUsingAlias(GugusSekolahPeer::SEKOLAH_ID, $sekolah->getSekolahId(), $comparison);
        } elseif ($sekolah instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(GugusSekolahPeer::SEKOLAH_ID, $sekolah->toKeyValue('PrimaryKey', 'SekolahId'), $comparison);
        } else {
            throw new PropelException('filterBySekolahRelatedBySekolahId() only accepts arguments of type Sekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SekolahRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return GugusSekolahQuery The current query, for fluid interface
     */
    public function joinSekolahRelatedBySekolahId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SekolahRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SekolahRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the SekolahRelatedBySekolahId relation Sekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SekolahQuery A secondary query class using the current class as primary query
     */
    public function useSekolahRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSekolahRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SekolahRelatedBySekolahId', '\angulex\Model\SekolahQuery');
    }

    /**
     * Filter the query by a related JenisGugus object
     *
     * @param   JenisGugus|PropelObjectCollection $jenisGugus The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 GugusSekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJenisGugusRelatedByJenisGugusId($jenisGugus, $comparison = null)
    {
        if ($jenisGugus instanceof JenisGugus) {
            return $this
                ->addUsingAlias(GugusSekolahPeer::JENIS_GUGUS_ID, $jenisGugus->getJenisGugusId(), $comparison);
        } elseif ($jenisGugus instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(GugusSekolahPeer::JENIS_GUGUS_ID, $jenisGugus->toKeyValue('PrimaryKey', 'JenisGugusId'), $comparison);
        } else {
            throw new PropelException('filterByJenisGugusRelatedByJenisGugusId() only accepts arguments of type JenisGugus or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the JenisGugusRelatedByJenisGugusId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return GugusSekolahQuery The current query, for fluid interface
     */
    public function joinJenisGugusRelatedByJenisGugusId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('JenisGugusRelatedByJenisGugusId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'JenisGugusRelatedByJenisGugusId');
        }

        return $this;
    }

    /**
     * Use the JenisGugusRelatedByJenisGugusId relation JenisGugus object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\JenisGugusQuery A secondary query class using the current class as primary query
     */
    public function useJenisGugusRelatedByJenisGugusIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinJenisGugusRelatedByJenisGugusId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'JenisGugusRelatedByJenisGugusId', '\angulex\Model\JenisGugusQuery');
    }

    /**
     * Filter the query by a related JenisGugus object
     *
     * @param   JenisGugus|PropelObjectCollection $jenisGugus The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 GugusSekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJenisGugusRelatedByJenisGugusId($jenisGugus, $comparison = null)
    {
        if ($jenisGugus instanceof JenisGugus) {
            return $this
                ->addUsingAlias(GugusSekolahPeer::JENIS_GUGUS_ID, $jenisGugus->getJenisGugusId(), $comparison);
        } elseif ($jenisGugus instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(GugusSekolahPeer::JENIS_GUGUS_ID, $jenisGugus->toKeyValue('PrimaryKey', 'JenisGugusId'), $comparison);
        } else {
            throw new PropelException('filterByJenisGugusRelatedByJenisGugusId() only accepts arguments of type JenisGugus or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the JenisGugusRelatedByJenisGugusId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return GugusSekolahQuery The current query, for fluid interface
     */
    public function joinJenisGugusRelatedByJenisGugusId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('JenisGugusRelatedByJenisGugusId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'JenisGugusRelatedByJenisGugusId');
        }

        return $this;
    }

    /**
     * Use the JenisGugusRelatedByJenisGugusId relation JenisGugus object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\JenisGugusQuery A secondary query class using the current class as primary query
     */
    public function useJenisGugusRelatedByJenisGugusIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinJenisGugusRelatedByJenisGugusId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'JenisGugusRelatedByJenisGugusId', '\angulex\Model\JenisGugusQuery');
    }

    /**
     * Filter the query by a related AnggotaGugus object
     *
     * @param   AnggotaGugus|PropelObjectCollection $anggotaGugus  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 GugusSekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByAnggotaGugusRelatedByGugusId($anggotaGugus, $comparison = null)
    {
        if ($anggotaGugus instanceof AnggotaGugus) {
            return $this
                ->addUsingAlias(GugusSekolahPeer::GUGUS_ID, $anggotaGugus->getGugusId(), $comparison);
        } elseif ($anggotaGugus instanceof PropelObjectCollection) {
            return $this
                ->useAnggotaGugusRelatedByGugusIdQuery()
                ->filterByPrimaryKeys($anggotaGugus->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByAnggotaGugusRelatedByGugusId() only accepts arguments of type AnggotaGugus or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the AnggotaGugusRelatedByGugusId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return GugusSekolahQuery The current query, for fluid interface
     */
    public function joinAnggotaGugusRelatedByGugusId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('AnggotaGugusRelatedByGugusId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'AnggotaGugusRelatedByGugusId');
        }

        return $this;
    }

    /**
     * Use the AnggotaGugusRelatedByGugusId relation AnggotaGugus object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\AnggotaGugusQuery A secondary query class using the current class as primary query
     */
    public function useAnggotaGugusRelatedByGugusIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinAnggotaGugusRelatedByGugusId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'AnggotaGugusRelatedByGugusId', '\angulex\Model\AnggotaGugusQuery');
    }

    /**
     * Filter the query by a related AnggotaGugus object
     *
     * @param   AnggotaGugus|PropelObjectCollection $anggotaGugus  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 GugusSekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByAnggotaGugusRelatedByGugusId($anggotaGugus, $comparison = null)
    {
        if ($anggotaGugus instanceof AnggotaGugus) {
            return $this
                ->addUsingAlias(GugusSekolahPeer::GUGUS_ID, $anggotaGugus->getGugusId(), $comparison);
        } elseif ($anggotaGugus instanceof PropelObjectCollection) {
            return $this
                ->useAnggotaGugusRelatedByGugusIdQuery()
                ->filterByPrimaryKeys($anggotaGugus->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByAnggotaGugusRelatedByGugusId() only accepts arguments of type AnggotaGugus or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the AnggotaGugusRelatedByGugusId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return GugusSekolahQuery The current query, for fluid interface
     */
    public function joinAnggotaGugusRelatedByGugusId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('AnggotaGugusRelatedByGugusId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'AnggotaGugusRelatedByGugusId');
        }

        return $this;
    }

    /**
     * Use the AnggotaGugusRelatedByGugusId relation AnggotaGugus object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\AnggotaGugusQuery A secondary query class using the current class as primary query
     */
    public function useAnggotaGugusRelatedByGugusIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinAnggotaGugusRelatedByGugusId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'AnggotaGugusRelatedByGugusId', '\angulex\Model\AnggotaGugusQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   GugusSekolah $gugusSekolah Object to remove from the list of results
     *
     * @return GugusSekolahQuery The current query, for fluid interface
     */
    public function prune($gugusSekolah = null)
    {
        if ($gugusSekolah) {
            $this->addUsingAlias(GugusSekolahPeer::GUGUS_ID, $gugusSekolah->getGugusId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
