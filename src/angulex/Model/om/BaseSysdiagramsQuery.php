<?php

namespace angulex\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \PDO;
use \Propel;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\Sysdiagrams;
use angulex\Model\SysdiagramsPeer;
use angulex\Model\SysdiagramsQuery;

/**
 * Base class that represents a query for the 'sysdiagrams' table.
 *
 * 
 *
 * @method SysdiagramsQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method SysdiagramsQuery orderByPrincipalId($order = Criteria::ASC) Order by the principal_id column
 * @method SysdiagramsQuery orderByDiagramId($order = Criteria::ASC) Order by the diagram_id column
 * @method SysdiagramsQuery orderByVersion($order = Criteria::ASC) Order by the version column
 * @method SysdiagramsQuery orderByDefinition($order = Criteria::ASC) Order by the definition column
 *
 * @method SysdiagramsQuery groupByName() Group by the name column
 * @method SysdiagramsQuery groupByPrincipalId() Group by the principal_id column
 * @method SysdiagramsQuery groupByDiagramId() Group by the diagram_id column
 * @method SysdiagramsQuery groupByVersion() Group by the version column
 * @method SysdiagramsQuery groupByDefinition() Group by the definition column
 *
 * @method SysdiagramsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SysdiagramsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SysdiagramsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method Sysdiagrams findOne(PropelPDO $con = null) Return the first Sysdiagrams matching the query
 * @method Sysdiagrams findOneOrCreate(PropelPDO $con = null) Return the first Sysdiagrams matching the query, or a new Sysdiagrams object populated from the query conditions when no match is found
 *
 * @method Sysdiagrams findOneByName(string $name) Return the first Sysdiagrams filtered by the name column
 * @method Sysdiagrams findOneByPrincipalId(int $principal_id) Return the first Sysdiagrams filtered by the principal_id column
 * @method Sysdiagrams findOneByVersion(int $version) Return the first Sysdiagrams filtered by the version column
 * @method Sysdiagrams findOneByDefinition(string $definition) Return the first Sysdiagrams filtered by the definition column
 *
 * @method array findByName(string $name) Return Sysdiagrams objects filtered by the name column
 * @method array findByPrincipalId(int $principal_id) Return Sysdiagrams objects filtered by the principal_id column
 * @method array findByDiagramId(int $diagram_id) Return Sysdiagrams objects filtered by the diagram_id column
 * @method array findByVersion(int $version) Return Sysdiagrams objects filtered by the version column
 * @method array findByDefinition(string $definition) Return Sysdiagrams objects filtered by the definition column
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseSysdiagramsQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSysdiagramsQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Dapodikmen', $modelName = 'angulex\\Model\\Sysdiagrams', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SysdiagramsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   SysdiagramsQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SysdiagramsQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SysdiagramsQuery) {
            return $criteria;
        }
        $query = new SysdiagramsQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Sysdiagrams|Sysdiagrams[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SysdiagramsPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SysdiagramsPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Sysdiagrams A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByDiagramId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Sysdiagrams A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [name], [principal_id], [diagram_id], [version], [definition] FROM [sysdiagrams] WHERE [diagram_id] = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Sysdiagrams();
            $obj->hydrate($row);
            SysdiagramsPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Sysdiagrams|Sysdiagrams[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Sysdiagrams[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SysdiagramsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SysdiagramsPeer::DIAGRAM_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SysdiagramsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SysdiagramsPeer::DIAGRAM_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysdiagramsQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SysdiagramsPeer::NAME, $name, $comparison);
    }

    /**
     * Filter the query on the principal_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPrincipalId(1234); // WHERE principal_id = 1234
     * $query->filterByPrincipalId(array(12, 34)); // WHERE principal_id IN (12, 34)
     * $query->filterByPrincipalId(array('min' => 12)); // WHERE principal_id >= 12
     * $query->filterByPrincipalId(array('max' => 12)); // WHERE principal_id <= 12
     * </code>
     *
     * @param     mixed $principalId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysdiagramsQuery The current query, for fluid interface
     */
    public function filterByPrincipalId($principalId = null, $comparison = null)
    {
        if (is_array($principalId)) {
            $useMinMax = false;
            if (isset($principalId['min'])) {
                $this->addUsingAlias(SysdiagramsPeer::PRINCIPAL_ID, $principalId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($principalId['max'])) {
                $this->addUsingAlias(SysdiagramsPeer::PRINCIPAL_ID, $principalId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SysdiagramsPeer::PRINCIPAL_ID, $principalId, $comparison);
    }

    /**
     * Filter the query on the diagram_id column
     *
     * Example usage:
     * <code>
     * $query->filterByDiagramId(1234); // WHERE diagram_id = 1234
     * $query->filterByDiagramId(array(12, 34)); // WHERE diagram_id IN (12, 34)
     * $query->filterByDiagramId(array('min' => 12)); // WHERE diagram_id >= 12
     * $query->filterByDiagramId(array('max' => 12)); // WHERE diagram_id <= 12
     * </code>
     *
     * @param     mixed $diagramId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysdiagramsQuery The current query, for fluid interface
     */
    public function filterByDiagramId($diagramId = null, $comparison = null)
    {
        if (is_array($diagramId)) {
            $useMinMax = false;
            if (isset($diagramId['min'])) {
                $this->addUsingAlias(SysdiagramsPeer::DIAGRAM_ID, $diagramId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($diagramId['max'])) {
                $this->addUsingAlias(SysdiagramsPeer::DIAGRAM_ID, $diagramId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SysdiagramsPeer::DIAGRAM_ID, $diagramId, $comparison);
    }

    /**
     * Filter the query on the version column
     *
     * Example usage:
     * <code>
     * $query->filterByVersion(1234); // WHERE version = 1234
     * $query->filterByVersion(array(12, 34)); // WHERE version IN (12, 34)
     * $query->filterByVersion(array('min' => 12)); // WHERE version >= 12
     * $query->filterByVersion(array('max' => 12)); // WHERE version <= 12
     * </code>
     *
     * @param     mixed $version The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysdiagramsQuery The current query, for fluid interface
     */
    public function filterByVersion($version = null, $comparison = null)
    {
        if (is_array($version)) {
            $useMinMax = false;
            if (isset($version['min'])) {
                $this->addUsingAlias(SysdiagramsPeer::VERSION, $version['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($version['max'])) {
                $this->addUsingAlias(SysdiagramsPeer::VERSION, $version['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SysdiagramsPeer::VERSION, $version, $comparison);
    }

    /**
     * Filter the query on the definition column
     *
     * @param     mixed $definition The value to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysdiagramsQuery The current query, for fluid interface
     */
    public function filterByDefinition($definition = null, $comparison = null)
    {

        return $this->addUsingAlias(SysdiagramsPeer::DEFINITION, $definition, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Sysdiagrams $sysdiagrams Object to remove from the list of results
     *
     * @return SysdiagramsQuery The current query, for fluid interface
     */
    public function prune($sysdiagrams = null)
    {
        if ($sysdiagrams) {
            $this->addUsingAlias(SysdiagramsPeer::DIAGRAM_ID, $sysdiagrams->getDiagramId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
