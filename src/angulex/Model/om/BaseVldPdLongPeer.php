<?php

namespace angulex\Model\om;

use \BasePeer;
use \Criteria;
use \PDO;
use \PDOStatement;
use \Propel;
use \PropelException;
use \PropelPDO;
use angulex\Model\ErrortypePeer;
use angulex\Model\PesertaDidikLongitudinalPeer;
use angulex\Model\VldPdLong;
use angulex\Model\VldPdLongPeer;
use angulex\Model\map\VldPdLongTableMap;

/**
 * Base static class for performing query and update operations on the 'vld_pd_long' table.
 *
 * 
 *
 * @package propel.generator.angulex.Model.om
 */
abstract class BaseVldPdLongPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'Dapodikmen';

    /** the table name for this class */
    const TABLE_NAME = 'vld_pd_long';

    /** the related Propel class for this table */
    const OM_CLASS = 'angulex\\Model\\VldPdLong';

    /** the related TableMap class for this table */
    const TM_CLASS = 'VldPdLongTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 14;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 14;

    /** the column name for the peserta_didik_id field */
    const PESERTA_DIDIK_ID = 'vld_pd_long.peserta_didik_id';

    /** the column name for the semester_id field */
    const SEMESTER_ID = 'vld_pd_long.semester_id';

    /** the column name for the logid field */
    const LOGID = 'vld_pd_long.logid';

    /** the column name for the idtype field */
    const IDTYPE = 'vld_pd_long.idtype';

    /** the column name for the status_validasi field */
    const STATUS_VALIDASI = 'vld_pd_long.status_validasi';

    /** the column name for the status_verifikasi field */
    const STATUS_VERIFIKASI = 'vld_pd_long.status_verifikasi';

    /** the column name for the field_error field */
    const FIELD_ERROR = 'vld_pd_long.field_error';

    /** the column name for the error_message field */
    const ERROR_MESSAGE = 'vld_pd_long.error_message';

    /** the column name for the keterangan field */
    const KETERANGAN = 'vld_pd_long.keterangan';

    /** the column name for the create_date field */
    const CREATE_DATE = 'vld_pd_long.create_date';

    /** the column name for the last_update field */
    const LAST_UPDATE = 'vld_pd_long.last_update';

    /** the column name for the expired_date field */
    const EXPIRED_DATE = 'vld_pd_long.expired_date';

    /** the column name for the last_sync field */
    const LAST_SYNC = 'vld_pd_long.last_sync';

    /** the column name for the app_username field */
    const APP_USERNAME = 'vld_pd_long.app_username';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identiy map to hold any loaded instances of VldPdLong objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array VldPdLong[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. VldPdLongPeer::$fieldNames[VldPdLongPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('PesertaDidikId', 'SemesterId', 'Logid', 'Idtype', 'StatusValidasi', 'StatusVerifikasi', 'FieldError', 'ErrorMessage', 'Keterangan', 'CreateDate', 'LastUpdate', 'ExpiredDate', 'LastSync', 'AppUsername', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('pesertaDidikId', 'semesterId', 'logid', 'idtype', 'statusValidasi', 'statusVerifikasi', 'fieldError', 'errorMessage', 'keterangan', 'createDate', 'lastUpdate', 'expiredDate', 'lastSync', 'appUsername', ),
        BasePeer::TYPE_COLNAME => array (VldPdLongPeer::PESERTA_DIDIK_ID, VldPdLongPeer::SEMESTER_ID, VldPdLongPeer::LOGID, VldPdLongPeer::IDTYPE, VldPdLongPeer::STATUS_VALIDASI, VldPdLongPeer::STATUS_VERIFIKASI, VldPdLongPeer::FIELD_ERROR, VldPdLongPeer::ERROR_MESSAGE, VldPdLongPeer::KETERANGAN, VldPdLongPeer::CREATE_DATE, VldPdLongPeer::LAST_UPDATE, VldPdLongPeer::EXPIRED_DATE, VldPdLongPeer::LAST_SYNC, VldPdLongPeer::APP_USERNAME, ),
        BasePeer::TYPE_RAW_COLNAME => array ('PESERTA_DIDIK_ID', 'SEMESTER_ID', 'LOGID', 'IDTYPE', 'STATUS_VALIDASI', 'STATUS_VERIFIKASI', 'FIELD_ERROR', 'ERROR_MESSAGE', 'KETERANGAN', 'CREATE_DATE', 'LAST_UPDATE', 'EXPIRED_DATE', 'LAST_SYNC', 'APP_USERNAME', ),
        BasePeer::TYPE_FIELDNAME => array ('peserta_didik_id', 'semester_id', 'logid', 'idtype', 'status_validasi', 'status_verifikasi', 'field_error', 'error_message', 'keterangan', 'create_date', 'last_update', 'expired_date', 'last_sync', 'app_username', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. VldPdLongPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('PesertaDidikId' => 0, 'SemesterId' => 1, 'Logid' => 2, 'Idtype' => 3, 'StatusValidasi' => 4, 'StatusVerifikasi' => 5, 'FieldError' => 6, 'ErrorMessage' => 7, 'Keterangan' => 8, 'CreateDate' => 9, 'LastUpdate' => 10, 'ExpiredDate' => 11, 'LastSync' => 12, 'AppUsername' => 13, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('pesertaDidikId' => 0, 'semesterId' => 1, 'logid' => 2, 'idtype' => 3, 'statusValidasi' => 4, 'statusVerifikasi' => 5, 'fieldError' => 6, 'errorMessage' => 7, 'keterangan' => 8, 'createDate' => 9, 'lastUpdate' => 10, 'expiredDate' => 11, 'lastSync' => 12, 'appUsername' => 13, ),
        BasePeer::TYPE_COLNAME => array (VldPdLongPeer::PESERTA_DIDIK_ID => 0, VldPdLongPeer::SEMESTER_ID => 1, VldPdLongPeer::LOGID => 2, VldPdLongPeer::IDTYPE => 3, VldPdLongPeer::STATUS_VALIDASI => 4, VldPdLongPeer::STATUS_VERIFIKASI => 5, VldPdLongPeer::FIELD_ERROR => 6, VldPdLongPeer::ERROR_MESSAGE => 7, VldPdLongPeer::KETERANGAN => 8, VldPdLongPeer::CREATE_DATE => 9, VldPdLongPeer::LAST_UPDATE => 10, VldPdLongPeer::EXPIRED_DATE => 11, VldPdLongPeer::LAST_SYNC => 12, VldPdLongPeer::APP_USERNAME => 13, ),
        BasePeer::TYPE_RAW_COLNAME => array ('PESERTA_DIDIK_ID' => 0, 'SEMESTER_ID' => 1, 'LOGID' => 2, 'IDTYPE' => 3, 'STATUS_VALIDASI' => 4, 'STATUS_VERIFIKASI' => 5, 'FIELD_ERROR' => 6, 'ERROR_MESSAGE' => 7, 'KETERANGAN' => 8, 'CREATE_DATE' => 9, 'LAST_UPDATE' => 10, 'EXPIRED_DATE' => 11, 'LAST_SYNC' => 12, 'APP_USERNAME' => 13, ),
        BasePeer::TYPE_FIELDNAME => array ('peserta_didik_id' => 0, 'semester_id' => 1, 'logid' => 2, 'idtype' => 3, 'status_validasi' => 4, 'status_verifikasi' => 5, 'field_error' => 6, 'error_message' => 7, 'keterangan' => 8, 'create_date' => 9, 'last_update' => 10, 'expired_date' => 11, 'last_sync' => 12, 'app_username' => 13, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = VldPdLongPeer::getFieldNames($toType);
        $key = isset(VldPdLongPeer::$fieldKeys[$fromType][$name]) ? VldPdLongPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(VldPdLongPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, VldPdLongPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return VldPdLongPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. VldPdLongPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(VldPdLongPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(VldPdLongPeer::PESERTA_DIDIK_ID);
            $criteria->addSelectColumn(VldPdLongPeer::SEMESTER_ID);
            $criteria->addSelectColumn(VldPdLongPeer::LOGID);
            $criteria->addSelectColumn(VldPdLongPeer::IDTYPE);
            $criteria->addSelectColumn(VldPdLongPeer::STATUS_VALIDASI);
            $criteria->addSelectColumn(VldPdLongPeer::STATUS_VERIFIKASI);
            $criteria->addSelectColumn(VldPdLongPeer::FIELD_ERROR);
            $criteria->addSelectColumn(VldPdLongPeer::ERROR_MESSAGE);
            $criteria->addSelectColumn(VldPdLongPeer::KETERANGAN);
            $criteria->addSelectColumn(VldPdLongPeer::CREATE_DATE);
            $criteria->addSelectColumn(VldPdLongPeer::LAST_UPDATE);
            $criteria->addSelectColumn(VldPdLongPeer::EXPIRED_DATE);
            $criteria->addSelectColumn(VldPdLongPeer::LAST_SYNC);
            $criteria->addSelectColumn(VldPdLongPeer::APP_USERNAME);
        } else {
            $criteria->addSelectColumn($alias . '.peserta_didik_id');
            $criteria->addSelectColumn($alias . '.semester_id');
            $criteria->addSelectColumn($alias . '.logid');
            $criteria->addSelectColumn($alias . '.idtype');
            $criteria->addSelectColumn($alias . '.status_validasi');
            $criteria->addSelectColumn($alias . '.status_verifikasi');
            $criteria->addSelectColumn($alias . '.field_error');
            $criteria->addSelectColumn($alias . '.error_message');
            $criteria->addSelectColumn($alias . '.keterangan');
            $criteria->addSelectColumn($alias . '.create_date');
            $criteria->addSelectColumn($alias . '.last_update');
            $criteria->addSelectColumn($alias . '.expired_date');
            $criteria->addSelectColumn($alias . '.last_sync');
            $criteria->addSelectColumn($alias . '.app_username');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(VldPdLongPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            VldPdLongPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(VldPdLongPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(VldPdLongPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 VldPdLong
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = VldPdLongPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return VldPdLongPeer::populateObjects(VldPdLongPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(VldPdLongPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            VldPdLongPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(VldPdLongPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      VldPdLong $obj A VldPdLong object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = serialize(array((string) $obj->getPesertaDidikId(), (string) $obj->getSemesterId(), (string) $obj->getLogid()));
            } // if key === null
            VldPdLongPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A VldPdLong object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof VldPdLong) {
                $key = serialize(array((string) $value->getPesertaDidikId(), (string) $value->getSemesterId(), (string) $value->getLogid()));
            } elseif (is_array($value) && count($value) === 3) {
                // assume we've been passed a primary key
                $key = serialize(array((string) $value[0], (string) $value[1], (string) $value[2]));
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or VldPdLong object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(VldPdLongPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   VldPdLong Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(VldPdLongPeer::$instances[$key])) {
                return VldPdLongPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }
    
    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references)
      {
        foreach (VldPdLongPeer::$instances as $instance)
        {
          $instance->clearAllReferences(true);
        }
      }
        VldPdLongPeer::$instances = array();
    }
    
    /**
     * Method to invalidate the instance pool of all tables related to vld_pd_long
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null && $row[$startcol + 1] === null && $row[$startcol + 2] === null) {
            return null;
        }

        return serialize(array((string) $row[$startcol], (string) $row[$startcol + 1], (string) $row[$startcol + 2]));
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return array((string) $row[$startcol], (string) $row[$startcol + 1], (string) $row[$startcol + 2]);
    }
    
    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();
    
        // set the class once to avoid overhead in the loop
        $cls = VldPdLongPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = VldPdLongPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = VldPdLongPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                VldPdLongPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (VldPdLong object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = VldPdLongPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = VldPdLongPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + VldPdLongPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = VldPdLongPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            VldPdLongPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related PesertaDidikLongitudinalRelatedByPesertaDidikIdSemesterId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinPesertaDidikLongitudinalRelatedByPesertaDidikIdSemesterId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(VldPdLongPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            VldPdLongPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(VldPdLongPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(VldPdLongPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addMultipleJoin(array(
        array(VldPdLongPeer::PESERTA_DIDIK_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::SEMESTER_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::PESERTA_DIDIK_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::SEMESTER_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
      ), $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related PesertaDidikLongitudinalRelatedByPesertaDidikIdSemesterId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinPesertaDidikLongitudinalRelatedByPesertaDidikIdSemesterId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(VldPdLongPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            VldPdLongPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(VldPdLongPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(VldPdLongPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addMultipleJoin(array(
        array(VldPdLongPeer::PESERTA_DIDIK_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::SEMESTER_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::PESERTA_DIDIK_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::SEMESTER_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
      ), $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related ErrortypeRelatedByIdtype table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinErrortypeRelatedByIdtype(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(VldPdLongPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            VldPdLongPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(VldPdLongPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(VldPdLongPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(VldPdLongPeer::IDTYPE, ErrortypePeer::IDTYPE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related ErrortypeRelatedByIdtype table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinErrortypeRelatedByIdtype(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(VldPdLongPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            VldPdLongPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(VldPdLongPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(VldPdLongPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(VldPdLongPeer::IDTYPE, ErrortypePeer::IDTYPE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of VldPdLong objects pre-filled with their PesertaDidikLongitudinal objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of VldPdLong objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinPesertaDidikLongitudinalRelatedByPesertaDidikIdSemesterId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(VldPdLongPeer::DATABASE_NAME);
        }

        VldPdLongPeer::addSelectColumns($criteria);
        $startcol = VldPdLongPeer::NUM_HYDRATE_COLUMNS;
        PesertaDidikLongitudinalPeer::addSelectColumns($criteria);

        $criteria->addMultipleJoin(array(
        array(VldPdLongPeer::PESERTA_DIDIK_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::SEMESTER_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::PESERTA_DIDIK_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::SEMESTER_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
      ), $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = VldPdLongPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = VldPdLongPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = VldPdLongPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                VldPdLongPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = PesertaDidikLongitudinalPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = PesertaDidikLongitudinalPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = PesertaDidikLongitudinalPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    PesertaDidikLongitudinalPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (VldPdLong) to $obj2 (PesertaDidikLongitudinal)
                $obj2->addVldPdLongRelatedByPesertaDidikIdSemesterId($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of VldPdLong objects pre-filled with their PesertaDidikLongitudinal objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of VldPdLong objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinPesertaDidikLongitudinalRelatedByPesertaDidikIdSemesterId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(VldPdLongPeer::DATABASE_NAME);
        }

        VldPdLongPeer::addSelectColumns($criteria);
        $startcol = VldPdLongPeer::NUM_HYDRATE_COLUMNS;
        PesertaDidikLongitudinalPeer::addSelectColumns($criteria);

        $criteria->addMultipleJoin(array(
        array(VldPdLongPeer::PESERTA_DIDIK_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::SEMESTER_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::PESERTA_DIDIK_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::SEMESTER_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
      ), $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = VldPdLongPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = VldPdLongPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = VldPdLongPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                VldPdLongPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = PesertaDidikLongitudinalPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = PesertaDidikLongitudinalPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = PesertaDidikLongitudinalPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    PesertaDidikLongitudinalPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (VldPdLong) to $obj2 (PesertaDidikLongitudinal)
                $obj2->addVldPdLongRelatedByPesertaDidikIdSemesterId($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of VldPdLong objects pre-filled with their Errortype objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of VldPdLong objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinErrortypeRelatedByIdtype(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(VldPdLongPeer::DATABASE_NAME);
        }

        VldPdLongPeer::addSelectColumns($criteria);
        $startcol = VldPdLongPeer::NUM_HYDRATE_COLUMNS;
        ErrortypePeer::addSelectColumns($criteria);

        $criteria->addJoin(VldPdLongPeer::IDTYPE, ErrortypePeer::IDTYPE, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = VldPdLongPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = VldPdLongPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = VldPdLongPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                VldPdLongPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = ErrortypePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = ErrortypePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = ErrortypePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    ErrortypePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (VldPdLong) to $obj2 (Errortype)
                $obj2->addVldPdLongRelatedByIdtype($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of VldPdLong objects pre-filled with their Errortype objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of VldPdLong objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinErrortypeRelatedByIdtype(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(VldPdLongPeer::DATABASE_NAME);
        }

        VldPdLongPeer::addSelectColumns($criteria);
        $startcol = VldPdLongPeer::NUM_HYDRATE_COLUMNS;
        ErrortypePeer::addSelectColumns($criteria);

        $criteria->addJoin(VldPdLongPeer::IDTYPE, ErrortypePeer::IDTYPE, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = VldPdLongPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = VldPdLongPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = VldPdLongPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                VldPdLongPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = ErrortypePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = ErrortypePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = ErrortypePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    ErrortypePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (VldPdLong) to $obj2 (Errortype)
                $obj2->addVldPdLongRelatedByIdtype($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(VldPdLongPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            VldPdLongPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(VldPdLongPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(VldPdLongPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addMultipleJoin(array(
        array(VldPdLongPeer::PESERTA_DIDIK_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::SEMESTER_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::PESERTA_DIDIK_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::SEMESTER_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
      ), $join_behavior);

        $criteria->addMultipleJoin(array(
        array(VldPdLongPeer::PESERTA_DIDIK_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::SEMESTER_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::PESERTA_DIDIK_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::SEMESTER_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
      ), $join_behavior);

        $criteria->addJoin(VldPdLongPeer::IDTYPE, ErrortypePeer::IDTYPE, $join_behavior);

        $criteria->addJoin(VldPdLongPeer::IDTYPE, ErrortypePeer::IDTYPE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of VldPdLong objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of VldPdLong objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(VldPdLongPeer::DATABASE_NAME);
        }

        VldPdLongPeer::addSelectColumns($criteria);
        $startcol2 = VldPdLongPeer::NUM_HYDRATE_COLUMNS;

        PesertaDidikLongitudinalPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + PesertaDidikLongitudinalPeer::NUM_HYDRATE_COLUMNS;

        PesertaDidikLongitudinalPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + PesertaDidikLongitudinalPeer::NUM_HYDRATE_COLUMNS;

        ErrortypePeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + ErrortypePeer::NUM_HYDRATE_COLUMNS;

        ErrortypePeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + ErrortypePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addMultipleJoin(array(
        array(VldPdLongPeer::PESERTA_DIDIK_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::SEMESTER_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::PESERTA_DIDIK_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::SEMESTER_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
      ), $join_behavior);

        $criteria->addMultipleJoin(array(
        array(VldPdLongPeer::PESERTA_DIDIK_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::SEMESTER_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::PESERTA_DIDIK_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::SEMESTER_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
      ), $join_behavior);

        $criteria->addJoin(VldPdLongPeer::IDTYPE, ErrortypePeer::IDTYPE, $join_behavior);

        $criteria->addJoin(VldPdLongPeer::IDTYPE, ErrortypePeer::IDTYPE, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = VldPdLongPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = VldPdLongPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = VldPdLongPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                VldPdLongPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined PesertaDidikLongitudinal rows

            $key2 = PesertaDidikLongitudinalPeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = PesertaDidikLongitudinalPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = PesertaDidikLongitudinalPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    PesertaDidikLongitudinalPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (VldPdLong) to the collection in $obj2 (PesertaDidikLongitudinal)
                $obj2->addVldPdLongRelatedByPesertaDidikIdSemesterId($obj1);
            } // if joined row not null

            // Add objects for joined PesertaDidikLongitudinal rows

            $key3 = PesertaDidikLongitudinalPeer::getPrimaryKeyHashFromRow($row, $startcol3);
            if ($key3 !== null) {
                $obj3 = PesertaDidikLongitudinalPeer::getInstanceFromPool($key3);
                if (!$obj3) {

                    $cls = PesertaDidikLongitudinalPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    PesertaDidikLongitudinalPeer::addInstanceToPool($obj3, $key3);
                } // if obj3 loaded

                // Add the $obj1 (VldPdLong) to the collection in $obj3 (PesertaDidikLongitudinal)
                $obj3->addVldPdLongRelatedByPesertaDidikIdSemesterId($obj1);
            } // if joined row not null

            // Add objects for joined Errortype rows

            $key4 = ErrortypePeer::getPrimaryKeyHashFromRow($row, $startcol4);
            if ($key4 !== null) {
                $obj4 = ErrortypePeer::getInstanceFromPool($key4);
                if (!$obj4) {

                    $cls = ErrortypePeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    ErrortypePeer::addInstanceToPool($obj4, $key4);
                } // if obj4 loaded

                // Add the $obj1 (VldPdLong) to the collection in $obj4 (Errortype)
                $obj4->addVldPdLongRelatedByIdtype($obj1);
            } // if joined row not null

            // Add objects for joined Errortype rows

            $key5 = ErrortypePeer::getPrimaryKeyHashFromRow($row, $startcol5);
            if ($key5 !== null) {
                $obj5 = ErrortypePeer::getInstanceFromPool($key5);
                if (!$obj5) {

                    $cls = ErrortypePeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    ErrortypePeer::addInstanceToPool($obj5, $key5);
                } // if obj5 loaded

                // Add the $obj1 (VldPdLong) to the collection in $obj5 (Errortype)
                $obj5->addVldPdLongRelatedByIdtype($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining the related PesertaDidikLongitudinalRelatedByPesertaDidikIdSemesterId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptPesertaDidikLongitudinalRelatedByPesertaDidikIdSemesterId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(VldPdLongPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            VldPdLongPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(VldPdLongPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(VldPdLongPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addJoin(VldPdLongPeer::IDTYPE, ErrortypePeer::IDTYPE, $join_behavior);

        $criteria->addJoin(VldPdLongPeer::IDTYPE, ErrortypePeer::IDTYPE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related PesertaDidikLongitudinalRelatedByPesertaDidikIdSemesterId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptPesertaDidikLongitudinalRelatedByPesertaDidikIdSemesterId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(VldPdLongPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            VldPdLongPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(VldPdLongPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(VldPdLongPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addJoin(VldPdLongPeer::IDTYPE, ErrortypePeer::IDTYPE, $join_behavior);

        $criteria->addJoin(VldPdLongPeer::IDTYPE, ErrortypePeer::IDTYPE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related ErrortypeRelatedByIdtype table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptErrortypeRelatedByIdtype(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(VldPdLongPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            VldPdLongPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(VldPdLongPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(VldPdLongPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addMultipleJoin(array(
        array(VldPdLongPeer::PESERTA_DIDIK_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::SEMESTER_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::PESERTA_DIDIK_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::SEMESTER_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
      ), $join_behavior);

        $criteria->addMultipleJoin(array(
        array(VldPdLongPeer::PESERTA_DIDIK_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::SEMESTER_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::PESERTA_DIDIK_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::SEMESTER_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
      ), $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related ErrortypeRelatedByIdtype table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptErrortypeRelatedByIdtype(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(VldPdLongPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            VldPdLongPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(VldPdLongPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(VldPdLongPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addMultipleJoin(array(
        array(VldPdLongPeer::PESERTA_DIDIK_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::SEMESTER_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::PESERTA_DIDIK_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::SEMESTER_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
      ), $join_behavior);

        $criteria->addMultipleJoin(array(
        array(VldPdLongPeer::PESERTA_DIDIK_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::SEMESTER_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::PESERTA_DIDIK_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::SEMESTER_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
      ), $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of VldPdLong objects pre-filled with all related objects except PesertaDidikLongitudinalRelatedByPesertaDidikIdSemesterId.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of VldPdLong objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptPesertaDidikLongitudinalRelatedByPesertaDidikIdSemesterId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(VldPdLongPeer::DATABASE_NAME);
        }

        VldPdLongPeer::addSelectColumns($criteria);
        $startcol2 = VldPdLongPeer::NUM_HYDRATE_COLUMNS;

        ErrortypePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + ErrortypePeer::NUM_HYDRATE_COLUMNS;

        ErrortypePeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + ErrortypePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(VldPdLongPeer::IDTYPE, ErrortypePeer::IDTYPE, $join_behavior);

        $criteria->addJoin(VldPdLongPeer::IDTYPE, ErrortypePeer::IDTYPE, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = VldPdLongPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = VldPdLongPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = VldPdLongPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                VldPdLongPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Errortype rows

                $key2 = ErrortypePeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = ErrortypePeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = ErrortypePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    ErrortypePeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (VldPdLong) to the collection in $obj2 (Errortype)
                $obj2->addVldPdLongRelatedByIdtype($obj1);

            } // if joined row is not null

                // Add objects for joined Errortype rows

                $key3 = ErrortypePeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = ErrortypePeer::getInstanceFromPool($key3);
                    if (!$obj3) {
    
                        $cls = ErrortypePeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    ErrortypePeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (VldPdLong) to the collection in $obj3 (Errortype)
                $obj3->addVldPdLongRelatedByIdtype($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of VldPdLong objects pre-filled with all related objects except PesertaDidikLongitudinalRelatedByPesertaDidikIdSemesterId.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of VldPdLong objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptPesertaDidikLongitudinalRelatedByPesertaDidikIdSemesterId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(VldPdLongPeer::DATABASE_NAME);
        }

        VldPdLongPeer::addSelectColumns($criteria);
        $startcol2 = VldPdLongPeer::NUM_HYDRATE_COLUMNS;

        ErrortypePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + ErrortypePeer::NUM_HYDRATE_COLUMNS;

        ErrortypePeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + ErrortypePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(VldPdLongPeer::IDTYPE, ErrortypePeer::IDTYPE, $join_behavior);

        $criteria->addJoin(VldPdLongPeer::IDTYPE, ErrortypePeer::IDTYPE, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = VldPdLongPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = VldPdLongPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = VldPdLongPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                VldPdLongPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Errortype rows

                $key2 = ErrortypePeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = ErrortypePeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = ErrortypePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    ErrortypePeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (VldPdLong) to the collection in $obj2 (Errortype)
                $obj2->addVldPdLongRelatedByIdtype($obj1);

            } // if joined row is not null

                // Add objects for joined Errortype rows

                $key3 = ErrortypePeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = ErrortypePeer::getInstanceFromPool($key3);
                    if (!$obj3) {
    
                        $cls = ErrortypePeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    ErrortypePeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (VldPdLong) to the collection in $obj3 (Errortype)
                $obj3->addVldPdLongRelatedByIdtype($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of VldPdLong objects pre-filled with all related objects except ErrortypeRelatedByIdtype.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of VldPdLong objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptErrortypeRelatedByIdtype(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(VldPdLongPeer::DATABASE_NAME);
        }

        VldPdLongPeer::addSelectColumns($criteria);
        $startcol2 = VldPdLongPeer::NUM_HYDRATE_COLUMNS;

        PesertaDidikLongitudinalPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + PesertaDidikLongitudinalPeer::NUM_HYDRATE_COLUMNS;

        PesertaDidikLongitudinalPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + PesertaDidikLongitudinalPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addMultipleJoin(array(
        array(VldPdLongPeer::PESERTA_DIDIK_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::SEMESTER_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::PESERTA_DIDIK_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::SEMESTER_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
      ), $join_behavior);

        $criteria->addMultipleJoin(array(
        array(VldPdLongPeer::PESERTA_DIDIK_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::SEMESTER_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::PESERTA_DIDIK_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::SEMESTER_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
      ), $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = VldPdLongPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = VldPdLongPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = VldPdLongPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                VldPdLongPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined PesertaDidikLongitudinal rows

                $key2 = PesertaDidikLongitudinalPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = PesertaDidikLongitudinalPeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = PesertaDidikLongitudinalPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    PesertaDidikLongitudinalPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (VldPdLong) to the collection in $obj2 (PesertaDidikLongitudinal)
                $obj2->addVldPdLongRelatedByPesertaDidikIdSemesterId($obj1);

            } // if joined row is not null

                // Add objects for joined PesertaDidikLongitudinal rows

                $key3 = PesertaDidikLongitudinalPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = PesertaDidikLongitudinalPeer::getInstanceFromPool($key3);
                    if (!$obj3) {
    
                        $cls = PesertaDidikLongitudinalPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    PesertaDidikLongitudinalPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (VldPdLong) to the collection in $obj3 (PesertaDidikLongitudinal)
                $obj3->addVldPdLongRelatedByPesertaDidikIdSemesterId($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of VldPdLong objects pre-filled with all related objects except ErrortypeRelatedByIdtype.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of VldPdLong objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptErrortypeRelatedByIdtype(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(VldPdLongPeer::DATABASE_NAME);
        }

        VldPdLongPeer::addSelectColumns($criteria);
        $startcol2 = VldPdLongPeer::NUM_HYDRATE_COLUMNS;

        PesertaDidikLongitudinalPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + PesertaDidikLongitudinalPeer::NUM_HYDRATE_COLUMNS;

        PesertaDidikLongitudinalPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + PesertaDidikLongitudinalPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addMultipleJoin(array(
        array(VldPdLongPeer::PESERTA_DIDIK_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::SEMESTER_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::PESERTA_DIDIK_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::SEMESTER_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
      ), $join_behavior);

        $criteria->addMultipleJoin(array(
        array(VldPdLongPeer::PESERTA_DIDIK_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::SEMESTER_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::PESERTA_DIDIK_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
        array(VldPdLongPeer::SEMESTER_ID, PesertaDidikLongitudinalPeer::SEMESTER_ID),
      ), $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = VldPdLongPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = VldPdLongPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = VldPdLongPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                VldPdLongPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined PesertaDidikLongitudinal rows

                $key2 = PesertaDidikLongitudinalPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = PesertaDidikLongitudinalPeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = PesertaDidikLongitudinalPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    PesertaDidikLongitudinalPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (VldPdLong) to the collection in $obj2 (PesertaDidikLongitudinal)
                $obj2->addVldPdLongRelatedByPesertaDidikIdSemesterId($obj1);

            } // if joined row is not null

                // Add objects for joined PesertaDidikLongitudinal rows

                $key3 = PesertaDidikLongitudinalPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = PesertaDidikLongitudinalPeer::getInstanceFromPool($key3);
                    if (!$obj3) {
    
                        $cls = PesertaDidikLongitudinalPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    PesertaDidikLongitudinalPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (VldPdLong) to the collection in $obj3 (PesertaDidikLongitudinal)
                $obj3->addVldPdLongRelatedByPesertaDidikIdSemesterId($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(VldPdLongPeer::DATABASE_NAME)->getTable(VldPdLongPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseVldPdLongPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseVldPdLongPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new VldPdLongTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass()
    {
        return VldPdLongPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a VldPdLong or Criteria object.
     *
     * @param      mixed $values Criteria or VldPdLong object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(VldPdLongPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from VldPdLong object
        }


        // Set the correct dbName
        $criteria->setDbName(VldPdLongPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a VldPdLong or Criteria object.
     *
     * @param      mixed $values Criteria or VldPdLong object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(VldPdLongPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(VldPdLongPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(VldPdLongPeer::PESERTA_DIDIK_ID);
            $value = $criteria->remove(VldPdLongPeer::PESERTA_DIDIK_ID);
            if ($value) {
                $selectCriteria->add(VldPdLongPeer::PESERTA_DIDIK_ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(VldPdLongPeer::TABLE_NAME);
            }

            $comparison = $criteria->getComparison(VldPdLongPeer::SEMESTER_ID);
            $value = $criteria->remove(VldPdLongPeer::SEMESTER_ID);
            if ($value) {
                $selectCriteria->add(VldPdLongPeer::SEMESTER_ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(VldPdLongPeer::TABLE_NAME);
            }

            $comparison = $criteria->getComparison(VldPdLongPeer::LOGID);
            $value = $criteria->remove(VldPdLongPeer::LOGID);
            if ($value) {
                $selectCriteria->add(VldPdLongPeer::LOGID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(VldPdLongPeer::TABLE_NAME);
            }

        } else { // $values is VldPdLong object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(VldPdLongPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the vld_pd_long table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(VldPdLongPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(VldPdLongPeer::TABLE_NAME, $con, VldPdLongPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            VldPdLongPeer::clearInstancePool();
            VldPdLongPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a VldPdLong or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or VldPdLong object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(VldPdLongPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            VldPdLongPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof VldPdLong) { // it's a model object
            // invalidate the cache for this single object
            VldPdLongPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(VldPdLongPeer::DATABASE_NAME);
            // primary key is composite; we therefore, expect
            // the primary key passed to be an array of pkey values
            if (count($values) == count($values, COUNT_RECURSIVE)) {
                // array is not multi-dimensional
                $values = array($values);
            }
            foreach ($values as $value) {
                $criterion = $criteria->getNewCriterion(VldPdLongPeer::PESERTA_DIDIK_ID, $value[0]);
                $criterion->addAnd($criteria->getNewCriterion(VldPdLongPeer::SEMESTER_ID, $value[1]));
                $criterion->addAnd($criteria->getNewCriterion(VldPdLongPeer::LOGID, $value[2]));
                $criteria->addOr($criterion);
                // we can invalidate the cache for this single PK
                VldPdLongPeer::removeInstanceFromPool($value);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(VldPdLongPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            
            $affectedRows += BasePeer::doDelete($criteria, $con);
            VldPdLongPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given VldPdLong object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      VldPdLong $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(VldPdLongPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(VldPdLongPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(VldPdLongPeer::DATABASE_NAME, VldPdLongPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve object using using composite pkey values.
     * @param   string $peserta_didik_id
     * @param   string $semester_id
     * @param   string $logid
     * @param      PropelPDO $con
     * @return   VldPdLong
     */
    public static function retrieveByPK($peserta_didik_id, $semester_id, $logid, PropelPDO $con = null) {
        $_instancePoolKey = serialize(array((string) $peserta_didik_id, (string) $semester_id, (string) $logid));
         if (null !== ($obj = VldPdLongPeer::getInstanceFromPool($_instancePoolKey))) {
             return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(VldPdLongPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $criteria = new Criteria(VldPdLongPeer::DATABASE_NAME);
        $criteria->add(VldPdLongPeer::PESERTA_DIDIK_ID, $peserta_didik_id);
        $criteria->add(VldPdLongPeer::SEMESTER_ID, $semester_id);
        $criteria->add(VldPdLongPeer::LOGID, $logid);
        $v = VldPdLongPeer::doSelect($criteria, $con);

        return !empty($v) ? $v[0] : null;
    }
} // BaseVldPdLongPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseVldPdLongPeer::buildTableMap();

