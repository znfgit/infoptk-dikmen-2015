<?php

namespace angulex\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelDateTime;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\Agama;
use angulex\Model\AgamaQuery;
use angulex\Model\Anak;
use angulex\Model\AnakQuery;
use angulex\Model\BeasiswaPtk;
use angulex\Model\BeasiswaPtkQuery;
use angulex\Model\BidangStudi;
use angulex\Model\BidangStudiQuery;
use angulex\Model\BukuPtk;
use angulex\Model\BukuPtkQuery;
use angulex\Model\Diklat;
use angulex\Model\DiklatQuery;
use angulex\Model\Inpassing;
use angulex\Model\InpassingQuery;
use angulex\Model\JenisPtk;
use angulex\Model\JenisPtkQuery;
use angulex\Model\KaryaTulis;
use angulex\Model\KaryaTulisQuery;
use angulex\Model\KeahlianLaboratorium;
use angulex\Model\KeahlianLaboratoriumQuery;
use angulex\Model\KebutuhanKhusus;
use angulex\Model\KebutuhanKhususQuery;
use angulex\Model\Kesejahteraan;
use angulex\Model\KesejahteraanQuery;
use angulex\Model\LembagaPengangkat;
use angulex\Model\LembagaPengangkatQuery;
use angulex\Model\MstWilayah;
use angulex\Model\MstWilayahQuery;
use angulex\Model\Negara;
use angulex\Model\NegaraQuery;
use angulex\Model\NilaiTest;
use angulex\Model\NilaiTestQuery;
use angulex\Model\PangkatGolongan;
use angulex\Model\PangkatGolonganQuery;
use angulex\Model\Pekerjaan;
use angulex\Model\PekerjaanQuery;
use angulex\Model\PengawasTerdaftar;
use angulex\Model\PengawasTerdaftarQuery;
use angulex\Model\Penghargaan;
use angulex\Model\PenghargaanQuery;
use angulex\Model\Ptk;
use angulex\Model\PtkBaru;
use angulex\Model\PtkBaruQuery;
use angulex\Model\PtkPeer;
use angulex\Model\PtkQuery;
use angulex\Model\PtkTerdaftar;
use angulex\Model\PtkTerdaftarQuery;
use angulex\Model\RiwayatGajiBerkala;
use angulex\Model\RiwayatGajiBerkalaQuery;
use angulex\Model\RombonganBelajar;
use angulex\Model\RombonganBelajarQuery;
use angulex\Model\RwyFungsional;
use angulex\Model\RwyFungsionalQuery;
use angulex\Model\RwyKepangkatan;
use angulex\Model\RwyKepangkatanQuery;
use angulex\Model\RwyPendFormal;
use angulex\Model\RwyPendFormalQuery;
use angulex\Model\RwySertifikasi;
use angulex\Model\RwySertifikasiQuery;
use angulex\Model\RwyStruktural;
use angulex\Model\RwyStrukturalQuery;
use angulex\Model\Sekolah;
use angulex\Model\SekolahQuery;
use angulex\Model\StatusKeaktifanPegawai;
use angulex\Model\StatusKeaktifanPegawaiQuery;
use angulex\Model\StatusKepegawaian;
use angulex\Model\StatusKepegawaianQuery;
use angulex\Model\SumberGaji;
use angulex\Model\SumberGajiQuery;
use angulex\Model\TugasTambahan;
use angulex\Model\TugasTambahanQuery;
use angulex\Model\Tunjangan;
use angulex\Model\TunjanganQuery;
use angulex\Model\VldPtk;
use angulex\Model\VldPtkQuery;

/**
 * Base class that represents a row from the 'ptk' table.
 *
 * 
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BasePtk extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'angulex\\Model\\PtkPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        PtkPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the ptk_id field.
     * @var        string
     */
    protected $ptk_id;

    /**
     * The value for the nama field.
     * @var        string
     */
    protected $nama;

    /**
     * The value for the nip field.
     * @var        string
     */
    protected $nip;

    /**
     * The value for the jenis_kelamin field.
     * @var        string
     */
    protected $jenis_kelamin;

    /**
     * The value for the tempat_lahir field.
     * @var        string
     */
    protected $tempat_lahir;

    /**
     * The value for the tanggal_lahir field.
     * @var        string
     */
    protected $tanggal_lahir;

    /**
     * The value for the nik field.
     * @var        string
     */
    protected $nik;

    /**
     * The value for the niy_nigk field.
     * @var        string
     */
    protected $niy_nigk;

    /**
     * The value for the nuptk field.
     * @var        string
     */
    protected $nuptk;

    /**
     * The value for the status_kepegawaian_id field.
     * @var        int
     */
    protected $status_kepegawaian_id;

    /**
     * The value for the jenis_ptk_id field.
     * @var        string
     */
    protected $jenis_ptk_id;

    /**
     * The value for the pengawas_bidang_studi_id field.
     * @var        int
     */
    protected $pengawas_bidang_studi_id;

    /**
     * The value for the agama_id field.
     * @var        int
     */
    protected $agama_id;

    /**
     * The value for the kewarganegaraan field.
     * @var        string
     */
    protected $kewarganegaraan;

    /**
     * The value for the alamat_jalan field.
     * @var        string
     */
    protected $alamat_jalan;

    /**
     * The value for the rt field.
     * @var        string
     */
    protected $rt;

    /**
     * The value for the rw field.
     * @var        string
     */
    protected $rw;

    /**
     * The value for the nama_dusun field.
     * @var        string
     */
    protected $nama_dusun;

    /**
     * The value for the desa_kelurahan field.
     * @var        string
     */
    protected $desa_kelurahan;

    /**
     * The value for the kode_wilayah field.
     * @var        string
     */
    protected $kode_wilayah;

    /**
     * The value for the kode_pos field.
     * @var        string
     */
    protected $kode_pos;

    /**
     * The value for the no_telepon_rumah field.
     * @var        string
     */
    protected $no_telepon_rumah;

    /**
     * The value for the no_hp field.
     * @var        string
     */
    protected $no_hp;

    /**
     * The value for the email field.
     * @var        string
     */
    protected $email;

    /**
     * The value for the entry_sekolah_id field.
     * @var        string
     */
    protected $entry_sekolah_id;

    /**
     * The value for the status_keaktifan_id field.
     * @var        string
     */
    protected $status_keaktifan_id;

    /**
     * The value for the sk_cpns field.
     * @var        string
     */
    protected $sk_cpns;

    /**
     * The value for the tgl_cpns field.
     * @var        string
     */
    protected $tgl_cpns;

    /**
     * The value for the sk_pengangkatan field.
     * @var        string
     */
    protected $sk_pengangkatan;

    /**
     * The value for the tmt_pengangkatan field.
     * @var        string
     */
    protected $tmt_pengangkatan;

    /**
     * The value for the lembaga_pengangkat_id field.
     * @var        string
     */
    protected $lembaga_pengangkat_id;

    /**
     * The value for the pangkat_golongan_id field.
     * @var        string
     */
    protected $pangkat_golongan_id;

    /**
     * The value for the keahlian_laboratorium_id field.
     * @var        int
     */
    protected $keahlian_laboratorium_id;

    /**
     * The value for the sumber_gaji_id field.
     * @var        string
     */
    protected $sumber_gaji_id;

    /**
     * The value for the nama_ibu_kandung field.
     * @var        string
     */
    protected $nama_ibu_kandung;

    /**
     * The value for the status_perkawinan field.
     * @var        string
     */
    protected $status_perkawinan;

    /**
     * The value for the nama_suami_istri field.
     * @var        string
     */
    protected $nama_suami_istri;

    /**
     * The value for the nip_suami_istri field.
     * @var        string
     */
    protected $nip_suami_istri;

    /**
     * The value for the pekerjaan_suami_istri field.
     * @var        int
     */
    protected $pekerjaan_suami_istri;

    /**
     * The value for the tmt_pns field.
     * @var        string
     */
    protected $tmt_pns;

    /**
     * The value for the sudah_lisensi_kepala_sekolah field.
     * @var        string
     */
    protected $sudah_lisensi_kepala_sekolah;

    /**
     * The value for the jumlah_sekolah_binaan field.
     * @var        int
     */
    protected $jumlah_sekolah_binaan;

    /**
     * The value for the pernah_diklat_kepengawasan field.
     * @var        string
     */
    protected $pernah_diklat_kepengawasan;

    /**
     * The value for the status_data field.
     * @var        int
     */
    protected $status_data;

    /**
     * The value for the mampu_handle_kk field.
     * @var        int
     */
    protected $mampu_handle_kk;

    /**
     * The value for the keahlian_braille field.
     * Note: this column has a database default value of: '((0))'
     * @var        string
     */
    protected $keahlian_braille;

    /**
     * The value for the keahlian_bhs_isyarat field.
     * Note: this column has a database default value of: '((0))'
     * @var        string
     */
    protected $keahlian_bhs_isyarat;

    /**
     * The value for the npwp field.
     * @var        string
     */
    protected $npwp;

    /**
     * The value for the last_update field.
     * @var        string
     */
    protected $last_update;

    /**
     * The value for the soft_delete field.
     * @var        string
     */
    protected $soft_delete;

    /**
     * The value for the last_sync field.
     * @var        string
     */
    protected $last_sync;

    /**
     * The value for the updater_id field.
     * @var        string
     */
    protected $updater_id;

    /**
     * @var        Sekolah
     */
    protected $aSekolahRelatedByEntrySekolahId;

    /**
     * @var        Sekolah
     */
    protected $aSekolahRelatedByEntrySekolahId;

    /**
     * @var        Agama
     */
    protected $aAgamaRelatedByAgamaId;

    /**
     * @var        Agama
     */
    protected $aAgamaRelatedByAgamaId;

    /**
     * @var        BidangStudi
     */
    protected $aBidangStudiRelatedByPengawasBidangStudiId;

    /**
     * @var        BidangStudi
     */
    protected $aBidangStudiRelatedByPengawasBidangStudiId;

    /**
     * @var        JenisPtk
     */
    protected $aJenisPtkRelatedByJenisPtkId;

    /**
     * @var        JenisPtk
     */
    protected $aJenisPtkRelatedByJenisPtkId;

    /**
     * @var        KeahlianLaboratorium
     */
    protected $aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId;

    /**
     * @var        KeahlianLaboratorium
     */
    protected $aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId;

    /**
     * @var        KebutuhanKhusus
     */
    protected $aKebutuhanKhususRelatedByMampuHandleKk;

    /**
     * @var        KebutuhanKhusus
     */
    protected $aKebutuhanKhususRelatedByMampuHandleKk;

    /**
     * @var        LembagaPengangkat
     */
    protected $aLembagaPengangkatRelatedByLembagaPengangkatId;

    /**
     * @var        LembagaPengangkat
     */
    protected $aLembagaPengangkatRelatedByLembagaPengangkatId;

    /**
     * @var        MstWilayah
     */
    protected $aMstWilayahRelatedByKodeWilayah;

    /**
     * @var        MstWilayah
     */
    protected $aMstWilayahRelatedByKodeWilayah;

    /**
     * @var        Negara
     */
    protected $aNegaraRelatedByKewarganegaraan;

    /**
     * @var        Negara
     */
    protected $aNegaraRelatedByKewarganegaraan;

    /**
     * @var        PangkatGolongan
     */
    protected $aPangkatGolonganRelatedByPangkatGolonganId;

    /**
     * @var        PangkatGolongan
     */
    protected $aPangkatGolonganRelatedByPangkatGolonganId;

    /**
     * @var        Pekerjaan
     */
    protected $aPekerjaanRelatedByPekerjaanSuamiIstri;

    /**
     * @var        Pekerjaan
     */
    protected $aPekerjaanRelatedByPekerjaanSuamiIstri;

    /**
     * @var        StatusKepegawaian
     */
    protected $aStatusKepegawaianRelatedByStatusKepegawaianId;

    /**
     * @var        StatusKepegawaian
     */
    protected $aStatusKepegawaianRelatedByStatusKepegawaianId;

    /**
     * @var        StatusKeaktifanPegawai
     */
    protected $aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId;

    /**
     * @var        StatusKeaktifanPegawai
     */
    protected $aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId;

    /**
     * @var        SumberGaji
     */
    protected $aSumberGajiRelatedBySumberGajiId;

    /**
     * @var        SumberGaji
     */
    protected $aSumberGajiRelatedBySumberGajiId;

    /**
     * @var        PropelObjectCollection|Kesejahteraan[] Collection to store aggregation of Kesejahteraan objects.
     */
    protected $collKesejahteraansRelatedByPtkId;
    protected $collKesejahteraansRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|Kesejahteraan[] Collection to store aggregation of Kesejahteraan objects.
     */
    protected $collKesejahteraansRelatedByPtkId;
    protected $collKesejahteraansRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|VldPtk[] Collection to store aggregation of VldPtk objects.
     */
    protected $collVldPtksRelatedByPtkId;
    protected $collVldPtksRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|VldPtk[] Collection to store aggregation of VldPtk objects.
     */
    protected $collVldPtksRelatedByPtkId;
    protected $collVldPtksRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|Penghargaan[] Collection to store aggregation of Penghargaan objects.
     */
    protected $collPenghargaansRelatedByPtkId;
    protected $collPenghargaansRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|Penghargaan[] Collection to store aggregation of Penghargaan objects.
     */
    protected $collPenghargaansRelatedByPtkId;
    protected $collPenghargaansRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|Inpassing[] Collection to store aggregation of Inpassing objects.
     */
    protected $collInpassingsRelatedByPtkId;
    protected $collInpassingsRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|Inpassing[] Collection to store aggregation of Inpassing objects.
     */
    protected $collInpassingsRelatedByPtkId;
    protected $collInpassingsRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|PtkTerdaftar[] Collection to store aggregation of PtkTerdaftar objects.
     */
    protected $collPtkTerdaftarsRelatedByPtkId;
    protected $collPtkTerdaftarsRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|PtkTerdaftar[] Collection to store aggregation of PtkTerdaftar objects.
     */
    protected $collPtkTerdaftarsRelatedByPtkId;
    protected $collPtkTerdaftarsRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|KaryaTulis[] Collection to store aggregation of KaryaTulis objects.
     */
    protected $collKaryaTulissRelatedByPtkId;
    protected $collKaryaTulissRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|KaryaTulis[] Collection to store aggregation of KaryaTulis objects.
     */
    protected $collKaryaTulissRelatedByPtkId;
    protected $collKaryaTulissRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|NilaiTest[] Collection to store aggregation of NilaiTest objects.
     */
    protected $collNilaiTestsRelatedByPtkId;
    protected $collNilaiTestsRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|NilaiTest[] Collection to store aggregation of NilaiTest objects.
     */
    protected $collNilaiTestsRelatedByPtkId;
    protected $collNilaiTestsRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|BukuPtk[] Collection to store aggregation of BukuPtk objects.
     */
    protected $collBukuPtksRelatedByPtkId;
    protected $collBukuPtksRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|BukuPtk[] Collection to store aggregation of BukuPtk objects.
     */
    protected $collBukuPtksRelatedByPtkId;
    protected $collBukuPtksRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|BeasiswaPtk[] Collection to store aggregation of BeasiswaPtk objects.
     */
    protected $collBeasiswaPtksRelatedByPtkId;
    protected $collBeasiswaPtksRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|BeasiswaPtk[] Collection to store aggregation of BeasiswaPtk objects.
     */
    protected $collBeasiswaPtksRelatedByPtkId;
    protected $collBeasiswaPtksRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|RwyKepangkatan[] Collection to store aggregation of RwyKepangkatan objects.
     */
    protected $collRwyKepangkatansRelatedByPtkId;
    protected $collRwyKepangkatansRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|RwyKepangkatan[] Collection to store aggregation of RwyKepangkatan objects.
     */
    protected $collRwyKepangkatansRelatedByPtkId;
    protected $collRwyKepangkatansRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|RombonganBelajar[] Collection to store aggregation of RombonganBelajar objects.
     */
    protected $collRombonganBelajarsRelatedByPtkId;
    protected $collRombonganBelajarsRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|RombonganBelajar[] Collection to store aggregation of RombonganBelajar objects.
     */
    protected $collRombonganBelajarsRelatedByPtkId;
    protected $collRombonganBelajarsRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|RiwayatGajiBerkala[] Collection to store aggregation of RiwayatGajiBerkala objects.
     */
    protected $collRiwayatGajiBerkalasRelatedByPtkId;
    protected $collRiwayatGajiBerkalasRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|RiwayatGajiBerkala[] Collection to store aggregation of RiwayatGajiBerkala objects.
     */
    protected $collRiwayatGajiBerkalasRelatedByPtkId;
    protected $collRiwayatGajiBerkalasRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|PengawasTerdaftar[] Collection to store aggregation of PengawasTerdaftar objects.
     */
    protected $collPengawasTerdaftarsRelatedByPtkId;
    protected $collPengawasTerdaftarsRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|PengawasTerdaftar[] Collection to store aggregation of PengawasTerdaftar objects.
     */
    protected $collPengawasTerdaftarsRelatedByPtkId;
    protected $collPengawasTerdaftarsRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|RwyStruktural[] Collection to store aggregation of RwyStruktural objects.
     */
    protected $collRwyStrukturalsRelatedByPtkId;
    protected $collRwyStrukturalsRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|RwyStruktural[] Collection to store aggregation of RwyStruktural objects.
     */
    protected $collRwyStrukturalsRelatedByPtkId;
    protected $collRwyStrukturalsRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|RwyPendFormal[] Collection to store aggregation of RwyPendFormal objects.
     */
    protected $collRwyPendFormalsRelatedByPtkId;
    protected $collRwyPendFormalsRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|RwyPendFormal[] Collection to store aggregation of RwyPendFormal objects.
     */
    protected $collRwyPendFormalsRelatedByPtkId;
    protected $collRwyPendFormalsRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|Anak[] Collection to store aggregation of Anak objects.
     */
    protected $collAnaksRelatedByPtkId;
    protected $collAnaksRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|Anak[] Collection to store aggregation of Anak objects.
     */
    protected $collAnaksRelatedByPtkId;
    protected $collAnaksRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|Diklat[] Collection to store aggregation of Diklat objects.
     */
    protected $collDiklatsRelatedByPtkId;
    protected $collDiklatsRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|Diklat[] Collection to store aggregation of Diklat objects.
     */
    protected $collDiklatsRelatedByPtkId;
    protected $collDiklatsRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|PtkBaru[] Collection to store aggregation of PtkBaru objects.
     */
    protected $collPtkBarusRelatedByPtkId;
    protected $collPtkBarusRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|PtkBaru[] Collection to store aggregation of PtkBaru objects.
     */
    protected $collPtkBarusRelatedByPtkId;
    protected $collPtkBarusRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|Tunjangan[] Collection to store aggregation of Tunjangan objects.
     */
    protected $collTunjangansRelatedByPtkId;
    protected $collTunjangansRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|Tunjangan[] Collection to store aggregation of Tunjangan objects.
     */
    protected $collTunjangansRelatedByPtkId;
    protected $collTunjangansRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|RwySertifikasi[] Collection to store aggregation of RwySertifikasi objects.
     */
    protected $collRwySertifikasisRelatedByPtkId;
    protected $collRwySertifikasisRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|RwySertifikasi[] Collection to store aggregation of RwySertifikasi objects.
     */
    protected $collRwySertifikasisRelatedByPtkId;
    protected $collRwySertifikasisRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|TugasTambahan[] Collection to store aggregation of TugasTambahan objects.
     */
    protected $collTugasTambahansRelatedByPtkId;
    protected $collTugasTambahansRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|TugasTambahan[] Collection to store aggregation of TugasTambahan objects.
     */
    protected $collTugasTambahansRelatedByPtkId;
    protected $collTugasTambahansRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|RwyFungsional[] Collection to store aggregation of RwyFungsional objects.
     */
    protected $collRwyFungsionalsRelatedByPtkId;
    protected $collRwyFungsionalsRelatedByPtkIdPartial;

    /**
     * @var        PropelObjectCollection|RwyFungsional[] Collection to store aggregation of RwyFungsional objects.
     */
    protected $collRwyFungsionalsRelatedByPtkId;
    protected $collRwyFungsionalsRelatedByPtkIdPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $kesejahteraansRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $kesejahteraansRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldPtksRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldPtksRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $penghargaansRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $penghargaansRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $inpassingsRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $inpassingsRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $ptkTerdaftarsRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $ptkTerdaftarsRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $karyaTulissRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $karyaTulissRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $nilaiTestsRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $nilaiTestsRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $bukuPtksRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $bukuPtksRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $beasiswaPtksRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $beasiswaPtksRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rwyKepangkatansRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rwyKepangkatansRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rombonganBelajarsRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rombonganBelajarsRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $riwayatGajiBerkalasRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $riwayatGajiBerkalasRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $pengawasTerdaftarsRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $pengawasTerdaftarsRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rwyStrukturalsRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rwyStrukturalsRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rwyPendFormalsRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rwyPendFormalsRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $anaksRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $anaksRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $diklatsRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $diklatsRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $ptkBarusRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $ptkBarusRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $tunjangansRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $tunjangansRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rwySertifikasisRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rwySertifikasisRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $tugasTambahansRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $tugasTambahansRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rwyFungsionalsRelatedByPtkIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rwyFungsionalsRelatedByPtkIdScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->keahlian_braille = '((0))';
        $this->keahlian_bhs_isyarat = '((0))';
    }

    /**
     * Initializes internal state of BasePtk object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [ptk_id] column value.
     * 
     * @return string
     */
    public function getPtkId()
    {
        return $this->ptk_id;
    }

    /**
     * Get the [nama] column value.
     * 
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Get the [nip] column value.
     * 
     * @return string
     */
    public function getNip()
    {
        return $this->nip;
    }

    /**
     * Get the [jenis_kelamin] column value.
     * 
     * @return string
     */
    public function getJenisKelamin()
    {
        return $this->jenis_kelamin;
    }

    /**
     * Get the [tempat_lahir] column value.
     * 
     * @return string
     */
    public function getTempatLahir()
    {
        return $this->tempat_lahir;
    }

    /**
     * Get the [tanggal_lahir] column value.
     * 
     * @return string
     */
    public function getTanggalLahir()
    {
        return $this->tanggal_lahir;
    }

    /**
     * Get the [nik] column value.
     * 
     * @return string
     */
    public function getNik()
    {
        return $this->nik;
    }

    /**
     * Get the [niy_nigk] column value.
     * 
     * @return string
     */
    public function getNiyNigk()
    {
        return $this->niy_nigk;
    }

    /**
     * Get the [nuptk] column value.
     * 
     * @return string
     */
    public function getNuptk()
    {
        return $this->nuptk;
    }

    /**
     * Get the [status_kepegawaian_id] column value.
     * 
     * @return int
     */
    public function getStatusKepegawaianId()
    {
        return $this->status_kepegawaian_id;
    }

    /**
     * Get the [jenis_ptk_id] column value.
     * 
     * @return string
     */
    public function getJenisPtkId()
    {
        return $this->jenis_ptk_id;
    }

    /**
     * Get the [pengawas_bidang_studi_id] column value.
     * 
     * @return int
     */
    public function getPengawasBidangStudiId()
    {
        return $this->pengawas_bidang_studi_id;
    }

    /**
     * Get the [agama_id] column value.
     * 
     * @return int
     */
    public function getAgamaId()
    {
        return $this->agama_id;
    }

    /**
     * Get the [kewarganegaraan] column value.
     * 
     * @return string
     */
    public function getKewarganegaraan()
    {
        return $this->kewarganegaraan;
    }

    /**
     * Get the [alamat_jalan] column value.
     * 
     * @return string
     */
    public function getAlamatJalan()
    {
        return $this->alamat_jalan;
    }

    /**
     * Get the [rt] column value.
     * 
     * @return string
     */
    public function getRt()
    {
        return $this->rt;
    }

    /**
     * Get the [rw] column value.
     * 
     * @return string
     */
    public function getRw()
    {
        return $this->rw;
    }

    /**
     * Get the [nama_dusun] column value.
     * 
     * @return string
     */
    public function getNamaDusun()
    {
        return $this->nama_dusun;
    }

    /**
     * Get the [desa_kelurahan] column value.
     * 
     * @return string
     */
    public function getDesaKelurahan()
    {
        return $this->desa_kelurahan;
    }

    /**
     * Get the [kode_wilayah] column value.
     * 
     * @return string
     */
    public function getKodeWilayah()
    {
        return $this->kode_wilayah;
    }

    /**
     * Get the [kode_pos] column value.
     * 
     * @return string
     */
    public function getKodePos()
    {
        return $this->kode_pos;
    }

    /**
     * Get the [no_telepon_rumah] column value.
     * 
     * @return string
     */
    public function getNoTeleponRumah()
    {
        return $this->no_telepon_rumah;
    }

    /**
     * Get the [no_hp] column value.
     * 
     * @return string
     */
    public function getNoHp()
    {
        return $this->no_hp;
    }

    /**
     * Get the [email] column value.
     * 
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Get the [entry_sekolah_id] column value.
     * 
     * @return string
     */
    public function getEntrySekolahId()
    {
        return $this->entry_sekolah_id;
    }

    /**
     * Get the [status_keaktifan_id] column value.
     * 
     * @return string
     */
    public function getStatusKeaktifanId()
    {
        return $this->status_keaktifan_id;
    }

    /**
     * Get the [sk_cpns] column value.
     * 
     * @return string
     */
    public function getSkCpns()
    {
        return $this->sk_cpns;
    }

    /**
     * Get the [tgl_cpns] column value.
     * 
     * @return string
     */
    public function getTglCpns()
    {
        return $this->tgl_cpns;
    }

    /**
     * Get the [sk_pengangkatan] column value.
     * 
     * @return string
     */
    public function getSkPengangkatan()
    {
        return $this->sk_pengangkatan;
    }

    /**
     * Get the [tmt_pengangkatan] column value.
     * 
     * @return string
     */
    public function getTmtPengangkatan()
    {
        return $this->tmt_pengangkatan;
    }

    /**
     * Get the [lembaga_pengangkat_id] column value.
     * 
     * @return string
     */
    public function getLembagaPengangkatId()
    {
        return $this->lembaga_pengangkat_id;
    }

    /**
     * Get the [pangkat_golongan_id] column value.
     * 
     * @return string
     */
    public function getPangkatGolonganId()
    {
        return $this->pangkat_golongan_id;
    }

    /**
     * Get the [keahlian_laboratorium_id] column value.
     * 
     * @return int
     */
    public function getKeahlianLaboratoriumId()
    {
        return $this->keahlian_laboratorium_id;
    }

    /**
     * Get the [sumber_gaji_id] column value.
     * 
     * @return string
     */
    public function getSumberGajiId()
    {
        return $this->sumber_gaji_id;
    }

    /**
     * Get the [nama_ibu_kandung] column value.
     * 
     * @return string
     */
    public function getNamaIbuKandung()
    {
        return $this->nama_ibu_kandung;
    }

    /**
     * Get the [status_perkawinan] column value.
     * 
     * @return string
     */
    public function getStatusPerkawinan()
    {
        return $this->status_perkawinan;
    }

    /**
     * Get the [nama_suami_istri] column value.
     * 
     * @return string
     */
    public function getNamaSuamiIstri()
    {
        return $this->nama_suami_istri;
    }

    /**
     * Get the [nip_suami_istri] column value.
     * 
     * @return string
     */
    public function getNipSuamiIstri()
    {
        return $this->nip_suami_istri;
    }

    /**
     * Get the [pekerjaan_suami_istri] column value.
     * 
     * @return int
     */
    public function getPekerjaanSuamiIstri()
    {
        return $this->pekerjaan_suami_istri;
    }

    /**
     * Get the [tmt_pns] column value.
     * 
     * @return string
     */
    public function getTmtPns()
    {
        return $this->tmt_pns;
    }

    /**
     * Get the [sudah_lisensi_kepala_sekolah] column value.
     * 
     * @return string
     */
    public function getSudahLisensiKepalaSekolah()
    {
        return $this->sudah_lisensi_kepala_sekolah;
    }

    /**
     * Get the [jumlah_sekolah_binaan] column value.
     * 
     * @return int
     */
    public function getJumlahSekolahBinaan()
    {
        return $this->jumlah_sekolah_binaan;
    }

    /**
     * Get the [pernah_diklat_kepengawasan] column value.
     * 
     * @return string
     */
    public function getPernahDiklatKepengawasan()
    {
        return $this->pernah_diklat_kepengawasan;
    }

    /**
     * Get the [status_data] column value.
     * 
     * @return int
     */
    public function getStatusData()
    {
        return $this->status_data;
    }

    /**
     * Get the [mampu_handle_kk] column value.
     * 
     * @return int
     */
    public function getMampuHandleKk()
    {
        return $this->mampu_handle_kk;
    }

    /**
     * Get the [keahlian_braille] column value.
     * 
     * @return string
     */
    public function getKeahlianBraille()
    {
        return $this->keahlian_braille;
    }

    /**
     * Get the [keahlian_bhs_isyarat] column value.
     * 
     * @return string
     */
    public function getKeahlianBhsIsyarat()
    {
        return $this->keahlian_bhs_isyarat;
    }

    /**
     * Get the [npwp] column value.
     * 
     * @return string
     */
    public function getNpwp()
    {
        return $this->npwp;
    }

    /**
     * Get the [optionally formatted] temporal [last_update] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastUpdate($format = 'Y-m-d H:i:s')
    {
        if ($this->last_update === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_update);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_update, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [soft_delete] column value.
     * 
     * @return string
     */
    public function getSoftDelete()
    {
        return $this->soft_delete;
    }

    /**
     * Get the [optionally formatted] temporal [last_sync] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastSync($format = 'Y-m-d H:i:s')
    {
        if ($this->last_sync === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_sync);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_sync, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [updater_id] column value.
     * 
     * @return string
     */
    public function getUpdaterId()
    {
        return $this->updater_id;
    }

    /**
     * Set the value of [ptk_id] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setPtkId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->ptk_id !== $v) {
            $this->ptk_id = $v;
            $this->modifiedColumns[] = PtkPeer::PTK_ID;
        }


        return $this;
    } // setPtkId()

    /**
     * Set the value of [nama] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setNama($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nama !== $v) {
            $this->nama = $v;
            $this->modifiedColumns[] = PtkPeer::NAMA;
        }


        return $this;
    } // setNama()

    /**
     * Set the value of [nip] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setNip($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nip !== $v) {
            $this->nip = $v;
            $this->modifiedColumns[] = PtkPeer::NIP;
        }


        return $this;
    } // setNip()

    /**
     * Set the value of [jenis_kelamin] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setJenisKelamin($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->jenis_kelamin !== $v) {
            $this->jenis_kelamin = $v;
            $this->modifiedColumns[] = PtkPeer::JENIS_KELAMIN;
        }


        return $this;
    } // setJenisKelamin()

    /**
     * Set the value of [tempat_lahir] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setTempatLahir($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tempat_lahir !== $v) {
            $this->tempat_lahir = $v;
            $this->modifiedColumns[] = PtkPeer::TEMPAT_LAHIR;
        }


        return $this;
    } // setTempatLahir()

    /**
     * Set the value of [tanggal_lahir] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setTanggalLahir($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tanggal_lahir !== $v) {
            $this->tanggal_lahir = $v;
            $this->modifiedColumns[] = PtkPeer::TANGGAL_LAHIR;
        }


        return $this;
    } // setTanggalLahir()

    /**
     * Set the value of [nik] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setNik($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nik !== $v) {
            $this->nik = $v;
            $this->modifiedColumns[] = PtkPeer::NIK;
        }


        return $this;
    } // setNik()

    /**
     * Set the value of [niy_nigk] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setNiyNigk($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->niy_nigk !== $v) {
            $this->niy_nigk = $v;
            $this->modifiedColumns[] = PtkPeer::NIY_NIGK;
        }


        return $this;
    } // setNiyNigk()

    /**
     * Set the value of [nuptk] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setNuptk($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nuptk !== $v) {
            $this->nuptk = $v;
            $this->modifiedColumns[] = PtkPeer::NUPTK;
        }


        return $this;
    } // setNuptk()

    /**
     * Set the value of [status_kepegawaian_id] column.
     * 
     * @param int $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setStatusKepegawaianId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->status_kepegawaian_id !== $v) {
            $this->status_kepegawaian_id = $v;
            $this->modifiedColumns[] = PtkPeer::STATUS_KEPEGAWAIAN_ID;
        }

        if ($this->aStatusKepegawaianRelatedByStatusKepegawaianId !== null && $this->aStatusKepegawaianRelatedByStatusKepegawaianId->getStatusKepegawaianId() !== $v) {
            $this->aStatusKepegawaianRelatedByStatusKepegawaianId = null;
        }

        if ($this->aStatusKepegawaianRelatedByStatusKepegawaianId !== null && $this->aStatusKepegawaianRelatedByStatusKepegawaianId->getStatusKepegawaianId() !== $v) {
            $this->aStatusKepegawaianRelatedByStatusKepegawaianId = null;
        }


        return $this;
    } // setStatusKepegawaianId()

    /**
     * Set the value of [jenis_ptk_id] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setJenisPtkId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->jenis_ptk_id !== $v) {
            $this->jenis_ptk_id = $v;
            $this->modifiedColumns[] = PtkPeer::JENIS_PTK_ID;
        }

        if ($this->aJenisPtkRelatedByJenisPtkId !== null && $this->aJenisPtkRelatedByJenisPtkId->getJenisPtkId() !== $v) {
            $this->aJenisPtkRelatedByJenisPtkId = null;
        }

        if ($this->aJenisPtkRelatedByJenisPtkId !== null && $this->aJenisPtkRelatedByJenisPtkId->getJenisPtkId() !== $v) {
            $this->aJenisPtkRelatedByJenisPtkId = null;
        }


        return $this;
    } // setJenisPtkId()

    /**
     * Set the value of [pengawas_bidang_studi_id] column.
     * 
     * @param int $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setPengawasBidangStudiId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->pengawas_bidang_studi_id !== $v) {
            $this->pengawas_bidang_studi_id = $v;
            $this->modifiedColumns[] = PtkPeer::PENGAWAS_BIDANG_STUDI_ID;
        }

        if ($this->aBidangStudiRelatedByPengawasBidangStudiId !== null && $this->aBidangStudiRelatedByPengawasBidangStudiId->getBidangStudiId() !== $v) {
            $this->aBidangStudiRelatedByPengawasBidangStudiId = null;
        }

        if ($this->aBidangStudiRelatedByPengawasBidangStudiId !== null && $this->aBidangStudiRelatedByPengawasBidangStudiId->getBidangStudiId() !== $v) {
            $this->aBidangStudiRelatedByPengawasBidangStudiId = null;
        }


        return $this;
    } // setPengawasBidangStudiId()

    /**
     * Set the value of [agama_id] column.
     * 
     * @param int $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setAgamaId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->agama_id !== $v) {
            $this->agama_id = $v;
            $this->modifiedColumns[] = PtkPeer::AGAMA_ID;
        }

        if ($this->aAgamaRelatedByAgamaId !== null && $this->aAgamaRelatedByAgamaId->getAgamaId() !== $v) {
            $this->aAgamaRelatedByAgamaId = null;
        }

        if ($this->aAgamaRelatedByAgamaId !== null && $this->aAgamaRelatedByAgamaId->getAgamaId() !== $v) {
            $this->aAgamaRelatedByAgamaId = null;
        }


        return $this;
    } // setAgamaId()

    /**
     * Set the value of [kewarganegaraan] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setKewarganegaraan($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->kewarganegaraan !== $v) {
            $this->kewarganegaraan = $v;
            $this->modifiedColumns[] = PtkPeer::KEWARGANEGARAAN;
        }

        if ($this->aNegaraRelatedByKewarganegaraan !== null && $this->aNegaraRelatedByKewarganegaraan->getNegaraId() !== $v) {
            $this->aNegaraRelatedByKewarganegaraan = null;
        }

        if ($this->aNegaraRelatedByKewarganegaraan !== null && $this->aNegaraRelatedByKewarganegaraan->getNegaraId() !== $v) {
            $this->aNegaraRelatedByKewarganegaraan = null;
        }


        return $this;
    } // setKewarganegaraan()

    /**
     * Set the value of [alamat_jalan] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setAlamatJalan($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->alamat_jalan !== $v) {
            $this->alamat_jalan = $v;
            $this->modifiedColumns[] = PtkPeer::ALAMAT_JALAN;
        }


        return $this;
    } // setAlamatJalan()

    /**
     * Set the value of [rt] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setRt($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->rt !== $v) {
            $this->rt = $v;
            $this->modifiedColumns[] = PtkPeer::RT;
        }


        return $this;
    } // setRt()

    /**
     * Set the value of [rw] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setRw($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->rw !== $v) {
            $this->rw = $v;
            $this->modifiedColumns[] = PtkPeer::RW;
        }


        return $this;
    } // setRw()

    /**
     * Set the value of [nama_dusun] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setNamaDusun($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nama_dusun !== $v) {
            $this->nama_dusun = $v;
            $this->modifiedColumns[] = PtkPeer::NAMA_DUSUN;
        }


        return $this;
    } // setNamaDusun()

    /**
     * Set the value of [desa_kelurahan] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setDesaKelurahan($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->desa_kelurahan !== $v) {
            $this->desa_kelurahan = $v;
            $this->modifiedColumns[] = PtkPeer::DESA_KELURAHAN;
        }


        return $this;
    } // setDesaKelurahan()

    /**
     * Set the value of [kode_wilayah] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setKodeWilayah($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->kode_wilayah !== $v) {
            $this->kode_wilayah = $v;
            $this->modifiedColumns[] = PtkPeer::KODE_WILAYAH;
        }

        if ($this->aMstWilayahRelatedByKodeWilayah !== null && $this->aMstWilayahRelatedByKodeWilayah->getKodeWilayah() !== $v) {
            $this->aMstWilayahRelatedByKodeWilayah = null;
        }

        if ($this->aMstWilayahRelatedByKodeWilayah !== null && $this->aMstWilayahRelatedByKodeWilayah->getKodeWilayah() !== $v) {
            $this->aMstWilayahRelatedByKodeWilayah = null;
        }


        return $this;
    } // setKodeWilayah()

    /**
     * Set the value of [kode_pos] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setKodePos($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->kode_pos !== $v) {
            $this->kode_pos = $v;
            $this->modifiedColumns[] = PtkPeer::KODE_POS;
        }


        return $this;
    } // setKodePos()

    /**
     * Set the value of [no_telepon_rumah] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setNoTeleponRumah($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->no_telepon_rumah !== $v) {
            $this->no_telepon_rumah = $v;
            $this->modifiedColumns[] = PtkPeer::NO_TELEPON_RUMAH;
        }


        return $this;
    } // setNoTeleponRumah()

    /**
     * Set the value of [no_hp] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setNoHp($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->no_hp !== $v) {
            $this->no_hp = $v;
            $this->modifiedColumns[] = PtkPeer::NO_HP;
        }


        return $this;
    } // setNoHp()

    /**
     * Set the value of [email] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setEmail($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->email !== $v) {
            $this->email = $v;
            $this->modifiedColumns[] = PtkPeer::EMAIL;
        }


        return $this;
    } // setEmail()

    /**
     * Set the value of [entry_sekolah_id] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setEntrySekolahId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->entry_sekolah_id !== $v) {
            $this->entry_sekolah_id = $v;
            $this->modifiedColumns[] = PtkPeer::ENTRY_SEKOLAH_ID;
        }

        if ($this->aSekolahRelatedByEntrySekolahId !== null && $this->aSekolahRelatedByEntrySekolahId->getSekolahId() !== $v) {
            $this->aSekolahRelatedByEntrySekolahId = null;
        }

        if ($this->aSekolahRelatedByEntrySekolahId !== null && $this->aSekolahRelatedByEntrySekolahId->getSekolahId() !== $v) {
            $this->aSekolahRelatedByEntrySekolahId = null;
        }


        return $this;
    } // setEntrySekolahId()

    /**
     * Set the value of [status_keaktifan_id] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setStatusKeaktifanId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->status_keaktifan_id !== $v) {
            $this->status_keaktifan_id = $v;
            $this->modifiedColumns[] = PtkPeer::STATUS_KEAKTIFAN_ID;
        }

        if ($this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId !== null && $this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId->getStatusKeaktifanId() !== $v) {
            $this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId = null;
        }

        if ($this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId !== null && $this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId->getStatusKeaktifanId() !== $v) {
            $this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId = null;
        }


        return $this;
    } // setStatusKeaktifanId()

    /**
     * Set the value of [sk_cpns] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setSkCpns($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->sk_cpns !== $v) {
            $this->sk_cpns = $v;
            $this->modifiedColumns[] = PtkPeer::SK_CPNS;
        }


        return $this;
    } // setSkCpns()

    /**
     * Set the value of [tgl_cpns] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setTglCpns($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tgl_cpns !== $v) {
            $this->tgl_cpns = $v;
            $this->modifiedColumns[] = PtkPeer::TGL_CPNS;
        }


        return $this;
    } // setTglCpns()

    /**
     * Set the value of [sk_pengangkatan] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setSkPengangkatan($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->sk_pengangkatan !== $v) {
            $this->sk_pengangkatan = $v;
            $this->modifiedColumns[] = PtkPeer::SK_PENGANGKATAN;
        }


        return $this;
    } // setSkPengangkatan()

    /**
     * Set the value of [tmt_pengangkatan] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setTmtPengangkatan($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tmt_pengangkatan !== $v) {
            $this->tmt_pengangkatan = $v;
            $this->modifiedColumns[] = PtkPeer::TMT_PENGANGKATAN;
        }


        return $this;
    } // setTmtPengangkatan()

    /**
     * Set the value of [lembaga_pengangkat_id] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setLembagaPengangkatId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->lembaga_pengangkat_id !== $v) {
            $this->lembaga_pengangkat_id = $v;
            $this->modifiedColumns[] = PtkPeer::LEMBAGA_PENGANGKAT_ID;
        }

        if ($this->aLembagaPengangkatRelatedByLembagaPengangkatId !== null && $this->aLembagaPengangkatRelatedByLembagaPengangkatId->getLembagaPengangkatId() !== $v) {
            $this->aLembagaPengangkatRelatedByLembagaPengangkatId = null;
        }

        if ($this->aLembagaPengangkatRelatedByLembagaPengangkatId !== null && $this->aLembagaPengangkatRelatedByLembagaPengangkatId->getLembagaPengangkatId() !== $v) {
            $this->aLembagaPengangkatRelatedByLembagaPengangkatId = null;
        }


        return $this;
    } // setLembagaPengangkatId()

    /**
     * Set the value of [pangkat_golongan_id] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setPangkatGolonganId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->pangkat_golongan_id !== $v) {
            $this->pangkat_golongan_id = $v;
            $this->modifiedColumns[] = PtkPeer::PANGKAT_GOLONGAN_ID;
        }

        if ($this->aPangkatGolonganRelatedByPangkatGolonganId !== null && $this->aPangkatGolonganRelatedByPangkatGolonganId->getPangkatGolonganId() !== $v) {
            $this->aPangkatGolonganRelatedByPangkatGolonganId = null;
        }

        if ($this->aPangkatGolonganRelatedByPangkatGolonganId !== null && $this->aPangkatGolonganRelatedByPangkatGolonganId->getPangkatGolonganId() !== $v) {
            $this->aPangkatGolonganRelatedByPangkatGolonganId = null;
        }


        return $this;
    } // setPangkatGolonganId()

    /**
     * Set the value of [keahlian_laboratorium_id] column.
     * 
     * @param int $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setKeahlianLaboratoriumId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->keahlian_laboratorium_id !== $v) {
            $this->keahlian_laboratorium_id = $v;
            $this->modifiedColumns[] = PtkPeer::KEAHLIAN_LABORATORIUM_ID;
        }

        if ($this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId !== null && $this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId->getKeahlianLaboratoriumId() !== $v) {
            $this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId = null;
        }

        if ($this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId !== null && $this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId->getKeahlianLaboratoriumId() !== $v) {
            $this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId = null;
        }


        return $this;
    } // setKeahlianLaboratoriumId()

    /**
     * Set the value of [sumber_gaji_id] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setSumberGajiId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->sumber_gaji_id !== $v) {
            $this->sumber_gaji_id = $v;
            $this->modifiedColumns[] = PtkPeer::SUMBER_GAJI_ID;
        }

        if ($this->aSumberGajiRelatedBySumberGajiId !== null && $this->aSumberGajiRelatedBySumberGajiId->getSumberGajiId() !== $v) {
            $this->aSumberGajiRelatedBySumberGajiId = null;
        }

        if ($this->aSumberGajiRelatedBySumberGajiId !== null && $this->aSumberGajiRelatedBySumberGajiId->getSumberGajiId() !== $v) {
            $this->aSumberGajiRelatedBySumberGajiId = null;
        }


        return $this;
    } // setSumberGajiId()

    /**
     * Set the value of [nama_ibu_kandung] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setNamaIbuKandung($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nama_ibu_kandung !== $v) {
            $this->nama_ibu_kandung = $v;
            $this->modifiedColumns[] = PtkPeer::NAMA_IBU_KANDUNG;
        }


        return $this;
    } // setNamaIbuKandung()

    /**
     * Set the value of [status_perkawinan] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setStatusPerkawinan($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->status_perkawinan !== $v) {
            $this->status_perkawinan = $v;
            $this->modifiedColumns[] = PtkPeer::STATUS_PERKAWINAN;
        }


        return $this;
    } // setStatusPerkawinan()

    /**
     * Set the value of [nama_suami_istri] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setNamaSuamiIstri($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nama_suami_istri !== $v) {
            $this->nama_suami_istri = $v;
            $this->modifiedColumns[] = PtkPeer::NAMA_SUAMI_ISTRI;
        }


        return $this;
    } // setNamaSuamiIstri()

    /**
     * Set the value of [nip_suami_istri] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setNipSuamiIstri($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nip_suami_istri !== $v) {
            $this->nip_suami_istri = $v;
            $this->modifiedColumns[] = PtkPeer::NIP_SUAMI_ISTRI;
        }


        return $this;
    } // setNipSuamiIstri()

    /**
     * Set the value of [pekerjaan_suami_istri] column.
     * 
     * @param int $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setPekerjaanSuamiIstri($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->pekerjaan_suami_istri !== $v) {
            $this->pekerjaan_suami_istri = $v;
            $this->modifiedColumns[] = PtkPeer::PEKERJAAN_SUAMI_ISTRI;
        }

        if ($this->aPekerjaanRelatedByPekerjaanSuamiIstri !== null && $this->aPekerjaanRelatedByPekerjaanSuamiIstri->getPekerjaanId() !== $v) {
            $this->aPekerjaanRelatedByPekerjaanSuamiIstri = null;
        }

        if ($this->aPekerjaanRelatedByPekerjaanSuamiIstri !== null && $this->aPekerjaanRelatedByPekerjaanSuamiIstri->getPekerjaanId() !== $v) {
            $this->aPekerjaanRelatedByPekerjaanSuamiIstri = null;
        }


        return $this;
    } // setPekerjaanSuamiIstri()

    /**
     * Set the value of [tmt_pns] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setTmtPns($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tmt_pns !== $v) {
            $this->tmt_pns = $v;
            $this->modifiedColumns[] = PtkPeer::TMT_PNS;
        }


        return $this;
    } // setTmtPns()

    /**
     * Set the value of [sudah_lisensi_kepala_sekolah] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setSudahLisensiKepalaSekolah($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->sudah_lisensi_kepala_sekolah !== $v) {
            $this->sudah_lisensi_kepala_sekolah = $v;
            $this->modifiedColumns[] = PtkPeer::SUDAH_LISENSI_KEPALA_SEKOLAH;
        }


        return $this;
    } // setSudahLisensiKepalaSekolah()

    /**
     * Set the value of [jumlah_sekolah_binaan] column.
     * 
     * @param int $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setJumlahSekolahBinaan($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->jumlah_sekolah_binaan !== $v) {
            $this->jumlah_sekolah_binaan = $v;
            $this->modifiedColumns[] = PtkPeer::JUMLAH_SEKOLAH_BINAAN;
        }


        return $this;
    } // setJumlahSekolahBinaan()

    /**
     * Set the value of [pernah_diklat_kepengawasan] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setPernahDiklatKepengawasan($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->pernah_diklat_kepengawasan !== $v) {
            $this->pernah_diklat_kepengawasan = $v;
            $this->modifiedColumns[] = PtkPeer::PERNAH_DIKLAT_KEPENGAWASAN;
        }


        return $this;
    } // setPernahDiklatKepengawasan()

    /**
     * Set the value of [status_data] column.
     * 
     * @param int $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setStatusData($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->status_data !== $v) {
            $this->status_data = $v;
            $this->modifiedColumns[] = PtkPeer::STATUS_DATA;
        }


        return $this;
    } // setStatusData()

    /**
     * Set the value of [mampu_handle_kk] column.
     * 
     * @param int $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setMampuHandleKk($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->mampu_handle_kk !== $v) {
            $this->mampu_handle_kk = $v;
            $this->modifiedColumns[] = PtkPeer::MAMPU_HANDLE_KK;
        }

        if ($this->aKebutuhanKhususRelatedByMampuHandleKk !== null && $this->aKebutuhanKhususRelatedByMampuHandleKk->getKebutuhanKhususId() !== $v) {
            $this->aKebutuhanKhususRelatedByMampuHandleKk = null;
        }

        if ($this->aKebutuhanKhususRelatedByMampuHandleKk !== null && $this->aKebutuhanKhususRelatedByMampuHandleKk->getKebutuhanKhususId() !== $v) {
            $this->aKebutuhanKhususRelatedByMampuHandleKk = null;
        }


        return $this;
    } // setMampuHandleKk()

    /**
     * Set the value of [keahlian_braille] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setKeahlianBraille($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->keahlian_braille !== $v) {
            $this->keahlian_braille = $v;
            $this->modifiedColumns[] = PtkPeer::KEAHLIAN_BRAILLE;
        }


        return $this;
    } // setKeahlianBraille()

    /**
     * Set the value of [keahlian_bhs_isyarat] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setKeahlianBhsIsyarat($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->keahlian_bhs_isyarat !== $v) {
            $this->keahlian_bhs_isyarat = $v;
            $this->modifiedColumns[] = PtkPeer::KEAHLIAN_BHS_ISYARAT;
        }


        return $this;
    } // setKeahlianBhsIsyarat()

    /**
     * Set the value of [npwp] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setNpwp($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->npwp !== $v) {
            $this->npwp = $v;
            $this->modifiedColumns[] = PtkPeer::NPWP;
        }


        return $this;
    } // setNpwp()

    /**
     * Sets the value of [last_update] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Ptk The current object (for fluent API support)
     */
    public function setLastUpdate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_update !== null || $dt !== null) {
            $currentDateAsString = ($this->last_update !== null && $tmpDt = new DateTime($this->last_update)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_update = $newDateAsString;
                $this->modifiedColumns[] = PtkPeer::LAST_UPDATE;
            }
        } // if either are not null


        return $this;
    } // setLastUpdate()

    /**
     * Set the value of [soft_delete] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setSoftDelete($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->soft_delete !== $v) {
            $this->soft_delete = $v;
            $this->modifiedColumns[] = PtkPeer::SOFT_DELETE;
        }


        return $this;
    } // setSoftDelete()

    /**
     * Sets the value of [last_sync] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Ptk The current object (for fluent API support)
     */
    public function setLastSync($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_sync !== null || $dt !== null) {
            $currentDateAsString = ($this->last_sync !== null && $tmpDt = new DateTime($this->last_sync)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_sync = $newDateAsString;
                $this->modifiedColumns[] = PtkPeer::LAST_SYNC;
            }
        } // if either are not null


        return $this;
    } // setLastSync()

    /**
     * Set the value of [updater_id] column.
     * 
     * @param string $v new value
     * @return Ptk The current object (for fluent API support)
     */
    public function setUpdaterId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->updater_id !== $v) {
            $this->updater_id = $v;
            $this->modifiedColumns[] = PtkPeer::UPDATER_ID;
        }


        return $this;
    } // setUpdaterId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->keahlian_braille !== '((0))') {
                return false;
            }

            if ($this->keahlian_bhs_isyarat !== '((0))') {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->ptk_id = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
            $this->nama = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->nip = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->jenis_kelamin = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->tempat_lahir = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->tanggal_lahir = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->nik = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->niy_nigk = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->nuptk = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->status_kepegawaian_id = ($row[$startcol + 9] !== null) ? (int) $row[$startcol + 9] : null;
            $this->jenis_ptk_id = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->pengawas_bidang_studi_id = ($row[$startcol + 11] !== null) ? (int) $row[$startcol + 11] : null;
            $this->agama_id = ($row[$startcol + 12] !== null) ? (int) $row[$startcol + 12] : null;
            $this->kewarganegaraan = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->alamat_jalan = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
            $this->rt = ($row[$startcol + 15] !== null) ? (string) $row[$startcol + 15] : null;
            $this->rw = ($row[$startcol + 16] !== null) ? (string) $row[$startcol + 16] : null;
            $this->nama_dusun = ($row[$startcol + 17] !== null) ? (string) $row[$startcol + 17] : null;
            $this->desa_kelurahan = ($row[$startcol + 18] !== null) ? (string) $row[$startcol + 18] : null;
            $this->kode_wilayah = ($row[$startcol + 19] !== null) ? (string) $row[$startcol + 19] : null;
            $this->kode_pos = ($row[$startcol + 20] !== null) ? (string) $row[$startcol + 20] : null;
            $this->no_telepon_rumah = ($row[$startcol + 21] !== null) ? (string) $row[$startcol + 21] : null;
            $this->no_hp = ($row[$startcol + 22] !== null) ? (string) $row[$startcol + 22] : null;
            $this->email = ($row[$startcol + 23] !== null) ? (string) $row[$startcol + 23] : null;
            $this->entry_sekolah_id = ($row[$startcol + 24] !== null) ? (string) $row[$startcol + 24] : null;
            $this->status_keaktifan_id = ($row[$startcol + 25] !== null) ? (string) $row[$startcol + 25] : null;
            $this->sk_cpns = ($row[$startcol + 26] !== null) ? (string) $row[$startcol + 26] : null;
            $this->tgl_cpns = ($row[$startcol + 27] !== null) ? (string) $row[$startcol + 27] : null;
            $this->sk_pengangkatan = ($row[$startcol + 28] !== null) ? (string) $row[$startcol + 28] : null;
            $this->tmt_pengangkatan = ($row[$startcol + 29] !== null) ? (string) $row[$startcol + 29] : null;
            $this->lembaga_pengangkat_id = ($row[$startcol + 30] !== null) ? (string) $row[$startcol + 30] : null;
            $this->pangkat_golongan_id = ($row[$startcol + 31] !== null) ? (string) $row[$startcol + 31] : null;
            $this->keahlian_laboratorium_id = ($row[$startcol + 32] !== null) ? (int) $row[$startcol + 32] : null;
            $this->sumber_gaji_id = ($row[$startcol + 33] !== null) ? (string) $row[$startcol + 33] : null;
            $this->nama_ibu_kandung = ($row[$startcol + 34] !== null) ? (string) $row[$startcol + 34] : null;
            $this->status_perkawinan = ($row[$startcol + 35] !== null) ? (string) $row[$startcol + 35] : null;
            $this->nama_suami_istri = ($row[$startcol + 36] !== null) ? (string) $row[$startcol + 36] : null;
            $this->nip_suami_istri = ($row[$startcol + 37] !== null) ? (string) $row[$startcol + 37] : null;
            $this->pekerjaan_suami_istri = ($row[$startcol + 38] !== null) ? (int) $row[$startcol + 38] : null;
            $this->tmt_pns = ($row[$startcol + 39] !== null) ? (string) $row[$startcol + 39] : null;
            $this->sudah_lisensi_kepala_sekolah = ($row[$startcol + 40] !== null) ? (string) $row[$startcol + 40] : null;
            $this->jumlah_sekolah_binaan = ($row[$startcol + 41] !== null) ? (int) $row[$startcol + 41] : null;
            $this->pernah_diklat_kepengawasan = ($row[$startcol + 42] !== null) ? (string) $row[$startcol + 42] : null;
            $this->status_data = ($row[$startcol + 43] !== null) ? (int) $row[$startcol + 43] : null;
            $this->mampu_handle_kk = ($row[$startcol + 44] !== null) ? (int) $row[$startcol + 44] : null;
            $this->keahlian_braille = ($row[$startcol + 45] !== null) ? (string) $row[$startcol + 45] : null;
            $this->keahlian_bhs_isyarat = ($row[$startcol + 46] !== null) ? (string) $row[$startcol + 46] : null;
            $this->npwp = ($row[$startcol + 47] !== null) ? (string) $row[$startcol + 47] : null;
            $this->last_update = ($row[$startcol + 48] !== null) ? (string) $row[$startcol + 48] : null;
            $this->soft_delete = ($row[$startcol + 49] !== null) ? (string) $row[$startcol + 49] : null;
            $this->last_sync = ($row[$startcol + 50] !== null) ? (string) $row[$startcol + 50] : null;
            $this->updater_id = ($row[$startcol + 51] !== null) ? (string) $row[$startcol + 51] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 52; // 52 = PtkPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Ptk object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aStatusKepegawaianRelatedByStatusKepegawaianId !== null && $this->status_kepegawaian_id !== $this->aStatusKepegawaianRelatedByStatusKepegawaianId->getStatusKepegawaianId()) {
            $this->aStatusKepegawaianRelatedByStatusKepegawaianId = null;
        }
        if ($this->aStatusKepegawaianRelatedByStatusKepegawaianId !== null && $this->status_kepegawaian_id !== $this->aStatusKepegawaianRelatedByStatusKepegawaianId->getStatusKepegawaianId()) {
            $this->aStatusKepegawaianRelatedByStatusKepegawaianId = null;
        }
        if ($this->aJenisPtkRelatedByJenisPtkId !== null && $this->jenis_ptk_id !== $this->aJenisPtkRelatedByJenisPtkId->getJenisPtkId()) {
            $this->aJenisPtkRelatedByJenisPtkId = null;
        }
        if ($this->aJenisPtkRelatedByJenisPtkId !== null && $this->jenis_ptk_id !== $this->aJenisPtkRelatedByJenisPtkId->getJenisPtkId()) {
            $this->aJenisPtkRelatedByJenisPtkId = null;
        }
        if ($this->aBidangStudiRelatedByPengawasBidangStudiId !== null && $this->pengawas_bidang_studi_id !== $this->aBidangStudiRelatedByPengawasBidangStudiId->getBidangStudiId()) {
            $this->aBidangStudiRelatedByPengawasBidangStudiId = null;
        }
        if ($this->aBidangStudiRelatedByPengawasBidangStudiId !== null && $this->pengawas_bidang_studi_id !== $this->aBidangStudiRelatedByPengawasBidangStudiId->getBidangStudiId()) {
            $this->aBidangStudiRelatedByPengawasBidangStudiId = null;
        }
        if ($this->aAgamaRelatedByAgamaId !== null && $this->agama_id !== $this->aAgamaRelatedByAgamaId->getAgamaId()) {
            $this->aAgamaRelatedByAgamaId = null;
        }
        if ($this->aAgamaRelatedByAgamaId !== null && $this->agama_id !== $this->aAgamaRelatedByAgamaId->getAgamaId()) {
            $this->aAgamaRelatedByAgamaId = null;
        }
        if ($this->aNegaraRelatedByKewarganegaraan !== null && $this->kewarganegaraan !== $this->aNegaraRelatedByKewarganegaraan->getNegaraId()) {
            $this->aNegaraRelatedByKewarganegaraan = null;
        }
        if ($this->aNegaraRelatedByKewarganegaraan !== null && $this->kewarganegaraan !== $this->aNegaraRelatedByKewarganegaraan->getNegaraId()) {
            $this->aNegaraRelatedByKewarganegaraan = null;
        }
        if ($this->aMstWilayahRelatedByKodeWilayah !== null && $this->kode_wilayah !== $this->aMstWilayahRelatedByKodeWilayah->getKodeWilayah()) {
            $this->aMstWilayahRelatedByKodeWilayah = null;
        }
        if ($this->aMstWilayahRelatedByKodeWilayah !== null && $this->kode_wilayah !== $this->aMstWilayahRelatedByKodeWilayah->getKodeWilayah()) {
            $this->aMstWilayahRelatedByKodeWilayah = null;
        }
        if ($this->aSekolahRelatedByEntrySekolahId !== null && $this->entry_sekolah_id !== $this->aSekolahRelatedByEntrySekolahId->getSekolahId()) {
            $this->aSekolahRelatedByEntrySekolahId = null;
        }
        if ($this->aSekolahRelatedByEntrySekolahId !== null && $this->entry_sekolah_id !== $this->aSekolahRelatedByEntrySekolahId->getSekolahId()) {
            $this->aSekolahRelatedByEntrySekolahId = null;
        }
        if ($this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId !== null && $this->status_keaktifan_id !== $this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId->getStatusKeaktifanId()) {
            $this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId = null;
        }
        if ($this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId !== null && $this->status_keaktifan_id !== $this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId->getStatusKeaktifanId()) {
            $this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId = null;
        }
        if ($this->aLembagaPengangkatRelatedByLembagaPengangkatId !== null && $this->lembaga_pengangkat_id !== $this->aLembagaPengangkatRelatedByLembagaPengangkatId->getLembagaPengangkatId()) {
            $this->aLembagaPengangkatRelatedByLembagaPengangkatId = null;
        }
        if ($this->aLembagaPengangkatRelatedByLembagaPengangkatId !== null && $this->lembaga_pengangkat_id !== $this->aLembagaPengangkatRelatedByLembagaPengangkatId->getLembagaPengangkatId()) {
            $this->aLembagaPengangkatRelatedByLembagaPengangkatId = null;
        }
        if ($this->aPangkatGolonganRelatedByPangkatGolonganId !== null && $this->pangkat_golongan_id !== $this->aPangkatGolonganRelatedByPangkatGolonganId->getPangkatGolonganId()) {
            $this->aPangkatGolonganRelatedByPangkatGolonganId = null;
        }
        if ($this->aPangkatGolonganRelatedByPangkatGolonganId !== null && $this->pangkat_golongan_id !== $this->aPangkatGolonganRelatedByPangkatGolonganId->getPangkatGolonganId()) {
            $this->aPangkatGolonganRelatedByPangkatGolonganId = null;
        }
        if ($this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId !== null && $this->keahlian_laboratorium_id !== $this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId->getKeahlianLaboratoriumId()) {
            $this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId = null;
        }
        if ($this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId !== null && $this->keahlian_laboratorium_id !== $this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId->getKeahlianLaboratoriumId()) {
            $this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId = null;
        }
        if ($this->aSumberGajiRelatedBySumberGajiId !== null && $this->sumber_gaji_id !== $this->aSumberGajiRelatedBySumberGajiId->getSumberGajiId()) {
            $this->aSumberGajiRelatedBySumberGajiId = null;
        }
        if ($this->aSumberGajiRelatedBySumberGajiId !== null && $this->sumber_gaji_id !== $this->aSumberGajiRelatedBySumberGajiId->getSumberGajiId()) {
            $this->aSumberGajiRelatedBySumberGajiId = null;
        }
        if ($this->aPekerjaanRelatedByPekerjaanSuamiIstri !== null && $this->pekerjaan_suami_istri !== $this->aPekerjaanRelatedByPekerjaanSuamiIstri->getPekerjaanId()) {
            $this->aPekerjaanRelatedByPekerjaanSuamiIstri = null;
        }
        if ($this->aPekerjaanRelatedByPekerjaanSuamiIstri !== null && $this->pekerjaan_suami_istri !== $this->aPekerjaanRelatedByPekerjaanSuamiIstri->getPekerjaanId()) {
            $this->aPekerjaanRelatedByPekerjaanSuamiIstri = null;
        }
        if ($this->aKebutuhanKhususRelatedByMampuHandleKk !== null && $this->mampu_handle_kk !== $this->aKebutuhanKhususRelatedByMampuHandleKk->getKebutuhanKhususId()) {
            $this->aKebutuhanKhususRelatedByMampuHandleKk = null;
        }
        if ($this->aKebutuhanKhususRelatedByMampuHandleKk !== null && $this->mampu_handle_kk !== $this->aKebutuhanKhususRelatedByMampuHandleKk->getKebutuhanKhususId()) {
            $this->aKebutuhanKhususRelatedByMampuHandleKk = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PtkPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = PtkPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aSekolahRelatedByEntrySekolahId = null;
            $this->aSekolahRelatedByEntrySekolahId = null;
            $this->aAgamaRelatedByAgamaId = null;
            $this->aAgamaRelatedByAgamaId = null;
            $this->aBidangStudiRelatedByPengawasBidangStudiId = null;
            $this->aBidangStudiRelatedByPengawasBidangStudiId = null;
            $this->aJenisPtkRelatedByJenisPtkId = null;
            $this->aJenisPtkRelatedByJenisPtkId = null;
            $this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId = null;
            $this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId = null;
            $this->aKebutuhanKhususRelatedByMampuHandleKk = null;
            $this->aKebutuhanKhususRelatedByMampuHandleKk = null;
            $this->aLembagaPengangkatRelatedByLembagaPengangkatId = null;
            $this->aLembagaPengangkatRelatedByLembagaPengangkatId = null;
            $this->aMstWilayahRelatedByKodeWilayah = null;
            $this->aMstWilayahRelatedByKodeWilayah = null;
            $this->aNegaraRelatedByKewarganegaraan = null;
            $this->aNegaraRelatedByKewarganegaraan = null;
            $this->aPangkatGolonganRelatedByPangkatGolonganId = null;
            $this->aPangkatGolonganRelatedByPangkatGolonganId = null;
            $this->aPekerjaanRelatedByPekerjaanSuamiIstri = null;
            $this->aPekerjaanRelatedByPekerjaanSuamiIstri = null;
            $this->aStatusKepegawaianRelatedByStatusKepegawaianId = null;
            $this->aStatusKepegawaianRelatedByStatusKepegawaianId = null;
            $this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId = null;
            $this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId = null;
            $this->aSumberGajiRelatedBySumberGajiId = null;
            $this->aSumberGajiRelatedBySumberGajiId = null;
            $this->collKesejahteraansRelatedByPtkId = null;

            $this->collKesejahteraansRelatedByPtkId = null;

            $this->collVldPtksRelatedByPtkId = null;

            $this->collVldPtksRelatedByPtkId = null;

            $this->collPenghargaansRelatedByPtkId = null;

            $this->collPenghargaansRelatedByPtkId = null;

            $this->collInpassingsRelatedByPtkId = null;

            $this->collInpassingsRelatedByPtkId = null;

            $this->collPtkTerdaftarsRelatedByPtkId = null;

            $this->collPtkTerdaftarsRelatedByPtkId = null;

            $this->collKaryaTulissRelatedByPtkId = null;

            $this->collKaryaTulissRelatedByPtkId = null;

            $this->collNilaiTestsRelatedByPtkId = null;

            $this->collNilaiTestsRelatedByPtkId = null;

            $this->collBukuPtksRelatedByPtkId = null;

            $this->collBukuPtksRelatedByPtkId = null;

            $this->collBeasiswaPtksRelatedByPtkId = null;

            $this->collBeasiswaPtksRelatedByPtkId = null;

            $this->collRwyKepangkatansRelatedByPtkId = null;

            $this->collRwyKepangkatansRelatedByPtkId = null;

            $this->collRombonganBelajarsRelatedByPtkId = null;

            $this->collRombonganBelajarsRelatedByPtkId = null;

            $this->collRiwayatGajiBerkalasRelatedByPtkId = null;

            $this->collRiwayatGajiBerkalasRelatedByPtkId = null;

            $this->collPengawasTerdaftarsRelatedByPtkId = null;

            $this->collPengawasTerdaftarsRelatedByPtkId = null;

            $this->collRwyStrukturalsRelatedByPtkId = null;

            $this->collRwyStrukturalsRelatedByPtkId = null;

            $this->collRwyPendFormalsRelatedByPtkId = null;

            $this->collRwyPendFormalsRelatedByPtkId = null;

            $this->collAnaksRelatedByPtkId = null;

            $this->collAnaksRelatedByPtkId = null;

            $this->collDiklatsRelatedByPtkId = null;

            $this->collDiklatsRelatedByPtkId = null;

            $this->collPtkBarusRelatedByPtkId = null;

            $this->collPtkBarusRelatedByPtkId = null;

            $this->collTunjangansRelatedByPtkId = null;

            $this->collTunjangansRelatedByPtkId = null;

            $this->collRwySertifikasisRelatedByPtkId = null;

            $this->collRwySertifikasisRelatedByPtkId = null;

            $this->collTugasTambahansRelatedByPtkId = null;

            $this->collTugasTambahansRelatedByPtkId = null;

            $this->collRwyFungsionalsRelatedByPtkId = null;

            $this->collRwyFungsionalsRelatedByPtkId = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PtkPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = PtkQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PtkPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                PtkPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSekolahRelatedByEntrySekolahId !== null) {
                if ($this->aSekolahRelatedByEntrySekolahId->isModified() || $this->aSekolahRelatedByEntrySekolahId->isNew()) {
                    $affectedRows += $this->aSekolahRelatedByEntrySekolahId->save($con);
                }
                $this->setSekolahRelatedByEntrySekolahId($this->aSekolahRelatedByEntrySekolahId);
            }

            if ($this->aSekolahRelatedByEntrySekolahId !== null) {
                if ($this->aSekolahRelatedByEntrySekolahId->isModified() || $this->aSekolahRelatedByEntrySekolahId->isNew()) {
                    $affectedRows += $this->aSekolahRelatedByEntrySekolahId->save($con);
                }
                $this->setSekolahRelatedByEntrySekolahId($this->aSekolahRelatedByEntrySekolahId);
            }

            if ($this->aAgamaRelatedByAgamaId !== null) {
                if ($this->aAgamaRelatedByAgamaId->isModified() || $this->aAgamaRelatedByAgamaId->isNew()) {
                    $affectedRows += $this->aAgamaRelatedByAgamaId->save($con);
                }
                $this->setAgamaRelatedByAgamaId($this->aAgamaRelatedByAgamaId);
            }

            if ($this->aAgamaRelatedByAgamaId !== null) {
                if ($this->aAgamaRelatedByAgamaId->isModified() || $this->aAgamaRelatedByAgamaId->isNew()) {
                    $affectedRows += $this->aAgamaRelatedByAgamaId->save($con);
                }
                $this->setAgamaRelatedByAgamaId($this->aAgamaRelatedByAgamaId);
            }

            if ($this->aBidangStudiRelatedByPengawasBidangStudiId !== null) {
                if ($this->aBidangStudiRelatedByPengawasBidangStudiId->isModified() || $this->aBidangStudiRelatedByPengawasBidangStudiId->isNew()) {
                    $affectedRows += $this->aBidangStudiRelatedByPengawasBidangStudiId->save($con);
                }
                $this->setBidangStudiRelatedByPengawasBidangStudiId($this->aBidangStudiRelatedByPengawasBidangStudiId);
            }

            if ($this->aBidangStudiRelatedByPengawasBidangStudiId !== null) {
                if ($this->aBidangStudiRelatedByPengawasBidangStudiId->isModified() || $this->aBidangStudiRelatedByPengawasBidangStudiId->isNew()) {
                    $affectedRows += $this->aBidangStudiRelatedByPengawasBidangStudiId->save($con);
                }
                $this->setBidangStudiRelatedByPengawasBidangStudiId($this->aBidangStudiRelatedByPengawasBidangStudiId);
            }

            if ($this->aJenisPtkRelatedByJenisPtkId !== null) {
                if ($this->aJenisPtkRelatedByJenisPtkId->isModified() || $this->aJenisPtkRelatedByJenisPtkId->isNew()) {
                    $affectedRows += $this->aJenisPtkRelatedByJenisPtkId->save($con);
                }
                $this->setJenisPtkRelatedByJenisPtkId($this->aJenisPtkRelatedByJenisPtkId);
            }

            if ($this->aJenisPtkRelatedByJenisPtkId !== null) {
                if ($this->aJenisPtkRelatedByJenisPtkId->isModified() || $this->aJenisPtkRelatedByJenisPtkId->isNew()) {
                    $affectedRows += $this->aJenisPtkRelatedByJenisPtkId->save($con);
                }
                $this->setJenisPtkRelatedByJenisPtkId($this->aJenisPtkRelatedByJenisPtkId);
            }

            if ($this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId !== null) {
                if ($this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId->isModified() || $this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId->isNew()) {
                    $affectedRows += $this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId->save($con);
                }
                $this->setKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId($this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId);
            }

            if ($this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId !== null) {
                if ($this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId->isModified() || $this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId->isNew()) {
                    $affectedRows += $this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId->save($con);
                }
                $this->setKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId($this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId);
            }

            if ($this->aKebutuhanKhususRelatedByMampuHandleKk !== null) {
                if ($this->aKebutuhanKhususRelatedByMampuHandleKk->isModified() || $this->aKebutuhanKhususRelatedByMampuHandleKk->isNew()) {
                    $affectedRows += $this->aKebutuhanKhususRelatedByMampuHandleKk->save($con);
                }
                $this->setKebutuhanKhususRelatedByMampuHandleKk($this->aKebutuhanKhususRelatedByMampuHandleKk);
            }

            if ($this->aKebutuhanKhususRelatedByMampuHandleKk !== null) {
                if ($this->aKebutuhanKhususRelatedByMampuHandleKk->isModified() || $this->aKebutuhanKhususRelatedByMampuHandleKk->isNew()) {
                    $affectedRows += $this->aKebutuhanKhususRelatedByMampuHandleKk->save($con);
                }
                $this->setKebutuhanKhususRelatedByMampuHandleKk($this->aKebutuhanKhususRelatedByMampuHandleKk);
            }

            if ($this->aLembagaPengangkatRelatedByLembagaPengangkatId !== null) {
                if ($this->aLembagaPengangkatRelatedByLembagaPengangkatId->isModified() || $this->aLembagaPengangkatRelatedByLembagaPengangkatId->isNew()) {
                    $affectedRows += $this->aLembagaPengangkatRelatedByLembagaPengangkatId->save($con);
                }
                $this->setLembagaPengangkatRelatedByLembagaPengangkatId($this->aLembagaPengangkatRelatedByLembagaPengangkatId);
            }

            if ($this->aLembagaPengangkatRelatedByLembagaPengangkatId !== null) {
                if ($this->aLembagaPengangkatRelatedByLembagaPengangkatId->isModified() || $this->aLembagaPengangkatRelatedByLembagaPengangkatId->isNew()) {
                    $affectedRows += $this->aLembagaPengangkatRelatedByLembagaPengangkatId->save($con);
                }
                $this->setLembagaPengangkatRelatedByLembagaPengangkatId($this->aLembagaPengangkatRelatedByLembagaPengangkatId);
            }

            if ($this->aMstWilayahRelatedByKodeWilayah !== null) {
                if ($this->aMstWilayahRelatedByKodeWilayah->isModified() || $this->aMstWilayahRelatedByKodeWilayah->isNew()) {
                    $affectedRows += $this->aMstWilayahRelatedByKodeWilayah->save($con);
                }
                $this->setMstWilayahRelatedByKodeWilayah($this->aMstWilayahRelatedByKodeWilayah);
            }

            if ($this->aMstWilayahRelatedByKodeWilayah !== null) {
                if ($this->aMstWilayahRelatedByKodeWilayah->isModified() || $this->aMstWilayahRelatedByKodeWilayah->isNew()) {
                    $affectedRows += $this->aMstWilayahRelatedByKodeWilayah->save($con);
                }
                $this->setMstWilayahRelatedByKodeWilayah($this->aMstWilayahRelatedByKodeWilayah);
            }

            if ($this->aNegaraRelatedByKewarganegaraan !== null) {
                if ($this->aNegaraRelatedByKewarganegaraan->isModified() || $this->aNegaraRelatedByKewarganegaraan->isNew()) {
                    $affectedRows += $this->aNegaraRelatedByKewarganegaraan->save($con);
                }
                $this->setNegaraRelatedByKewarganegaraan($this->aNegaraRelatedByKewarganegaraan);
            }

            if ($this->aNegaraRelatedByKewarganegaraan !== null) {
                if ($this->aNegaraRelatedByKewarganegaraan->isModified() || $this->aNegaraRelatedByKewarganegaraan->isNew()) {
                    $affectedRows += $this->aNegaraRelatedByKewarganegaraan->save($con);
                }
                $this->setNegaraRelatedByKewarganegaraan($this->aNegaraRelatedByKewarganegaraan);
            }

            if ($this->aPangkatGolonganRelatedByPangkatGolonganId !== null) {
                if ($this->aPangkatGolonganRelatedByPangkatGolonganId->isModified() || $this->aPangkatGolonganRelatedByPangkatGolonganId->isNew()) {
                    $affectedRows += $this->aPangkatGolonganRelatedByPangkatGolonganId->save($con);
                }
                $this->setPangkatGolonganRelatedByPangkatGolonganId($this->aPangkatGolonganRelatedByPangkatGolonganId);
            }

            if ($this->aPangkatGolonganRelatedByPangkatGolonganId !== null) {
                if ($this->aPangkatGolonganRelatedByPangkatGolonganId->isModified() || $this->aPangkatGolonganRelatedByPangkatGolonganId->isNew()) {
                    $affectedRows += $this->aPangkatGolonganRelatedByPangkatGolonganId->save($con);
                }
                $this->setPangkatGolonganRelatedByPangkatGolonganId($this->aPangkatGolonganRelatedByPangkatGolonganId);
            }

            if ($this->aPekerjaanRelatedByPekerjaanSuamiIstri !== null) {
                if ($this->aPekerjaanRelatedByPekerjaanSuamiIstri->isModified() || $this->aPekerjaanRelatedByPekerjaanSuamiIstri->isNew()) {
                    $affectedRows += $this->aPekerjaanRelatedByPekerjaanSuamiIstri->save($con);
                }
                $this->setPekerjaanRelatedByPekerjaanSuamiIstri($this->aPekerjaanRelatedByPekerjaanSuamiIstri);
            }

            if ($this->aPekerjaanRelatedByPekerjaanSuamiIstri !== null) {
                if ($this->aPekerjaanRelatedByPekerjaanSuamiIstri->isModified() || $this->aPekerjaanRelatedByPekerjaanSuamiIstri->isNew()) {
                    $affectedRows += $this->aPekerjaanRelatedByPekerjaanSuamiIstri->save($con);
                }
                $this->setPekerjaanRelatedByPekerjaanSuamiIstri($this->aPekerjaanRelatedByPekerjaanSuamiIstri);
            }

            if ($this->aStatusKepegawaianRelatedByStatusKepegawaianId !== null) {
                if ($this->aStatusKepegawaianRelatedByStatusKepegawaianId->isModified() || $this->aStatusKepegawaianRelatedByStatusKepegawaianId->isNew()) {
                    $affectedRows += $this->aStatusKepegawaianRelatedByStatusKepegawaianId->save($con);
                }
                $this->setStatusKepegawaianRelatedByStatusKepegawaianId($this->aStatusKepegawaianRelatedByStatusKepegawaianId);
            }

            if ($this->aStatusKepegawaianRelatedByStatusKepegawaianId !== null) {
                if ($this->aStatusKepegawaianRelatedByStatusKepegawaianId->isModified() || $this->aStatusKepegawaianRelatedByStatusKepegawaianId->isNew()) {
                    $affectedRows += $this->aStatusKepegawaianRelatedByStatusKepegawaianId->save($con);
                }
                $this->setStatusKepegawaianRelatedByStatusKepegawaianId($this->aStatusKepegawaianRelatedByStatusKepegawaianId);
            }

            if ($this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId !== null) {
                if ($this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId->isModified() || $this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId->isNew()) {
                    $affectedRows += $this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId->save($con);
                }
                $this->setStatusKeaktifanPegawaiRelatedByStatusKeaktifanId($this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId);
            }

            if ($this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId !== null) {
                if ($this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId->isModified() || $this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId->isNew()) {
                    $affectedRows += $this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId->save($con);
                }
                $this->setStatusKeaktifanPegawaiRelatedByStatusKeaktifanId($this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId);
            }

            if ($this->aSumberGajiRelatedBySumberGajiId !== null) {
                if ($this->aSumberGajiRelatedBySumberGajiId->isModified() || $this->aSumberGajiRelatedBySumberGajiId->isNew()) {
                    $affectedRows += $this->aSumberGajiRelatedBySumberGajiId->save($con);
                }
                $this->setSumberGajiRelatedBySumberGajiId($this->aSumberGajiRelatedBySumberGajiId);
            }

            if ($this->aSumberGajiRelatedBySumberGajiId !== null) {
                if ($this->aSumberGajiRelatedBySumberGajiId->isModified() || $this->aSumberGajiRelatedBySumberGajiId->isNew()) {
                    $affectedRows += $this->aSumberGajiRelatedBySumberGajiId->save($con);
                }
                $this->setSumberGajiRelatedBySumberGajiId($this->aSumberGajiRelatedBySumberGajiId);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->kesejahteraansRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->kesejahteraansRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    KesejahteraanQuery::create()
                        ->filterByPrimaryKeys($this->kesejahteraansRelatedByPtkIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->kesejahteraansRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collKesejahteraansRelatedByPtkId !== null) {
                foreach ($this->collKesejahteraansRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->kesejahteraansRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->kesejahteraansRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    KesejahteraanQuery::create()
                        ->filterByPrimaryKeys($this->kesejahteraansRelatedByPtkIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->kesejahteraansRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collKesejahteraansRelatedByPtkId !== null) {
                foreach ($this->collKesejahteraansRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldPtksRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->vldPtksRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    VldPtkQuery::create()
                        ->filterByPrimaryKeys($this->vldPtksRelatedByPtkIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldPtksRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collVldPtksRelatedByPtkId !== null) {
                foreach ($this->collVldPtksRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldPtksRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->vldPtksRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    VldPtkQuery::create()
                        ->filterByPrimaryKeys($this->vldPtksRelatedByPtkIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldPtksRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collVldPtksRelatedByPtkId !== null) {
                foreach ($this->collVldPtksRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->penghargaansRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->penghargaansRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    PenghargaanQuery::create()
                        ->filterByPrimaryKeys($this->penghargaansRelatedByPtkIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->penghargaansRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collPenghargaansRelatedByPtkId !== null) {
                foreach ($this->collPenghargaansRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->penghargaansRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->penghargaansRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    PenghargaanQuery::create()
                        ->filterByPrimaryKeys($this->penghargaansRelatedByPtkIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->penghargaansRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collPenghargaansRelatedByPtkId !== null) {
                foreach ($this->collPenghargaansRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->inpassingsRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->inpassingsRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->inpassingsRelatedByPtkIdScheduledForDeletion as $inpassingRelatedByPtkId) {
                        // need to save related object because we set the relation to null
                        $inpassingRelatedByPtkId->save($con);
                    }
                    $this->inpassingsRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collInpassingsRelatedByPtkId !== null) {
                foreach ($this->collInpassingsRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->inpassingsRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->inpassingsRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->inpassingsRelatedByPtkIdScheduledForDeletion as $inpassingRelatedByPtkId) {
                        // need to save related object because we set the relation to null
                        $inpassingRelatedByPtkId->save($con);
                    }
                    $this->inpassingsRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collInpassingsRelatedByPtkId !== null) {
                foreach ($this->collInpassingsRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->ptkTerdaftarsRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->ptkTerdaftarsRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    PtkTerdaftarQuery::create()
                        ->filterByPrimaryKeys($this->ptkTerdaftarsRelatedByPtkIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->ptkTerdaftarsRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collPtkTerdaftarsRelatedByPtkId !== null) {
                foreach ($this->collPtkTerdaftarsRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->ptkTerdaftarsRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->ptkTerdaftarsRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    PtkTerdaftarQuery::create()
                        ->filterByPrimaryKeys($this->ptkTerdaftarsRelatedByPtkIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->ptkTerdaftarsRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collPtkTerdaftarsRelatedByPtkId !== null) {
                foreach ($this->collPtkTerdaftarsRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->karyaTulissRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->karyaTulissRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    KaryaTulisQuery::create()
                        ->filterByPrimaryKeys($this->karyaTulissRelatedByPtkIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->karyaTulissRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collKaryaTulissRelatedByPtkId !== null) {
                foreach ($this->collKaryaTulissRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->karyaTulissRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->karyaTulissRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    KaryaTulisQuery::create()
                        ->filterByPrimaryKeys($this->karyaTulissRelatedByPtkIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->karyaTulissRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collKaryaTulissRelatedByPtkId !== null) {
                foreach ($this->collKaryaTulissRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->nilaiTestsRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->nilaiTestsRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    NilaiTestQuery::create()
                        ->filterByPrimaryKeys($this->nilaiTestsRelatedByPtkIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->nilaiTestsRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collNilaiTestsRelatedByPtkId !== null) {
                foreach ($this->collNilaiTestsRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->nilaiTestsRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->nilaiTestsRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    NilaiTestQuery::create()
                        ->filterByPrimaryKeys($this->nilaiTestsRelatedByPtkIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->nilaiTestsRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collNilaiTestsRelatedByPtkId !== null) {
                foreach ($this->collNilaiTestsRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->bukuPtksRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->bukuPtksRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    BukuPtkQuery::create()
                        ->filterByPrimaryKeys($this->bukuPtksRelatedByPtkIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->bukuPtksRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collBukuPtksRelatedByPtkId !== null) {
                foreach ($this->collBukuPtksRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->bukuPtksRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->bukuPtksRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    BukuPtkQuery::create()
                        ->filterByPrimaryKeys($this->bukuPtksRelatedByPtkIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->bukuPtksRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collBukuPtksRelatedByPtkId !== null) {
                foreach ($this->collBukuPtksRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->beasiswaPtksRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->beasiswaPtksRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    BeasiswaPtkQuery::create()
                        ->filterByPrimaryKeys($this->beasiswaPtksRelatedByPtkIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->beasiswaPtksRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collBeasiswaPtksRelatedByPtkId !== null) {
                foreach ($this->collBeasiswaPtksRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->beasiswaPtksRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->beasiswaPtksRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    BeasiswaPtkQuery::create()
                        ->filterByPrimaryKeys($this->beasiswaPtksRelatedByPtkIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->beasiswaPtksRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collBeasiswaPtksRelatedByPtkId !== null) {
                foreach ($this->collBeasiswaPtksRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rwyKepangkatansRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->rwyKepangkatansRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    RwyKepangkatanQuery::create()
                        ->filterByPrimaryKeys($this->rwyKepangkatansRelatedByPtkIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rwyKepangkatansRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collRwyKepangkatansRelatedByPtkId !== null) {
                foreach ($this->collRwyKepangkatansRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rwyKepangkatansRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->rwyKepangkatansRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    RwyKepangkatanQuery::create()
                        ->filterByPrimaryKeys($this->rwyKepangkatansRelatedByPtkIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rwyKepangkatansRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collRwyKepangkatansRelatedByPtkId !== null) {
                foreach ($this->collRwyKepangkatansRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rombonganBelajarsRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->rombonganBelajarsRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    RombonganBelajarQuery::create()
                        ->filterByPrimaryKeys($this->rombonganBelajarsRelatedByPtkIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rombonganBelajarsRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collRombonganBelajarsRelatedByPtkId !== null) {
                foreach ($this->collRombonganBelajarsRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rombonganBelajarsRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->rombonganBelajarsRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    RombonganBelajarQuery::create()
                        ->filterByPrimaryKeys($this->rombonganBelajarsRelatedByPtkIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rombonganBelajarsRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collRombonganBelajarsRelatedByPtkId !== null) {
                foreach ($this->collRombonganBelajarsRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->riwayatGajiBerkalasRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->riwayatGajiBerkalasRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    RiwayatGajiBerkalaQuery::create()
                        ->filterByPrimaryKeys($this->riwayatGajiBerkalasRelatedByPtkIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->riwayatGajiBerkalasRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collRiwayatGajiBerkalasRelatedByPtkId !== null) {
                foreach ($this->collRiwayatGajiBerkalasRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->riwayatGajiBerkalasRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->riwayatGajiBerkalasRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    RiwayatGajiBerkalaQuery::create()
                        ->filterByPrimaryKeys($this->riwayatGajiBerkalasRelatedByPtkIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->riwayatGajiBerkalasRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collRiwayatGajiBerkalasRelatedByPtkId !== null) {
                foreach ($this->collRiwayatGajiBerkalasRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->pengawasTerdaftarsRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->pengawasTerdaftarsRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    PengawasTerdaftarQuery::create()
                        ->filterByPrimaryKeys($this->pengawasTerdaftarsRelatedByPtkIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->pengawasTerdaftarsRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collPengawasTerdaftarsRelatedByPtkId !== null) {
                foreach ($this->collPengawasTerdaftarsRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->pengawasTerdaftarsRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->pengawasTerdaftarsRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    PengawasTerdaftarQuery::create()
                        ->filterByPrimaryKeys($this->pengawasTerdaftarsRelatedByPtkIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->pengawasTerdaftarsRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collPengawasTerdaftarsRelatedByPtkId !== null) {
                foreach ($this->collPengawasTerdaftarsRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rwyStrukturalsRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->rwyStrukturalsRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    RwyStrukturalQuery::create()
                        ->filterByPrimaryKeys($this->rwyStrukturalsRelatedByPtkIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rwyStrukturalsRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collRwyStrukturalsRelatedByPtkId !== null) {
                foreach ($this->collRwyStrukturalsRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rwyStrukturalsRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->rwyStrukturalsRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    RwyStrukturalQuery::create()
                        ->filterByPrimaryKeys($this->rwyStrukturalsRelatedByPtkIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rwyStrukturalsRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collRwyStrukturalsRelatedByPtkId !== null) {
                foreach ($this->collRwyStrukturalsRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rwyPendFormalsRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->rwyPendFormalsRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    RwyPendFormalQuery::create()
                        ->filterByPrimaryKeys($this->rwyPendFormalsRelatedByPtkIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rwyPendFormalsRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collRwyPendFormalsRelatedByPtkId !== null) {
                foreach ($this->collRwyPendFormalsRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rwyPendFormalsRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->rwyPendFormalsRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    RwyPendFormalQuery::create()
                        ->filterByPrimaryKeys($this->rwyPendFormalsRelatedByPtkIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rwyPendFormalsRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collRwyPendFormalsRelatedByPtkId !== null) {
                foreach ($this->collRwyPendFormalsRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->anaksRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->anaksRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    AnakQuery::create()
                        ->filterByPrimaryKeys($this->anaksRelatedByPtkIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->anaksRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collAnaksRelatedByPtkId !== null) {
                foreach ($this->collAnaksRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->anaksRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->anaksRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    AnakQuery::create()
                        ->filterByPrimaryKeys($this->anaksRelatedByPtkIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->anaksRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collAnaksRelatedByPtkId !== null) {
                foreach ($this->collAnaksRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->diklatsRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->diklatsRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    DiklatQuery::create()
                        ->filterByPrimaryKeys($this->diklatsRelatedByPtkIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->diklatsRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collDiklatsRelatedByPtkId !== null) {
                foreach ($this->collDiklatsRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->diklatsRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->diklatsRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    DiklatQuery::create()
                        ->filterByPrimaryKeys($this->diklatsRelatedByPtkIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->diklatsRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collDiklatsRelatedByPtkId !== null) {
                foreach ($this->collDiklatsRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->ptkBarusRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->ptkBarusRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->ptkBarusRelatedByPtkIdScheduledForDeletion as $ptkBaruRelatedByPtkId) {
                        // need to save related object because we set the relation to null
                        $ptkBaruRelatedByPtkId->save($con);
                    }
                    $this->ptkBarusRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collPtkBarusRelatedByPtkId !== null) {
                foreach ($this->collPtkBarusRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->ptkBarusRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->ptkBarusRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->ptkBarusRelatedByPtkIdScheduledForDeletion as $ptkBaruRelatedByPtkId) {
                        // need to save related object because we set the relation to null
                        $ptkBaruRelatedByPtkId->save($con);
                    }
                    $this->ptkBarusRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collPtkBarusRelatedByPtkId !== null) {
                foreach ($this->collPtkBarusRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->tunjangansRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->tunjangansRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    TunjanganQuery::create()
                        ->filterByPrimaryKeys($this->tunjangansRelatedByPtkIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->tunjangansRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collTunjangansRelatedByPtkId !== null) {
                foreach ($this->collTunjangansRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->tunjangansRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->tunjangansRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    TunjanganQuery::create()
                        ->filterByPrimaryKeys($this->tunjangansRelatedByPtkIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->tunjangansRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collTunjangansRelatedByPtkId !== null) {
                foreach ($this->collTunjangansRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rwySertifikasisRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->rwySertifikasisRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    RwySertifikasiQuery::create()
                        ->filterByPrimaryKeys($this->rwySertifikasisRelatedByPtkIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rwySertifikasisRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collRwySertifikasisRelatedByPtkId !== null) {
                foreach ($this->collRwySertifikasisRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rwySertifikasisRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->rwySertifikasisRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    RwySertifikasiQuery::create()
                        ->filterByPrimaryKeys($this->rwySertifikasisRelatedByPtkIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rwySertifikasisRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collRwySertifikasisRelatedByPtkId !== null) {
                foreach ($this->collRwySertifikasisRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->tugasTambahansRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->tugasTambahansRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    TugasTambahanQuery::create()
                        ->filterByPrimaryKeys($this->tugasTambahansRelatedByPtkIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->tugasTambahansRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collTugasTambahansRelatedByPtkId !== null) {
                foreach ($this->collTugasTambahansRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->tugasTambahansRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->tugasTambahansRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    TugasTambahanQuery::create()
                        ->filterByPrimaryKeys($this->tugasTambahansRelatedByPtkIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->tugasTambahansRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collTugasTambahansRelatedByPtkId !== null) {
                foreach ($this->collTugasTambahansRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rwyFungsionalsRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->rwyFungsionalsRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    RwyFungsionalQuery::create()
                        ->filterByPrimaryKeys($this->rwyFungsionalsRelatedByPtkIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rwyFungsionalsRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collRwyFungsionalsRelatedByPtkId !== null) {
                foreach ($this->collRwyFungsionalsRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rwyFungsionalsRelatedByPtkIdScheduledForDeletion !== null) {
                if (!$this->rwyFungsionalsRelatedByPtkIdScheduledForDeletion->isEmpty()) {
                    RwyFungsionalQuery::create()
                        ->filterByPrimaryKeys($this->rwyFungsionalsRelatedByPtkIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rwyFungsionalsRelatedByPtkIdScheduledForDeletion = null;
                }
            }

            if ($this->collRwyFungsionalsRelatedByPtkId !== null) {
                foreach ($this->collRwyFungsionalsRelatedByPtkId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $criteria = $this->buildCriteria();
        $pk = BasePeer::doInsert($criteria, $con);
        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSekolahRelatedByEntrySekolahId !== null) {
                if (!$this->aSekolahRelatedByEntrySekolahId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSekolahRelatedByEntrySekolahId->getValidationFailures());
                }
            }

            if ($this->aSekolahRelatedByEntrySekolahId !== null) {
                if (!$this->aSekolahRelatedByEntrySekolahId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSekolahRelatedByEntrySekolahId->getValidationFailures());
                }
            }

            if ($this->aAgamaRelatedByAgamaId !== null) {
                if (!$this->aAgamaRelatedByAgamaId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aAgamaRelatedByAgamaId->getValidationFailures());
                }
            }

            if ($this->aAgamaRelatedByAgamaId !== null) {
                if (!$this->aAgamaRelatedByAgamaId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aAgamaRelatedByAgamaId->getValidationFailures());
                }
            }

            if ($this->aBidangStudiRelatedByPengawasBidangStudiId !== null) {
                if (!$this->aBidangStudiRelatedByPengawasBidangStudiId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aBidangStudiRelatedByPengawasBidangStudiId->getValidationFailures());
                }
            }

            if ($this->aBidangStudiRelatedByPengawasBidangStudiId !== null) {
                if (!$this->aBidangStudiRelatedByPengawasBidangStudiId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aBidangStudiRelatedByPengawasBidangStudiId->getValidationFailures());
                }
            }

            if ($this->aJenisPtkRelatedByJenisPtkId !== null) {
                if (!$this->aJenisPtkRelatedByJenisPtkId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aJenisPtkRelatedByJenisPtkId->getValidationFailures());
                }
            }

            if ($this->aJenisPtkRelatedByJenisPtkId !== null) {
                if (!$this->aJenisPtkRelatedByJenisPtkId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aJenisPtkRelatedByJenisPtkId->getValidationFailures());
                }
            }

            if ($this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId !== null) {
                if (!$this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId->getValidationFailures());
                }
            }

            if ($this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId !== null) {
                if (!$this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId->getValidationFailures());
                }
            }

            if ($this->aKebutuhanKhususRelatedByMampuHandleKk !== null) {
                if (!$this->aKebutuhanKhususRelatedByMampuHandleKk->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aKebutuhanKhususRelatedByMampuHandleKk->getValidationFailures());
                }
            }

            if ($this->aKebutuhanKhususRelatedByMampuHandleKk !== null) {
                if (!$this->aKebutuhanKhususRelatedByMampuHandleKk->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aKebutuhanKhususRelatedByMampuHandleKk->getValidationFailures());
                }
            }

            if ($this->aLembagaPengangkatRelatedByLembagaPengangkatId !== null) {
                if (!$this->aLembagaPengangkatRelatedByLembagaPengangkatId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aLembagaPengangkatRelatedByLembagaPengangkatId->getValidationFailures());
                }
            }

            if ($this->aLembagaPengangkatRelatedByLembagaPengangkatId !== null) {
                if (!$this->aLembagaPengangkatRelatedByLembagaPengangkatId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aLembagaPengangkatRelatedByLembagaPengangkatId->getValidationFailures());
                }
            }

            if ($this->aMstWilayahRelatedByKodeWilayah !== null) {
                if (!$this->aMstWilayahRelatedByKodeWilayah->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aMstWilayahRelatedByKodeWilayah->getValidationFailures());
                }
            }

            if ($this->aMstWilayahRelatedByKodeWilayah !== null) {
                if (!$this->aMstWilayahRelatedByKodeWilayah->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aMstWilayahRelatedByKodeWilayah->getValidationFailures());
                }
            }

            if ($this->aNegaraRelatedByKewarganegaraan !== null) {
                if (!$this->aNegaraRelatedByKewarganegaraan->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aNegaraRelatedByKewarganegaraan->getValidationFailures());
                }
            }

            if ($this->aNegaraRelatedByKewarganegaraan !== null) {
                if (!$this->aNegaraRelatedByKewarganegaraan->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aNegaraRelatedByKewarganegaraan->getValidationFailures());
                }
            }

            if ($this->aPangkatGolonganRelatedByPangkatGolonganId !== null) {
                if (!$this->aPangkatGolonganRelatedByPangkatGolonganId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPangkatGolonganRelatedByPangkatGolonganId->getValidationFailures());
                }
            }

            if ($this->aPangkatGolonganRelatedByPangkatGolonganId !== null) {
                if (!$this->aPangkatGolonganRelatedByPangkatGolonganId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPangkatGolonganRelatedByPangkatGolonganId->getValidationFailures());
                }
            }

            if ($this->aPekerjaanRelatedByPekerjaanSuamiIstri !== null) {
                if (!$this->aPekerjaanRelatedByPekerjaanSuamiIstri->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPekerjaanRelatedByPekerjaanSuamiIstri->getValidationFailures());
                }
            }

            if ($this->aPekerjaanRelatedByPekerjaanSuamiIstri !== null) {
                if (!$this->aPekerjaanRelatedByPekerjaanSuamiIstri->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPekerjaanRelatedByPekerjaanSuamiIstri->getValidationFailures());
                }
            }

            if ($this->aStatusKepegawaianRelatedByStatusKepegawaianId !== null) {
                if (!$this->aStatusKepegawaianRelatedByStatusKepegawaianId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aStatusKepegawaianRelatedByStatusKepegawaianId->getValidationFailures());
                }
            }

            if ($this->aStatusKepegawaianRelatedByStatusKepegawaianId !== null) {
                if (!$this->aStatusKepegawaianRelatedByStatusKepegawaianId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aStatusKepegawaianRelatedByStatusKepegawaianId->getValidationFailures());
                }
            }

            if ($this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId !== null) {
                if (!$this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId->getValidationFailures());
                }
            }

            if ($this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId !== null) {
                if (!$this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId->getValidationFailures());
                }
            }

            if ($this->aSumberGajiRelatedBySumberGajiId !== null) {
                if (!$this->aSumberGajiRelatedBySumberGajiId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSumberGajiRelatedBySumberGajiId->getValidationFailures());
                }
            }

            if ($this->aSumberGajiRelatedBySumberGajiId !== null) {
                if (!$this->aSumberGajiRelatedBySumberGajiId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSumberGajiRelatedBySumberGajiId->getValidationFailures());
                }
            }


            if (($retval = PtkPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collKesejahteraansRelatedByPtkId !== null) {
                    foreach ($this->collKesejahteraansRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collKesejahteraansRelatedByPtkId !== null) {
                    foreach ($this->collKesejahteraansRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldPtksRelatedByPtkId !== null) {
                    foreach ($this->collVldPtksRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldPtksRelatedByPtkId !== null) {
                    foreach ($this->collVldPtksRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPenghargaansRelatedByPtkId !== null) {
                    foreach ($this->collPenghargaansRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPenghargaansRelatedByPtkId !== null) {
                    foreach ($this->collPenghargaansRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collInpassingsRelatedByPtkId !== null) {
                    foreach ($this->collInpassingsRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collInpassingsRelatedByPtkId !== null) {
                    foreach ($this->collInpassingsRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPtkTerdaftarsRelatedByPtkId !== null) {
                    foreach ($this->collPtkTerdaftarsRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPtkTerdaftarsRelatedByPtkId !== null) {
                    foreach ($this->collPtkTerdaftarsRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collKaryaTulissRelatedByPtkId !== null) {
                    foreach ($this->collKaryaTulissRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collKaryaTulissRelatedByPtkId !== null) {
                    foreach ($this->collKaryaTulissRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collNilaiTestsRelatedByPtkId !== null) {
                    foreach ($this->collNilaiTestsRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collNilaiTestsRelatedByPtkId !== null) {
                    foreach ($this->collNilaiTestsRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collBukuPtksRelatedByPtkId !== null) {
                    foreach ($this->collBukuPtksRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collBukuPtksRelatedByPtkId !== null) {
                    foreach ($this->collBukuPtksRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collBeasiswaPtksRelatedByPtkId !== null) {
                    foreach ($this->collBeasiswaPtksRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collBeasiswaPtksRelatedByPtkId !== null) {
                    foreach ($this->collBeasiswaPtksRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRwyKepangkatansRelatedByPtkId !== null) {
                    foreach ($this->collRwyKepangkatansRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRwyKepangkatansRelatedByPtkId !== null) {
                    foreach ($this->collRwyKepangkatansRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRombonganBelajarsRelatedByPtkId !== null) {
                    foreach ($this->collRombonganBelajarsRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRombonganBelajarsRelatedByPtkId !== null) {
                    foreach ($this->collRombonganBelajarsRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRiwayatGajiBerkalasRelatedByPtkId !== null) {
                    foreach ($this->collRiwayatGajiBerkalasRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRiwayatGajiBerkalasRelatedByPtkId !== null) {
                    foreach ($this->collRiwayatGajiBerkalasRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPengawasTerdaftarsRelatedByPtkId !== null) {
                    foreach ($this->collPengawasTerdaftarsRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPengawasTerdaftarsRelatedByPtkId !== null) {
                    foreach ($this->collPengawasTerdaftarsRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRwyStrukturalsRelatedByPtkId !== null) {
                    foreach ($this->collRwyStrukturalsRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRwyStrukturalsRelatedByPtkId !== null) {
                    foreach ($this->collRwyStrukturalsRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRwyPendFormalsRelatedByPtkId !== null) {
                    foreach ($this->collRwyPendFormalsRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRwyPendFormalsRelatedByPtkId !== null) {
                    foreach ($this->collRwyPendFormalsRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collAnaksRelatedByPtkId !== null) {
                    foreach ($this->collAnaksRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collAnaksRelatedByPtkId !== null) {
                    foreach ($this->collAnaksRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collDiklatsRelatedByPtkId !== null) {
                    foreach ($this->collDiklatsRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collDiklatsRelatedByPtkId !== null) {
                    foreach ($this->collDiklatsRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPtkBarusRelatedByPtkId !== null) {
                    foreach ($this->collPtkBarusRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPtkBarusRelatedByPtkId !== null) {
                    foreach ($this->collPtkBarusRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collTunjangansRelatedByPtkId !== null) {
                    foreach ($this->collTunjangansRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collTunjangansRelatedByPtkId !== null) {
                    foreach ($this->collTunjangansRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRwySertifikasisRelatedByPtkId !== null) {
                    foreach ($this->collRwySertifikasisRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRwySertifikasisRelatedByPtkId !== null) {
                    foreach ($this->collRwySertifikasisRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collTugasTambahansRelatedByPtkId !== null) {
                    foreach ($this->collTugasTambahansRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collTugasTambahansRelatedByPtkId !== null) {
                    foreach ($this->collTugasTambahansRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRwyFungsionalsRelatedByPtkId !== null) {
                    foreach ($this->collRwyFungsionalsRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRwyFungsionalsRelatedByPtkId !== null) {
                    foreach ($this->collRwyFungsionalsRelatedByPtkId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = PtkPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getPtkId();
                break;
            case 1:
                return $this->getNama();
                break;
            case 2:
                return $this->getNip();
                break;
            case 3:
                return $this->getJenisKelamin();
                break;
            case 4:
                return $this->getTempatLahir();
                break;
            case 5:
                return $this->getTanggalLahir();
                break;
            case 6:
                return $this->getNik();
                break;
            case 7:
                return $this->getNiyNigk();
                break;
            case 8:
                return $this->getNuptk();
                break;
            case 9:
                return $this->getStatusKepegawaianId();
                break;
            case 10:
                return $this->getJenisPtkId();
                break;
            case 11:
                return $this->getPengawasBidangStudiId();
                break;
            case 12:
                return $this->getAgamaId();
                break;
            case 13:
                return $this->getKewarganegaraan();
                break;
            case 14:
                return $this->getAlamatJalan();
                break;
            case 15:
                return $this->getRt();
                break;
            case 16:
                return $this->getRw();
                break;
            case 17:
                return $this->getNamaDusun();
                break;
            case 18:
                return $this->getDesaKelurahan();
                break;
            case 19:
                return $this->getKodeWilayah();
                break;
            case 20:
                return $this->getKodePos();
                break;
            case 21:
                return $this->getNoTeleponRumah();
                break;
            case 22:
                return $this->getNoHp();
                break;
            case 23:
                return $this->getEmail();
                break;
            case 24:
                return $this->getEntrySekolahId();
                break;
            case 25:
                return $this->getStatusKeaktifanId();
                break;
            case 26:
                return $this->getSkCpns();
                break;
            case 27:
                return $this->getTglCpns();
                break;
            case 28:
                return $this->getSkPengangkatan();
                break;
            case 29:
                return $this->getTmtPengangkatan();
                break;
            case 30:
                return $this->getLembagaPengangkatId();
                break;
            case 31:
                return $this->getPangkatGolonganId();
                break;
            case 32:
                return $this->getKeahlianLaboratoriumId();
                break;
            case 33:
                return $this->getSumberGajiId();
                break;
            case 34:
                return $this->getNamaIbuKandung();
                break;
            case 35:
                return $this->getStatusPerkawinan();
                break;
            case 36:
                return $this->getNamaSuamiIstri();
                break;
            case 37:
                return $this->getNipSuamiIstri();
                break;
            case 38:
                return $this->getPekerjaanSuamiIstri();
                break;
            case 39:
                return $this->getTmtPns();
                break;
            case 40:
                return $this->getSudahLisensiKepalaSekolah();
                break;
            case 41:
                return $this->getJumlahSekolahBinaan();
                break;
            case 42:
                return $this->getPernahDiklatKepengawasan();
                break;
            case 43:
                return $this->getStatusData();
                break;
            case 44:
                return $this->getMampuHandleKk();
                break;
            case 45:
                return $this->getKeahlianBraille();
                break;
            case 46:
                return $this->getKeahlianBhsIsyarat();
                break;
            case 47:
                return $this->getNpwp();
                break;
            case 48:
                return $this->getLastUpdate();
                break;
            case 49:
                return $this->getSoftDelete();
                break;
            case 50:
                return $this->getLastSync();
                break;
            case 51:
                return $this->getUpdaterId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Ptk'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Ptk'][$this->getPrimaryKey()] = true;
        $keys = PtkPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getPtkId(),
            $keys[1] => $this->getNama(),
            $keys[2] => $this->getNip(),
            $keys[3] => $this->getJenisKelamin(),
            $keys[4] => $this->getTempatLahir(),
            $keys[5] => $this->getTanggalLahir(),
            $keys[6] => $this->getNik(),
            $keys[7] => $this->getNiyNigk(),
            $keys[8] => $this->getNuptk(),
            $keys[9] => $this->getStatusKepegawaianId(),
            $keys[10] => $this->getJenisPtkId(),
            $keys[11] => $this->getPengawasBidangStudiId(),
            $keys[12] => $this->getAgamaId(),
            $keys[13] => $this->getKewarganegaraan(),
            $keys[14] => $this->getAlamatJalan(),
            $keys[15] => $this->getRt(),
            $keys[16] => $this->getRw(),
            $keys[17] => $this->getNamaDusun(),
            $keys[18] => $this->getDesaKelurahan(),
            $keys[19] => $this->getKodeWilayah(),
            $keys[20] => $this->getKodePos(),
            $keys[21] => $this->getNoTeleponRumah(),
            $keys[22] => $this->getNoHp(),
            $keys[23] => $this->getEmail(),
            $keys[24] => $this->getEntrySekolahId(),
            $keys[25] => $this->getStatusKeaktifanId(),
            $keys[26] => $this->getSkCpns(),
            $keys[27] => $this->getTglCpns(),
            $keys[28] => $this->getSkPengangkatan(),
            $keys[29] => $this->getTmtPengangkatan(),
            $keys[30] => $this->getLembagaPengangkatId(),
            $keys[31] => $this->getPangkatGolonganId(),
            $keys[32] => $this->getKeahlianLaboratoriumId(),
            $keys[33] => $this->getSumberGajiId(),
            $keys[34] => $this->getNamaIbuKandung(),
            $keys[35] => $this->getStatusPerkawinan(),
            $keys[36] => $this->getNamaSuamiIstri(),
            $keys[37] => $this->getNipSuamiIstri(),
            $keys[38] => $this->getPekerjaanSuamiIstri(),
            $keys[39] => $this->getTmtPns(),
            $keys[40] => $this->getSudahLisensiKepalaSekolah(),
            $keys[41] => $this->getJumlahSekolahBinaan(),
            $keys[42] => $this->getPernahDiklatKepengawasan(),
            $keys[43] => $this->getStatusData(),
            $keys[44] => $this->getMampuHandleKk(),
            $keys[45] => $this->getKeahlianBraille(),
            $keys[46] => $this->getKeahlianBhsIsyarat(),
            $keys[47] => $this->getNpwp(),
            $keys[48] => $this->getLastUpdate(),
            $keys[49] => $this->getSoftDelete(),
            $keys[50] => $this->getLastSync(),
            $keys[51] => $this->getUpdaterId(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aSekolahRelatedByEntrySekolahId) {
                $result['SekolahRelatedByEntrySekolahId'] = $this->aSekolahRelatedByEntrySekolahId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSekolahRelatedByEntrySekolahId) {
                $result['SekolahRelatedByEntrySekolahId'] = $this->aSekolahRelatedByEntrySekolahId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aAgamaRelatedByAgamaId) {
                $result['AgamaRelatedByAgamaId'] = $this->aAgamaRelatedByAgamaId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aAgamaRelatedByAgamaId) {
                $result['AgamaRelatedByAgamaId'] = $this->aAgamaRelatedByAgamaId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aBidangStudiRelatedByPengawasBidangStudiId) {
                $result['BidangStudiRelatedByPengawasBidangStudiId'] = $this->aBidangStudiRelatedByPengawasBidangStudiId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aBidangStudiRelatedByPengawasBidangStudiId) {
                $result['BidangStudiRelatedByPengawasBidangStudiId'] = $this->aBidangStudiRelatedByPengawasBidangStudiId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aJenisPtkRelatedByJenisPtkId) {
                $result['JenisPtkRelatedByJenisPtkId'] = $this->aJenisPtkRelatedByJenisPtkId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aJenisPtkRelatedByJenisPtkId) {
                $result['JenisPtkRelatedByJenisPtkId'] = $this->aJenisPtkRelatedByJenisPtkId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId) {
                $result['KeahlianLaboratoriumRelatedByKeahlianLaboratoriumId'] = $this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId) {
                $result['KeahlianLaboratoriumRelatedByKeahlianLaboratoriumId'] = $this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aKebutuhanKhususRelatedByMampuHandleKk) {
                $result['KebutuhanKhususRelatedByMampuHandleKk'] = $this->aKebutuhanKhususRelatedByMampuHandleKk->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aKebutuhanKhususRelatedByMampuHandleKk) {
                $result['KebutuhanKhususRelatedByMampuHandleKk'] = $this->aKebutuhanKhususRelatedByMampuHandleKk->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aLembagaPengangkatRelatedByLembagaPengangkatId) {
                $result['LembagaPengangkatRelatedByLembagaPengangkatId'] = $this->aLembagaPengangkatRelatedByLembagaPengangkatId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aLembagaPengangkatRelatedByLembagaPengangkatId) {
                $result['LembagaPengangkatRelatedByLembagaPengangkatId'] = $this->aLembagaPengangkatRelatedByLembagaPengangkatId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aMstWilayahRelatedByKodeWilayah) {
                $result['MstWilayahRelatedByKodeWilayah'] = $this->aMstWilayahRelatedByKodeWilayah->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aMstWilayahRelatedByKodeWilayah) {
                $result['MstWilayahRelatedByKodeWilayah'] = $this->aMstWilayahRelatedByKodeWilayah->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aNegaraRelatedByKewarganegaraan) {
                $result['NegaraRelatedByKewarganegaraan'] = $this->aNegaraRelatedByKewarganegaraan->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aNegaraRelatedByKewarganegaraan) {
                $result['NegaraRelatedByKewarganegaraan'] = $this->aNegaraRelatedByKewarganegaraan->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPangkatGolonganRelatedByPangkatGolonganId) {
                $result['PangkatGolonganRelatedByPangkatGolonganId'] = $this->aPangkatGolonganRelatedByPangkatGolonganId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPangkatGolonganRelatedByPangkatGolonganId) {
                $result['PangkatGolonganRelatedByPangkatGolonganId'] = $this->aPangkatGolonganRelatedByPangkatGolonganId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPekerjaanRelatedByPekerjaanSuamiIstri) {
                $result['PekerjaanRelatedByPekerjaanSuamiIstri'] = $this->aPekerjaanRelatedByPekerjaanSuamiIstri->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPekerjaanRelatedByPekerjaanSuamiIstri) {
                $result['PekerjaanRelatedByPekerjaanSuamiIstri'] = $this->aPekerjaanRelatedByPekerjaanSuamiIstri->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aStatusKepegawaianRelatedByStatusKepegawaianId) {
                $result['StatusKepegawaianRelatedByStatusKepegawaianId'] = $this->aStatusKepegawaianRelatedByStatusKepegawaianId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aStatusKepegawaianRelatedByStatusKepegawaianId) {
                $result['StatusKepegawaianRelatedByStatusKepegawaianId'] = $this->aStatusKepegawaianRelatedByStatusKepegawaianId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId) {
                $result['StatusKeaktifanPegawaiRelatedByStatusKeaktifanId'] = $this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId) {
                $result['StatusKeaktifanPegawaiRelatedByStatusKeaktifanId'] = $this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSumberGajiRelatedBySumberGajiId) {
                $result['SumberGajiRelatedBySumberGajiId'] = $this->aSumberGajiRelatedBySumberGajiId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSumberGajiRelatedBySumberGajiId) {
                $result['SumberGajiRelatedBySumberGajiId'] = $this->aSumberGajiRelatedBySumberGajiId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collKesejahteraansRelatedByPtkId) {
                $result['KesejahteraansRelatedByPtkId'] = $this->collKesejahteraansRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collKesejahteraansRelatedByPtkId) {
                $result['KesejahteraansRelatedByPtkId'] = $this->collKesejahteraansRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldPtksRelatedByPtkId) {
                $result['VldPtksRelatedByPtkId'] = $this->collVldPtksRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldPtksRelatedByPtkId) {
                $result['VldPtksRelatedByPtkId'] = $this->collVldPtksRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPenghargaansRelatedByPtkId) {
                $result['PenghargaansRelatedByPtkId'] = $this->collPenghargaansRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPenghargaansRelatedByPtkId) {
                $result['PenghargaansRelatedByPtkId'] = $this->collPenghargaansRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collInpassingsRelatedByPtkId) {
                $result['InpassingsRelatedByPtkId'] = $this->collInpassingsRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collInpassingsRelatedByPtkId) {
                $result['InpassingsRelatedByPtkId'] = $this->collInpassingsRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPtkTerdaftarsRelatedByPtkId) {
                $result['PtkTerdaftarsRelatedByPtkId'] = $this->collPtkTerdaftarsRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPtkTerdaftarsRelatedByPtkId) {
                $result['PtkTerdaftarsRelatedByPtkId'] = $this->collPtkTerdaftarsRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collKaryaTulissRelatedByPtkId) {
                $result['KaryaTulissRelatedByPtkId'] = $this->collKaryaTulissRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collKaryaTulissRelatedByPtkId) {
                $result['KaryaTulissRelatedByPtkId'] = $this->collKaryaTulissRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collNilaiTestsRelatedByPtkId) {
                $result['NilaiTestsRelatedByPtkId'] = $this->collNilaiTestsRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collNilaiTestsRelatedByPtkId) {
                $result['NilaiTestsRelatedByPtkId'] = $this->collNilaiTestsRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collBukuPtksRelatedByPtkId) {
                $result['BukuPtksRelatedByPtkId'] = $this->collBukuPtksRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collBukuPtksRelatedByPtkId) {
                $result['BukuPtksRelatedByPtkId'] = $this->collBukuPtksRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collBeasiswaPtksRelatedByPtkId) {
                $result['BeasiswaPtksRelatedByPtkId'] = $this->collBeasiswaPtksRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collBeasiswaPtksRelatedByPtkId) {
                $result['BeasiswaPtksRelatedByPtkId'] = $this->collBeasiswaPtksRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRwyKepangkatansRelatedByPtkId) {
                $result['RwyKepangkatansRelatedByPtkId'] = $this->collRwyKepangkatansRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRwyKepangkatansRelatedByPtkId) {
                $result['RwyKepangkatansRelatedByPtkId'] = $this->collRwyKepangkatansRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRombonganBelajarsRelatedByPtkId) {
                $result['RombonganBelajarsRelatedByPtkId'] = $this->collRombonganBelajarsRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRombonganBelajarsRelatedByPtkId) {
                $result['RombonganBelajarsRelatedByPtkId'] = $this->collRombonganBelajarsRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRiwayatGajiBerkalasRelatedByPtkId) {
                $result['RiwayatGajiBerkalasRelatedByPtkId'] = $this->collRiwayatGajiBerkalasRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRiwayatGajiBerkalasRelatedByPtkId) {
                $result['RiwayatGajiBerkalasRelatedByPtkId'] = $this->collRiwayatGajiBerkalasRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPengawasTerdaftarsRelatedByPtkId) {
                $result['PengawasTerdaftarsRelatedByPtkId'] = $this->collPengawasTerdaftarsRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPengawasTerdaftarsRelatedByPtkId) {
                $result['PengawasTerdaftarsRelatedByPtkId'] = $this->collPengawasTerdaftarsRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRwyStrukturalsRelatedByPtkId) {
                $result['RwyStrukturalsRelatedByPtkId'] = $this->collRwyStrukturalsRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRwyStrukturalsRelatedByPtkId) {
                $result['RwyStrukturalsRelatedByPtkId'] = $this->collRwyStrukturalsRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRwyPendFormalsRelatedByPtkId) {
                $result['RwyPendFormalsRelatedByPtkId'] = $this->collRwyPendFormalsRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRwyPendFormalsRelatedByPtkId) {
                $result['RwyPendFormalsRelatedByPtkId'] = $this->collRwyPendFormalsRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collAnaksRelatedByPtkId) {
                $result['AnaksRelatedByPtkId'] = $this->collAnaksRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collAnaksRelatedByPtkId) {
                $result['AnaksRelatedByPtkId'] = $this->collAnaksRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collDiklatsRelatedByPtkId) {
                $result['DiklatsRelatedByPtkId'] = $this->collDiklatsRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collDiklatsRelatedByPtkId) {
                $result['DiklatsRelatedByPtkId'] = $this->collDiklatsRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPtkBarusRelatedByPtkId) {
                $result['PtkBarusRelatedByPtkId'] = $this->collPtkBarusRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPtkBarusRelatedByPtkId) {
                $result['PtkBarusRelatedByPtkId'] = $this->collPtkBarusRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collTunjangansRelatedByPtkId) {
                $result['TunjangansRelatedByPtkId'] = $this->collTunjangansRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collTunjangansRelatedByPtkId) {
                $result['TunjangansRelatedByPtkId'] = $this->collTunjangansRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRwySertifikasisRelatedByPtkId) {
                $result['RwySertifikasisRelatedByPtkId'] = $this->collRwySertifikasisRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRwySertifikasisRelatedByPtkId) {
                $result['RwySertifikasisRelatedByPtkId'] = $this->collRwySertifikasisRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collTugasTambahansRelatedByPtkId) {
                $result['TugasTambahansRelatedByPtkId'] = $this->collTugasTambahansRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collTugasTambahansRelatedByPtkId) {
                $result['TugasTambahansRelatedByPtkId'] = $this->collTugasTambahansRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRwyFungsionalsRelatedByPtkId) {
                $result['RwyFungsionalsRelatedByPtkId'] = $this->collRwyFungsionalsRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRwyFungsionalsRelatedByPtkId) {
                $result['RwyFungsionalsRelatedByPtkId'] = $this->collRwyFungsionalsRelatedByPtkId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = PtkPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setPtkId($value);
                break;
            case 1:
                $this->setNama($value);
                break;
            case 2:
                $this->setNip($value);
                break;
            case 3:
                $this->setJenisKelamin($value);
                break;
            case 4:
                $this->setTempatLahir($value);
                break;
            case 5:
                $this->setTanggalLahir($value);
                break;
            case 6:
                $this->setNik($value);
                break;
            case 7:
                $this->setNiyNigk($value);
                break;
            case 8:
                $this->setNuptk($value);
                break;
            case 9:
                $this->setStatusKepegawaianId($value);
                break;
            case 10:
                $this->setJenisPtkId($value);
                break;
            case 11:
                $this->setPengawasBidangStudiId($value);
                break;
            case 12:
                $this->setAgamaId($value);
                break;
            case 13:
                $this->setKewarganegaraan($value);
                break;
            case 14:
                $this->setAlamatJalan($value);
                break;
            case 15:
                $this->setRt($value);
                break;
            case 16:
                $this->setRw($value);
                break;
            case 17:
                $this->setNamaDusun($value);
                break;
            case 18:
                $this->setDesaKelurahan($value);
                break;
            case 19:
                $this->setKodeWilayah($value);
                break;
            case 20:
                $this->setKodePos($value);
                break;
            case 21:
                $this->setNoTeleponRumah($value);
                break;
            case 22:
                $this->setNoHp($value);
                break;
            case 23:
                $this->setEmail($value);
                break;
            case 24:
                $this->setEntrySekolahId($value);
                break;
            case 25:
                $this->setStatusKeaktifanId($value);
                break;
            case 26:
                $this->setSkCpns($value);
                break;
            case 27:
                $this->setTglCpns($value);
                break;
            case 28:
                $this->setSkPengangkatan($value);
                break;
            case 29:
                $this->setTmtPengangkatan($value);
                break;
            case 30:
                $this->setLembagaPengangkatId($value);
                break;
            case 31:
                $this->setPangkatGolonganId($value);
                break;
            case 32:
                $this->setKeahlianLaboratoriumId($value);
                break;
            case 33:
                $this->setSumberGajiId($value);
                break;
            case 34:
                $this->setNamaIbuKandung($value);
                break;
            case 35:
                $this->setStatusPerkawinan($value);
                break;
            case 36:
                $this->setNamaSuamiIstri($value);
                break;
            case 37:
                $this->setNipSuamiIstri($value);
                break;
            case 38:
                $this->setPekerjaanSuamiIstri($value);
                break;
            case 39:
                $this->setTmtPns($value);
                break;
            case 40:
                $this->setSudahLisensiKepalaSekolah($value);
                break;
            case 41:
                $this->setJumlahSekolahBinaan($value);
                break;
            case 42:
                $this->setPernahDiklatKepengawasan($value);
                break;
            case 43:
                $this->setStatusData($value);
                break;
            case 44:
                $this->setMampuHandleKk($value);
                break;
            case 45:
                $this->setKeahlianBraille($value);
                break;
            case 46:
                $this->setKeahlianBhsIsyarat($value);
                break;
            case 47:
                $this->setNpwp($value);
                break;
            case 48:
                $this->setLastUpdate($value);
                break;
            case 49:
                $this->setSoftDelete($value);
                break;
            case 50:
                $this->setLastSync($value);
                break;
            case 51:
                $this->setUpdaterId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = PtkPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setPtkId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setNama($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setNip($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setJenisKelamin($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setTempatLahir($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setTanggalLahir($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setNik($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setNiyNigk($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setNuptk($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setStatusKepegawaianId($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setJenisPtkId($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setPengawasBidangStudiId($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setAgamaId($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setKewarganegaraan($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setAlamatJalan($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setRt($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setRw($arr[$keys[16]]);
        if (array_key_exists($keys[17], $arr)) $this->setNamaDusun($arr[$keys[17]]);
        if (array_key_exists($keys[18], $arr)) $this->setDesaKelurahan($arr[$keys[18]]);
        if (array_key_exists($keys[19], $arr)) $this->setKodeWilayah($arr[$keys[19]]);
        if (array_key_exists($keys[20], $arr)) $this->setKodePos($arr[$keys[20]]);
        if (array_key_exists($keys[21], $arr)) $this->setNoTeleponRumah($arr[$keys[21]]);
        if (array_key_exists($keys[22], $arr)) $this->setNoHp($arr[$keys[22]]);
        if (array_key_exists($keys[23], $arr)) $this->setEmail($arr[$keys[23]]);
        if (array_key_exists($keys[24], $arr)) $this->setEntrySekolahId($arr[$keys[24]]);
        if (array_key_exists($keys[25], $arr)) $this->setStatusKeaktifanId($arr[$keys[25]]);
        if (array_key_exists($keys[26], $arr)) $this->setSkCpns($arr[$keys[26]]);
        if (array_key_exists($keys[27], $arr)) $this->setTglCpns($arr[$keys[27]]);
        if (array_key_exists($keys[28], $arr)) $this->setSkPengangkatan($arr[$keys[28]]);
        if (array_key_exists($keys[29], $arr)) $this->setTmtPengangkatan($arr[$keys[29]]);
        if (array_key_exists($keys[30], $arr)) $this->setLembagaPengangkatId($arr[$keys[30]]);
        if (array_key_exists($keys[31], $arr)) $this->setPangkatGolonganId($arr[$keys[31]]);
        if (array_key_exists($keys[32], $arr)) $this->setKeahlianLaboratoriumId($arr[$keys[32]]);
        if (array_key_exists($keys[33], $arr)) $this->setSumberGajiId($arr[$keys[33]]);
        if (array_key_exists($keys[34], $arr)) $this->setNamaIbuKandung($arr[$keys[34]]);
        if (array_key_exists($keys[35], $arr)) $this->setStatusPerkawinan($arr[$keys[35]]);
        if (array_key_exists($keys[36], $arr)) $this->setNamaSuamiIstri($arr[$keys[36]]);
        if (array_key_exists($keys[37], $arr)) $this->setNipSuamiIstri($arr[$keys[37]]);
        if (array_key_exists($keys[38], $arr)) $this->setPekerjaanSuamiIstri($arr[$keys[38]]);
        if (array_key_exists($keys[39], $arr)) $this->setTmtPns($arr[$keys[39]]);
        if (array_key_exists($keys[40], $arr)) $this->setSudahLisensiKepalaSekolah($arr[$keys[40]]);
        if (array_key_exists($keys[41], $arr)) $this->setJumlahSekolahBinaan($arr[$keys[41]]);
        if (array_key_exists($keys[42], $arr)) $this->setPernahDiklatKepengawasan($arr[$keys[42]]);
        if (array_key_exists($keys[43], $arr)) $this->setStatusData($arr[$keys[43]]);
        if (array_key_exists($keys[44], $arr)) $this->setMampuHandleKk($arr[$keys[44]]);
        if (array_key_exists($keys[45], $arr)) $this->setKeahlianBraille($arr[$keys[45]]);
        if (array_key_exists($keys[46], $arr)) $this->setKeahlianBhsIsyarat($arr[$keys[46]]);
        if (array_key_exists($keys[47], $arr)) $this->setNpwp($arr[$keys[47]]);
        if (array_key_exists($keys[48], $arr)) $this->setLastUpdate($arr[$keys[48]]);
        if (array_key_exists($keys[49], $arr)) $this->setSoftDelete($arr[$keys[49]]);
        if (array_key_exists($keys[50], $arr)) $this->setLastSync($arr[$keys[50]]);
        if (array_key_exists($keys[51], $arr)) $this->setUpdaterId($arr[$keys[51]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(PtkPeer::DATABASE_NAME);

        if ($this->isColumnModified(PtkPeer::PTK_ID)) $criteria->add(PtkPeer::PTK_ID, $this->ptk_id);
        if ($this->isColumnModified(PtkPeer::NAMA)) $criteria->add(PtkPeer::NAMA, $this->nama);
        if ($this->isColumnModified(PtkPeer::NIP)) $criteria->add(PtkPeer::NIP, $this->nip);
        if ($this->isColumnModified(PtkPeer::JENIS_KELAMIN)) $criteria->add(PtkPeer::JENIS_KELAMIN, $this->jenis_kelamin);
        if ($this->isColumnModified(PtkPeer::TEMPAT_LAHIR)) $criteria->add(PtkPeer::TEMPAT_LAHIR, $this->tempat_lahir);
        if ($this->isColumnModified(PtkPeer::TANGGAL_LAHIR)) $criteria->add(PtkPeer::TANGGAL_LAHIR, $this->tanggal_lahir);
        if ($this->isColumnModified(PtkPeer::NIK)) $criteria->add(PtkPeer::NIK, $this->nik);
        if ($this->isColumnModified(PtkPeer::NIY_NIGK)) $criteria->add(PtkPeer::NIY_NIGK, $this->niy_nigk);
        if ($this->isColumnModified(PtkPeer::NUPTK)) $criteria->add(PtkPeer::NUPTK, $this->nuptk);
        if ($this->isColumnModified(PtkPeer::STATUS_KEPEGAWAIAN_ID)) $criteria->add(PtkPeer::STATUS_KEPEGAWAIAN_ID, $this->status_kepegawaian_id);
        if ($this->isColumnModified(PtkPeer::JENIS_PTK_ID)) $criteria->add(PtkPeer::JENIS_PTK_ID, $this->jenis_ptk_id);
        if ($this->isColumnModified(PtkPeer::PENGAWAS_BIDANG_STUDI_ID)) $criteria->add(PtkPeer::PENGAWAS_BIDANG_STUDI_ID, $this->pengawas_bidang_studi_id);
        if ($this->isColumnModified(PtkPeer::AGAMA_ID)) $criteria->add(PtkPeer::AGAMA_ID, $this->agama_id);
        if ($this->isColumnModified(PtkPeer::KEWARGANEGARAAN)) $criteria->add(PtkPeer::KEWARGANEGARAAN, $this->kewarganegaraan);
        if ($this->isColumnModified(PtkPeer::ALAMAT_JALAN)) $criteria->add(PtkPeer::ALAMAT_JALAN, $this->alamat_jalan);
        if ($this->isColumnModified(PtkPeer::RT)) $criteria->add(PtkPeer::RT, $this->rt);
        if ($this->isColumnModified(PtkPeer::RW)) $criteria->add(PtkPeer::RW, $this->rw);
        if ($this->isColumnModified(PtkPeer::NAMA_DUSUN)) $criteria->add(PtkPeer::NAMA_DUSUN, $this->nama_dusun);
        if ($this->isColumnModified(PtkPeer::DESA_KELURAHAN)) $criteria->add(PtkPeer::DESA_KELURAHAN, $this->desa_kelurahan);
        if ($this->isColumnModified(PtkPeer::KODE_WILAYAH)) $criteria->add(PtkPeer::KODE_WILAYAH, $this->kode_wilayah);
        if ($this->isColumnModified(PtkPeer::KODE_POS)) $criteria->add(PtkPeer::KODE_POS, $this->kode_pos);
        if ($this->isColumnModified(PtkPeer::NO_TELEPON_RUMAH)) $criteria->add(PtkPeer::NO_TELEPON_RUMAH, $this->no_telepon_rumah);
        if ($this->isColumnModified(PtkPeer::NO_HP)) $criteria->add(PtkPeer::NO_HP, $this->no_hp);
        if ($this->isColumnModified(PtkPeer::EMAIL)) $criteria->add(PtkPeer::EMAIL, $this->email);
        if ($this->isColumnModified(PtkPeer::ENTRY_SEKOLAH_ID)) $criteria->add(PtkPeer::ENTRY_SEKOLAH_ID, $this->entry_sekolah_id);
        if ($this->isColumnModified(PtkPeer::STATUS_KEAKTIFAN_ID)) $criteria->add(PtkPeer::STATUS_KEAKTIFAN_ID, $this->status_keaktifan_id);
        if ($this->isColumnModified(PtkPeer::SK_CPNS)) $criteria->add(PtkPeer::SK_CPNS, $this->sk_cpns);
        if ($this->isColumnModified(PtkPeer::TGL_CPNS)) $criteria->add(PtkPeer::TGL_CPNS, $this->tgl_cpns);
        if ($this->isColumnModified(PtkPeer::SK_PENGANGKATAN)) $criteria->add(PtkPeer::SK_PENGANGKATAN, $this->sk_pengangkatan);
        if ($this->isColumnModified(PtkPeer::TMT_PENGANGKATAN)) $criteria->add(PtkPeer::TMT_PENGANGKATAN, $this->tmt_pengangkatan);
        if ($this->isColumnModified(PtkPeer::LEMBAGA_PENGANGKAT_ID)) $criteria->add(PtkPeer::LEMBAGA_PENGANGKAT_ID, $this->lembaga_pengangkat_id);
        if ($this->isColumnModified(PtkPeer::PANGKAT_GOLONGAN_ID)) $criteria->add(PtkPeer::PANGKAT_GOLONGAN_ID, $this->pangkat_golongan_id);
        if ($this->isColumnModified(PtkPeer::KEAHLIAN_LABORATORIUM_ID)) $criteria->add(PtkPeer::KEAHLIAN_LABORATORIUM_ID, $this->keahlian_laboratorium_id);
        if ($this->isColumnModified(PtkPeer::SUMBER_GAJI_ID)) $criteria->add(PtkPeer::SUMBER_GAJI_ID, $this->sumber_gaji_id);
        if ($this->isColumnModified(PtkPeer::NAMA_IBU_KANDUNG)) $criteria->add(PtkPeer::NAMA_IBU_KANDUNG, $this->nama_ibu_kandung);
        if ($this->isColumnModified(PtkPeer::STATUS_PERKAWINAN)) $criteria->add(PtkPeer::STATUS_PERKAWINAN, $this->status_perkawinan);
        if ($this->isColumnModified(PtkPeer::NAMA_SUAMI_ISTRI)) $criteria->add(PtkPeer::NAMA_SUAMI_ISTRI, $this->nama_suami_istri);
        if ($this->isColumnModified(PtkPeer::NIP_SUAMI_ISTRI)) $criteria->add(PtkPeer::NIP_SUAMI_ISTRI, $this->nip_suami_istri);
        if ($this->isColumnModified(PtkPeer::PEKERJAAN_SUAMI_ISTRI)) $criteria->add(PtkPeer::PEKERJAAN_SUAMI_ISTRI, $this->pekerjaan_suami_istri);
        if ($this->isColumnModified(PtkPeer::TMT_PNS)) $criteria->add(PtkPeer::TMT_PNS, $this->tmt_pns);
        if ($this->isColumnModified(PtkPeer::SUDAH_LISENSI_KEPALA_SEKOLAH)) $criteria->add(PtkPeer::SUDAH_LISENSI_KEPALA_SEKOLAH, $this->sudah_lisensi_kepala_sekolah);
        if ($this->isColumnModified(PtkPeer::JUMLAH_SEKOLAH_BINAAN)) $criteria->add(PtkPeer::JUMLAH_SEKOLAH_BINAAN, $this->jumlah_sekolah_binaan);
        if ($this->isColumnModified(PtkPeer::PERNAH_DIKLAT_KEPENGAWASAN)) $criteria->add(PtkPeer::PERNAH_DIKLAT_KEPENGAWASAN, $this->pernah_diklat_kepengawasan);
        if ($this->isColumnModified(PtkPeer::STATUS_DATA)) $criteria->add(PtkPeer::STATUS_DATA, $this->status_data);
        if ($this->isColumnModified(PtkPeer::MAMPU_HANDLE_KK)) $criteria->add(PtkPeer::MAMPU_HANDLE_KK, $this->mampu_handle_kk);
        if ($this->isColumnModified(PtkPeer::KEAHLIAN_BRAILLE)) $criteria->add(PtkPeer::KEAHLIAN_BRAILLE, $this->keahlian_braille);
        if ($this->isColumnModified(PtkPeer::KEAHLIAN_BHS_ISYARAT)) $criteria->add(PtkPeer::KEAHLIAN_BHS_ISYARAT, $this->keahlian_bhs_isyarat);
        if ($this->isColumnModified(PtkPeer::NPWP)) $criteria->add(PtkPeer::NPWP, $this->npwp);
        if ($this->isColumnModified(PtkPeer::LAST_UPDATE)) $criteria->add(PtkPeer::LAST_UPDATE, $this->last_update);
        if ($this->isColumnModified(PtkPeer::SOFT_DELETE)) $criteria->add(PtkPeer::SOFT_DELETE, $this->soft_delete);
        if ($this->isColumnModified(PtkPeer::LAST_SYNC)) $criteria->add(PtkPeer::LAST_SYNC, $this->last_sync);
        if ($this->isColumnModified(PtkPeer::UPDATER_ID)) $criteria->add(PtkPeer::UPDATER_ID, $this->updater_id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(PtkPeer::DATABASE_NAME);
        $criteria->add(PtkPeer::PTK_ID, $this->ptk_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getPtkId();
    }

    /**
     * Generic method to set the primary key (ptk_id column).
     *
     * @param  string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setPtkId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getPtkId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Ptk (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setNama($this->getNama());
        $copyObj->setNip($this->getNip());
        $copyObj->setJenisKelamin($this->getJenisKelamin());
        $copyObj->setTempatLahir($this->getTempatLahir());
        $copyObj->setTanggalLahir($this->getTanggalLahir());
        $copyObj->setNik($this->getNik());
        $copyObj->setNiyNigk($this->getNiyNigk());
        $copyObj->setNuptk($this->getNuptk());
        $copyObj->setStatusKepegawaianId($this->getStatusKepegawaianId());
        $copyObj->setJenisPtkId($this->getJenisPtkId());
        $copyObj->setPengawasBidangStudiId($this->getPengawasBidangStudiId());
        $copyObj->setAgamaId($this->getAgamaId());
        $copyObj->setKewarganegaraan($this->getKewarganegaraan());
        $copyObj->setAlamatJalan($this->getAlamatJalan());
        $copyObj->setRt($this->getRt());
        $copyObj->setRw($this->getRw());
        $copyObj->setNamaDusun($this->getNamaDusun());
        $copyObj->setDesaKelurahan($this->getDesaKelurahan());
        $copyObj->setKodeWilayah($this->getKodeWilayah());
        $copyObj->setKodePos($this->getKodePos());
        $copyObj->setNoTeleponRumah($this->getNoTeleponRumah());
        $copyObj->setNoHp($this->getNoHp());
        $copyObj->setEmail($this->getEmail());
        $copyObj->setEntrySekolahId($this->getEntrySekolahId());
        $copyObj->setStatusKeaktifanId($this->getStatusKeaktifanId());
        $copyObj->setSkCpns($this->getSkCpns());
        $copyObj->setTglCpns($this->getTglCpns());
        $copyObj->setSkPengangkatan($this->getSkPengangkatan());
        $copyObj->setTmtPengangkatan($this->getTmtPengangkatan());
        $copyObj->setLembagaPengangkatId($this->getLembagaPengangkatId());
        $copyObj->setPangkatGolonganId($this->getPangkatGolonganId());
        $copyObj->setKeahlianLaboratoriumId($this->getKeahlianLaboratoriumId());
        $copyObj->setSumberGajiId($this->getSumberGajiId());
        $copyObj->setNamaIbuKandung($this->getNamaIbuKandung());
        $copyObj->setStatusPerkawinan($this->getStatusPerkawinan());
        $copyObj->setNamaSuamiIstri($this->getNamaSuamiIstri());
        $copyObj->setNipSuamiIstri($this->getNipSuamiIstri());
        $copyObj->setPekerjaanSuamiIstri($this->getPekerjaanSuamiIstri());
        $copyObj->setTmtPns($this->getTmtPns());
        $copyObj->setSudahLisensiKepalaSekolah($this->getSudahLisensiKepalaSekolah());
        $copyObj->setJumlahSekolahBinaan($this->getJumlahSekolahBinaan());
        $copyObj->setPernahDiklatKepengawasan($this->getPernahDiklatKepengawasan());
        $copyObj->setStatusData($this->getStatusData());
        $copyObj->setMampuHandleKk($this->getMampuHandleKk());
        $copyObj->setKeahlianBraille($this->getKeahlianBraille());
        $copyObj->setKeahlianBhsIsyarat($this->getKeahlianBhsIsyarat());
        $copyObj->setNpwp($this->getNpwp());
        $copyObj->setLastUpdate($this->getLastUpdate());
        $copyObj->setSoftDelete($this->getSoftDelete());
        $copyObj->setLastSync($this->getLastSync());
        $copyObj->setUpdaterId($this->getUpdaterId());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getKesejahteraansRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addKesejahteraanRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getKesejahteraansRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addKesejahteraanRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldPtksRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldPtkRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldPtksRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldPtkRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPenghargaansRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPenghargaanRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPenghargaansRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPenghargaanRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getInpassingsRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addInpassingRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getInpassingsRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addInpassingRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPtkTerdaftarsRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPtkTerdaftarRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPtkTerdaftarsRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPtkTerdaftarRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getKaryaTulissRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addKaryaTulisRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getKaryaTulissRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addKaryaTulisRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getNilaiTestsRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addNilaiTestRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getNilaiTestsRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addNilaiTestRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getBukuPtksRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBukuPtkRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getBukuPtksRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBukuPtkRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getBeasiswaPtksRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBeasiswaPtkRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getBeasiswaPtksRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBeasiswaPtkRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRwyKepangkatansRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRwyKepangkatanRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRwyKepangkatansRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRwyKepangkatanRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRombonganBelajarsRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRombonganBelajarRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRombonganBelajarsRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRombonganBelajarRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRiwayatGajiBerkalasRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRiwayatGajiBerkalaRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRiwayatGajiBerkalasRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRiwayatGajiBerkalaRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPengawasTerdaftarsRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPengawasTerdaftarRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPengawasTerdaftarsRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPengawasTerdaftarRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRwyStrukturalsRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRwyStrukturalRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRwyStrukturalsRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRwyStrukturalRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRwyPendFormalsRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRwyPendFormalRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRwyPendFormalsRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRwyPendFormalRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getAnaksRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addAnakRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getAnaksRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addAnakRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getDiklatsRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addDiklatRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getDiklatsRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addDiklatRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPtkBarusRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPtkBaruRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPtkBarusRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPtkBaruRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getTunjangansRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addTunjanganRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getTunjangansRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addTunjanganRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRwySertifikasisRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRwySertifikasiRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRwySertifikasisRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRwySertifikasiRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getTugasTambahansRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addTugasTambahanRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getTugasTambahansRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addTugasTambahanRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRwyFungsionalsRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRwyFungsionalRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRwyFungsionalsRelatedByPtkId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRwyFungsionalRelatedByPtkId($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setPtkId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Ptk Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return PtkPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new PtkPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a Sekolah object.
     *
     * @param             Sekolah $v
     * @return Ptk The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSekolahRelatedByEntrySekolahId(Sekolah $v = null)
    {
        if ($v === null) {
            $this->setEntrySekolahId(NULL);
        } else {
            $this->setEntrySekolahId($v->getSekolahId());
        }

        $this->aSekolahRelatedByEntrySekolahId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Sekolah object, it will not be re-added.
        if ($v !== null) {
            $v->addPtkRelatedByEntrySekolahId($this);
        }


        return $this;
    }


    /**
     * Get the associated Sekolah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Sekolah The associated Sekolah object.
     * @throws PropelException
     */
    public function getSekolahRelatedByEntrySekolahId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSekolahRelatedByEntrySekolahId === null && (($this->entry_sekolah_id !== "" && $this->entry_sekolah_id !== null)) && $doQuery) {
            $this->aSekolahRelatedByEntrySekolahId = SekolahQuery::create()->findPk($this->entry_sekolah_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSekolahRelatedByEntrySekolahId->addPtksRelatedByEntrySekolahId($this);
             */
        }

        return $this->aSekolahRelatedByEntrySekolahId;
    }

    /**
     * Declares an association between this object and a Sekolah object.
     *
     * @param             Sekolah $v
     * @return Ptk The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSekolahRelatedByEntrySekolahId(Sekolah $v = null)
    {
        if ($v === null) {
            $this->setEntrySekolahId(NULL);
        } else {
            $this->setEntrySekolahId($v->getSekolahId());
        }

        $this->aSekolahRelatedByEntrySekolahId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Sekolah object, it will not be re-added.
        if ($v !== null) {
            $v->addPtkRelatedByEntrySekolahId($this);
        }


        return $this;
    }


    /**
     * Get the associated Sekolah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Sekolah The associated Sekolah object.
     * @throws PropelException
     */
    public function getSekolahRelatedByEntrySekolahId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSekolahRelatedByEntrySekolahId === null && (($this->entry_sekolah_id !== "" && $this->entry_sekolah_id !== null)) && $doQuery) {
            $this->aSekolahRelatedByEntrySekolahId = SekolahQuery::create()->findPk($this->entry_sekolah_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSekolahRelatedByEntrySekolahId->addPtksRelatedByEntrySekolahId($this);
             */
        }

        return $this->aSekolahRelatedByEntrySekolahId;
    }

    /**
     * Declares an association between this object and a Agama object.
     *
     * @param             Agama $v
     * @return Ptk The current object (for fluent API support)
     * @throws PropelException
     */
    public function setAgamaRelatedByAgamaId(Agama $v = null)
    {
        if ($v === null) {
            $this->setAgamaId(NULL);
        } else {
            $this->setAgamaId($v->getAgamaId());
        }

        $this->aAgamaRelatedByAgamaId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Agama object, it will not be re-added.
        if ($v !== null) {
            $v->addPtkRelatedByAgamaId($this);
        }


        return $this;
    }


    /**
     * Get the associated Agama object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Agama The associated Agama object.
     * @throws PropelException
     */
    public function getAgamaRelatedByAgamaId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aAgamaRelatedByAgamaId === null && ($this->agama_id !== null) && $doQuery) {
            $this->aAgamaRelatedByAgamaId = AgamaQuery::create()->findPk($this->agama_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aAgamaRelatedByAgamaId->addPtksRelatedByAgamaId($this);
             */
        }

        return $this->aAgamaRelatedByAgamaId;
    }

    /**
     * Declares an association between this object and a Agama object.
     *
     * @param             Agama $v
     * @return Ptk The current object (for fluent API support)
     * @throws PropelException
     */
    public function setAgamaRelatedByAgamaId(Agama $v = null)
    {
        if ($v === null) {
            $this->setAgamaId(NULL);
        } else {
            $this->setAgamaId($v->getAgamaId());
        }

        $this->aAgamaRelatedByAgamaId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Agama object, it will not be re-added.
        if ($v !== null) {
            $v->addPtkRelatedByAgamaId($this);
        }


        return $this;
    }


    /**
     * Get the associated Agama object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Agama The associated Agama object.
     * @throws PropelException
     */
    public function getAgamaRelatedByAgamaId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aAgamaRelatedByAgamaId === null && ($this->agama_id !== null) && $doQuery) {
            $this->aAgamaRelatedByAgamaId = AgamaQuery::create()->findPk($this->agama_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aAgamaRelatedByAgamaId->addPtksRelatedByAgamaId($this);
             */
        }

        return $this->aAgamaRelatedByAgamaId;
    }

    /**
     * Declares an association between this object and a BidangStudi object.
     *
     * @param             BidangStudi $v
     * @return Ptk The current object (for fluent API support)
     * @throws PropelException
     */
    public function setBidangStudiRelatedByPengawasBidangStudiId(BidangStudi $v = null)
    {
        if ($v === null) {
            $this->setPengawasBidangStudiId(NULL);
        } else {
            $this->setPengawasBidangStudiId($v->getBidangStudiId());
        }

        $this->aBidangStudiRelatedByPengawasBidangStudiId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the BidangStudi object, it will not be re-added.
        if ($v !== null) {
            $v->addPtkRelatedByPengawasBidangStudiId($this);
        }


        return $this;
    }


    /**
     * Get the associated BidangStudi object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return BidangStudi The associated BidangStudi object.
     * @throws PropelException
     */
    public function getBidangStudiRelatedByPengawasBidangStudiId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aBidangStudiRelatedByPengawasBidangStudiId === null && ($this->pengawas_bidang_studi_id !== null) && $doQuery) {
            $this->aBidangStudiRelatedByPengawasBidangStudiId = BidangStudiQuery::create()->findPk($this->pengawas_bidang_studi_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aBidangStudiRelatedByPengawasBidangStudiId->addPtksRelatedByPengawasBidangStudiId($this);
             */
        }

        return $this->aBidangStudiRelatedByPengawasBidangStudiId;
    }

    /**
     * Declares an association between this object and a BidangStudi object.
     *
     * @param             BidangStudi $v
     * @return Ptk The current object (for fluent API support)
     * @throws PropelException
     */
    public function setBidangStudiRelatedByPengawasBidangStudiId(BidangStudi $v = null)
    {
        if ($v === null) {
            $this->setPengawasBidangStudiId(NULL);
        } else {
            $this->setPengawasBidangStudiId($v->getBidangStudiId());
        }

        $this->aBidangStudiRelatedByPengawasBidangStudiId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the BidangStudi object, it will not be re-added.
        if ($v !== null) {
            $v->addPtkRelatedByPengawasBidangStudiId($this);
        }


        return $this;
    }


    /**
     * Get the associated BidangStudi object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return BidangStudi The associated BidangStudi object.
     * @throws PropelException
     */
    public function getBidangStudiRelatedByPengawasBidangStudiId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aBidangStudiRelatedByPengawasBidangStudiId === null && ($this->pengawas_bidang_studi_id !== null) && $doQuery) {
            $this->aBidangStudiRelatedByPengawasBidangStudiId = BidangStudiQuery::create()->findPk($this->pengawas_bidang_studi_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aBidangStudiRelatedByPengawasBidangStudiId->addPtksRelatedByPengawasBidangStudiId($this);
             */
        }

        return $this->aBidangStudiRelatedByPengawasBidangStudiId;
    }

    /**
     * Declares an association between this object and a JenisPtk object.
     *
     * @param             JenisPtk $v
     * @return Ptk The current object (for fluent API support)
     * @throws PropelException
     */
    public function setJenisPtkRelatedByJenisPtkId(JenisPtk $v = null)
    {
        if ($v === null) {
            $this->setJenisPtkId(NULL);
        } else {
            $this->setJenisPtkId($v->getJenisPtkId());
        }

        $this->aJenisPtkRelatedByJenisPtkId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the JenisPtk object, it will not be re-added.
        if ($v !== null) {
            $v->addPtkRelatedByJenisPtkId($this);
        }


        return $this;
    }


    /**
     * Get the associated JenisPtk object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return JenisPtk The associated JenisPtk object.
     * @throws PropelException
     */
    public function getJenisPtkRelatedByJenisPtkId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aJenisPtkRelatedByJenisPtkId === null && (($this->jenis_ptk_id !== "" && $this->jenis_ptk_id !== null)) && $doQuery) {
            $this->aJenisPtkRelatedByJenisPtkId = JenisPtkQuery::create()->findPk($this->jenis_ptk_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aJenisPtkRelatedByJenisPtkId->addPtksRelatedByJenisPtkId($this);
             */
        }

        return $this->aJenisPtkRelatedByJenisPtkId;
    }

    /**
     * Declares an association between this object and a JenisPtk object.
     *
     * @param             JenisPtk $v
     * @return Ptk The current object (for fluent API support)
     * @throws PropelException
     */
    public function setJenisPtkRelatedByJenisPtkId(JenisPtk $v = null)
    {
        if ($v === null) {
            $this->setJenisPtkId(NULL);
        } else {
            $this->setJenisPtkId($v->getJenisPtkId());
        }

        $this->aJenisPtkRelatedByJenisPtkId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the JenisPtk object, it will not be re-added.
        if ($v !== null) {
            $v->addPtkRelatedByJenisPtkId($this);
        }


        return $this;
    }


    /**
     * Get the associated JenisPtk object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return JenisPtk The associated JenisPtk object.
     * @throws PropelException
     */
    public function getJenisPtkRelatedByJenisPtkId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aJenisPtkRelatedByJenisPtkId === null && (($this->jenis_ptk_id !== "" && $this->jenis_ptk_id !== null)) && $doQuery) {
            $this->aJenisPtkRelatedByJenisPtkId = JenisPtkQuery::create()->findPk($this->jenis_ptk_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aJenisPtkRelatedByJenisPtkId->addPtksRelatedByJenisPtkId($this);
             */
        }

        return $this->aJenisPtkRelatedByJenisPtkId;
    }

    /**
     * Declares an association between this object and a KeahlianLaboratorium object.
     *
     * @param             KeahlianLaboratorium $v
     * @return Ptk The current object (for fluent API support)
     * @throws PropelException
     */
    public function setKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId(KeahlianLaboratorium $v = null)
    {
        if ($v === null) {
            $this->setKeahlianLaboratoriumId(NULL);
        } else {
            $this->setKeahlianLaboratoriumId($v->getKeahlianLaboratoriumId());
        }

        $this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the KeahlianLaboratorium object, it will not be re-added.
        if ($v !== null) {
            $v->addPtkRelatedByKeahlianLaboratoriumId($this);
        }


        return $this;
    }


    /**
     * Get the associated KeahlianLaboratorium object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return KeahlianLaboratorium The associated KeahlianLaboratorium object.
     * @throws PropelException
     */
    public function getKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId === null && ($this->keahlian_laboratorium_id !== null) && $doQuery) {
            $this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId = KeahlianLaboratoriumQuery::create()->findPk($this->keahlian_laboratorium_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId->addPtksRelatedByKeahlianLaboratoriumId($this);
             */
        }

        return $this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId;
    }

    /**
     * Declares an association between this object and a KeahlianLaboratorium object.
     *
     * @param             KeahlianLaboratorium $v
     * @return Ptk The current object (for fluent API support)
     * @throws PropelException
     */
    public function setKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId(KeahlianLaboratorium $v = null)
    {
        if ($v === null) {
            $this->setKeahlianLaboratoriumId(NULL);
        } else {
            $this->setKeahlianLaboratoriumId($v->getKeahlianLaboratoriumId());
        }

        $this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the KeahlianLaboratorium object, it will not be re-added.
        if ($v !== null) {
            $v->addPtkRelatedByKeahlianLaboratoriumId($this);
        }


        return $this;
    }


    /**
     * Get the associated KeahlianLaboratorium object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return KeahlianLaboratorium The associated KeahlianLaboratorium object.
     * @throws PropelException
     */
    public function getKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId === null && ($this->keahlian_laboratorium_id !== null) && $doQuery) {
            $this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId = KeahlianLaboratoriumQuery::create()->findPk($this->keahlian_laboratorium_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId->addPtksRelatedByKeahlianLaboratoriumId($this);
             */
        }

        return $this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId;
    }

    /**
     * Declares an association between this object and a KebutuhanKhusus object.
     *
     * @param             KebutuhanKhusus $v
     * @return Ptk The current object (for fluent API support)
     * @throws PropelException
     */
    public function setKebutuhanKhususRelatedByMampuHandleKk(KebutuhanKhusus $v = null)
    {
        if ($v === null) {
            $this->setMampuHandleKk(NULL);
        } else {
            $this->setMampuHandleKk($v->getKebutuhanKhususId());
        }

        $this->aKebutuhanKhususRelatedByMampuHandleKk = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the KebutuhanKhusus object, it will not be re-added.
        if ($v !== null) {
            $v->addPtkRelatedByMampuHandleKk($this);
        }


        return $this;
    }


    /**
     * Get the associated KebutuhanKhusus object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return KebutuhanKhusus The associated KebutuhanKhusus object.
     * @throws PropelException
     */
    public function getKebutuhanKhususRelatedByMampuHandleKk(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aKebutuhanKhususRelatedByMampuHandleKk === null && ($this->mampu_handle_kk !== null) && $doQuery) {
            $this->aKebutuhanKhususRelatedByMampuHandleKk = KebutuhanKhususQuery::create()->findPk($this->mampu_handle_kk, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aKebutuhanKhususRelatedByMampuHandleKk->addPtksRelatedByMampuHandleKk($this);
             */
        }

        return $this->aKebutuhanKhususRelatedByMampuHandleKk;
    }

    /**
     * Declares an association between this object and a KebutuhanKhusus object.
     *
     * @param             KebutuhanKhusus $v
     * @return Ptk The current object (for fluent API support)
     * @throws PropelException
     */
    public function setKebutuhanKhususRelatedByMampuHandleKk(KebutuhanKhusus $v = null)
    {
        if ($v === null) {
            $this->setMampuHandleKk(NULL);
        } else {
            $this->setMampuHandleKk($v->getKebutuhanKhususId());
        }

        $this->aKebutuhanKhususRelatedByMampuHandleKk = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the KebutuhanKhusus object, it will not be re-added.
        if ($v !== null) {
            $v->addPtkRelatedByMampuHandleKk($this);
        }


        return $this;
    }


    /**
     * Get the associated KebutuhanKhusus object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return KebutuhanKhusus The associated KebutuhanKhusus object.
     * @throws PropelException
     */
    public function getKebutuhanKhususRelatedByMampuHandleKk(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aKebutuhanKhususRelatedByMampuHandleKk === null && ($this->mampu_handle_kk !== null) && $doQuery) {
            $this->aKebutuhanKhususRelatedByMampuHandleKk = KebutuhanKhususQuery::create()->findPk($this->mampu_handle_kk, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aKebutuhanKhususRelatedByMampuHandleKk->addPtksRelatedByMampuHandleKk($this);
             */
        }

        return $this->aKebutuhanKhususRelatedByMampuHandleKk;
    }

    /**
     * Declares an association between this object and a LembagaPengangkat object.
     *
     * @param             LembagaPengangkat $v
     * @return Ptk The current object (for fluent API support)
     * @throws PropelException
     */
    public function setLembagaPengangkatRelatedByLembagaPengangkatId(LembagaPengangkat $v = null)
    {
        if ($v === null) {
            $this->setLembagaPengangkatId(NULL);
        } else {
            $this->setLembagaPengangkatId($v->getLembagaPengangkatId());
        }

        $this->aLembagaPengangkatRelatedByLembagaPengangkatId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the LembagaPengangkat object, it will not be re-added.
        if ($v !== null) {
            $v->addPtkRelatedByLembagaPengangkatId($this);
        }


        return $this;
    }


    /**
     * Get the associated LembagaPengangkat object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return LembagaPengangkat The associated LembagaPengangkat object.
     * @throws PropelException
     */
    public function getLembagaPengangkatRelatedByLembagaPengangkatId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aLembagaPengangkatRelatedByLembagaPengangkatId === null && (($this->lembaga_pengangkat_id !== "" && $this->lembaga_pengangkat_id !== null)) && $doQuery) {
            $this->aLembagaPengangkatRelatedByLembagaPengangkatId = LembagaPengangkatQuery::create()->findPk($this->lembaga_pengangkat_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aLembagaPengangkatRelatedByLembagaPengangkatId->addPtksRelatedByLembagaPengangkatId($this);
             */
        }

        return $this->aLembagaPengangkatRelatedByLembagaPengangkatId;
    }

    /**
     * Declares an association between this object and a LembagaPengangkat object.
     *
     * @param             LembagaPengangkat $v
     * @return Ptk The current object (for fluent API support)
     * @throws PropelException
     */
    public function setLembagaPengangkatRelatedByLembagaPengangkatId(LembagaPengangkat $v = null)
    {
        if ($v === null) {
            $this->setLembagaPengangkatId(NULL);
        } else {
            $this->setLembagaPengangkatId($v->getLembagaPengangkatId());
        }

        $this->aLembagaPengangkatRelatedByLembagaPengangkatId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the LembagaPengangkat object, it will not be re-added.
        if ($v !== null) {
            $v->addPtkRelatedByLembagaPengangkatId($this);
        }


        return $this;
    }


    /**
     * Get the associated LembagaPengangkat object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return LembagaPengangkat The associated LembagaPengangkat object.
     * @throws PropelException
     */
    public function getLembagaPengangkatRelatedByLembagaPengangkatId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aLembagaPengangkatRelatedByLembagaPengangkatId === null && (($this->lembaga_pengangkat_id !== "" && $this->lembaga_pengangkat_id !== null)) && $doQuery) {
            $this->aLembagaPengangkatRelatedByLembagaPengangkatId = LembagaPengangkatQuery::create()->findPk($this->lembaga_pengangkat_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aLembagaPengangkatRelatedByLembagaPengangkatId->addPtksRelatedByLembagaPengangkatId($this);
             */
        }

        return $this->aLembagaPengangkatRelatedByLembagaPengangkatId;
    }

    /**
     * Declares an association between this object and a MstWilayah object.
     *
     * @param             MstWilayah $v
     * @return Ptk The current object (for fluent API support)
     * @throws PropelException
     */
    public function setMstWilayahRelatedByKodeWilayah(MstWilayah $v = null)
    {
        if ($v === null) {
            $this->setKodeWilayah(NULL);
        } else {
            $this->setKodeWilayah($v->getKodeWilayah());
        }

        $this->aMstWilayahRelatedByKodeWilayah = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the MstWilayah object, it will not be re-added.
        if ($v !== null) {
            $v->addPtkRelatedByKodeWilayah($this);
        }


        return $this;
    }


    /**
     * Get the associated MstWilayah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return MstWilayah The associated MstWilayah object.
     * @throws PropelException
     */
    public function getMstWilayahRelatedByKodeWilayah(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aMstWilayahRelatedByKodeWilayah === null && (($this->kode_wilayah !== "" && $this->kode_wilayah !== null)) && $doQuery) {
            $this->aMstWilayahRelatedByKodeWilayah = MstWilayahQuery::create()->findPk($this->kode_wilayah, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aMstWilayahRelatedByKodeWilayah->addPtksRelatedByKodeWilayah($this);
             */
        }

        return $this->aMstWilayahRelatedByKodeWilayah;
    }

    /**
     * Declares an association between this object and a MstWilayah object.
     *
     * @param             MstWilayah $v
     * @return Ptk The current object (for fluent API support)
     * @throws PropelException
     */
    public function setMstWilayahRelatedByKodeWilayah(MstWilayah $v = null)
    {
        if ($v === null) {
            $this->setKodeWilayah(NULL);
        } else {
            $this->setKodeWilayah($v->getKodeWilayah());
        }

        $this->aMstWilayahRelatedByKodeWilayah = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the MstWilayah object, it will not be re-added.
        if ($v !== null) {
            $v->addPtkRelatedByKodeWilayah($this);
        }


        return $this;
    }


    /**
     * Get the associated MstWilayah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return MstWilayah The associated MstWilayah object.
     * @throws PropelException
     */
    public function getMstWilayahRelatedByKodeWilayah(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aMstWilayahRelatedByKodeWilayah === null && (($this->kode_wilayah !== "" && $this->kode_wilayah !== null)) && $doQuery) {
            $this->aMstWilayahRelatedByKodeWilayah = MstWilayahQuery::create()->findPk($this->kode_wilayah, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aMstWilayahRelatedByKodeWilayah->addPtksRelatedByKodeWilayah($this);
             */
        }

        return $this->aMstWilayahRelatedByKodeWilayah;
    }

    /**
     * Declares an association between this object and a Negara object.
     *
     * @param             Negara $v
     * @return Ptk The current object (for fluent API support)
     * @throws PropelException
     */
    public function setNegaraRelatedByKewarganegaraan(Negara $v = null)
    {
        if ($v === null) {
            $this->setKewarganegaraan(NULL);
        } else {
            $this->setKewarganegaraan($v->getNegaraId());
        }

        $this->aNegaraRelatedByKewarganegaraan = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Negara object, it will not be re-added.
        if ($v !== null) {
            $v->addPtkRelatedByKewarganegaraan($this);
        }


        return $this;
    }


    /**
     * Get the associated Negara object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Negara The associated Negara object.
     * @throws PropelException
     */
    public function getNegaraRelatedByKewarganegaraan(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aNegaraRelatedByKewarganegaraan === null && (($this->kewarganegaraan !== "" && $this->kewarganegaraan !== null)) && $doQuery) {
            $this->aNegaraRelatedByKewarganegaraan = NegaraQuery::create()->findPk($this->kewarganegaraan, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aNegaraRelatedByKewarganegaraan->addPtksRelatedByKewarganegaraan($this);
             */
        }

        return $this->aNegaraRelatedByKewarganegaraan;
    }

    /**
     * Declares an association between this object and a Negara object.
     *
     * @param             Negara $v
     * @return Ptk The current object (for fluent API support)
     * @throws PropelException
     */
    public function setNegaraRelatedByKewarganegaraan(Negara $v = null)
    {
        if ($v === null) {
            $this->setKewarganegaraan(NULL);
        } else {
            $this->setKewarganegaraan($v->getNegaraId());
        }

        $this->aNegaraRelatedByKewarganegaraan = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Negara object, it will not be re-added.
        if ($v !== null) {
            $v->addPtkRelatedByKewarganegaraan($this);
        }


        return $this;
    }


    /**
     * Get the associated Negara object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Negara The associated Negara object.
     * @throws PropelException
     */
    public function getNegaraRelatedByKewarganegaraan(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aNegaraRelatedByKewarganegaraan === null && (($this->kewarganegaraan !== "" && $this->kewarganegaraan !== null)) && $doQuery) {
            $this->aNegaraRelatedByKewarganegaraan = NegaraQuery::create()->findPk($this->kewarganegaraan, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aNegaraRelatedByKewarganegaraan->addPtksRelatedByKewarganegaraan($this);
             */
        }

        return $this->aNegaraRelatedByKewarganegaraan;
    }

    /**
     * Declares an association between this object and a PangkatGolongan object.
     *
     * @param             PangkatGolongan $v
     * @return Ptk The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPangkatGolonganRelatedByPangkatGolonganId(PangkatGolongan $v = null)
    {
        if ($v === null) {
            $this->setPangkatGolonganId(NULL);
        } else {
            $this->setPangkatGolonganId($v->getPangkatGolonganId());
        }

        $this->aPangkatGolonganRelatedByPangkatGolonganId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the PangkatGolongan object, it will not be re-added.
        if ($v !== null) {
            $v->addPtkRelatedByPangkatGolonganId($this);
        }


        return $this;
    }


    /**
     * Get the associated PangkatGolongan object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return PangkatGolongan The associated PangkatGolongan object.
     * @throws PropelException
     */
    public function getPangkatGolonganRelatedByPangkatGolonganId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPangkatGolonganRelatedByPangkatGolonganId === null && (($this->pangkat_golongan_id !== "" && $this->pangkat_golongan_id !== null)) && $doQuery) {
            $this->aPangkatGolonganRelatedByPangkatGolonganId = PangkatGolonganQuery::create()->findPk($this->pangkat_golongan_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPangkatGolonganRelatedByPangkatGolonganId->addPtksRelatedByPangkatGolonganId($this);
             */
        }

        return $this->aPangkatGolonganRelatedByPangkatGolonganId;
    }

    /**
     * Declares an association between this object and a PangkatGolongan object.
     *
     * @param             PangkatGolongan $v
     * @return Ptk The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPangkatGolonganRelatedByPangkatGolonganId(PangkatGolongan $v = null)
    {
        if ($v === null) {
            $this->setPangkatGolonganId(NULL);
        } else {
            $this->setPangkatGolonganId($v->getPangkatGolonganId());
        }

        $this->aPangkatGolonganRelatedByPangkatGolonganId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the PangkatGolongan object, it will not be re-added.
        if ($v !== null) {
            $v->addPtkRelatedByPangkatGolonganId($this);
        }


        return $this;
    }


    /**
     * Get the associated PangkatGolongan object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return PangkatGolongan The associated PangkatGolongan object.
     * @throws PropelException
     */
    public function getPangkatGolonganRelatedByPangkatGolonganId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPangkatGolonganRelatedByPangkatGolonganId === null && (($this->pangkat_golongan_id !== "" && $this->pangkat_golongan_id !== null)) && $doQuery) {
            $this->aPangkatGolonganRelatedByPangkatGolonganId = PangkatGolonganQuery::create()->findPk($this->pangkat_golongan_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPangkatGolonganRelatedByPangkatGolonganId->addPtksRelatedByPangkatGolonganId($this);
             */
        }

        return $this->aPangkatGolonganRelatedByPangkatGolonganId;
    }

    /**
     * Declares an association between this object and a Pekerjaan object.
     *
     * @param             Pekerjaan $v
     * @return Ptk The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPekerjaanRelatedByPekerjaanSuamiIstri(Pekerjaan $v = null)
    {
        if ($v === null) {
            $this->setPekerjaanSuamiIstri(NULL);
        } else {
            $this->setPekerjaanSuamiIstri($v->getPekerjaanId());
        }

        $this->aPekerjaanRelatedByPekerjaanSuamiIstri = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Pekerjaan object, it will not be re-added.
        if ($v !== null) {
            $v->addPtkRelatedByPekerjaanSuamiIstri($this);
        }


        return $this;
    }


    /**
     * Get the associated Pekerjaan object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Pekerjaan The associated Pekerjaan object.
     * @throws PropelException
     */
    public function getPekerjaanRelatedByPekerjaanSuamiIstri(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPekerjaanRelatedByPekerjaanSuamiIstri === null && ($this->pekerjaan_suami_istri !== null) && $doQuery) {
            $this->aPekerjaanRelatedByPekerjaanSuamiIstri = PekerjaanQuery::create()->findPk($this->pekerjaan_suami_istri, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPekerjaanRelatedByPekerjaanSuamiIstri->addPtksRelatedByPekerjaanSuamiIstri($this);
             */
        }

        return $this->aPekerjaanRelatedByPekerjaanSuamiIstri;
    }

    /**
     * Declares an association between this object and a Pekerjaan object.
     *
     * @param             Pekerjaan $v
     * @return Ptk The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPekerjaanRelatedByPekerjaanSuamiIstri(Pekerjaan $v = null)
    {
        if ($v === null) {
            $this->setPekerjaanSuamiIstri(NULL);
        } else {
            $this->setPekerjaanSuamiIstri($v->getPekerjaanId());
        }

        $this->aPekerjaanRelatedByPekerjaanSuamiIstri = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Pekerjaan object, it will not be re-added.
        if ($v !== null) {
            $v->addPtkRelatedByPekerjaanSuamiIstri($this);
        }


        return $this;
    }


    /**
     * Get the associated Pekerjaan object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Pekerjaan The associated Pekerjaan object.
     * @throws PropelException
     */
    public function getPekerjaanRelatedByPekerjaanSuamiIstri(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPekerjaanRelatedByPekerjaanSuamiIstri === null && ($this->pekerjaan_suami_istri !== null) && $doQuery) {
            $this->aPekerjaanRelatedByPekerjaanSuamiIstri = PekerjaanQuery::create()->findPk($this->pekerjaan_suami_istri, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPekerjaanRelatedByPekerjaanSuamiIstri->addPtksRelatedByPekerjaanSuamiIstri($this);
             */
        }

        return $this->aPekerjaanRelatedByPekerjaanSuamiIstri;
    }

    /**
     * Declares an association between this object and a StatusKepegawaian object.
     *
     * @param             StatusKepegawaian $v
     * @return Ptk The current object (for fluent API support)
     * @throws PropelException
     */
    public function setStatusKepegawaianRelatedByStatusKepegawaianId(StatusKepegawaian $v = null)
    {
        if ($v === null) {
            $this->setStatusKepegawaianId(NULL);
        } else {
            $this->setStatusKepegawaianId($v->getStatusKepegawaianId());
        }

        $this->aStatusKepegawaianRelatedByStatusKepegawaianId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the StatusKepegawaian object, it will not be re-added.
        if ($v !== null) {
            $v->addPtkRelatedByStatusKepegawaianId($this);
        }


        return $this;
    }


    /**
     * Get the associated StatusKepegawaian object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return StatusKepegawaian The associated StatusKepegawaian object.
     * @throws PropelException
     */
    public function getStatusKepegawaianRelatedByStatusKepegawaianId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aStatusKepegawaianRelatedByStatusKepegawaianId === null && ($this->status_kepegawaian_id !== null) && $doQuery) {
            $this->aStatusKepegawaianRelatedByStatusKepegawaianId = StatusKepegawaianQuery::create()->findPk($this->status_kepegawaian_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aStatusKepegawaianRelatedByStatusKepegawaianId->addPtksRelatedByStatusKepegawaianId($this);
             */
        }

        return $this->aStatusKepegawaianRelatedByStatusKepegawaianId;
    }

    /**
     * Declares an association between this object and a StatusKepegawaian object.
     *
     * @param             StatusKepegawaian $v
     * @return Ptk The current object (for fluent API support)
     * @throws PropelException
     */
    public function setStatusKepegawaianRelatedByStatusKepegawaianId(StatusKepegawaian $v = null)
    {
        if ($v === null) {
            $this->setStatusKepegawaianId(NULL);
        } else {
            $this->setStatusKepegawaianId($v->getStatusKepegawaianId());
        }

        $this->aStatusKepegawaianRelatedByStatusKepegawaianId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the StatusKepegawaian object, it will not be re-added.
        if ($v !== null) {
            $v->addPtkRelatedByStatusKepegawaianId($this);
        }


        return $this;
    }


    /**
     * Get the associated StatusKepegawaian object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return StatusKepegawaian The associated StatusKepegawaian object.
     * @throws PropelException
     */
    public function getStatusKepegawaianRelatedByStatusKepegawaianId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aStatusKepegawaianRelatedByStatusKepegawaianId === null && ($this->status_kepegawaian_id !== null) && $doQuery) {
            $this->aStatusKepegawaianRelatedByStatusKepegawaianId = StatusKepegawaianQuery::create()->findPk($this->status_kepegawaian_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aStatusKepegawaianRelatedByStatusKepegawaianId->addPtksRelatedByStatusKepegawaianId($this);
             */
        }

        return $this->aStatusKepegawaianRelatedByStatusKepegawaianId;
    }

    /**
     * Declares an association between this object and a StatusKeaktifanPegawai object.
     *
     * @param             StatusKeaktifanPegawai $v
     * @return Ptk The current object (for fluent API support)
     * @throws PropelException
     */
    public function setStatusKeaktifanPegawaiRelatedByStatusKeaktifanId(StatusKeaktifanPegawai $v = null)
    {
        if ($v === null) {
            $this->setStatusKeaktifanId(NULL);
        } else {
            $this->setStatusKeaktifanId($v->getStatusKeaktifanId());
        }

        $this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the StatusKeaktifanPegawai object, it will not be re-added.
        if ($v !== null) {
            $v->addPtkRelatedByStatusKeaktifanId($this);
        }


        return $this;
    }


    /**
     * Get the associated StatusKeaktifanPegawai object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return StatusKeaktifanPegawai The associated StatusKeaktifanPegawai object.
     * @throws PropelException
     */
    public function getStatusKeaktifanPegawaiRelatedByStatusKeaktifanId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId === null && (($this->status_keaktifan_id !== "" && $this->status_keaktifan_id !== null)) && $doQuery) {
            $this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId = StatusKeaktifanPegawaiQuery::create()->findPk($this->status_keaktifan_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId->addPtksRelatedByStatusKeaktifanId($this);
             */
        }

        return $this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId;
    }

    /**
     * Declares an association between this object and a StatusKeaktifanPegawai object.
     *
     * @param             StatusKeaktifanPegawai $v
     * @return Ptk The current object (for fluent API support)
     * @throws PropelException
     */
    public function setStatusKeaktifanPegawaiRelatedByStatusKeaktifanId(StatusKeaktifanPegawai $v = null)
    {
        if ($v === null) {
            $this->setStatusKeaktifanId(NULL);
        } else {
            $this->setStatusKeaktifanId($v->getStatusKeaktifanId());
        }

        $this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the StatusKeaktifanPegawai object, it will not be re-added.
        if ($v !== null) {
            $v->addPtkRelatedByStatusKeaktifanId($this);
        }


        return $this;
    }


    /**
     * Get the associated StatusKeaktifanPegawai object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return StatusKeaktifanPegawai The associated StatusKeaktifanPegawai object.
     * @throws PropelException
     */
    public function getStatusKeaktifanPegawaiRelatedByStatusKeaktifanId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId === null && (($this->status_keaktifan_id !== "" && $this->status_keaktifan_id !== null)) && $doQuery) {
            $this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId = StatusKeaktifanPegawaiQuery::create()->findPk($this->status_keaktifan_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId->addPtksRelatedByStatusKeaktifanId($this);
             */
        }

        return $this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId;
    }

    /**
     * Declares an association between this object and a SumberGaji object.
     *
     * @param             SumberGaji $v
     * @return Ptk The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSumberGajiRelatedBySumberGajiId(SumberGaji $v = null)
    {
        if ($v === null) {
            $this->setSumberGajiId(NULL);
        } else {
            $this->setSumberGajiId($v->getSumberGajiId());
        }

        $this->aSumberGajiRelatedBySumberGajiId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the SumberGaji object, it will not be re-added.
        if ($v !== null) {
            $v->addPtkRelatedBySumberGajiId($this);
        }


        return $this;
    }


    /**
     * Get the associated SumberGaji object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return SumberGaji The associated SumberGaji object.
     * @throws PropelException
     */
    public function getSumberGajiRelatedBySumberGajiId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSumberGajiRelatedBySumberGajiId === null && (($this->sumber_gaji_id !== "" && $this->sumber_gaji_id !== null)) && $doQuery) {
            $this->aSumberGajiRelatedBySumberGajiId = SumberGajiQuery::create()->findPk($this->sumber_gaji_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSumberGajiRelatedBySumberGajiId->addPtksRelatedBySumberGajiId($this);
             */
        }

        return $this->aSumberGajiRelatedBySumberGajiId;
    }

    /**
     * Declares an association between this object and a SumberGaji object.
     *
     * @param             SumberGaji $v
     * @return Ptk The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSumberGajiRelatedBySumberGajiId(SumberGaji $v = null)
    {
        if ($v === null) {
            $this->setSumberGajiId(NULL);
        } else {
            $this->setSumberGajiId($v->getSumberGajiId());
        }

        $this->aSumberGajiRelatedBySumberGajiId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the SumberGaji object, it will not be re-added.
        if ($v !== null) {
            $v->addPtkRelatedBySumberGajiId($this);
        }


        return $this;
    }


    /**
     * Get the associated SumberGaji object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return SumberGaji The associated SumberGaji object.
     * @throws PropelException
     */
    public function getSumberGajiRelatedBySumberGajiId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSumberGajiRelatedBySumberGajiId === null && (($this->sumber_gaji_id !== "" && $this->sumber_gaji_id !== null)) && $doQuery) {
            $this->aSumberGajiRelatedBySumberGajiId = SumberGajiQuery::create()->findPk($this->sumber_gaji_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSumberGajiRelatedBySumberGajiId->addPtksRelatedBySumberGajiId($this);
             */
        }

        return $this->aSumberGajiRelatedBySumberGajiId;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('KesejahteraanRelatedByPtkId' == $relationName) {
            $this->initKesejahteraansRelatedByPtkId();
        }
        if ('KesejahteraanRelatedByPtkId' == $relationName) {
            $this->initKesejahteraansRelatedByPtkId();
        }
        if ('VldPtkRelatedByPtkId' == $relationName) {
            $this->initVldPtksRelatedByPtkId();
        }
        if ('VldPtkRelatedByPtkId' == $relationName) {
            $this->initVldPtksRelatedByPtkId();
        }
        if ('PenghargaanRelatedByPtkId' == $relationName) {
            $this->initPenghargaansRelatedByPtkId();
        }
        if ('PenghargaanRelatedByPtkId' == $relationName) {
            $this->initPenghargaansRelatedByPtkId();
        }
        if ('InpassingRelatedByPtkId' == $relationName) {
            $this->initInpassingsRelatedByPtkId();
        }
        if ('InpassingRelatedByPtkId' == $relationName) {
            $this->initInpassingsRelatedByPtkId();
        }
        if ('PtkTerdaftarRelatedByPtkId' == $relationName) {
            $this->initPtkTerdaftarsRelatedByPtkId();
        }
        if ('PtkTerdaftarRelatedByPtkId' == $relationName) {
            $this->initPtkTerdaftarsRelatedByPtkId();
        }
        if ('KaryaTulisRelatedByPtkId' == $relationName) {
            $this->initKaryaTulissRelatedByPtkId();
        }
        if ('KaryaTulisRelatedByPtkId' == $relationName) {
            $this->initKaryaTulissRelatedByPtkId();
        }
        if ('NilaiTestRelatedByPtkId' == $relationName) {
            $this->initNilaiTestsRelatedByPtkId();
        }
        if ('NilaiTestRelatedByPtkId' == $relationName) {
            $this->initNilaiTestsRelatedByPtkId();
        }
        if ('BukuPtkRelatedByPtkId' == $relationName) {
            $this->initBukuPtksRelatedByPtkId();
        }
        if ('BukuPtkRelatedByPtkId' == $relationName) {
            $this->initBukuPtksRelatedByPtkId();
        }
        if ('BeasiswaPtkRelatedByPtkId' == $relationName) {
            $this->initBeasiswaPtksRelatedByPtkId();
        }
        if ('BeasiswaPtkRelatedByPtkId' == $relationName) {
            $this->initBeasiswaPtksRelatedByPtkId();
        }
        if ('RwyKepangkatanRelatedByPtkId' == $relationName) {
            $this->initRwyKepangkatansRelatedByPtkId();
        }
        if ('RwyKepangkatanRelatedByPtkId' == $relationName) {
            $this->initRwyKepangkatansRelatedByPtkId();
        }
        if ('RombonganBelajarRelatedByPtkId' == $relationName) {
            $this->initRombonganBelajarsRelatedByPtkId();
        }
        if ('RombonganBelajarRelatedByPtkId' == $relationName) {
            $this->initRombonganBelajarsRelatedByPtkId();
        }
        if ('RiwayatGajiBerkalaRelatedByPtkId' == $relationName) {
            $this->initRiwayatGajiBerkalasRelatedByPtkId();
        }
        if ('RiwayatGajiBerkalaRelatedByPtkId' == $relationName) {
            $this->initRiwayatGajiBerkalasRelatedByPtkId();
        }
        if ('PengawasTerdaftarRelatedByPtkId' == $relationName) {
            $this->initPengawasTerdaftarsRelatedByPtkId();
        }
        if ('PengawasTerdaftarRelatedByPtkId' == $relationName) {
            $this->initPengawasTerdaftarsRelatedByPtkId();
        }
        if ('RwyStrukturalRelatedByPtkId' == $relationName) {
            $this->initRwyStrukturalsRelatedByPtkId();
        }
        if ('RwyStrukturalRelatedByPtkId' == $relationName) {
            $this->initRwyStrukturalsRelatedByPtkId();
        }
        if ('RwyPendFormalRelatedByPtkId' == $relationName) {
            $this->initRwyPendFormalsRelatedByPtkId();
        }
        if ('RwyPendFormalRelatedByPtkId' == $relationName) {
            $this->initRwyPendFormalsRelatedByPtkId();
        }
        if ('AnakRelatedByPtkId' == $relationName) {
            $this->initAnaksRelatedByPtkId();
        }
        if ('AnakRelatedByPtkId' == $relationName) {
            $this->initAnaksRelatedByPtkId();
        }
        if ('DiklatRelatedByPtkId' == $relationName) {
            $this->initDiklatsRelatedByPtkId();
        }
        if ('DiklatRelatedByPtkId' == $relationName) {
            $this->initDiklatsRelatedByPtkId();
        }
        if ('PtkBaruRelatedByPtkId' == $relationName) {
            $this->initPtkBarusRelatedByPtkId();
        }
        if ('PtkBaruRelatedByPtkId' == $relationName) {
            $this->initPtkBarusRelatedByPtkId();
        }
        if ('TunjanganRelatedByPtkId' == $relationName) {
            $this->initTunjangansRelatedByPtkId();
        }
        if ('TunjanganRelatedByPtkId' == $relationName) {
            $this->initTunjangansRelatedByPtkId();
        }
        if ('RwySertifikasiRelatedByPtkId' == $relationName) {
            $this->initRwySertifikasisRelatedByPtkId();
        }
        if ('RwySertifikasiRelatedByPtkId' == $relationName) {
            $this->initRwySertifikasisRelatedByPtkId();
        }
        if ('TugasTambahanRelatedByPtkId' == $relationName) {
            $this->initTugasTambahansRelatedByPtkId();
        }
        if ('TugasTambahanRelatedByPtkId' == $relationName) {
            $this->initTugasTambahansRelatedByPtkId();
        }
        if ('RwyFungsionalRelatedByPtkId' == $relationName) {
            $this->initRwyFungsionalsRelatedByPtkId();
        }
        if ('RwyFungsionalRelatedByPtkId' == $relationName) {
            $this->initRwyFungsionalsRelatedByPtkId();
        }
    }

    /**
     * Clears out the collKesejahteraansRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addKesejahteraansRelatedByPtkId()
     */
    public function clearKesejahteraansRelatedByPtkId()
    {
        $this->collKesejahteraansRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collKesejahteraansRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collKesejahteraansRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialKesejahteraansRelatedByPtkId($v = true)
    {
        $this->collKesejahteraansRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collKesejahteraansRelatedByPtkId collection.
     *
     * By default this just sets the collKesejahteraansRelatedByPtkId collection to an empty array (like clearcollKesejahteraansRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initKesejahteraansRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collKesejahteraansRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collKesejahteraansRelatedByPtkId = new PropelObjectCollection();
        $this->collKesejahteraansRelatedByPtkId->setModel('Kesejahteraan');
    }

    /**
     * Gets an array of Kesejahteraan objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Kesejahteraan[] List of Kesejahteraan objects
     * @throws PropelException
     */
    public function getKesejahteraansRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collKesejahteraansRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collKesejahteraansRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collKesejahteraansRelatedByPtkId) {
                // return empty collection
                $this->initKesejahteraansRelatedByPtkId();
            } else {
                $collKesejahteraansRelatedByPtkId = KesejahteraanQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collKesejahteraansRelatedByPtkIdPartial && count($collKesejahteraansRelatedByPtkId)) {
                      $this->initKesejahteraansRelatedByPtkId(false);

                      foreach($collKesejahteraansRelatedByPtkId as $obj) {
                        if (false == $this->collKesejahteraansRelatedByPtkId->contains($obj)) {
                          $this->collKesejahteraansRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collKesejahteraansRelatedByPtkIdPartial = true;
                    }

                    $collKesejahteraansRelatedByPtkId->getInternalIterator()->rewind();
                    return $collKesejahteraansRelatedByPtkId;
                }

                if($partial && $this->collKesejahteraansRelatedByPtkId) {
                    foreach($this->collKesejahteraansRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collKesejahteraansRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collKesejahteraansRelatedByPtkId = $collKesejahteraansRelatedByPtkId;
                $this->collKesejahteraansRelatedByPtkIdPartial = false;
            }
        }

        return $this->collKesejahteraansRelatedByPtkId;
    }

    /**
     * Sets a collection of KesejahteraanRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $kesejahteraansRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setKesejahteraansRelatedByPtkId(PropelCollection $kesejahteraansRelatedByPtkId, PropelPDO $con = null)
    {
        $kesejahteraansRelatedByPtkIdToDelete = $this->getKesejahteraansRelatedByPtkId(new Criteria(), $con)->diff($kesejahteraansRelatedByPtkId);

        $this->kesejahteraansRelatedByPtkIdScheduledForDeletion = unserialize(serialize($kesejahteraansRelatedByPtkIdToDelete));

        foreach ($kesejahteraansRelatedByPtkIdToDelete as $kesejahteraanRelatedByPtkIdRemoved) {
            $kesejahteraanRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collKesejahteraansRelatedByPtkId = null;
        foreach ($kesejahteraansRelatedByPtkId as $kesejahteraanRelatedByPtkId) {
            $this->addKesejahteraanRelatedByPtkId($kesejahteraanRelatedByPtkId);
        }

        $this->collKesejahteraansRelatedByPtkId = $kesejahteraansRelatedByPtkId;
        $this->collKesejahteraansRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Kesejahteraan objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Kesejahteraan objects.
     * @throws PropelException
     */
    public function countKesejahteraansRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collKesejahteraansRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collKesejahteraansRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collKesejahteraansRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getKesejahteraansRelatedByPtkId());
            }
            $query = KesejahteraanQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collKesejahteraansRelatedByPtkId);
    }

    /**
     * Method called to associate a Kesejahteraan object to this object
     * through the Kesejahteraan foreign key attribute.
     *
     * @param    Kesejahteraan $l Kesejahteraan
     * @return Ptk The current object (for fluent API support)
     */
    public function addKesejahteraanRelatedByPtkId(Kesejahteraan $l)
    {
        if ($this->collKesejahteraansRelatedByPtkId === null) {
            $this->initKesejahteraansRelatedByPtkId();
            $this->collKesejahteraansRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collKesejahteraansRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddKesejahteraanRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	KesejahteraanRelatedByPtkId $kesejahteraanRelatedByPtkId The kesejahteraanRelatedByPtkId object to add.
     */
    protected function doAddKesejahteraanRelatedByPtkId($kesejahteraanRelatedByPtkId)
    {
        $this->collKesejahteraansRelatedByPtkId[]= $kesejahteraanRelatedByPtkId;
        $kesejahteraanRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	KesejahteraanRelatedByPtkId $kesejahteraanRelatedByPtkId The kesejahteraanRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removeKesejahteraanRelatedByPtkId($kesejahteraanRelatedByPtkId)
    {
        if ($this->getKesejahteraansRelatedByPtkId()->contains($kesejahteraanRelatedByPtkId)) {
            $this->collKesejahteraansRelatedByPtkId->remove($this->collKesejahteraansRelatedByPtkId->search($kesejahteraanRelatedByPtkId));
            if (null === $this->kesejahteraansRelatedByPtkIdScheduledForDeletion) {
                $this->kesejahteraansRelatedByPtkIdScheduledForDeletion = clone $this->collKesejahteraansRelatedByPtkId;
                $this->kesejahteraansRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->kesejahteraansRelatedByPtkIdScheduledForDeletion[]= clone $kesejahteraanRelatedByPtkId;
            $kesejahteraanRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related KesejahteraansRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Kesejahteraan[] List of Kesejahteraan objects
     */
    public function getKesejahteraansRelatedByPtkIdJoinJenisKesejahteraanRelatedByJenisKesejahteraanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = KesejahteraanQuery::create(null, $criteria);
        $query->joinWith('JenisKesejahteraanRelatedByJenisKesejahteraanId', $join_behavior);

        return $this->getKesejahteraansRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related KesejahteraansRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Kesejahteraan[] List of Kesejahteraan objects
     */
    public function getKesejahteraansRelatedByPtkIdJoinJenisKesejahteraanRelatedByJenisKesejahteraanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = KesejahteraanQuery::create(null, $criteria);
        $query->joinWith('JenisKesejahteraanRelatedByJenisKesejahteraanId', $join_behavior);

        return $this->getKesejahteraansRelatedByPtkId($query, $con);
    }

    /**
     * Clears out the collKesejahteraansRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addKesejahteraansRelatedByPtkId()
     */
    public function clearKesejahteraansRelatedByPtkId()
    {
        $this->collKesejahteraansRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collKesejahteraansRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collKesejahteraansRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialKesejahteraansRelatedByPtkId($v = true)
    {
        $this->collKesejahteraansRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collKesejahteraansRelatedByPtkId collection.
     *
     * By default this just sets the collKesejahteraansRelatedByPtkId collection to an empty array (like clearcollKesejahteraansRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initKesejahteraansRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collKesejahteraansRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collKesejahteraansRelatedByPtkId = new PropelObjectCollection();
        $this->collKesejahteraansRelatedByPtkId->setModel('Kesejahteraan');
    }

    /**
     * Gets an array of Kesejahteraan objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Kesejahteraan[] List of Kesejahteraan objects
     * @throws PropelException
     */
    public function getKesejahteraansRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collKesejahteraansRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collKesejahteraansRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collKesejahteraansRelatedByPtkId) {
                // return empty collection
                $this->initKesejahteraansRelatedByPtkId();
            } else {
                $collKesejahteraansRelatedByPtkId = KesejahteraanQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collKesejahteraansRelatedByPtkIdPartial && count($collKesejahteraansRelatedByPtkId)) {
                      $this->initKesejahteraansRelatedByPtkId(false);

                      foreach($collKesejahteraansRelatedByPtkId as $obj) {
                        if (false == $this->collKesejahteraansRelatedByPtkId->contains($obj)) {
                          $this->collKesejahteraansRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collKesejahteraansRelatedByPtkIdPartial = true;
                    }

                    $collKesejahteraansRelatedByPtkId->getInternalIterator()->rewind();
                    return $collKesejahteraansRelatedByPtkId;
                }

                if($partial && $this->collKesejahteraansRelatedByPtkId) {
                    foreach($this->collKesejahteraansRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collKesejahteraansRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collKesejahteraansRelatedByPtkId = $collKesejahteraansRelatedByPtkId;
                $this->collKesejahteraansRelatedByPtkIdPartial = false;
            }
        }

        return $this->collKesejahteraansRelatedByPtkId;
    }

    /**
     * Sets a collection of KesejahteraanRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $kesejahteraansRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setKesejahteraansRelatedByPtkId(PropelCollection $kesejahteraansRelatedByPtkId, PropelPDO $con = null)
    {
        $kesejahteraansRelatedByPtkIdToDelete = $this->getKesejahteraansRelatedByPtkId(new Criteria(), $con)->diff($kesejahteraansRelatedByPtkId);

        $this->kesejahteraansRelatedByPtkIdScheduledForDeletion = unserialize(serialize($kesejahteraansRelatedByPtkIdToDelete));

        foreach ($kesejahteraansRelatedByPtkIdToDelete as $kesejahteraanRelatedByPtkIdRemoved) {
            $kesejahteraanRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collKesejahteraansRelatedByPtkId = null;
        foreach ($kesejahteraansRelatedByPtkId as $kesejahteraanRelatedByPtkId) {
            $this->addKesejahteraanRelatedByPtkId($kesejahteraanRelatedByPtkId);
        }

        $this->collKesejahteraansRelatedByPtkId = $kesejahteraansRelatedByPtkId;
        $this->collKesejahteraansRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Kesejahteraan objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Kesejahteraan objects.
     * @throws PropelException
     */
    public function countKesejahteraansRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collKesejahteraansRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collKesejahteraansRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collKesejahteraansRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getKesejahteraansRelatedByPtkId());
            }
            $query = KesejahteraanQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collKesejahteraansRelatedByPtkId);
    }

    /**
     * Method called to associate a Kesejahteraan object to this object
     * through the Kesejahteraan foreign key attribute.
     *
     * @param    Kesejahteraan $l Kesejahteraan
     * @return Ptk The current object (for fluent API support)
     */
    public function addKesejahteraanRelatedByPtkId(Kesejahteraan $l)
    {
        if ($this->collKesejahteraansRelatedByPtkId === null) {
            $this->initKesejahteraansRelatedByPtkId();
            $this->collKesejahteraansRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collKesejahteraansRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddKesejahteraanRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	KesejahteraanRelatedByPtkId $kesejahteraanRelatedByPtkId The kesejahteraanRelatedByPtkId object to add.
     */
    protected function doAddKesejahteraanRelatedByPtkId($kesejahteraanRelatedByPtkId)
    {
        $this->collKesejahteraansRelatedByPtkId[]= $kesejahteraanRelatedByPtkId;
        $kesejahteraanRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	KesejahteraanRelatedByPtkId $kesejahteraanRelatedByPtkId The kesejahteraanRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removeKesejahteraanRelatedByPtkId($kesejahteraanRelatedByPtkId)
    {
        if ($this->getKesejahteraansRelatedByPtkId()->contains($kesejahteraanRelatedByPtkId)) {
            $this->collKesejahteraansRelatedByPtkId->remove($this->collKesejahteraansRelatedByPtkId->search($kesejahteraanRelatedByPtkId));
            if (null === $this->kesejahteraansRelatedByPtkIdScheduledForDeletion) {
                $this->kesejahteraansRelatedByPtkIdScheduledForDeletion = clone $this->collKesejahteraansRelatedByPtkId;
                $this->kesejahteraansRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->kesejahteraansRelatedByPtkIdScheduledForDeletion[]= clone $kesejahteraanRelatedByPtkId;
            $kesejahteraanRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related KesejahteraansRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Kesejahteraan[] List of Kesejahteraan objects
     */
    public function getKesejahteraansRelatedByPtkIdJoinJenisKesejahteraanRelatedByJenisKesejahteraanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = KesejahteraanQuery::create(null, $criteria);
        $query->joinWith('JenisKesejahteraanRelatedByJenisKesejahteraanId', $join_behavior);

        return $this->getKesejahteraansRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related KesejahteraansRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Kesejahteraan[] List of Kesejahteraan objects
     */
    public function getKesejahteraansRelatedByPtkIdJoinJenisKesejahteraanRelatedByJenisKesejahteraanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = KesejahteraanQuery::create(null, $criteria);
        $query->joinWith('JenisKesejahteraanRelatedByJenisKesejahteraanId', $join_behavior);

        return $this->getKesejahteraansRelatedByPtkId($query, $con);
    }

    /**
     * Clears out the collVldPtksRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addVldPtksRelatedByPtkId()
     */
    public function clearVldPtksRelatedByPtkId()
    {
        $this->collVldPtksRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collVldPtksRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collVldPtksRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldPtksRelatedByPtkId($v = true)
    {
        $this->collVldPtksRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collVldPtksRelatedByPtkId collection.
     *
     * By default this just sets the collVldPtksRelatedByPtkId collection to an empty array (like clearcollVldPtksRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldPtksRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collVldPtksRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collVldPtksRelatedByPtkId = new PropelObjectCollection();
        $this->collVldPtksRelatedByPtkId->setModel('VldPtk');
    }

    /**
     * Gets an array of VldPtk objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldPtk[] List of VldPtk objects
     * @throws PropelException
     */
    public function getVldPtksRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldPtksRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collVldPtksRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldPtksRelatedByPtkId) {
                // return empty collection
                $this->initVldPtksRelatedByPtkId();
            } else {
                $collVldPtksRelatedByPtkId = VldPtkQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldPtksRelatedByPtkIdPartial && count($collVldPtksRelatedByPtkId)) {
                      $this->initVldPtksRelatedByPtkId(false);

                      foreach($collVldPtksRelatedByPtkId as $obj) {
                        if (false == $this->collVldPtksRelatedByPtkId->contains($obj)) {
                          $this->collVldPtksRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collVldPtksRelatedByPtkIdPartial = true;
                    }

                    $collVldPtksRelatedByPtkId->getInternalIterator()->rewind();
                    return $collVldPtksRelatedByPtkId;
                }

                if($partial && $this->collVldPtksRelatedByPtkId) {
                    foreach($this->collVldPtksRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collVldPtksRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collVldPtksRelatedByPtkId = $collVldPtksRelatedByPtkId;
                $this->collVldPtksRelatedByPtkIdPartial = false;
            }
        }

        return $this->collVldPtksRelatedByPtkId;
    }

    /**
     * Sets a collection of VldPtkRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldPtksRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setVldPtksRelatedByPtkId(PropelCollection $vldPtksRelatedByPtkId, PropelPDO $con = null)
    {
        $vldPtksRelatedByPtkIdToDelete = $this->getVldPtksRelatedByPtkId(new Criteria(), $con)->diff($vldPtksRelatedByPtkId);

        $this->vldPtksRelatedByPtkIdScheduledForDeletion = unserialize(serialize($vldPtksRelatedByPtkIdToDelete));

        foreach ($vldPtksRelatedByPtkIdToDelete as $vldPtkRelatedByPtkIdRemoved) {
            $vldPtkRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collVldPtksRelatedByPtkId = null;
        foreach ($vldPtksRelatedByPtkId as $vldPtkRelatedByPtkId) {
            $this->addVldPtkRelatedByPtkId($vldPtkRelatedByPtkId);
        }

        $this->collVldPtksRelatedByPtkId = $vldPtksRelatedByPtkId;
        $this->collVldPtksRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldPtk objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldPtk objects.
     * @throws PropelException
     */
    public function countVldPtksRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldPtksRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collVldPtksRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldPtksRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldPtksRelatedByPtkId());
            }
            $query = VldPtkQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collVldPtksRelatedByPtkId);
    }

    /**
     * Method called to associate a VldPtk object to this object
     * through the VldPtk foreign key attribute.
     *
     * @param    VldPtk $l VldPtk
     * @return Ptk The current object (for fluent API support)
     */
    public function addVldPtkRelatedByPtkId(VldPtk $l)
    {
        if ($this->collVldPtksRelatedByPtkId === null) {
            $this->initVldPtksRelatedByPtkId();
            $this->collVldPtksRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collVldPtksRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldPtkRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	VldPtkRelatedByPtkId $vldPtkRelatedByPtkId The vldPtkRelatedByPtkId object to add.
     */
    protected function doAddVldPtkRelatedByPtkId($vldPtkRelatedByPtkId)
    {
        $this->collVldPtksRelatedByPtkId[]= $vldPtkRelatedByPtkId;
        $vldPtkRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	VldPtkRelatedByPtkId $vldPtkRelatedByPtkId The vldPtkRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removeVldPtkRelatedByPtkId($vldPtkRelatedByPtkId)
    {
        if ($this->getVldPtksRelatedByPtkId()->contains($vldPtkRelatedByPtkId)) {
            $this->collVldPtksRelatedByPtkId->remove($this->collVldPtksRelatedByPtkId->search($vldPtkRelatedByPtkId));
            if (null === $this->vldPtksRelatedByPtkIdScheduledForDeletion) {
                $this->vldPtksRelatedByPtkIdScheduledForDeletion = clone $this->collVldPtksRelatedByPtkId;
                $this->vldPtksRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->vldPtksRelatedByPtkIdScheduledForDeletion[]= clone $vldPtkRelatedByPtkId;
            $vldPtkRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related VldPtksRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPtk[] List of VldPtk objects
     */
    public function getVldPtksRelatedByPtkIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPtkQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldPtksRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related VldPtksRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPtk[] List of VldPtk objects
     */
    public function getVldPtksRelatedByPtkIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPtkQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldPtksRelatedByPtkId($query, $con);
    }

    /**
     * Clears out the collVldPtksRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addVldPtksRelatedByPtkId()
     */
    public function clearVldPtksRelatedByPtkId()
    {
        $this->collVldPtksRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collVldPtksRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collVldPtksRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldPtksRelatedByPtkId($v = true)
    {
        $this->collVldPtksRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collVldPtksRelatedByPtkId collection.
     *
     * By default this just sets the collVldPtksRelatedByPtkId collection to an empty array (like clearcollVldPtksRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldPtksRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collVldPtksRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collVldPtksRelatedByPtkId = new PropelObjectCollection();
        $this->collVldPtksRelatedByPtkId->setModel('VldPtk');
    }

    /**
     * Gets an array of VldPtk objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldPtk[] List of VldPtk objects
     * @throws PropelException
     */
    public function getVldPtksRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldPtksRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collVldPtksRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldPtksRelatedByPtkId) {
                // return empty collection
                $this->initVldPtksRelatedByPtkId();
            } else {
                $collVldPtksRelatedByPtkId = VldPtkQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldPtksRelatedByPtkIdPartial && count($collVldPtksRelatedByPtkId)) {
                      $this->initVldPtksRelatedByPtkId(false);

                      foreach($collVldPtksRelatedByPtkId as $obj) {
                        if (false == $this->collVldPtksRelatedByPtkId->contains($obj)) {
                          $this->collVldPtksRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collVldPtksRelatedByPtkIdPartial = true;
                    }

                    $collVldPtksRelatedByPtkId->getInternalIterator()->rewind();
                    return $collVldPtksRelatedByPtkId;
                }

                if($partial && $this->collVldPtksRelatedByPtkId) {
                    foreach($this->collVldPtksRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collVldPtksRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collVldPtksRelatedByPtkId = $collVldPtksRelatedByPtkId;
                $this->collVldPtksRelatedByPtkIdPartial = false;
            }
        }

        return $this->collVldPtksRelatedByPtkId;
    }

    /**
     * Sets a collection of VldPtkRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldPtksRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setVldPtksRelatedByPtkId(PropelCollection $vldPtksRelatedByPtkId, PropelPDO $con = null)
    {
        $vldPtksRelatedByPtkIdToDelete = $this->getVldPtksRelatedByPtkId(new Criteria(), $con)->diff($vldPtksRelatedByPtkId);

        $this->vldPtksRelatedByPtkIdScheduledForDeletion = unserialize(serialize($vldPtksRelatedByPtkIdToDelete));

        foreach ($vldPtksRelatedByPtkIdToDelete as $vldPtkRelatedByPtkIdRemoved) {
            $vldPtkRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collVldPtksRelatedByPtkId = null;
        foreach ($vldPtksRelatedByPtkId as $vldPtkRelatedByPtkId) {
            $this->addVldPtkRelatedByPtkId($vldPtkRelatedByPtkId);
        }

        $this->collVldPtksRelatedByPtkId = $vldPtksRelatedByPtkId;
        $this->collVldPtksRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldPtk objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldPtk objects.
     * @throws PropelException
     */
    public function countVldPtksRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldPtksRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collVldPtksRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldPtksRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldPtksRelatedByPtkId());
            }
            $query = VldPtkQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collVldPtksRelatedByPtkId);
    }

    /**
     * Method called to associate a VldPtk object to this object
     * through the VldPtk foreign key attribute.
     *
     * @param    VldPtk $l VldPtk
     * @return Ptk The current object (for fluent API support)
     */
    public function addVldPtkRelatedByPtkId(VldPtk $l)
    {
        if ($this->collVldPtksRelatedByPtkId === null) {
            $this->initVldPtksRelatedByPtkId();
            $this->collVldPtksRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collVldPtksRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldPtkRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	VldPtkRelatedByPtkId $vldPtkRelatedByPtkId The vldPtkRelatedByPtkId object to add.
     */
    protected function doAddVldPtkRelatedByPtkId($vldPtkRelatedByPtkId)
    {
        $this->collVldPtksRelatedByPtkId[]= $vldPtkRelatedByPtkId;
        $vldPtkRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	VldPtkRelatedByPtkId $vldPtkRelatedByPtkId The vldPtkRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removeVldPtkRelatedByPtkId($vldPtkRelatedByPtkId)
    {
        if ($this->getVldPtksRelatedByPtkId()->contains($vldPtkRelatedByPtkId)) {
            $this->collVldPtksRelatedByPtkId->remove($this->collVldPtksRelatedByPtkId->search($vldPtkRelatedByPtkId));
            if (null === $this->vldPtksRelatedByPtkIdScheduledForDeletion) {
                $this->vldPtksRelatedByPtkIdScheduledForDeletion = clone $this->collVldPtksRelatedByPtkId;
                $this->vldPtksRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->vldPtksRelatedByPtkIdScheduledForDeletion[]= clone $vldPtkRelatedByPtkId;
            $vldPtkRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related VldPtksRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPtk[] List of VldPtk objects
     */
    public function getVldPtksRelatedByPtkIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPtkQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldPtksRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related VldPtksRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPtk[] List of VldPtk objects
     */
    public function getVldPtksRelatedByPtkIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPtkQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldPtksRelatedByPtkId($query, $con);
    }

    /**
     * Clears out the collPenghargaansRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addPenghargaansRelatedByPtkId()
     */
    public function clearPenghargaansRelatedByPtkId()
    {
        $this->collPenghargaansRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collPenghargaansRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPenghargaansRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPenghargaansRelatedByPtkId($v = true)
    {
        $this->collPenghargaansRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collPenghargaansRelatedByPtkId collection.
     *
     * By default this just sets the collPenghargaansRelatedByPtkId collection to an empty array (like clearcollPenghargaansRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPenghargaansRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collPenghargaansRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collPenghargaansRelatedByPtkId = new PropelObjectCollection();
        $this->collPenghargaansRelatedByPtkId->setModel('Penghargaan');
    }

    /**
     * Gets an array of Penghargaan objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Penghargaan[] List of Penghargaan objects
     * @throws PropelException
     */
    public function getPenghargaansRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPenghargaansRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collPenghargaansRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPenghargaansRelatedByPtkId) {
                // return empty collection
                $this->initPenghargaansRelatedByPtkId();
            } else {
                $collPenghargaansRelatedByPtkId = PenghargaanQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPenghargaansRelatedByPtkIdPartial && count($collPenghargaansRelatedByPtkId)) {
                      $this->initPenghargaansRelatedByPtkId(false);

                      foreach($collPenghargaansRelatedByPtkId as $obj) {
                        if (false == $this->collPenghargaansRelatedByPtkId->contains($obj)) {
                          $this->collPenghargaansRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collPenghargaansRelatedByPtkIdPartial = true;
                    }

                    $collPenghargaansRelatedByPtkId->getInternalIterator()->rewind();
                    return $collPenghargaansRelatedByPtkId;
                }

                if($partial && $this->collPenghargaansRelatedByPtkId) {
                    foreach($this->collPenghargaansRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collPenghargaansRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collPenghargaansRelatedByPtkId = $collPenghargaansRelatedByPtkId;
                $this->collPenghargaansRelatedByPtkIdPartial = false;
            }
        }

        return $this->collPenghargaansRelatedByPtkId;
    }

    /**
     * Sets a collection of PenghargaanRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $penghargaansRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setPenghargaansRelatedByPtkId(PropelCollection $penghargaansRelatedByPtkId, PropelPDO $con = null)
    {
        $penghargaansRelatedByPtkIdToDelete = $this->getPenghargaansRelatedByPtkId(new Criteria(), $con)->diff($penghargaansRelatedByPtkId);

        $this->penghargaansRelatedByPtkIdScheduledForDeletion = unserialize(serialize($penghargaansRelatedByPtkIdToDelete));

        foreach ($penghargaansRelatedByPtkIdToDelete as $penghargaanRelatedByPtkIdRemoved) {
            $penghargaanRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collPenghargaansRelatedByPtkId = null;
        foreach ($penghargaansRelatedByPtkId as $penghargaanRelatedByPtkId) {
            $this->addPenghargaanRelatedByPtkId($penghargaanRelatedByPtkId);
        }

        $this->collPenghargaansRelatedByPtkId = $penghargaansRelatedByPtkId;
        $this->collPenghargaansRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Penghargaan objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Penghargaan objects.
     * @throws PropelException
     */
    public function countPenghargaansRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPenghargaansRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collPenghargaansRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPenghargaansRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPenghargaansRelatedByPtkId());
            }
            $query = PenghargaanQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collPenghargaansRelatedByPtkId);
    }

    /**
     * Method called to associate a Penghargaan object to this object
     * through the Penghargaan foreign key attribute.
     *
     * @param    Penghargaan $l Penghargaan
     * @return Ptk The current object (for fluent API support)
     */
    public function addPenghargaanRelatedByPtkId(Penghargaan $l)
    {
        if ($this->collPenghargaansRelatedByPtkId === null) {
            $this->initPenghargaansRelatedByPtkId();
            $this->collPenghargaansRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collPenghargaansRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPenghargaanRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	PenghargaanRelatedByPtkId $penghargaanRelatedByPtkId The penghargaanRelatedByPtkId object to add.
     */
    protected function doAddPenghargaanRelatedByPtkId($penghargaanRelatedByPtkId)
    {
        $this->collPenghargaansRelatedByPtkId[]= $penghargaanRelatedByPtkId;
        $penghargaanRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	PenghargaanRelatedByPtkId $penghargaanRelatedByPtkId The penghargaanRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removePenghargaanRelatedByPtkId($penghargaanRelatedByPtkId)
    {
        if ($this->getPenghargaansRelatedByPtkId()->contains($penghargaanRelatedByPtkId)) {
            $this->collPenghargaansRelatedByPtkId->remove($this->collPenghargaansRelatedByPtkId->search($penghargaanRelatedByPtkId));
            if (null === $this->penghargaansRelatedByPtkIdScheduledForDeletion) {
                $this->penghargaansRelatedByPtkIdScheduledForDeletion = clone $this->collPenghargaansRelatedByPtkId;
                $this->penghargaansRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->penghargaansRelatedByPtkIdScheduledForDeletion[]= clone $penghargaanRelatedByPtkId;
            $penghargaanRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PenghargaansRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Penghargaan[] List of Penghargaan objects
     */
    public function getPenghargaansRelatedByPtkIdJoinJenisPenghargaanRelatedByJenisPenghargaanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PenghargaanQuery::create(null, $criteria);
        $query->joinWith('JenisPenghargaanRelatedByJenisPenghargaanId', $join_behavior);

        return $this->getPenghargaansRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PenghargaansRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Penghargaan[] List of Penghargaan objects
     */
    public function getPenghargaansRelatedByPtkIdJoinJenisPenghargaanRelatedByJenisPenghargaanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PenghargaanQuery::create(null, $criteria);
        $query->joinWith('JenisPenghargaanRelatedByJenisPenghargaanId', $join_behavior);

        return $this->getPenghargaansRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PenghargaansRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Penghargaan[] List of Penghargaan objects
     */
    public function getPenghargaansRelatedByPtkIdJoinTingkatPenghargaanRelatedByTingkatPenghargaanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PenghargaanQuery::create(null, $criteria);
        $query->joinWith('TingkatPenghargaanRelatedByTingkatPenghargaanId', $join_behavior);

        return $this->getPenghargaansRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PenghargaansRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Penghargaan[] List of Penghargaan objects
     */
    public function getPenghargaansRelatedByPtkIdJoinTingkatPenghargaanRelatedByTingkatPenghargaanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PenghargaanQuery::create(null, $criteria);
        $query->joinWith('TingkatPenghargaanRelatedByTingkatPenghargaanId', $join_behavior);

        return $this->getPenghargaansRelatedByPtkId($query, $con);
    }

    /**
     * Clears out the collPenghargaansRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addPenghargaansRelatedByPtkId()
     */
    public function clearPenghargaansRelatedByPtkId()
    {
        $this->collPenghargaansRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collPenghargaansRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPenghargaansRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPenghargaansRelatedByPtkId($v = true)
    {
        $this->collPenghargaansRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collPenghargaansRelatedByPtkId collection.
     *
     * By default this just sets the collPenghargaansRelatedByPtkId collection to an empty array (like clearcollPenghargaansRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPenghargaansRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collPenghargaansRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collPenghargaansRelatedByPtkId = new PropelObjectCollection();
        $this->collPenghargaansRelatedByPtkId->setModel('Penghargaan');
    }

    /**
     * Gets an array of Penghargaan objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Penghargaan[] List of Penghargaan objects
     * @throws PropelException
     */
    public function getPenghargaansRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPenghargaansRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collPenghargaansRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPenghargaansRelatedByPtkId) {
                // return empty collection
                $this->initPenghargaansRelatedByPtkId();
            } else {
                $collPenghargaansRelatedByPtkId = PenghargaanQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPenghargaansRelatedByPtkIdPartial && count($collPenghargaansRelatedByPtkId)) {
                      $this->initPenghargaansRelatedByPtkId(false);

                      foreach($collPenghargaansRelatedByPtkId as $obj) {
                        if (false == $this->collPenghargaansRelatedByPtkId->contains($obj)) {
                          $this->collPenghargaansRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collPenghargaansRelatedByPtkIdPartial = true;
                    }

                    $collPenghargaansRelatedByPtkId->getInternalIterator()->rewind();
                    return $collPenghargaansRelatedByPtkId;
                }

                if($partial && $this->collPenghargaansRelatedByPtkId) {
                    foreach($this->collPenghargaansRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collPenghargaansRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collPenghargaansRelatedByPtkId = $collPenghargaansRelatedByPtkId;
                $this->collPenghargaansRelatedByPtkIdPartial = false;
            }
        }

        return $this->collPenghargaansRelatedByPtkId;
    }

    /**
     * Sets a collection of PenghargaanRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $penghargaansRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setPenghargaansRelatedByPtkId(PropelCollection $penghargaansRelatedByPtkId, PropelPDO $con = null)
    {
        $penghargaansRelatedByPtkIdToDelete = $this->getPenghargaansRelatedByPtkId(new Criteria(), $con)->diff($penghargaansRelatedByPtkId);

        $this->penghargaansRelatedByPtkIdScheduledForDeletion = unserialize(serialize($penghargaansRelatedByPtkIdToDelete));

        foreach ($penghargaansRelatedByPtkIdToDelete as $penghargaanRelatedByPtkIdRemoved) {
            $penghargaanRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collPenghargaansRelatedByPtkId = null;
        foreach ($penghargaansRelatedByPtkId as $penghargaanRelatedByPtkId) {
            $this->addPenghargaanRelatedByPtkId($penghargaanRelatedByPtkId);
        }

        $this->collPenghargaansRelatedByPtkId = $penghargaansRelatedByPtkId;
        $this->collPenghargaansRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Penghargaan objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Penghargaan objects.
     * @throws PropelException
     */
    public function countPenghargaansRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPenghargaansRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collPenghargaansRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPenghargaansRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPenghargaansRelatedByPtkId());
            }
            $query = PenghargaanQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collPenghargaansRelatedByPtkId);
    }

    /**
     * Method called to associate a Penghargaan object to this object
     * through the Penghargaan foreign key attribute.
     *
     * @param    Penghargaan $l Penghargaan
     * @return Ptk The current object (for fluent API support)
     */
    public function addPenghargaanRelatedByPtkId(Penghargaan $l)
    {
        if ($this->collPenghargaansRelatedByPtkId === null) {
            $this->initPenghargaansRelatedByPtkId();
            $this->collPenghargaansRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collPenghargaansRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPenghargaanRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	PenghargaanRelatedByPtkId $penghargaanRelatedByPtkId The penghargaanRelatedByPtkId object to add.
     */
    protected function doAddPenghargaanRelatedByPtkId($penghargaanRelatedByPtkId)
    {
        $this->collPenghargaansRelatedByPtkId[]= $penghargaanRelatedByPtkId;
        $penghargaanRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	PenghargaanRelatedByPtkId $penghargaanRelatedByPtkId The penghargaanRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removePenghargaanRelatedByPtkId($penghargaanRelatedByPtkId)
    {
        if ($this->getPenghargaansRelatedByPtkId()->contains($penghargaanRelatedByPtkId)) {
            $this->collPenghargaansRelatedByPtkId->remove($this->collPenghargaansRelatedByPtkId->search($penghargaanRelatedByPtkId));
            if (null === $this->penghargaansRelatedByPtkIdScheduledForDeletion) {
                $this->penghargaansRelatedByPtkIdScheduledForDeletion = clone $this->collPenghargaansRelatedByPtkId;
                $this->penghargaansRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->penghargaansRelatedByPtkIdScheduledForDeletion[]= clone $penghargaanRelatedByPtkId;
            $penghargaanRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PenghargaansRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Penghargaan[] List of Penghargaan objects
     */
    public function getPenghargaansRelatedByPtkIdJoinJenisPenghargaanRelatedByJenisPenghargaanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PenghargaanQuery::create(null, $criteria);
        $query->joinWith('JenisPenghargaanRelatedByJenisPenghargaanId', $join_behavior);

        return $this->getPenghargaansRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PenghargaansRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Penghargaan[] List of Penghargaan objects
     */
    public function getPenghargaansRelatedByPtkIdJoinJenisPenghargaanRelatedByJenisPenghargaanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PenghargaanQuery::create(null, $criteria);
        $query->joinWith('JenisPenghargaanRelatedByJenisPenghargaanId', $join_behavior);

        return $this->getPenghargaansRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PenghargaansRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Penghargaan[] List of Penghargaan objects
     */
    public function getPenghargaansRelatedByPtkIdJoinTingkatPenghargaanRelatedByTingkatPenghargaanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PenghargaanQuery::create(null, $criteria);
        $query->joinWith('TingkatPenghargaanRelatedByTingkatPenghargaanId', $join_behavior);

        return $this->getPenghargaansRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PenghargaansRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Penghargaan[] List of Penghargaan objects
     */
    public function getPenghargaansRelatedByPtkIdJoinTingkatPenghargaanRelatedByTingkatPenghargaanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PenghargaanQuery::create(null, $criteria);
        $query->joinWith('TingkatPenghargaanRelatedByTingkatPenghargaanId', $join_behavior);

        return $this->getPenghargaansRelatedByPtkId($query, $con);
    }

    /**
     * Clears out the collInpassingsRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addInpassingsRelatedByPtkId()
     */
    public function clearInpassingsRelatedByPtkId()
    {
        $this->collInpassingsRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collInpassingsRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collInpassingsRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialInpassingsRelatedByPtkId($v = true)
    {
        $this->collInpassingsRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collInpassingsRelatedByPtkId collection.
     *
     * By default this just sets the collInpassingsRelatedByPtkId collection to an empty array (like clearcollInpassingsRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initInpassingsRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collInpassingsRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collInpassingsRelatedByPtkId = new PropelObjectCollection();
        $this->collInpassingsRelatedByPtkId->setModel('Inpassing');
    }

    /**
     * Gets an array of Inpassing objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Inpassing[] List of Inpassing objects
     * @throws PropelException
     */
    public function getInpassingsRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collInpassingsRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collInpassingsRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collInpassingsRelatedByPtkId) {
                // return empty collection
                $this->initInpassingsRelatedByPtkId();
            } else {
                $collInpassingsRelatedByPtkId = InpassingQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collInpassingsRelatedByPtkIdPartial && count($collInpassingsRelatedByPtkId)) {
                      $this->initInpassingsRelatedByPtkId(false);

                      foreach($collInpassingsRelatedByPtkId as $obj) {
                        if (false == $this->collInpassingsRelatedByPtkId->contains($obj)) {
                          $this->collInpassingsRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collInpassingsRelatedByPtkIdPartial = true;
                    }

                    $collInpassingsRelatedByPtkId->getInternalIterator()->rewind();
                    return $collInpassingsRelatedByPtkId;
                }

                if($partial && $this->collInpassingsRelatedByPtkId) {
                    foreach($this->collInpassingsRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collInpassingsRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collInpassingsRelatedByPtkId = $collInpassingsRelatedByPtkId;
                $this->collInpassingsRelatedByPtkIdPartial = false;
            }
        }

        return $this->collInpassingsRelatedByPtkId;
    }

    /**
     * Sets a collection of InpassingRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $inpassingsRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setInpassingsRelatedByPtkId(PropelCollection $inpassingsRelatedByPtkId, PropelPDO $con = null)
    {
        $inpassingsRelatedByPtkIdToDelete = $this->getInpassingsRelatedByPtkId(new Criteria(), $con)->diff($inpassingsRelatedByPtkId);

        $this->inpassingsRelatedByPtkIdScheduledForDeletion = unserialize(serialize($inpassingsRelatedByPtkIdToDelete));

        foreach ($inpassingsRelatedByPtkIdToDelete as $inpassingRelatedByPtkIdRemoved) {
            $inpassingRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collInpassingsRelatedByPtkId = null;
        foreach ($inpassingsRelatedByPtkId as $inpassingRelatedByPtkId) {
            $this->addInpassingRelatedByPtkId($inpassingRelatedByPtkId);
        }

        $this->collInpassingsRelatedByPtkId = $inpassingsRelatedByPtkId;
        $this->collInpassingsRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Inpassing objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Inpassing objects.
     * @throws PropelException
     */
    public function countInpassingsRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collInpassingsRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collInpassingsRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collInpassingsRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getInpassingsRelatedByPtkId());
            }
            $query = InpassingQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collInpassingsRelatedByPtkId);
    }

    /**
     * Method called to associate a Inpassing object to this object
     * through the Inpassing foreign key attribute.
     *
     * @param    Inpassing $l Inpassing
     * @return Ptk The current object (for fluent API support)
     */
    public function addInpassingRelatedByPtkId(Inpassing $l)
    {
        if ($this->collInpassingsRelatedByPtkId === null) {
            $this->initInpassingsRelatedByPtkId();
            $this->collInpassingsRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collInpassingsRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddInpassingRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	InpassingRelatedByPtkId $inpassingRelatedByPtkId The inpassingRelatedByPtkId object to add.
     */
    protected function doAddInpassingRelatedByPtkId($inpassingRelatedByPtkId)
    {
        $this->collInpassingsRelatedByPtkId[]= $inpassingRelatedByPtkId;
        $inpassingRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	InpassingRelatedByPtkId $inpassingRelatedByPtkId The inpassingRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removeInpassingRelatedByPtkId($inpassingRelatedByPtkId)
    {
        if ($this->getInpassingsRelatedByPtkId()->contains($inpassingRelatedByPtkId)) {
            $this->collInpassingsRelatedByPtkId->remove($this->collInpassingsRelatedByPtkId->search($inpassingRelatedByPtkId));
            if (null === $this->inpassingsRelatedByPtkIdScheduledForDeletion) {
                $this->inpassingsRelatedByPtkIdScheduledForDeletion = clone $this->collInpassingsRelatedByPtkId;
                $this->inpassingsRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->inpassingsRelatedByPtkIdScheduledForDeletion[]= $inpassingRelatedByPtkId;
            $inpassingRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related InpassingsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Inpassing[] List of Inpassing objects
     */
    public function getInpassingsRelatedByPtkIdJoinPangkatGolonganRelatedByPangkatGolonganId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = InpassingQuery::create(null, $criteria);
        $query->joinWith('PangkatGolonganRelatedByPangkatGolonganId', $join_behavior);

        return $this->getInpassingsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related InpassingsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Inpassing[] List of Inpassing objects
     */
    public function getInpassingsRelatedByPtkIdJoinPangkatGolonganRelatedByPangkatGolonganId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = InpassingQuery::create(null, $criteria);
        $query->joinWith('PangkatGolonganRelatedByPangkatGolonganId', $join_behavior);

        return $this->getInpassingsRelatedByPtkId($query, $con);
    }

    /**
     * Clears out the collInpassingsRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addInpassingsRelatedByPtkId()
     */
    public function clearInpassingsRelatedByPtkId()
    {
        $this->collInpassingsRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collInpassingsRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collInpassingsRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialInpassingsRelatedByPtkId($v = true)
    {
        $this->collInpassingsRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collInpassingsRelatedByPtkId collection.
     *
     * By default this just sets the collInpassingsRelatedByPtkId collection to an empty array (like clearcollInpassingsRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initInpassingsRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collInpassingsRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collInpassingsRelatedByPtkId = new PropelObjectCollection();
        $this->collInpassingsRelatedByPtkId->setModel('Inpassing');
    }

    /**
     * Gets an array of Inpassing objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Inpassing[] List of Inpassing objects
     * @throws PropelException
     */
    public function getInpassingsRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collInpassingsRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collInpassingsRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collInpassingsRelatedByPtkId) {
                // return empty collection
                $this->initInpassingsRelatedByPtkId();
            } else {
                $collInpassingsRelatedByPtkId = InpassingQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collInpassingsRelatedByPtkIdPartial && count($collInpassingsRelatedByPtkId)) {
                      $this->initInpassingsRelatedByPtkId(false);

                      foreach($collInpassingsRelatedByPtkId as $obj) {
                        if (false == $this->collInpassingsRelatedByPtkId->contains($obj)) {
                          $this->collInpassingsRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collInpassingsRelatedByPtkIdPartial = true;
                    }

                    $collInpassingsRelatedByPtkId->getInternalIterator()->rewind();
                    return $collInpassingsRelatedByPtkId;
                }

                if($partial && $this->collInpassingsRelatedByPtkId) {
                    foreach($this->collInpassingsRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collInpassingsRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collInpassingsRelatedByPtkId = $collInpassingsRelatedByPtkId;
                $this->collInpassingsRelatedByPtkIdPartial = false;
            }
        }

        return $this->collInpassingsRelatedByPtkId;
    }

    /**
     * Sets a collection of InpassingRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $inpassingsRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setInpassingsRelatedByPtkId(PropelCollection $inpassingsRelatedByPtkId, PropelPDO $con = null)
    {
        $inpassingsRelatedByPtkIdToDelete = $this->getInpassingsRelatedByPtkId(new Criteria(), $con)->diff($inpassingsRelatedByPtkId);

        $this->inpassingsRelatedByPtkIdScheduledForDeletion = unserialize(serialize($inpassingsRelatedByPtkIdToDelete));

        foreach ($inpassingsRelatedByPtkIdToDelete as $inpassingRelatedByPtkIdRemoved) {
            $inpassingRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collInpassingsRelatedByPtkId = null;
        foreach ($inpassingsRelatedByPtkId as $inpassingRelatedByPtkId) {
            $this->addInpassingRelatedByPtkId($inpassingRelatedByPtkId);
        }

        $this->collInpassingsRelatedByPtkId = $inpassingsRelatedByPtkId;
        $this->collInpassingsRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Inpassing objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Inpassing objects.
     * @throws PropelException
     */
    public function countInpassingsRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collInpassingsRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collInpassingsRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collInpassingsRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getInpassingsRelatedByPtkId());
            }
            $query = InpassingQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collInpassingsRelatedByPtkId);
    }

    /**
     * Method called to associate a Inpassing object to this object
     * through the Inpassing foreign key attribute.
     *
     * @param    Inpassing $l Inpassing
     * @return Ptk The current object (for fluent API support)
     */
    public function addInpassingRelatedByPtkId(Inpassing $l)
    {
        if ($this->collInpassingsRelatedByPtkId === null) {
            $this->initInpassingsRelatedByPtkId();
            $this->collInpassingsRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collInpassingsRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddInpassingRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	InpassingRelatedByPtkId $inpassingRelatedByPtkId The inpassingRelatedByPtkId object to add.
     */
    protected function doAddInpassingRelatedByPtkId($inpassingRelatedByPtkId)
    {
        $this->collInpassingsRelatedByPtkId[]= $inpassingRelatedByPtkId;
        $inpassingRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	InpassingRelatedByPtkId $inpassingRelatedByPtkId The inpassingRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removeInpassingRelatedByPtkId($inpassingRelatedByPtkId)
    {
        if ($this->getInpassingsRelatedByPtkId()->contains($inpassingRelatedByPtkId)) {
            $this->collInpassingsRelatedByPtkId->remove($this->collInpassingsRelatedByPtkId->search($inpassingRelatedByPtkId));
            if (null === $this->inpassingsRelatedByPtkIdScheduledForDeletion) {
                $this->inpassingsRelatedByPtkIdScheduledForDeletion = clone $this->collInpassingsRelatedByPtkId;
                $this->inpassingsRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->inpassingsRelatedByPtkIdScheduledForDeletion[]= $inpassingRelatedByPtkId;
            $inpassingRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related InpassingsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Inpassing[] List of Inpassing objects
     */
    public function getInpassingsRelatedByPtkIdJoinPangkatGolonganRelatedByPangkatGolonganId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = InpassingQuery::create(null, $criteria);
        $query->joinWith('PangkatGolonganRelatedByPangkatGolonganId', $join_behavior);

        return $this->getInpassingsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related InpassingsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Inpassing[] List of Inpassing objects
     */
    public function getInpassingsRelatedByPtkIdJoinPangkatGolonganRelatedByPangkatGolonganId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = InpassingQuery::create(null, $criteria);
        $query->joinWith('PangkatGolonganRelatedByPangkatGolonganId', $join_behavior);

        return $this->getInpassingsRelatedByPtkId($query, $con);
    }

    /**
     * Clears out the collPtkTerdaftarsRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addPtkTerdaftarsRelatedByPtkId()
     */
    public function clearPtkTerdaftarsRelatedByPtkId()
    {
        $this->collPtkTerdaftarsRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collPtkTerdaftarsRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPtkTerdaftarsRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPtkTerdaftarsRelatedByPtkId($v = true)
    {
        $this->collPtkTerdaftarsRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collPtkTerdaftarsRelatedByPtkId collection.
     *
     * By default this just sets the collPtkTerdaftarsRelatedByPtkId collection to an empty array (like clearcollPtkTerdaftarsRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPtkTerdaftarsRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collPtkTerdaftarsRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collPtkTerdaftarsRelatedByPtkId = new PropelObjectCollection();
        $this->collPtkTerdaftarsRelatedByPtkId->setModel('PtkTerdaftar');
    }

    /**
     * Gets an array of PtkTerdaftar objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     * @throws PropelException
     */
    public function getPtkTerdaftarsRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPtkTerdaftarsRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collPtkTerdaftarsRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPtkTerdaftarsRelatedByPtkId) {
                // return empty collection
                $this->initPtkTerdaftarsRelatedByPtkId();
            } else {
                $collPtkTerdaftarsRelatedByPtkId = PtkTerdaftarQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPtkTerdaftarsRelatedByPtkIdPartial && count($collPtkTerdaftarsRelatedByPtkId)) {
                      $this->initPtkTerdaftarsRelatedByPtkId(false);

                      foreach($collPtkTerdaftarsRelatedByPtkId as $obj) {
                        if (false == $this->collPtkTerdaftarsRelatedByPtkId->contains($obj)) {
                          $this->collPtkTerdaftarsRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collPtkTerdaftarsRelatedByPtkIdPartial = true;
                    }

                    $collPtkTerdaftarsRelatedByPtkId->getInternalIterator()->rewind();
                    return $collPtkTerdaftarsRelatedByPtkId;
                }

                if($partial && $this->collPtkTerdaftarsRelatedByPtkId) {
                    foreach($this->collPtkTerdaftarsRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collPtkTerdaftarsRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collPtkTerdaftarsRelatedByPtkId = $collPtkTerdaftarsRelatedByPtkId;
                $this->collPtkTerdaftarsRelatedByPtkIdPartial = false;
            }
        }

        return $this->collPtkTerdaftarsRelatedByPtkId;
    }

    /**
     * Sets a collection of PtkTerdaftarRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $ptkTerdaftarsRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setPtkTerdaftarsRelatedByPtkId(PropelCollection $ptkTerdaftarsRelatedByPtkId, PropelPDO $con = null)
    {
        $ptkTerdaftarsRelatedByPtkIdToDelete = $this->getPtkTerdaftarsRelatedByPtkId(new Criteria(), $con)->diff($ptkTerdaftarsRelatedByPtkId);

        $this->ptkTerdaftarsRelatedByPtkIdScheduledForDeletion = unserialize(serialize($ptkTerdaftarsRelatedByPtkIdToDelete));

        foreach ($ptkTerdaftarsRelatedByPtkIdToDelete as $ptkTerdaftarRelatedByPtkIdRemoved) {
            $ptkTerdaftarRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collPtkTerdaftarsRelatedByPtkId = null;
        foreach ($ptkTerdaftarsRelatedByPtkId as $ptkTerdaftarRelatedByPtkId) {
            $this->addPtkTerdaftarRelatedByPtkId($ptkTerdaftarRelatedByPtkId);
        }

        $this->collPtkTerdaftarsRelatedByPtkId = $ptkTerdaftarsRelatedByPtkId;
        $this->collPtkTerdaftarsRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PtkTerdaftar objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related PtkTerdaftar objects.
     * @throws PropelException
     */
    public function countPtkTerdaftarsRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPtkTerdaftarsRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collPtkTerdaftarsRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPtkTerdaftarsRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPtkTerdaftarsRelatedByPtkId());
            }
            $query = PtkTerdaftarQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collPtkTerdaftarsRelatedByPtkId);
    }

    /**
     * Method called to associate a PtkTerdaftar object to this object
     * through the PtkTerdaftar foreign key attribute.
     *
     * @param    PtkTerdaftar $l PtkTerdaftar
     * @return Ptk The current object (for fluent API support)
     */
    public function addPtkTerdaftarRelatedByPtkId(PtkTerdaftar $l)
    {
        if ($this->collPtkTerdaftarsRelatedByPtkId === null) {
            $this->initPtkTerdaftarsRelatedByPtkId();
            $this->collPtkTerdaftarsRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collPtkTerdaftarsRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPtkTerdaftarRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	PtkTerdaftarRelatedByPtkId $ptkTerdaftarRelatedByPtkId The ptkTerdaftarRelatedByPtkId object to add.
     */
    protected function doAddPtkTerdaftarRelatedByPtkId($ptkTerdaftarRelatedByPtkId)
    {
        $this->collPtkTerdaftarsRelatedByPtkId[]= $ptkTerdaftarRelatedByPtkId;
        $ptkTerdaftarRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	PtkTerdaftarRelatedByPtkId $ptkTerdaftarRelatedByPtkId The ptkTerdaftarRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removePtkTerdaftarRelatedByPtkId($ptkTerdaftarRelatedByPtkId)
    {
        if ($this->getPtkTerdaftarsRelatedByPtkId()->contains($ptkTerdaftarRelatedByPtkId)) {
            $this->collPtkTerdaftarsRelatedByPtkId->remove($this->collPtkTerdaftarsRelatedByPtkId->search($ptkTerdaftarRelatedByPtkId));
            if (null === $this->ptkTerdaftarsRelatedByPtkIdScheduledForDeletion) {
                $this->ptkTerdaftarsRelatedByPtkIdScheduledForDeletion = clone $this->collPtkTerdaftarsRelatedByPtkId;
                $this->ptkTerdaftarsRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->ptkTerdaftarsRelatedByPtkIdScheduledForDeletion[]= clone $ptkTerdaftarRelatedByPtkId;
            $ptkTerdaftarRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PtkTerdaftarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     */
    public function getPtkTerdaftarsRelatedByPtkIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkTerdaftarQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPtkTerdaftarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PtkTerdaftarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     */
    public function getPtkTerdaftarsRelatedByPtkIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkTerdaftarQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPtkTerdaftarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PtkTerdaftarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     */
    public function getPtkTerdaftarsRelatedByPtkIdJoinJenisKeluarRelatedByJenisKeluarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkTerdaftarQuery::create(null, $criteria);
        $query->joinWith('JenisKeluarRelatedByJenisKeluarId', $join_behavior);

        return $this->getPtkTerdaftarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PtkTerdaftarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     */
    public function getPtkTerdaftarsRelatedByPtkIdJoinJenisKeluarRelatedByJenisKeluarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkTerdaftarQuery::create(null, $criteria);
        $query->joinWith('JenisKeluarRelatedByJenisKeluarId', $join_behavior);

        return $this->getPtkTerdaftarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PtkTerdaftarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     */
    public function getPtkTerdaftarsRelatedByPtkIdJoinTahunAjaranRelatedByTahunAjaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkTerdaftarQuery::create(null, $criteria);
        $query->joinWith('TahunAjaranRelatedByTahunAjaranId', $join_behavior);

        return $this->getPtkTerdaftarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PtkTerdaftarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     */
    public function getPtkTerdaftarsRelatedByPtkIdJoinTahunAjaranRelatedByTahunAjaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkTerdaftarQuery::create(null, $criteria);
        $query->joinWith('TahunAjaranRelatedByTahunAjaranId', $join_behavior);

        return $this->getPtkTerdaftarsRelatedByPtkId($query, $con);
    }

    /**
     * Clears out the collPtkTerdaftarsRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addPtkTerdaftarsRelatedByPtkId()
     */
    public function clearPtkTerdaftarsRelatedByPtkId()
    {
        $this->collPtkTerdaftarsRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collPtkTerdaftarsRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPtkTerdaftarsRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPtkTerdaftarsRelatedByPtkId($v = true)
    {
        $this->collPtkTerdaftarsRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collPtkTerdaftarsRelatedByPtkId collection.
     *
     * By default this just sets the collPtkTerdaftarsRelatedByPtkId collection to an empty array (like clearcollPtkTerdaftarsRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPtkTerdaftarsRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collPtkTerdaftarsRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collPtkTerdaftarsRelatedByPtkId = new PropelObjectCollection();
        $this->collPtkTerdaftarsRelatedByPtkId->setModel('PtkTerdaftar');
    }

    /**
     * Gets an array of PtkTerdaftar objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     * @throws PropelException
     */
    public function getPtkTerdaftarsRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPtkTerdaftarsRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collPtkTerdaftarsRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPtkTerdaftarsRelatedByPtkId) {
                // return empty collection
                $this->initPtkTerdaftarsRelatedByPtkId();
            } else {
                $collPtkTerdaftarsRelatedByPtkId = PtkTerdaftarQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPtkTerdaftarsRelatedByPtkIdPartial && count($collPtkTerdaftarsRelatedByPtkId)) {
                      $this->initPtkTerdaftarsRelatedByPtkId(false);

                      foreach($collPtkTerdaftarsRelatedByPtkId as $obj) {
                        if (false == $this->collPtkTerdaftarsRelatedByPtkId->contains($obj)) {
                          $this->collPtkTerdaftarsRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collPtkTerdaftarsRelatedByPtkIdPartial = true;
                    }

                    $collPtkTerdaftarsRelatedByPtkId->getInternalIterator()->rewind();
                    return $collPtkTerdaftarsRelatedByPtkId;
                }

                if($partial && $this->collPtkTerdaftarsRelatedByPtkId) {
                    foreach($this->collPtkTerdaftarsRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collPtkTerdaftarsRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collPtkTerdaftarsRelatedByPtkId = $collPtkTerdaftarsRelatedByPtkId;
                $this->collPtkTerdaftarsRelatedByPtkIdPartial = false;
            }
        }

        return $this->collPtkTerdaftarsRelatedByPtkId;
    }

    /**
     * Sets a collection of PtkTerdaftarRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $ptkTerdaftarsRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setPtkTerdaftarsRelatedByPtkId(PropelCollection $ptkTerdaftarsRelatedByPtkId, PropelPDO $con = null)
    {
        $ptkTerdaftarsRelatedByPtkIdToDelete = $this->getPtkTerdaftarsRelatedByPtkId(new Criteria(), $con)->diff($ptkTerdaftarsRelatedByPtkId);

        $this->ptkTerdaftarsRelatedByPtkIdScheduledForDeletion = unserialize(serialize($ptkTerdaftarsRelatedByPtkIdToDelete));

        foreach ($ptkTerdaftarsRelatedByPtkIdToDelete as $ptkTerdaftarRelatedByPtkIdRemoved) {
            $ptkTerdaftarRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collPtkTerdaftarsRelatedByPtkId = null;
        foreach ($ptkTerdaftarsRelatedByPtkId as $ptkTerdaftarRelatedByPtkId) {
            $this->addPtkTerdaftarRelatedByPtkId($ptkTerdaftarRelatedByPtkId);
        }

        $this->collPtkTerdaftarsRelatedByPtkId = $ptkTerdaftarsRelatedByPtkId;
        $this->collPtkTerdaftarsRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PtkTerdaftar objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related PtkTerdaftar objects.
     * @throws PropelException
     */
    public function countPtkTerdaftarsRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPtkTerdaftarsRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collPtkTerdaftarsRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPtkTerdaftarsRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPtkTerdaftarsRelatedByPtkId());
            }
            $query = PtkTerdaftarQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collPtkTerdaftarsRelatedByPtkId);
    }

    /**
     * Method called to associate a PtkTerdaftar object to this object
     * through the PtkTerdaftar foreign key attribute.
     *
     * @param    PtkTerdaftar $l PtkTerdaftar
     * @return Ptk The current object (for fluent API support)
     */
    public function addPtkTerdaftarRelatedByPtkId(PtkTerdaftar $l)
    {
        if ($this->collPtkTerdaftarsRelatedByPtkId === null) {
            $this->initPtkTerdaftarsRelatedByPtkId();
            $this->collPtkTerdaftarsRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collPtkTerdaftarsRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPtkTerdaftarRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	PtkTerdaftarRelatedByPtkId $ptkTerdaftarRelatedByPtkId The ptkTerdaftarRelatedByPtkId object to add.
     */
    protected function doAddPtkTerdaftarRelatedByPtkId($ptkTerdaftarRelatedByPtkId)
    {
        $this->collPtkTerdaftarsRelatedByPtkId[]= $ptkTerdaftarRelatedByPtkId;
        $ptkTerdaftarRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	PtkTerdaftarRelatedByPtkId $ptkTerdaftarRelatedByPtkId The ptkTerdaftarRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removePtkTerdaftarRelatedByPtkId($ptkTerdaftarRelatedByPtkId)
    {
        if ($this->getPtkTerdaftarsRelatedByPtkId()->contains($ptkTerdaftarRelatedByPtkId)) {
            $this->collPtkTerdaftarsRelatedByPtkId->remove($this->collPtkTerdaftarsRelatedByPtkId->search($ptkTerdaftarRelatedByPtkId));
            if (null === $this->ptkTerdaftarsRelatedByPtkIdScheduledForDeletion) {
                $this->ptkTerdaftarsRelatedByPtkIdScheduledForDeletion = clone $this->collPtkTerdaftarsRelatedByPtkId;
                $this->ptkTerdaftarsRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->ptkTerdaftarsRelatedByPtkIdScheduledForDeletion[]= clone $ptkTerdaftarRelatedByPtkId;
            $ptkTerdaftarRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PtkTerdaftarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     */
    public function getPtkTerdaftarsRelatedByPtkIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkTerdaftarQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPtkTerdaftarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PtkTerdaftarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     */
    public function getPtkTerdaftarsRelatedByPtkIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkTerdaftarQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPtkTerdaftarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PtkTerdaftarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     */
    public function getPtkTerdaftarsRelatedByPtkIdJoinJenisKeluarRelatedByJenisKeluarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkTerdaftarQuery::create(null, $criteria);
        $query->joinWith('JenisKeluarRelatedByJenisKeluarId', $join_behavior);

        return $this->getPtkTerdaftarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PtkTerdaftarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     */
    public function getPtkTerdaftarsRelatedByPtkIdJoinJenisKeluarRelatedByJenisKeluarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkTerdaftarQuery::create(null, $criteria);
        $query->joinWith('JenisKeluarRelatedByJenisKeluarId', $join_behavior);

        return $this->getPtkTerdaftarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PtkTerdaftarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     */
    public function getPtkTerdaftarsRelatedByPtkIdJoinTahunAjaranRelatedByTahunAjaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkTerdaftarQuery::create(null, $criteria);
        $query->joinWith('TahunAjaranRelatedByTahunAjaranId', $join_behavior);

        return $this->getPtkTerdaftarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PtkTerdaftarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkTerdaftar[] List of PtkTerdaftar objects
     */
    public function getPtkTerdaftarsRelatedByPtkIdJoinTahunAjaranRelatedByTahunAjaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkTerdaftarQuery::create(null, $criteria);
        $query->joinWith('TahunAjaranRelatedByTahunAjaranId', $join_behavior);

        return $this->getPtkTerdaftarsRelatedByPtkId($query, $con);
    }

    /**
     * Clears out the collKaryaTulissRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addKaryaTulissRelatedByPtkId()
     */
    public function clearKaryaTulissRelatedByPtkId()
    {
        $this->collKaryaTulissRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collKaryaTulissRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collKaryaTulissRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialKaryaTulissRelatedByPtkId($v = true)
    {
        $this->collKaryaTulissRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collKaryaTulissRelatedByPtkId collection.
     *
     * By default this just sets the collKaryaTulissRelatedByPtkId collection to an empty array (like clearcollKaryaTulissRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initKaryaTulissRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collKaryaTulissRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collKaryaTulissRelatedByPtkId = new PropelObjectCollection();
        $this->collKaryaTulissRelatedByPtkId->setModel('KaryaTulis');
    }

    /**
     * Gets an array of KaryaTulis objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|KaryaTulis[] List of KaryaTulis objects
     * @throws PropelException
     */
    public function getKaryaTulissRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collKaryaTulissRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collKaryaTulissRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collKaryaTulissRelatedByPtkId) {
                // return empty collection
                $this->initKaryaTulissRelatedByPtkId();
            } else {
                $collKaryaTulissRelatedByPtkId = KaryaTulisQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collKaryaTulissRelatedByPtkIdPartial && count($collKaryaTulissRelatedByPtkId)) {
                      $this->initKaryaTulissRelatedByPtkId(false);

                      foreach($collKaryaTulissRelatedByPtkId as $obj) {
                        if (false == $this->collKaryaTulissRelatedByPtkId->contains($obj)) {
                          $this->collKaryaTulissRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collKaryaTulissRelatedByPtkIdPartial = true;
                    }

                    $collKaryaTulissRelatedByPtkId->getInternalIterator()->rewind();
                    return $collKaryaTulissRelatedByPtkId;
                }

                if($partial && $this->collKaryaTulissRelatedByPtkId) {
                    foreach($this->collKaryaTulissRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collKaryaTulissRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collKaryaTulissRelatedByPtkId = $collKaryaTulissRelatedByPtkId;
                $this->collKaryaTulissRelatedByPtkIdPartial = false;
            }
        }

        return $this->collKaryaTulissRelatedByPtkId;
    }

    /**
     * Sets a collection of KaryaTulisRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $karyaTulissRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setKaryaTulissRelatedByPtkId(PropelCollection $karyaTulissRelatedByPtkId, PropelPDO $con = null)
    {
        $karyaTulissRelatedByPtkIdToDelete = $this->getKaryaTulissRelatedByPtkId(new Criteria(), $con)->diff($karyaTulissRelatedByPtkId);

        $this->karyaTulissRelatedByPtkIdScheduledForDeletion = unserialize(serialize($karyaTulissRelatedByPtkIdToDelete));

        foreach ($karyaTulissRelatedByPtkIdToDelete as $karyaTulisRelatedByPtkIdRemoved) {
            $karyaTulisRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collKaryaTulissRelatedByPtkId = null;
        foreach ($karyaTulissRelatedByPtkId as $karyaTulisRelatedByPtkId) {
            $this->addKaryaTulisRelatedByPtkId($karyaTulisRelatedByPtkId);
        }

        $this->collKaryaTulissRelatedByPtkId = $karyaTulissRelatedByPtkId;
        $this->collKaryaTulissRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related KaryaTulis objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related KaryaTulis objects.
     * @throws PropelException
     */
    public function countKaryaTulissRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collKaryaTulissRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collKaryaTulissRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collKaryaTulissRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getKaryaTulissRelatedByPtkId());
            }
            $query = KaryaTulisQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collKaryaTulissRelatedByPtkId);
    }

    /**
     * Method called to associate a KaryaTulis object to this object
     * through the KaryaTulis foreign key attribute.
     *
     * @param    KaryaTulis $l KaryaTulis
     * @return Ptk The current object (for fluent API support)
     */
    public function addKaryaTulisRelatedByPtkId(KaryaTulis $l)
    {
        if ($this->collKaryaTulissRelatedByPtkId === null) {
            $this->initKaryaTulissRelatedByPtkId();
            $this->collKaryaTulissRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collKaryaTulissRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddKaryaTulisRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	KaryaTulisRelatedByPtkId $karyaTulisRelatedByPtkId The karyaTulisRelatedByPtkId object to add.
     */
    protected function doAddKaryaTulisRelatedByPtkId($karyaTulisRelatedByPtkId)
    {
        $this->collKaryaTulissRelatedByPtkId[]= $karyaTulisRelatedByPtkId;
        $karyaTulisRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	KaryaTulisRelatedByPtkId $karyaTulisRelatedByPtkId The karyaTulisRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removeKaryaTulisRelatedByPtkId($karyaTulisRelatedByPtkId)
    {
        if ($this->getKaryaTulissRelatedByPtkId()->contains($karyaTulisRelatedByPtkId)) {
            $this->collKaryaTulissRelatedByPtkId->remove($this->collKaryaTulissRelatedByPtkId->search($karyaTulisRelatedByPtkId));
            if (null === $this->karyaTulissRelatedByPtkIdScheduledForDeletion) {
                $this->karyaTulissRelatedByPtkIdScheduledForDeletion = clone $this->collKaryaTulissRelatedByPtkId;
                $this->karyaTulissRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->karyaTulissRelatedByPtkIdScheduledForDeletion[]= clone $karyaTulisRelatedByPtkId;
            $karyaTulisRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }

    /**
     * Clears out the collKaryaTulissRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addKaryaTulissRelatedByPtkId()
     */
    public function clearKaryaTulissRelatedByPtkId()
    {
        $this->collKaryaTulissRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collKaryaTulissRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collKaryaTulissRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialKaryaTulissRelatedByPtkId($v = true)
    {
        $this->collKaryaTulissRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collKaryaTulissRelatedByPtkId collection.
     *
     * By default this just sets the collKaryaTulissRelatedByPtkId collection to an empty array (like clearcollKaryaTulissRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initKaryaTulissRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collKaryaTulissRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collKaryaTulissRelatedByPtkId = new PropelObjectCollection();
        $this->collKaryaTulissRelatedByPtkId->setModel('KaryaTulis');
    }

    /**
     * Gets an array of KaryaTulis objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|KaryaTulis[] List of KaryaTulis objects
     * @throws PropelException
     */
    public function getKaryaTulissRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collKaryaTulissRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collKaryaTulissRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collKaryaTulissRelatedByPtkId) {
                // return empty collection
                $this->initKaryaTulissRelatedByPtkId();
            } else {
                $collKaryaTulissRelatedByPtkId = KaryaTulisQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collKaryaTulissRelatedByPtkIdPartial && count($collKaryaTulissRelatedByPtkId)) {
                      $this->initKaryaTulissRelatedByPtkId(false);

                      foreach($collKaryaTulissRelatedByPtkId as $obj) {
                        if (false == $this->collKaryaTulissRelatedByPtkId->contains($obj)) {
                          $this->collKaryaTulissRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collKaryaTulissRelatedByPtkIdPartial = true;
                    }

                    $collKaryaTulissRelatedByPtkId->getInternalIterator()->rewind();
                    return $collKaryaTulissRelatedByPtkId;
                }

                if($partial && $this->collKaryaTulissRelatedByPtkId) {
                    foreach($this->collKaryaTulissRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collKaryaTulissRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collKaryaTulissRelatedByPtkId = $collKaryaTulissRelatedByPtkId;
                $this->collKaryaTulissRelatedByPtkIdPartial = false;
            }
        }

        return $this->collKaryaTulissRelatedByPtkId;
    }

    /**
     * Sets a collection of KaryaTulisRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $karyaTulissRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setKaryaTulissRelatedByPtkId(PropelCollection $karyaTulissRelatedByPtkId, PropelPDO $con = null)
    {
        $karyaTulissRelatedByPtkIdToDelete = $this->getKaryaTulissRelatedByPtkId(new Criteria(), $con)->diff($karyaTulissRelatedByPtkId);

        $this->karyaTulissRelatedByPtkIdScheduledForDeletion = unserialize(serialize($karyaTulissRelatedByPtkIdToDelete));

        foreach ($karyaTulissRelatedByPtkIdToDelete as $karyaTulisRelatedByPtkIdRemoved) {
            $karyaTulisRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collKaryaTulissRelatedByPtkId = null;
        foreach ($karyaTulissRelatedByPtkId as $karyaTulisRelatedByPtkId) {
            $this->addKaryaTulisRelatedByPtkId($karyaTulisRelatedByPtkId);
        }

        $this->collKaryaTulissRelatedByPtkId = $karyaTulissRelatedByPtkId;
        $this->collKaryaTulissRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related KaryaTulis objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related KaryaTulis objects.
     * @throws PropelException
     */
    public function countKaryaTulissRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collKaryaTulissRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collKaryaTulissRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collKaryaTulissRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getKaryaTulissRelatedByPtkId());
            }
            $query = KaryaTulisQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collKaryaTulissRelatedByPtkId);
    }

    /**
     * Method called to associate a KaryaTulis object to this object
     * through the KaryaTulis foreign key attribute.
     *
     * @param    KaryaTulis $l KaryaTulis
     * @return Ptk The current object (for fluent API support)
     */
    public function addKaryaTulisRelatedByPtkId(KaryaTulis $l)
    {
        if ($this->collKaryaTulissRelatedByPtkId === null) {
            $this->initKaryaTulissRelatedByPtkId();
            $this->collKaryaTulissRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collKaryaTulissRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddKaryaTulisRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	KaryaTulisRelatedByPtkId $karyaTulisRelatedByPtkId The karyaTulisRelatedByPtkId object to add.
     */
    protected function doAddKaryaTulisRelatedByPtkId($karyaTulisRelatedByPtkId)
    {
        $this->collKaryaTulissRelatedByPtkId[]= $karyaTulisRelatedByPtkId;
        $karyaTulisRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	KaryaTulisRelatedByPtkId $karyaTulisRelatedByPtkId The karyaTulisRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removeKaryaTulisRelatedByPtkId($karyaTulisRelatedByPtkId)
    {
        if ($this->getKaryaTulissRelatedByPtkId()->contains($karyaTulisRelatedByPtkId)) {
            $this->collKaryaTulissRelatedByPtkId->remove($this->collKaryaTulissRelatedByPtkId->search($karyaTulisRelatedByPtkId));
            if (null === $this->karyaTulissRelatedByPtkIdScheduledForDeletion) {
                $this->karyaTulissRelatedByPtkIdScheduledForDeletion = clone $this->collKaryaTulissRelatedByPtkId;
                $this->karyaTulissRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->karyaTulissRelatedByPtkIdScheduledForDeletion[]= clone $karyaTulisRelatedByPtkId;
            $karyaTulisRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }

    /**
     * Clears out the collNilaiTestsRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addNilaiTestsRelatedByPtkId()
     */
    public function clearNilaiTestsRelatedByPtkId()
    {
        $this->collNilaiTestsRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collNilaiTestsRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collNilaiTestsRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialNilaiTestsRelatedByPtkId($v = true)
    {
        $this->collNilaiTestsRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collNilaiTestsRelatedByPtkId collection.
     *
     * By default this just sets the collNilaiTestsRelatedByPtkId collection to an empty array (like clearcollNilaiTestsRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initNilaiTestsRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collNilaiTestsRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collNilaiTestsRelatedByPtkId = new PropelObjectCollection();
        $this->collNilaiTestsRelatedByPtkId->setModel('NilaiTest');
    }

    /**
     * Gets an array of NilaiTest objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|NilaiTest[] List of NilaiTest objects
     * @throws PropelException
     */
    public function getNilaiTestsRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collNilaiTestsRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collNilaiTestsRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collNilaiTestsRelatedByPtkId) {
                // return empty collection
                $this->initNilaiTestsRelatedByPtkId();
            } else {
                $collNilaiTestsRelatedByPtkId = NilaiTestQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collNilaiTestsRelatedByPtkIdPartial && count($collNilaiTestsRelatedByPtkId)) {
                      $this->initNilaiTestsRelatedByPtkId(false);

                      foreach($collNilaiTestsRelatedByPtkId as $obj) {
                        if (false == $this->collNilaiTestsRelatedByPtkId->contains($obj)) {
                          $this->collNilaiTestsRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collNilaiTestsRelatedByPtkIdPartial = true;
                    }

                    $collNilaiTestsRelatedByPtkId->getInternalIterator()->rewind();
                    return $collNilaiTestsRelatedByPtkId;
                }

                if($partial && $this->collNilaiTestsRelatedByPtkId) {
                    foreach($this->collNilaiTestsRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collNilaiTestsRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collNilaiTestsRelatedByPtkId = $collNilaiTestsRelatedByPtkId;
                $this->collNilaiTestsRelatedByPtkIdPartial = false;
            }
        }

        return $this->collNilaiTestsRelatedByPtkId;
    }

    /**
     * Sets a collection of NilaiTestRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $nilaiTestsRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setNilaiTestsRelatedByPtkId(PropelCollection $nilaiTestsRelatedByPtkId, PropelPDO $con = null)
    {
        $nilaiTestsRelatedByPtkIdToDelete = $this->getNilaiTestsRelatedByPtkId(new Criteria(), $con)->diff($nilaiTestsRelatedByPtkId);

        $this->nilaiTestsRelatedByPtkIdScheduledForDeletion = unserialize(serialize($nilaiTestsRelatedByPtkIdToDelete));

        foreach ($nilaiTestsRelatedByPtkIdToDelete as $nilaiTestRelatedByPtkIdRemoved) {
            $nilaiTestRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collNilaiTestsRelatedByPtkId = null;
        foreach ($nilaiTestsRelatedByPtkId as $nilaiTestRelatedByPtkId) {
            $this->addNilaiTestRelatedByPtkId($nilaiTestRelatedByPtkId);
        }

        $this->collNilaiTestsRelatedByPtkId = $nilaiTestsRelatedByPtkId;
        $this->collNilaiTestsRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related NilaiTest objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related NilaiTest objects.
     * @throws PropelException
     */
    public function countNilaiTestsRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collNilaiTestsRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collNilaiTestsRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collNilaiTestsRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getNilaiTestsRelatedByPtkId());
            }
            $query = NilaiTestQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collNilaiTestsRelatedByPtkId);
    }

    /**
     * Method called to associate a NilaiTest object to this object
     * through the NilaiTest foreign key attribute.
     *
     * @param    NilaiTest $l NilaiTest
     * @return Ptk The current object (for fluent API support)
     */
    public function addNilaiTestRelatedByPtkId(NilaiTest $l)
    {
        if ($this->collNilaiTestsRelatedByPtkId === null) {
            $this->initNilaiTestsRelatedByPtkId();
            $this->collNilaiTestsRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collNilaiTestsRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddNilaiTestRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	NilaiTestRelatedByPtkId $nilaiTestRelatedByPtkId The nilaiTestRelatedByPtkId object to add.
     */
    protected function doAddNilaiTestRelatedByPtkId($nilaiTestRelatedByPtkId)
    {
        $this->collNilaiTestsRelatedByPtkId[]= $nilaiTestRelatedByPtkId;
        $nilaiTestRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	NilaiTestRelatedByPtkId $nilaiTestRelatedByPtkId The nilaiTestRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removeNilaiTestRelatedByPtkId($nilaiTestRelatedByPtkId)
    {
        if ($this->getNilaiTestsRelatedByPtkId()->contains($nilaiTestRelatedByPtkId)) {
            $this->collNilaiTestsRelatedByPtkId->remove($this->collNilaiTestsRelatedByPtkId->search($nilaiTestRelatedByPtkId));
            if (null === $this->nilaiTestsRelatedByPtkIdScheduledForDeletion) {
                $this->nilaiTestsRelatedByPtkIdScheduledForDeletion = clone $this->collNilaiTestsRelatedByPtkId;
                $this->nilaiTestsRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->nilaiTestsRelatedByPtkIdScheduledForDeletion[]= clone $nilaiTestRelatedByPtkId;
            $nilaiTestRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related NilaiTestsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|NilaiTest[] List of NilaiTest objects
     */
    public function getNilaiTestsRelatedByPtkIdJoinJenisTestRelatedByJenisTestId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = NilaiTestQuery::create(null, $criteria);
        $query->joinWith('JenisTestRelatedByJenisTestId', $join_behavior);

        return $this->getNilaiTestsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related NilaiTestsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|NilaiTest[] List of NilaiTest objects
     */
    public function getNilaiTestsRelatedByPtkIdJoinJenisTestRelatedByJenisTestId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = NilaiTestQuery::create(null, $criteria);
        $query->joinWith('JenisTestRelatedByJenisTestId', $join_behavior);

        return $this->getNilaiTestsRelatedByPtkId($query, $con);
    }

    /**
     * Clears out the collNilaiTestsRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addNilaiTestsRelatedByPtkId()
     */
    public function clearNilaiTestsRelatedByPtkId()
    {
        $this->collNilaiTestsRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collNilaiTestsRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collNilaiTestsRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialNilaiTestsRelatedByPtkId($v = true)
    {
        $this->collNilaiTestsRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collNilaiTestsRelatedByPtkId collection.
     *
     * By default this just sets the collNilaiTestsRelatedByPtkId collection to an empty array (like clearcollNilaiTestsRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initNilaiTestsRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collNilaiTestsRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collNilaiTestsRelatedByPtkId = new PropelObjectCollection();
        $this->collNilaiTestsRelatedByPtkId->setModel('NilaiTest');
    }

    /**
     * Gets an array of NilaiTest objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|NilaiTest[] List of NilaiTest objects
     * @throws PropelException
     */
    public function getNilaiTestsRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collNilaiTestsRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collNilaiTestsRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collNilaiTestsRelatedByPtkId) {
                // return empty collection
                $this->initNilaiTestsRelatedByPtkId();
            } else {
                $collNilaiTestsRelatedByPtkId = NilaiTestQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collNilaiTestsRelatedByPtkIdPartial && count($collNilaiTestsRelatedByPtkId)) {
                      $this->initNilaiTestsRelatedByPtkId(false);

                      foreach($collNilaiTestsRelatedByPtkId as $obj) {
                        if (false == $this->collNilaiTestsRelatedByPtkId->contains($obj)) {
                          $this->collNilaiTestsRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collNilaiTestsRelatedByPtkIdPartial = true;
                    }

                    $collNilaiTestsRelatedByPtkId->getInternalIterator()->rewind();
                    return $collNilaiTestsRelatedByPtkId;
                }

                if($partial && $this->collNilaiTestsRelatedByPtkId) {
                    foreach($this->collNilaiTestsRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collNilaiTestsRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collNilaiTestsRelatedByPtkId = $collNilaiTestsRelatedByPtkId;
                $this->collNilaiTestsRelatedByPtkIdPartial = false;
            }
        }

        return $this->collNilaiTestsRelatedByPtkId;
    }

    /**
     * Sets a collection of NilaiTestRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $nilaiTestsRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setNilaiTestsRelatedByPtkId(PropelCollection $nilaiTestsRelatedByPtkId, PropelPDO $con = null)
    {
        $nilaiTestsRelatedByPtkIdToDelete = $this->getNilaiTestsRelatedByPtkId(new Criteria(), $con)->diff($nilaiTestsRelatedByPtkId);

        $this->nilaiTestsRelatedByPtkIdScheduledForDeletion = unserialize(serialize($nilaiTestsRelatedByPtkIdToDelete));

        foreach ($nilaiTestsRelatedByPtkIdToDelete as $nilaiTestRelatedByPtkIdRemoved) {
            $nilaiTestRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collNilaiTestsRelatedByPtkId = null;
        foreach ($nilaiTestsRelatedByPtkId as $nilaiTestRelatedByPtkId) {
            $this->addNilaiTestRelatedByPtkId($nilaiTestRelatedByPtkId);
        }

        $this->collNilaiTestsRelatedByPtkId = $nilaiTestsRelatedByPtkId;
        $this->collNilaiTestsRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related NilaiTest objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related NilaiTest objects.
     * @throws PropelException
     */
    public function countNilaiTestsRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collNilaiTestsRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collNilaiTestsRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collNilaiTestsRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getNilaiTestsRelatedByPtkId());
            }
            $query = NilaiTestQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collNilaiTestsRelatedByPtkId);
    }

    /**
     * Method called to associate a NilaiTest object to this object
     * through the NilaiTest foreign key attribute.
     *
     * @param    NilaiTest $l NilaiTest
     * @return Ptk The current object (for fluent API support)
     */
    public function addNilaiTestRelatedByPtkId(NilaiTest $l)
    {
        if ($this->collNilaiTestsRelatedByPtkId === null) {
            $this->initNilaiTestsRelatedByPtkId();
            $this->collNilaiTestsRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collNilaiTestsRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddNilaiTestRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	NilaiTestRelatedByPtkId $nilaiTestRelatedByPtkId The nilaiTestRelatedByPtkId object to add.
     */
    protected function doAddNilaiTestRelatedByPtkId($nilaiTestRelatedByPtkId)
    {
        $this->collNilaiTestsRelatedByPtkId[]= $nilaiTestRelatedByPtkId;
        $nilaiTestRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	NilaiTestRelatedByPtkId $nilaiTestRelatedByPtkId The nilaiTestRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removeNilaiTestRelatedByPtkId($nilaiTestRelatedByPtkId)
    {
        if ($this->getNilaiTestsRelatedByPtkId()->contains($nilaiTestRelatedByPtkId)) {
            $this->collNilaiTestsRelatedByPtkId->remove($this->collNilaiTestsRelatedByPtkId->search($nilaiTestRelatedByPtkId));
            if (null === $this->nilaiTestsRelatedByPtkIdScheduledForDeletion) {
                $this->nilaiTestsRelatedByPtkIdScheduledForDeletion = clone $this->collNilaiTestsRelatedByPtkId;
                $this->nilaiTestsRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->nilaiTestsRelatedByPtkIdScheduledForDeletion[]= clone $nilaiTestRelatedByPtkId;
            $nilaiTestRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related NilaiTestsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|NilaiTest[] List of NilaiTest objects
     */
    public function getNilaiTestsRelatedByPtkIdJoinJenisTestRelatedByJenisTestId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = NilaiTestQuery::create(null, $criteria);
        $query->joinWith('JenisTestRelatedByJenisTestId', $join_behavior);

        return $this->getNilaiTestsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related NilaiTestsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|NilaiTest[] List of NilaiTest objects
     */
    public function getNilaiTestsRelatedByPtkIdJoinJenisTestRelatedByJenisTestId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = NilaiTestQuery::create(null, $criteria);
        $query->joinWith('JenisTestRelatedByJenisTestId', $join_behavior);

        return $this->getNilaiTestsRelatedByPtkId($query, $con);
    }

    /**
     * Clears out the collBukuPtksRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addBukuPtksRelatedByPtkId()
     */
    public function clearBukuPtksRelatedByPtkId()
    {
        $this->collBukuPtksRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collBukuPtksRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collBukuPtksRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialBukuPtksRelatedByPtkId($v = true)
    {
        $this->collBukuPtksRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collBukuPtksRelatedByPtkId collection.
     *
     * By default this just sets the collBukuPtksRelatedByPtkId collection to an empty array (like clearcollBukuPtksRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBukuPtksRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collBukuPtksRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collBukuPtksRelatedByPtkId = new PropelObjectCollection();
        $this->collBukuPtksRelatedByPtkId->setModel('BukuPtk');
    }

    /**
     * Gets an array of BukuPtk objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|BukuPtk[] List of BukuPtk objects
     * @throws PropelException
     */
    public function getBukuPtksRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collBukuPtksRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collBukuPtksRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBukuPtksRelatedByPtkId) {
                // return empty collection
                $this->initBukuPtksRelatedByPtkId();
            } else {
                $collBukuPtksRelatedByPtkId = BukuPtkQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collBukuPtksRelatedByPtkIdPartial && count($collBukuPtksRelatedByPtkId)) {
                      $this->initBukuPtksRelatedByPtkId(false);

                      foreach($collBukuPtksRelatedByPtkId as $obj) {
                        if (false == $this->collBukuPtksRelatedByPtkId->contains($obj)) {
                          $this->collBukuPtksRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collBukuPtksRelatedByPtkIdPartial = true;
                    }

                    $collBukuPtksRelatedByPtkId->getInternalIterator()->rewind();
                    return $collBukuPtksRelatedByPtkId;
                }

                if($partial && $this->collBukuPtksRelatedByPtkId) {
                    foreach($this->collBukuPtksRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collBukuPtksRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collBukuPtksRelatedByPtkId = $collBukuPtksRelatedByPtkId;
                $this->collBukuPtksRelatedByPtkIdPartial = false;
            }
        }

        return $this->collBukuPtksRelatedByPtkId;
    }

    /**
     * Sets a collection of BukuPtkRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $bukuPtksRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setBukuPtksRelatedByPtkId(PropelCollection $bukuPtksRelatedByPtkId, PropelPDO $con = null)
    {
        $bukuPtksRelatedByPtkIdToDelete = $this->getBukuPtksRelatedByPtkId(new Criteria(), $con)->diff($bukuPtksRelatedByPtkId);

        $this->bukuPtksRelatedByPtkIdScheduledForDeletion = unserialize(serialize($bukuPtksRelatedByPtkIdToDelete));

        foreach ($bukuPtksRelatedByPtkIdToDelete as $bukuPtkRelatedByPtkIdRemoved) {
            $bukuPtkRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collBukuPtksRelatedByPtkId = null;
        foreach ($bukuPtksRelatedByPtkId as $bukuPtkRelatedByPtkId) {
            $this->addBukuPtkRelatedByPtkId($bukuPtkRelatedByPtkId);
        }

        $this->collBukuPtksRelatedByPtkId = $bukuPtksRelatedByPtkId;
        $this->collBukuPtksRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BukuPtk objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related BukuPtk objects.
     * @throws PropelException
     */
    public function countBukuPtksRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collBukuPtksRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collBukuPtksRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBukuPtksRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getBukuPtksRelatedByPtkId());
            }
            $query = BukuPtkQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collBukuPtksRelatedByPtkId);
    }

    /**
     * Method called to associate a BukuPtk object to this object
     * through the BukuPtk foreign key attribute.
     *
     * @param    BukuPtk $l BukuPtk
     * @return Ptk The current object (for fluent API support)
     */
    public function addBukuPtkRelatedByPtkId(BukuPtk $l)
    {
        if ($this->collBukuPtksRelatedByPtkId === null) {
            $this->initBukuPtksRelatedByPtkId();
            $this->collBukuPtksRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collBukuPtksRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddBukuPtkRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	BukuPtkRelatedByPtkId $bukuPtkRelatedByPtkId The bukuPtkRelatedByPtkId object to add.
     */
    protected function doAddBukuPtkRelatedByPtkId($bukuPtkRelatedByPtkId)
    {
        $this->collBukuPtksRelatedByPtkId[]= $bukuPtkRelatedByPtkId;
        $bukuPtkRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	BukuPtkRelatedByPtkId $bukuPtkRelatedByPtkId The bukuPtkRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removeBukuPtkRelatedByPtkId($bukuPtkRelatedByPtkId)
    {
        if ($this->getBukuPtksRelatedByPtkId()->contains($bukuPtkRelatedByPtkId)) {
            $this->collBukuPtksRelatedByPtkId->remove($this->collBukuPtksRelatedByPtkId->search($bukuPtkRelatedByPtkId));
            if (null === $this->bukuPtksRelatedByPtkIdScheduledForDeletion) {
                $this->bukuPtksRelatedByPtkIdScheduledForDeletion = clone $this->collBukuPtksRelatedByPtkId;
                $this->bukuPtksRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->bukuPtksRelatedByPtkIdScheduledForDeletion[]= clone $bukuPtkRelatedByPtkId;
            $bukuPtkRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }

    /**
     * Clears out the collBukuPtksRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addBukuPtksRelatedByPtkId()
     */
    public function clearBukuPtksRelatedByPtkId()
    {
        $this->collBukuPtksRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collBukuPtksRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collBukuPtksRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialBukuPtksRelatedByPtkId($v = true)
    {
        $this->collBukuPtksRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collBukuPtksRelatedByPtkId collection.
     *
     * By default this just sets the collBukuPtksRelatedByPtkId collection to an empty array (like clearcollBukuPtksRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBukuPtksRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collBukuPtksRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collBukuPtksRelatedByPtkId = new PropelObjectCollection();
        $this->collBukuPtksRelatedByPtkId->setModel('BukuPtk');
    }

    /**
     * Gets an array of BukuPtk objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|BukuPtk[] List of BukuPtk objects
     * @throws PropelException
     */
    public function getBukuPtksRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collBukuPtksRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collBukuPtksRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBukuPtksRelatedByPtkId) {
                // return empty collection
                $this->initBukuPtksRelatedByPtkId();
            } else {
                $collBukuPtksRelatedByPtkId = BukuPtkQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collBukuPtksRelatedByPtkIdPartial && count($collBukuPtksRelatedByPtkId)) {
                      $this->initBukuPtksRelatedByPtkId(false);

                      foreach($collBukuPtksRelatedByPtkId as $obj) {
                        if (false == $this->collBukuPtksRelatedByPtkId->contains($obj)) {
                          $this->collBukuPtksRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collBukuPtksRelatedByPtkIdPartial = true;
                    }

                    $collBukuPtksRelatedByPtkId->getInternalIterator()->rewind();
                    return $collBukuPtksRelatedByPtkId;
                }

                if($partial && $this->collBukuPtksRelatedByPtkId) {
                    foreach($this->collBukuPtksRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collBukuPtksRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collBukuPtksRelatedByPtkId = $collBukuPtksRelatedByPtkId;
                $this->collBukuPtksRelatedByPtkIdPartial = false;
            }
        }

        return $this->collBukuPtksRelatedByPtkId;
    }

    /**
     * Sets a collection of BukuPtkRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $bukuPtksRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setBukuPtksRelatedByPtkId(PropelCollection $bukuPtksRelatedByPtkId, PropelPDO $con = null)
    {
        $bukuPtksRelatedByPtkIdToDelete = $this->getBukuPtksRelatedByPtkId(new Criteria(), $con)->diff($bukuPtksRelatedByPtkId);

        $this->bukuPtksRelatedByPtkIdScheduledForDeletion = unserialize(serialize($bukuPtksRelatedByPtkIdToDelete));

        foreach ($bukuPtksRelatedByPtkIdToDelete as $bukuPtkRelatedByPtkIdRemoved) {
            $bukuPtkRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collBukuPtksRelatedByPtkId = null;
        foreach ($bukuPtksRelatedByPtkId as $bukuPtkRelatedByPtkId) {
            $this->addBukuPtkRelatedByPtkId($bukuPtkRelatedByPtkId);
        }

        $this->collBukuPtksRelatedByPtkId = $bukuPtksRelatedByPtkId;
        $this->collBukuPtksRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BukuPtk objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related BukuPtk objects.
     * @throws PropelException
     */
    public function countBukuPtksRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collBukuPtksRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collBukuPtksRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBukuPtksRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getBukuPtksRelatedByPtkId());
            }
            $query = BukuPtkQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collBukuPtksRelatedByPtkId);
    }

    /**
     * Method called to associate a BukuPtk object to this object
     * through the BukuPtk foreign key attribute.
     *
     * @param    BukuPtk $l BukuPtk
     * @return Ptk The current object (for fluent API support)
     */
    public function addBukuPtkRelatedByPtkId(BukuPtk $l)
    {
        if ($this->collBukuPtksRelatedByPtkId === null) {
            $this->initBukuPtksRelatedByPtkId();
            $this->collBukuPtksRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collBukuPtksRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddBukuPtkRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	BukuPtkRelatedByPtkId $bukuPtkRelatedByPtkId The bukuPtkRelatedByPtkId object to add.
     */
    protected function doAddBukuPtkRelatedByPtkId($bukuPtkRelatedByPtkId)
    {
        $this->collBukuPtksRelatedByPtkId[]= $bukuPtkRelatedByPtkId;
        $bukuPtkRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	BukuPtkRelatedByPtkId $bukuPtkRelatedByPtkId The bukuPtkRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removeBukuPtkRelatedByPtkId($bukuPtkRelatedByPtkId)
    {
        if ($this->getBukuPtksRelatedByPtkId()->contains($bukuPtkRelatedByPtkId)) {
            $this->collBukuPtksRelatedByPtkId->remove($this->collBukuPtksRelatedByPtkId->search($bukuPtkRelatedByPtkId));
            if (null === $this->bukuPtksRelatedByPtkIdScheduledForDeletion) {
                $this->bukuPtksRelatedByPtkIdScheduledForDeletion = clone $this->collBukuPtksRelatedByPtkId;
                $this->bukuPtksRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->bukuPtksRelatedByPtkIdScheduledForDeletion[]= clone $bukuPtkRelatedByPtkId;
            $bukuPtkRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }

    /**
     * Clears out the collBeasiswaPtksRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addBeasiswaPtksRelatedByPtkId()
     */
    public function clearBeasiswaPtksRelatedByPtkId()
    {
        $this->collBeasiswaPtksRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collBeasiswaPtksRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collBeasiswaPtksRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialBeasiswaPtksRelatedByPtkId($v = true)
    {
        $this->collBeasiswaPtksRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collBeasiswaPtksRelatedByPtkId collection.
     *
     * By default this just sets the collBeasiswaPtksRelatedByPtkId collection to an empty array (like clearcollBeasiswaPtksRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBeasiswaPtksRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collBeasiswaPtksRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collBeasiswaPtksRelatedByPtkId = new PropelObjectCollection();
        $this->collBeasiswaPtksRelatedByPtkId->setModel('BeasiswaPtk');
    }

    /**
     * Gets an array of BeasiswaPtk objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|BeasiswaPtk[] List of BeasiswaPtk objects
     * @throws PropelException
     */
    public function getBeasiswaPtksRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collBeasiswaPtksRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collBeasiswaPtksRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBeasiswaPtksRelatedByPtkId) {
                // return empty collection
                $this->initBeasiswaPtksRelatedByPtkId();
            } else {
                $collBeasiswaPtksRelatedByPtkId = BeasiswaPtkQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collBeasiswaPtksRelatedByPtkIdPartial && count($collBeasiswaPtksRelatedByPtkId)) {
                      $this->initBeasiswaPtksRelatedByPtkId(false);

                      foreach($collBeasiswaPtksRelatedByPtkId as $obj) {
                        if (false == $this->collBeasiswaPtksRelatedByPtkId->contains($obj)) {
                          $this->collBeasiswaPtksRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collBeasiswaPtksRelatedByPtkIdPartial = true;
                    }

                    $collBeasiswaPtksRelatedByPtkId->getInternalIterator()->rewind();
                    return $collBeasiswaPtksRelatedByPtkId;
                }

                if($partial && $this->collBeasiswaPtksRelatedByPtkId) {
                    foreach($this->collBeasiswaPtksRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collBeasiswaPtksRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collBeasiswaPtksRelatedByPtkId = $collBeasiswaPtksRelatedByPtkId;
                $this->collBeasiswaPtksRelatedByPtkIdPartial = false;
            }
        }

        return $this->collBeasiswaPtksRelatedByPtkId;
    }

    /**
     * Sets a collection of BeasiswaPtkRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $beasiswaPtksRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setBeasiswaPtksRelatedByPtkId(PropelCollection $beasiswaPtksRelatedByPtkId, PropelPDO $con = null)
    {
        $beasiswaPtksRelatedByPtkIdToDelete = $this->getBeasiswaPtksRelatedByPtkId(new Criteria(), $con)->diff($beasiswaPtksRelatedByPtkId);

        $this->beasiswaPtksRelatedByPtkIdScheduledForDeletion = unserialize(serialize($beasiswaPtksRelatedByPtkIdToDelete));

        foreach ($beasiswaPtksRelatedByPtkIdToDelete as $beasiswaPtkRelatedByPtkIdRemoved) {
            $beasiswaPtkRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collBeasiswaPtksRelatedByPtkId = null;
        foreach ($beasiswaPtksRelatedByPtkId as $beasiswaPtkRelatedByPtkId) {
            $this->addBeasiswaPtkRelatedByPtkId($beasiswaPtkRelatedByPtkId);
        }

        $this->collBeasiswaPtksRelatedByPtkId = $beasiswaPtksRelatedByPtkId;
        $this->collBeasiswaPtksRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BeasiswaPtk objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related BeasiswaPtk objects.
     * @throws PropelException
     */
    public function countBeasiswaPtksRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collBeasiswaPtksRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collBeasiswaPtksRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBeasiswaPtksRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getBeasiswaPtksRelatedByPtkId());
            }
            $query = BeasiswaPtkQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collBeasiswaPtksRelatedByPtkId);
    }

    /**
     * Method called to associate a BeasiswaPtk object to this object
     * through the BeasiswaPtk foreign key attribute.
     *
     * @param    BeasiswaPtk $l BeasiswaPtk
     * @return Ptk The current object (for fluent API support)
     */
    public function addBeasiswaPtkRelatedByPtkId(BeasiswaPtk $l)
    {
        if ($this->collBeasiswaPtksRelatedByPtkId === null) {
            $this->initBeasiswaPtksRelatedByPtkId();
            $this->collBeasiswaPtksRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collBeasiswaPtksRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddBeasiswaPtkRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	BeasiswaPtkRelatedByPtkId $beasiswaPtkRelatedByPtkId The beasiswaPtkRelatedByPtkId object to add.
     */
    protected function doAddBeasiswaPtkRelatedByPtkId($beasiswaPtkRelatedByPtkId)
    {
        $this->collBeasiswaPtksRelatedByPtkId[]= $beasiswaPtkRelatedByPtkId;
        $beasiswaPtkRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	BeasiswaPtkRelatedByPtkId $beasiswaPtkRelatedByPtkId The beasiswaPtkRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removeBeasiswaPtkRelatedByPtkId($beasiswaPtkRelatedByPtkId)
    {
        if ($this->getBeasiswaPtksRelatedByPtkId()->contains($beasiswaPtkRelatedByPtkId)) {
            $this->collBeasiswaPtksRelatedByPtkId->remove($this->collBeasiswaPtksRelatedByPtkId->search($beasiswaPtkRelatedByPtkId));
            if (null === $this->beasiswaPtksRelatedByPtkIdScheduledForDeletion) {
                $this->beasiswaPtksRelatedByPtkIdScheduledForDeletion = clone $this->collBeasiswaPtksRelatedByPtkId;
                $this->beasiswaPtksRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->beasiswaPtksRelatedByPtkIdScheduledForDeletion[]= clone $beasiswaPtkRelatedByPtkId;
            $beasiswaPtkRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related BeasiswaPtksRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BeasiswaPtk[] List of BeasiswaPtk objects
     */
    public function getBeasiswaPtksRelatedByPtkIdJoinJenisBeasiswaRelatedByJenisBeasiswaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BeasiswaPtkQuery::create(null, $criteria);
        $query->joinWith('JenisBeasiswaRelatedByJenisBeasiswaId', $join_behavior);

        return $this->getBeasiswaPtksRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related BeasiswaPtksRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BeasiswaPtk[] List of BeasiswaPtk objects
     */
    public function getBeasiswaPtksRelatedByPtkIdJoinJenisBeasiswaRelatedByJenisBeasiswaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BeasiswaPtkQuery::create(null, $criteria);
        $query->joinWith('JenisBeasiswaRelatedByJenisBeasiswaId', $join_behavior);

        return $this->getBeasiswaPtksRelatedByPtkId($query, $con);
    }

    /**
     * Clears out the collBeasiswaPtksRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addBeasiswaPtksRelatedByPtkId()
     */
    public function clearBeasiswaPtksRelatedByPtkId()
    {
        $this->collBeasiswaPtksRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collBeasiswaPtksRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collBeasiswaPtksRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialBeasiswaPtksRelatedByPtkId($v = true)
    {
        $this->collBeasiswaPtksRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collBeasiswaPtksRelatedByPtkId collection.
     *
     * By default this just sets the collBeasiswaPtksRelatedByPtkId collection to an empty array (like clearcollBeasiswaPtksRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBeasiswaPtksRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collBeasiswaPtksRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collBeasiswaPtksRelatedByPtkId = new PropelObjectCollection();
        $this->collBeasiswaPtksRelatedByPtkId->setModel('BeasiswaPtk');
    }

    /**
     * Gets an array of BeasiswaPtk objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|BeasiswaPtk[] List of BeasiswaPtk objects
     * @throws PropelException
     */
    public function getBeasiswaPtksRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collBeasiswaPtksRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collBeasiswaPtksRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBeasiswaPtksRelatedByPtkId) {
                // return empty collection
                $this->initBeasiswaPtksRelatedByPtkId();
            } else {
                $collBeasiswaPtksRelatedByPtkId = BeasiswaPtkQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collBeasiswaPtksRelatedByPtkIdPartial && count($collBeasiswaPtksRelatedByPtkId)) {
                      $this->initBeasiswaPtksRelatedByPtkId(false);

                      foreach($collBeasiswaPtksRelatedByPtkId as $obj) {
                        if (false == $this->collBeasiswaPtksRelatedByPtkId->contains($obj)) {
                          $this->collBeasiswaPtksRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collBeasiswaPtksRelatedByPtkIdPartial = true;
                    }

                    $collBeasiswaPtksRelatedByPtkId->getInternalIterator()->rewind();
                    return $collBeasiswaPtksRelatedByPtkId;
                }

                if($partial && $this->collBeasiswaPtksRelatedByPtkId) {
                    foreach($this->collBeasiswaPtksRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collBeasiswaPtksRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collBeasiswaPtksRelatedByPtkId = $collBeasiswaPtksRelatedByPtkId;
                $this->collBeasiswaPtksRelatedByPtkIdPartial = false;
            }
        }

        return $this->collBeasiswaPtksRelatedByPtkId;
    }

    /**
     * Sets a collection of BeasiswaPtkRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $beasiswaPtksRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setBeasiswaPtksRelatedByPtkId(PropelCollection $beasiswaPtksRelatedByPtkId, PropelPDO $con = null)
    {
        $beasiswaPtksRelatedByPtkIdToDelete = $this->getBeasiswaPtksRelatedByPtkId(new Criteria(), $con)->diff($beasiswaPtksRelatedByPtkId);

        $this->beasiswaPtksRelatedByPtkIdScheduledForDeletion = unserialize(serialize($beasiswaPtksRelatedByPtkIdToDelete));

        foreach ($beasiswaPtksRelatedByPtkIdToDelete as $beasiswaPtkRelatedByPtkIdRemoved) {
            $beasiswaPtkRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collBeasiswaPtksRelatedByPtkId = null;
        foreach ($beasiswaPtksRelatedByPtkId as $beasiswaPtkRelatedByPtkId) {
            $this->addBeasiswaPtkRelatedByPtkId($beasiswaPtkRelatedByPtkId);
        }

        $this->collBeasiswaPtksRelatedByPtkId = $beasiswaPtksRelatedByPtkId;
        $this->collBeasiswaPtksRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BeasiswaPtk objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related BeasiswaPtk objects.
     * @throws PropelException
     */
    public function countBeasiswaPtksRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collBeasiswaPtksRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collBeasiswaPtksRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBeasiswaPtksRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getBeasiswaPtksRelatedByPtkId());
            }
            $query = BeasiswaPtkQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collBeasiswaPtksRelatedByPtkId);
    }

    /**
     * Method called to associate a BeasiswaPtk object to this object
     * through the BeasiswaPtk foreign key attribute.
     *
     * @param    BeasiswaPtk $l BeasiswaPtk
     * @return Ptk The current object (for fluent API support)
     */
    public function addBeasiswaPtkRelatedByPtkId(BeasiswaPtk $l)
    {
        if ($this->collBeasiswaPtksRelatedByPtkId === null) {
            $this->initBeasiswaPtksRelatedByPtkId();
            $this->collBeasiswaPtksRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collBeasiswaPtksRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddBeasiswaPtkRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	BeasiswaPtkRelatedByPtkId $beasiswaPtkRelatedByPtkId The beasiswaPtkRelatedByPtkId object to add.
     */
    protected function doAddBeasiswaPtkRelatedByPtkId($beasiswaPtkRelatedByPtkId)
    {
        $this->collBeasiswaPtksRelatedByPtkId[]= $beasiswaPtkRelatedByPtkId;
        $beasiswaPtkRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	BeasiswaPtkRelatedByPtkId $beasiswaPtkRelatedByPtkId The beasiswaPtkRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removeBeasiswaPtkRelatedByPtkId($beasiswaPtkRelatedByPtkId)
    {
        if ($this->getBeasiswaPtksRelatedByPtkId()->contains($beasiswaPtkRelatedByPtkId)) {
            $this->collBeasiswaPtksRelatedByPtkId->remove($this->collBeasiswaPtksRelatedByPtkId->search($beasiswaPtkRelatedByPtkId));
            if (null === $this->beasiswaPtksRelatedByPtkIdScheduledForDeletion) {
                $this->beasiswaPtksRelatedByPtkIdScheduledForDeletion = clone $this->collBeasiswaPtksRelatedByPtkId;
                $this->beasiswaPtksRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->beasiswaPtksRelatedByPtkIdScheduledForDeletion[]= clone $beasiswaPtkRelatedByPtkId;
            $beasiswaPtkRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related BeasiswaPtksRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BeasiswaPtk[] List of BeasiswaPtk objects
     */
    public function getBeasiswaPtksRelatedByPtkIdJoinJenisBeasiswaRelatedByJenisBeasiswaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BeasiswaPtkQuery::create(null, $criteria);
        $query->joinWith('JenisBeasiswaRelatedByJenisBeasiswaId', $join_behavior);

        return $this->getBeasiswaPtksRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related BeasiswaPtksRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BeasiswaPtk[] List of BeasiswaPtk objects
     */
    public function getBeasiswaPtksRelatedByPtkIdJoinJenisBeasiswaRelatedByJenisBeasiswaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BeasiswaPtkQuery::create(null, $criteria);
        $query->joinWith('JenisBeasiswaRelatedByJenisBeasiswaId', $join_behavior);

        return $this->getBeasiswaPtksRelatedByPtkId($query, $con);
    }

    /**
     * Clears out the collRwyKepangkatansRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addRwyKepangkatansRelatedByPtkId()
     */
    public function clearRwyKepangkatansRelatedByPtkId()
    {
        $this->collRwyKepangkatansRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collRwyKepangkatansRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collRwyKepangkatansRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialRwyKepangkatansRelatedByPtkId($v = true)
    {
        $this->collRwyKepangkatansRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collRwyKepangkatansRelatedByPtkId collection.
     *
     * By default this just sets the collRwyKepangkatansRelatedByPtkId collection to an empty array (like clearcollRwyKepangkatansRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRwyKepangkatansRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collRwyKepangkatansRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collRwyKepangkatansRelatedByPtkId = new PropelObjectCollection();
        $this->collRwyKepangkatansRelatedByPtkId->setModel('RwyKepangkatan');
    }

    /**
     * Gets an array of RwyKepangkatan objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RwyKepangkatan[] List of RwyKepangkatan objects
     * @throws PropelException
     */
    public function getRwyKepangkatansRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRwyKepangkatansRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collRwyKepangkatansRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRwyKepangkatansRelatedByPtkId) {
                // return empty collection
                $this->initRwyKepangkatansRelatedByPtkId();
            } else {
                $collRwyKepangkatansRelatedByPtkId = RwyKepangkatanQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRwyKepangkatansRelatedByPtkIdPartial && count($collRwyKepangkatansRelatedByPtkId)) {
                      $this->initRwyKepangkatansRelatedByPtkId(false);

                      foreach($collRwyKepangkatansRelatedByPtkId as $obj) {
                        if (false == $this->collRwyKepangkatansRelatedByPtkId->contains($obj)) {
                          $this->collRwyKepangkatansRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collRwyKepangkatansRelatedByPtkIdPartial = true;
                    }

                    $collRwyKepangkatansRelatedByPtkId->getInternalIterator()->rewind();
                    return $collRwyKepangkatansRelatedByPtkId;
                }

                if($partial && $this->collRwyKepangkatansRelatedByPtkId) {
                    foreach($this->collRwyKepangkatansRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collRwyKepangkatansRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collRwyKepangkatansRelatedByPtkId = $collRwyKepangkatansRelatedByPtkId;
                $this->collRwyKepangkatansRelatedByPtkIdPartial = false;
            }
        }

        return $this->collRwyKepangkatansRelatedByPtkId;
    }

    /**
     * Sets a collection of RwyKepangkatanRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rwyKepangkatansRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setRwyKepangkatansRelatedByPtkId(PropelCollection $rwyKepangkatansRelatedByPtkId, PropelPDO $con = null)
    {
        $rwyKepangkatansRelatedByPtkIdToDelete = $this->getRwyKepangkatansRelatedByPtkId(new Criteria(), $con)->diff($rwyKepangkatansRelatedByPtkId);

        $this->rwyKepangkatansRelatedByPtkIdScheduledForDeletion = unserialize(serialize($rwyKepangkatansRelatedByPtkIdToDelete));

        foreach ($rwyKepangkatansRelatedByPtkIdToDelete as $rwyKepangkatanRelatedByPtkIdRemoved) {
            $rwyKepangkatanRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collRwyKepangkatansRelatedByPtkId = null;
        foreach ($rwyKepangkatansRelatedByPtkId as $rwyKepangkatanRelatedByPtkId) {
            $this->addRwyKepangkatanRelatedByPtkId($rwyKepangkatanRelatedByPtkId);
        }

        $this->collRwyKepangkatansRelatedByPtkId = $rwyKepangkatansRelatedByPtkId;
        $this->collRwyKepangkatansRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RwyKepangkatan objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RwyKepangkatan objects.
     * @throws PropelException
     */
    public function countRwyKepangkatansRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRwyKepangkatansRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collRwyKepangkatansRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRwyKepangkatansRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getRwyKepangkatansRelatedByPtkId());
            }
            $query = RwyKepangkatanQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collRwyKepangkatansRelatedByPtkId);
    }

    /**
     * Method called to associate a RwyKepangkatan object to this object
     * through the RwyKepangkatan foreign key attribute.
     *
     * @param    RwyKepangkatan $l RwyKepangkatan
     * @return Ptk The current object (for fluent API support)
     */
    public function addRwyKepangkatanRelatedByPtkId(RwyKepangkatan $l)
    {
        if ($this->collRwyKepangkatansRelatedByPtkId === null) {
            $this->initRwyKepangkatansRelatedByPtkId();
            $this->collRwyKepangkatansRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collRwyKepangkatansRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRwyKepangkatanRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	RwyKepangkatanRelatedByPtkId $rwyKepangkatanRelatedByPtkId The rwyKepangkatanRelatedByPtkId object to add.
     */
    protected function doAddRwyKepangkatanRelatedByPtkId($rwyKepangkatanRelatedByPtkId)
    {
        $this->collRwyKepangkatansRelatedByPtkId[]= $rwyKepangkatanRelatedByPtkId;
        $rwyKepangkatanRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	RwyKepangkatanRelatedByPtkId $rwyKepangkatanRelatedByPtkId The rwyKepangkatanRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removeRwyKepangkatanRelatedByPtkId($rwyKepangkatanRelatedByPtkId)
    {
        if ($this->getRwyKepangkatansRelatedByPtkId()->contains($rwyKepangkatanRelatedByPtkId)) {
            $this->collRwyKepangkatansRelatedByPtkId->remove($this->collRwyKepangkatansRelatedByPtkId->search($rwyKepangkatanRelatedByPtkId));
            if (null === $this->rwyKepangkatansRelatedByPtkIdScheduledForDeletion) {
                $this->rwyKepangkatansRelatedByPtkIdScheduledForDeletion = clone $this->collRwyKepangkatansRelatedByPtkId;
                $this->rwyKepangkatansRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->rwyKepangkatansRelatedByPtkIdScheduledForDeletion[]= clone $rwyKepangkatanRelatedByPtkId;
            $rwyKepangkatanRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RwyKepangkatansRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwyKepangkatan[] List of RwyKepangkatan objects
     */
    public function getRwyKepangkatansRelatedByPtkIdJoinPangkatGolonganRelatedByPangkatGolonganId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwyKepangkatanQuery::create(null, $criteria);
        $query->joinWith('PangkatGolonganRelatedByPangkatGolonganId', $join_behavior);

        return $this->getRwyKepangkatansRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RwyKepangkatansRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwyKepangkatan[] List of RwyKepangkatan objects
     */
    public function getRwyKepangkatansRelatedByPtkIdJoinPangkatGolonganRelatedByPangkatGolonganId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwyKepangkatanQuery::create(null, $criteria);
        $query->joinWith('PangkatGolonganRelatedByPangkatGolonganId', $join_behavior);

        return $this->getRwyKepangkatansRelatedByPtkId($query, $con);
    }

    /**
     * Clears out the collRwyKepangkatansRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addRwyKepangkatansRelatedByPtkId()
     */
    public function clearRwyKepangkatansRelatedByPtkId()
    {
        $this->collRwyKepangkatansRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collRwyKepangkatansRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collRwyKepangkatansRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialRwyKepangkatansRelatedByPtkId($v = true)
    {
        $this->collRwyKepangkatansRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collRwyKepangkatansRelatedByPtkId collection.
     *
     * By default this just sets the collRwyKepangkatansRelatedByPtkId collection to an empty array (like clearcollRwyKepangkatansRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRwyKepangkatansRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collRwyKepangkatansRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collRwyKepangkatansRelatedByPtkId = new PropelObjectCollection();
        $this->collRwyKepangkatansRelatedByPtkId->setModel('RwyKepangkatan');
    }

    /**
     * Gets an array of RwyKepangkatan objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RwyKepangkatan[] List of RwyKepangkatan objects
     * @throws PropelException
     */
    public function getRwyKepangkatansRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRwyKepangkatansRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collRwyKepangkatansRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRwyKepangkatansRelatedByPtkId) {
                // return empty collection
                $this->initRwyKepangkatansRelatedByPtkId();
            } else {
                $collRwyKepangkatansRelatedByPtkId = RwyKepangkatanQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRwyKepangkatansRelatedByPtkIdPartial && count($collRwyKepangkatansRelatedByPtkId)) {
                      $this->initRwyKepangkatansRelatedByPtkId(false);

                      foreach($collRwyKepangkatansRelatedByPtkId as $obj) {
                        if (false == $this->collRwyKepangkatansRelatedByPtkId->contains($obj)) {
                          $this->collRwyKepangkatansRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collRwyKepangkatansRelatedByPtkIdPartial = true;
                    }

                    $collRwyKepangkatansRelatedByPtkId->getInternalIterator()->rewind();
                    return $collRwyKepangkatansRelatedByPtkId;
                }

                if($partial && $this->collRwyKepangkatansRelatedByPtkId) {
                    foreach($this->collRwyKepangkatansRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collRwyKepangkatansRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collRwyKepangkatansRelatedByPtkId = $collRwyKepangkatansRelatedByPtkId;
                $this->collRwyKepangkatansRelatedByPtkIdPartial = false;
            }
        }

        return $this->collRwyKepangkatansRelatedByPtkId;
    }

    /**
     * Sets a collection of RwyKepangkatanRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rwyKepangkatansRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setRwyKepangkatansRelatedByPtkId(PropelCollection $rwyKepangkatansRelatedByPtkId, PropelPDO $con = null)
    {
        $rwyKepangkatansRelatedByPtkIdToDelete = $this->getRwyKepangkatansRelatedByPtkId(new Criteria(), $con)->diff($rwyKepangkatansRelatedByPtkId);

        $this->rwyKepangkatansRelatedByPtkIdScheduledForDeletion = unserialize(serialize($rwyKepangkatansRelatedByPtkIdToDelete));

        foreach ($rwyKepangkatansRelatedByPtkIdToDelete as $rwyKepangkatanRelatedByPtkIdRemoved) {
            $rwyKepangkatanRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collRwyKepangkatansRelatedByPtkId = null;
        foreach ($rwyKepangkatansRelatedByPtkId as $rwyKepangkatanRelatedByPtkId) {
            $this->addRwyKepangkatanRelatedByPtkId($rwyKepangkatanRelatedByPtkId);
        }

        $this->collRwyKepangkatansRelatedByPtkId = $rwyKepangkatansRelatedByPtkId;
        $this->collRwyKepangkatansRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RwyKepangkatan objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RwyKepangkatan objects.
     * @throws PropelException
     */
    public function countRwyKepangkatansRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRwyKepangkatansRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collRwyKepangkatansRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRwyKepangkatansRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getRwyKepangkatansRelatedByPtkId());
            }
            $query = RwyKepangkatanQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collRwyKepangkatansRelatedByPtkId);
    }

    /**
     * Method called to associate a RwyKepangkatan object to this object
     * through the RwyKepangkatan foreign key attribute.
     *
     * @param    RwyKepangkatan $l RwyKepangkatan
     * @return Ptk The current object (for fluent API support)
     */
    public function addRwyKepangkatanRelatedByPtkId(RwyKepangkatan $l)
    {
        if ($this->collRwyKepangkatansRelatedByPtkId === null) {
            $this->initRwyKepangkatansRelatedByPtkId();
            $this->collRwyKepangkatansRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collRwyKepangkatansRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRwyKepangkatanRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	RwyKepangkatanRelatedByPtkId $rwyKepangkatanRelatedByPtkId The rwyKepangkatanRelatedByPtkId object to add.
     */
    protected function doAddRwyKepangkatanRelatedByPtkId($rwyKepangkatanRelatedByPtkId)
    {
        $this->collRwyKepangkatansRelatedByPtkId[]= $rwyKepangkatanRelatedByPtkId;
        $rwyKepangkatanRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	RwyKepangkatanRelatedByPtkId $rwyKepangkatanRelatedByPtkId The rwyKepangkatanRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removeRwyKepangkatanRelatedByPtkId($rwyKepangkatanRelatedByPtkId)
    {
        if ($this->getRwyKepangkatansRelatedByPtkId()->contains($rwyKepangkatanRelatedByPtkId)) {
            $this->collRwyKepangkatansRelatedByPtkId->remove($this->collRwyKepangkatansRelatedByPtkId->search($rwyKepangkatanRelatedByPtkId));
            if (null === $this->rwyKepangkatansRelatedByPtkIdScheduledForDeletion) {
                $this->rwyKepangkatansRelatedByPtkIdScheduledForDeletion = clone $this->collRwyKepangkatansRelatedByPtkId;
                $this->rwyKepangkatansRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->rwyKepangkatansRelatedByPtkIdScheduledForDeletion[]= clone $rwyKepangkatanRelatedByPtkId;
            $rwyKepangkatanRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RwyKepangkatansRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwyKepangkatan[] List of RwyKepangkatan objects
     */
    public function getRwyKepangkatansRelatedByPtkIdJoinPangkatGolonganRelatedByPangkatGolonganId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwyKepangkatanQuery::create(null, $criteria);
        $query->joinWith('PangkatGolonganRelatedByPangkatGolonganId', $join_behavior);

        return $this->getRwyKepangkatansRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RwyKepangkatansRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwyKepangkatan[] List of RwyKepangkatan objects
     */
    public function getRwyKepangkatansRelatedByPtkIdJoinPangkatGolonganRelatedByPangkatGolonganId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwyKepangkatanQuery::create(null, $criteria);
        $query->joinWith('PangkatGolonganRelatedByPangkatGolonganId', $join_behavior);

        return $this->getRwyKepangkatansRelatedByPtkId($query, $con);
    }

    /**
     * Clears out the collRombonganBelajarsRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addRombonganBelajarsRelatedByPtkId()
     */
    public function clearRombonganBelajarsRelatedByPtkId()
    {
        $this->collRombonganBelajarsRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collRombonganBelajarsRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collRombonganBelajarsRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialRombonganBelajarsRelatedByPtkId($v = true)
    {
        $this->collRombonganBelajarsRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collRombonganBelajarsRelatedByPtkId collection.
     *
     * By default this just sets the collRombonganBelajarsRelatedByPtkId collection to an empty array (like clearcollRombonganBelajarsRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRombonganBelajarsRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collRombonganBelajarsRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collRombonganBelajarsRelatedByPtkId = new PropelObjectCollection();
        $this->collRombonganBelajarsRelatedByPtkId->setModel('RombonganBelajar');
    }

    /**
     * Gets an array of RombonganBelajar objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     * @throws PropelException
     */
    public function getRombonganBelajarsRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRombonganBelajarsRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collRombonganBelajarsRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRombonganBelajarsRelatedByPtkId) {
                // return empty collection
                $this->initRombonganBelajarsRelatedByPtkId();
            } else {
                $collRombonganBelajarsRelatedByPtkId = RombonganBelajarQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRombonganBelajarsRelatedByPtkIdPartial && count($collRombonganBelajarsRelatedByPtkId)) {
                      $this->initRombonganBelajarsRelatedByPtkId(false);

                      foreach($collRombonganBelajarsRelatedByPtkId as $obj) {
                        if (false == $this->collRombonganBelajarsRelatedByPtkId->contains($obj)) {
                          $this->collRombonganBelajarsRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collRombonganBelajarsRelatedByPtkIdPartial = true;
                    }

                    $collRombonganBelajarsRelatedByPtkId->getInternalIterator()->rewind();
                    return $collRombonganBelajarsRelatedByPtkId;
                }

                if($partial && $this->collRombonganBelajarsRelatedByPtkId) {
                    foreach($this->collRombonganBelajarsRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collRombonganBelajarsRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collRombonganBelajarsRelatedByPtkId = $collRombonganBelajarsRelatedByPtkId;
                $this->collRombonganBelajarsRelatedByPtkIdPartial = false;
            }
        }

        return $this->collRombonganBelajarsRelatedByPtkId;
    }

    /**
     * Sets a collection of RombonganBelajarRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rombonganBelajarsRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setRombonganBelajarsRelatedByPtkId(PropelCollection $rombonganBelajarsRelatedByPtkId, PropelPDO $con = null)
    {
        $rombonganBelajarsRelatedByPtkIdToDelete = $this->getRombonganBelajarsRelatedByPtkId(new Criteria(), $con)->diff($rombonganBelajarsRelatedByPtkId);

        $this->rombonganBelajarsRelatedByPtkIdScheduledForDeletion = unserialize(serialize($rombonganBelajarsRelatedByPtkIdToDelete));

        foreach ($rombonganBelajarsRelatedByPtkIdToDelete as $rombonganBelajarRelatedByPtkIdRemoved) {
            $rombonganBelajarRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collRombonganBelajarsRelatedByPtkId = null;
        foreach ($rombonganBelajarsRelatedByPtkId as $rombonganBelajarRelatedByPtkId) {
            $this->addRombonganBelajarRelatedByPtkId($rombonganBelajarRelatedByPtkId);
        }

        $this->collRombonganBelajarsRelatedByPtkId = $rombonganBelajarsRelatedByPtkId;
        $this->collRombonganBelajarsRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RombonganBelajar objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RombonganBelajar objects.
     * @throws PropelException
     */
    public function countRombonganBelajarsRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRombonganBelajarsRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collRombonganBelajarsRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRombonganBelajarsRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getRombonganBelajarsRelatedByPtkId());
            }
            $query = RombonganBelajarQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collRombonganBelajarsRelatedByPtkId);
    }

    /**
     * Method called to associate a RombonganBelajar object to this object
     * through the RombonganBelajar foreign key attribute.
     *
     * @param    RombonganBelajar $l RombonganBelajar
     * @return Ptk The current object (for fluent API support)
     */
    public function addRombonganBelajarRelatedByPtkId(RombonganBelajar $l)
    {
        if ($this->collRombonganBelajarsRelatedByPtkId === null) {
            $this->initRombonganBelajarsRelatedByPtkId();
            $this->collRombonganBelajarsRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collRombonganBelajarsRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRombonganBelajarRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	RombonganBelajarRelatedByPtkId $rombonganBelajarRelatedByPtkId The rombonganBelajarRelatedByPtkId object to add.
     */
    protected function doAddRombonganBelajarRelatedByPtkId($rombonganBelajarRelatedByPtkId)
    {
        $this->collRombonganBelajarsRelatedByPtkId[]= $rombonganBelajarRelatedByPtkId;
        $rombonganBelajarRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	RombonganBelajarRelatedByPtkId $rombonganBelajarRelatedByPtkId The rombonganBelajarRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removeRombonganBelajarRelatedByPtkId($rombonganBelajarRelatedByPtkId)
    {
        if ($this->getRombonganBelajarsRelatedByPtkId()->contains($rombonganBelajarRelatedByPtkId)) {
            $this->collRombonganBelajarsRelatedByPtkId->remove($this->collRombonganBelajarsRelatedByPtkId->search($rombonganBelajarRelatedByPtkId));
            if (null === $this->rombonganBelajarsRelatedByPtkIdScheduledForDeletion) {
                $this->rombonganBelajarsRelatedByPtkIdScheduledForDeletion = clone $this->collRombonganBelajarsRelatedByPtkId;
                $this->rombonganBelajarsRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->rombonganBelajarsRelatedByPtkIdScheduledForDeletion[]= clone $rombonganBelajarRelatedByPtkId;
            $rombonganBelajarRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPtkIdJoinJurusanSpRelatedByJurusanSpId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('JurusanSpRelatedByJurusanSpId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPtkIdJoinJurusanSpRelatedByJurusanSpId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('JurusanSpRelatedByJurusanSpId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPtkIdJoinPrasaranaRelatedByPrasaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('PrasaranaRelatedByPrasaranaId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPtkIdJoinPrasaranaRelatedByPrasaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('PrasaranaRelatedByPrasaranaId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPtkIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPtkIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPtkIdJoinKebutuhanKhususRelatedByKebutuhanKhususId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPtkIdJoinKebutuhanKhususRelatedByKebutuhanKhususId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPtkIdJoinKurikulumRelatedByKurikulumId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('KurikulumRelatedByKurikulumId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPtkIdJoinKurikulumRelatedByKurikulumId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('KurikulumRelatedByKurikulumId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPtkIdJoinSemesterRelatedBySemesterId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('SemesterRelatedBySemesterId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPtkIdJoinSemesterRelatedBySemesterId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('SemesterRelatedBySemesterId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPtkIdJoinTingkatPendidikanRelatedByTingkatPendidikanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('TingkatPendidikanRelatedByTingkatPendidikanId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPtkIdJoinTingkatPendidikanRelatedByTingkatPendidikanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('TingkatPendidikanRelatedByTingkatPendidikanId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPtkId($query, $con);
    }

    /**
     * Clears out the collRombonganBelajarsRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addRombonganBelajarsRelatedByPtkId()
     */
    public function clearRombonganBelajarsRelatedByPtkId()
    {
        $this->collRombonganBelajarsRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collRombonganBelajarsRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collRombonganBelajarsRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialRombonganBelajarsRelatedByPtkId($v = true)
    {
        $this->collRombonganBelajarsRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collRombonganBelajarsRelatedByPtkId collection.
     *
     * By default this just sets the collRombonganBelajarsRelatedByPtkId collection to an empty array (like clearcollRombonganBelajarsRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRombonganBelajarsRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collRombonganBelajarsRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collRombonganBelajarsRelatedByPtkId = new PropelObjectCollection();
        $this->collRombonganBelajarsRelatedByPtkId->setModel('RombonganBelajar');
    }

    /**
     * Gets an array of RombonganBelajar objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     * @throws PropelException
     */
    public function getRombonganBelajarsRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRombonganBelajarsRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collRombonganBelajarsRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRombonganBelajarsRelatedByPtkId) {
                // return empty collection
                $this->initRombonganBelajarsRelatedByPtkId();
            } else {
                $collRombonganBelajarsRelatedByPtkId = RombonganBelajarQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRombonganBelajarsRelatedByPtkIdPartial && count($collRombonganBelajarsRelatedByPtkId)) {
                      $this->initRombonganBelajarsRelatedByPtkId(false);

                      foreach($collRombonganBelajarsRelatedByPtkId as $obj) {
                        if (false == $this->collRombonganBelajarsRelatedByPtkId->contains($obj)) {
                          $this->collRombonganBelajarsRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collRombonganBelajarsRelatedByPtkIdPartial = true;
                    }

                    $collRombonganBelajarsRelatedByPtkId->getInternalIterator()->rewind();
                    return $collRombonganBelajarsRelatedByPtkId;
                }

                if($partial && $this->collRombonganBelajarsRelatedByPtkId) {
                    foreach($this->collRombonganBelajarsRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collRombonganBelajarsRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collRombonganBelajarsRelatedByPtkId = $collRombonganBelajarsRelatedByPtkId;
                $this->collRombonganBelajarsRelatedByPtkIdPartial = false;
            }
        }

        return $this->collRombonganBelajarsRelatedByPtkId;
    }

    /**
     * Sets a collection of RombonganBelajarRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rombonganBelajarsRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setRombonganBelajarsRelatedByPtkId(PropelCollection $rombonganBelajarsRelatedByPtkId, PropelPDO $con = null)
    {
        $rombonganBelajarsRelatedByPtkIdToDelete = $this->getRombonganBelajarsRelatedByPtkId(new Criteria(), $con)->diff($rombonganBelajarsRelatedByPtkId);

        $this->rombonganBelajarsRelatedByPtkIdScheduledForDeletion = unserialize(serialize($rombonganBelajarsRelatedByPtkIdToDelete));

        foreach ($rombonganBelajarsRelatedByPtkIdToDelete as $rombonganBelajarRelatedByPtkIdRemoved) {
            $rombonganBelajarRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collRombonganBelajarsRelatedByPtkId = null;
        foreach ($rombonganBelajarsRelatedByPtkId as $rombonganBelajarRelatedByPtkId) {
            $this->addRombonganBelajarRelatedByPtkId($rombonganBelajarRelatedByPtkId);
        }

        $this->collRombonganBelajarsRelatedByPtkId = $rombonganBelajarsRelatedByPtkId;
        $this->collRombonganBelajarsRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RombonganBelajar objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RombonganBelajar objects.
     * @throws PropelException
     */
    public function countRombonganBelajarsRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRombonganBelajarsRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collRombonganBelajarsRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRombonganBelajarsRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getRombonganBelajarsRelatedByPtkId());
            }
            $query = RombonganBelajarQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collRombonganBelajarsRelatedByPtkId);
    }

    /**
     * Method called to associate a RombonganBelajar object to this object
     * through the RombonganBelajar foreign key attribute.
     *
     * @param    RombonganBelajar $l RombonganBelajar
     * @return Ptk The current object (for fluent API support)
     */
    public function addRombonganBelajarRelatedByPtkId(RombonganBelajar $l)
    {
        if ($this->collRombonganBelajarsRelatedByPtkId === null) {
            $this->initRombonganBelajarsRelatedByPtkId();
            $this->collRombonganBelajarsRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collRombonganBelajarsRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRombonganBelajarRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	RombonganBelajarRelatedByPtkId $rombonganBelajarRelatedByPtkId The rombonganBelajarRelatedByPtkId object to add.
     */
    protected function doAddRombonganBelajarRelatedByPtkId($rombonganBelajarRelatedByPtkId)
    {
        $this->collRombonganBelajarsRelatedByPtkId[]= $rombonganBelajarRelatedByPtkId;
        $rombonganBelajarRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	RombonganBelajarRelatedByPtkId $rombonganBelajarRelatedByPtkId The rombonganBelajarRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removeRombonganBelajarRelatedByPtkId($rombonganBelajarRelatedByPtkId)
    {
        if ($this->getRombonganBelajarsRelatedByPtkId()->contains($rombonganBelajarRelatedByPtkId)) {
            $this->collRombonganBelajarsRelatedByPtkId->remove($this->collRombonganBelajarsRelatedByPtkId->search($rombonganBelajarRelatedByPtkId));
            if (null === $this->rombonganBelajarsRelatedByPtkIdScheduledForDeletion) {
                $this->rombonganBelajarsRelatedByPtkIdScheduledForDeletion = clone $this->collRombonganBelajarsRelatedByPtkId;
                $this->rombonganBelajarsRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->rombonganBelajarsRelatedByPtkIdScheduledForDeletion[]= clone $rombonganBelajarRelatedByPtkId;
            $rombonganBelajarRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPtkIdJoinJurusanSpRelatedByJurusanSpId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('JurusanSpRelatedByJurusanSpId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPtkIdJoinJurusanSpRelatedByJurusanSpId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('JurusanSpRelatedByJurusanSpId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPtkIdJoinPrasaranaRelatedByPrasaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('PrasaranaRelatedByPrasaranaId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPtkIdJoinPrasaranaRelatedByPrasaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('PrasaranaRelatedByPrasaranaId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPtkIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPtkIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPtkIdJoinKebutuhanKhususRelatedByKebutuhanKhususId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPtkIdJoinKebutuhanKhususRelatedByKebutuhanKhususId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPtkIdJoinKurikulumRelatedByKurikulumId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('KurikulumRelatedByKurikulumId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPtkIdJoinKurikulumRelatedByKurikulumId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('KurikulumRelatedByKurikulumId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPtkIdJoinSemesterRelatedBySemesterId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('SemesterRelatedBySemesterId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPtkIdJoinSemesterRelatedBySemesterId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('SemesterRelatedBySemesterId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPtkIdJoinTingkatPendidikanRelatedByTingkatPendidikanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('TingkatPendidikanRelatedByTingkatPendidikanId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByPtkIdJoinTingkatPendidikanRelatedByTingkatPendidikanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('TingkatPendidikanRelatedByTingkatPendidikanId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByPtkId($query, $con);
    }

    /**
     * Clears out the collRiwayatGajiBerkalasRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addRiwayatGajiBerkalasRelatedByPtkId()
     */
    public function clearRiwayatGajiBerkalasRelatedByPtkId()
    {
        $this->collRiwayatGajiBerkalasRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collRiwayatGajiBerkalasRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collRiwayatGajiBerkalasRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialRiwayatGajiBerkalasRelatedByPtkId($v = true)
    {
        $this->collRiwayatGajiBerkalasRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collRiwayatGajiBerkalasRelatedByPtkId collection.
     *
     * By default this just sets the collRiwayatGajiBerkalasRelatedByPtkId collection to an empty array (like clearcollRiwayatGajiBerkalasRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRiwayatGajiBerkalasRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collRiwayatGajiBerkalasRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collRiwayatGajiBerkalasRelatedByPtkId = new PropelObjectCollection();
        $this->collRiwayatGajiBerkalasRelatedByPtkId->setModel('RiwayatGajiBerkala');
    }

    /**
     * Gets an array of RiwayatGajiBerkala objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RiwayatGajiBerkala[] List of RiwayatGajiBerkala objects
     * @throws PropelException
     */
    public function getRiwayatGajiBerkalasRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRiwayatGajiBerkalasRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collRiwayatGajiBerkalasRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRiwayatGajiBerkalasRelatedByPtkId) {
                // return empty collection
                $this->initRiwayatGajiBerkalasRelatedByPtkId();
            } else {
                $collRiwayatGajiBerkalasRelatedByPtkId = RiwayatGajiBerkalaQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRiwayatGajiBerkalasRelatedByPtkIdPartial && count($collRiwayatGajiBerkalasRelatedByPtkId)) {
                      $this->initRiwayatGajiBerkalasRelatedByPtkId(false);

                      foreach($collRiwayatGajiBerkalasRelatedByPtkId as $obj) {
                        if (false == $this->collRiwayatGajiBerkalasRelatedByPtkId->contains($obj)) {
                          $this->collRiwayatGajiBerkalasRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collRiwayatGajiBerkalasRelatedByPtkIdPartial = true;
                    }

                    $collRiwayatGajiBerkalasRelatedByPtkId->getInternalIterator()->rewind();
                    return $collRiwayatGajiBerkalasRelatedByPtkId;
                }

                if($partial && $this->collRiwayatGajiBerkalasRelatedByPtkId) {
                    foreach($this->collRiwayatGajiBerkalasRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collRiwayatGajiBerkalasRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collRiwayatGajiBerkalasRelatedByPtkId = $collRiwayatGajiBerkalasRelatedByPtkId;
                $this->collRiwayatGajiBerkalasRelatedByPtkIdPartial = false;
            }
        }

        return $this->collRiwayatGajiBerkalasRelatedByPtkId;
    }

    /**
     * Sets a collection of RiwayatGajiBerkalaRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $riwayatGajiBerkalasRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setRiwayatGajiBerkalasRelatedByPtkId(PropelCollection $riwayatGajiBerkalasRelatedByPtkId, PropelPDO $con = null)
    {
        $riwayatGajiBerkalasRelatedByPtkIdToDelete = $this->getRiwayatGajiBerkalasRelatedByPtkId(new Criteria(), $con)->diff($riwayatGajiBerkalasRelatedByPtkId);

        $this->riwayatGajiBerkalasRelatedByPtkIdScheduledForDeletion = unserialize(serialize($riwayatGajiBerkalasRelatedByPtkIdToDelete));

        foreach ($riwayatGajiBerkalasRelatedByPtkIdToDelete as $riwayatGajiBerkalaRelatedByPtkIdRemoved) {
            $riwayatGajiBerkalaRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collRiwayatGajiBerkalasRelatedByPtkId = null;
        foreach ($riwayatGajiBerkalasRelatedByPtkId as $riwayatGajiBerkalaRelatedByPtkId) {
            $this->addRiwayatGajiBerkalaRelatedByPtkId($riwayatGajiBerkalaRelatedByPtkId);
        }

        $this->collRiwayatGajiBerkalasRelatedByPtkId = $riwayatGajiBerkalasRelatedByPtkId;
        $this->collRiwayatGajiBerkalasRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RiwayatGajiBerkala objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RiwayatGajiBerkala objects.
     * @throws PropelException
     */
    public function countRiwayatGajiBerkalasRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRiwayatGajiBerkalasRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collRiwayatGajiBerkalasRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRiwayatGajiBerkalasRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getRiwayatGajiBerkalasRelatedByPtkId());
            }
            $query = RiwayatGajiBerkalaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collRiwayatGajiBerkalasRelatedByPtkId);
    }

    /**
     * Method called to associate a RiwayatGajiBerkala object to this object
     * through the RiwayatGajiBerkala foreign key attribute.
     *
     * @param    RiwayatGajiBerkala $l RiwayatGajiBerkala
     * @return Ptk The current object (for fluent API support)
     */
    public function addRiwayatGajiBerkalaRelatedByPtkId(RiwayatGajiBerkala $l)
    {
        if ($this->collRiwayatGajiBerkalasRelatedByPtkId === null) {
            $this->initRiwayatGajiBerkalasRelatedByPtkId();
            $this->collRiwayatGajiBerkalasRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collRiwayatGajiBerkalasRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRiwayatGajiBerkalaRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	RiwayatGajiBerkalaRelatedByPtkId $riwayatGajiBerkalaRelatedByPtkId The riwayatGajiBerkalaRelatedByPtkId object to add.
     */
    protected function doAddRiwayatGajiBerkalaRelatedByPtkId($riwayatGajiBerkalaRelatedByPtkId)
    {
        $this->collRiwayatGajiBerkalasRelatedByPtkId[]= $riwayatGajiBerkalaRelatedByPtkId;
        $riwayatGajiBerkalaRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	RiwayatGajiBerkalaRelatedByPtkId $riwayatGajiBerkalaRelatedByPtkId The riwayatGajiBerkalaRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removeRiwayatGajiBerkalaRelatedByPtkId($riwayatGajiBerkalaRelatedByPtkId)
    {
        if ($this->getRiwayatGajiBerkalasRelatedByPtkId()->contains($riwayatGajiBerkalaRelatedByPtkId)) {
            $this->collRiwayatGajiBerkalasRelatedByPtkId->remove($this->collRiwayatGajiBerkalasRelatedByPtkId->search($riwayatGajiBerkalaRelatedByPtkId));
            if (null === $this->riwayatGajiBerkalasRelatedByPtkIdScheduledForDeletion) {
                $this->riwayatGajiBerkalasRelatedByPtkIdScheduledForDeletion = clone $this->collRiwayatGajiBerkalasRelatedByPtkId;
                $this->riwayatGajiBerkalasRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->riwayatGajiBerkalasRelatedByPtkIdScheduledForDeletion[]= clone $riwayatGajiBerkalaRelatedByPtkId;
            $riwayatGajiBerkalaRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RiwayatGajiBerkalasRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RiwayatGajiBerkala[] List of RiwayatGajiBerkala objects
     */
    public function getRiwayatGajiBerkalasRelatedByPtkIdJoinPangkatGolonganRelatedByPangkatGolonganId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RiwayatGajiBerkalaQuery::create(null, $criteria);
        $query->joinWith('PangkatGolonganRelatedByPangkatGolonganId', $join_behavior);

        return $this->getRiwayatGajiBerkalasRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RiwayatGajiBerkalasRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RiwayatGajiBerkala[] List of RiwayatGajiBerkala objects
     */
    public function getRiwayatGajiBerkalasRelatedByPtkIdJoinPangkatGolonganRelatedByPangkatGolonganId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RiwayatGajiBerkalaQuery::create(null, $criteria);
        $query->joinWith('PangkatGolonganRelatedByPangkatGolonganId', $join_behavior);

        return $this->getRiwayatGajiBerkalasRelatedByPtkId($query, $con);
    }

    /**
     * Clears out the collRiwayatGajiBerkalasRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addRiwayatGajiBerkalasRelatedByPtkId()
     */
    public function clearRiwayatGajiBerkalasRelatedByPtkId()
    {
        $this->collRiwayatGajiBerkalasRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collRiwayatGajiBerkalasRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collRiwayatGajiBerkalasRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialRiwayatGajiBerkalasRelatedByPtkId($v = true)
    {
        $this->collRiwayatGajiBerkalasRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collRiwayatGajiBerkalasRelatedByPtkId collection.
     *
     * By default this just sets the collRiwayatGajiBerkalasRelatedByPtkId collection to an empty array (like clearcollRiwayatGajiBerkalasRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRiwayatGajiBerkalasRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collRiwayatGajiBerkalasRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collRiwayatGajiBerkalasRelatedByPtkId = new PropelObjectCollection();
        $this->collRiwayatGajiBerkalasRelatedByPtkId->setModel('RiwayatGajiBerkala');
    }

    /**
     * Gets an array of RiwayatGajiBerkala objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RiwayatGajiBerkala[] List of RiwayatGajiBerkala objects
     * @throws PropelException
     */
    public function getRiwayatGajiBerkalasRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRiwayatGajiBerkalasRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collRiwayatGajiBerkalasRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRiwayatGajiBerkalasRelatedByPtkId) {
                // return empty collection
                $this->initRiwayatGajiBerkalasRelatedByPtkId();
            } else {
                $collRiwayatGajiBerkalasRelatedByPtkId = RiwayatGajiBerkalaQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRiwayatGajiBerkalasRelatedByPtkIdPartial && count($collRiwayatGajiBerkalasRelatedByPtkId)) {
                      $this->initRiwayatGajiBerkalasRelatedByPtkId(false);

                      foreach($collRiwayatGajiBerkalasRelatedByPtkId as $obj) {
                        if (false == $this->collRiwayatGajiBerkalasRelatedByPtkId->contains($obj)) {
                          $this->collRiwayatGajiBerkalasRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collRiwayatGajiBerkalasRelatedByPtkIdPartial = true;
                    }

                    $collRiwayatGajiBerkalasRelatedByPtkId->getInternalIterator()->rewind();
                    return $collRiwayatGajiBerkalasRelatedByPtkId;
                }

                if($partial && $this->collRiwayatGajiBerkalasRelatedByPtkId) {
                    foreach($this->collRiwayatGajiBerkalasRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collRiwayatGajiBerkalasRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collRiwayatGajiBerkalasRelatedByPtkId = $collRiwayatGajiBerkalasRelatedByPtkId;
                $this->collRiwayatGajiBerkalasRelatedByPtkIdPartial = false;
            }
        }

        return $this->collRiwayatGajiBerkalasRelatedByPtkId;
    }

    /**
     * Sets a collection of RiwayatGajiBerkalaRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $riwayatGajiBerkalasRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setRiwayatGajiBerkalasRelatedByPtkId(PropelCollection $riwayatGajiBerkalasRelatedByPtkId, PropelPDO $con = null)
    {
        $riwayatGajiBerkalasRelatedByPtkIdToDelete = $this->getRiwayatGajiBerkalasRelatedByPtkId(new Criteria(), $con)->diff($riwayatGajiBerkalasRelatedByPtkId);

        $this->riwayatGajiBerkalasRelatedByPtkIdScheduledForDeletion = unserialize(serialize($riwayatGajiBerkalasRelatedByPtkIdToDelete));

        foreach ($riwayatGajiBerkalasRelatedByPtkIdToDelete as $riwayatGajiBerkalaRelatedByPtkIdRemoved) {
            $riwayatGajiBerkalaRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collRiwayatGajiBerkalasRelatedByPtkId = null;
        foreach ($riwayatGajiBerkalasRelatedByPtkId as $riwayatGajiBerkalaRelatedByPtkId) {
            $this->addRiwayatGajiBerkalaRelatedByPtkId($riwayatGajiBerkalaRelatedByPtkId);
        }

        $this->collRiwayatGajiBerkalasRelatedByPtkId = $riwayatGajiBerkalasRelatedByPtkId;
        $this->collRiwayatGajiBerkalasRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RiwayatGajiBerkala objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RiwayatGajiBerkala objects.
     * @throws PropelException
     */
    public function countRiwayatGajiBerkalasRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRiwayatGajiBerkalasRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collRiwayatGajiBerkalasRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRiwayatGajiBerkalasRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getRiwayatGajiBerkalasRelatedByPtkId());
            }
            $query = RiwayatGajiBerkalaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collRiwayatGajiBerkalasRelatedByPtkId);
    }

    /**
     * Method called to associate a RiwayatGajiBerkala object to this object
     * through the RiwayatGajiBerkala foreign key attribute.
     *
     * @param    RiwayatGajiBerkala $l RiwayatGajiBerkala
     * @return Ptk The current object (for fluent API support)
     */
    public function addRiwayatGajiBerkalaRelatedByPtkId(RiwayatGajiBerkala $l)
    {
        if ($this->collRiwayatGajiBerkalasRelatedByPtkId === null) {
            $this->initRiwayatGajiBerkalasRelatedByPtkId();
            $this->collRiwayatGajiBerkalasRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collRiwayatGajiBerkalasRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRiwayatGajiBerkalaRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	RiwayatGajiBerkalaRelatedByPtkId $riwayatGajiBerkalaRelatedByPtkId The riwayatGajiBerkalaRelatedByPtkId object to add.
     */
    protected function doAddRiwayatGajiBerkalaRelatedByPtkId($riwayatGajiBerkalaRelatedByPtkId)
    {
        $this->collRiwayatGajiBerkalasRelatedByPtkId[]= $riwayatGajiBerkalaRelatedByPtkId;
        $riwayatGajiBerkalaRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	RiwayatGajiBerkalaRelatedByPtkId $riwayatGajiBerkalaRelatedByPtkId The riwayatGajiBerkalaRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removeRiwayatGajiBerkalaRelatedByPtkId($riwayatGajiBerkalaRelatedByPtkId)
    {
        if ($this->getRiwayatGajiBerkalasRelatedByPtkId()->contains($riwayatGajiBerkalaRelatedByPtkId)) {
            $this->collRiwayatGajiBerkalasRelatedByPtkId->remove($this->collRiwayatGajiBerkalasRelatedByPtkId->search($riwayatGajiBerkalaRelatedByPtkId));
            if (null === $this->riwayatGajiBerkalasRelatedByPtkIdScheduledForDeletion) {
                $this->riwayatGajiBerkalasRelatedByPtkIdScheduledForDeletion = clone $this->collRiwayatGajiBerkalasRelatedByPtkId;
                $this->riwayatGajiBerkalasRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->riwayatGajiBerkalasRelatedByPtkIdScheduledForDeletion[]= clone $riwayatGajiBerkalaRelatedByPtkId;
            $riwayatGajiBerkalaRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RiwayatGajiBerkalasRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RiwayatGajiBerkala[] List of RiwayatGajiBerkala objects
     */
    public function getRiwayatGajiBerkalasRelatedByPtkIdJoinPangkatGolonganRelatedByPangkatGolonganId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RiwayatGajiBerkalaQuery::create(null, $criteria);
        $query->joinWith('PangkatGolonganRelatedByPangkatGolonganId', $join_behavior);

        return $this->getRiwayatGajiBerkalasRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RiwayatGajiBerkalasRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RiwayatGajiBerkala[] List of RiwayatGajiBerkala objects
     */
    public function getRiwayatGajiBerkalasRelatedByPtkIdJoinPangkatGolonganRelatedByPangkatGolonganId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RiwayatGajiBerkalaQuery::create(null, $criteria);
        $query->joinWith('PangkatGolonganRelatedByPangkatGolonganId', $join_behavior);

        return $this->getRiwayatGajiBerkalasRelatedByPtkId($query, $con);
    }

    /**
     * Clears out the collPengawasTerdaftarsRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addPengawasTerdaftarsRelatedByPtkId()
     */
    public function clearPengawasTerdaftarsRelatedByPtkId()
    {
        $this->collPengawasTerdaftarsRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collPengawasTerdaftarsRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPengawasTerdaftarsRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPengawasTerdaftarsRelatedByPtkId($v = true)
    {
        $this->collPengawasTerdaftarsRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collPengawasTerdaftarsRelatedByPtkId collection.
     *
     * By default this just sets the collPengawasTerdaftarsRelatedByPtkId collection to an empty array (like clearcollPengawasTerdaftarsRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPengawasTerdaftarsRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collPengawasTerdaftarsRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collPengawasTerdaftarsRelatedByPtkId = new PropelObjectCollection();
        $this->collPengawasTerdaftarsRelatedByPtkId->setModel('PengawasTerdaftar');
    }

    /**
     * Gets an array of PengawasTerdaftar objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     * @throws PropelException
     */
    public function getPengawasTerdaftarsRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPengawasTerdaftarsRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collPengawasTerdaftarsRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPengawasTerdaftarsRelatedByPtkId) {
                // return empty collection
                $this->initPengawasTerdaftarsRelatedByPtkId();
            } else {
                $collPengawasTerdaftarsRelatedByPtkId = PengawasTerdaftarQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPengawasTerdaftarsRelatedByPtkIdPartial && count($collPengawasTerdaftarsRelatedByPtkId)) {
                      $this->initPengawasTerdaftarsRelatedByPtkId(false);

                      foreach($collPengawasTerdaftarsRelatedByPtkId as $obj) {
                        if (false == $this->collPengawasTerdaftarsRelatedByPtkId->contains($obj)) {
                          $this->collPengawasTerdaftarsRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collPengawasTerdaftarsRelatedByPtkIdPartial = true;
                    }

                    $collPengawasTerdaftarsRelatedByPtkId->getInternalIterator()->rewind();
                    return $collPengawasTerdaftarsRelatedByPtkId;
                }

                if($partial && $this->collPengawasTerdaftarsRelatedByPtkId) {
                    foreach($this->collPengawasTerdaftarsRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collPengawasTerdaftarsRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collPengawasTerdaftarsRelatedByPtkId = $collPengawasTerdaftarsRelatedByPtkId;
                $this->collPengawasTerdaftarsRelatedByPtkIdPartial = false;
            }
        }

        return $this->collPengawasTerdaftarsRelatedByPtkId;
    }

    /**
     * Sets a collection of PengawasTerdaftarRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $pengawasTerdaftarsRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setPengawasTerdaftarsRelatedByPtkId(PropelCollection $pengawasTerdaftarsRelatedByPtkId, PropelPDO $con = null)
    {
        $pengawasTerdaftarsRelatedByPtkIdToDelete = $this->getPengawasTerdaftarsRelatedByPtkId(new Criteria(), $con)->diff($pengawasTerdaftarsRelatedByPtkId);

        $this->pengawasTerdaftarsRelatedByPtkIdScheduledForDeletion = unserialize(serialize($pengawasTerdaftarsRelatedByPtkIdToDelete));

        foreach ($pengawasTerdaftarsRelatedByPtkIdToDelete as $pengawasTerdaftarRelatedByPtkIdRemoved) {
            $pengawasTerdaftarRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collPengawasTerdaftarsRelatedByPtkId = null;
        foreach ($pengawasTerdaftarsRelatedByPtkId as $pengawasTerdaftarRelatedByPtkId) {
            $this->addPengawasTerdaftarRelatedByPtkId($pengawasTerdaftarRelatedByPtkId);
        }

        $this->collPengawasTerdaftarsRelatedByPtkId = $pengawasTerdaftarsRelatedByPtkId;
        $this->collPengawasTerdaftarsRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PengawasTerdaftar objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related PengawasTerdaftar objects.
     * @throws PropelException
     */
    public function countPengawasTerdaftarsRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPengawasTerdaftarsRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collPengawasTerdaftarsRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPengawasTerdaftarsRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPengawasTerdaftarsRelatedByPtkId());
            }
            $query = PengawasTerdaftarQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collPengawasTerdaftarsRelatedByPtkId);
    }

    /**
     * Method called to associate a PengawasTerdaftar object to this object
     * through the PengawasTerdaftar foreign key attribute.
     *
     * @param    PengawasTerdaftar $l PengawasTerdaftar
     * @return Ptk The current object (for fluent API support)
     */
    public function addPengawasTerdaftarRelatedByPtkId(PengawasTerdaftar $l)
    {
        if ($this->collPengawasTerdaftarsRelatedByPtkId === null) {
            $this->initPengawasTerdaftarsRelatedByPtkId();
            $this->collPengawasTerdaftarsRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collPengawasTerdaftarsRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPengawasTerdaftarRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	PengawasTerdaftarRelatedByPtkId $pengawasTerdaftarRelatedByPtkId The pengawasTerdaftarRelatedByPtkId object to add.
     */
    protected function doAddPengawasTerdaftarRelatedByPtkId($pengawasTerdaftarRelatedByPtkId)
    {
        $this->collPengawasTerdaftarsRelatedByPtkId[]= $pengawasTerdaftarRelatedByPtkId;
        $pengawasTerdaftarRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	PengawasTerdaftarRelatedByPtkId $pengawasTerdaftarRelatedByPtkId The pengawasTerdaftarRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removePengawasTerdaftarRelatedByPtkId($pengawasTerdaftarRelatedByPtkId)
    {
        if ($this->getPengawasTerdaftarsRelatedByPtkId()->contains($pengawasTerdaftarRelatedByPtkId)) {
            $this->collPengawasTerdaftarsRelatedByPtkId->remove($this->collPengawasTerdaftarsRelatedByPtkId->search($pengawasTerdaftarRelatedByPtkId));
            if (null === $this->pengawasTerdaftarsRelatedByPtkIdScheduledForDeletion) {
                $this->pengawasTerdaftarsRelatedByPtkIdScheduledForDeletion = clone $this->collPengawasTerdaftarsRelatedByPtkId;
                $this->pengawasTerdaftarsRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->pengawasTerdaftarsRelatedByPtkIdScheduledForDeletion[]= clone $pengawasTerdaftarRelatedByPtkId;
            $pengawasTerdaftarRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByPtkIdJoinLembagaNonSekolahRelatedByLembagaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('LembagaNonSekolahRelatedByLembagaId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByPtkIdJoinLembagaNonSekolahRelatedByLembagaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('LembagaNonSekolahRelatedByLembagaId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByPtkIdJoinBidangStudiRelatedByBidangStudiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('BidangStudiRelatedByBidangStudiId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByPtkIdJoinBidangStudiRelatedByBidangStudiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('BidangStudiRelatedByBidangStudiId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByPtkIdJoinJenisKeluarRelatedByJenisKeluarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('JenisKeluarRelatedByJenisKeluarId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByPtkIdJoinJenisKeluarRelatedByJenisKeluarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('JenisKeluarRelatedByJenisKeluarId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByPtkIdJoinJenjangKepengawasanRelatedByJenjangKepengawasanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('JenjangKepengawasanRelatedByJenjangKepengawasanId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByPtkIdJoinJenjangKepengawasanRelatedByJenjangKepengawasanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('JenjangKepengawasanRelatedByJenjangKepengawasanId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByPtkIdJoinMataPelajaranRelatedByMataPelajaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('MataPelajaranRelatedByMataPelajaranId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByPtkIdJoinMataPelajaranRelatedByMataPelajaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('MataPelajaranRelatedByMataPelajaranId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByPtkIdJoinTahunAjaranRelatedByTahunAjaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('TahunAjaranRelatedByTahunAjaranId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByPtkIdJoinTahunAjaranRelatedByTahunAjaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('TahunAjaranRelatedByTahunAjaranId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByPtkId($query, $con);
    }

    /**
     * Clears out the collPengawasTerdaftarsRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addPengawasTerdaftarsRelatedByPtkId()
     */
    public function clearPengawasTerdaftarsRelatedByPtkId()
    {
        $this->collPengawasTerdaftarsRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collPengawasTerdaftarsRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPengawasTerdaftarsRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPengawasTerdaftarsRelatedByPtkId($v = true)
    {
        $this->collPengawasTerdaftarsRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collPengawasTerdaftarsRelatedByPtkId collection.
     *
     * By default this just sets the collPengawasTerdaftarsRelatedByPtkId collection to an empty array (like clearcollPengawasTerdaftarsRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPengawasTerdaftarsRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collPengawasTerdaftarsRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collPengawasTerdaftarsRelatedByPtkId = new PropelObjectCollection();
        $this->collPengawasTerdaftarsRelatedByPtkId->setModel('PengawasTerdaftar');
    }

    /**
     * Gets an array of PengawasTerdaftar objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     * @throws PropelException
     */
    public function getPengawasTerdaftarsRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPengawasTerdaftarsRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collPengawasTerdaftarsRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPengawasTerdaftarsRelatedByPtkId) {
                // return empty collection
                $this->initPengawasTerdaftarsRelatedByPtkId();
            } else {
                $collPengawasTerdaftarsRelatedByPtkId = PengawasTerdaftarQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPengawasTerdaftarsRelatedByPtkIdPartial && count($collPengawasTerdaftarsRelatedByPtkId)) {
                      $this->initPengawasTerdaftarsRelatedByPtkId(false);

                      foreach($collPengawasTerdaftarsRelatedByPtkId as $obj) {
                        if (false == $this->collPengawasTerdaftarsRelatedByPtkId->contains($obj)) {
                          $this->collPengawasTerdaftarsRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collPengawasTerdaftarsRelatedByPtkIdPartial = true;
                    }

                    $collPengawasTerdaftarsRelatedByPtkId->getInternalIterator()->rewind();
                    return $collPengawasTerdaftarsRelatedByPtkId;
                }

                if($partial && $this->collPengawasTerdaftarsRelatedByPtkId) {
                    foreach($this->collPengawasTerdaftarsRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collPengawasTerdaftarsRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collPengawasTerdaftarsRelatedByPtkId = $collPengawasTerdaftarsRelatedByPtkId;
                $this->collPengawasTerdaftarsRelatedByPtkIdPartial = false;
            }
        }

        return $this->collPengawasTerdaftarsRelatedByPtkId;
    }

    /**
     * Sets a collection of PengawasTerdaftarRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $pengawasTerdaftarsRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setPengawasTerdaftarsRelatedByPtkId(PropelCollection $pengawasTerdaftarsRelatedByPtkId, PropelPDO $con = null)
    {
        $pengawasTerdaftarsRelatedByPtkIdToDelete = $this->getPengawasTerdaftarsRelatedByPtkId(new Criteria(), $con)->diff($pengawasTerdaftarsRelatedByPtkId);

        $this->pengawasTerdaftarsRelatedByPtkIdScheduledForDeletion = unserialize(serialize($pengawasTerdaftarsRelatedByPtkIdToDelete));

        foreach ($pengawasTerdaftarsRelatedByPtkIdToDelete as $pengawasTerdaftarRelatedByPtkIdRemoved) {
            $pengawasTerdaftarRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collPengawasTerdaftarsRelatedByPtkId = null;
        foreach ($pengawasTerdaftarsRelatedByPtkId as $pengawasTerdaftarRelatedByPtkId) {
            $this->addPengawasTerdaftarRelatedByPtkId($pengawasTerdaftarRelatedByPtkId);
        }

        $this->collPengawasTerdaftarsRelatedByPtkId = $pengawasTerdaftarsRelatedByPtkId;
        $this->collPengawasTerdaftarsRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PengawasTerdaftar objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related PengawasTerdaftar objects.
     * @throws PropelException
     */
    public function countPengawasTerdaftarsRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPengawasTerdaftarsRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collPengawasTerdaftarsRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPengawasTerdaftarsRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPengawasTerdaftarsRelatedByPtkId());
            }
            $query = PengawasTerdaftarQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collPengawasTerdaftarsRelatedByPtkId);
    }

    /**
     * Method called to associate a PengawasTerdaftar object to this object
     * through the PengawasTerdaftar foreign key attribute.
     *
     * @param    PengawasTerdaftar $l PengawasTerdaftar
     * @return Ptk The current object (for fluent API support)
     */
    public function addPengawasTerdaftarRelatedByPtkId(PengawasTerdaftar $l)
    {
        if ($this->collPengawasTerdaftarsRelatedByPtkId === null) {
            $this->initPengawasTerdaftarsRelatedByPtkId();
            $this->collPengawasTerdaftarsRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collPengawasTerdaftarsRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPengawasTerdaftarRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	PengawasTerdaftarRelatedByPtkId $pengawasTerdaftarRelatedByPtkId The pengawasTerdaftarRelatedByPtkId object to add.
     */
    protected function doAddPengawasTerdaftarRelatedByPtkId($pengawasTerdaftarRelatedByPtkId)
    {
        $this->collPengawasTerdaftarsRelatedByPtkId[]= $pengawasTerdaftarRelatedByPtkId;
        $pengawasTerdaftarRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	PengawasTerdaftarRelatedByPtkId $pengawasTerdaftarRelatedByPtkId The pengawasTerdaftarRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removePengawasTerdaftarRelatedByPtkId($pengawasTerdaftarRelatedByPtkId)
    {
        if ($this->getPengawasTerdaftarsRelatedByPtkId()->contains($pengawasTerdaftarRelatedByPtkId)) {
            $this->collPengawasTerdaftarsRelatedByPtkId->remove($this->collPengawasTerdaftarsRelatedByPtkId->search($pengawasTerdaftarRelatedByPtkId));
            if (null === $this->pengawasTerdaftarsRelatedByPtkIdScheduledForDeletion) {
                $this->pengawasTerdaftarsRelatedByPtkIdScheduledForDeletion = clone $this->collPengawasTerdaftarsRelatedByPtkId;
                $this->pengawasTerdaftarsRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->pengawasTerdaftarsRelatedByPtkIdScheduledForDeletion[]= clone $pengawasTerdaftarRelatedByPtkId;
            $pengawasTerdaftarRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByPtkIdJoinLembagaNonSekolahRelatedByLembagaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('LembagaNonSekolahRelatedByLembagaId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByPtkIdJoinLembagaNonSekolahRelatedByLembagaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('LembagaNonSekolahRelatedByLembagaId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByPtkIdJoinBidangStudiRelatedByBidangStudiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('BidangStudiRelatedByBidangStudiId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByPtkIdJoinBidangStudiRelatedByBidangStudiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('BidangStudiRelatedByBidangStudiId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByPtkIdJoinJenisKeluarRelatedByJenisKeluarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('JenisKeluarRelatedByJenisKeluarId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByPtkIdJoinJenisKeluarRelatedByJenisKeluarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('JenisKeluarRelatedByJenisKeluarId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByPtkIdJoinJenjangKepengawasanRelatedByJenjangKepengawasanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('JenjangKepengawasanRelatedByJenjangKepengawasanId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByPtkIdJoinJenjangKepengawasanRelatedByJenjangKepengawasanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('JenjangKepengawasanRelatedByJenjangKepengawasanId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByPtkIdJoinMataPelajaranRelatedByMataPelajaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('MataPelajaranRelatedByMataPelajaranId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByPtkIdJoinMataPelajaranRelatedByMataPelajaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('MataPelajaranRelatedByMataPelajaranId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByPtkIdJoinTahunAjaranRelatedByTahunAjaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('TahunAjaranRelatedByTahunAjaranId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PengawasTerdaftarsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PengawasTerdaftar[] List of PengawasTerdaftar objects
     */
    public function getPengawasTerdaftarsRelatedByPtkIdJoinTahunAjaranRelatedByTahunAjaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PengawasTerdaftarQuery::create(null, $criteria);
        $query->joinWith('TahunAjaranRelatedByTahunAjaranId', $join_behavior);

        return $this->getPengawasTerdaftarsRelatedByPtkId($query, $con);
    }

    /**
     * Clears out the collRwyStrukturalsRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addRwyStrukturalsRelatedByPtkId()
     */
    public function clearRwyStrukturalsRelatedByPtkId()
    {
        $this->collRwyStrukturalsRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collRwyStrukturalsRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collRwyStrukturalsRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialRwyStrukturalsRelatedByPtkId($v = true)
    {
        $this->collRwyStrukturalsRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collRwyStrukturalsRelatedByPtkId collection.
     *
     * By default this just sets the collRwyStrukturalsRelatedByPtkId collection to an empty array (like clearcollRwyStrukturalsRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRwyStrukturalsRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collRwyStrukturalsRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collRwyStrukturalsRelatedByPtkId = new PropelObjectCollection();
        $this->collRwyStrukturalsRelatedByPtkId->setModel('RwyStruktural');
    }

    /**
     * Gets an array of RwyStruktural objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RwyStruktural[] List of RwyStruktural objects
     * @throws PropelException
     */
    public function getRwyStrukturalsRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRwyStrukturalsRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collRwyStrukturalsRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRwyStrukturalsRelatedByPtkId) {
                // return empty collection
                $this->initRwyStrukturalsRelatedByPtkId();
            } else {
                $collRwyStrukturalsRelatedByPtkId = RwyStrukturalQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRwyStrukturalsRelatedByPtkIdPartial && count($collRwyStrukturalsRelatedByPtkId)) {
                      $this->initRwyStrukturalsRelatedByPtkId(false);

                      foreach($collRwyStrukturalsRelatedByPtkId as $obj) {
                        if (false == $this->collRwyStrukturalsRelatedByPtkId->contains($obj)) {
                          $this->collRwyStrukturalsRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collRwyStrukturalsRelatedByPtkIdPartial = true;
                    }

                    $collRwyStrukturalsRelatedByPtkId->getInternalIterator()->rewind();
                    return $collRwyStrukturalsRelatedByPtkId;
                }

                if($partial && $this->collRwyStrukturalsRelatedByPtkId) {
                    foreach($this->collRwyStrukturalsRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collRwyStrukturalsRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collRwyStrukturalsRelatedByPtkId = $collRwyStrukturalsRelatedByPtkId;
                $this->collRwyStrukturalsRelatedByPtkIdPartial = false;
            }
        }

        return $this->collRwyStrukturalsRelatedByPtkId;
    }

    /**
     * Sets a collection of RwyStrukturalRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rwyStrukturalsRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setRwyStrukturalsRelatedByPtkId(PropelCollection $rwyStrukturalsRelatedByPtkId, PropelPDO $con = null)
    {
        $rwyStrukturalsRelatedByPtkIdToDelete = $this->getRwyStrukturalsRelatedByPtkId(new Criteria(), $con)->diff($rwyStrukturalsRelatedByPtkId);

        $this->rwyStrukturalsRelatedByPtkIdScheduledForDeletion = unserialize(serialize($rwyStrukturalsRelatedByPtkIdToDelete));

        foreach ($rwyStrukturalsRelatedByPtkIdToDelete as $rwyStrukturalRelatedByPtkIdRemoved) {
            $rwyStrukturalRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collRwyStrukturalsRelatedByPtkId = null;
        foreach ($rwyStrukturalsRelatedByPtkId as $rwyStrukturalRelatedByPtkId) {
            $this->addRwyStrukturalRelatedByPtkId($rwyStrukturalRelatedByPtkId);
        }

        $this->collRwyStrukturalsRelatedByPtkId = $rwyStrukturalsRelatedByPtkId;
        $this->collRwyStrukturalsRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RwyStruktural objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RwyStruktural objects.
     * @throws PropelException
     */
    public function countRwyStrukturalsRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRwyStrukturalsRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collRwyStrukturalsRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRwyStrukturalsRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getRwyStrukturalsRelatedByPtkId());
            }
            $query = RwyStrukturalQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collRwyStrukturalsRelatedByPtkId);
    }

    /**
     * Method called to associate a RwyStruktural object to this object
     * through the RwyStruktural foreign key attribute.
     *
     * @param    RwyStruktural $l RwyStruktural
     * @return Ptk The current object (for fluent API support)
     */
    public function addRwyStrukturalRelatedByPtkId(RwyStruktural $l)
    {
        if ($this->collRwyStrukturalsRelatedByPtkId === null) {
            $this->initRwyStrukturalsRelatedByPtkId();
            $this->collRwyStrukturalsRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collRwyStrukturalsRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRwyStrukturalRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	RwyStrukturalRelatedByPtkId $rwyStrukturalRelatedByPtkId The rwyStrukturalRelatedByPtkId object to add.
     */
    protected function doAddRwyStrukturalRelatedByPtkId($rwyStrukturalRelatedByPtkId)
    {
        $this->collRwyStrukturalsRelatedByPtkId[]= $rwyStrukturalRelatedByPtkId;
        $rwyStrukturalRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	RwyStrukturalRelatedByPtkId $rwyStrukturalRelatedByPtkId The rwyStrukturalRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removeRwyStrukturalRelatedByPtkId($rwyStrukturalRelatedByPtkId)
    {
        if ($this->getRwyStrukturalsRelatedByPtkId()->contains($rwyStrukturalRelatedByPtkId)) {
            $this->collRwyStrukturalsRelatedByPtkId->remove($this->collRwyStrukturalsRelatedByPtkId->search($rwyStrukturalRelatedByPtkId));
            if (null === $this->rwyStrukturalsRelatedByPtkIdScheduledForDeletion) {
                $this->rwyStrukturalsRelatedByPtkIdScheduledForDeletion = clone $this->collRwyStrukturalsRelatedByPtkId;
                $this->rwyStrukturalsRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->rwyStrukturalsRelatedByPtkIdScheduledForDeletion[]= clone $rwyStrukturalRelatedByPtkId;
            $rwyStrukturalRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RwyStrukturalsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwyStruktural[] List of RwyStruktural objects
     */
    public function getRwyStrukturalsRelatedByPtkIdJoinJabatanTugasPtkRelatedByJabatanPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwyStrukturalQuery::create(null, $criteria);
        $query->joinWith('JabatanTugasPtkRelatedByJabatanPtkId', $join_behavior);

        return $this->getRwyStrukturalsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RwyStrukturalsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwyStruktural[] List of RwyStruktural objects
     */
    public function getRwyStrukturalsRelatedByPtkIdJoinJabatanTugasPtkRelatedByJabatanPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwyStrukturalQuery::create(null, $criteria);
        $query->joinWith('JabatanTugasPtkRelatedByJabatanPtkId', $join_behavior);

        return $this->getRwyStrukturalsRelatedByPtkId($query, $con);
    }

    /**
     * Clears out the collRwyStrukturalsRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addRwyStrukturalsRelatedByPtkId()
     */
    public function clearRwyStrukturalsRelatedByPtkId()
    {
        $this->collRwyStrukturalsRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collRwyStrukturalsRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collRwyStrukturalsRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialRwyStrukturalsRelatedByPtkId($v = true)
    {
        $this->collRwyStrukturalsRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collRwyStrukturalsRelatedByPtkId collection.
     *
     * By default this just sets the collRwyStrukturalsRelatedByPtkId collection to an empty array (like clearcollRwyStrukturalsRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRwyStrukturalsRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collRwyStrukturalsRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collRwyStrukturalsRelatedByPtkId = new PropelObjectCollection();
        $this->collRwyStrukturalsRelatedByPtkId->setModel('RwyStruktural');
    }

    /**
     * Gets an array of RwyStruktural objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RwyStruktural[] List of RwyStruktural objects
     * @throws PropelException
     */
    public function getRwyStrukturalsRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRwyStrukturalsRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collRwyStrukturalsRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRwyStrukturalsRelatedByPtkId) {
                // return empty collection
                $this->initRwyStrukturalsRelatedByPtkId();
            } else {
                $collRwyStrukturalsRelatedByPtkId = RwyStrukturalQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRwyStrukturalsRelatedByPtkIdPartial && count($collRwyStrukturalsRelatedByPtkId)) {
                      $this->initRwyStrukturalsRelatedByPtkId(false);

                      foreach($collRwyStrukturalsRelatedByPtkId as $obj) {
                        if (false == $this->collRwyStrukturalsRelatedByPtkId->contains($obj)) {
                          $this->collRwyStrukturalsRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collRwyStrukturalsRelatedByPtkIdPartial = true;
                    }

                    $collRwyStrukturalsRelatedByPtkId->getInternalIterator()->rewind();
                    return $collRwyStrukturalsRelatedByPtkId;
                }

                if($partial && $this->collRwyStrukturalsRelatedByPtkId) {
                    foreach($this->collRwyStrukturalsRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collRwyStrukturalsRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collRwyStrukturalsRelatedByPtkId = $collRwyStrukturalsRelatedByPtkId;
                $this->collRwyStrukturalsRelatedByPtkIdPartial = false;
            }
        }

        return $this->collRwyStrukturalsRelatedByPtkId;
    }

    /**
     * Sets a collection of RwyStrukturalRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rwyStrukturalsRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setRwyStrukturalsRelatedByPtkId(PropelCollection $rwyStrukturalsRelatedByPtkId, PropelPDO $con = null)
    {
        $rwyStrukturalsRelatedByPtkIdToDelete = $this->getRwyStrukturalsRelatedByPtkId(new Criteria(), $con)->diff($rwyStrukturalsRelatedByPtkId);

        $this->rwyStrukturalsRelatedByPtkIdScheduledForDeletion = unserialize(serialize($rwyStrukturalsRelatedByPtkIdToDelete));

        foreach ($rwyStrukturalsRelatedByPtkIdToDelete as $rwyStrukturalRelatedByPtkIdRemoved) {
            $rwyStrukturalRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collRwyStrukturalsRelatedByPtkId = null;
        foreach ($rwyStrukturalsRelatedByPtkId as $rwyStrukturalRelatedByPtkId) {
            $this->addRwyStrukturalRelatedByPtkId($rwyStrukturalRelatedByPtkId);
        }

        $this->collRwyStrukturalsRelatedByPtkId = $rwyStrukturalsRelatedByPtkId;
        $this->collRwyStrukturalsRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RwyStruktural objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RwyStruktural objects.
     * @throws PropelException
     */
    public function countRwyStrukturalsRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRwyStrukturalsRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collRwyStrukturalsRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRwyStrukturalsRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getRwyStrukturalsRelatedByPtkId());
            }
            $query = RwyStrukturalQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collRwyStrukturalsRelatedByPtkId);
    }

    /**
     * Method called to associate a RwyStruktural object to this object
     * through the RwyStruktural foreign key attribute.
     *
     * @param    RwyStruktural $l RwyStruktural
     * @return Ptk The current object (for fluent API support)
     */
    public function addRwyStrukturalRelatedByPtkId(RwyStruktural $l)
    {
        if ($this->collRwyStrukturalsRelatedByPtkId === null) {
            $this->initRwyStrukturalsRelatedByPtkId();
            $this->collRwyStrukturalsRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collRwyStrukturalsRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRwyStrukturalRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	RwyStrukturalRelatedByPtkId $rwyStrukturalRelatedByPtkId The rwyStrukturalRelatedByPtkId object to add.
     */
    protected function doAddRwyStrukturalRelatedByPtkId($rwyStrukturalRelatedByPtkId)
    {
        $this->collRwyStrukturalsRelatedByPtkId[]= $rwyStrukturalRelatedByPtkId;
        $rwyStrukturalRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	RwyStrukturalRelatedByPtkId $rwyStrukturalRelatedByPtkId The rwyStrukturalRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removeRwyStrukturalRelatedByPtkId($rwyStrukturalRelatedByPtkId)
    {
        if ($this->getRwyStrukturalsRelatedByPtkId()->contains($rwyStrukturalRelatedByPtkId)) {
            $this->collRwyStrukturalsRelatedByPtkId->remove($this->collRwyStrukturalsRelatedByPtkId->search($rwyStrukturalRelatedByPtkId));
            if (null === $this->rwyStrukturalsRelatedByPtkIdScheduledForDeletion) {
                $this->rwyStrukturalsRelatedByPtkIdScheduledForDeletion = clone $this->collRwyStrukturalsRelatedByPtkId;
                $this->rwyStrukturalsRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->rwyStrukturalsRelatedByPtkIdScheduledForDeletion[]= clone $rwyStrukturalRelatedByPtkId;
            $rwyStrukturalRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RwyStrukturalsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwyStruktural[] List of RwyStruktural objects
     */
    public function getRwyStrukturalsRelatedByPtkIdJoinJabatanTugasPtkRelatedByJabatanPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwyStrukturalQuery::create(null, $criteria);
        $query->joinWith('JabatanTugasPtkRelatedByJabatanPtkId', $join_behavior);

        return $this->getRwyStrukturalsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RwyStrukturalsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwyStruktural[] List of RwyStruktural objects
     */
    public function getRwyStrukturalsRelatedByPtkIdJoinJabatanTugasPtkRelatedByJabatanPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwyStrukturalQuery::create(null, $criteria);
        $query->joinWith('JabatanTugasPtkRelatedByJabatanPtkId', $join_behavior);

        return $this->getRwyStrukturalsRelatedByPtkId($query, $con);
    }

    /**
     * Clears out the collRwyPendFormalsRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addRwyPendFormalsRelatedByPtkId()
     */
    public function clearRwyPendFormalsRelatedByPtkId()
    {
        $this->collRwyPendFormalsRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collRwyPendFormalsRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collRwyPendFormalsRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialRwyPendFormalsRelatedByPtkId($v = true)
    {
        $this->collRwyPendFormalsRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collRwyPendFormalsRelatedByPtkId collection.
     *
     * By default this just sets the collRwyPendFormalsRelatedByPtkId collection to an empty array (like clearcollRwyPendFormalsRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRwyPendFormalsRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collRwyPendFormalsRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collRwyPendFormalsRelatedByPtkId = new PropelObjectCollection();
        $this->collRwyPendFormalsRelatedByPtkId->setModel('RwyPendFormal');
    }

    /**
     * Gets an array of RwyPendFormal objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RwyPendFormal[] List of RwyPendFormal objects
     * @throws PropelException
     */
    public function getRwyPendFormalsRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRwyPendFormalsRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collRwyPendFormalsRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRwyPendFormalsRelatedByPtkId) {
                // return empty collection
                $this->initRwyPendFormalsRelatedByPtkId();
            } else {
                $collRwyPendFormalsRelatedByPtkId = RwyPendFormalQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRwyPendFormalsRelatedByPtkIdPartial && count($collRwyPendFormalsRelatedByPtkId)) {
                      $this->initRwyPendFormalsRelatedByPtkId(false);

                      foreach($collRwyPendFormalsRelatedByPtkId as $obj) {
                        if (false == $this->collRwyPendFormalsRelatedByPtkId->contains($obj)) {
                          $this->collRwyPendFormalsRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collRwyPendFormalsRelatedByPtkIdPartial = true;
                    }

                    $collRwyPendFormalsRelatedByPtkId->getInternalIterator()->rewind();
                    return $collRwyPendFormalsRelatedByPtkId;
                }

                if($partial && $this->collRwyPendFormalsRelatedByPtkId) {
                    foreach($this->collRwyPendFormalsRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collRwyPendFormalsRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collRwyPendFormalsRelatedByPtkId = $collRwyPendFormalsRelatedByPtkId;
                $this->collRwyPendFormalsRelatedByPtkIdPartial = false;
            }
        }

        return $this->collRwyPendFormalsRelatedByPtkId;
    }

    /**
     * Sets a collection of RwyPendFormalRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rwyPendFormalsRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setRwyPendFormalsRelatedByPtkId(PropelCollection $rwyPendFormalsRelatedByPtkId, PropelPDO $con = null)
    {
        $rwyPendFormalsRelatedByPtkIdToDelete = $this->getRwyPendFormalsRelatedByPtkId(new Criteria(), $con)->diff($rwyPendFormalsRelatedByPtkId);

        $this->rwyPendFormalsRelatedByPtkIdScheduledForDeletion = unserialize(serialize($rwyPendFormalsRelatedByPtkIdToDelete));

        foreach ($rwyPendFormalsRelatedByPtkIdToDelete as $rwyPendFormalRelatedByPtkIdRemoved) {
            $rwyPendFormalRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collRwyPendFormalsRelatedByPtkId = null;
        foreach ($rwyPendFormalsRelatedByPtkId as $rwyPendFormalRelatedByPtkId) {
            $this->addRwyPendFormalRelatedByPtkId($rwyPendFormalRelatedByPtkId);
        }

        $this->collRwyPendFormalsRelatedByPtkId = $rwyPendFormalsRelatedByPtkId;
        $this->collRwyPendFormalsRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RwyPendFormal objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RwyPendFormal objects.
     * @throws PropelException
     */
    public function countRwyPendFormalsRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRwyPendFormalsRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collRwyPendFormalsRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRwyPendFormalsRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getRwyPendFormalsRelatedByPtkId());
            }
            $query = RwyPendFormalQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collRwyPendFormalsRelatedByPtkId);
    }

    /**
     * Method called to associate a RwyPendFormal object to this object
     * through the RwyPendFormal foreign key attribute.
     *
     * @param    RwyPendFormal $l RwyPendFormal
     * @return Ptk The current object (for fluent API support)
     */
    public function addRwyPendFormalRelatedByPtkId(RwyPendFormal $l)
    {
        if ($this->collRwyPendFormalsRelatedByPtkId === null) {
            $this->initRwyPendFormalsRelatedByPtkId();
            $this->collRwyPendFormalsRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collRwyPendFormalsRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRwyPendFormalRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	RwyPendFormalRelatedByPtkId $rwyPendFormalRelatedByPtkId The rwyPendFormalRelatedByPtkId object to add.
     */
    protected function doAddRwyPendFormalRelatedByPtkId($rwyPendFormalRelatedByPtkId)
    {
        $this->collRwyPendFormalsRelatedByPtkId[]= $rwyPendFormalRelatedByPtkId;
        $rwyPendFormalRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	RwyPendFormalRelatedByPtkId $rwyPendFormalRelatedByPtkId The rwyPendFormalRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removeRwyPendFormalRelatedByPtkId($rwyPendFormalRelatedByPtkId)
    {
        if ($this->getRwyPendFormalsRelatedByPtkId()->contains($rwyPendFormalRelatedByPtkId)) {
            $this->collRwyPendFormalsRelatedByPtkId->remove($this->collRwyPendFormalsRelatedByPtkId->search($rwyPendFormalRelatedByPtkId));
            if (null === $this->rwyPendFormalsRelatedByPtkIdScheduledForDeletion) {
                $this->rwyPendFormalsRelatedByPtkIdScheduledForDeletion = clone $this->collRwyPendFormalsRelatedByPtkId;
                $this->rwyPendFormalsRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->rwyPendFormalsRelatedByPtkIdScheduledForDeletion[]= clone $rwyPendFormalRelatedByPtkId;
            $rwyPendFormalRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RwyPendFormalsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwyPendFormal[] List of RwyPendFormal objects
     */
    public function getRwyPendFormalsRelatedByPtkIdJoinBidangStudiRelatedByBidangStudiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwyPendFormalQuery::create(null, $criteria);
        $query->joinWith('BidangStudiRelatedByBidangStudiId', $join_behavior);

        return $this->getRwyPendFormalsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RwyPendFormalsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwyPendFormal[] List of RwyPendFormal objects
     */
    public function getRwyPendFormalsRelatedByPtkIdJoinBidangStudiRelatedByBidangStudiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwyPendFormalQuery::create(null, $criteria);
        $query->joinWith('BidangStudiRelatedByBidangStudiId', $join_behavior);

        return $this->getRwyPendFormalsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RwyPendFormalsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwyPendFormal[] List of RwyPendFormal objects
     */
    public function getRwyPendFormalsRelatedByPtkIdJoinGelarAkademikRelatedByGelarAkademikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwyPendFormalQuery::create(null, $criteria);
        $query->joinWith('GelarAkademikRelatedByGelarAkademikId', $join_behavior);

        return $this->getRwyPendFormalsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RwyPendFormalsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwyPendFormal[] List of RwyPendFormal objects
     */
    public function getRwyPendFormalsRelatedByPtkIdJoinGelarAkademikRelatedByGelarAkademikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwyPendFormalQuery::create(null, $criteria);
        $query->joinWith('GelarAkademikRelatedByGelarAkademikId', $join_behavior);

        return $this->getRwyPendFormalsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RwyPendFormalsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwyPendFormal[] List of RwyPendFormal objects
     */
    public function getRwyPendFormalsRelatedByPtkIdJoinJenjangPendidikanRelatedByJenjangPendidikanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwyPendFormalQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanId', $join_behavior);

        return $this->getRwyPendFormalsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RwyPendFormalsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwyPendFormal[] List of RwyPendFormal objects
     */
    public function getRwyPendFormalsRelatedByPtkIdJoinJenjangPendidikanRelatedByJenjangPendidikanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwyPendFormalQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanId', $join_behavior);

        return $this->getRwyPendFormalsRelatedByPtkId($query, $con);
    }

    /**
     * Clears out the collRwyPendFormalsRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addRwyPendFormalsRelatedByPtkId()
     */
    public function clearRwyPendFormalsRelatedByPtkId()
    {
        $this->collRwyPendFormalsRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collRwyPendFormalsRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collRwyPendFormalsRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialRwyPendFormalsRelatedByPtkId($v = true)
    {
        $this->collRwyPendFormalsRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collRwyPendFormalsRelatedByPtkId collection.
     *
     * By default this just sets the collRwyPendFormalsRelatedByPtkId collection to an empty array (like clearcollRwyPendFormalsRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRwyPendFormalsRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collRwyPendFormalsRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collRwyPendFormalsRelatedByPtkId = new PropelObjectCollection();
        $this->collRwyPendFormalsRelatedByPtkId->setModel('RwyPendFormal');
    }

    /**
     * Gets an array of RwyPendFormal objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RwyPendFormal[] List of RwyPendFormal objects
     * @throws PropelException
     */
    public function getRwyPendFormalsRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRwyPendFormalsRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collRwyPendFormalsRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRwyPendFormalsRelatedByPtkId) {
                // return empty collection
                $this->initRwyPendFormalsRelatedByPtkId();
            } else {
                $collRwyPendFormalsRelatedByPtkId = RwyPendFormalQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRwyPendFormalsRelatedByPtkIdPartial && count($collRwyPendFormalsRelatedByPtkId)) {
                      $this->initRwyPendFormalsRelatedByPtkId(false);

                      foreach($collRwyPendFormalsRelatedByPtkId as $obj) {
                        if (false == $this->collRwyPendFormalsRelatedByPtkId->contains($obj)) {
                          $this->collRwyPendFormalsRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collRwyPendFormalsRelatedByPtkIdPartial = true;
                    }

                    $collRwyPendFormalsRelatedByPtkId->getInternalIterator()->rewind();
                    return $collRwyPendFormalsRelatedByPtkId;
                }

                if($partial && $this->collRwyPendFormalsRelatedByPtkId) {
                    foreach($this->collRwyPendFormalsRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collRwyPendFormalsRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collRwyPendFormalsRelatedByPtkId = $collRwyPendFormalsRelatedByPtkId;
                $this->collRwyPendFormalsRelatedByPtkIdPartial = false;
            }
        }

        return $this->collRwyPendFormalsRelatedByPtkId;
    }

    /**
     * Sets a collection of RwyPendFormalRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rwyPendFormalsRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setRwyPendFormalsRelatedByPtkId(PropelCollection $rwyPendFormalsRelatedByPtkId, PropelPDO $con = null)
    {
        $rwyPendFormalsRelatedByPtkIdToDelete = $this->getRwyPendFormalsRelatedByPtkId(new Criteria(), $con)->diff($rwyPendFormalsRelatedByPtkId);

        $this->rwyPendFormalsRelatedByPtkIdScheduledForDeletion = unserialize(serialize($rwyPendFormalsRelatedByPtkIdToDelete));

        foreach ($rwyPendFormalsRelatedByPtkIdToDelete as $rwyPendFormalRelatedByPtkIdRemoved) {
            $rwyPendFormalRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collRwyPendFormalsRelatedByPtkId = null;
        foreach ($rwyPendFormalsRelatedByPtkId as $rwyPendFormalRelatedByPtkId) {
            $this->addRwyPendFormalRelatedByPtkId($rwyPendFormalRelatedByPtkId);
        }

        $this->collRwyPendFormalsRelatedByPtkId = $rwyPendFormalsRelatedByPtkId;
        $this->collRwyPendFormalsRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RwyPendFormal objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RwyPendFormal objects.
     * @throws PropelException
     */
    public function countRwyPendFormalsRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRwyPendFormalsRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collRwyPendFormalsRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRwyPendFormalsRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getRwyPendFormalsRelatedByPtkId());
            }
            $query = RwyPendFormalQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collRwyPendFormalsRelatedByPtkId);
    }

    /**
     * Method called to associate a RwyPendFormal object to this object
     * through the RwyPendFormal foreign key attribute.
     *
     * @param    RwyPendFormal $l RwyPendFormal
     * @return Ptk The current object (for fluent API support)
     */
    public function addRwyPendFormalRelatedByPtkId(RwyPendFormal $l)
    {
        if ($this->collRwyPendFormalsRelatedByPtkId === null) {
            $this->initRwyPendFormalsRelatedByPtkId();
            $this->collRwyPendFormalsRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collRwyPendFormalsRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRwyPendFormalRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	RwyPendFormalRelatedByPtkId $rwyPendFormalRelatedByPtkId The rwyPendFormalRelatedByPtkId object to add.
     */
    protected function doAddRwyPendFormalRelatedByPtkId($rwyPendFormalRelatedByPtkId)
    {
        $this->collRwyPendFormalsRelatedByPtkId[]= $rwyPendFormalRelatedByPtkId;
        $rwyPendFormalRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	RwyPendFormalRelatedByPtkId $rwyPendFormalRelatedByPtkId The rwyPendFormalRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removeRwyPendFormalRelatedByPtkId($rwyPendFormalRelatedByPtkId)
    {
        if ($this->getRwyPendFormalsRelatedByPtkId()->contains($rwyPendFormalRelatedByPtkId)) {
            $this->collRwyPendFormalsRelatedByPtkId->remove($this->collRwyPendFormalsRelatedByPtkId->search($rwyPendFormalRelatedByPtkId));
            if (null === $this->rwyPendFormalsRelatedByPtkIdScheduledForDeletion) {
                $this->rwyPendFormalsRelatedByPtkIdScheduledForDeletion = clone $this->collRwyPendFormalsRelatedByPtkId;
                $this->rwyPendFormalsRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->rwyPendFormalsRelatedByPtkIdScheduledForDeletion[]= clone $rwyPendFormalRelatedByPtkId;
            $rwyPendFormalRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RwyPendFormalsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwyPendFormal[] List of RwyPendFormal objects
     */
    public function getRwyPendFormalsRelatedByPtkIdJoinBidangStudiRelatedByBidangStudiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwyPendFormalQuery::create(null, $criteria);
        $query->joinWith('BidangStudiRelatedByBidangStudiId', $join_behavior);

        return $this->getRwyPendFormalsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RwyPendFormalsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwyPendFormal[] List of RwyPendFormal objects
     */
    public function getRwyPendFormalsRelatedByPtkIdJoinBidangStudiRelatedByBidangStudiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwyPendFormalQuery::create(null, $criteria);
        $query->joinWith('BidangStudiRelatedByBidangStudiId', $join_behavior);

        return $this->getRwyPendFormalsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RwyPendFormalsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwyPendFormal[] List of RwyPendFormal objects
     */
    public function getRwyPendFormalsRelatedByPtkIdJoinGelarAkademikRelatedByGelarAkademikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwyPendFormalQuery::create(null, $criteria);
        $query->joinWith('GelarAkademikRelatedByGelarAkademikId', $join_behavior);

        return $this->getRwyPendFormalsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RwyPendFormalsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwyPendFormal[] List of RwyPendFormal objects
     */
    public function getRwyPendFormalsRelatedByPtkIdJoinGelarAkademikRelatedByGelarAkademikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwyPendFormalQuery::create(null, $criteria);
        $query->joinWith('GelarAkademikRelatedByGelarAkademikId', $join_behavior);

        return $this->getRwyPendFormalsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RwyPendFormalsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwyPendFormal[] List of RwyPendFormal objects
     */
    public function getRwyPendFormalsRelatedByPtkIdJoinJenjangPendidikanRelatedByJenjangPendidikanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwyPendFormalQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanId', $join_behavior);

        return $this->getRwyPendFormalsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RwyPendFormalsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwyPendFormal[] List of RwyPendFormal objects
     */
    public function getRwyPendFormalsRelatedByPtkIdJoinJenjangPendidikanRelatedByJenjangPendidikanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwyPendFormalQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanId', $join_behavior);

        return $this->getRwyPendFormalsRelatedByPtkId($query, $con);
    }

    /**
     * Clears out the collAnaksRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addAnaksRelatedByPtkId()
     */
    public function clearAnaksRelatedByPtkId()
    {
        $this->collAnaksRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collAnaksRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collAnaksRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialAnaksRelatedByPtkId($v = true)
    {
        $this->collAnaksRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collAnaksRelatedByPtkId collection.
     *
     * By default this just sets the collAnaksRelatedByPtkId collection to an empty array (like clearcollAnaksRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initAnaksRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collAnaksRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collAnaksRelatedByPtkId = new PropelObjectCollection();
        $this->collAnaksRelatedByPtkId->setModel('Anak');
    }

    /**
     * Gets an array of Anak objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Anak[] List of Anak objects
     * @throws PropelException
     */
    public function getAnaksRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collAnaksRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collAnaksRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collAnaksRelatedByPtkId) {
                // return empty collection
                $this->initAnaksRelatedByPtkId();
            } else {
                $collAnaksRelatedByPtkId = AnakQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collAnaksRelatedByPtkIdPartial && count($collAnaksRelatedByPtkId)) {
                      $this->initAnaksRelatedByPtkId(false);

                      foreach($collAnaksRelatedByPtkId as $obj) {
                        if (false == $this->collAnaksRelatedByPtkId->contains($obj)) {
                          $this->collAnaksRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collAnaksRelatedByPtkIdPartial = true;
                    }

                    $collAnaksRelatedByPtkId->getInternalIterator()->rewind();
                    return $collAnaksRelatedByPtkId;
                }

                if($partial && $this->collAnaksRelatedByPtkId) {
                    foreach($this->collAnaksRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collAnaksRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collAnaksRelatedByPtkId = $collAnaksRelatedByPtkId;
                $this->collAnaksRelatedByPtkIdPartial = false;
            }
        }

        return $this->collAnaksRelatedByPtkId;
    }

    /**
     * Sets a collection of AnakRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $anaksRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setAnaksRelatedByPtkId(PropelCollection $anaksRelatedByPtkId, PropelPDO $con = null)
    {
        $anaksRelatedByPtkIdToDelete = $this->getAnaksRelatedByPtkId(new Criteria(), $con)->diff($anaksRelatedByPtkId);

        $this->anaksRelatedByPtkIdScheduledForDeletion = unserialize(serialize($anaksRelatedByPtkIdToDelete));

        foreach ($anaksRelatedByPtkIdToDelete as $anakRelatedByPtkIdRemoved) {
            $anakRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collAnaksRelatedByPtkId = null;
        foreach ($anaksRelatedByPtkId as $anakRelatedByPtkId) {
            $this->addAnakRelatedByPtkId($anakRelatedByPtkId);
        }

        $this->collAnaksRelatedByPtkId = $anaksRelatedByPtkId;
        $this->collAnaksRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Anak objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Anak objects.
     * @throws PropelException
     */
    public function countAnaksRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collAnaksRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collAnaksRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collAnaksRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getAnaksRelatedByPtkId());
            }
            $query = AnakQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collAnaksRelatedByPtkId);
    }

    /**
     * Method called to associate a Anak object to this object
     * through the Anak foreign key attribute.
     *
     * @param    Anak $l Anak
     * @return Ptk The current object (for fluent API support)
     */
    public function addAnakRelatedByPtkId(Anak $l)
    {
        if ($this->collAnaksRelatedByPtkId === null) {
            $this->initAnaksRelatedByPtkId();
            $this->collAnaksRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collAnaksRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddAnakRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	AnakRelatedByPtkId $anakRelatedByPtkId The anakRelatedByPtkId object to add.
     */
    protected function doAddAnakRelatedByPtkId($anakRelatedByPtkId)
    {
        $this->collAnaksRelatedByPtkId[]= $anakRelatedByPtkId;
        $anakRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	AnakRelatedByPtkId $anakRelatedByPtkId The anakRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removeAnakRelatedByPtkId($anakRelatedByPtkId)
    {
        if ($this->getAnaksRelatedByPtkId()->contains($anakRelatedByPtkId)) {
            $this->collAnaksRelatedByPtkId->remove($this->collAnaksRelatedByPtkId->search($anakRelatedByPtkId));
            if (null === $this->anaksRelatedByPtkIdScheduledForDeletion) {
                $this->anaksRelatedByPtkIdScheduledForDeletion = clone $this->collAnaksRelatedByPtkId;
                $this->anaksRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->anaksRelatedByPtkIdScheduledForDeletion[]= clone $anakRelatedByPtkId;
            $anakRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related AnaksRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Anak[] List of Anak objects
     */
    public function getAnaksRelatedByPtkIdJoinJenjangPendidikanRelatedByJenjangPendidikanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AnakQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanId', $join_behavior);

        return $this->getAnaksRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related AnaksRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Anak[] List of Anak objects
     */
    public function getAnaksRelatedByPtkIdJoinJenjangPendidikanRelatedByJenjangPendidikanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AnakQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanId', $join_behavior);

        return $this->getAnaksRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related AnaksRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Anak[] List of Anak objects
     */
    public function getAnaksRelatedByPtkIdJoinStatusAnakRelatedByStatusAnakId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AnakQuery::create(null, $criteria);
        $query->joinWith('StatusAnakRelatedByStatusAnakId', $join_behavior);

        return $this->getAnaksRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related AnaksRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Anak[] List of Anak objects
     */
    public function getAnaksRelatedByPtkIdJoinStatusAnakRelatedByStatusAnakId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AnakQuery::create(null, $criteria);
        $query->joinWith('StatusAnakRelatedByStatusAnakId', $join_behavior);

        return $this->getAnaksRelatedByPtkId($query, $con);
    }

    /**
     * Clears out the collAnaksRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addAnaksRelatedByPtkId()
     */
    public function clearAnaksRelatedByPtkId()
    {
        $this->collAnaksRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collAnaksRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collAnaksRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialAnaksRelatedByPtkId($v = true)
    {
        $this->collAnaksRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collAnaksRelatedByPtkId collection.
     *
     * By default this just sets the collAnaksRelatedByPtkId collection to an empty array (like clearcollAnaksRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initAnaksRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collAnaksRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collAnaksRelatedByPtkId = new PropelObjectCollection();
        $this->collAnaksRelatedByPtkId->setModel('Anak');
    }

    /**
     * Gets an array of Anak objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Anak[] List of Anak objects
     * @throws PropelException
     */
    public function getAnaksRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collAnaksRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collAnaksRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collAnaksRelatedByPtkId) {
                // return empty collection
                $this->initAnaksRelatedByPtkId();
            } else {
                $collAnaksRelatedByPtkId = AnakQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collAnaksRelatedByPtkIdPartial && count($collAnaksRelatedByPtkId)) {
                      $this->initAnaksRelatedByPtkId(false);

                      foreach($collAnaksRelatedByPtkId as $obj) {
                        if (false == $this->collAnaksRelatedByPtkId->contains($obj)) {
                          $this->collAnaksRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collAnaksRelatedByPtkIdPartial = true;
                    }

                    $collAnaksRelatedByPtkId->getInternalIterator()->rewind();
                    return $collAnaksRelatedByPtkId;
                }

                if($partial && $this->collAnaksRelatedByPtkId) {
                    foreach($this->collAnaksRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collAnaksRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collAnaksRelatedByPtkId = $collAnaksRelatedByPtkId;
                $this->collAnaksRelatedByPtkIdPartial = false;
            }
        }

        return $this->collAnaksRelatedByPtkId;
    }

    /**
     * Sets a collection of AnakRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $anaksRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setAnaksRelatedByPtkId(PropelCollection $anaksRelatedByPtkId, PropelPDO $con = null)
    {
        $anaksRelatedByPtkIdToDelete = $this->getAnaksRelatedByPtkId(new Criteria(), $con)->diff($anaksRelatedByPtkId);

        $this->anaksRelatedByPtkIdScheduledForDeletion = unserialize(serialize($anaksRelatedByPtkIdToDelete));

        foreach ($anaksRelatedByPtkIdToDelete as $anakRelatedByPtkIdRemoved) {
            $anakRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collAnaksRelatedByPtkId = null;
        foreach ($anaksRelatedByPtkId as $anakRelatedByPtkId) {
            $this->addAnakRelatedByPtkId($anakRelatedByPtkId);
        }

        $this->collAnaksRelatedByPtkId = $anaksRelatedByPtkId;
        $this->collAnaksRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Anak objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Anak objects.
     * @throws PropelException
     */
    public function countAnaksRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collAnaksRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collAnaksRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collAnaksRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getAnaksRelatedByPtkId());
            }
            $query = AnakQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collAnaksRelatedByPtkId);
    }

    /**
     * Method called to associate a Anak object to this object
     * through the Anak foreign key attribute.
     *
     * @param    Anak $l Anak
     * @return Ptk The current object (for fluent API support)
     */
    public function addAnakRelatedByPtkId(Anak $l)
    {
        if ($this->collAnaksRelatedByPtkId === null) {
            $this->initAnaksRelatedByPtkId();
            $this->collAnaksRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collAnaksRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddAnakRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	AnakRelatedByPtkId $anakRelatedByPtkId The anakRelatedByPtkId object to add.
     */
    protected function doAddAnakRelatedByPtkId($anakRelatedByPtkId)
    {
        $this->collAnaksRelatedByPtkId[]= $anakRelatedByPtkId;
        $anakRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	AnakRelatedByPtkId $anakRelatedByPtkId The anakRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removeAnakRelatedByPtkId($anakRelatedByPtkId)
    {
        if ($this->getAnaksRelatedByPtkId()->contains($anakRelatedByPtkId)) {
            $this->collAnaksRelatedByPtkId->remove($this->collAnaksRelatedByPtkId->search($anakRelatedByPtkId));
            if (null === $this->anaksRelatedByPtkIdScheduledForDeletion) {
                $this->anaksRelatedByPtkIdScheduledForDeletion = clone $this->collAnaksRelatedByPtkId;
                $this->anaksRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->anaksRelatedByPtkIdScheduledForDeletion[]= clone $anakRelatedByPtkId;
            $anakRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related AnaksRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Anak[] List of Anak objects
     */
    public function getAnaksRelatedByPtkIdJoinJenjangPendidikanRelatedByJenjangPendidikanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AnakQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanId', $join_behavior);

        return $this->getAnaksRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related AnaksRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Anak[] List of Anak objects
     */
    public function getAnaksRelatedByPtkIdJoinJenjangPendidikanRelatedByJenjangPendidikanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AnakQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanId', $join_behavior);

        return $this->getAnaksRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related AnaksRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Anak[] List of Anak objects
     */
    public function getAnaksRelatedByPtkIdJoinStatusAnakRelatedByStatusAnakId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AnakQuery::create(null, $criteria);
        $query->joinWith('StatusAnakRelatedByStatusAnakId', $join_behavior);

        return $this->getAnaksRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related AnaksRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Anak[] List of Anak objects
     */
    public function getAnaksRelatedByPtkIdJoinStatusAnakRelatedByStatusAnakId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AnakQuery::create(null, $criteria);
        $query->joinWith('StatusAnakRelatedByStatusAnakId', $join_behavior);

        return $this->getAnaksRelatedByPtkId($query, $con);
    }

    /**
     * Clears out the collDiklatsRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addDiklatsRelatedByPtkId()
     */
    public function clearDiklatsRelatedByPtkId()
    {
        $this->collDiklatsRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collDiklatsRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collDiklatsRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialDiklatsRelatedByPtkId($v = true)
    {
        $this->collDiklatsRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collDiklatsRelatedByPtkId collection.
     *
     * By default this just sets the collDiklatsRelatedByPtkId collection to an empty array (like clearcollDiklatsRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initDiklatsRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collDiklatsRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collDiklatsRelatedByPtkId = new PropelObjectCollection();
        $this->collDiklatsRelatedByPtkId->setModel('Diklat');
    }

    /**
     * Gets an array of Diklat objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Diklat[] List of Diklat objects
     * @throws PropelException
     */
    public function getDiklatsRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collDiklatsRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collDiklatsRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collDiklatsRelatedByPtkId) {
                // return empty collection
                $this->initDiklatsRelatedByPtkId();
            } else {
                $collDiklatsRelatedByPtkId = DiklatQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collDiklatsRelatedByPtkIdPartial && count($collDiklatsRelatedByPtkId)) {
                      $this->initDiklatsRelatedByPtkId(false);

                      foreach($collDiklatsRelatedByPtkId as $obj) {
                        if (false == $this->collDiklatsRelatedByPtkId->contains($obj)) {
                          $this->collDiklatsRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collDiklatsRelatedByPtkIdPartial = true;
                    }

                    $collDiklatsRelatedByPtkId->getInternalIterator()->rewind();
                    return $collDiklatsRelatedByPtkId;
                }

                if($partial && $this->collDiklatsRelatedByPtkId) {
                    foreach($this->collDiklatsRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collDiklatsRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collDiklatsRelatedByPtkId = $collDiklatsRelatedByPtkId;
                $this->collDiklatsRelatedByPtkIdPartial = false;
            }
        }

        return $this->collDiklatsRelatedByPtkId;
    }

    /**
     * Sets a collection of DiklatRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $diklatsRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setDiklatsRelatedByPtkId(PropelCollection $diklatsRelatedByPtkId, PropelPDO $con = null)
    {
        $diklatsRelatedByPtkIdToDelete = $this->getDiklatsRelatedByPtkId(new Criteria(), $con)->diff($diklatsRelatedByPtkId);

        $this->diklatsRelatedByPtkIdScheduledForDeletion = unserialize(serialize($diklatsRelatedByPtkIdToDelete));

        foreach ($diklatsRelatedByPtkIdToDelete as $diklatRelatedByPtkIdRemoved) {
            $diklatRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collDiklatsRelatedByPtkId = null;
        foreach ($diklatsRelatedByPtkId as $diklatRelatedByPtkId) {
            $this->addDiklatRelatedByPtkId($diklatRelatedByPtkId);
        }

        $this->collDiklatsRelatedByPtkId = $diklatsRelatedByPtkId;
        $this->collDiklatsRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Diklat objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Diklat objects.
     * @throws PropelException
     */
    public function countDiklatsRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collDiklatsRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collDiklatsRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collDiklatsRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getDiklatsRelatedByPtkId());
            }
            $query = DiklatQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collDiklatsRelatedByPtkId);
    }

    /**
     * Method called to associate a Diklat object to this object
     * through the Diklat foreign key attribute.
     *
     * @param    Diklat $l Diklat
     * @return Ptk The current object (for fluent API support)
     */
    public function addDiklatRelatedByPtkId(Diklat $l)
    {
        if ($this->collDiklatsRelatedByPtkId === null) {
            $this->initDiklatsRelatedByPtkId();
            $this->collDiklatsRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collDiklatsRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddDiklatRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	DiklatRelatedByPtkId $diklatRelatedByPtkId The diklatRelatedByPtkId object to add.
     */
    protected function doAddDiklatRelatedByPtkId($diklatRelatedByPtkId)
    {
        $this->collDiklatsRelatedByPtkId[]= $diklatRelatedByPtkId;
        $diklatRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	DiklatRelatedByPtkId $diklatRelatedByPtkId The diklatRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removeDiklatRelatedByPtkId($diklatRelatedByPtkId)
    {
        if ($this->getDiklatsRelatedByPtkId()->contains($diklatRelatedByPtkId)) {
            $this->collDiklatsRelatedByPtkId->remove($this->collDiklatsRelatedByPtkId->search($diklatRelatedByPtkId));
            if (null === $this->diklatsRelatedByPtkIdScheduledForDeletion) {
                $this->diklatsRelatedByPtkIdScheduledForDeletion = clone $this->collDiklatsRelatedByPtkId;
                $this->diklatsRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->diklatsRelatedByPtkIdScheduledForDeletion[]= clone $diklatRelatedByPtkId;
            $diklatRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related DiklatsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Diklat[] List of Diklat objects
     */
    public function getDiklatsRelatedByPtkIdJoinJenisDiklatRelatedByJenisDiklatId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = DiklatQuery::create(null, $criteria);
        $query->joinWith('JenisDiklatRelatedByJenisDiklatId', $join_behavior);

        return $this->getDiklatsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related DiklatsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Diklat[] List of Diklat objects
     */
    public function getDiklatsRelatedByPtkIdJoinJenisDiklatRelatedByJenisDiklatId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = DiklatQuery::create(null, $criteria);
        $query->joinWith('JenisDiklatRelatedByJenisDiklatId', $join_behavior);

        return $this->getDiklatsRelatedByPtkId($query, $con);
    }

    /**
     * Clears out the collDiklatsRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addDiklatsRelatedByPtkId()
     */
    public function clearDiklatsRelatedByPtkId()
    {
        $this->collDiklatsRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collDiklatsRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collDiklatsRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialDiklatsRelatedByPtkId($v = true)
    {
        $this->collDiklatsRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collDiklatsRelatedByPtkId collection.
     *
     * By default this just sets the collDiklatsRelatedByPtkId collection to an empty array (like clearcollDiklatsRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initDiklatsRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collDiklatsRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collDiklatsRelatedByPtkId = new PropelObjectCollection();
        $this->collDiklatsRelatedByPtkId->setModel('Diklat');
    }

    /**
     * Gets an array of Diklat objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Diklat[] List of Diklat objects
     * @throws PropelException
     */
    public function getDiklatsRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collDiklatsRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collDiklatsRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collDiklatsRelatedByPtkId) {
                // return empty collection
                $this->initDiklatsRelatedByPtkId();
            } else {
                $collDiklatsRelatedByPtkId = DiklatQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collDiklatsRelatedByPtkIdPartial && count($collDiklatsRelatedByPtkId)) {
                      $this->initDiklatsRelatedByPtkId(false);

                      foreach($collDiklatsRelatedByPtkId as $obj) {
                        if (false == $this->collDiklatsRelatedByPtkId->contains($obj)) {
                          $this->collDiklatsRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collDiklatsRelatedByPtkIdPartial = true;
                    }

                    $collDiklatsRelatedByPtkId->getInternalIterator()->rewind();
                    return $collDiklatsRelatedByPtkId;
                }

                if($partial && $this->collDiklatsRelatedByPtkId) {
                    foreach($this->collDiklatsRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collDiklatsRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collDiklatsRelatedByPtkId = $collDiklatsRelatedByPtkId;
                $this->collDiklatsRelatedByPtkIdPartial = false;
            }
        }

        return $this->collDiklatsRelatedByPtkId;
    }

    /**
     * Sets a collection of DiklatRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $diklatsRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setDiklatsRelatedByPtkId(PropelCollection $diklatsRelatedByPtkId, PropelPDO $con = null)
    {
        $diklatsRelatedByPtkIdToDelete = $this->getDiklatsRelatedByPtkId(new Criteria(), $con)->diff($diklatsRelatedByPtkId);

        $this->diklatsRelatedByPtkIdScheduledForDeletion = unserialize(serialize($diklatsRelatedByPtkIdToDelete));

        foreach ($diklatsRelatedByPtkIdToDelete as $diklatRelatedByPtkIdRemoved) {
            $diklatRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collDiklatsRelatedByPtkId = null;
        foreach ($diklatsRelatedByPtkId as $diklatRelatedByPtkId) {
            $this->addDiklatRelatedByPtkId($diklatRelatedByPtkId);
        }

        $this->collDiklatsRelatedByPtkId = $diklatsRelatedByPtkId;
        $this->collDiklatsRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Diklat objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Diklat objects.
     * @throws PropelException
     */
    public function countDiklatsRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collDiklatsRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collDiklatsRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collDiklatsRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getDiklatsRelatedByPtkId());
            }
            $query = DiklatQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collDiklatsRelatedByPtkId);
    }

    /**
     * Method called to associate a Diklat object to this object
     * through the Diklat foreign key attribute.
     *
     * @param    Diklat $l Diklat
     * @return Ptk The current object (for fluent API support)
     */
    public function addDiklatRelatedByPtkId(Diklat $l)
    {
        if ($this->collDiklatsRelatedByPtkId === null) {
            $this->initDiklatsRelatedByPtkId();
            $this->collDiklatsRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collDiklatsRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddDiklatRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	DiklatRelatedByPtkId $diklatRelatedByPtkId The diklatRelatedByPtkId object to add.
     */
    protected function doAddDiklatRelatedByPtkId($diklatRelatedByPtkId)
    {
        $this->collDiklatsRelatedByPtkId[]= $diklatRelatedByPtkId;
        $diklatRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	DiklatRelatedByPtkId $diklatRelatedByPtkId The diklatRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removeDiklatRelatedByPtkId($diklatRelatedByPtkId)
    {
        if ($this->getDiklatsRelatedByPtkId()->contains($diklatRelatedByPtkId)) {
            $this->collDiklatsRelatedByPtkId->remove($this->collDiklatsRelatedByPtkId->search($diklatRelatedByPtkId));
            if (null === $this->diklatsRelatedByPtkIdScheduledForDeletion) {
                $this->diklatsRelatedByPtkIdScheduledForDeletion = clone $this->collDiklatsRelatedByPtkId;
                $this->diklatsRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->diklatsRelatedByPtkIdScheduledForDeletion[]= clone $diklatRelatedByPtkId;
            $diklatRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related DiklatsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Diklat[] List of Diklat objects
     */
    public function getDiklatsRelatedByPtkIdJoinJenisDiklatRelatedByJenisDiklatId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = DiklatQuery::create(null, $criteria);
        $query->joinWith('JenisDiklatRelatedByJenisDiklatId', $join_behavior);

        return $this->getDiklatsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related DiklatsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Diklat[] List of Diklat objects
     */
    public function getDiklatsRelatedByPtkIdJoinJenisDiklatRelatedByJenisDiklatId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = DiklatQuery::create(null, $criteria);
        $query->joinWith('JenisDiklatRelatedByJenisDiklatId', $join_behavior);

        return $this->getDiklatsRelatedByPtkId($query, $con);
    }

    /**
     * Clears out the collPtkBarusRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addPtkBarusRelatedByPtkId()
     */
    public function clearPtkBarusRelatedByPtkId()
    {
        $this->collPtkBarusRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collPtkBarusRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPtkBarusRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPtkBarusRelatedByPtkId($v = true)
    {
        $this->collPtkBarusRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collPtkBarusRelatedByPtkId collection.
     *
     * By default this just sets the collPtkBarusRelatedByPtkId collection to an empty array (like clearcollPtkBarusRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPtkBarusRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collPtkBarusRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collPtkBarusRelatedByPtkId = new PropelObjectCollection();
        $this->collPtkBarusRelatedByPtkId->setModel('PtkBaru');
    }

    /**
     * Gets an array of PtkBaru objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|PtkBaru[] List of PtkBaru objects
     * @throws PropelException
     */
    public function getPtkBarusRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPtkBarusRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collPtkBarusRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPtkBarusRelatedByPtkId) {
                // return empty collection
                $this->initPtkBarusRelatedByPtkId();
            } else {
                $collPtkBarusRelatedByPtkId = PtkBaruQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPtkBarusRelatedByPtkIdPartial && count($collPtkBarusRelatedByPtkId)) {
                      $this->initPtkBarusRelatedByPtkId(false);

                      foreach($collPtkBarusRelatedByPtkId as $obj) {
                        if (false == $this->collPtkBarusRelatedByPtkId->contains($obj)) {
                          $this->collPtkBarusRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collPtkBarusRelatedByPtkIdPartial = true;
                    }

                    $collPtkBarusRelatedByPtkId->getInternalIterator()->rewind();
                    return $collPtkBarusRelatedByPtkId;
                }

                if($partial && $this->collPtkBarusRelatedByPtkId) {
                    foreach($this->collPtkBarusRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collPtkBarusRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collPtkBarusRelatedByPtkId = $collPtkBarusRelatedByPtkId;
                $this->collPtkBarusRelatedByPtkIdPartial = false;
            }
        }

        return $this->collPtkBarusRelatedByPtkId;
    }

    /**
     * Sets a collection of PtkBaruRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $ptkBarusRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setPtkBarusRelatedByPtkId(PropelCollection $ptkBarusRelatedByPtkId, PropelPDO $con = null)
    {
        $ptkBarusRelatedByPtkIdToDelete = $this->getPtkBarusRelatedByPtkId(new Criteria(), $con)->diff($ptkBarusRelatedByPtkId);

        $this->ptkBarusRelatedByPtkIdScheduledForDeletion = unserialize(serialize($ptkBarusRelatedByPtkIdToDelete));

        foreach ($ptkBarusRelatedByPtkIdToDelete as $ptkBaruRelatedByPtkIdRemoved) {
            $ptkBaruRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collPtkBarusRelatedByPtkId = null;
        foreach ($ptkBarusRelatedByPtkId as $ptkBaruRelatedByPtkId) {
            $this->addPtkBaruRelatedByPtkId($ptkBaruRelatedByPtkId);
        }

        $this->collPtkBarusRelatedByPtkId = $ptkBarusRelatedByPtkId;
        $this->collPtkBarusRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PtkBaru objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related PtkBaru objects.
     * @throws PropelException
     */
    public function countPtkBarusRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPtkBarusRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collPtkBarusRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPtkBarusRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPtkBarusRelatedByPtkId());
            }
            $query = PtkBaruQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collPtkBarusRelatedByPtkId);
    }

    /**
     * Method called to associate a PtkBaru object to this object
     * through the PtkBaru foreign key attribute.
     *
     * @param    PtkBaru $l PtkBaru
     * @return Ptk The current object (for fluent API support)
     */
    public function addPtkBaruRelatedByPtkId(PtkBaru $l)
    {
        if ($this->collPtkBarusRelatedByPtkId === null) {
            $this->initPtkBarusRelatedByPtkId();
            $this->collPtkBarusRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collPtkBarusRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPtkBaruRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	PtkBaruRelatedByPtkId $ptkBaruRelatedByPtkId The ptkBaruRelatedByPtkId object to add.
     */
    protected function doAddPtkBaruRelatedByPtkId($ptkBaruRelatedByPtkId)
    {
        $this->collPtkBarusRelatedByPtkId[]= $ptkBaruRelatedByPtkId;
        $ptkBaruRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	PtkBaruRelatedByPtkId $ptkBaruRelatedByPtkId The ptkBaruRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removePtkBaruRelatedByPtkId($ptkBaruRelatedByPtkId)
    {
        if ($this->getPtkBarusRelatedByPtkId()->contains($ptkBaruRelatedByPtkId)) {
            $this->collPtkBarusRelatedByPtkId->remove($this->collPtkBarusRelatedByPtkId->search($ptkBaruRelatedByPtkId));
            if (null === $this->ptkBarusRelatedByPtkIdScheduledForDeletion) {
                $this->ptkBarusRelatedByPtkIdScheduledForDeletion = clone $this->collPtkBarusRelatedByPtkId;
                $this->ptkBarusRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->ptkBarusRelatedByPtkIdScheduledForDeletion[]= $ptkBaruRelatedByPtkId;
            $ptkBaruRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PtkBarusRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkBaru[] List of PtkBaru objects
     */
    public function getPtkBarusRelatedByPtkIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkBaruQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPtkBarusRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PtkBarusRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkBaru[] List of PtkBaru objects
     */
    public function getPtkBarusRelatedByPtkIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkBaruQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPtkBarusRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PtkBarusRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkBaru[] List of PtkBaru objects
     */
    public function getPtkBarusRelatedByPtkIdJoinTahunAjaranRelatedByTahunAjaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkBaruQuery::create(null, $criteria);
        $query->joinWith('TahunAjaranRelatedByTahunAjaranId', $join_behavior);

        return $this->getPtkBarusRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PtkBarusRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkBaru[] List of PtkBaru objects
     */
    public function getPtkBarusRelatedByPtkIdJoinTahunAjaranRelatedByTahunAjaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkBaruQuery::create(null, $criteria);
        $query->joinWith('TahunAjaranRelatedByTahunAjaranId', $join_behavior);

        return $this->getPtkBarusRelatedByPtkId($query, $con);
    }

    /**
     * Clears out the collPtkBarusRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addPtkBarusRelatedByPtkId()
     */
    public function clearPtkBarusRelatedByPtkId()
    {
        $this->collPtkBarusRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collPtkBarusRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPtkBarusRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPtkBarusRelatedByPtkId($v = true)
    {
        $this->collPtkBarusRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collPtkBarusRelatedByPtkId collection.
     *
     * By default this just sets the collPtkBarusRelatedByPtkId collection to an empty array (like clearcollPtkBarusRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPtkBarusRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collPtkBarusRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collPtkBarusRelatedByPtkId = new PropelObjectCollection();
        $this->collPtkBarusRelatedByPtkId->setModel('PtkBaru');
    }

    /**
     * Gets an array of PtkBaru objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|PtkBaru[] List of PtkBaru objects
     * @throws PropelException
     */
    public function getPtkBarusRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPtkBarusRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collPtkBarusRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPtkBarusRelatedByPtkId) {
                // return empty collection
                $this->initPtkBarusRelatedByPtkId();
            } else {
                $collPtkBarusRelatedByPtkId = PtkBaruQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPtkBarusRelatedByPtkIdPartial && count($collPtkBarusRelatedByPtkId)) {
                      $this->initPtkBarusRelatedByPtkId(false);

                      foreach($collPtkBarusRelatedByPtkId as $obj) {
                        if (false == $this->collPtkBarusRelatedByPtkId->contains($obj)) {
                          $this->collPtkBarusRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collPtkBarusRelatedByPtkIdPartial = true;
                    }

                    $collPtkBarusRelatedByPtkId->getInternalIterator()->rewind();
                    return $collPtkBarusRelatedByPtkId;
                }

                if($partial && $this->collPtkBarusRelatedByPtkId) {
                    foreach($this->collPtkBarusRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collPtkBarusRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collPtkBarusRelatedByPtkId = $collPtkBarusRelatedByPtkId;
                $this->collPtkBarusRelatedByPtkIdPartial = false;
            }
        }

        return $this->collPtkBarusRelatedByPtkId;
    }

    /**
     * Sets a collection of PtkBaruRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $ptkBarusRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setPtkBarusRelatedByPtkId(PropelCollection $ptkBarusRelatedByPtkId, PropelPDO $con = null)
    {
        $ptkBarusRelatedByPtkIdToDelete = $this->getPtkBarusRelatedByPtkId(new Criteria(), $con)->diff($ptkBarusRelatedByPtkId);

        $this->ptkBarusRelatedByPtkIdScheduledForDeletion = unserialize(serialize($ptkBarusRelatedByPtkIdToDelete));

        foreach ($ptkBarusRelatedByPtkIdToDelete as $ptkBaruRelatedByPtkIdRemoved) {
            $ptkBaruRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collPtkBarusRelatedByPtkId = null;
        foreach ($ptkBarusRelatedByPtkId as $ptkBaruRelatedByPtkId) {
            $this->addPtkBaruRelatedByPtkId($ptkBaruRelatedByPtkId);
        }

        $this->collPtkBarusRelatedByPtkId = $ptkBarusRelatedByPtkId;
        $this->collPtkBarusRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PtkBaru objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related PtkBaru objects.
     * @throws PropelException
     */
    public function countPtkBarusRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPtkBarusRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collPtkBarusRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPtkBarusRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPtkBarusRelatedByPtkId());
            }
            $query = PtkBaruQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collPtkBarusRelatedByPtkId);
    }

    /**
     * Method called to associate a PtkBaru object to this object
     * through the PtkBaru foreign key attribute.
     *
     * @param    PtkBaru $l PtkBaru
     * @return Ptk The current object (for fluent API support)
     */
    public function addPtkBaruRelatedByPtkId(PtkBaru $l)
    {
        if ($this->collPtkBarusRelatedByPtkId === null) {
            $this->initPtkBarusRelatedByPtkId();
            $this->collPtkBarusRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collPtkBarusRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPtkBaruRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	PtkBaruRelatedByPtkId $ptkBaruRelatedByPtkId The ptkBaruRelatedByPtkId object to add.
     */
    protected function doAddPtkBaruRelatedByPtkId($ptkBaruRelatedByPtkId)
    {
        $this->collPtkBarusRelatedByPtkId[]= $ptkBaruRelatedByPtkId;
        $ptkBaruRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	PtkBaruRelatedByPtkId $ptkBaruRelatedByPtkId The ptkBaruRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removePtkBaruRelatedByPtkId($ptkBaruRelatedByPtkId)
    {
        if ($this->getPtkBarusRelatedByPtkId()->contains($ptkBaruRelatedByPtkId)) {
            $this->collPtkBarusRelatedByPtkId->remove($this->collPtkBarusRelatedByPtkId->search($ptkBaruRelatedByPtkId));
            if (null === $this->ptkBarusRelatedByPtkIdScheduledForDeletion) {
                $this->ptkBarusRelatedByPtkIdScheduledForDeletion = clone $this->collPtkBarusRelatedByPtkId;
                $this->ptkBarusRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->ptkBarusRelatedByPtkIdScheduledForDeletion[]= $ptkBaruRelatedByPtkId;
            $ptkBaruRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PtkBarusRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkBaru[] List of PtkBaru objects
     */
    public function getPtkBarusRelatedByPtkIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkBaruQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPtkBarusRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PtkBarusRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkBaru[] List of PtkBaru objects
     */
    public function getPtkBarusRelatedByPtkIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkBaruQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPtkBarusRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PtkBarusRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkBaru[] List of PtkBaru objects
     */
    public function getPtkBarusRelatedByPtkIdJoinTahunAjaranRelatedByTahunAjaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkBaruQuery::create(null, $criteria);
        $query->joinWith('TahunAjaranRelatedByTahunAjaranId', $join_behavior);

        return $this->getPtkBarusRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related PtkBarusRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PtkBaru[] List of PtkBaru objects
     */
    public function getPtkBarusRelatedByPtkIdJoinTahunAjaranRelatedByTahunAjaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkBaruQuery::create(null, $criteria);
        $query->joinWith('TahunAjaranRelatedByTahunAjaranId', $join_behavior);

        return $this->getPtkBarusRelatedByPtkId($query, $con);
    }

    /**
     * Clears out the collTunjangansRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addTunjangansRelatedByPtkId()
     */
    public function clearTunjangansRelatedByPtkId()
    {
        $this->collTunjangansRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collTunjangansRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collTunjangansRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialTunjangansRelatedByPtkId($v = true)
    {
        $this->collTunjangansRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collTunjangansRelatedByPtkId collection.
     *
     * By default this just sets the collTunjangansRelatedByPtkId collection to an empty array (like clearcollTunjangansRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initTunjangansRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collTunjangansRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collTunjangansRelatedByPtkId = new PropelObjectCollection();
        $this->collTunjangansRelatedByPtkId->setModel('Tunjangan');
    }

    /**
     * Gets an array of Tunjangan objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Tunjangan[] List of Tunjangan objects
     * @throws PropelException
     */
    public function getTunjangansRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collTunjangansRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collTunjangansRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collTunjangansRelatedByPtkId) {
                // return empty collection
                $this->initTunjangansRelatedByPtkId();
            } else {
                $collTunjangansRelatedByPtkId = TunjanganQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collTunjangansRelatedByPtkIdPartial && count($collTunjangansRelatedByPtkId)) {
                      $this->initTunjangansRelatedByPtkId(false);

                      foreach($collTunjangansRelatedByPtkId as $obj) {
                        if (false == $this->collTunjangansRelatedByPtkId->contains($obj)) {
                          $this->collTunjangansRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collTunjangansRelatedByPtkIdPartial = true;
                    }

                    $collTunjangansRelatedByPtkId->getInternalIterator()->rewind();
                    return $collTunjangansRelatedByPtkId;
                }

                if($partial && $this->collTunjangansRelatedByPtkId) {
                    foreach($this->collTunjangansRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collTunjangansRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collTunjangansRelatedByPtkId = $collTunjangansRelatedByPtkId;
                $this->collTunjangansRelatedByPtkIdPartial = false;
            }
        }

        return $this->collTunjangansRelatedByPtkId;
    }

    /**
     * Sets a collection of TunjanganRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $tunjangansRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setTunjangansRelatedByPtkId(PropelCollection $tunjangansRelatedByPtkId, PropelPDO $con = null)
    {
        $tunjangansRelatedByPtkIdToDelete = $this->getTunjangansRelatedByPtkId(new Criteria(), $con)->diff($tunjangansRelatedByPtkId);

        $this->tunjangansRelatedByPtkIdScheduledForDeletion = unserialize(serialize($tunjangansRelatedByPtkIdToDelete));

        foreach ($tunjangansRelatedByPtkIdToDelete as $tunjanganRelatedByPtkIdRemoved) {
            $tunjanganRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collTunjangansRelatedByPtkId = null;
        foreach ($tunjangansRelatedByPtkId as $tunjanganRelatedByPtkId) {
            $this->addTunjanganRelatedByPtkId($tunjanganRelatedByPtkId);
        }

        $this->collTunjangansRelatedByPtkId = $tunjangansRelatedByPtkId;
        $this->collTunjangansRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Tunjangan objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Tunjangan objects.
     * @throws PropelException
     */
    public function countTunjangansRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collTunjangansRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collTunjangansRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collTunjangansRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getTunjangansRelatedByPtkId());
            }
            $query = TunjanganQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collTunjangansRelatedByPtkId);
    }

    /**
     * Method called to associate a Tunjangan object to this object
     * through the Tunjangan foreign key attribute.
     *
     * @param    Tunjangan $l Tunjangan
     * @return Ptk The current object (for fluent API support)
     */
    public function addTunjanganRelatedByPtkId(Tunjangan $l)
    {
        if ($this->collTunjangansRelatedByPtkId === null) {
            $this->initTunjangansRelatedByPtkId();
            $this->collTunjangansRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collTunjangansRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddTunjanganRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	TunjanganRelatedByPtkId $tunjanganRelatedByPtkId The tunjanganRelatedByPtkId object to add.
     */
    protected function doAddTunjanganRelatedByPtkId($tunjanganRelatedByPtkId)
    {
        $this->collTunjangansRelatedByPtkId[]= $tunjanganRelatedByPtkId;
        $tunjanganRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	TunjanganRelatedByPtkId $tunjanganRelatedByPtkId The tunjanganRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removeTunjanganRelatedByPtkId($tunjanganRelatedByPtkId)
    {
        if ($this->getTunjangansRelatedByPtkId()->contains($tunjanganRelatedByPtkId)) {
            $this->collTunjangansRelatedByPtkId->remove($this->collTunjangansRelatedByPtkId->search($tunjanganRelatedByPtkId));
            if (null === $this->tunjangansRelatedByPtkIdScheduledForDeletion) {
                $this->tunjangansRelatedByPtkIdScheduledForDeletion = clone $this->collTunjangansRelatedByPtkId;
                $this->tunjangansRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->tunjangansRelatedByPtkIdScheduledForDeletion[]= clone $tunjanganRelatedByPtkId;
            $tunjanganRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related TunjangansRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Tunjangan[] List of Tunjangan objects
     */
    public function getTunjangansRelatedByPtkIdJoinJenisTunjanganRelatedByJenisTunjanganId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = TunjanganQuery::create(null, $criteria);
        $query->joinWith('JenisTunjanganRelatedByJenisTunjanganId', $join_behavior);

        return $this->getTunjangansRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related TunjangansRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Tunjangan[] List of Tunjangan objects
     */
    public function getTunjangansRelatedByPtkIdJoinJenisTunjanganRelatedByJenisTunjanganId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = TunjanganQuery::create(null, $criteria);
        $query->joinWith('JenisTunjanganRelatedByJenisTunjanganId', $join_behavior);

        return $this->getTunjangansRelatedByPtkId($query, $con);
    }

    /**
     * Clears out the collTunjangansRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addTunjangansRelatedByPtkId()
     */
    public function clearTunjangansRelatedByPtkId()
    {
        $this->collTunjangansRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collTunjangansRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collTunjangansRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialTunjangansRelatedByPtkId($v = true)
    {
        $this->collTunjangansRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collTunjangansRelatedByPtkId collection.
     *
     * By default this just sets the collTunjangansRelatedByPtkId collection to an empty array (like clearcollTunjangansRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initTunjangansRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collTunjangansRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collTunjangansRelatedByPtkId = new PropelObjectCollection();
        $this->collTunjangansRelatedByPtkId->setModel('Tunjangan');
    }

    /**
     * Gets an array of Tunjangan objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Tunjangan[] List of Tunjangan objects
     * @throws PropelException
     */
    public function getTunjangansRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collTunjangansRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collTunjangansRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collTunjangansRelatedByPtkId) {
                // return empty collection
                $this->initTunjangansRelatedByPtkId();
            } else {
                $collTunjangansRelatedByPtkId = TunjanganQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collTunjangansRelatedByPtkIdPartial && count($collTunjangansRelatedByPtkId)) {
                      $this->initTunjangansRelatedByPtkId(false);

                      foreach($collTunjangansRelatedByPtkId as $obj) {
                        if (false == $this->collTunjangansRelatedByPtkId->contains($obj)) {
                          $this->collTunjangansRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collTunjangansRelatedByPtkIdPartial = true;
                    }

                    $collTunjangansRelatedByPtkId->getInternalIterator()->rewind();
                    return $collTunjangansRelatedByPtkId;
                }

                if($partial && $this->collTunjangansRelatedByPtkId) {
                    foreach($this->collTunjangansRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collTunjangansRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collTunjangansRelatedByPtkId = $collTunjangansRelatedByPtkId;
                $this->collTunjangansRelatedByPtkIdPartial = false;
            }
        }

        return $this->collTunjangansRelatedByPtkId;
    }

    /**
     * Sets a collection of TunjanganRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $tunjangansRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setTunjangansRelatedByPtkId(PropelCollection $tunjangansRelatedByPtkId, PropelPDO $con = null)
    {
        $tunjangansRelatedByPtkIdToDelete = $this->getTunjangansRelatedByPtkId(new Criteria(), $con)->diff($tunjangansRelatedByPtkId);

        $this->tunjangansRelatedByPtkIdScheduledForDeletion = unserialize(serialize($tunjangansRelatedByPtkIdToDelete));

        foreach ($tunjangansRelatedByPtkIdToDelete as $tunjanganRelatedByPtkIdRemoved) {
            $tunjanganRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collTunjangansRelatedByPtkId = null;
        foreach ($tunjangansRelatedByPtkId as $tunjanganRelatedByPtkId) {
            $this->addTunjanganRelatedByPtkId($tunjanganRelatedByPtkId);
        }

        $this->collTunjangansRelatedByPtkId = $tunjangansRelatedByPtkId;
        $this->collTunjangansRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Tunjangan objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Tunjangan objects.
     * @throws PropelException
     */
    public function countTunjangansRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collTunjangansRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collTunjangansRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collTunjangansRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getTunjangansRelatedByPtkId());
            }
            $query = TunjanganQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collTunjangansRelatedByPtkId);
    }

    /**
     * Method called to associate a Tunjangan object to this object
     * through the Tunjangan foreign key attribute.
     *
     * @param    Tunjangan $l Tunjangan
     * @return Ptk The current object (for fluent API support)
     */
    public function addTunjanganRelatedByPtkId(Tunjangan $l)
    {
        if ($this->collTunjangansRelatedByPtkId === null) {
            $this->initTunjangansRelatedByPtkId();
            $this->collTunjangansRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collTunjangansRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddTunjanganRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	TunjanganRelatedByPtkId $tunjanganRelatedByPtkId The tunjanganRelatedByPtkId object to add.
     */
    protected function doAddTunjanganRelatedByPtkId($tunjanganRelatedByPtkId)
    {
        $this->collTunjangansRelatedByPtkId[]= $tunjanganRelatedByPtkId;
        $tunjanganRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	TunjanganRelatedByPtkId $tunjanganRelatedByPtkId The tunjanganRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removeTunjanganRelatedByPtkId($tunjanganRelatedByPtkId)
    {
        if ($this->getTunjangansRelatedByPtkId()->contains($tunjanganRelatedByPtkId)) {
            $this->collTunjangansRelatedByPtkId->remove($this->collTunjangansRelatedByPtkId->search($tunjanganRelatedByPtkId));
            if (null === $this->tunjangansRelatedByPtkIdScheduledForDeletion) {
                $this->tunjangansRelatedByPtkIdScheduledForDeletion = clone $this->collTunjangansRelatedByPtkId;
                $this->tunjangansRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->tunjangansRelatedByPtkIdScheduledForDeletion[]= clone $tunjanganRelatedByPtkId;
            $tunjanganRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related TunjangansRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Tunjangan[] List of Tunjangan objects
     */
    public function getTunjangansRelatedByPtkIdJoinJenisTunjanganRelatedByJenisTunjanganId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = TunjanganQuery::create(null, $criteria);
        $query->joinWith('JenisTunjanganRelatedByJenisTunjanganId', $join_behavior);

        return $this->getTunjangansRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related TunjangansRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Tunjangan[] List of Tunjangan objects
     */
    public function getTunjangansRelatedByPtkIdJoinJenisTunjanganRelatedByJenisTunjanganId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = TunjanganQuery::create(null, $criteria);
        $query->joinWith('JenisTunjanganRelatedByJenisTunjanganId', $join_behavior);

        return $this->getTunjangansRelatedByPtkId($query, $con);
    }

    /**
     * Clears out the collRwySertifikasisRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addRwySertifikasisRelatedByPtkId()
     */
    public function clearRwySertifikasisRelatedByPtkId()
    {
        $this->collRwySertifikasisRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collRwySertifikasisRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collRwySertifikasisRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialRwySertifikasisRelatedByPtkId($v = true)
    {
        $this->collRwySertifikasisRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collRwySertifikasisRelatedByPtkId collection.
     *
     * By default this just sets the collRwySertifikasisRelatedByPtkId collection to an empty array (like clearcollRwySertifikasisRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRwySertifikasisRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collRwySertifikasisRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collRwySertifikasisRelatedByPtkId = new PropelObjectCollection();
        $this->collRwySertifikasisRelatedByPtkId->setModel('RwySertifikasi');
    }

    /**
     * Gets an array of RwySertifikasi objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RwySertifikasi[] List of RwySertifikasi objects
     * @throws PropelException
     */
    public function getRwySertifikasisRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRwySertifikasisRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collRwySertifikasisRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRwySertifikasisRelatedByPtkId) {
                // return empty collection
                $this->initRwySertifikasisRelatedByPtkId();
            } else {
                $collRwySertifikasisRelatedByPtkId = RwySertifikasiQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRwySertifikasisRelatedByPtkIdPartial && count($collRwySertifikasisRelatedByPtkId)) {
                      $this->initRwySertifikasisRelatedByPtkId(false);

                      foreach($collRwySertifikasisRelatedByPtkId as $obj) {
                        if (false == $this->collRwySertifikasisRelatedByPtkId->contains($obj)) {
                          $this->collRwySertifikasisRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collRwySertifikasisRelatedByPtkIdPartial = true;
                    }

                    $collRwySertifikasisRelatedByPtkId->getInternalIterator()->rewind();
                    return $collRwySertifikasisRelatedByPtkId;
                }

                if($partial && $this->collRwySertifikasisRelatedByPtkId) {
                    foreach($this->collRwySertifikasisRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collRwySertifikasisRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collRwySertifikasisRelatedByPtkId = $collRwySertifikasisRelatedByPtkId;
                $this->collRwySertifikasisRelatedByPtkIdPartial = false;
            }
        }

        return $this->collRwySertifikasisRelatedByPtkId;
    }

    /**
     * Sets a collection of RwySertifikasiRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rwySertifikasisRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setRwySertifikasisRelatedByPtkId(PropelCollection $rwySertifikasisRelatedByPtkId, PropelPDO $con = null)
    {
        $rwySertifikasisRelatedByPtkIdToDelete = $this->getRwySertifikasisRelatedByPtkId(new Criteria(), $con)->diff($rwySertifikasisRelatedByPtkId);

        $this->rwySertifikasisRelatedByPtkIdScheduledForDeletion = unserialize(serialize($rwySertifikasisRelatedByPtkIdToDelete));

        foreach ($rwySertifikasisRelatedByPtkIdToDelete as $rwySertifikasiRelatedByPtkIdRemoved) {
            $rwySertifikasiRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collRwySertifikasisRelatedByPtkId = null;
        foreach ($rwySertifikasisRelatedByPtkId as $rwySertifikasiRelatedByPtkId) {
            $this->addRwySertifikasiRelatedByPtkId($rwySertifikasiRelatedByPtkId);
        }

        $this->collRwySertifikasisRelatedByPtkId = $rwySertifikasisRelatedByPtkId;
        $this->collRwySertifikasisRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RwySertifikasi objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RwySertifikasi objects.
     * @throws PropelException
     */
    public function countRwySertifikasisRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRwySertifikasisRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collRwySertifikasisRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRwySertifikasisRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getRwySertifikasisRelatedByPtkId());
            }
            $query = RwySertifikasiQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collRwySertifikasisRelatedByPtkId);
    }

    /**
     * Method called to associate a RwySertifikasi object to this object
     * through the RwySertifikasi foreign key attribute.
     *
     * @param    RwySertifikasi $l RwySertifikasi
     * @return Ptk The current object (for fluent API support)
     */
    public function addRwySertifikasiRelatedByPtkId(RwySertifikasi $l)
    {
        if ($this->collRwySertifikasisRelatedByPtkId === null) {
            $this->initRwySertifikasisRelatedByPtkId();
            $this->collRwySertifikasisRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collRwySertifikasisRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRwySertifikasiRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	RwySertifikasiRelatedByPtkId $rwySertifikasiRelatedByPtkId The rwySertifikasiRelatedByPtkId object to add.
     */
    protected function doAddRwySertifikasiRelatedByPtkId($rwySertifikasiRelatedByPtkId)
    {
        $this->collRwySertifikasisRelatedByPtkId[]= $rwySertifikasiRelatedByPtkId;
        $rwySertifikasiRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	RwySertifikasiRelatedByPtkId $rwySertifikasiRelatedByPtkId The rwySertifikasiRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removeRwySertifikasiRelatedByPtkId($rwySertifikasiRelatedByPtkId)
    {
        if ($this->getRwySertifikasisRelatedByPtkId()->contains($rwySertifikasiRelatedByPtkId)) {
            $this->collRwySertifikasisRelatedByPtkId->remove($this->collRwySertifikasisRelatedByPtkId->search($rwySertifikasiRelatedByPtkId));
            if (null === $this->rwySertifikasisRelatedByPtkIdScheduledForDeletion) {
                $this->rwySertifikasisRelatedByPtkIdScheduledForDeletion = clone $this->collRwySertifikasisRelatedByPtkId;
                $this->rwySertifikasisRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->rwySertifikasisRelatedByPtkIdScheduledForDeletion[]= clone $rwySertifikasiRelatedByPtkId;
            $rwySertifikasiRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RwySertifikasisRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwySertifikasi[] List of RwySertifikasi objects
     */
    public function getRwySertifikasisRelatedByPtkIdJoinBidangStudiRelatedByBidangStudiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwySertifikasiQuery::create(null, $criteria);
        $query->joinWith('BidangStudiRelatedByBidangStudiId', $join_behavior);

        return $this->getRwySertifikasisRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RwySertifikasisRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwySertifikasi[] List of RwySertifikasi objects
     */
    public function getRwySertifikasisRelatedByPtkIdJoinBidangStudiRelatedByBidangStudiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwySertifikasiQuery::create(null, $criteria);
        $query->joinWith('BidangStudiRelatedByBidangStudiId', $join_behavior);

        return $this->getRwySertifikasisRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RwySertifikasisRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwySertifikasi[] List of RwySertifikasi objects
     */
    public function getRwySertifikasisRelatedByPtkIdJoinJenisSertifikasiRelatedByIdJenisSertifikasi($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwySertifikasiQuery::create(null, $criteria);
        $query->joinWith('JenisSertifikasiRelatedByIdJenisSertifikasi', $join_behavior);

        return $this->getRwySertifikasisRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RwySertifikasisRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwySertifikasi[] List of RwySertifikasi objects
     */
    public function getRwySertifikasisRelatedByPtkIdJoinJenisSertifikasiRelatedByIdJenisSertifikasi($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwySertifikasiQuery::create(null, $criteria);
        $query->joinWith('JenisSertifikasiRelatedByIdJenisSertifikasi', $join_behavior);

        return $this->getRwySertifikasisRelatedByPtkId($query, $con);
    }

    /**
     * Clears out the collRwySertifikasisRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addRwySertifikasisRelatedByPtkId()
     */
    public function clearRwySertifikasisRelatedByPtkId()
    {
        $this->collRwySertifikasisRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collRwySertifikasisRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collRwySertifikasisRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialRwySertifikasisRelatedByPtkId($v = true)
    {
        $this->collRwySertifikasisRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collRwySertifikasisRelatedByPtkId collection.
     *
     * By default this just sets the collRwySertifikasisRelatedByPtkId collection to an empty array (like clearcollRwySertifikasisRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRwySertifikasisRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collRwySertifikasisRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collRwySertifikasisRelatedByPtkId = new PropelObjectCollection();
        $this->collRwySertifikasisRelatedByPtkId->setModel('RwySertifikasi');
    }

    /**
     * Gets an array of RwySertifikasi objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RwySertifikasi[] List of RwySertifikasi objects
     * @throws PropelException
     */
    public function getRwySertifikasisRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRwySertifikasisRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collRwySertifikasisRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRwySertifikasisRelatedByPtkId) {
                // return empty collection
                $this->initRwySertifikasisRelatedByPtkId();
            } else {
                $collRwySertifikasisRelatedByPtkId = RwySertifikasiQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRwySertifikasisRelatedByPtkIdPartial && count($collRwySertifikasisRelatedByPtkId)) {
                      $this->initRwySertifikasisRelatedByPtkId(false);

                      foreach($collRwySertifikasisRelatedByPtkId as $obj) {
                        if (false == $this->collRwySertifikasisRelatedByPtkId->contains($obj)) {
                          $this->collRwySertifikasisRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collRwySertifikasisRelatedByPtkIdPartial = true;
                    }

                    $collRwySertifikasisRelatedByPtkId->getInternalIterator()->rewind();
                    return $collRwySertifikasisRelatedByPtkId;
                }

                if($partial && $this->collRwySertifikasisRelatedByPtkId) {
                    foreach($this->collRwySertifikasisRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collRwySertifikasisRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collRwySertifikasisRelatedByPtkId = $collRwySertifikasisRelatedByPtkId;
                $this->collRwySertifikasisRelatedByPtkIdPartial = false;
            }
        }

        return $this->collRwySertifikasisRelatedByPtkId;
    }

    /**
     * Sets a collection of RwySertifikasiRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rwySertifikasisRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setRwySertifikasisRelatedByPtkId(PropelCollection $rwySertifikasisRelatedByPtkId, PropelPDO $con = null)
    {
        $rwySertifikasisRelatedByPtkIdToDelete = $this->getRwySertifikasisRelatedByPtkId(new Criteria(), $con)->diff($rwySertifikasisRelatedByPtkId);

        $this->rwySertifikasisRelatedByPtkIdScheduledForDeletion = unserialize(serialize($rwySertifikasisRelatedByPtkIdToDelete));

        foreach ($rwySertifikasisRelatedByPtkIdToDelete as $rwySertifikasiRelatedByPtkIdRemoved) {
            $rwySertifikasiRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collRwySertifikasisRelatedByPtkId = null;
        foreach ($rwySertifikasisRelatedByPtkId as $rwySertifikasiRelatedByPtkId) {
            $this->addRwySertifikasiRelatedByPtkId($rwySertifikasiRelatedByPtkId);
        }

        $this->collRwySertifikasisRelatedByPtkId = $rwySertifikasisRelatedByPtkId;
        $this->collRwySertifikasisRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RwySertifikasi objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RwySertifikasi objects.
     * @throws PropelException
     */
    public function countRwySertifikasisRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRwySertifikasisRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collRwySertifikasisRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRwySertifikasisRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getRwySertifikasisRelatedByPtkId());
            }
            $query = RwySertifikasiQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collRwySertifikasisRelatedByPtkId);
    }

    /**
     * Method called to associate a RwySertifikasi object to this object
     * through the RwySertifikasi foreign key attribute.
     *
     * @param    RwySertifikasi $l RwySertifikasi
     * @return Ptk The current object (for fluent API support)
     */
    public function addRwySertifikasiRelatedByPtkId(RwySertifikasi $l)
    {
        if ($this->collRwySertifikasisRelatedByPtkId === null) {
            $this->initRwySertifikasisRelatedByPtkId();
            $this->collRwySertifikasisRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collRwySertifikasisRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRwySertifikasiRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	RwySertifikasiRelatedByPtkId $rwySertifikasiRelatedByPtkId The rwySertifikasiRelatedByPtkId object to add.
     */
    protected function doAddRwySertifikasiRelatedByPtkId($rwySertifikasiRelatedByPtkId)
    {
        $this->collRwySertifikasisRelatedByPtkId[]= $rwySertifikasiRelatedByPtkId;
        $rwySertifikasiRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	RwySertifikasiRelatedByPtkId $rwySertifikasiRelatedByPtkId The rwySertifikasiRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removeRwySertifikasiRelatedByPtkId($rwySertifikasiRelatedByPtkId)
    {
        if ($this->getRwySertifikasisRelatedByPtkId()->contains($rwySertifikasiRelatedByPtkId)) {
            $this->collRwySertifikasisRelatedByPtkId->remove($this->collRwySertifikasisRelatedByPtkId->search($rwySertifikasiRelatedByPtkId));
            if (null === $this->rwySertifikasisRelatedByPtkIdScheduledForDeletion) {
                $this->rwySertifikasisRelatedByPtkIdScheduledForDeletion = clone $this->collRwySertifikasisRelatedByPtkId;
                $this->rwySertifikasisRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->rwySertifikasisRelatedByPtkIdScheduledForDeletion[]= clone $rwySertifikasiRelatedByPtkId;
            $rwySertifikasiRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RwySertifikasisRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwySertifikasi[] List of RwySertifikasi objects
     */
    public function getRwySertifikasisRelatedByPtkIdJoinBidangStudiRelatedByBidangStudiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwySertifikasiQuery::create(null, $criteria);
        $query->joinWith('BidangStudiRelatedByBidangStudiId', $join_behavior);

        return $this->getRwySertifikasisRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RwySertifikasisRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwySertifikasi[] List of RwySertifikasi objects
     */
    public function getRwySertifikasisRelatedByPtkIdJoinBidangStudiRelatedByBidangStudiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwySertifikasiQuery::create(null, $criteria);
        $query->joinWith('BidangStudiRelatedByBidangStudiId', $join_behavior);

        return $this->getRwySertifikasisRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RwySertifikasisRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwySertifikasi[] List of RwySertifikasi objects
     */
    public function getRwySertifikasisRelatedByPtkIdJoinJenisSertifikasiRelatedByIdJenisSertifikasi($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwySertifikasiQuery::create(null, $criteria);
        $query->joinWith('JenisSertifikasiRelatedByIdJenisSertifikasi', $join_behavior);

        return $this->getRwySertifikasisRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RwySertifikasisRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwySertifikasi[] List of RwySertifikasi objects
     */
    public function getRwySertifikasisRelatedByPtkIdJoinJenisSertifikasiRelatedByIdJenisSertifikasi($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwySertifikasiQuery::create(null, $criteria);
        $query->joinWith('JenisSertifikasiRelatedByIdJenisSertifikasi', $join_behavior);

        return $this->getRwySertifikasisRelatedByPtkId($query, $con);
    }

    /**
     * Clears out the collTugasTambahansRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addTugasTambahansRelatedByPtkId()
     */
    public function clearTugasTambahansRelatedByPtkId()
    {
        $this->collTugasTambahansRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collTugasTambahansRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collTugasTambahansRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialTugasTambahansRelatedByPtkId($v = true)
    {
        $this->collTugasTambahansRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collTugasTambahansRelatedByPtkId collection.
     *
     * By default this just sets the collTugasTambahansRelatedByPtkId collection to an empty array (like clearcollTugasTambahansRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initTugasTambahansRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collTugasTambahansRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collTugasTambahansRelatedByPtkId = new PropelObjectCollection();
        $this->collTugasTambahansRelatedByPtkId->setModel('TugasTambahan');
    }

    /**
     * Gets an array of TugasTambahan objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|TugasTambahan[] List of TugasTambahan objects
     * @throws PropelException
     */
    public function getTugasTambahansRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collTugasTambahansRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collTugasTambahansRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collTugasTambahansRelatedByPtkId) {
                // return empty collection
                $this->initTugasTambahansRelatedByPtkId();
            } else {
                $collTugasTambahansRelatedByPtkId = TugasTambahanQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collTugasTambahansRelatedByPtkIdPartial && count($collTugasTambahansRelatedByPtkId)) {
                      $this->initTugasTambahansRelatedByPtkId(false);

                      foreach($collTugasTambahansRelatedByPtkId as $obj) {
                        if (false == $this->collTugasTambahansRelatedByPtkId->contains($obj)) {
                          $this->collTugasTambahansRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collTugasTambahansRelatedByPtkIdPartial = true;
                    }

                    $collTugasTambahansRelatedByPtkId->getInternalIterator()->rewind();
                    return $collTugasTambahansRelatedByPtkId;
                }

                if($partial && $this->collTugasTambahansRelatedByPtkId) {
                    foreach($this->collTugasTambahansRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collTugasTambahansRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collTugasTambahansRelatedByPtkId = $collTugasTambahansRelatedByPtkId;
                $this->collTugasTambahansRelatedByPtkIdPartial = false;
            }
        }

        return $this->collTugasTambahansRelatedByPtkId;
    }

    /**
     * Sets a collection of TugasTambahanRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $tugasTambahansRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setTugasTambahansRelatedByPtkId(PropelCollection $tugasTambahansRelatedByPtkId, PropelPDO $con = null)
    {
        $tugasTambahansRelatedByPtkIdToDelete = $this->getTugasTambahansRelatedByPtkId(new Criteria(), $con)->diff($tugasTambahansRelatedByPtkId);

        $this->tugasTambahansRelatedByPtkIdScheduledForDeletion = unserialize(serialize($tugasTambahansRelatedByPtkIdToDelete));

        foreach ($tugasTambahansRelatedByPtkIdToDelete as $tugasTambahanRelatedByPtkIdRemoved) {
            $tugasTambahanRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collTugasTambahansRelatedByPtkId = null;
        foreach ($tugasTambahansRelatedByPtkId as $tugasTambahanRelatedByPtkId) {
            $this->addTugasTambahanRelatedByPtkId($tugasTambahanRelatedByPtkId);
        }

        $this->collTugasTambahansRelatedByPtkId = $tugasTambahansRelatedByPtkId;
        $this->collTugasTambahansRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related TugasTambahan objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related TugasTambahan objects.
     * @throws PropelException
     */
    public function countTugasTambahansRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collTugasTambahansRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collTugasTambahansRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collTugasTambahansRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getTugasTambahansRelatedByPtkId());
            }
            $query = TugasTambahanQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collTugasTambahansRelatedByPtkId);
    }

    /**
     * Method called to associate a TugasTambahan object to this object
     * through the TugasTambahan foreign key attribute.
     *
     * @param    TugasTambahan $l TugasTambahan
     * @return Ptk The current object (for fluent API support)
     */
    public function addTugasTambahanRelatedByPtkId(TugasTambahan $l)
    {
        if ($this->collTugasTambahansRelatedByPtkId === null) {
            $this->initTugasTambahansRelatedByPtkId();
            $this->collTugasTambahansRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collTugasTambahansRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddTugasTambahanRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	TugasTambahanRelatedByPtkId $tugasTambahanRelatedByPtkId The tugasTambahanRelatedByPtkId object to add.
     */
    protected function doAddTugasTambahanRelatedByPtkId($tugasTambahanRelatedByPtkId)
    {
        $this->collTugasTambahansRelatedByPtkId[]= $tugasTambahanRelatedByPtkId;
        $tugasTambahanRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	TugasTambahanRelatedByPtkId $tugasTambahanRelatedByPtkId The tugasTambahanRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removeTugasTambahanRelatedByPtkId($tugasTambahanRelatedByPtkId)
    {
        if ($this->getTugasTambahansRelatedByPtkId()->contains($tugasTambahanRelatedByPtkId)) {
            $this->collTugasTambahansRelatedByPtkId->remove($this->collTugasTambahansRelatedByPtkId->search($tugasTambahanRelatedByPtkId));
            if (null === $this->tugasTambahansRelatedByPtkIdScheduledForDeletion) {
                $this->tugasTambahansRelatedByPtkIdScheduledForDeletion = clone $this->collTugasTambahansRelatedByPtkId;
                $this->tugasTambahansRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->tugasTambahansRelatedByPtkIdScheduledForDeletion[]= clone $tugasTambahanRelatedByPtkId;
            $tugasTambahanRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related TugasTambahansRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|TugasTambahan[] List of TugasTambahan objects
     */
    public function getTugasTambahansRelatedByPtkIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = TugasTambahanQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getTugasTambahansRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related TugasTambahansRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|TugasTambahan[] List of TugasTambahan objects
     */
    public function getTugasTambahansRelatedByPtkIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = TugasTambahanQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getTugasTambahansRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related TugasTambahansRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|TugasTambahan[] List of TugasTambahan objects
     */
    public function getTugasTambahansRelatedByPtkIdJoinJabatanTugasPtkRelatedByJabatanPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = TugasTambahanQuery::create(null, $criteria);
        $query->joinWith('JabatanTugasPtkRelatedByJabatanPtkId', $join_behavior);

        return $this->getTugasTambahansRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related TugasTambahansRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|TugasTambahan[] List of TugasTambahan objects
     */
    public function getTugasTambahansRelatedByPtkIdJoinJabatanTugasPtkRelatedByJabatanPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = TugasTambahanQuery::create(null, $criteria);
        $query->joinWith('JabatanTugasPtkRelatedByJabatanPtkId', $join_behavior);

        return $this->getTugasTambahansRelatedByPtkId($query, $con);
    }

    /**
     * Clears out the collTugasTambahansRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addTugasTambahansRelatedByPtkId()
     */
    public function clearTugasTambahansRelatedByPtkId()
    {
        $this->collTugasTambahansRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collTugasTambahansRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collTugasTambahansRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialTugasTambahansRelatedByPtkId($v = true)
    {
        $this->collTugasTambahansRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collTugasTambahansRelatedByPtkId collection.
     *
     * By default this just sets the collTugasTambahansRelatedByPtkId collection to an empty array (like clearcollTugasTambahansRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initTugasTambahansRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collTugasTambahansRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collTugasTambahansRelatedByPtkId = new PropelObjectCollection();
        $this->collTugasTambahansRelatedByPtkId->setModel('TugasTambahan');
    }

    /**
     * Gets an array of TugasTambahan objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|TugasTambahan[] List of TugasTambahan objects
     * @throws PropelException
     */
    public function getTugasTambahansRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collTugasTambahansRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collTugasTambahansRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collTugasTambahansRelatedByPtkId) {
                // return empty collection
                $this->initTugasTambahansRelatedByPtkId();
            } else {
                $collTugasTambahansRelatedByPtkId = TugasTambahanQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collTugasTambahansRelatedByPtkIdPartial && count($collTugasTambahansRelatedByPtkId)) {
                      $this->initTugasTambahansRelatedByPtkId(false);

                      foreach($collTugasTambahansRelatedByPtkId as $obj) {
                        if (false == $this->collTugasTambahansRelatedByPtkId->contains($obj)) {
                          $this->collTugasTambahansRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collTugasTambahansRelatedByPtkIdPartial = true;
                    }

                    $collTugasTambahansRelatedByPtkId->getInternalIterator()->rewind();
                    return $collTugasTambahansRelatedByPtkId;
                }

                if($partial && $this->collTugasTambahansRelatedByPtkId) {
                    foreach($this->collTugasTambahansRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collTugasTambahansRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collTugasTambahansRelatedByPtkId = $collTugasTambahansRelatedByPtkId;
                $this->collTugasTambahansRelatedByPtkIdPartial = false;
            }
        }

        return $this->collTugasTambahansRelatedByPtkId;
    }

    /**
     * Sets a collection of TugasTambahanRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $tugasTambahansRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setTugasTambahansRelatedByPtkId(PropelCollection $tugasTambahansRelatedByPtkId, PropelPDO $con = null)
    {
        $tugasTambahansRelatedByPtkIdToDelete = $this->getTugasTambahansRelatedByPtkId(new Criteria(), $con)->diff($tugasTambahansRelatedByPtkId);

        $this->tugasTambahansRelatedByPtkIdScheduledForDeletion = unserialize(serialize($tugasTambahansRelatedByPtkIdToDelete));

        foreach ($tugasTambahansRelatedByPtkIdToDelete as $tugasTambahanRelatedByPtkIdRemoved) {
            $tugasTambahanRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collTugasTambahansRelatedByPtkId = null;
        foreach ($tugasTambahansRelatedByPtkId as $tugasTambahanRelatedByPtkId) {
            $this->addTugasTambahanRelatedByPtkId($tugasTambahanRelatedByPtkId);
        }

        $this->collTugasTambahansRelatedByPtkId = $tugasTambahansRelatedByPtkId;
        $this->collTugasTambahansRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related TugasTambahan objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related TugasTambahan objects.
     * @throws PropelException
     */
    public function countTugasTambahansRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collTugasTambahansRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collTugasTambahansRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collTugasTambahansRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getTugasTambahansRelatedByPtkId());
            }
            $query = TugasTambahanQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collTugasTambahansRelatedByPtkId);
    }

    /**
     * Method called to associate a TugasTambahan object to this object
     * through the TugasTambahan foreign key attribute.
     *
     * @param    TugasTambahan $l TugasTambahan
     * @return Ptk The current object (for fluent API support)
     */
    public function addTugasTambahanRelatedByPtkId(TugasTambahan $l)
    {
        if ($this->collTugasTambahansRelatedByPtkId === null) {
            $this->initTugasTambahansRelatedByPtkId();
            $this->collTugasTambahansRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collTugasTambahansRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddTugasTambahanRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	TugasTambahanRelatedByPtkId $tugasTambahanRelatedByPtkId The tugasTambahanRelatedByPtkId object to add.
     */
    protected function doAddTugasTambahanRelatedByPtkId($tugasTambahanRelatedByPtkId)
    {
        $this->collTugasTambahansRelatedByPtkId[]= $tugasTambahanRelatedByPtkId;
        $tugasTambahanRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	TugasTambahanRelatedByPtkId $tugasTambahanRelatedByPtkId The tugasTambahanRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removeTugasTambahanRelatedByPtkId($tugasTambahanRelatedByPtkId)
    {
        if ($this->getTugasTambahansRelatedByPtkId()->contains($tugasTambahanRelatedByPtkId)) {
            $this->collTugasTambahansRelatedByPtkId->remove($this->collTugasTambahansRelatedByPtkId->search($tugasTambahanRelatedByPtkId));
            if (null === $this->tugasTambahansRelatedByPtkIdScheduledForDeletion) {
                $this->tugasTambahansRelatedByPtkIdScheduledForDeletion = clone $this->collTugasTambahansRelatedByPtkId;
                $this->tugasTambahansRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->tugasTambahansRelatedByPtkIdScheduledForDeletion[]= clone $tugasTambahanRelatedByPtkId;
            $tugasTambahanRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related TugasTambahansRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|TugasTambahan[] List of TugasTambahan objects
     */
    public function getTugasTambahansRelatedByPtkIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = TugasTambahanQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getTugasTambahansRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related TugasTambahansRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|TugasTambahan[] List of TugasTambahan objects
     */
    public function getTugasTambahansRelatedByPtkIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = TugasTambahanQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getTugasTambahansRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related TugasTambahansRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|TugasTambahan[] List of TugasTambahan objects
     */
    public function getTugasTambahansRelatedByPtkIdJoinJabatanTugasPtkRelatedByJabatanPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = TugasTambahanQuery::create(null, $criteria);
        $query->joinWith('JabatanTugasPtkRelatedByJabatanPtkId', $join_behavior);

        return $this->getTugasTambahansRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related TugasTambahansRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|TugasTambahan[] List of TugasTambahan objects
     */
    public function getTugasTambahansRelatedByPtkIdJoinJabatanTugasPtkRelatedByJabatanPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = TugasTambahanQuery::create(null, $criteria);
        $query->joinWith('JabatanTugasPtkRelatedByJabatanPtkId', $join_behavior);

        return $this->getTugasTambahansRelatedByPtkId($query, $con);
    }

    /**
     * Clears out the collRwyFungsionalsRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addRwyFungsionalsRelatedByPtkId()
     */
    public function clearRwyFungsionalsRelatedByPtkId()
    {
        $this->collRwyFungsionalsRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collRwyFungsionalsRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collRwyFungsionalsRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialRwyFungsionalsRelatedByPtkId($v = true)
    {
        $this->collRwyFungsionalsRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collRwyFungsionalsRelatedByPtkId collection.
     *
     * By default this just sets the collRwyFungsionalsRelatedByPtkId collection to an empty array (like clearcollRwyFungsionalsRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRwyFungsionalsRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collRwyFungsionalsRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collRwyFungsionalsRelatedByPtkId = new PropelObjectCollection();
        $this->collRwyFungsionalsRelatedByPtkId->setModel('RwyFungsional');
    }

    /**
     * Gets an array of RwyFungsional objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RwyFungsional[] List of RwyFungsional objects
     * @throws PropelException
     */
    public function getRwyFungsionalsRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRwyFungsionalsRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collRwyFungsionalsRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRwyFungsionalsRelatedByPtkId) {
                // return empty collection
                $this->initRwyFungsionalsRelatedByPtkId();
            } else {
                $collRwyFungsionalsRelatedByPtkId = RwyFungsionalQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRwyFungsionalsRelatedByPtkIdPartial && count($collRwyFungsionalsRelatedByPtkId)) {
                      $this->initRwyFungsionalsRelatedByPtkId(false);

                      foreach($collRwyFungsionalsRelatedByPtkId as $obj) {
                        if (false == $this->collRwyFungsionalsRelatedByPtkId->contains($obj)) {
                          $this->collRwyFungsionalsRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collRwyFungsionalsRelatedByPtkIdPartial = true;
                    }

                    $collRwyFungsionalsRelatedByPtkId->getInternalIterator()->rewind();
                    return $collRwyFungsionalsRelatedByPtkId;
                }

                if($partial && $this->collRwyFungsionalsRelatedByPtkId) {
                    foreach($this->collRwyFungsionalsRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collRwyFungsionalsRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collRwyFungsionalsRelatedByPtkId = $collRwyFungsionalsRelatedByPtkId;
                $this->collRwyFungsionalsRelatedByPtkIdPartial = false;
            }
        }

        return $this->collRwyFungsionalsRelatedByPtkId;
    }

    /**
     * Sets a collection of RwyFungsionalRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rwyFungsionalsRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setRwyFungsionalsRelatedByPtkId(PropelCollection $rwyFungsionalsRelatedByPtkId, PropelPDO $con = null)
    {
        $rwyFungsionalsRelatedByPtkIdToDelete = $this->getRwyFungsionalsRelatedByPtkId(new Criteria(), $con)->diff($rwyFungsionalsRelatedByPtkId);

        $this->rwyFungsionalsRelatedByPtkIdScheduledForDeletion = unserialize(serialize($rwyFungsionalsRelatedByPtkIdToDelete));

        foreach ($rwyFungsionalsRelatedByPtkIdToDelete as $rwyFungsionalRelatedByPtkIdRemoved) {
            $rwyFungsionalRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collRwyFungsionalsRelatedByPtkId = null;
        foreach ($rwyFungsionalsRelatedByPtkId as $rwyFungsionalRelatedByPtkId) {
            $this->addRwyFungsionalRelatedByPtkId($rwyFungsionalRelatedByPtkId);
        }

        $this->collRwyFungsionalsRelatedByPtkId = $rwyFungsionalsRelatedByPtkId;
        $this->collRwyFungsionalsRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RwyFungsional objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RwyFungsional objects.
     * @throws PropelException
     */
    public function countRwyFungsionalsRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRwyFungsionalsRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collRwyFungsionalsRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRwyFungsionalsRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getRwyFungsionalsRelatedByPtkId());
            }
            $query = RwyFungsionalQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collRwyFungsionalsRelatedByPtkId);
    }

    /**
     * Method called to associate a RwyFungsional object to this object
     * through the RwyFungsional foreign key attribute.
     *
     * @param    RwyFungsional $l RwyFungsional
     * @return Ptk The current object (for fluent API support)
     */
    public function addRwyFungsionalRelatedByPtkId(RwyFungsional $l)
    {
        if ($this->collRwyFungsionalsRelatedByPtkId === null) {
            $this->initRwyFungsionalsRelatedByPtkId();
            $this->collRwyFungsionalsRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collRwyFungsionalsRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRwyFungsionalRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	RwyFungsionalRelatedByPtkId $rwyFungsionalRelatedByPtkId The rwyFungsionalRelatedByPtkId object to add.
     */
    protected function doAddRwyFungsionalRelatedByPtkId($rwyFungsionalRelatedByPtkId)
    {
        $this->collRwyFungsionalsRelatedByPtkId[]= $rwyFungsionalRelatedByPtkId;
        $rwyFungsionalRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	RwyFungsionalRelatedByPtkId $rwyFungsionalRelatedByPtkId The rwyFungsionalRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removeRwyFungsionalRelatedByPtkId($rwyFungsionalRelatedByPtkId)
    {
        if ($this->getRwyFungsionalsRelatedByPtkId()->contains($rwyFungsionalRelatedByPtkId)) {
            $this->collRwyFungsionalsRelatedByPtkId->remove($this->collRwyFungsionalsRelatedByPtkId->search($rwyFungsionalRelatedByPtkId));
            if (null === $this->rwyFungsionalsRelatedByPtkIdScheduledForDeletion) {
                $this->rwyFungsionalsRelatedByPtkIdScheduledForDeletion = clone $this->collRwyFungsionalsRelatedByPtkId;
                $this->rwyFungsionalsRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->rwyFungsionalsRelatedByPtkIdScheduledForDeletion[]= clone $rwyFungsionalRelatedByPtkId;
            $rwyFungsionalRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RwyFungsionalsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwyFungsional[] List of RwyFungsional objects
     */
    public function getRwyFungsionalsRelatedByPtkIdJoinJabatanFungsionalRelatedByJabatanFungsionalId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwyFungsionalQuery::create(null, $criteria);
        $query->joinWith('JabatanFungsionalRelatedByJabatanFungsionalId', $join_behavior);

        return $this->getRwyFungsionalsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RwyFungsionalsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwyFungsional[] List of RwyFungsional objects
     */
    public function getRwyFungsionalsRelatedByPtkIdJoinJabatanFungsionalRelatedByJabatanFungsionalId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwyFungsionalQuery::create(null, $criteria);
        $query->joinWith('JabatanFungsionalRelatedByJabatanFungsionalId', $join_behavior);

        return $this->getRwyFungsionalsRelatedByPtkId($query, $con);
    }

    /**
     * Clears out the collRwyFungsionalsRelatedByPtkId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Ptk The current object (for fluent API support)
     * @see        addRwyFungsionalsRelatedByPtkId()
     */
    public function clearRwyFungsionalsRelatedByPtkId()
    {
        $this->collRwyFungsionalsRelatedByPtkId = null; // important to set this to null since that means it is uninitialized
        $this->collRwyFungsionalsRelatedByPtkIdPartial = null;

        return $this;
    }

    /**
     * reset is the collRwyFungsionalsRelatedByPtkId collection loaded partially
     *
     * @return void
     */
    public function resetPartialRwyFungsionalsRelatedByPtkId($v = true)
    {
        $this->collRwyFungsionalsRelatedByPtkIdPartial = $v;
    }

    /**
     * Initializes the collRwyFungsionalsRelatedByPtkId collection.
     *
     * By default this just sets the collRwyFungsionalsRelatedByPtkId collection to an empty array (like clearcollRwyFungsionalsRelatedByPtkId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRwyFungsionalsRelatedByPtkId($overrideExisting = true)
    {
        if (null !== $this->collRwyFungsionalsRelatedByPtkId && !$overrideExisting) {
            return;
        }
        $this->collRwyFungsionalsRelatedByPtkId = new PropelObjectCollection();
        $this->collRwyFungsionalsRelatedByPtkId->setModel('RwyFungsional');
    }

    /**
     * Gets an array of RwyFungsional objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Ptk is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RwyFungsional[] List of RwyFungsional objects
     * @throws PropelException
     */
    public function getRwyFungsionalsRelatedByPtkId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRwyFungsionalsRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collRwyFungsionalsRelatedByPtkId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRwyFungsionalsRelatedByPtkId) {
                // return empty collection
                $this->initRwyFungsionalsRelatedByPtkId();
            } else {
                $collRwyFungsionalsRelatedByPtkId = RwyFungsionalQuery::create(null, $criteria)
                    ->filterByPtkRelatedByPtkId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRwyFungsionalsRelatedByPtkIdPartial && count($collRwyFungsionalsRelatedByPtkId)) {
                      $this->initRwyFungsionalsRelatedByPtkId(false);

                      foreach($collRwyFungsionalsRelatedByPtkId as $obj) {
                        if (false == $this->collRwyFungsionalsRelatedByPtkId->contains($obj)) {
                          $this->collRwyFungsionalsRelatedByPtkId->append($obj);
                        }
                      }

                      $this->collRwyFungsionalsRelatedByPtkIdPartial = true;
                    }

                    $collRwyFungsionalsRelatedByPtkId->getInternalIterator()->rewind();
                    return $collRwyFungsionalsRelatedByPtkId;
                }

                if($partial && $this->collRwyFungsionalsRelatedByPtkId) {
                    foreach($this->collRwyFungsionalsRelatedByPtkId as $obj) {
                        if($obj->isNew()) {
                            $collRwyFungsionalsRelatedByPtkId[] = $obj;
                        }
                    }
                }

                $this->collRwyFungsionalsRelatedByPtkId = $collRwyFungsionalsRelatedByPtkId;
                $this->collRwyFungsionalsRelatedByPtkIdPartial = false;
            }
        }

        return $this->collRwyFungsionalsRelatedByPtkId;
    }

    /**
     * Sets a collection of RwyFungsionalRelatedByPtkId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rwyFungsionalsRelatedByPtkId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Ptk The current object (for fluent API support)
     */
    public function setRwyFungsionalsRelatedByPtkId(PropelCollection $rwyFungsionalsRelatedByPtkId, PropelPDO $con = null)
    {
        $rwyFungsionalsRelatedByPtkIdToDelete = $this->getRwyFungsionalsRelatedByPtkId(new Criteria(), $con)->diff($rwyFungsionalsRelatedByPtkId);

        $this->rwyFungsionalsRelatedByPtkIdScheduledForDeletion = unserialize(serialize($rwyFungsionalsRelatedByPtkIdToDelete));

        foreach ($rwyFungsionalsRelatedByPtkIdToDelete as $rwyFungsionalRelatedByPtkIdRemoved) {
            $rwyFungsionalRelatedByPtkIdRemoved->setPtkRelatedByPtkId(null);
        }

        $this->collRwyFungsionalsRelatedByPtkId = null;
        foreach ($rwyFungsionalsRelatedByPtkId as $rwyFungsionalRelatedByPtkId) {
            $this->addRwyFungsionalRelatedByPtkId($rwyFungsionalRelatedByPtkId);
        }

        $this->collRwyFungsionalsRelatedByPtkId = $rwyFungsionalsRelatedByPtkId;
        $this->collRwyFungsionalsRelatedByPtkIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RwyFungsional objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RwyFungsional objects.
     * @throws PropelException
     */
    public function countRwyFungsionalsRelatedByPtkId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRwyFungsionalsRelatedByPtkIdPartial && !$this->isNew();
        if (null === $this->collRwyFungsionalsRelatedByPtkId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRwyFungsionalsRelatedByPtkId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getRwyFungsionalsRelatedByPtkId());
            }
            $query = RwyFungsionalQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPtkRelatedByPtkId($this)
                ->count($con);
        }

        return count($this->collRwyFungsionalsRelatedByPtkId);
    }

    /**
     * Method called to associate a RwyFungsional object to this object
     * through the RwyFungsional foreign key attribute.
     *
     * @param    RwyFungsional $l RwyFungsional
     * @return Ptk The current object (for fluent API support)
     */
    public function addRwyFungsionalRelatedByPtkId(RwyFungsional $l)
    {
        if ($this->collRwyFungsionalsRelatedByPtkId === null) {
            $this->initRwyFungsionalsRelatedByPtkId();
            $this->collRwyFungsionalsRelatedByPtkIdPartial = true;
        }
        if (!in_array($l, $this->collRwyFungsionalsRelatedByPtkId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRwyFungsionalRelatedByPtkId($l);
        }

        return $this;
    }

    /**
     * @param	RwyFungsionalRelatedByPtkId $rwyFungsionalRelatedByPtkId The rwyFungsionalRelatedByPtkId object to add.
     */
    protected function doAddRwyFungsionalRelatedByPtkId($rwyFungsionalRelatedByPtkId)
    {
        $this->collRwyFungsionalsRelatedByPtkId[]= $rwyFungsionalRelatedByPtkId;
        $rwyFungsionalRelatedByPtkId->setPtkRelatedByPtkId($this);
    }

    /**
     * @param	RwyFungsionalRelatedByPtkId $rwyFungsionalRelatedByPtkId The rwyFungsionalRelatedByPtkId object to remove.
     * @return Ptk The current object (for fluent API support)
     */
    public function removeRwyFungsionalRelatedByPtkId($rwyFungsionalRelatedByPtkId)
    {
        if ($this->getRwyFungsionalsRelatedByPtkId()->contains($rwyFungsionalRelatedByPtkId)) {
            $this->collRwyFungsionalsRelatedByPtkId->remove($this->collRwyFungsionalsRelatedByPtkId->search($rwyFungsionalRelatedByPtkId));
            if (null === $this->rwyFungsionalsRelatedByPtkIdScheduledForDeletion) {
                $this->rwyFungsionalsRelatedByPtkIdScheduledForDeletion = clone $this->collRwyFungsionalsRelatedByPtkId;
                $this->rwyFungsionalsRelatedByPtkIdScheduledForDeletion->clear();
            }
            $this->rwyFungsionalsRelatedByPtkIdScheduledForDeletion[]= clone $rwyFungsionalRelatedByPtkId;
            $rwyFungsionalRelatedByPtkId->setPtkRelatedByPtkId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RwyFungsionalsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwyFungsional[] List of RwyFungsional objects
     */
    public function getRwyFungsionalsRelatedByPtkIdJoinJabatanFungsionalRelatedByJabatanFungsionalId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwyFungsionalQuery::create(null, $criteria);
        $query->joinWith('JabatanFungsionalRelatedByJabatanFungsionalId', $join_behavior);

        return $this->getRwyFungsionalsRelatedByPtkId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ptk is new, it will return
     * an empty collection; or if this Ptk has previously
     * been saved, it will retrieve related RwyFungsionalsRelatedByPtkId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ptk.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RwyFungsional[] List of RwyFungsional objects
     */
    public function getRwyFungsionalsRelatedByPtkIdJoinJabatanFungsionalRelatedByJabatanFungsionalId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RwyFungsionalQuery::create(null, $criteria);
        $query->joinWith('JabatanFungsionalRelatedByJabatanFungsionalId', $join_behavior);

        return $this->getRwyFungsionalsRelatedByPtkId($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->ptk_id = null;
        $this->nama = null;
        $this->nip = null;
        $this->jenis_kelamin = null;
        $this->tempat_lahir = null;
        $this->tanggal_lahir = null;
        $this->nik = null;
        $this->niy_nigk = null;
        $this->nuptk = null;
        $this->status_kepegawaian_id = null;
        $this->jenis_ptk_id = null;
        $this->pengawas_bidang_studi_id = null;
        $this->agama_id = null;
        $this->kewarganegaraan = null;
        $this->alamat_jalan = null;
        $this->rt = null;
        $this->rw = null;
        $this->nama_dusun = null;
        $this->desa_kelurahan = null;
        $this->kode_wilayah = null;
        $this->kode_pos = null;
        $this->no_telepon_rumah = null;
        $this->no_hp = null;
        $this->email = null;
        $this->entry_sekolah_id = null;
        $this->status_keaktifan_id = null;
        $this->sk_cpns = null;
        $this->tgl_cpns = null;
        $this->sk_pengangkatan = null;
        $this->tmt_pengangkatan = null;
        $this->lembaga_pengangkat_id = null;
        $this->pangkat_golongan_id = null;
        $this->keahlian_laboratorium_id = null;
        $this->sumber_gaji_id = null;
        $this->nama_ibu_kandung = null;
        $this->status_perkawinan = null;
        $this->nama_suami_istri = null;
        $this->nip_suami_istri = null;
        $this->pekerjaan_suami_istri = null;
        $this->tmt_pns = null;
        $this->sudah_lisensi_kepala_sekolah = null;
        $this->jumlah_sekolah_binaan = null;
        $this->pernah_diklat_kepengawasan = null;
        $this->status_data = null;
        $this->mampu_handle_kk = null;
        $this->keahlian_braille = null;
        $this->keahlian_bhs_isyarat = null;
        $this->npwp = null;
        $this->last_update = null;
        $this->soft_delete = null;
        $this->last_sync = null;
        $this->updater_id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collKesejahteraansRelatedByPtkId) {
                foreach ($this->collKesejahteraansRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collKesejahteraansRelatedByPtkId) {
                foreach ($this->collKesejahteraansRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldPtksRelatedByPtkId) {
                foreach ($this->collVldPtksRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldPtksRelatedByPtkId) {
                foreach ($this->collVldPtksRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPenghargaansRelatedByPtkId) {
                foreach ($this->collPenghargaansRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPenghargaansRelatedByPtkId) {
                foreach ($this->collPenghargaansRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collInpassingsRelatedByPtkId) {
                foreach ($this->collInpassingsRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collInpassingsRelatedByPtkId) {
                foreach ($this->collInpassingsRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPtkTerdaftarsRelatedByPtkId) {
                foreach ($this->collPtkTerdaftarsRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPtkTerdaftarsRelatedByPtkId) {
                foreach ($this->collPtkTerdaftarsRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collKaryaTulissRelatedByPtkId) {
                foreach ($this->collKaryaTulissRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collKaryaTulissRelatedByPtkId) {
                foreach ($this->collKaryaTulissRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collNilaiTestsRelatedByPtkId) {
                foreach ($this->collNilaiTestsRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collNilaiTestsRelatedByPtkId) {
                foreach ($this->collNilaiTestsRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collBukuPtksRelatedByPtkId) {
                foreach ($this->collBukuPtksRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collBukuPtksRelatedByPtkId) {
                foreach ($this->collBukuPtksRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collBeasiswaPtksRelatedByPtkId) {
                foreach ($this->collBeasiswaPtksRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collBeasiswaPtksRelatedByPtkId) {
                foreach ($this->collBeasiswaPtksRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRwyKepangkatansRelatedByPtkId) {
                foreach ($this->collRwyKepangkatansRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRwyKepangkatansRelatedByPtkId) {
                foreach ($this->collRwyKepangkatansRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRombonganBelajarsRelatedByPtkId) {
                foreach ($this->collRombonganBelajarsRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRombonganBelajarsRelatedByPtkId) {
                foreach ($this->collRombonganBelajarsRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRiwayatGajiBerkalasRelatedByPtkId) {
                foreach ($this->collRiwayatGajiBerkalasRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRiwayatGajiBerkalasRelatedByPtkId) {
                foreach ($this->collRiwayatGajiBerkalasRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPengawasTerdaftarsRelatedByPtkId) {
                foreach ($this->collPengawasTerdaftarsRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPengawasTerdaftarsRelatedByPtkId) {
                foreach ($this->collPengawasTerdaftarsRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRwyStrukturalsRelatedByPtkId) {
                foreach ($this->collRwyStrukturalsRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRwyStrukturalsRelatedByPtkId) {
                foreach ($this->collRwyStrukturalsRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRwyPendFormalsRelatedByPtkId) {
                foreach ($this->collRwyPendFormalsRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRwyPendFormalsRelatedByPtkId) {
                foreach ($this->collRwyPendFormalsRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collAnaksRelatedByPtkId) {
                foreach ($this->collAnaksRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collAnaksRelatedByPtkId) {
                foreach ($this->collAnaksRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collDiklatsRelatedByPtkId) {
                foreach ($this->collDiklatsRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collDiklatsRelatedByPtkId) {
                foreach ($this->collDiklatsRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPtkBarusRelatedByPtkId) {
                foreach ($this->collPtkBarusRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPtkBarusRelatedByPtkId) {
                foreach ($this->collPtkBarusRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collTunjangansRelatedByPtkId) {
                foreach ($this->collTunjangansRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collTunjangansRelatedByPtkId) {
                foreach ($this->collTunjangansRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRwySertifikasisRelatedByPtkId) {
                foreach ($this->collRwySertifikasisRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRwySertifikasisRelatedByPtkId) {
                foreach ($this->collRwySertifikasisRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collTugasTambahansRelatedByPtkId) {
                foreach ($this->collTugasTambahansRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collTugasTambahansRelatedByPtkId) {
                foreach ($this->collTugasTambahansRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRwyFungsionalsRelatedByPtkId) {
                foreach ($this->collRwyFungsionalsRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRwyFungsionalsRelatedByPtkId) {
                foreach ($this->collRwyFungsionalsRelatedByPtkId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aSekolahRelatedByEntrySekolahId instanceof Persistent) {
              $this->aSekolahRelatedByEntrySekolahId->clearAllReferences($deep);
            }
            if ($this->aSekolahRelatedByEntrySekolahId instanceof Persistent) {
              $this->aSekolahRelatedByEntrySekolahId->clearAllReferences($deep);
            }
            if ($this->aAgamaRelatedByAgamaId instanceof Persistent) {
              $this->aAgamaRelatedByAgamaId->clearAllReferences($deep);
            }
            if ($this->aAgamaRelatedByAgamaId instanceof Persistent) {
              $this->aAgamaRelatedByAgamaId->clearAllReferences($deep);
            }
            if ($this->aBidangStudiRelatedByPengawasBidangStudiId instanceof Persistent) {
              $this->aBidangStudiRelatedByPengawasBidangStudiId->clearAllReferences($deep);
            }
            if ($this->aBidangStudiRelatedByPengawasBidangStudiId instanceof Persistent) {
              $this->aBidangStudiRelatedByPengawasBidangStudiId->clearAllReferences($deep);
            }
            if ($this->aJenisPtkRelatedByJenisPtkId instanceof Persistent) {
              $this->aJenisPtkRelatedByJenisPtkId->clearAllReferences($deep);
            }
            if ($this->aJenisPtkRelatedByJenisPtkId instanceof Persistent) {
              $this->aJenisPtkRelatedByJenisPtkId->clearAllReferences($deep);
            }
            if ($this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId instanceof Persistent) {
              $this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId->clearAllReferences($deep);
            }
            if ($this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId instanceof Persistent) {
              $this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId->clearAllReferences($deep);
            }
            if ($this->aKebutuhanKhususRelatedByMampuHandleKk instanceof Persistent) {
              $this->aKebutuhanKhususRelatedByMampuHandleKk->clearAllReferences($deep);
            }
            if ($this->aKebutuhanKhususRelatedByMampuHandleKk instanceof Persistent) {
              $this->aKebutuhanKhususRelatedByMampuHandleKk->clearAllReferences($deep);
            }
            if ($this->aLembagaPengangkatRelatedByLembagaPengangkatId instanceof Persistent) {
              $this->aLembagaPengangkatRelatedByLembagaPengangkatId->clearAllReferences($deep);
            }
            if ($this->aLembagaPengangkatRelatedByLembagaPengangkatId instanceof Persistent) {
              $this->aLembagaPengangkatRelatedByLembagaPengangkatId->clearAllReferences($deep);
            }
            if ($this->aMstWilayahRelatedByKodeWilayah instanceof Persistent) {
              $this->aMstWilayahRelatedByKodeWilayah->clearAllReferences($deep);
            }
            if ($this->aMstWilayahRelatedByKodeWilayah instanceof Persistent) {
              $this->aMstWilayahRelatedByKodeWilayah->clearAllReferences($deep);
            }
            if ($this->aNegaraRelatedByKewarganegaraan instanceof Persistent) {
              $this->aNegaraRelatedByKewarganegaraan->clearAllReferences($deep);
            }
            if ($this->aNegaraRelatedByKewarganegaraan instanceof Persistent) {
              $this->aNegaraRelatedByKewarganegaraan->clearAllReferences($deep);
            }
            if ($this->aPangkatGolonganRelatedByPangkatGolonganId instanceof Persistent) {
              $this->aPangkatGolonganRelatedByPangkatGolonganId->clearAllReferences($deep);
            }
            if ($this->aPangkatGolonganRelatedByPangkatGolonganId instanceof Persistent) {
              $this->aPangkatGolonganRelatedByPangkatGolonganId->clearAllReferences($deep);
            }
            if ($this->aPekerjaanRelatedByPekerjaanSuamiIstri instanceof Persistent) {
              $this->aPekerjaanRelatedByPekerjaanSuamiIstri->clearAllReferences($deep);
            }
            if ($this->aPekerjaanRelatedByPekerjaanSuamiIstri instanceof Persistent) {
              $this->aPekerjaanRelatedByPekerjaanSuamiIstri->clearAllReferences($deep);
            }
            if ($this->aStatusKepegawaianRelatedByStatusKepegawaianId instanceof Persistent) {
              $this->aStatusKepegawaianRelatedByStatusKepegawaianId->clearAllReferences($deep);
            }
            if ($this->aStatusKepegawaianRelatedByStatusKepegawaianId instanceof Persistent) {
              $this->aStatusKepegawaianRelatedByStatusKepegawaianId->clearAllReferences($deep);
            }
            if ($this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId instanceof Persistent) {
              $this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId->clearAllReferences($deep);
            }
            if ($this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId instanceof Persistent) {
              $this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId->clearAllReferences($deep);
            }
            if ($this->aSumberGajiRelatedBySumberGajiId instanceof Persistent) {
              $this->aSumberGajiRelatedBySumberGajiId->clearAllReferences($deep);
            }
            if ($this->aSumberGajiRelatedBySumberGajiId instanceof Persistent) {
              $this->aSumberGajiRelatedBySumberGajiId->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collKesejahteraansRelatedByPtkId instanceof PropelCollection) {
            $this->collKesejahteraansRelatedByPtkId->clearIterator();
        }
        $this->collKesejahteraansRelatedByPtkId = null;
        if ($this->collKesejahteraansRelatedByPtkId instanceof PropelCollection) {
            $this->collKesejahteraansRelatedByPtkId->clearIterator();
        }
        $this->collKesejahteraansRelatedByPtkId = null;
        if ($this->collVldPtksRelatedByPtkId instanceof PropelCollection) {
            $this->collVldPtksRelatedByPtkId->clearIterator();
        }
        $this->collVldPtksRelatedByPtkId = null;
        if ($this->collVldPtksRelatedByPtkId instanceof PropelCollection) {
            $this->collVldPtksRelatedByPtkId->clearIterator();
        }
        $this->collVldPtksRelatedByPtkId = null;
        if ($this->collPenghargaansRelatedByPtkId instanceof PropelCollection) {
            $this->collPenghargaansRelatedByPtkId->clearIterator();
        }
        $this->collPenghargaansRelatedByPtkId = null;
        if ($this->collPenghargaansRelatedByPtkId instanceof PropelCollection) {
            $this->collPenghargaansRelatedByPtkId->clearIterator();
        }
        $this->collPenghargaansRelatedByPtkId = null;
        if ($this->collInpassingsRelatedByPtkId instanceof PropelCollection) {
            $this->collInpassingsRelatedByPtkId->clearIterator();
        }
        $this->collInpassingsRelatedByPtkId = null;
        if ($this->collInpassingsRelatedByPtkId instanceof PropelCollection) {
            $this->collInpassingsRelatedByPtkId->clearIterator();
        }
        $this->collInpassingsRelatedByPtkId = null;
        if ($this->collPtkTerdaftarsRelatedByPtkId instanceof PropelCollection) {
            $this->collPtkTerdaftarsRelatedByPtkId->clearIterator();
        }
        $this->collPtkTerdaftarsRelatedByPtkId = null;
        if ($this->collPtkTerdaftarsRelatedByPtkId instanceof PropelCollection) {
            $this->collPtkTerdaftarsRelatedByPtkId->clearIterator();
        }
        $this->collPtkTerdaftarsRelatedByPtkId = null;
        if ($this->collKaryaTulissRelatedByPtkId instanceof PropelCollection) {
            $this->collKaryaTulissRelatedByPtkId->clearIterator();
        }
        $this->collKaryaTulissRelatedByPtkId = null;
        if ($this->collKaryaTulissRelatedByPtkId instanceof PropelCollection) {
            $this->collKaryaTulissRelatedByPtkId->clearIterator();
        }
        $this->collKaryaTulissRelatedByPtkId = null;
        if ($this->collNilaiTestsRelatedByPtkId instanceof PropelCollection) {
            $this->collNilaiTestsRelatedByPtkId->clearIterator();
        }
        $this->collNilaiTestsRelatedByPtkId = null;
        if ($this->collNilaiTestsRelatedByPtkId instanceof PropelCollection) {
            $this->collNilaiTestsRelatedByPtkId->clearIterator();
        }
        $this->collNilaiTestsRelatedByPtkId = null;
        if ($this->collBukuPtksRelatedByPtkId instanceof PropelCollection) {
            $this->collBukuPtksRelatedByPtkId->clearIterator();
        }
        $this->collBukuPtksRelatedByPtkId = null;
        if ($this->collBukuPtksRelatedByPtkId instanceof PropelCollection) {
            $this->collBukuPtksRelatedByPtkId->clearIterator();
        }
        $this->collBukuPtksRelatedByPtkId = null;
        if ($this->collBeasiswaPtksRelatedByPtkId instanceof PropelCollection) {
            $this->collBeasiswaPtksRelatedByPtkId->clearIterator();
        }
        $this->collBeasiswaPtksRelatedByPtkId = null;
        if ($this->collBeasiswaPtksRelatedByPtkId instanceof PropelCollection) {
            $this->collBeasiswaPtksRelatedByPtkId->clearIterator();
        }
        $this->collBeasiswaPtksRelatedByPtkId = null;
        if ($this->collRwyKepangkatansRelatedByPtkId instanceof PropelCollection) {
            $this->collRwyKepangkatansRelatedByPtkId->clearIterator();
        }
        $this->collRwyKepangkatansRelatedByPtkId = null;
        if ($this->collRwyKepangkatansRelatedByPtkId instanceof PropelCollection) {
            $this->collRwyKepangkatansRelatedByPtkId->clearIterator();
        }
        $this->collRwyKepangkatansRelatedByPtkId = null;
        if ($this->collRombonganBelajarsRelatedByPtkId instanceof PropelCollection) {
            $this->collRombonganBelajarsRelatedByPtkId->clearIterator();
        }
        $this->collRombonganBelajarsRelatedByPtkId = null;
        if ($this->collRombonganBelajarsRelatedByPtkId instanceof PropelCollection) {
            $this->collRombonganBelajarsRelatedByPtkId->clearIterator();
        }
        $this->collRombonganBelajarsRelatedByPtkId = null;
        if ($this->collRiwayatGajiBerkalasRelatedByPtkId instanceof PropelCollection) {
            $this->collRiwayatGajiBerkalasRelatedByPtkId->clearIterator();
        }
        $this->collRiwayatGajiBerkalasRelatedByPtkId = null;
        if ($this->collRiwayatGajiBerkalasRelatedByPtkId instanceof PropelCollection) {
            $this->collRiwayatGajiBerkalasRelatedByPtkId->clearIterator();
        }
        $this->collRiwayatGajiBerkalasRelatedByPtkId = null;
        if ($this->collPengawasTerdaftarsRelatedByPtkId instanceof PropelCollection) {
            $this->collPengawasTerdaftarsRelatedByPtkId->clearIterator();
        }
        $this->collPengawasTerdaftarsRelatedByPtkId = null;
        if ($this->collPengawasTerdaftarsRelatedByPtkId instanceof PropelCollection) {
            $this->collPengawasTerdaftarsRelatedByPtkId->clearIterator();
        }
        $this->collPengawasTerdaftarsRelatedByPtkId = null;
        if ($this->collRwyStrukturalsRelatedByPtkId instanceof PropelCollection) {
            $this->collRwyStrukturalsRelatedByPtkId->clearIterator();
        }
        $this->collRwyStrukturalsRelatedByPtkId = null;
        if ($this->collRwyStrukturalsRelatedByPtkId instanceof PropelCollection) {
            $this->collRwyStrukturalsRelatedByPtkId->clearIterator();
        }
        $this->collRwyStrukturalsRelatedByPtkId = null;
        if ($this->collRwyPendFormalsRelatedByPtkId instanceof PropelCollection) {
            $this->collRwyPendFormalsRelatedByPtkId->clearIterator();
        }
        $this->collRwyPendFormalsRelatedByPtkId = null;
        if ($this->collRwyPendFormalsRelatedByPtkId instanceof PropelCollection) {
            $this->collRwyPendFormalsRelatedByPtkId->clearIterator();
        }
        $this->collRwyPendFormalsRelatedByPtkId = null;
        if ($this->collAnaksRelatedByPtkId instanceof PropelCollection) {
            $this->collAnaksRelatedByPtkId->clearIterator();
        }
        $this->collAnaksRelatedByPtkId = null;
        if ($this->collAnaksRelatedByPtkId instanceof PropelCollection) {
            $this->collAnaksRelatedByPtkId->clearIterator();
        }
        $this->collAnaksRelatedByPtkId = null;
        if ($this->collDiklatsRelatedByPtkId instanceof PropelCollection) {
            $this->collDiklatsRelatedByPtkId->clearIterator();
        }
        $this->collDiklatsRelatedByPtkId = null;
        if ($this->collDiklatsRelatedByPtkId instanceof PropelCollection) {
            $this->collDiklatsRelatedByPtkId->clearIterator();
        }
        $this->collDiklatsRelatedByPtkId = null;
        if ($this->collPtkBarusRelatedByPtkId instanceof PropelCollection) {
            $this->collPtkBarusRelatedByPtkId->clearIterator();
        }
        $this->collPtkBarusRelatedByPtkId = null;
        if ($this->collPtkBarusRelatedByPtkId instanceof PropelCollection) {
            $this->collPtkBarusRelatedByPtkId->clearIterator();
        }
        $this->collPtkBarusRelatedByPtkId = null;
        if ($this->collTunjangansRelatedByPtkId instanceof PropelCollection) {
            $this->collTunjangansRelatedByPtkId->clearIterator();
        }
        $this->collTunjangansRelatedByPtkId = null;
        if ($this->collTunjangansRelatedByPtkId instanceof PropelCollection) {
            $this->collTunjangansRelatedByPtkId->clearIterator();
        }
        $this->collTunjangansRelatedByPtkId = null;
        if ($this->collRwySertifikasisRelatedByPtkId instanceof PropelCollection) {
            $this->collRwySertifikasisRelatedByPtkId->clearIterator();
        }
        $this->collRwySertifikasisRelatedByPtkId = null;
        if ($this->collRwySertifikasisRelatedByPtkId instanceof PropelCollection) {
            $this->collRwySertifikasisRelatedByPtkId->clearIterator();
        }
        $this->collRwySertifikasisRelatedByPtkId = null;
        if ($this->collTugasTambahansRelatedByPtkId instanceof PropelCollection) {
            $this->collTugasTambahansRelatedByPtkId->clearIterator();
        }
        $this->collTugasTambahansRelatedByPtkId = null;
        if ($this->collTugasTambahansRelatedByPtkId instanceof PropelCollection) {
            $this->collTugasTambahansRelatedByPtkId->clearIterator();
        }
        $this->collTugasTambahansRelatedByPtkId = null;
        if ($this->collRwyFungsionalsRelatedByPtkId instanceof PropelCollection) {
            $this->collRwyFungsionalsRelatedByPtkId->clearIterator();
        }
        $this->collRwyFungsionalsRelatedByPtkId = null;
        if ($this->collRwyFungsionalsRelatedByPtkId instanceof PropelCollection) {
            $this->collRwyFungsionalsRelatedByPtkId->clearIterator();
        }
        $this->collRwyFungsionalsRelatedByPtkId = null;
        $this->aSekolahRelatedByEntrySekolahId = null;
        $this->aSekolahRelatedByEntrySekolahId = null;
        $this->aAgamaRelatedByAgamaId = null;
        $this->aAgamaRelatedByAgamaId = null;
        $this->aBidangStudiRelatedByPengawasBidangStudiId = null;
        $this->aBidangStudiRelatedByPengawasBidangStudiId = null;
        $this->aJenisPtkRelatedByJenisPtkId = null;
        $this->aJenisPtkRelatedByJenisPtkId = null;
        $this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId = null;
        $this->aKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId = null;
        $this->aKebutuhanKhususRelatedByMampuHandleKk = null;
        $this->aKebutuhanKhususRelatedByMampuHandleKk = null;
        $this->aLembagaPengangkatRelatedByLembagaPengangkatId = null;
        $this->aLembagaPengangkatRelatedByLembagaPengangkatId = null;
        $this->aMstWilayahRelatedByKodeWilayah = null;
        $this->aMstWilayahRelatedByKodeWilayah = null;
        $this->aNegaraRelatedByKewarganegaraan = null;
        $this->aNegaraRelatedByKewarganegaraan = null;
        $this->aPangkatGolonganRelatedByPangkatGolonganId = null;
        $this->aPangkatGolonganRelatedByPangkatGolonganId = null;
        $this->aPekerjaanRelatedByPekerjaanSuamiIstri = null;
        $this->aPekerjaanRelatedByPekerjaanSuamiIstri = null;
        $this->aStatusKepegawaianRelatedByStatusKepegawaianId = null;
        $this->aStatusKepegawaianRelatedByStatusKepegawaianId = null;
        $this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId = null;
        $this->aStatusKeaktifanPegawaiRelatedByStatusKeaktifanId = null;
        $this->aSumberGajiRelatedBySumberGajiId = null;
        $this->aSumberGajiRelatedBySumberGajiId = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(PtkPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
