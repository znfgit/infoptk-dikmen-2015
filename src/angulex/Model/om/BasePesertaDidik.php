<?php

namespace angulex\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelDateTime;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\Agama;
use angulex\Model\AgamaQuery;
use angulex\Model\AlatTransportasi;
use angulex\Model\AlatTransportasiQuery;
use angulex\Model\AnggotaRombel;
use angulex\Model\AnggotaRombelQuery;
use angulex\Model\BeasiswaPesertaDidik;
use angulex\Model\BeasiswaPesertaDidikQuery;
use angulex\Model\JenisTinggal;
use angulex\Model\JenisTinggalQuery;
use angulex\Model\JenjangPendidikan;
use angulex\Model\JenjangPendidikanQuery;
use angulex\Model\KebutuhanKhusus;
use angulex\Model\KebutuhanKhususQuery;
use angulex\Model\MstWilayah;
use angulex\Model\MstWilayahQuery;
use angulex\Model\Negara;
use angulex\Model\NegaraQuery;
use angulex\Model\Pekerjaan;
use angulex\Model\PekerjaanQuery;
use angulex\Model\PenghasilanOrangtuaWali;
use angulex\Model\PenghasilanOrangtuaWaliQuery;
use angulex\Model\PesertaDidik;
use angulex\Model\PesertaDidikBaru;
use angulex\Model\PesertaDidikBaruQuery;
use angulex\Model\PesertaDidikLongitudinal;
use angulex\Model\PesertaDidikLongitudinalQuery;
use angulex\Model\PesertaDidikPeer;
use angulex\Model\PesertaDidikQuery;
use angulex\Model\Prestasi;
use angulex\Model\PrestasiQuery;
use angulex\Model\RegistrasiPesertaDidik;
use angulex\Model\RegistrasiPesertaDidikQuery;
use angulex\Model\Sekolah;
use angulex\Model\SekolahQuery;
use angulex\Model\VldPesertaDidik;
use angulex\Model\VldPesertaDidikQuery;

/**
 * Base class that represents a row from the 'peserta_didik' table.
 *
 * 
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BasePesertaDidik extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'angulex\\Model\\PesertaDidikPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        PesertaDidikPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the peserta_didik_id field.
     * @var        string
     */
    protected $peserta_didik_id;

    /**
     * The value for the nama field.
     * @var        string
     */
    protected $nama;

    /**
     * The value for the jenis_kelamin field.
     * @var        string
     */
    protected $jenis_kelamin;

    /**
     * The value for the nisn field.
     * @var        string
     */
    protected $nisn;

    /**
     * The value for the nik field.
     * @var        string
     */
    protected $nik;

    /**
     * The value for the tempat_lahir field.
     * @var        string
     */
    protected $tempat_lahir;

    /**
     * The value for the tanggal_lahir field.
     * @var        string
     */
    protected $tanggal_lahir;

    /**
     * The value for the agama_id field.
     * @var        int
     */
    protected $agama_id;

    /**
     * The value for the kewarganegaraan field.
     * @var        string
     */
    protected $kewarganegaraan;

    /**
     * The value for the kebutuhan_khusus_id field.
     * @var        int
     */
    protected $kebutuhan_khusus_id;

    /**
     * The value for the sekolah_id field.
     * @var        string
     */
    protected $sekolah_id;

    /**
     * The value for the alamat_jalan field.
     * @var        string
     */
    protected $alamat_jalan;

    /**
     * The value for the rt field.
     * @var        string
     */
    protected $rt;

    /**
     * The value for the rw field.
     * @var        string
     */
    protected $rw;

    /**
     * The value for the nama_dusun field.
     * @var        string
     */
    protected $nama_dusun;

    /**
     * The value for the desa_kelurahan field.
     * @var        string
     */
    protected $desa_kelurahan;

    /**
     * The value for the kode_wilayah field.
     * @var        string
     */
    protected $kode_wilayah;

    /**
     * The value for the kode_pos field.
     * @var        string
     */
    protected $kode_pos;

    /**
     * The value for the jenis_tinggal_id field.
     * @var        string
     */
    protected $jenis_tinggal_id;

    /**
     * The value for the alat_transportasi_id field.
     * @var        string
     */
    protected $alat_transportasi_id;

    /**
     * The value for the nomor_telepon_rumah field.
     * @var        string
     */
    protected $nomor_telepon_rumah;

    /**
     * The value for the nomor_telepon_seluler field.
     * @var        string
     */
    protected $nomor_telepon_seluler;

    /**
     * The value for the email field.
     * @var        string
     */
    protected $email;

    /**
     * The value for the penerima_kps field.
     * @var        string
     */
    protected $penerima_kps;

    /**
     * The value for the no_kps field.
     * @var        string
     */
    protected $no_kps;

    /**
     * The value for the status_data field.
     * @var        int
     */
    protected $status_data;

    /**
     * The value for the nama_ayah field.
     * @var        string
     */
    protected $nama_ayah;

    /**
     * The value for the tahun_lahir_ayah field.
     * @var        string
     */
    protected $tahun_lahir_ayah;

    /**
     * The value for the jenjang_pendidikan_ayah field.
     * @var        string
     */
    protected $jenjang_pendidikan_ayah;

    /**
     * The value for the pekerjaan_id_ayah field.
     * @var        int
     */
    protected $pekerjaan_id_ayah;

    /**
     * The value for the penghasilan_id_ayah field.
     * @var        int
     */
    protected $penghasilan_id_ayah;

    /**
     * The value for the kebutuhan_khusus_id_ayah field.
     * @var        int
     */
    protected $kebutuhan_khusus_id_ayah;

    /**
     * The value for the nama_ibu_kandung field.
     * @var        string
     */
    protected $nama_ibu_kandung;

    /**
     * The value for the tahun_lahir_ibu field.
     * @var        string
     */
    protected $tahun_lahir_ibu;

    /**
     * The value for the jenjang_pendidikan_ibu field.
     * @var        string
     */
    protected $jenjang_pendidikan_ibu;

    /**
     * The value for the penghasilan_id_ibu field.
     * @var        int
     */
    protected $penghasilan_id_ibu;

    /**
     * The value for the pekerjaan_id_ibu field.
     * @var        int
     */
    protected $pekerjaan_id_ibu;

    /**
     * The value for the kebutuhan_khusus_id_ibu field.
     * @var        int
     */
    protected $kebutuhan_khusus_id_ibu;

    /**
     * The value for the nama_wali field.
     * @var        string
     */
    protected $nama_wali;

    /**
     * The value for the tahun_lahir_wali field.
     * @var        string
     */
    protected $tahun_lahir_wali;

    /**
     * The value for the jenjang_pendidikan_wali field.
     * @var        string
     */
    protected $jenjang_pendidikan_wali;

    /**
     * The value for the pekerjaan_id_wali field.
     * @var        int
     */
    protected $pekerjaan_id_wali;

    /**
     * The value for the penghasilan_id_wali field.
     * @var        int
     */
    protected $penghasilan_id_wali;

    /**
     * The value for the last_update field.
     * @var        string
     */
    protected $last_update;

    /**
     * The value for the soft_delete field.
     * @var        string
     */
    protected $soft_delete;

    /**
     * The value for the last_sync field.
     * @var        string
     */
    protected $last_sync;

    /**
     * The value for the updater_id field.
     * @var        string
     */
    protected $updater_id;

    /**
     * @var        Sekolah
     */
    protected $aSekolahRelatedBySekolahId;

    /**
     * @var        Sekolah
     */
    protected $aSekolahRelatedBySekolahId;

    /**
     * @var        Agama
     */
    protected $aAgamaRelatedByAgamaId;

    /**
     * @var        Agama
     */
    protected $aAgamaRelatedByAgamaId;

    /**
     * @var        AlatTransportasi
     */
    protected $aAlatTransportasiRelatedByAlatTransportasiId;

    /**
     * @var        AlatTransportasi
     */
    protected $aAlatTransportasiRelatedByAlatTransportasiId;

    /**
     * @var        JenisTinggal
     */
    protected $aJenisTinggalRelatedByJenisTinggalId;

    /**
     * @var        JenisTinggal
     */
    protected $aJenisTinggalRelatedByJenisTinggalId;

    /**
     * @var        JenjangPendidikan
     */
    protected $aJenjangPendidikanRelatedByJenjangPendidikanIbu;

    /**
     * @var        JenjangPendidikan
     */
    protected $aJenjangPendidikanRelatedByJenjangPendidikanAyah;

    /**
     * @var        JenjangPendidikan
     */
    protected $aJenjangPendidikanRelatedByJenjangPendidikanWali;

    /**
     * @var        JenjangPendidikan
     */
    protected $aJenjangPendidikanRelatedByJenjangPendidikanIbu;

    /**
     * @var        JenjangPendidikan
     */
    protected $aJenjangPendidikanRelatedByJenjangPendidikanAyah;

    /**
     * @var        JenjangPendidikan
     */
    protected $aJenjangPendidikanRelatedByJenjangPendidikanWali;

    /**
     * @var        KebutuhanKhusus
     */
    protected $aKebutuhanKhususRelatedByKebutuhanKhususIdAyah;

    /**
     * @var        KebutuhanKhusus
     */
    protected $aKebutuhanKhususRelatedByKebutuhanKhususIdIbu;

    /**
     * @var        KebutuhanKhusus
     */
    protected $aKebutuhanKhususRelatedByKebutuhanKhususId;

    /**
     * @var        KebutuhanKhusus
     */
    protected $aKebutuhanKhususRelatedByKebutuhanKhususIdAyah;

    /**
     * @var        KebutuhanKhusus
     */
    protected $aKebutuhanKhususRelatedByKebutuhanKhususIdIbu;

    /**
     * @var        KebutuhanKhusus
     */
    protected $aKebutuhanKhususRelatedByKebutuhanKhususId;

    /**
     * @var        MstWilayah
     */
    protected $aMstWilayahRelatedByKodeWilayah;

    /**
     * @var        MstWilayah
     */
    protected $aMstWilayahRelatedByKodeWilayah;

    /**
     * @var        Negara
     */
    protected $aNegaraRelatedByKewarganegaraan;

    /**
     * @var        Negara
     */
    protected $aNegaraRelatedByKewarganegaraan;

    /**
     * @var        Pekerjaan
     */
    protected $aPekerjaanRelatedByPekerjaanIdAyah;

    /**
     * @var        Pekerjaan
     */
    protected $aPekerjaanRelatedByPekerjaanIdIbu;

    /**
     * @var        Pekerjaan
     */
    protected $aPekerjaanRelatedByPekerjaanIdWali;

    /**
     * @var        Pekerjaan
     */
    protected $aPekerjaanRelatedByPekerjaanIdAyah;

    /**
     * @var        Pekerjaan
     */
    protected $aPekerjaanRelatedByPekerjaanIdIbu;

    /**
     * @var        Pekerjaan
     */
    protected $aPekerjaanRelatedByPekerjaanIdWali;

    /**
     * @var        PenghasilanOrangtuaWali
     */
    protected $aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah;

    /**
     * @var        PenghasilanOrangtuaWali
     */
    protected $aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali;

    /**
     * @var        PenghasilanOrangtuaWali
     */
    protected $aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu;

    /**
     * @var        PenghasilanOrangtuaWali
     */
    protected $aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah;

    /**
     * @var        PenghasilanOrangtuaWali
     */
    protected $aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali;

    /**
     * @var        PenghasilanOrangtuaWali
     */
    protected $aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu;

    /**
     * @var        PropelObjectCollection|Prestasi[] Collection to store aggregation of Prestasi objects.
     */
    protected $collPrestasisRelatedByPesertaDidikId;
    protected $collPrestasisRelatedByPesertaDidikIdPartial;

    /**
     * @var        PropelObjectCollection|Prestasi[] Collection to store aggregation of Prestasi objects.
     */
    protected $collPrestasisRelatedByPesertaDidikId;
    protected $collPrestasisRelatedByPesertaDidikIdPartial;

    /**
     * @var        PropelObjectCollection|AnggotaRombel[] Collection to store aggregation of AnggotaRombel objects.
     */
    protected $collAnggotaRombelsRelatedByPesertaDidikId;
    protected $collAnggotaRombelsRelatedByPesertaDidikIdPartial;

    /**
     * @var        PropelObjectCollection|AnggotaRombel[] Collection to store aggregation of AnggotaRombel objects.
     */
    protected $collAnggotaRombelsRelatedByPesertaDidikId;
    protected $collAnggotaRombelsRelatedByPesertaDidikIdPartial;

    /**
     * @var        PropelObjectCollection|PesertaDidikLongitudinal[] Collection to store aggregation of PesertaDidikLongitudinal objects.
     */
    protected $collPesertaDidikLongitudinalsRelatedByPesertaDidikId;
    protected $collPesertaDidikLongitudinalsRelatedByPesertaDidikIdPartial;

    /**
     * @var        PropelObjectCollection|PesertaDidikLongitudinal[] Collection to store aggregation of PesertaDidikLongitudinal objects.
     */
    protected $collPesertaDidikLongitudinalsRelatedByPesertaDidikId;
    protected $collPesertaDidikLongitudinalsRelatedByPesertaDidikIdPartial;

    /**
     * @var        PropelObjectCollection|BeasiswaPesertaDidik[] Collection to store aggregation of BeasiswaPesertaDidik objects.
     */
    protected $collBeasiswaPesertaDidiksRelatedByPesertaDidikId;
    protected $collBeasiswaPesertaDidiksRelatedByPesertaDidikIdPartial;

    /**
     * @var        PropelObjectCollection|BeasiswaPesertaDidik[] Collection to store aggregation of BeasiswaPesertaDidik objects.
     */
    protected $collBeasiswaPesertaDidiksRelatedByPesertaDidikId;
    protected $collBeasiswaPesertaDidiksRelatedByPesertaDidikIdPartial;

    /**
     * @var        PropelObjectCollection|RegistrasiPesertaDidik[] Collection to store aggregation of RegistrasiPesertaDidik objects.
     */
    protected $collRegistrasiPesertaDidiksRelatedByPesertaDidikId;
    protected $collRegistrasiPesertaDidiksRelatedByPesertaDidikIdPartial;

    /**
     * @var        PropelObjectCollection|RegistrasiPesertaDidik[] Collection to store aggregation of RegistrasiPesertaDidik objects.
     */
    protected $collRegistrasiPesertaDidiksRelatedByPesertaDidikId;
    protected $collRegistrasiPesertaDidiksRelatedByPesertaDidikIdPartial;

    /**
     * @var        PropelObjectCollection|VldPesertaDidik[] Collection to store aggregation of VldPesertaDidik objects.
     */
    protected $collVldPesertaDidiksRelatedByPesertaDidikId;
    protected $collVldPesertaDidiksRelatedByPesertaDidikIdPartial;

    /**
     * @var        PropelObjectCollection|VldPesertaDidik[] Collection to store aggregation of VldPesertaDidik objects.
     */
    protected $collVldPesertaDidiksRelatedByPesertaDidikId;
    protected $collVldPesertaDidiksRelatedByPesertaDidikIdPartial;

    /**
     * @var        PropelObjectCollection|PesertaDidikBaru[] Collection to store aggregation of PesertaDidikBaru objects.
     */
    protected $collPesertaDidikBarusRelatedByPesertaDidikId;
    protected $collPesertaDidikBarusRelatedByPesertaDidikIdPartial;

    /**
     * @var        PropelObjectCollection|PesertaDidikBaru[] Collection to store aggregation of PesertaDidikBaru objects.
     */
    protected $collPesertaDidikBarusRelatedByPesertaDidikId;
    protected $collPesertaDidikBarusRelatedByPesertaDidikIdPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $prestasisRelatedByPesertaDidikIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $prestasisRelatedByPesertaDidikIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $anggotaRombelsRelatedByPesertaDidikIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $anggotaRombelsRelatedByPesertaDidikIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $pesertaDidikLongitudinalsRelatedByPesertaDidikIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $pesertaDidikLongitudinalsRelatedByPesertaDidikIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $beasiswaPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $beasiswaPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $registrasiPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $registrasiPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $pesertaDidikBarusRelatedByPesertaDidikIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $pesertaDidikBarusRelatedByPesertaDidikIdScheduledForDeletion = null;

    /**
     * Get the [peserta_didik_id] column value.
     * 
     * @return string
     */
    public function getPesertaDidikId()
    {
        return $this->peserta_didik_id;
    }

    /**
     * Get the [nama] column value.
     * 
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Get the [jenis_kelamin] column value.
     * 
     * @return string
     */
    public function getJenisKelamin()
    {
        return $this->jenis_kelamin;
    }

    /**
     * Get the [nisn] column value.
     * 
     * @return string
     */
    public function getNisn()
    {
        return $this->nisn;
    }

    /**
     * Get the [nik] column value.
     * 
     * @return string
     */
    public function getNik()
    {
        return $this->nik;
    }

    /**
     * Get the [tempat_lahir] column value.
     * 
     * @return string
     */
    public function getTempatLahir()
    {
        return $this->tempat_lahir;
    }

    /**
     * Get the [tanggal_lahir] column value.
     * 
     * @return string
     */
    public function getTanggalLahir()
    {
        return $this->tanggal_lahir;
    }

    /**
     * Get the [agama_id] column value.
     * 
     * @return int
     */
    public function getAgamaId()
    {
        return $this->agama_id;
    }

    /**
     * Get the [kewarganegaraan] column value.
     * 
     * @return string
     */
    public function getKewarganegaraan()
    {
        return $this->kewarganegaraan;
    }

    /**
     * Get the [kebutuhan_khusus_id] column value.
     * 
     * @return int
     */
    public function getKebutuhanKhususId()
    {
        return $this->kebutuhan_khusus_id;
    }

    /**
     * Get the [sekolah_id] column value.
     * 
     * @return string
     */
    public function getSekolahId()
    {
        return $this->sekolah_id;
    }

    /**
     * Get the [alamat_jalan] column value.
     * 
     * @return string
     */
    public function getAlamatJalan()
    {
        return $this->alamat_jalan;
    }

    /**
     * Get the [rt] column value.
     * 
     * @return string
     */
    public function getRt()
    {
        return $this->rt;
    }

    /**
     * Get the [rw] column value.
     * 
     * @return string
     */
    public function getRw()
    {
        return $this->rw;
    }

    /**
     * Get the [nama_dusun] column value.
     * 
     * @return string
     */
    public function getNamaDusun()
    {
        return $this->nama_dusun;
    }

    /**
     * Get the [desa_kelurahan] column value.
     * 
     * @return string
     */
    public function getDesaKelurahan()
    {
        return $this->desa_kelurahan;
    }

    /**
     * Get the [kode_wilayah] column value.
     * 
     * @return string
     */
    public function getKodeWilayah()
    {
        return $this->kode_wilayah;
    }

    /**
     * Get the [kode_pos] column value.
     * 
     * @return string
     */
    public function getKodePos()
    {
        return $this->kode_pos;
    }

    /**
     * Get the [jenis_tinggal_id] column value.
     * 
     * @return string
     */
    public function getJenisTinggalId()
    {
        return $this->jenis_tinggal_id;
    }

    /**
     * Get the [alat_transportasi_id] column value.
     * 
     * @return string
     */
    public function getAlatTransportasiId()
    {
        return $this->alat_transportasi_id;
    }

    /**
     * Get the [nomor_telepon_rumah] column value.
     * 
     * @return string
     */
    public function getNomorTeleponRumah()
    {
        return $this->nomor_telepon_rumah;
    }

    /**
     * Get the [nomor_telepon_seluler] column value.
     * 
     * @return string
     */
    public function getNomorTeleponSeluler()
    {
        return $this->nomor_telepon_seluler;
    }

    /**
     * Get the [email] column value.
     * 
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Get the [penerima_kps] column value.
     * 
     * @return string
     */
    public function getPenerimaKps()
    {
        return $this->penerima_kps;
    }

    /**
     * Get the [no_kps] column value.
     * 
     * @return string
     */
    public function getNoKps()
    {
        return $this->no_kps;
    }

    /**
     * Get the [status_data] column value.
     * 
     * @return int
     */
    public function getStatusData()
    {
        return $this->status_data;
    }

    /**
     * Get the [nama_ayah] column value.
     * 
     * @return string
     */
    public function getNamaAyah()
    {
        return $this->nama_ayah;
    }

    /**
     * Get the [tahun_lahir_ayah] column value.
     * 
     * @return string
     */
    public function getTahunLahirAyah()
    {
        return $this->tahun_lahir_ayah;
    }

    /**
     * Get the [jenjang_pendidikan_ayah] column value.
     * 
     * @return string
     */
    public function getJenjangPendidikanAyah()
    {
        return $this->jenjang_pendidikan_ayah;
    }

    /**
     * Get the [pekerjaan_id_ayah] column value.
     * 
     * @return int
     */
    public function getPekerjaanIdAyah()
    {
        return $this->pekerjaan_id_ayah;
    }

    /**
     * Get the [penghasilan_id_ayah] column value.
     * 
     * @return int
     */
    public function getPenghasilanIdAyah()
    {
        return $this->penghasilan_id_ayah;
    }

    /**
     * Get the [kebutuhan_khusus_id_ayah] column value.
     * 
     * @return int
     */
    public function getKebutuhanKhususIdAyah()
    {
        return $this->kebutuhan_khusus_id_ayah;
    }

    /**
     * Get the [nama_ibu_kandung] column value.
     * 
     * @return string
     */
    public function getNamaIbuKandung()
    {
        return $this->nama_ibu_kandung;
    }

    /**
     * Get the [tahun_lahir_ibu] column value.
     * 
     * @return string
     */
    public function getTahunLahirIbu()
    {
        return $this->tahun_lahir_ibu;
    }

    /**
     * Get the [jenjang_pendidikan_ibu] column value.
     * 
     * @return string
     */
    public function getJenjangPendidikanIbu()
    {
        return $this->jenjang_pendidikan_ibu;
    }

    /**
     * Get the [penghasilan_id_ibu] column value.
     * 
     * @return int
     */
    public function getPenghasilanIdIbu()
    {
        return $this->penghasilan_id_ibu;
    }

    /**
     * Get the [pekerjaan_id_ibu] column value.
     * 
     * @return int
     */
    public function getPekerjaanIdIbu()
    {
        return $this->pekerjaan_id_ibu;
    }

    /**
     * Get the [kebutuhan_khusus_id_ibu] column value.
     * 
     * @return int
     */
    public function getKebutuhanKhususIdIbu()
    {
        return $this->kebutuhan_khusus_id_ibu;
    }

    /**
     * Get the [nama_wali] column value.
     * 
     * @return string
     */
    public function getNamaWali()
    {
        return $this->nama_wali;
    }

    /**
     * Get the [tahun_lahir_wali] column value.
     * 
     * @return string
     */
    public function getTahunLahirWali()
    {
        return $this->tahun_lahir_wali;
    }

    /**
     * Get the [jenjang_pendidikan_wali] column value.
     * 
     * @return string
     */
    public function getJenjangPendidikanWali()
    {
        return $this->jenjang_pendidikan_wali;
    }

    /**
     * Get the [pekerjaan_id_wali] column value.
     * 
     * @return int
     */
    public function getPekerjaanIdWali()
    {
        return $this->pekerjaan_id_wali;
    }

    /**
     * Get the [penghasilan_id_wali] column value.
     * 
     * @return int
     */
    public function getPenghasilanIdWali()
    {
        return $this->penghasilan_id_wali;
    }

    /**
     * Get the [optionally formatted] temporal [last_update] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastUpdate($format = 'Y-m-d H:i:s')
    {
        if ($this->last_update === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_update);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_update, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [soft_delete] column value.
     * 
     * @return string
     */
    public function getSoftDelete()
    {
        return $this->soft_delete;
    }

    /**
     * Get the [optionally formatted] temporal [last_sync] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastSync($format = 'Y-m-d H:i:s')
    {
        if ($this->last_sync === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_sync);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_sync, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [updater_id] column value.
     * 
     * @return string
     */
    public function getUpdaterId()
    {
        return $this->updater_id;
    }

    /**
     * Set the value of [peserta_didik_id] column.
     * 
     * @param string $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setPesertaDidikId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->peserta_didik_id !== $v) {
            $this->peserta_didik_id = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::PESERTA_DIDIK_ID;
        }


        return $this;
    } // setPesertaDidikId()

    /**
     * Set the value of [nama] column.
     * 
     * @param string $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setNama($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nama !== $v) {
            $this->nama = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::NAMA;
        }


        return $this;
    } // setNama()

    /**
     * Set the value of [jenis_kelamin] column.
     * 
     * @param string $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setJenisKelamin($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->jenis_kelamin !== $v) {
            $this->jenis_kelamin = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::JENIS_KELAMIN;
        }


        return $this;
    } // setJenisKelamin()

    /**
     * Set the value of [nisn] column.
     * 
     * @param string $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setNisn($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nisn !== $v) {
            $this->nisn = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::NISN;
        }


        return $this;
    } // setNisn()

    /**
     * Set the value of [nik] column.
     * 
     * @param string $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setNik($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nik !== $v) {
            $this->nik = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::NIK;
        }


        return $this;
    } // setNik()

    /**
     * Set the value of [tempat_lahir] column.
     * 
     * @param string $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setTempatLahir($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tempat_lahir !== $v) {
            $this->tempat_lahir = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::TEMPAT_LAHIR;
        }


        return $this;
    } // setTempatLahir()

    /**
     * Set the value of [tanggal_lahir] column.
     * 
     * @param string $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setTanggalLahir($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tanggal_lahir !== $v) {
            $this->tanggal_lahir = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::TANGGAL_LAHIR;
        }


        return $this;
    } // setTanggalLahir()

    /**
     * Set the value of [agama_id] column.
     * 
     * @param int $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setAgamaId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->agama_id !== $v) {
            $this->agama_id = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::AGAMA_ID;
        }

        if ($this->aAgamaRelatedByAgamaId !== null && $this->aAgamaRelatedByAgamaId->getAgamaId() !== $v) {
            $this->aAgamaRelatedByAgamaId = null;
        }

        if ($this->aAgamaRelatedByAgamaId !== null && $this->aAgamaRelatedByAgamaId->getAgamaId() !== $v) {
            $this->aAgamaRelatedByAgamaId = null;
        }


        return $this;
    } // setAgamaId()

    /**
     * Set the value of [kewarganegaraan] column.
     * 
     * @param string $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setKewarganegaraan($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->kewarganegaraan !== $v) {
            $this->kewarganegaraan = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::KEWARGANEGARAAN;
        }

        if ($this->aNegaraRelatedByKewarganegaraan !== null && $this->aNegaraRelatedByKewarganegaraan->getNegaraId() !== $v) {
            $this->aNegaraRelatedByKewarganegaraan = null;
        }

        if ($this->aNegaraRelatedByKewarganegaraan !== null && $this->aNegaraRelatedByKewarganegaraan->getNegaraId() !== $v) {
            $this->aNegaraRelatedByKewarganegaraan = null;
        }


        return $this;
    } // setKewarganegaraan()

    /**
     * Set the value of [kebutuhan_khusus_id] column.
     * 
     * @param int $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setKebutuhanKhususId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->kebutuhan_khusus_id !== $v) {
            $this->kebutuhan_khusus_id = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::KEBUTUHAN_KHUSUS_ID;
        }

        if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId !== null && $this->aKebutuhanKhususRelatedByKebutuhanKhususId->getKebutuhanKhususId() !== $v) {
            $this->aKebutuhanKhususRelatedByKebutuhanKhususId = null;
        }

        if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId !== null && $this->aKebutuhanKhususRelatedByKebutuhanKhususId->getKebutuhanKhususId() !== $v) {
            $this->aKebutuhanKhususRelatedByKebutuhanKhususId = null;
        }


        return $this;
    } // setKebutuhanKhususId()

    /**
     * Set the value of [sekolah_id] column.
     * 
     * @param string $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setSekolahId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->sekolah_id !== $v) {
            $this->sekolah_id = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::SEKOLAH_ID;
        }

        if ($this->aSekolahRelatedBySekolahId !== null && $this->aSekolahRelatedBySekolahId->getSekolahId() !== $v) {
            $this->aSekolahRelatedBySekolahId = null;
        }

        if ($this->aSekolahRelatedBySekolahId !== null && $this->aSekolahRelatedBySekolahId->getSekolahId() !== $v) {
            $this->aSekolahRelatedBySekolahId = null;
        }


        return $this;
    } // setSekolahId()

    /**
     * Set the value of [alamat_jalan] column.
     * 
     * @param string $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setAlamatJalan($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->alamat_jalan !== $v) {
            $this->alamat_jalan = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::ALAMAT_JALAN;
        }


        return $this;
    } // setAlamatJalan()

    /**
     * Set the value of [rt] column.
     * 
     * @param string $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setRt($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->rt !== $v) {
            $this->rt = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::RT;
        }


        return $this;
    } // setRt()

    /**
     * Set the value of [rw] column.
     * 
     * @param string $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setRw($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->rw !== $v) {
            $this->rw = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::RW;
        }


        return $this;
    } // setRw()

    /**
     * Set the value of [nama_dusun] column.
     * 
     * @param string $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setNamaDusun($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nama_dusun !== $v) {
            $this->nama_dusun = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::NAMA_DUSUN;
        }


        return $this;
    } // setNamaDusun()

    /**
     * Set the value of [desa_kelurahan] column.
     * 
     * @param string $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setDesaKelurahan($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->desa_kelurahan !== $v) {
            $this->desa_kelurahan = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::DESA_KELURAHAN;
        }


        return $this;
    } // setDesaKelurahan()

    /**
     * Set the value of [kode_wilayah] column.
     * 
     * @param string $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setKodeWilayah($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->kode_wilayah !== $v) {
            $this->kode_wilayah = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::KODE_WILAYAH;
        }

        if ($this->aMstWilayahRelatedByKodeWilayah !== null && $this->aMstWilayahRelatedByKodeWilayah->getKodeWilayah() !== $v) {
            $this->aMstWilayahRelatedByKodeWilayah = null;
        }

        if ($this->aMstWilayahRelatedByKodeWilayah !== null && $this->aMstWilayahRelatedByKodeWilayah->getKodeWilayah() !== $v) {
            $this->aMstWilayahRelatedByKodeWilayah = null;
        }


        return $this;
    } // setKodeWilayah()

    /**
     * Set the value of [kode_pos] column.
     * 
     * @param string $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setKodePos($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->kode_pos !== $v) {
            $this->kode_pos = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::KODE_POS;
        }


        return $this;
    } // setKodePos()

    /**
     * Set the value of [jenis_tinggal_id] column.
     * 
     * @param string $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setJenisTinggalId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->jenis_tinggal_id !== $v) {
            $this->jenis_tinggal_id = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::JENIS_TINGGAL_ID;
        }

        if ($this->aJenisTinggalRelatedByJenisTinggalId !== null && $this->aJenisTinggalRelatedByJenisTinggalId->getJenisTinggalId() !== $v) {
            $this->aJenisTinggalRelatedByJenisTinggalId = null;
        }

        if ($this->aJenisTinggalRelatedByJenisTinggalId !== null && $this->aJenisTinggalRelatedByJenisTinggalId->getJenisTinggalId() !== $v) {
            $this->aJenisTinggalRelatedByJenisTinggalId = null;
        }


        return $this;
    } // setJenisTinggalId()

    /**
     * Set the value of [alat_transportasi_id] column.
     * 
     * @param string $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setAlatTransportasiId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->alat_transportasi_id !== $v) {
            $this->alat_transportasi_id = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::ALAT_TRANSPORTASI_ID;
        }

        if ($this->aAlatTransportasiRelatedByAlatTransportasiId !== null && $this->aAlatTransportasiRelatedByAlatTransportasiId->getAlatTransportasiId() !== $v) {
            $this->aAlatTransportasiRelatedByAlatTransportasiId = null;
        }

        if ($this->aAlatTransportasiRelatedByAlatTransportasiId !== null && $this->aAlatTransportasiRelatedByAlatTransportasiId->getAlatTransportasiId() !== $v) {
            $this->aAlatTransportasiRelatedByAlatTransportasiId = null;
        }


        return $this;
    } // setAlatTransportasiId()

    /**
     * Set the value of [nomor_telepon_rumah] column.
     * 
     * @param string $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setNomorTeleponRumah($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nomor_telepon_rumah !== $v) {
            $this->nomor_telepon_rumah = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::NOMOR_TELEPON_RUMAH;
        }


        return $this;
    } // setNomorTeleponRumah()

    /**
     * Set the value of [nomor_telepon_seluler] column.
     * 
     * @param string $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setNomorTeleponSeluler($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nomor_telepon_seluler !== $v) {
            $this->nomor_telepon_seluler = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::NOMOR_TELEPON_SELULER;
        }


        return $this;
    } // setNomorTeleponSeluler()

    /**
     * Set the value of [email] column.
     * 
     * @param string $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setEmail($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->email !== $v) {
            $this->email = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::EMAIL;
        }


        return $this;
    } // setEmail()

    /**
     * Set the value of [penerima_kps] column.
     * 
     * @param string $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setPenerimaKps($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->penerima_kps !== $v) {
            $this->penerima_kps = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::PENERIMA_KPS;
        }


        return $this;
    } // setPenerimaKps()

    /**
     * Set the value of [no_kps] column.
     * 
     * @param string $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setNoKps($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->no_kps !== $v) {
            $this->no_kps = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::NO_KPS;
        }


        return $this;
    } // setNoKps()

    /**
     * Set the value of [status_data] column.
     * 
     * @param int $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setStatusData($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->status_data !== $v) {
            $this->status_data = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::STATUS_DATA;
        }


        return $this;
    } // setStatusData()

    /**
     * Set the value of [nama_ayah] column.
     * 
     * @param string $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setNamaAyah($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nama_ayah !== $v) {
            $this->nama_ayah = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::NAMA_AYAH;
        }


        return $this;
    } // setNamaAyah()

    /**
     * Set the value of [tahun_lahir_ayah] column.
     * 
     * @param string $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setTahunLahirAyah($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tahun_lahir_ayah !== $v) {
            $this->tahun_lahir_ayah = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::TAHUN_LAHIR_AYAH;
        }


        return $this;
    } // setTahunLahirAyah()

    /**
     * Set the value of [jenjang_pendidikan_ayah] column.
     * 
     * @param string $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setJenjangPendidikanAyah($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->jenjang_pendidikan_ayah !== $v) {
            $this->jenjang_pendidikan_ayah = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::JENJANG_PENDIDIKAN_AYAH;
        }

        if ($this->aJenjangPendidikanRelatedByJenjangPendidikanAyah !== null && $this->aJenjangPendidikanRelatedByJenjangPendidikanAyah->getJenjangPendidikanId() !== $v) {
            $this->aJenjangPendidikanRelatedByJenjangPendidikanAyah = null;
        }

        if ($this->aJenjangPendidikanRelatedByJenjangPendidikanAyah !== null && $this->aJenjangPendidikanRelatedByJenjangPendidikanAyah->getJenjangPendidikanId() !== $v) {
            $this->aJenjangPendidikanRelatedByJenjangPendidikanAyah = null;
        }


        return $this;
    } // setJenjangPendidikanAyah()

    /**
     * Set the value of [pekerjaan_id_ayah] column.
     * 
     * @param int $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setPekerjaanIdAyah($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->pekerjaan_id_ayah !== $v) {
            $this->pekerjaan_id_ayah = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::PEKERJAAN_ID_AYAH;
        }

        if ($this->aPekerjaanRelatedByPekerjaanIdAyah !== null && $this->aPekerjaanRelatedByPekerjaanIdAyah->getPekerjaanId() !== $v) {
            $this->aPekerjaanRelatedByPekerjaanIdAyah = null;
        }

        if ($this->aPekerjaanRelatedByPekerjaanIdAyah !== null && $this->aPekerjaanRelatedByPekerjaanIdAyah->getPekerjaanId() !== $v) {
            $this->aPekerjaanRelatedByPekerjaanIdAyah = null;
        }


        return $this;
    } // setPekerjaanIdAyah()

    /**
     * Set the value of [penghasilan_id_ayah] column.
     * 
     * @param int $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setPenghasilanIdAyah($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->penghasilan_id_ayah !== $v) {
            $this->penghasilan_id_ayah = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::PENGHASILAN_ID_AYAH;
        }

        if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah !== null && $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah->getPenghasilanOrangtuaWaliId() !== $v) {
            $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah = null;
        }

        if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah !== null && $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah->getPenghasilanOrangtuaWaliId() !== $v) {
            $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah = null;
        }


        return $this;
    } // setPenghasilanIdAyah()

    /**
     * Set the value of [kebutuhan_khusus_id_ayah] column.
     * 
     * @param int $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setKebutuhanKhususIdAyah($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->kebutuhan_khusus_id_ayah !== $v) {
            $this->kebutuhan_khusus_id_ayah = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::KEBUTUHAN_KHUSUS_ID_AYAH;
        }

        if ($this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah !== null && $this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah->getKebutuhanKhususId() !== $v) {
            $this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah = null;
        }

        if ($this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah !== null && $this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah->getKebutuhanKhususId() !== $v) {
            $this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah = null;
        }


        return $this;
    } // setKebutuhanKhususIdAyah()

    /**
     * Set the value of [nama_ibu_kandung] column.
     * 
     * @param string $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setNamaIbuKandung($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nama_ibu_kandung !== $v) {
            $this->nama_ibu_kandung = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::NAMA_IBU_KANDUNG;
        }


        return $this;
    } // setNamaIbuKandung()

    /**
     * Set the value of [tahun_lahir_ibu] column.
     * 
     * @param string $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setTahunLahirIbu($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tahun_lahir_ibu !== $v) {
            $this->tahun_lahir_ibu = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::TAHUN_LAHIR_IBU;
        }


        return $this;
    } // setTahunLahirIbu()

    /**
     * Set the value of [jenjang_pendidikan_ibu] column.
     * 
     * @param string $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setJenjangPendidikanIbu($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->jenjang_pendidikan_ibu !== $v) {
            $this->jenjang_pendidikan_ibu = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::JENJANG_PENDIDIKAN_IBU;
        }

        if ($this->aJenjangPendidikanRelatedByJenjangPendidikanIbu !== null && $this->aJenjangPendidikanRelatedByJenjangPendidikanIbu->getJenjangPendidikanId() !== $v) {
            $this->aJenjangPendidikanRelatedByJenjangPendidikanIbu = null;
        }

        if ($this->aJenjangPendidikanRelatedByJenjangPendidikanIbu !== null && $this->aJenjangPendidikanRelatedByJenjangPendidikanIbu->getJenjangPendidikanId() !== $v) {
            $this->aJenjangPendidikanRelatedByJenjangPendidikanIbu = null;
        }


        return $this;
    } // setJenjangPendidikanIbu()

    /**
     * Set the value of [penghasilan_id_ibu] column.
     * 
     * @param int $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setPenghasilanIdIbu($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->penghasilan_id_ibu !== $v) {
            $this->penghasilan_id_ibu = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::PENGHASILAN_ID_IBU;
        }

        if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu !== null && $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu->getPenghasilanOrangtuaWaliId() !== $v) {
            $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu = null;
        }

        if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu !== null && $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu->getPenghasilanOrangtuaWaliId() !== $v) {
            $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu = null;
        }


        return $this;
    } // setPenghasilanIdIbu()

    /**
     * Set the value of [pekerjaan_id_ibu] column.
     * 
     * @param int $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setPekerjaanIdIbu($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->pekerjaan_id_ibu !== $v) {
            $this->pekerjaan_id_ibu = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::PEKERJAAN_ID_IBU;
        }

        if ($this->aPekerjaanRelatedByPekerjaanIdIbu !== null && $this->aPekerjaanRelatedByPekerjaanIdIbu->getPekerjaanId() !== $v) {
            $this->aPekerjaanRelatedByPekerjaanIdIbu = null;
        }

        if ($this->aPekerjaanRelatedByPekerjaanIdIbu !== null && $this->aPekerjaanRelatedByPekerjaanIdIbu->getPekerjaanId() !== $v) {
            $this->aPekerjaanRelatedByPekerjaanIdIbu = null;
        }


        return $this;
    } // setPekerjaanIdIbu()

    /**
     * Set the value of [kebutuhan_khusus_id_ibu] column.
     * 
     * @param int $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setKebutuhanKhususIdIbu($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->kebutuhan_khusus_id_ibu !== $v) {
            $this->kebutuhan_khusus_id_ibu = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::KEBUTUHAN_KHUSUS_ID_IBU;
        }

        if ($this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu !== null && $this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu->getKebutuhanKhususId() !== $v) {
            $this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu = null;
        }

        if ($this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu !== null && $this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu->getKebutuhanKhususId() !== $v) {
            $this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu = null;
        }


        return $this;
    } // setKebutuhanKhususIdIbu()

    /**
     * Set the value of [nama_wali] column.
     * 
     * @param string $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setNamaWali($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nama_wali !== $v) {
            $this->nama_wali = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::NAMA_WALI;
        }


        return $this;
    } // setNamaWali()

    /**
     * Set the value of [tahun_lahir_wali] column.
     * 
     * @param string $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setTahunLahirWali($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tahun_lahir_wali !== $v) {
            $this->tahun_lahir_wali = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::TAHUN_LAHIR_WALI;
        }


        return $this;
    } // setTahunLahirWali()

    /**
     * Set the value of [jenjang_pendidikan_wali] column.
     * 
     * @param string $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setJenjangPendidikanWali($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->jenjang_pendidikan_wali !== $v) {
            $this->jenjang_pendidikan_wali = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::JENJANG_PENDIDIKAN_WALI;
        }

        if ($this->aJenjangPendidikanRelatedByJenjangPendidikanWali !== null && $this->aJenjangPendidikanRelatedByJenjangPendidikanWali->getJenjangPendidikanId() !== $v) {
            $this->aJenjangPendidikanRelatedByJenjangPendidikanWali = null;
        }

        if ($this->aJenjangPendidikanRelatedByJenjangPendidikanWali !== null && $this->aJenjangPendidikanRelatedByJenjangPendidikanWali->getJenjangPendidikanId() !== $v) {
            $this->aJenjangPendidikanRelatedByJenjangPendidikanWali = null;
        }


        return $this;
    } // setJenjangPendidikanWali()

    /**
     * Set the value of [pekerjaan_id_wali] column.
     * 
     * @param int $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setPekerjaanIdWali($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->pekerjaan_id_wali !== $v) {
            $this->pekerjaan_id_wali = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::PEKERJAAN_ID_WALI;
        }

        if ($this->aPekerjaanRelatedByPekerjaanIdWali !== null && $this->aPekerjaanRelatedByPekerjaanIdWali->getPekerjaanId() !== $v) {
            $this->aPekerjaanRelatedByPekerjaanIdWali = null;
        }

        if ($this->aPekerjaanRelatedByPekerjaanIdWali !== null && $this->aPekerjaanRelatedByPekerjaanIdWali->getPekerjaanId() !== $v) {
            $this->aPekerjaanRelatedByPekerjaanIdWali = null;
        }


        return $this;
    } // setPekerjaanIdWali()

    /**
     * Set the value of [penghasilan_id_wali] column.
     * 
     * @param int $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setPenghasilanIdWali($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->penghasilan_id_wali !== $v) {
            $this->penghasilan_id_wali = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::PENGHASILAN_ID_WALI;
        }

        if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali !== null && $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali->getPenghasilanOrangtuaWaliId() !== $v) {
            $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali = null;
        }

        if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali !== null && $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali->getPenghasilanOrangtuaWaliId() !== $v) {
            $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali = null;
        }


        return $this;
    } // setPenghasilanIdWali()

    /**
     * Sets the value of [last_update] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setLastUpdate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_update !== null || $dt !== null) {
            $currentDateAsString = ($this->last_update !== null && $tmpDt = new DateTime($this->last_update)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_update = $newDateAsString;
                $this->modifiedColumns[] = PesertaDidikPeer::LAST_UPDATE;
            }
        } // if either are not null


        return $this;
    } // setLastUpdate()

    /**
     * Set the value of [soft_delete] column.
     * 
     * @param string $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setSoftDelete($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->soft_delete !== $v) {
            $this->soft_delete = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::SOFT_DELETE;
        }


        return $this;
    } // setSoftDelete()

    /**
     * Sets the value of [last_sync] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setLastSync($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_sync !== null || $dt !== null) {
            $currentDateAsString = ($this->last_sync !== null && $tmpDt = new DateTime($this->last_sync)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_sync = $newDateAsString;
                $this->modifiedColumns[] = PesertaDidikPeer::LAST_SYNC;
            }
        } // if either are not null


        return $this;
    } // setLastSync()

    /**
     * Set the value of [updater_id] column.
     * 
     * @param string $v new value
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setUpdaterId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->updater_id !== $v) {
            $this->updater_id = $v;
            $this->modifiedColumns[] = PesertaDidikPeer::UPDATER_ID;
        }


        return $this;
    } // setUpdaterId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->peserta_didik_id = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
            $this->nama = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->jenis_kelamin = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->nisn = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->nik = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->tempat_lahir = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->tanggal_lahir = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->agama_id = ($row[$startcol + 7] !== null) ? (int) $row[$startcol + 7] : null;
            $this->kewarganegaraan = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->kebutuhan_khusus_id = ($row[$startcol + 9] !== null) ? (int) $row[$startcol + 9] : null;
            $this->sekolah_id = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->alamat_jalan = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->rt = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->rw = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->nama_dusun = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
            $this->desa_kelurahan = ($row[$startcol + 15] !== null) ? (string) $row[$startcol + 15] : null;
            $this->kode_wilayah = ($row[$startcol + 16] !== null) ? (string) $row[$startcol + 16] : null;
            $this->kode_pos = ($row[$startcol + 17] !== null) ? (string) $row[$startcol + 17] : null;
            $this->jenis_tinggal_id = ($row[$startcol + 18] !== null) ? (string) $row[$startcol + 18] : null;
            $this->alat_transportasi_id = ($row[$startcol + 19] !== null) ? (string) $row[$startcol + 19] : null;
            $this->nomor_telepon_rumah = ($row[$startcol + 20] !== null) ? (string) $row[$startcol + 20] : null;
            $this->nomor_telepon_seluler = ($row[$startcol + 21] !== null) ? (string) $row[$startcol + 21] : null;
            $this->email = ($row[$startcol + 22] !== null) ? (string) $row[$startcol + 22] : null;
            $this->penerima_kps = ($row[$startcol + 23] !== null) ? (string) $row[$startcol + 23] : null;
            $this->no_kps = ($row[$startcol + 24] !== null) ? (string) $row[$startcol + 24] : null;
            $this->status_data = ($row[$startcol + 25] !== null) ? (int) $row[$startcol + 25] : null;
            $this->nama_ayah = ($row[$startcol + 26] !== null) ? (string) $row[$startcol + 26] : null;
            $this->tahun_lahir_ayah = ($row[$startcol + 27] !== null) ? (string) $row[$startcol + 27] : null;
            $this->jenjang_pendidikan_ayah = ($row[$startcol + 28] !== null) ? (string) $row[$startcol + 28] : null;
            $this->pekerjaan_id_ayah = ($row[$startcol + 29] !== null) ? (int) $row[$startcol + 29] : null;
            $this->penghasilan_id_ayah = ($row[$startcol + 30] !== null) ? (int) $row[$startcol + 30] : null;
            $this->kebutuhan_khusus_id_ayah = ($row[$startcol + 31] !== null) ? (int) $row[$startcol + 31] : null;
            $this->nama_ibu_kandung = ($row[$startcol + 32] !== null) ? (string) $row[$startcol + 32] : null;
            $this->tahun_lahir_ibu = ($row[$startcol + 33] !== null) ? (string) $row[$startcol + 33] : null;
            $this->jenjang_pendidikan_ibu = ($row[$startcol + 34] !== null) ? (string) $row[$startcol + 34] : null;
            $this->penghasilan_id_ibu = ($row[$startcol + 35] !== null) ? (int) $row[$startcol + 35] : null;
            $this->pekerjaan_id_ibu = ($row[$startcol + 36] !== null) ? (int) $row[$startcol + 36] : null;
            $this->kebutuhan_khusus_id_ibu = ($row[$startcol + 37] !== null) ? (int) $row[$startcol + 37] : null;
            $this->nama_wali = ($row[$startcol + 38] !== null) ? (string) $row[$startcol + 38] : null;
            $this->tahun_lahir_wali = ($row[$startcol + 39] !== null) ? (string) $row[$startcol + 39] : null;
            $this->jenjang_pendidikan_wali = ($row[$startcol + 40] !== null) ? (string) $row[$startcol + 40] : null;
            $this->pekerjaan_id_wali = ($row[$startcol + 41] !== null) ? (int) $row[$startcol + 41] : null;
            $this->penghasilan_id_wali = ($row[$startcol + 42] !== null) ? (int) $row[$startcol + 42] : null;
            $this->last_update = ($row[$startcol + 43] !== null) ? (string) $row[$startcol + 43] : null;
            $this->soft_delete = ($row[$startcol + 44] !== null) ? (string) $row[$startcol + 44] : null;
            $this->last_sync = ($row[$startcol + 45] !== null) ? (string) $row[$startcol + 45] : null;
            $this->updater_id = ($row[$startcol + 46] !== null) ? (string) $row[$startcol + 46] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 47; // 47 = PesertaDidikPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating PesertaDidik object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aAgamaRelatedByAgamaId !== null && $this->agama_id !== $this->aAgamaRelatedByAgamaId->getAgamaId()) {
            $this->aAgamaRelatedByAgamaId = null;
        }
        if ($this->aAgamaRelatedByAgamaId !== null && $this->agama_id !== $this->aAgamaRelatedByAgamaId->getAgamaId()) {
            $this->aAgamaRelatedByAgamaId = null;
        }
        if ($this->aNegaraRelatedByKewarganegaraan !== null && $this->kewarganegaraan !== $this->aNegaraRelatedByKewarganegaraan->getNegaraId()) {
            $this->aNegaraRelatedByKewarganegaraan = null;
        }
        if ($this->aNegaraRelatedByKewarganegaraan !== null && $this->kewarganegaraan !== $this->aNegaraRelatedByKewarganegaraan->getNegaraId()) {
            $this->aNegaraRelatedByKewarganegaraan = null;
        }
        if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId !== null && $this->kebutuhan_khusus_id !== $this->aKebutuhanKhususRelatedByKebutuhanKhususId->getKebutuhanKhususId()) {
            $this->aKebutuhanKhususRelatedByKebutuhanKhususId = null;
        }
        if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId !== null && $this->kebutuhan_khusus_id !== $this->aKebutuhanKhususRelatedByKebutuhanKhususId->getKebutuhanKhususId()) {
            $this->aKebutuhanKhususRelatedByKebutuhanKhususId = null;
        }
        if ($this->aSekolahRelatedBySekolahId !== null && $this->sekolah_id !== $this->aSekolahRelatedBySekolahId->getSekolahId()) {
            $this->aSekolahRelatedBySekolahId = null;
        }
        if ($this->aSekolahRelatedBySekolahId !== null && $this->sekolah_id !== $this->aSekolahRelatedBySekolahId->getSekolahId()) {
            $this->aSekolahRelatedBySekolahId = null;
        }
        if ($this->aMstWilayahRelatedByKodeWilayah !== null && $this->kode_wilayah !== $this->aMstWilayahRelatedByKodeWilayah->getKodeWilayah()) {
            $this->aMstWilayahRelatedByKodeWilayah = null;
        }
        if ($this->aMstWilayahRelatedByKodeWilayah !== null && $this->kode_wilayah !== $this->aMstWilayahRelatedByKodeWilayah->getKodeWilayah()) {
            $this->aMstWilayahRelatedByKodeWilayah = null;
        }
        if ($this->aJenisTinggalRelatedByJenisTinggalId !== null && $this->jenis_tinggal_id !== $this->aJenisTinggalRelatedByJenisTinggalId->getJenisTinggalId()) {
            $this->aJenisTinggalRelatedByJenisTinggalId = null;
        }
        if ($this->aJenisTinggalRelatedByJenisTinggalId !== null && $this->jenis_tinggal_id !== $this->aJenisTinggalRelatedByJenisTinggalId->getJenisTinggalId()) {
            $this->aJenisTinggalRelatedByJenisTinggalId = null;
        }
        if ($this->aAlatTransportasiRelatedByAlatTransportasiId !== null && $this->alat_transportasi_id !== $this->aAlatTransportasiRelatedByAlatTransportasiId->getAlatTransportasiId()) {
            $this->aAlatTransportasiRelatedByAlatTransportasiId = null;
        }
        if ($this->aAlatTransportasiRelatedByAlatTransportasiId !== null && $this->alat_transportasi_id !== $this->aAlatTransportasiRelatedByAlatTransportasiId->getAlatTransportasiId()) {
            $this->aAlatTransportasiRelatedByAlatTransportasiId = null;
        }
        if ($this->aJenjangPendidikanRelatedByJenjangPendidikanAyah !== null && $this->jenjang_pendidikan_ayah !== $this->aJenjangPendidikanRelatedByJenjangPendidikanAyah->getJenjangPendidikanId()) {
            $this->aJenjangPendidikanRelatedByJenjangPendidikanAyah = null;
        }
        if ($this->aJenjangPendidikanRelatedByJenjangPendidikanAyah !== null && $this->jenjang_pendidikan_ayah !== $this->aJenjangPendidikanRelatedByJenjangPendidikanAyah->getJenjangPendidikanId()) {
            $this->aJenjangPendidikanRelatedByJenjangPendidikanAyah = null;
        }
        if ($this->aPekerjaanRelatedByPekerjaanIdAyah !== null && $this->pekerjaan_id_ayah !== $this->aPekerjaanRelatedByPekerjaanIdAyah->getPekerjaanId()) {
            $this->aPekerjaanRelatedByPekerjaanIdAyah = null;
        }
        if ($this->aPekerjaanRelatedByPekerjaanIdAyah !== null && $this->pekerjaan_id_ayah !== $this->aPekerjaanRelatedByPekerjaanIdAyah->getPekerjaanId()) {
            $this->aPekerjaanRelatedByPekerjaanIdAyah = null;
        }
        if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah !== null && $this->penghasilan_id_ayah !== $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah->getPenghasilanOrangtuaWaliId()) {
            $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah = null;
        }
        if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah !== null && $this->penghasilan_id_ayah !== $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah->getPenghasilanOrangtuaWaliId()) {
            $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah = null;
        }
        if ($this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah !== null && $this->kebutuhan_khusus_id_ayah !== $this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah->getKebutuhanKhususId()) {
            $this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah = null;
        }
        if ($this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah !== null && $this->kebutuhan_khusus_id_ayah !== $this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah->getKebutuhanKhususId()) {
            $this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah = null;
        }
        if ($this->aJenjangPendidikanRelatedByJenjangPendidikanIbu !== null && $this->jenjang_pendidikan_ibu !== $this->aJenjangPendidikanRelatedByJenjangPendidikanIbu->getJenjangPendidikanId()) {
            $this->aJenjangPendidikanRelatedByJenjangPendidikanIbu = null;
        }
        if ($this->aJenjangPendidikanRelatedByJenjangPendidikanIbu !== null && $this->jenjang_pendidikan_ibu !== $this->aJenjangPendidikanRelatedByJenjangPendidikanIbu->getJenjangPendidikanId()) {
            $this->aJenjangPendidikanRelatedByJenjangPendidikanIbu = null;
        }
        if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu !== null && $this->penghasilan_id_ibu !== $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu->getPenghasilanOrangtuaWaliId()) {
            $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu = null;
        }
        if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu !== null && $this->penghasilan_id_ibu !== $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu->getPenghasilanOrangtuaWaliId()) {
            $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu = null;
        }
        if ($this->aPekerjaanRelatedByPekerjaanIdIbu !== null && $this->pekerjaan_id_ibu !== $this->aPekerjaanRelatedByPekerjaanIdIbu->getPekerjaanId()) {
            $this->aPekerjaanRelatedByPekerjaanIdIbu = null;
        }
        if ($this->aPekerjaanRelatedByPekerjaanIdIbu !== null && $this->pekerjaan_id_ibu !== $this->aPekerjaanRelatedByPekerjaanIdIbu->getPekerjaanId()) {
            $this->aPekerjaanRelatedByPekerjaanIdIbu = null;
        }
        if ($this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu !== null && $this->kebutuhan_khusus_id_ibu !== $this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu->getKebutuhanKhususId()) {
            $this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu = null;
        }
        if ($this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu !== null && $this->kebutuhan_khusus_id_ibu !== $this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu->getKebutuhanKhususId()) {
            $this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu = null;
        }
        if ($this->aJenjangPendidikanRelatedByJenjangPendidikanWali !== null && $this->jenjang_pendidikan_wali !== $this->aJenjangPendidikanRelatedByJenjangPendidikanWali->getJenjangPendidikanId()) {
            $this->aJenjangPendidikanRelatedByJenjangPendidikanWali = null;
        }
        if ($this->aJenjangPendidikanRelatedByJenjangPendidikanWali !== null && $this->jenjang_pendidikan_wali !== $this->aJenjangPendidikanRelatedByJenjangPendidikanWali->getJenjangPendidikanId()) {
            $this->aJenjangPendidikanRelatedByJenjangPendidikanWali = null;
        }
        if ($this->aPekerjaanRelatedByPekerjaanIdWali !== null && $this->pekerjaan_id_wali !== $this->aPekerjaanRelatedByPekerjaanIdWali->getPekerjaanId()) {
            $this->aPekerjaanRelatedByPekerjaanIdWali = null;
        }
        if ($this->aPekerjaanRelatedByPekerjaanIdWali !== null && $this->pekerjaan_id_wali !== $this->aPekerjaanRelatedByPekerjaanIdWali->getPekerjaanId()) {
            $this->aPekerjaanRelatedByPekerjaanIdWali = null;
        }
        if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali !== null && $this->penghasilan_id_wali !== $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali->getPenghasilanOrangtuaWaliId()) {
            $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali = null;
        }
        if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali !== null && $this->penghasilan_id_wali !== $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali->getPenghasilanOrangtuaWaliId()) {
            $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PesertaDidikPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = PesertaDidikPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aSekolahRelatedBySekolahId = null;
            $this->aSekolahRelatedBySekolahId = null;
            $this->aAgamaRelatedByAgamaId = null;
            $this->aAgamaRelatedByAgamaId = null;
            $this->aAlatTransportasiRelatedByAlatTransportasiId = null;
            $this->aAlatTransportasiRelatedByAlatTransportasiId = null;
            $this->aJenisTinggalRelatedByJenisTinggalId = null;
            $this->aJenisTinggalRelatedByJenisTinggalId = null;
            $this->aJenjangPendidikanRelatedByJenjangPendidikanIbu = null;
            $this->aJenjangPendidikanRelatedByJenjangPendidikanAyah = null;
            $this->aJenjangPendidikanRelatedByJenjangPendidikanWali = null;
            $this->aJenjangPendidikanRelatedByJenjangPendidikanIbu = null;
            $this->aJenjangPendidikanRelatedByJenjangPendidikanAyah = null;
            $this->aJenjangPendidikanRelatedByJenjangPendidikanWali = null;
            $this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah = null;
            $this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu = null;
            $this->aKebutuhanKhususRelatedByKebutuhanKhususId = null;
            $this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah = null;
            $this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu = null;
            $this->aKebutuhanKhususRelatedByKebutuhanKhususId = null;
            $this->aMstWilayahRelatedByKodeWilayah = null;
            $this->aMstWilayahRelatedByKodeWilayah = null;
            $this->aNegaraRelatedByKewarganegaraan = null;
            $this->aNegaraRelatedByKewarganegaraan = null;
            $this->aPekerjaanRelatedByPekerjaanIdAyah = null;
            $this->aPekerjaanRelatedByPekerjaanIdIbu = null;
            $this->aPekerjaanRelatedByPekerjaanIdWali = null;
            $this->aPekerjaanRelatedByPekerjaanIdAyah = null;
            $this->aPekerjaanRelatedByPekerjaanIdIbu = null;
            $this->aPekerjaanRelatedByPekerjaanIdWali = null;
            $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah = null;
            $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali = null;
            $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu = null;
            $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah = null;
            $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali = null;
            $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu = null;
            $this->collPrestasisRelatedByPesertaDidikId = null;

            $this->collPrestasisRelatedByPesertaDidikId = null;

            $this->collAnggotaRombelsRelatedByPesertaDidikId = null;

            $this->collAnggotaRombelsRelatedByPesertaDidikId = null;

            $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId = null;

            $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId = null;

            $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId = null;

            $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId = null;

            $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId = null;

            $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId = null;

            $this->collVldPesertaDidiksRelatedByPesertaDidikId = null;

            $this->collVldPesertaDidiksRelatedByPesertaDidikId = null;

            $this->collPesertaDidikBarusRelatedByPesertaDidikId = null;

            $this->collPesertaDidikBarusRelatedByPesertaDidikId = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PesertaDidikPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = PesertaDidikQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PesertaDidikPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                PesertaDidikPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if ($this->aSekolahRelatedBySekolahId->isModified() || $this->aSekolahRelatedBySekolahId->isNew()) {
                    $affectedRows += $this->aSekolahRelatedBySekolahId->save($con);
                }
                $this->setSekolahRelatedBySekolahId($this->aSekolahRelatedBySekolahId);
            }

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if ($this->aSekolahRelatedBySekolahId->isModified() || $this->aSekolahRelatedBySekolahId->isNew()) {
                    $affectedRows += $this->aSekolahRelatedBySekolahId->save($con);
                }
                $this->setSekolahRelatedBySekolahId($this->aSekolahRelatedBySekolahId);
            }

            if ($this->aAgamaRelatedByAgamaId !== null) {
                if ($this->aAgamaRelatedByAgamaId->isModified() || $this->aAgamaRelatedByAgamaId->isNew()) {
                    $affectedRows += $this->aAgamaRelatedByAgamaId->save($con);
                }
                $this->setAgamaRelatedByAgamaId($this->aAgamaRelatedByAgamaId);
            }

            if ($this->aAgamaRelatedByAgamaId !== null) {
                if ($this->aAgamaRelatedByAgamaId->isModified() || $this->aAgamaRelatedByAgamaId->isNew()) {
                    $affectedRows += $this->aAgamaRelatedByAgamaId->save($con);
                }
                $this->setAgamaRelatedByAgamaId($this->aAgamaRelatedByAgamaId);
            }

            if ($this->aAlatTransportasiRelatedByAlatTransportasiId !== null) {
                if ($this->aAlatTransportasiRelatedByAlatTransportasiId->isModified() || $this->aAlatTransportasiRelatedByAlatTransportasiId->isNew()) {
                    $affectedRows += $this->aAlatTransportasiRelatedByAlatTransportasiId->save($con);
                }
                $this->setAlatTransportasiRelatedByAlatTransportasiId($this->aAlatTransportasiRelatedByAlatTransportasiId);
            }

            if ($this->aAlatTransportasiRelatedByAlatTransportasiId !== null) {
                if ($this->aAlatTransportasiRelatedByAlatTransportasiId->isModified() || $this->aAlatTransportasiRelatedByAlatTransportasiId->isNew()) {
                    $affectedRows += $this->aAlatTransportasiRelatedByAlatTransportasiId->save($con);
                }
                $this->setAlatTransportasiRelatedByAlatTransportasiId($this->aAlatTransportasiRelatedByAlatTransportasiId);
            }

            if ($this->aJenisTinggalRelatedByJenisTinggalId !== null) {
                if ($this->aJenisTinggalRelatedByJenisTinggalId->isModified() || $this->aJenisTinggalRelatedByJenisTinggalId->isNew()) {
                    $affectedRows += $this->aJenisTinggalRelatedByJenisTinggalId->save($con);
                }
                $this->setJenisTinggalRelatedByJenisTinggalId($this->aJenisTinggalRelatedByJenisTinggalId);
            }

            if ($this->aJenisTinggalRelatedByJenisTinggalId !== null) {
                if ($this->aJenisTinggalRelatedByJenisTinggalId->isModified() || $this->aJenisTinggalRelatedByJenisTinggalId->isNew()) {
                    $affectedRows += $this->aJenisTinggalRelatedByJenisTinggalId->save($con);
                }
                $this->setJenisTinggalRelatedByJenisTinggalId($this->aJenisTinggalRelatedByJenisTinggalId);
            }

            if ($this->aJenjangPendidikanRelatedByJenjangPendidikanIbu !== null) {
                if ($this->aJenjangPendidikanRelatedByJenjangPendidikanIbu->isModified() || $this->aJenjangPendidikanRelatedByJenjangPendidikanIbu->isNew()) {
                    $affectedRows += $this->aJenjangPendidikanRelatedByJenjangPendidikanIbu->save($con);
                }
                $this->setJenjangPendidikanRelatedByJenjangPendidikanIbu($this->aJenjangPendidikanRelatedByJenjangPendidikanIbu);
            }

            if ($this->aJenjangPendidikanRelatedByJenjangPendidikanAyah !== null) {
                if ($this->aJenjangPendidikanRelatedByJenjangPendidikanAyah->isModified() || $this->aJenjangPendidikanRelatedByJenjangPendidikanAyah->isNew()) {
                    $affectedRows += $this->aJenjangPendidikanRelatedByJenjangPendidikanAyah->save($con);
                }
                $this->setJenjangPendidikanRelatedByJenjangPendidikanAyah($this->aJenjangPendidikanRelatedByJenjangPendidikanAyah);
            }

            if ($this->aJenjangPendidikanRelatedByJenjangPendidikanWali !== null) {
                if ($this->aJenjangPendidikanRelatedByJenjangPendidikanWali->isModified() || $this->aJenjangPendidikanRelatedByJenjangPendidikanWali->isNew()) {
                    $affectedRows += $this->aJenjangPendidikanRelatedByJenjangPendidikanWali->save($con);
                }
                $this->setJenjangPendidikanRelatedByJenjangPendidikanWali($this->aJenjangPendidikanRelatedByJenjangPendidikanWali);
            }

            if ($this->aJenjangPendidikanRelatedByJenjangPendidikanIbu !== null) {
                if ($this->aJenjangPendidikanRelatedByJenjangPendidikanIbu->isModified() || $this->aJenjangPendidikanRelatedByJenjangPendidikanIbu->isNew()) {
                    $affectedRows += $this->aJenjangPendidikanRelatedByJenjangPendidikanIbu->save($con);
                }
                $this->setJenjangPendidikanRelatedByJenjangPendidikanIbu($this->aJenjangPendidikanRelatedByJenjangPendidikanIbu);
            }

            if ($this->aJenjangPendidikanRelatedByJenjangPendidikanAyah !== null) {
                if ($this->aJenjangPendidikanRelatedByJenjangPendidikanAyah->isModified() || $this->aJenjangPendidikanRelatedByJenjangPendidikanAyah->isNew()) {
                    $affectedRows += $this->aJenjangPendidikanRelatedByJenjangPendidikanAyah->save($con);
                }
                $this->setJenjangPendidikanRelatedByJenjangPendidikanAyah($this->aJenjangPendidikanRelatedByJenjangPendidikanAyah);
            }

            if ($this->aJenjangPendidikanRelatedByJenjangPendidikanWali !== null) {
                if ($this->aJenjangPendidikanRelatedByJenjangPendidikanWali->isModified() || $this->aJenjangPendidikanRelatedByJenjangPendidikanWali->isNew()) {
                    $affectedRows += $this->aJenjangPendidikanRelatedByJenjangPendidikanWali->save($con);
                }
                $this->setJenjangPendidikanRelatedByJenjangPendidikanWali($this->aJenjangPendidikanRelatedByJenjangPendidikanWali);
            }

            if ($this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah !== null) {
                if ($this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah->isModified() || $this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah->isNew()) {
                    $affectedRows += $this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah->save($con);
                }
                $this->setKebutuhanKhususRelatedByKebutuhanKhususIdAyah($this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah);
            }

            if ($this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu !== null) {
                if ($this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu->isModified() || $this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu->isNew()) {
                    $affectedRows += $this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu->save($con);
                }
                $this->setKebutuhanKhususRelatedByKebutuhanKhususIdIbu($this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu);
            }

            if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId !== null) {
                if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId->isModified() || $this->aKebutuhanKhususRelatedByKebutuhanKhususId->isNew()) {
                    $affectedRows += $this->aKebutuhanKhususRelatedByKebutuhanKhususId->save($con);
                }
                $this->setKebutuhanKhususRelatedByKebutuhanKhususId($this->aKebutuhanKhususRelatedByKebutuhanKhususId);
            }

            if ($this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah !== null) {
                if ($this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah->isModified() || $this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah->isNew()) {
                    $affectedRows += $this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah->save($con);
                }
                $this->setKebutuhanKhususRelatedByKebutuhanKhususIdAyah($this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah);
            }

            if ($this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu !== null) {
                if ($this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu->isModified() || $this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu->isNew()) {
                    $affectedRows += $this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu->save($con);
                }
                $this->setKebutuhanKhususRelatedByKebutuhanKhususIdIbu($this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu);
            }

            if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId !== null) {
                if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId->isModified() || $this->aKebutuhanKhususRelatedByKebutuhanKhususId->isNew()) {
                    $affectedRows += $this->aKebutuhanKhususRelatedByKebutuhanKhususId->save($con);
                }
                $this->setKebutuhanKhususRelatedByKebutuhanKhususId($this->aKebutuhanKhususRelatedByKebutuhanKhususId);
            }

            if ($this->aMstWilayahRelatedByKodeWilayah !== null) {
                if ($this->aMstWilayahRelatedByKodeWilayah->isModified() || $this->aMstWilayahRelatedByKodeWilayah->isNew()) {
                    $affectedRows += $this->aMstWilayahRelatedByKodeWilayah->save($con);
                }
                $this->setMstWilayahRelatedByKodeWilayah($this->aMstWilayahRelatedByKodeWilayah);
            }

            if ($this->aMstWilayahRelatedByKodeWilayah !== null) {
                if ($this->aMstWilayahRelatedByKodeWilayah->isModified() || $this->aMstWilayahRelatedByKodeWilayah->isNew()) {
                    $affectedRows += $this->aMstWilayahRelatedByKodeWilayah->save($con);
                }
                $this->setMstWilayahRelatedByKodeWilayah($this->aMstWilayahRelatedByKodeWilayah);
            }

            if ($this->aNegaraRelatedByKewarganegaraan !== null) {
                if ($this->aNegaraRelatedByKewarganegaraan->isModified() || $this->aNegaraRelatedByKewarganegaraan->isNew()) {
                    $affectedRows += $this->aNegaraRelatedByKewarganegaraan->save($con);
                }
                $this->setNegaraRelatedByKewarganegaraan($this->aNegaraRelatedByKewarganegaraan);
            }

            if ($this->aNegaraRelatedByKewarganegaraan !== null) {
                if ($this->aNegaraRelatedByKewarganegaraan->isModified() || $this->aNegaraRelatedByKewarganegaraan->isNew()) {
                    $affectedRows += $this->aNegaraRelatedByKewarganegaraan->save($con);
                }
                $this->setNegaraRelatedByKewarganegaraan($this->aNegaraRelatedByKewarganegaraan);
            }

            if ($this->aPekerjaanRelatedByPekerjaanIdAyah !== null) {
                if ($this->aPekerjaanRelatedByPekerjaanIdAyah->isModified() || $this->aPekerjaanRelatedByPekerjaanIdAyah->isNew()) {
                    $affectedRows += $this->aPekerjaanRelatedByPekerjaanIdAyah->save($con);
                }
                $this->setPekerjaanRelatedByPekerjaanIdAyah($this->aPekerjaanRelatedByPekerjaanIdAyah);
            }

            if ($this->aPekerjaanRelatedByPekerjaanIdIbu !== null) {
                if ($this->aPekerjaanRelatedByPekerjaanIdIbu->isModified() || $this->aPekerjaanRelatedByPekerjaanIdIbu->isNew()) {
                    $affectedRows += $this->aPekerjaanRelatedByPekerjaanIdIbu->save($con);
                }
                $this->setPekerjaanRelatedByPekerjaanIdIbu($this->aPekerjaanRelatedByPekerjaanIdIbu);
            }

            if ($this->aPekerjaanRelatedByPekerjaanIdWali !== null) {
                if ($this->aPekerjaanRelatedByPekerjaanIdWali->isModified() || $this->aPekerjaanRelatedByPekerjaanIdWali->isNew()) {
                    $affectedRows += $this->aPekerjaanRelatedByPekerjaanIdWali->save($con);
                }
                $this->setPekerjaanRelatedByPekerjaanIdWali($this->aPekerjaanRelatedByPekerjaanIdWali);
            }

            if ($this->aPekerjaanRelatedByPekerjaanIdAyah !== null) {
                if ($this->aPekerjaanRelatedByPekerjaanIdAyah->isModified() || $this->aPekerjaanRelatedByPekerjaanIdAyah->isNew()) {
                    $affectedRows += $this->aPekerjaanRelatedByPekerjaanIdAyah->save($con);
                }
                $this->setPekerjaanRelatedByPekerjaanIdAyah($this->aPekerjaanRelatedByPekerjaanIdAyah);
            }

            if ($this->aPekerjaanRelatedByPekerjaanIdIbu !== null) {
                if ($this->aPekerjaanRelatedByPekerjaanIdIbu->isModified() || $this->aPekerjaanRelatedByPekerjaanIdIbu->isNew()) {
                    $affectedRows += $this->aPekerjaanRelatedByPekerjaanIdIbu->save($con);
                }
                $this->setPekerjaanRelatedByPekerjaanIdIbu($this->aPekerjaanRelatedByPekerjaanIdIbu);
            }

            if ($this->aPekerjaanRelatedByPekerjaanIdWali !== null) {
                if ($this->aPekerjaanRelatedByPekerjaanIdWali->isModified() || $this->aPekerjaanRelatedByPekerjaanIdWali->isNew()) {
                    $affectedRows += $this->aPekerjaanRelatedByPekerjaanIdWali->save($con);
                }
                $this->setPekerjaanRelatedByPekerjaanIdWali($this->aPekerjaanRelatedByPekerjaanIdWali);
            }

            if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah !== null) {
                if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah->isModified() || $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah->isNew()) {
                    $affectedRows += $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah->save($con);
                }
                $this->setPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah);
            }

            if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali !== null) {
                if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali->isModified() || $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali->isNew()) {
                    $affectedRows += $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali->save($con);
                }
                $this->setPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali);
            }

            if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu !== null) {
                if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu->isModified() || $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu->isNew()) {
                    $affectedRows += $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu->save($con);
                }
                $this->setPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu);
            }

            if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah !== null) {
                if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah->isModified() || $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah->isNew()) {
                    $affectedRows += $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah->save($con);
                }
                $this->setPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah);
            }

            if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali !== null) {
                if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali->isModified() || $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali->isNew()) {
                    $affectedRows += $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali->save($con);
                }
                $this->setPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali);
            }

            if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu !== null) {
                if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu->isModified() || $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu->isNew()) {
                    $affectedRows += $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu->save($con);
                }
                $this->setPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->prestasisRelatedByPesertaDidikIdScheduledForDeletion !== null) {
                if (!$this->prestasisRelatedByPesertaDidikIdScheduledForDeletion->isEmpty()) {
                    PrestasiQuery::create()
                        ->filterByPrimaryKeys($this->prestasisRelatedByPesertaDidikIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->prestasisRelatedByPesertaDidikIdScheduledForDeletion = null;
                }
            }

            if ($this->collPrestasisRelatedByPesertaDidikId !== null) {
                foreach ($this->collPrestasisRelatedByPesertaDidikId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->prestasisRelatedByPesertaDidikIdScheduledForDeletion !== null) {
                if (!$this->prestasisRelatedByPesertaDidikIdScheduledForDeletion->isEmpty()) {
                    PrestasiQuery::create()
                        ->filterByPrimaryKeys($this->prestasisRelatedByPesertaDidikIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->prestasisRelatedByPesertaDidikIdScheduledForDeletion = null;
                }
            }

            if ($this->collPrestasisRelatedByPesertaDidikId !== null) {
                foreach ($this->collPrestasisRelatedByPesertaDidikId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->anggotaRombelsRelatedByPesertaDidikIdScheduledForDeletion !== null) {
                if (!$this->anggotaRombelsRelatedByPesertaDidikIdScheduledForDeletion->isEmpty()) {
                    AnggotaRombelQuery::create()
                        ->filterByPrimaryKeys($this->anggotaRombelsRelatedByPesertaDidikIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->anggotaRombelsRelatedByPesertaDidikIdScheduledForDeletion = null;
                }
            }

            if ($this->collAnggotaRombelsRelatedByPesertaDidikId !== null) {
                foreach ($this->collAnggotaRombelsRelatedByPesertaDidikId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->anggotaRombelsRelatedByPesertaDidikIdScheduledForDeletion !== null) {
                if (!$this->anggotaRombelsRelatedByPesertaDidikIdScheduledForDeletion->isEmpty()) {
                    AnggotaRombelQuery::create()
                        ->filterByPrimaryKeys($this->anggotaRombelsRelatedByPesertaDidikIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->anggotaRombelsRelatedByPesertaDidikIdScheduledForDeletion = null;
                }
            }

            if ($this->collAnggotaRombelsRelatedByPesertaDidikId !== null) {
                foreach ($this->collAnggotaRombelsRelatedByPesertaDidikId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->pesertaDidikLongitudinalsRelatedByPesertaDidikIdScheduledForDeletion !== null) {
                if (!$this->pesertaDidikLongitudinalsRelatedByPesertaDidikIdScheduledForDeletion->isEmpty()) {
                    PesertaDidikLongitudinalQuery::create()
                        ->filterByPrimaryKeys($this->pesertaDidikLongitudinalsRelatedByPesertaDidikIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->pesertaDidikLongitudinalsRelatedByPesertaDidikIdScheduledForDeletion = null;
                }
            }

            if ($this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId !== null) {
                foreach ($this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->pesertaDidikLongitudinalsRelatedByPesertaDidikIdScheduledForDeletion !== null) {
                if (!$this->pesertaDidikLongitudinalsRelatedByPesertaDidikIdScheduledForDeletion->isEmpty()) {
                    PesertaDidikLongitudinalQuery::create()
                        ->filterByPrimaryKeys($this->pesertaDidikLongitudinalsRelatedByPesertaDidikIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->pesertaDidikLongitudinalsRelatedByPesertaDidikIdScheduledForDeletion = null;
                }
            }

            if ($this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId !== null) {
                foreach ($this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->beasiswaPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion !== null) {
                if (!$this->beasiswaPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion->isEmpty()) {
                    BeasiswaPesertaDidikQuery::create()
                        ->filterByPrimaryKeys($this->beasiswaPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->beasiswaPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion = null;
                }
            }

            if ($this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId !== null) {
                foreach ($this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->beasiswaPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion !== null) {
                if (!$this->beasiswaPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion->isEmpty()) {
                    BeasiswaPesertaDidikQuery::create()
                        ->filterByPrimaryKeys($this->beasiswaPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->beasiswaPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion = null;
                }
            }

            if ($this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId !== null) {
                foreach ($this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->registrasiPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion !== null) {
                if (!$this->registrasiPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion->isEmpty()) {
                    RegistrasiPesertaDidikQuery::create()
                        ->filterByPrimaryKeys($this->registrasiPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->registrasiPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion = null;
                }
            }

            if ($this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId !== null) {
                foreach ($this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->registrasiPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion !== null) {
                if (!$this->registrasiPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion->isEmpty()) {
                    RegistrasiPesertaDidikQuery::create()
                        ->filterByPrimaryKeys($this->registrasiPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->registrasiPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion = null;
                }
            }

            if ($this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId !== null) {
                foreach ($this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion !== null) {
                if (!$this->vldPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion->isEmpty()) {
                    VldPesertaDidikQuery::create()
                        ->filterByPrimaryKeys($this->vldPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion = null;
                }
            }

            if ($this->collVldPesertaDidiksRelatedByPesertaDidikId !== null) {
                foreach ($this->collVldPesertaDidiksRelatedByPesertaDidikId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion !== null) {
                if (!$this->vldPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion->isEmpty()) {
                    VldPesertaDidikQuery::create()
                        ->filterByPrimaryKeys($this->vldPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion = null;
                }
            }

            if ($this->collVldPesertaDidiksRelatedByPesertaDidikId !== null) {
                foreach ($this->collVldPesertaDidiksRelatedByPesertaDidikId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->pesertaDidikBarusRelatedByPesertaDidikIdScheduledForDeletion !== null) {
                if (!$this->pesertaDidikBarusRelatedByPesertaDidikIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->pesertaDidikBarusRelatedByPesertaDidikIdScheduledForDeletion as $pesertaDidikBaruRelatedByPesertaDidikId) {
                        // need to save related object because we set the relation to null
                        $pesertaDidikBaruRelatedByPesertaDidikId->save($con);
                    }
                    $this->pesertaDidikBarusRelatedByPesertaDidikIdScheduledForDeletion = null;
                }
            }

            if ($this->collPesertaDidikBarusRelatedByPesertaDidikId !== null) {
                foreach ($this->collPesertaDidikBarusRelatedByPesertaDidikId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->pesertaDidikBarusRelatedByPesertaDidikIdScheduledForDeletion !== null) {
                if (!$this->pesertaDidikBarusRelatedByPesertaDidikIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->pesertaDidikBarusRelatedByPesertaDidikIdScheduledForDeletion as $pesertaDidikBaruRelatedByPesertaDidikId) {
                        // need to save related object because we set the relation to null
                        $pesertaDidikBaruRelatedByPesertaDidikId->save($con);
                    }
                    $this->pesertaDidikBarusRelatedByPesertaDidikIdScheduledForDeletion = null;
                }
            }

            if ($this->collPesertaDidikBarusRelatedByPesertaDidikId !== null) {
                foreach ($this->collPesertaDidikBarusRelatedByPesertaDidikId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $criteria = $this->buildCriteria();
        $pk = BasePeer::doInsert($criteria, $con);
        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if (!$this->aSekolahRelatedBySekolahId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSekolahRelatedBySekolahId->getValidationFailures());
                }
            }

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if (!$this->aSekolahRelatedBySekolahId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSekolahRelatedBySekolahId->getValidationFailures());
                }
            }

            if ($this->aAgamaRelatedByAgamaId !== null) {
                if (!$this->aAgamaRelatedByAgamaId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aAgamaRelatedByAgamaId->getValidationFailures());
                }
            }

            if ($this->aAgamaRelatedByAgamaId !== null) {
                if (!$this->aAgamaRelatedByAgamaId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aAgamaRelatedByAgamaId->getValidationFailures());
                }
            }

            if ($this->aAlatTransportasiRelatedByAlatTransportasiId !== null) {
                if (!$this->aAlatTransportasiRelatedByAlatTransportasiId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aAlatTransportasiRelatedByAlatTransportasiId->getValidationFailures());
                }
            }

            if ($this->aAlatTransportasiRelatedByAlatTransportasiId !== null) {
                if (!$this->aAlatTransportasiRelatedByAlatTransportasiId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aAlatTransportasiRelatedByAlatTransportasiId->getValidationFailures());
                }
            }

            if ($this->aJenisTinggalRelatedByJenisTinggalId !== null) {
                if (!$this->aJenisTinggalRelatedByJenisTinggalId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aJenisTinggalRelatedByJenisTinggalId->getValidationFailures());
                }
            }

            if ($this->aJenisTinggalRelatedByJenisTinggalId !== null) {
                if (!$this->aJenisTinggalRelatedByJenisTinggalId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aJenisTinggalRelatedByJenisTinggalId->getValidationFailures());
                }
            }

            if ($this->aJenjangPendidikanRelatedByJenjangPendidikanIbu !== null) {
                if (!$this->aJenjangPendidikanRelatedByJenjangPendidikanIbu->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aJenjangPendidikanRelatedByJenjangPendidikanIbu->getValidationFailures());
                }
            }

            if ($this->aJenjangPendidikanRelatedByJenjangPendidikanAyah !== null) {
                if (!$this->aJenjangPendidikanRelatedByJenjangPendidikanAyah->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aJenjangPendidikanRelatedByJenjangPendidikanAyah->getValidationFailures());
                }
            }

            if ($this->aJenjangPendidikanRelatedByJenjangPendidikanWali !== null) {
                if (!$this->aJenjangPendidikanRelatedByJenjangPendidikanWali->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aJenjangPendidikanRelatedByJenjangPendidikanWali->getValidationFailures());
                }
            }

            if ($this->aJenjangPendidikanRelatedByJenjangPendidikanIbu !== null) {
                if (!$this->aJenjangPendidikanRelatedByJenjangPendidikanIbu->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aJenjangPendidikanRelatedByJenjangPendidikanIbu->getValidationFailures());
                }
            }

            if ($this->aJenjangPendidikanRelatedByJenjangPendidikanAyah !== null) {
                if (!$this->aJenjangPendidikanRelatedByJenjangPendidikanAyah->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aJenjangPendidikanRelatedByJenjangPendidikanAyah->getValidationFailures());
                }
            }

            if ($this->aJenjangPendidikanRelatedByJenjangPendidikanWali !== null) {
                if (!$this->aJenjangPendidikanRelatedByJenjangPendidikanWali->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aJenjangPendidikanRelatedByJenjangPendidikanWali->getValidationFailures());
                }
            }

            if ($this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah !== null) {
                if (!$this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah->getValidationFailures());
                }
            }

            if ($this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu !== null) {
                if (!$this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu->getValidationFailures());
                }
            }

            if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId !== null) {
                if (!$this->aKebutuhanKhususRelatedByKebutuhanKhususId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aKebutuhanKhususRelatedByKebutuhanKhususId->getValidationFailures());
                }
            }

            if ($this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah !== null) {
                if (!$this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah->getValidationFailures());
                }
            }

            if ($this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu !== null) {
                if (!$this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu->getValidationFailures());
                }
            }

            if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId !== null) {
                if (!$this->aKebutuhanKhususRelatedByKebutuhanKhususId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aKebutuhanKhususRelatedByKebutuhanKhususId->getValidationFailures());
                }
            }

            if ($this->aMstWilayahRelatedByKodeWilayah !== null) {
                if (!$this->aMstWilayahRelatedByKodeWilayah->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aMstWilayahRelatedByKodeWilayah->getValidationFailures());
                }
            }

            if ($this->aMstWilayahRelatedByKodeWilayah !== null) {
                if (!$this->aMstWilayahRelatedByKodeWilayah->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aMstWilayahRelatedByKodeWilayah->getValidationFailures());
                }
            }

            if ($this->aNegaraRelatedByKewarganegaraan !== null) {
                if (!$this->aNegaraRelatedByKewarganegaraan->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aNegaraRelatedByKewarganegaraan->getValidationFailures());
                }
            }

            if ($this->aNegaraRelatedByKewarganegaraan !== null) {
                if (!$this->aNegaraRelatedByKewarganegaraan->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aNegaraRelatedByKewarganegaraan->getValidationFailures());
                }
            }

            if ($this->aPekerjaanRelatedByPekerjaanIdAyah !== null) {
                if (!$this->aPekerjaanRelatedByPekerjaanIdAyah->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPekerjaanRelatedByPekerjaanIdAyah->getValidationFailures());
                }
            }

            if ($this->aPekerjaanRelatedByPekerjaanIdIbu !== null) {
                if (!$this->aPekerjaanRelatedByPekerjaanIdIbu->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPekerjaanRelatedByPekerjaanIdIbu->getValidationFailures());
                }
            }

            if ($this->aPekerjaanRelatedByPekerjaanIdWali !== null) {
                if (!$this->aPekerjaanRelatedByPekerjaanIdWali->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPekerjaanRelatedByPekerjaanIdWali->getValidationFailures());
                }
            }

            if ($this->aPekerjaanRelatedByPekerjaanIdAyah !== null) {
                if (!$this->aPekerjaanRelatedByPekerjaanIdAyah->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPekerjaanRelatedByPekerjaanIdAyah->getValidationFailures());
                }
            }

            if ($this->aPekerjaanRelatedByPekerjaanIdIbu !== null) {
                if (!$this->aPekerjaanRelatedByPekerjaanIdIbu->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPekerjaanRelatedByPekerjaanIdIbu->getValidationFailures());
                }
            }

            if ($this->aPekerjaanRelatedByPekerjaanIdWali !== null) {
                if (!$this->aPekerjaanRelatedByPekerjaanIdWali->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPekerjaanRelatedByPekerjaanIdWali->getValidationFailures());
                }
            }

            if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah !== null) {
                if (!$this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah->getValidationFailures());
                }
            }

            if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali !== null) {
                if (!$this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali->getValidationFailures());
                }
            }

            if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu !== null) {
                if (!$this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu->getValidationFailures());
                }
            }

            if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah !== null) {
                if (!$this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah->getValidationFailures());
                }
            }

            if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali !== null) {
                if (!$this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali->getValidationFailures());
                }
            }

            if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu !== null) {
                if (!$this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu->getValidationFailures());
                }
            }


            if (($retval = PesertaDidikPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collPrestasisRelatedByPesertaDidikId !== null) {
                    foreach ($this->collPrestasisRelatedByPesertaDidikId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPrestasisRelatedByPesertaDidikId !== null) {
                    foreach ($this->collPrestasisRelatedByPesertaDidikId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collAnggotaRombelsRelatedByPesertaDidikId !== null) {
                    foreach ($this->collAnggotaRombelsRelatedByPesertaDidikId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collAnggotaRombelsRelatedByPesertaDidikId !== null) {
                    foreach ($this->collAnggotaRombelsRelatedByPesertaDidikId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId !== null) {
                    foreach ($this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId !== null) {
                    foreach ($this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId !== null) {
                    foreach ($this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId !== null) {
                    foreach ($this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId !== null) {
                    foreach ($this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId !== null) {
                    foreach ($this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldPesertaDidiksRelatedByPesertaDidikId !== null) {
                    foreach ($this->collVldPesertaDidiksRelatedByPesertaDidikId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldPesertaDidiksRelatedByPesertaDidikId !== null) {
                    foreach ($this->collVldPesertaDidiksRelatedByPesertaDidikId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPesertaDidikBarusRelatedByPesertaDidikId !== null) {
                    foreach ($this->collPesertaDidikBarusRelatedByPesertaDidikId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPesertaDidikBarusRelatedByPesertaDidikId !== null) {
                    foreach ($this->collPesertaDidikBarusRelatedByPesertaDidikId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = PesertaDidikPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getPesertaDidikId();
                break;
            case 1:
                return $this->getNama();
                break;
            case 2:
                return $this->getJenisKelamin();
                break;
            case 3:
                return $this->getNisn();
                break;
            case 4:
                return $this->getNik();
                break;
            case 5:
                return $this->getTempatLahir();
                break;
            case 6:
                return $this->getTanggalLahir();
                break;
            case 7:
                return $this->getAgamaId();
                break;
            case 8:
                return $this->getKewarganegaraan();
                break;
            case 9:
                return $this->getKebutuhanKhususId();
                break;
            case 10:
                return $this->getSekolahId();
                break;
            case 11:
                return $this->getAlamatJalan();
                break;
            case 12:
                return $this->getRt();
                break;
            case 13:
                return $this->getRw();
                break;
            case 14:
                return $this->getNamaDusun();
                break;
            case 15:
                return $this->getDesaKelurahan();
                break;
            case 16:
                return $this->getKodeWilayah();
                break;
            case 17:
                return $this->getKodePos();
                break;
            case 18:
                return $this->getJenisTinggalId();
                break;
            case 19:
                return $this->getAlatTransportasiId();
                break;
            case 20:
                return $this->getNomorTeleponRumah();
                break;
            case 21:
                return $this->getNomorTeleponSeluler();
                break;
            case 22:
                return $this->getEmail();
                break;
            case 23:
                return $this->getPenerimaKps();
                break;
            case 24:
                return $this->getNoKps();
                break;
            case 25:
                return $this->getStatusData();
                break;
            case 26:
                return $this->getNamaAyah();
                break;
            case 27:
                return $this->getTahunLahirAyah();
                break;
            case 28:
                return $this->getJenjangPendidikanAyah();
                break;
            case 29:
                return $this->getPekerjaanIdAyah();
                break;
            case 30:
                return $this->getPenghasilanIdAyah();
                break;
            case 31:
                return $this->getKebutuhanKhususIdAyah();
                break;
            case 32:
                return $this->getNamaIbuKandung();
                break;
            case 33:
                return $this->getTahunLahirIbu();
                break;
            case 34:
                return $this->getJenjangPendidikanIbu();
                break;
            case 35:
                return $this->getPenghasilanIdIbu();
                break;
            case 36:
                return $this->getPekerjaanIdIbu();
                break;
            case 37:
                return $this->getKebutuhanKhususIdIbu();
                break;
            case 38:
                return $this->getNamaWali();
                break;
            case 39:
                return $this->getTahunLahirWali();
                break;
            case 40:
                return $this->getJenjangPendidikanWali();
                break;
            case 41:
                return $this->getPekerjaanIdWali();
                break;
            case 42:
                return $this->getPenghasilanIdWali();
                break;
            case 43:
                return $this->getLastUpdate();
                break;
            case 44:
                return $this->getSoftDelete();
                break;
            case 45:
                return $this->getLastSync();
                break;
            case 46:
                return $this->getUpdaterId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['PesertaDidik'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['PesertaDidik'][$this->getPrimaryKey()] = true;
        $keys = PesertaDidikPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getPesertaDidikId(),
            $keys[1] => $this->getNama(),
            $keys[2] => $this->getJenisKelamin(),
            $keys[3] => $this->getNisn(),
            $keys[4] => $this->getNik(),
            $keys[5] => $this->getTempatLahir(),
            $keys[6] => $this->getTanggalLahir(),
            $keys[7] => $this->getAgamaId(),
            $keys[8] => $this->getKewarganegaraan(),
            $keys[9] => $this->getKebutuhanKhususId(),
            $keys[10] => $this->getSekolahId(),
            $keys[11] => $this->getAlamatJalan(),
            $keys[12] => $this->getRt(),
            $keys[13] => $this->getRw(),
            $keys[14] => $this->getNamaDusun(),
            $keys[15] => $this->getDesaKelurahan(),
            $keys[16] => $this->getKodeWilayah(),
            $keys[17] => $this->getKodePos(),
            $keys[18] => $this->getJenisTinggalId(),
            $keys[19] => $this->getAlatTransportasiId(),
            $keys[20] => $this->getNomorTeleponRumah(),
            $keys[21] => $this->getNomorTeleponSeluler(),
            $keys[22] => $this->getEmail(),
            $keys[23] => $this->getPenerimaKps(),
            $keys[24] => $this->getNoKps(),
            $keys[25] => $this->getStatusData(),
            $keys[26] => $this->getNamaAyah(),
            $keys[27] => $this->getTahunLahirAyah(),
            $keys[28] => $this->getJenjangPendidikanAyah(),
            $keys[29] => $this->getPekerjaanIdAyah(),
            $keys[30] => $this->getPenghasilanIdAyah(),
            $keys[31] => $this->getKebutuhanKhususIdAyah(),
            $keys[32] => $this->getNamaIbuKandung(),
            $keys[33] => $this->getTahunLahirIbu(),
            $keys[34] => $this->getJenjangPendidikanIbu(),
            $keys[35] => $this->getPenghasilanIdIbu(),
            $keys[36] => $this->getPekerjaanIdIbu(),
            $keys[37] => $this->getKebutuhanKhususIdIbu(),
            $keys[38] => $this->getNamaWali(),
            $keys[39] => $this->getTahunLahirWali(),
            $keys[40] => $this->getJenjangPendidikanWali(),
            $keys[41] => $this->getPekerjaanIdWali(),
            $keys[42] => $this->getPenghasilanIdWali(),
            $keys[43] => $this->getLastUpdate(),
            $keys[44] => $this->getSoftDelete(),
            $keys[45] => $this->getLastSync(),
            $keys[46] => $this->getUpdaterId(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aSekolahRelatedBySekolahId) {
                $result['SekolahRelatedBySekolahId'] = $this->aSekolahRelatedBySekolahId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSekolahRelatedBySekolahId) {
                $result['SekolahRelatedBySekolahId'] = $this->aSekolahRelatedBySekolahId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aAgamaRelatedByAgamaId) {
                $result['AgamaRelatedByAgamaId'] = $this->aAgamaRelatedByAgamaId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aAgamaRelatedByAgamaId) {
                $result['AgamaRelatedByAgamaId'] = $this->aAgamaRelatedByAgamaId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aAlatTransportasiRelatedByAlatTransportasiId) {
                $result['AlatTransportasiRelatedByAlatTransportasiId'] = $this->aAlatTransportasiRelatedByAlatTransportasiId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aAlatTransportasiRelatedByAlatTransportasiId) {
                $result['AlatTransportasiRelatedByAlatTransportasiId'] = $this->aAlatTransportasiRelatedByAlatTransportasiId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aJenisTinggalRelatedByJenisTinggalId) {
                $result['JenisTinggalRelatedByJenisTinggalId'] = $this->aJenisTinggalRelatedByJenisTinggalId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aJenisTinggalRelatedByJenisTinggalId) {
                $result['JenisTinggalRelatedByJenisTinggalId'] = $this->aJenisTinggalRelatedByJenisTinggalId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aJenjangPendidikanRelatedByJenjangPendidikanIbu) {
                $result['JenjangPendidikanRelatedByJenjangPendidikanIbu'] = $this->aJenjangPendidikanRelatedByJenjangPendidikanIbu->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aJenjangPendidikanRelatedByJenjangPendidikanAyah) {
                $result['JenjangPendidikanRelatedByJenjangPendidikanAyah'] = $this->aJenjangPendidikanRelatedByJenjangPendidikanAyah->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aJenjangPendidikanRelatedByJenjangPendidikanWali) {
                $result['JenjangPendidikanRelatedByJenjangPendidikanWali'] = $this->aJenjangPendidikanRelatedByJenjangPendidikanWali->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aJenjangPendidikanRelatedByJenjangPendidikanIbu) {
                $result['JenjangPendidikanRelatedByJenjangPendidikanIbu'] = $this->aJenjangPendidikanRelatedByJenjangPendidikanIbu->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aJenjangPendidikanRelatedByJenjangPendidikanAyah) {
                $result['JenjangPendidikanRelatedByJenjangPendidikanAyah'] = $this->aJenjangPendidikanRelatedByJenjangPendidikanAyah->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aJenjangPendidikanRelatedByJenjangPendidikanWali) {
                $result['JenjangPendidikanRelatedByJenjangPendidikanWali'] = $this->aJenjangPendidikanRelatedByJenjangPendidikanWali->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah) {
                $result['KebutuhanKhususRelatedByKebutuhanKhususIdAyah'] = $this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu) {
                $result['KebutuhanKhususRelatedByKebutuhanKhususIdIbu'] = $this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aKebutuhanKhususRelatedByKebutuhanKhususId) {
                $result['KebutuhanKhususRelatedByKebutuhanKhususId'] = $this->aKebutuhanKhususRelatedByKebutuhanKhususId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah) {
                $result['KebutuhanKhususRelatedByKebutuhanKhususIdAyah'] = $this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu) {
                $result['KebutuhanKhususRelatedByKebutuhanKhususIdIbu'] = $this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aKebutuhanKhususRelatedByKebutuhanKhususId) {
                $result['KebutuhanKhususRelatedByKebutuhanKhususId'] = $this->aKebutuhanKhususRelatedByKebutuhanKhususId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aMstWilayahRelatedByKodeWilayah) {
                $result['MstWilayahRelatedByKodeWilayah'] = $this->aMstWilayahRelatedByKodeWilayah->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aMstWilayahRelatedByKodeWilayah) {
                $result['MstWilayahRelatedByKodeWilayah'] = $this->aMstWilayahRelatedByKodeWilayah->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aNegaraRelatedByKewarganegaraan) {
                $result['NegaraRelatedByKewarganegaraan'] = $this->aNegaraRelatedByKewarganegaraan->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aNegaraRelatedByKewarganegaraan) {
                $result['NegaraRelatedByKewarganegaraan'] = $this->aNegaraRelatedByKewarganegaraan->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPekerjaanRelatedByPekerjaanIdAyah) {
                $result['PekerjaanRelatedByPekerjaanIdAyah'] = $this->aPekerjaanRelatedByPekerjaanIdAyah->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPekerjaanRelatedByPekerjaanIdIbu) {
                $result['PekerjaanRelatedByPekerjaanIdIbu'] = $this->aPekerjaanRelatedByPekerjaanIdIbu->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPekerjaanRelatedByPekerjaanIdWali) {
                $result['PekerjaanRelatedByPekerjaanIdWali'] = $this->aPekerjaanRelatedByPekerjaanIdWali->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPekerjaanRelatedByPekerjaanIdAyah) {
                $result['PekerjaanRelatedByPekerjaanIdAyah'] = $this->aPekerjaanRelatedByPekerjaanIdAyah->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPekerjaanRelatedByPekerjaanIdIbu) {
                $result['PekerjaanRelatedByPekerjaanIdIbu'] = $this->aPekerjaanRelatedByPekerjaanIdIbu->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPekerjaanRelatedByPekerjaanIdWali) {
                $result['PekerjaanRelatedByPekerjaanIdWali'] = $this->aPekerjaanRelatedByPekerjaanIdWali->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah) {
                $result['PenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah'] = $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali) {
                $result['PenghasilanOrangtuaWaliRelatedByPenghasilanIdWali'] = $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu) {
                $result['PenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu'] = $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah) {
                $result['PenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah'] = $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali) {
                $result['PenghasilanOrangtuaWaliRelatedByPenghasilanIdWali'] = $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu) {
                $result['PenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu'] = $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collPrestasisRelatedByPesertaDidikId) {
                $result['PrestasisRelatedByPesertaDidikId'] = $this->collPrestasisRelatedByPesertaDidikId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPrestasisRelatedByPesertaDidikId) {
                $result['PrestasisRelatedByPesertaDidikId'] = $this->collPrestasisRelatedByPesertaDidikId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collAnggotaRombelsRelatedByPesertaDidikId) {
                $result['AnggotaRombelsRelatedByPesertaDidikId'] = $this->collAnggotaRombelsRelatedByPesertaDidikId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collAnggotaRombelsRelatedByPesertaDidikId) {
                $result['AnggotaRombelsRelatedByPesertaDidikId'] = $this->collAnggotaRombelsRelatedByPesertaDidikId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId) {
                $result['PesertaDidikLongitudinalsRelatedByPesertaDidikId'] = $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId) {
                $result['PesertaDidikLongitudinalsRelatedByPesertaDidikId'] = $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId) {
                $result['BeasiswaPesertaDidiksRelatedByPesertaDidikId'] = $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId) {
                $result['BeasiswaPesertaDidiksRelatedByPesertaDidikId'] = $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId) {
                $result['RegistrasiPesertaDidiksRelatedByPesertaDidikId'] = $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId) {
                $result['RegistrasiPesertaDidiksRelatedByPesertaDidikId'] = $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldPesertaDidiksRelatedByPesertaDidikId) {
                $result['VldPesertaDidiksRelatedByPesertaDidikId'] = $this->collVldPesertaDidiksRelatedByPesertaDidikId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldPesertaDidiksRelatedByPesertaDidikId) {
                $result['VldPesertaDidiksRelatedByPesertaDidikId'] = $this->collVldPesertaDidiksRelatedByPesertaDidikId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPesertaDidikBarusRelatedByPesertaDidikId) {
                $result['PesertaDidikBarusRelatedByPesertaDidikId'] = $this->collPesertaDidikBarusRelatedByPesertaDidikId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPesertaDidikBarusRelatedByPesertaDidikId) {
                $result['PesertaDidikBarusRelatedByPesertaDidikId'] = $this->collPesertaDidikBarusRelatedByPesertaDidikId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = PesertaDidikPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setPesertaDidikId($value);
                break;
            case 1:
                $this->setNama($value);
                break;
            case 2:
                $this->setJenisKelamin($value);
                break;
            case 3:
                $this->setNisn($value);
                break;
            case 4:
                $this->setNik($value);
                break;
            case 5:
                $this->setTempatLahir($value);
                break;
            case 6:
                $this->setTanggalLahir($value);
                break;
            case 7:
                $this->setAgamaId($value);
                break;
            case 8:
                $this->setKewarganegaraan($value);
                break;
            case 9:
                $this->setKebutuhanKhususId($value);
                break;
            case 10:
                $this->setSekolahId($value);
                break;
            case 11:
                $this->setAlamatJalan($value);
                break;
            case 12:
                $this->setRt($value);
                break;
            case 13:
                $this->setRw($value);
                break;
            case 14:
                $this->setNamaDusun($value);
                break;
            case 15:
                $this->setDesaKelurahan($value);
                break;
            case 16:
                $this->setKodeWilayah($value);
                break;
            case 17:
                $this->setKodePos($value);
                break;
            case 18:
                $this->setJenisTinggalId($value);
                break;
            case 19:
                $this->setAlatTransportasiId($value);
                break;
            case 20:
                $this->setNomorTeleponRumah($value);
                break;
            case 21:
                $this->setNomorTeleponSeluler($value);
                break;
            case 22:
                $this->setEmail($value);
                break;
            case 23:
                $this->setPenerimaKps($value);
                break;
            case 24:
                $this->setNoKps($value);
                break;
            case 25:
                $this->setStatusData($value);
                break;
            case 26:
                $this->setNamaAyah($value);
                break;
            case 27:
                $this->setTahunLahirAyah($value);
                break;
            case 28:
                $this->setJenjangPendidikanAyah($value);
                break;
            case 29:
                $this->setPekerjaanIdAyah($value);
                break;
            case 30:
                $this->setPenghasilanIdAyah($value);
                break;
            case 31:
                $this->setKebutuhanKhususIdAyah($value);
                break;
            case 32:
                $this->setNamaIbuKandung($value);
                break;
            case 33:
                $this->setTahunLahirIbu($value);
                break;
            case 34:
                $this->setJenjangPendidikanIbu($value);
                break;
            case 35:
                $this->setPenghasilanIdIbu($value);
                break;
            case 36:
                $this->setPekerjaanIdIbu($value);
                break;
            case 37:
                $this->setKebutuhanKhususIdIbu($value);
                break;
            case 38:
                $this->setNamaWali($value);
                break;
            case 39:
                $this->setTahunLahirWali($value);
                break;
            case 40:
                $this->setJenjangPendidikanWali($value);
                break;
            case 41:
                $this->setPekerjaanIdWali($value);
                break;
            case 42:
                $this->setPenghasilanIdWali($value);
                break;
            case 43:
                $this->setLastUpdate($value);
                break;
            case 44:
                $this->setSoftDelete($value);
                break;
            case 45:
                $this->setLastSync($value);
                break;
            case 46:
                $this->setUpdaterId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = PesertaDidikPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setPesertaDidikId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setNama($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setJenisKelamin($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setNisn($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setNik($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setTempatLahir($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setTanggalLahir($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setAgamaId($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setKewarganegaraan($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setKebutuhanKhususId($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setSekolahId($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setAlamatJalan($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setRt($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setRw($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setNamaDusun($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setDesaKelurahan($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setKodeWilayah($arr[$keys[16]]);
        if (array_key_exists($keys[17], $arr)) $this->setKodePos($arr[$keys[17]]);
        if (array_key_exists($keys[18], $arr)) $this->setJenisTinggalId($arr[$keys[18]]);
        if (array_key_exists($keys[19], $arr)) $this->setAlatTransportasiId($arr[$keys[19]]);
        if (array_key_exists($keys[20], $arr)) $this->setNomorTeleponRumah($arr[$keys[20]]);
        if (array_key_exists($keys[21], $arr)) $this->setNomorTeleponSeluler($arr[$keys[21]]);
        if (array_key_exists($keys[22], $arr)) $this->setEmail($arr[$keys[22]]);
        if (array_key_exists($keys[23], $arr)) $this->setPenerimaKps($arr[$keys[23]]);
        if (array_key_exists($keys[24], $arr)) $this->setNoKps($arr[$keys[24]]);
        if (array_key_exists($keys[25], $arr)) $this->setStatusData($arr[$keys[25]]);
        if (array_key_exists($keys[26], $arr)) $this->setNamaAyah($arr[$keys[26]]);
        if (array_key_exists($keys[27], $arr)) $this->setTahunLahirAyah($arr[$keys[27]]);
        if (array_key_exists($keys[28], $arr)) $this->setJenjangPendidikanAyah($arr[$keys[28]]);
        if (array_key_exists($keys[29], $arr)) $this->setPekerjaanIdAyah($arr[$keys[29]]);
        if (array_key_exists($keys[30], $arr)) $this->setPenghasilanIdAyah($arr[$keys[30]]);
        if (array_key_exists($keys[31], $arr)) $this->setKebutuhanKhususIdAyah($arr[$keys[31]]);
        if (array_key_exists($keys[32], $arr)) $this->setNamaIbuKandung($arr[$keys[32]]);
        if (array_key_exists($keys[33], $arr)) $this->setTahunLahirIbu($arr[$keys[33]]);
        if (array_key_exists($keys[34], $arr)) $this->setJenjangPendidikanIbu($arr[$keys[34]]);
        if (array_key_exists($keys[35], $arr)) $this->setPenghasilanIdIbu($arr[$keys[35]]);
        if (array_key_exists($keys[36], $arr)) $this->setPekerjaanIdIbu($arr[$keys[36]]);
        if (array_key_exists($keys[37], $arr)) $this->setKebutuhanKhususIdIbu($arr[$keys[37]]);
        if (array_key_exists($keys[38], $arr)) $this->setNamaWali($arr[$keys[38]]);
        if (array_key_exists($keys[39], $arr)) $this->setTahunLahirWali($arr[$keys[39]]);
        if (array_key_exists($keys[40], $arr)) $this->setJenjangPendidikanWali($arr[$keys[40]]);
        if (array_key_exists($keys[41], $arr)) $this->setPekerjaanIdWali($arr[$keys[41]]);
        if (array_key_exists($keys[42], $arr)) $this->setPenghasilanIdWali($arr[$keys[42]]);
        if (array_key_exists($keys[43], $arr)) $this->setLastUpdate($arr[$keys[43]]);
        if (array_key_exists($keys[44], $arr)) $this->setSoftDelete($arr[$keys[44]]);
        if (array_key_exists($keys[45], $arr)) $this->setLastSync($arr[$keys[45]]);
        if (array_key_exists($keys[46], $arr)) $this->setUpdaterId($arr[$keys[46]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(PesertaDidikPeer::DATABASE_NAME);

        if ($this->isColumnModified(PesertaDidikPeer::PESERTA_DIDIK_ID)) $criteria->add(PesertaDidikPeer::PESERTA_DIDIK_ID, $this->peserta_didik_id);
        if ($this->isColumnModified(PesertaDidikPeer::NAMA)) $criteria->add(PesertaDidikPeer::NAMA, $this->nama);
        if ($this->isColumnModified(PesertaDidikPeer::JENIS_KELAMIN)) $criteria->add(PesertaDidikPeer::JENIS_KELAMIN, $this->jenis_kelamin);
        if ($this->isColumnModified(PesertaDidikPeer::NISN)) $criteria->add(PesertaDidikPeer::NISN, $this->nisn);
        if ($this->isColumnModified(PesertaDidikPeer::NIK)) $criteria->add(PesertaDidikPeer::NIK, $this->nik);
        if ($this->isColumnModified(PesertaDidikPeer::TEMPAT_LAHIR)) $criteria->add(PesertaDidikPeer::TEMPAT_LAHIR, $this->tempat_lahir);
        if ($this->isColumnModified(PesertaDidikPeer::TANGGAL_LAHIR)) $criteria->add(PesertaDidikPeer::TANGGAL_LAHIR, $this->tanggal_lahir);
        if ($this->isColumnModified(PesertaDidikPeer::AGAMA_ID)) $criteria->add(PesertaDidikPeer::AGAMA_ID, $this->agama_id);
        if ($this->isColumnModified(PesertaDidikPeer::KEWARGANEGARAAN)) $criteria->add(PesertaDidikPeer::KEWARGANEGARAAN, $this->kewarganegaraan);
        if ($this->isColumnModified(PesertaDidikPeer::KEBUTUHAN_KHUSUS_ID)) $criteria->add(PesertaDidikPeer::KEBUTUHAN_KHUSUS_ID, $this->kebutuhan_khusus_id);
        if ($this->isColumnModified(PesertaDidikPeer::SEKOLAH_ID)) $criteria->add(PesertaDidikPeer::SEKOLAH_ID, $this->sekolah_id);
        if ($this->isColumnModified(PesertaDidikPeer::ALAMAT_JALAN)) $criteria->add(PesertaDidikPeer::ALAMAT_JALAN, $this->alamat_jalan);
        if ($this->isColumnModified(PesertaDidikPeer::RT)) $criteria->add(PesertaDidikPeer::RT, $this->rt);
        if ($this->isColumnModified(PesertaDidikPeer::RW)) $criteria->add(PesertaDidikPeer::RW, $this->rw);
        if ($this->isColumnModified(PesertaDidikPeer::NAMA_DUSUN)) $criteria->add(PesertaDidikPeer::NAMA_DUSUN, $this->nama_dusun);
        if ($this->isColumnModified(PesertaDidikPeer::DESA_KELURAHAN)) $criteria->add(PesertaDidikPeer::DESA_KELURAHAN, $this->desa_kelurahan);
        if ($this->isColumnModified(PesertaDidikPeer::KODE_WILAYAH)) $criteria->add(PesertaDidikPeer::KODE_WILAYAH, $this->kode_wilayah);
        if ($this->isColumnModified(PesertaDidikPeer::KODE_POS)) $criteria->add(PesertaDidikPeer::KODE_POS, $this->kode_pos);
        if ($this->isColumnModified(PesertaDidikPeer::JENIS_TINGGAL_ID)) $criteria->add(PesertaDidikPeer::JENIS_TINGGAL_ID, $this->jenis_tinggal_id);
        if ($this->isColumnModified(PesertaDidikPeer::ALAT_TRANSPORTASI_ID)) $criteria->add(PesertaDidikPeer::ALAT_TRANSPORTASI_ID, $this->alat_transportasi_id);
        if ($this->isColumnModified(PesertaDidikPeer::NOMOR_TELEPON_RUMAH)) $criteria->add(PesertaDidikPeer::NOMOR_TELEPON_RUMAH, $this->nomor_telepon_rumah);
        if ($this->isColumnModified(PesertaDidikPeer::NOMOR_TELEPON_SELULER)) $criteria->add(PesertaDidikPeer::NOMOR_TELEPON_SELULER, $this->nomor_telepon_seluler);
        if ($this->isColumnModified(PesertaDidikPeer::EMAIL)) $criteria->add(PesertaDidikPeer::EMAIL, $this->email);
        if ($this->isColumnModified(PesertaDidikPeer::PENERIMA_KPS)) $criteria->add(PesertaDidikPeer::PENERIMA_KPS, $this->penerima_kps);
        if ($this->isColumnModified(PesertaDidikPeer::NO_KPS)) $criteria->add(PesertaDidikPeer::NO_KPS, $this->no_kps);
        if ($this->isColumnModified(PesertaDidikPeer::STATUS_DATA)) $criteria->add(PesertaDidikPeer::STATUS_DATA, $this->status_data);
        if ($this->isColumnModified(PesertaDidikPeer::NAMA_AYAH)) $criteria->add(PesertaDidikPeer::NAMA_AYAH, $this->nama_ayah);
        if ($this->isColumnModified(PesertaDidikPeer::TAHUN_LAHIR_AYAH)) $criteria->add(PesertaDidikPeer::TAHUN_LAHIR_AYAH, $this->tahun_lahir_ayah);
        if ($this->isColumnModified(PesertaDidikPeer::JENJANG_PENDIDIKAN_AYAH)) $criteria->add(PesertaDidikPeer::JENJANG_PENDIDIKAN_AYAH, $this->jenjang_pendidikan_ayah);
        if ($this->isColumnModified(PesertaDidikPeer::PEKERJAAN_ID_AYAH)) $criteria->add(PesertaDidikPeer::PEKERJAAN_ID_AYAH, $this->pekerjaan_id_ayah);
        if ($this->isColumnModified(PesertaDidikPeer::PENGHASILAN_ID_AYAH)) $criteria->add(PesertaDidikPeer::PENGHASILAN_ID_AYAH, $this->penghasilan_id_ayah);
        if ($this->isColumnModified(PesertaDidikPeer::KEBUTUHAN_KHUSUS_ID_AYAH)) $criteria->add(PesertaDidikPeer::KEBUTUHAN_KHUSUS_ID_AYAH, $this->kebutuhan_khusus_id_ayah);
        if ($this->isColumnModified(PesertaDidikPeer::NAMA_IBU_KANDUNG)) $criteria->add(PesertaDidikPeer::NAMA_IBU_KANDUNG, $this->nama_ibu_kandung);
        if ($this->isColumnModified(PesertaDidikPeer::TAHUN_LAHIR_IBU)) $criteria->add(PesertaDidikPeer::TAHUN_LAHIR_IBU, $this->tahun_lahir_ibu);
        if ($this->isColumnModified(PesertaDidikPeer::JENJANG_PENDIDIKAN_IBU)) $criteria->add(PesertaDidikPeer::JENJANG_PENDIDIKAN_IBU, $this->jenjang_pendidikan_ibu);
        if ($this->isColumnModified(PesertaDidikPeer::PENGHASILAN_ID_IBU)) $criteria->add(PesertaDidikPeer::PENGHASILAN_ID_IBU, $this->penghasilan_id_ibu);
        if ($this->isColumnModified(PesertaDidikPeer::PEKERJAAN_ID_IBU)) $criteria->add(PesertaDidikPeer::PEKERJAAN_ID_IBU, $this->pekerjaan_id_ibu);
        if ($this->isColumnModified(PesertaDidikPeer::KEBUTUHAN_KHUSUS_ID_IBU)) $criteria->add(PesertaDidikPeer::KEBUTUHAN_KHUSUS_ID_IBU, $this->kebutuhan_khusus_id_ibu);
        if ($this->isColumnModified(PesertaDidikPeer::NAMA_WALI)) $criteria->add(PesertaDidikPeer::NAMA_WALI, $this->nama_wali);
        if ($this->isColumnModified(PesertaDidikPeer::TAHUN_LAHIR_WALI)) $criteria->add(PesertaDidikPeer::TAHUN_LAHIR_WALI, $this->tahun_lahir_wali);
        if ($this->isColumnModified(PesertaDidikPeer::JENJANG_PENDIDIKAN_WALI)) $criteria->add(PesertaDidikPeer::JENJANG_PENDIDIKAN_WALI, $this->jenjang_pendidikan_wali);
        if ($this->isColumnModified(PesertaDidikPeer::PEKERJAAN_ID_WALI)) $criteria->add(PesertaDidikPeer::PEKERJAAN_ID_WALI, $this->pekerjaan_id_wali);
        if ($this->isColumnModified(PesertaDidikPeer::PENGHASILAN_ID_WALI)) $criteria->add(PesertaDidikPeer::PENGHASILAN_ID_WALI, $this->penghasilan_id_wali);
        if ($this->isColumnModified(PesertaDidikPeer::LAST_UPDATE)) $criteria->add(PesertaDidikPeer::LAST_UPDATE, $this->last_update);
        if ($this->isColumnModified(PesertaDidikPeer::SOFT_DELETE)) $criteria->add(PesertaDidikPeer::SOFT_DELETE, $this->soft_delete);
        if ($this->isColumnModified(PesertaDidikPeer::LAST_SYNC)) $criteria->add(PesertaDidikPeer::LAST_SYNC, $this->last_sync);
        if ($this->isColumnModified(PesertaDidikPeer::UPDATER_ID)) $criteria->add(PesertaDidikPeer::UPDATER_ID, $this->updater_id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(PesertaDidikPeer::DATABASE_NAME);
        $criteria->add(PesertaDidikPeer::PESERTA_DIDIK_ID, $this->peserta_didik_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getPesertaDidikId();
    }

    /**
     * Generic method to set the primary key (peserta_didik_id column).
     *
     * @param  string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setPesertaDidikId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getPesertaDidikId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of PesertaDidik (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setNama($this->getNama());
        $copyObj->setJenisKelamin($this->getJenisKelamin());
        $copyObj->setNisn($this->getNisn());
        $copyObj->setNik($this->getNik());
        $copyObj->setTempatLahir($this->getTempatLahir());
        $copyObj->setTanggalLahir($this->getTanggalLahir());
        $copyObj->setAgamaId($this->getAgamaId());
        $copyObj->setKewarganegaraan($this->getKewarganegaraan());
        $copyObj->setKebutuhanKhususId($this->getKebutuhanKhususId());
        $copyObj->setSekolahId($this->getSekolahId());
        $copyObj->setAlamatJalan($this->getAlamatJalan());
        $copyObj->setRt($this->getRt());
        $copyObj->setRw($this->getRw());
        $copyObj->setNamaDusun($this->getNamaDusun());
        $copyObj->setDesaKelurahan($this->getDesaKelurahan());
        $copyObj->setKodeWilayah($this->getKodeWilayah());
        $copyObj->setKodePos($this->getKodePos());
        $copyObj->setJenisTinggalId($this->getJenisTinggalId());
        $copyObj->setAlatTransportasiId($this->getAlatTransportasiId());
        $copyObj->setNomorTeleponRumah($this->getNomorTeleponRumah());
        $copyObj->setNomorTeleponSeluler($this->getNomorTeleponSeluler());
        $copyObj->setEmail($this->getEmail());
        $copyObj->setPenerimaKps($this->getPenerimaKps());
        $copyObj->setNoKps($this->getNoKps());
        $copyObj->setStatusData($this->getStatusData());
        $copyObj->setNamaAyah($this->getNamaAyah());
        $copyObj->setTahunLahirAyah($this->getTahunLahirAyah());
        $copyObj->setJenjangPendidikanAyah($this->getJenjangPendidikanAyah());
        $copyObj->setPekerjaanIdAyah($this->getPekerjaanIdAyah());
        $copyObj->setPenghasilanIdAyah($this->getPenghasilanIdAyah());
        $copyObj->setKebutuhanKhususIdAyah($this->getKebutuhanKhususIdAyah());
        $copyObj->setNamaIbuKandung($this->getNamaIbuKandung());
        $copyObj->setTahunLahirIbu($this->getTahunLahirIbu());
        $copyObj->setJenjangPendidikanIbu($this->getJenjangPendidikanIbu());
        $copyObj->setPenghasilanIdIbu($this->getPenghasilanIdIbu());
        $copyObj->setPekerjaanIdIbu($this->getPekerjaanIdIbu());
        $copyObj->setKebutuhanKhususIdIbu($this->getKebutuhanKhususIdIbu());
        $copyObj->setNamaWali($this->getNamaWali());
        $copyObj->setTahunLahirWali($this->getTahunLahirWali());
        $copyObj->setJenjangPendidikanWali($this->getJenjangPendidikanWali());
        $copyObj->setPekerjaanIdWali($this->getPekerjaanIdWali());
        $copyObj->setPenghasilanIdWali($this->getPenghasilanIdWali());
        $copyObj->setLastUpdate($this->getLastUpdate());
        $copyObj->setSoftDelete($this->getSoftDelete());
        $copyObj->setLastSync($this->getLastSync());
        $copyObj->setUpdaterId($this->getUpdaterId());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getPrestasisRelatedByPesertaDidikId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPrestasiRelatedByPesertaDidikId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPrestasisRelatedByPesertaDidikId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPrestasiRelatedByPesertaDidikId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getAnggotaRombelsRelatedByPesertaDidikId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addAnggotaRombelRelatedByPesertaDidikId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getAnggotaRombelsRelatedByPesertaDidikId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addAnggotaRombelRelatedByPesertaDidikId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPesertaDidikLongitudinalsRelatedByPesertaDidikId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPesertaDidikLongitudinalRelatedByPesertaDidikId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPesertaDidikLongitudinalsRelatedByPesertaDidikId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPesertaDidikLongitudinalRelatedByPesertaDidikId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getBeasiswaPesertaDidiksRelatedByPesertaDidikId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBeasiswaPesertaDidikRelatedByPesertaDidikId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getBeasiswaPesertaDidiksRelatedByPesertaDidikId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBeasiswaPesertaDidikRelatedByPesertaDidikId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRegistrasiPesertaDidiksRelatedByPesertaDidikId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRegistrasiPesertaDidikRelatedByPesertaDidikId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRegistrasiPesertaDidiksRelatedByPesertaDidikId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRegistrasiPesertaDidikRelatedByPesertaDidikId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldPesertaDidiksRelatedByPesertaDidikId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldPesertaDidikRelatedByPesertaDidikId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldPesertaDidiksRelatedByPesertaDidikId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldPesertaDidikRelatedByPesertaDidikId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPesertaDidikBarusRelatedByPesertaDidikId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPesertaDidikBaruRelatedByPesertaDidikId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPesertaDidikBarusRelatedByPesertaDidikId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPesertaDidikBaruRelatedByPesertaDidikId($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setPesertaDidikId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return PesertaDidik Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return PesertaDidikPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new PesertaDidikPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a Sekolah object.
     *
     * @param             Sekolah $v
     * @return PesertaDidik The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSekolahRelatedBySekolahId(Sekolah $v = null)
    {
        if ($v === null) {
            $this->setSekolahId(NULL);
        } else {
            $this->setSekolahId($v->getSekolahId());
        }

        $this->aSekolahRelatedBySekolahId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Sekolah object, it will not be re-added.
        if ($v !== null) {
            $v->addPesertaDidikRelatedBySekolahId($this);
        }


        return $this;
    }


    /**
     * Get the associated Sekolah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Sekolah The associated Sekolah object.
     * @throws PropelException
     */
    public function getSekolahRelatedBySekolahId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSekolahRelatedBySekolahId === null && (($this->sekolah_id !== "" && $this->sekolah_id !== null)) && $doQuery) {
            $this->aSekolahRelatedBySekolahId = SekolahQuery::create()->findPk($this->sekolah_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSekolahRelatedBySekolahId->addPesertaDidiksRelatedBySekolahId($this);
             */
        }

        return $this->aSekolahRelatedBySekolahId;
    }

    /**
     * Declares an association between this object and a Sekolah object.
     *
     * @param             Sekolah $v
     * @return PesertaDidik The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSekolahRelatedBySekolahId(Sekolah $v = null)
    {
        if ($v === null) {
            $this->setSekolahId(NULL);
        } else {
            $this->setSekolahId($v->getSekolahId());
        }

        $this->aSekolahRelatedBySekolahId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Sekolah object, it will not be re-added.
        if ($v !== null) {
            $v->addPesertaDidikRelatedBySekolahId($this);
        }


        return $this;
    }


    /**
     * Get the associated Sekolah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Sekolah The associated Sekolah object.
     * @throws PropelException
     */
    public function getSekolahRelatedBySekolahId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSekolahRelatedBySekolahId === null && (($this->sekolah_id !== "" && $this->sekolah_id !== null)) && $doQuery) {
            $this->aSekolahRelatedBySekolahId = SekolahQuery::create()->findPk($this->sekolah_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSekolahRelatedBySekolahId->addPesertaDidiksRelatedBySekolahId($this);
             */
        }

        return $this->aSekolahRelatedBySekolahId;
    }

    /**
     * Declares an association between this object and a Agama object.
     *
     * @param             Agama $v
     * @return PesertaDidik The current object (for fluent API support)
     * @throws PropelException
     */
    public function setAgamaRelatedByAgamaId(Agama $v = null)
    {
        if ($v === null) {
            $this->setAgamaId(NULL);
        } else {
            $this->setAgamaId($v->getAgamaId());
        }

        $this->aAgamaRelatedByAgamaId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Agama object, it will not be re-added.
        if ($v !== null) {
            $v->addPesertaDidikRelatedByAgamaId($this);
        }


        return $this;
    }


    /**
     * Get the associated Agama object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Agama The associated Agama object.
     * @throws PropelException
     */
    public function getAgamaRelatedByAgamaId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aAgamaRelatedByAgamaId === null && ($this->agama_id !== null) && $doQuery) {
            $this->aAgamaRelatedByAgamaId = AgamaQuery::create()->findPk($this->agama_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aAgamaRelatedByAgamaId->addPesertaDidiksRelatedByAgamaId($this);
             */
        }

        return $this->aAgamaRelatedByAgamaId;
    }

    /**
     * Declares an association between this object and a Agama object.
     *
     * @param             Agama $v
     * @return PesertaDidik The current object (for fluent API support)
     * @throws PropelException
     */
    public function setAgamaRelatedByAgamaId(Agama $v = null)
    {
        if ($v === null) {
            $this->setAgamaId(NULL);
        } else {
            $this->setAgamaId($v->getAgamaId());
        }

        $this->aAgamaRelatedByAgamaId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Agama object, it will not be re-added.
        if ($v !== null) {
            $v->addPesertaDidikRelatedByAgamaId($this);
        }


        return $this;
    }


    /**
     * Get the associated Agama object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Agama The associated Agama object.
     * @throws PropelException
     */
    public function getAgamaRelatedByAgamaId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aAgamaRelatedByAgamaId === null && ($this->agama_id !== null) && $doQuery) {
            $this->aAgamaRelatedByAgamaId = AgamaQuery::create()->findPk($this->agama_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aAgamaRelatedByAgamaId->addPesertaDidiksRelatedByAgamaId($this);
             */
        }

        return $this->aAgamaRelatedByAgamaId;
    }

    /**
     * Declares an association between this object and a AlatTransportasi object.
     *
     * @param             AlatTransportasi $v
     * @return PesertaDidik The current object (for fluent API support)
     * @throws PropelException
     */
    public function setAlatTransportasiRelatedByAlatTransportasiId(AlatTransportasi $v = null)
    {
        if ($v === null) {
            $this->setAlatTransportasiId(NULL);
        } else {
            $this->setAlatTransportasiId($v->getAlatTransportasiId());
        }

        $this->aAlatTransportasiRelatedByAlatTransportasiId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the AlatTransportasi object, it will not be re-added.
        if ($v !== null) {
            $v->addPesertaDidikRelatedByAlatTransportasiId($this);
        }


        return $this;
    }


    /**
     * Get the associated AlatTransportasi object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return AlatTransportasi The associated AlatTransportasi object.
     * @throws PropelException
     */
    public function getAlatTransportasiRelatedByAlatTransportasiId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aAlatTransportasiRelatedByAlatTransportasiId === null && (($this->alat_transportasi_id !== "" && $this->alat_transportasi_id !== null)) && $doQuery) {
            $this->aAlatTransportasiRelatedByAlatTransportasiId = AlatTransportasiQuery::create()->findPk($this->alat_transportasi_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aAlatTransportasiRelatedByAlatTransportasiId->addPesertaDidiksRelatedByAlatTransportasiId($this);
             */
        }

        return $this->aAlatTransportasiRelatedByAlatTransportasiId;
    }

    /**
     * Declares an association between this object and a AlatTransportasi object.
     *
     * @param             AlatTransportasi $v
     * @return PesertaDidik The current object (for fluent API support)
     * @throws PropelException
     */
    public function setAlatTransportasiRelatedByAlatTransportasiId(AlatTransportasi $v = null)
    {
        if ($v === null) {
            $this->setAlatTransportasiId(NULL);
        } else {
            $this->setAlatTransportasiId($v->getAlatTransportasiId());
        }

        $this->aAlatTransportasiRelatedByAlatTransportasiId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the AlatTransportasi object, it will not be re-added.
        if ($v !== null) {
            $v->addPesertaDidikRelatedByAlatTransportasiId($this);
        }


        return $this;
    }


    /**
     * Get the associated AlatTransportasi object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return AlatTransportasi The associated AlatTransportasi object.
     * @throws PropelException
     */
    public function getAlatTransportasiRelatedByAlatTransportasiId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aAlatTransportasiRelatedByAlatTransportasiId === null && (($this->alat_transportasi_id !== "" && $this->alat_transportasi_id !== null)) && $doQuery) {
            $this->aAlatTransportasiRelatedByAlatTransportasiId = AlatTransportasiQuery::create()->findPk($this->alat_transportasi_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aAlatTransportasiRelatedByAlatTransportasiId->addPesertaDidiksRelatedByAlatTransportasiId($this);
             */
        }

        return $this->aAlatTransportasiRelatedByAlatTransportasiId;
    }

    /**
     * Declares an association between this object and a JenisTinggal object.
     *
     * @param             JenisTinggal $v
     * @return PesertaDidik The current object (for fluent API support)
     * @throws PropelException
     */
    public function setJenisTinggalRelatedByJenisTinggalId(JenisTinggal $v = null)
    {
        if ($v === null) {
            $this->setJenisTinggalId(NULL);
        } else {
            $this->setJenisTinggalId($v->getJenisTinggalId());
        }

        $this->aJenisTinggalRelatedByJenisTinggalId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the JenisTinggal object, it will not be re-added.
        if ($v !== null) {
            $v->addPesertaDidikRelatedByJenisTinggalId($this);
        }


        return $this;
    }


    /**
     * Get the associated JenisTinggal object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return JenisTinggal The associated JenisTinggal object.
     * @throws PropelException
     */
    public function getJenisTinggalRelatedByJenisTinggalId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aJenisTinggalRelatedByJenisTinggalId === null && (($this->jenis_tinggal_id !== "" && $this->jenis_tinggal_id !== null)) && $doQuery) {
            $this->aJenisTinggalRelatedByJenisTinggalId = JenisTinggalQuery::create()->findPk($this->jenis_tinggal_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aJenisTinggalRelatedByJenisTinggalId->addPesertaDidiksRelatedByJenisTinggalId($this);
             */
        }

        return $this->aJenisTinggalRelatedByJenisTinggalId;
    }

    /**
     * Declares an association between this object and a JenisTinggal object.
     *
     * @param             JenisTinggal $v
     * @return PesertaDidik The current object (for fluent API support)
     * @throws PropelException
     */
    public function setJenisTinggalRelatedByJenisTinggalId(JenisTinggal $v = null)
    {
        if ($v === null) {
            $this->setJenisTinggalId(NULL);
        } else {
            $this->setJenisTinggalId($v->getJenisTinggalId());
        }

        $this->aJenisTinggalRelatedByJenisTinggalId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the JenisTinggal object, it will not be re-added.
        if ($v !== null) {
            $v->addPesertaDidikRelatedByJenisTinggalId($this);
        }


        return $this;
    }


    /**
     * Get the associated JenisTinggal object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return JenisTinggal The associated JenisTinggal object.
     * @throws PropelException
     */
    public function getJenisTinggalRelatedByJenisTinggalId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aJenisTinggalRelatedByJenisTinggalId === null && (($this->jenis_tinggal_id !== "" && $this->jenis_tinggal_id !== null)) && $doQuery) {
            $this->aJenisTinggalRelatedByJenisTinggalId = JenisTinggalQuery::create()->findPk($this->jenis_tinggal_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aJenisTinggalRelatedByJenisTinggalId->addPesertaDidiksRelatedByJenisTinggalId($this);
             */
        }

        return $this->aJenisTinggalRelatedByJenisTinggalId;
    }

    /**
     * Declares an association between this object and a JenjangPendidikan object.
     *
     * @param             JenjangPendidikan $v
     * @return PesertaDidik The current object (for fluent API support)
     * @throws PropelException
     */
    public function setJenjangPendidikanRelatedByJenjangPendidikanIbu(JenjangPendidikan $v = null)
    {
        if ($v === null) {
            $this->setJenjangPendidikanIbu(NULL);
        } else {
            $this->setJenjangPendidikanIbu($v->getJenjangPendidikanId());
        }

        $this->aJenjangPendidikanRelatedByJenjangPendidikanIbu = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the JenjangPendidikan object, it will not be re-added.
        if ($v !== null) {
            $v->addPesertaDidikRelatedByJenjangPendidikanIbu($this);
        }


        return $this;
    }


    /**
     * Get the associated JenjangPendidikan object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return JenjangPendidikan The associated JenjangPendidikan object.
     * @throws PropelException
     */
    public function getJenjangPendidikanRelatedByJenjangPendidikanIbu(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aJenjangPendidikanRelatedByJenjangPendidikanIbu === null && (($this->jenjang_pendidikan_ibu !== "" && $this->jenjang_pendidikan_ibu !== null)) && $doQuery) {
            $this->aJenjangPendidikanRelatedByJenjangPendidikanIbu = JenjangPendidikanQuery::create()->findPk($this->jenjang_pendidikan_ibu, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aJenjangPendidikanRelatedByJenjangPendidikanIbu->addPesertaDidiksRelatedByJenjangPendidikanIbu($this);
             */
        }

        return $this->aJenjangPendidikanRelatedByJenjangPendidikanIbu;
    }

    /**
     * Declares an association between this object and a JenjangPendidikan object.
     *
     * @param             JenjangPendidikan $v
     * @return PesertaDidik The current object (for fluent API support)
     * @throws PropelException
     */
    public function setJenjangPendidikanRelatedByJenjangPendidikanAyah(JenjangPendidikan $v = null)
    {
        if ($v === null) {
            $this->setJenjangPendidikanAyah(NULL);
        } else {
            $this->setJenjangPendidikanAyah($v->getJenjangPendidikanId());
        }

        $this->aJenjangPendidikanRelatedByJenjangPendidikanAyah = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the JenjangPendidikan object, it will not be re-added.
        if ($v !== null) {
            $v->addPesertaDidikRelatedByJenjangPendidikanAyah($this);
        }


        return $this;
    }


    /**
     * Get the associated JenjangPendidikan object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return JenjangPendidikan The associated JenjangPendidikan object.
     * @throws PropelException
     */
    public function getJenjangPendidikanRelatedByJenjangPendidikanAyah(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aJenjangPendidikanRelatedByJenjangPendidikanAyah === null && (($this->jenjang_pendidikan_ayah !== "" && $this->jenjang_pendidikan_ayah !== null)) && $doQuery) {
            $this->aJenjangPendidikanRelatedByJenjangPendidikanAyah = JenjangPendidikanQuery::create()->findPk($this->jenjang_pendidikan_ayah, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aJenjangPendidikanRelatedByJenjangPendidikanAyah->addPesertaDidiksRelatedByJenjangPendidikanAyah($this);
             */
        }

        return $this->aJenjangPendidikanRelatedByJenjangPendidikanAyah;
    }

    /**
     * Declares an association between this object and a JenjangPendidikan object.
     *
     * @param             JenjangPendidikan $v
     * @return PesertaDidik The current object (for fluent API support)
     * @throws PropelException
     */
    public function setJenjangPendidikanRelatedByJenjangPendidikanWali(JenjangPendidikan $v = null)
    {
        if ($v === null) {
            $this->setJenjangPendidikanWali(NULL);
        } else {
            $this->setJenjangPendidikanWali($v->getJenjangPendidikanId());
        }

        $this->aJenjangPendidikanRelatedByJenjangPendidikanWali = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the JenjangPendidikan object, it will not be re-added.
        if ($v !== null) {
            $v->addPesertaDidikRelatedByJenjangPendidikanWali($this);
        }


        return $this;
    }


    /**
     * Get the associated JenjangPendidikan object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return JenjangPendidikan The associated JenjangPendidikan object.
     * @throws PropelException
     */
    public function getJenjangPendidikanRelatedByJenjangPendidikanWali(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aJenjangPendidikanRelatedByJenjangPendidikanWali === null && (($this->jenjang_pendidikan_wali !== "" && $this->jenjang_pendidikan_wali !== null)) && $doQuery) {
            $this->aJenjangPendidikanRelatedByJenjangPendidikanWali = JenjangPendidikanQuery::create()->findPk($this->jenjang_pendidikan_wali, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aJenjangPendidikanRelatedByJenjangPendidikanWali->addPesertaDidiksRelatedByJenjangPendidikanWali($this);
             */
        }

        return $this->aJenjangPendidikanRelatedByJenjangPendidikanWali;
    }

    /**
     * Declares an association between this object and a JenjangPendidikan object.
     *
     * @param             JenjangPendidikan $v
     * @return PesertaDidik The current object (for fluent API support)
     * @throws PropelException
     */
    public function setJenjangPendidikanRelatedByJenjangPendidikanIbu(JenjangPendidikan $v = null)
    {
        if ($v === null) {
            $this->setJenjangPendidikanIbu(NULL);
        } else {
            $this->setJenjangPendidikanIbu($v->getJenjangPendidikanId());
        }

        $this->aJenjangPendidikanRelatedByJenjangPendidikanIbu = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the JenjangPendidikan object, it will not be re-added.
        if ($v !== null) {
            $v->addPesertaDidikRelatedByJenjangPendidikanIbu($this);
        }


        return $this;
    }


    /**
     * Get the associated JenjangPendidikan object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return JenjangPendidikan The associated JenjangPendidikan object.
     * @throws PropelException
     */
    public function getJenjangPendidikanRelatedByJenjangPendidikanIbu(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aJenjangPendidikanRelatedByJenjangPendidikanIbu === null && (($this->jenjang_pendidikan_ibu !== "" && $this->jenjang_pendidikan_ibu !== null)) && $doQuery) {
            $this->aJenjangPendidikanRelatedByJenjangPendidikanIbu = JenjangPendidikanQuery::create()->findPk($this->jenjang_pendidikan_ibu, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aJenjangPendidikanRelatedByJenjangPendidikanIbu->addPesertaDidiksRelatedByJenjangPendidikanIbu($this);
             */
        }

        return $this->aJenjangPendidikanRelatedByJenjangPendidikanIbu;
    }

    /**
     * Declares an association between this object and a JenjangPendidikan object.
     *
     * @param             JenjangPendidikan $v
     * @return PesertaDidik The current object (for fluent API support)
     * @throws PropelException
     */
    public function setJenjangPendidikanRelatedByJenjangPendidikanAyah(JenjangPendidikan $v = null)
    {
        if ($v === null) {
            $this->setJenjangPendidikanAyah(NULL);
        } else {
            $this->setJenjangPendidikanAyah($v->getJenjangPendidikanId());
        }

        $this->aJenjangPendidikanRelatedByJenjangPendidikanAyah = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the JenjangPendidikan object, it will not be re-added.
        if ($v !== null) {
            $v->addPesertaDidikRelatedByJenjangPendidikanAyah($this);
        }


        return $this;
    }


    /**
     * Get the associated JenjangPendidikan object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return JenjangPendidikan The associated JenjangPendidikan object.
     * @throws PropelException
     */
    public function getJenjangPendidikanRelatedByJenjangPendidikanAyah(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aJenjangPendidikanRelatedByJenjangPendidikanAyah === null && (($this->jenjang_pendidikan_ayah !== "" && $this->jenjang_pendidikan_ayah !== null)) && $doQuery) {
            $this->aJenjangPendidikanRelatedByJenjangPendidikanAyah = JenjangPendidikanQuery::create()->findPk($this->jenjang_pendidikan_ayah, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aJenjangPendidikanRelatedByJenjangPendidikanAyah->addPesertaDidiksRelatedByJenjangPendidikanAyah($this);
             */
        }

        return $this->aJenjangPendidikanRelatedByJenjangPendidikanAyah;
    }

    /**
     * Declares an association between this object and a JenjangPendidikan object.
     *
     * @param             JenjangPendidikan $v
     * @return PesertaDidik The current object (for fluent API support)
     * @throws PropelException
     */
    public function setJenjangPendidikanRelatedByJenjangPendidikanWali(JenjangPendidikan $v = null)
    {
        if ($v === null) {
            $this->setJenjangPendidikanWali(NULL);
        } else {
            $this->setJenjangPendidikanWali($v->getJenjangPendidikanId());
        }

        $this->aJenjangPendidikanRelatedByJenjangPendidikanWali = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the JenjangPendidikan object, it will not be re-added.
        if ($v !== null) {
            $v->addPesertaDidikRelatedByJenjangPendidikanWali($this);
        }


        return $this;
    }


    /**
     * Get the associated JenjangPendidikan object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return JenjangPendidikan The associated JenjangPendidikan object.
     * @throws PropelException
     */
    public function getJenjangPendidikanRelatedByJenjangPendidikanWali(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aJenjangPendidikanRelatedByJenjangPendidikanWali === null && (($this->jenjang_pendidikan_wali !== "" && $this->jenjang_pendidikan_wali !== null)) && $doQuery) {
            $this->aJenjangPendidikanRelatedByJenjangPendidikanWali = JenjangPendidikanQuery::create()->findPk($this->jenjang_pendidikan_wali, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aJenjangPendidikanRelatedByJenjangPendidikanWali->addPesertaDidiksRelatedByJenjangPendidikanWali($this);
             */
        }

        return $this->aJenjangPendidikanRelatedByJenjangPendidikanWali;
    }

    /**
     * Declares an association between this object and a KebutuhanKhusus object.
     *
     * @param             KebutuhanKhusus $v
     * @return PesertaDidik The current object (for fluent API support)
     * @throws PropelException
     */
    public function setKebutuhanKhususRelatedByKebutuhanKhususIdAyah(KebutuhanKhusus $v = null)
    {
        if ($v === null) {
            $this->setKebutuhanKhususIdAyah(NULL);
        } else {
            $this->setKebutuhanKhususIdAyah($v->getKebutuhanKhususId());
        }

        $this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the KebutuhanKhusus object, it will not be re-added.
        if ($v !== null) {
            $v->addPesertaDidikRelatedByKebutuhanKhususIdAyah($this);
        }


        return $this;
    }


    /**
     * Get the associated KebutuhanKhusus object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return KebutuhanKhusus The associated KebutuhanKhusus object.
     * @throws PropelException
     */
    public function getKebutuhanKhususRelatedByKebutuhanKhususIdAyah(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah === null && ($this->kebutuhan_khusus_id_ayah !== null) && $doQuery) {
            $this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah = KebutuhanKhususQuery::create()->findPk($this->kebutuhan_khusus_id_ayah, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah->addPesertaDidiksRelatedByKebutuhanKhususIdAyah($this);
             */
        }

        return $this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah;
    }

    /**
     * Declares an association between this object and a KebutuhanKhusus object.
     *
     * @param             KebutuhanKhusus $v
     * @return PesertaDidik The current object (for fluent API support)
     * @throws PropelException
     */
    public function setKebutuhanKhususRelatedByKebutuhanKhususIdIbu(KebutuhanKhusus $v = null)
    {
        if ($v === null) {
            $this->setKebutuhanKhususIdIbu(NULL);
        } else {
            $this->setKebutuhanKhususIdIbu($v->getKebutuhanKhususId());
        }

        $this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the KebutuhanKhusus object, it will not be re-added.
        if ($v !== null) {
            $v->addPesertaDidikRelatedByKebutuhanKhususIdIbu($this);
        }


        return $this;
    }


    /**
     * Get the associated KebutuhanKhusus object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return KebutuhanKhusus The associated KebutuhanKhusus object.
     * @throws PropelException
     */
    public function getKebutuhanKhususRelatedByKebutuhanKhususIdIbu(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu === null && ($this->kebutuhan_khusus_id_ibu !== null) && $doQuery) {
            $this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu = KebutuhanKhususQuery::create()->findPk($this->kebutuhan_khusus_id_ibu, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu->addPesertaDidiksRelatedByKebutuhanKhususIdIbu($this);
             */
        }

        return $this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu;
    }

    /**
     * Declares an association between this object and a KebutuhanKhusus object.
     *
     * @param             KebutuhanKhusus $v
     * @return PesertaDidik The current object (for fluent API support)
     * @throws PropelException
     */
    public function setKebutuhanKhususRelatedByKebutuhanKhususId(KebutuhanKhusus $v = null)
    {
        if ($v === null) {
            $this->setKebutuhanKhususId(NULL);
        } else {
            $this->setKebutuhanKhususId($v->getKebutuhanKhususId());
        }

        $this->aKebutuhanKhususRelatedByKebutuhanKhususId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the KebutuhanKhusus object, it will not be re-added.
        if ($v !== null) {
            $v->addPesertaDidikRelatedByKebutuhanKhususId($this);
        }


        return $this;
    }


    /**
     * Get the associated KebutuhanKhusus object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return KebutuhanKhusus The associated KebutuhanKhusus object.
     * @throws PropelException
     */
    public function getKebutuhanKhususRelatedByKebutuhanKhususId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId === null && ($this->kebutuhan_khusus_id !== null) && $doQuery) {
            $this->aKebutuhanKhususRelatedByKebutuhanKhususId = KebutuhanKhususQuery::create()->findPk($this->kebutuhan_khusus_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aKebutuhanKhususRelatedByKebutuhanKhususId->addPesertaDidiksRelatedByKebutuhanKhususId($this);
             */
        }

        return $this->aKebutuhanKhususRelatedByKebutuhanKhususId;
    }

    /**
     * Declares an association between this object and a KebutuhanKhusus object.
     *
     * @param             KebutuhanKhusus $v
     * @return PesertaDidik The current object (for fluent API support)
     * @throws PropelException
     */
    public function setKebutuhanKhususRelatedByKebutuhanKhususIdAyah(KebutuhanKhusus $v = null)
    {
        if ($v === null) {
            $this->setKebutuhanKhususIdAyah(NULL);
        } else {
            $this->setKebutuhanKhususIdAyah($v->getKebutuhanKhususId());
        }

        $this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the KebutuhanKhusus object, it will not be re-added.
        if ($v !== null) {
            $v->addPesertaDidikRelatedByKebutuhanKhususIdAyah($this);
        }


        return $this;
    }


    /**
     * Get the associated KebutuhanKhusus object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return KebutuhanKhusus The associated KebutuhanKhusus object.
     * @throws PropelException
     */
    public function getKebutuhanKhususRelatedByKebutuhanKhususIdAyah(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah === null && ($this->kebutuhan_khusus_id_ayah !== null) && $doQuery) {
            $this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah = KebutuhanKhususQuery::create()->findPk($this->kebutuhan_khusus_id_ayah, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah->addPesertaDidiksRelatedByKebutuhanKhususIdAyah($this);
             */
        }

        return $this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah;
    }

    /**
     * Declares an association between this object and a KebutuhanKhusus object.
     *
     * @param             KebutuhanKhusus $v
     * @return PesertaDidik The current object (for fluent API support)
     * @throws PropelException
     */
    public function setKebutuhanKhususRelatedByKebutuhanKhususIdIbu(KebutuhanKhusus $v = null)
    {
        if ($v === null) {
            $this->setKebutuhanKhususIdIbu(NULL);
        } else {
            $this->setKebutuhanKhususIdIbu($v->getKebutuhanKhususId());
        }

        $this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the KebutuhanKhusus object, it will not be re-added.
        if ($v !== null) {
            $v->addPesertaDidikRelatedByKebutuhanKhususIdIbu($this);
        }


        return $this;
    }


    /**
     * Get the associated KebutuhanKhusus object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return KebutuhanKhusus The associated KebutuhanKhusus object.
     * @throws PropelException
     */
    public function getKebutuhanKhususRelatedByKebutuhanKhususIdIbu(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu === null && ($this->kebutuhan_khusus_id_ibu !== null) && $doQuery) {
            $this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu = KebutuhanKhususQuery::create()->findPk($this->kebutuhan_khusus_id_ibu, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu->addPesertaDidiksRelatedByKebutuhanKhususIdIbu($this);
             */
        }

        return $this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu;
    }

    /**
     * Declares an association between this object and a KebutuhanKhusus object.
     *
     * @param             KebutuhanKhusus $v
     * @return PesertaDidik The current object (for fluent API support)
     * @throws PropelException
     */
    public function setKebutuhanKhususRelatedByKebutuhanKhususId(KebutuhanKhusus $v = null)
    {
        if ($v === null) {
            $this->setKebutuhanKhususId(NULL);
        } else {
            $this->setKebutuhanKhususId($v->getKebutuhanKhususId());
        }

        $this->aKebutuhanKhususRelatedByKebutuhanKhususId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the KebutuhanKhusus object, it will not be re-added.
        if ($v !== null) {
            $v->addPesertaDidikRelatedByKebutuhanKhususId($this);
        }


        return $this;
    }


    /**
     * Get the associated KebutuhanKhusus object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return KebutuhanKhusus The associated KebutuhanKhusus object.
     * @throws PropelException
     */
    public function getKebutuhanKhususRelatedByKebutuhanKhususId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId === null && ($this->kebutuhan_khusus_id !== null) && $doQuery) {
            $this->aKebutuhanKhususRelatedByKebutuhanKhususId = KebutuhanKhususQuery::create()->findPk($this->kebutuhan_khusus_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aKebutuhanKhususRelatedByKebutuhanKhususId->addPesertaDidiksRelatedByKebutuhanKhususId($this);
             */
        }

        return $this->aKebutuhanKhususRelatedByKebutuhanKhususId;
    }

    /**
     * Declares an association between this object and a MstWilayah object.
     *
     * @param             MstWilayah $v
     * @return PesertaDidik The current object (for fluent API support)
     * @throws PropelException
     */
    public function setMstWilayahRelatedByKodeWilayah(MstWilayah $v = null)
    {
        if ($v === null) {
            $this->setKodeWilayah(NULL);
        } else {
            $this->setKodeWilayah($v->getKodeWilayah());
        }

        $this->aMstWilayahRelatedByKodeWilayah = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the MstWilayah object, it will not be re-added.
        if ($v !== null) {
            $v->addPesertaDidikRelatedByKodeWilayah($this);
        }


        return $this;
    }


    /**
     * Get the associated MstWilayah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return MstWilayah The associated MstWilayah object.
     * @throws PropelException
     */
    public function getMstWilayahRelatedByKodeWilayah(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aMstWilayahRelatedByKodeWilayah === null && (($this->kode_wilayah !== "" && $this->kode_wilayah !== null)) && $doQuery) {
            $this->aMstWilayahRelatedByKodeWilayah = MstWilayahQuery::create()->findPk($this->kode_wilayah, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aMstWilayahRelatedByKodeWilayah->addPesertaDidiksRelatedByKodeWilayah($this);
             */
        }

        return $this->aMstWilayahRelatedByKodeWilayah;
    }

    /**
     * Declares an association between this object and a MstWilayah object.
     *
     * @param             MstWilayah $v
     * @return PesertaDidik The current object (for fluent API support)
     * @throws PropelException
     */
    public function setMstWilayahRelatedByKodeWilayah(MstWilayah $v = null)
    {
        if ($v === null) {
            $this->setKodeWilayah(NULL);
        } else {
            $this->setKodeWilayah($v->getKodeWilayah());
        }

        $this->aMstWilayahRelatedByKodeWilayah = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the MstWilayah object, it will not be re-added.
        if ($v !== null) {
            $v->addPesertaDidikRelatedByKodeWilayah($this);
        }


        return $this;
    }


    /**
     * Get the associated MstWilayah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return MstWilayah The associated MstWilayah object.
     * @throws PropelException
     */
    public function getMstWilayahRelatedByKodeWilayah(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aMstWilayahRelatedByKodeWilayah === null && (($this->kode_wilayah !== "" && $this->kode_wilayah !== null)) && $doQuery) {
            $this->aMstWilayahRelatedByKodeWilayah = MstWilayahQuery::create()->findPk($this->kode_wilayah, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aMstWilayahRelatedByKodeWilayah->addPesertaDidiksRelatedByKodeWilayah($this);
             */
        }

        return $this->aMstWilayahRelatedByKodeWilayah;
    }

    /**
     * Declares an association between this object and a Negara object.
     *
     * @param             Negara $v
     * @return PesertaDidik The current object (for fluent API support)
     * @throws PropelException
     */
    public function setNegaraRelatedByKewarganegaraan(Negara $v = null)
    {
        if ($v === null) {
            $this->setKewarganegaraan(NULL);
        } else {
            $this->setKewarganegaraan($v->getNegaraId());
        }

        $this->aNegaraRelatedByKewarganegaraan = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Negara object, it will not be re-added.
        if ($v !== null) {
            $v->addPesertaDidikRelatedByKewarganegaraan($this);
        }


        return $this;
    }


    /**
     * Get the associated Negara object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Negara The associated Negara object.
     * @throws PropelException
     */
    public function getNegaraRelatedByKewarganegaraan(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aNegaraRelatedByKewarganegaraan === null && (($this->kewarganegaraan !== "" && $this->kewarganegaraan !== null)) && $doQuery) {
            $this->aNegaraRelatedByKewarganegaraan = NegaraQuery::create()->findPk($this->kewarganegaraan, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aNegaraRelatedByKewarganegaraan->addPesertaDidiksRelatedByKewarganegaraan($this);
             */
        }

        return $this->aNegaraRelatedByKewarganegaraan;
    }

    /**
     * Declares an association between this object and a Negara object.
     *
     * @param             Negara $v
     * @return PesertaDidik The current object (for fluent API support)
     * @throws PropelException
     */
    public function setNegaraRelatedByKewarganegaraan(Negara $v = null)
    {
        if ($v === null) {
            $this->setKewarganegaraan(NULL);
        } else {
            $this->setKewarganegaraan($v->getNegaraId());
        }

        $this->aNegaraRelatedByKewarganegaraan = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Negara object, it will not be re-added.
        if ($v !== null) {
            $v->addPesertaDidikRelatedByKewarganegaraan($this);
        }


        return $this;
    }


    /**
     * Get the associated Negara object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Negara The associated Negara object.
     * @throws PropelException
     */
    public function getNegaraRelatedByKewarganegaraan(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aNegaraRelatedByKewarganegaraan === null && (($this->kewarganegaraan !== "" && $this->kewarganegaraan !== null)) && $doQuery) {
            $this->aNegaraRelatedByKewarganegaraan = NegaraQuery::create()->findPk($this->kewarganegaraan, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aNegaraRelatedByKewarganegaraan->addPesertaDidiksRelatedByKewarganegaraan($this);
             */
        }

        return $this->aNegaraRelatedByKewarganegaraan;
    }

    /**
     * Declares an association between this object and a Pekerjaan object.
     *
     * @param             Pekerjaan $v
     * @return PesertaDidik The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPekerjaanRelatedByPekerjaanIdAyah(Pekerjaan $v = null)
    {
        if ($v === null) {
            $this->setPekerjaanIdAyah(NULL);
        } else {
            $this->setPekerjaanIdAyah($v->getPekerjaanId());
        }

        $this->aPekerjaanRelatedByPekerjaanIdAyah = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Pekerjaan object, it will not be re-added.
        if ($v !== null) {
            $v->addPesertaDidikRelatedByPekerjaanIdAyah($this);
        }


        return $this;
    }


    /**
     * Get the associated Pekerjaan object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Pekerjaan The associated Pekerjaan object.
     * @throws PropelException
     */
    public function getPekerjaanRelatedByPekerjaanIdAyah(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPekerjaanRelatedByPekerjaanIdAyah === null && ($this->pekerjaan_id_ayah !== null) && $doQuery) {
            $this->aPekerjaanRelatedByPekerjaanIdAyah = PekerjaanQuery::create()->findPk($this->pekerjaan_id_ayah, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPekerjaanRelatedByPekerjaanIdAyah->addPesertaDidiksRelatedByPekerjaanIdAyah($this);
             */
        }

        return $this->aPekerjaanRelatedByPekerjaanIdAyah;
    }

    /**
     * Declares an association between this object and a Pekerjaan object.
     *
     * @param             Pekerjaan $v
     * @return PesertaDidik The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPekerjaanRelatedByPekerjaanIdIbu(Pekerjaan $v = null)
    {
        if ($v === null) {
            $this->setPekerjaanIdIbu(NULL);
        } else {
            $this->setPekerjaanIdIbu($v->getPekerjaanId());
        }

        $this->aPekerjaanRelatedByPekerjaanIdIbu = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Pekerjaan object, it will not be re-added.
        if ($v !== null) {
            $v->addPesertaDidikRelatedByPekerjaanIdIbu($this);
        }


        return $this;
    }


    /**
     * Get the associated Pekerjaan object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Pekerjaan The associated Pekerjaan object.
     * @throws PropelException
     */
    public function getPekerjaanRelatedByPekerjaanIdIbu(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPekerjaanRelatedByPekerjaanIdIbu === null && ($this->pekerjaan_id_ibu !== null) && $doQuery) {
            $this->aPekerjaanRelatedByPekerjaanIdIbu = PekerjaanQuery::create()->findPk($this->pekerjaan_id_ibu, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPekerjaanRelatedByPekerjaanIdIbu->addPesertaDidiksRelatedByPekerjaanIdIbu($this);
             */
        }

        return $this->aPekerjaanRelatedByPekerjaanIdIbu;
    }

    /**
     * Declares an association between this object and a Pekerjaan object.
     *
     * @param             Pekerjaan $v
     * @return PesertaDidik The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPekerjaanRelatedByPekerjaanIdWali(Pekerjaan $v = null)
    {
        if ($v === null) {
            $this->setPekerjaanIdWali(NULL);
        } else {
            $this->setPekerjaanIdWali($v->getPekerjaanId());
        }

        $this->aPekerjaanRelatedByPekerjaanIdWali = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Pekerjaan object, it will not be re-added.
        if ($v !== null) {
            $v->addPesertaDidikRelatedByPekerjaanIdWali($this);
        }


        return $this;
    }


    /**
     * Get the associated Pekerjaan object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Pekerjaan The associated Pekerjaan object.
     * @throws PropelException
     */
    public function getPekerjaanRelatedByPekerjaanIdWali(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPekerjaanRelatedByPekerjaanIdWali === null && ($this->pekerjaan_id_wali !== null) && $doQuery) {
            $this->aPekerjaanRelatedByPekerjaanIdWali = PekerjaanQuery::create()->findPk($this->pekerjaan_id_wali, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPekerjaanRelatedByPekerjaanIdWali->addPesertaDidiksRelatedByPekerjaanIdWali($this);
             */
        }

        return $this->aPekerjaanRelatedByPekerjaanIdWali;
    }

    /**
     * Declares an association between this object and a Pekerjaan object.
     *
     * @param             Pekerjaan $v
     * @return PesertaDidik The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPekerjaanRelatedByPekerjaanIdAyah(Pekerjaan $v = null)
    {
        if ($v === null) {
            $this->setPekerjaanIdAyah(NULL);
        } else {
            $this->setPekerjaanIdAyah($v->getPekerjaanId());
        }

        $this->aPekerjaanRelatedByPekerjaanIdAyah = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Pekerjaan object, it will not be re-added.
        if ($v !== null) {
            $v->addPesertaDidikRelatedByPekerjaanIdAyah($this);
        }


        return $this;
    }


    /**
     * Get the associated Pekerjaan object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Pekerjaan The associated Pekerjaan object.
     * @throws PropelException
     */
    public function getPekerjaanRelatedByPekerjaanIdAyah(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPekerjaanRelatedByPekerjaanIdAyah === null && ($this->pekerjaan_id_ayah !== null) && $doQuery) {
            $this->aPekerjaanRelatedByPekerjaanIdAyah = PekerjaanQuery::create()->findPk($this->pekerjaan_id_ayah, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPekerjaanRelatedByPekerjaanIdAyah->addPesertaDidiksRelatedByPekerjaanIdAyah($this);
             */
        }

        return $this->aPekerjaanRelatedByPekerjaanIdAyah;
    }

    /**
     * Declares an association between this object and a Pekerjaan object.
     *
     * @param             Pekerjaan $v
     * @return PesertaDidik The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPekerjaanRelatedByPekerjaanIdIbu(Pekerjaan $v = null)
    {
        if ($v === null) {
            $this->setPekerjaanIdIbu(NULL);
        } else {
            $this->setPekerjaanIdIbu($v->getPekerjaanId());
        }

        $this->aPekerjaanRelatedByPekerjaanIdIbu = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Pekerjaan object, it will not be re-added.
        if ($v !== null) {
            $v->addPesertaDidikRelatedByPekerjaanIdIbu($this);
        }


        return $this;
    }


    /**
     * Get the associated Pekerjaan object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Pekerjaan The associated Pekerjaan object.
     * @throws PropelException
     */
    public function getPekerjaanRelatedByPekerjaanIdIbu(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPekerjaanRelatedByPekerjaanIdIbu === null && ($this->pekerjaan_id_ibu !== null) && $doQuery) {
            $this->aPekerjaanRelatedByPekerjaanIdIbu = PekerjaanQuery::create()->findPk($this->pekerjaan_id_ibu, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPekerjaanRelatedByPekerjaanIdIbu->addPesertaDidiksRelatedByPekerjaanIdIbu($this);
             */
        }

        return $this->aPekerjaanRelatedByPekerjaanIdIbu;
    }

    /**
     * Declares an association between this object and a Pekerjaan object.
     *
     * @param             Pekerjaan $v
     * @return PesertaDidik The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPekerjaanRelatedByPekerjaanIdWali(Pekerjaan $v = null)
    {
        if ($v === null) {
            $this->setPekerjaanIdWali(NULL);
        } else {
            $this->setPekerjaanIdWali($v->getPekerjaanId());
        }

        $this->aPekerjaanRelatedByPekerjaanIdWali = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Pekerjaan object, it will not be re-added.
        if ($v !== null) {
            $v->addPesertaDidikRelatedByPekerjaanIdWali($this);
        }


        return $this;
    }


    /**
     * Get the associated Pekerjaan object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Pekerjaan The associated Pekerjaan object.
     * @throws PropelException
     */
    public function getPekerjaanRelatedByPekerjaanIdWali(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPekerjaanRelatedByPekerjaanIdWali === null && ($this->pekerjaan_id_wali !== null) && $doQuery) {
            $this->aPekerjaanRelatedByPekerjaanIdWali = PekerjaanQuery::create()->findPk($this->pekerjaan_id_wali, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPekerjaanRelatedByPekerjaanIdWali->addPesertaDidiksRelatedByPekerjaanIdWali($this);
             */
        }

        return $this->aPekerjaanRelatedByPekerjaanIdWali;
    }

    /**
     * Declares an association between this object and a PenghasilanOrangtuaWali object.
     *
     * @param             PenghasilanOrangtuaWali $v
     * @return PesertaDidik The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah(PenghasilanOrangtuaWali $v = null)
    {
        if ($v === null) {
            $this->setPenghasilanIdAyah(NULL);
        } else {
            $this->setPenghasilanIdAyah($v->getPenghasilanOrangtuaWaliId());
        }

        $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the PenghasilanOrangtuaWali object, it will not be re-added.
        if ($v !== null) {
            $v->addPesertaDidikRelatedByPenghasilanIdAyah($this);
        }


        return $this;
    }


    /**
     * Get the associated PenghasilanOrangtuaWali object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return PenghasilanOrangtuaWali The associated PenghasilanOrangtuaWali object.
     * @throws PropelException
     */
    public function getPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah === null && ($this->penghasilan_id_ayah !== null) && $doQuery) {
            $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah = PenghasilanOrangtuaWaliQuery::create()->findPk($this->penghasilan_id_ayah, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah->addPesertaDidiksRelatedByPenghasilanIdAyah($this);
             */
        }

        return $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah;
    }

    /**
     * Declares an association between this object and a PenghasilanOrangtuaWali object.
     *
     * @param             PenghasilanOrangtuaWali $v
     * @return PesertaDidik The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali(PenghasilanOrangtuaWali $v = null)
    {
        if ($v === null) {
            $this->setPenghasilanIdWali(NULL);
        } else {
            $this->setPenghasilanIdWali($v->getPenghasilanOrangtuaWaliId());
        }

        $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the PenghasilanOrangtuaWali object, it will not be re-added.
        if ($v !== null) {
            $v->addPesertaDidikRelatedByPenghasilanIdWali($this);
        }


        return $this;
    }


    /**
     * Get the associated PenghasilanOrangtuaWali object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return PenghasilanOrangtuaWali The associated PenghasilanOrangtuaWali object.
     * @throws PropelException
     */
    public function getPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali === null && ($this->penghasilan_id_wali !== null) && $doQuery) {
            $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali = PenghasilanOrangtuaWaliQuery::create()->findPk($this->penghasilan_id_wali, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali->addPesertaDidiksRelatedByPenghasilanIdWali($this);
             */
        }

        return $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali;
    }

    /**
     * Declares an association between this object and a PenghasilanOrangtuaWali object.
     *
     * @param             PenghasilanOrangtuaWali $v
     * @return PesertaDidik The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu(PenghasilanOrangtuaWali $v = null)
    {
        if ($v === null) {
            $this->setPenghasilanIdIbu(NULL);
        } else {
            $this->setPenghasilanIdIbu($v->getPenghasilanOrangtuaWaliId());
        }

        $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the PenghasilanOrangtuaWali object, it will not be re-added.
        if ($v !== null) {
            $v->addPesertaDidikRelatedByPenghasilanIdIbu($this);
        }


        return $this;
    }


    /**
     * Get the associated PenghasilanOrangtuaWali object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return PenghasilanOrangtuaWali The associated PenghasilanOrangtuaWali object.
     * @throws PropelException
     */
    public function getPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu === null && ($this->penghasilan_id_ibu !== null) && $doQuery) {
            $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu = PenghasilanOrangtuaWaliQuery::create()->findPk($this->penghasilan_id_ibu, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu->addPesertaDidiksRelatedByPenghasilanIdIbu($this);
             */
        }

        return $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu;
    }

    /**
     * Declares an association between this object and a PenghasilanOrangtuaWali object.
     *
     * @param             PenghasilanOrangtuaWali $v
     * @return PesertaDidik The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah(PenghasilanOrangtuaWali $v = null)
    {
        if ($v === null) {
            $this->setPenghasilanIdAyah(NULL);
        } else {
            $this->setPenghasilanIdAyah($v->getPenghasilanOrangtuaWaliId());
        }

        $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the PenghasilanOrangtuaWali object, it will not be re-added.
        if ($v !== null) {
            $v->addPesertaDidikRelatedByPenghasilanIdAyah($this);
        }


        return $this;
    }


    /**
     * Get the associated PenghasilanOrangtuaWali object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return PenghasilanOrangtuaWali The associated PenghasilanOrangtuaWali object.
     * @throws PropelException
     */
    public function getPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah === null && ($this->penghasilan_id_ayah !== null) && $doQuery) {
            $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah = PenghasilanOrangtuaWaliQuery::create()->findPk($this->penghasilan_id_ayah, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah->addPesertaDidiksRelatedByPenghasilanIdAyah($this);
             */
        }

        return $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah;
    }

    /**
     * Declares an association between this object and a PenghasilanOrangtuaWali object.
     *
     * @param             PenghasilanOrangtuaWali $v
     * @return PesertaDidik The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali(PenghasilanOrangtuaWali $v = null)
    {
        if ($v === null) {
            $this->setPenghasilanIdWali(NULL);
        } else {
            $this->setPenghasilanIdWali($v->getPenghasilanOrangtuaWaliId());
        }

        $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the PenghasilanOrangtuaWali object, it will not be re-added.
        if ($v !== null) {
            $v->addPesertaDidikRelatedByPenghasilanIdWali($this);
        }


        return $this;
    }


    /**
     * Get the associated PenghasilanOrangtuaWali object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return PenghasilanOrangtuaWali The associated PenghasilanOrangtuaWali object.
     * @throws PropelException
     */
    public function getPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali === null && ($this->penghasilan_id_wali !== null) && $doQuery) {
            $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali = PenghasilanOrangtuaWaliQuery::create()->findPk($this->penghasilan_id_wali, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali->addPesertaDidiksRelatedByPenghasilanIdWali($this);
             */
        }

        return $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali;
    }

    /**
     * Declares an association between this object and a PenghasilanOrangtuaWali object.
     *
     * @param             PenghasilanOrangtuaWali $v
     * @return PesertaDidik The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu(PenghasilanOrangtuaWali $v = null)
    {
        if ($v === null) {
            $this->setPenghasilanIdIbu(NULL);
        } else {
            $this->setPenghasilanIdIbu($v->getPenghasilanOrangtuaWaliId());
        }

        $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the PenghasilanOrangtuaWali object, it will not be re-added.
        if ($v !== null) {
            $v->addPesertaDidikRelatedByPenghasilanIdIbu($this);
        }


        return $this;
    }


    /**
     * Get the associated PenghasilanOrangtuaWali object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return PenghasilanOrangtuaWali The associated PenghasilanOrangtuaWali object.
     * @throws PropelException
     */
    public function getPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu === null && ($this->penghasilan_id_ibu !== null) && $doQuery) {
            $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu = PenghasilanOrangtuaWaliQuery::create()->findPk($this->penghasilan_id_ibu, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu->addPesertaDidiksRelatedByPenghasilanIdIbu($this);
             */
        }

        return $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('PrestasiRelatedByPesertaDidikId' == $relationName) {
            $this->initPrestasisRelatedByPesertaDidikId();
        }
        if ('PrestasiRelatedByPesertaDidikId' == $relationName) {
            $this->initPrestasisRelatedByPesertaDidikId();
        }
        if ('AnggotaRombelRelatedByPesertaDidikId' == $relationName) {
            $this->initAnggotaRombelsRelatedByPesertaDidikId();
        }
        if ('AnggotaRombelRelatedByPesertaDidikId' == $relationName) {
            $this->initAnggotaRombelsRelatedByPesertaDidikId();
        }
        if ('PesertaDidikLongitudinalRelatedByPesertaDidikId' == $relationName) {
            $this->initPesertaDidikLongitudinalsRelatedByPesertaDidikId();
        }
        if ('PesertaDidikLongitudinalRelatedByPesertaDidikId' == $relationName) {
            $this->initPesertaDidikLongitudinalsRelatedByPesertaDidikId();
        }
        if ('BeasiswaPesertaDidikRelatedByPesertaDidikId' == $relationName) {
            $this->initBeasiswaPesertaDidiksRelatedByPesertaDidikId();
        }
        if ('BeasiswaPesertaDidikRelatedByPesertaDidikId' == $relationName) {
            $this->initBeasiswaPesertaDidiksRelatedByPesertaDidikId();
        }
        if ('RegistrasiPesertaDidikRelatedByPesertaDidikId' == $relationName) {
            $this->initRegistrasiPesertaDidiksRelatedByPesertaDidikId();
        }
        if ('RegistrasiPesertaDidikRelatedByPesertaDidikId' == $relationName) {
            $this->initRegistrasiPesertaDidiksRelatedByPesertaDidikId();
        }
        if ('VldPesertaDidikRelatedByPesertaDidikId' == $relationName) {
            $this->initVldPesertaDidiksRelatedByPesertaDidikId();
        }
        if ('VldPesertaDidikRelatedByPesertaDidikId' == $relationName) {
            $this->initVldPesertaDidiksRelatedByPesertaDidikId();
        }
        if ('PesertaDidikBaruRelatedByPesertaDidikId' == $relationName) {
            $this->initPesertaDidikBarusRelatedByPesertaDidikId();
        }
        if ('PesertaDidikBaruRelatedByPesertaDidikId' == $relationName) {
            $this->initPesertaDidikBarusRelatedByPesertaDidikId();
        }
    }

    /**
     * Clears out the collPrestasisRelatedByPesertaDidikId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return PesertaDidik The current object (for fluent API support)
     * @see        addPrestasisRelatedByPesertaDidikId()
     */
    public function clearPrestasisRelatedByPesertaDidikId()
    {
        $this->collPrestasisRelatedByPesertaDidikId = null; // important to set this to null since that means it is uninitialized
        $this->collPrestasisRelatedByPesertaDidikIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPrestasisRelatedByPesertaDidikId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPrestasisRelatedByPesertaDidikId($v = true)
    {
        $this->collPrestasisRelatedByPesertaDidikIdPartial = $v;
    }

    /**
     * Initializes the collPrestasisRelatedByPesertaDidikId collection.
     *
     * By default this just sets the collPrestasisRelatedByPesertaDidikId collection to an empty array (like clearcollPrestasisRelatedByPesertaDidikId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPrestasisRelatedByPesertaDidikId($overrideExisting = true)
    {
        if (null !== $this->collPrestasisRelatedByPesertaDidikId && !$overrideExisting) {
            return;
        }
        $this->collPrestasisRelatedByPesertaDidikId = new PropelObjectCollection();
        $this->collPrestasisRelatedByPesertaDidikId->setModel('Prestasi');
    }

    /**
     * Gets an array of Prestasi objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this PesertaDidik is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Prestasi[] List of Prestasi objects
     * @throws PropelException
     */
    public function getPrestasisRelatedByPesertaDidikId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPrestasisRelatedByPesertaDidikIdPartial && !$this->isNew();
        if (null === $this->collPrestasisRelatedByPesertaDidikId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPrestasisRelatedByPesertaDidikId) {
                // return empty collection
                $this->initPrestasisRelatedByPesertaDidikId();
            } else {
                $collPrestasisRelatedByPesertaDidikId = PrestasiQuery::create(null, $criteria)
                    ->filterByPesertaDidikRelatedByPesertaDidikId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPrestasisRelatedByPesertaDidikIdPartial && count($collPrestasisRelatedByPesertaDidikId)) {
                      $this->initPrestasisRelatedByPesertaDidikId(false);

                      foreach($collPrestasisRelatedByPesertaDidikId as $obj) {
                        if (false == $this->collPrestasisRelatedByPesertaDidikId->contains($obj)) {
                          $this->collPrestasisRelatedByPesertaDidikId->append($obj);
                        }
                      }

                      $this->collPrestasisRelatedByPesertaDidikIdPartial = true;
                    }

                    $collPrestasisRelatedByPesertaDidikId->getInternalIterator()->rewind();
                    return $collPrestasisRelatedByPesertaDidikId;
                }

                if($partial && $this->collPrestasisRelatedByPesertaDidikId) {
                    foreach($this->collPrestasisRelatedByPesertaDidikId as $obj) {
                        if($obj->isNew()) {
                            $collPrestasisRelatedByPesertaDidikId[] = $obj;
                        }
                    }
                }

                $this->collPrestasisRelatedByPesertaDidikId = $collPrestasisRelatedByPesertaDidikId;
                $this->collPrestasisRelatedByPesertaDidikIdPartial = false;
            }
        }

        return $this->collPrestasisRelatedByPesertaDidikId;
    }

    /**
     * Sets a collection of PrestasiRelatedByPesertaDidikId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $prestasisRelatedByPesertaDidikId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setPrestasisRelatedByPesertaDidikId(PropelCollection $prestasisRelatedByPesertaDidikId, PropelPDO $con = null)
    {
        $prestasisRelatedByPesertaDidikIdToDelete = $this->getPrestasisRelatedByPesertaDidikId(new Criteria(), $con)->diff($prestasisRelatedByPesertaDidikId);

        $this->prestasisRelatedByPesertaDidikIdScheduledForDeletion = unserialize(serialize($prestasisRelatedByPesertaDidikIdToDelete));

        foreach ($prestasisRelatedByPesertaDidikIdToDelete as $prestasiRelatedByPesertaDidikIdRemoved) {
            $prestasiRelatedByPesertaDidikIdRemoved->setPesertaDidikRelatedByPesertaDidikId(null);
        }

        $this->collPrestasisRelatedByPesertaDidikId = null;
        foreach ($prestasisRelatedByPesertaDidikId as $prestasiRelatedByPesertaDidikId) {
            $this->addPrestasiRelatedByPesertaDidikId($prestasiRelatedByPesertaDidikId);
        }

        $this->collPrestasisRelatedByPesertaDidikId = $prestasisRelatedByPesertaDidikId;
        $this->collPrestasisRelatedByPesertaDidikIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Prestasi objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Prestasi objects.
     * @throws PropelException
     */
    public function countPrestasisRelatedByPesertaDidikId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPrestasisRelatedByPesertaDidikIdPartial && !$this->isNew();
        if (null === $this->collPrestasisRelatedByPesertaDidikId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPrestasisRelatedByPesertaDidikId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPrestasisRelatedByPesertaDidikId());
            }
            $query = PrestasiQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPesertaDidikRelatedByPesertaDidikId($this)
                ->count($con);
        }

        return count($this->collPrestasisRelatedByPesertaDidikId);
    }

    /**
     * Method called to associate a Prestasi object to this object
     * through the Prestasi foreign key attribute.
     *
     * @param    Prestasi $l Prestasi
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function addPrestasiRelatedByPesertaDidikId(Prestasi $l)
    {
        if ($this->collPrestasisRelatedByPesertaDidikId === null) {
            $this->initPrestasisRelatedByPesertaDidikId();
            $this->collPrestasisRelatedByPesertaDidikIdPartial = true;
        }
        if (!in_array($l, $this->collPrestasisRelatedByPesertaDidikId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPrestasiRelatedByPesertaDidikId($l);
        }

        return $this;
    }

    /**
     * @param	PrestasiRelatedByPesertaDidikId $prestasiRelatedByPesertaDidikId The prestasiRelatedByPesertaDidikId object to add.
     */
    protected function doAddPrestasiRelatedByPesertaDidikId($prestasiRelatedByPesertaDidikId)
    {
        $this->collPrestasisRelatedByPesertaDidikId[]= $prestasiRelatedByPesertaDidikId;
        $prestasiRelatedByPesertaDidikId->setPesertaDidikRelatedByPesertaDidikId($this);
    }

    /**
     * @param	PrestasiRelatedByPesertaDidikId $prestasiRelatedByPesertaDidikId The prestasiRelatedByPesertaDidikId object to remove.
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function removePrestasiRelatedByPesertaDidikId($prestasiRelatedByPesertaDidikId)
    {
        if ($this->getPrestasisRelatedByPesertaDidikId()->contains($prestasiRelatedByPesertaDidikId)) {
            $this->collPrestasisRelatedByPesertaDidikId->remove($this->collPrestasisRelatedByPesertaDidikId->search($prestasiRelatedByPesertaDidikId));
            if (null === $this->prestasisRelatedByPesertaDidikIdScheduledForDeletion) {
                $this->prestasisRelatedByPesertaDidikIdScheduledForDeletion = clone $this->collPrestasisRelatedByPesertaDidikId;
                $this->prestasisRelatedByPesertaDidikIdScheduledForDeletion->clear();
            }
            $this->prestasisRelatedByPesertaDidikIdScheduledForDeletion[]= clone $prestasiRelatedByPesertaDidikId;
            $prestasiRelatedByPesertaDidikId->setPesertaDidikRelatedByPesertaDidikId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related PrestasisRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Prestasi[] List of Prestasi objects
     */
    public function getPrestasisRelatedByPesertaDidikIdJoinJenisPrestasiRelatedByJenisPrestasiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PrestasiQuery::create(null, $criteria);
        $query->joinWith('JenisPrestasiRelatedByJenisPrestasiId', $join_behavior);

        return $this->getPrestasisRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related PrestasisRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Prestasi[] List of Prestasi objects
     */
    public function getPrestasisRelatedByPesertaDidikIdJoinJenisPrestasiRelatedByJenisPrestasiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PrestasiQuery::create(null, $criteria);
        $query->joinWith('JenisPrestasiRelatedByJenisPrestasiId', $join_behavior);

        return $this->getPrestasisRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related PrestasisRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Prestasi[] List of Prestasi objects
     */
    public function getPrestasisRelatedByPesertaDidikIdJoinTingkatPrestasiRelatedByTingkatPrestasiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PrestasiQuery::create(null, $criteria);
        $query->joinWith('TingkatPrestasiRelatedByTingkatPrestasiId', $join_behavior);

        return $this->getPrestasisRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related PrestasisRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Prestasi[] List of Prestasi objects
     */
    public function getPrestasisRelatedByPesertaDidikIdJoinTingkatPrestasiRelatedByTingkatPrestasiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PrestasiQuery::create(null, $criteria);
        $query->joinWith('TingkatPrestasiRelatedByTingkatPrestasiId', $join_behavior);

        return $this->getPrestasisRelatedByPesertaDidikId($query, $con);
    }

    /**
     * Clears out the collPrestasisRelatedByPesertaDidikId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return PesertaDidik The current object (for fluent API support)
     * @see        addPrestasisRelatedByPesertaDidikId()
     */
    public function clearPrestasisRelatedByPesertaDidikId()
    {
        $this->collPrestasisRelatedByPesertaDidikId = null; // important to set this to null since that means it is uninitialized
        $this->collPrestasisRelatedByPesertaDidikIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPrestasisRelatedByPesertaDidikId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPrestasisRelatedByPesertaDidikId($v = true)
    {
        $this->collPrestasisRelatedByPesertaDidikIdPartial = $v;
    }

    /**
     * Initializes the collPrestasisRelatedByPesertaDidikId collection.
     *
     * By default this just sets the collPrestasisRelatedByPesertaDidikId collection to an empty array (like clearcollPrestasisRelatedByPesertaDidikId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPrestasisRelatedByPesertaDidikId($overrideExisting = true)
    {
        if (null !== $this->collPrestasisRelatedByPesertaDidikId && !$overrideExisting) {
            return;
        }
        $this->collPrestasisRelatedByPesertaDidikId = new PropelObjectCollection();
        $this->collPrestasisRelatedByPesertaDidikId->setModel('Prestasi');
    }

    /**
     * Gets an array of Prestasi objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this PesertaDidik is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Prestasi[] List of Prestasi objects
     * @throws PropelException
     */
    public function getPrestasisRelatedByPesertaDidikId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPrestasisRelatedByPesertaDidikIdPartial && !$this->isNew();
        if (null === $this->collPrestasisRelatedByPesertaDidikId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPrestasisRelatedByPesertaDidikId) {
                // return empty collection
                $this->initPrestasisRelatedByPesertaDidikId();
            } else {
                $collPrestasisRelatedByPesertaDidikId = PrestasiQuery::create(null, $criteria)
                    ->filterByPesertaDidikRelatedByPesertaDidikId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPrestasisRelatedByPesertaDidikIdPartial && count($collPrestasisRelatedByPesertaDidikId)) {
                      $this->initPrestasisRelatedByPesertaDidikId(false);

                      foreach($collPrestasisRelatedByPesertaDidikId as $obj) {
                        if (false == $this->collPrestasisRelatedByPesertaDidikId->contains($obj)) {
                          $this->collPrestasisRelatedByPesertaDidikId->append($obj);
                        }
                      }

                      $this->collPrestasisRelatedByPesertaDidikIdPartial = true;
                    }

                    $collPrestasisRelatedByPesertaDidikId->getInternalIterator()->rewind();
                    return $collPrestasisRelatedByPesertaDidikId;
                }

                if($partial && $this->collPrestasisRelatedByPesertaDidikId) {
                    foreach($this->collPrestasisRelatedByPesertaDidikId as $obj) {
                        if($obj->isNew()) {
                            $collPrestasisRelatedByPesertaDidikId[] = $obj;
                        }
                    }
                }

                $this->collPrestasisRelatedByPesertaDidikId = $collPrestasisRelatedByPesertaDidikId;
                $this->collPrestasisRelatedByPesertaDidikIdPartial = false;
            }
        }

        return $this->collPrestasisRelatedByPesertaDidikId;
    }

    /**
     * Sets a collection of PrestasiRelatedByPesertaDidikId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $prestasisRelatedByPesertaDidikId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setPrestasisRelatedByPesertaDidikId(PropelCollection $prestasisRelatedByPesertaDidikId, PropelPDO $con = null)
    {
        $prestasisRelatedByPesertaDidikIdToDelete = $this->getPrestasisRelatedByPesertaDidikId(new Criteria(), $con)->diff($prestasisRelatedByPesertaDidikId);

        $this->prestasisRelatedByPesertaDidikIdScheduledForDeletion = unserialize(serialize($prestasisRelatedByPesertaDidikIdToDelete));

        foreach ($prestasisRelatedByPesertaDidikIdToDelete as $prestasiRelatedByPesertaDidikIdRemoved) {
            $prestasiRelatedByPesertaDidikIdRemoved->setPesertaDidikRelatedByPesertaDidikId(null);
        }

        $this->collPrestasisRelatedByPesertaDidikId = null;
        foreach ($prestasisRelatedByPesertaDidikId as $prestasiRelatedByPesertaDidikId) {
            $this->addPrestasiRelatedByPesertaDidikId($prestasiRelatedByPesertaDidikId);
        }

        $this->collPrestasisRelatedByPesertaDidikId = $prestasisRelatedByPesertaDidikId;
        $this->collPrestasisRelatedByPesertaDidikIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Prestasi objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Prestasi objects.
     * @throws PropelException
     */
    public function countPrestasisRelatedByPesertaDidikId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPrestasisRelatedByPesertaDidikIdPartial && !$this->isNew();
        if (null === $this->collPrestasisRelatedByPesertaDidikId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPrestasisRelatedByPesertaDidikId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPrestasisRelatedByPesertaDidikId());
            }
            $query = PrestasiQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPesertaDidikRelatedByPesertaDidikId($this)
                ->count($con);
        }

        return count($this->collPrestasisRelatedByPesertaDidikId);
    }

    /**
     * Method called to associate a Prestasi object to this object
     * through the Prestasi foreign key attribute.
     *
     * @param    Prestasi $l Prestasi
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function addPrestasiRelatedByPesertaDidikId(Prestasi $l)
    {
        if ($this->collPrestasisRelatedByPesertaDidikId === null) {
            $this->initPrestasisRelatedByPesertaDidikId();
            $this->collPrestasisRelatedByPesertaDidikIdPartial = true;
        }
        if (!in_array($l, $this->collPrestasisRelatedByPesertaDidikId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPrestasiRelatedByPesertaDidikId($l);
        }

        return $this;
    }

    /**
     * @param	PrestasiRelatedByPesertaDidikId $prestasiRelatedByPesertaDidikId The prestasiRelatedByPesertaDidikId object to add.
     */
    protected function doAddPrestasiRelatedByPesertaDidikId($prestasiRelatedByPesertaDidikId)
    {
        $this->collPrestasisRelatedByPesertaDidikId[]= $prestasiRelatedByPesertaDidikId;
        $prestasiRelatedByPesertaDidikId->setPesertaDidikRelatedByPesertaDidikId($this);
    }

    /**
     * @param	PrestasiRelatedByPesertaDidikId $prestasiRelatedByPesertaDidikId The prestasiRelatedByPesertaDidikId object to remove.
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function removePrestasiRelatedByPesertaDidikId($prestasiRelatedByPesertaDidikId)
    {
        if ($this->getPrestasisRelatedByPesertaDidikId()->contains($prestasiRelatedByPesertaDidikId)) {
            $this->collPrestasisRelatedByPesertaDidikId->remove($this->collPrestasisRelatedByPesertaDidikId->search($prestasiRelatedByPesertaDidikId));
            if (null === $this->prestasisRelatedByPesertaDidikIdScheduledForDeletion) {
                $this->prestasisRelatedByPesertaDidikIdScheduledForDeletion = clone $this->collPrestasisRelatedByPesertaDidikId;
                $this->prestasisRelatedByPesertaDidikIdScheduledForDeletion->clear();
            }
            $this->prestasisRelatedByPesertaDidikIdScheduledForDeletion[]= clone $prestasiRelatedByPesertaDidikId;
            $prestasiRelatedByPesertaDidikId->setPesertaDidikRelatedByPesertaDidikId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related PrestasisRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Prestasi[] List of Prestasi objects
     */
    public function getPrestasisRelatedByPesertaDidikIdJoinJenisPrestasiRelatedByJenisPrestasiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PrestasiQuery::create(null, $criteria);
        $query->joinWith('JenisPrestasiRelatedByJenisPrestasiId', $join_behavior);

        return $this->getPrestasisRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related PrestasisRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Prestasi[] List of Prestasi objects
     */
    public function getPrestasisRelatedByPesertaDidikIdJoinJenisPrestasiRelatedByJenisPrestasiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PrestasiQuery::create(null, $criteria);
        $query->joinWith('JenisPrestasiRelatedByJenisPrestasiId', $join_behavior);

        return $this->getPrestasisRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related PrestasisRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Prestasi[] List of Prestasi objects
     */
    public function getPrestasisRelatedByPesertaDidikIdJoinTingkatPrestasiRelatedByTingkatPrestasiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PrestasiQuery::create(null, $criteria);
        $query->joinWith('TingkatPrestasiRelatedByTingkatPrestasiId', $join_behavior);

        return $this->getPrestasisRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related PrestasisRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Prestasi[] List of Prestasi objects
     */
    public function getPrestasisRelatedByPesertaDidikIdJoinTingkatPrestasiRelatedByTingkatPrestasiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PrestasiQuery::create(null, $criteria);
        $query->joinWith('TingkatPrestasiRelatedByTingkatPrestasiId', $join_behavior);

        return $this->getPrestasisRelatedByPesertaDidikId($query, $con);
    }

    /**
     * Clears out the collAnggotaRombelsRelatedByPesertaDidikId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return PesertaDidik The current object (for fluent API support)
     * @see        addAnggotaRombelsRelatedByPesertaDidikId()
     */
    public function clearAnggotaRombelsRelatedByPesertaDidikId()
    {
        $this->collAnggotaRombelsRelatedByPesertaDidikId = null; // important to set this to null since that means it is uninitialized
        $this->collAnggotaRombelsRelatedByPesertaDidikIdPartial = null;

        return $this;
    }

    /**
     * reset is the collAnggotaRombelsRelatedByPesertaDidikId collection loaded partially
     *
     * @return void
     */
    public function resetPartialAnggotaRombelsRelatedByPesertaDidikId($v = true)
    {
        $this->collAnggotaRombelsRelatedByPesertaDidikIdPartial = $v;
    }

    /**
     * Initializes the collAnggotaRombelsRelatedByPesertaDidikId collection.
     *
     * By default this just sets the collAnggotaRombelsRelatedByPesertaDidikId collection to an empty array (like clearcollAnggotaRombelsRelatedByPesertaDidikId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initAnggotaRombelsRelatedByPesertaDidikId($overrideExisting = true)
    {
        if (null !== $this->collAnggotaRombelsRelatedByPesertaDidikId && !$overrideExisting) {
            return;
        }
        $this->collAnggotaRombelsRelatedByPesertaDidikId = new PropelObjectCollection();
        $this->collAnggotaRombelsRelatedByPesertaDidikId->setModel('AnggotaRombel');
    }

    /**
     * Gets an array of AnggotaRombel objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this PesertaDidik is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|AnggotaRombel[] List of AnggotaRombel objects
     * @throws PropelException
     */
    public function getAnggotaRombelsRelatedByPesertaDidikId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collAnggotaRombelsRelatedByPesertaDidikIdPartial && !$this->isNew();
        if (null === $this->collAnggotaRombelsRelatedByPesertaDidikId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collAnggotaRombelsRelatedByPesertaDidikId) {
                // return empty collection
                $this->initAnggotaRombelsRelatedByPesertaDidikId();
            } else {
                $collAnggotaRombelsRelatedByPesertaDidikId = AnggotaRombelQuery::create(null, $criteria)
                    ->filterByPesertaDidikRelatedByPesertaDidikId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collAnggotaRombelsRelatedByPesertaDidikIdPartial && count($collAnggotaRombelsRelatedByPesertaDidikId)) {
                      $this->initAnggotaRombelsRelatedByPesertaDidikId(false);

                      foreach($collAnggotaRombelsRelatedByPesertaDidikId as $obj) {
                        if (false == $this->collAnggotaRombelsRelatedByPesertaDidikId->contains($obj)) {
                          $this->collAnggotaRombelsRelatedByPesertaDidikId->append($obj);
                        }
                      }

                      $this->collAnggotaRombelsRelatedByPesertaDidikIdPartial = true;
                    }

                    $collAnggotaRombelsRelatedByPesertaDidikId->getInternalIterator()->rewind();
                    return $collAnggotaRombelsRelatedByPesertaDidikId;
                }

                if($partial && $this->collAnggotaRombelsRelatedByPesertaDidikId) {
                    foreach($this->collAnggotaRombelsRelatedByPesertaDidikId as $obj) {
                        if($obj->isNew()) {
                            $collAnggotaRombelsRelatedByPesertaDidikId[] = $obj;
                        }
                    }
                }

                $this->collAnggotaRombelsRelatedByPesertaDidikId = $collAnggotaRombelsRelatedByPesertaDidikId;
                $this->collAnggotaRombelsRelatedByPesertaDidikIdPartial = false;
            }
        }

        return $this->collAnggotaRombelsRelatedByPesertaDidikId;
    }

    /**
     * Sets a collection of AnggotaRombelRelatedByPesertaDidikId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $anggotaRombelsRelatedByPesertaDidikId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setAnggotaRombelsRelatedByPesertaDidikId(PropelCollection $anggotaRombelsRelatedByPesertaDidikId, PropelPDO $con = null)
    {
        $anggotaRombelsRelatedByPesertaDidikIdToDelete = $this->getAnggotaRombelsRelatedByPesertaDidikId(new Criteria(), $con)->diff($anggotaRombelsRelatedByPesertaDidikId);

        $this->anggotaRombelsRelatedByPesertaDidikIdScheduledForDeletion = unserialize(serialize($anggotaRombelsRelatedByPesertaDidikIdToDelete));

        foreach ($anggotaRombelsRelatedByPesertaDidikIdToDelete as $anggotaRombelRelatedByPesertaDidikIdRemoved) {
            $anggotaRombelRelatedByPesertaDidikIdRemoved->setPesertaDidikRelatedByPesertaDidikId(null);
        }

        $this->collAnggotaRombelsRelatedByPesertaDidikId = null;
        foreach ($anggotaRombelsRelatedByPesertaDidikId as $anggotaRombelRelatedByPesertaDidikId) {
            $this->addAnggotaRombelRelatedByPesertaDidikId($anggotaRombelRelatedByPesertaDidikId);
        }

        $this->collAnggotaRombelsRelatedByPesertaDidikId = $anggotaRombelsRelatedByPesertaDidikId;
        $this->collAnggotaRombelsRelatedByPesertaDidikIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related AnggotaRombel objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related AnggotaRombel objects.
     * @throws PropelException
     */
    public function countAnggotaRombelsRelatedByPesertaDidikId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collAnggotaRombelsRelatedByPesertaDidikIdPartial && !$this->isNew();
        if (null === $this->collAnggotaRombelsRelatedByPesertaDidikId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collAnggotaRombelsRelatedByPesertaDidikId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getAnggotaRombelsRelatedByPesertaDidikId());
            }
            $query = AnggotaRombelQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPesertaDidikRelatedByPesertaDidikId($this)
                ->count($con);
        }

        return count($this->collAnggotaRombelsRelatedByPesertaDidikId);
    }

    /**
     * Method called to associate a AnggotaRombel object to this object
     * through the AnggotaRombel foreign key attribute.
     *
     * @param    AnggotaRombel $l AnggotaRombel
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function addAnggotaRombelRelatedByPesertaDidikId(AnggotaRombel $l)
    {
        if ($this->collAnggotaRombelsRelatedByPesertaDidikId === null) {
            $this->initAnggotaRombelsRelatedByPesertaDidikId();
            $this->collAnggotaRombelsRelatedByPesertaDidikIdPartial = true;
        }
        if (!in_array($l, $this->collAnggotaRombelsRelatedByPesertaDidikId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddAnggotaRombelRelatedByPesertaDidikId($l);
        }

        return $this;
    }

    /**
     * @param	AnggotaRombelRelatedByPesertaDidikId $anggotaRombelRelatedByPesertaDidikId The anggotaRombelRelatedByPesertaDidikId object to add.
     */
    protected function doAddAnggotaRombelRelatedByPesertaDidikId($anggotaRombelRelatedByPesertaDidikId)
    {
        $this->collAnggotaRombelsRelatedByPesertaDidikId[]= $anggotaRombelRelatedByPesertaDidikId;
        $anggotaRombelRelatedByPesertaDidikId->setPesertaDidikRelatedByPesertaDidikId($this);
    }

    /**
     * @param	AnggotaRombelRelatedByPesertaDidikId $anggotaRombelRelatedByPesertaDidikId The anggotaRombelRelatedByPesertaDidikId object to remove.
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function removeAnggotaRombelRelatedByPesertaDidikId($anggotaRombelRelatedByPesertaDidikId)
    {
        if ($this->getAnggotaRombelsRelatedByPesertaDidikId()->contains($anggotaRombelRelatedByPesertaDidikId)) {
            $this->collAnggotaRombelsRelatedByPesertaDidikId->remove($this->collAnggotaRombelsRelatedByPesertaDidikId->search($anggotaRombelRelatedByPesertaDidikId));
            if (null === $this->anggotaRombelsRelatedByPesertaDidikIdScheduledForDeletion) {
                $this->anggotaRombelsRelatedByPesertaDidikIdScheduledForDeletion = clone $this->collAnggotaRombelsRelatedByPesertaDidikId;
                $this->anggotaRombelsRelatedByPesertaDidikIdScheduledForDeletion->clear();
            }
            $this->anggotaRombelsRelatedByPesertaDidikIdScheduledForDeletion[]= clone $anggotaRombelRelatedByPesertaDidikId;
            $anggotaRombelRelatedByPesertaDidikId->setPesertaDidikRelatedByPesertaDidikId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related AnggotaRombelsRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|AnggotaRombel[] List of AnggotaRombel objects
     */
    public function getAnggotaRombelsRelatedByPesertaDidikIdJoinRombonganBelajarRelatedByRombonganBelajarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AnggotaRombelQuery::create(null, $criteria);
        $query->joinWith('RombonganBelajarRelatedByRombonganBelajarId', $join_behavior);

        return $this->getAnggotaRombelsRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related AnggotaRombelsRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|AnggotaRombel[] List of AnggotaRombel objects
     */
    public function getAnggotaRombelsRelatedByPesertaDidikIdJoinRombonganBelajarRelatedByRombonganBelajarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AnggotaRombelQuery::create(null, $criteria);
        $query->joinWith('RombonganBelajarRelatedByRombonganBelajarId', $join_behavior);

        return $this->getAnggotaRombelsRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related AnggotaRombelsRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|AnggotaRombel[] List of AnggotaRombel objects
     */
    public function getAnggotaRombelsRelatedByPesertaDidikIdJoinJenisPendaftaranRelatedByJenisPendaftaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AnggotaRombelQuery::create(null, $criteria);
        $query->joinWith('JenisPendaftaranRelatedByJenisPendaftaranId', $join_behavior);

        return $this->getAnggotaRombelsRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related AnggotaRombelsRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|AnggotaRombel[] List of AnggotaRombel objects
     */
    public function getAnggotaRombelsRelatedByPesertaDidikIdJoinJenisPendaftaranRelatedByJenisPendaftaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AnggotaRombelQuery::create(null, $criteria);
        $query->joinWith('JenisPendaftaranRelatedByJenisPendaftaranId', $join_behavior);

        return $this->getAnggotaRombelsRelatedByPesertaDidikId($query, $con);
    }

    /**
     * Clears out the collAnggotaRombelsRelatedByPesertaDidikId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return PesertaDidik The current object (for fluent API support)
     * @see        addAnggotaRombelsRelatedByPesertaDidikId()
     */
    public function clearAnggotaRombelsRelatedByPesertaDidikId()
    {
        $this->collAnggotaRombelsRelatedByPesertaDidikId = null; // important to set this to null since that means it is uninitialized
        $this->collAnggotaRombelsRelatedByPesertaDidikIdPartial = null;

        return $this;
    }

    /**
     * reset is the collAnggotaRombelsRelatedByPesertaDidikId collection loaded partially
     *
     * @return void
     */
    public function resetPartialAnggotaRombelsRelatedByPesertaDidikId($v = true)
    {
        $this->collAnggotaRombelsRelatedByPesertaDidikIdPartial = $v;
    }

    /**
     * Initializes the collAnggotaRombelsRelatedByPesertaDidikId collection.
     *
     * By default this just sets the collAnggotaRombelsRelatedByPesertaDidikId collection to an empty array (like clearcollAnggotaRombelsRelatedByPesertaDidikId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initAnggotaRombelsRelatedByPesertaDidikId($overrideExisting = true)
    {
        if (null !== $this->collAnggotaRombelsRelatedByPesertaDidikId && !$overrideExisting) {
            return;
        }
        $this->collAnggotaRombelsRelatedByPesertaDidikId = new PropelObjectCollection();
        $this->collAnggotaRombelsRelatedByPesertaDidikId->setModel('AnggotaRombel');
    }

    /**
     * Gets an array of AnggotaRombel objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this PesertaDidik is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|AnggotaRombel[] List of AnggotaRombel objects
     * @throws PropelException
     */
    public function getAnggotaRombelsRelatedByPesertaDidikId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collAnggotaRombelsRelatedByPesertaDidikIdPartial && !$this->isNew();
        if (null === $this->collAnggotaRombelsRelatedByPesertaDidikId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collAnggotaRombelsRelatedByPesertaDidikId) {
                // return empty collection
                $this->initAnggotaRombelsRelatedByPesertaDidikId();
            } else {
                $collAnggotaRombelsRelatedByPesertaDidikId = AnggotaRombelQuery::create(null, $criteria)
                    ->filterByPesertaDidikRelatedByPesertaDidikId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collAnggotaRombelsRelatedByPesertaDidikIdPartial && count($collAnggotaRombelsRelatedByPesertaDidikId)) {
                      $this->initAnggotaRombelsRelatedByPesertaDidikId(false);

                      foreach($collAnggotaRombelsRelatedByPesertaDidikId as $obj) {
                        if (false == $this->collAnggotaRombelsRelatedByPesertaDidikId->contains($obj)) {
                          $this->collAnggotaRombelsRelatedByPesertaDidikId->append($obj);
                        }
                      }

                      $this->collAnggotaRombelsRelatedByPesertaDidikIdPartial = true;
                    }

                    $collAnggotaRombelsRelatedByPesertaDidikId->getInternalIterator()->rewind();
                    return $collAnggotaRombelsRelatedByPesertaDidikId;
                }

                if($partial && $this->collAnggotaRombelsRelatedByPesertaDidikId) {
                    foreach($this->collAnggotaRombelsRelatedByPesertaDidikId as $obj) {
                        if($obj->isNew()) {
                            $collAnggotaRombelsRelatedByPesertaDidikId[] = $obj;
                        }
                    }
                }

                $this->collAnggotaRombelsRelatedByPesertaDidikId = $collAnggotaRombelsRelatedByPesertaDidikId;
                $this->collAnggotaRombelsRelatedByPesertaDidikIdPartial = false;
            }
        }

        return $this->collAnggotaRombelsRelatedByPesertaDidikId;
    }

    /**
     * Sets a collection of AnggotaRombelRelatedByPesertaDidikId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $anggotaRombelsRelatedByPesertaDidikId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setAnggotaRombelsRelatedByPesertaDidikId(PropelCollection $anggotaRombelsRelatedByPesertaDidikId, PropelPDO $con = null)
    {
        $anggotaRombelsRelatedByPesertaDidikIdToDelete = $this->getAnggotaRombelsRelatedByPesertaDidikId(new Criteria(), $con)->diff($anggotaRombelsRelatedByPesertaDidikId);

        $this->anggotaRombelsRelatedByPesertaDidikIdScheduledForDeletion = unserialize(serialize($anggotaRombelsRelatedByPesertaDidikIdToDelete));

        foreach ($anggotaRombelsRelatedByPesertaDidikIdToDelete as $anggotaRombelRelatedByPesertaDidikIdRemoved) {
            $anggotaRombelRelatedByPesertaDidikIdRemoved->setPesertaDidikRelatedByPesertaDidikId(null);
        }

        $this->collAnggotaRombelsRelatedByPesertaDidikId = null;
        foreach ($anggotaRombelsRelatedByPesertaDidikId as $anggotaRombelRelatedByPesertaDidikId) {
            $this->addAnggotaRombelRelatedByPesertaDidikId($anggotaRombelRelatedByPesertaDidikId);
        }

        $this->collAnggotaRombelsRelatedByPesertaDidikId = $anggotaRombelsRelatedByPesertaDidikId;
        $this->collAnggotaRombelsRelatedByPesertaDidikIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related AnggotaRombel objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related AnggotaRombel objects.
     * @throws PropelException
     */
    public function countAnggotaRombelsRelatedByPesertaDidikId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collAnggotaRombelsRelatedByPesertaDidikIdPartial && !$this->isNew();
        if (null === $this->collAnggotaRombelsRelatedByPesertaDidikId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collAnggotaRombelsRelatedByPesertaDidikId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getAnggotaRombelsRelatedByPesertaDidikId());
            }
            $query = AnggotaRombelQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPesertaDidikRelatedByPesertaDidikId($this)
                ->count($con);
        }

        return count($this->collAnggotaRombelsRelatedByPesertaDidikId);
    }

    /**
     * Method called to associate a AnggotaRombel object to this object
     * through the AnggotaRombel foreign key attribute.
     *
     * @param    AnggotaRombel $l AnggotaRombel
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function addAnggotaRombelRelatedByPesertaDidikId(AnggotaRombel $l)
    {
        if ($this->collAnggotaRombelsRelatedByPesertaDidikId === null) {
            $this->initAnggotaRombelsRelatedByPesertaDidikId();
            $this->collAnggotaRombelsRelatedByPesertaDidikIdPartial = true;
        }
        if (!in_array($l, $this->collAnggotaRombelsRelatedByPesertaDidikId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddAnggotaRombelRelatedByPesertaDidikId($l);
        }

        return $this;
    }

    /**
     * @param	AnggotaRombelRelatedByPesertaDidikId $anggotaRombelRelatedByPesertaDidikId The anggotaRombelRelatedByPesertaDidikId object to add.
     */
    protected function doAddAnggotaRombelRelatedByPesertaDidikId($anggotaRombelRelatedByPesertaDidikId)
    {
        $this->collAnggotaRombelsRelatedByPesertaDidikId[]= $anggotaRombelRelatedByPesertaDidikId;
        $anggotaRombelRelatedByPesertaDidikId->setPesertaDidikRelatedByPesertaDidikId($this);
    }

    /**
     * @param	AnggotaRombelRelatedByPesertaDidikId $anggotaRombelRelatedByPesertaDidikId The anggotaRombelRelatedByPesertaDidikId object to remove.
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function removeAnggotaRombelRelatedByPesertaDidikId($anggotaRombelRelatedByPesertaDidikId)
    {
        if ($this->getAnggotaRombelsRelatedByPesertaDidikId()->contains($anggotaRombelRelatedByPesertaDidikId)) {
            $this->collAnggotaRombelsRelatedByPesertaDidikId->remove($this->collAnggotaRombelsRelatedByPesertaDidikId->search($anggotaRombelRelatedByPesertaDidikId));
            if (null === $this->anggotaRombelsRelatedByPesertaDidikIdScheduledForDeletion) {
                $this->anggotaRombelsRelatedByPesertaDidikIdScheduledForDeletion = clone $this->collAnggotaRombelsRelatedByPesertaDidikId;
                $this->anggotaRombelsRelatedByPesertaDidikIdScheduledForDeletion->clear();
            }
            $this->anggotaRombelsRelatedByPesertaDidikIdScheduledForDeletion[]= clone $anggotaRombelRelatedByPesertaDidikId;
            $anggotaRombelRelatedByPesertaDidikId->setPesertaDidikRelatedByPesertaDidikId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related AnggotaRombelsRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|AnggotaRombel[] List of AnggotaRombel objects
     */
    public function getAnggotaRombelsRelatedByPesertaDidikIdJoinRombonganBelajarRelatedByRombonganBelajarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AnggotaRombelQuery::create(null, $criteria);
        $query->joinWith('RombonganBelajarRelatedByRombonganBelajarId', $join_behavior);

        return $this->getAnggotaRombelsRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related AnggotaRombelsRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|AnggotaRombel[] List of AnggotaRombel objects
     */
    public function getAnggotaRombelsRelatedByPesertaDidikIdJoinRombonganBelajarRelatedByRombonganBelajarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AnggotaRombelQuery::create(null, $criteria);
        $query->joinWith('RombonganBelajarRelatedByRombonganBelajarId', $join_behavior);

        return $this->getAnggotaRombelsRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related AnggotaRombelsRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|AnggotaRombel[] List of AnggotaRombel objects
     */
    public function getAnggotaRombelsRelatedByPesertaDidikIdJoinJenisPendaftaranRelatedByJenisPendaftaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AnggotaRombelQuery::create(null, $criteria);
        $query->joinWith('JenisPendaftaranRelatedByJenisPendaftaranId', $join_behavior);

        return $this->getAnggotaRombelsRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related AnggotaRombelsRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|AnggotaRombel[] List of AnggotaRombel objects
     */
    public function getAnggotaRombelsRelatedByPesertaDidikIdJoinJenisPendaftaranRelatedByJenisPendaftaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AnggotaRombelQuery::create(null, $criteria);
        $query->joinWith('JenisPendaftaranRelatedByJenisPendaftaranId', $join_behavior);

        return $this->getAnggotaRombelsRelatedByPesertaDidikId($query, $con);
    }

    /**
     * Clears out the collPesertaDidikLongitudinalsRelatedByPesertaDidikId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return PesertaDidik The current object (for fluent API support)
     * @see        addPesertaDidikLongitudinalsRelatedByPesertaDidikId()
     */
    public function clearPesertaDidikLongitudinalsRelatedByPesertaDidikId()
    {
        $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId = null; // important to set this to null since that means it is uninitialized
        $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPesertaDidikLongitudinalsRelatedByPesertaDidikId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPesertaDidikLongitudinalsRelatedByPesertaDidikId($v = true)
    {
        $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikIdPartial = $v;
    }

    /**
     * Initializes the collPesertaDidikLongitudinalsRelatedByPesertaDidikId collection.
     *
     * By default this just sets the collPesertaDidikLongitudinalsRelatedByPesertaDidikId collection to an empty array (like clearcollPesertaDidikLongitudinalsRelatedByPesertaDidikId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPesertaDidikLongitudinalsRelatedByPesertaDidikId($overrideExisting = true)
    {
        if (null !== $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId && !$overrideExisting) {
            return;
        }
        $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId = new PropelObjectCollection();
        $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId->setModel('PesertaDidikLongitudinal');
    }

    /**
     * Gets an array of PesertaDidikLongitudinal objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this PesertaDidik is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|PesertaDidikLongitudinal[] List of PesertaDidikLongitudinal objects
     * @throws PropelException
     */
    public function getPesertaDidikLongitudinalsRelatedByPesertaDidikId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikIdPartial && !$this->isNew();
        if (null === $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId) {
                // return empty collection
                $this->initPesertaDidikLongitudinalsRelatedByPesertaDidikId();
            } else {
                $collPesertaDidikLongitudinalsRelatedByPesertaDidikId = PesertaDidikLongitudinalQuery::create(null, $criteria)
                    ->filterByPesertaDidikRelatedByPesertaDidikId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikIdPartial && count($collPesertaDidikLongitudinalsRelatedByPesertaDidikId)) {
                      $this->initPesertaDidikLongitudinalsRelatedByPesertaDidikId(false);

                      foreach($collPesertaDidikLongitudinalsRelatedByPesertaDidikId as $obj) {
                        if (false == $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId->contains($obj)) {
                          $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId->append($obj);
                        }
                      }

                      $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikIdPartial = true;
                    }

                    $collPesertaDidikLongitudinalsRelatedByPesertaDidikId->getInternalIterator()->rewind();
                    return $collPesertaDidikLongitudinalsRelatedByPesertaDidikId;
                }

                if($partial && $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId) {
                    foreach($this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId as $obj) {
                        if($obj->isNew()) {
                            $collPesertaDidikLongitudinalsRelatedByPesertaDidikId[] = $obj;
                        }
                    }
                }

                $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId = $collPesertaDidikLongitudinalsRelatedByPesertaDidikId;
                $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikIdPartial = false;
            }
        }

        return $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId;
    }

    /**
     * Sets a collection of PesertaDidikLongitudinalRelatedByPesertaDidikId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $pesertaDidikLongitudinalsRelatedByPesertaDidikId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setPesertaDidikLongitudinalsRelatedByPesertaDidikId(PropelCollection $pesertaDidikLongitudinalsRelatedByPesertaDidikId, PropelPDO $con = null)
    {
        $pesertaDidikLongitudinalsRelatedByPesertaDidikIdToDelete = $this->getPesertaDidikLongitudinalsRelatedByPesertaDidikId(new Criteria(), $con)->diff($pesertaDidikLongitudinalsRelatedByPesertaDidikId);

        $this->pesertaDidikLongitudinalsRelatedByPesertaDidikIdScheduledForDeletion = unserialize(serialize($pesertaDidikLongitudinalsRelatedByPesertaDidikIdToDelete));

        foreach ($pesertaDidikLongitudinalsRelatedByPesertaDidikIdToDelete as $pesertaDidikLongitudinalRelatedByPesertaDidikIdRemoved) {
            $pesertaDidikLongitudinalRelatedByPesertaDidikIdRemoved->setPesertaDidikRelatedByPesertaDidikId(null);
        }

        $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId = null;
        foreach ($pesertaDidikLongitudinalsRelatedByPesertaDidikId as $pesertaDidikLongitudinalRelatedByPesertaDidikId) {
            $this->addPesertaDidikLongitudinalRelatedByPesertaDidikId($pesertaDidikLongitudinalRelatedByPesertaDidikId);
        }

        $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId = $pesertaDidikLongitudinalsRelatedByPesertaDidikId;
        $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PesertaDidikLongitudinal objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related PesertaDidikLongitudinal objects.
     * @throws PropelException
     */
    public function countPesertaDidikLongitudinalsRelatedByPesertaDidikId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikIdPartial && !$this->isNew();
        if (null === $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPesertaDidikLongitudinalsRelatedByPesertaDidikId());
            }
            $query = PesertaDidikLongitudinalQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPesertaDidikRelatedByPesertaDidikId($this)
                ->count($con);
        }

        return count($this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId);
    }

    /**
     * Method called to associate a PesertaDidikLongitudinal object to this object
     * through the PesertaDidikLongitudinal foreign key attribute.
     *
     * @param    PesertaDidikLongitudinal $l PesertaDidikLongitudinal
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function addPesertaDidikLongitudinalRelatedByPesertaDidikId(PesertaDidikLongitudinal $l)
    {
        if ($this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId === null) {
            $this->initPesertaDidikLongitudinalsRelatedByPesertaDidikId();
            $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikIdPartial = true;
        }
        if (!in_array($l, $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPesertaDidikLongitudinalRelatedByPesertaDidikId($l);
        }

        return $this;
    }

    /**
     * @param	PesertaDidikLongitudinalRelatedByPesertaDidikId $pesertaDidikLongitudinalRelatedByPesertaDidikId The pesertaDidikLongitudinalRelatedByPesertaDidikId object to add.
     */
    protected function doAddPesertaDidikLongitudinalRelatedByPesertaDidikId($pesertaDidikLongitudinalRelatedByPesertaDidikId)
    {
        $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId[]= $pesertaDidikLongitudinalRelatedByPesertaDidikId;
        $pesertaDidikLongitudinalRelatedByPesertaDidikId->setPesertaDidikRelatedByPesertaDidikId($this);
    }

    /**
     * @param	PesertaDidikLongitudinalRelatedByPesertaDidikId $pesertaDidikLongitudinalRelatedByPesertaDidikId The pesertaDidikLongitudinalRelatedByPesertaDidikId object to remove.
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function removePesertaDidikLongitudinalRelatedByPesertaDidikId($pesertaDidikLongitudinalRelatedByPesertaDidikId)
    {
        if ($this->getPesertaDidikLongitudinalsRelatedByPesertaDidikId()->contains($pesertaDidikLongitudinalRelatedByPesertaDidikId)) {
            $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId->remove($this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId->search($pesertaDidikLongitudinalRelatedByPesertaDidikId));
            if (null === $this->pesertaDidikLongitudinalsRelatedByPesertaDidikIdScheduledForDeletion) {
                $this->pesertaDidikLongitudinalsRelatedByPesertaDidikIdScheduledForDeletion = clone $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId;
                $this->pesertaDidikLongitudinalsRelatedByPesertaDidikIdScheduledForDeletion->clear();
            }
            $this->pesertaDidikLongitudinalsRelatedByPesertaDidikIdScheduledForDeletion[]= clone $pesertaDidikLongitudinalRelatedByPesertaDidikId;
            $pesertaDidikLongitudinalRelatedByPesertaDidikId->setPesertaDidikRelatedByPesertaDidikId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related PesertaDidikLongitudinalsRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidikLongitudinal[] List of PesertaDidikLongitudinal objects
     */
    public function getPesertaDidikLongitudinalsRelatedByPesertaDidikIdJoinSemesterRelatedBySemesterId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikLongitudinalQuery::create(null, $criteria);
        $query->joinWith('SemesterRelatedBySemesterId', $join_behavior);

        return $this->getPesertaDidikLongitudinalsRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related PesertaDidikLongitudinalsRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidikLongitudinal[] List of PesertaDidikLongitudinal objects
     */
    public function getPesertaDidikLongitudinalsRelatedByPesertaDidikIdJoinSemesterRelatedBySemesterId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikLongitudinalQuery::create(null, $criteria);
        $query->joinWith('SemesterRelatedBySemesterId', $join_behavior);

        return $this->getPesertaDidikLongitudinalsRelatedByPesertaDidikId($query, $con);
    }

    /**
     * Clears out the collPesertaDidikLongitudinalsRelatedByPesertaDidikId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return PesertaDidik The current object (for fluent API support)
     * @see        addPesertaDidikLongitudinalsRelatedByPesertaDidikId()
     */
    public function clearPesertaDidikLongitudinalsRelatedByPesertaDidikId()
    {
        $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId = null; // important to set this to null since that means it is uninitialized
        $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPesertaDidikLongitudinalsRelatedByPesertaDidikId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPesertaDidikLongitudinalsRelatedByPesertaDidikId($v = true)
    {
        $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikIdPartial = $v;
    }

    /**
     * Initializes the collPesertaDidikLongitudinalsRelatedByPesertaDidikId collection.
     *
     * By default this just sets the collPesertaDidikLongitudinalsRelatedByPesertaDidikId collection to an empty array (like clearcollPesertaDidikLongitudinalsRelatedByPesertaDidikId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPesertaDidikLongitudinalsRelatedByPesertaDidikId($overrideExisting = true)
    {
        if (null !== $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId && !$overrideExisting) {
            return;
        }
        $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId = new PropelObjectCollection();
        $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId->setModel('PesertaDidikLongitudinal');
    }

    /**
     * Gets an array of PesertaDidikLongitudinal objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this PesertaDidik is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|PesertaDidikLongitudinal[] List of PesertaDidikLongitudinal objects
     * @throws PropelException
     */
    public function getPesertaDidikLongitudinalsRelatedByPesertaDidikId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikIdPartial && !$this->isNew();
        if (null === $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId) {
                // return empty collection
                $this->initPesertaDidikLongitudinalsRelatedByPesertaDidikId();
            } else {
                $collPesertaDidikLongitudinalsRelatedByPesertaDidikId = PesertaDidikLongitudinalQuery::create(null, $criteria)
                    ->filterByPesertaDidikRelatedByPesertaDidikId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikIdPartial && count($collPesertaDidikLongitudinalsRelatedByPesertaDidikId)) {
                      $this->initPesertaDidikLongitudinalsRelatedByPesertaDidikId(false);

                      foreach($collPesertaDidikLongitudinalsRelatedByPesertaDidikId as $obj) {
                        if (false == $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId->contains($obj)) {
                          $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId->append($obj);
                        }
                      }

                      $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikIdPartial = true;
                    }

                    $collPesertaDidikLongitudinalsRelatedByPesertaDidikId->getInternalIterator()->rewind();
                    return $collPesertaDidikLongitudinalsRelatedByPesertaDidikId;
                }

                if($partial && $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId) {
                    foreach($this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId as $obj) {
                        if($obj->isNew()) {
                            $collPesertaDidikLongitudinalsRelatedByPesertaDidikId[] = $obj;
                        }
                    }
                }

                $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId = $collPesertaDidikLongitudinalsRelatedByPesertaDidikId;
                $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikIdPartial = false;
            }
        }

        return $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId;
    }

    /**
     * Sets a collection of PesertaDidikLongitudinalRelatedByPesertaDidikId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $pesertaDidikLongitudinalsRelatedByPesertaDidikId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setPesertaDidikLongitudinalsRelatedByPesertaDidikId(PropelCollection $pesertaDidikLongitudinalsRelatedByPesertaDidikId, PropelPDO $con = null)
    {
        $pesertaDidikLongitudinalsRelatedByPesertaDidikIdToDelete = $this->getPesertaDidikLongitudinalsRelatedByPesertaDidikId(new Criteria(), $con)->diff($pesertaDidikLongitudinalsRelatedByPesertaDidikId);

        $this->pesertaDidikLongitudinalsRelatedByPesertaDidikIdScheduledForDeletion = unserialize(serialize($pesertaDidikLongitudinalsRelatedByPesertaDidikIdToDelete));

        foreach ($pesertaDidikLongitudinalsRelatedByPesertaDidikIdToDelete as $pesertaDidikLongitudinalRelatedByPesertaDidikIdRemoved) {
            $pesertaDidikLongitudinalRelatedByPesertaDidikIdRemoved->setPesertaDidikRelatedByPesertaDidikId(null);
        }

        $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId = null;
        foreach ($pesertaDidikLongitudinalsRelatedByPesertaDidikId as $pesertaDidikLongitudinalRelatedByPesertaDidikId) {
            $this->addPesertaDidikLongitudinalRelatedByPesertaDidikId($pesertaDidikLongitudinalRelatedByPesertaDidikId);
        }

        $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId = $pesertaDidikLongitudinalsRelatedByPesertaDidikId;
        $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PesertaDidikLongitudinal objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related PesertaDidikLongitudinal objects.
     * @throws PropelException
     */
    public function countPesertaDidikLongitudinalsRelatedByPesertaDidikId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikIdPartial && !$this->isNew();
        if (null === $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPesertaDidikLongitudinalsRelatedByPesertaDidikId());
            }
            $query = PesertaDidikLongitudinalQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPesertaDidikRelatedByPesertaDidikId($this)
                ->count($con);
        }

        return count($this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId);
    }

    /**
     * Method called to associate a PesertaDidikLongitudinal object to this object
     * through the PesertaDidikLongitudinal foreign key attribute.
     *
     * @param    PesertaDidikLongitudinal $l PesertaDidikLongitudinal
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function addPesertaDidikLongitudinalRelatedByPesertaDidikId(PesertaDidikLongitudinal $l)
    {
        if ($this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId === null) {
            $this->initPesertaDidikLongitudinalsRelatedByPesertaDidikId();
            $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikIdPartial = true;
        }
        if (!in_array($l, $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPesertaDidikLongitudinalRelatedByPesertaDidikId($l);
        }

        return $this;
    }

    /**
     * @param	PesertaDidikLongitudinalRelatedByPesertaDidikId $pesertaDidikLongitudinalRelatedByPesertaDidikId The pesertaDidikLongitudinalRelatedByPesertaDidikId object to add.
     */
    protected function doAddPesertaDidikLongitudinalRelatedByPesertaDidikId($pesertaDidikLongitudinalRelatedByPesertaDidikId)
    {
        $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId[]= $pesertaDidikLongitudinalRelatedByPesertaDidikId;
        $pesertaDidikLongitudinalRelatedByPesertaDidikId->setPesertaDidikRelatedByPesertaDidikId($this);
    }

    /**
     * @param	PesertaDidikLongitudinalRelatedByPesertaDidikId $pesertaDidikLongitudinalRelatedByPesertaDidikId The pesertaDidikLongitudinalRelatedByPesertaDidikId object to remove.
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function removePesertaDidikLongitudinalRelatedByPesertaDidikId($pesertaDidikLongitudinalRelatedByPesertaDidikId)
    {
        if ($this->getPesertaDidikLongitudinalsRelatedByPesertaDidikId()->contains($pesertaDidikLongitudinalRelatedByPesertaDidikId)) {
            $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId->remove($this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId->search($pesertaDidikLongitudinalRelatedByPesertaDidikId));
            if (null === $this->pesertaDidikLongitudinalsRelatedByPesertaDidikIdScheduledForDeletion) {
                $this->pesertaDidikLongitudinalsRelatedByPesertaDidikIdScheduledForDeletion = clone $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId;
                $this->pesertaDidikLongitudinalsRelatedByPesertaDidikIdScheduledForDeletion->clear();
            }
            $this->pesertaDidikLongitudinalsRelatedByPesertaDidikIdScheduledForDeletion[]= clone $pesertaDidikLongitudinalRelatedByPesertaDidikId;
            $pesertaDidikLongitudinalRelatedByPesertaDidikId->setPesertaDidikRelatedByPesertaDidikId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related PesertaDidikLongitudinalsRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidikLongitudinal[] List of PesertaDidikLongitudinal objects
     */
    public function getPesertaDidikLongitudinalsRelatedByPesertaDidikIdJoinSemesterRelatedBySemesterId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikLongitudinalQuery::create(null, $criteria);
        $query->joinWith('SemesterRelatedBySemesterId', $join_behavior);

        return $this->getPesertaDidikLongitudinalsRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related PesertaDidikLongitudinalsRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidikLongitudinal[] List of PesertaDidikLongitudinal objects
     */
    public function getPesertaDidikLongitudinalsRelatedByPesertaDidikIdJoinSemesterRelatedBySemesterId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikLongitudinalQuery::create(null, $criteria);
        $query->joinWith('SemesterRelatedBySemesterId', $join_behavior);

        return $this->getPesertaDidikLongitudinalsRelatedByPesertaDidikId($query, $con);
    }

    /**
     * Clears out the collBeasiswaPesertaDidiksRelatedByPesertaDidikId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return PesertaDidik The current object (for fluent API support)
     * @see        addBeasiswaPesertaDidiksRelatedByPesertaDidikId()
     */
    public function clearBeasiswaPesertaDidiksRelatedByPesertaDidikId()
    {
        $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId = null; // important to set this to null since that means it is uninitialized
        $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikIdPartial = null;

        return $this;
    }

    /**
     * reset is the collBeasiswaPesertaDidiksRelatedByPesertaDidikId collection loaded partially
     *
     * @return void
     */
    public function resetPartialBeasiswaPesertaDidiksRelatedByPesertaDidikId($v = true)
    {
        $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikIdPartial = $v;
    }

    /**
     * Initializes the collBeasiswaPesertaDidiksRelatedByPesertaDidikId collection.
     *
     * By default this just sets the collBeasiswaPesertaDidiksRelatedByPesertaDidikId collection to an empty array (like clearcollBeasiswaPesertaDidiksRelatedByPesertaDidikId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBeasiswaPesertaDidiksRelatedByPesertaDidikId($overrideExisting = true)
    {
        if (null !== $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId && !$overrideExisting) {
            return;
        }
        $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId = new PropelObjectCollection();
        $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId->setModel('BeasiswaPesertaDidik');
    }

    /**
     * Gets an array of BeasiswaPesertaDidik objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this PesertaDidik is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|BeasiswaPesertaDidik[] List of BeasiswaPesertaDidik objects
     * @throws PropelException
     */
    public function getBeasiswaPesertaDidiksRelatedByPesertaDidikId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikIdPartial && !$this->isNew();
        if (null === $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId) {
                // return empty collection
                $this->initBeasiswaPesertaDidiksRelatedByPesertaDidikId();
            } else {
                $collBeasiswaPesertaDidiksRelatedByPesertaDidikId = BeasiswaPesertaDidikQuery::create(null, $criteria)
                    ->filterByPesertaDidikRelatedByPesertaDidikId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikIdPartial && count($collBeasiswaPesertaDidiksRelatedByPesertaDidikId)) {
                      $this->initBeasiswaPesertaDidiksRelatedByPesertaDidikId(false);

                      foreach($collBeasiswaPesertaDidiksRelatedByPesertaDidikId as $obj) {
                        if (false == $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId->contains($obj)) {
                          $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId->append($obj);
                        }
                      }

                      $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikIdPartial = true;
                    }

                    $collBeasiswaPesertaDidiksRelatedByPesertaDidikId->getInternalIterator()->rewind();
                    return $collBeasiswaPesertaDidiksRelatedByPesertaDidikId;
                }

                if($partial && $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId) {
                    foreach($this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId as $obj) {
                        if($obj->isNew()) {
                            $collBeasiswaPesertaDidiksRelatedByPesertaDidikId[] = $obj;
                        }
                    }
                }

                $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId = $collBeasiswaPesertaDidiksRelatedByPesertaDidikId;
                $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikIdPartial = false;
            }
        }

        return $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId;
    }

    /**
     * Sets a collection of BeasiswaPesertaDidikRelatedByPesertaDidikId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $beasiswaPesertaDidiksRelatedByPesertaDidikId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setBeasiswaPesertaDidiksRelatedByPesertaDidikId(PropelCollection $beasiswaPesertaDidiksRelatedByPesertaDidikId, PropelPDO $con = null)
    {
        $beasiswaPesertaDidiksRelatedByPesertaDidikIdToDelete = $this->getBeasiswaPesertaDidiksRelatedByPesertaDidikId(new Criteria(), $con)->diff($beasiswaPesertaDidiksRelatedByPesertaDidikId);

        $this->beasiswaPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion = unserialize(serialize($beasiswaPesertaDidiksRelatedByPesertaDidikIdToDelete));

        foreach ($beasiswaPesertaDidiksRelatedByPesertaDidikIdToDelete as $beasiswaPesertaDidikRelatedByPesertaDidikIdRemoved) {
            $beasiswaPesertaDidikRelatedByPesertaDidikIdRemoved->setPesertaDidikRelatedByPesertaDidikId(null);
        }

        $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId = null;
        foreach ($beasiswaPesertaDidiksRelatedByPesertaDidikId as $beasiswaPesertaDidikRelatedByPesertaDidikId) {
            $this->addBeasiswaPesertaDidikRelatedByPesertaDidikId($beasiswaPesertaDidikRelatedByPesertaDidikId);
        }

        $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId = $beasiswaPesertaDidiksRelatedByPesertaDidikId;
        $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BeasiswaPesertaDidik objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related BeasiswaPesertaDidik objects.
     * @throws PropelException
     */
    public function countBeasiswaPesertaDidiksRelatedByPesertaDidikId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikIdPartial && !$this->isNew();
        if (null === $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getBeasiswaPesertaDidiksRelatedByPesertaDidikId());
            }
            $query = BeasiswaPesertaDidikQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPesertaDidikRelatedByPesertaDidikId($this)
                ->count($con);
        }

        return count($this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId);
    }

    /**
     * Method called to associate a BeasiswaPesertaDidik object to this object
     * through the BeasiswaPesertaDidik foreign key attribute.
     *
     * @param    BeasiswaPesertaDidik $l BeasiswaPesertaDidik
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function addBeasiswaPesertaDidikRelatedByPesertaDidikId(BeasiswaPesertaDidik $l)
    {
        if ($this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId === null) {
            $this->initBeasiswaPesertaDidiksRelatedByPesertaDidikId();
            $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikIdPartial = true;
        }
        if (!in_array($l, $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddBeasiswaPesertaDidikRelatedByPesertaDidikId($l);
        }

        return $this;
    }

    /**
     * @param	BeasiswaPesertaDidikRelatedByPesertaDidikId $beasiswaPesertaDidikRelatedByPesertaDidikId The beasiswaPesertaDidikRelatedByPesertaDidikId object to add.
     */
    protected function doAddBeasiswaPesertaDidikRelatedByPesertaDidikId($beasiswaPesertaDidikRelatedByPesertaDidikId)
    {
        $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId[]= $beasiswaPesertaDidikRelatedByPesertaDidikId;
        $beasiswaPesertaDidikRelatedByPesertaDidikId->setPesertaDidikRelatedByPesertaDidikId($this);
    }

    /**
     * @param	BeasiswaPesertaDidikRelatedByPesertaDidikId $beasiswaPesertaDidikRelatedByPesertaDidikId The beasiswaPesertaDidikRelatedByPesertaDidikId object to remove.
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function removeBeasiswaPesertaDidikRelatedByPesertaDidikId($beasiswaPesertaDidikRelatedByPesertaDidikId)
    {
        if ($this->getBeasiswaPesertaDidiksRelatedByPesertaDidikId()->contains($beasiswaPesertaDidikRelatedByPesertaDidikId)) {
            $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId->remove($this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId->search($beasiswaPesertaDidikRelatedByPesertaDidikId));
            if (null === $this->beasiswaPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion) {
                $this->beasiswaPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion = clone $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId;
                $this->beasiswaPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion->clear();
            }
            $this->beasiswaPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion[]= clone $beasiswaPesertaDidikRelatedByPesertaDidikId;
            $beasiswaPesertaDidikRelatedByPesertaDidikId->setPesertaDidikRelatedByPesertaDidikId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related BeasiswaPesertaDidiksRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BeasiswaPesertaDidik[] List of BeasiswaPesertaDidik objects
     */
    public function getBeasiswaPesertaDidiksRelatedByPesertaDidikIdJoinJenisBeasiswaRelatedByJenisBeasiswaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BeasiswaPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisBeasiswaRelatedByJenisBeasiswaId', $join_behavior);

        return $this->getBeasiswaPesertaDidiksRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related BeasiswaPesertaDidiksRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BeasiswaPesertaDidik[] List of BeasiswaPesertaDidik objects
     */
    public function getBeasiswaPesertaDidiksRelatedByPesertaDidikIdJoinJenisBeasiswaRelatedByJenisBeasiswaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BeasiswaPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisBeasiswaRelatedByJenisBeasiswaId', $join_behavior);

        return $this->getBeasiswaPesertaDidiksRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related BeasiswaPesertaDidiksRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BeasiswaPesertaDidik[] List of BeasiswaPesertaDidik objects
     */
    public function getBeasiswaPesertaDidiksRelatedByPesertaDidikIdJoinTahunAjaranRelatedByTahunSelesai($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BeasiswaPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('TahunAjaranRelatedByTahunSelesai', $join_behavior);

        return $this->getBeasiswaPesertaDidiksRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related BeasiswaPesertaDidiksRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BeasiswaPesertaDidik[] List of BeasiswaPesertaDidik objects
     */
    public function getBeasiswaPesertaDidiksRelatedByPesertaDidikIdJoinTahunAjaranRelatedByTahunMulai($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BeasiswaPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('TahunAjaranRelatedByTahunMulai', $join_behavior);

        return $this->getBeasiswaPesertaDidiksRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related BeasiswaPesertaDidiksRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BeasiswaPesertaDidik[] List of BeasiswaPesertaDidik objects
     */
    public function getBeasiswaPesertaDidiksRelatedByPesertaDidikIdJoinTahunAjaranRelatedByTahunSelesai($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BeasiswaPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('TahunAjaranRelatedByTahunSelesai', $join_behavior);

        return $this->getBeasiswaPesertaDidiksRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related BeasiswaPesertaDidiksRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BeasiswaPesertaDidik[] List of BeasiswaPesertaDidik objects
     */
    public function getBeasiswaPesertaDidiksRelatedByPesertaDidikIdJoinTahunAjaranRelatedByTahunMulai($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BeasiswaPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('TahunAjaranRelatedByTahunMulai', $join_behavior);

        return $this->getBeasiswaPesertaDidiksRelatedByPesertaDidikId($query, $con);
    }

    /**
     * Clears out the collBeasiswaPesertaDidiksRelatedByPesertaDidikId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return PesertaDidik The current object (for fluent API support)
     * @see        addBeasiswaPesertaDidiksRelatedByPesertaDidikId()
     */
    public function clearBeasiswaPesertaDidiksRelatedByPesertaDidikId()
    {
        $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId = null; // important to set this to null since that means it is uninitialized
        $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikIdPartial = null;

        return $this;
    }

    /**
     * reset is the collBeasiswaPesertaDidiksRelatedByPesertaDidikId collection loaded partially
     *
     * @return void
     */
    public function resetPartialBeasiswaPesertaDidiksRelatedByPesertaDidikId($v = true)
    {
        $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikIdPartial = $v;
    }

    /**
     * Initializes the collBeasiswaPesertaDidiksRelatedByPesertaDidikId collection.
     *
     * By default this just sets the collBeasiswaPesertaDidiksRelatedByPesertaDidikId collection to an empty array (like clearcollBeasiswaPesertaDidiksRelatedByPesertaDidikId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBeasiswaPesertaDidiksRelatedByPesertaDidikId($overrideExisting = true)
    {
        if (null !== $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId && !$overrideExisting) {
            return;
        }
        $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId = new PropelObjectCollection();
        $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId->setModel('BeasiswaPesertaDidik');
    }

    /**
     * Gets an array of BeasiswaPesertaDidik objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this PesertaDidik is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|BeasiswaPesertaDidik[] List of BeasiswaPesertaDidik objects
     * @throws PropelException
     */
    public function getBeasiswaPesertaDidiksRelatedByPesertaDidikId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikIdPartial && !$this->isNew();
        if (null === $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId) {
                // return empty collection
                $this->initBeasiswaPesertaDidiksRelatedByPesertaDidikId();
            } else {
                $collBeasiswaPesertaDidiksRelatedByPesertaDidikId = BeasiswaPesertaDidikQuery::create(null, $criteria)
                    ->filterByPesertaDidikRelatedByPesertaDidikId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikIdPartial && count($collBeasiswaPesertaDidiksRelatedByPesertaDidikId)) {
                      $this->initBeasiswaPesertaDidiksRelatedByPesertaDidikId(false);

                      foreach($collBeasiswaPesertaDidiksRelatedByPesertaDidikId as $obj) {
                        if (false == $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId->contains($obj)) {
                          $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId->append($obj);
                        }
                      }

                      $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikIdPartial = true;
                    }

                    $collBeasiswaPesertaDidiksRelatedByPesertaDidikId->getInternalIterator()->rewind();
                    return $collBeasiswaPesertaDidiksRelatedByPesertaDidikId;
                }

                if($partial && $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId) {
                    foreach($this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId as $obj) {
                        if($obj->isNew()) {
                            $collBeasiswaPesertaDidiksRelatedByPesertaDidikId[] = $obj;
                        }
                    }
                }

                $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId = $collBeasiswaPesertaDidiksRelatedByPesertaDidikId;
                $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikIdPartial = false;
            }
        }

        return $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId;
    }

    /**
     * Sets a collection of BeasiswaPesertaDidikRelatedByPesertaDidikId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $beasiswaPesertaDidiksRelatedByPesertaDidikId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setBeasiswaPesertaDidiksRelatedByPesertaDidikId(PropelCollection $beasiswaPesertaDidiksRelatedByPesertaDidikId, PropelPDO $con = null)
    {
        $beasiswaPesertaDidiksRelatedByPesertaDidikIdToDelete = $this->getBeasiswaPesertaDidiksRelatedByPesertaDidikId(new Criteria(), $con)->diff($beasiswaPesertaDidiksRelatedByPesertaDidikId);

        $this->beasiswaPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion = unserialize(serialize($beasiswaPesertaDidiksRelatedByPesertaDidikIdToDelete));

        foreach ($beasiswaPesertaDidiksRelatedByPesertaDidikIdToDelete as $beasiswaPesertaDidikRelatedByPesertaDidikIdRemoved) {
            $beasiswaPesertaDidikRelatedByPesertaDidikIdRemoved->setPesertaDidikRelatedByPesertaDidikId(null);
        }

        $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId = null;
        foreach ($beasiswaPesertaDidiksRelatedByPesertaDidikId as $beasiswaPesertaDidikRelatedByPesertaDidikId) {
            $this->addBeasiswaPesertaDidikRelatedByPesertaDidikId($beasiswaPesertaDidikRelatedByPesertaDidikId);
        }

        $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId = $beasiswaPesertaDidiksRelatedByPesertaDidikId;
        $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BeasiswaPesertaDidik objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related BeasiswaPesertaDidik objects.
     * @throws PropelException
     */
    public function countBeasiswaPesertaDidiksRelatedByPesertaDidikId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikIdPartial && !$this->isNew();
        if (null === $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getBeasiswaPesertaDidiksRelatedByPesertaDidikId());
            }
            $query = BeasiswaPesertaDidikQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPesertaDidikRelatedByPesertaDidikId($this)
                ->count($con);
        }

        return count($this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId);
    }

    /**
     * Method called to associate a BeasiswaPesertaDidik object to this object
     * through the BeasiswaPesertaDidik foreign key attribute.
     *
     * @param    BeasiswaPesertaDidik $l BeasiswaPesertaDidik
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function addBeasiswaPesertaDidikRelatedByPesertaDidikId(BeasiswaPesertaDidik $l)
    {
        if ($this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId === null) {
            $this->initBeasiswaPesertaDidiksRelatedByPesertaDidikId();
            $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikIdPartial = true;
        }
        if (!in_array($l, $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddBeasiswaPesertaDidikRelatedByPesertaDidikId($l);
        }

        return $this;
    }

    /**
     * @param	BeasiswaPesertaDidikRelatedByPesertaDidikId $beasiswaPesertaDidikRelatedByPesertaDidikId The beasiswaPesertaDidikRelatedByPesertaDidikId object to add.
     */
    protected function doAddBeasiswaPesertaDidikRelatedByPesertaDidikId($beasiswaPesertaDidikRelatedByPesertaDidikId)
    {
        $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId[]= $beasiswaPesertaDidikRelatedByPesertaDidikId;
        $beasiswaPesertaDidikRelatedByPesertaDidikId->setPesertaDidikRelatedByPesertaDidikId($this);
    }

    /**
     * @param	BeasiswaPesertaDidikRelatedByPesertaDidikId $beasiswaPesertaDidikRelatedByPesertaDidikId The beasiswaPesertaDidikRelatedByPesertaDidikId object to remove.
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function removeBeasiswaPesertaDidikRelatedByPesertaDidikId($beasiswaPesertaDidikRelatedByPesertaDidikId)
    {
        if ($this->getBeasiswaPesertaDidiksRelatedByPesertaDidikId()->contains($beasiswaPesertaDidikRelatedByPesertaDidikId)) {
            $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId->remove($this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId->search($beasiswaPesertaDidikRelatedByPesertaDidikId));
            if (null === $this->beasiswaPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion) {
                $this->beasiswaPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion = clone $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId;
                $this->beasiswaPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion->clear();
            }
            $this->beasiswaPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion[]= clone $beasiswaPesertaDidikRelatedByPesertaDidikId;
            $beasiswaPesertaDidikRelatedByPesertaDidikId->setPesertaDidikRelatedByPesertaDidikId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related BeasiswaPesertaDidiksRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BeasiswaPesertaDidik[] List of BeasiswaPesertaDidik objects
     */
    public function getBeasiswaPesertaDidiksRelatedByPesertaDidikIdJoinJenisBeasiswaRelatedByJenisBeasiswaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BeasiswaPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisBeasiswaRelatedByJenisBeasiswaId', $join_behavior);

        return $this->getBeasiswaPesertaDidiksRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related BeasiswaPesertaDidiksRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BeasiswaPesertaDidik[] List of BeasiswaPesertaDidik objects
     */
    public function getBeasiswaPesertaDidiksRelatedByPesertaDidikIdJoinJenisBeasiswaRelatedByJenisBeasiswaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BeasiswaPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisBeasiswaRelatedByJenisBeasiswaId', $join_behavior);

        return $this->getBeasiswaPesertaDidiksRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related BeasiswaPesertaDidiksRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BeasiswaPesertaDidik[] List of BeasiswaPesertaDidik objects
     */
    public function getBeasiswaPesertaDidiksRelatedByPesertaDidikIdJoinTahunAjaranRelatedByTahunSelesai($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BeasiswaPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('TahunAjaranRelatedByTahunSelesai', $join_behavior);

        return $this->getBeasiswaPesertaDidiksRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related BeasiswaPesertaDidiksRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BeasiswaPesertaDidik[] List of BeasiswaPesertaDidik objects
     */
    public function getBeasiswaPesertaDidiksRelatedByPesertaDidikIdJoinTahunAjaranRelatedByTahunMulai($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BeasiswaPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('TahunAjaranRelatedByTahunMulai', $join_behavior);

        return $this->getBeasiswaPesertaDidiksRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related BeasiswaPesertaDidiksRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BeasiswaPesertaDidik[] List of BeasiswaPesertaDidik objects
     */
    public function getBeasiswaPesertaDidiksRelatedByPesertaDidikIdJoinTahunAjaranRelatedByTahunSelesai($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BeasiswaPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('TahunAjaranRelatedByTahunSelesai', $join_behavior);

        return $this->getBeasiswaPesertaDidiksRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related BeasiswaPesertaDidiksRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BeasiswaPesertaDidik[] List of BeasiswaPesertaDidik objects
     */
    public function getBeasiswaPesertaDidiksRelatedByPesertaDidikIdJoinTahunAjaranRelatedByTahunMulai($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BeasiswaPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('TahunAjaranRelatedByTahunMulai', $join_behavior);

        return $this->getBeasiswaPesertaDidiksRelatedByPesertaDidikId($query, $con);
    }

    /**
     * Clears out the collRegistrasiPesertaDidiksRelatedByPesertaDidikId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return PesertaDidik The current object (for fluent API support)
     * @see        addRegistrasiPesertaDidiksRelatedByPesertaDidikId()
     */
    public function clearRegistrasiPesertaDidiksRelatedByPesertaDidikId()
    {
        $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId = null; // important to set this to null since that means it is uninitialized
        $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikIdPartial = null;

        return $this;
    }

    /**
     * reset is the collRegistrasiPesertaDidiksRelatedByPesertaDidikId collection loaded partially
     *
     * @return void
     */
    public function resetPartialRegistrasiPesertaDidiksRelatedByPesertaDidikId($v = true)
    {
        $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikIdPartial = $v;
    }

    /**
     * Initializes the collRegistrasiPesertaDidiksRelatedByPesertaDidikId collection.
     *
     * By default this just sets the collRegistrasiPesertaDidiksRelatedByPesertaDidikId collection to an empty array (like clearcollRegistrasiPesertaDidiksRelatedByPesertaDidikId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRegistrasiPesertaDidiksRelatedByPesertaDidikId($overrideExisting = true)
    {
        if (null !== $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId && !$overrideExisting) {
            return;
        }
        $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId = new PropelObjectCollection();
        $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId->setModel('RegistrasiPesertaDidik');
    }

    /**
     * Gets an array of RegistrasiPesertaDidik objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this PesertaDidik is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     * @throws PropelException
     */
    public function getRegistrasiPesertaDidiksRelatedByPesertaDidikId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikIdPartial && !$this->isNew();
        if (null === $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId) {
                // return empty collection
                $this->initRegistrasiPesertaDidiksRelatedByPesertaDidikId();
            } else {
                $collRegistrasiPesertaDidiksRelatedByPesertaDidikId = RegistrasiPesertaDidikQuery::create(null, $criteria)
                    ->filterByPesertaDidikRelatedByPesertaDidikId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikIdPartial && count($collRegistrasiPesertaDidiksRelatedByPesertaDidikId)) {
                      $this->initRegistrasiPesertaDidiksRelatedByPesertaDidikId(false);

                      foreach($collRegistrasiPesertaDidiksRelatedByPesertaDidikId as $obj) {
                        if (false == $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId->contains($obj)) {
                          $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId->append($obj);
                        }
                      }

                      $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikIdPartial = true;
                    }

                    $collRegistrasiPesertaDidiksRelatedByPesertaDidikId->getInternalIterator()->rewind();
                    return $collRegistrasiPesertaDidiksRelatedByPesertaDidikId;
                }

                if($partial && $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId) {
                    foreach($this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId as $obj) {
                        if($obj->isNew()) {
                            $collRegistrasiPesertaDidiksRelatedByPesertaDidikId[] = $obj;
                        }
                    }
                }

                $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId = $collRegistrasiPesertaDidiksRelatedByPesertaDidikId;
                $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikIdPartial = false;
            }
        }

        return $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId;
    }

    /**
     * Sets a collection of RegistrasiPesertaDidikRelatedByPesertaDidikId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $registrasiPesertaDidiksRelatedByPesertaDidikId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setRegistrasiPesertaDidiksRelatedByPesertaDidikId(PropelCollection $registrasiPesertaDidiksRelatedByPesertaDidikId, PropelPDO $con = null)
    {
        $registrasiPesertaDidiksRelatedByPesertaDidikIdToDelete = $this->getRegistrasiPesertaDidiksRelatedByPesertaDidikId(new Criteria(), $con)->diff($registrasiPesertaDidiksRelatedByPesertaDidikId);

        $this->registrasiPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion = unserialize(serialize($registrasiPesertaDidiksRelatedByPesertaDidikIdToDelete));

        foreach ($registrasiPesertaDidiksRelatedByPesertaDidikIdToDelete as $registrasiPesertaDidikRelatedByPesertaDidikIdRemoved) {
            $registrasiPesertaDidikRelatedByPesertaDidikIdRemoved->setPesertaDidikRelatedByPesertaDidikId(null);
        }

        $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId = null;
        foreach ($registrasiPesertaDidiksRelatedByPesertaDidikId as $registrasiPesertaDidikRelatedByPesertaDidikId) {
            $this->addRegistrasiPesertaDidikRelatedByPesertaDidikId($registrasiPesertaDidikRelatedByPesertaDidikId);
        }

        $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId = $registrasiPesertaDidiksRelatedByPesertaDidikId;
        $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RegistrasiPesertaDidik objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RegistrasiPesertaDidik objects.
     * @throws PropelException
     */
    public function countRegistrasiPesertaDidiksRelatedByPesertaDidikId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikIdPartial && !$this->isNew();
        if (null === $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getRegistrasiPesertaDidiksRelatedByPesertaDidikId());
            }
            $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPesertaDidikRelatedByPesertaDidikId($this)
                ->count($con);
        }

        return count($this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId);
    }

    /**
     * Method called to associate a RegistrasiPesertaDidik object to this object
     * through the RegistrasiPesertaDidik foreign key attribute.
     *
     * @param    RegistrasiPesertaDidik $l RegistrasiPesertaDidik
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function addRegistrasiPesertaDidikRelatedByPesertaDidikId(RegistrasiPesertaDidik $l)
    {
        if ($this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId === null) {
            $this->initRegistrasiPesertaDidiksRelatedByPesertaDidikId();
            $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikIdPartial = true;
        }
        if (!in_array($l, $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRegistrasiPesertaDidikRelatedByPesertaDidikId($l);
        }

        return $this;
    }

    /**
     * @param	RegistrasiPesertaDidikRelatedByPesertaDidikId $registrasiPesertaDidikRelatedByPesertaDidikId The registrasiPesertaDidikRelatedByPesertaDidikId object to add.
     */
    protected function doAddRegistrasiPesertaDidikRelatedByPesertaDidikId($registrasiPesertaDidikRelatedByPesertaDidikId)
    {
        $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId[]= $registrasiPesertaDidikRelatedByPesertaDidikId;
        $registrasiPesertaDidikRelatedByPesertaDidikId->setPesertaDidikRelatedByPesertaDidikId($this);
    }

    /**
     * @param	RegistrasiPesertaDidikRelatedByPesertaDidikId $registrasiPesertaDidikRelatedByPesertaDidikId The registrasiPesertaDidikRelatedByPesertaDidikId object to remove.
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function removeRegistrasiPesertaDidikRelatedByPesertaDidikId($registrasiPesertaDidikRelatedByPesertaDidikId)
    {
        if ($this->getRegistrasiPesertaDidiksRelatedByPesertaDidikId()->contains($registrasiPesertaDidikRelatedByPesertaDidikId)) {
            $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId->remove($this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId->search($registrasiPesertaDidikRelatedByPesertaDidikId));
            if (null === $this->registrasiPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion) {
                $this->registrasiPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion = clone $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId;
                $this->registrasiPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion->clear();
            }
            $this->registrasiPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion[]= clone $registrasiPesertaDidikRelatedByPesertaDidikId;
            $registrasiPesertaDidikRelatedByPesertaDidikId->setPesertaDidikRelatedByPesertaDidikId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByPesertaDidikIdJoinJurusanSpRelatedByJurusanSpId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JurusanSpRelatedByJurusanSpId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByPesertaDidikIdJoinJurusanSpRelatedByJurusanSpId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JurusanSpRelatedByJurusanSpId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByPesertaDidikIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByPesertaDidikIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByPesertaDidikIdJoinJenisKeluarRelatedByJenisKeluarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisKeluarRelatedByJenisKeluarId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByPesertaDidikIdJoinJenisKeluarRelatedByJenisKeluarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisKeluarRelatedByJenisKeluarId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByPesertaDidikIdJoinJenisPendaftaranRelatedByJenisPendaftaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisPendaftaranRelatedByJenisPendaftaranId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByPesertaDidikIdJoinJenisPendaftaranRelatedByJenisPendaftaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisPendaftaranRelatedByJenisPendaftaranId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByPesertaDidikId($query, $con);
    }

    /**
     * Clears out the collRegistrasiPesertaDidiksRelatedByPesertaDidikId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return PesertaDidik The current object (for fluent API support)
     * @see        addRegistrasiPesertaDidiksRelatedByPesertaDidikId()
     */
    public function clearRegistrasiPesertaDidiksRelatedByPesertaDidikId()
    {
        $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId = null; // important to set this to null since that means it is uninitialized
        $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikIdPartial = null;

        return $this;
    }

    /**
     * reset is the collRegistrasiPesertaDidiksRelatedByPesertaDidikId collection loaded partially
     *
     * @return void
     */
    public function resetPartialRegistrasiPesertaDidiksRelatedByPesertaDidikId($v = true)
    {
        $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikIdPartial = $v;
    }

    /**
     * Initializes the collRegistrasiPesertaDidiksRelatedByPesertaDidikId collection.
     *
     * By default this just sets the collRegistrasiPesertaDidiksRelatedByPesertaDidikId collection to an empty array (like clearcollRegistrasiPesertaDidiksRelatedByPesertaDidikId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRegistrasiPesertaDidiksRelatedByPesertaDidikId($overrideExisting = true)
    {
        if (null !== $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId && !$overrideExisting) {
            return;
        }
        $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId = new PropelObjectCollection();
        $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId->setModel('RegistrasiPesertaDidik');
    }

    /**
     * Gets an array of RegistrasiPesertaDidik objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this PesertaDidik is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     * @throws PropelException
     */
    public function getRegistrasiPesertaDidiksRelatedByPesertaDidikId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikIdPartial && !$this->isNew();
        if (null === $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId) {
                // return empty collection
                $this->initRegistrasiPesertaDidiksRelatedByPesertaDidikId();
            } else {
                $collRegistrasiPesertaDidiksRelatedByPesertaDidikId = RegistrasiPesertaDidikQuery::create(null, $criteria)
                    ->filterByPesertaDidikRelatedByPesertaDidikId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikIdPartial && count($collRegistrasiPesertaDidiksRelatedByPesertaDidikId)) {
                      $this->initRegistrasiPesertaDidiksRelatedByPesertaDidikId(false);

                      foreach($collRegistrasiPesertaDidiksRelatedByPesertaDidikId as $obj) {
                        if (false == $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId->contains($obj)) {
                          $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId->append($obj);
                        }
                      }

                      $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikIdPartial = true;
                    }

                    $collRegistrasiPesertaDidiksRelatedByPesertaDidikId->getInternalIterator()->rewind();
                    return $collRegistrasiPesertaDidiksRelatedByPesertaDidikId;
                }

                if($partial && $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId) {
                    foreach($this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId as $obj) {
                        if($obj->isNew()) {
                            $collRegistrasiPesertaDidiksRelatedByPesertaDidikId[] = $obj;
                        }
                    }
                }

                $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId = $collRegistrasiPesertaDidiksRelatedByPesertaDidikId;
                $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikIdPartial = false;
            }
        }

        return $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId;
    }

    /**
     * Sets a collection of RegistrasiPesertaDidikRelatedByPesertaDidikId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $registrasiPesertaDidiksRelatedByPesertaDidikId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setRegistrasiPesertaDidiksRelatedByPesertaDidikId(PropelCollection $registrasiPesertaDidiksRelatedByPesertaDidikId, PropelPDO $con = null)
    {
        $registrasiPesertaDidiksRelatedByPesertaDidikIdToDelete = $this->getRegistrasiPesertaDidiksRelatedByPesertaDidikId(new Criteria(), $con)->diff($registrasiPesertaDidiksRelatedByPesertaDidikId);

        $this->registrasiPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion = unserialize(serialize($registrasiPesertaDidiksRelatedByPesertaDidikIdToDelete));

        foreach ($registrasiPesertaDidiksRelatedByPesertaDidikIdToDelete as $registrasiPesertaDidikRelatedByPesertaDidikIdRemoved) {
            $registrasiPesertaDidikRelatedByPesertaDidikIdRemoved->setPesertaDidikRelatedByPesertaDidikId(null);
        }

        $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId = null;
        foreach ($registrasiPesertaDidiksRelatedByPesertaDidikId as $registrasiPesertaDidikRelatedByPesertaDidikId) {
            $this->addRegistrasiPesertaDidikRelatedByPesertaDidikId($registrasiPesertaDidikRelatedByPesertaDidikId);
        }

        $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId = $registrasiPesertaDidiksRelatedByPesertaDidikId;
        $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RegistrasiPesertaDidik objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RegistrasiPesertaDidik objects.
     * @throws PropelException
     */
    public function countRegistrasiPesertaDidiksRelatedByPesertaDidikId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikIdPartial && !$this->isNew();
        if (null === $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getRegistrasiPesertaDidiksRelatedByPesertaDidikId());
            }
            $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPesertaDidikRelatedByPesertaDidikId($this)
                ->count($con);
        }

        return count($this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId);
    }

    /**
     * Method called to associate a RegistrasiPesertaDidik object to this object
     * through the RegistrasiPesertaDidik foreign key attribute.
     *
     * @param    RegistrasiPesertaDidik $l RegistrasiPesertaDidik
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function addRegistrasiPesertaDidikRelatedByPesertaDidikId(RegistrasiPesertaDidik $l)
    {
        if ($this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId === null) {
            $this->initRegistrasiPesertaDidiksRelatedByPesertaDidikId();
            $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikIdPartial = true;
        }
        if (!in_array($l, $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRegistrasiPesertaDidikRelatedByPesertaDidikId($l);
        }

        return $this;
    }

    /**
     * @param	RegistrasiPesertaDidikRelatedByPesertaDidikId $registrasiPesertaDidikRelatedByPesertaDidikId The registrasiPesertaDidikRelatedByPesertaDidikId object to add.
     */
    protected function doAddRegistrasiPesertaDidikRelatedByPesertaDidikId($registrasiPesertaDidikRelatedByPesertaDidikId)
    {
        $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId[]= $registrasiPesertaDidikRelatedByPesertaDidikId;
        $registrasiPesertaDidikRelatedByPesertaDidikId->setPesertaDidikRelatedByPesertaDidikId($this);
    }

    /**
     * @param	RegistrasiPesertaDidikRelatedByPesertaDidikId $registrasiPesertaDidikRelatedByPesertaDidikId The registrasiPesertaDidikRelatedByPesertaDidikId object to remove.
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function removeRegistrasiPesertaDidikRelatedByPesertaDidikId($registrasiPesertaDidikRelatedByPesertaDidikId)
    {
        if ($this->getRegistrasiPesertaDidiksRelatedByPesertaDidikId()->contains($registrasiPesertaDidikRelatedByPesertaDidikId)) {
            $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId->remove($this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId->search($registrasiPesertaDidikRelatedByPesertaDidikId));
            if (null === $this->registrasiPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion) {
                $this->registrasiPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion = clone $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId;
                $this->registrasiPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion->clear();
            }
            $this->registrasiPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion[]= clone $registrasiPesertaDidikRelatedByPesertaDidikId;
            $registrasiPesertaDidikRelatedByPesertaDidikId->setPesertaDidikRelatedByPesertaDidikId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByPesertaDidikIdJoinJurusanSpRelatedByJurusanSpId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JurusanSpRelatedByJurusanSpId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByPesertaDidikIdJoinJurusanSpRelatedByJurusanSpId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JurusanSpRelatedByJurusanSpId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByPesertaDidikIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByPesertaDidikIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByPesertaDidikIdJoinJenisKeluarRelatedByJenisKeluarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisKeluarRelatedByJenisKeluarId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByPesertaDidikIdJoinJenisKeluarRelatedByJenisKeluarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisKeluarRelatedByJenisKeluarId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByPesertaDidikIdJoinJenisPendaftaranRelatedByJenisPendaftaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisPendaftaranRelatedByJenisPendaftaranId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByPesertaDidikIdJoinJenisPendaftaranRelatedByJenisPendaftaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisPendaftaranRelatedByJenisPendaftaranId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByPesertaDidikId($query, $con);
    }

    /**
     * Clears out the collVldPesertaDidiksRelatedByPesertaDidikId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return PesertaDidik The current object (for fluent API support)
     * @see        addVldPesertaDidiksRelatedByPesertaDidikId()
     */
    public function clearVldPesertaDidiksRelatedByPesertaDidikId()
    {
        $this->collVldPesertaDidiksRelatedByPesertaDidikId = null; // important to set this to null since that means it is uninitialized
        $this->collVldPesertaDidiksRelatedByPesertaDidikIdPartial = null;

        return $this;
    }

    /**
     * reset is the collVldPesertaDidiksRelatedByPesertaDidikId collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldPesertaDidiksRelatedByPesertaDidikId($v = true)
    {
        $this->collVldPesertaDidiksRelatedByPesertaDidikIdPartial = $v;
    }

    /**
     * Initializes the collVldPesertaDidiksRelatedByPesertaDidikId collection.
     *
     * By default this just sets the collVldPesertaDidiksRelatedByPesertaDidikId collection to an empty array (like clearcollVldPesertaDidiksRelatedByPesertaDidikId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldPesertaDidiksRelatedByPesertaDidikId($overrideExisting = true)
    {
        if (null !== $this->collVldPesertaDidiksRelatedByPesertaDidikId && !$overrideExisting) {
            return;
        }
        $this->collVldPesertaDidiksRelatedByPesertaDidikId = new PropelObjectCollection();
        $this->collVldPesertaDidiksRelatedByPesertaDidikId->setModel('VldPesertaDidik');
    }

    /**
     * Gets an array of VldPesertaDidik objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this PesertaDidik is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldPesertaDidik[] List of VldPesertaDidik objects
     * @throws PropelException
     */
    public function getVldPesertaDidiksRelatedByPesertaDidikId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldPesertaDidiksRelatedByPesertaDidikIdPartial && !$this->isNew();
        if (null === $this->collVldPesertaDidiksRelatedByPesertaDidikId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldPesertaDidiksRelatedByPesertaDidikId) {
                // return empty collection
                $this->initVldPesertaDidiksRelatedByPesertaDidikId();
            } else {
                $collVldPesertaDidiksRelatedByPesertaDidikId = VldPesertaDidikQuery::create(null, $criteria)
                    ->filterByPesertaDidikRelatedByPesertaDidikId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldPesertaDidiksRelatedByPesertaDidikIdPartial && count($collVldPesertaDidiksRelatedByPesertaDidikId)) {
                      $this->initVldPesertaDidiksRelatedByPesertaDidikId(false);

                      foreach($collVldPesertaDidiksRelatedByPesertaDidikId as $obj) {
                        if (false == $this->collVldPesertaDidiksRelatedByPesertaDidikId->contains($obj)) {
                          $this->collVldPesertaDidiksRelatedByPesertaDidikId->append($obj);
                        }
                      }

                      $this->collVldPesertaDidiksRelatedByPesertaDidikIdPartial = true;
                    }

                    $collVldPesertaDidiksRelatedByPesertaDidikId->getInternalIterator()->rewind();
                    return $collVldPesertaDidiksRelatedByPesertaDidikId;
                }

                if($partial && $this->collVldPesertaDidiksRelatedByPesertaDidikId) {
                    foreach($this->collVldPesertaDidiksRelatedByPesertaDidikId as $obj) {
                        if($obj->isNew()) {
                            $collVldPesertaDidiksRelatedByPesertaDidikId[] = $obj;
                        }
                    }
                }

                $this->collVldPesertaDidiksRelatedByPesertaDidikId = $collVldPesertaDidiksRelatedByPesertaDidikId;
                $this->collVldPesertaDidiksRelatedByPesertaDidikIdPartial = false;
            }
        }

        return $this->collVldPesertaDidiksRelatedByPesertaDidikId;
    }

    /**
     * Sets a collection of VldPesertaDidikRelatedByPesertaDidikId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldPesertaDidiksRelatedByPesertaDidikId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setVldPesertaDidiksRelatedByPesertaDidikId(PropelCollection $vldPesertaDidiksRelatedByPesertaDidikId, PropelPDO $con = null)
    {
        $vldPesertaDidiksRelatedByPesertaDidikIdToDelete = $this->getVldPesertaDidiksRelatedByPesertaDidikId(new Criteria(), $con)->diff($vldPesertaDidiksRelatedByPesertaDidikId);

        $this->vldPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion = unserialize(serialize($vldPesertaDidiksRelatedByPesertaDidikIdToDelete));

        foreach ($vldPesertaDidiksRelatedByPesertaDidikIdToDelete as $vldPesertaDidikRelatedByPesertaDidikIdRemoved) {
            $vldPesertaDidikRelatedByPesertaDidikIdRemoved->setPesertaDidikRelatedByPesertaDidikId(null);
        }

        $this->collVldPesertaDidiksRelatedByPesertaDidikId = null;
        foreach ($vldPesertaDidiksRelatedByPesertaDidikId as $vldPesertaDidikRelatedByPesertaDidikId) {
            $this->addVldPesertaDidikRelatedByPesertaDidikId($vldPesertaDidikRelatedByPesertaDidikId);
        }

        $this->collVldPesertaDidiksRelatedByPesertaDidikId = $vldPesertaDidiksRelatedByPesertaDidikId;
        $this->collVldPesertaDidiksRelatedByPesertaDidikIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldPesertaDidik objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldPesertaDidik objects.
     * @throws PropelException
     */
    public function countVldPesertaDidiksRelatedByPesertaDidikId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldPesertaDidiksRelatedByPesertaDidikIdPartial && !$this->isNew();
        if (null === $this->collVldPesertaDidiksRelatedByPesertaDidikId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldPesertaDidiksRelatedByPesertaDidikId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldPesertaDidiksRelatedByPesertaDidikId());
            }
            $query = VldPesertaDidikQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPesertaDidikRelatedByPesertaDidikId($this)
                ->count($con);
        }

        return count($this->collVldPesertaDidiksRelatedByPesertaDidikId);
    }

    /**
     * Method called to associate a VldPesertaDidik object to this object
     * through the VldPesertaDidik foreign key attribute.
     *
     * @param    VldPesertaDidik $l VldPesertaDidik
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function addVldPesertaDidikRelatedByPesertaDidikId(VldPesertaDidik $l)
    {
        if ($this->collVldPesertaDidiksRelatedByPesertaDidikId === null) {
            $this->initVldPesertaDidiksRelatedByPesertaDidikId();
            $this->collVldPesertaDidiksRelatedByPesertaDidikIdPartial = true;
        }
        if (!in_array($l, $this->collVldPesertaDidiksRelatedByPesertaDidikId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldPesertaDidikRelatedByPesertaDidikId($l);
        }

        return $this;
    }

    /**
     * @param	VldPesertaDidikRelatedByPesertaDidikId $vldPesertaDidikRelatedByPesertaDidikId The vldPesertaDidikRelatedByPesertaDidikId object to add.
     */
    protected function doAddVldPesertaDidikRelatedByPesertaDidikId($vldPesertaDidikRelatedByPesertaDidikId)
    {
        $this->collVldPesertaDidiksRelatedByPesertaDidikId[]= $vldPesertaDidikRelatedByPesertaDidikId;
        $vldPesertaDidikRelatedByPesertaDidikId->setPesertaDidikRelatedByPesertaDidikId($this);
    }

    /**
     * @param	VldPesertaDidikRelatedByPesertaDidikId $vldPesertaDidikRelatedByPesertaDidikId The vldPesertaDidikRelatedByPesertaDidikId object to remove.
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function removeVldPesertaDidikRelatedByPesertaDidikId($vldPesertaDidikRelatedByPesertaDidikId)
    {
        if ($this->getVldPesertaDidiksRelatedByPesertaDidikId()->contains($vldPesertaDidikRelatedByPesertaDidikId)) {
            $this->collVldPesertaDidiksRelatedByPesertaDidikId->remove($this->collVldPesertaDidiksRelatedByPesertaDidikId->search($vldPesertaDidikRelatedByPesertaDidikId));
            if (null === $this->vldPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion) {
                $this->vldPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion = clone $this->collVldPesertaDidiksRelatedByPesertaDidikId;
                $this->vldPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion->clear();
            }
            $this->vldPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion[]= clone $vldPesertaDidikRelatedByPesertaDidikId;
            $vldPesertaDidikRelatedByPesertaDidikId->setPesertaDidikRelatedByPesertaDidikId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related VldPesertaDidiksRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPesertaDidik[] List of VldPesertaDidik objects
     */
    public function getVldPesertaDidiksRelatedByPesertaDidikIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldPesertaDidiksRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related VldPesertaDidiksRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPesertaDidik[] List of VldPesertaDidik objects
     */
    public function getVldPesertaDidiksRelatedByPesertaDidikIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldPesertaDidiksRelatedByPesertaDidikId($query, $con);
    }

    /**
     * Clears out the collVldPesertaDidiksRelatedByPesertaDidikId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return PesertaDidik The current object (for fluent API support)
     * @see        addVldPesertaDidiksRelatedByPesertaDidikId()
     */
    public function clearVldPesertaDidiksRelatedByPesertaDidikId()
    {
        $this->collVldPesertaDidiksRelatedByPesertaDidikId = null; // important to set this to null since that means it is uninitialized
        $this->collVldPesertaDidiksRelatedByPesertaDidikIdPartial = null;

        return $this;
    }

    /**
     * reset is the collVldPesertaDidiksRelatedByPesertaDidikId collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldPesertaDidiksRelatedByPesertaDidikId($v = true)
    {
        $this->collVldPesertaDidiksRelatedByPesertaDidikIdPartial = $v;
    }

    /**
     * Initializes the collVldPesertaDidiksRelatedByPesertaDidikId collection.
     *
     * By default this just sets the collVldPesertaDidiksRelatedByPesertaDidikId collection to an empty array (like clearcollVldPesertaDidiksRelatedByPesertaDidikId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldPesertaDidiksRelatedByPesertaDidikId($overrideExisting = true)
    {
        if (null !== $this->collVldPesertaDidiksRelatedByPesertaDidikId && !$overrideExisting) {
            return;
        }
        $this->collVldPesertaDidiksRelatedByPesertaDidikId = new PropelObjectCollection();
        $this->collVldPesertaDidiksRelatedByPesertaDidikId->setModel('VldPesertaDidik');
    }

    /**
     * Gets an array of VldPesertaDidik objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this PesertaDidik is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldPesertaDidik[] List of VldPesertaDidik objects
     * @throws PropelException
     */
    public function getVldPesertaDidiksRelatedByPesertaDidikId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldPesertaDidiksRelatedByPesertaDidikIdPartial && !$this->isNew();
        if (null === $this->collVldPesertaDidiksRelatedByPesertaDidikId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldPesertaDidiksRelatedByPesertaDidikId) {
                // return empty collection
                $this->initVldPesertaDidiksRelatedByPesertaDidikId();
            } else {
                $collVldPesertaDidiksRelatedByPesertaDidikId = VldPesertaDidikQuery::create(null, $criteria)
                    ->filterByPesertaDidikRelatedByPesertaDidikId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldPesertaDidiksRelatedByPesertaDidikIdPartial && count($collVldPesertaDidiksRelatedByPesertaDidikId)) {
                      $this->initVldPesertaDidiksRelatedByPesertaDidikId(false);

                      foreach($collVldPesertaDidiksRelatedByPesertaDidikId as $obj) {
                        if (false == $this->collVldPesertaDidiksRelatedByPesertaDidikId->contains($obj)) {
                          $this->collVldPesertaDidiksRelatedByPesertaDidikId->append($obj);
                        }
                      }

                      $this->collVldPesertaDidiksRelatedByPesertaDidikIdPartial = true;
                    }

                    $collVldPesertaDidiksRelatedByPesertaDidikId->getInternalIterator()->rewind();
                    return $collVldPesertaDidiksRelatedByPesertaDidikId;
                }

                if($partial && $this->collVldPesertaDidiksRelatedByPesertaDidikId) {
                    foreach($this->collVldPesertaDidiksRelatedByPesertaDidikId as $obj) {
                        if($obj->isNew()) {
                            $collVldPesertaDidiksRelatedByPesertaDidikId[] = $obj;
                        }
                    }
                }

                $this->collVldPesertaDidiksRelatedByPesertaDidikId = $collVldPesertaDidiksRelatedByPesertaDidikId;
                $this->collVldPesertaDidiksRelatedByPesertaDidikIdPartial = false;
            }
        }

        return $this->collVldPesertaDidiksRelatedByPesertaDidikId;
    }

    /**
     * Sets a collection of VldPesertaDidikRelatedByPesertaDidikId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldPesertaDidiksRelatedByPesertaDidikId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setVldPesertaDidiksRelatedByPesertaDidikId(PropelCollection $vldPesertaDidiksRelatedByPesertaDidikId, PropelPDO $con = null)
    {
        $vldPesertaDidiksRelatedByPesertaDidikIdToDelete = $this->getVldPesertaDidiksRelatedByPesertaDidikId(new Criteria(), $con)->diff($vldPesertaDidiksRelatedByPesertaDidikId);

        $this->vldPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion = unserialize(serialize($vldPesertaDidiksRelatedByPesertaDidikIdToDelete));

        foreach ($vldPesertaDidiksRelatedByPesertaDidikIdToDelete as $vldPesertaDidikRelatedByPesertaDidikIdRemoved) {
            $vldPesertaDidikRelatedByPesertaDidikIdRemoved->setPesertaDidikRelatedByPesertaDidikId(null);
        }

        $this->collVldPesertaDidiksRelatedByPesertaDidikId = null;
        foreach ($vldPesertaDidiksRelatedByPesertaDidikId as $vldPesertaDidikRelatedByPesertaDidikId) {
            $this->addVldPesertaDidikRelatedByPesertaDidikId($vldPesertaDidikRelatedByPesertaDidikId);
        }

        $this->collVldPesertaDidiksRelatedByPesertaDidikId = $vldPesertaDidiksRelatedByPesertaDidikId;
        $this->collVldPesertaDidiksRelatedByPesertaDidikIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldPesertaDidik objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldPesertaDidik objects.
     * @throws PropelException
     */
    public function countVldPesertaDidiksRelatedByPesertaDidikId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldPesertaDidiksRelatedByPesertaDidikIdPartial && !$this->isNew();
        if (null === $this->collVldPesertaDidiksRelatedByPesertaDidikId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldPesertaDidiksRelatedByPesertaDidikId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldPesertaDidiksRelatedByPesertaDidikId());
            }
            $query = VldPesertaDidikQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPesertaDidikRelatedByPesertaDidikId($this)
                ->count($con);
        }

        return count($this->collVldPesertaDidiksRelatedByPesertaDidikId);
    }

    /**
     * Method called to associate a VldPesertaDidik object to this object
     * through the VldPesertaDidik foreign key attribute.
     *
     * @param    VldPesertaDidik $l VldPesertaDidik
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function addVldPesertaDidikRelatedByPesertaDidikId(VldPesertaDidik $l)
    {
        if ($this->collVldPesertaDidiksRelatedByPesertaDidikId === null) {
            $this->initVldPesertaDidiksRelatedByPesertaDidikId();
            $this->collVldPesertaDidiksRelatedByPesertaDidikIdPartial = true;
        }
        if (!in_array($l, $this->collVldPesertaDidiksRelatedByPesertaDidikId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldPesertaDidikRelatedByPesertaDidikId($l);
        }

        return $this;
    }

    /**
     * @param	VldPesertaDidikRelatedByPesertaDidikId $vldPesertaDidikRelatedByPesertaDidikId The vldPesertaDidikRelatedByPesertaDidikId object to add.
     */
    protected function doAddVldPesertaDidikRelatedByPesertaDidikId($vldPesertaDidikRelatedByPesertaDidikId)
    {
        $this->collVldPesertaDidiksRelatedByPesertaDidikId[]= $vldPesertaDidikRelatedByPesertaDidikId;
        $vldPesertaDidikRelatedByPesertaDidikId->setPesertaDidikRelatedByPesertaDidikId($this);
    }

    /**
     * @param	VldPesertaDidikRelatedByPesertaDidikId $vldPesertaDidikRelatedByPesertaDidikId The vldPesertaDidikRelatedByPesertaDidikId object to remove.
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function removeVldPesertaDidikRelatedByPesertaDidikId($vldPesertaDidikRelatedByPesertaDidikId)
    {
        if ($this->getVldPesertaDidiksRelatedByPesertaDidikId()->contains($vldPesertaDidikRelatedByPesertaDidikId)) {
            $this->collVldPesertaDidiksRelatedByPesertaDidikId->remove($this->collVldPesertaDidiksRelatedByPesertaDidikId->search($vldPesertaDidikRelatedByPesertaDidikId));
            if (null === $this->vldPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion) {
                $this->vldPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion = clone $this->collVldPesertaDidiksRelatedByPesertaDidikId;
                $this->vldPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion->clear();
            }
            $this->vldPesertaDidiksRelatedByPesertaDidikIdScheduledForDeletion[]= clone $vldPesertaDidikRelatedByPesertaDidikId;
            $vldPesertaDidikRelatedByPesertaDidikId->setPesertaDidikRelatedByPesertaDidikId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related VldPesertaDidiksRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPesertaDidik[] List of VldPesertaDidik objects
     */
    public function getVldPesertaDidiksRelatedByPesertaDidikIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldPesertaDidiksRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related VldPesertaDidiksRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPesertaDidik[] List of VldPesertaDidik objects
     */
    public function getVldPesertaDidiksRelatedByPesertaDidikIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldPesertaDidiksRelatedByPesertaDidikId($query, $con);
    }

    /**
     * Clears out the collPesertaDidikBarusRelatedByPesertaDidikId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return PesertaDidik The current object (for fluent API support)
     * @see        addPesertaDidikBarusRelatedByPesertaDidikId()
     */
    public function clearPesertaDidikBarusRelatedByPesertaDidikId()
    {
        $this->collPesertaDidikBarusRelatedByPesertaDidikId = null; // important to set this to null since that means it is uninitialized
        $this->collPesertaDidikBarusRelatedByPesertaDidikIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPesertaDidikBarusRelatedByPesertaDidikId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPesertaDidikBarusRelatedByPesertaDidikId($v = true)
    {
        $this->collPesertaDidikBarusRelatedByPesertaDidikIdPartial = $v;
    }

    /**
     * Initializes the collPesertaDidikBarusRelatedByPesertaDidikId collection.
     *
     * By default this just sets the collPesertaDidikBarusRelatedByPesertaDidikId collection to an empty array (like clearcollPesertaDidikBarusRelatedByPesertaDidikId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPesertaDidikBarusRelatedByPesertaDidikId($overrideExisting = true)
    {
        if (null !== $this->collPesertaDidikBarusRelatedByPesertaDidikId && !$overrideExisting) {
            return;
        }
        $this->collPesertaDidikBarusRelatedByPesertaDidikId = new PropelObjectCollection();
        $this->collPesertaDidikBarusRelatedByPesertaDidikId->setModel('PesertaDidikBaru');
    }

    /**
     * Gets an array of PesertaDidikBaru objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this PesertaDidik is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|PesertaDidikBaru[] List of PesertaDidikBaru objects
     * @throws PropelException
     */
    public function getPesertaDidikBarusRelatedByPesertaDidikId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPesertaDidikBarusRelatedByPesertaDidikIdPartial && !$this->isNew();
        if (null === $this->collPesertaDidikBarusRelatedByPesertaDidikId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPesertaDidikBarusRelatedByPesertaDidikId) {
                // return empty collection
                $this->initPesertaDidikBarusRelatedByPesertaDidikId();
            } else {
                $collPesertaDidikBarusRelatedByPesertaDidikId = PesertaDidikBaruQuery::create(null, $criteria)
                    ->filterByPesertaDidikRelatedByPesertaDidikId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPesertaDidikBarusRelatedByPesertaDidikIdPartial && count($collPesertaDidikBarusRelatedByPesertaDidikId)) {
                      $this->initPesertaDidikBarusRelatedByPesertaDidikId(false);

                      foreach($collPesertaDidikBarusRelatedByPesertaDidikId as $obj) {
                        if (false == $this->collPesertaDidikBarusRelatedByPesertaDidikId->contains($obj)) {
                          $this->collPesertaDidikBarusRelatedByPesertaDidikId->append($obj);
                        }
                      }

                      $this->collPesertaDidikBarusRelatedByPesertaDidikIdPartial = true;
                    }

                    $collPesertaDidikBarusRelatedByPesertaDidikId->getInternalIterator()->rewind();
                    return $collPesertaDidikBarusRelatedByPesertaDidikId;
                }

                if($partial && $this->collPesertaDidikBarusRelatedByPesertaDidikId) {
                    foreach($this->collPesertaDidikBarusRelatedByPesertaDidikId as $obj) {
                        if($obj->isNew()) {
                            $collPesertaDidikBarusRelatedByPesertaDidikId[] = $obj;
                        }
                    }
                }

                $this->collPesertaDidikBarusRelatedByPesertaDidikId = $collPesertaDidikBarusRelatedByPesertaDidikId;
                $this->collPesertaDidikBarusRelatedByPesertaDidikIdPartial = false;
            }
        }

        return $this->collPesertaDidikBarusRelatedByPesertaDidikId;
    }

    /**
     * Sets a collection of PesertaDidikBaruRelatedByPesertaDidikId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $pesertaDidikBarusRelatedByPesertaDidikId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setPesertaDidikBarusRelatedByPesertaDidikId(PropelCollection $pesertaDidikBarusRelatedByPesertaDidikId, PropelPDO $con = null)
    {
        $pesertaDidikBarusRelatedByPesertaDidikIdToDelete = $this->getPesertaDidikBarusRelatedByPesertaDidikId(new Criteria(), $con)->diff($pesertaDidikBarusRelatedByPesertaDidikId);

        $this->pesertaDidikBarusRelatedByPesertaDidikIdScheduledForDeletion = unserialize(serialize($pesertaDidikBarusRelatedByPesertaDidikIdToDelete));

        foreach ($pesertaDidikBarusRelatedByPesertaDidikIdToDelete as $pesertaDidikBaruRelatedByPesertaDidikIdRemoved) {
            $pesertaDidikBaruRelatedByPesertaDidikIdRemoved->setPesertaDidikRelatedByPesertaDidikId(null);
        }

        $this->collPesertaDidikBarusRelatedByPesertaDidikId = null;
        foreach ($pesertaDidikBarusRelatedByPesertaDidikId as $pesertaDidikBaruRelatedByPesertaDidikId) {
            $this->addPesertaDidikBaruRelatedByPesertaDidikId($pesertaDidikBaruRelatedByPesertaDidikId);
        }

        $this->collPesertaDidikBarusRelatedByPesertaDidikId = $pesertaDidikBarusRelatedByPesertaDidikId;
        $this->collPesertaDidikBarusRelatedByPesertaDidikIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PesertaDidikBaru objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related PesertaDidikBaru objects.
     * @throws PropelException
     */
    public function countPesertaDidikBarusRelatedByPesertaDidikId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPesertaDidikBarusRelatedByPesertaDidikIdPartial && !$this->isNew();
        if (null === $this->collPesertaDidikBarusRelatedByPesertaDidikId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPesertaDidikBarusRelatedByPesertaDidikId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPesertaDidikBarusRelatedByPesertaDidikId());
            }
            $query = PesertaDidikBaruQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPesertaDidikRelatedByPesertaDidikId($this)
                ->count($con);
        }

        return count($this->collPesertaDidikBarusRelatedByPesertaDidikId);
    }

    /**
     * Method called to associate a PesertaDidikBaru object to this object
     * through the PesertaDidikBaru foreign key attribute.
     *
     * @param    PesertaDidikBaru $l PesertaDidikBaru
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function addPesertaDidikBaruRelatedByPesertaDidikId(PesertaDidikBaru $l)
    {
        if ($this->collPesertaDidikBarusRelatedByPesertaDidikId === null) {
            $this->initPesertaDidikBarusRelatedByPesertaDidikId();
            $this->collPesertaDidikBarusRelatedByPesertaDidikIdPartial = true;
        }
        if (!in_array($l, $this->collPesertaDidikBarusRelatedByPesertaDidikId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPesertaDidikBaruRelatedByPesertaDidikId($l);
        }

        return $this;
    }

    /**
     * @param	PesertaDidikBaruRelatedByPesertaDidikId $pesertaDidikBaruRelatedByPesertaDidikId The pesertaDidikBaruRelatedByPesertaDidikId object to add.
     */
    protected function doAddPesertaDidikBaruRelatedByPesertaDidikId($pesertaDidikBaruRelatedByPesertaDidikId)
    {
        $this->collPesertaDidikBarusRelatedByPesertaDidikId[]= $pesertaDidikBaruRelatedByPesertaDidikId;
        $pesertaDidikBaruRelatedByPesertaDidikId->setPesertaDidikRelatedByPesertaDidikId($this);
    }

    /**
     * @param	PesertaDidikBaruRelatedByPesertaDidikId $pesertaDidikBaruRelatedByPesertaDidikId The pesertaDidikBaruRelatedByPesertaDidikId object to remove.
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function removePesertaDidikBaruRelatedByPesertaDidikId($pesertaDidikBaruRelatedByPesertaDidikId)
    {
        if ($this->getPesertaDidikBarusRelatedByPesertaDidikId()->contains($pesertaDidikBaruRelatedByPesertaDidikId)) {
            $this->collPesertaDidikBarusRelatedByPesertaDidikId->remove($this->collPesertaDidikBarusRelatedByPesertaDidikId->search($pesertaDidikBaruRelatedByPesertaDidikId));
            if (null === $this->pesertaDidikBarusRelatedByPesertaDidikIdScheduledForDeletion) {
                $this->pesertaDidikBarusRelatedByPesertaDidikIdScheduledForDeletion = clone $this->collPesertaDidikBarusRelatedByPesertaDidikId;
                $this->pesertaDidikBarusRelatedByPesertaDidikIdScheduledForDeletion->clear();
            }
            $this->pesertaDidikBarusRelatedByPesertaDidikIdScheduledForDeletion[]= $pesertaDidikBaruRelatedByPesertaDidikId;
            $pesertaDidikBaruRelatedByPesertaDidikId->setPesertaDidikRelatedByPesertaDidikId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related PesertaDidikBarusRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidikBaru[] List of PesertaDidikBaru objects
     */
    public function getPesertaDidikBarusRelatedByPesertaDidikIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikBaruQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPesertaDidikBarusRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related PesertaDidikBarusRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidikBaru[] List of PesertaDidikBaru objects
     */
    public function getPesertaDidikBarusRelatedByPesertaDidikIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikBaruQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPesertaDidikBarusRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related PesertaDidikBarusRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidikBaru[] List of PesertaDidikBaru objects
     */
    public function getPesertaDidikBarusRelatedByPesertaDidikIdJoinJenisPendaftaranRelatedByJenisPendaftaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikBaruQuery::create(null, $criteria);
        $query->joinWith('JenisPendaftaranRelatedByJenisPendaftaranId', $join_behavior);

        return $this->getPesertaDidikBarusRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related PesertaDidikBarusRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidikBaru[] List of PesertaDidikBaru objects
     */
    public function getPesertaDidikBarusRelatedByPesertaDidikIdJoinJenisPendaftaranRelatedByJenisPendaftaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikBaruQuery::create(null, $criteria);
        $query->joinWith('JenisPendaftaranRelatedByJenisPendaftaranId', $join_behavior);

        return $this->getPesertaDidikBarusRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related PesertaDidikBarusRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidikBaru[] List of PesertaDidikBaru objects
     */
    public function getPesertaDidikBarusRelatedByPesertaDidikIdJoinTahunAjaranRelatedByTahunAjaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikBaruQuery::create(null, $criteria);
        $query->joinWith('TahunAjaranRelatedByTahunAjaranId', $join_behavior);

        return $this->getPesertaDidikBarusRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related PesertaDidikBarusRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidikBaru[] List of PesertaDidikBaru objects
     */
    public function getPesertaDidikBarusRelatedByPesertaDidikIdJoinTahunAjaranRelatedByTahunAjaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikBaruQuery::create(null, $criteria);
        $query->joinWith('TahunAjaranRelatedByTahunAjaranId', $join_behavior);

        return $this->getPesertaDidikBarusRelatedByPesertaDidikId($query, $con);
    }

    /**
     * Clears out the collPesertaDidikBarusRelatedByPesertaDidikId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return PesertaDidik The current object (for fluent API support)
     * @see        addPesertaDidikBarusRelatedByPesertaDidikId()
     */
    public function clearPesertaDidikBarusRelatedByPesertaDidikId()
    {
        $this->collPesertaDidikBarusRelatedByPesertaDidikId = null; // important to set this to null since that means it is uninitialized
        $this->collPesertaDidikBarusRelatedByPesertaDidikIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPesertaDidikBarusRelatedByPesertaDidikId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPesertaDidikBarusRelatedByPesertaDidikId($v = true)
    {
        $this->collPesertaDidikBarusRelatedByPesertaDidikIdPartial = $v;
    }

    /**
     * Initializes the collPesertaDidikBarusRelatedByPesertaDidikId collection.
     *
     * By default this just sets the collPesertaDidikBarusRelatedByPesertaDidikId collection to an empty array (like clearcollPesertaDidikBarusRelatedByPesertaDidikId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPesertaDidikBarusRelatedByPesertaDidikId($overrideExisting = true)
    {
        if (null !== $this->collPesertaDidikBarusRelatedByPesertaDidikId && !$overrideExisting) {
            return;
        }
        $this->collPesertaDidikBarusRelatedByPesertaDidikId = new PropelObjectCollection();
        $this->collPesertaDidikBarusRelatedByPesertaDidikId->setModel('PesertaDidikBaru');
    }

    /**
     * Gets an array of PesertaDidikBaru objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this PesertaDidik is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|PesertaDidikBaru[] List of PesertaDidikBaru objects
     * @throws PropelException
     */
    public function getPesertaDidikBarusRelatedByPesertaDidikId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPesertaDidikBarusRelatedByPesertaDidikIdPartial && !$this->isNew();
        if (null === $this->collPesertaDidikBarusRelatedByPesertaDidikId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPesertaDidikBarusRelatedByPesertaDidikId) {
                // return empty collection
                $this->initPesertaDidikBarusRelatedByPesertaDidikId();
            } else {
                $collPesertaDidikBarusRelatedByPesertaDidikId = PesertaDidikBaruQuery::create(null, $criteria)
                    ->filterByPesertaDidikRelatedByPesertaDidikId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPesertaDidikBarusRelatedByPesertaDidikIdPartial && count($collPesertaDidikBarusRelatedByPesertaDidikId)) {
                      $this->initPesertaDidikBarusRelatedByPesertaDidikId(false);

                      foreach($collPesertaDidikBarusRelatedByPesertaDidikId as $obj) {
                        if (false == $this->collPesertaDidikBarusRelatedByPesertaDidikId->contains($obj)) {
                          $this->collPesertaDidikBarusRelatedByPesertaDidikId->append($obj);
                        }
                      }

                      $this->collPesertaDidikBarusRelatedByPesertaDidikIdPartial = true;
                    }

                    $collPesertaDidikBarusRelatedByPesertaDidikId->getInternalIterator()->rewind();
                    return $collPesertaDidikBarusRelatedByPesertaDidikId;
                }

                if($partial && $this->collPesertaDidikBarusRelatedByPesertaDidikId) {
                    foreach($this->collPesertaDidikBarusRelatedByPesertaDidikId as $obj) {
                        if($obj->isNew()) {
                            $collPesertaDidikBarusRelatedByPesertaDidikId[] = $obj;
                        }
                    }
                }

                $this->collPesertaDidikBarusRelatedByPesertaDidikId = $collPesertaDidikBarusRelatedByPesertaDidikId;
                $this->collPesertaDidikBarusRelatedByPesertaDidikIdPartial = false;
            }
        }

        return $this->collPesertaDidikBarusRelatedByPesertaDidikId;
    }

    /**
     * Sets a collection of PesertaDidikBaruRelatedByPesertaDidikId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $pesertaDidikBarusRelatedByPesertaDidikId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function setPesertaDidikBarusRelatedByPesertaDidikId(PropelCollection $pesertaDidikBarusRelatedByPesertaDidikId, PropelPDO $con = null)
    {
        $pesertaDidikBarusRelatedByPesertaDidikIdToDelete = $this->getPesertaDidikBarusRelatedByPesertaDidikId(new Criteria(), $con)->diff($pesertaDidikBarusRelatedByPesertaDidikId);

        $this->pesertaDidikBarusRelatedByPesertaDidikIdScheduledForDeletion = unserialize(serialize($pesertaDidikBarusRelatedByPesertaDidikIdToDelete));

        foreach ($pesertaDidikBarusRelatedByPesertaDidikIdToDelete as $pesertaDidikBaruRelatedByPesertaDidikIdRemoved) {
            $pesertaDidikBaruRelatedByPesertaDidikIdRemoved->setPesertaDidikRelatedByPesertaDidikId(null);
        }

        $this->collPesertaDidikBarusRelatedByPesertaDidikId = null;
        foreach ($pesertaDidikBarusRelatedByPesertaDidikId as $pesertaDidikBaruRelatedByPesertaDidikId) {
            $this->addPesertaDidikBaruRelatedByPesertaDidikId($pesertaDidikBaruRelatedByPesertaDidikId);
        }

        $this->collPesertaDidikBarusRelatedByPesertaDidikId = $pesertaDidikBarusRelatedByPesertaDidikId;
        $this->collPesertaDidikBarusRelatedByPesertaDidikIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PesertaDidikBaru objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related PesertaDidikBaru objects.
     * @throws PropelException
     */
    public function countPesertaDidikBarusRelatedByPesertaDidikId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPesertaDidikBarusRelatedByPesertaDidikIdPartial && !$this->isNew();
        if (null === $this->collPesertaDidikBarusRelatedByPesertaDidikId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPesertaDidikBarusRelatedByPesertaDidikId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPesertaDidikBarusRelatedByPesertaDidikId());
            }
            $query = PesertaDidikBaruQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPesertaDidikRelatedByPesertaDidikId($this)
                ->count($con);
        }

        return count($this->collPesertaDidikBarusRelatedByPesertaDidikId);
    }

    /**
     * Method called to associate a PesertaDidikBaru object to this object
     * through the PesertaDidikBaru foreign key attribute.
     *
     * @param    PesertaDidikBaru $l PesertaDidikBaru
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function addPesertaDidikBaruRelatedByPesertaDidikId(PesertaDidikBaru $l)
    {
        if ($this->collPesertaDidikBarusRelatedByPesertaDidikId === null) {
            $this->initPesertaDidikBarusRelatedByPesertaDidikId();
            $this->collPesertaDidikBarusRelatedByPesertaDidikIdPartial = true;
        }
        if (!in_array($l, $this->collPesertaDidikBarusRelatedByPesertaDidikId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPesertaDidikBaruRelatedByPesertaDidikId($l);
        }

        return $this;
    }

    /**
     * @param	PesertaDidikBaruRelatedByPesertaDidikId $pesertaDidikBaruRelatedByPesertaDidikId The pesertaDidikBaruRelatedByPesertaDidikId object to add.
     */
    protected function doAddPesertaDidikBaruRelatedByPesertaDidikId($pesertaDidikBaruRelatedByPesertaDidikId)
    {
        $this->collPesertaDidikBarusRelatedByPesertaDidikId[]= $pesertaDidikBaruRelatedByPesertaDidikId;
        $pesertaDidikBaruRelatedByPesertaDidikId->setPesertaDidikRelatedByPesertaDidikId($this);
    }

    /**
     * @param	PesertaDidikBaruRelatedByPesertaDidikId $pesertaDidikBaruRelatedByPesertaDidikId The pesertaDidikBaruRelatedByPesertaDidikId object to remove.
     * @return PesertaDidik The current object (for fluent API support)
     */
    public function removePesertaDidikBaruRelatedByPesertaDidikId($pesertaDidikBaruRelatedByPesertaDidikId)
    {
        if ($this->getPesertaDidikBarusRelatedByPesertaDidikId()->contains($pesertaDidikBaruRelatedByPesertaDidikId)) {
            $this->collPesertaDidikBarusRelatedByPesertaDidikId->remove($this->collPesertaDidikBarusRelatedByPesertaDidikId->search($pesertaDidikBaruRelatedByPesertaDidikId));
            if (null === $this->pesertaDidikBarusRelatedByPesertaDidikIdScheduledForDeletion) {
                $this->pesertaDidikBarusRelatedByPesertaDidikIdScheduledForDeletion = clone $this->collPesertaDidikBarusRelatedByPesertaDidikId;
                $this->pesertaDidikBarusRelatedByPesertaDidikIdScheduledForDeletion->clear();
            }
            $this->pesertaDidikBarusRelatedByPesertaDidikIdScheduledForDeletion[]= $pesertaDidikBaruRelatedByPesertaDidikId;
            $pesertaDidikBaruRelatedByPesertaDidikId->setPesertaDidikRelatedByPesertaDidikId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related PesertaDidikBarusRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidikBaru[] List of PesertaDidikBaru objects
     */
    public function getPesertaDidikBarusRelatedByPesertaDidikIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikBaruQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPesertaDidikBarusRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related PesertaDidikBarusRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidikBaru[] List of PesertaDidikBaru objects
     */
    public function getPesertaDidikBarusRelatedByPesertaDidikIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikBaruQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPesertaDidikBarusRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related PesertaDidikBarusRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidikBaru[] List of PesertaDidikBaru objects
     */
    public function getPesertaDidikBarusRelatedByPesertaDidikIdJoinJenisPendaftaranRelatedByJenisPendaftaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikBaruQuery::create(null, $criteria);
        $query->joinWith('JenisPendaftaranRelatedByJenisPendaftaranId', $join_behavior);

        return $this->getPesertaDidikBarusRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related PesertaDidikBarusRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidikBaru[] List of PesertaDidikBaru objects
     */
    public function getPesertaDidikBarusRelatedByPesertaDidikIdJoinJenisPendaftaranRelatedByJenisPendaftaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikBaruQuery::create(null, $criteria);
        $query->joinWith('JenisPendaftaranRelatedByJenisPendaftaranId', $join_behavior);

        return $this->getPesertaDidikBarusRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related PesertaDidikBarusRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidikBaru[] List of PesertaDidikBaru objects
     */
    public function getPesertaDidikBarusRelatedByPesertaDidikIdJoinTahunAjaranRelatedByTahunAjaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikBaruQuery::create(null, $criteria);
        $query->joinWith('TahunAjaranRelatedByTahunAjaranId', $join_behavior);

        return $this->getPesertaDidikBarusRelatedByPesertaDidikId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PesertaDidik is new, it will return
     * an empty collection; or if this PesertaDidik has previously
     * been saved, it will retrieve related PesertaDidikBarusRelatedByPesertaDidikId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PesertaDidik.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidikBaru[] List of PesertaDidikBaru objects
     */
    public function getPesertaDidikBarusRelatedByPesertaDidikIdJoinTahunAjaranRelatedByTahunAjaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikBaruQuery::create(null, $criteria);
        $query->joinWith('TahunAjaranRelatedByTahunAjaranId', $join_behavior);

        return $this->getPesertaDidikBarusRelatedByPesertaDidikId($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->peserta_didik_id = null;
        $this->nama = null;
        $this->jenis_kelamin = null;
        $this->nisn = null;
        $this->nik = null;
        $this->tempat_lahir = null;
        $this->tanggal_lahir = null;
        $this->agama_id = null;
        $this->kewarganegaraan = null;
        $this->kebutuhan_khusus_id = null;
        $this->sekolah_id = null;
        $this->alamat_jalan = null;
        $this->rt = null;
        $this->rw = null;
        $this->nama_dusun = null;
        $this->desa_kelurahan = null;
        $this->kode_wilayah = null;
        $this->kode_pos = null;
        $this->jenis_tinggal_id = null;
        $this->alat_transportasi_id = null;
        $this->nomor_telepon_rumah = null;
        $this->nomor_telepon_seluler = null;
        $this->email = null;
        $this->penerima_kps = null;
        $this->no_kps = null;
        $this->status_data = null;
        $this->nama_ayah = null;
        $this->tahun_lahir_ayah = null;
        $this->jenjang_pendidikan_ayah = null;
        $this->pekerjaan_id_ayah = null;
        $this->penghasilan_id_ayah = null;
        $this->kebutuhan_khusus_id_ayah = null;
        $this->nama_ibu_kandung = null;
        $this->tahun_lahir_ibu = null;
        $this->jenjang_pendidikan_ibu = null;
        $this->penghasilan_id_ibu = null;
        $this->pekerjaan_id_ibu = null;
        $this->kebutuhan_khusus_id_ibu = null;
        $this->nama_wali = null;
        $this->tahun_lahir_wali = null;
        $this->jenjang_pendidikan_wali = null;
        $this->pekerjaan_id_wali = null;
        $this->penghasilan_id_wali = null;
        $this->last_update = null;
        $this->soft_delete = null;
        $this->last_sync = null;
        $this->updater_id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collPrestasisRelatedByPesertaDidikId) {
                foreach ($this->collPrestasisRelatedByPesertaDidikId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPrestasisRelatedByPesertaDidikId) {
                foreach ($this->collPrestasisRelatedByPesertaDidikId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collAnggotaRombelsRelatedByPesertaDidikId) {
                foreach ($this->collAnggotaRombelsRelatedByPesertaDidikId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collAnggotaRombelsRelatedByPesertaDidikId) {
                foreach ($this->collAnggotaRombelsRelatedByPesertaDidikId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId) {
                foreach ($this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId) {
                foreach ($this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId) {
                foreach ($this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId) {
                foreach ($this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId) {
                foreach ($this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId) {
                foreach ($this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldPesertaDidiksRelatedByPesertaDidikId) {
                foreach ($this->collVldPesertaDidiksRelatedByPesertaDidikId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldPesertaDidiksRelatedByPesertaDidikId) {
                foreach ($this->collVldPesertaDidiksRelatedByPesertaDidikId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPesertaDidikBarusRelatedByPesertaDidikId) {
                foreach ($this->collPesertaDidikBarusRelatedByPesertaDidikId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPesertaDidikBarusRelatedByPesertaDidikId) {
                foreach ($this->collPesertaDidikBarusRelatedByPesertaDidikId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aSekolahRelatedBySekolahId instanceof Persistent) {
              $this->aSekolahRelatedBySekolahId->clearAllReferences($deep);
            }
            if ($this->aSekolahRelatedBySekolahId instanceof Persistent) {
              $this->aSekolahRelatedBySekolahId->clearAllReferences($deep);
            }
            if ($this->aAgamaRelatedByAgamaId instanceof Persistent) {
              $this->aAgamaRelatedByAgamaId->clearAllReferences($deep);
            }
            if ($this->aAgamaRelatedByAgamaId instanceof Persistent) {
              $this->aAgamaRelatedByAgamaId->clearAllReferences($deep);
            }
            if ($this->aAlatTransportasiRelatedByAlatTransportasiId instanceof Persistent) {
              $this->aAlatTransportasiRelatedByAlatTransportasiId->clearAllReferences($deep);
            }
            if ($this->aAlatTransportasiRelatedByAlatTransportasiId instanceof Persistent) {
              $this->aAlatTransportasiRelatedByAlatTransportasiId->clearAllReferences($deep);
            }
            if ($this->aJenisTinggalRelatedByJenisTinggalId instanceof Persistent) {
              $this->aJenisTinggalRelatedByJenisTinggalId->clearAllReferences($deep);
            }
            if ($this->aJenisTinggalRelatedByJenisTinggalId instanceof Persistent) {
              $this->aJenisTinggalRelatedByJenisTinggalId->clearAllReferences($deep);
            }
            if ($this->aJenjangPendidikanRelatedByJenjangPendidikanIbu instanceof Persistent) {
              $this->aJenjangPendidikanRelatedByJenjangPendidikanIbu->clearAllReferences($deep);
            }
            if ($this->aJenjangPendidikanRelatedByJenjangPendidikanAyah instanceof Persistent) {
              $this->aJenjangPendidikanRelatedByJenjangPendidikanAyah->clearAllReferences($deep);
            }
            if ($this->aJenjangPendidikanRelatedByJenjangPendidikanWali instanceof Persistent) {
              $this->aJenjangPendidikanRelatedByJenjangPendidikanWali->clearAllReferences($deep);
            }
            if ($this->aJenjangPendidikanRelatedByJenjangPendidikanIbu instanceof Persistent) {
              $this->aJenjangPendidikanRelatedByJenjangPendidikanIbu->clearAllReferences($deep);
            }
            if ($this->aJenjangPendidikanRelatedByJenjangPendidikanAyah instanceof Persistent) {
              $this->aJenjangPendidikanRelatedByJenjangPendidikanAyah->clearAllReferences($deep);
            }
            if ($this->aJenjangPendidikanRelatedByJenjangPendidikanWali instanceof Persistent) {
              $this->aJenjangPendidikanRelatedByJenjangPendidikanWali->clearAllReferences($deep);
            }
            if ($this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah instanceof Persistent) {
              $this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah->clearAllReferences($deep);
            }
            if ($this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu instanceof Persistent) {
              $this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu->clearAllReferences($deep);
            }
            if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId instanceof Persistent) {
              $this->aKebutuhanKhususRelatedByKebutuhanKhususId->clearAllReferences($deep);
            }
            if ($this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah instanceof Persistent) {
              $this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah->clearAllReferences($deep);
            }
            if ($this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu instanceof Persistent) {
              $this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu->clearAllReferences($deep);
            }
            if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId instanceof Persistent) {
              $this->aKebutuhanKhususRelatedByKebutuhanKhususId->clearAllReferences($deep);
            }
            if ($this->aMstWilayahRelatedByKodeWilayah instanceof Persistent) {
              $this->aMstWilayahRelatedByKodeWilayah->clearAllReferences($deep);
            }
            if ($this->aMstWilayahRelatedByKodeWilayah instanceof Persistent) {
              $this->aMstWilayahRelatedByKodeWilayah->clearAllReferences($deep);
            }
            if ($this->aNegaraRelatedByKewarganegaraan instanceof Persistent) {
              $this->aNegaraRelatedByKewarganegaraan->clearAllReferences($deep);
            }
            if ($this->aNegaraRelatedByKewarganegaraan instanceof Persistent) {
              $this->aNegaraRelatedByKewarganegaraan->clearAllReferences($deep);
            }
            if ($this->aPekerjaanRelatedByPekerjaanIdAyah instanceof Persistent) {
              $this->aPekerjaanRelatedByPekerjaanIdAyah->clearAllReferences($deep);
            }
            if ($this->aPekerjaanRelatedByPekerjaanIdIbu instanceof Persistent) {
              $this->aPekerjaanRelatedByPekerjaanIdIbu->clearAllReferences($deep);
            }
            if ($this->aPekerjaanRelatedByPekerjaanIdWali instanceof Persistent) {
              $this->aPekerjaanRelatedByPekerjaanIdWali->clearAllReferences($deep);
            }
            if ($this->aPekerjaanRelatedByPekerjaanIdAyah instanceof Persistent) {
              $this->aPekerjaanRelatedByPekerjaanIdAyah->clearAllReferences($deep);
            }
            if ($this->aPekerjaanRelatedByPekerjaanIdIbu instanceof Persistent) {
              $this->aPekerjaanRelatedByPekerjaanIdIbu->clearAllReferences($deep);
            }
            if ($this->aPekerjaanRelatedByPekerjaanIdWali instanceof Persistent) {
              $this->aPekerjaanRelatedByPekerjaanIdWali->clearAllReferences($deep);
            }
            if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah instanceof Persistent) {
              $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah->clearAllReferences($deep);
            }
            if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali instanceof Persistent) {
              $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali->clearAllReferences($deep);
            }
            if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu instanceof Persistent) {
              $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu->clearAllReferences($deep);
            }
            if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah instanceof Persistent) {
              $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah->clearAllReferences($deep);
            }
            if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali instanceof Persistent) {
              $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali->clearAllReferences($deep);
            }
            if ($this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu instanceof Persistent) {
              $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collPrestasisRelatedByPesertaDidikId instanceof PropelCollection) {
            $this->collPrestasisRelatedByPesertaDidikId->clearIterator();
        }
        $this->collPrestasisRelatedByPesertaDidikId = null;
        if ($this->collPrestasisRelatedByPesertaDidikId instanceof PropelCollection) {
            $this->collPrestasisRelatedByPesertaDidikId->clearIterator();
        }
        $this->collPrestasisRelatedByPesertaDidikId = null;
        if ($this->collAnggotaRombelsRelatedByPesertaDidikId instanceof PropelCollection) {
            $this->collAnggotaRombelsRelatedByPesertaDidikId->clearIterator();
        }
        $this->collAnggotaRombelsRelatedByPesertaDidikId = null;
        if ($this->collAnggotaRombelsRelatedByPesertaDidikId instanceof PropelCollection) {
            $this->collAnggotaRombelsRelatedByPesertaDidikId->clearIterator();
        }
        $this->collAnggotaRombelsRelatedByPesertaDidikId = null;
        if ($this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId instanceof PropelCollection) {
            $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId->clearIterator();
        }
        $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId = null;
        if ($this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId instanceof PropelCollection) {
            $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId->clearIterator();
        }
        $this->collPesertaDidikLongitudinalsRelatedByPesertaDidikId = null;
        if ($this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId instanceof PropelCollection) {
            $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId->clearIterator();
        }
        $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId = null;
        if ($this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId instanceof PropelCollection) {
            $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId->clearIterator();
        }
        $this->collBeasiswaPesertaDidiksRelatedByPesertaDidikId = null;
        if ($this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId instanceof PropelCollection) {
            $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId->clearIterator();
        }
        $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId = null;
        if ($this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId instanceof PropelCollection) {
            $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId->clearIterator();
        }
        $this->collRegistrasiPesertaDidiksRelatedByPesertaDidikId = null;
        if ($this->collVldPesertaDidiksRelatedByPesertaDidikId instanceof PropelCollection) {
            $this->collVldPesertaDidiksRelatedByPesertaDidikId->clearIterator();
        }
        $this->collVldPesertaDidiksRelatedByPesertaDidikId = null;
        if ($this->collVldPesertaDidiksRelatedByPesertaDidikId instanceof PropelCollection) {
            $this->collVldPesertaDidiksRelatedByPesertaDidikId->clearIterator();
        }
        $this->collVldPesertaDidiksRelatedByPesertaDidikId = null;
        if ($this->collPesertaDidikBarusRelatedByPesertaDidikId instanceof PropelCollection) {
            $this->collPesertaDidikBarusRelatedByPesertaDidikId->clearIterator();
        }
        $this->collPesertaDidikBarusRelatedByPesertaDidikId = null;
        if ($this->collPesertaDidikBarusRelatedByPesertaDidikId instanceof PropelCollection) {
            $this->collPesertaDidikBarusRelatedByPesertaDidikId->clearIterator();
        }
        $this->collPesertaDidikBarusRelatedByPesertaDidikId = null;
        $this->aSekolahRelatedBySekolahId = null;
        $this->aSekolahRelatedBySekolahId = null;
        $this->aAgamaRelatedByAgamaId = null;
        $this->aAgamaRelatedByAgamaId = null;
        $this->aAlatTransportasiRelatedByAlatTransportasiId = null;
        $this->aAlatTransportasiRelatedByAlatTransportasiId = null;
        $this->aJenisTinggalRelatedByJenisTinggalId = null;
        $this->aJenisTinggalRelatedByJenisTinggalId = null;
        $this->aJenjangPendidikanRelatedByJenjangPendidikanIbu = null;
        $this->aJenjangPendidikanRelatedByJenjangPendidikanAyah = null;
        $this->aJenjangPendidikanRelatedByJenjangPendidikanWali = null;
        $this->aJenjangPendidikanRelatedByJenjangPendidikanIbu = null;
        $this->aJenjangPendidikanRelatedByJenjangPendidikanAyah = null;
        $this->aJenjangPendidikanRelatedByJenjangPendidikanWali = null;
        $this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah = null;
        $this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu = null;
        $this->aKebutuhanKhususRelatedByKebutuhanKhususId = null;
        $this->aKebutuhanKhususRelatedByKebutuhanKhususIdAyah = null;
        $this->aKebutuhanKhususRelatedByKebutuhanKhususIdIbu = null;
        $this->aKebutuhanKhususRelatedByKebutuhanKhususId = null;
        $this->aMstWilayahRelatedByKodeWilayah = null;
        $this->aMstWilayahRelatedByKodeWilayah = null;
        $this->aNegaraRelatedByKewarganegaraan = null;
        $this->aNegaraRelatedByKewarganegaraan = null;
        $this->aPekerjaanRelatedByPekerjaanIdAyah = null;
        $this->aPekerjaanRelatedByPekerjaanIdIbu = null;
        $this->aPekerjaanRelatedByPekerjaanIdWali = null;
        $this->aPekerjaanRelatedByPekerjaanIdAyah = null;
        $this->aPekerjaanRelatedByPekerjaanIdIbu = null;
        $this->aPekerjaanRelatedByPekerjaanIdWali = null;
        $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah = null;
        $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali = null;
        $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu = null;
        $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah = null;
        $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali = null;
        $this->aPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(PesertaDidikPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
