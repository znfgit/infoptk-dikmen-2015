<?php

namespace angulex\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelDateTime;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\BukuAlat;
use angulex\Model\BukuAlatLongitudinal;
use angulex\Model\BukuAlatLongitudinalQuery;
use angulex\Model\BukuAlatPeer;
use angulex\Model\BukuAlatQuery;
use angulex\Model\JenisBukuAlat;
use angulex\Model\JenisBukuAlatQuery;
use angulex\Model\MataPelajaran;
use angulex\Model\MataPelajaranQuery;
use angulex\Model\Prasarana;
use angulex\Model\PrasaranaQuery;
use angulex\Model\Sekolah;
use angulex\Model\SekolahQuery;
use angulex\Model\TingkatPendidikan;
use angulex\Model\TingkatPendidikanQuery;

/**
 * Base class that represents a row from the 'buku_alat' table.
 *
 * 
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseBukuAlat extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'angulex\\Model\\BukuAlatPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        BukuAlatPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the buku_alat_id field.
     * @var        string
     */
    protected $buku_alat_id;

    /**
     * The value for the mata_pelajaran_id field.
     * @var        int
     */
    protected $mata_pelajaran_id;

    /**
     * The value for the prasarana_id field.
     * @var        string
     */
    protected $prasarana_id;

    /**
     * The value for the sekolah_id field.
     * @var        string
     */
    protected $sekolah_id;

    /**
     * The value for the tingkat_pendidikan_id field.
     * @var        string
     */
    protected $tingkat_pendidikan_id;

    /**
     * The value for the jenis_buku_alat_id field.
     * @var        string
     */
    protected $jenis_buku_alat_id;

    /**
     * The value for the buku_alat field.
     * @var        string
     */
    protected $buku_alat;

    /**
     * The value for the last_update field.
     * @var        string
     */
    protected $last_update;

    /**
     * The value for the soft_delete field.
     * @var        string
     */
    protected $soft_delete;

    /**
     * The value for the last_sync field.
     * @var        string
     */
    protected $last_sync;

    /**
     * The value for the updater_id field.
     * @var        string
     */
    protected $updater_id;

    /**
     * @var        Prasarana
     */
    protected $aPrasaranaRelatedByPrasaranaId;

    /**
     * @var        Prasarana
     */
    protected $aPrasaranaRelatedByPrasaranaId;

    /**
     * @var        Sekolah
     */
    protected $aSekolahRelatedBySekolahId;

    /**
     * @var        Sekolah
     */
    protected $aSekolahRelatedBySekolahId;

    /**
     * @var        JenisBukuAlat
     */
    protected $aJenisBukuAlatRelatedByJenisBukuAlatId;

    /**
     * @var        JenisBukuAlat
     */
    protected $aJenisBukuAlatRelatedByJenisBukuAlatId;

    /**
     * @var        MataPelajaran
     */
    protected $aMataPelajaranRelatedByMataPelajaranId;

    /**
     * @var        MataPelajaran
     */
    protected $aMataPelajaranRelatedByMataPelajaranId;

    /**
     * @var        TingkatPendidikan
     */
    protected $aTingkatPendidikanRelatedByTingkatPendidikanId;

    /**
     * @var        TingkatPendidikan
     */
    protected $aTingkatPendidikanRelatedByTingkatPendidikanId;

    /**
     * @var        PropelObjectCollection|BukuAlatLongitudinal[] Collection to store aggregation of BukuAlatLongitudinal objects.
     */
    protected $collBukuAlatLongitudinalsRelatedByBukuAlatId;
    protected $collBukuAlatLongitudinalsRelatedByBukuAlatIdPartial;

    /**
     * @var        PropelObjectCollection|BukuAlatLongitudinal[] Collection to store aggregation of BukuAlatLongitudinal objects.
     */
    protected $collBukuAlatLongitudinalsRelatedByBukuAlatId;
    protected $collBukuAlatLongitudinalsRelatedByBukuAlatIdPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $bukuAlatLongitudinalsRelatedByBukuAlatIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $bukuAlatLongitudinalsRelatedByBukuAlatIdScheduledForDeletion = null;

    /**
     * Get the [buku_alat_id] column value.
     * 
     * @return string
     */
    public function getBukuAlatId()
    {
        return $this->buku_alat_id;
    }

    /**
     * Get the [mata_pelajaran_id] column value.
     * 
     * @return int
     */
    public function getMataPelajaranId()
    {
        return $this->mata_pelajaran_id;
    }

    /**
     * Get the [prasarana_id] column value.
     * 
     * @return string
     */
    public function getPrasaranaId()
    {
        return $this->prasarana_id;
    }

    /**
     * Get the [sekolah_id] column value.
     * 
     * @return string
     */
    public function getSekolahId()
    {
        return $this->sekolah_id;
    }

    /**
     * Get the [tingkat_pendidikan_id] column value.
     * 
     * @return string
     */
    public function getTingkatPendidikanId()
    {
        return $this->tingkat_pendidikan_id;
    }

    /**
     * Get the [jenis_buku_alat_id] column value.
     * 
     * @return string
     */
    public function getJenisBukuAlatId()
    {
        return $this->jenis_buku_alat_id;
    }

    /**
     * Get the [buku_alat] column value.
     * 
     * @return string
     */
    public function getBukuAlat()
    {
        return $this->buku_alat;
    }

    /**
     * Get the [optionally formatted] temporal [last_update] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastUpdate($format = 'Y-m-d H:i:s')
    {
        if ($this->last_update === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_update);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_update, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [soft_delete] column value.
     * 
     * @return string
     */
    public function getSoftDelete()
    {
        return $this->soft_delete;
    }

    /**
     * Get the [optionally formatted] temporal [last_sync] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastSync($format = 'Y-m-d H:i:s')
    {
        if ($this->last_sync === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_sync);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_sync, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [updater_id] column value.
     * 
     * @return string
     */
    public function getUpdaterId()
    {
        return $this->updater_id;
    }

    /**
     * Set the value of [buku_alat_id] column.
     * 
     * @param string $v new value
     * @return BukuAlat The current object (for fluent API support)
     */
    public function setBukuAlatId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->buku_alat_id !== $v) {
            $this->buku_alat_id = $v;
            $this->modifiedColumns[] = BukuAlatPeer::BUKU_ALAT_ID;
        }


        return $this;
    } // setBukuAlatId()

    /**
     * Set the value of [mata_pelajaran_id] column.
     * 
     * @param int $v new value
     * @return BukuAlat The current object (for fluent API support)
     */
    public function setMataPelajaranId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->mata_pelajaran_id !== $v) {
            $this->mata_pelajaran_id = $v;
            $this->modifiedColumns[] = BukuAlatPeer::MATA_PELAJARAN_ID;
        }

        if ($this->aMataPelajaranRelatedByMataPelajaranId !== null && $this->aMataPelajaranRelatedByMataPelajaranId->getMataPelajaranId() !== $v) {
            $this->aMataPelajaranRelatedByMataPelajaranId = null;
        }

        if ($this->aMataPelajaranRelatedByMataPelajaranId !== null && $this->aMataPelajaranRelatedByMataPelajaranId->getMataPelajaranId() !== $v) {
            $this->aMataPelajaranRelatedByMataPelajaranId = null;
        }


        return $this;
    } // setMataPelajaranId()

    /**
     * Set the value of [prasarana_id] column.
     * 
     * @param string $v new value
     * @return BukuAlat The current object (for fluent API support)
     */
    public function setPrasaranaId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->prasarana_id !== $v) {
            $this->prasarana_id = $v;
            $this->modifiedColumns[] = BukuAlatPeer::PRASARANA_ID;
        }

        if ($this->aPrasaranaRelatedByPrasaranaId !== null && $this->aPrasaranaRelatedByPrasaranaId->getPrasaranaId() !== $v) {
            $this->aPrasaranaRelatedByPrasaranaId = null;
        }

        if ($this->aPrasaranaRelatedByPrasaranaId !== null && $this->aPrasaranaRelatedByPrasaranaId->getPrasaranaId() !== $v) {
            $this->aPrasaranaRelatedByPrasaranaId = null;
        }


        return $this;
    } // setPrasaranaId()

    /**
     * Set the value of [sekolah_id] column.
     * 
     * @param string $v new value
     * @return BukuAlat The current object (for fluent API support)
     */
    public function setSekolahId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->sekolah_id !== $v) {
            $this->sekolah_id = $v;
            $this->modifiedColumns[] = BukuAlatPeer::SEKOLAH_ID;
        }

        if ($this->aSekolahRelatedBySekolahId !== null && $this->aSekolahRelatedBySekolahId->getSekolahId() !== $v) {
            $this->aSekolahRelatedBySekolahId = null;
        }

        if ($this->aSekolahRelatedBySekolahId !== null && $this->aSekolahRelatedBySekolahId->getSekolahId() !== $v) {
            $this->aSekolahRelatedBySekolahId = null;
        }


        return $this;
    } // setSekolahId()

    /**
     * Set the value of [tingkat_pendidikan_id] column.
     * 
     * @param string $v new value
     * @return BukuAlat The current object (for fluent API support)
     */
    public function setTingkatPendidikanId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tingkat_pendidikan_id !== $v) {
            $this->tingkat_pendidikan_id = $v;
            $this->modifiedColumns[] = BukuAlatPeer::TINGKAT_PENDIDIKAN_ID;
        }

        if ($this->aTingkatPendidikanRelatedByTingkatPendidikanId !== null && $this->aTingkatPendidikanRelatedByTingkatPendidikanId->getTingkatPendidikanId() !== $v) {
            $this->aTingkatPendidikanRelatedByTingkatPendidikanId = null;
        }

        if ($this->aTingkatPendidikanRelatedByTingkatPendidikanId !== null && $this->aTingkatPendidikanRelatedByTingkatPendidikanId->getTingkatPendidikanId() !== $v) {
            $this->aTingkatPendidikanRelatedByTingkatPendidikanId = null;
        }


        return $this;
    } // setTingkatPendidikanId()

    /**
     * Set the value of [jenis_buku_alat_id] column.
     * 
     * @param string $v new value
     * @return BukuAlat The current object (for fluent API support)
     */
    public function setJenisBukuAlatId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->jenis_buku_alat_id !== $v) {
            $this->jenis_buku_alat_id = $v;
            $this->modifiedColumns[] = BukuAlatPeer::JENIS_BUKU_ALAT_ID;
        }

        if ($this->aJenisBukuAlatRelatedByJenisBukuAlatId !== null && $this->aJenisBukuAlatRelatedByJenisBukuAlatId->getJenisBukuAlatId() !== $v) {
            $this->aJenisBukuAlatRelatedByJenisBukuAlatId = null;
        }

        if ($this->aJenisBukuAlatRelatedByJenisBukuAlatId !== null && $this->aJenisBukuAlatRelatedByJenisBukuAlatId->getJenisBukuAlatId() !== $v) {
            $this->aJenisBukuAlatRelatedByJenisBukuAlatId = null;
        }


        return $this;
    } // setJenisBukuAlatId()

    /**
     * Set the value of [buku_alat] column.
     * 
     * @param string $v new value
     * @return BukuAlat The current object (for fluent API support)
     */
    public function setBukuAlat($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->buku_alat !== $v) {
            $this->buku_alat = $v;
            $this->modifiedColumns[] = BukuAlatPeer::BUKU_ALAT;
        }


        return $this;
    } // setBukuAlat()

    /**
     * Sets the value of [last_update] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return BukuAlat The current object (for fluent API support)
     */
    public function setLastUpdate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_update !== null || $dt !== null) {
            $currentDateAsString = ($this->last_update !== null && $tmpDt = new DateTime($this->last_update)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_update = $newDateAsString;
                $this->modifiedColumns[] = BukuAlatPeer::LAST_UPDATE;
            }
        } // if either are not null


        return $this;
    } // setLastUpdate()

    /**
     * Set the value of [soft_delete] column.
     * 
     * @param string $v new value
     * @return BukuAlat The current object (for fluent API support)
     */
    public function setSoftDelete($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->soft_delete !== $v) {
            $this->soft_delete = $v;
            $this->modifiedColumns[] = BukuAlatPeer::SOFT_DELETE;
        }


        return $this;
    } // setSoftDelete()

    /**
     * Sets the value of [last_sync] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return BukuAlat The current object (for fluent API support)
     */
    public function setLastSync($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_sync !== null || $dt !== null) {
            $currentDateAsString = ($this->last_sync !== null && $tmpDt = new DateTime($this->last_sync)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_sync = $newDateAsString;
                $this->modifiedColumns[] = BukuAlatPeer::LAST_SYNC;
            }
        } // if either are not null


        return $this;
    } // setLastSync()

    /**
     * Set the value of [updater_id] column.
     * 
     * @param string $v new value
     * @return BukuAlat The current object (for fluent API support)
     */
    public function setUpdaterId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->updater_id !== $v) {
            $this->updater_id = $v;
            $this->modifiedColumns[] = BukuAlatPeer::UPDATER_ID;
        }


        return $this;
    } // setUpdaterId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->buku_alat_id = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
            $this->mata_pelajaran_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->prasarana_id = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->sekolah_id = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->tingkat_pendidikan_id = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->jenis_buku_alat_id = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->buku_alat = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->last_update = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->soft_delete = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->last_sync = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->updater_id = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 11; // 11 = BukuAlatPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating BukuAlat object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aMataPelajaranRelatedByMataPelajaranId !== null && $this->mata_pelajaran_id !== $this->aMataPelajaranRelatedByMataPelajaranId->getMataPelajaranId()) {
            $this->aMataPelajaranRelatedByMataPelajaranId = null;
        }
        if ($this->aMataPelajaranRelatedByMataPelajaranId !== null && $this->mata_pelajaran_id !== $this->aMataPelajaranRelatedByMataPelajaranId->getMataPelajaranId()) {
            $this->aMataPelajaranRelatedByMataPelajaranId = null;
        }
        if ($this->aPrasaranaRelatedByPrasaranaId !== null && $this->prasarana_id !== $this->aPrasaranaRelatedByPrasaranaId->getPrasaranaId()) {
            $this->aPrasaranaRelatedByPrasaranaId = null;
        }
        if ($this->aPrasaranaRelatedByPrasaranaId !== null && $this->prasarana_id !== $this->aPrasaranaRelatedByPrasaranaId->getPrasaranaId()) {
            $this->aPrasaranaRelatedByPrasaranaId = null;
        }
        if ($this->aSekolahRelatedBySekolahId !== null && $this->sekolah_id !== $this->aSekolahRelatedBySekolahId->getSekolahId()) {
            $this->aSekolahRelatedBySekolahId = null;
        }
        if ($this->aSekolahRelatedBySekolahId !== null && $this->sekolah_id !== $this->aSekolahRelatedBySekolahId->getSekolahId()) {
            $this->aSekolahRelatedBySekolahId = null;
        }
        if ($this->aTingkatPendidikanRelatedByTingkatPendidikanId !== null && $this->tingkat_pendidikan_id !== $this->aTingkatPendidikanRelatedByTingkatPendidikanId->getTingkatPendidikanId()) {
            $this->aTingkatPendidikanRelatedByTingkatPendidikanId = null;
        }
        if ($this->aTingkatPendidikanRelatedByTingkatPendidikanId !== null && $this->tingkat_pendidikan_id !== $this->aTingkatPendidikanRelatedByTingkatPendidikanId->getTingkatPendidikanId()) {
            $this->aTingkatPendidikanRelatedByTingkatPendidikanId = null;
        }
        if ($this->aJenisBukuAlatRelatedByJenisBukuAlatId !== null && $this->jenis_buku_alat_id !== $this->aJenisBukuAlatRelatedByJenisBukuAlatId->getJenisBukuAlatId()) {
            $this->aJenisBukuAlatRelatedByJenisBukuAlatId = null;
        }
        if ($this->aJenisBukuAlatRelatedByJenisBukuAlatId !== null && $this->jenis_buku_alat_id !== $this->aJenisBukuAlatRelatedByJenisBukuAlatId->getJenisBukuAlatId()) {
            $this->aJenisBukuAlatRelatedByJenisBukuAlatId = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(BukuAlatPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = BukuAlatPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aPrasaranaRelatedByPrasaranaId = null;
            $this->aPrasaranaRelatedByPrasaranaId = null;
            $this->aSekolahRelatedBySekolahId = null;
            $this->aSekolahRelatedBySekolahId = null;
            $this->aJenisBukuAlatRelatedByJenisBukuAlatId = null;
            $this->aJenisBukuAlatRelatedByJenisBukuAlatId = null;
            $this->aMataPelajaranRelatedByMataPelajaranId = null;
            $this->aMataPelajaranRelatedByMataPelajaranId = null;
            $this->aTingkatPendidikanRelatedByTingkatPendidikanId = null;
            $this->aTingkatPendidikanRelatedByTingkatPendidikanId = null;
            $this->collBukuAlatLongitudinalsRelatedByBukuAlatId = null;

            $this->collBukuAlatLongitudinalsRelatedByBukuAlatId = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(BukuAlatPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = BukuAlatQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(BukuAlatPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                BukuAlatPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPrasaranaRelatedByPrasaranaId !== null) {
                if ($this->aPrasaranaRelatedByPrasaranaId->isModified() || $this->aPrasaranaRelatedByPrasaranaId->isNew()) {
                    $affectedRows += $this->aPrasaranaRelatedByPrasaranaId->save($con);
                }
                $this->setPrasaranaRelatedByPrasaranaId($this->aPrasaranaRelatedByPrasaranaId);
            }

            if ($this->aPrasaranaRelatedByPrasaranaId !== null) {
                if ($this->aPrasaranaRelatedByPrasaranaId->isModified() || $this->aPrasaranaRelatedByPrasaranaId->isNew()) {
                    $affectedRows += $this->aPrasaranaRelatedByPrasaranaId->save($con);
                }
                $this->setPrasaranaRelatedByPrasaranaId($this->aPrasaranaRelatedByPrasaranaId);
            }

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if ($this->aSekolahRelatedBySekolahId->isModified() || $this->aSekolahRelatedBySekolahId->isNew()) {
                    $affectedRows += $this->aSekolahRelatedBySekolahId->save($con);
                }
                $this->setSekolahRelatedBySekolahId($this->aSekolahRelatedBySekolahId);
            }

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if ($this->aSekolahRelatedBySekolahId->isModified() || $this->aSekolahRelatedBySekolahId->isNew()) {
                    $affectedRows += $this->aSekolahRelatedBySekolahId->save($con);
                }
                $this->setSekolahRelatedBySekolahId($this->aSekolahRelatedBySekolahId);
            }

            if ($this->aJenisBukuAlatRelatedByJenisBukuAlatId !== null) {
                if ($this->aJenisBukuAlatRelatedByJenisBukuAlatId->isModified() || $this->aJenisBukuAlatRelatedByJenisBukuAlatId->isNew()) {
                    $affectedRows += $this->aJenisBukuAlatRelatedByJenisBukuAlatId->save($con);
                }
                $this->setJenisBukuAlatRelatedByJenisBukuAlatId($this->aJenisBukuAlatRelatedByJenisBukuAlatId);
            }

            if ($this->aJenisBukuAlatRelatedByJenisBukuAlatId !== null) {
                if ($this->aJenisBukuAlatRelatedByJenisBukuAlatId->isModified() || $this->aJenisBukuAlatRelatedByJenisBukuAlatId->isNew()) {
                    $affectedRows += $this->aJenisBukuAlatRelatedByJenisBukuAlatId->save($con);
                }
                $this->setJenisBukuAlatRelatedByJenisBukuAlatId($this->aJenisBukuAlatRelatedByJenisBukuAlatId);
            }

            if ($this->aMataPelajaranRelatedByMataPelajaranId !== null) {
                if ($this->aMataPelajaranRelatedByMataPelajaranId->isModified() || $this->aMataPelajaranRelatedByMataPelajaranId->isNew()) {
                    $affectedRows += $this->aMataPelajaranRelatedByMataPelajaranId->save($con);
                }
                $this->setMataPelajaranRelatedByMataPelajaranId($this->aMataPelajaranRelatedByMataPelajaranId);
            }

            if ($this->aMataPelajaranRelatedByMataPelajaranId !== null) {
                if ($this->aMataPelajaranRelatedByMataPelajaranId->isModified() || $this->aMataPelajaranRelatedByMataPelajaranId->isNew()) {
                    $affectedRows += $this->aMataPelajaranRelatedByMataPelajaranId->save($con);
                }
                $this->setMataPelajaranRelatedByMataPelajaranId($this->aMataPelajaranRelatedByMataPelajaranId);
            }

            if ($this->aTingkatPendidikanRelatedByTingkatPendidikanId !== null) {
                if ($this->aTingkatPendidikanRelatedByTingkatPendidikanId->isModified() || $this->aTingkatPendidikanRelatedByTingkatPendidikanId->isNew()) {
                    $affectedRows += $this->aTingkatPendidikanRelatedByTingkatPendidikanId->save($con);
                }
                $this->setTingkatPendidikanRelatedByTingkatPendidikanId($this->aTingkatPendidikanRelatedByTingkatPendidikanId);
            }

            if ($this->aTingkatPendidikanRelatedByTingkatPendidikanId !== null) {
                if ($this->aTingkatPendidikanRelatedByTingkatPendidikanId->isModified() || $this->aTingkatPendidikanRelatedByTingkatPendidikanId->isNew()) {
                    $affectedRows += $this->aTingkatPendidikanRelatedByTingkatPendidikanId->save($con);
                }
                $this->setTingkatPendidikanRelatedByTingkatPendidikanId($this->aTingkatPendidikanRelatedByTingkatPendidikanId);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->bukuAlatLongitudinalsRelatedByBukuAlatIdScheduledForDeletion !== null) {
                if (!$this->bukuAlatLongitudinalsRelatedByBukuAlatIdScheduledForDeletion->isEmpty()) {
                    BukuAlatLongitudinalQuery::create()
                        ->filterByPrimaryKeys($this->bukuAlatLongitudinalsRelatedByBukuAlatIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->bukuAlatLongitudinalsRelatedByBukuAlatIdScheduledForDeletion = null;
                }
            }

            if ($this->collBukuAlatLongitudinalsRelatedByBukuAlatId !== null) {
                foreach ($this->collBukuAlatLongitudinalsRelatedByBukuAlatId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->bukuAlatLongitudinalsRelatedByBukuAlatIdScheduledForDeletion !== null) {
                if (!$this->bukuAlatLongitudinalsRelatedByBukuAlatIdScheduledForDeletion->isEmpty()) {
                    BukuAlatLongitudinalQuery::create()
                        ->filterByPrimaryKeys($this->bukuAlatLongitudinalsRelatedByBukuAlatIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->bukuAlatLongitudinalsRelatedByBukuAlatIdScheduledForDeletion = null;
                }
            }

            if ($this->collBukuAlatLongitudinalsRelatedByBukuAlatId !== null) {
                foreach ($this->collBukuAlatLongitudinalsRelatedByBukuAlatId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $criteria = $this->buildCriteria();
        $pk = BasePeer::doInsert($criteria, $con);
        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPrasaranaRelatedByPrasaranaId !== null) {
                if (!$this->aPrasaranaRelatedByPrasaranaId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPrasaranaRelatedByPrasaranaId->getValidationFailures());
                }
            }

            if ($this->aPrasaranaRelatedByPrasaranaId !== null) {
                if (!$this->aPrasaranaRelatedByPrasaranaId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPrasaranaRelatedByPrasaranaId->getValidationFailures());
                }
            }

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if (!$this->aSekolahRelatedBySekolahId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSekolahRelatedBySekolahId->getValidationFailures());
                }
            }

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if (!$this->aSekolahRelatedBySekolahId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSekolahRelatedBySekolahId->getValidationFailures());
                }
            }

            if ($this->aJenisBukuAlatRelatedByJenisBukuAlatId !== null) {
                if (!$this->aJenisBukuAlatRelatedByJenisBukuAlatId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aJenisBukuAlatRelatedByJenisBukuAlatId->getValidationFailures());
                }
            }

            if ($this->aJenisBukuAlatRelatedByJenisBukuAlatId !== null) {
                if (!$this->aJenisBukuAlatRelatedByJenisBukuAlatId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aJenisBukuAlatRelatedByJenisBukuAlatId->getValidationFailures());
                }
            }

            if ($this->aMataPelajaranRelatedByMataPelajaranId !== null) {
                if (!$this->aMataPelajaranRelatedByMataPelajaranId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aMataPelajaranRelatedByMataPelajaranId->getValidationFailures());
                }
            }

            if ($this->aMataPelajaranRelatedByMataPelajaranId !== null) {
                if (!$this->aMataPelajaranRelatedByMataPelajaranId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aMataPelajaranRelatedByMataPelajaranId->getValidationFailures());
                }
            }

            if ($this->aTingkatPendidikanRelatedByTingkatPendidikanId !== null) {
                if (!$this->aTingkatPendidikanRelatedByTingkatPendidikanId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aTingkatPendidikanRelatedByTingkatPendidikanId->getValidationFailures());
                }
            }

            if ($this->aTingkatPendidikanRelatedByTingkatPendidikanId !== null) {
                if (!$this->aTingkatPendidikanRelatedByTingkatPendidikanId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aTingkatPendidikanRelatedByTingkatPendidikanId->getValidationFailures());
                }
            }


            if (($retval = BukuAlatPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collBukuAlatLongitudinalsRelatedByBukuAlatId !== null) {
                    foreach ($this->collBukuAlatLongitudinalsRelatedByBukuAlatId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collBukuAlatLongitudinalsRelatedByBukuAlatId !== null) {
                    foreach ($this->collBukuAlatLongitudinalsRelatedByBukuAlatId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = BukuAlatPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getBukuAlatId();
                break;
            case 1:
                return $this->getMataPelajaranId();
                break;
            case 2:
                return $this->getPrasaranaId();
                break;
            case 3:
                return $this->getSekolahId();
                break;
            case 4:
                return $this->getTingkatPendidikanId();
                break;
            case 5:
                return $this->getJenisBukuAlatId();
                break;
            case 6:
                return $this->getBukuAlat();
                break;
            case 7:
                return $this->getLastUpdate();
                break;
            case 8:
                return $this->getSoftDelete();
                break;
            case 9:
                return $this->getLastSync();
                break;
            case 10:
                return $this->getUpdaterId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['BukuAlat'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['BukuAlat'][$this->getPrimaryKey()] = true;
        $keys = BukuAlatPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getBukuAlatId(),
            $keys[1] => $this->getMataPelajaranId(),
            $keys[2] => $this->getPrasaranaId(),
            $keys[3] => $this->getSekolahId(),
            $keys[4] => $this->getTingkatPendidikanId(),
            $keys[5] => $this->getJenisBukuAlatId(),
            $keys[6] => $this->getBukuAlat(),
            $keys[7] => $this->getLastUpdate(),
            $keys[8] => $this->getSoftDelete(),
            $keys[9] => $this->getLastSync(),
            $keys[10] => $this->getUpdaterId(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aPrasaranaRelatedByPrasaranaId) {
                $result['PrasaranaRelatedByPrasaranaId'] = $this->aPrasaranaRelatedByPrasaranaId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPrasaranaRelatedByPrasaranaId) {
                $result['PrasaranaRelatedByPrasaranaId'] = $this->aPrasaranaRelatedByPrasaranaId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSekolahRelatedBySekolahId) {
                $result['SekolahRelatedBySekolahId'] = $this->aSekolahRelatedBySekolahId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSekolahRelatedBySekolahId) {
                $result['SekolahRelatedBySekolahId'] = $this->aSekolahRelatedBySekolahId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aJenisBukuAlatRelatedByJenisBukuAlatId) {
                $result['JenisBukuAlatRelatedByJenisBukuAlatId'] = $this->aJenisBukuAlatRelatedByJenisBukuAlatId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aJenisBukuAlatRelatedByJenisBukuAlatId) {
                $result['JenisBukuAlatRelatedByJenisBukuAlatId'] = $this->aJenisBukuAlatRelatedByJenisBukuAlatId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aMataPelajaranRelatedByMataPelajaranId) {
                $result['MataPelajaranRelatedByMataPelajaranId'] = $this->aMataPelajaranRelatedByMataPelajaranId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aMataPelajaranRelatedByMataPelajaranId) {
                $result['MataPelajaranRelatedByMataPelajaranId'] = $this->aMataPelajaranRelatedByMataPelajaranId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aTingkatPendidikanRelatedByTingkatPendidikanId) {
                $result['TingkatPendidikanRelatedByTingkatPendidikanId'] = $this->aTingkatPendidikanRelatedByTingkatPendidikanId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aTingkatPendidikanRelatedByTingkatPendidikanId) {
                $result['TingkatPendidikanRelatedByTingkatPendidikanId'] = $this->aTingkatPendidikanRelatedByTingkatPendidikanId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collBukuAlatLongitudinalsRelatedByBukuAlatId) {
                $result['BukuAlatLongitudinalsRelatedByBukuAlatId'] = $this->collBukuAlatLongitudinalsRelatedByBukuAlatId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collBukuAlatLongitudinalsRelatedByBukuAlatId) {
                $result['BukuAlatLongitudinalsRelatedByBukuAlatId'] = $this->collBukuAlatLongitudinalsRelatedByBukuAlatId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = BukuAlatPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setBukuAlatId($value);
                break;
            case 1:
                $this->setMataPelajaranId($value);
                break;
            case 2:
                $this->setPrasaranaId($value);
                break;
            case 3:
                $this->setSekolahId($value);
                break;
            case 4:
                $this->setTingkatPendidikanId($value);
                break;
            case 5:
                $this->setJenisBukuAlatId($value);
                break;
            case 6:
                $this->setBukuAlat($value);
                break;
            case 7:
                $this->setLastUpdate($value);
                break;
            case 8:
                $this->setSoftDelete($value);
                break;
            case 9:
                $this->setLastSync($value);
                break;
            case 10:
                $this->setUpdaterId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = BukuAlatPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setBukuAlatId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setMataPelajaranId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setPrasaranaId($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setSekolahId($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setTingkatPendidikanId($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setJenisBukuAlatId($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setBukuAlat($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setLastUpdate($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setSoftDelete($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setLastSync($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setUpdaterId($arr[$keys[10]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(BukuAlatPeer::DATABASE_NAME);

        if ($this->isColumnModified(BukuAlatPeer::BUKU_ALAT_ID)) $criteria->add(BukuAlatPeer::BUKU_ALAT_ID, $this->buku_alat_id);
        if ($this->isColumnModified(BukuAlatPeer::MATA_PELAJARAN_ID)) $criteria->add(BukuAlatPeer::MATA_PELAJARAN_ID, $this->mata_pelajaran_id);
        if ($this->isColumnModified(BukuAlatPeer::PRASARANA_ID)) $criteria->add(BukuAlatPeer::PRASARANA_ID, $this->prasarana_id);
        if ($this->isColumnModified(BukuAlatPeer::SEKOLAH_ID)) $criteria->add(BukuAlatPeer::SEKOLAH_ID, $this->sekolah_id);
        if ($this->isColumnModified(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID)) $criteria->add(BukuAlatPeer::TINGKAT_PENDIDIKAN_ID, $this->tingkat_pendidikan_id);
        if ($this->isColumnModified(BukuAlatPeer::JENIS_BUKU_ALAT_ID)) $criteria->add(BukuAlatPeer::JENIS_BUKU_ALAT_ID, $this->jenis_buku_alat_id);
        if ($this->isColumnModified(BukuAlatPeer::BUKU_ALAT)) $criteria->add(BukuAlatPeer::BUKU_ALAT, $this->buku_alat);
        if ($this->isColumnModified(BukuAlatPeer::LAST_UPDATE)) $criteria->add(BukuAlatPeer::LAST_UPDATE, $this->last_update);
        if ($this->isColumnModified(BukuAlatPeer::SOFT_DELETE)) $criteria->add(BukuAlatPeer::SOFT_DELETE, $this->soft_delete);
        if ($this->isColumnModified(BukuAlatPeer::LAST_SYNC)) $criteria->add(BukuAlatPeer::LAST_SYNC, $this->last_sync);
        if ($this->isColumnModified(BukuAlatPeer::UPDATER_ID)) $criteria->add(BukuAlatPeer::UPDATER_ID, $this->updater_id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(BukuAlatPeer::DATABASE_NAME);
        $criteria->add(BukuAlatPeer::BUKU_ALAT_ID, $this->buku_alat_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getBukuAlatId();
    }

    /**
     * Generic method to set the primary key (buku_alat_id column).
     *
     * @param  string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setBukuAlatId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getBukuAlatId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of BukuAlat (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setMataPelajaranId($this->getMataPelajaranId());
        $copyObj->setPrasaranaId($this->getPrasaranaId());
        $copyObj->setSekolahId($this->getSekolahId());
        $copyObj->setTingkatPendidikanId($this->getTingkatPendidikanId());
        $copyObj->setJenisBukuAlatId($this->getJenisBukuAlatId());
        $copyObj->setBukuAlat($this->getBukuAlat());
        $copyObj->setLastUpdate($this->getLastUpdate());
        $copyObj->setSoftDelete($this->getSoftDelete());
        $copyObj->setLastSync($this->getLastSync());
        $copyObj->setUpdaterId($this->getUpdaterId());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getBukuAlatLongitudinalsRelatedByBukuAlatId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBukuAlatLongitudinalRelatedByBukuAlatId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getBukuAlatLongitudinalsRelatedByBukuAlatId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBukuAlatLongitudinalRelatedByBukuAlatId($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setBukuAlatId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return BukuAlat Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return BukuAlatPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new BukuAlatPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a Prasarana object.
     *
     * @param             Prasarana $v
     * @return BukuAlat The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPrasaranaRelatedByPrasaranaId(Prasarana $v = null)
    {
        if ($v === null) {
            $this->setPrasaranaId(NULL);
        } else {
            $this->setPrasaranaId($v->getPrasaranaId());
        }

        $this->aPrasaranaRelatedByPrasaranaId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Prasarana object, it will not be re-added.
        if ($v !== null) {
            $v->addBukuAlatRelatedByPrasaranaId($this);
        }


        return $this;
    }


    /**
     * Get the associated Prasarana object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Prasarana The associated Prasarana object.
     * @throws PropelException
     */
    public function getPrasaranaRelatedByPrasaranaId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPrasaranaRelatedByPrasaranaId === null && (($this->prasarana_id !== "" && $this->prasarana_id !== null)) && $doQuery) {
            $this->aPrasaranaRelatedByPrasaranaId = PrasaranaQuery::create()->findPk($this->prasarana_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPrasaranaRelatedByPrasaranaId->addBukuAlatsRelatedByPrasaranaId($this);
             */
        }

        return $this->aPrasaranaRelatedByPrasaranaId;
    }

    /**
     * Declares an association between this object and a Prasarana object.
     *
     * @param             Prasarana $v
     * @return BukuAlat The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPrasaranaRelatedByPrasaranaId(Prasarana $v = null)
    {
        if ($v === null) {
            $this->setPrasaranaId(NULL);
        } else {
            $this->setPrasaranaId($v->getPrasaranaId());
        }

        $this->aPrasaranaRelatedByPrasaranaId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Prasarana object, it will not be re-added.
        if ($v !== null) {
            $v->addBukuAlatRelatedByPrasaranaId($this);
        }


        return $this;
    }


    /**
     * Get the associated Prasarana object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Prasarana The associated Prasarana object.
     * @throws PropelException
     */
    public function getPrasaranaRelatedByPrasaranaId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPrasaranaRelatedByPrasaranaId === null && (($this->prasarana_id !== "" && $this->prasarana_id !== null)) && $doQuery) {
            $this->aPrasaranaRelatedByPrasaranaId = PrasaranaQuery::create()->findPk($this->prasarana_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPrasaranaRelatedByPrasaranaId->addBukuAlatsRelatedByPrasaranaId($this);
             */
        }

        return $this->aPrasaranaRelatedByPrasaranaId;
    }

    /**
     * Declares an association between this object and a Sekolah object.
     *
     * @param             Sekolah $v
     * @return BukuAlat The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSekolahRelatedBySekolahId(Sekolah $v = null)
    {
        if ($v === null) {
            $this->setSekolahId(NULL);
        } else {
            $this->setSekolahId($v->getSekolahId());
        }

        $this->aSekolahRelatedBySekolahId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Sekolah object, it will not be re-added.
        if ($v !== null) {
            $v->addBukuAlatRelatedBySekolahId($this);
        }


        return $this;
    }


    /**
     * Get the associated Sekolah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Sekolah The associated Sekolah object.
     * @throws PropelException
     */
    public function getSekolahRelatedBySekolahId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSekolahRelatedBySekolahId === null && (($this->sekolah_id !== "" && $this->sekolah_id !== null)) && $doQuery) {
            $this->aSekolahRelatedBySekolahId = SekolahQuery::create()->findPk($this->sekolah_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSekolahRelatedBySekolahId->addBukuAlatsRelatedBySekolahId($this);
             */
        }

        return $this->aSekolahRelatedBySekolahId;
    }

    /**
     * Declares an association between this object and a Sekolah object.
     *
     * @param             Sekolah $v
     * @return BukuAlat The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSekolahRelatedBySekolahId(Sekolah $v = null)
    {
        if ($v === null) {
            $this->setSekolahId(NULL);
        } else {
            $this->setSekolahId($v->getSekolahId());
        }

        $this->aSekolahRelatedBySekolahId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Sekolah object, it will not be re-added.
        if ($v !== null) {
            $v->addBukuAlatRelatedBySekolahId($this);
        }


        return $this;
    }


    /**
     * Get the associated Sekolah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Sekolah The associated Sekolah object.
     * @throws PropelException
     */
    public function getSekolahRelatedBySekolahId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSekolahRelatedBySekolahId === null && (($this->sekolah_id !== "" && $this->sekolah_id !== null)) && $doQuery) {
            $this->aSekolahRelatedBySekolahId = SekolahQuery::create()->findPk($this->sekolah_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSekolahRelatedBySekolahId->addBukuAlatsRelatedBySekolahId($this);
             */
        }

        return $this->aSekolahRelatedBySekolahId;
    }

    /**
     * Declares an association between this object and a JenisBukuAlat object.
     *
     * @param             JenisBukuAlat $v
     * @return BukuAlat The current object (for fluent API support)
     * @throws PropelException
     */
    public function setJenisBukuAlatRelatedByJenisBukuAlatId(JenisBukuAlat $v = null)
    {
        if ($v === null) {
            $this->setJenisBukuAlatId(NULL);
        } else {
            $this->setJenisBukuAlatId($v->getJenisBukuAlatId());
        }

        $this->aJenisBukuAlatRelatedByJenisBukuAlatId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the JenisBukuAlat object, it will not be re-added.
        if ($v !== null) {
            $v->addBukuAlatRelatedByJenisBukuAlatId($this);
        }


        return $this;
    }


    /**
     * Get the associated JenisBukuAlat object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return JenisBukuAlat The associated JenisBukuAlat object.
     * @throws PropelException
     */
    public function getJenisBukuAlatRelatedByJenisBukuAlatId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aJenisBukuAlatRelatedByJenisBukuAlatId === null && (($this->jenis_buku_alat_id !== "" && $this->jenis_buku_alat_id !== null)) && $doQuery) {
            $this->aJenisBukuAlatRelatedByJenisBukuAlatId = JenisBukuAlatQuery::create()->findPk($this->jenis_buku_alat_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aJenisBukuAlatRelatedByJenisBukuAlatId->addBukuAlatsRelatedByJenisBukuAlatId($this);
             */
        }

        return $this->aJenisBukuAlatRelatedByJenisBukuAlatId;
    }

    /**
     * Declares an association between this object and a JenisBukuAlat object.
     *
     * @param             JenisBukuAlat $v
     * @return BukuAlat The current object (for fluent API support)
     * @throws PropelException
     */
    public function setJenisBukuAlatRelatedByJenisBukuAlatId(JenisBukuAlat $v = null)
    {
        if ($v === null) {
            $this->setJenisBukuAlatId(NULL);
        } else {
            $this->setJenisBukuAlatId($v->getJenisBukuAlatId());
        }

        $this->aJenisBukuAlatRelatedByJenisBukuAlatId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the JenisBukuAlat object, it will not be re-added.
        if ($v !== null) {
            $v->addBukuAlatRelatedByJenisBukuAlatId($this);
        }


        return $this;
    }


    /**
     * Get the associated JenisBukuAlat object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return JenisBukuAlat The associated JenisBukuAlat object.
     * @throws PropelException
     */
    public function getJenisBukuAlatRelatedByJenisBukuAlatId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aJenisBukuAlatRelatedByJenisBukuAlatId === null && (($this->jenis_buku_alat_id !== "" && $this->jenis_buku_alat_id !== null)) && $doQuery) {
            $this->aJenisBukuAlatRelatedByJenisBukuAlatId = JenisBukuAlatQuery::create()->findPk($this->jenis_buku_alat_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aJenisBukuAlatRelatedByJenisBukuAlatId->addBukuAlatsRelatedByJenisBukuAlatId($this);
             */
        }

        return $this->aJenisBukuAlatRelatedByJenisBukuAlatId;
    }

    /**
     * Declares an association between this object and a MataPelajaran object.
     *
     * @param             MataPelajaran $v
     * @return BukuAlat The current object (for fluent API support)
     * @throws PropelException
     */
    public function setMataPelajaranRelatedByMataPelajaranId(MataPelajaran $v = null)
    {
        if ($v === null) {
            $this->setMataPelajaranId(NULL);
        } else {
            $this->setMataPelajaranId($v->getMataPelajaranId());
        }

        $this->aMataPelajaranRelatedByMataPelajaranId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the MataPelajaran object, it will not be re-added.
        if ($v !== null) {
            $v->addBukuAlatRelatedByMataPelajaranId($this);
        }


        return $this;
    }


    /**
     * Get the associated MataPelajaran object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return MataPelajaran The associated MataPelajaran object.
     * @throws PropelException
     */
    public function getMataPelajaranRelatedByMataPelajaranId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aMataPelajaranRelatedByMataPelajaranId === null && ($this->mata_pelajaran_id !== null) && $doQuery) {
            $this->aMataPelajaranRelatedByMataPelajaranId = MataPelajaranQuery::create()->findPk($this->mata_pelajaran_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aMataPelajaranRelatedByMataPelajaranId->addBukuAlatsRelatedByMataPelajaranId($this);
             */
        }

        return $this->aMataPelajaranRelatedByMataPelajaranId;
    }

    /**
     * Declares an association between this object and a MataPelajaran object.
     *
     * @param             MataPelajaran $v
     * @return BukuAlat The current object (for fluent API support)
     * @throws PropelException
     */
    public function setMataPelajaranRelatedByMataPelajaranId(MataPelajaran $v = null)
    {
        if ($v === null) {
            $this->setMataPelajaranId(NULL);
        } else {
            $this->setMataPelajaranId($v->getMataPelajaranId());
        }

        $this->aMataPelajaranRelatedByMataPelajaranId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the MataPelajaran object, it will not be re-added.
        if ($v !== null) {
            $v->addBukuAlatRelatedByMataPelajaranId($this);
        }


        return $this;
    }


    /**
     * Get the associated MataPelajaran object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return MataPelajaran The associated MataPelajaran object.
     * @throws PropelException
     */
    public function getMataPelajaranRelatedByMataPelajaranId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aMataPelajaranRelatedByMataPelajaranId === null && ($this->mata_pelajaran_id !== null) && $doQuery) {
            $this->aMataPelajaranRelatedByMataPelajaranId = MataPelajaranQuery::create()->findPk($this->mata_pelajaran_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aMataPelajaranRelatedByMataPelajaranId->addBukuAlatsRelatedByMataPelajaranId($this);
             */
        }

        return $this->aMataPelajaranRelatedByMataPelajaranId;
    }

    /**
     * Declares an association between this object and a TingkatPendidikan object.
     *
     * @param             TingkatPendidikan $v
     * @return BukuAlat The current object (for fluent API support)
     * @throws PropelException
     */
    public function setTingkatPendidikanRelatedByTingkatPendidikanId(TingkatPendidikan $v = null)
    {
        if ($v === null) {
            $this->setTingkatPendidikanId(NULL);
        } else {
            $this->setTingkatPendidikanId($v->getTingkatPendidikanId());
        }

        $this->aTingkatPendidikanRelatedByTingkatPendidikanId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the TingkatPendidikan object, it will not be re-added.
        if ($v !== null) {
            $v->addBukuAlatRelatedByTingkatPendidikanId($this);
        }


        return $this;
    }


    /**
     * Get the associated TingkatPendidikan object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return TingkatPendidikan The associated TingkatPendidikan object.
     * @throws PropelException
     */
    public function getTingkatPendidikanRelatedByTingkatPendidikanId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aTingkatPendidikanRelatedByTingkatPendidikanId === null && (($this->tingkat_pendidikan_id !== "" && $this->tingkat_pendidikan_id !== null)) && $doQuery) {
            $this->aTingkatPendidikanRelatedByTingkatPendidikanId = TingkatPendidikanQuery::create()->findPk($this->tingkat_pendidikan_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aTingkatPendidikanRelatedByTingkatPendidikanId->addBukuAlatsRelatedByTingkatPendidikanId($this);
             */
        }

        return $this->aTingkatPendidikanRelatedByTingkatPendidikanId;
    }

    /**
     * Declares an association between this object and a TingkatPendidikan object.
     *
     * @param             TingkatPendidikan $v
     * @return BukuAlat The current object (for fluent API support)
     * @throws PropelException
     */
    public function setTingkatPendidikanRelatedByTingkatPendidikanId(TingkatPendidikan $v = null)
    {
        if ($v === null) {
            $this->setTingkatPendidikanId(NULL);
        } else {
            $this->setTingkatPendidikanId($v->getTingkatPendidikanId());
        }

        $this->aTingkatPendidikanRelatedByTingkatPendidikanId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the TingkatPendidikan object, it will not be re-added.
        if ($v !== null) {
            $v->addBukuAlatRelatedByTingkatPendidikanId($this);
        }


        return $this;
    }


    /**
     * Get the associated TingkatPendidikan object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return TingkatPendidikan The associated TingkatPendidikan object.
     * @throws PropelException
     */
    public function getTingkatPendidikanRelatedByTingkatPendidikanId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aTingkatPendidikanRelatedByTingkatPendidikanId === null && (($this->tingkat_pendidikan_id !== "" && $this->tingkat_pendidikan_id !== null)) && $doQuery) {
            $this->aTingkatPendidikanRelatedByTingkatPendidikanId = TingkatPendidikanQuery::create()->findPk($this->tingkat_pendidikan_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aTingkatPendidikanRelatedByTingkatPendidikanId->addBukuAlatsRelatedByTingkatPendidikanId($this);
             */
        }

        return $this->aTingkatPendidikanRelatedByTingkatPendidikanId;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('BukuAlatLongitudinalRelatedByBukuAlatId' == $relationName) {
            $this->initBukuAlatLongitudinalsRelatedByBukuAlatId();
        }
        if ('BukuAlatLongitudinalRelatedByBukuAlatId' == $relationName) {
            $this->initBukuAlatLongitudinalsRelatedByBukuAlatId();
        }
    }

    /**
     * Clears out the collBukuAlatLongitudinalsRelatedByBukuAlatId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return BukuAlat The current object (for fluent API support)
     * @see        addBukuAlatLongitudinalsRelatedByBukuAlatId()
     */
    public function clearBukuAlatLongitudinalsRelatedByBukuAlatId()
    {
        $this->collBukuAlatLongitudinalsRelatedByBukuAlatId = null; // important to set this to null since that means it is uninitialized
        $this->collBukuAlatLongitudinalsRelatedByBukuAlatIdPartial = null;

        return $this;
    }

    /**
     * reset is the collBukuAlatLongitudinalsRelatedByBukuAlatId collection loaded partially
     *
     * @return void
     */
    public function resetPartialBukuAlatLongitudinalsRelatedByBukuAlatId($v = true)
    {
        $this->collBukuAlatLongitudinalsRelatedByBukuAlatIdPartial = $v;
    }

    /**
     * Initializes the collBukuAlatLongitudinalsRelatedByBukuAlatId collection.
     *
     * By default this just sets the collBukuAlatLongitudinalsRelatedByBukuAlatId collection to an empty array (like clearcollBukuAlatLongitudinalsRelatedByBukuAlatId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBukuAlatLongitudinalsRelatedByBukuAlatId($overrideExisting = true)
    {
        if (null !== $this->collBukuAlatLongitudinalsRelatedByBukuAlatId && !$overrideExisting) {
            return;
        }
        $this->collBukuAlatLongitudinalsRelatedByBukuAlatId = new PropelObjectCollection();
        $this->collBukuAlatLongitudinalsRelatedByBukuAlatId->setModel('BukuAlatLongitudinal');
    }

    /**
     * Gets an array of BukuAlatLongitudinal objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this BukuAlat is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|BukuAlatLongitudinal[] List of BukuAlatLongitudinal objects
     * @throws PropelException
     */
    public function getBukuAlatLongitudinalsRelatedByBukuAlatId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collBukuAlatLongitudinalsRelatedByBukuAlatIdPartial && !$this->isNew();
        if (null === $this->collBukuAlatLongitudinalsRelatedByBukuAlatId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBukuAlatLongitudinalsRelatedByBukuAlatId) {
                // return empty collection
                $this->initBukuAlatLongitudinalsRelatedByBukuAlatId();
            } else {
                $collBukuAlatLongitudinalsRelatedByBukuAlatId = BukuAlatLongitudinalQuery::create(null, $criteria)
                    ->filterByBukuAlatRelatedByBukuAlatId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collBukuAlatLongitudinalsRelatedByBukuAlatIdPartial && count($collBukuAlatLongitudinalsRelatedByBukuAlatId)) {
                      $this->initBukuAlatLongitudinalsRelatedByBukuAlatId(false);

                      foreach($collBukuAlatLongitudinalsRelatedByBukuAlatId as $obj) {
                        if (false == $this->collBukuAlatLongitudinalsRelatedByBukuAlatId->contains($obj)) {
                          $this->collBukuAlatLongitudinalsRelatedByBukuAlatId->append($obj);
                        }
                      }

                      $this->collBukuAlatLongitudinalsRelatedByBukuAlatIdPartial = true;
                    }

                    $collBukuAlatLongitudinalsRelatedByBukuAlatId->getInternalIterator()->rewind();
                    return $collBukuAlatLongitudinalsRelatedByBukuAlatId;
                }

                if($partial && $this->collBukuAlatLongitudinalsRelatedByBukuAlatId) {
                    foreach($this->collBukuAlatLongitudinalsRelatedByBukuAlatId as $obj) {
                        if($obj->isNew()) {
                            $collBukuAlatLongitudinalsRelatedByBukuAlatId[] = $obj;
                        }
                    }
                }

                $this->collBukuAlatLongitudinalsRelatedByBukuAlatId = $collBukuAlatLongitudinalsRelatedByBukuAlatId;
                $this->collBukuAlatLongitudinalsRelatedByBukuAlatIdPartial = false;
            }
        }

        return $this->collBukuAlatLongitudinalsRelatedByBukuAlatId;
    }

    /**
     * Sets a collection of BukuAlatLongitudinalRelatedByBukuAlatId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $bukuAlatLongitudinalsRelatedByBukuAlatId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return BukuAlat The current object (for fluent API support)
     */
    public function setBukuAlatLongitudinalsRelatedByBukuAlatId(PropelCollection $bukuAlatLongitudinalsRelatedByBukuAlatId, PropelPDO $con = null)
    {
        $bukuAlatLongitudinalsRelatedByBukuAlatIdToDelete = $this->getBukuAlatLongitudinalsRelatedByBukuAlatId(new Criteria(), $con)->diff($bukuAlatLongitudinalsRelatedByBukuAlatId);

        $this->bukuAlatLongitudinalsRelatedByBukuAlatIdScheduledForDeletion = unserialize(serialize($bukuAlatLongitudinalsRelatedByBukuAlatIdToDelete));

        foreach ($bukuAlatLongitudinalsRelatedByBukuAlatIdToDelete as $bukuAlatLongitudinalRelatedByBukuAlatIdRemoved) {
            $bukuAlatLongitudinalRelatedByBukuAlatIdRemoved->setBukuAlatRelatedByBukuAlatId(null);
        }

        $this->collBukuAlatLongitudinalsRelatedByBukuAlatId = null;
        foreach ($bukuAlatLongitudinalsRelatedByBukuAlatId as $bukuAlatLongitudinalRelatedByBukuAlatId) {
            $this->addBukuAlatLongitudinalRelatedByBukuAlatId($bukuAlatLongitudinalRelatedByBukuAlatId);
        }

        $this->collBukuAlatLongitudinalsRelatedByBukuAlatId = $bukuAlatLongitudinalsRelatedByBukuAlatId;
        $this->collBukuAlatLongitudinalsRelatedByBukuAlatIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BukuAlatLongitudinal objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related BukuAlatLongitudinal objects.
     * @throws PropelException
     */
    public function countBukuAlatLongitudinalsRelatedByBukuAlatId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collBukuAlatLongitudinalsRelatedByBukuAlatIdPartial && !$this->isNew();
        if (null === $this->collBukuAlatLongitudinalsRelatedByBukuAlatId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBukuAlatLongitudinalsRelatedByBukuAlatId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getBukuAlatLongitudinalsRelatedByBukuAlatId());
            }
            $query = BukuAlatLongitudinalQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByBukuAlatRelatedByBukuAlatId($this)
                ->count($con);
        }

        return count($this->collBukuAlatLongitudinalsRelatedByBukuAlatId);
    }

    /**
     * Method called to associate a BukuAlatLongitudinal object to this object
     * through the BukuAlatLongitudinal foreign key attribute.
     *
     * @param    BukuAlatLongitudinal $l BukuAlatLongitudinal
     * @return BukuAlat The current object (for fluent API support)
     */
    public function addBukuAlatLongitudinalRelatedByBukuAlatId(BukuAlatLongitudinal $l)
    {
        if ($this->collBukuAlatLongitudinalsRelatedByBukuAlatId === null) {
            $this->initBukuAlatLongitudinalsRelatedByBukuAlatId();
            $this->collBukuAlatLongitudinalsRelatedByBukuAlatIdPartial = true;
        }
        if (!in_array($l, $this->collBukuAlatLongitudinalsRelatedByBukuAlatId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddBukuAlatLongitudinalRelatedByBukuAlatId($l);
        }

        return $this;
    }

    /**
     * @param	BukuAlatLongitudinalRelatedByBukuAlatId $bukuAlatLongitudinalRelatedByBukuAlatId The bukuAlatLongitudinalRelatedByBukuAlatId object to add.
     */
    protected function doAddBukuAlatLongitudinalRelatedByBukuAlatId($bukuAlatLongitudinalRelatedByBukuAlatId)
    {
        $this->collBukuAlatLongitudinalsRelatedByBukuAlatId[]= $bukuAlatLongitudinalRelatedByBukuAlatId;
        $bukuAlatLongitudinalRelatedByBukuAlatId->setBukuAlatRelatedByBukuAlatId($this);
    }

    /**
     * @param	BukuAlatLongitudinalRelatedByBukuAlatId $bukuAlatLongitudinalRelatedByBukuAlatId The bukuAlatLongitudinalRelatedByBukuAlatId object to remove.
     * @return BukuAlat The current object (for fluent API support)
     */
    public function removeBukuAlatLongitudinalRelatedByBukuAlatId($bukuAlatLongitudinalRelatedByBukuAlatId)
    {
        if ($this->getBukuAlatLongitudinalsRelatedByBukuAlatId()->contains($bukuAlatLongitudinalRelatedByBukuAlatId)) {
            $this->collBukuAlatLongitudinalsRelatedByBukuAlatId->remove($this->collBukuAlatLongitudinalsRelatedByBukuAlatId->search($bukuAlatLongitudinalRelatedByBukuAlatId));
            if (null === $this->bukuAlatLongitudinalsRelatedByBukuAlatIdScheduledForDeletion) {
                $this->bukuAlatLongitudinalsRelatedByBukuAlatIdScheduledForDeletion = clone $this->collBukuAlatLongitudinalsRelatedByBukuAlatId;
                $this->bukuAlatLongitudinalsRelatedByBukuAlatIdScheduledForDeletion->clear();
            }
            $this->bukuAlatLongitudinalsRelatedByBukuAlatIdScheduledForDeletion[]= clone $bukuAlatLongitudinalRelatedByBukuAlatId;
            $bukuAlatLongitudinalRelatedByBukuAlatId->setBukuAlatRelatedByBukuAlatId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BukuAlat is new, it will return
     * an empty collection; or if this BukuAlat has previously
     * been saved, it will retrieve related BukuAlatLongitudinalsRelatedByBukuAlatId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BukuAlat.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BukuAlatLongitudinal[] List of BukuAlatLongitudinal objects
     */
    public function getBukuAlatLongitudinalsRelatedByBukuAlatIdJoinSemesterRelatedBySemesterId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BukuAlatLongitudinalQuery::create(null, $criteria);
        $query->joinWith('SemesterRelatedBySemesterId', $join_behavior);

        return $this->getBukuAlatLongitudinalsRelatedByBukuAlatId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BukuAlat is new, it will return
     * an empty collection; or if this BukuAlat has previously
     * been saved, it will retrieve related BukuAlatLongitudinalsRelatedByBukuAlatId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BukuAlat.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BukuAlatLongitudinal[] List of BukuAlatLongitudinal objects
     */
    public function getBukuAlatLongitudinalsRelatedByBukuAlatIdJoinSemesterRelatedBySemesterId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BukuAlatLongitudinalQuery::create(null, $criteria);
        $query->joinWith('SemesterRelatedBySemesterId', $join_behavior);

        return $this->getBukuAlatLongitudinalsRelatedByBukuAlatId($query, $con);
    }

    /**
     * Clears out the collBukuAlatLongitudinalsRelatedByBukuAlatId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return BukuAlat The current object (for fluent API support)
     * @see        addBukuAlatLongitudinalsRelatedByBukuAlatId()
     */
    public function clearBukuAlatLongitudinalsRelatedByBukuAlatId()
    {
        $this->collBukuAlatLongitudinalsRelatedByBukuAlatId = null; // important to set this to null since that means it is uninitialized
        $this->collBukuAlatLongitudinalsRelatedByBukuAlatIdPartial = null;

        return $this;
    }

    /**
     * reset is the collBukuAlatLongitudinalsRelatedByBukuAlatId collection loaded partially
     *
     * @return void
     */
    public function resetPartialBukuAlatLongitudinalsRelatedByBukuAlatId($v = true)
    {
        $this->collBukuAlatLongitudinalsRelatedByBukuAlatIdPartial = $v;
    }

    /**
     * Initializes the collBukuAlatLongitudinalsRelatedByBukuAlatId collection.
     *
     * By default this just sets the collBukuAlatLongitudinalsRelatedByBukuAlatId collection to an empty array (like clearcollBukuAlatLongitudinalsRelatedByBukuAlatId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBukuAlatLongitudinalsRelatedByBukuAlatId($overrideExisting = true)
    {
        if (null !== $this->collBukuAlatLongitudinalsRelatedByBukuAlatId && !$overrideExisting) {
            return;
        }
        $this->collBukuAlatLongitudinalsRelatedByBukuAlatId = new PropelObjectCollection();
        $this->collBukuAlatLongitudinalsRelatedByBukuAlatId->setModel('BukuAlatLongitudinal');
    }

    /**
     * Gets an array of BukuAlatLongitudinal objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this BukuAlat is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|BukuAlatLongitudinal[] List of BukuAlatLongitudinal objects
     * @throws PropelException
     */
    public function getBukuAlatLongitudinalsRelatedByBukuAlatId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collBukuAlatLongitudinalsRelatedByBukuAlatIdPartial && !$this->isNew();
        if (null === $this->collBukuAlatLongitudinalsRelatedByBukuAlatId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBukuAlatLongitudinalsRelatedByBukuAlatId) {
                // return empty collection
                $this->initBukuAlatLongitudinalsRelatedByBukuAlatId();
            } else {
                $collBukuAlatLongitudinalsRelatedByBukuAlatId = BukuAlatLongitudinalQuery::create(null, $criteria)
                    ->filterByBukuAlatRelatedByBukuAlatId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collBukuAlatLongitudinalsRelatedByBukuAlatIdPartial && count($collBukuAlatLongitudinalsRelatedByBukuAlatId)) {
                      $this->initBukuAlatLongitudinalsRelatedByBukuAlatId(false);

                      foreach($collBukuAlatLongitudinalsRelatedByBukuAlatId as $obj) {
                        if (false == $this->collBukuAlatLongitudinalsRelatedByBukuAlatId->contains($obj)) {
                          $this->collBukuAlatLongitudinalsRelatedByBukuAlatId->append($obj);
                        }
                      }

                      $this->collBukuAlatLongitudinalsRelatedByBukuAlatIdPartial = true;
                    }

                    $collBukuAlatLongitudinalsRelatedByBukuAlatId->getInternalIterator()->rewind();
                    return $collBukuAlatLongitudinalsRelatedByBukuAlatId;
                }

                if($partial && $this->collBukuAlatLongitudinalsRelatedByBukuAlatId) {
                    foreach($this->collBukuAlatLongitudinalsRelatedByBukuAlatId as $obj) {
                        if($obj->isNew()) {
                            $collBukuAlatLongitudinalsRelatedByBukuAlatId[] = $obj;
                        }
                    }
                }

                $this->collBukuAlatLongitudinalsRelatedByBukuAlatId = $collBukuAlatLongitudinalsRelatedByBukuAlatId;
                $this->collBukuAlatLongitudinalsRelatedByBukuAlatIdPartial = false;
            }
        }

        return $this->collBukuAlatLongitudinalsRelatedByBukuAlatId;
    }

    /**
     * Sets a collection of BukuAlatLongitudinalRelatedByBukuAlatId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $bukuAlatLongitudinalsRelatedByBukuAlatId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return BukuAlat The current object (for fluent API support)
     */
    public function setBukuAlatLongitudinalsRelatedByBukuAlatId(PropelCollection $bukuAlatLongitudinalsRelatedByBukuAlatId, PropelPDO $con = null)
    {
        $bukuAlatLongitudinalsRelatedByBukuAlatIdToDelete = $this->getBukuAlatLongitudinalsRelatedByBukuAlatId(new Criteria(), $con)->diff($bukuAlatLongitudinalsRelatedByBukuAlatId);

        $this->bukuAlatLongitudinalsRelatedByBukuAlatIdScheduledForDeletion = unserialize(serialize($bukuAlatLongitudinalsRelatedByBukuAlatIdToDelete));

        foreach ($bukuAlatLongitudinalsRelatedByBukuAlatIdToDelete as $bukuAlatLongitudinalRelatedByBukuAlatIdRemoved) {
            $bukuAlatLongitudinalRelatedByBukuAlatIdRemoved->setBukuAlatRelatedByBukuAlatId(null);
        }

        $this->collBukuAlatLongitudinalsRelatedByBukuAlatId = null;
        foreach ($bukuAlatLongitudinalsRelatedByBukuAlatId as $bukuAlatLongitudinalRelatedByBukuAlatId) {
            $this->addBukuAlatLongitudinalRelatedByBukuAlatId($bukuAlatLongitudinalRelatedByBukuAlatId);
        }

        $this->collBukuAlatLongitudinalsRelatedByBukuAlatId = $bukuAlatLongitudinalsRelatedByBukuAlatId;
        $this->collBukuAlatLongitudinalsRelatedByBukuAlatIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BukuAlatLongitudinal objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related BukuAlatLongitudinal objects.
     * @throws PropelException
     */
    public function countBukuAlatLongitudinalsRelatedByBukuAlatId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collBukuAlatLongitudinalsRelatedByBukuAlatIdPartial && !$this->isNew();
        if (null === $this->collBukuAlatLongitudinalsRelatedByBukuAlatId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBukuAlatLongitudinalsRelatedByBukuAlatId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getBukuAlatLongitudinalsRelatedByBukuAlatId());
            }
            $query = BukuAlatLongitudinalQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByBukuAlatRelatedByBukuAlatId($this)
                ->count($con);
        }

        return count($this->collBukuAlatLongitudinalsRelatedByBukuAlatId);
    }

    /**
     * Method called to associate a BukuAlatLongitudinal object to this object
     * through the BukuAlatLongitudinal foreign key attribute.
     *
     * @param    BukuAlatLongitudinal $l BukuAlatLongitudinal
     * @return BukuAlat The current object (for fluent API support)
     */
    public function addBukuAlatLongitudinalRelatedByBukuAlatId(BukuAlatLongitudinal $l)
    {
        if ($this->collBukuAlatLongitudinalsRelatedByBukuAlatId === null) {
            $this->initBukuAlatLongitudinalsRelatedByBukuAlatId();
            $this->collBukuAlatLongitudinalsRelatedByBukuAlatIdPartial = true;
        }
        if (!in_array($l, $this->collBukuAlatLongitudinalsRelatedByBukuAlatId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddBukuAlatLongitudinalRelatedByBukuAlatId($l);
        }

        return $this;
    }

    /**
     * @param	BukuAlatLongitudinalRelatedByBukuAlatId $bukuAlatLongitudinalRelatedByBukuAlatId The bukuAlatLongitudinalRelatedByBukuAlatId object to add.
     */
    protected function doAddBukuAlatLongitudinalRelatedByBukuAlatId($bukuAlatLongitudinalRelatedByBukuAlatId)
    {
        $this->collBukuAlatLongitudinalsRelatedByBukuAlatId[]= $bukuAlatLongitudinalRelatedByBukuAlatId;
        $bukuAlatLongitudinalRelatedByBukuAlatId->setBukuAlatRelatedByBukuAlatId($this);
    }

    /**
     * @param	BukuAlatLongitudinalRelatedByBukuAlatId $bukuAlatLongitudinalRelatedByBukuAlatId The bukuAlatLongitudinalRelatedByBukuAlatId object to remove.
     * @return BukuAlat The current object (for fluent API support)
     */
    public function removeBukuAlatLongitudinalRelatedByBukuAlatId($bukuAlatLongitudinalRelatedByBukuAlatId)
    {
        if ($this->getBukuAlatLongitudinalsRelatedByBukuAlatId()->contains($bukuAlatLongitudinalRelatedByBukuAlatId)) {
            $this->collBukuAlatLongitudinalsRelatedByBukuAlatId->remove($this->collBukuAlatLongitudinalsRelatedByBukuAlatId->search($bukuAlatLongitudinalRelatedByBukuAlatId));
            if (null === $this->bukuAlatLongitudinalsRelatedByBukuAlatIdScheduledForDeletion) {
                $this->bukuAlatLongitudinalsRelatedByBukuAlatIdScheduledForDeletion = clone $this->collBukuAlatLongitudinalsRelatedByBukuAlatId;
                $this->bukuAlatLongitudinalsRelatedByBukuAlatIdScheduledForDeletion->clear();
            }
            $this->bukuAlatLongitudinalsRelatedByBukuAlatIdScheduledForDeletion[]= clone $bukuAlatLongitudinalRelatedByBukuAlatId;
            $bukuAlatLongitudinalRelatedByBukuAlatId->setBukuAlatRelatedByBukuAlatId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BukuAlat is new, it will return
     * an empty collection; or if this BukuAlat has previously
     * been saved, it will retrieve related BukuAlatLongitudinalsRelatedByBukuAlatId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BukuAlat.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BukuAlatLongitudinal[] List of BukuAlatLongitudinal objects
     */
    public function getBukuAlatLongitudinalsRelatedByBukuAlatIdJoinSemesterRelatedBySemesterId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BukuAlatLongitudinalQuery::create(null, $criteria);
        $query->joinWith('SemesterRelatedBySemesterId', $join_behavior);

        return $this->getBukuAlatLongitudinalsRelatedByBukuAlatId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BukuAlat is new, it will return
     * an empty collection; or if this BukuAlat has previously
     * been saved, it will retrieve related BukuAlatLongitudinalsRelatedByBukuAlatId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BukuAlat.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BukuAlatLongitudinal[] List of BukuAlatLongitudinal objects
     */
    public function getBukuAlatLongitudinalsRelatedByBukuAlatIdJoinSemesterRelatedBySemesterId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BukuAlatLongitudinalQuery::create(null, $criteria);
        $query->joinWith('SemesterRelatedBySemesterId', $join_behavior);

        return $this->getBukuAlatLongitudinalsRelatedByBukuAlatId($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->buku_alat_id = null;
        $this->mata_pelajaran_id = null;
        $this->prasarana_id = null;
        $this->sekolah_id = null;
        $this->tingkat_pendidikan_id = null;
        $this->jenis_buku_alat_id = null;
        $this->buku_alat = null;
        $this->last_update = null;
        $this->soft_delete = null;
        $this->last_sync = null;
        $this->updater_id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collBukuAlatLongitudinalsRelatedByBukuAlatId) {
                foreach ($this->collBukuAlatLongitudinalsRelatedByBukuAlatId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collBukuAlatLongitudinalsRelatedByBukuAlatId) {
                foreach ($this->collBukuAlatLongitudinalsRelatedByBukuAlatId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aPrasaranaRelatedByPrasaranaId instanceof Persistent) {
              $this->aPrasaranaRelatedByPrasaranaId->clearAllReferences($deep);
            }
            if ($this->aPrasaranaRelatedByPrasaranaId instanceof Persistent) {
              $this->aPrasaranaRelatedByPrasaranaId->clearAllReferences($deep);
            }
            if ($this->aSekolahRelatedBySekolahId instanceof Persistent) {
              $this->aSekolahRelatedBySekolahId->clearAllReferences($deep);
            }
            if ($this->aSekolahRelatedBySekolahId instanceof Persistent) {
              $this->aSekolahRelatedBySekolahId->clearAllReferences($deep);
            }
            if ($this->aJenisBukuAlatRelatedByJenisBukuAlatId instanceof Persistent) {
              $this->aJenisBukuAlatRelatedByJenisBukuAlatId->clearAllReferences($deep);
            }
            if ($this->aJenisBukuAlatRelatedByJenisBukuAlatId instanceof Persistent) {
              $this->aJenisBukuAlatRelatedByJenisBukuAlatId->clearAllReferences($deep);
            }
            if ($this->aMataPelajaranRelatedByMataPelajaranId instanceof Persistent) {
              $this->aMataPelajaranRelatedByMataPelajaranId->clearAllReferences($deep);
            }
            if ($this->aMataPelajaranRelatedByMataPelajaranId instanceof Persistent) {
              $this->aMataPelajaranRelatedByMataPelajaranId->clearAllReferences($deep);
            }
            if ($this->aTingkatPendidikanRelatedByTingkatPendidikanId instanceof Persistent) {
              $this->aTingkatPendidikanRelatedByTingkatPendidikanId->clearAllReferences($deep);
            }
            if ($this->aTingkatPendidikanRelatedByTingkatPendidikanId instanceof Persistent) {
              $this->aTingkatPendidikanRelatedByTingkatPendidikanId->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collBukuAlatLongitudinalsRelatedByBukuAlatId instanceof PropelCollection) {
            $this->collBukuAlatLongitudinalsRelatedByBukuAlatId->clearIterator();
        }
        $this->collBukuAlatLongitudinalsRelatedByBukuAlatId = null;
        if ($this->collBukuAlatLongitudinalsRelatedByBukuAlatId instanceof PropelCollection) {
            $this->collBukuAlatLongitudinalsRelatedByBukuAlatId->clearIterator();
        }
        $this->collBukuAlatLongitudinalsRelatedByBukuAlatId = null;
        $this->aPrasaranaRelatedByPrasaranaId = null;
        $this->aPrasaranaRelatedByPrasaranaId = null;
        $this->aSekolahRelatedBySekolahId = null;
        $this->aSekolahRelatedBySekolahId = null;
        $this->aJenisBukuAlatRelatedByJenisBukuAlatId = null;
        $this->aJenisBukuAlatRelatedByJenisBukuAlatId = null;
        $this->aMataPelajaranRelatedByMataPelajaranId = null;
        $this->aMataPelajaranRelatedByMataPelajaranId = null;
        $this->aTingkatPendidikanRelatedByTingkatPendidikanId = null;
        $this->aTingkatPendidikanRelatedByTingkatPendidikanId = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(BukuAlatPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
