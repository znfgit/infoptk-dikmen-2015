<?php

namespace angulex\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelDateTime;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\JenisTest;
use angulex\Model\JenisTestQuery;
use angulex\Model\NilaiTest;
use angulex\Model\NilaiTestPeer;
use angulex\Model\NilaiTestQuery;
use angulex\Model\Ptk;
use angulex\Model\PtkQuery;
use angulex\Model\VldNilaiTest;
use angulex\Model\VldNilaiTestQuery;

/**
 * Base class that represents a row from the 'nilai_test' table.
 *
 * 
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseNilaiTest extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'angulex\\Model\\NilaiTestPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        NilaiTestPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the nilai_test_id field.
     * @var        string
     */
    protected $nilai_test_id;

    /**
     * The value for the jenis_test_id field.
     * @var        string
     */
    protected $jenis_test_id;

    /**
     * The value for the ptk_id field.
     * @var        string
     */
    protected $ptk_id;

    /**
     * The value for the nama field.
     * @var        string
     */
    protected $nama;

    /**
     * The value for the penyelenggara field.
     * @var        string
     */
    protected $penyelenggara;

    /**
     * The value for the tahun field.
     * @var        string
     */
    protected $tahun;

    /**
     * The value for the skor field.
     * @var        string
     */
    protected $skor;

    /**
     * The value for the last_update field.
     * @var        string
     */
    protected $last_update;

    /**
     * The value for the soft_delete field.
     * @var        string
     */
    protected $soft_delete;

    /**
     * The value for the last_sync field.
     * @var        string
     */
    protected $last_sync;

    /**
     * The value for the updater_id field.
     * @var        string
     */
    protected $updater_id;

    /**
     * @var        Ptk
     */
    protected $aPtkRelatedByPtkId;

    /**
     * @var        Ptk
     */
    protected $aPtkRelatedByPtkId;

    /**
     * @var        JenisTest
     */
    protected $aJenisTestRelatedByJenisTestId;

    /**
     * @var        JenisTest
     */
    protected $aJenisTestRelatedByJenisTestId;

    /**
     * @var        PropelObjectCollection|VldNilaiTest[] Collection to store aggregation of VldNilaiTest objects.
     */
    protected $collVldNilaiTestsRelatedByNilaiTestId;
    protected $collVldNilaiTestsRelatedByNilaiTestIdPartial;

    /**
     * @var        PropelObjectCollection|VldNilaiTest[] Collection to store aggregation of VldNilaiTest objects.
     */
    protected $collVldNilaiTestsRelatedByNilaiTestId;
    protected $collVldNilaiTestsRelatedByNilaiTestIdPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldNilaiTestsRelatedByNilaiTestIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldNilaiTestsRelatedByNilaiTestIdScheduledForDeletion = null;

    /**
     * Get the [nilai_test_id] column value.
     * 
     * @return string
     */
    public function getNilaiTestId()
    {
        return $this->nilai_test_id;
    }

    /**
     * Get the [jenis_test_id] column value.
     * 
     * @return string
     */
    public function getJenisTestId()
    {
        return $this->jenis_test_id;
    }

    /**
     * Get the [ptk_id] column value.
     * 
     * @return string
     */
    public function getPtkId()
    {
        return $this->ptk_id;
    }

    /**
     * Get the [nama] column value.
     * 
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Get the [penyelenggara] column value.
     * 
     * @return string
     */
    public function getPenyelenggara()
    {
        return $this->penyelenggara;
    }

    /**
     * Get the [tahun] column value.
     * 
     * @return string
     */
    public function getTahun()
    {
        return $this->tahun;
    }

    /**
     * Get the [skor] column value.
     * 
     * @return string
     */
    public function getSkor()
    {
        return $this->skor;
    }

    /**
     * Get the [optionally formatted] temporal [last_update] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastUpdate($format = 'Y-m-d H:i:s')
    {
        if ($this->last_update === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_update);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_update, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [soft_delete] column value.
     * 
     * @return string
     */
    public function getSoftDelete()
    {
        return $this->soft_delete;
    }

    /**
     * Get the [optionally formatted] temporal [last_sync] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastSync($format = 'Y-m-d H:i:s')
    {
        if ($this->last_sync === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_sync);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_sync, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [updater_id] column value.
     * 
     * @return string
     */
    public function getUpdaterId()
    {
        return $this->updater_id;
    }

    /**
     * Set the value of [nilai_test_id] column.
     * 
     * @param string $v new value
     * @return NilaiTest The current object (for fluent API support)
     */
    public function setNilaiTestId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nilai_test_id !== $v) {
            $this->nilai_test_id = $v;
            $this->modifiedColumns[] = NilaiTestPeer::NILAI_TEST_ID;
        }


        return $this;
    } // setNilaiTestId()

    /**
     * Set the value of [jenis_test_id] column.
     * 
     * @param string $v new value
     * @return NilaiTest The current object (for fluent API support)
     */
    public function setJenisTestId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->jenis_test_id !== $v) {
            $this->jenis_test_id = $v;
            $this->modifiedColumns[] = NilaiTestPeer::JENIS_TEST_ID;
        }

        if ($this->aJenisTestRelatedByJenisTestId !== null && $this->aJenisTestRelatedByJenisTestId->getJenisTestId() !== $v) {
            $this->aJenisTestRelatedByJenisTestId = null;
        }

        if ($this->aJenisTestRelatedByJenisTestId !== null && $this->aJenisTestRelatedByJenisTestId->getJenisTestId() !== $v) {
            $this->aJenisTestRelatedByJenisTestId = null;
        }


        return $this;
    } // setJenisTestId()

    /**
     * Set the value of [ptk_id] column.
     * 
     * @param string $v new value
     * @return NilaiTest The current object (for fluent API support)
     */
    public function setPtkId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->ptk_id !== $v) {
            $this->ptk_id = $v;
            $this->modifiedColumns[] = NilaiTestPeer::PTK_ID;
        }

        if ($this->aPtkRelatedByPtkId !== null && $this->aPtkRelatedByPtkId->getPtkId() !== $v) {
            $this->aPtkRelatedByPtkId = null;
        }

        if ($this->aPtkRelatedByPtkId !== null && $this->aPtkRelatedByPtkId->getPtkId() !== $v) {
            $this->aPtkRelatedByPtkId = null;
        }


        return $this;
    } // setPtkId()

    /**
     * Set the value of [nama] column.
     * 
     * @param string $v new value
     * @return NilaiTest The current object (for fluent API support)
     */
    public function setNama($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nama !== $v) {
            $this->nama = $v;
            $this->modifiedColumns[] = NilaiTestPeer::NAMA;
        }


        return $this;
    } // setNama()

    /**
     * Set the value of [penyelenggara] column.
     * 
     * @param string $v new value
     * @return NilaiTest The current object (for fluent API support)
     */
    public function setPenyelenggara($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->penyelenggara !== $v) {
            $this->penyelenggara = $v;
            $this->modifiedColumns[] = NilaiTestPeer::PENYELENGGARA;
        }


        return $this;
    } // setPenyelenggara()

    /**
     * Set the value of [tahun] column.
     * 
     * @param string $v new value
     * @return NilaiTest The current object (for fluent API support)
     */
    public function setTahun($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tahun !== $v) {
            $this->tahun = $v;
            $this->modifiedColumns[] = NilaiTestPeer::TAHUN;
        }


        return $this;
    } // setTahun()

    /**
     * Set the value of [skor] column.
     * 
     * @param string $v new value
     * @return NilaiTest The current object (for fluent API support)
     */
    public function setSkor($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->skor !== $v) {
            $this->skor = $v;
            $this->modifiedColumns[] = NilaiTestPeer::SKOR;
        }


        return $this;
    } // setSkor()

    /**
     * Sets the value of [last_update] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return NilaiTest The current object (for fluent API support)
     */
    public function setLastUpdate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_update !== null || $dt !== null) {
            $currentDateAsString = ($this->last_update !== null && $tmpDt = new DateTime($this->last_update)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_update = $newDateAsString;
                $this->modifiedColumns[] = NilaiTestPeer::LAST_UPDATE;
            }
        } // if either are not null


        return $this;
    } // setLastUpdate()

    /**
     * Set the value of [soft_delete] column.
     * 
     * @param string $v new value
     * @return NilaiTest The current object (for fluent API support)
     */
    public function setSoftDelete($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->soft_delete !== $v) {
            $this->soft_delete = $v;
            $this->modifiedColumns[] = NilaiTestPeer::SOFT_DELETE;
        }


        return $this;
    } // setSoftDelete()

    /**
     * Sets the value of [last_sync] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return NilaiTest The current object (for fluent API support)
     */
    public function setLastSync($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_sync !== null || $dt !== null) {
            $currentDateAsString = ($this->last_sync !== null && $tmpDt = new DateTime($this->last_sync)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_sync = $newDateAsString;
                $this->modifiedColumns[] = NilaiTestPeer::LAST_SYNC;
            }
        } // if either are not null


        return $this;
    } // setLastSync()

    /**
     * Set the value of [updater_id] column.
     * 
     * @param string $v new value
     * @return NilaiTest The current object (for fluent API support)
     */
    public function setUpdaterId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->updater_id !== $v) {
            $this->updater_id = $v;
            $this->modifiedColumns[] = NilaiTestPeer::UPDATER_ID;
        }


        return $this;
    } // setUpdaterId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->nilai_test_id = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
            $this->jenis_test_id = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->ptk_id = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->nama = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->penyelenggara = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->tahun = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->skor = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->last_update = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->soft_delete = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->last_sync = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->updater_id = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 11; // 11 = NilaiTestPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating NilaiTest object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aJenisTestRelatedByJenisTestId !== null && $this->jenis_test_id !== $this->aJenisTestRelatedByJenisTestId->getJenisTestId()) {
            $this->aJenisTestRelatedByJenisTestId = null;
        }
        if ($this->aJenisTestRelatedByJenisTestId !== null && $this->jenis_test_id !== $this->aJenisTestRelatedByJenisTestId->getJenisTestId()) {
            $this->aJenisTestRelatedByJenisTestId = null;
        }
        if ($this->aPtkRelatedByPtkId !== null && $this->ptk_id !== $this->aPtkRelatedByPtkId->getPtkId()) {
            $this->aPtkRelatedByPtkId = null;
        }
        if ($this->aPtkRelatedByPtkId !== null && $this->ptk_id !== $this->aPtkRelatedByPtkId->getPtkId()) {
            $this->aPtkRelatedByPtkId = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(NilaiTestPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = NilaiTestPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aPtkRelatedByPtkId = null;
            $this->aPtkRelatedByPtkId = null;
            $this->aJenisTestRelatedByJenisTestId = null;
            $this->aJenisTestRelatedByJenisTestId = null;
            $this->collVldNilaiTestsRelatedByNilaiTestId = null;

            $this->collVldNilaiTestsRelatedByNilaiTestId = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(NilaiTestPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = NilaiTestQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(NilaiTestPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                NilaiTestPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPtkRelatedByPtkId !== null) {
                if ($this->aPtkRelatedByPtkId->isModified() || $this->aPtkRelatedByPtkId->isNew()) {
                    $affectedRows += $this->aPtkRelatedByPtkId->save($con);
                }
                $this->setPtkRelatedByPtkId($this->aPtkRelatedByPtkId);
            }

            if ($this->aPtkRelatedByPtkId !== null) {
                if ($this->aPtkRelatedByPtkId->isModified() || $this->aPtkRelatedByPtkId->isNew()) {
                    $affectedRows += $this->aPtkRelatedByPtkId->save($con);
                }
                $this->setPtkRelatedByPtkId($this->aPtkRelatedByPtkId);
            }

            if ($this->aJenisTestRelatedByJenisTestId !== null) {
                if ($this->aJenisTestRelatedByJenisTestId->isModified() || $this->aJenisTestRelatedByJenisTestId->isNew()) {
                    $affectedRows += $this->aJenisTestRelatedByJenisTestId->save($con);
                }
                $this->setJenisTestRelatedByJenisTestId($this->aJenisTestRelatedByJenisTestId);
            }

            if ($this->aJenisTestRelatedByJenisTestId !== null) {
                if ($this->aJenisTestRelatedByJenisTestId->isModified() || $this->aJenisTestRelatedByJenisTestId->isNew()) {
                    $affectedRows += $this->aJenisTestRelatedByJenisTestId->save($con);
                }
                $this->setJenisTestRelatedByJenisTestId($this->aJenisTestRelatedByJenisTestId);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->vldNilaiTestsRelatedByNilaiTestIdScheduledForDeletion !== null) {
                if (!$this->vldNilaiTestsRelatedByNilaiTestIdScheduledForDeletion->isEmpty()) {
                    VldNilaiTestQuery::create()
                        ->filterByPrimaryKeys($this->vldNilaiTestsRelatedByNilaiTestIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldNilaiTestsRelatedByNilaiTestIdScheduledForDeletion = null;
                }
            }

            if ($this->collVldNilaiTestsRelatedByNilaiTestId !== null) {
                foreach ($this->collVldNilaiTestsRelatedByNilaiTestId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldNilaiTestsRelatedByNilaiTestIdScheduledForDeletion !== null) {
                if (!$this->vldNilaiTestsRelatedByNilaiTestIdScheduledForDeletion->isEmpty()) {
                    VldNilaiTestQuery::create()
                        ->filterByPrimaryKeys($this->vldNilaiTestsRelatedByNilaiTestIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldNilaiTestsRelatedByNilaiTestIdScheduledForDeletion = null;
                }
            }

            if ($this->collVldNilaiTestsRelatedByNilaiTestId !== null) {
                foreach ($this->collVldNilaiTestsRelatedByNilaiTestId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $criteria = $this->buildCriteria();
        $pk = BasePeer::doInsert($criteria, $con);
        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPtkRelatedByPtkId !== null) {
                if (!$this->aPtkRelatedByPtkId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPtkRelatedByPtkId->getValidationFailures());
                }
            }

            if ($this->aPtkRelatedByPtkId !== null) {
                if (!$this->aPtkRelatedByPtkId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPtkRelatedByPtkId->getValidationFailures());
                }
            }

            if ($this->aJenisTestRelatedByJenisTestId !== null) {
                if (!$this->aJenisTestRelatedByJenisTestId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aJenisTestRelatedByJenisTestId->getValidationFailures());
                }
            }

            if ($this->aJenisTestRelatedByJenisTestId !== null) {
                if (!$this->aJenisTestRelatedByJenisTestId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aJenisTestRelatedByJenisTestId->getValidationFailures());
                }
            }


            if (($retval = NilaiTestPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collVldNilaiTestsRelatedByNilaiTestId !== null) {
                    foreach ($this->collVldNilaiTestsRelatedByNilaiTestId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldNilaiTestsRelatedByNilaiTestId !== null) {
                    foreach ($this->collVldNilaiTestsRelatedByNilaiTestId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = NilaiTestPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getNilaiTestId();
                break;
            case 1:
                return $this->getJenisTestId();
                break;
            case 2:
                return $this->getPtkId();
                break;
            case 3:
                return $this->getNama();
                break;
            case 4:
                return $this->getPenyelenggara();
                break;
            case 5:
                return $this->getTahun();
                break;
            case 6:
                return $this->getSkor();
                break;
            case 7:
                return $this->getLastUpdate();
                break;
            case 8:
                return $this->getSoftDelete();
                break;
            case 9:
                return $this->getLastSync();
                break;
            case 10:
                return $this->getUpdaterId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['NilaiTest'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['NilaiTest'][$this->getPrimaryKey()] = true;
        $keys = NilaiTestPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getNilaiTestId(),
            $keys[1] => $this->getJenisTestId(),
            $keys[2] => $this->getPtkId(),
            $keys[3] => $this->getNama(),
            $keys[4] => $this->getPenyelenggara(),
            $keys[5] => $this->getTahun(),
            $keys[6] => $this->getSkor(),
            $keys[7] => $this->getLastUpdate(),
            $keys[8] => $this->getSoftDelete(),
            $keys[9] => $this->getLastSync(),
            $keys[10] => $this->getUpdaterId(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aPtkRelatedByPtkId) {
                $result['PtkRelatedByPtkId'] = $this->aPtkRelatedByPtkId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPtkRelatedByPtkId) {
                $result['PtkRelatedByPtkId'] = $this->aPtkRelatedByPtkId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aJenisTestRelatedByJenisTestId) {
                $result['JenisTestRelatedByJenisTestId'] = $this->aJenisTestRelatedByJenisTestId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aJenisTestRelatedByJenisTestId) {
                $result['JenisTestRelatedByJenisTestId'] = $this->aJenisTestRelatedByJenisTestId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collVldNilaiTestsRelatedByNilaiTestId) {
                $result['VldNilaiTestsRelatedByNilaiTestId'] = $this->collVldNilaiTestsRelatedByNilaiTestId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldNilaiTestsRelatedByNilaiTestId) {
                $result['VldNilaiTestsRelatedByNilaiTestId'] = $this->collVldNilaiTestsRelatedByNilaiTestId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = NilaiTestPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setNilaiTestId($value);
                break;
            case 1:
                $this->setJenisTestId($value);
                break;
            case 2:
                $this->setPtkId($value);
                break;
            case 3:
                $this->setNama($value);
                break;
            case 4:
                $this->setPenyelenggara($value);
                break;
            case 5:
                $this->setTahun($value);
                break;
            case 6:
                $this->setSkor($value);
                break;
            case 7:
                $this->setLastUpdate($value);
                break;
            case 8:
                $this->setSoftDelete($value);
                break;
            case 9:
                $this->setLastSync($value);
                break;
            case 10:
                $this->setUpdaterId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = NilaiTestPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setNilaiTestId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setJenisTestId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setPtkId($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setNama($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setPenyelenggara($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setTahun($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setSkor($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setLastUpdate($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setSoftDelete($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setLastSync($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setUpdaterId($arr[$keys[10]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(NilaiTestPeer::DATABASE_NAME);

        if ($this->isColumnModified(NilaiTestPeer::NILAI_TEST_ID)) $criteria->add(NilaiTestPeer::NILAI_TEST_ID, $this->nilai_test_id);
        if ($this->isColumnModified(NilaiTestPeer::JENIS_TEST_ID)) $criteria->add(NilaiTestPeer::JENIS_TEST_ID, $this->jenis_test_id);
        if ($this->isColumnModified(NilaiTestPeer::PTK_ID)) $criteria->add(NilaiTestPeer::PTK_ID, $this->ptk_id);
        if ($this->isColumnModified(NilaiTestPeer::NAMA)) $criteria->add(NilaiTestPeer::NAMA, $this->nama);
        if ($this->isColumnModified(NilaiTestPeer::PENYELENGGARA)) $criteria->add(NilaiTestPeer::PENYELENGGARA, $this->penyelenggara);
        if ($this->isColumnModified(NilaiTestPeer::TAHUN)) $criteria->add(NilaiTestPeer::TAHUN, $this->tahun);
        if ($this->isColumnModified(NilaiTestPeer::SKOR)) $criteria->add(NilaiTestPeer::SKOR, $this->skor);
        if ($this->isColumnModified(NilaiTestPeer::LAST_UPDATE)) $criteria->add(NilaiTestPeer::LAST_UPDATE, $this->last_update);
        if ($this->isColumnModified(NilaiTestPeer::SOFT_DELETE)) $criteria->add(NilaiTestPeer::SOFT_DELETE, $this->soft_delete);
        if ($this->isColumnModified(NilaiTestPeer::LAST_SYNC)) $criteria->add(NilaiTestPeer::LAST_SYNC, $this->last_sync);
        if ($this->isColumnModified(NilaiTestPeer::UPDATER_ID)) $criteria->add(NilaiTestPeer::UPDATER_ID, $this->updater_id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(NilaiTestPeer::DATABASE_NAME);
        $criteria->add(NilaiTestPeer::NILAI_TEST_ID, $this->nilai_test_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getNilaiTestId();
    }

    /**
     * Generic method to set the primary key (nilai_test_id column).
     *
     * @param  string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setNilaiTestId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getNilaiTestId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of NilaiTest (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setJenisTestId($this->getJenisTestId());
        $copyObj->setPtkId($this->getPtkId());
        $copyObj->setNama($this->getNama());
        $copyObj->setPenyelenggara($this->getPenyelenggara());
        $copyObj->setTahun($this->getTahun());
        $copyObj->setSkor($this->getSkor());
        $copyObj->setLastUpdate($this->getLastUpdate());
        $copyObj->setSoftDelete($this->getSoftDelete());
        $copyObj->setLastSync($this->getLastSync());
        $copyObj->setUpdaterId($this->getUpdaterId());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getVldNilaiTestsRelatedByNilaiTestId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldNilaiTestRelatedByNilaiTestId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldNilaiTestsRelatedByNilaiTestId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldNilaiTestRelatedByNilaiTestId($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setNilaiTestId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return NilaiTest Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return NilaiTestPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new NilaiTestPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a Ptk object.
     *
     * @param             Ptk $v
     * @return NilaiTest The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPtkRelatedByPtkId(Ptk $v = null)
    {
        if ($v === null) {
            $this->setPtkId(NULL);
        } else {
            $this->setPtkId($v->getPtkId());
        }

        $this->aPtkRelatedByPtkId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Ptk object, it will not be re-added.
        if ($v !== null) {
            $v->addNilaiTestRelatedByPtkId($this);
        }


        return $this;
    }


    /**
     * Get the associated Ptk object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Ptk The associated Ptk object.
     * @throws PropelException
     */
    public function getPtkRelatedByPtkId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPtkRelatedByPtkId === null && (($this->ptk_id !== "" && $this->ptk_id !== null)) && $doQuery) {
            $this->aPtkRelatedByPtkId = PtkQuery::create()->findPk($this->ptk_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPtkRelatedByPtkId->addNilaiTestsRelatedByPtkId($this);
             */
        }

        return $this->aPtkRelatedByPtkId;
    }

    /**
     * Declares an association between this object and a Ptk object.
     *
     * @param             Ptk $v
     * @return NilaiTest The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPtkRelatedByPtkId(Ptk $v = null)
    {
        if ($v === null) {
            $this->setPtkId(NULL);
        } else {
            $this->setPtkId($v->getPtkId());
        }

        $this->aPtkRelatedByPtkId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Ptk object, it will not be re-added.
        if ($v !== null) {
            $v->addNilaiTestRelatedByPtkId($this);
        }


        return $this;
    }


    /**
     * Get the associated Ptk object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Ptk The associated Ptk object.
     * @throws PropelException
     */
    public function getPtkRelatedByPtkId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPtkRelatedByPtkId === null && (($this->ptk_id !== "" && $this->ptk_id !== null)) && $doQuery) {
            $this->aPtkRelatedByPtkId = PtkQuery::create()->findPk($this->ptk_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPtkRelatedByPtkId->addNilaiTestsRelatedByPtkId($this);
             */
        }

        return $this->aPtkRelatedByPtkId;
    }

    /**
     * Declares an association between this object and a JenisTest object.
     *
     * @param             JenisTest $v
     * @return NilaiTest The current object (for fluent API support)
     * @throws PropelException
     */
    public function setJenisTestRelatedByJenisTestId(JenisTest $v = null)
    {
        if ($v === null) {
            $this->setJenisTestId(NULL);
        } else {
            $this->setJenisTestId($v->getJenisTestId());
        }

        $this->aJenisTestRelatedByJenisTestId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the JenisTest object, it will not be re-added.
        if ($v !== null) {
            $v->addNilaiTestRelatedByJenisTestId($this);
        }


        return $this;
    }


    /**
     * Get the associated JenisTest object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return JenisTest The associated JenisTest object.
     * @throws PropelException
     */
    public function getJenisTestRelatedByJenisTestId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aJenisTestRelatedByJenisTestId === null && (($this->jenis_test_id !== "" && $this->jenis_test_id !== null)) && $doQuery) {
            $this->aJenisTestRelatedByJenisTestId = JenisTestQuery::create()->findPk($this->jenis_test_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aJenisTestRelatedByJenisTestId->addNilaiTestsRelatedByJenisTestId($this);
             */
        }

        return $this->aJenisTestRelatedByJenisTestId;
    }

    /**
     * Declares an association between this object and a JenisTest object.
     *
     * @param             JenisTest $v
     * @return NilaiTest The current object (for fluent API support)
     * @throws PropelException
     */
    public function setJenisTestRelatedByJenisTestId(JenisTest $v = null)
    {
        if ($v === null) {
            $this->setJenisTestId(NULL);
        } else {
            $this->setJenisTestId($v->getJenisTestId());
        }

        $this->aJenisTestRelatedByJenisTestId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the JenisTest object, it will not be re-added.
        if ($v !== null) {
            $v->addNilaiTestRelatedByJenisTestId($this);
        }


        return $this;
    }


    /**
     * Get the associated JenisTest object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return JenisTest The associated JenisTest object.
     * @throws PropelException
     */
    public function getJenisTestRelatedByJenisTestId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aJenisTestRelatedByJenisTestId === null && (($this->jenis_test_id !== "" && $this->jenis_test_id !== null)) && $doQuery) {
            $this->aJenisTestRelatedByJenisTestId = JenisTestQuery::create()->findPk($this->jenis_test_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aJenisTestRelatedByJenisTestId->addNilaiTestsRelatedByJenisTestId($this);
             */
        }

        return $this->aJenisTestRelatedByJenisTestId;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('VldNilaiTestRelatedByNilaiTestId' == $relationName) {
            $this->initVldNilaiTestsRelatedByNilaiTestId();
        }
        if ('VldNilaiTestRelatedByNilaiTestId' == $relationName) {
            $this->initVldNilaiTestsRelatedByNilaiTestId();
        }
    }

    /**
     * Clears out the collVldNilaiTestsRelatedByNilaiTestId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return NilaiTest The current object (for fluent API support)
     * @see        addVldNilaiTestsRelatedByNilaiTestId()
     */
    public function clearVldNilaiTestsRelatedByNilaiTestId()
    {
        $this->collVldNilaiTestsRelatedByNilaiTestId = null; // important to set this to null since that means it is uninitialized
        $this->collVldNilaiTestsRelatedByNilaiTestIdPartial = null;

        return $this;
    }

    /**
     * reset is the collVldNilaiTestsRelatedByNilaiTestId collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldNilaiTestsRelatedByNilaiTestId($v = true)
    {
        $this->collVldNilaiTestsRelatedByNilaiTestIdPartial = $v;
    }

    /**
     * Initializes the collVldNilaiTestsRelatedByNilaiTestId collection.
     *
     * By default this just sets the collVldNilaiTestsRelatedByNilaiTestId collection to an empty array (like clearcollVldNilaiTestsRelatedByNilaiTestId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldNilaiTestsRelatedByNilaiTestId($overrideExisting = true)
    {
        if (null !== $this->collVldNilaiTestsRelatedByNilaiTestId && !$overrideExisting) {
            return;
        }
        $this->collVldNilaiTestsRelatedByNilaiTestId = new PropelObjectCollection();
        $this->collVldNilaiTestsRelatedByNilaiTestId->setModel('VldNilaiTest');
    }

    /**
     * Gets an array of VldNilaiTest objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this NilaiTest is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldNilaiTest[] List of VldNilaiTest objects
     * @throws PropelException
     */
    public function getVldNilaiTestsRelatedByNilaiTestId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldNilaiTestsRelatedByNilaiTestIdPartial && !$this->isNew();
        if (null === $this->collVldNilaiTestsRelatedByNilaiTestId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldNilaiTestsRelatedByNilaiTestId) {
                // return empty collection
                $this->initVldNilaiTestsRelatedByNilaiTestId();
            } else {
                $collVldNilaiTestsRelatedByNilaiTestId = VldNilaiTestQuery::create(null, $criteria)
                    ->filterByNilaiTestRelatedByNilaiTestId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldNilaiTestsRelatedByNilaiTestIdPartial && count($collVldNilaiTestsRelatedByNilaiTestId)) {
                      $this->initVldNilaiTestsRelatedByNilaiTestId(false);

                      foreach($collVldNilaiTestsRelatedByNilaiTestId as $obj) {
                        if (false == $this->collVldNilaiTestsRelatedByNilaiTestId->contains($obj)) {
                          $this->collVldNilaiTestsRelatedByNilaiTestId->append($obj);
                        }
                      }

                      $this->collVldNilaiTestsRelatedByNilaiTestIdPartial = true;
                    }

                    $collVldNilaiTestsRelatedByNilaiTestId->getInternalIterator()->rewind();
                    return $collVldNilaiTestsRelatedByNilaiTestId;
                }

                if($partial && $this->collVldNilaiTestsRelatedByNilaiTestId) {
                    foreach($this->collVldNilaiTestsRelatedByNilaiTestId as $obj) {
                        if($obj->isNew()) {
                            $collVldNilaiTestsRelatedByNilaiTestId[] = $obj;
                        }
                    }
                }

                $this->collVldNilaiTestsRelatedByNilaiTestId = $collVldNilaiTestsRelatedByNilaiTestId;
                $this->collVldNilaiTestsRelatedByNilaiTestIdPartial = false;
            }
        }

        return $this->collVldNilaiTestsRelatedByNilaiTestId;
    }

    /**
     * Sets a collection of VldNilaiTestRelatedByNilaiTestId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldNilaiTestsRelatedByNilaiTestId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return NilaiTest The current object (for fluent API support)
     */
    public function setVldNilaiTestsRelatedByNilaiTestId(PropelCollection $vldNilaiTestsRelatedByNilaiTestId, PropelPDO $con = null)
    {
        $vldNilaiTestsRelatedByNilaiTestIdToDelete = $this->getVldNilaiTestsRelatedByNilaiTestId(new Criteria(), $con)->diff($vldNilaiTestsRelatedByNilaiTestId);

        $this->vldNilaiTestsRelatedByNilaiTestIdScheduledForDeletion = unserialize(serialize($vldNilaiTestsRelatedByNilaiTestIdToDelete));

        foreach ($vldNilaiTestsRelatedByNilaiTestIdToDelete as $vldNilaiTestRelatedByNilaiTestIdRemoved) {
            $vldNilaiTestRelatedByNilaiTestIdRemoved->setNilaiTestRelatedByNilaiTestId(null);
        }

        $this->collVldNilaiTestsRelatedByNilaiTestId = null;
        foreach ($vldNilaiTestsRelatedByNilaiTestId as $vldNilaiTestRelatedByNilaiTestId) {
            $this->addVldNilaiTestRelatedByNilaiTestId($vldNilaiTestRelatedByNilaiTestId);
        }

        $this->collVldNilaiTestsRelatedByNilaiTestId = $vldNilaiTestsRelatedByNilaiTestId;
        $this->collVldNilaiTestsRelatedByNilaiTestIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldNilaiTest objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldNilaiTest objects.
     * @throws PropelException
     */
    public function countVldNilaiTestsRelatedByNilaiTestId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldNilaiTestsRelatedByNilaiTestIdPartial && !$this->isNew();
        if (null === $this->collVldNilaiTestsRelatedByNilaiTestId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldNilaiTestsRelatedByNilaiTestId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldNilaiTestsRelatedByNilaiTestId());
            }
            $query = VldNilaiTestQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByNilaiTestRelatedByNilaiTestId($this)
                ->count($con);
        }

        return count($this->collVldNilaiTestsRelatedByNilaiTestId);
    }

    /**
     * Method called to associate a VldNilaiTest object to this object
     * through the VldNilaiTest foreign key attribute.
     *
     * @param    VldNilaiTest $l VldNilaiTest
     * @return NilaiTest The current object (for fluent API support)
     */
    public function addVldNilaiTestRelatedByNilaiTestId(VldNilaiTest $l)
    {
        if ($this->collVldNilaiTestsRelatedByNilaiTestId === null) {
            $this->initVldNilaiTestsRelatedByNilaiTestId();
            $this->collVldNilaiTestsRelatedByNilaiTestIdPartial = true;
        }
        if (!in_array($l, $this->collVldNilaiTestsRelatedByNilaiTestId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldNilaiTestRelatedByNilaiTestId($l);
        }

        return $this;
    }

    /**
     * @param	VldNilaiTestRelatedByNilaiTestId $vldNilaiTestRelatedByNilaiTestId The vldNilaiTestRelatedByNilaiTestId object to add.
     */
    protected function doAddVldNilaiTestRelatedByNilaiTestId($vldNilaiTestRelatedByNilaiTestId)
    {
        $this->collVldNilaiTestsRelatedByNilaiTestId[]= $vldNilaiTestRelatedByNilaiTestId;
        $vldNilaiTestRelatedByNilaiTestId->setNilaiTestRelatedByNilaiTestId($this);
    }

    /**
     * @param	VldNilaiTestRelatedByNilaiTestId $vldNilaiTestRelatedByNilaiTestId The vldNilaiTestRelatedByNilaiTestId object to remove.
     * @return NilaiTest The current object (for fluent API support)
     */
    public function removeVldNilaiTestRelatedByNilaiTestId($vldNilaiTestRelatedByNilaiTestId)
    {
        if ($this->getVldNilaiTestsRelatedByNilaiTestId()->contains($vldNilaiTestRelatedByNilaiTestId)) {
            $this->collVldNilaiTestsRelatedByNilaiTestId->remove($this->collVldNilaiTestsRelatedByNilaiTestId->search($vldNilaiTestRelatedByNilaiTestId));
            if (null === $this->vldNilaiTestsRelatedByNilaiTestIdScheduledForDeletion) {
                $this->vldNilaiTestsRelatedByNilaiTestIdScheduledForDeletion = clone $this->collVldNilaiTestsRelatedByNilaiTestId;
                $this->vldNilaiTestsRelatedByNilaiTestIdScheduledForDeletion->clear();
            }
            $this->vldNilaiTestsRelatedByNilaiTestIdScheduledForDeletion[]= clone $vldNilaiTestRelatedByNilaiTestId;
            $vldNilaiTestRelatedByNilaiTestId->setNilaiTestRelatedByNilaiTestId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this NilaiTest is new, it will return
     * an empty collection; or if this NilaiTest has previously
     * been saved, it will retrieve related VldNilaiTestsRelatedByNilaiTestId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in NilaiTest.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldNilaiTest[] List of VldNilaiTest objects
     */
    public function getVldNilaiTestsRelatedByNilaiTestIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldNilaiTestQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldNilaiTestsRelatedByNilaiTestId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this NilaiTest is new, it will return
     * an empty collection; or if this NilaiTest has previously
     * been saved, it will retrieve related VldNilaiTestsRelatedByNilaiTestId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in NilaiTest.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldNilaiTest[] List of VldNilaiTest objects
     */
    public function getVldNilaiTestsRelatedByNilaiTestIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldNilaiTestQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldNilaiTestsRelatedByNilaiTestId($query, $con);
    }

    /**
     * Clears out the collVldNilaiTestsRelatedByNilaiTestId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return NilaiTest The current object (for fluent API support)
     * @see        addVldNilaiTestsRelatedByNilaiTestId()
     */
    public function clearVldNilaiTestsRelatedByNilaiTestId()
    {
        $this->collVldNilaiTestsRelatedByNilaiTestId = null; // important to set this to null since that means it is uninitialized
        $this->collVldNilaiTestsRelatedByNilaiTestIdPartial = null;

        return $this;
    }

    /**
     * reset is the collVldNilaiTestsRelatedByNilaiTestId collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldNilaiTestsRelatedByNilaiTestId($v = true)
    {
        $this->collVldNilaiTestsRelatedByNilaiTestIdPartial = $v;
    }

    /**
     * Initializes the collVldNilaiTestsRelatedByNilaiTestId collection.
     *
     * By default this just sets the collVldNilaiTestsRelatedByNilaiTestId collection to an empty array (like clearcollVldNilaiTestsRelatedByNilaiTestId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldNilaiTestsRelatedByNilaiTestId($overrideExisting = true)
    {
        if (null !== $this->collVldNilaiTestsRelatedByNilaiTestId && !$overrideExisting) {
            return;
        }
        $this->collVldNilaiTestsRelatedByNilaiTestId = new PropelObjectCollection();
        $this->collVldNilaiTestsRelatedByNilaiTestId->setModel('VldNilaiTest');
    }

    /**
     * Gets an array of VldNilaiTest objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this NilaiTest is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldNilaiTest[] List of VldNilaiTest objects
     * @throws PropelException
     */
    public function getVldNilaiTestsRelatedByNilaiTestId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldNilaiTestsRelatedByNilaiTestIdPartial && !$this->isNew();
        if (null === $this->collVldNilaiTestsRelatedByNilaiTestId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldNilaiTestsRelatedByNilaiTestId) {
                // return empty collection
                $this->initVldNilaiTestsRelatedByNilaiTestId();
            } else {
                $collVldNilaiTestsRelatedByNilaiTestId = VldNilaiTestQuery::create(null, $criteria)
                    ->filterByNilaiTestRelatedByNilaiTestId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldNilaiTestsRelatedByNilaiTestIdPartial && count($collVldNilaiTestsRelatedByNilaiTestId)) {
                      $this->initVldNilaiTestsRelatedByNilaiTestId(false);

                      foreach($collVldNilaiTestsRelatedByNilaiTestId as $obj) {
                        if (false == $this->collVldNilaiTestsRelatedByNilaiTestId->contains($obj)) {
                          $this->collVldNilaiTestsRelatedByNilaiTestId->append($obj);
                        }
                      }

                      $this->collVldNilaiTestsRelatedByNilaiTestIdPartial = true;
                    }

                    $collVldNilaiTestsRelatedByNilaiTestId->getInternalIterator()->rewind();
                    return $collVldNilaiTestsRelatedByNilaiTestId;
                }

                if($partial && $this->collVldNilaiTestsRelatedByNilaiTestId) {
                    foreach($this->collVldNilaiTestsRelatedByNilaiTestId as $obj) {
                        if($obj->isNew()) {
                            $collVldNilaiTestsRelatedByNilaiTestId[] = $obj;
                        }
                    }
                }

                $this->collVldNilaiTestsRelatedByNilaiTestId = $collVldNilaiTestsRelatedByNilaiTestId;
                $this->collVldNilaiTestsRelatedByNilaiTestIdPartial = false;
            }
        }

        return $this->collVldNilaiTestsRelatedByNilaiTestId;
    }

    /**
     * Sets a collection of VldNilaiTestRelatedByNilaiTestId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldNilaiTestsRelatedByNilaiTestId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return NilaiTest The current object (for fluent API support)
     */
    public function setVldNilaiTestsRelatedByNilaiTestId(PropelCollection $vldNilaiTestsRelatedByNilaiTestId, PropelPDO $con = null)
    {
        $vldNilaiTestsRelatedByNilaiTestIdToDelete = $this->getVldNilaiTestsRelatedByNilaiTestId(new Criteria(), $con)->diff($vldNilaiTestsRelatedByNilaiTestId);

        $this->vldNilaiTestsRelatedByNilaiTestIdScheduledForDeletion = unserialize(serialize($vldNilaiTestsRelatedByNilaiTestIdToDelete));

        foreach ($vldNilaiTestsRelatedByNilaiTestIdToDelete as $vldNilaiTestRelatedByNilaiTestIdRemoved) {
            $vldNilaiTestRelatedByNilaiTestIdRemoved->setNilaiTestRelatedByNilaiTestId(null);
        }

        $this->collVldNilaiTestsRelatedByNilaiTestId = null;
        foreach ($vldNilaiTestsRelatedByNilaiTestId as $vldNilaiTestRelatedByNilaiTestId) {
            $this->addVldNilaiTestRelatedByNilaiTestId($vldNilaiTestRelatedByNilaiTestId);
        }

        $this->collVldNilaiTestsRelatedByNilaiTestId = $vldNilaiTestsRelatedByNilaiTestId;
        $this->collVldNilaiTestsRelatedByNilaiTestIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldNilaiTest objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldNilaiTest objects.
     * @throws PropelException
     */
    public function countVldNilaiTestsRelatedByNilaiTestId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldNilaiTestsRelatedByNilaiTestIdPartial && !$this->isNew();
        if (null === $this->collVldNilaiTestsRelatedByNilaiTestId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldNilaiTestsRelatedByNilaiTestId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldNilaiTestsRelatedByNilaiTestId());
            }
            $query = VldNilaiTestQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByNilaiTestRelatedByNilaiTestId($this)
                ->count($con);
        }

        return count($this->collVldNilaiTestsRelatedByNilaiTestId);
    }

    /**
     * Method called to associate a VldNilaiTest object to this object
     * through the VldNilaiTest foreign key attribute.
     *
     * @param    VldNilaiTest $l VldNilaiTest
     * @return NilaiTest The current object (for fluent API support)
     */
    public function addVldNilaiTestRelatedByNilaiTestId(VldNilaiTest $l)
    {
        if ($this->collVldNilaiTestsRelatedByNilaiTestId === null) {
            $this->initVldNilaiTestsRelatedByNilaiTestId();
            $this->collVldNilaiTestsRelatedByNilaiTestIdPartial = true;
        }
        if (!in_array($l, $this->collVldNilaiTestsRelatedByNilaiTestId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldNilaiTestRelatedByNilaiTestId($l);
        }

        return $this;
    }

    /**
     * @param	VldNilaiTestRelatedByNilaiTestId $vldNilaiTestRelatedByNilaiTestId The vldNilaiTestRelatedByNilaiTestId object to add.
     */
    protected function doAddVldNilaiTestRelatedByNilaiTestId($vldNilaiTestRelatedByNilaiTestId)
    {
        $this->collVldNilaiTestsRelatedByNilaiTestId[]= $vldNilaiTestRelatedByNilaiTestId;
        $vldNilaiTestRelatedByNilaiTestId->setNilaiTestRelatedByNilaiTestId($this);
    }

    /**
     * @param	VldNilaiTestRelatedByNilaiTestId $vldNilaiTestRelatedByNilaiTestId The vldNilaiTestRelatedByNilaiTestId object to remove.
     * @return NilaiTest The current object (for fluent API support)
     */
    public function removeVldNilaiTestRelatedByNilaiTestId($vldNilaiTestRelatedByNilaiTestId)
    {
        if ($this->getVldNilaiTestsRelatedByNilaiTestId()->contains($vldNilaiTestRelatedByNilaiTestId)) {
            $this->collVldNilaiTestsRelatedByNilaiTestId->remove($this->collVldNilaiTestsRelatedByNilaiTestId->search($vldNilaiTestRelatedByNilaiTestId));
            if (null === $this->vldNilaiTestsRelatedByNilaiTestIdScheduledForDeletion) {
                $this->vldNilaiTestsRelatedByNilaiTestIdScheduledForDeletion = clone $this->collVldNilaiTestsRelatedByNilaiTestId;
                $this->vldNilaiTestsRelatedByNilaiTestIdScheduledForDeletion->clear();
            }
            $this->vldNilaiTestsRelatedByNilaiTestIdScheduledForDeletion[]= clone $vldNilaiTestRelatedByNilaiTestId;
            $vldNilaiTestRelatedByNilaiTestId->setNilaiTestRelatedByNilaiTestId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this NilaiTest is new, it will return
     * an empty collection; or if this NilaiTest has previously
     * been saved, it will retrieve related VldNilaiTestsRelatedByNilaiTestId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in NilaiTest.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldNilaiTest[] List of VldNilaiTest objects
     */
    public function getVldNilaiTestsRelatedByNilaiTestIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldNilaiTestQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldNilaiTestsRelatedByNilaiTestId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this NilaiTest is new, it will return
     * an empty collection; or if this NilaiTest has previously
     * been saved, it will retrieve related VldNilaiTestsRelatedByNilaiTestId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in NilaiTest.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldNilaiTest[] List of VldNilaiTest objects
     */
    public function getVldNilaiTestsRelatedByNilaiTestIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldNilaiTestQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldNilaiTestsRelatedByNilaiTestId($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->nilai_test_id = null;
        $this->jenis_test_id = null;
        $this->ptk_id = null;
        $this->nama = null;
        $this->penyelenggara = null;
        $this->tahun = null;
        $this->skor = null;
        $this->last_update = null;
        $this->soft_delete = null;
        $this->last_sync = null;
        $this->updater_id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collVldNilaiTestsRelatedByNilaiTestId) {
                foreach ($this->collVldNilaiTestsRelatedByNilaiTestId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldNilaiTestsRelatedByNilaiTestId) {
                foreach ($this->collVldNilaiTestsRelatedByNilaiTestId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aPtkRelatedByPtkId instanceof Persistent) {
              $this->aPtkRelatedByPtkId->clearAllReferences($deep);
            }
            if ($this->aPtkRelatedByPtkId instanceof Persistent) {
              $this->aPtkRelatedByPtkId->clearAllReferences($deep);
            }
            if ($this->aJenisTestRelatedByJenisTestId instanceof Persistent) {
              $this->aJenisTestRelatedByJenisTestId->clearAllReferences($deep);
            }
            if ($this->aJenisTestRelatedByJenisTestId instanceof Persistent) {
              $this->aJenisTestRelatedByJenisTestId->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collVldNilaiTestsRelatedByNilaiTestId instanceof PropelCollection) {
            $this->collVldNilaiTestsRelatedByNilaiTestId->clearIterator();
        }
        $this->collVldNilaiTestsRelatedByNilaiTestId = null;
        if ($this->collVldNilaiTestsRelatedByNilaiTestId instanceof PropelCollection) {
            $this->collVldNilaiTestsRelatedByNilaiTestId->clearIterator();
        }
        $this->collVldNilaiTestsRelatedByNilaiTestId = null;
        $this->aPtkRelatedByPtkId = null;
        $this->aPtkRelatedByPtkId = null;
        $this->aJenisTestRelatedByJenisTestId = null;
        $this->aJenisTestRelatedByJenisTestId = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(NilaiTestPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
