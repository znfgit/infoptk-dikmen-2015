<?php

namespace angulex\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelDateTime;
use \PropelException;
use \PropelPDO;
use angulex\Model\AnggotaRombel;
use angulex\Model\AnggotaRombelPeer;
use angulex\Model\AnggotaRombelQuery;
use angulex\Model\JenisPendaftaran;
use angulex\Model\JenisPendaftaranQuery;
use angulex\Model\PesertaDidik;
use angulex\Model\PesertaDidikQuery;
use angulex\Model\RombonganBelajar;
use angulex\Model\RombonganBelajarQuery;

/**
 * Base class that represents a row from the 'anggota_rombel' table.
 *
 * 
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseAnggotaRombel extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'angulex\\Model\\AnggotaRombelPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        AnggotaRombelPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the anggota_rombel_id field.
     * @var        string
     */
    protected $anggota_rombel_id;

    /**
     * The value for the rombongan_belajar_id field.
     * @var        string
     */
    protected $rombongan_belajar_id;

    /**
     * The value for the peserta_didik_id field.
     * @var        string
     */
    protected $peserta_didik_id;

    /**
     * The value for the jenis_pendaftaran_id field.
     * @var        string
     */
    protected $jenis_pendaftaran_id;

    /**
     * The value for the last_update field.
     * @var        string
     */
    protected $last_update;

    /**
     * The value for the soft_delete field.
     * @var        string
     */
    protected $soft_delete;

    /**
     * The value for the last_sync field.
     * @var        string
     */
    protected $last_sync;

    /**
     * The value for the updater_id field.
     * @var        string
     */
    protected $updater_id;

    /**
     * @var        PesertaDidik
     */
    protected $aPesertaDidikRelatedByPesertaDidikId;

    /**
     * @var        PesertaDidik
     */
    protected $aPesertaDidikRelatedByPesertaDidikId;

    /**
     * @var        RombonganBelajar
     */
    protected $aRombonganBelajarRelatedByRombonganBelajarId;

    /**
     * @var        RombonganBelajar
     */
    protected $aRombonganBelajarRelatedByRombonganBelajarId;

    /**
     * @var        JenisPendaftaran
     */
    protected $aJenisPendaftaranRelatedByJenisPendaftaranId;

    /**
     * @var        JenisPendaftaran
     */
    protected $aJenisPendaftaranRelatedByJenisPendaftaranId;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Get the [anggota_rombel_id] column value.
     * 
     * @return string
     */
    public function getAnggotaRombelId()
    {
        return $this->anggota_rombel_id;
    }

    /**
     * Get the [rombongan_belajar_id] column value.
     * 
     * @return string
     */
    public function getRombonganBelajarId()
    {
        return $this->rombongan_belajar_id;
    }

    /**
     * Get the [peserta_didik_id] column value.
     * 
     * @return string
     */
    public function getPesertaDidikId()
    {
        return $this->peserta_didik_id;
    }

    /**
     * Get the [jenis_pendaftaran_id] column value.
     * 
     * @return string
     */
    public function getJenisPendaftaranId()
    {
        return $this->jenis_pendaftaran_id;
    }

    /**
     * Get the [optionally formatted] temporal [last_update] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastUpdate($format = 'Y-m-d H:i:s')
    {
        if ($this->last_update === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_update);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_update, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [soft_delete] column value.
     * 
     * @return string
     */
    public function getSoftDelete()
    {
        return $this->soft_delete;
    }

    /**
     * Get the [optionally formatted] temporal [last_sync] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastSync($format = 'Y-m-d H:i:s')
    {
        if ($this->last_sync === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_sync);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_sync, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [updater_id] column value.
     * 
     * @return string
     */
    public function getUpdaterId()
    {
        return $this->updater_id;
    }

    /**
     * Set the value of [anggota_rombel_id] column.
     * 
     * @param string $v new value
     * @return AnggotaRombel The current object (for fluent API support)
     */
    public function setAnggotaRombelId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->anggota_rombel_id !== $v) {
            $this->anggota_rombel_id = $v;
            $this->modifiedColumns[] = AnggotaRombelPeer::ANGGOTA_ROMBEL_ID;
        }


        return $this;
    } // setAnggotaRombelId()

    /**
     * Set the value of [rombongan_belajar_id] column.
     * 
     * @param string $v new value
     * @return AnggotaRombel The current object (for fluent API support)
     */
    public function setRombonganBelajarId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->rombongan_belajar_id !== $v) {
            $this->rombongan_belajar_id = $v;
            $this->modifiedColumns[] = AnggotaRombelPeer::ROMBONGAN_BELAJAR_ID;
        }

        if ($this->aRombonganBelajarRelatedByRombonganBelajarId !== null && $this->aRombonganBelajarRelatedByRombonganBelajarId->getRombonganBelajarId() !== $v) {
            $this->aRombonganBelajarRelatedByRombonganBelajarId = null;
        }

        if ($this->aRombonganBelajarRelatedByRombonganBelajarId !== null && $this->aRombonganBelajarRelatedByRombonganBelajarId->getRombonganBelajarId() !== $v) {
            $this->aRombonganBelajarRelatedByRombonganBelajarId = null;
        }


        return $this;
    } // setRombonganBelajarId()

    /**
     * Set the value of [peserta_didik_id] column.
     * 
     * @param string $v new value
     * @return AnggotaRombel The current object (for fluent API support)
     */
    public function setPesertaDidikId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->peserta_didik_id !== $v) {
            $this->peserta_didik_id = $v;
            $this->modifiedColumns[] = AnggotaRombelPeer::PESERTA_DIDIK_ID;
        }

        if ($this->aPesertaDidikRelatedByPesertaDidikId !== null && $this->aPesertaDidikRelatedByPesertaDidikId->getPesertaDidikId() !== $v) {
            $this->aPesertaDidikRelatedByPesertaDidikId = null;
        }

        if ($this->aPesertaDidikRelatedByPesertaDidikId !== null && $this->aPesertaDidikRelatedByPesertaDidikId->getPesertaDidikId() !== $v) {
            $this->aPesertaDidikRelatedByPesertaDidikId = null;
        }


        return $this;
    } // setPesertaDidikId()

    /**
     * Set the value of [jenis_pendaftaran_id] column.
     * 
     * @param string $v new value
     * @return AnggotaRombel The current object (for fluent API support)
     */
    public function setJenisPendaftaranId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->jenis_pendaftaran_id !== $v) {
            $this->jenis_pendaftaran_id = $v;
            $this->modifiedColumns[] = AnggotaRombelPeer::JENIS_PENDAFTARAN_ID;
        }

        if ($this->aJenisPendaftaranRelatedByJenisPendaftaranId !== null && $this->aJenisPendaftaranRelatedByJenisPendaftaranId->getJenisPendaftaranId() !== $v) {
            $this->aJenisPendaftaranRelatedByJenisPendaftaranId = null;
        }

        if ($this->aJenisPendaftaranRelatedByJenisPendaftaranId !== null && $this->aJenisPendaftaranRelatedByJenisPendaftaranId->getJenisPendaftaranId() !== $v) {
            $this->aJenisPendaftaranRelatedByJenisPendaftaranId = null;
        }


        return $this;
    } // setJenisPendaftaranId()

    /**
     * Sets the value of [last_update] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return AnggotaRombel The current object (for fluent API support)
     */
    public function setLastUpdate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_update !== null || $dt !== null) {
            $currentDateAsString = ($this->last_update !== null && $tmpDt = new DateTime($this->last_update)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_update = $newDateAsString;
                $this->modifiedColumns[] = AnggotaRombelPeer::LAST_UPDATE;
            }
        } // if either are not null


        return $this;
    } // setLastUpdate()

    /**
     * Set the value of [soft_delete] column.
     * 
     * @param string $v new value
     * @return AnggotaRombel The current object (for fluent API support)
     */
    public function setSoftDelete($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->soft_delete !== $v) {
            $this->soft_delete = $v;
            $this->modifiedColumns[] = AnggotaRombelPeer::SOFT_DELETE;
        }


        return $this;
    } // setSoftDelete()

    /**
     * Sets the value of [last_sync] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return AnggotaRombel The current object (for fluent API support)
     */
    public function setLastSync($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_sync !== null || $dt !== null) {
            $currentDateAsString = ($this->last_sync !== null && $tmpDt = new DateTime($this->last_sync)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_sync = $newDateAsString;
                $this->modifiedColumns[] = AnggotaRombelPeer::LAST_SYNC;
            }
        } // if either are not null


        return $this;
    } // setLastSync()

    /**
     * Set the value of [updater_id] column.
     * 
     * @param string $v new value
     * @return AnggotaRombel The current object (for fluent API support)
     */
    public function setUpdaterId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->updater_id !== $v) {
            $this->updater_id = $v;
            $this->modifiedColumns[] = AnggotaRombelPeer::UPDATER_ID;
        }


        return $this;
    } // setUpdaterId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->anggota_rombel_id = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
            $this->rombongan_belajar_id = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->peserta_didik_id = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->jenis_pendaftaran_id = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->last_update = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->soft_delete = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->last_sync = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->updater_id = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 8; // 8 = AnggotaRombelPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating AnggotaRombel object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aRombonganBelajarRelatedByRombonganBelajarId !== null && $this->rombongan_belajar_id !== $this->aRombonganBelajarRelatedByRombonganBelajarId->getRombonganBelajarId()) {
            $this->aRombonganBelajarRelatedByRombonganBelajarId = null;
        }
        if ($this->aRombonganBelajarRelatedByRombonganBelajarId !== null && $this->rombongan_belajar_id !== $this->aRombonganBelajarRelatedByRombonganBelajarId->getRombonganBelajarId()) {
            $this->aRombonganBelajarRelatedByRombonganBelajarId = null;
        }
        if ($this->aPesertaDidikRelatedByPesertaDidikId !== null && $this->peserta_didik_id !== $this->aPesertaDidikRelatedByPesertaDidikId->getPesertaDidikId()) {
            $this->aPesertaDidikRelatedByPesertaDidikId = null;
        }
        if ($this->aPesertaDidikRelatedByPesertaDidikId !== null && $this->peserta_didik_id !== $this->aPesertaDidikRelatedByPesertaDidikId->getPesertaDidikId()) {
            $this->aPesertaDidikRelatedByPesertaDidikId = null;
        }
        if ($this->aJenisPendaftaranRelatedByJenisPendaftaranId !== null && $this->jenis_pendaftaran_id !== $this->aJenisPendaftaranRelatedByJenisPendaftaranId->getJenisPendaftaranId()) {
            $this->aJenisPendaftaranRelatedByJenisPendaftaranId = null;
        }
        if ($this->aJenisPendaftaranRelatedByJenisPendaftaranId !== null && $this->jenis_pendaftaran_id !== $this->aJenisPendaftaranRelatedByJenisPendaftaranId->getJenisPendaftaranId()) {
            $this->aJenisPendaftaranRelatedByJenisPendaftaranId = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(AnggotaRombelPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = AnggotaRombelPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aPesertaDidikRelatedByPesertaDidikId = null;
            $this->aPesertaDidikRelatedByPesertaDidikId = null;
            $this->aRombonganBelajarRelatedByRombonganBelajarId = null;
            $this->aRombonganBelajarRelatedByRombonganBelajarId = null;
            $this->aJenisPendaftaranRelatedByJenisPendaftaranId = null;
            $this->aJenisPendaftaranRelatedByJenisPendaftaranId = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(AnggotaRombelPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = AnggotaRombelQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(AnggotaRombelPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                AnggotaRombelPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPesertaDidikRelatedByPesertaDidikId !== null) {
                if ($this->aPesertaDidikRelatedByPesertaDidikId->isModified() || $this->aPesertaDidikRelatedByPesertaDidikId->isNew()) {
                    $affectedRows += $this->aPesertaDidikRelatedByPesertaDidikId->save($con);
                }
                $this->setPesertaDidikRelatedByPesertaDidikId($this->aPesertaDidikRelatedByPesertaDidikId);
            }

            if ($this->aPesertaDidikRelatedByPesertaDidikId !== null) {
                if ($this->aPesertaDidikRelatedByPesertaDidikId->isModified() || $this->aPesertaDidikRelatedByPesertaDidikId->isNew()) {
                    $affectedRows += $this->aPesertaDidikRelatedByPesertaDidikId->save($con);
                }
                $this->setPesertaDidikRelatedByPesertaDidikId($this->aPesertaDidikRelatedByPesertaDidikId);
            }

            if ($this->aRombonganBelajarRelatedByRombonganBelajarId !== null) {
                if ($this->aRombonganBelajarRelatedByRombonganBelajarId->isModified() || $this->aRombonganBelajarRelatedByRombonganBelajarId->isNew()) {
                    $affectedRows += $this->aRombonganBelajarRelatedByRombonganBelajarId->save($con);
                }
                $this->setRombonganBelajarRelatedByRombonganBelajarId($this->aRombonganBelajarRelatedByRombonganBelajarId);
            }

            if ($this->aRombonganBelajarRelatedByRombonganBelajarId !== null) {
                if ($this->aRombonganBelajarRelatedByRombonganBelajarId->isModified() || $this->aRombonganBelajarRelatedByRombonganBelajarId->isNew()) {
                    $affectedRows += $this->aRombonganBelajarRelatedByRombonganBelajarId->save($con);
                }
                $this->setRombonganBelajarRelatedByRombonganBelajarId($this->aRombonganBelajarRelatedByRombonganBelajarId);
            }

            if ($this->aJenisPendaftaranRelatedByJenisPendaftaranId !== null) {
                if ($this->aJenisPendaftaranRelatedByJenisPendaftaranId->isModified() || $this->aJenisPendaftaranRelatedByJenisPendaftaranId->isNew()) {
                    $affectedRows += $this->aJenisPendaftaranRelatedByJenisPendaftaranId->save($con);
                }
                $this->setJenisPendaftaranRelatedByJenisPendaftaranId($this->aJenisPendaftaranRelatedByJenisPendaftaranId);
            }

            if ($this->aJenisPendaftaranRelatedByJenisPendaftaranId !== null) {
                if ($this->aJenisPendaftaranRelatedByJenisPendaftaranId->isModified() || $this->aJenisPendaftaranRelatedByJenisPendaftaranId->isNew()) {
                    $affectedRows += $this->aJenisPendaftaranRelatedByJenisPendaftaranId->save($con);
                }
                $this->setJenisPendaftaranRelatedByJenisPendaftaranId($this->aJenisPendaftaranRelatedByJenisPendaftaranId);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $criteria = $this->buildCriteria();
        $pk = BasePeer::doInsert($criteria, $con);
        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPesertaDidikRelatedByPesertaDidikId !== null) {
                if (!$this->aPesertaDidikRelatedByPesertaDidikId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPesertaDidikRelatedByPesertaDidikId->getValidationFailures());
                }
            }

            if ($this->aPesertaDidikRelatedByPesertaDidikId !== null) {
                if (!$this->aPesertaDidikRelatedByPesertaDidikId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPesertaDidikRelatedByPesertaDidikId->getValidationFailures());
                }
            }

            if ($this->aRombonganBelajarRelatedByRombonganBelajarId !== null) {
                if (!$this->aRombonganBelajarRelatedByRombonganBelajarId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aRombonganBelajarRelatedByRombonganBelajarId->getValidationFailures());
                }
            }

            if ($this->aRombonganBelajarRelatedByRombonganBelajarId !== null) {
                if (!$this->aRombonganBelajarRelatedByRombonganBelajarId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aRombonganBelajarRelatedByRombonganBelajarId->getValidationFailures());
                }
            }

            if ($this->aJenisPendaftaranRelatedByJenisPendaftaranId !== null) {
                if (!$this->aJenisPendaftaranRelatedByJenisPendaftaranId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aJenisPendaftaranRelatedByJenisPendaftaranId->getValidationFailures());
                }
            }

            if ($this->aJenisPendaftaranRelatedByJenisPendaftaranId !== null) {
                if (!$this->aJenisPendaftaranRelatedByJenisPendaftaranId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aJenisPendaftaranRelatedByJenisPendaftaranId->getValidationFailures());
                }
            }


            if (($retval = AnggotaRombelPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = AnggotaRombelPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getAnggotaRombelId();
                break;
            case 1:
                return $this->getRombonganBelajarId();
                break;
            case 2:
                return $this->getPesertaDidikId();
                break;
            case 3:
                return $this->getJenisPendaftaranId();
                break;
            case 4:
                return $this->getLastUpdate();
                break;
            case 5:
                return $this->getSoftDelete();
                break;
            case 6:
                return $this->getLastSync();
                break;
            case 7:
                return $this->getUpdaterId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['AnggotaRombel'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['AnggotaRombel'][$this->getPrimaryKey()] = true;
        $keys = AnggotaRombelPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getAnggotaRombelId(),
            $keys[1] => $this->getRombonganBelajarId(),
            $keys[2] => $this->getPesertaDidikId(),
            $keys[3] => $this->getJenisPendaftaranId(),
            $keys[4] => $this->getLastUpdate(),
            $keys[5] => $this->getSoftDelete(),
            $keys[6] => $this->getLastSync(),
            $keys[7] => $this->getUpdaterId(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aPesertaDidikRelatedByPesertaDidikId) {
                $result['PesertaDidikRelatedByPesertaDidikId'] = $this->aPesertaDidikRelatedByPesertaDidikId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPesertaDidikRelatedByPesertaDidikId) {
                $result['PesertaDidikRelatedByPesertaDidikId'] = $this->aPesertaDidikRelatedByPesertaDidikId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aRombonganBelajarRelatedByRombonganBelajarId) {
                $result['RombonganBelajarRelatedByRombonganBelajarId'] = $this->aRombonganBelajarRelatedByRombonganBelajarId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aRombonganBelajarRelatedByRombonganBelajarId) {
                $result['RombonganBelajarRelatedByRombonganBelajarId'] = $this->aRombonganBelajarRelatedByRombonganBelajarId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aJenisPendaftaranRelatedByJenisPendaftaranId) {
                $result['JenisPendaftaranRelatedByJenisPendaftaranId'] = $this->aJenisPendaftaranRelatedByJenisPendaftaranId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aJenisPendaftaranRelatedByJenisPendaftaranId) {
                $result['JenisPendaftaranRelatedByJenisPendaftaranId'] = $this->aJenisPendaftaranRelatedByJenisPendaftaranId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = AnggotaRombelPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setAnggotaRombelId($value);
                break;
            case 1:
                $this->setRombonganBelajarId($value);
                break;
            case 2:
                $this->setPesertaDidikId($value);
                break;
            case 3:
                $this->setJenisPendaftaranId($value);
                break;
            case 4:
                $this->setLastUpdate($value);
                break;
            case 5:
                $this->setSoftDelete($value);
                break;
            case 6:
                $this->setLastSync($value);
                break;
            case 7:
                $this->setUpdaterId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = AnggotaRombelPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setAnggotaRombelId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setRombonganBelajarId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setPesertaDidikId($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setJenisPendaftaranId($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setLastUpdate($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setSoftDelete($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setLastSync($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setUpdaterId($arr[$keys[7]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(AnggotaRombelPeer::DATABASE_NAME);

        if ($this->isColumnModified(AnggotaRombelPeer::ANGGOTA_ROMBEL_ID)) $criteria->add(AnggotaRombelPeer::ANGGOTA_ROMBEL_ID, $this->anggota_rombel_id);
        if ($this->isColumnModified(AnggotaRombelPeer::ROMBONGAN_BELAJAR_ID)) $criteria->add(AnggotaRombelPeer::ROMBONGAN_BELAJAR_ID, $this->rombongan_belajar_id);
        if ($this->isColumnModified(AnggotaRombelPeer::PESERTA_DIDIK_ID)) $criteria->add(AnggotaRombelPeer::PESERTA_DIDIK_ID, $this->peserta_didik_id);
        if ($this->isColumnModified(AnggotaRombelPeer::JENIS_PENDAFTARAN_ID)) $criteria->add(AnggotaRombelPeer::JENIS_PENDAFTARAN_ID, $this->jenis_pendaftaran_id);
        if ($this->isColumnModified(AnggotaRombelPeer::LAST_UPDATE)) $criteria->add(AnggotaRombelPeer::LAST_UPDATE, $this->last_update);
        if ($this->isColumnModified(AnggotaRombelPeer::SOFT_DELETE)) $criteria->add(AnggotaRombelPeer::SOFT_DELETE, $this->soft_delete);
        if ($this->isColumnModified(AnggotaRombelPeer::LAST_SYNC)) $criteria->add(AnggotaRombelPeer::LAST_SYNC, $this->last_sync);
        if ($this->isColumnModified(AnggotaRombelPeer::UPDATER_ID)) $criteria->add(AnggotaRombelPeer::UPDATER_ID, $this->updater_id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(AnggotaRombelPeer::DATABASE_NAME);
        $criteria->add(AnggotaRombelPeer::ANGGOTA_ROMBEL_ID, $this->anggota_rombel_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getAnggotaRombelId();
    }

    /**
     * Generic method to set the primary key (anggota_rombel_id column).
     *
     * @param  string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setAnggotaRombelId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getAnggotaRombelId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of AnggotaRombel (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setRombonganBelajarId($this->getRombonganBelajarId());
        $copyObj->setPesertaDidikId($this->getPesertaDidikId());
        $copyObj->setJenisPendaftaranId($this->getJenisPendaftaranId());
        $copyObj->setLastUpdate($this->getLastUpdate());
        $copyObj->setSoftDelete($this->getSoftDelete());
        $copyObj->setLastSync($this->getLastSync());
        $copyObj->setUpdaterId($this->getUpdaterId());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setAnggotaRombelId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return AnggotaRombel Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return AnggotaRombelPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new AnggotaRombelPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a PesertaDidik object.
     *
     * @param             PesertaDidik $v
     * @return AnggotaRombel The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPesertaDidikRelatedByPesertaDidikId(PesertaDidik $v = null)
    {
        if ($v === null) {
            $this->setPesertaDidikId(NULL);
        } else {
            $this->setPesertaDidikId($v->getPesertaDidikId());
        }

        $this->aPesertaDidikRelatedByPesertaDidikId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the PesertaDidik object, it will not be re-added.
        if ($v !== null) {
            $v->addAnggotaRombelRelatedByPesertaDidikId($this);
        }


        return $this;
    }


    /**
     * Get the associated PesertaDidik object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return PesertaDidik The associated PesertaDidik object.
     * @throws PropelException
     */
    public function getPesertaDidikRelatedByPesertaDidikId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPesertaDidikRelatedByPesertaDidikId === null && (($this->peserta_didik_id !== "" && $this->peserta_didik_id !== null)) && $doQuery) {
            $this->aPesertaDidikRelatedByPesertaDidikId = PesertaDidikQuery::create()->findPk($this->peserta_didik_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPesertaDidikRelatedByPesertaDidikId->addAnggotaRombelsRelatedByPesertaDidikId($this);
             */
        }

        return $this->aPesertaDidikRelatedByPesertaDidikId;
    }

    /**
     * Declares an association between this object and a PesertaDidik object.
     *
     * @param             PesertaDidik $v
     * @return AnggotaRombel The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPesertaDidikRelatedByPesertaDidikId(PesertaDidik $v = null)
    {
        if ($v === null) {
            $this->setPesertaDidikId(NULL);
        } else {
            $this->setPesertaDidikId($v->getPesertaDidikId());
        }

        $this->aPesertaDidikRelatedByPesertaDidikId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the PesertaDidik object, it will not be re-added.
        if ($v !== null) {
            $v->addAnggotaRombelRelatedByPesertaDidikId($this);
        }


        return $this;
    }


    /**
     * Get the associated PesertaDidik object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return PesertaDidik The associated PesertaDidik object.
     * @throws PropelException
     */
    public function getPesertaDidikRelatedByPesertaDidikId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPesertaDidikRelatedByPesertaDidikId === null && (($this->peserta_didik_id !== "" && $this->peserta_didik_id !== null)) && $doQuery) {
            $this->aPesertaDidikRelatedByPesertaDidikId = PesertaDidikQuery::create()->findPk($this->peserta_didik_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPesertaDidikRelatedByPesertaDidikId->addAnggotaRombelsRelatedByPesertaDidikId($this);
             */
        }

        return $this->aPesertaDidikRelatedByPesertaDidikId;
    }

    /**
     * Declares an association between this object and a RombonganBelajar object.
     *
     * @param             RombonganBelajar $v
     * @return AnggotaRombel The current object (for fluent API support)
     * @throws PropelException
     */
    public function setRombonganBelajarRelatedByRombonganBelajarId(RombonganBelajar $v = null)
    {
        if ($v === null) {
            $this->setRombonganBelajarId(NULL);
        } else {
            $this->setRombonganBelajarId($v->getRombonganBelajarId());
        }

        $this->aRombonganBelajarRelatedByRombonganBelajarId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the RombonganBelajar object, it will not be re-added.
        if ($v !== null) {
            $v->addAnggotaRombelRelatedByRombonganBelajarId($this);
        }


        return $this;
    }


    /**
     * Get the associated RombonganBelajar object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return RombonganBelajar The associated RombonganBelajar object.
     * @throws PropelException
     */
    public function getRombonganBelajarRelatedByRombonganBelajarId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aRombonganBelajarRelatedByRombonganBelajarId === null && (($this->rombongan_belajar_id !== "" && $this->rombongan_belajar_id !== null)) && $doQuery) {
            $this->aRombonganBelajarRelatedByRombonganBelajarId = RombonganBelajarQuery::create()->findPk($this->rombongan_belajar_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aRombonganBelajarRelatedByRombonganBelajarId->addAnggotaRombelsRelatedByRombonganBelajarId($this);
             */
        }

        return $this->aRombonganBelajarRelatedByRombonganBelajarId;
    }

    /**
     * Declares an association between this object and a RombonganBelajar object.
     *
     * @param             RombonganBelajar $v
     * @return AnggotaRombel The current object (for fluent API support)
     * @throws PropelException
     */
    public function setRombonganBelajarRelatedByRombonganBelajarId(RombonganBelajar $v = null)
    {
        if ($v === null) {
            $this->setRombonganBelajarId(NULL);
        } else {
            $this->setRombonganBelajarId($v->getRombonganBelajarId());
        }

        $this->aRombonganBelajarRelatedByRombonganBelajarId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the RombonganBelajar object, it will not be re-added.
        if ($v !== null) {
            $v->addAnggotaRombelRelatedByRombonganBelajarId($this);
        }


        return $this;
    }


    /**
     * Get the associated RombonganBelajar object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return RombonganBelajar The associated RombonganBelajar object.
     * @throws PropelException
     */
    public function getRombonganBelajarRelatedByRombonganBelajarId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aRombonganBelajarRelatedByRombonganBelajarId === null && (($this->rombongan_belajar_id !== "" && $this->rombongan_belajar_id !== null)) && $doQuery) {
            $this->aRombonganBelajarRelatedByRombonganBelajarId = RombonganBelajarQuery::create()->findPk($this->rombongan_belajar_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aRombonganBelajarRelatedByRombonganBelajarId->addAnggotaRombelsRelatedByRombonganBelajarId($this);
             */
        }

        return $this->aRombonganBelajarRelatedByRombonganBelajarId;
    }

    /**
     * Declares an association between this object and a JenisPendaftaran object.
     *
     * @param             JenisPendaftaran $v
     * @return AnggotaRombel The current object (for fluent API support)
     * @throws PropelException
     */
    public function setJenisPendaftaranRelatedByJenisPendaftaranId(JenisPendaftaran $v = null)
    {
        if ($v === null) {
            $this->setJenisPendaftaranId(NULL);
        } else {
            $this->setJenisPendaftaranId($v->getJenisPendaftaranId());
        }

        $this->aJenisPendaftaranRelatedByJenisPendaftaranId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the JenisPendaftaran object, it will not be re-added.
        if ($v !== null) {
            $v->addAnggotaRombelRelatedByJenisPendaftaranId($this);
        }


        return $this;
    }


    /**
     * Get the associated JenisPendaftaran object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return JenisPendaftaran The associated JenisPendaftaran object.
     * @throws PropelException
     */
    public function getJenisPendaftaranRelatedByJenisPendaftaranId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aJenisPendaftaranRelatedByJenisPendaftaranId === null && (($this->jenis_pendaftaran_id !== "" && $this->jenis_pendaftaran_id !== null)) && $doQuery) {
            $this->aJenisPendaftaranRelatedByJenisPendaftaranId = JenisPendaftaranQuery::create()->findPk($this->jenis_pendaftaran_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aJenisPendaftaranRelatedByJenisPendaftaranId->addAnggotaRombelsRelatedByJenisPendaftaranId($this);
             */
        }

        return $this->aJenisPendaftaranRelatedByJenisPendaftaranId;
    }

    /**
     * Declares an association between this object and a JenisPendaftaran object.
     *
     * @param             JenisPendaftaran $v
     * @return AnggotaRombel The current object (for fluent API support)
     * @throws PropelException
     */
    public function setJenisPendaftaranRelatedByJenisPendaftaranId(JenisPendaftaran $v = null)
    {
        if ($v === null) {
            $this->setJenisPendaftaranId(NULL);
        } else {
            $this->setJenisPendaftaranId($v->getJenisPendaftaranId());
        }

        $this->aJenisPendaftaranRelatedByJenisPendaftaranId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the JenisPendaftaran object, it will not be re-added.
        if ($v !== null) {
            $v->addAnggotaRombelRelatedByJenisPendaftaranId($this);
        }


        return $this;
    }


    /**
     * Get the associated JenisPendaftaran object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return JenisPendaftaran The associated JenisPendaftaran object.
     * @throws PropelException
     */
    public function getJenisPendaftaranRelatedByJenisPendaftaranId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aJenisPendaftaranRelatedByJenisPendaftaranId === null && (($this->jenis_pendaftaran_id !== "" && $this->jenis_pendaftaran_id !== null)) && $doQuery) {
            $this->aJenisPendaftaranRelatedByJenisPendaftaranId = JenisPendaftaranQuery::create()->findPk($this->jenis_pendaftaran_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aJenisPendaftaranRelatedByJenisPendaftaranId->addAnggotaRombelsRelatedByJenisPendaftaranId($this);
             */
        }

        return $this->aJenisPendaftaranRelatedByJenisPendaftaranId;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->anggota_rombel_id = null;
        $this->rombongan_belajar_id = null;
        $this->peserta_didik_id = null;
        $this->jenis_pendaftaran_id = null;
        $this->last_update = null;
        $this->soft_delete = null;
        $this->last_sync = null;
        $this->updater_id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->aPesertaDidikRelatedByPesertaDidikId instanceof Persistent) {
              $this->aPesertaDidikRelatedByPesertaDidikId->clearAllReferences($deep);
            }
            if ($this->aPesertaDidikRelatedByPesertaDidikId instanceof Persistent) {
              $this->aPesertaDidikRelatedByPesertaDidikId->clearAllReferences($deep);
            }
            if ($this->aRombonganBelajarRelatedByRombonganBelajarId instanceof Persistent) {
              $this->aRombonganBelajarRelatedByRombonganBelajarId->clearAllReferences($deep);
            }
            if ($this->aRombonganBelajarRelatedByRombonganBelajarId instanceof Persistent) {
              $this->aRombonganBelajarRelatedByRombonganBelajarId->clearAllReferences($deep);
            }
            if ($this->aJenisPendaftaranRelatedByJenisPendaftaranId instanceof Persistent) {
              $this->aJenisPendaftaranRelatedByJenisPendaftaranId->clearAllReferences($deep);
            }
            if ($this->aJenisPendaftaranRelatedByJenisPendaftaranId instanceof Persistent) {
              $this->aJenisPendaftaranRelatedByJenisPendaftaranId->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        $this->aPesertaDidikRelatedByPesertaDidikId = null;
        $this->aPesertaDidikRelatedByPesertaDidikId = null;
        $this->aRombonganBelajarRelatedByRombonganBelajarId = null;
        $this->aRombonganBelajarRelatedByRombonganBelajarId = null;
        $this->aJenisPendaftaranRelatedByJenisPendaftaranId = null;
        $this->aJenisPendaftaranRelatedByJenisPendaftaranId = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(AnggotaRombelPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
