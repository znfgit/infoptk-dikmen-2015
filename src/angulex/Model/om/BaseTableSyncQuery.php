<?php

namespace angulex\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\TableSync;
use angulex\Model\TableSyncLog;
use angulex\Model\TableSyncPeer;
use angulex\Model\TableSyncQuery;
use angulex\Model\WtDstSyncLog;
use angulex\Model\WtSrcSyncLog;

/**
 * Base class that represents a query for the 'ref.table_sync' table.
 *
 * 
 *
 * @method TableSyncQuery orderByTableName($order = Criteria::ASC) Order by the table_name column
 * @method TableSyncQuery orderBySyncType($order = Criteria::ASC) Order by the sync_type column
 * @method TableSyncQuery orderBySyncSeq($order = Criteria::ASC) Order by the sync_seq column
 *
 * @method TableSyncQuery groupByTableName() Group by the table_name column
 * @method TableSyncQuery groupBySyncType() Group by the sync_type column
 * @method TableSyncQuery groupBySyncSeq() Group by the sync_seq column
 *
 * @method TableSyncQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method TableSyncQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method TableSyncQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method TableSyncQuery leftJoinWtSrcSyncLogRelatedByTableName($relationAlias = null) Adds a LEFT JOIN clause to the query using the WtSrcSyncLogRelatedByTableName relation
 * @method TableSyncQuery rightJoinWtSrcSyncLogRelatedByTableName($relationAlias = null) Adds a RIGHT JOIN clause to the query using the WtSrcSyncLogRelatedByTableName relation
 * @method TableSyncQuery innerJoinWtSrcSyncLogRelatedByTableName($relationAlias = null) Adds a INNER JOIN clause to the query using the WtSrcSyncLogRelatedByTableName relation
 *
 * @method TableSyncQuery leftJoinWtSrcSyncLogRelatedByTableName($relationAlias = null) Adds a LEFT JOIN clause to the query using the WtSrcSyncLogRelatedByTableName relation
 * @method TableSyncQuery rightJoinWtSrcSyncLogRelatedByTableName($relationAlias = null) Adds a RIGHT JOIN clause to the query using the WtSrcSyncLogRelatedByTableName relation
 * @method TableSyncQuery innerJoinWtSrcSyncLogRelatedByTableName($relationAlias = null) Adds a INNER JOIN clause to the query using the WtSrcSyncLogRelatedByTableName relation
 *
 * @method TableSyncQuery leftJoinTableSyncLogRelatedByTableName($relationAlias = null) Adds a LEFT JOIN clause to the query using the TableSyncLogRelatedByTableName relation
 * @method TableSyncQuery rightJoinTableSyncLogRelatedByTableName($relationAlias = null) Adds a RIGHT JOIN clause to the query using the TableSyncLogRelatedByTableName relation
 * @method TableSyncQuery innerJoinTableSyncLogRelatedByTableName($relationAlias = null) Adds a INNER JOIN clause to the query using the TableSyncLogRelatedByTableName relation
 *
 * @method TableSyncQuery leftJoinTableSyncLogRelatedByTableName($relationAlias = null) Adds a LEFT JOIN clause to the query using the TableSyncLogRelatedByTableName relation
 * @method TableSyncQuery rightJoinTableSyncLogRelatedByTableName($relationAlias = null) Adds a RIGHT JOIN clause to the query using the TableSyncLogRelatedByTableName relation
 * @method TableSyncQuery innerJoinTableSyncLogRelatedByTableName($relationAlias = null) Adds a INNER JOIN clause to the query using the TableSyncLogRelatedByTableName relation
 *
 * @method TableSyncQuery leftJoinWtDstSyncLogRelatedByTableName($relationAlias = null) Adds a LEFT JOIN clause to the query using the WtDstSyncLogRelatedByTableName relation
 * @method TableSyncQuery rightJoinWtDstSyncLogRelatedByTableName($relationAlias = null) Adds a RIGHT JOIN clause to the query using the WtDstSyncLogRelatedByTableName relation
 * @method TableSyncQuery innerJoinWtDstSyncLogRelatedByTableName($relationAlias = null) Adds a INNER JOIN clause to the query using the WtDstSyncLogRelatedByTableName relation
 *
 * @method TableSyncQuery leftJoinWtDstSyncLogRelatedByTableName($relationAlias = null) Adds a LEFT JOIN clause to the query using the WtDstSyncLogRelatedByTableName relation
 * @method TableSyncQuery rightJoinWtDstSyncLogRelatedByTableName($relationAlias = null) Adds a RIGHT JOIN clause to the query using the WtDstSyncLogRelatedByTableName relation
 * @method TableSyncQuery innerJoinWtDstSyncLogRelatedByTableName($relationAlias = null) Adds a INNER JOIN clause to the query using the WtDstSyncLogRelatedByTableName relation
 *
 * @method TableSync findOne(PropelPDO $con = null) Return the first TableSync matching the query
 * @method TableSync findOneOrCreate(PropelPDO $con = null) Return the first TableSync matching the query, or a new TableSync object populated from the query conditions when no match is found
 *
 * @method TableSync findOneBySyncType(string $sync_type) Return the first TableSync filtered by the sync_type column
 * @method TableSync findOneBySyncSeq(string $sync_seq) Return the first TableSync filtered by the sync_seq column
 *
 * @method array findByTableName(string $table_name) Return TableSync objects filtered by the table_name column
 * @method array findBySyncType(string $sync_type) Return TableSync objects filtered by the sync_type column
 * @method array findBySyncSeq(string $sync_seq) Return TableSync objects filtered by the sync_seq column
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseTableSyncQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseTableSyncQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Dapodikmen', $modelName = 'angulex\\Model\\TableSync', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new TableSyncQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   TableSyncQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return TableSyncQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof TableSyncQuery) {
            return $criteria;
        }
        $query = new TableSyncQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   TableSync|TableSync[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = TableSyncPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(TableSyncPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 TableSync A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByTableName($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 TableSync A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [table_name], [sync_type], [sync_seq] FROM [ref].[table_sync] WHERE [table_name] = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new TableSync();
            $obj->hydrate($row);
            TableSyncPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return TableSync|TableSync[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|TableSync[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return TableSyncQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(TableSyncPeer::TABLE_NAME, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return TableSyncQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(TableSyncPeer::TABLE_NAME, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the table_name column
     *
     * Example usage:
     * <code>
     * $query->filterByTableName('fooValue');   // WHERE table_name = 'fooValue'
     * $query->filterByTableName('%fooValue%'); // WHERE table_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tableName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TableSyncQuery The current query, for fluid interface
     */
    public function filterByTableName($tableName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tableName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tableName)) {
                $tableName = str_replace('*', '%', $tableName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TableSyncPeer::TABLE_NAME, $tableName, $comparison);
    }

    /**
     * Filter the query on the sync_type column
     *
     * Example usage:
     * <code>
     * $query->filterBySyncType('fooValue');   // WHERE sync_type = 'fooValue'
     * $query->filterBySyncType('%fooValue%'); // WHERE sync_type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $syncType The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TableSyncQuery The current query, for fluid interface
     */
    public function filterBySyncType($syncType = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($syncType)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $syncType)) {
                $syncType = str_replace('*', '%', $syncType);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TableSyncPeer::SYNC_TYPE, $syncType, $comparison);
    }

    /**
     * Filter the query on the sync_seq column
     *
     * Example usage:
     * <code>
     * $query->filterBySyncSeq(1234); // WHERE sync_seq = 1234
     * $query->filterBySyncSeq(array(12, 34)); // WHERE sync_seq IN (12, 34)
     * $query->filterBySyncSeq(array('min' => 12)); // WHERE sync_seq >= 12
     * $query->filterBySyncSeq(array('max' => 12)); // WHERE sync_seq <= 12
     * </code>
     *
     * @param     mixed $syncSeq The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TableSyncQuery The current query, for fluid interface
     */
    public function filterBySyncSeq($syncSeq = null, $comparison = null)
    {
        if (is_array($syncSeq)) {
            $useMinMax = false;
            if (isset($syncSeq['min'])) {
                $this->addUsingAlias(TableSyncPeer::SYNC_SEQ, $syncSeq['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($syncSeq['max'])) {
                $this->addUsingAlias(TableSyncPeer::SYNC_SEQ, $syncSeq['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TableSyncPeer::SYNC_SEQ, $syncSeq, $comparison);
    }

    /**
     * Filter the query by a related WtSrcSyncLog object
     *
     * @param   WtSrcSyncLog|PropelObjectCollection $wtSrcSyncLog  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 TableSyncQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByWtSrcSyncLogRelatedByTableName($wtSrcSyncLog, $comparison = null)
    {
        if ($wtSrcSyncLog instanceof WtSrcSyncLog) {
            return $this
                ->addUsingAlias(TableSyncPeer::TABLE_NAME, $wtSrcSyncLog->getTableName(), $comparison);
        } elseif ($wtSrcSyncLog instanceof PropelObjectCollection) {
            return $this
                ->useWtSrcSyncLogRelatedByTableNameQuery()
                ->filterByPrimaryKeys($wtSrcSyncLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByWtSrcSyncLogRelatedByTableName() only accepts arguments of type WtSrcSyncLog or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the WtSrcSyncLogRelatedByTableName relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return TableSyncQuery The current query, for fluid interface
     */
    public function joinWtSrcSyncLogRelatedByTableName($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('WtSrcSyncLogRelatedByTableName');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'WtSrcSyncLogRelatedByTableName');
        }

        return $this;
    }

    /**
     * Use the WtSrcSyncLogRelatedByTableName relation WtSrcSyncLog object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\WtSrcSyncLogQuery A secondary query class using the current class as primary query
     */
    public function useWtSrcSyncLogRelatedByTableNameQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinWtSrcSyncLogRelatedByTableName($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'WtSrcSyncLogRelatedByTableName', '\angulex\Model\WtSrcSyncLogQuery');
    }

    /**
     * Filter the query by a related WtSrcSyncLog object
     *
     * @param   WtSrcSyncLog|PropelObjectCollection $wtSrcSyncLog  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 TableSyncQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByWtSrcSyncLogRelatedByTableName($wtSrcSyncLog, $comparison = null)
    {
        if ($wtSrcSyncLog instanceof WtSrcSyncLog) {
            return $this
                ->addUsingAlias(TableSyncPeer::TABLE_NAME, $wtSrcSyncLog->getTableName(), $comparison);
        } elseif ($wtSrcSyncLog instanceof PropelObjectCollection) {
            return $this
                ->useWtSrcSyncLogRelatedByTableNameQuery()
                ->filterByPrimaryKeys($wtSrcSyncLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByWtSrcSyncLogRelatedByTableName() only accepts arguments of type WtSrcSyncLog or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the WtSrcSyncLogRelatedByTableName relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return TableSyncQuery The current query, for fluid interface
     */
    public function joinWtSrcSyncLogRelatedByTableName($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('WtSrcSyncLogRelatedByTableName');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'WtSrcSyncLogRelatedByTableName');
        }

        return $this;
    }

    /**
     * Use the WtSrcSyncLogRelatedByTableName relation WtSrcSyncLog object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\WtSrcSyncLogQuery A secondary query class using the current class as primary query
     */
    public function useWtSrcSyncLogRelatedByTableNameQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinWtSrcSyncLogRelatedByTableName($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'WtSrcSyncLogRelatedByTableName', '\angulex\Model\WtSrcSyncLogQuery');
    }

    /**
     * Filter the query by a related TableSyncLog object
     *
     * @param   TableSyncLog|PropelObjectCollection $tableSyncLog  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 TableSyncQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByTableSyncLogRelatedByTableName($tableSyncLog, $comparison = null)
    {
        if ($tableSyncLog instanceof TableSyncLog) {
            return $this
                ->addUsingAlias(TableSyncPeer::TABLE_NAME, $tableSyncLog->getTableName(), $comparison);
        } elseif ($tableSyncLog instanceof PropelObjectCollection) {
            return $this
                ->useTableSyncLogRelatedByTableNameQuery()
                ->filterByPrimaryKeys($tableSyncLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByTableSyncLogRelatedByTableName() only accepts arguments of type TableSyncLog or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the TableSyncLogRelatedByTableName relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return TableSyncQuery The current query, for fluid interface
     */
    public function joinTableSyncLogRelatedByTableName($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('TableSyncLogRelatedByTableName');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'TableSyncLogRelatedByTableName');
        }

        return $this;
    }

    /**
     * Use the TableSyncLogRelatedByTableName relation TableSyncLog object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\TableSyncLogQuery A secondary query class using the current class as primary query
     */
    public function useTableSyncLogRelatedByTableNameQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTableSyncLogRelatedByTableName($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'TableSyncLogRelatedByTableName', '\angulex\Model\TableSyncLogQuery');
    }

    /**
     * Filter the query by a related TableSyncLog object
     *
     * @param   TableSyncLog|PropelObjectCollection $tableSyncLog  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 TableSyncQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByTableSyncLogRelatedByTableName($tableSyncLog, $comparison = null)
    {
        if ($tableSyncLog instanceof TableSyncLog) {
            return $this
                ->addUsingAlias(TableSyncPeer::TABLE_NAME, $tableSyncLog->getTableName(), $comparison);
        } elseif ($tableSyncLog instanceof PropelObjectCollection) {
            return $this
                ->useTableSyncLogRelatedByTableNameQuery()
                ->filterByPrimaryKeys($tableSyncLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByTableSyncLogRelatedByTableName() only accepts arguments of type TableSyncLog or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the TableSyncLogRelatedByTableName relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return TableSyncQuery The current query, for fluid interface
     */
    public function joinTableSyncLogRelatedByTableName($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('TableSyncLogRelatedByTableName');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'TableSyncLogRelatedByTableName');
        }

        return $this;
    }

    /**
     * Use the TableSyncLogRelatedByTableName relation TableSyncLog object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\TableSyncLogQuery A secondary query class using the current class as primary query
     */
    public function useTableSyncLogRelatedByTableNameQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTableSyncLogRelatedByTableName($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'TableSyncLogRelatedByTableName', '\angulex\Model\TableSyncLogQuery');
    }

    /**
     * Filter the query by a related WtDstSyncLog object
     *
     * @param   WtDstSyncLog|PropelObjectCollection $wtDstSyncLog  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 TableSyncQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByWtDstSyncLogRelatedByTableName($wtDstSyncLog, $comparison = null)
    {
        if ($wtDstSyncLog instanceof WtDstSyncLog) {
            return $this
                ->addUsingAlias(TableSyncPeer::TABLE_NAME, $wtDstSyncLog->getTableName(), $comparison);
        } elseif ($wtDstSyncLog instanceof PropelObjectCollection) {
            return $this
                ->useWtDstSyncLogRelatedByTableNameQuery()
                ->filterByPrimaryKeys($wtDstSyncLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByWtDstSyncLogRelatedByTableName() only accepts arguments of type WtDstSyncLog or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the WtDstSyncLogRelatedByTableName relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return TableSyncQuery The current query, for fluid interface
     */
    public function joinWtDstSyncLogRelatedByTableName($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('WtDstSyncLogRelatedByTableName');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'WtDstSyncLogRelatedByTableName');
        }

        return $this;
    }

    /**
     * Use the WtDstSyncLogRelatedByTableName relation WtDstSyncLog object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\WtDstSyncLogQuery A secondary query class using the current class as primary query
     */
    public function useWtDstSyncLogRelatedByTableNameQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinWtDstSyncLogRelatedByTableName($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'WtDstSyncLogRelatedByTableName', '\angulex\Model\WtDstSyncLogQuery');
    }

    /**
     * Filter the query by a related WtDstSyncLog object
     *
     * @param   WtDstSyncLog|PropelObjectCollection $wtDstSyncLog  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 TableSyncQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByWtDstSyncLogRelatedByTableName($wtDstSyncLog, $comparison = null)
    {
        if ($wtDstSyncLog instanceof WtDstSyncLog) {
            return $this
                ->addUsingAlias(TableSyncPeer::TABLE_NAME, $wtDstSyncLog->getTableName(), $comparison);
        } elseif ($wtDstSyncLog instanceof PropelObjectCollection) {
            return $this
                ->useWtDstSyncLogRelatedByTableNameQuery()
                ->filterByPrimaryKeys($wtDstSyncLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByWtDstSyncLogRelatedByTableName() only accepts arguments of type WtDstSyncLog or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the WtDstSyncLogRelatedByTableName relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return TableSyncQuery The current query, for fluid interface
     */
    public function joinWtDstSyncLogRelatedByTableName($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('WtDstSyncLogRelatedByTableName');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'WtDstSyncLogRelatedByTableName');
        }

        return $this;
    }

    /**
     * Use the WtDstSyncLogRelatedByTableName relation WtDstSyncLog object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\WtDstSyncLogQuery A secondary query class using the current class as primary query
     */
    public function useWtDstSyncLogRelatedByTableNameQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinWtDstSyncLogRelatedByTableName($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'WtDstSyncLogRelatedByTableName', '\angulex\Model\WtDstSyncLogQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   TableSync $tableSync Object to remove from the list of results
     *
     * @return TableSyncQuery The current query, for fluid interface
     */
    public function prune($tableSync = null)
    {
        if ($tableSync) {
            $this->addUsingAlias(TableSyncPeer::TABLE_NAME, $tableSync->getTableName(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
